﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Command.AccountingObjects.Documents;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AODecisionsToCreate
{
    public class DeleteAODecisionToCreateCommand : ICommand<DeleteAODecisionToCreateContext>
    {
        ISessionFactoryGb factory;

        public DeleteAODecisionToCreateCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteAODecisionToCreateContext context)
        {            
            if (context.xId.HasValue && context.xId != 0)
            {
                var entity = new Repository<AODecisionToCreate>().Get(context.xId.Value);

                if (entity != null)
                {
                    using (var session = factory.Create())
                    {
                        try
                        {
                            foreach (var item in entity.Documents)
                            {
                                new DeleteDocumentCommand(factory)
                                    .Execute(new DeleteDocumentContext()
                                {
                                    xId = item.Id,
                                    Session = session
                                });
                            }

                            session.Delete(entity);

                            session.Commit();
                        }
                        catch (Exception)
                        {
                            session.Rollback();
                            throw;
                        }
                    }
                }
            }
        }
    }
}
