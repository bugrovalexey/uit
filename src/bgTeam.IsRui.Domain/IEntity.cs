﻿namespace bgTeam.IsRui.Domain
{
    using System;

    public interface IEntity
    {
        int Id { get; }
    }

    public interface IEntityName : IEntity
    {
        string Name { get; set; }
    }

    public interface IEntityCodeName : IEntityName
    {
        string Code { get; set; }

        string CodeName { get; }
    }

    public interface IPeriodDirectories : IEntity
    {
        DateTime? DateStart { get; set; }

        DateTime? DateEnd { get; set; }
    }

    //TODO : зачем этот интерфейс
    public interface ITree
    {
        int Id { get; }

        int? ParentId { get; }
    }
}