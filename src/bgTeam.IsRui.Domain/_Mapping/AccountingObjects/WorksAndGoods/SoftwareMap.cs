﻿namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects.WorksAndGoods
{
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;

    internal class SoftwareMap : ClassMapHiLo<Software>
    {
        public SoftwareMap()
            : base("OU_Software")
        {
        }

        protected override void InitMap()
        {
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
            Map(x => x.Name, "OU_Software_Name").Not.Nullable();
            References(x => x.CategoryKindOfSupport, "VO_Category_ID").Not.Nullable();
            References(x => x.Manufacturer, "Producer_ID").Nullable();
            Map(x => x.Amount, "OU_Software_Count").Not.Nullable();
            Map(x => x.Summa, "OU_Software_Sum").Not.Nullable();
            Map(x => x.Right, "OU_Software_Rights").CustomType<RightsOnSoftwareEnum>().Not.Nullable();
            Map(x => x.ManufacturerCustomName, "Manufacturer_Castom_Name").Nullable();

            HasOne(x => x.CharacteristicsOfLicense).PropertyRef(z => z.Software).Cascade.Delete();
        }
    }
}