﻿namespace GB.MKRF.Entities.Documents
{
    /// <summary>Связь бумажной формы с документом</summary>
    public class DocToPaper : EntityBase
    {
        protected DocToPaper()
        {
        }

        public DocToPaper(PaperObject PaperObject, Document Document, DocToPaperType Type)
        {
            this.PaperObject = PaperObject;
            this.Document = Document;
            this.Type = Type;
        }

        /// <summary>Бумажный план</summary>
        public virtual PaperObject PaperObject { get; set; }

        /// <summary>Документ</summary>
        public virtual Document Document { get; set; }

        /// <summary>Тип связи</summary>
        public virtual DocToPaperType Type { get; set; }

        /// <summary>Файл является экспертным заключением</summary>
        public virtual bool IsExpertConclusion { get; set; }
    }

    /// <summary>Тип связи</summary>
    public enum DocToPaperType
    {
        /// <summary>1-документ от ОГВ</summary>
        Ogv = 1,

        /// <summary>2-документ от МКС</summary>
        Mks = 2
    }
}