﻿using GB;
using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Validation
{
    class PaperObject
    {
        /// <summary>Параметры для валидации</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create)
            {
                Entities.Documents.PaperObject po = Entity as Entities.Documents.PaperObject;

                if (po != null)
                    return "0;" + po.Owner.Id.ToString() + ";" + (int)po.Owner.GetTableTypeOld();
                else
                    return "0"; 
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
