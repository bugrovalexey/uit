﻿namespace bgTeam.IsRui.Story.Common
{
    public class GetDictionaryStoryContext
    {
        public string Name { get; set; }

        public string Search { get; set; }
    }
}