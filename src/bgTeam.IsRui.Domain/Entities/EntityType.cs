﻿using System.ComponentModel;

namespace bgTeam.IsRui.Domain.Entities
{
    /// <summary>
    /// Тип объекта
    /// </summary>
    public enum EntityType
    {
        /// <summary>Мероприятие</summary>
        [Description("Мероприятие")]
        PlansActivity = 1, //было 8

        /// <summary>Объект учета</summary>
        [Description("Объект учета")]
        AccountingObject = 2, //было 41

        /// <summary>Экспертиза по мероприятию</summary>
        [Description("Экспертиза по мероприятию")]
        ActivityExpertise = 3,

        /// <summary>Документ объекта учета</summary>
        [Description("Документ объекта учета")]
        DocumentAccountingObject = 4, //было 39

        /// <summary>Другие основания реализации мероприятия</summary>
        [Description("Другое основание реализации мероприятия")]
        OtherReason = 5, //было 40

        /// <summary>Сведения о прохождении специальных проверок</summary>
        [Description("Сведения о прохождении специальных проверок")]
        SpecialCheck = 6,

        /// <summary>Основания создания ОУ</summary>
        [Description("Основания создания ОУ")]
        AODocumentGroundsForCreation = 7,

        /// <summary>Ввод в эксплуатацию ОУ</summary>
        [Description("Ввод в эксплуатацию ОУ")]
        AODocumentCommissioning = 8,

        /// <summary>Вывод из эксплуатации ОУ</summary>
        [Description("Вывод из эксплуатации ОУ")]
        AODocumentDecommissioning = 9,

        /// <summary>Акт сдачи-приемки</summary>
        [Description("Акт сдачи-приемки")]
        ActOfAcceptance = 10,

        /// <summary>Прикрепленные документы к комментарию</summary>
        [Description("Прикрепленные документы к комментарию")]
        AccountingObjectCommentDocument = 11,

        /// <summary>
        /// Заключение по статусу бизнесс процесса
        /// </summary>
        WorkflowUserConclusion = 12,

        /// <summary>
        /// Заключение по истории статуса бизнесс процесса
        /// </summary>
        WorkflowUserConclusionHistory = 13,

        /// <summary>
        /// Статус заявок
        /// </summary>
        ClaimWorkflow = 14,

        /// <summary>
        /// Документ заявки
        /// </summary>
        ClaimDocument = 15,

        /// <summary>
        /// Акт заявки
        /// </summary>
        ClaimAct = 16,

        /// <summary>
        /// Комментарий к заявке
        /// </summary>
        ClaimUserComment = 17,

        /// <summary>
        /// Акт мероприятия
        /// </summary>
        ActivityAct = 18
    }
}