﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;

    public class ActivityAddOrUpdatePurchaseStoryContext
    {
        public int? Id { get; set; }

        public int? ActivityId { get; set; }

        public int Number { get; set; }

        public Month Month { get; set; }

        public DateTime? PublishDate { get; set; }

        public DateTime? AcceptanceEndDate { get; set; }

        public DateTime? ResultDate { get; set; }
    }
}
