﻿using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Admin
{
    internal class UserRequestMap : ClassMapBase<UserRequest>
    {
        public UserRequestMap()
            : base("Users_Request")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Users_Request_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            //ApplyFilter<SecurityFilter>("Users_Request_ID in (select * from Get_Object_List(:UserId, " + (int)PortalTableEnum.UserRequest + ", 'r'))");

            //            string argsList = @"
            //                         @Users_Request_Surname = ?
            //                        ,@Users_Request_Name = ?
            //                        ,@Users_Request_Middlename = ?
            //                        ,@Users_Request_Work_Phone = ?
            //                        ,@Users_Request_Mob_Phone = ?
            //                        ,@Users_Request_Email = ?
            //                        ,@Users_Request_Job = ?
            //                        ,@Users_Request_Comm = ?
            //                        ,@Users_Request_Is_Complete = ?
            //                        ,@Users_Request_DT_Complete = ?
            //                        ,@Users_ID = ?
            //                        ,@Govt_Organ_ID = ?
            //                        ,@Users_Request_ID = ?";

            //            SqlInsert(@"exec ZZZ_Prc_Users_Request_Insert " + argsList).Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Users_Request_Update " + argsList).Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Users_Request_Delete
            //						  @Users_Request_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Surname, "Users_Request_Surname");
            Map(x => x.Name, "Users_Request_Name");
            Map(x => x.Middlename, "Users_Request_Middlename");
            Map(x => x.PhoneWork, "Users_Request_Work_Phone");
            Map(x => x.PhoneMob, "Users_Request_Mob_Phone");
            Map(x => x.Email, "Users_Request_Email");
            Map(x => x.Job, "Users_Request_Job");
            Map(x => x.Comment, "Users_Request_Comm");
            Map(x => x.IsComplete, "Users_Request_Is_Complete");
            Map(x => x.CompletionDate, "Users_Request_DT_Complete");
            Map(x => x.CreationDate, "Users_Request_DT_Create").ReadOnly();

            References<User>(x => x.Author, "Users_ID").Not.Nullable();
            References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
        }
    }
}