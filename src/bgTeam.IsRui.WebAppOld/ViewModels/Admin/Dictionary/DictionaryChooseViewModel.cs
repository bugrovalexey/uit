﻿using bgTeam.IsRui.MKRF.DictyonaryEdit;
using System.Collections.Generic;
using System.Data;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin.Dictionary
{
    public class DictionaryChooseViewModel
    {
        public List<MetaDictionaryColumn> Columns { get; set; }

        public MetaDictionaryColumn PrimaryKey { get; set; }

        public DataTable Data { get; set; }

        public string InputName { get; set; }
    }
}