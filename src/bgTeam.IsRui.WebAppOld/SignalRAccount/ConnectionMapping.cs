﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace bgTeam.IsRui.WebApp.SignalRAccount
{
    public class ConnectionMapping<T>
    {
        private readonly ConcurrentDictionary<T, Dictionary<string, DateTime>> _sessions = new ConcurrentDictionary<T, Dictionary<string, DateTime>>();        

        public int Count
        {
            get
            {
                return _sessions.Count;
            }
        }

        public void Add(T key, string connectionId, DateTime date)
        {            
            Dictionary<string, DateTime> connections;
            if (!_sessions.TryGetValue(key, out connections))            
            {
                connections = new Dictionary<string, DateTime>();                
                _sessions.TryAdd(key, connections);                
            }
                
            connections.Add(connectionId, date);                
        }

        public Dictionary<string, DateTime> GetConnections(T key)
        {
            Dictionary<string, DateTime> connections;            
            if (_sessions.TryGetValue(key, out connections))
            {
                return connections;
            }

            return new Dictionary<string, DateTime>();
        }

        public void Remove(T key, string connectionId)
        {            
            Dictionary<string, DateTime> connections;                
            if (!_sessions.TryGetValue(key, out connections))
            {
                return;
            }
                
            connections.Remove(connectionId);

            if (connections.Count == 0)
            {                        
                Dictionary<string, DateTime> retVal;
                _sessions.TryRemove(key, out retVal);
            }                
        }

        public void RemoveAllExcept(T key, string exceptConnectionId)
        {            
            Dictionary<string, DateTime> connections;                
            if (!_sessions.TryGetValue(key, out connections))
            {
                return;
            }
                                   
            DateTime date;
            if (connections.TryGetValue(exceptConnectionId, out date))
            {
                connections.Clear();
                connections.Add(exceptConnectionId, date);
                return;
            }

            connections.Clear();                
        }
    }
}