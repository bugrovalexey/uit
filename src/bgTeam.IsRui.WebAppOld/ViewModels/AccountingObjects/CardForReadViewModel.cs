﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.Documents;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.MKRF.Logic.AccountingObjectActions;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class CardForReadViewModel : AccountingObjectBase
    {
        public static readonly string NOT_DATA = "Нет данных"; 

        public string ShortName
        {
            get { return Current.ShortName; }
        }

        public string FullName
        {
            get { return Current.FullName; }
        }

        public string Number
        {
            get { return Current.Number.ReturnThisOrDefault(NOT_DATA); }
        }

        public string Kind
        {
            get { return Current.Kind.Name.ReturnThisOrDefault(NOT_DATA); }
        }

        public string Status
        {
            get { return Current.Status.Name; }
        }

        public string Stage
        {
            get { return Current.Stage.GetDescription(); }
        }

        public string AdoptionBudgetDate
        {
            get { return Current.AdoptionBudgetDate.ToStringOrDefault(); }
        }

        public string BalanceCost
        {
            get { return Current.BalanceCost.ToMoneyString().ReturnThisOrDefault(NOT_DATA); }
        }

        public string AdoptionBudgetBasis
        {
            get 
            { 
                return Current.AdoptionBudgetBasis.ReturnThisOrDefault(NOT_DATA); 
            }
        }

        public string TotalCost { get; set; }

        public string CreateCost { get; set; }

        public string DevelopCost { get; set; }

        public string ExploitationCost { get; set; }

        public string TotalCostFact { get; set; }

        public string CreateCostFact { get; set; }

        public string DevelopCostFact { get; set; }

        public string ExploitationCostFact { get; set; }

        public string Targets
        {
            get { return Current.Targets.ReturnThisOrDefault(NOT_DATA); }
        }

        public string PurposeAndScope
        {
            get { return Current.PurposeAndScope.ReturnThisOrDefault(NOT_DATA); }
        }

        public string KindName
        {
            get { return Current.Kind.Return(k => k.Name, default(string)).ReturnThisOrDefault(NOT_DATA); }
        }

        public IEnumerable<TableAODecisionToCreate> DecisionsToCreate { get; set; }

        public string GroundsForCreationDateAdoption
        {
            get { return Current.AdoptionBudgetDate.ToDisplay().ReturnThisOrDefault(NOT_DATA); }
        }

        public List<ResponsibleDescription> Responsibles { get; set; }

        private void AddToResponsible(Responsible responsible, string description)
        {
            if (responsible != null)
            {
                Responsibles.Add(new ResponsibleDescription(responsible, description));
            }
        }

        public string CodeBK
        {
            get
            {
                return Current.CodeBK.ReturnThisOrDefault(NOT_DATA);
            }
        }

        public Department ResponsibleDepartment
        {
            get { return Current.ResponsibleDepartment; }
        }

        public AOExploitationDepartment ExploitationDepartment
        {
            get { return Current.ExploitationDepartment; }
        }

        public string LocationOrgName { get { return Current.LocationOrgName.ReturnThisOrDefault(NOT_DATA); } }

        public string LocationFioResponsible { get { return Current.LocationFioResponsible.ReturnThisOrDefault(NOT_DATA); } }

        public string LocationOrgAddress { get { return Current.LocationOrgAddress.ReturnThisOrDefault(NOT_DATA); } }

        public string LocationOrgPhone { get { return Current.LocationOrgPhone.ReturnThisOrDefault(NOT_DATA); } }



        #region Ввод в эксплуатацию


        public AODocumentCommissioning DocumentCommissioning { get { return Current.DocumentCommissioning; } }

        public string CommissioningDocName
        {
            get { return CommissioningFileLink ?? DocumentCommissioning.Return(dc => dc.Name, default(string)).ReturnThisOrDefault(NOT_DATA); }
        }

        public string CommissioningDocNumber
        {
            get { return DocumentCommissioning.Return(dc => dc.Number, default(string)).ReturnThisOrDefault(NOT_DATA); }
        }

        public string CommissioningDocParagraph
        {
            get { return DocumentCommissioning.Return(dc => dc.Paragraph, default(string)).ReturnThisOrDefault(NOT_DATA); }
        }

        public string CommissioningDocDateAdoption
        {
            get { return DocumentCommissioning.Return(dc => dc.DateAdoption, default(DateTime?)).ToStringOrDefault().ReturnThisOrDefault(NOT_DATA); }
        }

        public string CommissioningDate
        {
            get { return Current.CommissioningDate.ToStringOrDefault().ReturnThisOrDefault(NOT_DATA); }
        }

        public string CommissioningFilenameExtension { get; set; }

        public string CommissioningFileLink { get; set; }


        #endregion Ввод в эксплуатацию



        #region Основания для принятия к бюджетному учету

        public AODocumentGroundsForCreation DocumentGroundsForCreation { get { return Current.DocumentGroundsForCreation; } }

        public string GroundsForCreationDocName
        {
            get { return GroundsForCreationFileLink ?? DocumentGroundsForCreation.Return(dc => dc.Name, default(string)).ReturnThisOrDefault(NOT_DATA); }
        }

        public string GroundsForCreationDocNumber
        {
            get { return DocumentGroundsForCreation.Return(dc => dc.Number, default(string)).ReturnThisOrDefault(NOT_DATA); }
        }

        public string GroundsForCreationDocParagraph
        {
            get { return DocumentGroundsForCreation.Return(dc => dc.Paragraph, default(string)).ReturnThisOrDefault(NOT_DATA); }
        }

        public string GroundsForCreationDocDateAdoption
        {
            get { return DocumentGroundsForCreation.Return(dc => dc.DateAdoption, default(DateTime?)).ToStringOrDefault().ReturnThisOrDefault(NOT_DATA); }
        }

        public string GroundsForCreationFilenameExtension { get; set; }

        public string GroundsForCreationFileLink { get; set; }

        #endregion Основания для принятия к бюджетному учету



        #region Вывод из эксплуатации

        public AODocumentDecommissioning DocumentDecommissioning { get { return Current.DocumentDecommissioning; } }

        public string DecommissioningDocName
        {
            get { return DecommissioningFileLink ?? DocumentDecommissioning.Return(dc => dc.Name, default(string)).ReturnThisOrDefault(NOT_DATA); }
        }

        public string DecommissioningDocNumber
        {
            get { return DocumentDecommissioning.Return(dc => dc.Number, default(string)).ReturnThisOrDefault(NOT_DATA); }
        }

        public string DecommissioningDocDateAdoption
        {
            get { return DocumentDecommissioning.Return(dc => dc.DateAdoption, default(DateTime?)).ToStringOrDefault().ReturnThisOrDefault(NOT_DATA); }
        }

        public string DecommissioningDate
        {
            get { return Current.DecommissioningDate.ToStringOrDefault().ReturnThisOrDefault(NOT_DATA); }
        }

        public string DecommissioningReasons
        {
            get { return Current.DecommissioningReasons.ReturnThisOrDefault(NOT_DATA); }
        }

        public string DecommissioningFilenameExtension { get; set; }

        public string DecommissioningFileLink { get; set; }

        #endregion Вывод из эксплуатации


        public IEnumerable<TablePurchase> Purchases { get; set; }

        public string InformAboutActivityGovOrgan
        {
            get { return Current.InformAboutActivityGovOrgan.ToRussian(); }
        }

        public IEnumerable<TableAOGovServices> GovServices { get; set; }

        public IEnumerable<AoActivity> Activities { get; set; }

        public IEnumerable<TableGovernmentContract> Contracts { get; set; }

        public IEnumerable<TableActOfAcceptance> Acts { get; set; }

        public CardForReadViewModel(int id)
            : base(id)
        {
            var handler = Singleton<ManagerActions>.Instance.GetHandler(Current.Stage);

            ActionsList = handler.GetActionsList().Select(x => string.Format(x.Template, x.Name, LinkBuilder.Url(x.Action, x.Controller, new { Id = Current.Id })));

            KindList = new SelectList(new IKTComponentRepository().GetAllWhoNotHaveChildren(), "Id", "Name");

            DecisionsToCreate =
                new RepositoryDc<AODecisionToCreate>().GetAllEx(
                    q => q.Fetch(x => x.Documents).Eager.Where(x => x.AccountingObject.Id == id))
                    .Select(doc => new TableAODecisionToCreate(doc));

            DepartmentList = new SelectList(new RepositoryDc<Department>().GetAllEx(q => q.OrderBy(x => x.Code)), "Id", "Name");

            ResponsibleCoord =
                Current.ResponsibleCoord.Return(
                    x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.Name } }, default(List<IEntityName>));

            ResponsibleInformation =
                Current.ResponsibleInformation.Return(
                    x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.Name } }, default(List<IEntityName>));



            LevelsOfSecurity = new SelectList(EnumHelper.ToDictionary<LevelOfSecurity>(), "Key", "Value");

            SpecChecks =
                new RepositoryDc<SpecialCheck>().GetAllEx(
                    q => q.Fetch(x => x.Documents).Eager.Where(x => x.AccountingObject.Id == id))
                    .Select(x => new TableSpecCheck(x));

            ResponsiblePurchase =
                Current.ResponsiblePurchase.Return(
                    x => new List<IEntityName>() {new SelectEntityInfo() {Id = x.Id, Name = x.Name}}, default(List<IEntityName>));


            var commissioningDoc = DocumentCommissioning.With(dc => dc.Documents.FirstOrDefault());

            commissioningDoc.Do(doc =>
            {
                CommissioningFilenameExtension = doc.FileName.GetExtension();
                CommissioningFileLink = LinkBuilder.Link(DocumentCommissioning.Name, doc.Id);
            });

            var groundsForCreationDoc = DocumentGroundsForCreation.With(dc => dc.Documents.FirstOrDefault());

            groundsForCreationDoc.Do(doc => 
            {
                GroundsForCreationFilenameExtension = doc.FileName.GetExtension();
                GroundsForCreationFileLink = LinkBuilder.Link(DocumentGroundsForCreation.Name, doc.Id);
            });

            var decommissioningDoc = DocumentDecommissioning.With(dc => dc.Documents.FirstOrDefault());

            decommissioningDoc.Do(doc =>
            {
                DecommissioningFilenameExtension = doc.FileName.GetExtension();
                DecommissioningFileLink = LinkBuilder.Link(DocumentDecommissioning.Name, doc.Id);
            });

            BK_GRBS = Current.BK_GRBS.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });
            BK_PZRZ = Current.BK_PZRZ.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });
            BK_CSR = Current.BK_CSR.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });
            BK_WR = Current.BK_WR.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });


            Responsibles = new List<ResponsibleDescription>();
            AddToResponsible(Current.ResponsibleCoord, "Ответственный за координацию мероприятий по информатизации");
            AddToResponsible(Current.ResponsibleExploitation, "Ответственный за обеспечение эксплуатации");
            AddToResponsible(Current.ResponsiblePurchase, "Ответственный за организацию закупок");
            AddToResponsible(Current.ResponsibleInformation, "Ответственный за размещение сведений об объекте учета");

            //Списки по всем ОУ в ветке
            var aoIds = new AccountingObjectRepository().GetAllChildrensIds(id).ToList();

            TechnicalSupports =
                new RepositoryDc<TechnicalSupport>().GetAllEx(
                    q => q.WhereRestrictionOn(x => x.AccountingObject.Id).IsIn(aoIds))
                    .Select(x => new TableTechnicalSupport(x));

            Softwares =
                 new RepositoryDc<Software>().GetAllEx(
                    q => q.WhereRestrictionOn(x => x.AccountingObject.Id).IsIn(aoIds))
                .Select(x => new TableSoftware(x));

            WorksAndServices =
                new RepositoryDc<WorkAndService>().GetAllEx(
                    q => q.WhereRestrictionOn(x => x.AccountingObject.Id).IsIn(aoIds))
                .Select(x => new TableWorkAndService(x));

            Characteristics = new AccountingObjectCharacteristicsRepository()
                .GetAllByAccountingObjects(aoIds).Select(x => new
                    TableCharacteristic(x));

            Activities = new RepositoryDc<AoActivity>().GetAllEx(query => query
                .WhereRestrictionOn(x => x.AccountingObject.Id).IsIn(aoIds));

            GovServices =
                new RepositoryDc<AOGovService>().GetAllEx(q => q.WhereRestrictionOn(x => x.AccountingObject.Id).IsIn(aoIds))
                    .Select(x => new TableAOGovServices(x));

            InternalIsAndComponentsItki =
                new RepositoryDc<InternalIsAndComponentsItki>().GetAllEx(
                    q =>
                        q.Fetch(x => x.Object).Eager
                            .Fetch(x => x.Object.ResponsibleDepartment).Eager
                            .Fetch(x => x.Object.Kind).Eager
                            .WhereRestrictionOn(x => x.Owner.Id).IsIn(aoIds))
                    .Select(x => new TableInternalIsAndComponentItki(x));

            ExternalIsAndComponentsItki =
                new RepositoryDc<ExternalIsAndComponentsItki>().GetAllEx(
                    q => q.WhereRestrictionOn(x => x.Owner.Id).IsIn(aoIds))
                    .Select(x => new TableExternalIsAndComponentItki(x));

            ExternalUserInterfacesOfIs =
                new RepositoryDc<ExternalUserInterfaceOfIs>().GetAllEx(
                    q => q.Fetch(x => x.TypeOfInterface).Eager.WhereRestrictionOn(x => x.Owner.Id).IsIn(aoIds))
                    .Select(x => new TableExternalUserInterfaceOfIs(x));

            Contracts = new RepositoryDc<GovContract>().GetAllEx(query => query
                .WhereRestrictionOn(x => x.AccountingObject.Id).IsIn(aoIds))
                .Select(x => new TableGovernmentContract(x));

            GovContract contr = null;

            Acts = new RepositoryDc<ActOfAcceptance>().GetAllEx(q => q.JoinAlias(x => x.GovContract, () => contr)
                .WhereRestrictionOn(x => contr.AccountingObject.Id).IsIn(aoIds))
                .Select(x => new TableActOfAcceptance(x));

            Purchases = new RepositoryDc<GovPurchase>().GetAllEx(query => query
                .WhereRestrictionOn(x => x.AccountingObject.Id).IsIn(aoIds))
                .Select(x => new TablePurchase(x));


            //Данные из списков

            var createCost =
                Activities.Where(x => x.ActivityType == ActivityTypeEnum.Create).Select(x => x.CostY0).Sum();
            CreateCost = createCost.ToMoneyString();

            var developCost =
                Activities.Where(x => x.ActivityType == ActivityTypeEnum.Develop).Select(x => x.CostY0).Sum();
            DevelopCost = developCost.ToMoneyString();

            var exploitationCost =
                Activities.Where(x => x.ActivityType == ActivityTypeEnum.Exploitation).Select(x => x.CostY0).Sum();
            ExploitationCost = exploitationCost.ToMoneyString();

            TotalCost = (createCost + developCost + exploitationCost).ToMoneyString();


            var createCostFact =
                Acts.Where(x => x.ActivityType == ActivityTypeEnum.Create).Select(x => x.CostMoney).Sum();
            CreateCostFact = createCostFact.ToMoneyString();

            var developCostFact =
                Acts.Where(x => x.ActivityType == ActivityTypeEnum.Develop).Select(x => x.CostMoney).Sum();
            DevelopCostFact = developCostFact.ToMoneyString();

            var exploitationCostFact =
                Acts.Where(x => x.ActivityType == ActivityTypeEnum.Exploitation).Select(x => x.CostMoney).Sum();
            ExploitationCostFact = exploitationCostFact.ToMoneyString();

            TotalCostFact = (createCostFact + developCostFact + exploitationCostFact).ToMoneyString();
        }


        [DisplayName(KindCaption)]
        public int? KindId 
        {
            get { return Current.Kind.Return(k => k.Id, default(int)); }
        }

        public SelectList KindList { get; set; }


        public int ResponsibleDepartmentId
        {
            get { return Current.ResponsibleDepartment.Return(d => d.Id, default(int)); }
        }

        public SelectList DepartmentList { get; set; }

        [DisplayName(ResponsibleCoordCaption)]
        public IList<IEntityName> ResponsibleCoord { get; private set; }

        [DisplayName(ResponsibleInformationCaption)]
        public IList<IEntityName> ResponsibleInformation { get; private set; }

        public IEnumerable<TableTechnicalSupport> TechnicalSupports { get; set; }

        public IEnumerable<TableSoftware> Softwares { get; set; }

        public IEnumerable<TableWorkAndService> WorksAndServices { get; set; }

        public IEnumerable<TableCharacteristic> Characteristics { get; set; }

        [DisplayName(LevelOfSecurityCaption)]
        public LevelOfSecurity? LevelOfSecurity
        {
            get { return Current.Return(ao => ao.LevelOfSecurity, default(LevelOfSecurity?)); }
        }

        public SelectList LevelsOfSecurity { get; set; }

        public IEnumerable<TableSpecCheck> SpecChecks { get; set; }

        public IEnumerable<TableInternalIsAndComponentItki> InternalIsAndComponentsItki { get; set; }

        public IEnumerable<TableExternalIsAndComponentItki> ExternalIsAndComponentsItki { get; set; }

        public IEnumerable<TableExternalUserInterfaceOfIs> ExternalUserInterfacesOfIs { get; set; }

        [DisplayName(ResponsiblePurchaseCaption)]
        public IList<IEntityName> ResponsiblePurchase { get; private set; }

        [DisplayName(GRBSCaption)]
        public IList<IEntityName> BK_GRBS { get; private set; }

        [DisplayName(PZRZCaption)]
        public IList<IEntityName> BK_PZRZ { get; private set; }

        [DisplayName(CSRCaption)]
        public IList<IEntityName> BK_CSR { get; private set; }

        [DisplayName(WorkFormCaption)]
        public IList<IEntityName> BK_WR { get; private set; }

        public SelectList KOSGUList { get; set; }

        //public IEnumerable<AoActivity> Activities { get; set; }

        public IEnumerable<string> ActionsList { get; set; }


        #region Consts

        private const string ShortNameCaption = "Краткое наименование";
        private const string FullNameCaption = "Полное наименование";
        private const string KindCaption = "Классификационная категория ОУ";
        private const string TargetsCaption = "Цели создания объекта учета";
        private const string PurposeAndScopeCaption = "Назначение и область применения объекта учета";
        private const string ResponsibleCoordCaption = "Ответственный за координацию мероприятий по информатизации";
        private const string ResponsibleInformationCaption = "Ответственный за размещение сведений об объекте учета";
        private const string LevelOfSecurityCaption = "Уровень защищенности";
        private const string ResponsiblePurchaseCaption = "Ответственный за организацию закупок";
        private const string DocNameCaption = "Наименование";
        private const string DocNumberCaption = "Номер документа";
        private const string DocDateAdoptionCaption = "Дата подписания";
        private const string CommissioningDateCaption = "Дата ввода в эксплуатацию";
        private const string FilesDataCaption = "Документ";
        private const string AdoptionBudgetDateCaption = "Дата";
        private const string GRBSCaption = "БК ГРБС";
        private const string PZRZCaption = "БК ПЗ/РЗ";
        private const string CSRCaption = "БК ЦСР";
        private const string WorkFormCaption = "БК ВР";
        private const string DecommissioningDateCaption = "Дата вывода из эксплуатации";

        #endregion Consts
    }
}