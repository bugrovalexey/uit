﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class InformationAboutSecurityViewModel : StepBaseViewModel
    {
        #region consts

        private const string LevelOfSecurityCaption = "Уровень защищенности";

        #endregion consts

        public InformationAboutSecurityViewModel(int id)
            : base(id, MasterItemEnum.InformationAboutSecurity)
        {
            LevelsOfSecurity = new SelectList(EnumHelper.ToDictionary<LevelOfSecurity>(), "Key", "Value");

            SpecChecks =
                new RepositoryDc<SpecialCheck>().GetAllEx(
                    q => q.Fetch(x => x.Documents).Eager.Where(x => x.AccountingObject.Id == id))
                    .Select(x => new TableSpecCheck(x));
        }

        [DisplayName(LevelOfSecurityCaption)]
        public LevelOfSecurity? LevelOfSecurity
        {
            get { return Current.Return(ao => ao.LevelOfSecurity, default(LevelOfSecurity?)); }
        }

        public SelectList LevelsOfSecurity { get; set; }

        public IEnumerable<TableSpecCheck> SpecChecks { get; set; }

    }
}