﻿namespace bgTeam.IsRui.Story.Claims
{
    public class ClaimSetJustificationStoryContext
    {
        public int? ClaimId { get; set; }

        public string Justification { get; set; }
    }
}
