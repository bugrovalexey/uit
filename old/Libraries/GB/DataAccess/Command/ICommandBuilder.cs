﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess
{
    /// <summary>
    /// Интерфейс для построителя команд.  Позволяет создать и выполнить команду с определенным контекстом.
    /// </summary>
    public interface ICommandBuilder
    {
        /// <summary>
        /// Создает команду с определенным контекстом и выполняет её.
        /// </summary>
        /// <typeparam name="TCommandContext">Тип контекста команды.</typeparam>
        /// <param name="commandContext">Контекст команды.</param>
        void Execute<TCommandContext>(TCommandContext commandContext) where TCommandContext : ICommandContext;

        TResult Execute<TCommandContext, TResult>(TCommandContext commandContext) where TCommandContext : ICommandContext;
    }
}
