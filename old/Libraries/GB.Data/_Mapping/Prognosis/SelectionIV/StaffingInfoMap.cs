﻿using Uviri.MKRF.Entities.Prognosis;
using Uviri.MKRF.Entities.Prognosis.SelectionIV;

namespace Uviri.MKRF.Mapping.Prognosis.SelectionIV
{
    public class StaffingInfoMap : ClassMapBase<StaffingInfo>
    {
        public StaffingInfoMap()
            : base("MF4")
        {
//            Table("w_");

//            Id(x => x.Id, "MF4_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string field =
//                        @"@MF4_CA_19_1 = ?
//                         ,@MF4_TP_19_1 = ?
//                         ,@MF4_CA_19_2 = ?
//                         ,@MF4_TP_19_2 = ?
//                         ,@MF4_CA_19_3 = ?
//                         ,@MF4_TP_19_3 = ?
//                         ,@MF4_CA_19_4 = ?
//                         ,@MF4_TP_19_4 = ?
//                         ,@MF4_20 = ?
//                         ,@MF4_20_1 = ?
//                         ,@MF4_21 = ?
//                         ,@MF4_21_1 = ?
//                         ,@MF4_21_2 = ?
//                         ,@MF4_22 = ?
//                         ,@MF4_22_1 = ?
//                         ,@MF4_22_2 = ?
//                         ,@MF4_23 = ?
//                         ,@MF4_23_1 = ?
//                         ,@MF4_24 = ?
//                         ,@MF4_25 = ?
//                         ,@MF4_26 = ?
//                         ,@MF4_27 = ?
//                         ,@Mon_Form_ID = ?
//                         ,@MF4_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_MF4_Insert " + field).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF4_Update " + field).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF4_Delete
//						  @MF4_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.SoftwareDevelopment_CA, "MF4_CA_19_1");
            Map(x => x.SoftwareDevelopment_TP, "MF4_TP_19_1");
            Map(x => x.SupportUsersOffice_CA, "MF4_CA_19_2");
            Map(x => x.SupportUsersOffice_TP, "MF4_TP_19_2");
            Map(x => x.SupportUsersSoft_CA, "MF4_CA_19_3");
            Map(x => x.SupportUsersSoft_TP, "MF4_TP_19_3");
            Map(x => x.SupportComputerNetworks_CA, "MF4_CA_19_4");
            Map(x => x.SupportComputerNetworks_TP, "MF4_TP_19_4");
            Map(x => x.Requirements_20, "MF4_20");
            Map(x => x.Act_20, "MF4_20_1");
            Map(x => x.Requirements_21, "MF4_21");
            Map(x => x.Developer_21, "MF4_21_1");
            Map(x => x.Act_21, "MF4_21_2");
            Map(x => x.Requirements_22, "MF4_22");
            Map(x => x.Developer_22, "MF4_22_1");
            Map(x => x.Act_22, "MF4_22_2");
            Map(x => x.AvailabilitySecurityConcepts, "MF4_23");
            Map(x => x.Act_23, "MF4_23_1");
            Map(x => x.WorkerBasicSkills, "MF4_24");
            Map(x => x.WorkerTrainedOfficeApp, "MF4_25");
            Map(x => x.WorkerTrainedApp, "MF4_26");
            Map(x => x.WorkerTrainedOtherApp, "MF4_27");

            References<Prognos>(x => x.Prognos, "Mon_Form_ID").Not.Nullable();
        }
    }
}
