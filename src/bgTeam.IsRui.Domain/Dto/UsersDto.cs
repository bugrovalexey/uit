﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UsersDto
    {
        public int? Id { get; set; }

        public string Login { get; set; }

        public bool IsActive { get; set; }

        public string Surname { get; set; }

        public string Name { get; set; }

        public string Middlename { get; set; }

        public string Job { get; set; }

        public string WorkPhone { get; set; }

        public string MobPhone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string Work { get; set; }

        public string Education { get; set; }

        public bool ReceiveMail { get; set; }

        public bool IsResponsible { get; set; }

        public string Snils { get; set; }

        public bool IsESIA { get; set; }

        public string Token { get; set; }

        public int? GovtOrganId { get; set; }
    }
}
