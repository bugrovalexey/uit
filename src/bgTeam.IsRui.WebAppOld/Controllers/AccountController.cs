﻿using bgTeam.IsRui.WebApp.ViewModels.Account;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IUserRepository _userRepository;

        public AccountController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }


        /// <summary>
        /// Перенаправить пользователя на страницу по умолчанию
        /// </summary>
        public ActionResult Index()
        {
            return Redirect(_userRepository.GetCurrent().Roles.First().WebPage);
        }

        public ActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            string error = string.Empty;

            if (string.IsNullOrWhiteSpace(model.Login))
                error = string.Concat(error, "Не заполнено поле Логин. Пожалуйста, заполните его.");

            if (string.IsNullOrWhiteSpace(model.Password))
                error = string.Concat(error, " Не заполнено поле Пароль. Пожалуйста, заполните его.");

            if (!string.IsNullOrEmpty(error))
            {
                model.InvalidMessage = error;
                return View(model);
            }

            if (Membership.Provider != null && Membership.Provider.ValidateUser(model.Login, model.Password))
            {
                //FormsAuthentication.SetAuthCookie(model.Login, true);
                FormsAuthentication.RedirectFromLoginPage(model.Login, true);

                //                return RedirectToAction("LoginRedirect");
            }

            model.InvalidMessage = "Неправильный логин или пароль!";
            return View(model);
        }

        public ActionResult Logout()
        {
            //var user = UserRepository.GetCurrent();
            //if (user != null && user.IsESIAUser)
            //{
            //    EsiaLogout();
            //    return null;
            //}

            FormsAuthentication.SignOut();
            Session.Abandon();
            //return View();
            return RedirectToAction("Login");
        }
    }
}