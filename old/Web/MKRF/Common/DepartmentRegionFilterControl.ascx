﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DepartmentRegionFilterControl.ascx.cs"
    Inherits="Uviri.MKRF.Web.Common.DepartmentRegionFilterControl" %>
<table width="100%">
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <div >
                        <dxe:ASPxLabel runat="server" ID="DepartmentChooseLabel" ClientInstanceName="DepartmentChooseLabel"
                            Text="Показать данные по: " Width="140px" Font-Bold="True" style="margin-left: 7px;">
                        </dxe:ASPxLabel>
                        </div>
                    </td>
                    <td>
                        <dxe:ASPxComboBox runat="server" ID="ParentChooseComboBox" ClientInstanceName="ParentChooseComboBox"
                            TextField="Name" ValueField="Value" OnDataBinding="ParentChooseComboBox_DataBinding"
                            Width="270px">
                        </dxe:ASPxComboBox>
                    </td>
                    <td>
                        <dxe:ASPxComboBox runat="server" ID="YearsComboBox" Width="80px" ValueType="System.Int32"
                            ValueField="Year" TextField="Year" ClientInstanceName="YearsComboBox" OnDataBinding="YearsComboBox_DataBinding">
                        </dxe:ASPxComboBox>
                    </td>
                    <td>
                        <dxe:ASPxComboBox runat="server" ID="StageFilterComboBox" Width="120px" ValueType="System.Int32"
                            Visible="false" ValueField="Id" TextField="Name" ClientInstanceName="StageFilterComboBox"
                            OnDataBinding="StageFilterComboBox_DataBinding">
                        </dxe:ASPxComboBox>
                    </td>
                    <td align="left" width="100%">
                        <dxe:ASPxButton runat="server" ID="filterButton" ClientInstanceName="filterButton"
                            Text="OK" onclick="filterButton_Click">
                        </dxe:ASPxButton>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
