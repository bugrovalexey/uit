﻿using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Common;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    public class AccessRightsViewModel
    {
        public EntityType Type { get; set; }

        public SelectList TypesList { get; set; }

        public IList<TableAccessToEntity> AccessTable { get; private set; }

        public AccessRightsViewModel()
        {
            var types = EnumHelper.ToDictionary<EntityType>();
            TypesList = new SelectList(types, "Key", "Value");
            Type = types.Keys.FirstOrDefault();
            FillAccessTable();
        }

        public AccessRightsViewModel(EntityType type)
        {
            Type = type;
            FillAccessTable();
        }

        private void FillAccessTable()
        {
            AccessTable = new RepositoryDc<AccessToEntity>().GetAllEx(
                q => q.Where(x => x.EntityType == Type).OrderBy(x => x.Status).Asc
                    .Fetch(x => x.Status).Eager
                    .Fetch(x => x.Role).Eager.Clone()
                    )
                .Select(x => new TableAccessToEntity(x))
                .ToList();
        }
    }
}