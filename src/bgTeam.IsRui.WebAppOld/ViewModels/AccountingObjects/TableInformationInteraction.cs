﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.Extensions;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class TableInformationInteraction : NotificationBaseViewModel
    {
        #region Consts

        public const string NameCaption = "Наименование";
        public const string PurposeCaption = "Назначение";
        public const string TypeCaption = "Тип";
        public const string SmevCaption = "СМЭВ";
        public const string TitleCaption = "Заголовок";
        public const string UrlCaption = "URL";

        #endregion Consts

        #region Ctors

        public TableInformationInteraction(InformationInteraction entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            Purpose = entity.Purpose;
            TypeName = entity.Type.Return(type => type.Name, default(string));
            SMEV = entity.SMEV;
            Title = entity.Title;
            Url = entity.Url;
        }

        #endregion Ctors

        #region Props

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(PurposeCaption)]
        public string Purpose { get; set; }

        [DisplayName(TypeCaption)]
        public string TypeName { get; set; }

        [DisplayName(SmevCaption)]
        public string SMEV { get; set; }

        [DisplayName(TitleCaption)]
        public string Title { get; set; }

        public string Url { get; set; }

        [DisplayName(UrlCaption)]
        public string UrlLink
        {
            get { return Url.Return(url => LinkBuilder.Link(url, url), default(string)); }
        }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteInformationInteraction", "AccountingObjects", new { id = Id }, "Grid");
            }
        }

        #endregion Props
    }
}