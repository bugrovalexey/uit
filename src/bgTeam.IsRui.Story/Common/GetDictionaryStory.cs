﻿namespace bgTeam.IsRui.Story.Common
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using NHibernate.Criterion;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GetDictionaryStory : IStory<GetDictionaryStoryContext, IEnumerable<DictionaryDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;

        public GetDictionaryStory(
            IMapperBase mapper,
            IRepositoryRui repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public IEnumerable<DictionaryDto> Execute(GetDictionaryStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<DictionaryDto>> ExecuteAsync(GetDictionaryStoryContext context)
        {
            context.Name.CheckNull(nameof(context.Name));

            switch (context.Name)
            {
                case "Units": return GetDictionary<Units>(context.Search);
                case "Okved": return GetDictionary<OKVED>(context.Search);
                case "Manufacturer": return GetDictionary<Manufacturer>(context.Search);
                case "CategoryKind": return GetDictionary<CategoryKindOfSupport>(context.Search);
                case "ArticleBudget": return GetDictionary<ArticleBudget>(context.Search);

                case "aoArticle": return GetDictionary<AoArticle>(context.Search);
                case "aoCategory": return GetDictionary<IKTComponent>(context.Search);
                case "aoCharacteristicType": return GetDictionary<AccountingObjectCharacteristicsType>(context.Search);

                case "ActivityType": return GetDictionary<ActivityType>(context.Search);
                case "ExpenditureItem": return GetDictionary<ExpenditureItem>(context.Search);
                case "OKPD": return GetDictionary<OKPD>(context.Search);
                case "ExpenseDirection": return GetDictionary<ExpenseDirection>(context.Search);
                case "ExpenseType": return GetDictionary<ExpenseType>(context.Search);
                case "ProductGroup": return GetDictionary<ProductGroup>(context.Search);

                case "SpecialistCategory": return GetDictionary<SpecialistCategoryType>(context.Search);
                case "ClassificationCategory": return GetTreeDictionary<ClassificationCategory, TreeDictionaryDto>(context.Search);

                case "Departments": return GetDictionary<Department>(context.Search);

                case "Okei": return GetDictionary<Okei>(context.Search);
            }

            throw new ArgumentException($"Not support dictionary - {context.Name}");
        }

        private IEnumerable<DictionaryDto> GetDictionary<T>(string search)
            where T : EntityDirectories
        {
            return GetDictionary<T, DictionaryDto>(search);
        }

        private IEnumerable<TDto> GetDictionary<T, TDto>(string search)
            where T : EntityDirectories
            where TDto : DictionaryDto
        {
            IEnumerable<T> res;

            if (string.IsNullOrEmpty(search))
            {
                res = _repository.GetAll<T>();
            }
            else
            {
                res = _repository.GetAllEx<T>(x => x.Where(Restrictions.Like("Name", $"{search}", MatchMode.Anywhere)));
            }

            return _mapper.Map(res, new List<TDto>());
        }

        private IEnumerable<DictionaryDto> GetTreeDictionary<T, TDto>(string search)
            where T : EntityTreeDirectories<T>
            where TDto : DictionaryDto
        {
            return GetTreeDictionary<T, TDto>(_mapper, _repository, search);
        }

        internal static IEnumerable<TDto> GetTreeDictionary<T, TDto>(IMapperBase mapper, IRepositoryRui repository, string search)
            where T : EntityTreeDirectories<T>
            //where TDto : DictionaryDto
        {
            IEnumerable<T> res;

            if (string.IsNullOrEmpty(search))
            {
                res = repository.GetAllEx<T>(q => q.Where(x => x.Parent == null));
            }
            else
            {
                res = repository.GetAllEx<T>(q => q
                .Where(x => x.Parent == null))

                // Выполняется вне запроса
                .Where(x => x.Childs.Any(a => a.Name.Contains(search)));
            }

            return mapper.Map(res, new List<TDto>());
        }
    }
}
