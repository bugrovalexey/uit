﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Activities/EditCard.master" AutoEventWireup="true" CodeBehind="CharacteristicsOU.aspx.cs" Inherits="GB.MKRF.Web.Activities.CharacteristicsOU" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function validateComboBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && e.value != 0;
        }

        function GridSave() {
            hf.Set('ValName', NameTextBox.GetText());
            hf.Set('ValType', TypeComboBox.GetValue());
            hf.Set('ValUnit', UnitComboBox.GetValue());
            hf.Set('ValNorm', NormTextBox.GetText());
            hf.Set('ValMax', MaxTextBox.GetText());
        }

        //function PopupControl_Save() {
        //    if (ASPxClientEdit.ValidateGroup('CharacteristicsNew')) {

        //        hf.Set('CrName', CrNameTextBox.GetText());
        //        hf.Set('CrType', CrTypeComboBox.GetValue());
        //        hf.Set('CrUnit', CrUnitComboBox.GetValue());
        //        hf.Set('CrNorm', CrNormTextBox.GetText());
        //        hf.Set('CrMax', CrMaxTextBox.GetText());

        //        callBack.PerformCallback('');
        //    }
        //}

        //function PopupControl_Close() {
        //    PopupControl.Hide();
        //}

        //function CharacteristicsNewEnd(e) {
        //    CrNameTextBox.SetText('');
        //    CrTypeComboBox.SetValue(0);
        //    CrUnitComboBox.SetValue(0);
        //    CrNormTextBox.SetText('');
        //    CrMaxTextBox.SetText('');

        //    PopupControl.Hide();
        //    Grid.Refresh();
        //    MainLoadingPanel.Hide();
        //}
    </script>
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
    <dxe:ASPxButton ID="addButton" runat="server" ClientInstanceName="addButton" AutoPostBack="false"
        Text="Добавить" CssClass="topButton fleft">
        <ClientSideEvents Click="function(s,e) { Grid.AddNewRow(); }" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="Grid" KeyFieldName="CharId" Width="100%" AutoGenerateColumns="False"
        ClientInstanceName="Grid" OnBeforePerformDataSelect="Grid_BeforePerformDataSelect" OnStartRowEditing="Grid_StartRowEditing"
        OnRowInserting="Grid_RowInserting" OnRowUpdating="Grid_RowUpdating" OnRowDeleting="Grid_RowDeleting" OnHtmlRowPrepared="Grid_HtmlRowPrepared"
        OnCommandButtonInitialize="Grid_CommandButtonInitialize">
        <Settings ShowFilterRow="true" />
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="true" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Характеристика" />
            <dxwgv:GridViewDataTextColumn FieldName="Type" Caption="Тип" />
            <dxwgv:GridViewDataTextColumn FieldName="Unit" Caption="Единица измерения" />
            <dxwgv:GridViewDataTextColumn FieldName="Norm" Caption="Номинальное значение" />
            <dxwgv:GridViewDataTextColumn FieldName="Max" Caption="Максимальное значение" />
            <dxwgv:GridViewDataTextColumn FieldName="IsRegister" Caption=" " Visible="false" />
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc required">Наименование характеристики
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="NameTextBox" runat="server" ClientInstanceName="NameTextBox"
                                OnDataBinding="NameTextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Тип:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="TypeComboBox" runat="server" ClientInstanceName="TypeComboBox"
                                TextField="Name" ValueField="Id" ValueType="System.Int32"
                                OnDataBinding="TypeComboBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text" />
                                <ClientSideEvents Validation="function(s,e){ validateComboBox(s, e); }" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Единица измерения:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="UnitComboBox" runat="server" ClientInstanceName="UnitComboBox"
                                TextField="Name" ValueField="Id" ValueType="System.Int32"
                                OnDataBinding="UnitComboBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text" />
                                <ClientSideEvents Validation="function(s,e){ validateComboBox(s, e); }" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Номинальное значение:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="NormTextBox" ClientInstanceName="NormTextBox"
                                MaskSettings-Mask="<0..999999999>" OnDataBinding="NormTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Максимальное значение:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="MaxTextBox" ClientInstanceName="MaxTextBox"
                                MaskSettings-Mask="<0..999999999>" OnDataBinding="MaxTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok"><a onclick="Grid.UpdateEdit();">Сохранить</a></span>
                            <span class="cancel"><a onclick="Grid.CancelEdit();">Отмена</a></span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') GridSave(); }" />
    </dxwgv:ASPxGridView>

    <%--<dxpc:ASPxPopupControl ID="PopupControl" runat="server" AllowDragging="True" AllowResize="True"
        CloseAction="CloseButton" Modal="True" EnableViewState="False" HeaderText="Характеристика"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="300px"
        ClientInstanceName="PopupControl" EnableHierarchyRecreation="True">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                <table class="form_edit a_cd_ou">

                    <tr>
                        <td class="form_edit_desc">Номинальное значение:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="CrNormTextBox" ClientInstanceName="CrNormTextBox"
                                MaskSettings-Mask="<0..999999999>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Максимальное значение:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="CrMaxTextBox" ClientInstanceName="CrMaxTextBox"
                                MaskSettings-Mask="<0..999999999>" OnDataBinding="MaxTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok"><a onclick="PopupControl_Save()" href="javascript:;">Сохранить</a></span>
                            <span class="cancel"><a onclick="PopupControl_Close()" href="javascript:;">Отмена</a></span>
                        </td>
                    </tr>
                </table>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
     <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" OnCallback="callBack_Callback">
        <ClientSideEvents CallbackComplete="function(s,e) { CharacteristicsNewEnd(e); }" CallbackError="function(s,e) { CharacteristicsNewEnd(e); }" />
    </dxc:ASPxCallback>--%>
</asp:Content>