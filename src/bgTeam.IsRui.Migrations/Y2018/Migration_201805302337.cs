﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201805302337)]
    public class Migration_201805302337 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("Claim")
                .WithColumn("Claim_ID").AsInt32().PrimaryKey()
                .WithColumn("Claim_OU_ID").AsInt32().Nullable()
                .WithColumn("Claim_Plans_Activity_ID").AsInt32().NotNullable()
                .WithColumn("Claim_Status_ID").AsInt32().Nullable()
                .WithColumn("Claim_Number").AsString(15).NotNullable()
                .WithColumn("Claim_SubDivisionName").AsString(512).Nullable()
                .WithColumn("Claim_Cost").AsDecimal().Nullable()
                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "Claim" });
        }
    }
}
