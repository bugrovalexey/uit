﻿using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class ReasonsDecommissioningViewModel : StepBaseViewModel
    {
        [DisplayName("Причина")]
        public string Reasons { get; set; }

        public ReasonsDecommissioningViewModel(int id)
            : base(id, MasterItemEnum.ReasonsDecommissioning)
        {
            Reasons = Current.DecommissioningReasons;
        }
    }
}