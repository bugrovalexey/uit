﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.Documents;
using bgTeam.IsRui.FileSystem;
using bgTeam.IsRui.WebApp.Helpers;
using System;
using System.IO;
using System.Web;
using FileInfo = bgTeam.IsRui.DataAccess.FileInfo;

namespace bgTeam.IsRui.Handlers
{
    /// <summary>
    /// Обработчик загруженных файлов
    /// </summary>
    public class WebFileHandler : IHttpHandler
    {
        #region IHttpHandler

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            FileInfo info = FileInfo.FromFullUrl(context.Request.RawUrl);
            SendFile(context, info);
        }

        #endregion IHttpHandler


        private void SendFile(HttpContext context, FileInfo info)
        {
            string physicalPath = FileProvider.GetPhysicalPath(info.Id);

            Logger.Debug("Попытка скачать файл - " + physicalPath);

            if (File.Exists(physicalPath) == false)
            {
                var errorTxt = string.Format("Файл не найден id={0} filename={1} path={2}",
                    info.With(i => i.Id).Return(id => id.ToString(), "null"),
                    info.Return(i => i.FileName, "null"),
                    physicalPath);

                Logger.Debug(errorTxt);
                //ResponseHelper.FileNotFound(context.Response, errorTxt);
                return;
            }

            string fileName = info.Id is Guid
                    ? info.FileName // если файл находится в Temp
                    : GetFileName(info.Id);// иначе получаем имя файла из БД                    

            // Если имя не задано == файл не доступен
            if (string.IsNullOrWhiteSpace(fileName))
            {
                ResponseHelper.AccessDenied(context.Response,
                    string.Format("Отказ в получении файла id={0}", info.Id.Return(id => info.Id.ToString(), "null")));
                return;
            }

            Logger.Debug("Отправляем файл");
            
            FillResponse(context, physicalPath, fileName);
        }


        /// <summary>
        /// Возвращает имя файла из БД
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns>Если документ не доступен возвращает null</returns>
        private string GetFileName(object fileId)
        {
            var fileName =
                new RepositoryDc<Files>().SelectSingle<string>(q => q.Select(x => x.Name).Where(x => x.Id == (int) fileId));
            return fileName;
        }

        /// <summary>
        /// Отсылает файл клиенту
        /// </summary>
        private void FillResponse(HttpContext context, string physicalFilePath, string fileName)
        {
            context.Response.Clear();
            context.Response.Buffer = false;
            context.Response.ContentType = "application/octet-stream";

            context.Response.AppendHeader("Content-Length", (new System.IO.FileInfo(physicalFilePath)).Length.ToString());
            context.Response.AppendHeader("Content-Transfer-Encoding", "binary");
            context.Response.AppendHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0, max-age=0");

            context.Response.AppendHeader(
                "Content-Disposition",
                string.Format("attachment; filename=\"{0}\";", fileName));

            context.Response.TransmitFile(physicalFilePath);
            context.Response.End();
        }


    }
}