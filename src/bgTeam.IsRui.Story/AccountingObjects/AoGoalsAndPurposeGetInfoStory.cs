﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoGoalsAndPurposeGetInfoStory : IStory<AoGoalsAndPurposeGetInfoStoryContext, GoalsAndPurposeInfoDto>
    {
        private readonly IRepositoryRui _repository;

        public AoGoalsAndPurposeGetInfoStory(IRepositoryRui repository)
        {
            _repository = repository;
        }

        public GoalsAndPurposeInfoDto Execute(AoGoalsAndPurposeGetInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<GoalsAndPurposeInfoDto> ExecuteAsync(AoGoalsAndPurposeGetInfoStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            AccountingObject ao = _repository.Get<AccountingObject>(context.AoId);

            var result = new GoalsAndPurposeInfoDto();

            result.AoId = context.AoId;

            result.Targets = ao.Targets;

            result.PurposeAndScope = ao.PurposeAndScope;

            return result;
        }
    }
}
