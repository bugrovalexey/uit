﻿using GB.Data.Workflow;

namespace GB.Data.Extensions
{
    public static class StatusGroupExtension
    {
        public static bool IsEq(this StatusGroupEnum e, StatusGroup o)
        {
            return o.Id == (int)e;
        }
    }
}