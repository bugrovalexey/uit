﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Entities.Statuses;
using GB.MKRF.Workflow;

namespace GB.Coord.Workflow
{
    public class ExpertiseWorkflowHandler : WorkflowHandlerBase<ActivityExpertiseConclusion>
    {
        protected override void CheckNewStatus(ActivityExpertiseConclusion e, Status newStatus)
        {
            StatusEnum status = (StatusEnum)newStatus.Id;
            switch (status)
            {
                case StatusEnum.ExpAppointment:
                    break;
                case StatusEnum.ExpTechnician:
                    if(!e.Experts.Any(x=>x.Role == ActivityExpertEnum.Technician))
                        throw new InvalidOperationException("Необходимо назначить технического специалиста для экпертизы");
                    if(!e.Experts.Any(x=>x.Role == ActivityExpertEnum.Economist))
                        throw new InvalidOperationException("Необходимо назначить экономиста для экпертизы");
                    break;
                case StatusEnum.ExpEconomist:
                    break;
                case StatusEnum.ExpAgreed:
                    break;
                case StatusEnum.ActOnCompletion:
                    //TODO возможно надо проверить наличие сообщения от эксперта
                    break;
                case StatusEnum.ActAgreed:
                    break;
                default:
                    throw new InvalidOperationException("Экспертиза не может иметь статус: " + status);
            }
        }
    }
}