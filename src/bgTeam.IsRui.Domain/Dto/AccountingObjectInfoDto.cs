﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using bgTeam.IsRui.Domain.Entities.Workflow;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Информация об объекте учёта
    /// </summary>
    public class AccountingObjectInfoDto
    {
        /// <summary>
        /// Идентификатор ОУ
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Режим ОУ (составное или одинарное)
        /// </summary>
        public AccountingObjectEnum Mode { get; set; }

        /// <summary>
        /// Родитель
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// Учреждение, которое добавило в систему этот ОУ
        /// </summary>
        public DepartmentDto AddAoDepartment { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public StatusDto Status { get; set; }

        /// <summary>
        /// Стадия
        /// </summary>
        public AccountingObjectsStageEnum Stage { get; set; }

        /// <summary>
        /// Уникальный идентификационный номер ОУ
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Тип (родитель). Идентификатор
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Тип (родитель). Наименование
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// Тип (родитель). Код
        /// </summary>
        public string TypeCode { get; set; }

        /// <summary>
        /// Вид (потомок). Идентификатор
        /// </summary>
        public int KindId { get; set; }

        /// <summary>
        /// Классификационная категория
        /// </summary>
        public string KindName { get; set; }

        /// <summary>
        /// Классификационная категория. Код
        /// </summary>
        public string KindCode { get; set; }

        public int ArticleId { get; set; }

        public string ArticleName { get; set; }

        /// <summary>
        /// Полное наименование
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Цели создания ОУ
        /// </summary>
        public string Targets { get; set; }

        /// <summary>
        /// Назначение и область применения объекта учета
        /// </summary>
        public string PurposeAndScope { get; set; }

        /// <summary>
        /// Информационная система, направлена на информирование о деятельности государственного органа
        /// </summary>
        public bool InformAboutActivityGovOrgan { get; set; }

        /// <summary>
        /// Уровень защищенности
        /// </summary>
        public LevelOfSecurity? LevelOfSecurity { get; set; }

        /// <summary>
        /// Дата принятия к бюджетному учету
        /// </summary>
        public DateTime? AdoptionBudgetDate { get; set; }

        /// <summary>
        /// Код по БК ГРБС
        /// </summary>
        public GRBSDto Grbs { get; set; }

        /// <summary>
        /// Код по БК ПЗ/РЗ
        /// </summary>
        public SectionKBKDto SectionKbkPzrz { get; set; }

        /// <summary>
        /// Код по БК ЦСР
        /// </summary>
        public ExpenseItemDto ExpenseItemCsr { get; set; }

        /// <summary>
        /// Код по БК ВР
        /// </summary>
        public WorkFormDto WorkFormWr { get; set; }

        /// <summary>
        /// Код по БК КОСГУ
        /// </summary>
        public ExpenditureItemDto ExpenditureItemKosgu { get; set; }

        /// <summary>
        /// Балансовая стоимость
        /// </summary>
        public decimal BalanceCost { get; set; }

        /// <summary>
        /// Основания принятия к бюджетному учету
        /// </summary>
        public string AdoptionBudgetBasis { get; set; }

        /// <summary>
        /// Введен в эксплуатацию
        /// </summary>
        public DateTime? CommissioningDate { get; set; }

        /// <summary>
        /// Выведен из эксплуатации
        /// </summary>
        public DateTime? DecommissioningDate { get; set; }

        /// <summary>
        /// Выведен из эксплуатации
        /// </summary>
        public string DecommissioningReasons { get; set; }

        /// <summary>
        /// Учреждение, уполномоченное на создание, развитие ОУ
        /// </summary>
        public DepartmentDto ResponsibleDepartment { get; set; }

        /// <summary>
        /// Учреждение или иное юридическое лицо, уполномоченное на эксплуатацию
        /// <para>Идентификатор</para>
        /// </summary>
        public int ExploitationDepartmentId { get; set; }

        /// <summary>
        /// Учреждение или иное юридическое лицо, уполномоченное на эксплуатацию
        /// <para>Наименование</para>
        /// </summary>
        public string ExploitationDepartmentName { get; set; }

        /// <summary>
        /// Ответственный за координацию мероприятий
        /// по информатизации
        /// </summary>
        public ResponsibleDto ResponsibleCoord { get; set; }

        /// <summary>
        /// Ответственный за обеспечение эксплуатации
        /// </summary>
        public ResponsibleDto ResponsibleExploitation { get; set; }

        /// <summary>
        /// Ответственный за организацию закупок
        /// </summary>
        public ResponsibleDto ResponsiblePurchase { get; set; }

        /// <summary>
        /// Ответственный за размещение сведений об объекте учета
        /// </summary>
        public ResponsibleDto ResponsibleInformation { get; set; }

        /// <summary>
        /// Наименование организации
        /// </summary>
        public string LocationOrgName { get; set; }

        /// <summary>
        /// ФИО ответственного
        /// </summary>
        public string LocationFioResponsible { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string LocationOrgAddress { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string LocationOrgPhone { get; set; }

        /// <summary>
        /// Планируемая стоимость создания ОУ
        /// </summary>
        public decimal CreateCost { get; set; }

        /// <summary>
        /// Планируемая стоимость развития ОУ
        /// </summary>
        public decimal DevelopCost { get; set; }

        /// <summary>
        /// Планируемая стоимость эксплуатации ОУ
        /// </summary>
        public decimal ExploitationCost { get; set; }

        /// <summary>
        /// Фактическая стоимость создания ОУ
        /// </summary>
        public decimal CreateCostFact { get; set; }

        /// <summary>
        /// Фактическая стоимость развития ОУ
        /// </summary>
        public decimal DevelopCostFact { get; set; }

        /// <summary>
        /// Фактическая стоимость эксплуатации ОУ
        /// </summary>
        public decimal ExploitationCostFact { get; set; }

        /// <summary>
        /// Вид деятельности
        /// </summary>
        public string KindOfActivityName { get; set; }

        // Lists

        /// <summary>
        /// Реализуемые государственные услуги и функции
        /// </summary>
        public IEnumerable<AOGovServiceDto> AOGovService { get; set; }

        /// <summary>
        /// Характеристики
        /// </summary>
        public IEnumerable<AccountingObjectCharacteristicsDto> Characteristics { get; set; }

        /// <summary>
        /// Программное обеспечение ОУ
        /// </summary>
        public IEnumerable<SoftwareDto> Software { get; set; }

        /// <summary>
        /// Работы и услуги ОУ
        /// </summary>
        public IEnumerable<WorkAndServiceDto> WorksAndServices { get; set; }

        /// <summary>
        /// Внутренние ИС и компоненты ИТКИ
        /// </summary>
        public IEnumerable<InternalIsAndComponentsItkiDto> InternalIsAndComponentsItki { get; set; }

        /// <summary>
        /// Внешние ИС и компоненты ИТКИ
        /// </summary>
        public IEnumerable<ExternalIsAndComponentsItkiDto> ExternalIsAndComponentsItki { get; set; }

        /// <summary>
        /// Внешние пользовательские интерфейсы ИС
        /// </summary>
        public IEnumerable<ExternalUserInterfaceOfIsDto> ExternalUserInterfacesOfIs { get; set; }

        /// <summary>
        /// Закупки
        /// </summary>
        public IEnumerable<GovPurchaseDto> GovPurchases { get; set; }

        /// <summary>
        /// Госконтракты
        /// </summary>
        public IEnumerable<GovContractDto> GovContracts { get; set; }

        /// <summary>
        /// Акт сдачи-приемки
        /// </summary>
        public IEnumerable<ActOfAcceptanceDto> Acts { get; set; }

        /// <summary>
        /// решения о создании (закупки) оу
        /// </summary>
        public IEnumerable<AODecisionToCreateDto> Documents { get; set; }

        /// <summary>
        /// техническое обеспечение оу
        /// </summary>
        public IEnumerable<TechnicalSupportDto> TechnicalSupports { get; set; }

        /// <summary>
        /// Мероприятия
        /// </summary>
        public IEnumerable<ActivityDto> Activities { get; set; }

        /// <summary>
        /// акты
        /// </summary>
        public IEnumerable<ActsDto> AccountingObjectActs { get; set; }

        /// <summary>
        /// Финансирование
        /// </summary>
        public decimal? FinancingQuarter1 { get; set; }

        public decimal? FinancingQuarter2 { get; set; }

        public decimal? FinancingQuarter3 { get; set; }

        public decimal? FinancingQuarter4 { get; set; }

        /// <summary>
        /// Договоры
        /// </summary>
        public IEnumerable<ActivityContractDto> Contracts { get; set; }
    }
}