﻿using GB.MKRF.Logic.MasterEdit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Logic.AccountingObjectActions.Handler
{
    abstract class ActionBaseHandler : IManagerActionsHandler
    {
        const string controller = "AccountingObjects";

        protected string a_action_template = "<a href='{1}'>{0}</a>";

        IList<ActionDescription> _ActionsList = new List<ActionDescription>();

        public ActionBaseHandler()
        {
            Init();
        }

        protected abstract void Init();


        protected void Add(ActionDescription action)
        {
            _ActionsList.Add(action);
        }


        protected void Add(string name, MasterItemEnum action, string template)
        {
            Add(name, action.ToString(), template);
        }

        protected void Add(string name, string action, string template)
        {
            _ActionsList.Add(new ActionDescription() { Name = name, Action = action, Controller = controller, Template = template });
        }

        public IEnumerable<ActionDescription> GetActionsList()
        {
            return _ActionsList;
        }
    }
}
