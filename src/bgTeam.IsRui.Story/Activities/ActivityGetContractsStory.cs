﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityGetContractsStory : IStory<ActivityGetContractsStoryContext, IEnumerable<ActivityContractDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapperBase;

        public ActivityGetContractsStory(
            IRepositoryRui repository,
            IMapperBase mapperBase)
        {
            _repository = repository;
            _mapperBase = mapperBase;
        }

        public IEnumerable<ActivityContractDto> Execute(ActivityGetContractsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivityContractDto>> ExecuteAsync(ActivityGetContractsStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var activity = _repository.Get<PlansActivity>(context.ActivityId);

            if (activity == null)
            {
                throw new IsRuiException("Не найдено мероприятие");
            }

            var contracts = _repository.GetAllEx<ActivityContract>(q =>
                q.Where(x => x.PlansActivity.Id == activity.Id));

            return contracts.Select(x => _mapperBase.Map(x, new ActivityContractDto()));
        }
    }
}
