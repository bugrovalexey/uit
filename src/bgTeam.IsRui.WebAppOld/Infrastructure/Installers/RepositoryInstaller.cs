﻿using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.DataAccess.Impl;
using bgTeam.IsRui.DataAccess.Repository;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace bgTeam.IsRui.WebApp.Infrastructure.Installers
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IRepositoryOldFactory>().AsFactory().LifestyleTransient(),
                Component.For<ISessionNHibernateFactory>().ImplementedBy<SessionNHibernateFactory>().LifestyleSingleton(),
                Component.For(typeof(IRepositoryOld)).ImplementedBy(typeof(NHRepository)).LifestyleTransient());

            container.Register(
                Component.For<ISessionNHibernateConfiguration>().ImplementedBy<SessionNHibernateConfigurationMsSql>().LifestyleSingleton(),
                Component.For<IUserRepository>().ImplementedBy<UserRepository>().LifestyleTransient(),
                Component.For<IStatusRepository>().ImplementedBy<StatusRepository>().LifestyleTransient());
        }
    }
}
