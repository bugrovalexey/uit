﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityDeleteContractStory : IStory<ActivityDeleteContractStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;

        public ActivityDeleteContractStory(
            ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _factory = sessionNHibernateFactory;
        }

        public bool Execute(ActivityDeleteContractStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(ActivityDeleteContractStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            using (var session = _factory.OpenSession())
            {
                await session.DeleteAsync<ActivityContract>(context.Id.Value);
            }

            return true;
        }
    }
}
