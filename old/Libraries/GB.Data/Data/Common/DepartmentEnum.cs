﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace GB.Data.Common
{
    public enum DepartmentEnum
    {
        None = 0,

        [Description("Иное юридическое лицо")]
        OtherLegalEntity = 5096,
    }
}
