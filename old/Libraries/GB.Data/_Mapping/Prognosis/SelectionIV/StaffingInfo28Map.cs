﻿using Uviri.MKRF.Entities.Prognosis.SelectionIV;

namespace Uviri.MKRF.Mapping.Prognosis.SelectionIV
{
    public class StaffingInfo28Map : ClassMapBase<StaffingInfo28> 
    {
        public StaffingInfo28Map()
            : base("MF4_28")
        {
//            Table("w_");

//            Id(x => x.Id, "MF4_28_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF4_28_Insert
//                          @MF4_28_Num = ?
//                         ,@MF4_28_Func = ?
//                         ,@MF4_28_Org = ?
//                         ,@MF4_ID = ?
//                         ,@MF4_28_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF4_28_Update
//                          @MF4_28_Num = ?
//                         ,@MF4_28_Func = ?
//                         ,@MF4_28_Org = ?
//                         ,@MF4_ID = ?
//                         ,@MF4_28_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF4_28_Delete
//						  @MF4_28_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "MF4_28_Num");
            Map(x => x.Function, "MF4_28_Func");
            Map(x => x.Department, "MF4_28_Org");

            References<StaffingInfo>(x => x.StaffingInfo, "MF4_ID").Not.Nullable();
        }
    }
}
