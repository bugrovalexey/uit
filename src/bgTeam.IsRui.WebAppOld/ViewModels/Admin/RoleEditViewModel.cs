﻿using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    [MetadataType(typeof(RoleEditViewModel_Valid))]
    public class RoleEditViewModel : ViewModelBase
    {
        private const string NameCaption = "Наименование";
        private const string WebPageCaption = "Web страница";

        public RoleEditViewModel(int? id)
        {
            var role = id.HasValue ? new RepositoryDc<Role>().GetExisting(id) : null;
            role.Do(Fill);
        }

        public void Fill(Role role)
        {
            Id = role.Id;
            Name = role.Name;
            WebPage = role.WebPage;
        }

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(WebPageCaption)]
        public string WebPage { get; set; }
    }

    internal abstract class RoleEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }
    }
}