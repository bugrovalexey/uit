﻿namespace bgTeam.IsRui.Story.Activities
{
    using System;
    using bgTeam.IsRui.Domain.Entities.Documents;

    public class ConclusionAddFileStoryContext
    {
        public int? ActivityId { get; set; }

        public int? ConclusionId { get; set; }

        public Guid TempGuid { get; set; }

        public string FileName { get; set; }

        public DocKindEnum Kind { get; set; }

        public string Extension { get; set; }

        public int? Size { get; set; }

        public string Url { get; set; }

        public string MimeType { get; set; }
    }
}
