﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ZodMap : ClassMapBase<Zod>
    {
#warning delete this
        public ZodMap()
            : base("ZOD")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.EnergyConsumption, "ZOD_Energy_Consumption");
            Map(x => x.ContractNumber, "ZOD_Contract_Number");
            Map(x => x.RequiredSpace, "ZOD_Required_Space");
            Map(x => x.IKTComponent, "ZOD_IKT_Component");

            References<PlansActivity>(x => x.PlansActivity, "Plans_Activity_ID");
            References<Department>(x => x.OGV, "Govt_Organ_ID");
            References<Department>(x => x.OGVAuthorized, "Authorized_Govt_Organ_ID");
            References<FgisInfo>(x => x.FgisInfo, "FGIS_Info_ID");
        }
    }
}
