﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimGetMarksStory : IStory<ClaimGetMarksStoryContext, IEnumerable<ClaimMarkDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapperBase;

        public ClaimGetMarksStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase)
        {
            _repository = repositoryRui;
            _mapperBase = mapperBase;
        }

        public IEnumerable<ClaimMarkDto> Execute(ClaimGetMarksStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ClaimMarkDto>> ExecuteAsync(ClaimGetMarksStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка: {context.ClaimId}");
            }

            var marks = _repository
                .GetAllEx<ClaimMark>(q => q.Where(x => x.Claim.Id == context.ClaimId));

            return marks.Select(x => _mapperBase.Map(x, new ClaimMarkDto()));
        }
    }
}
