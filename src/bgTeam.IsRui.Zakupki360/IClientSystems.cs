﻿using bgTeam.IsRui.Zakupki360.Dto;
using System.Collections.Generic;

namespace bgTeam.IsRui.Zakupki360
{
    public interface IClientSystems
    {
        void Test();

        IEnumerable<OrderInfo> GetOrders(SearchParametersOrders filter, out string error);

        IEnumerable<ContractInfo> GetConracts(SearchParametersContracts filter, out string error);
    }
}
