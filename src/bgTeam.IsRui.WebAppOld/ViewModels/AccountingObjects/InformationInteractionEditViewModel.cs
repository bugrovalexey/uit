﻿using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using bgTeam.IsRui.MKRF.Logic.Access;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    [MetadataType(typeof(InformationInteractionEditViewModel_Valid))]
    public class InformationInteractionEditViewModel : ViewModelBase
    {
        #region Ctors

        public InformationInteractionEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;
            ReadOnly = ManagerAccess.CanEdit(new RepositoryDc<AccountingObject>().GetExisting(accountingObjectId)) ==
                       false;

            TypesList = new SelectList(new RepositoryDc<InformationInteractionType>().GetAll(), "Id", "Name");

            var entity = id.HasValue ? new InformationInteractionRepository().GetExisting(id) : null;
            entity.Do(Fill);
        }

        #endregion Ctors

        #region Methods

        protected void Fill(InformationInteraction entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            Purpose = entity.Purpose;
            TypeId = entity.Type.Return(type => type.Id, EntityBase.Default);
            SMEV = entity.SMEV;
            Title = entity.Title;
            Url = entity.Url;
        }

        #endregion Methods

        #region Props

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        [DisplayName(TableInformationInteraction.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableInformationInteraction.PurposeCaption)]
        public string Purpose { get; set; }

        [DisplayName(TableInformationInteraction.TypeCaption)]
        public int TypeId { get; set; }

        [DisplayName(TableInformationInteraction.SmevCaption)]
        public string SMEV { get; set; }

        [DisplayName(TableInformationInteraction.TitleCaption)]
        public string Title { get; set; }

        [DisplayName(TableInformationInteraction.UrlCaption)]
        public string Url { get; set; }

        public SelectList TypesList { get; set; }

        #endregion Props
    }

    public class InformationInteractionEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }
    }
}