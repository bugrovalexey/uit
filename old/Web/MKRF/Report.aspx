﻿<!DOCTYPE html>

<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="ReportPage.aspx.cs" Inherits="GB.MKRF.Web.ReportPage" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Отчёт</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/Content/GB/Styles/Imgs/icon.png" rel="shortcut icon" />
    <link href="/Content/GB/Styles/Fgis/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <pr:PrintControl ID="PrintControl" runat="server" />
    </form>
</body>
</html>
