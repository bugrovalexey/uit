﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.TechnicalSupports
{
    public class AddOrSaveTechnicalSupportContext : ICommandContext
    {
        public AddOrSaveTechnicalSupportContext()
        {
            CharacteristicsOfTechnicalSupportIds = new List<int>();
        }

        public int? xId { get; set; }

        public int? AccountingObjectId { get; set; }

        public string Name { get; set; }

        public int? CategoryKindOfSupportId { get; set; }

        public int? ManufacturerId { get; set; }

        public string ManufacturerName { get; set; }

        public int Amount { get; set; }

        public decimal Summa { get; set; }

        public List<int> CharacteristicsOfTechnicalSupportIds { get; set; }
    }
}
