﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GB.Extentions
{
    public static class CustomAttributeProviderExtensions
    {
        // Methods
        public static T[] GetCustomAttributes<T>(this ICustomAttributeProvider attributeProvider, bool inherit) where T : Attribute
        {
            return (T[])attributeProvider.GetCustomAttributes(typeof(T), inherit);
        }
    }
}
