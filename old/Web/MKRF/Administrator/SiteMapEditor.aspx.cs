﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Web;
using GB.Infrastructure;
using GB.MKRF.Entities.Admin;
using GB.Providers;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTreeList;

namespace GB.MKRF.Web.Administrator
{
    public partial class SiteMapEditor : System.Web.UI.Page
    {
        protected bool isNewNode = false;
        private DBSiteMapNode currentActivity = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            _TreeList.DataBind();

            if (!IsPostBack)
            {
                //_TreeList.ExpandAll();
                _Grid.DataBind();
            }
        }

        #region TreeList
        protected void _TreeList_DataBinding(object sender, EventArgs e)
        {
            string sql = @"select  Site_Map_Node_ID as Id,
									Site_Map_Node_Par_ID as ParentId,
									Site_Map_Node_Url as Url,
									Site_Map_Node_Title as Title,
									Site_Map_Node_Descr,
									Site_Map_Node_Roles,
									Site_Map_Node_Order as OrderIndex,
                                    Site_Map_Node_Visible as Visible
							from    v_Site_Map_Node z
							order by Site_Map_Node_Level,
									Site_Map_Node_Order,
									Site_Map_Node_Title";

            (sender as ASPxTreeList).DataSource = RepositoryBase<DBSiteMapNode>.GetTableViaSQL(sql);//RepositoryBase<DBSiteMapNode>.GetAll().OrderBy(x => x.OrderIndex).ToList();
        }

        protected void _TreeList_InitNewNode(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            isNewNode = true;
        }

        protected void _TreeList_StartNodeEditing(object sender, TreeListNodeEditingEventArgs e)
        {
            currentActivity = RepositoryBase<DBSiteMapNode>.Get(int.Parse(e.NodeKey));
        }

        protected void _TreeList_NodeInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            DBSiteMapNode node = new DBSiteMapNode();

            if (!Convert.ToBoolean(hf["Root"]))
            {
                if (_TreeList.FocusedNode != null)
                {
                    node.Parent = RepositoryBase<DBSiteMapNode>.Get(int.Parse(_TreeList.FocusedNode.Key));
                }
            }

            SaveRow(node);

            reloadTree(e);
        }

        protected void _TreeList_NodeUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            DBSiteMapNode node = RepositoryBase<DBSiteMapNode>.Get(int.Parse(e.Keys[0] as string));

            int parentId = Convert.ToInt32(hf["Parent"]);

            if (parentId != node.ParentId)
            {
                node.Parent = RepositoryBase<DBSiteMapNode>.Get(parentId);
            }

            SaveRow(node);

            reloadTree(e);
        }

        protected void _TreeList_NodeDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            DBSiteMapNode node = RepositoryBase<DBSiteMapNode>.Get(int.Parse(e.Keys[0] as string));

            foreach (NodeToRole item in node.NodeToRole)
            {
                RepositoryBase<NodeToRole>.Delete(item.Id);
            }

            RepositoryBase<DBSiteMapNode>.Delete(node.Id);
            
            reloadTree(e);
        }

        private void reloadTree(CancelEventArgs e)
        {
            _TreeList.CancelEdit();
            e.Cancel = true;
            _TreeList.DataBind();
        }

        protected void SaveRow(DBSiteMapNode node)
        {
            node.Title = hf["Title"] as string;
            node.Url = hf["Url"] as string;
            node.Visible = (bool)(hf["Visible"]);
            node.OrderIndex = Convert.ToInt32(hf["Order"]);

            RepositoryBase<DBSiteMapNode>.SaveOrUpdate(node);
        }

        protected void _TextBox_Title_DataBinding(object sender, EventArgs e)
        {
            if (isNewNode || currentActivity == null)
                return;

            ASPxTextBox tbox = (ASPxTextBox)sender;

            tbox.Text = currentActivity.Title;
        }

        protected void _TextBox_Url_DataBinding(object sender, EventArgs e)
        {
            if (isNewNode || currentActivity == null)
                return;

            ASPxTextBox tbox = (ASPxTextBox)sender;

            tbox.Text = currentActivity.Url;
        }

        protected void ParentComboBox_DataBinding(object sender, EventArgs e)
        {
            var s = sender as ASPxComboBox;

            s.DataSource = RepositoryBase<DBSiteMapNode>.GetAll().OrderBy(x => x.Title);

            if (currentActivity == null)
                return;

            s.Value = currentActivity.ParentId;
        }

        protected void _Visible_CheckBox_DataBinding(object sender, EventArgs e)
        {
            if (isNewNode || currentActivity == null)
                return;

            ASPxCheckBox cb = (ASPxCheckBox)sender;

            cb.Checked = currentActivity.Visible;
        }

        protected void OrderNumberEdit_DataBinding(object sender, EventArgs e)
        {
            ASPxSpinEdit cb = sender as ASPxSpinEdit;

            cb.Value = 0;

            if (currentActivity == null)
                return;

            cb.Value = currentActivity.OrderIndex;
        }
        #endregion

        #region Grid
        protected void Grid_DataBinding(object sender, EventArgs e)
        {
            if (_TreeList.FocusedNode != null)
            {
                System.Data.DataRowView row = _TreeList.FocusedNode.DataItem as System.Data.DataRowView;

                (sender as ASPxGridView).DataSource = RepositoryBase<DBSiteMapNode>.Get(row.Row[0]).NodeToRole;
            }
        }

        protected void _Grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            RepositoryBase<NodeToRole>.Delete((int)e.Keys[0]);

            _TreeList.CancelEdit();
            e.Cancel = true;

            _TreeList.DataBind();
        }
        
        protected void _Grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            if (_TreeList.FocusedNode == null)
                throw new Exception("Не выбран узел проекта");

            NodeToRole node = new NodeToRole();

            node.Node = RepositoryBase<DBSiteMapNode>.Get(int.Parse(_TreeList.FocusedNode.Key));
            node.Role = RepositoryBase<Role>.Get(int.Parse(hfg["Role"] as string));

            RepositoryBase<NodeToRole>.SaveOrUpdate(node);

            _Grid.CancelEdit();
            e.Cancel = true;

            _Grid.DataBind();
        }
        
        protected void _Role_ComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = (ASPxComboBox)sender;

            box.DataSource = RepositoryBase<Role>.GetAll();

            box.SelectedIndex = 1;
        }
        #endregion

        protected void _CopyRole_Button_Click(object sender, EventArgs e)
        {
            if (_TreeList.FocusedNode == null)
                throw new Exception("Не выбран узел проекта");

            DBSiteMapNode node = RepositoryBase<DBSiteMapNode>.Get(int.Parse(_TreeList.FocusedNode.Key));

            foreach (DBSiteMapNode item in node.Childs)
            {
                foreach (NodeToRole nrole in item.NodeToRole)
                {
                    RepositoryBase<NodeToRole>.Delete(nrole.Id);
                }

                foreach (NodeToRole nrole in node.NodeToRole)
                {
                    NodeToRole nodeNew = new NodeToRole();

                    nodeNew.Node = item;
                    nodeNew.Role = nrole.Role;

                    RepositoryBase<NodeToRole>.SaveOrUpdate(nodeNew);
                }
            }
        }

        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            Singleton<GbSiteMapProvider>.Instance.Reset();
        }
    }
}