﻿namespace bgTeam.IsRui.Common.Extentions
{
    using Firefly.SimpleXls.Attributes;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [XlsSheet(Name = "FinancingReportPositions")]
    public class XlsOrderViewModelExtension
    {
        [XlsHeader(Name = "№ п/п")]
        public int? Id { get; set; }

        [XlsHeader(Name = "Структурное подразделение")]
        public string SubDivision { get; set; }

        [XlsHeader(Name = "Статья")]
        public string Article { get; set; }

        [XlsHeader(Name = "Объект учёта")]
        public string AccountingObject { get; set; }

        [XlsHeader(Name = "Наименование")]
        public string Name { get; set; }

        [XlsHeader(Name = "I квартал")]
        public decimal Quart1 { get; set; }

        [XlsHeader(Name = "II квартал")]
        public decimal Quart2 { get; set; }

        [XlsHeader(Name = "III квартал")]
        public decimal Quart3 { get; set; }

        [XlsHeader(Name = "IV квартал")]
        public decimal Quart4 { get; set; }

        [XlsHeader(Name = "Итого")]
        public decimal Summ { get; set; }
    }
}
