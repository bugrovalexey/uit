﻿using Microsoft.AspNetCore.Http.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.bgTeam.IsRui.Unit.Common;
using Xunit;

namespace Test.bgTeam.IsRui.Unit.Tests
{
    public class Tests
    {
        private const string FILE_CONTEN = "Test file.";

        Factory _factory;

        public Tests()
        {
            _factory = new Factory();
        }

        [Fact]
        public void Test_FilePrividerRui_OpenFileToRead()
        {
            var provider = _factory.fileProviderRui;
            string file = string.Empty;

            using (var stream = provider.OpenFileToRead(7))
            using (var sr = new StreamReader(stream))
            {
                file = sr.ReadToEnd();
            }

            Assert.Equal(FILE_CONTEN, file);
        }

        [Fact]
        public void Test_FilePrividerRui_ReadFileAsync()
        {
            //var provider = _factory.fileProviderRui;

            //var fileByte = provider.ReadFileAsync(7).Result;

            //Assert.Equal(FILE_CONTEN, Encoding.UTF8.GetString(fileByte));
        }

        [Fact]
        public void Test_FileProviderRui_Save_Move()
        {
            var provider = _factory.fileProviderRui;

            var byteStr = Encoding.UTF8.GetBytes(FILE_CONTEN);
            var ms = new MemoryStream(byteStr);

            var file = new FormFile(ms, 0, byteStr.LongLength, "Test", "TestFile");
            var fileName = provider.SaveAsync(file).Result;

            provider.MoveToGeneric(Guid.Parse(fileName), 7, true);

            Assert.True(File.Exists("WebRootPath\\Files\\7"));
        }

        [Fact]
        public void Test_FileProviderRui_Delete()
        {
            var provider = _factory.fileProviderRui;

            var byteStr = Encoding.UTF8.GetBytes(FILE_CONTEN);
            var ms = new MemoryStream(byteStr);

            var file = new FormFile(ms, 0, byteStr.LongLength, "Test", "TestFile");
            var fileName = provider.SaveAsync(file).Result;

            provider.MoveToGeneric(Guid.Parse(fileName), 111, true);

            provider.DeleteFile(111);

            Assert.False(File.Exists("WebRootPath\\Files\\111"));
        }
    }
}
