﻿using System;
using System.ComponentModel;
using System.Web;
using DevExpress.Web.ASPxEditors;
using GB.MKRF.Report;
using GB.Report;

namespace GB.MKRF.Web.Common
{
    public partial class ExportReportControl : System.Web.UI.UserControl
    {
        public string ExportName { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //EventAddExportReportType();

                //if (TypeReportComboBox.Items.Count > 0)
                //    TypeReportComboBox.SelectedIndex = 0;


                FormatReportComboBox.Items.Add("PDF", ExportFormat.Pdf, "/Content/Style/Coord/portal/pdf_16.jpg");
                FormatReportComboBox.Items.Add("RTF", ExportFormat.Rtf, "/Content/Style/Coord/portal/rtf_16.jpg");
                FormatReportComboBox.Items.Add("XLS", ExportFormat.Xls, "/Content/Style/Coord/portal/xls_16.jpg");

                FormatReportComboBox.SelectedIndex = 0;
            }

            FormatReportComboBox.ShowImageInEditBox = true;
            FormatReportComboBox.ShowLoadingPanelImage = true;
        }

        protected void ExportButton_Click(object sender, EventArgs e)
        {
            if (this.IsPostBack )
            {
                ReportTypeExport type = ReportTypeExport.Plan2012NewSectionFull;
                ExportFormat format = (ExportFormat)Enum.Parse(typeof(ExportFormat), FormatReportComboBox.SelectedItem.Value.ToString());

                ZipFileCollection zip = new ZipFileCollection();

                
                System.Collections.Generic.List<object[]> Data = EventBeforeDataBind();

                if (Data != null && Data.Count > 0)
                {
                    for (int i = 0; i < Data.Count; i++)
                    {
                        ReportCreator.Export(zip, new ReportData(type, format, Data[i]));
                    }

                    ExportName = String.Format("Экспорт_планов_информатизации(формат {0})", format.ToString());
                    zip.SendUser(HttpContext.Current, this.Page, ExportName);
                }
            }
        }
       

        //[Category("Data")]
        //public event ExportReportTypeEventHandler AddExportReportType;

        //protected void EventAddExportReportType()
        //{
        //    if (AddExportReportType != null)
        //    {
        //        ListEditItemCollection items = new ListEditItemCollection();
        //        items.Add("В обычном виде", ReportTypeExport.Plan2012NewSectionFull);
        //        AddExportReportType(this, items);
        //    }
        //}
        

        [Category("Data")]
        public event ExportDataEventHandler BeforeDataBind;

        protected System.Collections.Generic.List<object[]> EventBeforeDataBind()
        {
            if (BeforeDataBind != null)
            {
                ExportDataEventArgs e = new ExportDataEventArgs();
                
                BeforeDataBind(this, e);

                return e.Data;
            }

            return null;
        }
    }

    public delegate void ExportDataEventHandler(ExportReportControl sender, ExportDataEventArgs e);
    public delegate void ExportReportTypeEventHandler(ExportReportControl sender, DevExpress.Web.ASPxEditors.ListEditItemCollection item);

    public class ExportDataEventArgs : EventArgs
    {
        System.Collections.Generic.List<object[]> _Data;

        public System.Collections.Generic.List<object[]> Data
        {
            get 
            { 
                return _Data; 
            }
            set 
            { 
                _Data = value; 
            }
        }
        
        public ExportDataEventArgs()
        {
            _Data = new System.Collections.Generic.List<object[]>();
        }
    }
}