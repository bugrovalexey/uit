﻿
namespace Uviri.Coord.Web.Search
{
    public enum DocField
    {
        Id = 0,
        Type = 1,
        Num = 2,
        Department = 3,
        Year1 = 4,
        ModDate = 5,
        Author = 6,
        Status = 7,
        sF = 8,
        sR = 9,
        sV = 10,
        sSum = 11,
        sY0 = 12,
        sY1 = 13,
        sY2 = 14,
        sAddY0 = 15,
        sAddY1 = 16,
        sAddY2 = 17,
        sSumY0 = 18,
        sSumY1 = 19,
        sSumY2 = 20,
        Curator = 21,
        IKT = 22,
        Text = 23,
        Name = 24,
        Year2 = 25,
        OwnerPTI = 26,
        OwnerId = 27,
        OwnerInfo = 28
    }
}
