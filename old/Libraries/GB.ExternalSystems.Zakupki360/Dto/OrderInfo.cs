﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.ExternalSystems.Zakupki360.Dto
{
    public class OrderInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string OrderNumber { get; set; }

        public DateTime? PublishDate { get; set; }

        public DateTime? OrderEndingDate { get; set; }

        public string CodeBK { get; set; }

        public string TenderType { get; set; }

        public string TenderStage { get; set; }

        public string Customer { get; set; }

        public string ContactAddress { get; set; }

        public decimal Price { get; set; }

        public string Url { get; set; }
    }
}
