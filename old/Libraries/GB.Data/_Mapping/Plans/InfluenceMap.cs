﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class InfluenceMap : ClassMapBase<Influence>
    {
        public InfluenceMap()
            : base("Influence")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Indicator, "Influence_Ind");
            Map(x => x.Document, "Influence_Document");

            References<Reference>(x => x.Reference, "Reference_ID").Not.Nullable();
        }
    }
}
