﻿namespace bgTeam.IsRui.WebApp.Admin.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.IsRui.WebApp.Admin.Domain;

    public interface IBuilderQuery
    {
        Task<string> BuildAsync(MetaDictionary dict, IEnumerable<MetaColumn> columns);

        Task<string> BuildWithDelete(MetaDictionary dict, IEnumerable<MetaColumn> columns);

        Task<string> BuildItemAsync(MetaDictionary dict, IEnumerable<MetaColumn> columns, int itemId);

        string BuildGetPrimaryKey(MetaDictionary dict, MetaColumn column);

        string BuildInsert(MetaDictionary dict, IEnumerable<MetaColumn> columns);

        string BuildUpdate(MetaDictionary dict, IEnumerable<MetaColumn> columns);

        string BuildDelete(MetaDictionary dict, MetaColumn column);

        string BuildDeleteWithDelete(MetaDictionary dict, MetaColumn column);

        string BuildSelectList(MetaDictionary dict, string fieldValue, string fieldName);
    }
}