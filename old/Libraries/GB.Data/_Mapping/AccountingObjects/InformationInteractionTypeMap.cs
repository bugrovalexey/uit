﻿using GB.Data.AccountingObjects;

namespace GB._Mapping.AccountingObjects
{
    internal class InformationInteractionTypeMap : ClassMapHiLo<InformationInteractionType>
    {
        public InformationInteractionTypeMap()
            : base("Information_Exchange_Type")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Information_Exchange_Type_Name");
        }
    }
}