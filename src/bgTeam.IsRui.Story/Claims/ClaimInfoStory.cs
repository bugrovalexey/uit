﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimInfoStory : IStory<ClaimInfoStoryContext, ClaimInfoDto>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IMapperBase _mapper;
        private readonly IUserRepository _userRepository;
        private readonly IStory<WorkflowStatusGetNextListStoryContext, IEnumerable<StatusDto>> _storyWorkflow;
        private readonly IStory<ClaimGetUserCommentStoryContext, WorkflowUserConclusionDto> _conclusionStory;
        private readonly IStory<ClaimGetDocumentsStoryContext, IEnumerable<ClaimDocumentDto>> _docStory;
        private readonly IStory<ClaimGetActsStoryContext, IEnumerable<ClaimActDto>> _actStory;

        public ClaimInfoStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase,
            IUserRepository userRepository,
            IStory<WorkflowStatusGetNextListStoryContext, IEnumerable<StatusDto>> storyWorkflow,
            IStory<ClaimGetUserCommentStoryContext, WorkflowUserConclusionDto> conclusionStory,
            IStory<ClaimGetDocumentsStoryContext, IEnumerable<ClaimDocumentDto>> docStory,
            IStory<ClaimGetActsStoryContext, IEnumerable<ClaimActDto>> actStory)
        {
            _repositoryRui = repositoryRui;
            _mapper = mapperBase;
            _userRepository = userRepository;
            _storyWorkflow = storyWorkflow;
            _conclusionStory = conclusionStory;
            _docStory = docStory;
            _actStory = actStory;
        }

        public ClaimInfoDto Execute(ClaimInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ClaimInfoDto> ExecuteAsync(ClaimInfoStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var user = _userRepository.GetCurrent();

            var claim = _repositoryRui
                .Get<Claim>(q =>
                    q.Where(x => x.Id == context.ClaimId)
                    .JoinQueryOver(x => x.PlansActivity)
                    .Where(x => x.Department.Id == user.Department.Id));

            if (claim == null)
            {
                throw new IsRui.Common.IsRuiException("Заявка не найдена");
            }

            var dto = _mapper.Map(claim, new ClaimInfoDto());
            dto.StatusGetNext = await _storyWorkflow
                .ExecuteAsync(
                new WorkflowStatusGetNextListStoryContext
                {
                    WorkflowId = dto.WorkflowId
                });

            dto.Conclusion = await _conclusionStory
                .ExecuteAsync(new ClaimGetUserCommentStoryContext
                {
                    ClaimId = dto.Id
                });

            dto.Acts = await _actStory.ExecuteAsync(
                new ClaimGetActsStoryContext
                {
                    ClaimId = dto.Id
                });

            dto.Documents = await _docStory.ExecuteAsync(
                new ClaimGetDocumentsStoryContext
                {
                    ClaimId = dto.Id
                });

            return dto;
        }
    }
}
