﻿using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.WorksAndGoods;

namespace GB._Mapping.AccountingObjects.WorksAndGoods
{
    internal class SoftwareMap : ClassMapHiLo<Software>
    {
        public SoftwareMap()
            : base("OU_Software")
        {
        }

        protected override void InitMap()
        {
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
            Map(x => x.Name, "OU_Software_Name").Not.Nullable();
            References(x => x.CategoryKindOfSupport, "VO_Category_ID").Not.Nullable();
            References(x => x.Manufacturer, "Producer_ID").Not.Nullable();
            Map(x => x.Amount, "OU_Software_Count").Not.Nullable();
            Map(x => x.Summa, "OU_Software_Sum").Not.Nullable();
            Map(x => x.Right, "OU_Software_Rights").CustomType<RightsOnSoftwareEnum>().Not.Nullable();

            HasOne(x => x.CharacteristicsOfLicense).PropertyRef(z => z.Software).Cascade.Delete();
        }
    }
}