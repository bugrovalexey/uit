﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    class ExternalUserInterfaceOfIsMap : ClassMapHiLo<ExternalUserInterfaceOfIs>
    {
        public ExternalUserInterfaceOfIsMap()
            : base("OU_Externel_User_Interface")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Externel_User_Interface_Name").Not.Nullable();
            Map(x => x.Purpose, "OU_Externel_User_Interface_Goal");
            Map(x => x.Url, "OU_Externel_User_Interface_URL");

            References(x => x.TypeOfInterface, "Interface_Type_ID");
            References(x => x.Owner, "OU_ID").Not.Nullable();
        }

    }
}
