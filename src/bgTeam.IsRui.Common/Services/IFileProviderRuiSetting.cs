﻿namespace bgTeam.IsRui.Common.Services
{
    public interface IFileProviderRuiSetting
    {
        string TempFolderPhysicalPath { get; }

        string FilesFolderPhysicalPath { get; }
    }
}
