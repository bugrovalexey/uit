﻿using System;
using System.Collections.Generic;
using System.Xml;
using Core.EBEntity;

namespace Core.Import.NSIservice.Publish
{
    public class PublishHelper<T> where T : BaseEBEntity
    {
        protected T currentItem;

        public PublishHelper(T item)
        {
            currentItem = item;
        }

        public string Execute()
        {
            string err = string.Empty;

            try
            {
                err = publishElement();
            }
            catch (Exception ee)
            {
                return "Id = "+ currentItem.ID + "--" + ee;
            }

            return err;
        }

        private standardBankService CreateServiceReference()
        {
            standardBankService srv = new standardBankServiceClient(); //standardBankService();
            //srv.Url = ConfigurationManager.AppSettings["EBServiceUrl"];
            //srv.Timeout = Timeout.Infinite;
            //srv.UseDefaultCredentials = true;
            return srv;
        }

        private string publishElement()
        {
            ElementRevisionInfo elementRevision = new ElementRevisionInfo();
            elementRevision.ComponentCode = currentItem.CatalogCode;
            elementRevision.Code = currentItem.ID.ToString();
            
            if (currentItem.DocDateSpecified)
            {
                var dateStart = new DateTime();
                if(!DateTime.TryParse(currentItem.DateStart, out dateStart))
                    dateStart = new DateTime(1900, 1, 1);
                
                elementRevision.StartDate = dateStart;
            }
            else
            {
                elementRevision.StartDate = Convert.ToDateTime("01.01.1900");
            }

            
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(ReferenceTranslator.GetXmlDataFromCanonicalData(currentItem));
            elementRevision.ElementXml = xdoc.DocumentElement;
            var result = string.Empty;
            standardBankService srv = CreateServiceReference();
            {
                BaseMessageType message = CreateMessageType();
                message.MessageData.AppData.Item = CreatePublishElement(elementRevision);
                var requestRequest = new requestRequest(message);
                var response = srv.request(requestRequest);
                if (response.Response.MessageData.AppData.response.status == "FAILURE")
                {
                    result = response.Response.MessageData.AppData.response.errorMessage;
                }
            }
            if (!string.IsNullOrEmpty(result) || !currentItem.DocDateSpecified)
                return result;

            var dateEnd = new DateTime();
            if (DateTime.TryParse(currentItem.DateEnd, out dateEnd))
            {
                return publishAnnulElement(dateEnd);
            }
            return result;
        }

        private string publishAnnulElement(DateTime dateEnd)
        {
            ElementRevisionInfo elementRevision = new ElementRevisionInfo();
            elementRevision.ComponentCode = currentItem.CatalogCode;
            elementRevision.Code = currentItem.ID.ToString();

            elementRevision.StartDate = DateTime.Now;

            annulElement request = new annulElement();
            request.componentCode = elementRevision.ComponentCode;
            request.code = elementRevision.Code;
            request.endDate = dateEnd;

            standardBankService srv = CreateServiceReference();
            {
                BaseMessageType message = CreateMessageType();

                message.MessageData.AppData.Item = request;
                var requestRequest = new requestRequest(message);
                var response = srv.request(requestRequest);
                if (response.Response.MessageData.AppData.response.status == "FAILURE")
                {
                    return response.Response.MessageData.AppData.response.errorMessage;
                }
                
                return "";
            }
        }

        public string FillElemnets(long lastSSN = 0)
        {
            standardBankService srv = CreateServiceReference();
            {
                BaseMessageType message = CreateMessageType();
                request reque = new getLastUpdates()
                {
                    componentCode = currentItem.CatalogCode, 
                    lastSsn = lastSSN,
                    date = DateTime.Now
                };
                message.MessageData.AppData.Item = reque;
                var requestRequest = new requestRequest(message);
                var response = srv.request(requestRequest);
                if (response.Response.MessageData.AppData.response.status == "FAILURE")
                {
                    return response.Response.MessageData.AppData.response.errorMessage;
                }

                return response.Response.MessageData.AppData.response.Any[0].InnerXml;
            }
        }

        public static publishElementRevision CreatePublishElement(ElementRevisionInfo elementRevision)
        {
            publishElementRevision request = new publishElementRevision();
            request.componentCode = elementRevision.ComponentCode;
            request.code = elementRevision.Code;
            request.startDate = elementRevision.StartDate;
            request.Any = elementRevision.ElementXml;
            return request;
        }

        public static BaseMessageType CreateMessageType()
        {
            BaseMessageType message = new BaseMessageType();
            //message.Message = new MessageType();
            //message.Message.Date = DateTime.Now;
            //message.Message.Sender = new orgExternalType();
            //message.Message.Sender.Code = "UVIRI";

            message.MessageData = new MessageDataType();
            message.MessageData.AppData = new AppDataType();

            return message;
        }

        public static IEnumerable<T> GetDictionary<T>() where T : BaseEBEntity, new()
        {
            return GetDictionary<T>(0);
        }
        private static IEnumerable<T> GetDictionary<T>(long lastSSN = 0) where T : BaseEBEntity, new()
        {
            T item = new T();
            var list = new List<T>();
            var helper = new PublishHelper<T>(item);
            var result = helper.FillElemnets(lastSSN);

            var x = new XmlDocument();
            var rootnode = x.CreateElement("root");
            rootnode.InnerXml = result.Replace("nsi:", string.Empty).Replace("q1:", string.Empty).Replace(@"xmlns:nsi=""http://nsi.gosuslugi.ru/rev111111""", string.Empty);

            x.AppendChild(rootnode);

            var navigator = x.CreateNavigator();
            var iterator = navigator.Select("root/elementRevision");
            
            while (iterator.MoveNext())
            {
                if (iterator.Current == null)
                    continue;
                
                
                var element = iterator.Current.Select(item.ClassName);
                element.MoveNext();
                if(element.Current == null)
                    continue;

                if (item is EBOGV)
                {
                    var a = element.Current.OuterXml
                        .Replace("q1:", string.Empty)
                        .Replace(@"xmlns:startDate=""http://ensi.gosuslugi.ru/rev111111""", string.Empty)
                        .Replace(@"xmlns=""http://ensi.gosuslugi.ru/rev111111""", string.Empty)
                        .Replace(@"xmlns:endDate=""http://ensi.gosuslugi.ru/rev111111""", string.Empty)
                        .Replace(@"endDate:endDate", "endDate")
                        .Replace(@"startDate:startDate", "startDate")
                        .Replace(@"xmlns:q1=""http://ensi.gosuslugi.ru/rev111111/ogv""", string.Empty);

                    element.Current.OuterXml = a;
                }
                try
                {
                    item = ReferenceTranslator.GetCanonicalDataFromXmlData<T>(element.Current.OuterXml);
                }
                catch
                {
                    continue;
                }
                
                element = iterator.Current.Select("startDate");
                element.MoveNext();
                if (element.Current != null)
                    item.DateStart = element.Current.InnerXml;

                element = iterator.Current.Select("endDate");
                element.MoveNext();
                if (element.Current != null)
                    item.DateEnd = element.Current.InnerXml;

                list.Add(item);
            }

            // НСИ отдаёт только первые 1000 элементов, если элементов больше, то надо взять последний SSN и вызывать НСИ ещё раз
            if (list.Count > 999)
            {
                var iteratorSSN = navigator.Select("root/elementRevision[last()]/ssn");
                iteratorSSN.MoveNext();
                if (iteratorSSN.Current != null)
                {
                    var valueSSN = iteratorSSN.Current.ValueAsLong;
                    list.AddRange(GetDictionary<T>(valueSSN));
                }
            }
            return list;
            //var distinct = list.Distinct<T>(new BaseComparer<T>());
            //return distinct;
        }

        public class BaseComparer<T> : IEqualityComparer<T> where T : BaseEBEntity
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(T x, T y)
            {
                if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                    return false;

                return x.ID == y.ID && x.Code == y.Code && x.Name == y.Name;
            }

            public int GetHashCode(T x)
            {
                if (Object.ReferenceEquals(x, null)) 
                    return 0;

                return x.ID.GetHashCode() ^ x.CodeName.GetHashCode();
            }
        }
    }
}
