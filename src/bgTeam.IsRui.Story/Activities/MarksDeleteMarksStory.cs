﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class MarksDeleteMarksStory : IStory<MarksDeleteMarksStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;

        public MarksDeleteMarksStory(ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _factory = sessionNHibernateFactory;
        }

        public bool Execute(MarksDeleteMarksStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(MarksDeleteMarksStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            bool result;

            using (var session = _factory.OpenSession())
            {
                await session.DeleteAsync<PlansActivityMark>(context.Id.Value);

                result = true;
            }

            return result;
        }
    }
}
