﻿using System.Collections.Generic;

namespace bgTeam.IsRui.Zakupki360.Dto
{
    class ResponseGetOrders
    {
        public string Error { get; set; }

        public IEnumerable<OrderInfo> Orders { get; set; }
    }
}
