﻿using Uviri.MKRF.Entities.Other;

namespace Uviri.MKRF.Mapping.Other
{
    public class SummaryExpertResolutionMap : ClassMapBase<SummaryExpertResolution>
    {
        public SummaryExpertResolutionMap():
            base("Summary_Expert_Resolution")
        {

        }

        protected override void InitMap()
        {
            Map(x => x.Year, "Summary_Expert_Resolution_Year").Not.Nullable();
            Map(x => x.EffectiveAction, "Summary_Expert_Resolution_Effective_Action");
            Map(x => x.ImprovementIKT, "Summary_Expert_Resolution_Inprovement_IKT");
            Map(x => x.ReworkPlan, "Summary_Expert_Resolution_Rework_Plan");
        }
    }
}
