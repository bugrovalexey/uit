﻿using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using System;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Story.AccountingObjects
{
    public class AoCharacteristicsDeleteCharacteristicStory : IStory<AoCharacteristicsDeleteCharacteristicStoryContext, bool>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;

        public AoCharacteristicsDeleteCharacteristicStory(IMapperBase mapper, IRepositoryRui repository, ISessionNHibernateFactory factory)
        {
            _mapper = mapper;
            _repository = repository;
            _factory = factory;
        }

        public bool Execute(AoCharacteristicsDeleteCharacteristicStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(AoCharacteristicsDeleteCharacteristicStoryContext context)
        {
            bool result = false;

            using (var session = _factory.OpenSession())
            {
                try
                {

                    await session.DeleteAsync<AccountingObjectCharacteristics>(context.Id);

                    result = true;
                }
                catch (Exception exc)
                {
                    await session.RollbackAsync();

                    throw;
                }
            }

                return result;
        }
    }
}
