﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Helpers;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ExecutorMap : ClassMapBase<Executor>
	{
        public ExecutorMap()
            : base("Govt_Organ_Dep_List")
		{
//            Table("w_");

//            Id(x => x.Id, "Govt_Organ_Dep_List_ID").GeneratedBy.Custom(typeof(IdGenerator));

            


//            string sql = @"  @Govt_Organ_ID = ?
//                            ,@Govt_Organ_Dep_ID = ?
//                            ,@Govt_Organ_Dep_List_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Govt_Organ_Dep_List_Insert " + sql).Check.None();
            
//            SqlUpdate(@"exec ZZZ_Prc_Govt_Organ_Dep_List_Update " + sql).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Govt_Organ_Dep_List_Delete
//                            @Govt_Organ_Dep_List_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            References<Department>(x => x.Parent, "Govt_Organ_ID").Not.Nullable();
            References<Department>(x => x.Child, "Govt_Organ_Dep_ID").Not.Nullable();

            ApplyFilter<SecurityFilter>("Govt_Organ_Dep_List_ID in (select * from Get_Object_List(:UserId, " + (int)TableTypeEnum.Executors + ", 'r'))");
        }
    }
}
