﻿using System;
using System.IO;

namespace GB
{
    /// <summary>
    /// Включает различные методы для работы с файлами
    ///
    /// Рекомендации по работе с файлами:
    ///
    /// 1. Загрузить файл во временную папку - SaveInTemp. Метод сохранит файл и вернёт его GUID
    /// 2. Сохранить информацию о файле в БД (название, дату и всё такое) и получить Id записи (таблицы Document)
    /// 3. Используя GUID и Id Переместить файл из Temp в Files методом MoveToGeneric(GUID, Id)
    /// 4. Если файл нужно удалить вызывать DeleteFile(key). В зависимости от типа key файл удаляется либо из Temp, либо из Files
    /// 5. Для получения полного пути к файлу использовать GetPhysicalPath(key). Важно: если key типа GUID будет ссылка на файл в Temp,
    ///    если key типа int или string будет ссылка на файл в папке Files
    /// </summary>
    public static class FileProvider
    {
        #region fields

        private const string TEMP_FOLDER_KEY = "TempFolder";
        private const string FILES_FOLDER_KEY = "FilesFolder";
        private const string LOGS_FOLDER_KEY = "LogsFolder";

        private const string TEMP_FOLDER = "~/Temp/";
        public const string FILES_FOLDER = "~/Files/";
        private const string LOGS_FOLDER = "~/Logs/";

        private static readonly string FilesFolderPhysicalPath;
        private static readonly string TempFolderPhysicalPath;
        private static readonly string LogsFolderPhysicalPath;

        #endregion fields

        #region ctor

        static FileProvider()
        {
            FilesFolderPhysicalPath = InitPhysicalFolder(FILES_FOLDER, FILES_FOLDER_KEY);
            TempFolderPhysicalPath = InitPhysicalFolder(TEMP_FOLDER, TEMP_FOLDER_KEY);
            LogsFolderPhysicalPath = InitPhysicalFolder(LOGS_FOLDER, LOGS_FOLDER_KEY);
        }

        #endregion ctor

        #region public

        /// <summary>
        /// Вызывается для инициализации конструктора
        /// </summary>
        public static void Init()
        {
            if (!Directory.Exists(LogsFolderPhysicalPath))
                throw new Exception("Не найдено хранилище логов. Проверьте параметр - " + LOGS_FOLDER_KEY);
            if (!Directory.Exists(FilesFolderPhysicalPath))
                throw new Exception("Не найдено хранилище файлов. Проверьте параметр - " + FILES_FOLDER_KEY);
            if (!Directory.Exists(TempFolderPhysicalPath))
                throw new Exception("Не найдено хранилище временных файлов. Проверьте параметр - " + TEMP_FOLDER_KEY);
        }

        #region CRUD

        /// <summary>
        /// Сохранить в темповую папку
        /// </summary>
        public static string SaveInTemp(Stream stream, string name)
        {
            string fname = !string.IsNullOrEmpty(name) ? name : Guid.NewGuid().ToString();

            string fpath = string.Concat(TempFolderPhysicalPath, fname);

            using (Stream file = File.OpenWrite(fpath))
            {
                byte[] buffer = new byte[8 * 1024];
                int len;
                while ((len = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    file.Write(buffer, 0, len);
                }
            }

            return fpath;
        }

        /// <summary>
        /// Перемещает файл из папки Temp в папку Files
        /// </summary>
        /// <param name="guid">Guid в папке Temp</param>
        /// <param name="id">Id c которым сохранить файл в ~/Files/</param>
        /// <param name="replace"></param>
        public static void MoveToGeneric(Guid guid, int id, bool replace = false)
        {
            string source = GetPhysicalPath(guid);
            string dest = GetPhysicalPath(id);
            if (replace && File.Exists(dest))
            {
                File.Delete(dest);
            }

            File.Move(source, dest);
        }

        /// <summary>Удаляет файл</summary>
        public static void DeleteFile(object id)
        {
            string path = GetPhysicalPath(id);
            File.Delete(path);
        }

        #endregion CRUD

        /// <summary>
        /// Полный путь к файлу
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetPhysicalPath(object id)
        {
            if (id is int)
            {
                return string.Concat(FilesFolderPhysicalPath, id);
            }

            if (id is Guid)
            {
                return string.Concat(TempFolderPhysicalPath, id);
            }

            return null;
        }


        /// <summary>
        /// Генерирует новое случайное название файла
        /// </summary>
        public static string GenerateFileName()
        {
            var fileName = Guid.NewGuid();
            return fileName.ToString();
        }

        #endregion public

        #region private

        //TODO : выпилить - System.Configuration.ConfigurationManager.AppSettings
        private static string InitPhysicalFolder(string directory, string settingsKey)
        {
            string settingsValue = System.Configuration.ConfigurationManager.AppSettings[settingsKey];
            if (!string.IsNullOrWhiteSpace(settingsValue))
            {
                directory = settingsValue;
            }

            directory = GetAbsoluteDirectory(directory);

            Logger.Info(string.Format("Инициализация директории - {0} : {1}", settingsKey, directory));

            if (!Directory.Exists(directory))
            {
                try
                {
                    Directory.CreateDirectory(directory);
                }
                catch (Exception exp)
                {
                    Logger.Error(exp);
                }
            }

            return directory;
        }

        //TODO : Выпилить System.Web.Hosting.HostingEnvironment
        private static string GetAbsoluteDirectory(string directory)
        {
            var result = System.Web.Hosting.HostingEnvironment.MapPath(directory);
            return result;
        }

        #endregion private

    }
}