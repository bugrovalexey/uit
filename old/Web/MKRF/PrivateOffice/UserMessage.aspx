﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateOffice/UserOffice.master" AutoEventWireup="true" CodeBehind="UserMessage.aspx.cs" Inherits="GB.MKRF.Web.PrivateOffice.UserMessage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function AddMessage() {
            var mes = MessageTextBox.GetText();

            if (mes) {
                AddMessageCallBack.PerformCallback(mes);
                return;
            }

            ErrorPanelShow("Поле <b>Вопрос</b> не заполнено, введите текст.");
        }

        function FormClose() {
            ErrorPanelHide();
            AddMessagePopupControl.Hide();
            MessageTextBox.SetText(null);
        }
    </script>
    <h3><a class="command" href="javascript:void(0)" onclick="AddMessagePopupControl.Show()" >Задать вопрос?</a></h3>
    <table align="center" width="100%">
        <tr>
            <td width="30px"></td>
            <td></td>
        </tr>
        <%=this.GetMessageList() %>
    </table>
    <dxpc:ASPxPopupControl ID="MessagePopupControl" runat="server" ClientInstanceName="AddMessagePopupControl"
        Modal="True" AllowDragging="true" 
        CloseAction="None" CloseButtonImage-Width="0" CloseButtonStyle-Cursor="default"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" 
        Width="520"
        HeaderText="Вопрос администратору..." >
        <ContentStyle BackColor="#E2EEFA">
            <Paddings Padding="0" />
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl >
                <div class="div_panel_sep"></div>
                <table align="center" width="95%" >
                    <tr>
                        <td>
                            <dxe:ASPxMemo runat="server" ID="MessageTextBox" ClientInstanceName="MessageTextBox"
                                Width="100%" Rows="6" NullText="Введите текст вопроса" >
                                <NullTextStyle ForeColor="#aaaaaa" />
                            </dxe:ASPxMemo>
                        </td>
                    </tr>
                </table>
                <table style="text-align: center; padding-top: 6px; width: 90%;">
                    <tr>
                        <td style="text-align: right; width: 50%;">
                            <a class="command" href="javascript:void(0)" onclick="FormClose();" >Отмена</a>
                        </td>
                        <td>
                            <dxe:ASPxButton runat="server" ID="AddMessageButton" Text="Отправить" AutoPostBack="False">
                                <ClientSideEvents Click="function(s,e){ AddMessage(); }" />
                            </dxe:ASPxButton>
                            <dxc:ASPxCallback runat="server" ID="AddMessageCallBack" ClientInstanceName="AddMessageCallBack" OnCallback="AddMessageCallBack_Callback" >
                                <ClientSideEvents CallbackComplete="function(s,e){ 
                                    if (e.result == 'true') {
                                        FormClose();

                                        __doPostBack('__Page','');
                                    }
                                    else {
                                        ErrorPanelShow(e.result);
                                    }
                                }" />
                            </dxc:ASPxCallback>
                        </td>
                    </tr>
                </table>
                <div class="div_panel_sep"></div>
                <div id="div_error_panel" class="div_error_panel_none"></div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
</asp:Content>
