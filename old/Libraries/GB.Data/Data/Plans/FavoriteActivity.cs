﻿using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Entities.Plans
{
    public class FavoriteActivity : EntityBase
    {
        public virtual PlansActivity PlansActivity { get; set; }

        public virtual User User { get; set; }

        public virtual bool IsFavorite { get; set; }
    }
}