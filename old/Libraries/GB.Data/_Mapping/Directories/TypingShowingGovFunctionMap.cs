﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class TypingShowingGovFunctionMap : ClassMapBase<TypingShowingGovFunction>
    {
        public TypingShowingGovFunctionMap() :
            base("Typical_Showing_Gov_Function")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Typical_Showing_Gov_Function_Name");
            References<MeasureUnit>(x => x.Measure, "Measure_Unit_ID");

            HasManyToMany(x => x.ActivityTypes)
                .Table("Typical_Showing_Type")
                .ParentKeyColumn("Typical_Showing_Gov_Function_ID")
                .ChildKeyColumn("Plans_Activity_Type2_ID")
                .ReadOnly();

            HasManyToMany(x => x.IKTComponents)
                .Table("Typical_Showing_IKT_Component")
                .ParentKeyColumn("Typical_Showing_Gov_Function_ID")
                .ChildKeyColumn("IKT_Component_ID")
                .ReadOnly();
        }
    }
}