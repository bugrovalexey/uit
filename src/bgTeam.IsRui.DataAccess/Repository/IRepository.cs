﻿namespace bgTeam.IsRui.DataAccess.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Domain.Entities;
    using NHibernate;

    public interface IRepositoryRui
    {
        T Get<T>(object id);

        T Get<T>(Action<IQueryOver<T, T>> query)
            where T : EntityBase;

        IList<T> GetAll<T>();

        IList<T> GetAllEx<T>(Action<IQueryOver<T, T>> query)
            where T : EntityBase;

        T GetDefault<T>();

        T GetExisting<T>(object id);

        T GetOrDefault<T>(object id);

        IList<TReturn> Select<T, TReturn>(Action<IQueryOver<T, T>> query)
            where T : EntityBase;

        TReturn SelectSingle<T, TReturn>(Action<IQueryOver<T, T>> query)
            where T : EntityBase;

        Task<IEnumerable<T>> SelectExecuteAsync<T>(string str, ParameterProcedure[] parameters);

        int RowCount<T>(Action<IQueryOver<T, T>> query)
            where T : EntityBase;
    }

    public interface IRepositoryDc<T>
        where T : EntityBase
    {
        T Get(Action<IQueryOver<T, T>> query);

        T Get(object id);

        IList<T> GetAll();

        IList<T> GetAllEx(Action<IQueryOver<T, T>> query);

        T GetDefault();

        T GetExisting(object id);

        T GetOrDefault(object id);

        int RowCount(Action<IQueryOver<T, T>> query);

        IList<TReturn> Select<TReturn>(Action<IQueryOver<T, T>> query);

        TReturn SelectSingle<TReturn>(Action<IQueryOver<T, T>> query);
    }
}