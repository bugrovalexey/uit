﻿using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Domain.Entities.Purchase;

namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    internal class PlansWorkMap : ClassMapHiLo<PlansWork>
    {
        public PlansWorkMap()
            : base("Plans_Work")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Work_Name");
            Map(x => x.Res, "Plans_Work_Res");
            Map(x => x.ExpenseVolumeFB, "Plans_Work_Expense_Vol_FB").Not.Nullable();
            Map(x => x.ExpenseVolumeRB, "Plans_Work_Expense_Vol_RB").Not.Nullable();
            Map(x => x.ExpenseVolumeVB, "Plans_Work_Expense_Vol_VB").Not.Nullable();
            Map(x => x.SubEntry, "Plans_Work_Subentry");
            Map(x => x.Comm, "Plans_Work_Expert_Comm");
            Map(x => x.ExpenseVolumeFB_Fact, "Plans_Work_Expense_Vol_FB_Fact").Not.Nullable();
            Map(x => x.ExpenseVolumeRB_Fact, "Plans_Work_Expense_Vol_RB_Fact").Not.Nullable();
            Map(x => x.ExpenseVolumeVB_Fact, "Plans_Work_Expense_Vol_VB_Fact").Not.Nullable();
            Map(x => x.Num, "Plans_Work_Num");
            Map(x => x.OrigNum, "Plans_Work_Orig_Num");
            Map(x => x.ExpenseVolFBY0, "Plans_Work_P_Vol_FB_Y0");
            Map(x => x.ExpenseVolVBY0, "Plans_Work_P_Vol_VB_Y0");
            Map(x => x.ExpenseAddVolFBY0, "Plans_Work_P_Add_Vol_FB_Y0");
            Map(x => x.ExpenseAddVolVBY0, "Plans_Work_P_Add_Vol_VB_Y0");
            Map(x => x.ExpenseVolFBY1, "Plans_Work_P_Vol_FB_Y1");
            Map(x => x.ExpenseVolVBY1, "Plans_Work_P_Vol_VB_Y1");
            Map(x => x.ExpenseAddVolFBY1, "Plans_Work_P_Add_Vol_FB_Y1");
            Map(x => x.ExpenseAddVolVBY1, "Plans_Work_P_Add_Vol_VB_Y1");
            Map(x => x.ExpenseVolFBY2, "Plans_Work_P_Vol_FB_Y2");
            Map(x => x.ExpenseVolVBY2, "Plans_Work_P_Vol_VB_Y2");
            Map(x => x.ExpenseAddVolFBY2, "Plans_Work_P_Add_Vol_FB_Y2");
            Map(x => x.ExpenseAddVolVBY2, "Plans_Work_P_Add_Vol_VB_Y2");
            Map(x => x.ResultY0, "Plans_Work_P_Res_Y0");
            Map(x => x.ResultY1, "Plans_Work_P_Res_Y1");
            Map(x => x.ResultY2, "Plans_Work_P_Res_Y2");

            Map(x => x.AgreedExpenseVolY0, "Plans_Work_S_Vol_Y0");
            Map(x => x.AgreedExpenseVolY1, "Plans_Work_S_Vol_Y1");
            Map(x => x.AgreedExpenseVolY2, "Plans_Work_S_Vol_Y2");
            Map(x => x.AgreedExpenseAddVolY0, "Plans_Work_S_Add_Vol_Y0");
            Map(x => x.AgreedExpenseAddVolY1, "Plans_Work_S_Add_Vol_Y1");
            Map(x => x.AgreedExpenseAddVolY2, "Plans_Work_S_Add_Vol_Y2");
            Map(x => x.AgreedExpenseVolVBY0, "Plans_Work_S_Vol_VB_Y0");
            Map(x => x.AgreedExpenseVolVBY1, "Plans_Work_S_Vol_VB_Y1");
            Map(x => x.AgreedExpenseVolVBY2, "Plans_Work_S_Vol_VB_Y2");
            Map(x => x.AgreedExpenseAddVolVBY0, "Plans_Work_S_Add_Vol_VB_Y0");
            Map(x => x.AgreedExpenseAddVolVBY1, "Plans_Work_S_Add_Vol_VB_Y1");
            Map(x => x.AgreedExpenseAddVolVBY2, "Plans_Work_S_Add_Vol_VB_Y2");

            Map(x => x.PRSFactRes, "Plans_Work_PRS_Fact_Res");
            Map(x => x.PRSNextPeriod, "Plans_Work_PRS_Next_Period");
            Map(x => x.ReestrNumber, "Plans_Work_Reestr_Num");
            Map(x => x.DetailsActs, "Plans_Work_Rekv_Act");
            Map(x => x.FactBuget, "Plans_Work_Vol_Budg_Fact");
            Map(x => x.PRSContract, "Plans_Work_PRS_Contract");
            Map(x => x.PRSContractor, "Plans_Work_PRS_Contractor");

            Map(x => x.ExpenseVolumeCurrentYear, "Plans_Work_Assgn_Vol_Y0");
            Map(x => x.ExpenseVolumeFirstYear, "Plans_Work_Assgn_Vol_Y1");
            Map(x => x.ExpenseVolumeSecondYear, "Plans_Work_Assgn_Vol_Y2");

            Map(x => x.Result, "Plans_Work_Result_31");
            Map(x => x.BudgetVolume, "Plans_Work_BV_31")
                      .Not.Nullable()
                      .Default("0");

            Map(x => x.Comment, "Plans_Work_Comment");
            Map(x => x.IsDraft, "Plans_Work_Is_Draft");

            Map(x => x.Year, "Plans_Work_Year").CustomType(typeof(YearEnum)).Nullable();
            Map(x => x.SpecialistsCount, "Plans_Work_Spec_Count");
            Map(x => x.Salary, "Plans_Work_Salary");
            Map(x => x.Duration, "Plans_Work_Duration");

            References<PlansActivity>(x => x.PlanActivity, "Plans_Activity_ID").Not.Nullable();
            References<ExpenseDirection>(x => x.ExpenseDirection, "Expense_Direction_ID").Not.Nullable();
            References<ExpenseDirection>(x => x.ReportExpenseDirection, "Report_Expense_Direction_ID").Nullable();
            References<ExpenseType>(x => x.ExpenseType, "Expense_Type_ID").Not.Nullable();
            References<ExpenseType>(x => x.ReportExpenseType, "Report_Expense_Type_ID").Nullable();
            References<GRBS>(x => x.GRBS, "GRBS_ID");
            References<SectionKBK>(x => x.SectionKBK, "Section_KBK_ID").Not.Nullable();
            References<ExpenseItem>(x => x.ExpenseItem, "Expense_Item_ID").Not.Nullable();
            References<WorkForm>(x => x.WorkForm, "Work_Form_ID");
            //References<FgisInfo>(x => x.FgisInfo, "FGIS_Info_ID").Not.Nullable();
            //References<ApfInfo>(x => x.ApfInfo, "APF_Info_ID").Not.Nullable();
            References<OKPD>(x => x.OKPD, "OKDP_ID").Not.Nullable();
            // References<Status>(x => x.Status, "Status_ID"); - в Plans_Work нет поля Status_ID
            References<Contract>(x => x.Contract, "Contract_ID");
            References<ExpenditureItem>(x => x.ExpenditureItem, "Expenditure_Items_ID");
            References<OKVED>(x => x.OKVED, "OKVED_ID");

            HasMany<WorkType>(x => x.WorkType).KeyColumns.Add("Plans_Work_ID").Inverse();
        }
    }
}