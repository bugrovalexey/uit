﻿using Uviri.MKRF.Entities.Prognosis;
using Uviri.MKRF.Entities.Prognosis.SectionIII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionIII
{
    public class AdditionalNeedMap : ClassMapBase<AdditionalNeed>
	{
        public AdditionalNeedMap()
            : base("MF3_18")
        {
//            Table("w_");

//            Id(x => x.Id, "MF3_18_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_MF3_18_Insert
//                          @MF3_18_Num = ?
//                         ,@MF3_18_1 = ?
//                         ,@MF3_18_2 = ?
//						 ,@MF3_18_3 = ?
//						 ,@MF3_18_4 = ?
//						 ,@Mon_Form_ID = ?
//                         ,@MF3_18_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF3_18_Update
//                          @MF3_18_Num = ?
//                         ,@MF3_18_1 = ?
//                         ,@MF3_18_2 = ?
//						 ,@MF3_18_3 = ?
//						 ,@MF3_18_4 = ?
//						 ,@Mon_Form_ID = ?
//                         ,@MF3_18_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF3_18_Delete
//						  @MF3_18_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "MF3_18_Num");
			Map(x => x.FOIVFunc, "MF3_18_1");
			Map(x => x.ISName, "MF3_18_2");
			Map(x => x.Period, "MF3_18_3");
			Map(x => x.Expense, "MF3_18_4");

			References<Prognos>(x => x.Prognos, "Mon_Form_ID").Not.Nullable();
        }
    }
}