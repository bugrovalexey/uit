﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    internal class AccountingObjectCharacteristicsMap : ClassMapHiLo<AccountingObjectCharacteristics>
    {
        public AccountingObjectCharacteristicsMap()
            : base("OU_Charact")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Charact_Name");
            Map(x => x.Norm, "OU_Charact_Nominal_Value");
            Map(x => x.Max, "OU_Charact_Max_Value");
            Map(x => x.Fact, "OU_Charact_Fact");

            Map(x => x.DateBeginning, "OU_Charact_Date");
            Map(x => x.DateEnd, "OU_Charact_End_Date");
            Map(x => x.IsRegister, "OU_Charact_Is_Register").Not.Nullable();

            References(x => x.Type, "OU_Charact_Type_ID").Not.Nullable();
            References(x => x.Unit, "MUnit_ID").Not.Nullable();
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();

        }
    }
}