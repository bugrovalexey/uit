﻿using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.Documents;

namespace GB._Mapping.AccountingObjects
{
    internal class AccountingObjectMap : ClassMapHiLo<AccountingObject>
    {
        public AccountingObjectMap()
            : base("OU")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.CreateDate, "OU_Create_DT").Not.Nullable();
            Map(x => x.FullName, "OU_FName");
            Map(x => x.ShortName, "OU_SName");
            Map(x => x.Number, "OU_Number");
            Map(x => x.Stage, "OU_Stage_ID").Not.Nullable().CustomType<AccountingObjectsStageEnum>();
            Map(x => x.Mode, "OU_Mode").CustomType<AccountingObjectEnum>();

            References(x => x.Type, "OU_Type_ID").Not.Nullable();
            References(x => x.Kind, "OU_Kind_ID").Not.Nullable();
            References(x => x.Status, "OU_Status_ID").Not.Nullable();

            HasMany(x => x.Documents).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();

            Map(x => x.Targets, "OU_Goal");
            Map(x => x.PurposeAndScope, "OU_Range");

            Map(x => x.AdoptionBudgetDate, "OU_Budgetary_Account_Date");


            References(x => x.BK_GRBS, "GRBS_ID");
            References(x => x.BK_PZRZ, "Section_KBK_ID");
            References(x => x.BK_CSR, "Expense_Item_ID");
            References(x => x.BK_WR, "Work_Form_ID");
            References(x => x.BK_KOSGU, "Expenditure_Items_ID");

            Map(x => x.BalanceCost, "OU_Budgetary_Account_Balance").Not.Nullable();
            Map(x => x.AdoptionBudgetBasis, "OU_Budgetary_Account_Basis");


            HasMany(x => x.DocumentsGroundsForCreation).KeyColumns.Add("OU_ID")
                .Where("OU_Document_Type = " + (int)AODocumentGroundsForCreation.DocType).Cascade.Delete().Inverse();

            HasMany(x => x.DocumentsCommissioning).KeyColumns.Add("OU_ID")
                .Where("OU_Document_Type = " + (int)AODocumentCommissioning.DocType).Cascade.Delete().Inverse();

            Map(x => x.CommissioningDate, "OU_Input_In_Operation_Date");

            HasMany(x => x.DocumentsDecommissioning).KeyColumns.Add("OU_ID")
                .Where("OU_Document_Type = " + (int)AODocumentDecommissioning.DocType).Cascade.Delete().Inverse();


            Map(x => x.DecommissioningDate, "OU_Output_From_Operation_Date");
            //TOD переделать столбец
            Map(x => x.DecommissioningReasons, "DecommissioningReasons");

            References(x => x.Department, "Govt_Organ_ID").Not.Nullable();
            References(x => x.ResponsibleDepartment, "Responsible_Govt_Organ_ID");

            HasOne(x => x.ExploitationDepartment).PropertyRef(x => x.AccountingObject).Cascade.Delete();

            References(x => x.ResponsibleCoord, "Responsible_Coord_ID");
            References(x => x.ResponsibleExploitation, "Responsible_Operation_ID");
            References(x => x.ResponsiblePurchase, "Responsible_Zakupki_ID");
            References(x => x.ResponsibleInformation, "Responsible_OU_ID");

            Map(x => x.LocationOrgName, "OU_Location_Name");
            Map(x => x.LocationFioResponsible, "OU_Location_FIO");
            Map(x => x.LocationOrgAddress, "OU_Location_Address");
            Map(x => x.LocationOrgPhone, "OU_Location_Phone");

            HasMany(x => x.TechnicalSupports).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();
            HasMany(x => x.Softwares).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();
            HasMany(x => x.WorksAndServices).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();
            HasMany(x => x.WorkAndGoods).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();

            HasMany(x => x.GovPurchases).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();

            HasMany(x => x.Characteristics).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();

            Map(x => x.LevelOfSecurity, "OU_Security_Level").CustomType<LevelOfSecurity>();

            HasMany(x => x.SpecialChecks).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();

            HasMany(x => x.InternalIsAndComponentsItki).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();
            HasMany(x => x.RelationInternalIsAndComponentsItki).KeyColumns.Add("OU_Object_ID").Cascade.Delete().Inverse();
            HasMany(x => x.ExternalIsAndComponentsItki).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();
            HasMany(x => x.ExternalUserInterfacesOfIs).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();

            Map(x => x.InformAboutActivityGovOrgan, "OU_Is_OGV_Direction");

            References(x => x.Parent, "OU_Parent_ID");

            HasMany(x => x.Favorites).KeyColumns.Add("OU_ID").Cascade.AllDeleteOrphan().Inverse();

            HasMany(x => x.AoActivities).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();
            
            //HasMany(x => x.Activities).KeyColumns.Add("OU_ID").Cascade.Delete();            

            HasMany(x => x.UserComments).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();

            HasMany(x => x.AOGovServices).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();

            HasMany(x => x.GovContracts).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();
        }
    }
}