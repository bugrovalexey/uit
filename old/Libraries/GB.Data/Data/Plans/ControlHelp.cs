﻿namespace Uviri.MKRF.Entities.Plans
{
    public class ControlHelp: EntityBase
    {
        public virtual string PageName { get; set; }
        public virtual string ControlId { get; set; }
        public virtual string Help { get; set; }
        public virtual string Example { get; set; }
    }
}
