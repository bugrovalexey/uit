﻿using System;

namespace GB.MKRF.Web.Services.Fap
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName = "Uviri.Coord.Web.Services.Fap.FapServiceSoap")]
    public interface FapServiceSoap
    {

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/ExistFapUser", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        bool ExistFapUser(string userLogin);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetSoftware", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        System.Data.DataTable GetSoftware(SoftwareFilter filter);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetSoftwareData", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        SoftwareInfoData GetSoftwareData(int softId, string userLogin);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetSyncronizeData", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        System.Data.DataTable GetSyncronizeData(System.DateTime date);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetDictionary", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        System.Data.DataTable GetDictionary(DictionaryEnum dic);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/Import", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        void Import();

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/UpdateSoftwareRating", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        void UpdateSoftwareRating(int entityId, string userLogin, int val);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetContractList", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        System.Data.DataTable GetContractList(int softId);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetVideoListForSolution", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        System.Data.DataTable GetVideoListForSolution(int ownerId);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetDocsListForSolution", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        System.Data.DataTable GetDocsListForSolution(int ownerId);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetScreenshotsForSolution", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        string GetScreenshotsForSolution(int ownerId);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetUsersCount", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        int GetUsersCount(int softId);

        // CODEGEN: Параметр "dateFrom" требует дополнительной информации о схеме, которую невозможно получить в режиме параметров. Указан атрибут "System.Xml.Serialization.XmlElementAttribute".
        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetOffersList", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        GetOffersListResponse GetOffersList(GetOffersListRequest request);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetFindedOffersList", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        System.Data.DataTable GetFindedOffersList(string search);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetOffer", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        OfferData GetOffer(int id);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetOfferJson", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        string GetOfferJson(int id);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/SaveOffer", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        void SaveOffer(OfferData offer, string userLogin);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/MakeSoftwareOnOffer", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        string MakeSoftwareOnOffer(int offerId, string userLogin);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/MakeSoftwareOnPassport", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        string MakeSoftwareOnPassport(int passId, string userLogin);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetOfferStatuses", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        StatusData[] GetOfferStatuses();

        // CODEGEN: Параметр "dateFrom" требует дополнительной информации о схеме, которую невозможно получить в режиме параметров. Указан атрибут "System.Xml.Serialization.XmlElementAttribute".
        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetOrdersList", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        GetOrdersListResponse GetOrdersList(GetOrdersListRequest request);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetFindedOrdersList", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        System.Data.DataTable GetFindedOrdersList(string search);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetOrder", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        OrderData GetOrder(int id);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetOrderJson", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        string GetOrderJson(int id);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/SaveOrder", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        void SaveOrder(OrderData order, string userLogin);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetOrderStatuses", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        StatusData[] GetOrderStatuses();

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetOGVUsingSolution", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        System.Data.DataTable GetOGVUsingSolution(int solutionId);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/GetAllSolution", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(OfferOrderDataBase))]
        System.Data.DataTable GetAllSolution();
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "3.0.4506.2152")]
    [Serializable()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class SoftwareFilter
    {

        private string nameField;

        private string nameAnnotationField;

        private string fgisNumberField;

        private string annotationField;

        private string developerField;

        private string departmentField;

        private string kindField;

        private string softUsesField;

        private string softTypesField;

        private string hardwareField;

        private string osField;

        private string dbField;

        private bool isDuplicatedField;

        private bool saasPossibilityField;

        private bool isBestPracticeField;

        private bool isPublicField;

        private bool isBestPracticeSignificantField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string NameAnnotation
        {
            get
            {
                return this.nameAnnotationField;
            }
            set
            {
                this.nameAnnotationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string FgisNumber
        {
            get
            {
                return this.fgisNumberField;
            }
            set
            {
                this.fgisNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Annotation
        {
            get
            {
                return this.annotationField;
            }
            set
            {
                this.annotationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Developer
        {
            get
            {
                return this.developerField;
            }
            set
            {
                this.developerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Department
        {
            get
            {
                return this.departmentField;
            }
            set
            {
                this.departmentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Kind
        {
            get
            {
                return this.kindField;
            }
            set
            {
                this.kindField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string SoftUses
        {
            get
            {
                return this.softUsesField;
            }
            set
            {
                this.softUsesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string SoftTypes
        {
            get
            {
                return this.softTypesField;
            }
            set
            {
                this.softTypesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Hardware
        {
            get
            {
                return this.hardwareField;
            }
            set
            {
                this.hardwareField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string OS
        {
            get
            {
                return this.osField;
            }
            set
            {
                this.osField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string DB
        {
            get
            {
                return this.dbField;
            }
            set
            {
                this.dbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public bool IsDuplicated
        {
            get
            {
                return this.isDuplicatedField;
            }
            set
            {
                this.isDuplicatedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public bool SaasPossibility
        {
            get
            {
                return this.saasPossibilityField;
            }
            set
            {
                this.saasPossibilityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public bool IsBestPractice
        {
            get
            {
                return this.isBestPracticeField;
            }
            set
            {
                this.isBestPracticeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public bool IsPublic
        {
            get
            {
                return this.isPublicField;
            }
            set
            {
                this.isPublicField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public bool IsBestPracticeSignificant
        {
            get
            {
                return this.isBestPracticeSignificantField;
            }
            set
            {
                this.isBestPracticeSignificantField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "3.0.4506.2152")]
    [Serializable()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class StatusData
    {

        private int idField;

        private string nameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OrderData))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OfferData))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "3.0.4506.2152")]
    [Serializable()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class OfferOrderDataBase
    {

        private int idField;

        private string nameField;

        private string authorInfoField;

        private int departmentIdField;

        private string creationDateField;

        private string statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string AuthorInfo
        {
            get
            {
                return this.authorInfoField;
            }
            set
            {
                this.authorInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public int DepartmentId
        {
            get
            {
                return this.departmentIdField;
            }
            set
            {
                this.departmentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string CreationDate
        {
            get
            {
                return this.creationDateField;
            }
            set
            {
                this.creationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "3.0.4506.2152")]
    [Serializable()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class OrderData : OfferOrderDataBase
    {

        private string pUsersField;

        private string tasksField;

        private string expectedPeriodField;

        private string commentsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string PUsers
        {
            get
            {
                return this.pUsersField;
            }
            set
            {
                this.pUsersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Tasks
        {
            get
            {
                return this.tasksField;
            }
            set
            {
                this.tasksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string ExpectedPeriod
        {
            get
            {
                return this.expectedPeriodField;
            }
            set
            {
                this.expectedPeriodField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "3.0.4506.2152")]
    [Serializable()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class OfferData : OfferOrderDataBase
    {

        private string descriptionField;

        private string functionsField;

        private string linkField;

        private string pUsersField;

        private string contactsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Functions
        {
            get
            {
                return this.functionsField;
            }
            set
            {
                this.functionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Link
        {
            get
            {
                return this.linkField;
            }
            set
            {
                this.linkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string PUsers
        {
            get
            {
                return this.pUsersField;
            }
            set
            {
                this.pUsersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Contacts
        {
            get
            {
                return this.contactsField;
            }
            set
            {
                this.contactsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "3.0.4506.2152")]
    [Serializable()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class SoftwareInfoData
    {

        private string departmentField;

        private string nameField;

        private string kindField;

        private string typeField;

        private string useField;

        private string annotationField;

        private string functionsField;

        private string hardField;

        private string osField;

        private string dbField;

        private string cotactsInfoField;

        private string fGISNumberField;

        private bool isPublicField;

        private bool isSAASField;

        private bool isDuplicatedField;

        private decimal avgRatingField;

        private int votesCountField;

        private System.Nullable<decimal> currentUserRatingField;

        private string developersField;

        private string additionalSoftField;

        private string usersField;

        private string plannedWorksField;

        private string actualResultsField;

        private string saasClientTypeField;

        private string saasDescriptionField;

        private string saasCharacteristicField;

        private string saasLicenseField;

        private string saasInfrastructureField;

        private bool isSaasClientWebBrowserField;

        private string browsersField;

        private string browsersExtentionsField;

        private string webServersField;

        private string[] linksField;

        private string ratingField;

        private string publicityField;

        private string sAASField;

        private string duplicatedField;

        private string contractsField;

        private bool mostSignificantSolutionField;

        private bool isUserField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string Department
        {
            get
            {
                return this.departmentField;
            }
            set
            {
                this.departmentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Kind
        {
            get
            {
                return this.kindField;
            }
            set
            {
                this.kindField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Use
        {
            get
            {
                return this.useField;
            }
            set
            {
                this.useField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Annotation
        {
            get
            {
                return this.annotationField;
            }
            set
            {
                this.annotationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Functions
        {
            get
            {
                return this.functionsField;
            }
            set
            {
                this.functionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Hard
        {
            get
            {
                return this.hardField;
            }
            set
            {
                this.hardField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string OS
        {
            get
            {
                return this.osField;
            }
            set
            {
                this.osField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string DB
        {
            get
            {
                return this.dbField;
            }
            set
            {
                this.dbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string CotactsInfo
        {
            get
            {
                return this.cotactsInfoField;
            }
            set
            {
                this.cotactsInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string FGISNumber
        {
            get
            {
                return this.fGISNumberField;
            }
            set
            {
                this.fGISNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public bool IsPublic
        {
            get
            {
                return this.isPublicField;
            }
            set
            {
                this.isPublicField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public bool IsSAAS
        {
            get
            {
                return this.isSAASField;
            }
            set
            {
                this.isSAASField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public bool IsDuplicated
        {
            get
            {
                return this.isDuplicatedField;
            }
            set
            {
                this.isDuplicatedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public decimal AvgRating
        {
            get
            {
                return this.avgRatingField;
            }
            set
            {
                this.avgRatingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public int VotesCount
        {
            get
            {
                return this.votesCountField;
            }
            set
            {
                this.votesCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 17)]
        public System.Nullable<decimal> CurrentUserRating
        {
            get
            {
                return this.currentUserRatingField;
            }
            set
            {
                this.currentUserRatingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string Developers
        {
            get
            {
                return this.developersField;
            }
            set
            {
                this.developersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string AdditionalSoft
        {
            get
            {
                return this.additionalSoftField;
            }
            set
            {
                this.additionalSoftField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string Users
        {
            get
            {
                return this.usersField;
            }
            set
            {
                this.usersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string PlannedWorks
        {
            get
            {
                return this.plannedWorksField;
            }
            set
            {
                this.plannedWorksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string ActualResults
        {
            get
            {
                return this.actualResultsField;
            }
            set
            {
                this.actualResultsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string SaasClientType
        {
            get
            {
                return this.saasClientTypeField;
            }
            set
            {
                this.saasClientTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string SaasDescription
        {
            get
            {
                return this.saasDescriptionField;
            }
            set
            {
                this.saasDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string SaasCharacteristic
        {
            get
            {
                return this.saasCharacteristicField;
            }
            set
            {
                this.saasCharacteristicField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 26)]
        public string SaasLicense
        {
            get
            {
                return this.saasLicenseField;
            }
            set
            {
                this.saasLicenseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 27)]
        public string SaasInfrastructure
        {
            get
            {
                return this.saasInfrastructureField;
            }
            set
            {
                this.saasInfrastructureField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 28)]
        public bool IsSaasClientWebBrowser
        {
            get
            {
                return this.isSaasClientWebBrowserField;
            }
            set
            {
                this.isSaasClientWebBrowserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 29)]
        public string Browsers
        {
            get
            {
                return this.browsersField;
            }
            set
            {
                this.browsersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 30)]
        public string BrowsersExtentions
        {
            get
            {
                return this.browsersExtentionsField;
            }
            set
            {
                this.browsersExtentionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 31)]
        public string WebServers
        {
            get
            {
                return this.webServersField;
            }
            set
            {
                this.webServersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 32)]
        public string[] Links
        {
            get
            {
                return this.linksField;
            }
            set
            {
                this.linksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 33)]
        public string Rating
        {
            get
            {
                return this.ratingField;
            }
            set
            {
                this.ratingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 34)]
        public string Publicity
        {
            get
            {
                return this.publicityField;
            }
            set
            {
                this.publicityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 35)]
        public string SAAS
        {
            get
            {
                return this.sAASField;
            }
            set
            {
                this.sAASField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 36)]
        public string Duplicated
        {
            get
            {
                return this.duplicatedField;
            }
            set
            {
                this.duplicatedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 37)]
        public string Contracts
        {
            get
            {
                return this.contractsField;
            }
            set
            {
                this.contractsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 38)]
        public bool MostSignificantSolution
        {
            get
            {
                return this.mostSignificantSolutionField;
            }
            set
            {
                this.mostSignificantSolutionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 39)]
        public bool IsUser
        {
            get
            {
                return this.isUserField;
            }
            set
            {
                this.isUserField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "3.0.4506.2152")]
    [Serializable()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public enum DictionaryEnum
    {

        /// <remarks/>
        SoftType,

        /// <remarks/>
        SoftUse,

        /// <remarks/>
        SoftKind,
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "GetOffersList", WrapperNamespace = "http://tempuri.org/", IsWrapped = true)]
    public partial class GetOffersListRequest
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 0)]
        public string name;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 1)]
        public string author;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 2)]
        public string status;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 3)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public System.Nullable<System.DateTime> dateFrom;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 4)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public System.Nullable<System.DateTime> dateTo;

        public GetOffersListRequest()
        {
        }

        public GetOffersListRequest(string name, string author, string status, System.Nullable<System.DateTime> dateFrom, System.Nullable<System.DateTime> dateTo)
        {
            this.name = name;
            this.author = author;
            this.status = status;
            this.dateFrom = dateFrom;
            this.dateTo = dateTo;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "GetOffersListResponse", WrapperNamespace = "http://tempuri.org/", IsWrapped = true)]
    public partial class GetOffersListResponse
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 0)]
        public System.Data.DataTable GetOffersListResult;

        public GetOffersListResponse()
        {
        }

        public GetOffersListResponse(System.Data.DataTable GetOffersListResult)
        {
            this.GetOffersListResult = GetOffersListResult;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "GetOrdersList", WrapperNamespace = "http://tempuri.org/", IsWrapped = true)]
    public partial class GetOrdersListRequest
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 0)]
        public string name;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 1)]
        public string author;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 2)]
        public string status;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 3)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public System.Nullable<System.DateTime> dateFrom;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 4)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public System.Nullable<System.DateTime> dateTo;

        public GetOrdersListRequest()
        {
        }

        public GetOrdersListRequest(string name, string author, string status, System.Nullable<System.DateTime> dateFrom, System.Nullable<System.DateTime> dateTo)
        {
            this.name = name;
            this.author = author;
            this.status = status;
            this.dateFrom = dateFrom;
            this.dateTo = dateTo;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "GetOrdersListResponse", WrapperNamespace = "http://tempuri.org/", IsWrapped = true)]
    public partial class GetOrdersListResponse
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://tempuri.org/", Order = 0)]
        public System.Data.DataTable GetOrdersListResult;

        public GetOrdersListResponse()
        {
        }

        public GetOrdersListResponse(System.Data.DataTable GetOrdersListResult)
        {
            this.GetOrdersListResult = GetOrdersListResult;
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    public interface FapServiceSoapChannel : FapServiceSoap, System.ServiceModel.IClientChannel
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    public partial class FapServiceSoapClient : System.ServiceModel.ClientBase<FapServiceSoap>, FapServiceSoap
    {

        public FapServiceSoapClient()
        {
        }

        public FapServiceSoapClient(string endpointConfigurationName) :
            base(endpointConfigurationName)
        {
        }

        public FapServiceSoapClient(string endpointConfigurationName, string remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public FapServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public FapServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
            base(binding, remoteAddress)
        {
        }

        public bool ExistFapUser(string userLogin)
        {
            return base.Channel.ExistFapUser(userLogin);
        }

        public System.Data.DataTable GetSoftware(SoftwareFilter filter)
        {
            return base.Channel.GetSoftware(filter);
        }

        public SoftwareInfoData GetSoftwareData(int softId, string userLogin)
        {
            return base.Channel.GetSoftwareData(softId, userLogin);
        }

        public System.Data.DataTable GetSyncronizeData(System.DateTime date)
        {
            return base.Channel.GetSyncronizeData(date);
        }

        public System.Data.DataTable GetDictionary(DictionaryEnum dic)
        {
            return base.Channel.GetDictionary(dic);
        }

        public void Import()
        {
            base.Channel.Import();
        }

        public void UpdateSoftwareRating(int entityId, string userLogin, int val)
        {
            base.Channel.UpdateSoftwareRating(entityId, userLogin, val);
        }

        public System.Data.DataTable GetContractList(int softId)
        {
            return base.Channel.GetContractList(softId);
        }

        public System.Data.DataTable GetVideoListForSolution(int ownerId)
        {
            return base.Channel.GetVideoListForSolution(ownerId);
        }

        public System.Data.DataTable GetDocsListForSolution(int ownerId)
        {
            return base.Channel.GetDocsListForSolution(ownerId);
        }

        public string GetScreenshotsForSolution(int ownerId)
        {
            return base.Channel.GetScreenshotsForSolution(ownerId);
        }

        public int GetUsersCount(int softId)
        {
            return base.Channel.GetUsersCount(softId);
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        GetOffersListResponse FapServiceSoap.GetOffersList(GetOffersListRequest request)
        {
            return base.Channel.GetOffersList(request);
        }

        public System.Data.DataTable GetOffersList(string name, string author, string status, System.Nullable<System.DateTime> dateFrom, System.Nullable<System.DateTime> dateTo)
        {
            GetOffersListRequest inValue = new GetOffersListRequest();
            inValue.name = name;
            inValue.author = author;
            inValue.status = status;
            inValue.dateFrom = dateFrom;
            inValue.dateTo = dateTo;
            GetOffersListResponse retVal = ((FapServiceSoap)(this)).GetOffersList(inValue);
            return retVal.GetOffersListResult;
        }

        public System.Data.DataTable GetFindedOffersList(string search)
        {
            return base.Channel.GetFindedOffersList(search);
        }

        public OfferData GetOffer(int id)
        {
            return base.Channel.GetOffer(id);
        }

        public string GetOfferJson(int id)
        {
            return base.Channel.GetOfferJson(id);
        }

        public void SaveOffer(OfferData offer, string userLogin)
        {
            base.Channel.SaveOffer(offer, userLogin);
        }

        public string MakeSoftwareOnOffer(int offerId, string userLogin)
        {
            return base.Channel.MakeSoftwareOnOffer(offerId, userLogin);
        }

        public string MakeSoftwareOnPassport(int passId, string userLogin)
        {
            return base.Channel.MakeSoftwareOnPassport(passId, userLogin);
        }

        public StatusData[] GetOfferStatuses()
        {
            return base.Channel.GetOfferStatuses();
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        GetOrdersListResponse FapServiceSoap.GetOrdersList(GetOrdersListRequest request)
        {
            return base.Channel.GetOrdersList(request);
        }

        public System.Data.DataTable GetOrdersList(string name, string author, string status, System.Nullable<System.DateTime> dateFrom, System.Nullable<System.DateTime> dateTo)
        {
            GetOrdersListRequest inValue = new GetOrdersListRequest();
            inValue.name = name;
            inValue.author = author;
            inValue.status = status;
            inValue.dateFrom = dateFrom;
            inValue.dateTo = dateTo;
            GetOrdersListResponse retVal = ((FapServiceSoap)(this)).GetOrdersList(inValue);
            return retVal.GetOrdersListResult;
        }

        public System.Data.DataTable GetFindedOrdersList(string search)
        {
            return base.Channel.GetFindedOrdersList(search);
        }

        public OrderData GetOrder(int id)
        {
            return base.Channel.GetOrder(id);
        }

        public string GetOrderJson(int id)
        {
            return base.Channel.GetOrderJson(id);
        }

        public void SaveOrder(OrderData order, string userLogin)
        {
            base.Channel.SaveOrder(order, userLogin);
        }

        public StatusData[] GetOrderStatuses()
        {
            return base.Channel.GetOrderStatuses();
        }

        public System.Data.DataTable GetOGVUsingSolution(int solutionId)
        {
            return base.Channel.GetOGVUsingSolution(solutionId);
        }

        public System.Data.DataTable GetAllSolution()
        {
            return base.Channel.GetAllSolution();
        }
    }
}
