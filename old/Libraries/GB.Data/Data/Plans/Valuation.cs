﻿namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Оценка документа по информатизации
    /// </summary>
    public class Valuation: EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }
    }
}
