﻿namespace bgTeam.IsRui.Domain.Entities.Directories
{
    /// <summary>Год</summary>
    public class Years : EntityBase
    {
        /// <summary>Год</summary>
        public virtual int Value { get; set; }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}