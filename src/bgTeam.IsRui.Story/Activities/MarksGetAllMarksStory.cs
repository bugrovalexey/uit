﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class MarksGetAllMarksStory : IStory<MarksGetAllMarksStoryContext, IEnumerable<ActivitysMarksDto>>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IMapperBase _mapper;

        public MarksGetAllMarksStory(IRepositoryRui repositoryRui, IMapperBase mapperBase)
        {
            _repositoryRui = repositoryRui;
            _mapper = mapperBase;
        }

        public IEnumerable<ActivitysMarksDto> Execute(MarksGetAllMarksStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivitysMarksDto>> ExecuteAsync(MarksGetAllMarksStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var marks = _repositoryRui.GetAllEx<PlansActivityMark>(
               q => q.Where(x => x.PlansActivity.Id == context.ActivityId))
               .Select(x => _mapper.Map(x, new ActivitysMarksDto()));

            return marks;
        }
    }
}
