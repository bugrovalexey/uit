﻿using bgTeam.IsRui.Domain.Entities.Common;
using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    public class RoleViewModel : ViewModelBase
    {
        public IList<TableRoles> Items { get; private set; }

        public RoleViewModel()
        {
            Items = new RepositoryDc<Role>().GetAll().Select(x => new TableRoles(x)).ToList();
        }
    }
}