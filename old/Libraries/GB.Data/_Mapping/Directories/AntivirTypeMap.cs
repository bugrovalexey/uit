﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class AntivirTypeMap : ClassMapBase<AntivirType>
	{
        public AntivirTypeMap()
            : base("Antivir_Type")
        {
            //Table("w_");

            //Id(x => x.Id, "Antivir_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Antivir_Type_Name");
        }
    }
}