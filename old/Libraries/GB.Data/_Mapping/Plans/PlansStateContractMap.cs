﻿using GB.Entity.Directories.Zakupki;
using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Mapping.Plans
{
    class PlansStateContractMap : ClassMapBase<PlansStateContract>
    {
        public PlansStateContractMap()
            : base("Plans_State_Contract")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.DescriptionPreviousResult, "Plans_State_Contract_Results_Discr");
            Map(x => x.Year, "Plans_State_Contract_Year");
            Map(x => x.ImplementationIndicatorY0, "Plans_State_Contract_Implementation_Indicator_Y0");
            Map(x => x.ImplementationIndicatorY1, "Plans_State_Contract_Implementation_Indicator_Y1");
            Map(x => x.ImplementationIndicatorY2, "Plans_State_Contract_Implementation_Indicator_Y2");

            References<Contract>(x => x.Contract, "Contract_ID");
            References<PlansActivity>(x => x.PlanActivity, "Plans_Activity_ID").Not.Nullable();
            //References<PlansActGT>(x => x.PlansActGT, "Plans_Act_GT_ID");
            References<PlansGoalIndicator>(x => x.PlansGoalIndicator, "Plans_Goal_Indicator_ID");
        }
    }
}