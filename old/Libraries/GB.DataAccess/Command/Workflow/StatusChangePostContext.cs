﻿using GB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.Workflow
{
    public class StatusChangePostContext : ICommandContext
    {
        public IWorkflowObject Obj { get; set; }
    }
}
