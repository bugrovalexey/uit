﻿namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Documents;

    internal class AODecisionToCreateMap : ClassMapHiLo<AODecisionToCreate>
    {
        public AODecisionToCreateMap()
            : base("OU_Purchase_Decision")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Purchase_Decision_Name").Not.Nullable();
            Map(x => x.Number, "OU_Purchase_Decision_Number");
            Map(x => x.DateAdoption, "OU_Purchase_Decision_Date");
            Map(x => x.ParticleOfDocument, "OU_Purchase_Document_Paragraph");
            Map(x => x.Url, "OU_Purchase_Decision_URL");

            References(x => x.AccountingObject, "OU_ID").Not.Nullable();

            HasMany(x => x.Documents).KeyColumns.Add("EntityOwnerId")
                .Where("EntityType = " + (int)EntityType.DocumentAccountingObject)
                .Where("Type = " + (int)DocTypeEnum.AccountingObjectCommentDocument)
                .Cascade.Delete().Inverse();
        }
    }
}