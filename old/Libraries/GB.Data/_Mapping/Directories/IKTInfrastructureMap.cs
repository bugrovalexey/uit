﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
	public class IKTInfrastructureMap : ClassMapBase<IKTInfrastructure>
    {
        public IKTInfrastructureMap()
            : base("IKT_IS")
        {
            //Table("w_");

            //Id(x => x.Id, "IKT_IS_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Type, "IKT_IS_Type").CustomType(typeof(IKTInfrastructureTypeEnum)).Not.Nullable();
            Map(x => x.Name, "IKT_IS_Name");
            Map(x => x.FGIS, "IKT_IS_FGIS");
            Map(x => x.IsActive, "IKT_IS_Is_Active");
            Map(x => x.ExternalKey, "IKT_IS_External_Key");
			Map(x => x.Status, "IKT_IS_Status");
			Map(x => x.DepartmentCode, "IKT_IS_GO_Code");
			Map(x => x.Purpose, "IKT_IS_Purpose");
            Map(x => x.Goal, "IKT_IS_Goal");
			Map(x => x.Scope, "IKT_IS_Scope");
			Map(x => x.Func, "IKT_IS_Func");
			Map(x => x.RegDate, "IKT_IS_RegDate").Nullable();

            References<IKTComponent>(x => x.IKTComponent, "IKT_Component_ID");
            References<FgisInfo>(x => x.FgisInfo, "FGIS_Info_ID");
        }
    }
}