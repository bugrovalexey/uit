﻿using DevExpress.Web.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Controls;
using GB.Extentions;
using GB.Controls.Extensions;
using GB;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using GB.Repository;

namespace GB.MKRF.Web.Activities
{
    public partial class Services : System.Web.UI.Page
    {
        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as EditCard).CurrentActivity;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            var col = Grid.Columns["yearColumn"] as GridViewDataComboBoxColumn;
            col.PropertiesComboBox.DataSource = repository.GetYearList();

            Grid.DataBind();

            base.OnPreRender(e);
        }

        private void FillServiceArgs(PlansActivityServiceArgs args, IDictionary<string, object> values)
        {
            args.Name = Convert.ToString(values["Name"]);
            args.Year = (YearEnum)Convert.ToInt32(values["Year"]);
            args.Count = Convert.ToInt32(values["Count"]);
            args.Cost = Convert.ToDouble(values["Cost"]);
            args.KOSGU = Convert.ToInt32(values["KOSGU"]);
        }

        #region ReasonGrid

        PlansActivityServiceController controller = new PlansActivityServiceController();
        PlansActivityServiceRepository repository = new PlansActivityServiceRepository();
        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            (sender as ASPxGridView).DataSource = controller.Repository.GetAll(x => x.Activity == CurrentActivity);
        }

        protected void Grid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void Grid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            var args = new PlansActivityServiceArgs { Activity = CurrentActivity };
            FillServiceArgs(args, hf);
            controller.Create(args);

            Grid.GridCancelEdit(e);
        }

        protected void Grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            var args = new PlansActivityServiceArgs { Id = Convert.ToInt32(e.Keys[0]) };
            FillServiceArgs(args, hf);
            controller.Update(args);

            Grid.GridCancelEdit(e);
        }

        protected void Grid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            controller.Delete(new PlansActivityServiceArgs { Id = Convert.ToInt32(e.Keys[0]) });

            Grid.GridCancelEdit(e);
        }

        #endregion

        protected void NameTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(controller.CurrentEdit.With(x => x.Name));
        }

        protected void YearComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(repository.GetYearList())
                .SetValueComboBox<int>(controller.CurrentEdit.Return(x => (int)x.Year, 1));
        }

        protected void CountTextBox_DataBinding(object sender, EventArgs e)
        {
            controller.CurrentEdit.With(x => x.Count).Do(sender.SetValueTextBoxVal<int>);
        }

        protected void CostTextBox_DataBinding(object sender, EventArgs e)
        {
            controller.CurrentEdit.With(x => x.Cost).DoVal(sender.SetValueTextBoxVal<double>);
        }

        protected void KOSGUComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(DirectoriesRepositary.GetDirectories<ExpenditureItem>(DateTime.Now.Year).OrderBy(x => x.Code))
                .SetValueComboBox<int>(controller.CurrentEdit.Return(x => x.KOSGU.Id, EntityBase.Default));
        }
    }
}