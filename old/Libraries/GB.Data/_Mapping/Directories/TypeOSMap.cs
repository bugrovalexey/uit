﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class TypeOSMap : ClassMapBase<OSType>
    {
        public TypeOSMap()
            : base("OS_Type")
        {
            //Table("w_");

            //Id(x => x.Id, "OS_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OS_Type_Name");
            Map(x => x.NameSmall, "OS_Type_SName");
            Map(x => x.Version, "OS_Type_Ver");
            Map(x => x.Developer, "OS_Type_Dev");
        }
    }
}