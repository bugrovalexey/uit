﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;

    public class ClaimDocumentDto
    {
        public int Id { get; set; }

        public int ClaimId { get; set; }

        public FilesDto File { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public string ParticleOfDocument { get; set; }

        public DateTime? DateAdoption { get; set; }
    }
}
