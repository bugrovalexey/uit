﻿using Uviri.MKRF.Entities.Prognosis;
using Uviri.MKRF.Entities.Prognosis.SectionIII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionIII
{
    public class SharedSystemMap : ClassMapBase<SharedSystem>
	{
		public SharedSystemMap()
            : base("MF3_16")
        {
//            Table("w_");

//            Id(x => x.Id, "MF3_16_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_MF3_16_Insert
//                          @MF3_16_Num = ?
//                         ,@MF3_16_1 = ?
//                         ,@MF3_16_2 = ?
//                         ,@MF3_16_3 = ?
//                         ,@MF3_16_4 = ?
//                         ,@MF3_16_5 = ?
//						 ,@MF3_16_6 = ?
//						 ,@Mon_Form_ID = ?
//                         ,@MF3_16_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF3_16_Update
//                          @MF3_16_Num = ?
//                         ,@MF3_16_1 = ?
//                         ,@MF3_16_2 = ?
//                         ,@MF3_16_3 = ?
//                         ,@MF3_16_4 = ?
//                         ,@MF3_16_5 = ?
//						 ,@MF3_16_6 = ?
//						 ,@Mon_Form_ID = ?
//                         ,@MF3_16_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF3_16_Delete
//						  @MF3_16_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "MF3_16_Num");
			Map(x => x.FOIVFunc, "MF3_16_1");
			Map(x => x.AccessOrder, "MF3_16_2");
            Map(x => x.GiveDepartment, "MF3_16_3");
            Map(x => x.GetDepartment, "MF3_16_4");
			Map(x => x.ISName, "MF3_16_5");
			Map(x => x.AccessMethodPeriod, "MF3_16_6");

			References<Prognos>(x => x.Prognos, "Mon_Form_ID").Not.Nullable();
            //References<Department>(x => x.GiveDepartment, "Govt_Organ_ID_16_3").Not.Nullable();
            //References<Department>(x => x.GetDepartment, "Govt_Organ_ID_16_4").Not.Nullable();
        }
    }
}