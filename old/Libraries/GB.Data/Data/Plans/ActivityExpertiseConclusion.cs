﻿using GB.Data.Workflow;
using System.Collections.Generic;

namespace GB.Data.Plans
{
    /// <summary>
    /// Экспертное заключение по мероприятию
    /// </summary>
    public class ActivityExpertiseConclusion : EntityBase, IWorkflowObject
    {
        public ActivityExpertiseConclusion() { }

        internal ActivityExpertiseConclusion(int id)
        {
            // TODO: Complete member initialization
            this.Id = id;
        }

        public virtual Status Status { get; set; }

        /// <summary>Эксперты</summary>
        public virtual IList<ActivityExpert> Experts { get; set; }

        
    }
}