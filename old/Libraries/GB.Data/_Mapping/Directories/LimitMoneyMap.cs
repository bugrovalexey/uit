﻿using GB.Data.Common;
using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class LimitMoneyMap : ClassMapHiLo<LimitMoney>
    {
        public LimitMoneyMap()
            : base("Lim_242")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Money, "Lim_242_Money");

            References<Department>(x => x.Department, "Govt_Organ_ID");
            References<Years>(x => x.Year, "Year");
        }
    }
}