﻿namespace bgTeam.IsRui.Story.Account
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UpdateInfoStoryContext
    {
        /// <summary>Имя</summary>
        public string Name { get; set; }

        /// <summary>Фамилия</summary>
        public string Surname { get; set; }

        /// <summary>Отчество</summary>
        public string Middlename { get; set; }

        /// <summary>Должность</summary>
        public string Job { get; set; }

        /// <summary>Рабочий телефон</summary>
        public string WorkPhone { get; set; }

        /// <summary>Мобильный телефон</summary>
        public string MobPhone { get; set; }

        /// <summary>Факс</summary>
        public string Fax { get; set; }

    }
}
