﻿using System;
using GB.Helpers;
using GB.MKRF.Entities.Admin;
using GB.Repository;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Web.Helpers;

namespace GB.MKRF.Web.Selectors
{
    public partial class Notifications : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            SelectorsNotificationsGrid.DataBind();

            base.OnPreRender(e);
        }

        protected void SelectorsNotificationsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            (sender as ASPxGridView).DataSource = EntityHelper.ConvertToAnonymous<Notification>(RepositoryBase<Notification>.GetAll(),
                 delegate(Notification n)
                 {
                     return new
                     {
                         Id = n.Id,
                         Name = n.Name,
                         Subject = n.Subject,
                         Message = n.Message,
                     };
                 });
        }

        protected void SelectorsNotificationsGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "$(this).addClass(\"grid_mouseover\")");
            e.Row.Attributes.Add("onmouseout", "$(this).removeClass(\"grid_mouseover\")");
        }
    }
}