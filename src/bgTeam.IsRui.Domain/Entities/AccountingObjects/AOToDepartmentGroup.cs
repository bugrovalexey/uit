﻿using bgTeam.IsRui.Domain.Entities.Common;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    public class AOToDepartmentGroup : EntityBase
    {
        public virtual DepartmentGroup DepartmentGroup { get; set; }

        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Орган государственной власти (ведомство)
        /// </summary>
        public virtual Department Department { get; set; }

        /// <summary>
        /// Год
        /// </summary>
        public virtual DepartmentGroupYearEnum Year { get; set; }
    }
}
