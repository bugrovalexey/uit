﻿namespace bgTeam.IsRui.Domain.Entities.Directories
{
    /// <summary>
    /// Классификационная категория видов обеспечения
    /// </summary>
    public class CategoryKindOfSupport : EntityDirectories
    {
        /// <summary>
        /// Вид обеспечения
        /// </summary>
        public virtual CategoryTypeOfSupportEnum Type { get; set; }
    }
}