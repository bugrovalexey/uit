﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ExpCriterionMap : ClassMapBase<ExpCriterion>
	{
        public ExpCriterionMap()
            : base("Exp_Criterion")
		{
//            Table("w_");

//            Id(x => x.Id, "Exp_Criterion_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_Exp_Criterion_Insert
//						  @Exp_Criterion_Num = ?
//						 ,@Exp_Criterion_Name = ?
//						 ,@Exp_Obj_Type_ID = ?
//                         ,@Exp_Criterion_ID = ?").Check.None();


//            SqlUpdate(@"exec ZZZ_Prc_Exp_Criterion_Update
//						  @Exp_Criterion_Num = ?
//						 ,@Exp_Criterion_Name = ?
//						 ,@Exp_Obj_Type_ID = ?
//                         ,@Exp_Criterion_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Criterion_Delete
//						  @Exp_Criterion_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Num, "Exp_Criterion_num").Nullable();
            Map(x => x.Char, "Exp_Criterion_Char").Nullable();
			Map(x => x.Name, "Exp_Criterion_Name").Nullable();
			Map(x => x.ExpObjectType, "Exp_Obj_Type_ID").CustomType(typeof(ExpObjectTypeEnum)).Not.Nullable();

            HasMany<ExpReason>(x => x.Reasons)
                .KeyColumns.Add("Exp_Criterion_ID")
                .Inverse()
                .Cascade.AllDeleteOrphan();
        }
    }
}
