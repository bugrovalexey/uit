﻿using System.ComponentModel;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class GovernmentContractEditViewModel
    {
        public int Id { get; set; }

        [DisplayName("Реестровый номер")]
        public string Number { get; set; }

        [DisplayName("Закупка")]
        public string Order { get; set; }

        [DisplayName("Статус госконтракта")]
        public string Status { get; set; }

        [DisplayName("Исполнитель")]
        public string Executer { get; set; }
        
        [DisplayName("Дата заключения")]
        public string Date { get; set; }

        [DisplayName("Стоимость")]
        public string Price { get; set; }

        [DisplayName("Причины отклонения от ожидаемых результатов")]
        public string RcDeviation { get; set; }


        public GovernmentContractEditViewModel(int id)
        {
            var gc = new bgTeam.IsRui.DataAccess.Repository.RepositoryDc<GovContract>().Get(id);

            Id = gc.Id;
            Number = gc.Number;
            Order = gc.Order;
            Status = gc.Status.GetDescription();
            Executer = gc.Executer;
            Date = gc.Date.Value.ToString("dd.MM.yyyy");
            Price = gc.Price.ToString("N2");
            RcDeviation = gc.ReasonsCostDeviation;
        }
    }
}