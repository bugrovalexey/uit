﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.Decommissioning
{
    public class SaveReasonDecommissioningContext : ICommandContext
    {
        public int xId { get; set; }

        public string Reason { get; set; }
    }
}
