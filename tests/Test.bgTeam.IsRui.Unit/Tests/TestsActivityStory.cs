﻿using bgTeam.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Dto;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Documents;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Story.AccountingObjects;
using bgTeam.IsRui.Story.Activities;
using bgTeam.IsRui.Story.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Test.bgTeam.IsRui.Unit.Common;
using Xunit;

namespace Test.bgTeam.IsRui.Unit.Tests
{
    public class TestsStory
    {
        private Factory _fac;

        public TestsStory()
        {
            _fac = new Factory();
        }

        [Fact]
        public void Story_GetListActivitiesStory()
        {
            var story = new GetListActivitiesStory(_fac.repositoryRui, _fac.storyMapper, _fac.userRepository);

            var res = story.Execute(new GetListActivitiesStoryContext());

            Assert.NotEmpty(res);
        }

        [Fact]
        public void Story_CreateActivityStory()
        {
            var story = new CreateActivityStory(_fac.storyMapper, _fac.sessionFactory, _fac.userRepository, _fac.repositoryRui, _fac.statusRepository);

            var res = story.Execute(new CreateActivityStoryContext()
            {
                Name = "Test Activity" + new Random().Next(999999),
                TypeId = 1,
                AoId = 46
            });

            Assert.NotNull(res);
            Assert.NotNull(res.Name);
            Assert.NotNull(res.StatusName);
            Assert.NotNull(res.TypeName);
        }

        [Fact]
        public void Story_DeleteActivityStory()
        {
            var storyCerate = new CreateActivityStory(_fac.storyMapper, _fac.sessionFactory, _fac.userRepository, _fac.repositoryRui, _fac.statusRepository);

            var activity = storyCerate.Execute(new CreateActivityStoryContext()
            {
                Name = "Test Activity",
                TypeId = 1
            });

            var story = new DeleteActivityStory(_fac.repositoryRui, _fac.sessionFactory);

            var res = story.Execute(new DeleteActivityStoryContext()
            {
                Id = activity.Id
            });

            Assert.True(res);
        }

        [Theory]
        [InlineData(48)]
        [InlineData(252)]
        public void Story_ActivityGetObjectInfo(int id)
        {
            var actStory = new ActivityGetActsStory(
                _fac.repositoryRui,
                _fac.storyMapper);

            var story = new GetObjectInfoStory(
                _fac.repositoryRui,
                _fac.userRepository,
                _fac.storyMapper,
                actStory);

            var result = story.Execute(new GetObjectInfoStoryContext() { Id = id });

            Assert.NotNull(result.Id);
            Assert.NotEmpty(result.Purchases);
            Assert.NotEmpty(result.Acts);
        }

        [Fact]
        public void Story_CostsGetCosts()
        {
            var story = new CostsGetCostsStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new CostsGetCostsStoryContext() { ActivityId = 30 });

            Assert.NotNull(result.ActivityId);
            Assert.NotNull(result.ForecastQuarter1);
            Assert.NotNull(result.ForecastQuarter2);
            Assert.NotNull(result.ForecastQuarter3);
            Assert.NotNull(result.ForecastQuarter4);
            Assert.NotNull(result.PlanQuarter1);
            Assert.NotNull(result.PlanQuarter2);
            Assert.NotNull(result.PlanQuarter3);
            Assert.NotNull(result.PlanQuarter4);
            Assert.NotNull(result.BudgetItemName);
        }

        [Fact]
        public void Story_CostsUpdateCosts()
        {
            var story = new CostsUpdateCostsStory(_fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var result = story.Execute(new CostsUpdateCostsStoryContext()
            {
                ActivityId = 30,
                BudgetItemId = 1,
                ForecastQuarter1 = 1M,
                ForecastQuarter2 = 2M,
                ForecastQuarter3 = 3M,
                ForecastQuarter4 = 4M,
                PlanQuarter1 = 5M,
                PlanQuarter2 = 6M,
                PlanQuarter3 = 7M,
                PlanQuarter4 = 8M
            });

            Assert.NotNull(result.ActivityId);
            Assert.Equal(1M, result.ForecastQuarter1);
            Assert.Equal(2M, result.ForecastQuarter2);
            Assert.Equal(3M, result.ForecastQuarter3);
            Assert.Equal(4M, result.ForecastQuarter4);
            Assert.Equal(5M, result.PlanQuarter1);
            Assert.Equal(6M, result.PlanQuarter2);
            Assert.Equal(7M, result.PlanQuarter3);
            Assert.Equal(8M, result.PlanQuarter4);
        }

        [Fact]
        public void Story_DocumentsGetAllDocuments()
        {
            var story = new DocumentsGetAllDocumentsStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new DocumentsGetAllDocumentsStoryContext() { ActivityId = 48 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_DocumentsCreateOrUpdateDocument()
        {
            var story = new DocumentsCreateOrUpdateDocumentStory(_fac.storyMapper, _fac.repositoryRui, _fac.sessionFactory, _fac.fileProviderRui);

            var result = story.Execute(new DocumentsCreateOrUpdateDocumentStoryContext()
            {
                ActivityId = 30,
                Name = "Name",
                Number = "123",
                DateAdoption = DateTime.Now,
                Particleofdocument = "asdasd",
                file = new CreateDocumentFileData()
                {
                    FileName = "File Name",
                    Kind = DocKindEnum.GoogleDrive,
                    Size = 100,
                    Url = "https://drive.google.com/file/d/1w-jVBm0KNBQrQMb-QH_3VOGp5QagneUV/preview?usp=drive_web",
                    MimeType = "image/png"
                }
            });

            Assert.NotNull(result.Id);

            var resultUpdate = story.Execute(new DocumentsCreateOrUpdateDocumentStoryContext()
            {
                Id = result.Id,
                ActivityId = 30,
                Name = "Name123",
                Number = "123321",
                DateAdoption = DateTime.Now,
                Particleofdocument = "asdasd123321",
                file = new CreateDocumentFileData()
                {
                    Id = result.file.Id,
                }
            });

            Assert.NotNull(resultUpdate.Id);
            Assert.NotNull(resultUpdate.file.Id);
        }

        [Fact]
        public void Story_DocumentsDeleteDocument()
        {
            var story = new DocumentsCreateOrUpdateDocumentStory(_fac.storyMapper, _fac.repositoryRui, _fac.sessionFactory, _fac.fileProviderRui);

            var result = story.Execute(new DocumentsCreateOrUpdateDocumentStoryContext()
            {
                ActivityId = 30,
                Name = "Name",
                Number = "123",
                DateAdoption = DateTime.Now,
                Particleofdocument = "asdasd",
                file = new CreateDocumentFileData()
                {
                    FileName = "File Name",
                    Kind = DocKindEnum.GoogleDrive,
                    Size = 100,
                    Url = "https://drive.google.com/file/d/1w-jVBm0KNBQrQMb-QH_3VOGp5QagneUV/preview?usp=drive_web",
                    MimeType = "image/png"
                }
            });

            Assert.NotNull(result.Id);

            var storyDel = new DocumentsDeleteDocumentStory(_fac.repositoryRui, _fac.sessionFactory);

            var resDel = storyDel.Execute(new DocumentsDeleteDocumentStoryContext() { Id = result.Id });

            Assert.True(resDel);
        }

        [Fact]
        public void Story_ServicesGetAllServicesStory()
        {
            var story = new ServicesGetAllServicesStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new ServicesGetAllServicesStoryContext() { ActivityId = 30 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_ServicesCreateOrUpdateService()
        {
            var story = new ServicesCreateOrUpdateServiceStory(_fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var result = story.Execute(new ServicesCreateOrUpdateServiceStoryContext()
            {
                ActivityId = 30,
                Name = "Name",
                KOSGUId = 1,
                Year = YearEnum.First

            });

            Assert.NotNull(result.Id);

            var resultUpdate = story.Execute(new ServicesCreateOrUpdateServiceStoryContext()
            {
                Id = result.Id,
                ActivityId = 30,
                Name = "NameNew",
                KOSGUId = 2,
                Year = YearEnum.Next
            });

            Assert.NotNull(resultUpdate.Id);
            Assert.Equal(2, resultUpdate.KOSGUId);
            Assert.Equal(YearEnum.Next, resultUpdate.Year);

            
        }

        [Fact]
        public void Story_ServicesDeleteService()
        {
            var story = new ServicesCreateOrUpdateServiceStory(_fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var result = story.Execute(new ServicesCreateOrUpdateServiceStoryContext()
            {
                ActivityId = 30,
                Name = "Name",
                KOSGUId = 1,
                Year = YearEnum.First

            });

            Assert.NotNull(result.Id);

            var storyDel = new ServicesDeleteServiceStory(_fac.sessionFactory);

            var resultDel = storyDel.Execute(new ServicesDeleteServiceStoryContext() { Id = result.Id });

            Assert.True(resultDel);
        }

        [Fact]
        public void Story_MarksGetAllMarks()
        {
            var story = new MarksGetAllMarksStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new MarksGetAllMarksStoryContext() { ActivityId = 30 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_MarksCreateOrUpdateMark()
        {
            var story = new MarksCreateOrUpdateMarkStory(_fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var result = story.Execute(new MarksCreateOrUpdateMarkStoryContext()
            {
                ActivityId = 30,
                Name = "Name",
                UnitId = 1,
                DateToAchieveGoalValue = DateTime.Now,
                AlgorithmOfCalculating = "AlgorithmO"

            });

            Assert.NotNull(result.Id);

            var resultUpdate = story.Execute(new MarksCreateOrUpdateMarkStoryContext()
            {
                Id = result.Id,
                ActivityId = 30,
                Name = "NameNew",
                UnitId = 2,
                DateToAchieveGoalValue = DateTime.Now,
            });

            Assert.NotNull(resultUpdate.Id);
        }

        [Fact]
        public void Story_MarksDeleteMarks()
        {
            var story = new MarksCreateOrUpdateMarkStory(_fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var result = story.Execute(new MarksCreateOrUpdateMarkStoryContext()
            {
                ActivityId = 30,
                Name = "Name",
                UnitId = 1,
                DateToAchieveGoalValue = DateTime.Now,

            });

            Assert.NotNull(result.Id);

            var storyDel = new MarksDeleteMarksStory(_fac.sessionFactory);

            var resultDel = storyDel.Execute(new MarksDeleteMarksStoryContext() { Id = result.Id });

            Assert.True(resultDel);
        }

        [Fact]
        public void Story_IndicatorsGetAllIndicators()
        {
            var story = new IndicatorsGetAllIndicatorsStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new IndicatorsGetAllIndicatorsStoryContext() { ActivityId = 30 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_IndicatorsCreateOrUpdateIndicator()
        {
            var story = new IndicatorsCreateOrUpdateIndicatorStory(_fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var result = story.Execute(new IndicatorsCreateOrUpdateIndicatorStoryContext()
            {
                ActivityId = 30,
                Name = "Name",
                UnitId = 1,
                DateToAchieveGoalValue = DateTime.Now,

            });

            Assert.NotNull(result.Id);

            var resultUpdate = story.Execute(new IndicatorsCreateOrUpdateIndicatorStoryContext()
            {
                Id = result.Id,
                ActivityId = 30,
                Name = "NameNew",
                UnitId = 2,
                DateToAchieveGoalValue = DateTime.Now,
            });

            Assert.NotNull(resultUpdate.Id);
        }

        [Fact]
        public void Story_IndicatorsDeleteIndicator()
        {
            var story = new IndicatorsCreateOrUpdateIndicatorStory(_fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var result = story.Execute(new IndicatorsCreateOrUpdateIndicatorStoryContext()
            {
                ActivityId = 30,
                Name = "Name",
                UnitId = 1,
                DateToAchieveGoalValue = DateTime.Now,

            });

            Assert.NotNull(result.Id);

            Assert.NotNull(result.Id);

            var storyDel = new IndicatorsDeleteIndicatorStory(_fac.sessionFactory);

            var resultDel = storyDel.Execute(new IndicatorsDeleteIndicatorStoryContext() { Id = result.Id });

            Assert.True(resultDel);
        }

        [Fact]
        public void Story_WorksGetAllWork()
        {
            var story = new WorksGetAllWorkStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new WorksGetAllWorkStoryContext() { ActivityId = 30 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_WorksCreateOrUpdateWork()
        {
            var story = new WorksCreateOrUpdateWorkStory(_fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var result = story.Execute(new WorksCreateOrUpdateWorkStoryContext()
            {
                ActivityId = 30,
                Name = "Name",
                Duration = 1000,
                Summ = 10000000,
                Quarter = QuarterEnum.Quarter1
            });

            Assert.NotNull(result.Id);

            var resultUpdate = story.Execute(new WorksCreateOrUpdateWorkStoryContext()
            {
                Id = result.Id,
                ActivityId = 30,
                Name = "NameNew",
                Duration = 1000,
                Summ = 10000000,
                Quarter = QuarterEnum.Quarter1
            });

            Assert.NotNull(resultUpdate.Id);
        }

        [Fact]
        public void Story_WorksDeleteWork()
        {
            var story = new WorksCreateOrUpdateWorkStory(_fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var result = story.Execute(new WorksCreateOrUpdateWorkStoryContext()
            {
                ActivityId = 30,
                Name = "Name"

            });

            Assert.NotNull(result.Id);

            var storyDel = new WorksDeleteWorkStory(_fac.sessionFactory);

            var resultDel = storyDel.Execute(new WorksDeleteWorkStoryContext() { Id = result.Id });

            Assert.True(resultDel);
        }

        [Fact]
        public void Story_CommonInfoGetInfo()
        {
            var story = new CommonInfoGetInfoStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new CommonInfoGetInfoStoryContext() { ActivityId = 30 });

            Assert.NotNull(result.ActivityId);
        }

        [Fact]
        public void Story_CommonInfoUpdateInfo()
        {
            var story = new CommonInfoUpdateInfoStory(_fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var result = story.Execute(new CommonInfoUpdateInfoStoryContext()
            {
                ActivityId = 30,
                Description = "Descr",
                Name = "Name",
                ResponsibleDepartmentId = 1,
                SigningId = 1
            });

            Assert.NotNull(result.ActivityId);
            Assert.NotNull(result.Description);
            Assert.NotNull(result.Name);

        }

        [Fact]
        public void Story_GoodsGetAllGoods()
        {
            var story = new GoodsGetAllGoodsStory( _fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new GoodsGetAllGoodsStoryContext() { ActivityId = 30 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_GoodsCreateOrUpdateGoods()
        {
            var story = new GoodsCreateOrUpdateGoodsStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var charactList = new List<GoodsCharacteristic>();

            var result = story.Execute(new GoodsCreateOrUpdateGoodsStoryContext()
            {
                ActivityId = 30,
                Name = "Name",
                AnalogCost = 123M,
                AnalogName = "Analog",
                ChangeDate = DateTime.Now,
                Cost = 132,
                Quantity = 11,
                Quarter = QuarterEnum.Quarter3,
                Summ = 500M
            });

            Assert.NotNull(result.Id);

            var charact = new GoodsCharacteristic()
            {
                Id = 6,
            };

            charactList.Add(charact);

            var resultUpdate = story.Execute(new GoodsCreateOrUpdateGoodsStoryContext()
            {
                Id = result.Id,
                ActivityId = 30,
                Name = "Name123",
                AnalogCost = 123M,
                AnalogName = "Analog",
                ChangeDate = DateTime.Now,
                Cost = 132,
                Quantity = 11,
                Quarter = QuarterEnum.Quarter3,
                Summ = 500M,
                Characteristic = charactList
            });

            Assert.NotNull(resultUpdate.Id);
            Assert.NotEmpty(resultUpdate.Characteristic);

        }

        [Fact]
        public void Story_GoodsDeleteGoods()
        {
            var story = new GoodsCreateOrUpdateGoodsStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new GoodsCreateOrUpdateGoodsStoryContext()
            {
                ActivityId = 30,
                Name = "Name"
            });

            var storyDelete = new GoodsDeleteGoodsStory(_fac.sessionFactory);

            var resultDelete = storyDelete.Execute(new GoodsDeleteGoodsStoryContext() { Id = result.Id.GetValueOrDefault() });

            Assert.True(resultDelete);
        }

        [Fact]
        public void Story_GoodsAddOrUpdateGoodsCharacteristic()
        {
            var story = new GoodsAddOrUpdateGoodsCharacteristicStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new GoodsAddOrUpdateGoodsCharacteristicStoryContext() { GoodsId = 30, Name = "Name", UnitId = 1, Value = 1 });

            Assert.NotNull(result.Id);
        }

        [Fact]
        public void Story_GoodsDeleteGoodsCharacteristic()
        {
            var storyCreate = new GoodsAddOrUpdateGoodsCharacteristicStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var resultCreate = storyCreate.Execute(new GoodsAddOrUpdateGoodsCharacteristicStoryContext() { GoodsId = 30, Name = "Name", UnitId = 1 });

            var storyDelete = new GoodsDeleteGoodsCharacteristicStory(_fac.sessionFactory);

            var resultDelete = storyDelete.Execute(new GoodsDeleteGoodsCharacteristicStoryContext() { Id = resultCreate.Id.GetValueOrDefault() });

            Assert.True(resultDelete);
        }

        [Fact]
        public void Story_WorkflowConclusionSaveOrUpdateStory()
        {
            var story = new WorkflowUserConclusionSaveOrUpdateStory(_fac.storyMapper, _fac.repositoryRui, _fac.userRepository, _fac.sessionFactory, _fac.statusRepository);
            var res = story.Execute(new WorkflowUserConclusionSaveOrUpdateStoryContext
            {
                ActivityId = 48,
                Text = "Test workflow null",
            });

            Assert.NotNull(res);
            Assert.NotEqual(0, res.Id);
            Assert.NotNull(res.Text);

            var res2 = story.Execute(new WorkflowUserConclusionSaveOrUpdateStoryContext
            {
                Id = res.Id,
                Text = "Test id",
            });
        }

        [Fact]
        public void Story_GetConclusionHistory()
        {
            var story = new GetConclusionHistoryStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new GetConclusionHistoryStoryContext() { Id = 186 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_ConclusionAddFile()
        {

            var story = new ConclusionAddFileStory(_fac.storyMapper, _fac.repositoryRui, _fac.fileProviderRui, _fac.sessionFactory, _fac.statusRepository, _fac.userRepository);

            var result = story.Execute(new ConclusionAddFileStoryContext()
            {
                ActivityId = 48,
                ConclusionId = 310,
                FileName = "File Name",
                Kind = DocKindEnum.GoogleDrive,
                Size = 100,
                Url = "https://drive.google.com/file/d/1w-jVBm0KNBQrQMb-QH_3VOGp5QagneUV/preview?usp=drive_web",
                MimeType = "image/png"
            });
        }

        [Fact(Skip = "")]
        public void Story_ConclusionDeleteFile()
        {
            var story = new ConclusionAddFileStory(_fac.storyMapper, _fac.repositoryRui, _fac.fileProviderRui, _fac.sessionFactory, _fac.statusRepository, _fac.userRepository);

            var result = story.Execute(new ConclusionAddFileStoryContext()
            {
                ActivityId = 48,
                ConclusionId = 310,
                FileName = "File Name",
                //TempGuid = new Guid("ef4c3ab4-58a2-44fa-8f46-3ba38d3e2002")
                Kind = DocKindEnum.GoogleDrive,
                Size = 100,
                Url = "https://drive.google.com/file/d/1w-jVBm0KNBQrQMb-QH_3VOGp5QagneUV/preview?usp=drive_web",
                MimeType = "image/png",
            });

            var storyDel = new ConclusionDeleteFileStory(_fac.sessionFactory);

            var resDel = storyDel.Execute(new ConclusionDeleteFileStoryContext()
            {
                Id = result.Id
            });

            Assert.True(resDel);

        }

        [Fact]
        public void Story_GetGovtOrganHierarchy()
        {
            var story = new GetGovtOrganHierarchyStory(_fac.storyMapper, _fac.userRepository, _fac.repositoryRui);

            var result = story.Execute(new GetGovtOrganHierarchyStoryContext() { Id = 1 });

            Assert.NotEmpty(result);
        }

        [Theory]
        [InlineData(220, 188, null)]
        [InlineData(220, null, "External Sys Name")]
        [InlineData(184, null, "KITVLG-246")]
        public void Story_SetInformationInteractionStory(int activity, int? aoid, string exName)
        {
            var story = new SetInformationInteractionStory(
                _fac.storyMapper,
                _fac.sessionFactory,
                _fac.repositoryRui);

            var context = new SetInformationInteractionStoryContext
            {
                ActivityId = activity,
                AoId = aoid,
                ExternalSystemName = exName
            };

            var result = story.Execute(context);

            Assert.NotNull(result);
        }

        [Theory]
        [InlineData(220)]
        [InlineData(184)]
        [InlineData(250)]
        public void Story_GetInformationInteractionStory(int id)
        {
            var story = new GetInformationInteractionStory(
                _fac.repositoryRui,
                _fac.storyMapper);

            var context = new GetInformationInteractionStoryContext
            {
                ActivityId = id
            };

            var result = story.Execute(context);

            Assert.NotNull(result);
        }

        [Fact]
        public void Story_ActivityCreateOrUpdateActStory_Create()
        {
            var story = new ActivityCreateOrUpdateActStory(
                _fac.repositoryRui,
                _fac.sessionFactory,
                _fac.storyMapper,
                new FileProviderRuiMock());

            var context = new ActivityCreateOrUpdateActStoryContext
            {
                ActivityId = 220,
                Cost = 1000,
                Name = "Activity act name",
                Number = "Activity act number",
                File = new ActsFileDto
                {
                    FileName = "ActFile.png",
                    TempGuid = new Guid("40fd14ac-c7e2-418e-8a85-163897d62aa6")
                },
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ActivityCreateOrUpdateActStory_Update()
        {
            var story = new ActivityCreateOrUpdateActStory(
                _fac.repositoryRui,
                _fac.sessionFactory,
                _fac.storyMapper,
                new FileProviderRuiMock());

            var context = new ActivityCreateOrUpdateActStoryContext
            {
                Id = 4,
                ActivityId = 220,
                Cost = 1233,
                Name = "Activity act name $!",
                Number = "Activity act number #234",
                File = null
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ActivityGetActsStory()
        {
            var story = new ActivityGetActsStory(
                _fac.repositoryRui,
                _fac.storyMapper);

            var context = new ActivityGetActsStoryContext
            {
                ActivityId = 220
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.NotEmpty(res);
        }

        [Fact]
        public void Story_ActivityAddOrUpdateContractStory_Create()
        {
            var story = new ActivityAddOrUpdateContractStory(
                _fac.repositoryRui,
                _fac.storyMapper,
                _fac.sessionFactory);

            var context = new ActivityAddOrUpdateContractStoryContext
            {
                ActivityId = 244,
                Name = "Contract name",
                Number = "#1",
                BeginDate = DateTime.Now,
                ChangeDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1)
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ActivityAddOrUpdateContractStory_Update()
        {
            var story = new ActivityAddOrUpdateContractStory(
                _fac.repositoryRui,
                _fac.storyMapper,
                _fac.sessionFactory);

            var context = new ActivityAddOrUpdateContractStoryContext
            {
                Id = 2,
                ActivityId = 244,
                Name = "Contract name",
                Number = "#2",
                BeginDate = DateTime.Now,
                ChangeDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1)
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ActivityGetContractsStory()
        {
            var story = new ActivityGetContractsStory(
                _fac.repositoryRui,
                _fac.storyMapper);

            var context = new ActivityGetContractsStoryContext
            {
                ActivityId = 244
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.NotEmpty(res);
        }

        [Fact]
        public void Story_ActivityAddOrUpdatePurchaseStory()
        {
            var story = new ActivityAddOrUpdatePurchaseStory(
                _fac.repositoryRui,
                _fac.storyMapper,
                _fac.sessionFactory);

            var context = new ActivityAddOrUpdatePurchaseStoryContext
            {
                Id = null,
                AcceptanceEndDate = DateTime.Now,
                ActivityId = 244,
                Month = Month.July,
                Number = 123,
                PublishDate = DateTime.Now,
                ResultDate = DateTime.Now
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ActivityGetPurchasesStory()
        {
            var story = new ActivityGetPurchasesStory(
                _fac.repositoryRui,
                _fac.storyMapper);

            var context = new ActivityGetPurchasesStoryContext
            {
                ActivityId = 244
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.NotEmpty(res);
        }
    }
}