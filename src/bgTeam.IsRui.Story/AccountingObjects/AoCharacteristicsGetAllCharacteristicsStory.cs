﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;

    public class AoCharacteristicsGetAllCharacteristicsStory : IStory<AoCharacteristicsGetAllCharacteristicsStoryContext, IEnumerable<AoAllCharacteristicsDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public AoCharacteristicsGetAllCharacteristicsStory(IRepositoryRui repository, IMapperBase mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<AoAllCharacteristicsDto> Execute(AoCharacteristicsGetAllCharacteristicsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<AoAllCharacteristicsDto>> ExecuteAsync(AoCharacteristicsGetAllCharacteristicsStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            var list = _repository.GetAllEx<AccountingObjectCharacteristics>(q =>
                  {
                      q.Where(x => x.AccountingObject.Id == context.AoId);
                  });

            return _mapper.Map(list, new List<AoAllCharacteristicsDto>());
        }
    }
}
