﻿using GB.Data.Common;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Избранные ОУ
    /// </summary>
    public class AOFavorite : EntityBase
    {
        /// <summary>
        /// ОУ
        /// </summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public virtual User User { get; set; }
    }
}
