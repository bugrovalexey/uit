﻿namespace bgTeam.IsRui.WebApp.ViewModels.Account
{
    public class LoginViewModel
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public string InvalidMessage { get; set; }
    }
}