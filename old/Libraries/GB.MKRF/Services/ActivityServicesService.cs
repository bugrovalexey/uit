﻿using GB.Data.Directories;
using GB.Data.Plans;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    public class ActivityServicesService : ServiceBase<PlansActivityService>, IService<PlansActivityService, PlansActivityServiceArgs>
    {
        public PlansActivityService Create(PlansActivityServiceArgs args)
        {
            var pas = new PlansActivityService(args.Activity ?? GetEntity<PlansActivity>(args.ActivityId));

            return SaveOrUpdate(pas, args);
        }

        public PlansActivityService Update(PlansActivityServiceArgs args)
        {
            return SaveOrUpdate(Repository.GetExisting(args.Id), args);
        }

        public void Delete(PlansActivityServiceArgs args)
        {
            base.Delete(args.Id);
        }

        private PlansActivityService SaveOrUpdate(PlansActivityService pas, PlansActivityServiceArgs args)
        {
            pas.Name = args.Name as string;
            pas.Year = args.Year;
            pas.Count = args.Count;
            pas.Cost = args.Cost;
            pas.KOSGU = GetEntity<ExpenditureItem>(args.KOSGU);

            base.SaveOrUpdate(pas);

            return pas;
        }
    }

    public class PlansActivityServiceArgs : BaseArgsNew
    {
        public string Name { get; set; }

        public YearEnum Year { get; set; }

        public int? Count { get; set; }

        public double Cost { get; set; }

        public int KOSGU { get; set; }

        public PlansActivity Activity { get; set; }

        public int ActivityId { get; set; }
    }
}