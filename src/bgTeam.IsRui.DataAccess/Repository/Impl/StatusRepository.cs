﻿namespace bgTeam.IsRui.DataAccess.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.Workflow;
    using bgTeam.IsRui.Domain.Extensions;

    public interface IStatusRepository : IRepositoryDc<Status>
    {
        Status GetDefault(IWorkflowObject obj);

        Status GetDefault<T>(T obj) where T : IEntityWorkflow;

        IList<StatusList> GetNext(params IWorkflowObject[] objects);

        IList<Status> GetStatuses(EntityType entityType);
    }

    public class StatusRepository : RepositoryDc<Status>, IStatusRepository
    {
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;

        public StatusRepository(
            IRepositoryRui repository,
            IUserRepository userRepository,
            ISessionNHibernateFactory factory) 
            : base(factory)
        {
            _repository = repository;
            _userRepository = userRepository;
        }

        public Status GetDefault<T>(T obj) where T : IEntityWorkflow
        {
            if (obj is Domain.Entities.Plans.PlansActivity)
            {
                return Get(StatusEnum.AoDraft);
            }
            else if (obj is Domain.Entities.Claims.Claim)
            {
                return Get(StatusEnum.ClaimCreate);
            }

            return Get(StatusEnum.None);
        }

        public Status GetDefault(IWorkflowObject obj)
        {
            switch (obj.GetTableType())
            {
                case EntityType.PlansActivity:
                    return Get(StatusEnum.ActCreated);

                case EntityType.ActivityExpertise:
                    return Get(StatusEnum.ExpAppointment);

                case EntityType.AccountingObject:
                    return Get(StatusEnum.AoDraft);
            }

            throw new Exception("Пришел неизвестный объект для получения статуса. " + obj.GetType().Name);
        }

        /// <summary>
        /// Поличить список доступных статусов для указанных объектов
        /// </summary>
        public IList<StatusList> GetNext(params IWorkflowObject[] objects)
        {
            if (objects == null)
                throw new ArgumentNullException("objects");

            var notNullObjects = objects.Where(x => x != null);
            if (!notNullObjects.Any())
                throw new ArgumentException("Не задано ни одного объекта для перевода статуса", "objects");

            var statuses = notNullObjects.Select(o => o.Status).ToArray();
            var roles = _userRepository.GetCurrent().Roles.ToArray();
            return _repository.GetAllEx<StatusList>(q => q
                .WhereRestrictionOn(x => x.From).IsIn(statuses)
                .WhereRestrictionOn(x => x.Role).IsIn(roles)
                .JoinQueryOver(x => x.To));
            //.Where(x => x.From == obj.Status)
            //.Where(x => x.Role == UserRepository.GetCurrent().Role)
            //        .Select(n => n.To);
        }

        /// <summary>
        /// Возвращает список статусов для указанного типа сущности (по группе статусов)
        /// </summary>
        public IList<Status> GetStatuses(EntityType entityType)
        {
            StatusGroupEnum group;
            switch (entityType)
            {
                case EntityType.PlansActivity:
                    group = StatusGroupEnum.PlansActivity;
                    break;

                case EntityType.AccountingObject:
                    group = StatusGroupEnum.AccountingObject;
                    break;

                case EntityType.ActivityExpertise:
                    group = StatusGroupEnum.ActivityExpertise;
                    break;

                default:
                    group = (StatusGroupEnum)0;
                    break;
            }
            return GetAllEx(q => q.Where(st => st.StatusGroup.Id == (int)group));
        }

        /*
        private void AddHistory<T>(T obj, string comment, Status oldStatus, Status newStatus) where T : IWorkflowObject
        {
            StatusHistory h = new StatusHistory();

            h.User = UserRepository.GetCurrent();
            h.From = oldStatus;
            h.In = newStatus;
            h.Date = DateTime.Now;
            h.Comment = comment;
            h.EntityId = obj.Id;
            h.EntityType = obj.GetTableType();

            ////Применяется только для версии объекта фонда
            ////Нужно подумать как это лучше переделать
            //if (demand.HasValue)
            //{
            //    h.File = FilesRepository.Get(demand.Value);
            //}
            //else
            //{
            //    h.File = Workflow.Workflow.GetDemandForStatusChange(newStatus.Id, oldStatus, entity.Id, comment);
            //}

            //RepositoryBase<StatusSchemeHistory>.SaveOrUpdate(h);
        }
        */
    }
}