﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807200035)]
    public class Migration_201807200035 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("ActivityPurchase")
                .WithColumn("ActivityPurchase_ID").AsInt32().PrimaryKey()
                .WithColumn("Plans_Activity_ID").AsInt32().NotNullable()
                .WithColumn("AP_AcceptanceEndDate").AsDateTime().Nullable()
                .WithColumn("AP_Month").AsString(32).Nullable()
                .WithColumn("AP_Number").AsInt32().Nullable()
                .WithColumn("AP_PublishDate").AsDateTime().Nullable()
                .WithColumn("AP_ResultDate").AsDateTime().Nullable()
                .WithColumn("AP_TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "ActivityPurchase" });
        }
    }
}
