﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="HistoryActionDetails.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.HistoryActionDetails" %>
<%@ Import Namespace="GB.MKRF.Web.Plans.v2012new" %>
<%@ Register Src="~/Common/GridColumnManager.ascx" TagName="GridColumnManager" TagPrefix="gcm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">

<style type="text/css">
    .historyTableCaption
    {
        color: #333333;
        margin: 30px 00px 00px 00px;
        font: 1em Arial,sans-serif !important;
        font-weight: bold  !important;
    }
    .transparentGrid
    {
        background-color: transparent !important;
    }
    .dxgvResponsibleRow_MetropolisBlue
    {
        height: 20px !important;
    }
</style>

<script type="text/javascript">
    var currentDate = <%=date1.Ticks%>;
    function showActionComparison(d2) {
        if (currentDate && d2 && currentDate == d2)
            return;
        document.location.href = 'HistoryActionDetails.aspx?Activity=<%=CurrentActivityId%>&t=' + currentDate + '&t2=' + d2;
    }
</script>
 <div class="page" style="position: relative">
        <table style="width:100%; position: absolute; top: -38px;">
            <tr>
                <td style="width: 50%" align="left">
                    <div class="arrow_back">
                        <a href="<%=getActivityListUrl()%>">Вернуться к <%=date2.HasValue? "сравнению" : "списку" %> мероприятий</a>
                    </div>
                </td>
                <td style="width: 50%" align="right">
                    <%=GetLegend() %>
                </td>
            </tr>
        </table>
        <table class="form_edit">
            <tr>
                <td>
                    <table style="margin: 5px; font-size: 1.2em" cellspacing="3px">
                        <tr>
                            <td style="white-space: nowrap; text-align: right">История мероприятия:
                            </td>
                            <td style="font-weight: bold">
                                <%=getActivityInfo("Plans_Activity_Code")%>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">плана:
                            </td>
                            <td style="font-weight: bold">
                                <%=getActivityInfo("Govt_Organ_Name") + ", " + getActivityInfo("Year")+ "г."%>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">от:
                            </td>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="border-collapse: collapse">
                                    <tr>
                                        <td style="font-weight: bold"><%=date1.ToString() %></td>
                                        <td style="padding-left: 20px; white-space: nowrap; padding-right: 5px">сравнить с версией от:</td>
                                        <td>
                                            <dxe:ASPxComboBox runat="server" ID="cb" ClientInstanceName="cb" ValueField="Id" TextField="Name" ValueType="System.String" OnDataBinding="cb_DataBinding">
                                                <ClientSideEvents SelectedIndexChanged="function(s,e){  showActionComparison(s.GetSelectedItem().value.toString() ); }" />
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
<br />
<div class="page">
    <dxtc:ASPxPageControl runat="server" ID="tc" ActiveTabIndex="0" Width="100%"
            EnableViewState="false">
            <ContentStyle BackColor="Transparent" BorderLeft-BorderStyle="None" BorderBottom-BorderStyle="None" Border-BorderStyle="None" />
            <TabStyle Width="150px" Height="30px" HorizontalAlign="Center" VerticalAlign="Middle">

            </TabStyle>
            <TabPages>
                <dxtc:TabPage Text="Общие сведения" Name="Common">
                    <ContentCollection>
                        <dxw:ContentControl>
                            <table>
                                <tr>
                                    <td style="vertical-align: top">
                                        <dxwgv:ASPxGridView runat="server" ID="gridResponsible" AutoGenerateColumns="False" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                                            OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" CssClass="transparentGrid">
                                            <Border BorderStyle="None" />
                                            <SettingsPager Mode="ShowAllRecords" Visible="False" />
                                            <Settings GridLines="None" ShowGroupPanel="false" ShowColumnHeaders="False" />
                                            <Styles Table-BackColor="Transparent">
                                                <SelectedRow CssClass="dxgvResponsibleRow_MetropolisBlue"></SelectedRow>
                                                <Row CssClass="dxgvResponsibleRow_MetropolisBlue"></Row>
                                            </Styles>
                                            
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn FieldName="Key" CellStyle-HorizontalAlign="Right" Width="450">
                                                    <CellStyle HorizontalAlign="Right" Font-Bold="true" Wrap="True"></CellStyle>
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Value" Width="450">
                                                    <CellStyle HorizontalAlign="Left" Wrap="True"></CellStyle>
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Order" SortIndex="0" SortOrder="Ascending"
                                                    Visible="false">
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                        </dxwgv:ASPxGridView>
                                    </td>
                                    <td style="vertical-align: top">
                                        <table>
                                            <tr>
                                                <td style="width: 150px"></td>
                                                <td style="padding-left: 20px; padding-bottom: 10px; color: black">
                                                    <b>Ответственный за мероприятие</b>
                                                </td>
                                            </tr>
                                        </table>
                                        <dxwgv:ASPxGridView runat="server" ID="gridResponsible2" AutoGenerateColumns="False" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                                            OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" CssClass="transparentGrid">
                                            <Border BorderStyle="None" />
                                            <Styles Table-BackColor="Transparent">
                                                <SelectedRow CssClass="dxgvResponsibleRow_MetropolisBlue"></SelectedRow>
                                                <Row CssClass="dxgvResponsibleRow_MetropolisBlue"></Row>
                                            </Styles>
                                            <SettingsPager Mode="ShowAllRecords" Visible="False" />
                                            <Settings GridLines="None" ShowGroupPanel="false" ShowColumnHeaders="False" />
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn FieldName="Key" CellStyle-HorizontalAlign="Right" Width="150">
                                                    <CellStyle HorizontalAlign="Right" Font-Bold="true" Wrap="True"></CellStyle>
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Value" Width="450">
                                                    <CellStyle HorizontalAlign="Left" Wrap="True"></CellStyle>
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Order" SortIndex="0" SortOrder="Ascending"
                                                    Visible="false">
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                        </dxwgv:ASPxGridView>

                                    </td>
                                </tr>
                            </table>
                            <table class="form_edit">
                                <tr>
                                    <td>
                                        <div class="historyTableCaption">
                                            <p>
                                                Наименование и реквизиты основания реализации мероприятия и (или) решения о создании
                    системы с указанием пунктов, статей (федеральный закон, акт Президента Российской
                    Федерации, Правительства Российской Федерации)
                                            </p>
                                        </div>
                                        <dxwgv:ASPxGridView runat="server" ID="gridReason" KeyFieldName="Id" Width="100%"
                                            AutoGenerateColumns="False" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                                            OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" Settings-GroupFormat="{1}" SettingsBehavior-AutoExpandAllGroups="true">
                                            <Styles>
                                                <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                </Header>
                                            </Styles>
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn VisibleIndex="0" FieldName="Caption" Caption=" " Name="Caption" Visible="false" PropertiesTextEdit-EncodeHtml="false">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn  FieldName="Name" VisibleIndex="2" Caption="Наименование">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Rekvisit" VisibleIndex="3" Caption="Реквизиты документа">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Item" VisibleIndex="4" Caption="Пункт, статья документа">
                                                </dxwgv:GridViewDataTextColumn>
                                                <%--<dxwgv:GridViewDataTextColumn VisibleIndex="5" ShowInCustomizationForm="False">
                                                    <Settings AllowDragDrop="False" />
                                                    <DataItemTemplate>
                                                        <%#getDocumentUrl(Eval("DocumentId"))%>
                                                    </DataItemTemplate>
                                                </dxwgv:GridViewDataTextColumn>--%>
                                            </Columns>
                                        </dxwgv:ASPxGridView>
                                        <br />
                                            <div class="historyTableCaption">
                                            <p>
                                                    Цели мероприятия
                                            </p>
                                        </div>
                                        <dxwgv:ASPxGridView runat="server" ID="gridPlansActivityGoal" KeyFieldName="Id" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                                            OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" Settings-GroupFormat="{1}" SettingsBehavior-AutoExpandAllGroups="true" Settings-ShowColumnHeaders="false">
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn VisibleIndex="0" FieldName="Caption" Caption=" " Name="Caption" Visible="false" PropertiesTextEdit-EncodeHtml="false">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Цель мероприятия" FieldName="Name"></dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                        </dxwgv:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </dxw:ContentControl>
                    </ContentCollection>
                </dxtc:TabPage>
                <dxtc:TabPage Text="Функции" Name="Functions">
                    <ContentCollection>
                        <dxw:ContentControl>
                            <div class="historyTableCaption">
                                <p>
                                    Функции и полномочия государственного органа, на автоматизацию которого направлено мероприятие:
                                </p>
                            </div>
                            <dxwgv:ASPxGridView runat="server" ID="gridFunc" KeyFieldName="Id" Width="100%" AutoGenerateColumns="False" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                                OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" Settings-GroupFormat="{1}" SettingsBehavior-AutoExpandAllGroups="true">
                                <Styles>
                                    <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                    </Header>
                                </Styles>
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn VisibleIndex="0" FieldName="Caption" Caption=" " Name="Caption" Visible="false" PropertiesTextEdit-EncodeHtml="false">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn VisibleIndex="1" FieldName="Name" Caption="Выполняемые функции органом государственной власти">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn VisibleIndex="2" FieldName="Type" Caption="Вид деятельности">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn VisibleIndex="3" FieldName="NormativeAct" Caption="Наименование и реквизиты регулирующих НПА (с указанием конкретного пункта, статьи нормативно-правового акта)">
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                            </dxwgv:ASPxGridView>
                        </dxw:ContentControl>
                    </ContentCollection>
                </dxtc:TabPage>
                <dxtc:TabPage Text="Показатели/индикаторы" Name="GoalIndicators">
                    <ContentCollection>
                        <dxw:ContentControl>
                            <table class="form_edit">
                                <tr>
                                    <td>
                                        <div class="historyTableCaption">
                                            <p>
                                                Целевые показатели
                                            </p>
                                        </div>
                                        <dxwgv:ASPxGridView KeyFieldName="Id" runat="server" ID="gridGoalIndicator"
                                            AutoGenerateColumns="False" Width="100%" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                                            OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" Settings-GroupFormat="{1}" SettingsBehavior-AutoExpandAllGroups="true">
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn Caption=" " FieldName="Caption" Name="Caption" Visible="false" PropertiesTextEdit-EncodeHtml="false" />
                                                <dxwgv:GridViewDataTextColumn Caption="Год" FieldName="Year" Name="Year" />
                                                <dxwgv:GridViewDataTextColumn Caption="Тип" FieldName="FeatureType" />
                                                <dxwgv:GridViewDataTextColumn Caption="Наименование/описание" FieldName="Name" />
                                                <dxwgv:GridViewDataTextColumn Caption="Единица измерения" FieldName="UOM" />
                                                <dxwgv:GridViewDataTextColumn Caption="Значение" FieldName="PRSFactVal" />
                                                <dxwgv:GridViewDataTextColumn Caption="Госуслуга/госфункция" FieldName="Func" />
                                                <dxwgv:GridViewDataTextColumn Caption="Цель мероприятия" FieldName="Goal" />
                                                <dxwgv:GridViewDataTextColumn Caption="Дополнительное финансирование" FieldName="AddFounding">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Алгоритм расчетов" FieldName="Algorythm" />
                                                <dxwgv:GridViewDataTextColumn Caption="Значение" FieldName="IsNotCurrent" SortIndex="1" SortOrder="Ascending">
                                                    <DataItemTemplate><%#Eval("IsNotCurrent").ToString() == "1" ?  "плановый" : "базовый"%></DataItemTemplate>
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                            <Styles>
                                                <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                </Header>
                                            </Styles>
                                        </dxwgv:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </dxw:ContentControl>
                    </ContentCollection>
                </dxtc:TabPage>
                
                <dxtc:TabPage Text="Расходы" Name="Expenses">
                    <ContentCollection>
                        <dxw:ContentControl>
                            <table class="form_edit">
                                <tr>
                                    <td>
                                        <div class="historyTableCaption">
                                            <p>
                                                Сведения об объемах расходов по мероприятиям за счет средств федерального бюджета (государственных внебюджетных фондов)
                                            </p>
                                        </div>
                                        <dxwgv:ASPxGridView runat="server" ID="gridExpense1" OnBeforePerformDataSelect="grid_BeforePerformDataSelect" OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" CssClass="transparentGrid">
                                            <Settings GridLines="None" ShowGroupPanel="false" ShowColumnHeaders="false" />
                                            <Border BorderStyle="None" />
                                            <Styles Table-BackColor="Transparent">
                                                <AlternatingRow Enabled="False"></AlternatingRow>
                                            </Styles>
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn FieldName="Key" CellStyle-HorizontalAlign="Right" CellStyle-Wrap="False">
                                                    <CellStyle HorizontalAlign="Right" Font-Bold="true"></CellStyle>
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Value">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Order" SortIndex="0" SortOrder="Ascending"
                                                    Visible="false">
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                        </dxwgv:ASPxGridView>
                                        <hr />
                                        <div class="historyTableCaption">
                                            <p>
                                                Объем планируемых бюджетных ассигнований, тыс. руб.
                                            </p>
                                        </div>
                                        <dxwgv:ASPxGridView runat="server" ID="gridExpense2" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                                            OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" CssClass="transparentGrid">
                                            <Settings GridLines="None" ShowGroupPanel="false" />
                                            <Border BorderStyle="None" />
                                            <Styles Table-BackColor="Transparent">
                                                <Header BackColor="Transparent" Border-BorderStyle="None">
                                                </Header>
                                                <AlternatingRow Enabled="False"></AlternatingRow>
                                            </Styles>
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn FieldName="Key" Caption=" " Width="300px">
                                                    <CellStyle HorizontalAlign="Right" Font-Bold="true"></CellStyle>
                                                    <HeaderStyle Border-BorderStyle="None" BackColor="Transparent" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Value" Caption="Основной объем:" Width="300px" CellStyle-HorizontalAlign="Center">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataColumn Caption=" " Width="30px">
                                                </dxwgv:GridViewDataColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Value2" Caption="Дополнительная потребность:" Width="300px" CellStyle-HorizontalAlign="Center">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Order" SortIndex="0" SortOrder="Ascending"
                                                    Visible="false">
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                        </dxwgv:ASPxGridView>
                                        <hr />
                                        <div class="historyTableCaption">
                                            <p>
                                                Объем фактически израсходованных бюджетных ассигнований, тыс. руб.
                                            </p>
                                        </div>
                                        <dxwgv:ASPxGridView runat="server" ID="gridExpense3" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                                            OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" CssClass="transparentGrid">
                                            <Settings GridLines="None" ShowGroupPanel="false" ShowColumnHeaders="False" />
                                            <Border BorderStyle="None" />
                                            <Styles Table-BackColor="Transparent"></Styles>
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn FieldName="Key" CellStyle-HorizontalAlign="Right">
                                                    <CellStyle HorizontalAlign="Right" Font-Bold="true"></CellStyle>
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Value">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Order" SortIndex="0" SortOrder="Ascending"
                                                    Visible="false">
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                        </dxwgv:ASPxGridView>
                                        <div class="historyTableCaption">
                                            <p>
                                                Направления расходования средств на ИКТ
                                            </p>
                                        </div>
                                        <dxwgv:ASPxGridView runat="server" ID="gridExpenseDirection" KeyFieldName="Id" Width="100%"
                                            AutoGenerateColumns="False" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                                            OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" Settings-GroupFormat="{1}" SettingsBehavior-AutoExpandAllGroups="true">
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn VisibleIndex="0" FieldName="Caption" Caption=" " Name="Caption" Visible="false" PropertiesTextEdit-EncodeHtml="false">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="CodeName" Caption="Направление расходов"
                                                    SortIndex="0" SortOrder="Ascending" />
                                            </Columns>
                                            <Styles>
                                                <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                            </Styles>
                                        </dxwgv:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </dxw:ContentControl>
                    </ContentCollection>
                </dxtc:TabPage>
                <dxtc:TabPage Text="Результаты" Name="GoalsResults">
                    <ContentCollection>
                        <dxw:ContentControl>
                            <table class="form_edit">
                                <tr>
                                    <td>
                                        <div class="historyTableCaption">
                                            <p>
                                                Достигнутые результаты
                                            </p>
                                        </div>
                                        Ранее заключенные гос. контракты
                                        <dxwgv:ASPxGridView runat="server" ID="gridStateContract" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                            OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" Settings-GroupFormat="{1}" SettingsBehavior-AutoExpandAllGroups="true" Width="100%">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="Caption" Caption=" " Name="Caption" Visible="false" PropertiesTextEdit-EncodeHtml="false">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="Number" Caption="Номер контракта" />
                                <dxwgv:GridViewDataMemoColumn FieldName="Subject" Caption="Предмет контракта">
                                    <PropertiesMemoEdit EncodeHtml="False">
                                    </PropertiesMemoEdit>
                                </dxwgv:GridViewDataMemoColumn>
                                <dxwgv:GridViewDataDateColumn FieldName="ContractDate" Caption="Дата заключения контракта" />
                                <dxwgv:GridViewDataDateColumn FieldName="DescriptionPreviousResult" Caption="Описание ранее достигнутых результатов" />
                            </Columns>
                        </dxwgv:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </dxw:ContentControl>
                    </ContentCollection>
                </dxtc:TabPage>
                <dxtc:TabPage Text="Работы" Name="Works">
                    <ContentCollection>
                        <dxw:ContentControl>
                            <table class="form_edit">
                                <tr>
                                    <td>
                                        <dxwgv:ASPxGridView runat="server" ID="gridWork" KeyFieldName="Id" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                                            OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" Width="100%"
                                            OnDataBound="grid_DataBound"
                                            Settings-GroupFormat="{1}"
                                            SettingsBehavior-AutoExpandAllGroups="true"
                                            SettingsDetail-ShowDetailRow="true">
                                            <Styles>
                                                <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                </Header>
                                            </Styles>
                                            <Columns>
                                                <dxwgv:GridViewDataColumn FieldName="Id" ShowInCustomizationForm="False" Visible="false" VisibleIndex="0" />
                                                <dxwgv:GridViewDataTextColumn Caption=" " FieldName="Caption" Name="Caption" Visible="false" PropertiesTextEdit-EncodeHtml="false" />
                                                <dxwgv:GridViewDataTextColumn Caption="Код" FieldName="ActivityWorkNum" ReadOnly="True"
                                                    ShowInCustomizationForm="True" VisibleIndex="2">
                                                    <Settings AllowSort="False" SortMode="Value" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="Name" ShowInCustomizationForm="True" Caption="Наименование"
                                                    VisibleIndex="3">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Вид затрат" FieldName="ExpenseDirection_CodeName"
                                                    VisibleIndex="5">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Тип затрат" FieldName="ExpenseType_Name"
                                                    VisibleIndex="5">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Код ОКДП" FieldName="OKDP_CodeName" VisibleIndex="6">
                                                </dxwgv:GridViewDataTextColumn>
                                                <%--<dxwgv:GridViewDataTextColumn Caption="Код БК ВР" FieldName="WorkForm_CodeName" VisibleIndex="7">
                                                </dxwgv:GridViewDataTextColumn>--%>
                                                <dxwgv:GridViewBandColumn Caption="Объем бюджетных ассигнований, тыс. руб." VisibleIndex="9"
                                                    Name="ExpenseVolume">
                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn Caption="на очередной финансовый год" FieldName="ExpenseVolumeYear0"
                                                            VisibleIndex="11">
                                                            <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                            </PropertiesTextEdit>
                                                        </dxwgv:GridViewDataTextColumn>
                                                    </Columns>
                                                </dxwgv:GridViewBandColumn>
                                            </Columns>
                                            <Templates>
                                                <DetailRow>
                                                    <p>
                                                        <b>Типы затрат</b>
                                                    </p>
                                                    <dxwgv:ASPxGridView runat="server" ID="gridTypesWork" AutoGenerateColumns="false" KeyFieldName="Id" SettingsBehavior-AutoExpandAllGroups="true"
                                                        OnBeforePerformDataSelect="grid_BeforePerformDataSelect" Settings-GroupFormat="{1}"
                                                        OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared">
                                                        <Columns>
                                                            <dxwgv:GridViewDataTextColumn Caption=" " FieldName="Caption" Name="Caption" Visible="false" PropertiesTextEdit-EncodeHtml="false" />
                                                            <dxwgv:GridViewDataTextColumn Caption="Тип затрат" FieldName="Name" VisibleIndex="2"></dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewBandColumn Caption="Объем бюджетных ассигнований, тыс. руб." VisibleIndex="3"
                                                                Name="ExpenseVolume">
                                                                <Columns>
                                                                    <dxwgv:GridViewDataTextColumn Caption="на очередной финансовый год" FieldName="ExpY0">
                                                                        <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                                        </PropertiesTextEdit>
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                </Columns>
                                                            </dxwgv:GridViewBandColumn>
                                                        </Columns>
                                                        <Styles>
                                                            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            </Header>
                                                        </Styles>
                                                    </dxwgv:ASPxGridView>
                                                </DetailRow>
                                            </Templates>
                                        </dxwgv:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </dxw:ContentControl>
                    </ContentCollection>
                </dxtc:TabPage>
                <dxtc:TabPage Text="Товары" Name="Specifications">
                    <ContentCollection>
                        <dxw:ContentControl>
                            <table class="form_edit">
                                <tr>
                                    <td>
                                        <dxwgv:ASPxGridView runat="server" ID="gridSpecification" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
                                            OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" OnDataBinding="grid_DataBound"  
                                            Settings-GroupFormat="{1}" SettingsBehavior-AutoExpandAllGroups="true"
                                            SettingsDetail-ShowDetailRow="true" KeyFieldName="Id">
                                            <Styles>
                                                <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                </Header>
                                            </Styles>
                                            <Columns>
                                                <dxwgv:GridViewDataColumn FieldName="Id" ShowInCustomizationForm="False" Visible="false" VisibleIndex="0" />
                                                <dxwgv:GridViewDataTextColumn Caption=" " FieldName="Caption" Name="Caption" Visible="false" PropertiesTextEdit-EncodeHtml="false" />
                                                <dxwgv:GridViewDataTextColumn Caption="Код" FieldName="ActivitySpecNum" ReadOnly="True"
                                                    ShowInCustomizationForm="True" SortIndex="1" SortOrder="Ascending" VisibleIndex="10">
                                                    <Settings AllowSort="True" SortMode="Value" />
                                                    <EditFormSettings Visible="False" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Наименование оборудования, программного обеспечения"
                                                    FieldName="Name" VisibleIndex="20">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Тип оборудования (программного обеспечения)"
                                                    FieldName="QT" VisibleIndex="30">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Код ОКДП" FieldName="OKDP_CodeName" VisibleIndex="32">
                                                </dxwgv:GridViewDataTextColumn>
                                                <%--<dxwgv:GridViewDataTextColumn Caption="Код БК ВР" FieldName="WorkForm_CodeName" VisibleIndex="35">
                                                </dxwgv:GridViewDataTextColumn>--%>
                                                <dxwgv:GridViewDataTextColumn Caption="Вид затрат" FieldName="ExpenseDirection_CodeName"
                                                    VisibleIndex="40">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Тип затрат" VisibleIndex="42" FieldName="ExpName"></dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewBandColumn Caption="Оборудование, лицензии программного обеспечения, в том числе свободное программное обеспечение с нулевой стоимостью"
                                                    VisibleIndex="50" Name="Equipment">
                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn Caption="Аналог для расчета стоимости" FieldName="Analog"
                                                            VisibleIndex="60">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Количество, ед." FieldName="Quantity" VisibleIndex="70" CellStyle-HorizontalAlign="Center">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Цена аналога, тыс. руб" FieldName="AnalogPrice"
                                                            VisibleIndex="80" CellStyle-HorizontalAlign="Center">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Стоимость, тыс. руб." FieldName="Cost" VisibleIndex="90" CellStyle-HorizontalAlign="Center">
                                                            <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                            </PropertiesTextEdit>
                                                        </dxwgv:GridViewDataTextColumn>
                                                    </Columns>
                                                </dxwgv:GridViewBandColumn>
                                            </Columns>
                                            <Templates>
                                                <DetailRow>
                                                    <p>
                                                        <b>Характеристики</b>
                                                    </p>
                                                    <dxwgv:ASPxGridView runat="server" ID="gridTypesSpec" AutoGenerateColumns="false" KeyFieldName="Id"
                                                        OnBeforePerformDataSelect="grid_BeforePerformDataSelect" OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared"
                                                        Settings-GroupFormat="{1}" SettingsBehavior-AutoExpandAllGroups="true">
                                                        <Columns>
                                                            <dxwgv:GridViewDataTextColumn Caption=" " FieldName="Caption" Name="Caption" Visible="false" PropertiesTextEdit-EncodeHtml="false" />
                                                            <dxwgv:GridViewBandColumn Caption="Оборудование, лицензии программного обеспечения, в том числе свободное программное обеспечение с нулевой стоимостью"
                                                                Name="Equipment">
                                                                <Columns>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Наименование характеристики" FieldName="CharName"></dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Ед.изм.характеристики" FieldName="CharUnit"></dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Значение характеристики" FieldName="CharValue"></dxwgv:GridViewDataTextColumn>
                                                                </Columns>
                                                            </dxwgv:GridViewBandColumn>
                                                        </Columns>
                                                        <Styles>
                                                            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            </Header>
                                                        </Styles>
                                                    </dxwgv:ASPxGridView>
                                                </DetailRow>
                                            </Templates>
                                        </dxwgv:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </dxw:ContentControl>
                    </ContentCollection>
                </dxtc:TabPage>
            </TabPages>
        </dxtc:ASPxPageControl>
</div>
</asp:Content>