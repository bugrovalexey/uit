﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    using System;
    using System.Collections.Generic;
    using bgTeam.Extensions;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Workflow;

    /// <summary>Мероприятие Плана</summary>
    public class PlansActivity : EntityNumerable, IWorkflowObject, IEntityWorkflow
    {
        public PlansActivity()
        {
        }

        public PlansActivity(Plan Plan)
        {
            this.Plan = Plan;
            this.ExpenseDirections = new List<ActivityExpenseDirections>();
            this.Works = new List<PlansWork>();
            this.Specifications = new List<PlansSpec>();
        }

        internal PlansActivity(int id)
        {
            this.Id = id;
        }

        public virtual DateTime? CreateDate { get; set; }

        /// <summary>План</summary>
        public virtual Plan Plan { get; set; }

        /// <summary>Тип для новых мероприятий</summary>
        public virtual ActivityType ActivityType2 { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Номер</summary>
        public virtual string ActivityNum
        {
            get
            {
                if (Num < 100 && Num >= 10)
                    return string.Format("{0}00", Num);
                if (Num < 10)
                    return string.Format("0{0}00", Num);
                return Num.ToString();
            }
        }

        /// <summary>Наименование + Номер</summary>
        public virtual string NameNum
        {
            get { return string.Format("{0} - {1}", ActivityNum, Name); }
        }

        /// <summary>Работы мероприятия</summary>
        public virtual IList<PlansWork> Works { get; protected set; }

        /// <summary>Спецификации мероприятия</summary>
        public virtual IList<PlansSpec> Specifications { get; protected set; }

        /// <summary>Цели и задачи</summary>
        //public virtual IList<PlansActGT> PlansActGTs{ get; protected set; }

        /// <summary>Целевые показатели</summary>
        public virtual IList<PlansGoalIndicator> PlansGoalIndicators { get; protected set; }

        /// <summary>Наименование и реквизиты основания реализации мероприятия</summary>
        //public virtual IList<PlansActivityReason> ActivityReasons { get; protected set; }

        /// <summary>Направления расходов</summary>
        public virtual IList<ActivityExpenseDirections> ExpenseDirections { get; protected set; }

        /// <summary>Функции и полномочия</summary>
        //public virtual IList<PlansActivityFunc> Functions { get; protected set; }

        /// <summary>ИКТ компонент</summary>
        public virtual IKTComponent IKTComponent { get; set; }

        /// <summary>Статус ФГИС</summary>
        public virtual Status FgisStatus { get; set; }

        /// <summary>Ведомство</summary>
        public virtual Department Department { get; set; }

        /// <summary>Код по БК ГРБС</summary>
        public virtual GRBS GRBS { get; set; }

        /// <summary>Код по БК ПЗ/РЗ</summary>
        public virtual SectionKBK SectionKBK { get; set; }

        /// <summary>Код по БК ЦСР</summary>
        public virtual ExpenseItem ExpenseItem { get; set; }

        /// <summary>Код по БК ВР</summary>
        public virtual WorkForm WorkForm { get; set; }

        /// <summary> Код по БК КОСГУ </summary>
        public virtual ExpenditureItem ExpenditureItem { get; set; }

        /// <summary>Ответственный</summary>
        public virtual Responsible Responsible { get; set; }

        /// <summary>Подпрограмма</summary>
        public virtual string Subentry { get; set; }

        /// <summary>Код по БК</summary>
        public virtual string CodeBK
        {
            get { return string.Concat(GRBS.Code, " ", SectionKBK.Code, " ", ExpenseItem.Code, " ", WorkForm.Code); }
        }

        //===================================================Plan2012new=================================================================

        /// <summary>Признак приоритетности мероприятия</summary>
        public virtual bool IsPriority { get; set; }

        #region OldFields
        /// <summary>ЦОДы</summary>
        //public virtual IList<Zod> Zods{ get; protected set; }

        /// <summary>
        /// Наименования программного решения в ФАП
        /// </summary>
        //public virtual IList<ActivityApfInfo> ActivityApfInfos{ get; protected set; }

        /// <summary>
        /// Заключенные государственные контракты
        /// </summary>
        //public virtual IList<PlansStateContract> PlansStateContracts { get; protected set; }

        /// <summary>
        /// Объем планируемых бюджетных ассигнований на очередной финансовый год
        /// </summary>
        //public virtual double? PlansActivityVolY0 { get; set; }

        /// <summary>
        /// Объем планируемых бюджетных ассигнований на первый год планового периода
        /// </summary>
        //public virtual double? PlansActivityVolY1 { get; set; }

        /// <summary>
        /// Объем планируемых бюджетных ассигнований на второй год планового периода
        /// </summary>
        //public virtual double? PlansActivityVolY2 { get; set; }

        /// <summary>
        /// Дополнительная потребность на очередной финансовый год
        /// </summary>
        //public virtual double? PlansActivityAddVolY0 { get; set; }

        /// <summary>
        /// Дополнительная потребность на первый год планового периода
        /// </summary>
        //public virtual double? PlansActivityAddVolY1 { get; set; }

        /// <summary>
        /// Дополнительная потребность на второй год планового периода
        /// </summary>
        //public virtual double? PlansActivityAddVolY2 { get; set; }

        /// <summary>
        /// Величина фактических расходов по состоянию на дату
        /// </summary>
        //public virtual DateTime? PlansActivityFactDate { get; set; }

        /// <summary>
        /// Объем фактически израсходованных бюджетных ассигнований на очередной финансовый год
        /// </summary>
        //public virtual double PlansActivityFactVolY0 { get; set; }

        /// <summary>
        /// Объем фактически израсходованных бюджетных ассигнований на первый год планового периода
        /// </summary>
        //public virtual double PlansActivityFactVolY1 { get; set; }

        /// <summary>
        /// Объем фактически израсходованных бюджетных ассигнований на второй год планового периода
        /// </summary>
        //public virtual double PlansActivityFactVolY2 { get; set; }
        #endregion

        /// <summary>
        /// Аннотация
        /// </summary>
        public virtual string AnnotationData { get; set; }

        ///// <summary>Ответственный за мероприятие (строка)</summary>
        //public virtual string ResponsibleStr { get; set; }

        ///// <summary>Должность</summary>
        //public virtual string ResponsibleJob { get; set; }

        ///// <summary>Email</summary>
        //public virtual string ResponsibleEmail { get; set; }

        ///// <summary>Телефон</summary>
        //public virtual string ResponsiblePhone { get; set; }

        public virtual string FgisInfoText { get; set; }

        public virtual IList<PlansActivityGoal> PlansActivityGoals { get; protected set; }

        //public virtual IList<InitialActivity> PlansInitialActivity { get; protected set; }

        public virtual string Code { get; set; }

        //public virtual SourceFunding SourceFunding { get; set; }

        //public virtual IKTObject IKTObject { get; set; }

        /// <summary> Согасованное мероприятие </summary>
        public virtual bool IsAgree { get; set; }

        public virtual DateTime? DatePlacement { get; set; }

        //public virtual IList<PlansLots> Lots { get; set; }

        //public virtual IList<PlansZakupki> PlansZakupki { get; set; }

        public virtual int? PlansActivityCoordId { get; set; }

        public virtual string PlansActivityCoordCode { get; set; }

        /// <summary>
        /// Присоединёно к..
        /// </summary>
        public virtual PlansActivity JoinedTo { get; set; }

        /// <summary>
        /// Скопировано из..
        /// </summary>
        public virtual int? BasedOn { get; set; }

        public virtual User ResponsibleInformation { get; set; }

        public virtual User ResponsibleTechnical { get; set; }

        public virtual User ResponsibleFinancial { get; set; }

        public virtual AccountingObject AccountingObject
        {
            get { return AoActivity.Return(a => a.AccountingObject, default); }
        }

        public virtual IList<Files> Documents { get; protected set; }

        public virtual IList<ActivityDocuments> ActivityDocuments { get; set; }

        public virtual Status Status { get; set; }

        /// <summary>Показатели</summary>
        public virtual IList<PlansActivityMark> Marks { get; set; }

        /// <summary>Индикаторы</summary>
        public virtual IList<PlansActivityIndicator> Indicators { get; set; }

        /// <summary>Логотип</summary>
        public virtual IList<Files> Logo { get; protected set; }

        /// <summary>Краткое наименование (NOT NULL)</summary>
        //public virtual string NameSmall { get; set; }

        /// <summary>Описание</summary>
        public virtual string Description { get; set; }

        /// <summary>Ответственное структурное подразделение</summary>
        public virtual string ResponsibleDepartment { get; set; }

        /// <summary>Сотрудник, подписывающий проект</summary>
        public virtual User Signing { get; set; }

        /// <summary>Год</summary>
        public virtual int Year { get; set; }

        public virtual IList<ActivityWorks> ActivityWorks { get; set; }

        #region Концепция

        /// <summary>Приказ №</summary>
        public virtual int? Order { get; set; }

        /// <summary>Дата приказа</summary>
        public virtual DateTime? OrderDate { get; set; }

        /// <summary>Концепция</summary>
        public virtual IList<Files> Conception { get; protected set; }

        /// <summary>Предпосылки реализации мероприятия</summary>
        public virtual string Prerequisites { get; set; }

        /// <summary>Цели мероприятия</summary>
        public virtual string Goal { get; set; }

        /// <summary>Задачи мероприятия</summary>
        public virtual string Task { get; set; }

        #endregion Концепция

        ///// <summary>Сотрудник, подписывающий проект</summary>
        //public virtual IList<ActivityConception> Conception { get; set; }

        #region Основания

        /// <summary>Государственная программа</summary>
        public virtual StateProgram StateProgram { get; set; }

        /// <summary>Подпрограмма государственной программы</summary>
        public virtual StateProgram StateSubProgram { get; set; }

        /// <summary>Основное мероприятие государственной программы</summary>
        public virtual StateProgram StateMainEvent { get; set; }

        /// <summary>Другие основания реализации</summary>
        public virtual IList<Reason> OtherReasons { get; protected set; }

        #endregion Основания

        public virtual ActivityExpertiseConclusion ExpertiseConclusion { get; set; }


        /// <summary>
        /// Мероприятие ОУ
        /// </summary>
        public virtual AoActivity AoActivity { get; set; }

        public virtual Common.Workflow Workflow { get; set; }

        public virtual ArticleBudget BudgetItem { get; set; }

        public virtual decimal? PlanQuarter1 { get; set; }

        public virtual decimal? PlanQuarter2 { get; set; }

        public virtual decimal? PlanQuarter3 { get; set; }

        public virtual decimal? PlanQuarter4 { get; set; }

        public virtual decimal? ForecastQuarter1 { get; set; }

        public virtual decimal? ForecastQuarter2 { get; set; }

        public virtual decimal? ForecastQuarter3 { get; set; }

        public virtual decimal? ForecastQuarter4 { get; set; }
    }

    /// <summary>
    /// Дополнительные методы для PlansActivity
    /// </summary>
    //public static class PlansActivityHelper
    //{
    //}
    //public enum PlansActivityKind
    //{
    //    CA = 0,
    //    TP = 1,
    //}
}