﻿namespace bgTeam.IsRui.Domain._Mapping.Claims
{
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class ClaimUserCommentMap : ClassMapHiLo<ClaimUserComment>
    {
        public ClaimUserCommentMap()
            : base("ClaimUserComment")
        {
        }

        protected override void InitMap()
        {
            References<Claim>(x => x.Claim, "ClaimUserComment_Claim_ID").Not.Nullable();

            References(x => x.Status,   "ClaimUserComment_Status_ID").Nullable();
            References(x => x.User,     "ClaimUserComment_User_ID").Not.Nullable();
            Map(x => x.Comment,         "ClaimUserComment_Comment").Nullable();
            Map(x => x.CreateDate,      "ClaimUserComment_CreateDate").Not.Nullable();
            Map(x => x.TimeStamp,       "ClaimUserComment_TimeStamp").Not.Nullable();

            HasMany(x => x.Documents).KeyColumns.Add("EntityOwnerId")
                .Where("EntityType = " + (int)EntityType.ClaimUserComment)
                .Where("Type = " + (int)DocTypeEnum.ClaimUserComment)
                .Cascade.Delete().Inverse();
        }
    }
}
