﻿namespace bgTeam.IsRui.Domain.Entities
{
    using bgTeam.IsRui.Domain.Entities.Workflow;

    public interface IEntityWorkflow : IEntity
    {
        Common.Workflow Workflow { get; set; }
    }

    public interface IWorkflowObject : IEntity, IAccess
    {
        Status Status { get; set; }
    }
}