﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Activities/EditCard.master" AutoEventWireup="true" CodeBehind="Services.aspx.cs" Inherits="GB.MKRF.Web.Activities.Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function validateSelectBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && e.value != 'Не задано';
        }

        function GridSave() {
            hf.Set('Name', NameTextBox.GetText());
            hf.Set('Year', YearComboBox.GetValue());
            hf.Set('Count', CountTextBox.GetValue());
            hf.Set('Cost', CostTextBox.GetText());
            hf.Set('KOSGU', KOSGUComboBox.GetValue());
        }
    </script>
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" SyncWithServer="true" />
    <dxpc:ASPxPopupControl ID="PopupControl" runat="server" AllowDragging="True" AllowResize="True"
        CloseAction="CloseButton" Modal="True" EnableViewState="False"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="600px"
        ClientInstanceName="PopupControl" EnableHierarchyRecreation="True">
    </dxpc:ASPxPopupControl>
    <dxe:ASPxButton ID="AddReasonsButton" runat="server"
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {Grid.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="Grid" Width="100%" ClientInstanceName="Grid"
        KeyFieldName="Id"
        OnBeforePerformDataSelect="Grid_BeforePerformDataSelect"
        OnRowInserting="Grid_RowInserting"
        OnRowUpdating="Grid_RowUpdating"
        OnStartRowEditing="Grid_StartRowEditing"
        OnRowDeleting="Grid_RowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Наименование" />
            <dxwgv:GridViewDataComboBoxColumn FieldName="Year" Caption="Год" Name="yearColumn">
                <PropertiesComboBox TextField="Value" ValueField="Key" ValueType="System.Int32" />
            </dxwgv:GridViewDataComboBoxColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Count" Caption="Количество" />
            <dxwgv:GridViewDataTextColumn FieldName="Cost" Caption="Стоимость" >
                <PropertiesTextEdit DisplayFormatString="#,##0.00" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="KOSGU.CodeName" Caption="КОСГУ" />
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc required">Наименование
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo runat="server" ID="NameTextBox" ClientInstanceName="NameTextBox" Rows="5"
                                OnDataBinding="NameTextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                                </ValidationSettings>
                            </dxe:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Год
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox runat="server" ID="YearComboBox" ClientInstanceName="YearComboBox"
                                OnDataBinding="YearComboBox_DataBinding" TextField="Value" ValueField="Key"
                                ValueType="System.Int32" >
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Количество
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="CountTextBox" ClientInstanceName="CountTextBox"
                                MaskSettings-Mask="<0..999999999>" OnDataBinding="CountTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Стоимость, руб.
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="CostTextBox" ClientInstanceName="CostTextBox"
                                MaskSettings-Mask="<0..999999999g>.<00..99>" OnDataBinding="CostTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">БК КОСГУ
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox runat="server" ID="KOSGUComboBox" ClientInstanceName="KOSGUComboBox"
                                OnDataBinding="KOSGUComboBox_DataBinding" TextField="CodeName" ValueField="Id"
                                ValueType="System.Int32">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok"><a onclick="Grid.UpdateEdit();">Сохранить</a></span>
                            <span class="cancel"><a onclick="Grid.CancelEdit();">Отмена</a></span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') GridSave(); }" />
    </dxwgv:ASPxGridView>
</asp:Content>
