﻿///Наша обёртка над родным гридом datatables.net
///требует подключеные библиотеки jquery.js и jquery.dataTables.js
///Переопределены некоторые "родные" свойства, которые будут применятся у всех таблиц, например вс надписи
///Также реализована возможность переопределения некоторых индивидуальных параметров для конкретной таблицы по средством конфига
///Функция BuildDataTable обёртывает стандартный html-элемент
///Параметры table - jquery объект нужной таблицы; 
///advConfig - конфиг, расширяющий функционал. см. jquery.dataTables.js объект DataTable.defaults (стр. 7831) 
///также имеет расширенные свойства:
/// toolbar - jquery селектор, описывающий элемент страницы, который нужно поместить в контейнер непосредственно над таблицей
/// multiSelect - если true то добавляет выбранным строкам css класс row_selected, что позволяет определить выбранные строки
///               если false то по клику по строке начинается обработка события OnRowClick (по умолчанию переход на страницу редактирования записи)
///               по умолчанию false
/// OnRowClick - событие на клик строки в таблице
/// OnMouseOver - событие на наведение курсора на строку в таблице
/// OnMouseOut - событие на выход курсора со строки в таблице
/// см. примеры вызова
///
///
///Примеры вызова:
///Грид по умолчанию: BuildDataTable($("#grid")); - где grid это id аттрибут нужной таблицы
///Грид с параметрами (Таблица справочников):
//BuildDataTable($('#dictGrid'), {
//    "OnRowClick": function (nRow, aData, iDisplayIndex, iDisplayIndexFull, clickLink) {
//        if (clickLink)
//            window.location = clickLink;
//    }
//});
/// Таблица выбора значений справочника:
//BuildDataTable($('#chooseGrid'), {
//    "bAutoWidth": false,
//    "iDisplayLength": 10,
//    "OnRowClick": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//        var tr = $(nRow);
//        var id = tr.data('id');
//        var primaryKeyName = tr.data("name");
//        $('input[name=hf_' + primaryKeyName + ']').val(id);
//        var tds = tr.find('td');
//        $.each(tds, function (i, e) {
//            var name = $(e).data("name");
//            if (i == 0)
//                $('input[name=' + primaryKeyName + ']').val(aData[i]);
//            else
//                $('input[name=' + name + ']').val(aData[i]);
//        });
//        popup.close();
//    },
//    "OnMouseOver": function (nRow) {
//        $(nRow).css('cursor', 'pointer');
//    },
//    "OnMouseOut": function (nRow) {
//        $(nRow).css('cursor', '');
//    }
//});

function BuildDataTable(table, advConfig) {

    var filters = "";

    var aoColumns = [];
    $.each($(table).find('tr').first().find('th'), function (i, e) {
        var column = {
            "orderable": $(e).hasClass('not-sortable') ? false : true,
            "searchable": $(e).hasClass('not-searchable') ? false : true,
            "orderDataType": $(e).hasClass('checkboxColumn') ? "dom-checkbox" : "",
            "orderSequence":$(e).hasClass('not-sortable') ? [] : ["asc", "desc", "none"]
        };
        aoColumns.push(column);
    });

    var aSelected = [];

    var defaultConfig = {
        "columns": aoColumns,
        "order": [],
        "lengthChange": false,
        "info": false,
        "paging": false,
        "pagingType": "simple_numbers",
        "pageLength": 10,
        "dom": '<"toolbar">rtip',
        "language": {
            "aria": {
                "sortAscending": ": сортировка по возрастанию",
                "sortDescending": ": сортировка по убыванию"
            },
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            },
            "emptyTable": "Нет данных для отображения",
            "info": "Показано с _START_ по _END_ запись из _TOTAL_",
            "infoEmpty": "Показано 0 записей",
            "infoFiltered": "(выбрано из _MAX_ записей)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Показывать по _MENU_ записей",
            "loadingRecords": "Загрузка...",
            "processing": "Обработка...",
            "search": "Фильтр:",
            "url": "",
            "zeroRecords": "Не найдено данных"
        },
        "rowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var $nRow = $(nRow);

            $nRow.off(); //сначала отписываемся от всех событий, которые были до этого (нужно для того, чтобы не дублировались хэндлеры при сортировке например)
            $nRow.live('mouseover', function (e) {
                var link = $nRow.data('clicklink');
                if (advConfig !== undefined && advConfig.OnMouseOver) {
                    advConfig.OnMouseOver(nRow, aData, iDisplayIndex, iDisplayIndexFull, link);
                }
                else {
                    if (link) $nRow.css("cursor", "pointer");
                }
            });

            $nRow.live('mouseout', function (e) {
                var link = $nRow.data('clicklink');
                if (advConfig !== undefined && advConfig.OnMouseOut) {
                    advConfig.OnMouseOut(nRow, aData, iDisplayIndex, iDisplayIndexFull, link);
                }
                else {
                    if (link) $nRow.css("cursor", "");
                }
            });

            if (advConfig !== undefined && advConfig.multiSelect) {
                if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
                    $nRow.addClass('row_selected');
                }

                $nRow.live('click', function (e) {
                    var id = this.id;
                    var index = $.inArray(id, aSelected);

                    if (index === -1) {
                        aSelected.push(id);
                    } else {
                        aSelected.splice(index, 1);
                    }

                    $(this).toggleClass('row_selected');
                    if (advConfig.OnMultiSelectRowClick)
                        advConfig.OnMultiSelectRowClick(nRow, aData, iDisplayIndex, iDisplayIndexFull);
                    e.preventDefault();
                });
            }
            else {
                var link = $nRow.data('clicklink');
                $nRow.live('click', function (e) {
                    if (e.target.nodeName != 'A') {
                        if (advConfig.OnRowClick)
                            advConfig.OnRowClick(nRow, aData, iDisplayIndex, iDisplayIndexFull, link);
                        else {
                            if (link)
                                window.location = link;
                        }
                    }
                });
            }
        }
    };

    var config = defaultConfig;
    if (advConfig)
        config = $.extend(true, defaultConfig, advConfig);
    
    if (advConfig && advConfig.pagingPanel) {
        config.info = true,
        config.paging = true
    }

    var dataTable = $(table).dataTable(config);

    var toolbarContainer = $(table).siblings('div.toolbar');
    //toolbarContainer.css('margin-top','20px');
    var toolbarContent = "<div style='position: relative;'>";

    if (advConfig && advConfig.toolbar) {
        var toolbarOuter = $(advConfig.toolbar);
        if (toolbarOuter.length != 0) {
            toolbarContent += toolbarOuter.html();
            toolbarOuter.remove();
        }
    }


    $.each($(table).find('tr').first().find('th'), function (i, e) {
        if (!$(e).hasClass('not-searchable')) {
            var text = $(e).text();
            if ($(e).hasClass('selectable')) {
                filters += fnCreateSelect(dataTable.api().column(i).data().unique(), text);
                $('select[name="' + text + '"]').live('change', function () {
                    var $this = $(this);
                    if ($this.val() != '') {
                        $this.removeClass("search_init");
                    }
                    else {
                        $this.addClass("search_init");
                    }
                    dataTable.fnFilter($(this).val(), i);
                    TogglerToClearButton();
                });
            }
            else {
                filters += "<input type='text' name='" + text + "' value='" + text + "' class='search_init filtercontrol'/>";
            }
            //if ((i + 1) % 3 == 0)
            //    filters += "<br/>";
        } else {
            filters += "<input type='hidden' class='filtercontrol'/>";
        }
    });

    //инициализируемый контейнер с фильтрами
    toolbarContent += "<div class='filterToggler' style='position: absolute; right: 0; bottom: 0;" +
        "'><a class='command'>Подробный фильтр</a></div></div>" +
        //"<div style='clear: both;'></div>" +
        "<div class='filter_panel' style='display: none;'></div>";

    toolbarContainer.append(toolbarContent);
    
    if (!advConfig.showDefaultFilter) {
        toolbarContainer.find('.filterToggler').hide();
    }

    var filterPanel = toolbarContainer.find('.filter_panel');

    //TODO : Нигде не используется
    if (advConfig.FilterContent) {
        toolbarContainer.find('.filter_panel').append(advConfig.FilterContent);
    }
    else {
        toolbarContainer.find('.filter_panel').append(filters);
    }

    var filterinputs = filterPanel.find('input');
    var filterselects = filterPanel.find('select');
    var filtercontrols = filterPanel.find('.filtercontrol');
    filterinputs.keyup(function (e) {
        /* Filter on the column (the index) of this element */
        dataTable.fnFilter(this.value, filtercontrols.index(this));
        TogglerToClearButton();
    });

    var asInitVals = new Array();
    filtercontrols.each(function (i) {
        asInitVals[i] = this.value;
    });

    filterinputs.change(function () {
        if ($(this).val() == '')
            FilterInputDefaultify(this);
        TogglerToClearButton();
    });

    filterinputs.focus(function () {
        var $this = $(this);
        if ($this.hasClass("search_init")) {
            $this.removeClass("search_init");
            $this.val("");
        }
    });

    filterinputs.blur(function (i) {
        if (this.value == "") {
            FilterInputDefaultify(this);
        }
    });

    toolbarContainer.find('.filterToggler a').click(function () {
        var $this = $(this);
        if ($this.text() == "Подробный фильтр") {
            $this.text('Cкрыть фильтр');
            $this.parent().css('background-color', '#EEEECC');
            filterPanel.slideDown(400);
        } else if ($this.text() == "Сбросить фильтр") {
            $this.text('Cкрыть фильтр');
            $.each(filterinputs, function (i, e) {
                if ($(this).is(':not(:disabled)'))
                    FilterInputDefaultify(this);
            });
            $.each(filterselects, function (i, e) {
                if ($(this).is(':not(:disabled)'))
                    FilterSelectDefaultify(this);
            });
        } else {//else if (this.text == "Cкрыть фильтр") {
            $this.text('Подробный фильтр');
            $this.parent().css('background-color', 'transparent');
            filterPanel.slideUp(400);
        }

        if (advConfig.OnFilterTogglerClick)
            advConfig.OnFilterTogglerClick();
    });

    function TogglerToClearButton() {
        if (filterPanel.is(':hidden')) {
            return;
        }

        var toClearButton = false;
        $.each(filterinputs, function (e, i) {
            if ($(this).val() != '' && !$(this).hasClass('search_init')) {
                toClearButton = true;
                return;
            }
        });
        $.each(filterselects, function (e, i) {
            if ($(this).val() != '') {
                toClearButton = true;
                return;
            }
        });

        if (toClearButton) {
            toolbarContainer.find('.filterToggler a').text('Сбросить фильтр');
        }
        else {
            toolbarContainer.find('.filterToggler a').text('Скрыть фильтр');
        }
    }
    function FilterInputDefaultify(filter) {
        var $filter = $(filter);
        $filter.addClass("search_init");
        $filter.val(asInitVals[filtercontrols.index(filter)]);
        dataTable.fnFilter('', filtercontrols.index(filter));
    }
    function FilterSelectDefaultify(filter) {
        var $filter = $(filter);
        $filter.addClass("search_init");
        $filter.find("option:first").attr("selected", "selected");
        dataTable.fnFilter('', filtercontrols.index(filter));
    }

    function fnCreateSelect(aData, text) {
        var r = '<select name="' + text + '" style="width: 32%; margin: 6px;" class="search_init filtercontrol"><option value="" selected="selected">' + text + '...</option>', i, iLen = aData.length;
        for (i = 0 ; i < iLen ; i++) {
            r += '<option value="' + aData[i] + '">' + aData[i] + '</option>';
        }
        return r + '</select>';
    }

    return dataTable;
}