﻿using GB.Data.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using GB.DataAccess.Repository;

namespace GB.MKRF.Providers
{
    public class AccessControllerProvider
    {
        private readonly object _lock = new object();
        private readonly ManualResetEvent _mre = new ManualResetEvent(false);
        private IDictionary<string, SiteMapData> _accessRights = new Dictionary<string, SiteMapData>();

        private readonly string[] _IgnoreAction = new string[] { "IndexCard" };

        /// <summary>
        /// Использовать через Singleton
        /// </summary>
        protected AccessControllerProvider()
        {
            LoadData();
        }

        /// <summary>
        /// Обновляет список прав
        /// </summary>
        public void Reset()
        {
            LoadData();
        }

        public bool СheckAccessCurrentUser(string controller, string action)
        {
            return true;

            // action которые не нужно обрабатывать
            if (_IgnoreAction.Contains(action)) return true;

            var user = UserRepository.GetCurrent();

            // Если нужно автоматически сгенерировать записи для Site_Map_Node
            //try { new SiteMapService().Create(new MKRF.Dtos.Admin.SiteMapArgs() { Controller = controller, Action = action }); }
            //catch { }

            return CheckAccess(controller, action, user);
        }

        private bool CheckAccess(string controller, string action, User user)
        {
            Logger.Debug(string.Concat(controller, " : ", action));

            var data = GetValue(controller, action);

            if (data == null)
                return false;

            foreach (var item in user.Roles)
            {
                if (data.Roles.Contains(item.Id))
                    return true;
            }

            return false;
        }

        private SiteMapData GetValue(string controller, string action)
        {
            _mre.WaitOne(); //ждем окончания работы LoadAccessRights

            var key = string.Concat(controller, action);
            if (_accessRights.ContainsKey(key))
                return _accessRights[key];

            key = controller;
            if (_accessRights.ContainsKey(key))
                return _accessRights[key];

            return null;
        }

        private void LoadData()
        {
            //lock (_lock)
            //{
            //    _mre.Reset(); //блокируем читающие потоки
            //    _accessRights.Clear();
            //    var list = RepositorySt.ExecuteSqlToObject<SiteMapData>("SELECT *  FROM [v_Site_Map_Node]");
            //    foreach (var item in list)
            //    {
            //        _accessRights.Add(string.Concat(item.Controller, item.Action), item.Init());
            //    }
            //    _mre.Set(); //разблокируем читающие потоки
            //}
        }

        private class SiteMapData
        {
            public string Controller { get; set; }

            public string Action { get; set; }

            public string Title { get; set; }

            public string Description { get; set; }

            public IEnumerable<int> Roles { get; set; }

            public string RolesStr { get; set; }

            internal SiteMapData Init()
            {
                Roles = RolesStr.Split(',').Select(x => int.Parse(x));

                return this;
            }
        }
    }
}