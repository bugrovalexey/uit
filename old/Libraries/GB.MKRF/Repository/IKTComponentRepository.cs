﻿using GB.Data;
using GB.Data.Directories;
using System.Collections.Generic;
using System.Linq;

namespace GB.MKRF.Repository
{
    public class IKTComponentRepository : Repository<IKTComponent>
    {
        public List<IKTComponent> GetAllParents()
        {
            return GetAll(x => x.Parent == null).OrderBy(x => x.Id).ToList();
        }

        public List<IKTComponent> GetAllChildren(int parentId)
        {
            return GetAll(x => (x.Parent != null && x.Parent.Id == parentId) || x.Id == EntityBase.Default).OrderBy(x => x.Id).ToList();
        }
    }
}