﻿using Uviri.MKRF.Entities.PlanReport.SectionIX;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionIX
{
    public class OutFuncMap : ClassMapBase<OutFunc>
    {
        public OutFuncMap()
            : base("PRS_9b")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_9b_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_9b_Num = ?
//                          ,@PRS_9b_Func = ?
//                          ,@PRS_9b_Org = ?
//                          ,@Plans_ID = ?
//						  ,@PRS_9b_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_9b_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_9b_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_9b_Delete @PRS_9b_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_9b_Num");
			Map(x => x.Functions, "PRS_9b_Func");
			Map(x => x.Organisation, "PRS_9b_Org");

			References<Plan>(x => x.Plan, "Plans_ID");
        }
    }
}