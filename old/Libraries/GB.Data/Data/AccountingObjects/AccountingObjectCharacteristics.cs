﻿using GB.Data.Directories;
using System;
using System.Collections.Generic;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Характеристика ОУ
    /// </summary>
    public class AccountingObjectCharacteristics : EntityBase
    {
        public AccountingObjectCharacteristics()
        {
        }

        public AccountingObjectCharacteristics(AccountingObject ao)
        {
            this.AccountingObject = ao;
        }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Тип</summary>
        public virtual AccountingObjectCharacteristicsType Type { get; set; }

        /// <summary>Единица измерения</summary>
        public virtual Units Unit { get; set; }

        /// <summary>Номинальное значение</summary>
        public virtual int Norm { get; set; }

        /// <summary>Максимальное значение</summary>
        public virtual int? Max { get; set; }

        /// <summary>Фактическое значение</summary>
        public virtual int? Fact { get; set; }

        /// <summary>Дата начала</summary>
        public virtual DateTime? DateBeginning { get; set; }

        /// <summary>Дата конца</summary>
        public virtual DateTime? DateEnd { get; set; }

        /// <summary>Объект учета</summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Зарегистрирована в ОУ
        /// </summary>
        public virtual bool IsRegister { get; set; }

    }
}