﻿namespace bgTeam.IsRui.Domain.Entities.Claims
{
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Workflow;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimUserComment : EntityBase
    {
        public ClaimUserComment()
        {
            Documents = new List<Files>();
        }

        /// <summary>Комментарий</summary>
        public virtual string Comment { get; set; }

        /// <summary>ОУ</summary>
        public virtual Claim Claim { get; set; }

        /// <summary>Пользователь</summary>
        public virtual User User { get; set; }

        /// <summary> Дата создания </summary>
        public virtual DateTime CreateDate { get; set; }

        /// <summary> Документ </summary>
        public virtual IList<Files> Documents { get; set; }

        /// <summary> Статус </summary>
        public virtual Status Status { get; set; }
    }
}
