﻿namespace bgTeam.AdminDB.Web
{
    using System;
    using System.Reflection;
    using bgTeam.AdminDB.Web.Controllers;
    using bgTeam.Core;
    using bgTeam.Core.Impl;
    using bgTeam.DataAccess;
    using bgTeam.DataAccess.Impl.Dapper;
    using bgTeam.DataAccess.Impl.MsSql;
    using bgTeam.DataAccess.Impl.Oracle;
    using bgTeam.Impl;
    using bgTeam.Infrastructure;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.Common.Services.Impl;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;

    public static class DIContainer
    {
        public static void Configure(IServiceCollection services)
        {
            services.Scan(scan => scan
                .FromExecutingAssembly()
                .AddClasses(classes => classes.AssignableTo(typeof(IStory<,>)))
                .AsImplementedInterfaces()
                .WithTransientLifetime());

            //services.Scan(scan => scan
            //        .FromAssemblyOf<IDataAccessLibrary>()
            //        .AddClasses(classes => classes.AssignableTo(typeof(ICommand<>)))
            //        .AsImplementedInterfaces()
            //        .WithTransientLifetime());

            services.Scan(scan => scan
                //.FromAssemblies(Assembly.GetExecutingAssembly())
                .FromExecutingAssembly()
                .AddClasses(classes => classes.AssignableTo<Controller>())
                .AsImplementedInterfaces()
                .WithTransientLifetime());

            //services.Scan(scan => scan
            //        .FromAssemblyOf<IAppSettings>()
            //        .AddClasses(classes => classes.AssignableTo(typeof(IAppSettings)))
            //        .AddClasses(classes => classes.AssignableTo(typeof(IFileProviderRuiSetting)))
            //        .AsImplementedInterfaces()
            //        .WithTransientLifetime());

            //var commandfactory = new CommandFactory(services);
            var storyfactory = new StoryFactory(services);

            var config = new AppConfigurationDefault();

            services.AddSingleton<IAppConfiguration>(config)
                .AddSingleton<IAppLogger, AppLoggerDefault>()
                .AddSingleton<IStoryFactory>(storyfactory)
                .AddSingleton<IStoryBuilder, StoryBuilder>()
                .AddSingleton<IConnectionSetting, AppSettings>();

            var dbType = (SqlDialectEnum)Enum.Parse(typeof(SqlDialectEnum), config["DBTYPE"]);

            switch (dbType)
            {
                case SqlDialectEnum.MsSql:
                    services.AddSingleton<IConnectionFactory, ConnectionFactoryMsSql>();
                    break;
                case SqlDialectEnum.Oracle:
                    services.AddSingleton<IConnectionFactory, ConnectionFactoryOracle>();
                    break;
                default:
                    throw new ArgumentNullException("DBTYPE");
            }

            services
                .AddSingleton<IConvertHtmlToXls, ConvertHtmlToXls>()
                .AddSingleton<ISqlDialect, SqlDialectDapper>()
                .AddSingleton<IRepository, RepositoryDapper>()
                .AddSingleton<ICrudService, CrudServiceDapper>()
                .AddSingleton<IBuilderQuery>(x =>
                {
                    var logger = x.GetService<IAppLogger>();
                    var repository = x.GetService<IRepository>();

                    return new BuilderQuery(logger, repository, dbType);
                });

            services.BuildServiceProvider();
        }
    }
}
