﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.MKRF.Logic.Status.Handler;

namespace bgTeam.IsRui.MKRF.Logic.Status
{
    /// <summary>
    /// Регистрация менеджера, логика по переводу статусов объекта
    /// </summary>
    public class WorkflowRegistration : ManagerRegistrationBase<WorkflowManager>
    {
        protected override void RegisterHandlers(WorkflowManager manager)
        {
            manager.RegisterHandler<PlansActivity>(new ActivityWorkflowHandler());
            manager.RegisterHandler<ActivityExpertiseConclusion>(new ExpertiseWorkflowHandler());

            //manager.RegisterHandler<AccountingObject>(new AccountingObjectWorkflowHandler());
        }
    }
}