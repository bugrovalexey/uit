﻿using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.Zakupki360.Dto;
using System.ComponentModel;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.SelectLists
{
    public class FindGovernmentContractViewModel
    {
        public int Id { get; set; }
        
        [DisplayName("Реестровый номер")]
        public string Number { get; set; }

        [DisplayName("Закупка")]
        public string Order { get; set; }

        [DisplayName("Статус госконтракта")]
        public int? Status { get; set; }

        [DisplayName("Исполнитель")]
        public string Executer { get; set; }

        [DisplayName("Дата заключения c")]
        public string FromDate { get; set; }

        [DisplayName("Дата заключения по")]
        public string ToDate { get; set; }

        [DisplayName("Стоимость ")]
        public object Price { get; set; }

        [DisplayName("Дата заключения")]
        public object Date { get; set; }

        [DisplayName("Причины отклонения от ожидаемых результатов")]
        public string RcDeviation { get; set; }
        
        public SelectList StatusList { get; set; }


        public FindGovernmentContractViewModel(int id)
        {
            Id = id;

            StatusList = new SelectList(EnumExtensions.ToKeyValuePairs<ContractStatusEnum>(),
                    "Key", "Value");
        }

        
    }
}