﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.WebApp.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public class ServicesViewModel : ActivitiesCardBase
    {
        public IList<TableService> Items { get; private set; }

        public ServicesViewModel(IStatusRepository statusRepository, int ownerId)
            : base(statusRepository, ownerId)
        {
            var controller = new ActivityServicesService();
            Items = controller.Repository
                .GetAllEx(q => q
                    .Where(x => x.Activity == CurrentActivity)
                    .Fetch(x => x.KOSGU).Eager.Clone())
                .Select(x => new TableService(x)).ToList();
        }
    }

    public class TableService : NotificationBaseViewModel
    {
        public int Id { get; set; }

        [DisplayName(ServiceEditViewModel.NameCaption)]
        public string Name { get; set; }

        [DisplayName(ServiceEditViewModel.CountCaption)]
        public int? Count { get; set; }

        [DisplayName(ServiceEditViewModel.CostCaption)]
        public string Cost { get; set; }

        [DisplayName(ServiceEditViewModel.YearCaption)]
        public string YearText { get; set; }

        [DisplayName(ServiceEditViewModel.KOSGUCaption)]
        public string KOSGUText { get; set; }

        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("ServiceDelete", "Activities", new { id = Id }, "Grid"); }
        }

        public TableService(PlansActivityService service)
        {
            if (service == null)
                throw new ArgumentNullException("service");

            Id = service.Id;
            Name = service.Name;
            YearText = YearRepository.GetYearText(service.Year);
            Count = service.Count;
            Cost = service.Cost.ToMoneyString();
            KOSGUText = service.KOSGU.Return(x => x.CodeName, default(string)); ;
        }
    }

    [MetadataType(typeof(ServiceEditViewModel_Validation))]
    public class ServiceEditViewModel : ActivitiesCardBase
    {
        public const string EditActionLink = "ServiceEdit";

        public const string NameCaption = "Наименование";
        public const string YearCaption = "Год";
        public const string CountCaption = "Количество";
        public const string CostCaption = "Стоимость, руб.";
        public const string KOSGUCaption = "КОСГУ";

        public int ServiceId { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(YearCaption)]
        public YearEnum Year { get; set; }

        [DisplayName(CountCaption)]
        public int? Count { get; set; }

        [DisplayName(CostCaption)]
        public double Cost { get; set; }

        public SelectList KOSGUList { get; set; }

        [DisplayName(KOSGUCaption)]
        public int KOSGUId { get; set; }

        public SelectList YearList { get; set; }

        public ServiceEditViewModel(IStatusRepository statusRepository, int ownerId, int? id)
            : base(statusRepository, ownerId)
        {
            if (id.HasValue)
            {
                FillData(new RepositoryDc<PlansActivityService>().GetExisting(id));
            }

            YearList = new SelectList(
                YearRepository.YearList,
                "Key", "Value", Year.ToString());
            KOSGUList = new SelectList(
                DirectoriesRepositary.GetDirectories<ExpenditureItem>(DateTime.Now.Year).OrderBy(x => x.Code),
                "Id", "CodeName", KOSGUId);
        }

        private void FillData(PlansActivityService service)
        {
            ServiceId = service.Id;
            Name = service.Name;
            Year = service.Year;
            Count = service.Count;
            Cost = service.Cost;
            KOSGUId = service.KOSGU.Return(x => x.Id, default(int));
        }
    }

    public class ServiceEditViewModel_Validation : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int Count { get; set; }

        [Range(MoneyMin, MoneyMax, ErrorMessage = RangeMoneyText)]
        public double Cost { get; set; }
    }
}