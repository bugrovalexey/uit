﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Expertise;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpFormHelp5Map : ClassMapBase<ExpFormHelp5>
    {
        public ExpFormHelp5Map()
            : base("Exp_Concl_Sprv_5")
        {
//            Table("w_");

//            Id(x => x.Id, "Exp_Concl_Sprv_5_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@Exp_Concl_Sprv_5_Comm = ?
//                          ,@Plans_Activity_ID = ?
//                          ,@Exp_Concl_Sprv_ID = ?
//                          ,@APF_Info_ID = ?
//						  ,@Exp_Concl_Sprv_5_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Exp_Concl_Sprv_5_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Concl_Sprv_5_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Exp_Concl_Sprv_5_Delete @Exp_Concl_Sprv_5_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Comment, "Exp_Concl_Sprv_5_Comm");

            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID").Not.Nullable();
            References<ExpFormHelp>(x => x.FormHelp, "Exp_Concl_Sprv_ID").Not.Nullable();
            References<ApfInfo>(x => x.ApfInfo, "APF_Info_ID").Not.Nullable();
        }
    }
}
