﻿var Uploader = function (name, HiddenFieldObject, UploadFileName, ValidatorName, Mode, FileContainerId, UploadButtonContainerId,
							FileRowTemplate, FileItemTemplate, DeleteFileButtonTemplate)
{
    
    this.name = name;
    this.MultiSelect = (Mode == "MultiFile");
    this.HiddenField = HiddenFieldObject;

    this.UploadFile = window[UploadFileName];
    this.Valiadtor = window[ValidatorName];
    
    this.FileContainer = $("#" + FileContainerId);
    this.UploadButtonContainer = $("#" + UploadButtonContainerId);    

    if (FileRowTemplate) {
        this.FileRowTemplate = FileRowTemplate;
    }
    else {
        this.FileRowTemplate = "<div class='fileupload_item'><table class='table_not_border'><tr><td>{file}</td><td>{delete}</td></tr></table></div>";
    };

    if (FileItemTemplate) {
        this.FileItemTemplate = FileItemTemplate;
    }
    else {
        this.FileItemTemplate = "<a href='/coord/files/{id}'>{name}</a>";
    }

    if (DeleteFileButtonTemplate) {
        this.DeleteFileButtonTemplate = DeleteFileButtonTemplate;
    }
    else {
        this.DeleteFileButtonTemplate = "<div class='fileupload_sep fileupload_item_delete' title='Удалить' onclick='{command}'></div>";
    }
}

$.extend(Uploader.prototype, {

    ShowUploadButton: function (show) {
        if (show) {
            this.UploadButtonContainer.removeClass('fileupload_none');
        }
        else {
            this.UploadButtonContainer.addClass('fileupload_none');
        }
    },

    AddFileContainer: function (fileInfo) {
        var pair = fileInfo.split(';', 2);
        this.AddContainer(pair[0], pair[1])
    },

    UpdateContainer: function (id, name) {
        var length = 0;
        var properties = this.HiddenField.properties;

        this.FileContainer.empty();

        for (var i in properties) {
            var id = i.substring(ASPxClientHiddenField.TopLevelKeyPrefix.length);
            var name = properties[i];

            var fileItem = this.replaceAll(this.FileItemTemplate, { '{id}': id, '{name}': name });
            var deleteBtn = this.replaceAll(this.DeleteFileButtonTemplate, { '{command}': this.name + '.DeleteFile(\"' + id + '\")' });
            this.FileContainer.append(this.replaceAll(this.FileRowTemplate, { '{file}': fileItem, '{delete}': deleteBtn }));

            length++;
        }

        this.UploadFile.SetVisible(this.MultiSelect || length <= 0);
    },

    AddContainer: function (id, name) {
        this.HiddenField.properties[ASPxClientHiddenField.TopLevelKeyPrefix + id] = name;
        this.UpdateContainer();
    },

    DeleteFile : function(id)
    {
        this.HiddenField.Remove(id);
        this.UpdateContainer();
    },

    Upload : function ()
    {
        this.UploadFile.Upload();
    },

    Validate: function () {
        var properties = this.HiddenField.properties;
        var length = 0;
        for (var i in properties) {
            length++;
        }
        return length > 0;
    },

    GetFiles: function () {
        var data = [];
        var properties = this.HiddenField.properties;

        for (var i in properties) {
             var id = i.substring(ASPxClientHiddenField.TopLevelKeyPrefix.length);
             var name = properties[i];
             data.push(id + ':' + name);
         }
        return data.join(';');
    },

    replaceAll: function (text, replace) {
        var find = [];
        for (var name in replace) {
            find.push(name);
        }

        return text.replace(new RegExp(find.join('|'), 'g'),
                            function (symb) {
                                return replace[symb];
                            });
    }
});
