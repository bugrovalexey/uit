﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.Extensions;

namespace GB.DataAccess.Command.AccountingObjects.Activities
{
    public class DeleteActivityCommand : ICommand<DeleteActivityContext>
    {
        ISessionFactoryGb factory;

        public DeleteActivityCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteActivityContext context)
        {
            using (var session = factory.Create())
            {
                session.Delete<AoActivity>(context.Id);
                session.Commit();
            }
        }
    }
}
