﻿namespace bgTeam.IsRui.Domain._Mapping.Claims
{
    using bgTeam.IsRui.Domain.Entities.Claims;

    internal class ClaimIndicatorMap : ClassMapHiLo<ClaimIndicator>
    {
        public ClaimIndicatorMap()
            : base("Claim_Indicators")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Claim_Indicators_Name");
            Map(x => x.ExplanationRelationshipWithProjectMark, "Claim_Indicators_Explan");
            Map(x => x.BaseValue, "Claim_Indicators_Basic_Value");
            Map(x => x.TargetedValue, "Claim_Indicators_Goal_Value");
            Map(x => x.DateToAchieveGoalValue, "Claim_Indicators_Date");
            Map(x => x.AlgorithmOfCalculating, "Claim_Indicators_Algorithm");
            Map(x => x.Justification, "Claim_Indicators_Justification");

            References(x => x.Unit, "MUnit_ID");
            References(x => x.Claim, "Claim_ID");
        }
    }
}
