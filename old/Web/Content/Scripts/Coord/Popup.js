﻿(function (window) {
    var currentOverlay;
    var popupseed = 0;
    var zIndexSeed = 21;

    //---------------------------------------------------------------------------------------------------
    // public functions
    function minipopup(html_or_elements, el, options) {
        options = $.extend({}, options);

        //options.bottom = true;
        popupseed++;
        var _el = $(el);
        var offset = _el.offset();

        var _container = $("<div id='mdropopup_" + popupseed + "' class='close-handler' style='visibility:hidden'></div>");

        var _parentContainer = _el.closest('.close-handler');
        if (_parentContainer.length) {
            _parentContainer[0].childContainers = _parentContainer.childContainers || [];
            _parentContainer[0].childContainers.push(_container);
        }

        var shadow = !options.bottom ? "2px 2px 5px #888" : "2px 1px 4px #888";
        var _popup = $("<div style='background: #f5f5f5; padding: 6px 10px 8px; border: solid 3px #ccc; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; -moz-box-shadow: " + shadow + "; -webkit-box-shadow: " + shadow + "; box-shadow: " + shadow + ";'></div>");
        _popup.css("z-index", ++zIndexSeed);
        var _popupArrow;
        if (!options.bottom) {
            _popupArrow = $("<img src='/Content/css/images/popup_arrow.png' alt='' style='position: absolute; top: -11px;'/>");
        } else {
            _popupArrow = $("<img src='/Content/css/images/popup_arrow_bottom.png' alt='' style='position: absolute; bottom: -11px;'/>");
        }
        if (options.right === undefined && !options.firstLetterPosition) {
            var center = offset.left + _el.outerWidth() / 2;
            options.right = center > $(window).width() / 2;
        }
        //var 
        _popupArrow.css("z-index", ++zIndexSeed);
        _popupArrow.css(options.right ? { right: "23px"} : { left: "13px" });
        var _closeBox = $("<img src='/Content/css/images/fancy_closebox.png' alt='' style='position: absolute; right: -15px; top: -11px; cursor:pointer;'/>");
        _closeBox.css("z-index", zIndexSeed);
        _container.append(_popup);
        _popup.append(_popupArrow);
        if (!options.disableCloseBox) {
            _popup.append(_closeBox);
        }
        _popup.append(html_or_elements);

        // positioningFunc allows setting some rules on size/position of the popup
        // we'll have some predefiend functions (for static, for element-related and for buildings as custom thing).
        function closePopup(recursive) {
            log("Closing popup...");
            if (!confirmUnsavedForm(_container)) return false;
            _container.remove();
            $("body").unbind("mousedown", bodyMouseDownHandler);
            if (options.close) options.close();
            if (recursive && _parentContainer[0]) {
                _parentContainer[0].close();
            }
        }
        _container[0].close = closePopup;
        this.close = closePopup;

        function bodyMouseDownHandler(e) {
            function isEventInsideContainer(c, e) {
                log("isEventInsideContainer", c, c.childContainers, e);
                if (c == e.target || $(c).has(e.target).length) return true;
                for (var i = 0; c.childContainers && i < c.childContainers.length; i++) {
                    if (isEventInsideContainer(c.childContainers[i], e)) return true;
                }
                return false;
            }

            if (!isEventInsideContainer(_container[0], e)) {
                closePopup();
            }
        }
        $("body").mousedown(bodyMouseDownHandler);
        _closeBox.one("click", function () { closePopup(); });
        $("#" + _container.attr('id') + " .popup-close").live('click', closePopup);


        $("body").prepend(_container);
        // position & show
        _popup.css("position", "absolute");
        if (!options.bottom) {
            log(offset.top, _el.outerHeight());
            _popup.css("top", offset.top + _el.outerHeight() + 5);
        } else {
            _popup.css("top", offset.top - _popup.outerHeight() - 5);
        }
        //log(offset);
        if (!options.firstLetterPosition) {
            _popup.css(options.right ? { right: $(window).width() - offset.left - _el.outerWidth() - 10} : { left: offset.left });
        } else {
            _popup.css("left", offset.left - 27 + (_el.outerWidth() / 2));
        }
        _container.css("visibility", "visible");
    }

    function popover(options) {
        var url = options.url,
            method = options.method || "GET",
            data = options.data,
            html = options.html,
            width = options.width || 750,
            height = options.height,
            scroll = options.scroll === undefined ? !height : options.scroll,
            back = options.back === undefined ? !height : options.back,
            onclose = options.onclose,
            onload = options.onload,
            showTitle = options.showTitle === undefined ? true : options.showTitle,
            titleText = options.titleText;


        if (!html == !url) alert("html or url shoud be supplied but not both.");
        popupseed++;
        var _shadow = $("<div style='position:fixed; left: 0; top: 0; background: #666; background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQI12NIS0ubCQAENAHMsbKjNwAAAABJRU5ErkJggg==); width: 100%; height: 100%'></div>");
        _shadow.css("z-index", ++zIndexSeed);
        var _container = $("<div style='width: 860px; margin:auto; position: relative;' id='mdropopup_" + popupseed + "' class='close-handler'></div>");
        _container.css("z-index", ++zIndexSeed);
        var _content = $("<div style='display:none; position:fixed; background: #ccc; width: 650px; border: solid 8px #ccc; margin-left: -8px; border-radius: 10px; -moz-border-radius: 10px; -webkit-border-radius: 10px;-moz-box-shadow: 0 0 20px rgba(0,0,0,0.7); -webkit-box-shadow: 0 0 20px rgba(0,0,0,0.7); box-shadow: 0 0 20px rgba(0,0,0,0.7);'></div>");
        var _titleContent = $("<div style='background: #eee; padding: 6px 10px 4px; border-bottom: solid 1px #e5e5e5; border-radius: 4px 4px 0 0; -moz-border-radius: 4px 4px 0 0; -webkit-border-radius: 4px 4px 0 0;  user-select: none;-o-user-select:none;-moz-user-select: none;-khtml-user-select: none;-webkit-user-select: none; cursor: default'></div>");
        var _backButton = $("<a href='javascript://' class='popup-close' style='text-decoration:none;line-height:1.5em'>&#0171; <u>Назад</u></a><br/>");
        var _innerContent = $("<div class='scroll-async-reset' style='padding: 10px 10px; margin: 0 0 1px; border-radius: 0 0 4px 4px; -moz-border-radius: 0 0 4px 4px; -webkit-border-radius: 0 0 4px 4px; position: relative; background: #fff'></div>");
        var _closeBox = $("<div class='popup-close' style='position: absolute; right: 3px; top: 6px; cursor:pointer; background: url(/Content/css/images/fancy_closebox.png) no-repeat right center; padding: 0 32px 4px 0; line-height: 24px; font-size: 18px; color: #777; text-shadow: 0 1px 1px #fff'><span style='border-bottom: dotted 1px #999'>Закрыть</span></div>");
        _closeBox.css("z-index", ++zIndexSeed);

        _content.appendTo(_container);
        if (back) {
            _backButton.prependTo(_titleContent);
        }
        if (showTitle) {
            _titleContent.appendTo(_content);
            _closeBox.appendTo(_content);
        }
        _innerContent.appendTo(_content);

        var request;

        // prevent mousewheel
        _shadow.mousewheel(function (e) { e.preventDefault(); });
        _content.mousewheel(function (e, delta) {
            if (delta > 0 && _innerContent.scrollTop() == 0 || delta < 0 &&
                    _innerContent[0].scrollHeight - _innerContent.outerHeight() <= _innerContent.scrollTop() + 2) {
                e.preventDefault();
            }
        });

        var isDragging = false;
        var dragX;
        var dragY;
        var altX = 0;
        var altY = 0;
        _titleContent.mousedown(function (e) {
            log("mousedown", e);
            dragX = e.pageX - altX;
            dragY = e.pageY - altY;
            isDragging = true;
        });
        function handleMove(e) {
            if (!isDragging) return;
            log("mousemove", e);
            altX = e.pageX - dragX;
            altY = e.pageY - dragY;
            setPosition();
        }
        function handleMouseUp(e) {
            if (!isDragging) return;
            log("mouseup", e);
            isDragging = false;
        }
        $(window).mousemove(handleMove).mouseup(handleMouseUp);

        function setPosition() {
            var localHeight = height;
            if (!localHeight) {
                var goodHeightPercent = 90;
                var minHeightPercent = 95;
                var minHeightAfter = 600;

                var totalHeight = $(window).height() - 10;
                var goodHeight = totalHeight * goodHeightPercent / 100;

                if (goodHeight < minHeightAfter) {
                    localHeight = totalHeight * minHeightPercent / 100;
                } else {
                    localHeight = goodHeight;
                }

                // fix padding and border
                localHeight -= 10 * 2 + 3 * 2 + 20 + 46;

                var currentTop = (totalHeight - height) / 2 * 0.5;
                _content.css("top", currentTop);
            }

            if (scroll) {
                // hidden thing required for IE7
                _innerContent.css({ "overflow-y": "scroll", "overflow-x": "hidden" });
                _innerContent.css("height", localHeight);
            } else {
                _innerContent.css("min-height", localHeight);
            }
            //_innerContent.css("height", localHeight);

            // calculate left, top
            var left = ($(window).width() - width) / 2;
            var top = ($(window).height() - localHeight) / 2 - 40;
            _content.width(width).css({ left: left + altX, top: top + altY });
        }
        setPosition();

        $(window).resize(setPosition);

        function closePopup() {
            // display confirmation box is there's something dirty inside popup
            if (!confirmUnsavedForm(_content)) return false;
            $(window).unbind("resize", setPosition).unbind("mousemove", handleMove).unbind("mouseup", handleMouseUp);
            if (request) request.abort();
            _shadow.remove();
            _container.remove();
            if (onclose) onclose();
            currentOverlay = null;
            return false;
        }

        $("#" + _container.attr('id') + " .popup-close").live('click', closePopup);
        _container[0].close = closePopup;

        function showHtml(el) {
            //выбрать только контент страницы
            var _el = $(el);

            // discover and set title
            //var title = _el.filter("h1:first").hide().html();
            //if (!title) {
            //title = _el.find("h1").first().hide().html();
            //}

            _titleContent.append("<span style='font-size:30px'>" + titleText + "</span>");

            _el.appendTo(_innerContent);
            _content.fadeIn("fast");
        }

        function showRootElements() {
            $("body").prepend(_container);
            $("body").prepend(_shadow);
        }

        showRootElements();

        if (html) {
            showHtml(html);
            if (onload) onload();
        } else {
            // add racing with timer
            request = $.ajax({
                url: url,
                data: data,
                type: method,
                cache: false,
                dataType: "html",
                success: function (result) {
                    //_loading.hide();
                    showHtml(result);
                    if (onload) onload();
                }, error: function () { alert("Error."); closePopup(); }
            });
        }
        currentOverlay = this;
        this.close = closePopup;
        return this;
    }

    function closeContainer(innerElement) {
        log("closeContainer", innerElement);
        $(innerElement).closest('.close-handler')[0].close();
    }

    //---------------------------------------------------------------------------------------------------
    // private functions
    function confirmUnsavedForm(element) {
        var hasDirtyForms = $("form.dirty", element).length;
        return !hasDirtyForms || confirm("The form contains unsaved data.\n\nClose it anyway?");
    }

    // export
    window.popup = {
        popover: popover,
        minipopup: minipopup,
        closeContainer: closeContainer
    };

})(window);