﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/v2012new/ActivityCard.master"
    AutoEventWireup="true" CodeBehind="GoalsResults.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.GoalsResults" %>

<%@ Import Namespace="GB.MKRF.Entities.Admin" %>
<%@ Import Namespace="GB.MKRF.Repository" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function hfSaveSC(e) {
            hfGR.Set('ContractId', hfContract.Get("ContractId"));
            hfGR.Set('NumContract', ContractNumber.GetValue());
            hfGR.Set('Subject', ContractProduct.GetValue());
            hfGR.Set('Contract', ContractDate.GetValue());
            hfGR.Set('Description', DescriptionTextBox.GetText());
        }

        function validateResults(s, e) {
            //надо переделать

            //if(Grid.GetVisibleRowsOnPage() > 1 || <=MainFuncCount%> > 0){
            //    e.isValid = s.GetText() != "";
            //}
            //else{
            //     e.isValid = true;
            //}
        }

        function SetContentUrl(s, e) {
            s.SetContentUrl('<%=GetContractUrl() %>');
        }

        function PopupClose(e) {
            //alert(e);
            ContractPopupControl.Hide();
            ContractPanel.PerformCallback(e);
        }
        function CallbackComplete(s, e) {
            ContractNumber.SetText(hfContract.Get("Number"));
            ContractProduct.SetText(hfContract.Get("Product"));
            ContractDate.SetText(hfContract.Get("SignDate"));
        }
    </script>

    <p class="header_grid">
        Ранее заключенные гос. контракты
        <span class="subtitle">
            Заполняется на втором этапе
        </span>
    </p>

    <asp:Panel runat="server" ID="panelGR">


        <dxe:ASPxButton ID="AddButton" runat="server" AutoPostBack="false" Text="Добавить"
            Style="margin-bottom: 10px;">
            <ClientSideEvents Click="function(s,e) { SCGrid.AddNewRow(); hfGR.Set('ContractId', null);  hfContract.Set('ContractId', null); }" />
        </dxe:ASPxButton>
        <dxcp:ASPxCallbackPanel runat="server" ID="ContractPanel" ClientInstanceName="ContractPanel" OnCallback="ContractPanel_Callback">
            <PanelCollection>
                <dxp:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                    <dxwgv:ASPxGridView runat="server" ID="SCGrid" KeyFieldName="Id" Width="100%" AutoGenerateColumns="False"
                        ClientInstanceName="SCGrid" OnBeforePerformDataSelect="SCGrid_BeforePerformDataSelect"
                        OnRowDeleting="SCGrid_RowDeleting" OnRowInserting="SCGrid_RowInserting" OnRowUpdating="SCGrid_RowUpdating"
                        OnStartRowEditing="SCGrid_StartRowEditing">
                        <Columns>
                            <dxwgv:GridViewCommandColumn Caption=" " VisibleIndex="0" Name="cmdColumn" ShowInCustomizationForm="False"
                                AllowDragDrop="False">
                                <EditButton Visible="True" />
                                <DeleteButton Visible="True" />
                                <ClearFilterButton Visible="True" />
                            </dxwgv:GridViewCommandColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="Number" Caption="Номер контракта" />
                            <dxwgv:GridViewDataMemoColumn FieldName="Subject" Caption="Предмет контракта">
                                <PropertiesMemoEdit EncodeHtml="False">
                                </PropertiesMemoEdit>
                            </dxwgv:GridViewDataMemoColumn>
                            <dxwgv:GridViewDataDateColumn FieldName="ContractDate" Caption="Дата заключения контракта" />
                            <dxwgv:GridViewDataDateColumn FieldName="DescriptionPreviousResult" Caption="Описание ранее достигнутых результатов" />
                        </Columns>
                        <SettingsBehavior ConfirmDelete="True" />
                        <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
                        <Styles>
                            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            </Header>
                        </Styles>
                        <SettingsEditing Mode="EditForm" />
                        <Templates>
                            <EditForm>
                                <table class="form_edit">
                                    <tr>
                                        <td class="form_edit_desc"></td>
                                        <td class="form_edit_input">
                                            <a class="command" id="contractLink">Выбрать госконтракт</a>
                                            <dxpc:ASPxPopupControl ID="ContractPopupControl" runat="server" AllowDragging="True" AllowResize="True"
                                                CloseAction="CloseButton" Modal="True" EnableViewState="False" PopupElementID="contractLink"
                                                PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="820px" MinHeight="600px"
                                                HeaderText="Выберите госконтракт" ClientInstanceName="ContractPopupControl" EnableHierarchyRecreation="True">
                                                <ClientSideEvents Init="SetContentUrl"></ClientSideEvents>
                                            </dxpc:ASPxPopupControl>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form_edit_desc"></td>
                                        <td class="form_edit_input">
                                            <div style="height: 10px"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form_edit_desc">Номер контракта:
                                        </td>
                                        <td class="form_edit_input">
                                            <dxe:ASPxLabel runat="server" ID="ContractNumber" ClientInstanceName="ContractNumber"
                                                OnDataBinding="ContractNumber_DataBinding" EncodeHtml="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form_edit_desc" valign="top">Предмет контракта:
                                        </td>
                                        <td class="form_edit_input">
                                            <dxe:ASPxLabel runat="server" ID="ContractProduct" ClientInstanceName="ContractProduct"
                                                OnDataBinding="ContractProduct_DataBinding" EncodeHtml="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form_edit_desc">Дата заключения контракта:
                                        </td>
                                        <td class="form_edit_input">
                                            <dxe:ASPxLabel runat="server" ID="ContractDate" ClientInstanceName="ContractDate"
                                                OnDataBinding="ContractDate_DataBinding" EncodeHtml="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form_edit_desc">
                                            <span class="required"></span>
                                            Описание ранее достигнутых результатов:
                                        </td>
                                        <td class="form_edit_input">
                                            <dxe:ASPxMemo ID="DescriptionTextBox" runat="server" ClientInstanceName="DescriptionTextBox" MaxLength="4000"
                                                OnDataBinding="DescriptionTextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                                <ValidationSettings ErrorDisplayMode="None">
                                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                                </ValidationSettings>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="form_edit_buttons">
                                            <span class="ok"><dxwgv:ASPxGridViewTemplateReplacement ID="ReasonGridTemplateReplacement1" runat="server" ReplacementType="EditFormUpdateButton" /></span>
                                            <span class="cancel"><dxwgv:ASPxGridViewTemplateReplacement ID="ReasonGridTemplateReplacement2" runat="server" ReplacementType="EditFormCancelButton" /></span>
                                        </td>
                                    </tr>
                                </table>
                            </EditForm>
                        </Templates>
                        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') hfSaveSC(e); if (e.command=='STARTEDIT') hfContract.Set('ContractId', null); }" />
                    </dxwgv:ASPxGridView>
                    <dxhf:ASPxHiddenField runat="server" ClientInstanceName="hfContract" ID="hfContract" />
                </dxp:PanelContent>
            </PanelCollection>
            <ClientSideEvents EndCallback="CallbackComplete" />
        </dxcp:ASPxCallbackPanel>

       
        
    </asp:Panel>
    <dxhf:ASPxHiddenField runat="server" ID="hfGR" ClientInstanceName="hfGR" />
    <dxc:ASPxCallback runat="server" ID="callBackGR" ClientInstanceName="callBackGR"
        OnCallback="callBackGR_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanelGR.Hide();}" />
    </dxc:ASPxCallback>
    <dxlp:ASPxLoadingPanel ID="LoadingPanelGR" runat="server" ClientInstanceName="LoadingPanelGR"
        Modal="True" ContainerElementID="panelGR">
    </dxlp:ASPxLoadingPanel>
</asp:Content>
