﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class AddOrSaveAODocumentBaseContext : BaseAddOrSaveContext
    {        
        public int AccountingObjectId { get; set; }
        
        public string Name { get; set; }
        
        public string Number { get; set; }
        
        public DateTime? DateAdoption { get; set; }

        public string Paragraph { get; set; }

        public DateTime? Date { get; set; }

        public string Files { get; set; }
    }
}
