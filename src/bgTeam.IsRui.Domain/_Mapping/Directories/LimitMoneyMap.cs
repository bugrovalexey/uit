﻿using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    internal class LimitMoneyMap : ClassMapHiLo<LimitMoney>
    {
        public LimitMoneyMap()
            : base("Lim_242")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Money, "Lim_242_Money");

            References<Department>(x => x.Department, "Govt_Organ_ID");
            References<Years>(x => x.Year, "Year");
        }
    }
}