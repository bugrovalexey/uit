﻿using GB;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GB.MKRF.Web.Monitoring.v2012new
{
    public partial class ReportActivity : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            DepComboBox.DataBind();
            YearsComboBox.DataBind();

            base.OnPreRender(e);
        }

        protected void DepComboBox_DataBinding(object sender, EventArgs e)
        {
            var data = RepositoryBase<Department>.GetAll(x => x.Id != EntityBase.Default).OrderBy(x => x.Name);

            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = data;
            box.SelectedIndex = 0;
        }


        protected string GetPlansList()
        {
            var plans = PlanRepository.GetAll(x => x.Department.Id == (int)DepComboBox.Value && x.PlanType == ExpObjectTypeEnum.Plan2012new).OrderBy(x => x.Year.Value).ToList();

            StringBuilder sb = new StringBuilder();

            sb.Append("<ul style='padding-left: 105px;'>");

            foreach (var p in plans)
            {
                sb.AppendFormat("<li>План за {1} год - <a style='cursor: pointer' onclick='GenerateReport({0}); return false;'>Сформировать отчет</a></>", p.Id, p.Year);
            }

            if(plans.Count == 0)
                sb.Append("Планов нет");

            sb.Append("</ul>");

            return sb.ToString();
        }

        protected void YearsComboBox_DataBinding(object sender, EventArgs e)
        {
            FillYearComboBox(sender, (int)DepComboBox.Value);
        }

        protected void YearsComboBox_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            FillYearComboBox(sender, int.Parse(e.Parameter));

            (sender as ASPxComboBox).DataBind();
        }
        
        private void FillYearComboBox(object sender, int depId)
        {
            var list = PlanRepository.GetAll(x => x.Department.Id == depId && x.PlanType == ExpObjectTypeEnum.Plan2012new).OrderBy(x => x.Year.Value)
                .Select(x => new
                {
                    Key = x.Id,
                    Text = x.Year.Id
                });

            ASPxComboBox box = sender as ASPxComboBox;
            box.DataSource = list;

            foreach (var item in list)
            {
                if(item.Text == DateTime.Now.Year)
                {
                    box.Value = item.Key;
                }
            }
        }
    }
}