﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class OKPDMap : ClassMapHiLo<OKPD>
    {
        public OKPDMap()
            : base("OKDP")
        {
            //            Table("w_");

            //            Id(x => x.Id, "OKDP_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_OKDP_Insert
            //                          @OKDP_Code = ?
            //                         ,@OKDP_Name = ?
            //                         ,@OKDP_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_OKDP_Update
            //                          @OKDP_Code = ?
            //                         ,@OKDP_Name = ?
            //                         ,@OKDP_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_OKDP_Delete
            //						  @OKDP_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "OKDP_Code");
            Map(x => x.Name, "OKDP_Name");
            Map(x => x.DateStart, "OKDP_Start_Date").Nullable();
            Map(x => x.DateEnd, "OKDP_End_Date").Nullable();
        }
    }
}