﻿using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public class IndicatorsAndMarksViewModel : ActivitiesCardBase
    {
        public IndicatorsAndMarksViewModel(IStatusRepository statusRepository, int id)
            : base(statusRepository, id)
        {
            Marks =
                new PlansActivityMarkService().Repository.GetAllByPlansActivity(id)
                    .Select(x => new TableMark(x));
            Indicators =
                new PlansActivityIndicatorService().Repository.GetAllByPlansActivity(id)
                    .Select(x => new TableIndicator(x));
        }

        public IEnumerable<object> Marks { get; set; }

        public IEnumerable<object> Indicators { get; set; }
    }
}