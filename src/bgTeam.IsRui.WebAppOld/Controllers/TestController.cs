﻿using bgTeam.IsRui.WebApp.Controllers.TestClass;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.GovContracts;
using System.Web.Mvc;
using bgTeam.DataAccess;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public class TestController : Controller
    {
        public IoCTest ioc { get; set; }
        public ICommandFactory _commandFactory;

        public ICommand<DeleteGovContractContext> Dcommand { get; set; }

        public TestController(ICommandFactory _factory)
        {

        }

        public ActionResult TestIOC(int id)
        {
            var context = new DeleteGovContractContext(1);

            //Dcommand.Execute(context);

            //var command = _commandFactory.Create<DeleteGovContractContext>();

            ioc.Execute();

            return View("IoC");
        }
    }
}