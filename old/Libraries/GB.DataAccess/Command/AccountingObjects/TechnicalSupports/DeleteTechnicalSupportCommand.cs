﻿using GB.Data.AccountingObjects.WorksAndGoods;
using GB.Data.Directories;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.TechnicalSupports
{
    public class DeleteTechnicalSupportCommand : ICommand<DeleteTechnicalSupportContext>
    {
        ISessionFactoryGb factory;

        public DeleteTechnicalSupportCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteTechnicalSupportContext context)
        {            
            if (context.xId.HasValue && context.xId != 0)
            {
                var entity = new Repository<TechnicalSupport>().Get(context.xId.Value);

                if (entity != null)
                {
                    var workAndGoods = new Repository<WorkAndGoods>().Get(q => q.Where(x => x.ObjectId == context.xId && x.Type == CategoryTypeOfSupportEnum.TechnicalSupport));

                    using (var session = factory.Create())
                    {                        
                        session.Delete(entity);
                        
                        if (workAndGoods != null)
                        {
                            workAndGoods.ActOfAcceptance = null;
                            session.SaveOrUpdate(workAndGoods);
                            session.Delete(workAndGoods);
                        }

                        session.Commit();
                    }
                }
            }
        }
    }
}
