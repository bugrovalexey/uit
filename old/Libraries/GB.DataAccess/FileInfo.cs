﻿using System;

namespace GB.DataAccess
{
    public class FileInfo
    {
        private object _id;
        public object Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (value == null || value is int || value is Guid)
                {
                    _id = value;
                }
                else
                {
                    throw new ArgumentException(value.ToString());
                }
            }
        }

        public string FileName { get; set; }

        private FileInfo()
        {
        }

        public static FileInfo FromShortUrl(string shortUrl)
        {
            if (string.IsNullOrWhiteSpace(shortUrl))
                return null;

            string[] urlParts = shortUrl.Split('/');

            if (urlParts.Length == 0)
                return null;

            int id;
            Guid guid;
            FileInfo info = new FileInfo();

            if (int.TryParse(urlParts[0], out id))
            {
                info.Id = id;
            }
            else if (Guid.TryParse(urlParts[0], out guid))
            {
                info.Id = guid;
            }

            if (info.Id != null && urlParts.Length > 1)
            {
                info.FileName = urlParts[1];
            }

            return info;
        }

        /// <summary>
        /// Разбирает ссылку на файл и извлекает из неё ИД документа и
        /// имя файла (если есть). Возвращает null, если отсутствует ИД
        ///
        /// Файл в ссылке всегда должен начинаться с идентификатора файла - числа или GUID,
        /// за которым через '/' может следовать имя файла.
        /// Пример: "files/65_мероприятие1.jpg",  "files/7323f90c-4809-4688-93e5-31c23bf7c070/document_1.pdf"
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static FileInfo FromFullUrl(string url)
        {
            const string urlPrefix = @"/files/";

            if (string.IsNullOrEmpty(url))
                return null;

            int slashIndex = url.LastIndexOf(urlPrefix, StringComparison.InvariantCultureIgnoreCase);

            if (slashIndex < 0)
                slashIndex = 0;
            else
                slashIndex += urlPrefix.Length;

            string shortUrl = url.Substring(slashIndex);

            var info = FromShortUrl(shortUrl);
            return info;
        }
    }
}