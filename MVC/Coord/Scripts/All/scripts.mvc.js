﻿//выставить у меню текущию станицу
//и проставить в ссылках параметры если нет
function UpdateMenu(menu_container, controllerName) {
    var url = document.location.href.split('?')[0];
    var selected = $(menu_container).find("a");
    
    $.each($(selected), function () {
        var current = false;

        var parent = $(this).closest("ul.submenu");

        var urlForCheck = this.href.toLowerCase();

        var condition = controllerName && controllerName != 'Public'
            ? urlForCheck.indexOf('/' + controllerName.toLowerCase()) > 0
            : urlForCheck == url.toLowerCase();
        if (condition) {

            $(this).addClass('current');
            $(this).closest('li').addClass('current');
            //$(this).removeAttr('href');
//            $(this).closest("li").addClass('current');

            current = true;

            if (parent.length > 0) {
                var parentlink = parent.siblings("a");

                parentlink.addClass('current');
                parentlink.closest('li').addClass('current');
               // parentlink.removeAttr('href');
            }
        }
    });
    
    //выстовить размер дочерним меню
    $(menu_container).find("ul.submenu").each(function () {
        var pwidth = $(this).prev().outerWidth(true);

        $(this).find("a").each(function () {

            $(this).css("min-width", pwidth);
        });
    });

    $(menu_container).find("li").each(function () {
        var submenu = $(this).find("ul.submenu");
        if (submenu.length > 0 && !submenu.hasClass('alwaysdisplay')) {

            //show subnav on hover
            $(this).mouseenter(function () {
                submenu.stop(true, true).slideDown("fast");
            });

            //hide submenus on exit
            $(this).mouseleave(function () {
                submenu.stop(true, true).slideUp("fast");
            });
        }
    });
}

function parseDate(strDate) {
    var dateParts = strDate.split(".");

    var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);

    return date.toString();
}

function AddValidationRule(el, option) {
    if (el.length > 0)
        el.rules("add", option);
}

function TooglePopupButton(enabled) {
    if (enabled)
        $('.edit_form .buttons_popup button:contains(Выбрать)').removeAttr("disabled");
    else
        $('.edit_form .buttons_popup button:contains(Выбрать)').attr("disabled", "disabled");
}