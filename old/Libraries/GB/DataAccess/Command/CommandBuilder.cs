﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess
{
    /// <summary>
    /// Стандартная реализация интерефейса ICommandBuilder
    /// </summary>
    public class CommandBuilder : ICommandBuilder
    {
        private readonly ICommandFactory _commandFactory;

        public CommandBuilder(ICommandFactory commandFactory)
        {
            this._commandFactory = commandFactory;
        }

        public void Execute<TCommandContext>(TCommandContext commandContext) where TCommandContext : ICommandContext
        {
            this._commandFactory.Create<TCommandContext>().Execute(commandContext);
        }

        public TResult Execute<TCommandContext, TResult>(TCommandContext commandContext) where TCommandContext : ICommandContext
        {
            return this._commandFactory.Create<TCommandContext, TResult>().Execute(commandContext);
        }
    }
}
