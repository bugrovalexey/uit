﻿namespace bgTeam.IsRui.Domain._Mapping
{
    using bgTeam.IsRui.Domain.Entities;
    using FluentNHibernate.Mapping;

    internal abstract class ClassMapHiLo<T> : ClassMap<T>
        where T : EntityBase
    {
        public ClassMapHiLo(string table)
        {
            Table(table);

            Id(x => x.Id, table + "_ID")
                .GeneratedBy.HiLo("NH_HiLo", "NextHi", "1", "TableKey = '" + table + "'");

            InitMap();
        }

        /// <summary>
        /// Инициализировать маппинг
        /// </summary>
        protected abstract void InitMap();
    }

    internal abstract class ClassMapHiLoNew<T> : ClassMap<T>
        where T : EntityBase
    {
        public ClassMapHiLoNew(string table)
        {
            Table(table);

            Id(x => x.Id, "Id")
                .GeneratedBy.HiLo("NH_HiLo", "NextHi", "1", "TableKey = '" + table + "'");

            InitMap();
        }

        /// <summary>
        /// Инициализировать маппинг
        /// </summary>
        protected abstract void InitMap();
    }
}
