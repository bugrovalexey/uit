﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Госконтрак (ГК)
    /// </summary>
    public class GovContractDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Реестровый номер
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Дата заключения
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Статус госконтракта
        /// </summary>
        public GovContractStatusEnum Status { get; set; }

        /// <summary>
        /// Исполнитель
        /// </summary>
        public string Executer { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Ссылка на сайт госзакупок
        /// </summary>
        public string UrlOnZakupki { get; set; }

        /// <summary>
        /// Закупка (поле состоит из реестрового номера закупки, в рамках которой заключен ГК.)
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Причины отклонения от ожидаемых результатов
        /// </summary>
        public string ReasonsCostDeviation { get; set; }

        /// <summary>
        /// Id из системы - закупки 360
        /// </summary>
        public int IdZakupki360 { get; set; }
    }
}
