﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Plans
{
    public partial class MonitoringActivities : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            grid.DataBind();
        }

        protected void grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            grid.DataSource = MonitoringRepository.GetMonitoringActivities((Master as PlanCard).CurrentPlan);
        }

        protected string GetLinkToActivityCard(object id, object name)
        {
            return string.Format("<a class='gridLink' href='../Plans/v2012new/ActivityReadOnly.aspx?ActivityId={0}&from={1}'>{2}</a>", id, 0, name);
        }
    }
}