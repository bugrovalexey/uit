﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimDeleteBaseStory<TEntity, TContext> : IStory<TContext, bool>
        where TEntity : class, IEntity
        where TContext : ClaimDeleteBaseStoryContext
    {
        protected readonly IRepositoryRui _repository;
        protected readonly ISessionNHibernateFactory _factory;

        public ClaimDeleteBaseStory(
            IRepositoryRui repositoryRui,
            ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _repository = repositoryRui;
            _factory = sessionNHibernateFactory;
        }

        public bool Execute(TContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public virtual async Task<bool> ExecuteAsync(TContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            var claim = _repository.GetOrDefault<TEntity>(context.Id);

            if (claim != null)
            {
                await DeleteReferenceAsync(claim);

                using (var session = _factory.OpenSession())
                {
                    await session.DeleteAsync<TEntity>(context.Id.Value);
                    await session.CommitAsync();
                }

                return true;
            }
            else
            {
                throw new IsRuiException($"Не найден объект id: {context.Id}");
            }
        }

        protected virtual Task DeleteReferenceAsync(TEntity entity)
        {
            return Task.Delay(0);
        }
    }
}
