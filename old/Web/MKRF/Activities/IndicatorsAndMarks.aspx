﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="EditCard.master" CodeBehind="IndicatorsAndMarks.aspx.cs" Inherits="GB.MKRF.Web.Activities.IndicatorsAndMarks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function GridSaveMarks(e) {
            hfMark.Set('Explanation', TxtExplanationMark.GetValue());
            hfMark.Set('Name', TxtNameMark.GetValue());
            hfMark.Set('Justification', TxtJustification.GetValue());
            hfMark.Set('BaseValue', TxtBaseValueMark.GetValue());
            hfMark.Set('TargetedValue', TxtTargetedValueMark.GetValue());
            hfMark.Set('UnitId', cbxUnitMark.GetValue());
            hfMark.Set('Date', deDateMark.GetValue());
        }

        function GridSaveIndicators(e) {
            hfIndicators.Set('Explanation', TxtExplanationIndicator.GetValue());
            hfIndicators.Set('Name', TxtNameIndicator.GetValue());
            hfIndicators.Set('AlgorithmOfCalculating', TxtAlgorithmOfCalculating.GetValue());
            hfIndicators.Set('BaseValue', TxtBaseValueIndicator.GetValue());
            hfIndicators.Set('TargetedValue', TxtTargetedValueIndicator.GetValue());
            hfIndicators.Set('UnitId', cbxUnitIndicator.GetValue());
            hfIndicators.Set('Date', deDateIndicator.GetValue());
        }

    </script>

    <dxhf:ASPxHiddenField runat="server" ID="hfMark" ClientInstanceName="hfMark" />
    <dxhf:ASPxHiddenField runat="server" ID="hfIndicators" ClientInstanceName="hfIndicators" />

    <p class="header_grid">
        Показатели 
   
   
    </p>

    <dxe:ASPxButton ID="bntAddMark" runat="server" ClientInstanceName="bntAddMark" CssClass="fleft topButton"
        AutoPostBack="false" Text="Добавить">
        <ClientSideEvents Click="function(s,e) {gridMarks.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="gridMarks" Width="100%" ClientInstanceName="gridMarks"
        KeyFieldName="Id" ViewStateMode="Disabled" AutoGenerateColumns="False"
        OnBeforePerformDataSelect="gridMarks_OnBeforePerformDataSelect"
        OnRowInserting="gridMarks_OnRowInserting"
        OnRowUpdating="gridMarks_OnRowUpdating"
        OnStartRowEditing="gridMarks_OnStartRowEditing"
        OnRowDeleting="gridMarks_OnRowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Пояснение связи с показателем проекта" FieldName="ExplanationRelationshipWithProjectMark" />
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" />
            <dxwgv:GridViewDataTextColumn Caption="Обоснование" FieldName="Justification" />
            <dxwgv:GridViewDataTextColumn Caption="Базовое значение" FieldName="BaseValue" />
            <dxwgv:GridViewDataTextColumn Caption="Целевое значение" FieldName="TargetedValue" />
            <dxwgv:GridViewDataTextColumn Caption="Единица измерения" FieldName="Unit.Name" />
            <dxwgv:GridViewDataTextColumn Caption="Дата" FieldName="Date">
                <PropertiesTextEdit DisplayFormatString="dd.MM.yyyy" />
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Пояснение связи с показателем проекта:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="TxtExplanationMark" ClientInstanceName="TxtExplanationMark"
                                runat="server" OnDataBinding="TxtExplanationMark_OnDataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Наименование:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="TxtNameMark" ClientInstanceName="TxtNameMark"
                                runat="server" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                OnDataBinding="TxtNameMark_OnDataBinding">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Обоснование:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="TxtJustification" ClientInstanceName="TxtJustification"
                                runat="server" OnDataBinding="TxtJustification_OnDataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Базовое значение:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="TxtBaseValueMark" ClientInstanceName="TxtBaseValueMark"
                                runat="server" OnDataBinding="TxtBaseValueMark_OnDataBinding"  MaskSettings-Mask="<0..999999999>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Целевое значение:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="TxtTargetedValueMark" ClientInstanceName="TxtTargetedValueMark"
                                runat="server" OnDataBinding="TxtTargetedValueMark_OnDataBinding"   MaskSettings-Mask="<0..999999999>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Единица измерения:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="cbxUnitMark" runat="server" ClientInstanceName="cbxUnitMark"
                                TextField="Name" ValueField="Id" ValueType="System.Int32"
                                OnDataBinding="cbxUnitMark_OnDataBinding" EnableSynchronization="False" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Дата:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="deDateMark" ClientInstanceName="deDateMark"
                                OnDataBinding="deDateMark_OnDataBinding">
                            </dxe:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents
            BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') {GridSaveMarks(e);} }" />
    </dxwgv:ASPxGridView>


    <p class="header_grid">
        Индикаторы 
   
   
    </p>

    <dxe:ASPxButton ID="BtnAddIndicator" runat="server" ClientInstanceName="BtnAddIndicator" CssClass="fleft topButton"
        AutoPostBack="false" Text="Добавить">
        <ClientSideEvents Click="function(s,e) {gridIndicators.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="gridIndicators" Width="100%" ClientInstanceName="gridIndicators"
        KeyFieldName="Id" ViewStateMode="Disabled" AutoGenerateColumns="False"
        OnBeforePerformDataSelect="gridIndicators_OnBeforePerformDataSelect"
        OnRowInserting="gridIndicators_OnRowInserting"
        OnRowUpdating="gridIndicators_OnRowUpdating"
        OnStartRowEditing="gridIndicators_OnStartRowEditing"
        OnRowDeleting="gridIndicators_OnRowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Пояснение связи с показателем проекта" FieldName="ExplanationRelationshipWithProjectMark" />
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" />
            <dxwgv:GridViewDataTextColumn Caption="Алгоритм расчета" FieldName="AlgorithmOfCalculating" />
            <dxwgv:GridViewDataTextColumn Caption="Базовое значение" FieldName="BaseValue" />
            <dxwgv:GridViewDataTextColumn Caption="Целевое значение" FieldName="TargetedValue" />
            <dxwgv:GridViewDataTextColumn Caption="Единица измерения" FieldName="Unit.Name" />
            <dxwgv:GridViewDataTextColumn Caption="Дата" FieldName="Date">
                <PropertiesTextEdit DisplayFormatString="dd.MM.yyyy" />
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Пояснение связи с показателем проекта:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="TxtExplanationIndicator" ClientInstanceName="TxtExplanationIndicator"
                                runat="server" OnDataBinding="TxtExplanationIndicator_OnDataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Наименование:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="TxtNameIndicator" ClientInstanceName="TxtNameIndicator"
                                runat="server" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                OnDataBinding="TxtNameIndicator_OnDataBinding">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Алгоритм расчета:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="TxtAlgorithmOfCalculating" ClientInstanceName="TxtAlgorithmOfCalculating"
                                runat="server" OnDataBinding="TxtAlgorithmOfCalculating_OnDataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Базовое значение:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="TxtBaseValueIndicator" ClientInstanceName="TxtBaseValueIndicator"
                                runat="server" OnDataBinding="TxtBaseValueIndicator_OnDataBinding"  MaskSettings-Mask="<0..999999999>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Целевое значение:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="TxtTargetedValueIndicator" ClientInstanceName="TxtTargetedValueIndicator"
                                runat="server" OnDataBinding="TxtTargetedValueIndicator_OnDataBinding"  MaskSettings-Mask="<0..999999999>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Единица измерения:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="cbxUnitIndicator" runat="server" ClientInstanceName="cbxUnitIndicator"
                                TextField="Name" ValueField="Id" ValueType="System.Int32"
                                OnDataBinding="cbxUnitIndicator_OnDataBinding" EnableSynchronization="False" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Дата:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="deDateIndicator" ClientInstanceName="deDateIndicator"
                                OnDataBinding="deDateIndicator_OnDataBinding">
                            </dxe:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents
            BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') {GridSaveIndicators(e);} }" />
    </dxwgv:ASPxGridView>

</asp:Content>
