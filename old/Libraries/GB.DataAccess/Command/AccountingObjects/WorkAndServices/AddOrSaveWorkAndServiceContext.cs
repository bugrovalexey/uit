﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.WorkAndServices
{
    public class AddOrSaveWorkAndServiceContext : ICommandContext
    {
        public int? xId { get; set; }

        public string Name { get; set; }
        
        public int? AccountingObjectId { get; set; }
        
        public int? CategoryKindOfSupportId { get; set; }
        
        public string InformationLeasedInfrastructure { get; set; }
        
        public int? OkvedId { get; set; }
        
        public decimal Summa { get; set; }
    }
}
