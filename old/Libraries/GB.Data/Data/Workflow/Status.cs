﻿namespace GB.Data.Workflow
{
    /// <summary>Статус</summary>
    public class Status : EntityBase, IEntityName
    {
        /// <summary>Группа</summary>
        public virtual StatusGroup StatusGroup { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null

        /// <summary>Название для кнопок</summary>
        public virtual string ActionName { get; set; }//null

        /// <summary>Стили для кнопок</summary>
        public virtual string Css { get; set; }//null

        /// <summary>Номер статуса</summary>
        //public virtual int? Num { get; set; }//null

        /// <summary>Дней действия статуса</summary>
        //public virtual int? DaysCount { get; set; }//null
    }
}