﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class IndicatorsCreateOrUpdateIndicatorStory : IStory<IndicatorsCreateOrUpdateIndicatorStoryContext, ActivityIndicatorDto>
    {
        private readonly IMapperBase _mapper;
        private readonly ISessionNHibernateFactory _session;
        private readonly IRepositoryRui _repository;

        public IndicatorsCreateOrUpdateIndicatorStory(IMapperBase mapper, ISessionNHibernateFactory factory, IRepositoryRui repository)
        {
            _mapper = mapper;
            _session = factory;
            _repository = repository;
        }

        public ActivityIndicatorDto Execute(IndicatorsCreateOrUpdateIndicatorStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityIndicatorDto> ExecuteAsync(IndicatorsCreateOrUpdateIndicatorStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            using (var session = _session.OpenSession())
            {
                PlansActivityIndicator indicator;

                if (context.Id.HasValue)
                {
                    indicator = _repository.Get<PlansActivityIndicator>(context.Id);
                }
                else
                {
                    indicator = new PlansActivityIndicator();

                    indicator.PlansActivity = _repository.Get<PlansActivity>(context.ActivityId);
                }

                _mapper.Map(context, indicator);

                indicator.Unit = _repository.Get<Units>(context.UnitId);

                await session.SaveOrUpdateAsync(indicator);

                return _mapper.Map(indicator, new ActivityIndicatorDto());
            }
        }
    }
}
