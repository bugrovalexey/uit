﻿
using GB;
using GB.Entity.Directories.FPGU;

namespace GB.MKRF.Mapping.FPGU
{
    public class NPA_DocumentMap : ClassMapBase<NPA_Document>
    {
        public NPA_DocumentMap()
            : base("NPA_Document")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Sn, "NPA_Document_SN");
            Map(x => x.ApprovalDate, "NPA_Document_Approval_Date");
            Map(x => x.Name, "NPA_Document_Name");
            References<Gov_Service>(x => x.Gov_Service, "Gov_Service_ID").Not.Nullable();
        }
    }
}