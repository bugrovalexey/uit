using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.DataAccess.Impl;
using bgTeam.IsRui.DataAccess.Repository;
//using bgTeam.IsRui.MKRF.DictyonaryEdit;
using System;
using Xunit;

namespace Test.bgTeam.IsRui.Unit
{
    public class TestsRepository
    {
        [Fact]
        public void Test_User()
        {
            var rep = Create_UserRepository();

            var list = rep.GetAll();
        }

        [Fact]
        public void Test_DictionaryRepository()
        {
            var rep = Create_UserRepository();

           // var dr = new DictionaryRepository(rep);

           // var res = DictionaryRepository.GetDataDictionary(1);
        }

        public IUserRepository Create_UserRepository()
        {
            var factory = Create_SessionNHibernateFactory();

            return new UserRepository(factory, null);
        }

        public ISessionNHibernateFactory Create_SessionNHibernateFactory()
        {
            var conf = Create_SessionNHibernateConfiguration();
            var conStr = "Server=uir-dev.database.windows.net;Initial Catalog=uirdb;Persist Security Info=False;User ID=bgteam;Password=JhonDoe1983Werewolf1@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            return new SessionNHibernateFactory(conf, conStr);
        }

        public ISessionNHibernateConfiguration Create_SessionNHibernateConfiguration()
        {
            return new SessionNHibernateConfigurationMsSql();
        }


    }
}
