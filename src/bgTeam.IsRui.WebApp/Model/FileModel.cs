﻿namespace bgTeam.IsRui.WebApp.Model
{
    public class FileModel
    {
        public string Guid { get; set; }

        public string FileName { get; set; }
    }
}
