﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class PlansActGTMap : ClassMapBase<PlansActGT>
    {
        public PlansActGTMap()
            : base("Plans_Act_GT")
		{
//            Table("w_");

//            Id(x => x.Id, "Plans_Act_GT_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
            
//            SqlInsert(@"exec ZZZ_Prc_Plans_Act_GT_Insert 
//                        @Plans_Act_GT_Goal = ?
//                       ,@Plans_Act_GT_Task = ?
//                       ,@Plans_Act_GT_Priority = ?
//                       ,@Plans_Act_GT_Props_Norm_Act = ?
//                       ,@Plans_Act_GT_Functionality = ?
//                       ,@Plans_Act_GT_Govt_Props_Norm_Act = ?
//					   ,@Plans_Activity_Direction_ID = ?
//					   ,@Plans_Activity_ID = ?
//					   ,@Func_Type_ID = ?
//                       ,@Plans_Act_GT_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Plans_Act_GT_Update 
//                        @Plans_Act_GT_Goal = ?
//                       ,@Plans_Act_GT_Task = ?
//                       ,@Plans_Act_GT_Priority = ?
//                       ,@Plans_Act_GT_Props_Norm_Act = ?
//                       ,@Plans_Act_GT_Functionality = ?
//                       ,@Plans_Act_GT_Govt_Props_Norm_Act = ?
//					   ,@Plans_Activity_Direction_ID = ?
//					   ,@Plans_Activity_ID = ?
//					   ,@Func_Type_ID = ?
//                       ,@Plans_Act_GT_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Plans_Act_GT_Delete 
//						@Plans_Act_GT_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Goal, "Plans_Act_GT_Goal");
			Map(x => x.Task, "Plans_Act_GT_Task");
			Map(x => x.Priority, "Plans_Act_GT_Priority");
			Map(x => x.PropsNormAct, "Plans_Act_GT_Props_Norm_Act");
			Map(x => x.Functionality, "Plans_Act_GT_Functionality");
			Map(x => x.GovtPropsNormAct, "Plans_Act_GT_Govt_Props_Norm_Act");

			References<PlansActivityDirection>(x => x.PlanActivityDirection, "Plans_Activity_Direction_ID").Not.Nullable();
			References<PlansActivity>(x => x.PlanActivity, "Plans_Activity_ID").Not.Nullable();
			References<FuncType>(x => x.FuncType, "Func_Type_ID").Not.Nullable();
        }
    }
}