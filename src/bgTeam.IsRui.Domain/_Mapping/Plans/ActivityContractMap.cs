﻿namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Plans;

    internal class ActivityContractMap : ClassMapHiLo<ActivityContract>
    {
        public ActivityContractMap()
            : base("Plans_Activity_Contract")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "PA_Contract_Name").Nullable();
            Map(x => x.Number, "PA_Contract_Number").Nullable();
            Map(x => x.BeginDate, "PA_Contract_BeginDate").Nullable();
            Map(x => x.EndDate, "PA_Contract_EndDate").Nullable();
            Map(x => x.ChangeDate, "PA_Contract_ChangeDate").Nullable();

            References(x => x.PlansActivity, "Plans_Activity_ID").Not.Nullable();
        }
    }
}
