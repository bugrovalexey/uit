﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Mapping.Admin
{
    public class LogMessageMap : ClassMapBase<LogMessage>
    {
        public LogMessageMap()
            : base("Logs", true)
        {
            //Table("w_");

            //Id(x => x.Id, "Id").GeneratedBy.Assigned(); ;

            

            //SqlInsert(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlUpdate(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlDelete(@"raiserror ('Unsupported',10,1)").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Date, "Date").CustomType("DateTime2");
            Map(x => x.Username, "Username");
            Map(x => x.Identity, "[Identity]");
            Map(x => x.Thread, "Thread");
            Map(x => x.Level, "Level");
            Map(x => x.Message, "Message");
            Map(x => x.Exception, "Exception");
        }
    }
}
