﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Threading.Tasks;
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Directories;

    public class AoCharacteristicsAddOrUpdateCharacteristicStory : IStory<AoCharacteristicsAddOrUpdateCharacteristicStoryContext, CharacteristicsAddOrUpdateDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;

        public AoCharacteristicsAddOrUpdateCharacteristicStory(IMapperBase mapper, IRepositoryRui repository, ISessionNHibernateFactory factory)
        {
            _mapper = mapper;
            _repository = repository;
            _factory = factory;
        }

        public CharacteristicsAddOrUpdateDto Execute(AoCharacteristicsAddOrUpdateCharacteristicStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<CharacteristicsAddOrUpdateDto> ExecuteAsync(AoCharacteristicsAddOrUpdateCharacteristicStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));
            context.Name.CheckNull(nameof(context.Name));
            context.TypeId.CheckNull(nameof(context.TypeId));
            context.UnitId.CheckNull(nameof(context.UnitId));
            context.DateBeginning.CheckNull(nameof(context.DateBeginning));

            AccountingObjectCharacteristics character;

            using (var session = _factory.OpenSession())
            {
                try
                {

                    if (context.Id.HasValue)
                    {
                        character = _repository.Get<AccountingObjectCharacteristics>(context.Id);
                        _mapper.Map(context, character);

                        await session.UpdateAsync(character);
                    }
                    else
                    {
                        character = new AccountingObjectCharacteristics();
                        _mapper.Map(context, character);

                        character.Type = _repository.Get<AccountingObjectCharacteristicsType>(context.TypeId);
                        character.Unit = _repository.Get<Units>(context.UnitId);
                        character.AccountingObject = _repository.Get<AccountingObject>(context.AoId);

                        await session.InsertAsync(character);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return _mapper.Map(character, new CharacteristicsAddOrUpdateDto());
        }
    }
}
