﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class APFActivityMap: ClassMapBase<APFActivity>
    {

        public APFActivityMap():
            base("APF_Activity")
        {

        }

        protected override void InitMap()
        {
            References<ApfInfo>(x => x.ApfInfo, "APF_Info_ID").Not.Nullable();
            References<PlansActivity>(x => x.PlansActivity, "Plans_Activity_ID").Not.Nullable();
        }
    }
}
