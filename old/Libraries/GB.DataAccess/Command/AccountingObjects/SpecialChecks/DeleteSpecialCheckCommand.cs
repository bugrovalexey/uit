﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.SpecialChecks
{
    public class DeleteSpecialCheckCommand : ICommand<DeleteSpecialCheckContext>
    {
        ISessionFactoryGb factory;

        public DeleteSpecialCheckCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteSpecialCheckContext context)
        {            
            if (context.xId.HasValue && context.xId != 0)
            {
                var entity = new Repository<SpecialCheck>().Get(context.xId.Value);

                if (entity != null)
                {
                    using (var session = factory.Create())
                    {                        
                        session.Delete(entity);
                        session.Commit();
                    }
                }
            }
        }
    }
}
