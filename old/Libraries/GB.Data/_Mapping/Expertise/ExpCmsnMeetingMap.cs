﻿using Uviri.MKRF.Entities.Expertise;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpCmsnMeetingMap : ClassMapBase<ExpCmsnMeeting>
	{
        public ExpCmsnMeetingMap()
            : base("Exp_Cmsn_Meeting")
		{
//            Table("w_");

//            Id(x => x.Id, "Exp_Cmsn_Meeting_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_Exp_Cmsn_Meeting_Insert
//						  @Exp_Cmsn_Meeting_Num = ?
//						 ,@Exp_Cmsn_Meeting_Date = ?
//						 ,@Exp_Cmsn_Meeting_Name = ?
//						 ,@Exp_Cmsn_Meeting_ID = ?").Check.None();
			
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Cmsn_Meeting_Update
//						  @Exp_Cmsn_Meeting_Num = ?
//						 ,@Exp_Cmsn_Meeting_Date = ?
//						 ,@Exp_Cmsn_Meeting_Name = ?
//						 ,@Exp_Cmsn_Meeting_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Cmsn_Meeting_Delete
//						  @Exp_Cmsn_Meeting_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Num, "Exp_Cmsn_Meeting_Num");
			Map(x => x.Date, "Exp_Cmsn_Meeting_Date");
			Map(x => x.Name, "Exp_Cmsn_Meeting_Name");
        }
    }
}