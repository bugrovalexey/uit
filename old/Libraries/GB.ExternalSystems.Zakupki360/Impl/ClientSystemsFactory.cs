﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace GB.ExternalSystems.Zakupki360
{
    public class ClientSystemsFactory
    {
        public IClientSystems Create()
        {
            var host = ConfigurationManager.AppSettings["Zakupki360.Host"];
            var key = ConfigurationManager.AppSettings["Zakupki360.Key"];

            return new ClientSystems(host, key);
        }
    }
}
