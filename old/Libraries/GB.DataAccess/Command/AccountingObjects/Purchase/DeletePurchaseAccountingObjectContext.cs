﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.Purchase
{
    public class DeletePurchaseAccountingObjectContext : ICommandContext
    {
        public int Id { get; set; }
    }
}
