﻿using GB.Data.Common;

//using Core.Membership;

namespace GB.MKRF.Repository
{
    public class DepartmentRepository : Repository<Department>
    {
        //        public static Department Get(int id)
        //        {
        //            // чтобы применился фильтр, вместо Get нужно использовать ICriteria
        //            IList<Department> depList = RepositoryBase<Department>.GetAll(x => x.Id == id);
        //            return (depList != null && depList.Count == 1) ? depList[0] : null;
        //        }

        //        /// <summary>Получить все ведомства без учета фильтра</summary>
        //        public static IList<Department> GetAllWithoutSecurityFilter()
        //        {
        //            SecurityFilter.DisableFilter(Session);
        //            IList<Department> result = RepositoryBase<Department>.GetAll().OrderBy(x=>x.Id).ToList();
        //            SecurityFilter.EnableFilter(Session, UserRepository.GetCurrent().Id);
        //            return result;
        //        }

        //        /// <summary>Получить ведомство без учета фильтра</summary>
        //        public static Department GetWithoutSecurityFilter(int id)
        //        {
        //            SecurityFilter.DisableFilter(Session);
        //            Department result = RepositoryBase<Department>.Get(id);
        //            SecurityFilter.EnableFilter(Session, UserRepository.CurrentUserId);
        //            return result;
        //        }

        //        internal static HashSet<int> GetSubdepartments()
        //        {
        //            HashSet<int> set = new HashSet<int>();
        //            string sql = String.Format(@"select  Govt_Organ_ID
        //                            from    dbo.v_Users_Govt_Organ2
        //                            where   Users_Login = '{0}'
        //                            and Is_My_Govt_Organ in (0,1)", UserRepository.GetCurrent().Login);

        //            DataTable table = RepositoryBase<Department>.GetTableViaSQL(sql);
        //            if (table != null && table.Rows.Count > 0)
        //            {
        //                foreach (DataRow row in table.Rows)
        //                    set.Add((int)row[0]);
        //            }

        //            return set;
        //        }

        //        internal static HashSet<int> GetSubdepartments(int depId, bool onlyActive = false)
        //        {
        //            HashSet<int> set = new HashSet<int>();
        //            string sql = String.Format(@"select  Govt_Organ_ID
        //            from    dbo.{1}({0}, -1)
        //            where   Govt_Organ_ID != {0}", depId, !onlyActive ? "v_Govt_Organ_Tree_By_GO2" : "v_Govt_Organ_Tree_By_GO");

        //            DataTable table = RepositoryBase<Department>.GetTableViaSQL(sql);
        //            if (table != null && table.Rows.Count > 0)
        //            {
        //                foreach (DataRow row in table.Rows)
        //                    set.Add((int)row[0]);
        //            }

        //            return set;
        //        }

        //        /// <summary>
        //        /// Получить список доступных пользователю ведомств
        //        /// </summary>
        //        public static IList<Department> GetAvailableDepatmentsForUser()
        //        {
        //            HashSet<int> subdepartments = DepartmentRepository.GetSubdepartments();

        //            IList<Department> deps = (from d in DepartmentRepository.GetAll()
        //                                      where subdepartments.Contains(d.Id)  select d).ToList();
        //            return deps;
        //        }

        //        /// <summary>
        //        /// Получить список родителей для ведомства
        //        /// </summary>
        //        public static IList<Department> GetAllParentsForDepartment(Department department)
        //        {
        //            List<Department> parents = new List<Department>();

        //            while (department.Parent != null)
        //            {
        //                parents.Add(department.Parent);
        //                department = department.Parent;
        //            }

        //            return parents;
        //        }

        //        /// <summary>
        //        /// Получить список детей для ведомства
        //        /// </summary>
        //        public static IList<Department> GetAllChildren(Department department, DepartmentTypeEnum type, bool onlyActive = false)
        //        {
        //            List<Department> children = new List<Department>();

        //            foreach (int id in GetSubdepartments(department.Id, onlyActive))
        //            {
        //                children.Add(RepositoryBase<Department>.Get(id));
        //            }
        //            children.Add(department);

        //            if (type != DepartmentTypeEnum.All && type != DepartmentTypeEnum.Self && type != DepartmentTypeEnum.Invited)
        //                children = children.Where(c => c.Type.Id == (int)type).ToList();

        //            return children;
        //        }

        //        /// <summary>
        //        /// Получить список Id детей для ведомства
        //        /// </summary>
        //        public static List<int> GetAllChildrenIds(Department department)
        //        {
        //            List<int> childrenIds = new List<int>();
        //            childrenIds.Add(department.Id);
        //            childrenIds.AddRange(GetSubdepartments(department.Id).ToList());
        //            return childrenIds;
        //        }

        //        //TODO : Надо переделать т.к. идут подвызовы
        //        /// <summary>
        //        /// Получить список Id и Name детей для ведомства
        //        /// </summary>
        //        public static DataTable GetAllChildrenIdName(Department department, DepartmentTypeEnum type, bool onlyActive = false)
        //        {
        //            DataTable t = new DataTable();
        //            t.Columns.Add("Id", typeof(int));
        //            t.Columns.Add("Name", typeof(string));

        //            var sql = string.Format(
        //@"
        //SELECT govt_organ_id                             AS 'Id',
        //       isnull(govt_organ_code, '') + ' - ' + govt_organ_name AS 'Name'
        //FROM   govt_organ
        //WHERE  govt_organ_id IN (SELECT govt_organ_id
        //                         FROM   {0}(@depId, -1)
        //                         WHERE  govt_organ_id != @depId)
        //{1}
        //UNION
        //SELECT govt_organ_id,
        //       isnull(govt_organ_code, '') + ' - ' + govt_organ_name
        //FROM   govt_organ
        //WHERE  govt_organ_id = @depId",
        //            !onlyActive ? "v_Govt_Organ_Tree_By_GO2" : "v_Govt_Organ_Tree_By_GO",
        //            (type != DepartmentTypeEnum.All
        //                && type != DepartmentTypeEnum.Self
        //                && type != DepartmentTypeEnum.Invited) ? "AND Govt_Organ_Type_ID = @depTypeId" : string.Empty);

        //            var pars = new Dictionary<string, object>();
        //            pars.Add("@depId", department != null ? department.Id : -1);
        //            pars.Add("@depTypeId", (int)type);

        //            t = GetTableViaSQL(sql, pars);

        //            return t;
        //        }

        //        /// <summary>
        //        /// Получить количество детей определенного типа
        //        /// </summary>
        //        public static int GetChildrenCountByDepartmentType(Department department, DepartmentTypeEnum depType)
        //        {
        //            string sql = @"select  count(*)
        //                            from    v_Govt_Organ_Tree_By_GO2(@Govt_Organ_ID, @Govt_Organ_Type_ID)";
        //                            //where   Govt_Organ_ID != @Govt_Organ_ID";

        //            Dictionary<string, object> pars = new Dictionary<string, object>();
        //            pars.Add("@Govt_Organ_ID", department.Id);
        //            pars.Add("@Govt_Organ_Type_ID", (int)depType);

        //            DataTable table = RepositoryBase<EntityBase>.GetTableViaSQL(sql, pars);

        //            return Convert.ToInt32(table.Rows[0][0]);
        //        }

        //        /// <summary>
        //        /// Возвращает список исполнителей по ведомству
        //        /// </summary>
        //        /// <param name="department">Ведомство</param>
        //        /// <returns></returns>
        //        //public static IList<User> GetExecutors(Department department)
        //        //{
        //        //    IList<Executor> executors = RepositoryBase<Executor>.GetAll(x => x.Parent == department);

        //        //    IList<Department> subDepartments = (from e in executors select e.Child).ToList<Department>();
        //        //    subDepartments.Insert(0, department);

        //        //    IList<User> users = Session.QueryOver<User>()
        //        //        .Where(Restrictions.On<User>(o => o.Department).IsInG<Department>(subDepartments))
        //        //        .Fetch(z => z.Department).Eager
        //        //        .List<User>();

        //        //    return users;
        //        //}

        //        public static XDocument GetDepartmentsXML()
        //        {
        //            XElement departmentList = new XElement("Departments");

        //            XDocument xdoc = new XDocument(new XElement("Root", departmentList));

        //            IList<Department> list = GetAllWithoutSecurityFilter();

        //            foreach (Department d in list)
        //            {
        //                departmentList.Add(
        //                    new XElement("Department",
        //                        new XElement("Id", d.Id),
        //                        new XElement("Address", d.Addr),
        //                        new XElement("Code", d.Code),
        //                        new XElement("IsActive", d.IsActive),
        //                        new XElement("Name", d.Name),
        //                        new XElement("ParentId", d.Parent != null ? (int?)d.Parent.Id : null),
        //                        new XElement("Phone", d.Phone),
        //                        new XElement("ShortName", d.NameSmall),
        //                        new XElement("Type", d.Type != null ? d.Type.Name : null),
        //                        new XElement("Web", d.Web)));

        //            }

        //            return xdoc;
        //        }

        //        public static DataTable GetAllWithPlan()
        //        {
        //            string sql =
        //@"SELECT DISTINCT g.Govt_Organ_ID AS Id,
        //                g.Govt_Organ_Name AS Name
        //FROM dbo.w_Plans p
        //LEFT JOIN w_Govt_Organ g ON g.Govt_Organ_ID = p.Govt_Organ_id
        //ORDER BY g.Govt_Organ_Name";

        //            return RepositoryBase.GetTableViaSQL(sql);
        //        }
    }
}