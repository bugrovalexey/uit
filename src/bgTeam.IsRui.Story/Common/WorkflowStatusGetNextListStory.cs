﻿namespace bgTeam.IsRui.Story.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Workflow;

    public class WorkflowStatusGetNextListStory : IStory<WorkflowStatusGetNextListStoryContext, IEnumerable<StatusDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;

        public WorkflowStatusGetNextListStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IUserRepository userRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _userRepository = userRepository;
        }

        public IEnumerable<StatusDto> Execute(WorkflowStatusGetNextListStoryContext context)
        {
            return ExecuteAsync(context).GetAwaiter().GetResult();
        }

        public async Task<IEnumerable<StatusDto>> ExecuteAsync(WorkflowStatusGetNextListStoryContext context)
        {
            context.WorkflowId.CheckNull(nameof(context.WorkflowId));

            var res = _repository.GetExisting<Workflow>(context.WorkflowId);

            var roles = _userRepository.GetCurrent().Roles.ToArray();

            var list = _repository.GetAllEx<StatusList>(q => q
                //.WhereRestrictionOn(x => x.From).IsIn(statuses)
                .Where(x => x.From == res.Status)
                .WhereRestrictionOn(x => x.Role).IsIn(roles)
                .JoinQueryOver(x => x.To));

            return list.Select(x => _mapper.Map(x.To, new StatusDto()));
        }
    }
}
