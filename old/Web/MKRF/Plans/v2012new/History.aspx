﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/PlanCard.master" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.History" %>

<%@ Register src="~/Common/GridColumnManager.ascx" tagname="GridColumnManager" tagprefix="gcm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">

    <div class="page">

    
    <dxwgv:ASPxGridView runat="server" ID="HistoryGrid" 
        AutoGenerateColumns="False"  ClientInstanceName="HistoryGrid"
        Width="100%" EnableRowsCache="False" EnableViewState="False"
        onbeforeperformdataselect="HistoryGrid_BeforePerformDataSelect">
        <Columns>
            <dxwgv:GridViewDataTextColumn Caption="Номер" VisibleIndex="5" FieldName="Num" >
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Версия" VisibleIndex="10" Width="85px" 
                FieldName="Date_Time" SortIndex="0" SortOrder="Descending">
                <Settings AllowSort="True" SortMode="Value" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="ФИО ответственного за формирование плана" 
                VisibleIndex="60" FieldName="User_Full_Info">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Cтатус назначения" VisibleIndex="70" FieldName="Status_Name">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption=" " VisibleIndex="90" 
                ShowInCustomizationForm="False">
                <Settings AllowDragDrop="False" />
                <DataItemTemplate>
                    <%#getHistoryActionLink((DateTime)Eval("DT"))%>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
        <Styles>
            <Header Wrap="True" HorizontalAlign="Center">
            </Header>
        </Styles>
    </dxwgv:ASPxGridView>
    </div>
</asp:Content>
