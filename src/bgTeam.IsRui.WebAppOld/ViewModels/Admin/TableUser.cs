﻿using bgTeam.Extensions;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.WebApp.Helpers;
using System.ComponentModel;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    public class TableUser : NotificationBaseViewModel
    {
        private const string LoginCaption = "Логин";
        private const string RolesCaption = "Роли";
        private const string FullNameCaption = "ФИО";
        private const string DepartmentNameCaption = "Учреждение";
        private const string WorkPhoneCaption = "Рабочий телефон";
        private const string MobPhoneCaption = "Мобильный телефон";
        private const string IsEsiaCaption = "ЕСИА";
        private const string IsActiveCaption = "Активный";

        public TableUser(User user)
        {
            Id = user.Id;
            Login = user.Login;
            FullName = user.FullName;
            Roles = user.Roles.Return(roles => string.Join(", ", roles.Select(r => r.Name)), default(string));
            DepartmentName = user.Department.Return(dep => dep.Name, default(string));
            MobPhone = user.MobPhone;
            WorkPhone = user.WorkPhone;
            IsEsia = user.Snils.Return(shils => shils.Length == 14, false);
            IsActive = user.IsActive;
        }

        public int Id { get; set; }

        [DisplayName(LoginCaption)]
        public string Login { get; set; }

        [DisplayName(RolesCaption)]
        public string Roles { get; set; }

        [DisplayName(FullNameCaption)]
        public string FullName { get; set; }

        [DisplayName(DepartmentNameCaption)]
        public string DepartmentName { get; set; }

        [DisplayName(WorkPhoneCaption)]
        public string WorkPhone { get; set; }

        [DisplayName(MobPhoneCaption)]
        public string MobPhone { get; set; }

        [DisplayName(IsEsiaCaption)]
        public bool IsEsia { get; set; }

        [DisplayName(IsActiveCaption)]
        public bool IsActive { get; set; }

        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("DeleteUser", "Admin", new { id = Id }, "Grid"); }
        }
    }
}