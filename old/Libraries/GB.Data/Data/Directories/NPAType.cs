﻿namespace GB.MKRF.Entities.Directories
{
    /// <summary>Тип НПА</summary>
    public class NPAType : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }
    }
}