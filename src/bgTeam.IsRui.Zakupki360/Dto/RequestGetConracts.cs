﻿namespace bgTeam.IsRui.Zakupki360.Dto
{
    class RequestGetConracts
    {
        public SearchParametersContracts Filter { get; set; }

        public string ApiKey { get; set; }
    }
}
