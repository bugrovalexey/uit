﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Mapping.Admin
{
    public class UsersAnswerMap : ClassMapBase<UsersAnswer>
	{
        public UsersAnswerMap()
            : base("Results")
		{
//            Table("w_");
            
//            Id(x => x.Id, "Results_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string argsList =
//                    @"@Results_Text = ?
//                    ,@Quest_User_List_ID = ?
//                    ,@Answers_ID = ?
//                    ,@Results_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Results_Insert " + argsList).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Results_Update " + argsList).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Results_Delete @Results_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Text, "Results_Text");
            Map(x => x.Date, "Results_Create_DT").ReadOnly();

            References<UserQuestionnaire>(x=>x.UserQuestionnaire, "Quest_User_List_ID").Not.Nullable();
            References<QuestionAnswer>(x=>x.Answer, "Answers_ID").Not.Nullable();
        }
    }
}
