﻿using bgTeam.IsRui.Domain.Entities.Common;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.Profile
{
    [MetadataType(typeof(ProfileViewModel_Validation))]
    public class UserCardViewModel
    {
        #region const

        public const string LoginCaption = "Логин";
        public const string RoleCaption = "Роли";
        public const string PasswordCaption = "Пароль";
        public const string NameCaption = "Имя";
        public const string MiddlenameCaption = "Отчество";
        public const string SurnameCaption = "Фамилия";
        public const string JobCaption = "Должность";
        public const string EmailCaption = "Электронная почта";
        public const string FaxCaption = "Факс";
        public const string MobPhoneCaption = "Мобильный телефон";
        public const string WorkPhoneCaption = "Рабочий телефон";

        #endregion const

        #region props

        public int Id { get; set; }

        [DisplayName(LoginCaption)]
        public string Login { get; set; }

        [DisplayName(RoleCaption)]
        public string Role { get; set; }

        [DisplayName(PasswordCaption)]
        public string Password { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(MiddlenameCaption)]
        public string Middlename { get; set; }

        [DisplayName(SurnameCaption)]
        public string Surname { get; set; }

        [DisplayName(JobCaption)]
        public string Job { get; set; }

        [DisplayName(EmailCaption)]
        public string Email { get; set; }

        [DisplayName(FaxCaption)]
        public string Fax { get; set; }

        [DisplayName(MobPhoneCaption)]
        public string MobPhone { get; set; }

        [DisplayName(WorkPhoneCaption)]
        public string WorkPhone { get; set; }

        #endregion props

        public UserCardViewModel()
        {
        }

        public UserCardViewModel(User user)
        {
            Id = user.Id;
            Login = user.Login;
            Role = string.Join(",\r\n", user.Roles.Select(x => x.Name));
            Password = user.Password;
            Name = user.Name;
            Middlename = user.Middlename;
            Surname = user.Surname;
            Job = user.Job;
            Email = user.Email;
            Fax = user.Fax;
            MobPhone = user.MobPhone;
            WorkPhone = user.WorkPhone;
        }
    }

    //public class UserCardArgs //: IPlansActivityArgs, IAccountingObjectControllerArgs
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public string Description { get; set; }
    //    public string ResponsibleDepartment { get; set; }
    //    public int ResponsibleInformation { get; set; }
    //    public int ResponsibleTechnical { get; set; }
    //    public int ResponsibleFinancial { get; set; }
    //    public int Signing { get; set; }
    //    public int AccountingObject { get; set; }
    //    public string Logo { get; set; }
    //    public string Files { get; set; }

    //    public int AO { get; set; }
    //    public string AOName { get; set; }
    //    public string AOShortName { get; set; }
    //    public int AOType { get; set; }
    //    public int AOKind { get; set; }
    //}

    public class ProfileViewModel_Validation : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string Surname { get; set; }
    }
}