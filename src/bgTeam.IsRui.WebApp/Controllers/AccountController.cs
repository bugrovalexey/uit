﻿namespace bgTeam.IsRui.WebApp.Controllers
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common.Helpers;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Story.Account;
    using bgTeam.IsRui.WebApp.Model;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;

    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IMapperBase _mapper;
        private readonly IUserRepository _userRepository;
        private readonly IStoryBuilder _builder;

        public AccountController(
            IMapperBase mapper,
            IUserRepository userRepository,
            IStoryBuilder storyBuilder)
        {
            _mapper = mapper;
            _userRepository = userRepository;
            _builder = storyBuilder;
        }

        /// <summary>
        /// Получить текущего пользователя
        /// </summary>
        [HttpGet]
        [IgnoreAntiforgeryToken]
        public string Index()
        {
            var user = _userRepository.GetCurrent();

            return user == null ? "Необходимо авторизоваться" : $"{user.FullName}, {user.Login}";
        }

        /// <summary>
        /// Авторизовать пользователя
        /// </summary>
        [HttpPost]
        //[VsalidateAntiForgeryToken]
        [Produces("application/json")]
        [IgnoreAntiforgeryToken]
        public async Task<UserDto> Login([FromBody] LoginModel model)
        {
            //_userRepository.Refresh();

            if (string.IsNullOrEmpty(model.Login))
            {
                throw new ArgumentNullException(nameof(model.Login));
            }

            if (string.IsNullOrEmpty(model.Password))
            {
                throw new ArgumentNullException(nameof(model.Password));
            }

            var user = _userRepository.Get(x => x.Where(t => t.Login == model.Login).Fetch(f => f.Roles));
            if (user != null)
            {
                var pass = CryptoHelper.GetMD5Hash(model.Password);

                if (user.Password == pass && user.IsActive)
                {
                    // аутентификация
                    await Authenticate(user.Login, user.Roles.Where(x => !string.IsNullOrWhiteSpace(x.Code)).Select(x => x.Code));

                    return _mapper.Map(user, new UserDto());
                }
            }

            HttpContext.Response.StatusCode = 401;
            HttpContext.Response.ContentType = "text/html";

            var text = "Неправильный логин или пароль";
            byte[] data = Encoding.UTF8.GetBytes(text);

            await HttpContext.Response.Body.WriteAsync(data, 0, data.Length);

            return null;
        }

        /// <summary>
        /// Проверить авторизацию
        /// </summary>
        [HttpGet]
        [Authorize]
        [IgnoreAntiforgeryToken]
        public async Task<UserDto> IsAuthenticate()
        {
            _userRepository.Refresh();

            var user = _userRepository.GetCurrent();

            return _mapper.Map(user, new UserDto());
        }

        private async Task Authenticate(string userName, IEnumerable<string> roles = null)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };

            if (roles != null)
            {
                claims.AddRange(roles.Select(x => new Claim(ClaimsIdentity.DefaultRoleClaimType, x)));
            }

            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        /// <summary>
        /// Разлогинить пользователя
        /// </summary>
        [HttpGet]
        [IgnoreAntiforgeryToken]
        public async Task Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        /// <summary>
        /// Изменить данные пользователя
        /// </summary>
        [HttpPost]
        public async Task<UserDto> UpdateInfo([FromBody] UpdateInfoStoryContext context)
        {
            _userRepository.Refresh();

            return await _builder
                 .Build(context)
                 .ReturnAsync<UserDto>();
        }

        /// <summary>
        /// Изменить пароль
        /// </summary>
        [HttpPost]
        public async Task<ChangePasswordDto> ChangePasswordStory([FromBody] ChangePasswordStoryContext context)
        {
            var result = await _builder
                 .Build(context)
                 .ReturnAsync<ChangePasswordDto>();

            if (result.Result)
            {
                //_userRepository.Refresh();

                return result;
            }

            HttpContext.Response.StatusCode = 400;

            return result;
        }
    }
}
