﻿using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Mapping.Plans
{
    class PlansActivityCompareMap : ClassMapBase<PlansActivityCompare>
    {
        public PlansActivityCompareMap()
            : base("Activity_Compare")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Date1, "Activity_Compare_Date1");
            Map(x => x.Date2, "Activity_Compare_Date2");
            Map(x => x.Changes, "Activity_Compare_Comment");

            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID");
        }
    }
}