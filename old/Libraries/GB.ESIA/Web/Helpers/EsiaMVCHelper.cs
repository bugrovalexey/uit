﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using GB.ESIA.Web.Helpers;

namespace GB.ESIA.Web.Helpers
{
    public class EsiaMVCHelper
    {
        public static void Login(Page Page, string absoluteUrl)
        {
            // Создание запроса на аутентификацию.
            var authenticationRequest = new AuthenticationRequest(Page, absoluteUrl, CertificateAccessor.ServiceProviderCert, ConfigAccessor.SingleSignOnServiceUrl, ConfigAccessor.AssertionServiceUrl(Page.Request));

            if (ConfigAccessor.DebugMode)
                Util.WriteLog("Temp/LastAuthRequest.xml", authenticationRequest.Xml.OuterXml);

            // Определение URL'а, на который следует перейти в случае успешной аутентификации.
            string urlToRedirect = Util.GetAbsoluteUrl(Page, FormsAuthentication.GetRedirectUrl(string.Empty, false));

            // Сохранение URL'а в кэше.
            Util.InsertIntoCache(authenticationRequest.Id, urlToRedirect, new TimeSpan(1, 0, 0));
            // Отправка запроса.
            authenticationRequest.Send();
        }

        public static void Logout(string Snils, Page Page, string absoluteUrl)
        {
            var nameIdentifier = string.Empty;
            if (!string.IsNullOrEmpty(Snils))
                nameIdentifier = (string)Util.RemoveFromCache(Snils);

            var logoutRequest = new LogoutRequest(Page, absoluteUrl, CertificateAccessor.ServiceProviderCert, ConfigAccessor.SingleLogoutServiceUrl, nameIdentifier);
            if (ConfigAccessor.DebugMode)
                Util.WriteLog("Temp/LastLogoutRequest.xml", logoutRequest.Xml.OuterXml);

            logoutRequest.Send();
        }

        public static bool IsLogoutCompleted(Page page, string absoluteUrl)
        {
            var redirect = false;
            string logoutRequestId = GetLogoutRequestIdFromIdp(page);
            if (string.IsNullOrEmpty(logoutRequestId))
            {
                logoutRequestId = GetLogoutResponseIdFromIdp(page);
                redirect = true;
            }
            // Создание запроса, подтверждающего успешное отключение на стороне ServiceProvider'а.
            var logoutResponse = new LogoutResponse(page, absoluteUrl, CertificateAccessor.ServiceProviderCert, ConfigAccessor.SingleLogoutServiceResponseUrl, logoutRequestId);
            if (ConfigAccessor.DebugMode)
                Util.WriteLog("Temp/LastLogoutResponse.xml", logoutResponse.Xml.OuterXml);

            logoutResponse.Send();
            return redirect;
        }

        /// <summary>
        /// Возвращает идентификатор запроса на подтверждение отключения от IdentityProvider'а.
        /// </summary>
        /// <returns>Идентификатор запроса или, если не удалось обработать запрос, null.</returns>
        private static string GetLogoutRequestIdFromIdp(Page Page)
        {
            try
            {
                // Расшифровка запроса на подтверждение отключения от IdentityProvider'а.
                var logoutRequestFromIdp = new LogoutRequest(Page, CertificateAccessor.IdentityProviderCert);
                if (ConfigAccessor.DebugMode)
                    Util.WriteLog("Temp/LastLogoutRequestFromIdp.xml", logoutRequestFromIdp.Xml.OuterXml);

                return logoutRequestFromIdp.Id;
            }
            catch (Exception ex)
            {
                if (ConfigAccessor.DebugMode)
                    Util.WriteLog("Temp/LastLogoutRequestFromIdp.xml", ex.ToString());

                return null;
            }
        }

        /// <summary>
        /// Возвращает идентификатор ответа от IdentityProvider'а.
        /// </summary>
        /// <returns>Идентификатор ответа или, если не удалось обработать ответ, null.</returns>
        private static string GetLogoutResponseIdFromIdp(Page Page)
        {
            try
            {
                // Расшифровка ответа от IdentityProvider'а.
                var logoutResponseFromIdp = new LogoutResponse(Page, CertificateAccessor.IdentityProviderCert);
                if (ConfigAccessor.DebugMode)
                    Util.WriteLog("Temp/LastLogoutRequestFromIdp.xml", logoutResponseFromIdp.Xml.OuterXml);

                return logoutResponseFromIdp.Id;
            }
            catch (Exception ex)
            {
                if (ConfigAccessor.DebugMode)
                    Util.WriteLog("Temp/LastLogoutResponseFromIdp.xml", ex.ToString());

                return null;
            }
        }
    }
}
