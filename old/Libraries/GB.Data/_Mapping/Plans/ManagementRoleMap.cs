﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ManagementRoleMap : ClassMapBase<ManagementRole>
    {
        public ManagementRoleMap()
            : base("Gov_Struct_Role")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Role, "Gov_Struct_Role_Name");
            Map(x => x.Managment, "Gov_Struct_Role_Management");
            Map(x => x.Function, "Gov_Struct_Role_Function");
            
            References<ManagementStructure>(x => x.Structure, "Gov_Struct_ID");
        }
    }
}