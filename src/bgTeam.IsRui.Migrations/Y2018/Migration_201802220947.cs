﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201802221455)]
    public class Migration_201802221455 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("OU").AddColumn("OU_Financing_quarter_I").AsDecimal().Nullable();
            Alter.Table("OU").AddColumn("OU_Financing_quarter_II").AsDecimal().Nullable();
            Alter.Table("OU").AddColumn("OU_Financing_quarter_III").AsDecimal().Nullable();
            Alter.Table("OU").AddColumn("OU_Financing_quarter_IV").AsDecimal().Nullable();
        }
    }
}
