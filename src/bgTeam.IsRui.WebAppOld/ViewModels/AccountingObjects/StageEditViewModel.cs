﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Repository;
using System.ComponentModel;
using System.Web.Mvc;
using bgTeam.IsRui.Common.Extentions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class StageEditViewModel
    {
        public int Id { get; set; }

        [DisplayName("Стадия")]
        public int StageId { get; set; }

        public SelectList StageList { get; set; }


        public StageEditViewModel(int id)
        {
            var ao = new RepositoryDc<AccountingObject>().Get(id);

            Id = ao.Id;
            StageId = (int)ao.Stage;

            StageList = new SelectList(EnumExtensions.ToKeyValuePairs<AccountingObjectsStageEnum>(), "Key", "Value");
        }
    }
}