﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoFinancingUpdateInfoStory : IStory<AoFinancingUpdateInfoStoryContext, FinancingDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;

        public AoFinancingUpdateInfoStory(IRepositoryRui repository, IMapperBase mapper, ISessionNHibernateFactory factory)
        {
            _mapper = mapper;
            _repository = repository;
            _factory = factory;
        }

        public FinancingDto Execute(AoFinancingUpdateInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<FinancingDto> ExecuteAsync(AoFinancingUpdateInfoStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            var ao = _repository.Get<AccountingObject>(context.AoId);

            using (var session = _factory.OpenSession())
            {
                _mapper.Map(context, ao);

                await session.UpdateAsync(ao);
            }

            return _mapper.Map(ao, new FinancingDto());
        }
    }
}
