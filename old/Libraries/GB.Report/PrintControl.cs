﻿using System;
using System.ComponentModel;
using System.IO;
using System.Web;
using System.Web.UI;
using GB.Helpers;
using Stimulsoft.Report.Export;

namespace GB.Report
{
    public class PrintControl : Stimulsoft.Report.Web.StiWebViewer
    {
        public IReportData ReportData { get; set; }


        public PrintControl()
        {
            this.RenderMode = Stimulsoft.Report.Web.StiRenderMode.UseCache;

            this.ShowExportToBmp = false;
            this.ShowExportToCsv = false;
            this.ShowExportToDbf = false;
            this.ShowExportToExcel2007 = false;
            this.ShowExportToExcelXml = false;
            this.ShowExportToGif = false;
            this.ShowExportToHtml = false;
            this.ShowExportToJpeg = false;
            this.ShowExportToMetafile = false;
            this.ShowExportToMht = false;
            this.ShowExportToOds = false;
            this.ShowExportToOdt = false;
            this.ShowExportToPcx = false;
            this.ShowExportToPng = false;
            this.ShowExportToPowerPoint = false;
            this.ShowExportToText = false;
            this.ShowExportToTiff = false;
            this.ShowExportToWord2007 = false;
            this.ShowExportToXml = false;
            this.ShowExportToXps = false;
            this.ShowExportToDif = false;
            this.ShowExportToDocument = false;
            this.ShowExportToSvg = false;
            this.ShowExportToSvgz = false;
            this.ShowExportToSylk = false;

            this.ShowCurrentPage = false;
            this.ShowPrintButton = false;
            this.ShowFirstButton = false;
            this.ShowLastButton = false;
            this.ShowNextButton = false;
            this.ShowPrevButton = false;
            this.ShowViewMode = false;

            this.ShowZoom = false;
            this.ExcelPageBreaks = true;

            this.ToolbarAlignment = Stimulsoft.Report.Web.StiContentAlignment.Center;

            this.ViewMode = Stimulsoft.Report.Web.StiWebViewMode.WholeReport;
            this.PrintDestination = Stimulsoft.Report.Web.StiPrintDestination.Direct;
        }

        protected override void OnReportExport(Stimulsoft.Report.Web.StiExportDataEventArgs e)
        {
            MemoryStream stream = new MemoryStream();
            string extension = null;

            if (!e.Report.IsRendered)
            {
                e.Report.Render(false);
            }

            switch (e.Settings.GetExportFormat())
            {
                case Stimulsoft.Report.StiExportFormat.Pdf:
                    extension = ".pdf";      
                    new StiPdfExportService().ExportPdf(e.Report, (Stream)stream);
                    break;

                case Stimulsoft.Report.StiExportFormat.Excel:
                    extension = ".xls";
                    new StiExcelExportService().ExportExcel(e.Report, (Stream)stream);
                    break;

                case Stimulsoft.Report.StiExportFormat.Rtf:
                    extension = ".rtf";
                    new StiRtfExportService().ExportRtf(e.Report, (Stream)stream);
                    break;

                default:
                    base.OnReportExport(e);
                    return;
            }

            FileHelper.SendFile(HttpContext.Current, e.Report.ReportAlias + extension, "application/octet-stream", stream.ToArray());
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                Stimulsoft.Report.StiReport report = ReportCreator.Create(ReportData);

                if (OnReportCreate != null)
                {
                    var er = new PrintControlEventArgs(report);

                    OnReportCreate(this, er);

                    report = er.Report;
                }

                this.Report = report;
            }
            catch (AccessReportException)
            {
                // Разобраться с вызовом 403 ошибки из кода.
                ResponseHelper.DenyAccessToPage(HttpContext.Current.Response);
            }

            base.OnPreRender(e);
        }

        public event PrintControlEventHandler OnReportCreate;
    }

    public delegate void PrintControlEventHandler(object sender, PrintControlEventArgs e);

    public class PrintControlEventArgs : EventArgs
    {
        public Stimulsoft.Report.StiReport Report { get; set; }

        public PrintControlEventArgs(Stimulsoft.Report.StiReport report)
        {
            Report = report;
        }
    }
}
