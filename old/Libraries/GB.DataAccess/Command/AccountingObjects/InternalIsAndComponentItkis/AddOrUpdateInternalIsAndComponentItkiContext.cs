﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.InternalIsAndComponentItkis
{
    public class AddOrUpdateInternalIsAndComponentItkiContext : BaseAddOrSaveContext
    {        
        public int AccountingObjectId { get; set; }
        
        public string Purpose { get; set; }
        
        public int? ObjectId { get; set; }
    }
}
