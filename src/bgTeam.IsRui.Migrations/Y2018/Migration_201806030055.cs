﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201806030055)]
    public class Migration_201806030055 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Claim").AddColumn("WorkflowId").AsInt32().Nullable();
        }
    }
}
