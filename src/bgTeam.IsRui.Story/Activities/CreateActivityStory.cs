﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CreateActivityStory : IStory<CreateActivityStoryContext, ActivityDto>
    {
        private readonly IMapperBase _mapper;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IUserRepository _userRepository;
        private readonly IRepositoryRui _repositoryRui;
        private readonly IStatusRepository _statusRepository;

        public CreateActivityStory(
            IMapperBase mapper,
            ISessionNHibernateFactory factory,
            IUserRepository userRepository,
            IRepositoryRui repositoryRui,
            IStatusRepository statusRepository)
        {
            _mapper = mapper;
            _factory = factory;
            _userRepository = userRepository;
            _repositoryRui = repositoryRui;
            _statusRepository = statusRepository;
        }

        public ActivityDto Execute(CreateActivityStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityDto> ExecuteAsync(CreateActivityStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            var ou = _repositoryRui.Get<AccountingObject>(context.AoId);

            if (ou == null)
            {
                throw new IsRuiException("Не найден объект учёта");
            }

            var curUser = _userRepository.GetCurrent();

            var activity = new PlansActivity()
            {
                Name = context.Name,
                ActivityType2 = _repositoryRui.GetExisting<ActivityType>(context.TypeId),
                Department = new Department() { Id = curUser.Department.Id },
                CreateDate = DateTime.Now,
                Year = context.Year.HasValue ? context.Year.Value : DateTime.Now.Year,
                ForecastQuarter1 = 0M,
                ForecastQuarter2 = 0M,
                ForecastQuarter3 = 0M,
                ForecastQuarter4 = 0M,
                PlanQuarter1 = 0M,
                PlanQuarter2 = 0M,
                PlanQuarter3 = 0M,
                PlanQuarter4 = 0M
            };

            var aoActivity = new AoActivity
            {
                AccountingObject = ou,
                Activity = activity,
                ActivityType = (ActivityTypeEnum)context.TypeId,
                Name = context.Name,
                TimeStamp = DateTime.Now,
                Year = (short)(context.Year.HasValue ? context.Year.Value : DateTime.Now.Year)
            };

            using (var session = _factory.OpenSession())
            {
                await session.InsertAsync(activity);

                await new WorkflowCreateStory(_statusRepository)
                    .ExecuteAsync(new WorkflowCreateStoryContext()
                    {
                        Entity = activity,
                        Session = session,
                    });

                await session.InsertAsync(aoActivity);
            }

            return _mapper.Map(activity, new ActivityDto());
        }
    }
}
