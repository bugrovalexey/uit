﻿using System;
using System.Data;
using GB;
using Stimulsoft.Report;

namespace GB.Report
{
    public abstract class BaseReportRepository
    {
        private string _ReportName = "Отчёт";

        protected string ReportName
        {
            get { return _ReportName; }
        }

        public BaseReportRepository()
        {

        }

        public BaseReportRepository(string repName)
        {
            _ReportName = repName;
        }

        protected virtual string GetReportName()
        {
            return _ReportName;
        }

        public void FillData(StiReport report, object[] data)
        {
            Logger.Info("Формируем отчёт");

            if (ValidationData(data))
            {
                if (ValidationAccess())
                {
                    DataSet ds = new DataSet("ReportData");

                    AddData(ds);

                    report.RegData(ds);

                    RegData(report);

                    report.ReportName = GetReportName();
                }
                else
                    throw new AccessReportException();
            }
            else
                throw new DataReportException();
        }


        protected virtual bool ValidationAccess()
        {
            return true;
        }
        
        protected abstract bool ValidationData(object[] data);

        protected virtual void AddData(DataSet ds)
        {
        }

        protected virtual void RegData(StiReport report)
        {
        }

        public static DataTable ToDataTable(System.Collections.ICollection data, string TableName, DataSet ds = null)
        {
            if (data == null || data.Count <= 0)
                return null;

            DataTable table = new DataTable(TableName);

            var enumerator = data.GetEnumerator();
            enumerator.Reset();
            enumerator.MoveNext();

            var enumeratorType = enumerator.Current.GetType();
            System.ComponentModel.PropertyDescriptorCollection props =
                System.ComponentModel.TypeDescriptor.GetProperties(enumeratorType);

            for (int i = 0; i < props.Count; i++)
            {
                System.ComponentModel.PropertyDescriptor prop = props[i];

                var propertyType = prop.PropertyType;
                if (propertyType.IsGenericType &&
                    propertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                {
                    propertyType = System.Nullable.GetUnderlyingType(propertyType);
                }
                table.Columns.Add(prop.Name, propertyType);
            }

            object[] values = new object[props.Count];
            foreach (var item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }

            if (ds != null && !ds.Tables.Contains(TableName))
            {
                ds.Tables.Add(table);
            }

            return table;
        }
    }

    public class AccessReportException : System.Web.HttpException
    {
        public AccessReportException()
            : base(404, "Нет доступа к отчёту.")
        {
        }
    }

    public class DataReportException : System.Web.HttpException
    {
        public DataReportException()
            : base(100, "Переданные параметры не распознаны.")
        {
        }
    }
}
