﻿namespace bgTeam.IsRui.DataAccess.Impl
{
    using bgTeam.IsRui.Common.Extentions;
    using bgTeam.IsRui.Domain;
    using bgTeam.IsRui.Domain.Entities.Common;
    using NHibernate;
    using NHibernate.Transform;
    using NHibernate.Type;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class SessionNHibernate : ISessionNHibernate
    {
        private static readonly object _lock = new object();

        private readonly ISession _session;
        private readonly ITransaction _transaction;

        public SessionNHibernate(ISession session)
        {
            _session = session;
        }

        public SessionNHibernate(ISession session, System.Data.IsolationLevel isolationLevel)
        {
            _session = session;

            _transaction = _session.BeginTransaction(isolationLevel);
        }

        public ISession GetSession()
        {
            return _session;
        }

        public void Insert(IEntity entity)
        {
            InsertAsync(entity).Wait();
        }

        public async Task InsertAsync(IEntity entity)
        {
            try
            {
                await _session.SaveAsync(entity);
                await _session.FlushAsync();
            }
            catch (Exception exp)
            {
                if (_transaction != null)
                {
                    await _transaction.RollbackAsync();
                }

                throw exp.GetInnerException();
            }
        }

        public void Update(IEntity entity)
        {
            UpdateAsync(entity).Wait();
        }

        public async Task UpdateAsync(IEntity entity)
        {
            try
            {
                await _session.UpdateAsync(entity);
                await _session.FlushAsync();
            }
            catch (Exception exp)
            {
                await _session.EvictAsync(entity);

                if (_transaction != null)
                {
                    await _transaction.RollbackAsync();
                }

                throw exp.GetInnerException();
            }
        }

        public async Task UpdateAsync<T>(IList<T> list)
            where T : IEntity
        {
            try
            {
                foreach (var item in list)
                {
                    await _session.UpdateAsync(item);
                    await _session.FlushAsync();
                }
            }
            catch (Exception exp)
            {
                foreach (var item in list)
                {
                    await _session.EvictAsync(item);
                }

                if (_transaction != null)
                {
                    await _transaction.RollbackAsync();
                }

                throw exp.GetInnerException();
            }
        }

        public async Task SaveOrUpdateAsync(IEntity entity)
        {
            try
            {
                if (entity.Id <= 0)
                {
                    await _session.SaveAsync(entity);
                }
                else
                {
                    await _session.UpdateAsync(entity);
                }

                await _session.FlushAsync();
            }
            catch (Exception exp)
            {
                await _session.EvictAsync(entity);

                if (_transaction != null)
                {
                    await _transaction.RollbackAsync();
                }

                throw exp.GetInnerException();
            }
        }

        public async Task DeleteAsync<T>(int id)
            where T : IEntity
        {
            T entity = await _session.GetAsync<T>(id);

            if (entity != null)
            {
                await DeleteAsync(entity);
            }
        }

        public async Task DeleteAsync(IEntity entity)
        {
            try
            {
                await _session.DeleteAsync(entity);
                await _session.FlushAsync();
            }
            catch (Exception exp)
            {
                if (_transaction != null)
                {
                    await _transaction.RollbackAsync();
                }

                throw exp.GetInnerException();
            }
        }

        public async Task<IEnumerable<T>> ExecuteProcedure<T>(string str, ParameterProcedure[] parameters)
            where T : IEntity
        {
            IQuery query = _session.CreateSQLQuery(str);

            foreach (var item in parameters)
            {
                query.SetParameter(item.Name, item.Value, item.Type);
            }

            return await query
                .SetResultTransformer(Transformers.AliasToBean<T>())
                .ListAsync<T>();
        }

        public async Task RefreshAsync(IEntity entity)
        {
            try
            {
                await _session.RefreshAsync(entity);
                await _session.FlushAsync();
            }
            catch (Exception exp)
            {
                if (_transaction != null)
                {
                    await _transaction.RollbackAsync();
                }

                throw exp.GetInnerException();
            }
        }

        public async Task CommitAsync()
        {
            if (_transaction != null)
            {
                await _transaction.CommitAsync();
            }
        }

        public async Task RollbackAsync()
        {
            if (_transaction != null)
            {
                await _transaction.RollbackAsync();
            }
        }

        public void Dispose()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
            }
        }
    }
}
