﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess
{
    /// <summary>
    /// Интерфейс фабрики, создающей команду для определенного контекста.
    /// </summary>
    public interface ICommandFactory
    {
        /// <summary>
        /// Создает команду по контексту команды.
        /// </summary>
        /// <typeparam name="TCommandContext">Контекст команды</typeparam>
        /// <returns>Экземпляр TCommandContext c заданным контекстом.</returns>
        ICommand<TCommandContext> Create<TCommandContext>() where TCommandContext : ICommandContext;

        /// <summary>
        /// Создает команду по контексту команды.
        /// </summary>
        /// <typeparam name="TCommandContext">Контекст команды</typeparam>
        /// <typeparam name="TResult">Результат команды</typeparam>
        /// <returns>Экземпляр TCommandContext c заданным контекстом.</returns>
        ICommand<TCommandContext, TResult> Create<TCommandContext, TResult>() where TCommandContext : ICommandContext;
    }
}
