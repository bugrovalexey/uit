﻿using bgTeam.IsRui.Domain.Entities.Common;

namespace bgTeam.IsRui.Domain._Mapping.Common
{
    internal class DepartmentMap : ClassMapHiLo<Department>
    {
        public DepartmentMap()
            : base("Govt_Organ")
        {
            //Table("w_");

            //Id(x => x.Id, "Govt_Organ_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //SqlInsert(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlUpdate(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlDelete(@"raiserror ('Unsupported',10,1)").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "Govt_Organ_Code").Nullable();
            Map(x => x.Name, "Govt_Organ_Name").Nullable();
            Map(x => x.NameSmall, "Govt_Organ_SName").Nullable();
            Map(x => x.Addr, "Govt_Organ_Addr").Nullable();
            Map(x => x.Phone, "Govt_Organ_Phone").Nullable();
            Map(x => x.Web, "Govt_Organ_Web").Nullable();
            Map(x => x.IsActive, "Govt_Organ_Is_Active").Nullable();
            //Map(x => x.SignatureIsRequired, "Govt_Organ_Required_DS");
            Map(x => x.SPZ, "Govt_Organ_SPZ");
            //Map(x => x.INN, "Govt_Organ_Inn");

            //Map(x => x.IsResponsibleRecieveEmails, "Govt_Organ_Is_Responsible_Recieve_Mails");

            References<Department>(x => x.Parent, "Govt_Organ_Par_ID");
            //References<DepartmentType>(x => x.Type, "Govt_Organ_Type_ID");

            //HasMany<Department>(x => x.Childs).KeyColumns.Add("Govt_Organ_Par_ID").Inverse();
            //HasMany<Executor>(x => x.Executors).KeyColumns.Add("Govt_Organ_ID").Inverse();

            //ApplyFilter<UserFilter>("Govt_Organ_ID in (select * from Get_Object_List(:UserId, " + (int)TableTypeEnum.Department + ", 'r'))");
        }
    }
}