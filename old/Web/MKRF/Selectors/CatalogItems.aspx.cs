﻿using System;
using GB.Entity.Directories;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxGridView;
using System.Linq;
using GB.Extentions;

namespace GB.MKRF.Web.Selectors
{
    public partial class CatalogItems : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            var a = 0;
            base.OnInit(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            CatalogItemsGrid.DataBind();

            base.OnPreRender(e);
        }

        protected void CatalogItemsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            int year = DateTime.Now.Year;

            if(!string.IsNullOrWhiteSpace(Request.QueryString["year"]))
                year = int.Parse(Request.QueryString["year"]);

            var type = Request.QueryString["type"];
            switch (type.ToUpper())
            {
                case "EXPENSEITEM":
                    CatalogItemsGrid.DataSource = DirectoriesRepositary.GetDirectories<ExpenseItem>(year).OrderBy(x => x.Code);
                    break;
                case "GRBS":
                    CatalogItemsGrid.DataSource = DirectoriesRepositary.GetDirectories<GRBS>(year).OrderBy(x => x.Code);
                    break;
                case "SECTIONKBK":
                    CatalogItemsGrid.DataSource = DirectoriesRepositary.GetDirectories<SectionKBK>(year).OrderBy(x => x.Code);
                    break;
                case "WORKFORM":
                    CatalogItemsGrid.DataSource = DirectoriesRepositary.GetDirectories<WorkForm>(year).OrderBy(x => x.Code);
                    break;
                case "KOSGU":
                case "EXPENDITUREITEMS":
                    CatalogItemsGrid.DataSource = DirectoriesRepositary.GetDirectories<ExpenditureItem>(year).OrderBy(x => x.Code);
                    break;
            }

        }

        //protected void CatalogItemsGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        //{
        //    e.Row.Attributes.Add("onmouseover", "$(this).addClass(\"grid_mouseover\")");
        //    e.Row.Attributes.Add("onmouseout", "$(this).removeClass(\"grid_mouseover\")");
        //}
    }
}