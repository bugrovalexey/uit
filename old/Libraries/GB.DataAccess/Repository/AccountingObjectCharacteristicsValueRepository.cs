﻿using System.Collections.Generic;
using GB.Data.AccountingObjects;
using GB.Data.Plans;

namespace GB.DataAccess.Repository
{
    public class AccountingObjectCharacteristicsValueRepository : Repository<AccountingObjectCharacteristicsValue>
    {
        public static IList<GetActivityCharact> GetAllForActivity(PlansActivity pa)
        {
            var sql = "SELECT * FROM [Get_Activity_Charact] (:ID)";

            return ExecuteSqlToObject<GetActivityCharact>(sql, q => q
                .SetParameter("ID", pa.Id));
        }
    }

    public class GetActivityCharact
    {
        public object CharId { get; set; }

        public object CharValueId { get; set; }

        public object Name { get; set; }

        public object Type { get; set; }

        public object Unit { get; set; }

        public object Fact { get; set; }

        public bool IsRegister { get; set; }

        public object Norm { get; set; }

        public object Max { get; set; }
    }
}