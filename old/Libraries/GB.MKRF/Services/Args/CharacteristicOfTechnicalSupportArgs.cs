﻿namespace GB.MKRF.Services.Args
{
    public class CharacteristicOfTechnicalSupportArgs : BaseArgsNew, IServiceArgs
    {
        public int TechnicalSupportId { get; set; }

        public string Name { get; set; }

        public int UnitId { get; set; }

        public double? Value { get; set; }
    }
}