﻿namespace bgTeam.IsRui.Domain._Mapping.Claims
{
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class ClaimActMap : ClassMapHiLo<ClaimAct>
    {
        public ClaimActMap()
            : base("ClaimAct")
        {
        }

        protected override void InitMap()
        {
            References<Files>(x => x.File, "ClaimAct_File_ID").Nullable();
            References<Claim>(x => x.Claim, "ClaimAct_Claim_ID").Not.Nullable();

            Map(x => x.DocName, "ClaimAct_Name").Nullable();
            Map(x => x.DocNumber, "ClaimAct_Number").Not.Nullable();
            Map(x => x.DateAdoption, "ClaimDocument_DateAdoption").Nullable();
            Map(x => x.Cost, "ClaimDocument_Cost").Nullable();
        }
    }
}
