﻿using GB.Data;
using GB.Data.Workflow;
using GB.MKRF.Logic.Status;
using GB.MKRF.Repository;
using GB.MKRF.Services.Args;
using GB.Patterns;
using System;

namespace GB.MKRF.Services
{
    public sealed class StatusService : ServiceBase<Status>, IService<Status, StatusArgs>
    {
        public Status Create(StatusArgs args)
        {
            var entity = new Status();
            return SaveOrUpdate(entity, args);
        }

        public Status Update(StatusArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private Status SaveOrUpdate(Status entity, StatusArgs args)
        {
            //TODO StatusService SaveOrUpdate
            //entity.Role = GetEntity<Role>(args.RoleId);
            //entity.From = GetEntity<Status>(args.FromId);
            //entity.To = GetEntity<Status>(args.ToId);

            base.SaveOrUpdate(entity);
            return entity;
        }

        public void Delete(StatusArgs args)
        {
            base.Delete(args.Id);
        }

        public void Change<T>(T obj, int newStatusId, string comment = null) where T : IWorkflowObject
        {
            Status nowStatus = obj.Status;
            Status newStatus = new Repository<Status>().Get(newStatusId);

            if (newStatus == null)
                throw new ArgumentException("Не найден статус", "newStatusId");

            var handler = Singleton<WorkflowManager>.Instance.GetHandler<T>();
            if (handler != null)
            {
                string confirm, error;
                if (!handler.ReadyForNewStatus(obj, newStatus, out error, out confirm))
                {
                    throw new InvalidOperationException(error);
                }
            }

            obj.Status = newStatus;

            InternalStaticMethods.Update(obj);

            //try
            //{
            //    //выполнить какие либо действия при переводе статуса
            //    Workflow.Workflow.StatusChangeEvent<T>(entity, nowStatus, comment);
            //}
            //catch (Exception exp)
            //{
            //    Logger.Fatal(exp);
            //}

            //AddHistory(obj, comment, nowStatus, newStatus);
        }
    }
}