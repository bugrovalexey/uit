﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoConclusionAddOrUpdateStory : IStory<AoConclusionAddOrUpdateStoryContext, AccountingObjectUserCommentDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;
        private readonly ISessionNHibernateFactory _factory;

        public AoConclusionAddOrUpdateStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IUserRepository userRepository,
            ISessionNHibernateFactory factory)
        {
            _mapper = mapper;
            _repository = repository;
            _userRepository = userRepository;
            _factory = factory;
        }

        public AccountingObjectUserCommentDto Execute(AoConclusionAddOrUpdateStoryContext context)
        {
            return ExecuteAsync(context).GetAwaiter().GetResult();
        }

        public async Task<AccountingObjectUserCommentDto> ExecuteAsync(AoConclusionAddOrUpdateStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            AccountingObjectUserComment entity = null;

            if (context.Id.HasValue)
            {
                entity = _repository.Get<AccountingObjectUserComment>(context.Id.Value);
            }
            else
            {
                var curUser = _userRepository.GetCurrent();

                entity = _repository.Get<AccountingObjectUserComment>(q => q
                    .Where(x => x.AccountingObject.Id == context.AoId && x.User.Id == curUser.Id && x.Status == null));

                if (entity == null)
                {
                    entity = new AccountingObjectUserComment();
                    entity.User = curUser;
                    entity.CreateDate = DateTime.Now;
                }
            }

            entity.AccountingObject = new AccountingObject() { Id = context.AoId };
            entity.Comment = context.Comment;

            using (var session = _factory.OpenSession())
            {
                await session.SaveOrUpdateAsync(entity);
            }

            return _mapper.Map(entity, new AccountingObjectUserCommentDto());
        }
    }
}
