﻿using System.Runtime.Remoting.Messaging;
using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.Directories;
using GB.Data.Plans;
using GB.DataAccess.Services;
using GB.DataAccess.Services.Args;
using NUnit.Framework;
using System;

namespace GB.DataAccess.Test.Services
{
    [TestFixture]
    public class ServicesTest
    {
        public ServicesTest()
        {
            Init.InitTest();
        }

        [TearDown]
        public void Dispose()
        {
            CallContext.SetData("CoordNHibernateSession", null);
        }


        [Test]
        public void AOGovServiceNPAService()
        {
            var args = new AOGovServiceNPAArgs()
            {
                AOGovServiceId = 1,
                Name = "Тестовая государственная услуга",
                DocumentNumber = "Ф100",
                Date = DateTime.Now,
                ParticleOfDocument = "Тест",
            };

            Test(new AOGovServiceNPAService(), args);
        }

        [Test]
        public void AOGovServiceService()
        {
            var args = new AOGovServiceArgs()
            {
                AccountingObjectId = 1,
                PercentOfUseResource = 100,
            };

            Test(new AOGovServiceService(), args);
        }

        [Test]
        public void TechnicalSupportService()
        {
            var args = new TechnicalSupportArgs()
            {
                
                AccountingObjectId = 1,
                Name = "Тест сервисов",
                CategoryKindOfSupportId = 1,
                ManufacturerId = 1,
                Amount = 1,
                Summa = 99.99M,
            };

            Test(new TechnicalSupportService(), args);
        }

        [Test]
        public void AccessToEntityService()
        {
            var args = new AccessToEntityArgs()
            {
            };

            Test(new AccessToEntityService(), args);
        }

        [Test]
        public void AccountingObjectCharacteristicsService()
        {
            var args = new AccountingObjectCharacteristicsArgs()
            {
                AccountingObjectId = 1,
                TypeId = EntityBase.Default,
                UnitId = EntityBase.Default,
            };

            Test(new AccountingObjectCharacteristicsService(), args);
        }

        //[Test]
        //public void AccountingObjectCharacteristicsValueService()
        //{
        //    var args = new AccountingObjectCharacteristicsValueArgs()
        //    {
        //    };

        //    Test(new AccountingObjectCharacteristicsValueService(), args);
        //}

        [Test]
        public void AccountingObjectService()
        {
            var args = new AccountingObjectArgs()
            {
                ShortName = "Тест сервисов",

            };

            Test(new AccountingObjectService(), args);
        }

        //[Test]
        //public void ActivityCommentService()
        //{
        //    var args = new ActivityCommentArgs()
        //    {
        //    };

        //    Test(new ActivityCommentService(), args);
        //}

        //[Test]
        //public void ActivityExpertiseConclusionService()
        //{
        //    var args = new ActivityExpertiseConclusionArgs()
        //    {
        //    };

        //    Test(new ActivityExpertiseConclusionService(), args);
        //}

        //[Test]
        //public void ActivityExpertService()
        //{
        //    var args = new ActivityExpertArgs()
        //    {
        //    };

        //    Test(new ActivityExpertService(), args);
        //}

        //[Test]
        //public void ActivityService()
        //{
        //    var args = new ActivityServiceArgs()
        //    {
        //    };

        //    Test(new ActivityService(), args);
        //}

        [Test]
        public void ActivityServicesService()
        {
            var args = new PlansActivityServiceArgs()
            {
                Name = "Тест сервисов",
            };


            Test(new ActivityServicesService(), args);
        }

        [Test]
        public void ActOfAcceptanceService()
        {
            var args = new ActOfAcceptanceArgs()
            {
                GovContractId = 1,
            };

            Test(new ActOfAcceptanceService(), args);
        }

        [Test]
        public void AODecisionToCreateService()
        {
            var args = new AODecisionToCreateArgs()
            {
                AccountingObjectId =  1,
                Name = "Тест сервисов",
            };

            Test(new AODecisionToCreateService(), args);
        }

        [Test]
        public void CharacteristicOfTechnicalSupportService()
        {
            var args = new CharacteristicOfTechnicalSupportArgs()
            {
                Name = "Тест сервисов",
                UnitId = EntityBase.Default,
            };

            Test(new CharacteristicOfTechnicalSupportService(), args);
        }

        //[Test]
        //public void DocumentService()
        //{
        //    var args = new DocumentArgs()
        //    {
        //    };

        //    Test(new DocumentService(), args);
        //}

        [Test]
        public void InformationInteractionService()
        {
            var args = new InformationInteractionArgs()
            {
                AccountingObjectId = 1,
                Name = "Тест сервисов",
            };

            Test(new InformationInteractionService(), args);
        }

        [Test]
        public void OtherReasonsService()
        {
            var args = new ProjectReasonArgs()
            {
                OwnerId = GetIdFirstPlansActivity(),
                Num = "1",
                SignerName = "Тест сервисов",
                SignerPosition = "Тест сервисов",
                Name = "Тест сервисов",
            };

            Test(new OtherReasonsService(), args);
        }

        [Test]
        public void PlansActivityIndicatorService()
        {
            var args = new PlansActivityIndicatorArgs()
            {
                Name = "Тест сервисов",
                PlansActivityId = GetIdFirstPlansActivity(),
            };

            Test(new PlansActivityIndicatorService(), args);
        }

        [Test]
        public void PlansActivityMarkService()
        {
            var args = new PlansActivityMarkArgs()
            {
                Name = "Тест сервисов",
                PlansActivityId = GetIdFirstPlansActivity(),
            };

            Test(new PlansActivityMarkService(), args);
        }

        [Test]
        public void PlansSpecCharacteristicsService()
        {
            var args = new PlansSpecCharacteristicArgs()
            {
                GoodsId = 1,
            };

            Test(new PlansSpecCharacteristicsService(), args);
        }

        [Test]
        public void PlansSpecService()
        {
            var args = new PlansSpecArgs()
            {
                ActivityId = GetIdFirstPlansActivity(),
                ExpenseDirection = 1,
                Year = YearEnum.First,
                ExpenseType = 1,
                OKPD = 1,
            };

            Test(new PlansSpecService(), args);
        }

        [Test]
        public void PlansWorkService()
        {
            var args = new PlansWorkArgs()
            {
                ActivityId = GetIdFirstPlansActivity(),
                ExpenseDirection = 1,
                Name = "Тест сервисов",
                Year = YearEnum.First,
                ExpenseType = 1,
                OKPD = 1,
            };

            Test(new PlansWorkService(), args);
        }

        [Test]
        public void RoleService()
        {
            var args = new RoleArgs()
            {
                Name = "Тест сервисов",
            };

            Test(new RoleService(), args);
        }

        [Test]
        public void SiteMapService()
        {
            var args = new SiteMapArgs()
            {
            };

            Test(new SiteMapService(), args);
        }

                [Test]
        public void SiteMapToRoleService ()
        {
            var args = new SiteMapToRoleArgs()
            {
            };

            Test(new SiteMapToRoleService(), args);
        }

            [Test]
        public void SoftwareService()
        {
            var args = new SoftwareArgs()
            {
                AccountingObjectId = 1,
                Name = "Тест сервисов",
                CategoryKindOfSupportId = 1,
                ManufacturerId = 1,
                Right = RightsOnSoftwareEnum.Exclusive,
            };

            Test(new SoftwareService(), args);
        }

        [Test]
        public void SpecCheckService()
        {
            var args = new SpecialCheckArgs()
            {
                AccountingObjectId = 1,
                Name = "Тест сервисов",
            };

            Test(new SpecCheckService(), args);
        }

        [Test]
        public void StatusListService()
        {
            var args = new StatusListArgs()
            {
                RoleId = 1,
                FromId = 1,
                ToId = 1,                
            };

            Test(new StatusListService(), args);
        }

        [Test]
        public void StatusService()
        {
            var args = new StatusArgs()
            {
            };

            Test(new StatusService(), args);
        }

        [Test]
        public void UserService()
        {
            var args = new UserArgs()
            {
                DepartmentId = 1,
            };

            Test(new UserService(), args);
        }

        [Test]
        public void WorkAndServiceService()
        {
            var args = new WorkAndServiceArgs()
            {
                AccountingObjectId = 1,
                Name = "Тест сервисов",
                CategoryKindOfSupportId = 1,
            };


            Test(new WorkAndServiceService(), args);
        }

        //[Test]
        //public void WorksAndGoodsService()
        //{
        //    var args = new WorkAndGoodsArgs()
        //    {
        //    };

        //    Test(new WorksAndGoodsService(), args);
        //}

//---------------------------------- Шаблон
        //[Test]
        //public void ()
        //{
        //    var args = new Args()
        //    {
        //    };

        //    Test(new (), args);
        //}

//------------------------------------
        #region Helpers

        private void Test<T, TArgs>(IService<T, TArgs> service, TArgs args)
            where T : EntityBase
            where TArgs : BaseArgsNew
        {
            var entity = service.Create(args);
            args.Id = entity.Id;
            service.Update(args);
            service.Delete(args);
        }


        private int GetIdFirstPlansActivity()
        {
            return new Repository.Repository<PlansActivity>().SelectSingle<int>(q => q.Select(x => x.Id).Take(1));
        }

        #endregion Helpers

    }
}
