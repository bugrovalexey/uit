﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;
using Uviri.MKRF.Entities;
using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Documents;
using Uviri.MKRF.Entities.Plans;
using Uviri.MKRF.Repository;
using Uviri.MKRF.Web.Helpers;
using Uviri.MKRF.Web.Services.Registry;
using Uviri.Repository;

namespace Uviri.MKRF.Web.Services
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "https://www.365.minsvyaz.ru/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CoordService : System.Web.Services.WebService
    {
        //public CoordService()
        //{
        //    //System.ServiceModel.ServiceHost 
        //}

        int DepartmentMKSId = 71;
        DepartmentTypeEnum DepartmentMKS = DepartmentTypeEnum.Establishment;
        int Year = 2011;//DateTime.Now.Year;

        /// <summary>
        /// Получить дерево ИКТ-компонент
        /// </summary>
        //[WebMethod]
        //public string GetIKTComponentTree(Uviri.Coord.Web.Helpers.TreeType type, int depId)
        //{
        //    return DirectoriesHelper.GetIKTComponentsTree(type, depId);
        //}

        [WebMethod]
        public string GetIKTTree(int year, int depId)
        {
            return DirectoriesHelper.GetIKTTree(year, depId);
        }

        /// <summary>
        /// Получить данные для отчета "Результат проведения экспертной оценки планов информатизации"
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public DataResultExpertAssessment GetDataForResultExpertAssessment()
        {
            Dictionary<string, int> values;

            MonitoringRepository.GetDataForResultExpertAssessment(DepartmentMKSId, Year, (int)DepartmentMKS, out values);

            return new DataResultExpertAssessment(values);
        }

        /// <summary>
        /// Получить список планов
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public DataTable GetPlansList()
        {
            DataTable dt = DemandRepository<ExpDemandBase>.GetList();

            dt.TableName = "Table";

            return dt;
        }

        /// <summary>
        /// Получить данные для отчета "Объемы расходов на мероприятия планов по направлениям расходов на ИКТ"
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public DataTable GetExpenseVolByIKT()
        {
            return MonitoringRepository.GetDataForExpenseVolByIKT(-1, Year, ExpObjectTypeEnum.Plan).Tables["Grid"];
        }

        /// <summary>
        /// Получить данные для отчета "Планируемые затраты по БК ПЗ/РЗ"
        /// </summary>
        [WebMethod]
        public DataTable GetProjectedCosts(int year)
        {
            return MonitoringRepository.GetProjectedCosts(DepartmentMKSId, 1, year);
        }

        /// <summary>
        /// Общедоступные объекты экспертной оценки подсистемы "Координация"
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public DataTable GetPublicObjects()
        {
            string sql = @"select  a.Govt_Organ_CodeName as DepartmentName
                                           ,a.Year as Year
                                           ,a.Plans_Expense_Vol_FB  as ExpenseVolumeFB
                                           ,a.Plans_Expense_Vol_RB  as ExpenseVolumeRB
                                           ,a.Plans_Expense_Vol_VB  as ExpenseVolumeVB
                                           ,a.Plans_Expense_Vol_Itogo as SummaryExpense
                                    from    dbo.v_Plans a
                                    join    dbo.v_Govt_Organ_Active b
                                            on a.Govt_Organ_ID = b.Govt_Organ_ID
                                    where   Plans_Is_Public = 1
                                            and b.Govt_Organ_Type_ID = 1 order by a.Year, a.Govt_Organ_CodeName";

            DataTable dt = RepositoryBase<EntityBase>.GetTableViaSQL(sql);
            dt.TableName = "Table";

            return dt;
        }

//        /// <summary>
//        /// Общедоступные экспертные заключения подсистемы "Координация"
//        /// </summary>
//        /// <returns></returns>
//        [WebMethod]
//        public DataTable GetPublicConclusions()
//        {
//            string sql = @"select distinct
//                                    c.Govt_Organ_CodeName as DepartmentName
//                                   ,c.Plans_Name as Name
//                                   ,c.Year as Year
//                            from    dbo.w_Exp_Conclusion a
//                            join    dbo.w_Exp_Demand b
//                                    on a.Exp_Demand_ID = b.Exp_Demand_ID
//                            join    dbo.v_Plans c
//                                    on b.Plans_ID = c.Plans_ID
//                            where   Exp_Conclusion_Is_Public = 1 order by c.Year, c.Govt_Organ_CodeName";

//            DataTable dt = RepositoryBase<EntityBase>.GetTableViaSQL(sql);
//            dt.TableName = "Table";

//            return dt;
//        }

//        /// <summary>
//        /// Общедоступные экспертные заключения от Минкультуры подсистемы "Координация"
//        /// </summary>
//        /// <returns></returns>
//        [WebMethod]
//        public DataTable GetPublicConclusionsMks()
//        {
//            string sql = @"
//                            SELECT  a.[Year] ,
//                                    a.Govt_Organ_Name AS DepartmentName ,
//                                    CASE WHEN a.Exp_Obj_Type_ID = 7
//                                         THEN a.Exp_Obj_Type_Name + ' (' + b.Other_Doc_Type_Name + ')'
//                                         ELSE a.Exp_Obj_Type_Name
//                                    END AS TypeName ,
//                                    a.Exp_Obj_Type_ID AS TypeId ,
//                                    a.Plans_Paper_MKC_Out_Num AS OutNum ,
//                                    a.Plans_Paper_MKC_Out_Date AS OutDate ,
//                                    a.DocId ,
//                                    a.ExpId
//                            FROM    coord.dbo.v_Report26 a
//                                    LEFT JOIN dbo.w_Other_Doc_Type b ON b.Other_Doc_Type_ID = a.OtherDocTypeId";

//            DataTable dt = RepositoryBase.GetTableViaSQL(sql, null, "PublicConclusionsMks");

//            dt.Columns.Add("DocLink", typeof(string));
//            dt.Columns.Add("ExpLink", typeof(string));

//            string Plan2012newId = ((int)Uviri.Coord.Entities.Directories.ExpObjectTypeEnum.Plan2012new).ToString();
//            foreach (DataRow dr in dt.Rows)
//            {
//                string documentId = dr["DocId"].ToString();
//                if (!string.IsNullOrEmpty(documentId))
//                {
//                    dr["DocLink"] = "<a href='" + Uviri.Helpers.UrlHelper.Resolve("~/Common/publicfile.aspx") + "?f=" + documentId + "'>Файл</a>";
//                }

//                string expId = dr["ExpId"].ToString();
//                if (!string.IsNullOrEmpty(expId))
//                {
//                    string[] idArray = expId.Split(';');
//                    int expCnt = (idArray.Length > 1) ? 1 : 0;
                    
//                    bool is2012 = (dr["TypeId"].ToString() == Plan2012newId);
//                    int temp = 0;

//                    bool isExpObj = (dr["TypeId"].ToString() == "7"); // иной документ?
//                    // ид, начинающееся с 's' является ид сводного ЭЗ

//                    var links = from sId in idArray
//                            where 
//                                    !string.IsNullOrEmpty(sId) &&   
//                                    int.TryParse(sId.StartsWith("s") ? sId.Substring(1) : sId, out temp)
//                            select 
//                                    ("<a href='" +

//                                    (isExpObj ? Uviri.Coord.Web.Common.ExpObjConclusionControl.getReportUrl(temp, true) :
//                                                Uviri.Coord.Web.Common.ConclusionControl.getReportUrl(temp, true) )
                                    
//                                    .Replace("Report.aspx", "common/publicreport.aspx") +
                                    
//                                    "'>Заключение" + (expCnt > 0 ? expCnt++.ToString() : string.Empty) +
//                                                     (sId.StartsWith("s") ? "(с)" : string.Empty) +
                                    
//                                    "</a>");
                    
//                    dr["ExpLink"] = string.Join("<br/>", links.ToArray());
//                }
//            }

//            return dt;
//        }

        /// <summary>
        /// Проверить наличие файла по id документа "Координации"
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public bool IsFileViaDocIdExist(int docId)
        {
            Document doc = DocumentRepository.Get(docId);

            if (doc != null && doc.File != null && doc.File.Length > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Получить файл по id документа "Координации"
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string[] GetFileViaDocId(int docId)
        {
            string[] result = new string[2];

            Document doc = DocumentRepository.Get(docId);

            if (doc != null && doc.File != null && doc.File.Length > 0)
            {
                result[0] = Convert.ToBase64String(doc.File);
                result[1] = doc.FileName;
            }
            
            return result;		
        }

        /// <summary>
        /// Получить данные для отчета "Анализ объемов расходов" для плана информатизации
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public DataExpenseVol GetExpenseVol()
        {
            Dictionary<string, int> _values;
            double[] _sums = {10000000, 5000000, 1000000, 500000, 200000};

            MonitoringRepository.GetDataForExpenseVol(DepartmentMKSId, (int)DepartmentMKS, Year, _sums, 0, 0, new ExpObjectTypeEnum[] { ExpObjectTypeEnum.Plan }, out _values);

            return new DataExpenseVol(_values);
        }

        /// <summary>
        /// Получить данные для отчета "Анализ объемов расходов" для формы сведений
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public DataExpenseVol2012 GetExpenseVol2012()
        {
            Dictionary<string, int> _values;
            double[] _sums = { 10000000, 5000000, 1000000, 500000, 200000 };

            MonitoringRepository.GetDataForPreExpenseVol(DepartmentMKSId, (int)DepartmentMKS, 2012, _sums, 0, 0, 0, out _values);

            return new DataExpenseVol2012(_values);
        }

        /// <summary>
        /// По формам мониторинга использования ИКТ за 2010 год
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<double> GetFormMonitoringIKT2010()
        {
            List<double> _values = null;

            PrognosisAnalyticsRepository.GetDataForExpenseDirections(2010, out _values);

            return _values;
        }

        /// <summary>Отчет об объемах расходов на мероприятия по компонентам ИКТ</summary>
        [WebMethod]
        public DataTable GetExpenseActivitiesIKT()
        {
            Dictionary<string, decimal> values;

            DataTable dt = new DataTable("Table");
            dt.Columns.Add("Key", typeof (string));
            dt.Columns.Add("Value", typeof (decimal));

            MonitoringRepository.GetDataForExpenseActivitiesICT(2012, out values);

            foreach (KeyValuePair<string, decimal> item in values)
            {
                dt.Rows.Add(item.Key, item.Value);
            }

            return dt;
        }

        /// <summary>Количество специалистов, отвечающих за вопросы развития и использования ИКТ: итого</summary>
        [WebMethod]
        public List<double> GetSpecCountSummary()
        {
            List<double> values;
            PrognosisAnalyticsRepository.GetDataForSpecCountSummary_Sum(2010, out values);
            return values;
        }

        /// <summary>Количество специалистов, отвечающих за вопросы развития и использования ИКТ: ЦА</summary>
        [WebMethod]
        public List<double> GetSpecCountCA()
        {
            List<double> values;
            PrognosisAnalyticsRepository.GetDataForSpecCountCA_Sum(2010, out values);
            return values;
        }

        /// <summary>Обеспеченность серверами десяти лидирующих по количеству серверов ФОИВ</summary>
        [WebMethod]
        public DataTable GetTop10Server()
        {
            Dictionary<string, double[]> values;

            DataTable dt = new DataTable("Table");
            dt.Columns.Add("Key", typeof(string));
            dt.Columns.Add("x86", typeof(int));
            dt.Columns.Add("Other", typeof(int));

            PrognosisAnalyticsRepository.GetDataForTop10Server(2010, out values);

            foreach (KeyValuePair<string, double[]> item in values)
            {
                dt.Rows.Add(item.Key, item.Value[0], item.Value[1]);
            }

            return dt;
        }

        /// <summary>Обеспеченность персональными компьютерами. Десять лидирующих по количеству персональных компьютеров (ниже РIII) ФОИВ: ТП</summary>
        [WebMethod]
        public DataTable GetPCperEmployeeTop10PIIILowerTP()
        {
            Dictionary<string, double > values;

            DataTable dt = new DataTable("Table");
            dt.Columns.Add("Key", typeof(string));
            dt.Columns.Add("Value", typeof(decimal));

            PrognosisAnalyticsRepository.GetDataForPCperEmployeeTop10PIIILowerTP(2010, out values);

            foreach (KeyValuePair<string, double > item in values)
            {
                dt.Rows.Add(item.Key, item.Value);
            }

            return dt;
        }

        /// <summary>Обеспеченность персональными компьютерами. Десять лидирующих по количеству персональных компьютеров (ниже РIII) ФОИВ: ЦА</summary>
        [WebMethod]
        public DataTable GetPCperEmployeeTop10PIIILowerCA()
        {
            Dictionary<string, double> values;

            DataTable dt = new DataTable("Table");
            dt.Columns.Add("Key", typeof(string));
            dt.Columns.Add("Value", typeof(decimal));

            PrognosisAnalyticsRepository.GetDataForPCperEmployeeTop10PIIILowerCA(2010, out values);

            foreach (KeyValuePair<string, double> item in values)
            {
                dt.Rows.Add(item.Key, item.Value);
            }

            return dt;
        }

        /// <summary>Обеспеченность персональными компьютерами. Десять лидирующих по количеству персональных компьютеров ФОИВ: ТП</summary>
        [WebMethod]
        public DataTable GetPCperEmployeeTop10TP()
        {
            Dictionary<string, double> values;

            DataTable dt = new DataTable("Table");
            dt.Columns.Add("Key", typeof(string));
            dt.Columns.Add("Value", typeof(decimal));

            PrognosisAnalyticsRepository.GetDataForPCperEmployeeTop10TP(2010, out values);

            foreach (KeyValuePair<string, double> item in values)
            {
                dt.Rows.Add(item.Key, item.Value);
            }

            return dt;
        }

        /// <summary>Обеспеченность персональными компьютерами. Десять лидирующих по количеству персональных компьютеров ФОИВ: ЦА</summary>
        [WebMethod]
        public DataTable GetPCperEmployeeTop10CA()
        {
            Dictionary<string, double> values;

            DataTable dt = new DataTable("Table");
            dt.Columns.Add("Key", typeof(string));
            dt.Columns.Add("Value", typeof(decimal));

            PrognosisAnalyticsRepository.GetDataForPCperEmployeeTop10CA(2010, out values);

            foreach (KeyValuePair<string, double> item in values)
            {
                dt.Rows.Add(item.Key, item.Value);
            }

            return dt;
        }

        /// <summary>Топ-10 по объемам средств, планируемых на создание и развитие ИКТ (2011-2014)</summary>
        [WebMethod]
        public DataTable GetPlanedExpense()
        {
            DataTable dt = MonitoringRepository.GetDataForPlanedExpense();
            dt.TableName = "PlanedExpense";
            return dt;
        }

        /// <summary>Расходы ФОГВ по 242 виду расходов, отраженные в проекте  Федерального закона «О федеральном бюджете на 2012 год и на плановый период 2013 и 2014 годов»</summary>
        [WebMethod]
        public DataTable Get242TypeExpense()
        {
            DataTable dt = MonitoringRepository.GetDataFor242TypeExpense();
            dt.TableName = "242TypeExpense";
            return dt;
        }

        /// <summary>Планы информатизации на 2013 г. и плановый период для всех ОГВ и регионов</summary>
        [WebMethod]
        public DataTable GetAllPlanInformatization()
        {
            DataTable dt = MonitoringRepository.GetPlanInformatization();
            dt.TableName = "PlanInformatization";
            return dt;
        }

        /// <summary>Планы информатизации на 2013 г. и плановый период для всех регионов</summary>
        [WebMethod]
        public DataTable GetAllRegionPlanInformatization()
        {
            DataTable dt = MonitoringRepository.GetPlanInformatization(region:true);
            dt.TableName = "PlanInformatization";
            return dt;
        }

        /// <summary>Планы информатизации на 2013 г. и плановый период для всех ОГВ</summary>
        [WebMethod]
        public DataTable GetAllFoivPlanInformatization()
        {
            DataTable dt = MonitoringRepository.GetPlanInformatization(allFoiv: true);
            dt.TableName = "PlanInformatization";
            return dt;
        }

        /// <summary>Планы информатизации на 2013 г. и плановый период для конкретного ОГВ </summary>
        [WebMethod]
        public DataTable GetFoivPlanInformatization(int foivId)
        {
            DataTable dt = MonitoringRepository.GetPlanInformatization(foiv: foivId);
            dt.TableName = "PlanInformatization";
            return dt;
        }

        /// <summary>Обновление справочников через СМЭВ</summary>
        //[WebMethod]
        //public void UpdateIKT(string DirectoryName)
        //{
        //    //Smev smev = new Smev();
        //    Web.Code.Smev.Run(DirectoryName);
        //}

        [WebMethod]
        public byte[] GetPlansXML(int year)
        {
            Web.Services.SmevCoordService service = new Services.SmevCoordService();

            Uviri.Smev.Data.FunctionRequest request = new Uviri.Smev.Data.FunctionRequest();

            request.MessageData.AppData.CoordGetPlansRequest.Year = year;

            return service.GetPlans(request).MessageData.AppData.CoordGetPlansResult.File;
        }

        [WebMethod]
        public DataTable GetPlansActivityForPassport(int id)
        {
            string sql = @"SELECT isnull(g.Govt_Organ_Code + ' - ', '') +
                                   isnull(g.Govt_Organ_Name, '') as Department,
                                   c.User_Full_Info AS UserInfo,
                                   a.Plans_Activity_ID AS Id,
                                   d.Year,
                                   a.Plans_Activity_Name AS Name,
                                   a.Plans_Activity_P_Vol_Y0 + a.Plans_Activity_P_Add_Vol_Y0 as VolY0,
                                   a.Plans_Activity_P_Vol_Y1 + a.Plans_Activity_P_Add_Vol_Y1 as VolY1,
                                   a.Plans_Activity_P_Vol_Y2 + a.Plans_Activity_P_Add_Vol_Y2 as VolY2
                              FROM dbo.w_Plans_Activity a
                              LEFT JOIN dbo.v_Plans_All d
                                ON d.Plans_ID = a.Plans_ID
                              Left join w_Govt_Organ g
                                on g.Govt_Organ_ID = d.Govt_Organ_ID
                              LEFT JOIN dbo.w_Registry_Info b
                                ON b.Registry_Info_ID = a.Registry_Info_ID
                              LEFT JOIN dbo.V_Users c
                                ON c.Users_ID = d.Users_ID
                             WHERE b.Registry_Info_Orig_ID = @PassId";

            Dictionary<string, object> values = new Dictionary<string, object>();
            values.Add("PassId", id);

            return RepositoryBase<EntityBase>.GetTableViaSQL(sql, values, "PlansActivity");
        }

        [WebMethod]
        public DataTable GetPlansActivityWithFundingsForPassport(int id)
        {
            return PlanRepository.GetActivitiesForRegistryPassport(id);
        }

        [WebMethod]
        public DataTable GetPlansCostsForPassport(int id)
        {
            return PlanRepository.GetPlansCostsForRegistryPassport(id);
        }

        [WebMethod]
        public decimal GetPlansActivitySumForPassportForYear(int id, int year)
        {
            var table = PlanRepository.GetPlansCostsForRegistryPassport(id);
            decimal result = 0.0m;
            //сортируем записи по году и пробегаем их
            foreach (DataRow row in table.Select("", "Year asc"))
            {
                int rowYear = Convert.ToInt32(row["Year"]);
                //Не удалять!!! Здесь реализована одна из логик подсчета планируемых затрат, которая может всплыть
                //if (year == rowYear + 2) //если искомый год - это второй плановый для текущей записи, то берём затраты на второй год планового периода
                //    result = row["VolY2"] != DBNull.Value ? Convert.ToDecimal(row["VolY2"]) : 0.0m;
                //if (year == rowYear + 1) //если искомый год - это первый плановый для текущей записи, то берём затраты на первый год планового периода
                //    result = row["VolY1"] != DBNull.Value ? Convert.ToDecimal(row["VolY1"]) : 0.0m;
                if (year == rowYear) //если искомый год - это очередной для текущей записи, то берём затраты на очередной финансовый год
                {
                    result = row["PlanIz"] != DBNull.Value ? Convert.ToDecimal(row["PlanIz"]) : 0.0m;
                    return result;
                }
            }
            return result;
        }

        [WebMethod]
        public decimal GetPlansActivityTotalSumForPassport(int id)
        {
            //var table = GetPlansActivityForPassport(id);
            //return table.Rows.Cast<DataRow>().Sum(row => ((decimal) row["VolY0"]) + ((decimal) row["VolY1"]) + ((decimal) row["VolY2"]));
            var table = PlanRepository.GetActivitiesForRegistryPassport(id);
            return table.Rows.Cast<DataRow>().Sum(row => ((decimal)row["PlanIz"]));
        }

        /// <summary>
        /// Cумма планируемых затрат по всем ОГВ по всем планам на  год
        /// </summary>
        [WebMethod]
        public double GetTotalPlansActivityVolY0Sum(int year)
        {
            var ogvByYear = PlanRepository.GetAll().Where(x => x.Year != null && x.Year.Year == year);
            var activities = new List<PlansActivity>();
            foreach (Plan plan in ogvByYear.Where(plan => plan.Activities != null))
            {
                activities.AddRange(plan.Activities);
            }
            return activities.Sum(x => x.PlansActivityVolY0);
        }

        [WebMethod]
        public DataTable GetISPassIDForPreparing(string str, int year, int ogvId)
        {
            DataTable dt = OGVRepository.GetISPassIDForPreparing(str, year, ogvId);
            return dt;
        }

        [WebMethod]
        public DataTable GetSummaryByOGV(int year)
        {
            DataTable dt = OGVRepository.GetSummaryByOGV(year);

            // берем данные из учета, фактические затраты по ОГВ
            RegistryServiceSoapClient regService = new RegistryServiceSoapClient();
            DataTable regTable = regService.GetFactCostSumByYear(year);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j=0; j<regTable.Rows.Count; j++)
                if (dt.Rows[i]["OGV_ID"].ToString() == regTable.Rows[j]["OGV_ID"].ToString())
                {
                    float fz = 0;
                    string inStr = (regTable.Rows[j]["FactIz"].ToString()).Replace("M","");
                    if (float.TryParse(inStr, out fz))
                    {
                        dt.Rows[i]["FactIz"] = fz;
                        regTable.Rows[j]["used"] = true;
                    }
                }
            }

            // добавляем в ДТ неиспользованные значения
            for (int i = 0; i < regTable.Rows.Count; i++)
            {
                if (regTable.Rows[i]["used"].ToString() == "False")
                {
                    var newRow = dt.NewRow();
                    newRow["OGV_ID"] = regTable.Rows[i]["OGV_ID"].ToString();
                    newRow["FactIz"] = (int)(decimal)regTable.Rows[i]["FactIz"];
                    newRow["PlanIz"] = 0;

                    dt.Rows.Add(newRow);

                }
            }

            return dt;
        }

        [WebMethod]
        public DataTable GetPlansByYear(int year)
        {
            var dt = OGVRepository.GetSummaryPlansByYear(year);
            return dt;
        }

        [WebMethod]
        public DataTable GetOGVList(int year)
        {
            var dt = OGVRepository.GetOGVList(year);
            return dt;
        }

        [WebMethod]
        public DataTable GetIKTComponentsTree(int FoivId, int Year)
        {
            return OGVRepository.GetIKTComponentsTree(FoivId, Year);
        }

        [WebMethod]
        public DataTable GetRegistrySystemList(int ogvId, int iktId, int year)
        {
            return OGVRepository.GetRegistrySystemList(ogvId, iktId, year);
        }

        [WebMethod]
        public DataTable GetSums(int year)
        {
            var dt = OGVRepository.GetSums(year);

            // берем данные из учета, фактические затраты за год
            RegistryServiceSoapClient regService = new RegistryServiceSoapClient();
            DataTable regTable = regService.GetFactSums(year);

            if (regTable.Rows.Count > 0)
            {
                float fz = 0;
                string inStr = (regTable.Rows[0]["FactIz"].ToString()).Replace("M", "");
                if (float.TryParse(inStr, out fz))
                {
                    dt.Rows[0]["FactIzSum"] = fz;
                }
            }

            return dt;
        }

        [WebMethod]
        public DataTable GetFactCostSumByOGVandYear(int year, string ogvId, string ikt)
        {
            return OGVRepository.GetFactCostSumByOGVandYear(year, ogvId, ikt);
        }

        [WebMethod]
        public DataTable GetAmountOfFundings(int ogvId)
        {
            return OGVRepository.GetAmountOfFundings(ogvId);
        }

        [WebMethod]
        public DataTable GetAmountOfFundingsByActivities(int ogvId, int year)
        {
            return OGVRepository.GetAmountOfFundingsByActivities(ogvId, year);
        }

        [WebMethod]
        public DataTable GetRatingByCountPriv(int year)
        {
            DataTable dt = RatingRepository.GetRatingByCountPriv(year);
            return dt;
        }

        [WebMethod]
        public DataTable GetRatingByVolPriv(int year)
        {
            DataTable dt = RatingRepository.GetRatingByVolPriv(year);
            return dt;
        }

        [WebMethod]
        public DataTable GetRatingByMaxAm(int year)
        {
            DataTable dt = RatingRepository.GetRatingByMaxAm(year);
            return dt;
        }

        [WebMethod]
        public DataTable GetResponsibleByOGVandYear(int year, int ogvId)
        {
            DataTable dt = OGVRepository.GetResponsibleByOGVandYear(year, ogvId);
            return dt;
        }
        
        [WebMethod]
        public DataTable GetSupplersData()
        {
            return MonitoringRepository.GetSupplersData();
        }

        [WebMethod]
        public DataTable GetSupplersDetails(object supplerId)
        {
            return MonitoringRepository.GetSupplersDetails(supplerId);
        }

        [WebMethod]
        public DataTable GetOgvIndastryReport(int year)
        {
            return ReportRepository.GetOgvIndastryReport(year);
        }
    }


    [Serializable]
    public class DataResultExpertAssessment
    {
        public int PositiveExpertConclusion { get; set; }
        public int ExpertAssessmentNotFormed { get; set; }

        public DataResultExpertAssessment()
        {
        }

        public DataResultExpertAssessment(Dictionary<string, int> values)
        {
            PositiveExpertConclusion = values["PositiveExpertConclusion"];
            ExpertAssessmentNotFormed = values["ExpertAssessmentNotFormed"];
        }
    }

    [Serializable]
    public class DataExpenseVol
    {
        public int P_02 { get; set; }
        public int P_05 { get; set; }
        public int P_1 { get; set; }
        public int P_5 { get; set; }
        public int P_10 { get; set; }
        public int P_10m { get; set; }

        public DataExpenseVol()
        {

        }

        public DataExpenseVol(Dictionary<string, int> values)
        {
            P_02 = values["02"];
            P_05 = values["02_05"];
            P_1 = values["05_1"];
            P_5 = values["1_5"];
            P_10 = values["5_10"];
            P_10m = values["10"];
        }
    }

    [Serializable]
    public class DataExpenseVol2012
    {
        public int P_02_0 { get; set; }
        public int P_02_1 { get; set; }
        public int P_02_2 { get; set; }

        public int P_05_0 { get; set; }
        public int P_05_1 { get; set; }
        public int P_05_2 { get; set; }

        public int P_1_0 { get; set; }
        public int P_1_1 { get; set; }
        public int P_1_2 { get; set; }

        public int P_5_0 { get; set; }
        public int P_5_1 { get; set; }
        public int P_5_2 { get; set; }

        public int P_10_0 { get; set; }
        public int P_10_1 { get; set; }
        public int P_10_2 { get; set; }

        public int P_10m_0 { get; set; }
        public int P_10m_1 { get; set; }
        public int P_10m_2 { get; set; }

        public DataExpenseVol2012()
        {

        }

        public DataExpenseVol2012(Dictionary<string, int> values)
        {
            P_02_0 = values["02_0"];
            P_02_1 = values["02_1"];
            P_02_2 = values["02_2"];

            P_05_0 = values["02_05_0"];
            P_05_1 = values["02_05_1"];
            P_05_2 = values["02_05_2"];

            P_1_0 = values["05_1_0"];
            P_1_1 = values["05_1_1"];
            P_1_2 = values["05_1_2"];

            P_5_0 = values["1_5_0"];
            P_5_1 = values["1_5_1"];
            P_5_2 = values["1_5_2"];

            P_10_0 = values["5_10_0"];
            P_10_1 = values["5_10_1"];
            P_10_2 = values["5_10_2"];

            P_10m_0 = values["10_0"];
            P_10m_1 = values["10_1"];
            P_10m_2 = values["10_2"];
        }

    }
}
