﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Workflow;
using bgTeam.IsRui.Domain.Extensions;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.MKRF.Logic.Access.Handlers
{
    public class AccountingObjectAccessHandler : AccessHandlerBase<AccountingObject>
    {
        private readonly IUserRepository _userRepository;

        public override AccountingObject Entity { get; set; }


        public AccountingObjectAccessHandler(
            IUserRepository userRepository,
            AccountingObject ao)
            : base(ao)
        {
            _userRepository = userRepository;
        }

        public override bool CanRead()
        {
            if (base.CanRead() == false)
            {
                return false;
            }

            var user = _userRepository.GetCurrent();

            if (user.Roles.IsEq(RoleEnum.Institution))
            {
                if (Entity.Department.Id == user.Department.Id)
                    return true;
            }

            if (user.Roles.IsEq(RoleEnum.CA))
            {
                //if (Entity.Parent.Department.Id == user.Department.Id)
                return true;
            }

            return false;
        }

        public override bool CanEdit()
        {
            if (base.CanEdit() == false)
            {
                return false;
            }

            var user = _userRepository.GetCurrent();

            if (Entity.Department.Id == user.Department.Id)
            {
                return Entity.Status.IsEq(StatusEnum.AoDraft, StatusEnum.AoOnCompletion);                
            }

            return false;
        }

        public override bool CanDelete()
        {
            if (base.CanDelete() == false)
            {
                return false;
            }

            var user = _userRepository.GetCurrent();

            if (Entity.Department.Id == user.Department.Id)
            {
                return Entity.Status.IsEq(StatusEnum.AoDraft, StatusEnum.AoOnCompletion);
            }

            return false;
        }

    }
}
