﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityAddOrUpdateContractStory : IStory<ActivityAddOrUpdateContractStoryContext, ActivityContractDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;
        private readonly ISessionNHibernateFactory _factory;

        public ActivityAddOrUpdateContractStory(
            IRepositoryRui repository,
            IMapperBase mapper,
            ISessionNHibernateFactory factory)
        {
            _mapper = mapper;
            _repository = repository;
            _factory = factory;
        }

        public ActivityContractDto Execute(ActivityAddOrUpdateContractStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityContractDto> ExecuteAsync(ActivityAddOrUpdateContractStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var activity = _repository.Get<PlansActivity>(context.ActivityId);

            if (activity == null)
            {
                throw new IsRuiException("Не найдено мероприятие");
            }

            var contract = _mapper.Map(context, new ActivityContract());
            contract.PlansActivity = activity;
            contract.ChangeDate = DateTime.Now;

            using (var session = _factory.OpenSession())
            {
                if (context.Id.HasValue)
                {
                    await session.UpdateAsync(contract);
                }
                else
                {
                    await session.InsertAsync(contract);
                }
            }

            return _mapper.Map(contract, new ActivityContractDto());
        }
    }
}
