﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    class NPAKindMap : ClassMapHiLo<NPAKind>
    {
        public NPAKindMap()
            : base("NPA_Kind")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "NPA_Kind_Name");
        }
    }
}