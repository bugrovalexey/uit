﻿namespace GB.Data.Directories
{
    public enum ExpenseDirectionTypeEnum
    {
        None = 0,

        /// <summary>Работы</summary>
        Works = 1,

        /// <summary>Товары</summary>
        Goods = 2
    }
}