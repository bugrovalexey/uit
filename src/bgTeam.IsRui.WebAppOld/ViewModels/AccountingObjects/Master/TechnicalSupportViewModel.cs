﻿using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class TechnicalSupportViewModel : StepBaseViewModel
    {
        public IEnumerable<TableTechnicalSupport> TechnicalSupports { get; set; }

        public TechnicalSupportViewModel(int id)
            : base(id, MasterItemEnum.TechnicalSupport, x => x.TechnicalSupports)
        {
            TechnicalSupports = Current.TechnicalSupports.Select(x => new TableTechnicalSupport(x));
        }

    }
}