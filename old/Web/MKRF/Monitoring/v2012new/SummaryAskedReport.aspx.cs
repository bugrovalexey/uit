﻿using GB;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxPivotGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GB.MKRF.Web.Monitoring.v2012new
{
    public partial class SummaryAskedReport : System.Web.UI.Page
    {
        Department CurrentDep
        {
            get
            {
                return RepositoryBase<Department>.Get(DepComboBox.Value);
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            DepComboBox.DataBind();
            YearComboBox.DataBind(); 

            PivotGrid.DataBind();

            base.OnPreRender(e);
        }


        protected void DepComboBox_DataBinding(object sender, EventArgs e)
        {
            var data = RepositoryBase<Department>.GetAll(x => x.Id != EntityBase.Default).OrderBy(x => x.Name);

            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = data;
            box.SelectedIndex = 0;
        }

        protected void YearComboBox_DataBinding(object sender, EventArgs e)
        {
            var plans = PlanRepository.GetAll(x => x.Department.Id == (int)DepComboBox.Value && x.PlanType == ExpObjectTypeEnum.Plan2012new).OrderBy(x => x.Year.Value).ToList();

            Dictionary<int, string> list = new Dictionary<int, string>();

            foreach (var p in plans)
            {
                list.Add(p.Id, string.Concat("План за ", p.Year.Value, " г."));
            }

            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = list;
            box.SelectedIndex = 0;
        }

        protected void PivotGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var grid = sender as ASPxPivotGrid;

            var plan = Repository.PlanRepository.Get(YearComboBox.Value);

            grid.DataSource = plan.Activities.Select(x => new
            {
                IsNotPriority = (x.IsPriority ? 0 : 1),
                Code = x.Code,
                Name = x.Name,
                Type = x.ActivityType2.Name,
                IsIkt = x.IKTComponent == null ? null : x.IKTComponent.CodeName,
                NameIsIkt = (x.IKTObject == null) ? null : x.IKTObject.Name,
                Annotation = x.AnnotationData,
                Source = x.SourceFunding == null ? null : x.SourceFunding.Name,
                Responsible = x.Responsible == null ? null : x.Responsible.Name,
                Goals = GetGoals(x.PlansActivityGoals),

                GRBS = (x.GRBS != null) ? x.GRBS.CodeName : String.Empty,
                SectionKBK = (x.SectionKBK != null) ? x.SectionKBK.CodeName : String.Empty, // "Код по БК ПЗ/РЗ"
                ExpenseItem = (x.ExpenseItem != null) ? x.ExpenseItem.CodeName : String.Empty, // "Код по БК ЦС"
                WorkForm = (x.WorkForm != null) ? x.WorkForm.CodeName : String.Empty, // "Код по БК ВР"
//                ExpenditureItem = (x.ExpenditureItem != null) ? x.ExpenditureItem.CodeName : String.Empty, // "Код по БК КОСГУ"

                Contract = GetContracts(x.PlansStateContracts),

                VolY0 = x.PlansActivityVolY0,
                VolY1 = x.PlansActivityVolY1,
                VolY2 = x.PlansActivityVolY2,

                AddVolY0 = x.PlansActivityAddVolY0,
                AddVolY1 = x.PlansActivityAddVolY1,
                AddVolY2 = x.PlansActivityAddVolY2,

                FactDate = x.PlansActivityFactDate.HasValue ? x.PlansActivityFactDate.ToString() : string.Empty,
                FactVolY0 = x.PlansActivityFactVolY0,
                FactVolY1 = x.PlansActivityFactVolY1,
                FactVolY2 = x.PlansActivityFactVolY2,
            });
        }

        private string GetGoals(IList<PlansActivityGoal> list)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var g in list)
            {
                sb.Append(g.Name);
                sb.Append(';');
                sb.AppendLine();
            }

            return sb.ToString();
        }

        private object GetContracts(IList<PlansStateContract> list)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var c in list)
            {
                if (c.Contract != null)
                {
                    sb.AppendFormat("{0} от {1} ({2})", c.Contract.Number, c.Contract.Sign_Date.ToShortDateString(), c.Contract.Products != null ? string.Join("; ", c.Contract.Products.Select(i => i.Name.Trim()).ToArray()) : string.Empty);
                    sb.AppendLine();
                }
            }

            return sb.ToString();
        }

        
    }
}