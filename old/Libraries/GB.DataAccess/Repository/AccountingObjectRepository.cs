﻿using System.Collections.Generic;
using GB.Data.AccountingObjects;
using System.Linq;

namespace GB.DataAccess.Repository
{
    public class AccountingObjectRepository : Repository<AccountingObject>
    {

        public IList<int> GetAllChildrensIds(int accountingObjId)
        {
            //TODO : Нужно собирать данные с дочерних ОУ
            return new int[] { accountingObjId };

            //return Session.CreateSQLQuery("select OU_ID from dbo.v_OU_Tree(?)").SetInt32(0, accountingObjId).List<int>();
        }

    }
}