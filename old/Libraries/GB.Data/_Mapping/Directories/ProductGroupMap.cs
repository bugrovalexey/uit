﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class ProductGroupMap : ClassMapHiLo<ProductGroup>
    {
        public ProductGroupMap()
            : base("Product_Group")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Product_Group_Name").Nullable();
        }
    }
}