//(function (window) {
//    //---------------------------------------------------------------------------------------------------
//    // public functions
//    function showSuccess(message, timeout) {
//        showMessage(message, { "background": "#6edf6e" }, timeout || 2000);
//    }

//    function showWarning(message, timeout) {
//        showMessage(message, { "background": "#ff6060" }, timeout || 5000);
//    }

//    function showResult(res) {
//        if (res.success)
//            notify.showSuccess(res.message);
//        else
//            notify.showWarning(res.message);
//    }

//    //---------------------------------------------------------------------------------------------------
//    // private functions
//    function showMessage(message, css, timeout) {

//        var _container = $("<div style='position: fixed; bottom: 10px; right: 10px; max-width: 500px;' class='popup_panel'></div>");
//        var _message = $("<div style='padding: 10px 14px; font-size: 1em; color: black;'></div>");
//        _message.appendTo(_container);

//        _message.css(css);
//        _message.html(message);

//        var _lastNotify = $(".popup_panel").last();
//        var top = 10 + (_lastNotify.length ? (parseInt(_lastNotify.css("bottom"), 10) + _lastNotify.height()) : 0);
////        _container.css("bottom", top);
//        _container.css("bottom", '20px;');

//        _container.appendTo(document.body).show();

//        window.setTimeout(function () {
//            _container.fadeOut(function () { _container.remove(); });
//        }, timeout);
//    }

//    // export
//    window.notify = { showSuccess: showSuccess, showWarning: showWarning, showResult: showResult };
//})(window);

(function (window) {
    //---------------------------------------------------------------------------------------------------
    // public functions
    function showSuccess(message, timeout) {
        Materialize.toast(message, timeout || 4000, 'success-toast');
    }

    function showWarning(message, timeout) {
        Materialize.toast(message, timeout || 20000, 'warning-toast');
    }

    function showResult(res) {
        if (res.success)
            notify.showSuccess(res.message);
        else
            notify.showWarning(res.message);
    }


    // export
    window.notify = { showSuccess: showSuccess, showWarning: showWarning, showResult: showResult };
})(window);