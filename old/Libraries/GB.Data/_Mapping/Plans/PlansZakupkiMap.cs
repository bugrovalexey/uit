﻿using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Mapping.Plans
{
    class PlansZakupkiMap : ClassMapBase<PlansZakupki>
    {
        public PlansZakupkiMap()
            : base("Plans_Zakupki")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Number, "Plans_Zakupki_Number").ReadOnly();
            Map(x => x.PublishDate, "Plans_Zakupki_Publish_Date");
            Map(x => x.Name, "Plans_Zakupki_Contract_Product_Name");
            Map(x => x.IzvNumb, "Plans_Zakupki_Izv_Numb");
            Map(x => x.IzvDate, "Plans_Zakupki_Izv_Date");
            Map(x => x.OpenDate, "Plans_Zakupki_Open_Date");
            Map(x => x.ConsDate, "Plans_Zakupki_Cons_Date");
            Map(x => x.ItogDate, "Plans_Zakupki_Itog_Date");
            Map(x => x.Site, "Plans_Zakupki_Site");

            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID");
            References<PlansZakupkiStatus>(x => x.Status, "Plans_Zakupki_Status_ID");
            References<OrderWay>(x => x.OrderWay, "Order_Way_ID");

            HasMany<PlansLots>(x => x.Lots).KeyColumns.Add("Plans_Zakupki_ID").Inverse();
        }
    }
}