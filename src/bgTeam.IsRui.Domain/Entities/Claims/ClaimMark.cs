﻿namespace bgTeam.IsRui.Domain.Entities.Claims
{
    using bgTeam.IsRui.Domain.Entities.Directories;

    public class ClaimMark : BaseIndicator
    {
        /// <summary>
        /// Обоснование
        /// </summary>
        public virtual string Justification { get; set; }

        /// <summary>
        /// True - когда источник формирования показателя - Госпрограмма
        /// False - Другой источник
        /// </summary>
        public virtual bool SourceIsStateProgram { get; set; }

        /// <summary>
        /// Наименование источника формирования
        /// </summary>
        public virtual string SourceName { get; set; }

        /// <summary>
        /// Пояснение связи с показателем госпрограммы
        /// </summary>
        public virtual string ExplanationRelationshipWithStateProgramMark { get; set; }

        /// <summary>
        /// Показатель гос. программы
        /// </summary>
        public virtual StateProgramMark StateProgramMark { get; set; }

        public virtual string AlgorithmOfCalculating { get; set; }
    }
}
