﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AODecisionToCreateDto
    {
        public int AoId { get; set; }

        public int Id { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string Name { get; set; }

        public DateTime? DateAdoption { get; set; }

        public string ParticleOfDocument { get; set; }

        public FilesDto File { get; set; }


    }
}
