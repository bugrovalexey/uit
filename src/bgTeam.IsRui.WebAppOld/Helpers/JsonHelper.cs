﻿namespace bgTeam.IsRui.WebApp.Helpers
{
    public static class JsonHelper
    {
        public static string ToJson(object value)
        {
            return System.Web.Helpers.Json.Encode(value);
        }
    }
}