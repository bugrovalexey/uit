﻿namespace bgTeam.IsRui.WebApp.Controllers
{
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Story.AccountingObjects;
    using bgTeam.IsRui.Story.Claims;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Authorize]
    [Route("api/[controller]/[action]")]
    public class ClaimsController
    {
        private readonly IStoryBuilder _builder;

        public ClaimsController(IStoryBuilder builder)
        {
            _builder = builder;
        }

        /// <summary>
        /// Возвращает заявки связанные с учреждением пользователя
        /// </summary>
        /// <param name="context">Пустой контекст</param>
        [HttpPost]
        public async Task<IEnumerable<ClaimDto>> GetList([FromBody] ClaimGetMyListStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ClaimDto>>();
        }

        /// <summary>
        /// Создание заявки
        /// </summary>
        /// <param name="context">
        /// ActivityId - Идентификатор связанного мероприятия
        /// </param>
        [HttpPost]
        public async Task<ClaimDto> Create([FromBody] ClaimCreateStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ClaimDto>();
        }

        /// <summary>
        /// Получение информации по заявке
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<ClaimInfoDto> Info([FromBody] ClaimInfoStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ClaimInfoDto>();
        }

        /// <summary>
        /// Удаление заявки
        /// </summary>
        /// <param name="context">
        /// Id - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<bool> Delete([FromBody] ClaimDeleteStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получение содержания заявки
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<ClaimContentDto> GetContent([FromBody] ClaimGetContentStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ClaimContentDto>();
        }

        /// <summary>
        /// Замена содержания заявки
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// Content - Новое содержание заявки
        /// </param>
        [HttpPost]
        public async Task<bool> SetContent([FromBody] ClaimSetContentStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получение обоснования заявки
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<ClaimJustificationDto> GetJustification([FromBody] ClaimGetJustificationStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ClaimJustificationDto>();
        }

        /// <summary>
        /// Замена обоснования заявки
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// Justification - Новое обоснование заявки
        /// </param>
        [HttpPost]
        public async Task<bool> SetJustification([FromBody] ClaimSetJustificationStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить показатели заявки
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<IEnumerable<ClaimIndicatorDto>> GetIndicators([FromBody] ClaimGetIndicatorsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ClaimIndicatorDto>>();
        }

        /// <summary>
        /// Создать или обновить показатель
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<ClaimIndicatorDto> CreateOrUpdateIndicator([FromBody] ClaimCreateOrUpdateIndicatorStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ClaimIndicatorDto>();
        }

        /// <summary>
        /// Удалить показатель заявки
        /// </summary>
        /// <param name="context">
        /// IndicatorId - Идентификатор показателя
        /// </param>
        [HttpPost]
        public async Task<bool> DeleteIndicator([FromBody] ClaimDeleteIndicatorStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить индикаторы заявки
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<IEnumerable<ClaimMarkDto>> GetMarks([FromBody] ClaimGetMarksStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ClaimMarkDto>>();
        }

        /// <summary>
        /// Создать или обновить индикатор
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<ClaimMarkDto> CreateOrUpdateMark([FromBody] ClaimCreateOrUpdateMarkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ClaimMarkDto>();
        }

        /// <summary>
        /// Удалить индикатор заявки
        /// </summary>
        /// <param name="context">
        /// MarkId - Идентификатор индикатора
        /// </param>
        [HttpPost]
        public async Task<bool> DeleteMark([FromBody] ClaimDeleteMarkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить документы заявки
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<IEnumerable<ClaimDocumentDto>> GetDocuments([FromBody] ClaimGetDocumentsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ClaimDocumentDto>>();
        }

        /// <summary>
        /// Создать или обновить документ
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<ClaimDocumentDto> CreateOrUpdateDocument([FromBody] ClaimCreateOrUpdateDocumentStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ClaimDocumentDto>();
        }

        /// <summary>
        /// Удалить документ заявки
        /// </summary>
        /// <param name="context">
        /// DocumentId - Идентификатор документа
        /// </param>
        [HttpPost]
        public async Task<bool> DeleteDocument([FromBody] ClaimDeleteDocumentStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получение базовой информации по заявке
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<ClaimBaseInfoDto> GetBaseInfo([FromBody] ClaimGetBaseInfoStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ClaimBaseInfoDto>();
        }

        /// <summary>
        /// Получение актов по заявке
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<IEnumerable<ClaimActDto>> GetActs([FromBody] ClaimGetActsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ClaimActDto>>();
        }

        /// <summary>
        /// Создать или обновить акт
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<ClaimActDto> CreateOrUpdateAct([FromBody] ClaimCreateOrUpdateActStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ClaimActDto>();
        }

        /// <summary>
        /// Удалить акт
        /// </summary>
        /// <param name="context">
        /// Id - Идентификатор акта
        /// </param>
        [HttpPost]
        public async Task<bool> DeleteAct([FromBody] ClaimDeleteActStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получение объёма работ по заявке
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<IEnumerable<ClaimScopeOfWorkDto>> GetScopeOfWork([FromBody] ClaimGetScopeOfWorkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ClaimScopeOfWorkDto>>();
        }

        /// <summary>
        /// Создать или обновить объём работ
        /// </summary>
        /// <param name="context">
        /// Id - Идентификатор объёма работ
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<ClaimScopeOfWorkDto> CreateOrUpdateScopeOfWork([FromBody] ClaimCreateOrUpdateScopeOfWorkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ClaimScopeOfWorkDto>();
        }

        /// <summary>
        /// Удалить объём работ
        /// </summary>
        /// <param name="context">
        /// Id - Идентификатор объёма работ
        /// </param>
        [HttpPost]
        public async Task<bool> DeleteScopeOfWork([FromBody] ClaimDeleteScopeOfWorkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить заключение к заявке
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<WorkflowUserConclusionDto> GetConclusion([FromBody] ClaimGetUserCommentStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<WorkflowUserConclusionDto>();
        }

        /// <summary>
        /// Создать или обновить заключение
        /// </summary>
        /// <param name="context">
        /// Id - Идентификатор заключения
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<WorkflowUserConclusionDto> CreatOrUpdateConclusion([FromBody] ClaimCreateOrUpdateUserCommentStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<WorkflowUserConclusionDto>();
        }

        /// <summary>
        /// Добавить файл к заключению
        /// </summary>
        /// <param name="context">
        /// Id - Идентификатор заключения
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<FilesDto> AddFileConclusion([FromBody] ClaimAddFileUserCommentStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<FilesDto>();
        }

        /// <summary>
        /// Удалить файл у заключения
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> DeleteFileConclusion([FromBody] FileDeleteStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить историю заключений
        /// </summary>
        /// <param name="context">
        /// ClaimId - Идентификатор заявки
        /// </param>
        [HttpPost]
        public async Task<IEnumerable<ConclusionHistoryDto>> GetConclusionHistory([FromBody] ClaimGetConclusionHistoryStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ConclusionHistoryDto>>();
        }
    }
}
