﻿using GB.Data.AccountingObjects;
using System.Collections.Generic;

namespace GB.MKRF.Repository
{
    public class InformationInteractionRepository : Repository<InformationInteraction>
    {
        public IList<InformationInteraction> GetAllByAccountingObject(int? accountingObjectId)
        {
            return GetAllEx(q => q
                .Fetch(x => x.Type).Eager
                .Where(x => x.AccountingObject.Id == accountingObjectId));
        }
    }
}