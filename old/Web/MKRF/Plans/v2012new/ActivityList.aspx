﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/PlanCard.master" AutoEventWireup="true"
    CodeBehind="ActivityList.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.ActivityList" %>

<%@ Import Namespace="GB.Repository" %>

<%@ Register Src="~/Common/GridColumnManager.ascx" TagName="GridColumnManager" TagPrefix="gcm" %>
<%@ Import Namespace="GB.MKRF.Entities.Admin" %>
<%@ Register TagPrefix="ex" Namespace="GB.MKRF.Web.Common" Assembly="WebMKRF" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxSplitter" Assembly="DevExpress.Web.v14.1, Version=14.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxcp" Namespace="DevExpress.Web.ASPxCallback" Assembly="DevExpress.Web.v14.1, Version=14.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script src="/Content/GB/Scripts/jquery.json-2.3.js"></script>
    <style>
        .dxgvCommandColumnItem_MetropolisBlue
        {
            display: block;
            margin: 3px 5px;
        }

        #ctl00_ctl00_center_center_Splitter_1_CC
        {
            width: 100% !important;
        }

        .separatorActivityList
        {
            display: none;
        }

        .spanPrevNextButtons
        {
            float: right;
            padding-right: 10px;
        }

        .spanPrevNextButtons > span
        {
            width: 16px;
            height: 16px;
            display: inline-block;
            cursor: pointer;
        }

        span.spanPrevButton
        {
            background: url('/Content/GB/Styles/Imgs/prev_blue.png') no-repeat;
        }

        span.spanNextButton
        {
            background: url('/Content/GB/Styles/Imgs/next_blue.png') no-repeat;
        }

        span.none
        {
            display: none;
        }

        .actCodeStyle
        {
            width: 30px;
        }

        .isAgreeStyle
        {
            width: 90px;
        }

        .unallocatedBtn {
            position: relative;
        }

        .unallocatedBtn span.dx-vam {
            color: red;
            font-size: 14px;
            font-weight: 600;
            /*position: absolute;
            right: 2px;
            top: -3px;*/
        }
    </style>
    <script type="text/javascript" src="/Content/GB/Scripts/ActivityReadMode.js"></script>
    <script type="text/javascript">
        var visibleIndex = 0;
        function hfSave(e) {
            hf.Set('Name', NameMemo.GetText());
            hf.Set('IsPriority', IsPriorityCheckBox.GetValue());

            //if (<=IsRegistryIntegration ? "true" : "false"%>) {
            //    hf.Set('Registry', RegistryComboBox.GetValue());
            //hf.Set('RegistryName', RegistryComboBox.GetText());
            //}
        }



        function muActivitySave(e) {
            hf.Set('MUName', MUNameMemo.GetText());
            hf.Set('MUAnnotation', AnnotationMEmo.GetText());

            hf.Set('MUGRBS', GRBSComboBox.GetValue());
            hf.Set('MUCodeBK', CodeBKComboBox.GetValue());
            hf.Set('MUExpenseItem', ExpenseComboBox.GetValue());
            hf.Set('MUWorkForm', WorkFormComboBox.GetValue());

            hf.Set('Subentry', SubentryTextBox.GetValue());


            hf.Set('FactVolY0', FactVolY0SpinEdit.GetText());
            hf.Set('FactVolY1', FactVolY1SpinEdit.GetText());
            hf.Set('FactVolY2', FactVolY2SpinEdit.GetText());

            hf.Set('BudgetY0', BudgetY0SpinEdit.GetText());
            hf.Set('BudgetY1', BudgetY1SpinEdit.GetText());
            hf.Set('BudgetY2', BudgetY2SpinEdit.GetText());

            hf.Set('ExpenseAddY0', ExpenseAddY0SpinEdit.GetText());
            hf.Set('ExpenseAddY1', ExpenseAddY1SpinEdit.GetText());
            hf.Set('ExpenseAddY2', ExpenseAddY2SpinEdit.GetText());

            hf.Set('JointFundingY0', JointFundingY0SpinEdit.GetText());
            hf.Set('JointFundingY1', JointFundingY1SpinEdit.GetText());
            hf.Set('JointFundingY2', JointFundingY2SpinEdit.GetText());

            hf.Set('MURegions', ActivityMUGrid.cpRegions);

        }

        function PopupClose(idList, textList) {
            ActivityMUGrid.cpRegions = idList;
            var ctr = $("div.region_container");
            ctr[0].innerHTML = textList;

            RegionPopupControl.Hide();
        }
        function GetIds() {
            return ActivityMUGrid.cpRegions;
        }

        function RefreshPanel() {
            var key = listGrid.GetRowKey(visibleIndex);
            if (key) {
                ActivityDetailsCallbackPanel.PerformCallback(key);
            }
        }

        $(function () {
            $('.spanPrevButton').live("click", function () {
                if (visibleIndex == 1) return;
                listGrid.SelectRow(visibleIndex - 1, true);
                RefreshPanel();
            });
            $('.spanNextButton').live("click", function () {
                if ((listGrid.GetVisibleRowsOnPage() - 1) == visibleIndex) return;
                listGrid.SelectRow(visibleIndex + 1, true);
                RefreshPanel();
            });


            $('span.checkFavor').live("click", function () {
                favorChange(this);
            });

            function favorChange(s) {
                var actId = ($(s).data('id'));
                ActivityDetailsCallbackPanel.PerformCallback('favor;' + actId);
                if ($(s).hasClass("unfavorite")) {
                    $(s).removeClass("unfavorite")
                        .addClass("favorite")
                        .attr("title", "Отмечено как избранное");

                }
                else $(s).removeClass("favorite")
                         .addClass("unfavorite").
                         attr("title", "");

            }
        });

        function CheckAndSetSplitButtonVisible() {
            var start = listGrid.GetTopVisibleIndex();
            var key = listGrid.GetRowKey(start + 1);
            if (key) {
                styleBtnSplit.SetClientVisible(true);
            }
            else styleBtnSplit.SetClientVisible(false);
        }

        function changeAgree(s, e) {

            alert(s.GetChecked());
            var key = s.GetRowKey(e.visibleIndex);
        }

        function RefreshGrid(grid, key) {
            var f = hf.Get(key);

            if (f == 1) {
                grid.Refresh();
                hf.Set(key, 0);
            }
        }
    </script>



    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
    <div id="activityListGeneralPanel" class="page">
        <div class="detailTop">
            <dxe:ASPxButton ID="addButton" runat="server" ClientInstanceName="addButton" AutoPostBack="false"
                Text="Добавить" CssClass="topButton fleft">
                <ClientSideEvents Click="function(s,e) { listGrid.AddNewRow(); }" />
            </dxe:ASPxButton>

            <dxe:ASPxButton ID="printBtn" runat="server" ClientInstanceName="printBtn" AutoPostBack="false"
                Text="Печать ▼" CssClass="fleft topButton">
            </dxe:ASPxButton>
            <dxpc:ASPxPopupControl ID="PopupControl" runat="server" CloseAction="OuterMouseClick" PopupElementID="printBtn" PopupVerticalAlign="Below" PopupHorizontalAlign="LeftSides"
                ShowFooter="False" ShowHeader="False" Width="160">
                <ContentCollection>
                    <dxpc:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                        <br />
                        <span style="font-weight: bold; font-size: 1.2em;">Печатные формы: </span>
                        <br />
                        <%=getViewLinks(CurrentPlan)%>
                    </dxpc:PopupControlContentControl>
                </ContentCollection>
            </dxpc:ASPxPopupControl>

            <dxe:ASPxButton ID="ohterButton" runat="server" ClientInstanceName="ohterButton" AutoPostBack="false"
                Text="Иное ▼" CssClass="fleft topButton">
            </dxe:ASPxButton>
            <dxpc:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="ASPxPopupControl1" runat="server" CloseAction="OuterMouseClick" PopupElementID="ohterButton" PopupVerticalAlign="Below" PopupHorizontalAlign="LeftSides"
                ShowFooter="False" ShowHeader="False" Width="160">
                <ContentCollection>
                    <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                        <div id="otherLinksContainer">
                            <a href="<%=GetHtmlVersionUrl() %>" target="_blank">Выгрузить html-версию</a>
                            <% if (ExportEnabled)
                               {%>
                            <br />
                            <a href="<%= ExportPlan() %>" target="_blank">Экспортировать план</a>
                            <% }
                               if (ImportEnabled)
                               { %>
                            <br />
                            <a href="javascript://" onclick="$('#otherLinksContainer').hide(); $('#importFileUploaderContainer').show();">Импортировать план</a>
                            <% } %>
                            <%if (UpdateUviriEnabled)
                              { %>
                            <br />
                            <a href="javascript://" onclick="UpdateUviriPopup.Show();">Отправить в АИС УВИРИ</a>
                            <% } %>
                            <%if (UpdateFromUviriEnabled)
                              { %>
                            <br />
                            <a href="javascript://" onclick="UpdateUviriCallback.PerformCallback('fromUviriCheckSynch');">Обновить из АИС УВИРИ</a>
                            <% } %>
                        </div>
                        <div id="importFileUploaderContainer" style="display: none; height: 60px;">
                            Выберите файл для импорта
                            <cc:FileUploadControl2 runat="server" ID="importUploader" Mode="OneFile" ClientInstanceName="importUploader"
                                OnFileUploadComplete="importUploader_FileUploadComplete" ClientFileUploadComplete="location.reload(true);" />
                        </div>
                    </dxpc:PopupControlContentControl>
                </ContentCollection>
            </dxpc:ASPxPopupControl>
            <% if (AllowJoining)
               { %>
            <dxe:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="false" Text="Объединить" CssClass="fleft topButton">
                <ClientSideEvents Click="function(){showUnionPopup();}" />
            </dxe:ASPxButton>
            <script type="text/javascript">
                function showUnionPopup() {
                    ASPxPopupControl2.Show();
                    if (unionGrid.cpIsEmpty) {
                        unionGrid.Refresh();
                        //joinToComboBox.PerformCallback('update');
                    }
                }
                function unionGridCallbackComplete(s, e) {
                    if (s.cpJoinToItems && joinToComboBox.GetItemCount() <= 0) {
                        joinToComboBox.BeginUpdate();
                        var data = $.parseJSON(s.cpJoinToItems);
                        $.each(data, function (i, item) {
                            joinToComboBox.AddItem(data[i].Name, data[i].Id);
                        });
                        joinToComboBox.EndUpdate();

                        if (joinToComboBox.GetItemCount() > 0) {
                            joinToComboBox.SetSelectedIndex(0);
                        }
                    }
                }
                function doUnion(s, e) {
                    var selectedKeys = unionGrid.GetSelectedKeysOnPage();

                    if (selectedKeys.length <= 0) {
                        alert('Необходимо выбрать как минимум одно мероприятие');
                        return;
                    }

                    var joinTo = joinToComboBox.GetSelectedItem();
                    if (!joinTo) {
                        alert('Необходимо указать целевое мероприятие'); // маловероятно
                        return;
                    }
                    else {
                        joinTo = joinTo.value;
                    }

                    if (joinTo == 0) {
                        if (selectedKeys.length == 1) {
                            alert('Для присоединения к новому мероприятию необходимо выбрать два и более исходных мероприятий');
                            return;
                        }

                        if (!joiningNameTextBox.GetText()) {
                            alert('Для нового мероприятия необходимо указать его наименование');
                            return;
                        }
                    }

                    unionTask = [];

                    unionArgs = {
                        target: joinTo,
                        name: joiningNameTextBox.GetText(),
                        priority: joiningProirityCheckBox.GetChecked()
                    };

                    $.each(selectedKeys, function (i, item) {
                        unionTask.push(item);
                    });

                    unionArgs.total = unionTask.length;

                    LoadingPanel.Show();

                    doMerging();
                }

                function doMerging() {

                    var id = unionTask.pop();

                    if (id) {
                        LoadingPanel.SetText('Объединение мероприятий: ' + (unionArgs.total - unionTask.length) + ' из ' + unionArgs.total);
                        unionArgs.source = id;
                        callBackUnion.PerformCallback($.toJSON(unionArgs));
                    }
                    else {
                        LoadingPanel.SetText('Объединение завершено. Обновление страницы.');
                        location.reload();
                    }
                }

                function callBackUnionError(s, e) {
                    // ошибка
                    e.handled = true;
                    doMerging();
                }

                function callBackUnionComplete(s, e) {
                    if (!unionArgs.target) {
                        unionArgs.target = e.result;
                    }
                    doMerging();
                }

                var unionTask = [];
                var unionArgs = null;
            </script>


            <script type="text/javascript">

                var copyTask = [];
                var copyArgs = null;

                function doCopyActivity(s, e) {
                    var item = sourceActivityComboBox.GetSelectedItem();

                    if (!item) {
                        alert('Необходимо выбрать мероприятие');
                        return;
                    }

                    var activityId = item.value;

                    var contentUrl = "CopyActivity.aspx?id=" + activityId;
                    ASPxPopupControl4.SetContentUrl(contentUrl);

                    //var codeText = $('#pac_' + activityId).text();
                    var nameText = $('#pan_' + activityId).text();

                    //$("#CopyActivityHeaderCode").text(codeText);
                    $("#CopyActivityHeaderName").text(item.text);

                    ASPxPopupControl3.Hide();
                    ASPxPopupControl4.Show();

                    splitActivityPopup.Show();
                }

                //function copyActivityClick(activityId) {

                //    var contentUrl = "CopyActivity.aspx?id=" + activityId;
                //    ASPxPopupControl4.SetContentUrl(contentUrl);

                //    var codeText = $('#pac_' + activityId).text();
                //    var nameText = $('#pan_' + activityId).text();

                //    $("#CopyActivityHeaderCode").text(codeText);
                //    $("#CopyActivityHeaderName").text(nameText);
                //    ASPxPopupControl4.Show();

                //}



                function doCopy() {
                    var id = copyTask.pop();

                    if (id) {
                        LoadingPanel.SetText('Создание копии мероприятия № ' + (copyArgs.total - copyTask.length) + ' из ' + copyArgs.total);
                        copyCallback.PerformCallback('doCopy' + id);
                    }
                    else {
                        LoadingPanel.SetText('Копирование завершено. Обновление страницы.');
                        location.reload();
                    }
                }

                function copyCallbackComplete(s, e) {
                    if (e.parameter && e.parameter.indexOf('getActivities') == 0) {
                        if (e.result && sourceActivityComboBox.GetItemCount() <= 0) {
                            sourceActivityComboBox.BeginUpdate();
                            var data = $.parseJSON(e.result);
                            $.each(data, function (i, item) {
                                sourceActivityComboBox.AddItem(data[i].Name, data[i].Id);
                            });
                            joinToComboBox.EndUpdate();

                            if (sourceActivityComboBox.GetItemCount() > 0) {
                                sourceActivityComboBox.SetSelectedIndex(0);
                            }
                        }
                    }
                    else if (e.parameter && e.parameter.indexOf('doCopy') == 0) {
                        doCopy();
                    }
                }

                function copyCallbackError(s, e) {
                    e.handled = true;
                    if (e.parameter && e.parameter.indexOf('doCopy') == 0) {
                        doCopy();
                    }
                }

                function doSplit() {
                    ASPxPopupControl4.GetContentIFrameWindow().doWork();
                }

                        </script>
            <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CloseAction="OuterMouseClick" PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter"
                ShowFooter="False" ShowHeader="true" HeaderText=" "  Width="800" ClientInstanceName="ASPxPopupControl2" Modal="true">
                <ClientSideEvents Shown="" />
                <ContentCollection>
                    <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                        <dxwgv:ASPxGridView runat="server" ID="unionGrid" KeyFieldName="Id" Width="760" ClientInstanceName="unionGrid"
                            OnBeforePerformDataSelect="unionGrid_BeforePerformDataSelect"
                            OnCustomJSProperties="unionGrid_CustomJSProperties">
                            <Columns>
                                <dxwgv:GridViewDataCheckColumn Caption="Приоритетность" FieldName="IsNotPriority" GroupIndex="0" SortIndex="0" SortOrder="Ascending"
                                    Name="IsNotPriority" ShowInCustomizationForm="True" Visible="False">
                                    <PropertiesCheckEdit DisplayTextChecked="Мероприятия по информатизации, не относящиеся к приоритетным"
                                        DisplayTextUnchecked="Приоритетные мероприятия">
                                    </PropertiesCheckEdit>
                                </dxwgv:GridViewDataCheckColumn>
                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="30"></dxwgv:GridViewCommandColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Код" FieldName="ActCode" Width="50"></dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Наименование мероприятия" FieldName="Name"></dxwgv:GridViewDataTextColumn>
                            </Columns>
                            <Settings VerticalScrollableHeight="300" VerticalScrollBarMode="Visible" GroupFormat="{1} {2}" />
                            <SettingsBehavior AutoExpandAllGroups="true" />
                            <SettingsPager Mode="ShowAllRecords">
                                <PageSizeItemSettings Visible="false" ShowAllItem="true" />
                            </SettingsPager>
                            <ClientSideEvents EndCallback="unionGridCallbackComplete" />
                        </dxwgv:ASPxGridView>
                        <br />
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 5%; padding-right: 10px;">Присоединить&nbsp;к:</td>
                                <td>
                                    <dxe:ASPxComboBox Width="100%" runat="server" ID="joinToComboBox" ValueField="Id" TextField="Name"
                                        ValueType="System.Int32" ClientInstanceName="joinToComboBox">
                                        <ClientSideEvents SelectedIndexChanged="function(s,e){  if (s.GetValue() == 0)  $('#joiningParameters').show();  else  $('#joiningParameters').hide();}" />
                                    </dxe:ASPxComboBox>
                                </td>
                                <td style="width: 5%; padding-left: 10px">
                                    <dxe:ASPxButton runat="server" ID="btnUnion" AutoPostBack="false" Wrap="False" Text="Объединить выбранные мероприятия">
                                        <ClientSideEvents Click="doUnion" />
                                    </dxe:ASPxButton>
                                </td>
                            </tr>
                            <tr id="joiningParameters">
                                <td></td>
                                <td>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 5%; padding-right: 10px">Наименование:</td>
                                            <td>
                                                <dxe:ASPxTextBox Width="100%" runat="server" ID="joiningNameTextBox" ClientInstanceName="joiningNameTextBox">
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5%">Приоритетное:</td>
                                            <td>
                                                <dxe:ASPxCheckBox runat="server" ID="joiningProirityCheckBox" ClientInstanceName="joiningProirityCheckBox"></dxe:ASPxCheckBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </dxpc:PopupControlContentControl>
                </ContentCollection>
            </dxpc:ASPxPopupControl>
            <dxc:ASPxCallback runat="server" ID="callBackUnion" ClientInstanceName="callBackUnion" OnCallback="callBackUnion_Callback">
                <ClientSideEvents CallbackError="callBackUnionError" CallbackComplete="callBackUnionComplete" />
            </dxc:ASPxCallback>

            <dxe:ASPxButton ID="splitButton" runat="server" ClientInstanceName="splitButton" AutoPostBack="false" Text="Разъединить ▼" CssClass="fleft topButton"></dxe:ASPxButton>
            <dxpc:ASPxPopupControl ID="ASPxPopupControl3" ClientInstanceName="ASPxPopupControl3" runat="server" CloseAction="OuterMouseClick" PopupElementID="splitButton" PopupVerticalAlign="Below" PopupHorizontalAlign="LeftSides"
                ShowFooter="False" ShowHeader="False" Width="520">
                <ClientSideEvents Shown="function(s,e){copyCallback.PerformCallback('getActivities');}" />
                <ContentCollection>
                    <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                        <table style="width: 100%">
                            <tr>
                                <td>Мероприятие:</td>
                                <td style="width: 95%">
                                    <dxe:ASPxComboBox runat="server" ID="sourceActivityComboBox" ClientInstanceName="sourceActivityComboBox" Width="100%" TextField="Name" ValueField="Id" ValueType="System.Int32" LoadingPanelText="Загрузка..." ShowLoadingPanel="true">
                                    </dxe:ASPxComboBox>
                                </td>
                                <td>
                                    <dxe:ASPxButton runat="server" ID="doSplitButton" AutoPostBack="false" Text="Далее">
                                        <ClientSideEvents Click="function(s,e){ doCopyActivity(); }" />
                                    </dxe:ASPxButton>
                                </td>
                            </tr>
                        </table>
                        <dxc:ASPxCallback ID="copyCallback" runat="server" ClientInstanceName="copyCallback"
                            OnCallback="copyCallback_Callback">
                            <ClientSideEvents CallbackError="function(s,e){}"
                                CallbackComplete="copyCallbackComplete" />
                        </dxc:ASPxCallback>
                    </dxpc:PopupControlContentControl>
                </ContentCollection>
            </dxpc:ASPxPopupControl>

            <dxpc:ASPxPopupControl ID="ASPxPopupControl4" ClientInstanceName="ASPxPopupControl4" runat="server" Width="1150px" Height="800px"
                Modal="true" AllowDragging="false" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" HeaderText="Копирование мероприятия"
                ShowFooter="true" ShowHeader="true">
                <HeaderStyle Wrap="False" />
                <HeaderContentTemplate>
                    <div style="width:1110px; height:30px; overflow:hidden; text-overflow: ellipsis;">
                        <span id="CopyActivityHeaderCode" style="color:#C53939"></span>
                        <span id="CopyActivityHeaderName"></span>
                    </div>
                </HeaderContentTemplate>
                <FooterContentTemplate>
                    <table style="width:100%">
                        <tr>
                            <td class="form_edit_buttons" style="text-align:center">
                                 <span class="ok"><a onclick="doSplit()">Выполнить разъединение</a></span>
                            </td>
                        </tr>
                    </table>
                </FooterContentTemplate>
            </dxpc:ASPxPopupControl>
            <% } %>

            <div class="fleft topButton" style="padding-top: 5px;">
                Группировать по:
            </div>

            <dxe:ASPxComboBox runat="server" ID="GroupFields" ValueType="System.Int32" ClientInstanceName="GroupFields" CssClass="fleft topButton" SelectedIndex="0" Width="200px">
                <Items>
                    <dxe:ListEditItem Text="Приоритету" Value="0" />
                    <dxe:ListEditItem Text="Источнику финансирования" Value="1" />
                </Items>
                <ClientSideEvents SelectedIndexChanged="function(s) { listGrid.PerformCallback('GroupFields:'+s.GetValue()) }" />
            </dxe:ASPxComboBox>



            <%--<dxe:ASPxButton ID="GroupButton" runat="server" ClientInstanceName="GroupButton" AutoPostBack="false"
                Text="Группировать по ▼" CssClass="fleft topButton">
            </dxe:ASPxButton>
            <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CloseAction="OuterMouseClick" PopupElementID="GroupButton" PopupVerticalAlign="Below" PopupHorizontalAlign="LeftSides"
                ShowFooter="False" ShowHeader="False" Width="190">
                <ContentCollection>
                    <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                        

                        <div>
                            <a onclick=" listGrid.GroupBy('IsNotPriority');">Приоритету</a>
                            <br />
                            <a onclick="listGrid.GroupBy('IsAgree'); ">Источнику финансирования</a>
                        </div>

                    </dxpc:PopupControlContentControl>
                </ContentCollection>
            </dxpc:ASPxPopupControl>--%>

            <%--<dxhf:ASPxHiddenField runat="server" ID="hfFilterMode" ClientInstanceName="hfFilterMode">
                <ClientSideEvents Init="function(s,e){
                        s.Set('filterMode', 'all');
                    }"></ClientSideEvents>
            </dxhf:ASPxHiddenField>
            <dxe:ASPxButton ID="filterBtnAll" runat="server" ClientInstanceName="filterBtnAll" AutoPostBack="false"
                Text="Все" CssClass="topButton fleft filterBtn" GroupName="FilterMode" Checked="True">
                <ClientSideEvents
                    Init="function(s,e){
                 
                    }"
                    Click="function(s,e) { 
                        hfFilterMode.Set('filterMode', 'all');
                        listGrid.Refresh();
                    }" />
            </dxe:ASPxButton>
            <dxe:ASPxButton ID="filterBtnPriority" runat="server" ClientInstanceName="filterBtnPriority" AutoPostBack="false"
                Text="Приоритетные" CssClass="topButton fleft filterBtn" Style="margin-left: -1px;" GroupName="FilterMode">
                <ClientSideEvents Click="function(s,e) {
                        hfFilterMode.Set('filterMode', 'priority');
                        listGrid.Refresh();
                    }" />
            </dxe:ASPxButton>
            <dxe:ASPxButton ID="filterBtnNotPriority" runat="server" ClientInstanceName="filterBtnNotPriority" AutoPostBack="false"
                Text="Неприоритетные" CssClass="topButton fleft filterBtn" Style="margin-left: -1px;" GroupName="FilterMode">
                <ClientSideEvents Click="function(s,e) {
                        hfFilterMode.Set('filterMode', 'notpriority');
                        listGrid.Refresh();
                    }" />
            </dxe:ASPxButton>--%>

            <dxe:ASPxButton ID="styleBtnList" runat="server" ClientInstanceName="styleBtnList" AutoPostBack="false"
                CssClass="topButton fleft iconButton styleBtn" Image-Url="/Content/GB/Styles/Imgs/list_16.png" Image-ToolTip="Списком" Image-Height="14px"
                GroupName="ViewStyle" Checked="True">
                <ClientSideEvents
                    Click="function(s,e) {
                        listGrid.UnselectRows();
                        $('.separatorActivityList').css('display', 'none');
                        $('#activityListGeneralPanel').addClass('page');
                        Splitter.AdjustControl();

                        var activitydetailsPane = Splitter.GetPaneByName('ActivityDetailsPane');
                        activitydetailsPane.Collapse(activitydetailsPane);
                    }" />
            </dxe:ASPxButton>
            <dxe:ASPxButton ID="styleBtnSplit" runat="server" ClientInstanceName="styleBtnSplit" AutoPostBack="false"
                CssClass="topButton fleft iconButton styleBtn" Style="margin-left: -1px;" Image-Url="/Content/GB/Styles/Imgs/split_16.png"
                Image-ToolTip="с просмотром сведений" Image-Height="14px"
                GroupName="ViewStyle">
                <ClientSideEvents Click="function(s,e) {
                        $('#activityListGeneralPanel').removeClass('page');
                        Splitter.AdjustControl();

                        $('.separatorActivityList').css('display', 'table-cell');
                        var pane2 = Splitter.GetPaneByName('ActivityDetailsPane');
                        pane2.Expand();

                        var i = listGrid.GetTopVisibleIndex();
                        if(listGrid.IsGroupRow(i))
                            i++;

                        listGrid.SelectRow(i, true);

                        ActivityDetailsCallbackPanel.PerformCallback(listGrid.GetRowKey(i));
                    }" />
            </dxe:ASPxButton>

            <%--<dxe:ASPxCheckBox runat="server" ID="ChBEmpty" Text="только незаполненные" CssClass="fleft topButton">
                <ClientSideEvents CheckedChanged="function(s,e){ listGrid.Refresh(); }"></ClientSideEvents>
            </dxe:ASPxCheckBox>--%>

            <dxe:ASPxCheckBox runat="server" ID="ChBFavourites" Text="избранные" CssClass="fleft topButton">
                <ClientSideEvents CheckedChanged="function(s,e){ listGrid.Refresh(); }"></ClientSideEvents>
            </dxe:ASPxCheckBox>

            <dxe:ASPxButton ID="UnallocatedBtn" runat="server" ClientInstanceName="UnallocatedBtn" AutoPostBack="false" ToolTip="Нераспределённые" Text=""
                CssClass="topButton fleft iconButton styleBtn unallocatedBtn" Image-Url="/Content/GB/Styles/Imgs/unallocated_16.png" Image-ToolTip="Нераспределённые" OnClick="UnallocatedBtn_Click">
                <ClientSideEvents
                    Click="function(s,e) {
                    }" />
            </dxe:ASPxButton>
            <dxhf:ASPxHiddenField runat="server" ID="hfUnallocated" ClientInstanceName="hfUnallocated">
                <%--<ClientSideEvents Init="function(s,e){
                        s.Set('unallocated', false);
                    }"></ClientSideEvents>--%>
            </dxhf:ASPxHiddenField>

            <div style="float: right;">
                <gcm:GridColumnManager ID="_GridColumnManager" runat="server" GridName="listGrid" UserStyle="width:45px; top: 14px; position: relative;"
                    OnPreClickClearButtom="GroupFields.SetValue(0); ItemsPerPage.SetValue(10);" />
            </div>
        </div>

        <dx:ASPxSplitter ID="Splitter" ClientInstanceName="Splitter" ResizingMode="Live" runat="server" Width="100%">
            <Panes>
                <dx:SplitterPane Name="ActivityListPane" ScrollBars="Auto" AutoHeight="True" PaneStyle-Paddings-Padding="0px">
                    <ContentCollection>
                        <dx:SplitterContentControl runat="server">
                            <dxwgv:ASPxGridView runat="server" ID="listGrid" KeyFieldName="Id" Width="100%" AutoGenerateColumns="False"
                                ClientInstanceName="listGrid" OnBeforePerformDataSelect="listGrid_BeforePerformDataSelect"
                                OnRowDeleting="listGrid_RowDeleting" OnRowInserting="listGrid_RowInserting"
                                OnStartRowEditing="listGrid_StartRowEditing" OnRowUpdating="listGrid_RowUpdating"
                                OnCustomButtonCallback="listGrid_CustomButtonCallback" OnCommandButtonInitialize="listGrid_CommandButtonInitialize"
                                OnCustomButtonInitialize="listGrid_CustomButtonInitialize" OnHtmlDataCellPrepared="listGrid_HtmlDataCellPrepared"
                                OnCustomCallback="listGrid_CustomCallback" OnInit="listGrid_Init">
                                <Columns>
                                    <dxwgv:GridViewBandColumn Caption=" " ShowInCustomizationForm="false" FixedStyle="Left">
                                        <Columns>
                                            <dxwgv:GridViewDataTextColumn Caption=" " Name="favColumn" ShowInCustomizationForm="false"
                                                CellStyle-VerticalAlign="Middle" CellStyle-HorizontalAlign="Center" HeaderStyle-CssClass="bottomborder">
                                                <DataItemTemplate>
                                                    <%#GetFavStatus((bool)Eval("isFavor"),(int)Eval("Id"))%>
                                                </DataItemTemplate>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewCommandColumn Caption=" " Name="copyColumn" ShowInCustomizationForm="False"
                                                AllowDragDrop="False" CellStyle-VerticalAlign="Middle" ButtonType="Image" HeaderStyle-CssClass="bottomborder">
                                                <CustomButtons>
                                                    <dxwgv:GridViewCommandColumnCustomButton ID="copyButton" Image-ToolTip="Копировать" Image-Url="/Content/GB/Styles/Imgs/copy_16.png" />
                                                </CustomButtons>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False" HeaderStyle-CssClass="bottomborder"
                                                AllowDragDrop="False" ButtonType="Image" CellStyle-VerticalAlign="Middle">
                                                <EditButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/edit_16.png" Image-ToolTip="Изменить" />
                                                <ClearFilterButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/clear_16.png" Image-ToolTip="Очистить" />
                                            </dxwgv:GridViewCommandColumn>

                                            <dxwgv:GridViewCommandColumn Caption=" " Name="updownColumn" ShowInCustomizationForm="False" HeaderStyle-CssClass="bottomborder"
                                                AllowDragDrop="False" ButtonType="Image">
                                                <CustomButtons>
                                                    <dxwgv:GridViewCommandColumnCustomButton ID="upButton" Image-Url="/Content/GB/Styles/Imgs/up_16.png" Image-ToolTip="Вверх" Image-Height="12" />
                                                    <dxwgv:GridViewCommandColumnCustomButton ID="downButton" Image-Url="/Content/GB/Styles/Imgs/down_16.png" Image-ToolTip="Вниз" Image-Height="12" />
                                                </CustomButtons>
                                            </dxwgv:GridViewCommandColumn>
                                        </Columns>
                                        <HeaderStyle Border-BorderWidth="0px"></HeaderStyle>
                                    </dxwgv:GridViewBandColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Код" FieldName="ActCode" CellStyle-HorizontalAlign="Center" HeaderStyle-CssClass="actCodeStyle" FixedStyle="Left">
                                        <DataItemTemplate>
                                            <span id='pac_<%#Eval("Id")%>'><%#Eval("ActCode")%></span>
                                        </DataItemTemplate>
                                        <Settings AutoFilterCondition="Contains" />
                                        <Settings AllowSort="True" SortMode="Value" />
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Код строки" FieldName="Num" Visible="False"
                                        CellStyle-HorizontalAlign="Center">
                                        <Settings AllowSort="True" SortMode="Value" />
                                    </dxwgv:GridViewDataTextColumn>
                                    
                                    <dxwgv:GridViewDataTextColumn Caption="Код в АИС УВИРИ" FieldName="CoordCode" Visible="False"
                                        CellStyle-HorizontalAlign="Center">
                                        <Settings AllowSort="True" SortMode="Value" />
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataCheckColumn Caption="Согласовано" FieldName="IsAgree" CellStyle-HorizontalAlign="Center" HeaderStyle-CssClass="isAgreeStyle" FixedStyle="Left">
                                        <DataItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkIsAgree" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsAgree")) %>' OnInit="chk_Init">
                                            </dxe:ASPxCheckBox>
                                        </DataItemTemplate>
                                        <PropertiesCheckEdit DisplayTextChecked="Согласовано" DisplayTextUnchecked="Не согласовано" UseDisplayImages="false" />
                                        <%--<Settings AllowAutoFilter="False" />--%>
                                    </dxwgv:GridViewDataCheckColumn>

                                    <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Наименование мероприятия" FixedStyle="Left">
                                        <DataItemTemplate>
                                            <%#getLinkToActivityCard(Eval("Id"), Eval("Name").ToString(), Eval("IsShow").ToString(), (int ?)Eval("Composite"))%>
                                        </DataItemTemplate>
                                        <Settings AutoFilterCondition="Contains" />
                                        <Settings AllowSort="True" SortMode="Value" />
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn FieldName="Funding" Caption="Источник финансирования" CellStyle-HorizontalAlign="Center">
                                        <Settings AllowHeaderFilter="True" />
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn FieldName="DepartmentName" Visible="false" Name="DepartmentNameColumn"
                                        Caption="Департамент">
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn FieldName="Type" Caption="Тип" Visible="False">
                                        <Settings AllowHeaderFilter="True" />
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewBandColumn Caption="Объем планируемых бюджетных ассигнований, тыс. руб"
                                        Name="ExpenseVolFB" Visible="False">
                                        <Columns>
                                            <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="PlansActivityVolY0"
                                                Width="150px">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                </PropertiesTextEdit>
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />

                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="PlansActivityVolY1"
                                                Width="150px">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                </PropertiesTextEdit>
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="PlansActivityVolY2"
                                                Width="150px">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                </PropertiesTextEdit>
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                    </dxwgv:GridViewBandColumn>

                                    <dxwgv:GridViewBandColumn Caption="Дополнительная потребность, тыс. руб" Visible="False"
                                        Name="ExpenseAddVolFB">
                                        <Columns>
                                            <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="PlansActivityAddVolY0"
                                                Width="150px">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                </PropertiesTextEdit>
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="PlansActivityAddVolY1"
                                                Width="150px">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                </PropertiesTextEdit>
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="PlansActivityAddVolY2"
                                                Width="150px">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                </PropertiesTextEdit>
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                    </dxwgv:GridViewBandColumn>

                                    <dxwgv:GridViewBandColumn Caption="Объем фактически израсходованных бюджетных ассигнований, тыс. руб" Visible="False"
                                        Name="ExpenseFactVolFB">
                                        <Columns>
                                            <dxwgv:GridViewDataTextColumn Caption="текущий финансовый год" FieldName="PlansActivityFactVolY0"
                                                Width="150px">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                </PropertiesTextEdit>
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="отчетный финансовый год (текущий финансовый год-1)"
                                                FieldName="PlansActivityFactVolY1" Width="150px">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                </PropertiesTextEdit>
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="год, предшествующий отчетному финансовому году (текущий финансовый год-2"
                                                FieldName="PlansActivityFactVolY2" Width="150px">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                                                </PropertiesTextEdit>
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                    </dxwgv:GridViewBandColumn>

                                    <dxwgv:GridViewBandColumn Caption="Итого, тыс. руб" Name="ExpenseFactVolFB">
                                        <Columns>
                                            <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="VolY0">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="VolY1">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="VolY2">
                                                <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                                <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                    </dxwgv:GridViewBandColumn>

                                    <dxwgv:GridViewDataTextColumn FieldName="Id" ShowInCustomizationForm="False" Visible="False" />
                                    <%--<dxwgv:GridViewDataTextColumn FieldName="Funding" Caption="Источник финансирования" ShowInCustomizationForm="False" Visible="False" />--%>
                                    <%--<dxwgv:GridViewDataTextColumn FieldName="Num" SortIndex="2" SortOrder="Ascending"
                                        ShowInCustomizationForm="False" Visible="False" >
                                    </dxwgv:GridViewDataTextColumn>--%>

                                    <dxwgv:GridViewDataTextColumn FieldName="ZakCount" Caption="Количество закупок" Visible="false">
                                        <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn FieldName="ActivityInitialRegNums" Caption="Рег. №" Visible="false">
                                        <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataCheckColumn Caption="Приоритетность" FieldName="IsNotPriority" GroupIndex="0" SortIndex="0" SortOrder="Ascending"
                                        Name="IsNotPriority" ShowInCustomizationForm="True" Visible="False">
                                        <PropertiesCheckEdit DisplayTextChecked="Мероприятия по информатизации, не относящиеся к приоритетным"
                                            DisplayTextUnchecked="Приоритетные мероприятия">
                                        </PropertiesCheckEdit>
                                    </dxwgv:GridViewDataCheckColumn>

                                    <dxwgv:GridViewCommandColumn Caption=" " Name="delColumn" ShowInCustomizationForm="False" AllowDragDrop="False" ButtonType="Image">
                                        <DeleteButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/delete_16.png" Image-ToolTip="Удалить" />
                                    </dxwgv:GridViewCommandColumn>

                                </Columns>
                                <Settings ShowFooter="True" GroupFormat="{1} {2}" ShowFilterRow="true" ShowGroupFooter="VisibleAlways" GridLines="Horizontal" />
                                <SettingsBehavior ConfirmDelete="True" AutoExpandAllGroups="True" AllowSelectSingleRowOnly="True" AllowSort="false" />
                                <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
                                <SettingsEditing Mode="EditForm" />
                                <SettingsPager AlwaysShowPager="true" Summary-Text="Страница {0} из {1}" />
                                <Styles>
                                    <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                    <Footer BackColor="#FDF3D8" />
                                </Styles>
                                <GroupSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY0"
                                        ShowInGroupFooterColumn="PlansActivityVolY0" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY1"
                                        ShowInGroupFooterColumn="PlansActivityVolY1" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY2"
                                        ShowInGroupFooterColumn="PlansActivityVolY2" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY0"
                                        ShowInGroupFooterColumn="PlansActivityAddVolY0" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY1"
                                        ShowInGroupFooterColumn="PlansActivityAddVolY1" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY2"
                                        ShowInGroupFooterColumn="PlansActivityAddVolY2" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY0"
                                        ShowInGroupFooterColumn="PlansActivityFactVolY0" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY1"
                                        ShowInGroupFooterColumn="PlansActivityFactVolY1" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY2"
                                        ShowInGroupFooterColumn="PlansActivityFactVolY2" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY0" ShowInGroupFooterColumn="VolY0"
                                        SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY1" ShowInGroupFooterColumn="VolY1"
                                        SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY2" ShowInGroupFooterColumn="VolY2"
                                        SummaryType="Sum" />
                                </GroupSummary>
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem ShowInColumn="Name" DisplayFormat="Количество: {0}" SummaryType="Count" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY0"
                                        ShowInColumn="PlansActivityVolY0" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY1"
                                        ShowInColumn="PlansActivityVolY1" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY2"
                                        ShowInColumn="PlansActivityVolY2" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY0"
                                        ShowInColumn="PlansActivityAddVolY0" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY1"
                                        ShowInColumn="PlansActivityAddVolY1" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY2"
                                        ShowInColumn="PlansActivityAddVolY2" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY0"
                                        ShowInColumn="PlansActivityFactVolY0" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY1"
                                        ShowInColumn="PlansActivityFactVolY1" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY2"
                                        ShowInColumn="PlansActivityFactVolY2" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY0" ShowInColumn="VolY0"
                                        SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY1" ShowInColumn="VolY1"
                                        SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY2" ShowInColumn="VolY2"
                                        SummaryType="Sum" />
                                </TotalSummary>
                                <Templates>
                                    <EditForm>
                                        <table class="form_edit">
                                            <tr>
                                                <td class="form_edit_desc">
                                                    <span class="required"></span>
                                                    Наименование:
                                                </td>
                                                <td class="form_edit_input">
                                                    <dxe:ASPxMemo ID="NameMemo" runat="server" ClientInstanceName="NameMemo" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup%>"
                                                        OnDataBinding="NameMemo_DataBinding">
                                                        <ValidationSettings ErrorDisplayMode="Text">
                                                            <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxMemo>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="form_edit_desc">Приоритетное мероприятие:
                                                </td>
                                                <td class="form_edit_input">
                                                    <dxe:ASPxCheckBox runat="server" ID="IsPriorityCheckBox" ClientInstanceName="IsPriorityCheckBox"
                                                        OnDataBinding="IsPriorityCheckBox_DataBinding">
                                                    </dxe:ASPxCheckBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="form_edit_buttons">
                                                    <span class="ok">
                                                        <a onclick="listGrid.UpdateEdit();">Сохранить</a>
                                                    </span>
                                                    <span class="cancel">
                                                        <a onclick="listGrid.CancelEdit();">Отмена</a>
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </EditForm>
                                </Templates>
                                <ClientSideEvents
                                    CustomButtonClick="function(s,e){
                                        //if(e.buttonID == 'copyButton')
                                        //{
                                        //    e.processOnServer = false;
                                        //    var key = s.GetRowKey(e.visibleIndex);
                                        //    copyActivityClick(key);
                                        //}
                                        //else
                                        {
                                            e.processOnServer = true;
                                        }
                                    }"
                                    BeginCallback="function(s,e){ 
                                        if (e.command=='UPDATEEDIT') hfSave(e); 
                                        if(e.command=='DELETEROW') hf.Set('UPDATE_ITOGO', 1);
                                        if(e.command=='CUSTOMBUTTON') hf.Set('UPDATE_GRID', 1);
                                    }"
                                    EndCallback="function(s,e){ 

                                        RefreshGrid(summaryGrid, 'UPDATE_ITOGO');
                                        RefreshGrid(s, 'UPDATE_GRID');
                                        CheckAndSetSplitButtonVisible();
                                        var activitydetailsPane = Splitter.GetPaneByName('ActivityDetailsPane');
                                        if (!activitydetailsPane.IsCollapsed()){
                                            var key = s.GetRowKey(1);
                                            if(key){
                                                ActivityDetailsCallbackPanel.PerformCallback(key);
                                                s.SelectRow(1, true);
                                            }
                                        }
                                    }"
                                    RowClick="function(s,e){
                                        var activitydetailsPane = Splitter.GetPaneByName('ActivityDetailsPane');
                                        if (!activitydetailsPane.IsCollapsed()){
                                            var key = s.GetRowKey(e.visibleIndex);
                                            if(key){
                                                ActivityDetailsCallbackPanel.PerformCallback(key);
                                                s.SelectRow(e.visibleIndex, true);
                                            }
                                        }
                                    }"
                                    SelectionChanged="function(s,e){
                                        s.SelectRow(e.visibleIndex, true);
                                        visibleIndex = e.visibleIndex;
                                    }" />
                            </dxwgv:ASPxGridView>
                        </dx:SplitterContentControl>
                    </ContentCollection>
                </dx:SplitterPane>
                <dx:SplitterPane Name="ActivityDetailsPane" ScrollBars="Auto" Collapsed="True" AutoHeight="True" Size="50%">
                    <ContentCollection>
                        <dx:SplitterContentControl runat="server">
                            <dxcp:ASPxCallbackPanel runat="server" ID="ActivityDetailsCallbackPanel" ClientInstanceName="ActivityDetailsCallbackPanel"
                                OnCallback="ActivityDetailsCallbackPanel_OnCallback" />
                        </dx:SplitterContentControl>
                    </ContentCollection>
                    <Separator>
                        <SeparatorStyle CssClass="separatorActivityList" />
                    </Separator>
                </dx:SplitterPane>
            </Panes>
            <%--<ClientSideEvents PaneResized="function(s,e) { debugger; }" />--%>
        </dx:ASPxSplitter>

        <div style="padding-top: 5px; text-align: right">
            <div style="display: inline-block">
                Мероприятий на странице:
            </div>
            <div style="display: inline-block; position: relative; top: 8px;">
                <dxe:ASPxComboBox runat="server" ID="ItemsPerPage" ValueType="System.Int32" ClientInstanceName="ItemsPerPage" SelectedIndex="0" Width="50px">
                    <Items>
                        <dxe:ListEditItem Text="10" Value="10" />
                        <dxe:ListEditItem Text="25" Value="25" />
                        <dxe:ListEditItem Text="50" Value="50" />
                        <dxe:ListEditItem Text="Все" Value="0" />
                    </Items>
                    <ClientSideEvents SelectedIndexChanged="function(s) { listGrid.PerformCallback('ItemsPerPage:'+s.GetValue()) }" />
                </dxe:ASPxComboBox>
            </div>
        </div>

    </div>

    <div class="page">
        <p class="header_grid">
            Итого
        </p>
        <dxwgv:ASPxGridView runat="server" ID="summaryGrid" Width="100%" AutoGenerateColumns="False" Settings-GroupFormat="{1}"
            ClientInstanceName="summaryGrid" OnBeforePerformDataSelect="summaryGrid_BeforePerformDataSelect">
            <Columns>
                <dxwgv:GridViewDataTextColumn Caption=" " FieldName="Name" VisibleIndex="0" Width="180px">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Тип" GroupIndex="0" FieldName="Group">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="на очередной финансовый год" FieldName="Y0"
                    VisibleIndex="1">
                    <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                    </PropertiesTextEdit>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="на первый год планового периода" FieldName="Y1"
                    VisibleIndex="2">
                    <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                    </PropertiesTextEdit>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="на второй год планового периода" FieldName="Y2"
                    VisibleIndex="3">
                    <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                    </PropertiesTextEdit>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Итого" FieldName="Y" VisibleIndex="4">
                    <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                    </PropertiesTextEdit>
                </dxwgv:GridViewDataTextColumn>

            </Columns>
            <Styles>
                <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                </Header>
            </Styles>
        </dxwgv:ASPxGridView>

        <dxcp:ASPxCallbackPanel runat="server" ID="Lim242Panel" ClientInstanceName="Lim242Panel" OnCallback="Lim242Panel_Callback">
            <PanelCollection>
                <dxp:PanelContent>
                    <%#getExpense242Labels()%>
                </dxp:PanelContent>
            </PanelCollection>
        </dxcp:ASPxCallbackPanel>

        <%--<p runat="server" id="expense242TitleLabel">
            <b>Лимит финансирования по 242 виду расходов согласно бюджету</b>
            <br />
        </p>--%>
    </div>

    <dxlp:ASPxLoadingPanel runat="server" ID="LoadingPanel" ClientInstanceName="LoadingPanel" Modal="True">
    </dxlp:ASPxLoadingPanel>
    
    <dxpc:ASPxPopupControl ID="UpdateUviriCheckSynchPopup" runat="server" CloseAction="OuterMouseClick" PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter"
                            ShowFooter="True" ShowHeader="False" ClientInstanceName="UpdateUviriCheckSynchPopup" Modal="true" Width="400px" >
                            <ClientSideEvents />
                            <FooterTemplate>
                                <dxe:ASPxButton ID="UpdateUviriCancelButton" runat="server" Text="Отмена" AutoPostBack="False" Style="margin: 4px; float: right;"
                                    ClientSideEvents-Click="function(s, e) { UpdateUviriCheckSynchPopup.Hide();}" />
                                <dxe:ASPxButton ID="UpdateUviriButton2" runat="server" Text="Отправить всё равно" AutoPostBack="False" Style="margin: 4px; float: right;"
                                    ClientSideEvents-Click="function(s, e) { UpdateUviriCallback.PerformCallback('toUviri');}" 
                                    ClientVisible="False" ClientInstanceName="UpdateUviriButton2"/>
                                <dxe:ASPxButton ID="UpdateFromUviriButton2" runat="server" Text="Обновить всё равно" AutoPostBack="False" Style="margin: 4px; float: right;"
                                    ClientSideEvents-Click="function(s, e) { UpdateUviriCallback.PerformCallback('fromUviri');}" 
                                    ClientVisible="False" ClientInstanceName="UpdateFromUviriButton2"/>
                                <div class="clear"></div>
                            </FooterTemplate>
                            <ClientSideEvents></ClientSideEvents>
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="UpdateUviriPopup" runat="server" CloseAction="OuterMouseClick" PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter"
                            ShowFooter="False" ShowHeader="False" Width="800" ClientInstanceName="UpdateUviriPopup" Modal="true">
                            <ClientSideEvents Shown="" />
                            <ContentCollection>
                                <dxpc:PopupControlContentControl ID="UpdateUviriPopupContentControl" runat="server">
                                    <h2>Выберите мероприятия для переноса</h2>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 30%;">Тип мероприятия: 
                                                <dxe:ASPxListBox SelectionMode="CheckColumn" runat="server"
                                                    ID="ActivityTypeListBox" ClientInstanceName="ActivityTypeListBox"
                                                    OnPreRender="ListBox_PreRender" OnDataBinding="ActivityTypeListBox_DataBinding"
                                                    ValueField="ID" TextField="Name" Width="100%" Height="150px">
                                                </dxe:ASPxListBox>
                                            </td>
                                            <td style="width: 20px;"></td>
                                            <td style="width: 30%;">Источник финансирования: 
                                                <dxe:ASPxListBox SelectionMode="CheckColumn" runat="server"
                                                    ID="SourceFundingListBox" ClientInstanceName="SourceFundingListBox"
                                                    OnPreRender="ListBox_PreRender" OnDataBinding="SourceFundingListBox_DataBinding"
                                                    ValueField="ID" TextField="Name" Width="100%" Height="150px">
                                                </dxe:ASPxListBox>
                                            </td>
                                            <td style="width: 20px;"></td>
                                            <td style="width: 30%;">Приоритетность:
                                                <dxe:ASPxRadioButtonList ID="PriorityRadioButtonList" runat="server" RepeatLayout="Table"
                                                    Width="100%" Height="150px">
                                                    <RadioButtonStyle CssClass="radioButtonStyle">
                                                    </RadioButtonStyle>
                                                    <Items>
                                                        <dxe:ListEditItem Text="Все" />
                                                        <dxe:ListEditItem Text="Приоритетные" Value="1" />
                                                        <dxe:ListEditItem Text="Неприоритетные" Value="0" />
                                                    </Items>
                                                    <ClientSideEvents Init="function(){
                                                            $('.radioButtonStyle').closest('.dxe').css('vertical-align', 'top');
                                                        }" />
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="padding: 4px">
                                                <dxe:ASPxButton ID="UpdateUviriButton" AutoPostBack="False" runat="server" Text="Отправить" Style="float: right">
                                                    <ClientSideEvents Click="function(){
                                                            UpdateUviriCallback.PerformCallback('toUviriCheckSynch');
                                                        }"></ClientSideEvents>
                                                </dxe:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </dxpc:PopupControlContentControl>
                            </ContentCollection>
    </dxpc:ASPxPopupControl>
    <dxcp:ASPxCallback runat="server" ID="UpdateUviriCallback" ClientInstanceName="UpdateUviriCallback" OnCallback="UpdateUviriCallback_Callback">
                            <ClientSideEvents CallbackComplete="function(s,e){
                                if (e.result) {
                                    if (e.result == 'UviriNotSynchronized' || e.result == 'MkrfNotSynchronized'){
                                        var systemName = '';
                                        if(e.result == 'UviriNotSynchronized'){
                                            systemName = 'АИС УВИРИ';
                                            UpdateFromUviriButton2.SetVisible(false);
                                            UpdateUviriButton2.SetVisible(true);
                                        }
                                        else{
                                            systemName = 'АИС Координация Культура России';
                                            UpdateUviriButton2.SetVisible(false);
                                            UpdateFromUviriButton2.SetVisible(true);
                                        }
                                        var html = '<p>Существуют изменения, сделанные в '+ systemName +' в мероприятиях:</p>';
                                        $.each(s.cpNotSynchronizedActivities, function(i, e){
                                           html += '<p>'+e+'</p>';
                                        });
                                        html += '<p>В случае обновления они будут утеряны. Вы уверены что хотите обновить эти мероприятия?</p>';
                                        UpdateUviriCheckSynchPopup.SetContentHTML(html);
                                        UpdateUviriCheckSynchPopup.Show();
                                    }
                                    else{
                                        alert(e.result);
                                    }
                                }  else{
                                    location.reload(true);
                                }}"
                                BeginCallback="function(s,e){LoadingPanel.Show();}"
                                EndCallback="function(s,e){LoadingPanel.Hide();}"></ClientSideEvents>
    </dxcp:ASPxCallback>
</asp:Content>
