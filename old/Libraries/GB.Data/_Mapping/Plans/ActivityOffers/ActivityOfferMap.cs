﻿using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Plans;
using Uviri.MKRF.Entities.Plans.ActivityOffers;

namespace Uviri.MKRF.Mapping.Plans.ActivityOffers
{
    public class ActivityOfferMap : ClassMapBase<ActivityOffer>
    {
        public ActivityOfferMap()
            : base("Activity_Offer_Inform")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "Activity_Offer_Inform_Code");
            Map(x => x.Name, "Activity_Offer_Inform_Name");
            Map(x => x.YearEnd, "Activity_Offer_Inform_Year");
            Map(x => x.Subprograms, "Activity_Offer_Inform_Subprogrammes");
            Map(x => x.Subjects, "Activity_Offer_Inform_Subjects");

            References<Plan>(x => x.Plan, "Plans_ID");
            References<User>(x => x.Responsible, "Users_ID");

            HasMany(x => x.OfferApf).KeyColumns.Add("Activity_Offer_Inform_ID");
            HasMany(x => x.OfferDoc).KeyColumns.Add("Activity_Offer_Inform_ID");
            HasMany(x => x.OfferFunction).KeyColumns.Add("Activity_Offer_Inform_ID");
            HasMany(x => x.OfferIS).KeyColumns.Add("Activity_Offer_Inform_ID");
            HasMany(x => x.OfferSharingIS).KeyColumns.Add("Activity_Offer_Inform_ID");
        }
    }
}
