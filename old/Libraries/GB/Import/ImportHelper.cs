﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using Core.Entity.Import;
using Core.Import.SUIS;
using Core.Repository;

namespace Core.Import
{
    public class ImportHelper
    {
        public static string ImportIKT(suiswsClient client)
        {
            var header = new HeaderType();

            var msg = new GetModelMsgType
            {
                Message = new MessageType
                {
                    Sender = new orgExternalType(),
                    Recipient = new orgExternalType(),
                    Originator = new orgExternalType(),
                },
                MessageData = new MessageDataType()
            };

            var response = client.GetModel(ref header, msg);

            if (response != null &&
                response.MessageData != null &&
                response.MessageData.AppData != null &&
                !string.IsNullOrEmpty(response.MessageData.AppData.XMLData))
            {
                var x = new XmlDocument();
                x.LoadXml(response.MessageData.AppData.XMLData);

#if DEBUG
                try
                {
                    File.WriteAllText(string.Concat(System.AppDomain.CurrentDomain.BaseDirectory, "Temp\\ImportIKT.xml"), response.MessageData.AppData.XMLData);
                }
                catch (Exception exp)
                {
                    Logger.Debug(exp.Message);
                }
#endif

                var navigator = x.CreateNavigator();
                XPathNodeIterator iterator = null; ;

                try
                {
                    iterator = navigator.Select("AM/CLASSIFICATION");
                }
                catch (Exception exp)
                {
                    return "Полученные данные не соответствуют описанию";
                }

                var result = new List<CLASSIFICATION>();

                while (iterator.MoveNext())
                {
                    if (iterator.Current == null)
                        continue;

                    try
                    {
                        result.Add(ReferenceTranslator.GetCanonicalDataFromXmlData<CLASSIFICATION>(iterator.Current.OuterXml));
                    }
                    catch
                    {
                        continue;
                    }
                }

                foreach (var cl in result)
                {
                    RepositoryBase<RegistryImportIKT>.SaveOrUpdate(new RegistryImportIKT()
                    {
                        Name = cl.CLASS_NAME,
                        Orig_ID = cl.CLASS_ID,
                        Orig_Par_ID = cl.CLASS_PAR_ID,
                        Code = cl.CLASS_CODE,
                        Status = cl.CLASS_STATUS
                    });
                }
                return string.Format("Загружено и обработано {0} ИКТ", result.Count);
            }

            return "Нет данных";
        }

        public static string ImportRegistry(suiswsClient client)
        {
            var header = new HeaderType
            {
                NodeId = "reqnodeid",
                MessageId = "reqmsgid",
                TimeStamp = DateTime.Now,
                MessageClass = MessageClassType.REQUEST
            };

            var msg = new GetAssetMsgType
            {
                Message = new MessageType
                {
                    Sender = new orgExternalType()
                    {
                        Code = "sendercode",
                        Name = "sendername"
                    },
                    Recipient = new orgExternalType()
                    {
                        Code = "SUIS",
                        Name = "СУИС"
                    },
                    TypeCode = TypeCodeType.GSRV,
                    Date = DateTime.Now,
                    ExchangeType = "?"
                },
                MessageData = new MessageDataType()
            };

            var response = client.GetAsset(ref header, msg);

            if (response != null &&
                response.MessageData != null &&
                response.MessageData.AppData != null &&
                !string.IsNullOrEmpty(response.MessageData.AppData.XMLData))
            {
                var x = new XmlDocument();
                x.LoadXml(response.MessageData.AppData.XMLData);

#if DEBUG
                try
                {
                    File.WriteAllText(string.Concat(System.AppDomain.CurrentDomain.BaseDirectory, "Temp\\ImportRegistry.xml"), response.MessageData.AppData.XMLData);
                }
                catch (Exception exp)
                {
                    Logger.Debug(exp.Message);
                }
#endif

                var navigator = x.CreateNavigator();
                XPathNodeIterator iterator = null;

                try
                {
                    iterator = navigator.Select("AM/IKT");
                }
                catch (Exception exp)
                {
                    return "Полученные данные не соответствуют описанию";
                }


                var result = new List<IKT>();

                while (iterator.MoveNext())
                {
                    if (iterator.Current == null)
                        continue;

                    try
                    {
                        result.Add(ReferenceTranslator.GetCanonicalDataFromXmlData<IKT>(iterator.Current.OuterXml));
                    }
                    catch
                    {
                        continue;
                    }
                }

                foreach (var ikt in result)
                {
                    RepositoryBase<RegistryImport>.SaveOrUpdate(new RegistryImport()
                    {
                        Number = ikt.IKT_NUM,
                        Name = ikt.IKT_NAME,
                        FullName = ikt.IKT_FULLNAME,
                        FgisNum = ikt.IKT_FGIS,
                        IKT = RepositoryBase<RegistryImportIKT>.GetAll(y => y.Orig_ID == ikt.CLASS_ID).SingleOrDefault()
                    });
                }
                return string.Format("Загружено и обработано {0} объектов учета", result.Count);
            }

            return "Нет данных";
        }
    }

    public class ReferenceTranslator
    {
        private static readonly Encoding _encoding = Encoding.UTF8;

        /// <summary>
        /// Возвращает каноническое представление элемента справочника из строки XML.
        /// </summary>
        /// <typeparam name="T">Тип канонической формы элемента справочника.</typeparam>
        /// <param name="xml">Данные элемента справочника в виде строки XML.</param>
        /// <returns>Объект канонического представления элемента справочника.</returns>
        public static T GetCanonicalDataFromXmlData<T>(string xml)
        {
            using (MemoryStream memoryStream = new MemoryStream(_encoding.GetBytes(xml)))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(memoryStream);
            }
        }
    }
}
