﻿using System;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Administrator
{
    public partial class Dictionary : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            ctlDictionary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["CoordConnectionString"].ConnectionString;

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            (Master as AdminMaster).SetTabActivity("Dictionary");

            ctlDictionary.UserId = UserRepository.GetCurrent().Id;
        }
    }
}