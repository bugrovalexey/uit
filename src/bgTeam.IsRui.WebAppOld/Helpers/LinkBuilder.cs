﻿using bgTeam.IsRui.Domain;
using System.Text;
using System.Web;
using System.Web.Routing;

namespace bgTeam.IsRui.WebApp.Helpers
{
    public static class LinkBuilder
    {
        //static readonly System.Web.Mvc.UrlHelper Helper =
        //  new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext); //такой вариант иногда падает с NullReferenceException
        private static System.Web.Mvc.UrlHelper Helper
        {
            get
            {
                //TODO : Каждый раз создается новый объект
                return new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext);
            }
        }

        public static string Url(string actionName, string controllerName, object routeValues = null)
        {
            return Helper.Action(actionName, controllerName, routeValues);
        }

        public static string Url(string actionName, string controllerName, RouteValueDictionary routeValues = null)
        {
            return Helper.Action(actionName, controllerName, routeValues);
        }

        public static string Link(string text, string url, string onclick = null, bool inNewTab = false, string @class = null)
        {
            return string.Format("<a href='{0}' target='{2}' onclick='{3}' class='{4}'>{1}</a>", url, text, inNewTab ? "_blank" : "_self", onclick, @class);
        }

        public static string Link(IEntityName file)
        {
            return Link(file.Name, GetUrlFile(file.Id));
        }

        public static string Link(string text, int fileId)
        {
            return Link(text, GetUrlFile(fileId));
        }

        public static string ActionLink(string text, string actionName, string controllerName)
        {
            return ActionLink(text, actionName, controllerName, null);
        }

        public static string ActionLink(string text, string actionName, string controllerName, object routeValues, string @class = null)
        {
            return string.Format("<a href='{0}' class='{2}'>{1}</a>",
                Helper.Action(actionName, controllerName, routeValues), text, @class);
        }

        public static string  ActionLink(string text, string actionName, string controllerName, RouteValueDictionary routeValues, string @class = null)
        {
            return string.Format("<a href='{0}' class='{2}'>{1}</a>",
                Helper.Action(actionName, controllerName, routeValues), text, @class);
        }

        public static string AjaxLink(string text, string callbackFunctionName, string confirmMessage, string url, string @class = null)
        {
            return string.Format("<a href='{0}' class='command {4}' data-ajax-success='{2}' data-ajax-method='Post' data-ajax-confirm='{3}' data-ajax='true'>{1}</a>",
                    url, text, callbackFunctionName, confirmMessage, @class);
        }

        public static string AjaxLink(string text, string callbackFunctionName, string confirmMessage, string actionName, string controllerName, object routeValues)
        {
            return AjaxLink(text, callbackFunctionName, confirmMessage, Helper.Action(actionName, controllerName, routeValues));
        }

        private static string AjaxDeleteLink(string url, string refreshTableName, string text)
        {
            return string.Format("<a href='{0}' class='icon icon-delete tooltipped' data-ajax-success='{1}_onDelete' data-ajax-method='Post' data-tooltip='Удалить' data-ajax-confirm='{2}' data-ajax='true'></a>",
                    url, refreshTableName, text);
        }

        public static string AjaxDeleteLink(string actionName, string controllerName, object routeValues, string refreshTableName,
            string text = "Вы действительно хотите удалить запись?")
        {
            return AjaxDeleteLink(Helper.Action(actionName, controllerName, routeValues), refreshTableName, text);
        }

        public static string DisabledLink(string text)
        {
            return string.Format("<span style='color: #BBBBBB;'>{0}</span>", text);
        }

        public static string PopupLink(string text, string url)
        {
            return
                string.Format(
                    "<a onclick='popup.popover({{url: \"{0}\"}}); return false;' href='#' class='command'>{1}</a>", url,
                    text);
        }

        public static string PopupLink(string text, string actionName, string controllerName, object routeValues)
        {
            return
                string.Format(
                    "<a onclick='popup.popover({{url: \"{0}\"}}); return false;' href='#' class='command'>{1}</a>",
                    Helper.Action(actionName, controllerName, routeValues), text);
        }

        /// <summary>
        /// Формирует ссылку на файл
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="urlRoot"></param>
        /// <returns></returns>
        public static string GetUrlFile(object id, string name = null, string urlRoot = "~/Files/Download/")
        {
            if (id == null)
            {
                return UrlHelper.Resolve(urlRoot);
            }

            StringBuilder builder = new StringBuilder();
            builder.Append(UrlHelper.Resolve(urlRoot));
            builder.Append(id);

            if (!string.IsNullOrEmpty(name))
            {
                builder.Append("/");
                builder.Append(name);
            }

            return builder.ToString();
        }

    }
}