﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class WorkFormToYearMap : ClassMapBase<WorkFormToYear>
    {
        public WorkFormToYearMap()
            : base("Work_Form_Years", "v_", true)
        {
            //Table("v_");

            //Id(x => x.Id, "Work_Form_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "Work_Form_Code");
            Map(x => x.CodeName, "Work_Form_CodeName");
            Map(x => x.Year, "Year");
        }
    }
}
