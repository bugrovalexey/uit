﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;

    public class AccountingObjectChangeStageStory : IStory<AccountingObjectChangeStageStoryContext, AccountingObjectChangeStageDto>
    {
        private static readonly Dictionary<string, AccountingObjectsStageEnum> _dictionary;

        static AccountingObjectChangeStageStory()
        {
            _dictionary = typeof(AccountingObjectsStageEnum)
                .GetFields(BindingFlags.Public | BindingFlags.Static)
                .Where(x => x.IsLiteral)
                .Select(x => new
                {
                    key = x.GetCustomAttribute<DescriptionAttribute>()?.Description,
                    value = (AccountingObjectsStageEnum)x.GetValue(null)
                })
                .Where(x => !string.IsNullOrEmpty(x.key))
                .ToDictionary(x => x.key, x => x.value);
        }

        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;

        public AccountingObjectChangeStageStory(
            IRepositoryRui repository,
            ISessionNHibernateFactory factory)
        {
            _repository = repository;
            _factory = factory;
        }

        public AccountingObjectChangeStageDto Execute(AccountingObjectChangeStageStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<AccountingObjectChangeStageDto> ExecuteAsync(AccountingObjectChangeStageStoryContext context)
        {
            var ao = _repository.GetExisting<AccountingObject>(context.Id);

            if (Enum.TryParse<AccountingObjectsStageEnum>(context.Stage, out var stage) ||
                _dictionary.TryGetValue(context.Stage, out stage))
            {
                ao.Stage = stage;
                using (var session = _factory.OpenSession())
                {
                    await session.UpdateAsync(ao);
                    return new AccountingObjectChangeStageDto
                    {
                        Id = context.Id,
                        Stage = context.Stage
                    };
                }
            }

            throw new ArgumentException($"Стадия работы с объкетом учёта с наименованием \"{context.Stage}\" - не найдена!", nameof(context.Stage));
        }
    }
}
