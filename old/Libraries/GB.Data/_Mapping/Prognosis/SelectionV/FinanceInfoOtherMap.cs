﻿using Uviri.MKRF.Entities.Prognosis.SelectionV;

namespace Uviri.MKRF.Mapping.Prognosis.SelectionV
{
    public class FinanceInfoOtherMap : ClassMapBase<FinanceInfoOther>
    {
        public FinanceInfoOtherMap()
            : base("MF5_30_13")
        {
//            Table("w_");

//            Id(x => x.Id, "MF5_30_13_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF5_30_13_Insert
//                                  @MF5_30_13_Num = ?
//                                 ,@MF5_30_13_Name = ?
//                                 ,@MF5_30_13_Prev = ?
//                                 ,@MF5_30_13_Cur = ?
//                                 ,@MF5_ID = ?
//                                 ,@MF5_30_13_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF5_30_13_Update
//                                  @MF5_30_13_Num = ?
//                                 ,@MF5_30_13_Name = ?
//                                 ,@MF5_30_13_Prev = ?
//                                 ,@MF5_30_13_Cur = ?
//                                 ,@MF5_ID = ?
//                                 ,@MF5_30_13_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF5_30_13_Delete
//						  @MF5_30_13_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "MF5_30_13_Num");
            Map(x => x.Name, "MF5_30_13_Name");
            Map(x => x.Previous, "MF5_30_13_Prev");
            Map(x => x.Current, "MF5_30_13_Cur");

            References<FinanceInfo>(x => x.FinanceInfo, "MF5_ID").Not.Nullable();
        }
    }
}
