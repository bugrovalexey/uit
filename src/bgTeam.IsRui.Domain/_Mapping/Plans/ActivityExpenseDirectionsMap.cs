﻿using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Plans;

namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    internal class ActivityExpenseDirectionsMap : ClassMapHiLo<ActivityExpenseDirections>
    {
        public ActivityExpenseDirectionsMap()
            : base("Plans_Activity_ED")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.ExpenseVolY0, "Plans_Activity_ED_VB_Y0");
            Map(x => x.ExpenseVolY1, "Plans_Activity_ED_VB_Y1");
            Map(x => x.ExpenseVolY2, "Plans_Activity_ED_VB_Y2");

            Map(x => x.ExpenseAddVolY0, "Plans_Activity_ED_Add_Y0");
            Map(x => x.ExpenseAddVolY1, "Plans_Activity_ED_Add_Y1");
            Map(x => x.ExpenseAddVolY2, "Plans_Activity_ED_Add_Y2");

            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID");
            References<ExpenseDirection>(x => x.ExpenseDirection, "Expense_Direction_ID").NotFound.Ignore();
        }
    }
}