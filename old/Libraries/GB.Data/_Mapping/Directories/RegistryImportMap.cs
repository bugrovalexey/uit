﻿using GB;
using GB.Entity.Import;

namespace GB.MKRF.Mapping.Directories
{
    public class RegistryImportMap : ClassMapBase<RegistryImport>
    {
        public RegistryImportMap()
            : base("Registry_Import")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Registry_Import_Name");
            Map(x => x.Number, "Registry_Import_Number");

            Map(x => x.FgisNum, "Registry_Import_Fgis_Number");
            Map(x => x.FullName, "Registry_Import_Full_Name");

            References<RegistryImportIKT>(x => x.IKT, "Registry_Import_IKT_ID");
        }
    }
}
