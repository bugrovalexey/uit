﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class SoftOfficeTypeMap : ClassMapBase<SoftOfficeType>
	{
        public SoftOfficeTypeMap()
            : base("Soft_Office_Type")
        {
            //Table("w_");
            //Id(x => x.Id, "Soft_Office_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));
            
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Soft_Office_Type_Name");
        }
    }
}