﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityContract : EntityBase
    {
        public virtual string Name { get; set; }

        public virtual string Number { get; set; }

        public virtual DateTime? BeginDate { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual DateTime? ChangeDate { get; set; }

        public virtual PlansActivity PlansActivity { get; set; }
    }
}
