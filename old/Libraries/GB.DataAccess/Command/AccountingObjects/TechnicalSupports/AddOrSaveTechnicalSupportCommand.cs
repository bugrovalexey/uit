﻿using GB.Data.AccountingObjects.WorksAndGoods;
using GB.Data.Directories;
using GB.DataAccess.Command.AccountingObjects.CharacteristicOfTechnicalSupports;
using GB.DataAccess.Command.AccountingObjects.WorkAndGood;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;
using GB.DataAccess.Services.Args;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.TechnicalSupports
{
    public class AddOrSaveTechnicalSupportCommand : ICommand<AddOrSaveTechnicalSupportContext, TechnicalSupport>
    {
        ISessionFactoryGb factory;

        public AddOrSaveTechnicalSupportCommand()
        {
            factory = new SessionFactory();
        }

        public TechnicalSupport Execute(AddOrSaveTechnicalSupportContext context)
        {
            TechnicalSupport technical;

            if (context.xId.HasValue && context.xId != 0)
                technical = new Repository<TechnicalSupport>().Get(context.xId.Value);
            else
            {
                technical = new TechnicalSupport();                
            }

            AutoMapper.Mapper.Map(context, technical);
            
            Manufacturer man;

            if (context.ManufacturerId.HasValue == false && string.IsNullOrWhiteSpace(context.ManufacturerName) == false)
            {
                var мanufacturerName = context.ManufacturerName.Trim();
                man = new Repository<Manufacturer>().GetAllEx(q => q.Where(x => x.Name == мanufacturerName)).FirstOrDefault();

                if (man == null)
                {
                    man = new BaseAddOrSaveCommand<AddOrSaveDirectoryContext, Manufacturer>().Execute(new AddOrSaveDirectoryContext()
                    //new CrudService<Manufacturer, EntityDirectoriesArgs>().Create(new EntityDirectoriesArgs()
                    {
                        Name = мanufacturerName,
                        xId = 0
                    });
                }
                
                technical.Manufacturer = man;
            }

            using (var session = factory.Create())
            {
                session.SaveOrUpdate(technical);
                session.Commit();
            }            

            if (!context.xId.HasValue || context.xId == 0)
            {
                new SetTechnicalSupportForCharacteristicsCommand().Execute(new SetTechnicalSupportForCharacteristicsContext
                {
                    CharacteristicsOfTechnicalSupportIds = context.CharacteristicsOfTechnicalSupportIds,
                    TechnicalSupport = technical
                });                
            }

            new AddOrSaveWorkAndGoodsCommand().Execute(new AddOrSaveWorkAndGoodsContext()
            {
                Type = CategoryTypeOfSupportEnum.TechnicalSupport,
                AccountingObjectId = context.AccountingObjectId,
                CategoryKindOfSupportId = context.CategoryKindOfSupportId,
                Name = context.Name,
                ObjectId = technical.Id,
                Summa = context.Summa,
                IsNew = !context.xId.HasValue || context.xId == 0
            });

            return technical;
        }
    }
}
