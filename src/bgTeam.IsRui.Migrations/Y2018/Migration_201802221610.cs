﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201802221615)]
    public class Migration_201802221610 : ForwardOnlyMigration
    {
        public override void Up()
        {

            Create.Table("OU_Contract")
                .WithColumn("OU_Contract_ID").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("OU_ID").AsInt32().NotNullable()
                .WithColumn("OU_Contract_Name").AsString().NotNullable()
                .WithColumn("OU_Contract_Number").AsString().NotNullable()
                .WithColumn("OU_Contract_BeginDate").AsDateTime().NotNullable()
                .WithColumn("OU_Contract_EndDate").AsDateTime().NotNullable()
                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                 .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Create.ForeignKey("FK_OU_Contract")
               .FromTable("OU_Contract").ForeignColumn("OU_ID")
               .ToTable("OU").PrimaryColumn("OU_ID");

        }
    }
}
