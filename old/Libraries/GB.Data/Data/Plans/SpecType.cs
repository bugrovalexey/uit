﻿using GB.Data.Directories;

namespace GB.Data.Plans
{
    /// <summary>Тип работы</summary>
    public class SpecType : EntityBase
    {
        public SpecType()
        {
        }

        public SpecType(PlansSpec spec)
        {
            PlanSpec = spec;
        }

        /// <summary>Спецификация плана</summary>
        public virtual PlansSpec PlanSpec { get; set; }

        /// <summary>Наименование характеристики</summary>
        public virtual string CharName { get; set; }//null

        /// <summary>Ед. измерения</summary>
        public virtual string CharUnit { get; set; }//null

        /// <summary>Наименование характеристики</summary>
        public virtual string CharValue { get; set; }//null

        /// <summary>Аналог</summary>
        public virtual string Analog { get; set; }//null

        /// <summary>Количество </summary>
        public virtual int Quantity { get; set; }//null

        /// <summary>Цена аналога</summary>
        public virtual double AnalogPrice { get; set; }

        /// <summary>Стоимость</summary>
        public virtual double? Cost { get; set; }//null

        /// <summary>
        /// Объем бюджетных ассигнований в очередной финансовый год
        /// </summary>
        public virtual double ExpenseVolumeYear0 { get; set; }

        /// <summary>
        /// Объем бюджетных ассигнований на первый год планового периода
        /// </summary>
        public virtual double ExpenseVolumeYear1 { get; set; }

        /// <summary>
        /// Объем бюджетных ассигнований на второй год планового периода
        /// </summary>
        public virtual double ExpenseVolumeYear2 { get; set; }

        /// <summary>
        /// комментирий для отчета по плану
        /// </summary>
        public virtual string Comment { get; set; }

        /// <summary>
        /// Вид расходов
        /// </summary>
        public virtual ExpenseDirection ReportExpenseDirection { get; set; }
    }
}