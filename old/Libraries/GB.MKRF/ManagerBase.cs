﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF
{
    public class ManagerBase<THandler, TType>
    {
        readonly object syncRoot = new object();
        readonly Dictionary<Type, THandler> handlers = new Dictionary<Type, THandler>();

        /// <summary>Использовать через Singleton</summary>
        //protected ManagerBase()
        //{
        //}

        public THandler GetHandler<T>() where T : TType
        {
            THandler handler;
            lock (syncRoot)
            {
                handlers.TryGetValue(typeof(T), out handler);
            }
            return handler;
        }

        public void RegisterHandler<T>(THandler handler) where T : TType
        {
            lock (syncRoot)
            {
                handlers[typeof(T)] = handler;
            }
        }
    }
}
