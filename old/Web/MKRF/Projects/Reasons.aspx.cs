﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.Controls;
using GB.Extentions;
using GB.Handlers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Projects;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.Data;
using GB.Controls.Extensions;
using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Web.Projects
{
    //Проект - Основания
    public partial class Reasons : System.Web.UI.Page
    {
        ProjectController controller = new ProjectController();

        Project CurrentProject
        {
            get { return ((EditCard)Master).CurrentProject; }
        }

        //ProjectReasonController reasonController;
        //ProjectReasonController ReasonController
        //{
        //    get { return reasonController ?? (reasonController = new ProjectReasonController(CurrentProject)); }
        //}

        /*
        # Наименование государственной программы - выбор значений, соответствующего уровня (1),  из справочника государственных программ, с возможностью поиска. 
        # Наименование подпрограммы государственной программы -выбор значений, соответствующего уровня (2),  из справочника государственных программ, с возможностью поиска.
        # Наименование основного мероприятия государственной программы -выбор значений, соответствующего уровня (3),  из справочника государственных программ, с возможностью поиска.
        
        Справочник "Государственные программы" уникален для каждого ОГВ. Ведение справочника осуществляется через админку. 
        Справочник является иерархическим с тремя уровнями (сверху родительские - снизу дочерние элементы):
         * Государственные программы;
         * Подпрограммы государственной программы;
         * Мероприятия подпрограммы государственной программы.
        */

        protected override void OnPreRender(EventArgs e)
        {
            StateProgramComboBox.DataBind();
            StateSubProgramComboBox.DataBind();
            StateMainEventComboBox.DataBind();

            //ReasonsGrid.DataBind();

            base.OnPreRender(e);
        }

        protected void StateProgramComboBox_DataBinding(object sender, EventArgs e)
        {

        }

        protected void StateProgramComboBox_ValueChanged(object sender, EventArgs e)
        {

        }

        protected void StateSubProgramComboBox_DataBinding(object sender, EventArgs e)
        {

        }

        protected void StateMainEventComboBox_DataBinding(object sender, EventArgs e)
        {

        }

        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            //var controller = new PlansActivityController();

            //controller.Update3(new PlansActivityArgs()
            //{
            //    Id = CurrentActivity.Id,
            //    ResponsibleInformation = hf["ResponsibleInformationId"],
            //    ResponsibleTechnical = hf["ResponsibleTechnicalId"],
            //    ResponsibleFinancial = hf["ResponsibleFinancialId"],
            //    AccountingObject = hf["OUId"],
            //    Files = hf["ActivityFiles"]
            //});
        }

        #region ReasonGrid
        
        protected void ReasonsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            //ReasonsGrid.DataSource = CurrentProject.Reasons;
        }

        protected void ReasonsGrid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            //reasonController.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void ReasonsGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            //reasonController.Create(new PlansActivityReasonArgs()
            //{
            //    ActivityId = CurrentProject.Id,
            //    Name = hf["ReasonName"],
            //    Type = hf["ReasonTypeId"],
            //    Num = hf["ReasonNum"],
            //    Date = hf["ReasonDate"],
            //    Item = hf["ReasonItem"],
            //    Files = hf["ReasonFiles"]
            //});

            ReasonsGrid.GridCancelEdit(e);
        }

        protected void ReasonsGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            //reasonController.Update(new PlansActivityReasonArgs()
            //{
            //    Id = e.Keys[0],
            //    Name = hf["ReasonName"],
            //    Type = hf["ReasonTypeId"],
            //    Num = hf["ReasonNum"],
            //    Date = hf["ReasonDate"],
            //    Item = hf["ReasonItem"],
            //    Files = hf["ReasonFiles"]
            //});

            ReasonsGrid.GridCancelEdit(e);
        }

        protected void ReasonsGrid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            //reasonController.Delete(new PlansActivityReasonArgs()
            //{
            //    Id = e.Keys[0]
            //});

            ReasonsGrid.GridCancelEdit(e);
        }


        protected void ReasonNPANameTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxMemo box = (ASPxMemo)sender;
            //box.Text = reasonController.CurrentEdit == null ? string.Empty : reasonController.CurrentEdit.Name;
        }

        protected void ReasonNPANumTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = (ASPxTextBox)sender;

            //box.Text = reasonController.CurrentEdit == null ? string.Empty : reasonController.CurrentEdit.Num;
        }

        protected void ReasonNPADateDateEdit_DataBinding(object sender, EventArgs e)
        {
            ASPxDateEdit box = (ASPxDateEdit)sender;

            //box.Value = reasonController.CurrentEdit == null ? null : reasonController.CurrentEdit.Date;
        }

        protected void ReasonItemTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = (ASPxTextBox)sender;

            //box.Text = reasonController.CurrentEdit == null ? string.Empty : reasonController.CurrentEdit.Item;
        }

        protected void FileUploader_DataBinding(object sender, EventArgs e)
        {
            //if (reasonController.CurrentEdit != null)
            //{
            //    FileUploaderDataBinding(sender as FileUploadControl2, reasonController.CurrentEdit.Documents);
            //}


            //FileUploadControl2 uploader = sender as FileUploadControl2;
            //uploader.FileItemTemplate = "<a href=\"" + FileHandler.GetUrl(string.Empty) + "{id}\">{name}</a>";

            //if (reasonController.CurrentEdit != null)
            //{
            //    foreach (Document doc in reasonController.CurrentEdit.Documents)
            //    {
            //        uploader.SetValue(doc.FileName, doc.Id.ToString());
            //    }
            //}
        }
        
        #endregion

        protected string GetDownloadUrl(string FileInfo)
        {
            if (string.IsNullOrEmpty(FileInfo))
                return null;

            List<string> links = new List<string>();

            string[] files = FileInfo.Split(';');
            foreach (string f in files)
            {
                string[] pair = f.Split(':');
                links.Add(string.Format("<a href='{0}'>{1}</a>", FileHandler.GetUrl(pair[0]),
                                                                 pair.Length > 1 ? pair[1] : "Скачать"));
            }
            return string.Join("<br />", links);
        }

        private void FileUploaderDataBinding(FileUploadControl2 uploader, IList<Document> list)
        {
            uploader.FileItemTemplate = "<a href=\"" + FileHandler.GetUrl(string.Empty) + "{id}\">{name}</a>";

            foreach (Document doc in list)
            {
                uploader.SetValue(doc.FileName, doc.Id.ToString());
            }
        }

    }
}