﻿using GB.Data.AccountingObjects;

namespace GB._Mapping.AccountingObjects
{
    internal class AccountingObjectCharacteristicsValueMap : ClassMapHiLo<AccountingObjectCharacteristicsValue>
    {
        public AccountingObjectCharacteristicsValueMap()
            : base("OU_Charact_Value")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Norm, "OU_Charact_Value_Nominal");
            Map(x => x.Max, "OU_Charact_Value_Max");

            References(x => x.Characteristics, "OU_Charact_ID");
            References(x => x.Activity, "Plans_Activity_ID");
        }
    }
}