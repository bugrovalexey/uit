﻿using System;
using System.Collections.Generic;
using System.Linq;
using GB;
using GB.Entity.Directories.Zakupki;
using GB.Helpers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Plans;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Controllers;
using GB.MKRF.Repository;
using GB.MKRF.Web.Helpers;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class GoalsResults : System.Web.UI.Page
    {
        /// <summary>
        /// Текущее мероприятие
        /// </summary>
        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as ActivityCard).CurrentActivity;
            }
        }

        /// <summary>
        /// Разрешено редактировать мероприятие ?
        /// </summary>
        public bool EnableEdit
        {
            get
            {
                return (Master as ActivityCard).EnableEdit;
            }
        }

        public static string GetUrl(int ActivityId)
        {
            return UrlHelper.Resolve("~/Plans/v2012new/GoalsResults.aspx") + "?" + Plans.v2012new.ActivityCard.ActivityIdKey + "=" + ActivityId.ToString();
        }


        protected override void OnPreRender(EventArgs e)
        {
            PlansActivity activity = CurrentActivity;

            SCGrid.DataBind();
            SetReadOnly(EnableEdit);

            base.OnPreRender(e);
        }

        private void SetReadOnly(bool ro)
        {
            AddButton.Visible = ro;
            SCGrid.Columns["cmdColumn"].Visible = ro;
        }

        protected void callBackGR_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            if (Validation.Validator.Validate(CurrentActivity, ObjActionEnum.edit))
            {
                RepositoryBase<PlansActivity>.SaveOrUpdate(CurrentActivity);
            }
        }

        protected void ContractPanel_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            string[] strMass = e.Parameter.Split(';');

            var id = Convert.ToInt32(strMass[0]);
            var contract = RepositoryBase<Contract>.Get(id);

            hfContract["ContractId"] = id.ToString();
            hfContract["Number"] = contract.Number;
            hfContract["Product"] = string.Join("<br />", contract.Products.Select(i => i.Name).ToArray());
            hfContract["SignDate"] = contract.Sign_Date.ToShortDateString();
        }


        #region SCGrid
        PlansStateContract curentSC;

        protected void SCGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var plansContract = RepositoryBase<PlansStateContract>.GetAll(x => x.PlanActivity.Id == CurrentActivity.Id);
            if (plansContract != null && plansContract.Count > 0)
            {
                var source = plansContract.Select(x => new
                {
                    Id = x.Id,
                    Number = (x.Contract != null) ? x.Contract.Number : "",
                    Subject = (x.Contract != null && x.Contract.Products != null) ? string.Join("<br />", x.Contract.Products.Select(i => i.Name).ToArray()) : "",
                    ContractDate = (x.Contract != null) ? x.Contract.Sign_Date.ToShortDateString() : "",
                    DescriptionPreviousResult = x.DescriptionPreviousResult
                });

                (sender as ASPxGridView).DataSource = source;
            }
        }

        protected void SCGrid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            curentSC = RepositoryBase<PlansStateContract>.Get(e.EditingKeyValue);
        }

        protected void SCGrid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            PlansStateContract sc = new PlansStateContract(CurrentActivity);

            saveSC(sc);

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void SCGrid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            PlansStateContract sc = RepositoryBase<PlansStateContract>.Get((int)e.Keys[0]);

            saveSC(sc);

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void SCGrid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            PlansStateContract sc = RepositoryBase<PlansStateContract>.Get((int)e.Keys[0]);
            RepositoryBase<PlansStateContract>.Delete(sc);

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        private void saveSC(PlansStateContract sc)
        {
            bool unsave = (sc.Id == EntityBase.UnsavedId);

            if (unsave && (!hfGR.Contains("ContractId") || string.IsNullOrEmpty(hfGR["ContractId"] as string)))
                throw new Exception("Необходимо выбрать госконтракт");
            
            var id = 0;
            if (hfGR.Contains("ContractId") && hfGR["ContractId"] != null &&  Int32.TryParse(hfGR["ContractId"].ToString(), out id))
            {
                Contract contract = RepositoryBase<Contract>.Get(x => x.Id == id);
                sc.Contract = contract;
            }
            
            sc.DescriptionPreviousResult = hfGR["Description"] as string;
            sc.DescriptionPreviousResult = sc.DescriptionPreviousResult.Replace("\r\n", string.Empty);

            RepositoryBase<PlansStateContract>.SaveOrUpdate(sc);
            hfGR["ContractId"] = null;
            hfContract["ContractId"] = null;
        }

        protected void NumContractComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            List<Contract> list = RepositoryBase<Contract>.GetAll().ToList();

            box.DataSource = list;
            box.ValueField = "Id";
            box.TextField = "Number";

            if (null != curentSC)
            {
                box.SelectedItem = new ListEditItem(curentSC.Contract.Number);
            }
        }

        protected void ContractNumber_DataBinding(object sender, EventArgs e)
        {
            if (curentSC == null || curentSC.Contract == null)
                return;

            (sender as ASPxLabel).Text = curentSC.Contract.Number;
        }

        protected void ContractProduct_DataBinding(object sender, EventArgs e)
        {
            if (curentSC == null || curentSC.Contract == null)
                return;

            (sender as ASPxLabel).Text = string.Join("<br />", curentSC.Contract.Products.Select(i => i.Name).ToArray());
        }

        protected void ContractDate_DataBinding(object sender, EventArgs e)
        {
            if (curentSC == null || curentSC.Contract == null)
                return;

            (sender as ASPxLabel).Value = curentSC.Contract.Sign_Date;
        }

        protected void DescriptionTextBox_DataBinding(object sender, EventArgs e)
        {
            if (curentSC == null)
                return;

            (sender as ASPxMemo).Text = curentSC.DescriptionPreviousResult;
        }
        #endregion

        protected string GetContractUrl()
        {
            if (CurrentActivity.Department.Parent != null)
                return "../../Selectors/Contracts.aspx?spz=" + CurrentActivity.Department.Parent.SPZ;

            return "../../Selectors/Contracts.aspx?spz=" + CurrentActivity.Department.SPZ;
        }
    }
}