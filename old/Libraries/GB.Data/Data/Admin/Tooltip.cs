﻿namespace Uviri.MKRF.Entities.Admin
{
	/// <summary>Подсказка</summary>
	public class Tooltip : EntityBase
	{
		/// <summary>Наименование (имя_страницы+имя_контрола)</summary>
		public virtual string Name { get; set; }

		/// <summary>Содержание</summary>
		public virtual string Content { get; set; }//null

		/// <summary>Ширина</summary>
		public virtual int Width { get; set; }//null

		/// <summary>Высота</summary>
		public virtual int Height { get; set; }//null

		/// <summary>Положение угла подсказки</summary>
		public virtual string TooltipCorner { get; set; }//null

		/// <summary>Положение угла цели</summary>
		public virtual string TargetCorner { get; set; }//null
	}

	public enum TooltipCornersEnum
	{
		rightTop = 0,
		rightBottom = 1,
		rightMiddle = 2,
		leftTop = 3,
		leftBottom = 4,
		leftMiddle = 5,
	}

	public static class TooltipCornersExtension
	{
		public static string GetWord(this TooltipCornersEnum e)
		{
			switch (e)
			{
				case TooltipCornersEnum.rightTop:
					return "rightTop";

                case TooltipCornersEnum.rightBottom:
					return "rightBottom";
				
                case TooltipCornersEnum.rightMiddle:
					return "rightMiddle";
				
                case TooltipCornersEnum.leftTop:
					return "leftTop";
				
                case TooltipCornersEnum.leftBottom:
					return "leftBottom";
				
                case TooltipCornersEnum.leftMiddle:
					return "leftMiddle";
				
                default:
					return "rightTop";
			}
		}
	}
}
