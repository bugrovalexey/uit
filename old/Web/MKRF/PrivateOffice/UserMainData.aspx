﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateOffice/UserOffice.master" AutoEventWireup="true" CodeBehind="UserMainData.aspx.cs" Inherits="GB.MKRF.Web.PrivateOffice.UserMainData" %>
<%@ Import Namespace="GB.MKRF.Repository" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <style>
        .notification_section {
            display: none;
        }
    </style>
    <script type="text/javascript">
        function PasswordChange() {
            var old = PassOldTextBox.GetText();
            var pnew = PassNewTextBox.GetText();
            var pcomf = PassConfirmTextBox.GetText();

            if (old && pnew && pcomf) {
                if (pnew == pcomf) {
                    PassChangeCallBack.PerformCallback(PassOldTextBox.GetText() + ";" + PassNewTextBox.GetText());
                    return;
                }
                else {
                    ErrorPanelShow("Значение <b>Новый пароль</b> и <b>Подтверждение пароля</b> не совподают.");
                    return;
                }
            }

            var errtext = '';

            if (!old) { errtext += "Не заполнено поле <b>Старый пароль</b>. Пожалуйста заполните его."; }
            if (!pnew) { if (errtext) errtext += "<br>"; errtext += "Не заполнено поле <b>Новый пароль</b>. Пожалуйста заполните его."; }
            if (!pcomf) { if (errtext) errtext += "<br>"; errtext += "Не заполнено поле <b>Подтверждение пароля</b>. Пожалуйста заполните его."; }

            ErrorPanelShow(errtext);
        }

        function PassFormClose() {
            ErrorPanelHide();
            PasswordChangePopupControl.Hide();
            PassOldTextBox.SetText(null);
            PassNewTextBox.SetText(null);
            PassConfirmTextBox.SetText(null);
        }
    </script>
    <table align="center" width="100%">
        <tr>
            <td class="form_edit_desc" >
                Логин
            </td>
            <td class="form_edit_input" >
                <%=UserRepository.GetCurrent().Login %>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                Пароль
            </td>
            <td class="form_edit_input">
                <a class="command" href='javascript:void(0)' onclick='PasswordChangePopupControl.Show()' >Изменить пароль</a>
                <dxpc:ASPxPopupControl ID="Password_PopupControl" runat="server" ClientInstanceName="PasswordChangePopupControl"
                    Modal="True" AllowDragging="true" 
                    CloseAction="None" CloseButtonImage-Width="0" CloseButtonStyle-Cursor="default"
                    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" 
                    Width="520"
                    HeaderText="Изменить пароль ..." >
                    <ContentStyle BackColor="#E2EEFA">
                        <Paddings Padding="0" />
                    </ContentStyle>
                    <ContentCollection>
                        <dxpc:PopupControlContentControl >
                            <div class="div_panel_sep"></div>
                            <table align="center" width="90%" >
                                <tr>
                                    <td class="form_edit_desc" width="150" >
                                        Старый пароль
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxTextBox runat="server" ID="PassOldTextBox" ClientInstanceName="PassOldTextBox"
                                            NullText="Обязательное поле" >
                                            <NullTextStyle ForeColor="#aaaaaa" Font-Size="Small" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="form_edit_desc" >
                                        Новый пароль
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxTextBox runat="server" ID="PassNewTextBox" ClientInstanceName="PassNewTextBox"
                                            NullText="Обязательное поле" >
                                            <NullTextStyle ForeColor="#aaaaaa" Font-Size="Small" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="form_edit_desc" >
                                        Подтверждение пароля
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxTextBox runat="server" ID="PassConfirmTextBox" ClientInstanceName="PassConfirmTextBox"
                                            NullText="Обязательное поле" >
                                            <NullTextStyle ForeColor="#aaaaaa" Font-Size="Small" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                            </table>
                            <table style="text-align: center; padding-top: 6px; width: 90%;">
                                <tr>
                                    <td style="text-align: right; width: 50%;">
                                        <a class="command" href="javascript:void(0)" onclick="PassFormClose();" >Отмена</a>
                                    </td>
                                    <td>
                                        <dxe:ASPxButton runat="server" ID="PassChangeButton" Text="Изменить" AutoPostBack="False">
                                            <ClientSideEvents Click="function(s,e){ PasswordChange(); }" />
                                        </dxe:ASPxButton>
                                        <dxc:ASPxCallback runat="server" ID="PassChangeCallBack" ClientInstanceName="PassChangeCallBack" OnCallback="PassChangeCallBack_Callback" >
                                            <ClientSideEvents CallbackComplete="function(s,e){ 
                                                if (e.result == 'true') {
                                                    PassFormClose();
                                                }
                                                else {
                                                    ErrorPanelShow(e.result);
                                                }
                                            }" />
                                        </dxc:ASPxCallback>
                                    </td>
                                </tr>
                            </table>
                            <div class="div_panel_sep"></div>
                            <div id="div_error_panel" class="div_error_panel_none"></div>
                        </dxpc:PopupControlContentControl>
                    </ContentCollection>
                </dxpc:ASPxPopupControl>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                Ведомство
            </td>
            <td class="form_edit_input">
                <%=UserRepository.GetCurrent().Department.Name %>
            </td>
        </tr>
        <tr>
            <td class="form_edit_section" colspan="2">
                <div class="text">Персональные данные</div>
                <div class="line"></div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc" >
                Фамилия
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="FTextBox" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                Имя
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="ITextBox" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                Отчество
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="OTextBox" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                Образование
            </td>
            <td class="form_edit_input">
                <dxe:ASPxMemo runat="server" ID="ETextBox" Rows="5" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_section" colspan="2">
                <div class="text">Информация о работе</div>
                <div class="line"></div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                Место работы
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="WorkTextBox" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                Должность
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="JobTextBox" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_section" colspan="2">
                <div class="text">Контактные данные</div>
                <div class="line"></div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                Электронная почта
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="EmailTextBox" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                Факс
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="FaxTextBox" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                Сотовый телефон
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="MobPhoneTextBox" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                Рабочий телефон
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="WorkPhoneTextBox" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
            </td>
            <td class="form_edit_input">
                <dxe:ASPxCheckBox runat="server" ID="EmailCheckBox" Text="Получать письма" >
                    <ClientSideEvents CheckedChanged="function(s, e){
                            if (s.GetChecked()){
                                $('.notification_section').show();
                            }
                            else{
                                $('.notification_section').hide();
                            }
                        }" Init="function(s, e){
                            if (s.GetChecked()){
                                $('.notification_section').show();
                            }
                        }"></ClientSideEvents>
                </dxe:ASPxCheckBox> 
            </td>
        </tr>
        <tr class="notification_section">
            <td class="form_edit_section" colspan="2">
                <div class="text">Рассылки</div>
                <div class="line"></div>
            </td>
        </tr>
        <tr class="notification_section">
            <td class="form_edit_desc">
            </td>
            <td class="form_edit_input">
                <dxe:ASPxCheckBox runat="server" ID="PublishDateNtfCheckBox" Text="Уведомление о наступлении планируемой даты публикации" /> 
            </td>
        </tr>
        <tr class="notification_section">
            <td class="form_edit_desc">
            </td>
            <td class="form_edit_input">
                <dxe:ASPxCheckBox runat="server" ID="OpenDateNtfCheckBox" Text="Уведомление о наступлении даты вскрытия конвертов" /> 
            </td>
        </tr>
        <tr class="notification_section">
            <td class="form_edit_desc">
            </td>
            <td class="form_edit_input">
                <dxe:ASPxCheckBox runat="server" ID="ConsDateNtfCheckBox" Text="Уведомление о наступлении даты рассмотрения заявок" /> 
            </td>
        </tr>
        <tr class="notification_section">
            <td class="form_edit_desc">
            </td>
            <td class="form_edit_input">
                <dxe:ASPxCheckBox runat="server" ID="ItogDateNtfCheckBox" Text="Уведомление о наступлении даты подведения итогов" /> 
            </td>
        </tr>
        <tr class="notification_section">
            <td class="form_edit_desc">
            </td>
            <td class="form_edit_input">
                <dxe:ASPxCheckBox runat="server" ID="ViolationOfTermsNtfCheckBox" Text="Уведомление о нарушении сроков" /> 
            </td>
        </tr>
    </table>
    <div align="center" style="width: 100%">
        <dxe:ASPxButton runat="server" ID="SaveButton" Text="Сохранить" AutoPostBack="False">
            <ClientSideEvents Click="function(s,e){ LoadingPanel.Show(); SaveUserCallback.PerformCallback(''); }" />
        </dxe:ASPxButton>
        <dxc:ASPxCallback runat="server" ID="SaveUserCallback" ClientInstanceName="SaveUserCallback" Modal="True" OnCallback="SaveUserCallback_Callback" >
            <ClientSideEvents CallbackComplete="function(s,e){ LoadingPanel.Hide(); }" />
        </dxc:ASPxCallback>
    </div>
    <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel" Modal="True" />
</asp:Content>
