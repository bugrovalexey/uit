﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    [MetadataType(typeof(InternalIsAndComponentItkiEditViewModel_Valid))]
    public class InternalIsAndComponentItkiEditViewModel : ViewModelBase
    {
        #region consts

        private const string ObjectCaption = "Внутренний ИС и компоненты ИТКИ";
        
        #endregion consts

        public InternalIsAndComponentItkiEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;

            var entity = id.HasValue
                ? new RepositoryDc<InternalIsAndComponentsItki>().Get(id)
                : null;

            entity.Do(Fill);
        }

        #region Methods

        protected void Fill(InternalIsAndComponentsItki entity)
        {
            Id = entity.Id;
            AccountingObjectId = entity.Owner.Id;
            entity.Object.Do(
                o => Object = new List<IEntityName>() {new SelectEntityInfo() {Id = o.Id, Name = o.ShortName}});
            Purpose = entity.Purpose;
        }

        #endregion Methods

        #region Props

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        [DisplayName(ObjectCaption)]
        public IList<IEntityName> Object { get; set; }

        [DisplayName(TableInternalIsAndComponentItki.PurposeCaption)]
        public string Purpose { get; set; }

        #endregion Props
    }

    public class InternalIsAndComponentItkiEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Purpose { get; set; }
    }

}