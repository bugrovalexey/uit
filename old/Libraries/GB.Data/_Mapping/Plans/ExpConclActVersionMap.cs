﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ExpConclActVersionMap: ClassMapBase<ExpConclActVersion>
    {
        public ExpConclActVersionMap():
            base("Exp_Conclusion_Activity_Ver")
        {

        }

        protected override void InitMap()
        {
            References<ExpConclusion>(x => x.ExpConclusion, "Exp_Conclusion_Ver_ID").Not.Nullable();

            Map(x => x.Completion, "Exp_Conclusion_Activity_Ver_Completion");
            Map(x => x.Disagreement, "Exp_Conclusion_Activity_Ver_Disagreement");
            Map(x => x.AppropriateVolumeFinanceY0, "Exp_Conclusion_Activity_Ver_Appropriate_VF_Y0");
            Map(x => x.AppropriateVolumeFinanceY1, "Exp_Conclusion_Activity_Ver_Appropriate_VF_Y1");
            Map(x => x.AppropriateVolumeFinanceY2, "Exp_Conclusion_Activity_Ver_Appropriate_VF_Y2");
            Map(x => x.Code, "Exp_Conclusion_Activity_Ver_Code");

            References<Appropriate>(x => x.GoalEffective, "Goal_Effective_ID").Not.Nullable();
            References<Appropriate>(x => x.OrderFinance, "Order_Finance_ID").Not.Nullable();

        }
    }
}
