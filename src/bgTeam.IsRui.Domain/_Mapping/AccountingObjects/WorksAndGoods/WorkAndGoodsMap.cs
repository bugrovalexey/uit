﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects.WorksAndGoods
{
    class WorkAndGoodsMap : ClassMapHiLo<WorkAndGoods>
    {
        public WorkAndGoodsMap()
            : base("OU_Work_And_Goods")
        {
            
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Work_And_Goods_Name");
            Map(x => x.Summa, "OU_Work_And_Goods_Sum");
            References(x => x.CategoryKindOfSupport, "VO_Category_ID").Not.Nullable();
            Map(x => x.ObjectId, "OU_Work_And_Goods_Object_ID").Not.Nullable();
            Map(x => x.Type, "OU_Work_And_Goods_Type").CustomType<CategoryTypeOfSupportEnum>();
            
            References(x => x.ActOfAcceptance, "Act_ID");
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
        }
    }
}
