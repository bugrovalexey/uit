﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Directories
{
    public class GKPropsMap : ClassMapBase<GKProps>
	{
        public GKPropsMap()
            : base("PRS_Rekv_GK")
		{
//            Table("w_");

//            Id(x => x.Id, "PRS_Rekv_GK_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string args = @" @PRS_Rekv_GK_Num = ? 
//							,@PRS_Rekv_GK_DT = ? 
//							,@PRS_Rekv_GK_Predmet = ? 
//							,@PRS_Rekv_GK_Executor = ? 
//							,@Plans_Work_ID = ?
//							,@PRS_Rekv_GK_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_Rekv_GK_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_Rekv_GK_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_Rekv_GK_Delete
//						  @PRS_Rekv_GK_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_Rekv_GK_Num");
			Map(x => x.Date, "PRS_Rekv_GK_DT");
			Map(x => x.Subject, "PRS_Rekv_GK_Predmet");
			Map(x => x.Executor, "PRS_Rekv_GK_Executor");

			References<PlansWork>(x => x.Work, "Plans_Work_ID").Not.Nullable();
        }
    }
}