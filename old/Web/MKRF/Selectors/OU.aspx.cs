﻿using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Selectors
{
    public partial class OU : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            Grid.DataBind();

            base.OnPreRender(e);
        }

        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            (sender as ASPxGridView).DataSource = new AccountingObjectRepository().GetAllEx(q => q
                .Fetch(f => f.Type).Eager
                .Fetch(f => f.Kind).Eager.Clone())
                    .Select(x =>
                    new
                    {
                        Id = x.Id,
                        Name = x.ShortName,
                        Type = x.Type.Name,
                        Kind = x.Kind.Name
                    });
        }
    }
}