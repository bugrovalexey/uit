﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201803031254)]
    public class Migration_201803031254 : ForwardOnlyMigration
    {
        public override void Up()
        {

            Create.Table("Plans_Activity_Goods")
                .WithColumn("Plans_Activity_Goods_ID").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Plans_Activity_ID").AsInt32().NotNullable()
                .WithColumn("Plans_Activity_Goods_Name").AsString().Nullable()
                .WithColumn("Plans_Activity_Goods_OKPDId").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Goods_ProductGroupId").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Goods_ExpenseDirectionId").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Goods_ExpenseTypeId").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Goods_KOSGUId").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Goods_Year").AsString().Nullable()
                .WithColumn("Plans_Activity_Goods_Quantity").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Goods_Duration").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Goods_Cost").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Goods_Summ").AsDecimal().Nullable()
                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Create.ForeignKey("FK_Plans_Activity_Goods")
               .FromTable("Plans_Activity_Goods").ForeignColumn("Plans_Activity_ID")
               .ToTable("Plans_Activity").PrimaryColumn("Plans_Activity_ID");

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "Plans_Activity_Goods" });

            Create.Table("Plans_Activity_GoodsCharacteristic")
                .WithColumn("Plans_Activity_GoodsCharacteristic_ID").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Plans_Activity_Goods_ID").AsInt32().Nullable()
                .WithColumn("Plans_Activity_GoodsCharacteristic_Name").AsString().Nullable()
                .WithColumn("Plans_Activity_GoodsCharacteristic_UnitId").AsInt32().Nullable()
                .WithColumn("Plans_Activity_GoodsCharacteristic_Value").AsInt32().Nullable()
                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Create.ForeignKey("FK_Plans_Activity_GoodsCharacteristic")
               .FromTable("Plans_Activity_GoodsCharacteristic").ForeignColumn("Plans_Activity_Goods_ID")
               .ToTable("Plans_Activity_Goods").PrimaryColumn("Plans_Activity_Goods_ID");

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "Plans_Activity_GoodsCharacteristic" });

        }
    }
}
