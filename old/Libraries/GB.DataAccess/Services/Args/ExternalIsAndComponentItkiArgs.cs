﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Services.Args
{
    public class ExternalIsAndComponentItkiArgs : BaseArgsNew
    {
        /// <summary>
        /// Id объекта учета
        /// </summary>
        public int AccountingObjectId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// СМЭВ
        /// </summary>
        public string Smev { get; set; }

        /// <summary>
        /// URL
        /// </summary>
        public string Url { get; set; }

    }
}
