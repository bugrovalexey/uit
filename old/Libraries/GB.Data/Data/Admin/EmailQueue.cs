﻿using GB.Entity.Mails;
using System;

namespace GB.MKRF.Entities.Admin
{
    /// <summary>
    /// Приоритет письма
    /// </summary>
    public enum EMailPriority
    {
        /// <summary>Нормальный</summary>
        Normal = 0,

        /// <summary>Низкий</summary>
        Low = 1,

        /// <summary>Высокий</summary>
        High = 2,
    }

    /// <summary>
    /// Элемент очереди рассылки
    /// </summary>
    public class EmailQueue : EntityBase, IEmailQueueable
    {
        /// <summary>
        /// Отправитель
        /// </summary>
        public virtual string From { get; set; }

        /// <summary>
        /// Кому
        /// </summary>
        public virtual string To { get; set; }

        /// <summary>
        /// Копия
        /// </summary>
        public virtual string CopyTo { get; set; }

        /// <summary>
        /// Скрытая копия
        /// </summary>
        public virtual string HiddenCopyTo { get; set; }

        /// <summary>
        /// Заголовок
        /// </summary>
        public virtual string Subject { get; set; }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public virtual string Text { get; set; }

        /// <summary>
        /// Приоритет сообщения
        /// </summary>
        public virtual EMailPriority Priority { get; set; }

        /// <summary>
        /// Признак форматированного сообщения
        /// </summary>
        public virtual bool IsHtml { get; set; }

        /// <summary>
        /// Плановая дата отправки
        /// </summary>
        public virtual DateTime SheduledDate { get; set; }

        /// <summary>
        /// Фактическая дата отправки
        /// </summary>
        public virtual DateTime? SendDate { get; set; }

        /// <summary>
        /// Код ошибки
        /// </summary>
        public virtual string ErrorCode { get; set; }

        /// <summary>
        /// Служебные данные к письму
        /// </summary>
        public virtual string SysData { get; set; }

        /// <summary>
        /// Служебные данные к письму
        /// </summary>
        public virtual string Attachments { get; set; }

        /// <summary>
        /// Шаблон рассылки
        /// </summary>
        public virtual Notification Notification { get; set; }
    }
}