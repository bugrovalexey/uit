﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true"
    CodeBehind="List.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.List" %>

<%@ Import Namespace="GB" %>
<%@ Import Namespace="GB.MKRF.Entities.Directories" %>
<%@ Import Namespace="GB.MKRF.Web.Plans" %>

<%@ Register Src="~/Common/ExportReportControl.ascx" TagName="ExportReportControl"
    TagPrefix="ex" %>
<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="~/Common/GridColumnManager.ascx" TagName="GridColumnManager" TagPrefix="gcm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script>
        ActiveMenuItem = 1;
    </script>
    <style>
        .dxgvGroupRow_MetropolisBlue
        {
            height: 50px;
        }

        .dxgvSelectedRow_MetropolisBlue
        {
            height: 55px;
        }

        .dxgvCommandColumnItem_MetropolisBlue
        {
            display: inline;
            margin: 0px 5px;
        }
    </style>
    <script type="text/javascript">
        function hfSave(e) {
            hf.Set('Year', YearComboBox.GetValue());
            hf.Set('User', UserComboBox.GetValue());
            hf.Set('Copy', PlansComboBox.GetValue());


            //hf.Set('Stage', StageComboBox.GetValue());
            //var obj = null;
            //try { obj = eval('PlansComboBox'); } catch (e) { }
            //if (ASPxClientUtils.IsExists(obj))
            //    hf.Set(['CopyPlanId'], PlansComboBox.GetValue());
        }

        //function togglePublish(s, id) {
        //    callBack.PerformCallback((s.GetValue() ? "1" : "0") + "#" + id);
        //}
    </script>
    <%--<dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" OnCallback="callBack_Callback">
    </dxc:ASPxCallback>--%>
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
    <dxwgve:ASPxGridViewExporter ID="GridViewExporter" runat="server" GridViewID="plansGrid" />

    <div class="page">

        <div class="detailTop">
            <dxe:ASPxButton ID="addButton" runat="server" ClientInstanceName="addButton" AutoPostBack="false"
                Text="Добавить" Width="80px" CssClass="fleft topButton">
                <ClientSideEvents Click="function(s,e) { plansGrid.AddNewRow(); }" />
            </dxe:ASPxButton>

            <dxe:ASPxButton ID="exportBtn" runat="server" ClientInstanceName="exportBtn" AutoPostBack="false"
                Text="Экспорт плана ▼" CssClass="fleft topButton">
            </dxe:ASPxButton>
            <dxpc:ASPxPopupControl ID="PopupControl" runat="server" CloseAction="OuterMouseClick" PopupElementID="exportBtn" PopupVerticalAlign="Below" PopupHorizontalAlign="LeftSides"
                ShowFooter="False" ShowHeader="False">
                <ContentCollection>
                    <dxpc:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                        <ex:ExportReportControl ID="ExportReportControl1" runat="server" ExportName="Планы информатизации 2012"
                            OnBeforeDataBind="ExportReportControl_BeforeDataBind" />
                    </dxpc:PopupControlContentControl>
                </ContentCollection>
            </dxpc:ASPxPopupControl>

            <dxe:ASPxButton ID="ExportButton" runat="server" Text="Выгрузить список в Excel"
                Width="180px" OnClick="ExportButton_Click" CssClass="fleft topButton" />
            <dxhf:ASPxHiddenField runat="server" ID="hfFilterMode" ClientInstanceName="hfFilterMode">
                <ClientSideEvents Init="function(s,e){
                        s.Set('filterMode', 'all');
                    }"></ClientSideEvents>
            </dxhf:ASPxHiddenField>

            <dxe:ASPxButton ID="SamplesButton" runat="server" Text="Пример заполнения" AutoPostBack="false"
                Width="180px" CssClass="fleft topButton" ClientSideEvents-Click="function(s,e){ window.location = '../help/List.aspx'}" />

            <%--            <dxe:ASPxButton ID="filterBtnAll" runat="server" ClientInstanceName="filterBtnAll" AutoPostBack="false"
                Text="Все" CssClass="topButton fleft filterBtn" GroupName="FilterMode" Checked="True">
                <ClientSideEvents
                    Init="function(s,e){
                 
                    }"
                    Click="function(s,e) { 
                        hfFilterMode.Set('filterMode', 'all');
                        hfCache.Clear();
                        plansGrid.Refresh();
                    }" />
            </dxe:ASPxButton>
            <dxe:ASPxButton ID="filterBtnDepartments" runat="server" ClientInstanceName="filterBtnDepartments" AutoPostBack="false"
                Text="Департаменты" CssClass="topButton fleft filterBtn" Style="margin-left: -1px;" GroupName="FilterMode">
                <ClientSideEvents Click="function(s,e) {
                        hfFilterMode.Set('filterMode', 'departments');
                        hfCache.Clear();
                        plansGrid.Refresh();
                    }" />
            </dxe:ASPxButton>--%>
            <%--<dxe:ASPxButton ID="filterBtnSubordinate" runat="server" ClientInstanceName="filterBtnSubordinate" AutoPostBack="false"
                Text="Подведомственные департаменты" CssClass="topButton fleft filterBtn" Style="margin-left: -1px;" GroupName="FilterMode">
                <ClientSideEvents Click="function(s,e) {
                        hfFilterMode.Set('filterMode', 'subordinate');
                        hfCache.Clear();
                        plansGrid.Refresh();
                    }" />
            </dxe:ASPxButton>
        <dxe:ASPxButton ID="filterBtnRegion" runat="server" ClientInstanceName="filterBtnRegion" AutoPostBack="false"
                Text="Территориальные органы" CssClass="topButton fleft filterBtn" Style="margin-left: -1px;" GroupName="FilterMode">
                <ClientSideEvents Click="function(s,e) {
                        hfFilterMode.Set('filterMode', 'region');
                        hfCache.Clear();
                        plansGrid.Refresh();
                    }" />
            </dxe:ASPxButton>--%>

            <%--<dxe:ASPxCheckBox runat="server" ID="ChBAll" Text="с подведомственными" CssClass="fleft topButton">
            <ClientSideEvents CheckedChanged="function(s,e){ hfCache.Clear(); plansGrid.Refresh(); }"></ClientSideEvents>
        </dxe:ASPxCheckBox>--%>

            <%--<dxe:ASPxCheckBox runat="server" ID="ChBExpired" Text="просроченные" CssClass="fleft topButton">
            <ClientSideEvents CheckedChanged="function(s,e){ hfCache.Clear(); plansGrid.Refresh(); }"></ClientSideEvents>
        </dxe:ASPxCheckBox>--%>

            <div style="float: right;">
                <gcm:GridColumnManager ID="GridColumnManager" runat="server" GridName="plansGrid" UserStyle="width:45px; top: 14px; position: relative;" />
            </div>
        </div>
        <dxhf:ASPxHiddenField runat="server" ID="hfCache" ClientInstanceName="hfCache"></dxhf:ASPxHiddenField>
        <dxwgv:ASPxGridView runat="server" ID="plansGrid" KeyFieldName="Id" Width="100%"
            ClientInstanceName="plansGrid" ViewStateMode="Disabled" AutoGenerateColumns="False"
            OnBeforePerformDataSelect="plansGrid_BeforePerformDataSelect" OnRowDeleting="plansGrid_RowDeleting"
            OnRowInserting="plansGrid_RowInserting" OnRowUpdating="plansGrid_RowUpdating"
            OnStartRowEditing="plansGrid_StartRowEditing" OnCommandButtonInitialize="plansGrid_CommandButtonInitialize"
            OnCustomButtonCallback="plansGrid_CustomButtonCallback"
            OnCustomButtonInitialize="plansGrid_CustomButtonInitialize" OnCustomJSProperties="plansGrid_CustomJSProperties"
            OnInit="plansGrid_Init">
            <Columns>
                <dxwgv:GridViewBandColumn Caption=" " ShowInCustomizationForm="false">
                    <Columns>
                        <dxwgv:GridViewCommandColumn Caption=" " Name="copyColumn" AllowDragDrop="False" ShowInCustomizationForm="False"
                            ButtonType="Image" HeaderStyle-CssClass="bottomborder" Width="20px">
                            <CustomButtons>
                                <dxwgv:GridViewCommandColumnCustomButton ID="copyButton" Image-ToolTip="Копировать" Image-Url="/Content/GB/Styles/Imgs/copy_16.png" />
                            </CustomButtons>
                        </dxwgv:GridViewCommandColumn>
                        <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" AllowDragDrop="False" ShowInCustomizationForm="False" ButtonType="Image" HeaderStyle-CssClass="bottomborder" Width="20px">
                            <EditButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/edit_16.png" Image-ToolTip="Изменить" />
                            <ClearFilterButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/clear_16.png" Image-ToolTip="Очистить" />
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <HeaderStyle Border-BorderWidth="0px"></HeaderStyle>
                </dxwgv:GridViewBandColumn>
                <dxwgv:GridViewCommandColumn Caption=" " Name="delColumn" VisibleIndex="99" AllowDragDrop="False" ShowInCustomizationForm="False" ButtonType="Image" Width="20px">
                    <DeleteButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/delete_16.png" Image-ToolTip="Удалить" />
                </dxwgv:GridViewCommandColumn>
                <dxwgv:GridViewCommandColumn Caption=" " ShowSelectCheckbox="True" AllowDragDrop="False" ShowInCustomizationForm="False" Width="40px" />
                <dxwgv:GridViewDataTextColumn Caption="Год" FieldName="Year" Width="35px">
                    <Settings AllowHeaderFilter="True" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Наименование департамента" FieldName="DepartmentName">
                    <DataItemTemplate>
                        <%#GetNameLink(Eval("PlanId"), Eval("DepartmentName"), Eval("IsShow")) %>
                    </DataItemTemplate>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Статус" FieldName="Status" Width="200px">
                    <Settings AllowHeaderFilter="True" />
                </dxwgv:GridViewDataTextColumn>
                <%--<dxwgv:GridViewDataTextColumn Caption="Статус хода экспертизы" Visible="False" FieldName="ExpStatus" Width="200px">
                <Settings AllowHeaderFilter="True" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataColumn Caption=" " FieldName="CheckEx" CellStyle-VerticalAlign="Middle" CellStyle-HorizontalAlign="Center" ShowInCustomizationForm="False" >
                <DataItemTemplate>
                    <%#getExpFull((int)Eval("cnt"), (int)Eval("CheckEx"))%>
                </DataItemTemplate>
                <Settings AllowAutoFilter="False" />
            </dxwgv:GridViewDataColumn>--%>
                <dxwgv:GridViewDataDateColumn Caption="Дата изменения статуса" FieldName="StatusChangeDate" Width="100px" PropertiesDateEdit-DisplayFormatString="{0:dd.MM.yyyy HH:mm}" />

                <dxwgv:GridViewDataTextColumn Caption="Департаменты" ShowInCustomizationForm="False" FieldName="DepartmentName" Visible="false" Name="FieldForGrouping" />

                <dxwgv:GridViewDataTextColumn Caption="Ответственный" FieldName="UserInfo" Visible="false" />

                <dxwgv:GridViewDataTextColumn Caption="Количество мероприятий" FieldName="cnt" Visible="false" Width="100px" />

                <dxwgv:GridViewBandColumn Caption="Итого, тыс. руб" Visible="False">
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="VolY0">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="VolY1">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="VolY2">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                </dxwgv:GridViewBandColumn>

                <dxwgv:GridViewBandColumn Caption="Дополнительная потребность, тыс. руб" Visible="False">
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="PlansActivityAddVolY0"
                            Width="150px">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                            </PropertiesTextEdit>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="PlansActivityAddVolY1"
                            Width="150px">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                            </PropertiesTextEdit>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="PlansActivityAddVolY2"
                            Width="150px">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                            </PropertiesTextEdit>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                </dxwgv:GridViewBandColumn>

                <dxwgv:GridViewDataTextColumn Caption="Печатные формы" Width="150px" Visible="False">
                    <DataItemTemplate>
                        <%#getViewLinks((int)Eval("PlanId"))%>
                    </DataItemTemplate>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Composite" FieldName="Composite" Visible="False" ShowInCustomizationForm="False" />
            </Columns>
            <TotalSummary>
                <dxwgv:ASPxSummaryItem ShowInColumn="DepartmentName" DisplayFormat="Количество планов: {0}" SummaryType="Count" />
            </TotalSummary>
            <Settings ShowFilterRow="True" ShowHeaderFilterBlankItems="False" GridLines="Horizontal"
                ShowFooter="True" GroupFormat="{1} {2}" ShowGroupFooter="Hidden" ShowFilterRowMenu="true" />
            <SettingsBehavior ConfirmDelete="True" />
            <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
            <SettingsPager PageSize="50" />
            <SettingsEditing Mode="EditForm" />
            <SettingsCookies Enabled="true" StoreFiltering="true" />
            <Styles>
                <GroupRow Cursor="pointer" />
            </Styles>
            <Templates>
                <EditForm>
                    <table class="form_edit">
                        <tr>
                            <td class="form_edit_desc">Год:
                            </td>
                            <td class="form_edit_input">
                                <dxe:ASPxComboBox ID="YearComboBox" runat="server" ClientInstanceName="YearComboBox"
                                    TextField="Year" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                    ValueField="Id" ValueType="System.Int32" OnDataBinding="YearComboBox_DataBinding">
                                    <ValidationSettings ErrorDisplayMode="Text">
                                        <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                    </ValidationSettings>
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="form_edit_desc">Наименование департамента:
                            </td>
                            <td class="form_edit_input">
                                <dxe:ASPxLabel ID="DepartmentTextBox" runat="server" OnDataBinding="DepartmentTextBox_DataBinding" />
                            </td>
                        </tr>
                        <tr>
                            <td class="form_edit_desc">Ответственный:
                            </td>
                            <td class="form_edit_input">
                                <dxe:ASPxComboBox ID="UserComboBox" runat="server" ClientInstanceName="UserComboBox"
                                    TextField="FullName" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                    ValueField="Id" ValueType="System.Int32" OnDataBinding="UserComboBox_DataBinding">
                                    <ValidationSettings ErrorDisplayMode="Text">
                                        <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                    </ValidationSettings>
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>

                        <tr runat="server" id="trCopyPlan">
                            <td class="form_edit_desc">Скопировать мероприятия из плана за год:
                            </td>
                            <td class="form_edit_input">
                                <dxe:ASPxComboBox ID="PlansComboBox" runat="server" ClientInstanceName="PlansComboBox"
                                    TextField="Value" ValueField="Key" ValueType="System.Int32" OnDataBinding="PlansComboBox_DataBinding">
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="form_edit_buttons">
                                <span class="ok">
                                    <a onclick="plansGrid.UpdateEdit();">Сохранить</a>
                                    <%--<dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />--%>
                                </span>
                                <span class="cancel">
                                    <a onclick="plansGrid.CancelEdit();">Отмена</a>
                                    <%--<dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />--%>
                                </span>
                            </td>
                        </tr>
                    </table>
                </EditForm>
            </Templates>
            <ClientSideEvents
                BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') {hfSave(e);} }"
                EndCallback="function(s,e) {
                                if(s.cpCache){
                                    hfCache.Set('t', s.cpCache);
                                };
                            }"
                RowClick="function(s, e) {
                        if (plansGrid.IsGroupRowExpanded(e.visibleIndex))
                            plansGrid.CollapseRow(e.visibleIndex);
                        else
                            plansGrid.ExpandRow(e.visibleIndex);
                        }" />
        </dxwgv:ASPxGridView>
    </div>
</asp:Content>
