﻿using System;
using DevExpress.Web.ASPxTreeList;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Selectors
{
    public partial class IKT : System.Web.UI.Page
    {
        int? _year = null;
        int? yearValue
        {
            get
            {
                if ((Request["year"] != null) && (!string.IsNullOrEmpty(Request.QueryString["year"])))
                {
                    int a = 0;
                    if (Int32.TryParse(Request["year"].ToString(), out a))
                    {
                        _year = a;
                    }
                    else
                    {
                        _year = null;
                    }
                }
                return _year;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            IKTTreeList.DataBind(); 
        }

        protected override void OnPreRender(EventArgs e)
        {
            IKTTreeList.ExpandToLevel(3);

            base.OnPreRender(e);
        }

        protected void IKTTreeList_DataBinding(object sender, EventArgs e)
        {
            (sender as ASPxTreeList).DataSource = DirectoriesRepositary.GetDirectories<IKTComponent>(yearValue.HasValue ? yearValue.Value : DateTime.Now.Year); 
        }

        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            TreeListNode node = IKTTreeList.FindNodeByKeyValue(e.Parameter);
            e.Result = GetEntryText(node);
        }
        
        protected string GetEntryText(TreeListNode node)
        {
            if (node != null)
            {
                return String.Format("{0};{1} {2}", node["Id"].ToString(), node["Code"].ToString(), node["Name"].ToString());
            }
            return string.Empty;
        }

        protected void IKTTreeList_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "if (!$(this).hasClass('dxtlFocusedNode_MetropolisBlue')){$(this).addClass(\"grid_mouseover\")}");
            e.Row.Attributes.Add("onmouseout", "$(this).removeClass(\"grid_mouseover\")");
        }
    }
}