﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Logic.MasterEdit
{
    public class MasterItem
    {
        public int Position { get; set; }

        public string Title { get; set; }
        public string TitleStep { get; set; }

        public string Action { get; set; }
        public object Params { get; set; }

        public string Prev { get; set; }
        public object PrevParams { get; set; }

        public string Next { get; set; }
        public object NextParams { get; set; }

        public MasterItemEnum Item { get; set; }
    }
}
