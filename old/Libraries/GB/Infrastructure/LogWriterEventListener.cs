﻿using System.Data;
using NHibernate;
using NHibernate.Event;

namespace Uviri.Infrastructure
{
    //public class PreInsertEventListener : IPreInsertEventListener
    //{
    //    public bool OnPreInsert(PreInsertEvent e)
    //    {
    //        return false;
    //    }
    //}


    /// <summary>
    /// Реализация интерфейса добавления объекта
    /// </summary>
    public class PostInsertLogWriterEventListener : IPostInsertEventListener
    {
        /// <summary>
        /// Действия производимые после добавления объекта
        /// </summary>
        /// <param name="event">событие</param>
        public void OnPostInsert(PostInsertEvent e)
        {
            //ListenerHelper.SaveToLog(@event.Session, ListenerHelper.COMMAND_INSERT, ((SingleTableEntityPersister)(@event.Persister)).TableName, (int)@event.Id);
        }
    }

    /// <summary>
    /// Реализация интерфейса обновления объекта
    /// </summary>
    public class PostUpdateLogWriterEventListener : IPostUpdateEventListener
    {


        /// <summary>
        /// Действия производимые перед обновлением объекта
        /// </summary>
        /// <param name="event">событие</param>
        /// <returns></returns>
        //public bool OnPreUpdate(PreUpdateEvent @event)
        //{
            
            
        //    return false;
        //}

        public void OnPostUpdate(PostUpdateEvent @event)
        {
            //ListenerHelper.SaveToLog(@event.Session, ListenerHelper.COMMAND_UPDATE, ((SingleTableEntityPersister)(@event.Persister)).TableName, (int)@event.Id);
        }
    }

    /// <summary>
    /// Реализация интерфейса удаления объекта
    /// </summary>
    public class PostDeleteLogWriterEventListener : IPostDeleteEventListener
    {
        /// <summary>
        /// Действия производимые перед удалением объекта
        /// </summary>
        /// <param name="event">событие</param>
        /// <returns></returns>
        //public bool OnPreDelete(PreDeleteEvent @event)
        //{
            
            
        //    return false;
        //}

        public void OnPostDelete(PostDeleteEvent @event)
        {
            //ListenerHelper.SaveToLog(@event.Session, ListenerHelper.COMMAND_DELETE, ((SingleTableEntityPersister)(@event.Persister)).TableName, (int)@event.Id);
        }
    }

    internal static class ListenerHelper
    {
        public const string COMMAND_INSERT = "i";
        public const string COMMAND_UPDATE = "u";
        public const string COMMAND_DELETE = "d";

        /// <summary>
        /// Сохранение записи в историю
        /// </summary>
        /// <param name="session">сессия</param>
        /// <param name="commandCode">Тип операции i / u / d </param>
        /// <param name="tableName">таблица объекта</param>
        /// <param name="id">идентификатор объекта</param>
        public static void SaveToLog(ISession session, string commandCode, string tableName, int id)
        {
            IDbCommand command = session.Connection.CreateCommand();
            session.Transaction.Enlist(command);

            if (tableName.StartsWith("w_", System.StringComparison.InvariantCultureIgnoreCase))
                tableName = tableName.Substring(2);

            command.CommandText = "exec Write_To_Log @opType = '" + commandCode + "', @ID=" + id + ", @Table = '" + tableName + "'";

            //TODO : переделать на параметры

            //IDbDataParameter p1 = command.CreateParameter();
            //p1.ParameterName = "opType";
            //p1.Value = commandCode;
            //command.Parameters.Add(p1);

            //IDbDataParameter p2 = command.CreateParameter();
            //p2.ParameterName = "ID";
            //p2.Value = id;
            //command.Parameters.Add(p2);

            //IDbDataParameter p3 = command.CreateParameter();
            //p3.ParameterName = "Table";
            //p3.Value = tableName;
            //command.Parameters.Add(p3);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (System.Exception) { }
        }
    }
}
