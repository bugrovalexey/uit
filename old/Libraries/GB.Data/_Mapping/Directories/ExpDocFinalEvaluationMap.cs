﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ExpDocFinalEvaluationMap : ClassMapBase<ExpDocFinalEvaluation>
	{
        public ExpDocFinalEvaluationMap()
            : base("Exp_Doc_Final_Evaluation") {}

        protected override void InitMap()
        {
            Map(x => x.Name, "Exp_Doc_Final_Evaluation_Name").Nullable();
        }
    }
}
