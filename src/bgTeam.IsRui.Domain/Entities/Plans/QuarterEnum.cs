﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    public enum QuarterEnum
    {
        /// <summary>
        /// Первый квартал
        /// </summary>
        Quarter1 = 0,

        /// <summary>
        /// Второй квартал
        /// </summary>
        Quarter2 = 1,

        /// <summary>
        /// Третий квартал
        /// </summary>
        Quarter3 = 2,

        /// <summary>
        /// Четвёртый квартал
        /// </summary>
        Quarter4 = 3
    }
}
