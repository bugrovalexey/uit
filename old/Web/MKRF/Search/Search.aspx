﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true"
    CodeBehind="Search.aspx.cs" Inherits="Uviri.MKRF.Web.Search.Search" %>
<%@ Import Namespace="Uviri" %>
<%@ Import Namespace="Core.Helpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server" EnableViewState="false">
    <script type="text/javascript" src="/Content/Scripts/jquery-ui-1.8.16.custom.min.js"></script>
    <style type="text/css">
        ul, li
        {
            border: 0 none;
            margin: 0;
            padding: 0;
            list-style: none outside none;
        }
        em
        {
            font-style: normal;
            font-weight: bold;
        }
        .time, .source, .info, .attachment, .owner
        {
            padding-right: 0.5em;
            font-size: 0.8em;
            font-style: italic;
        }
        .current-page, .pager-link
        {
            margin: 0.15em 0.3em;
            font-size: 125%;
        }
        .header-link
        {
            font-size: 125%;
            line-height: 1.3em;
        }
        .no-result
        {
            margin: 2em;
            font-size: 125%;
        }
        .dxgvStatusRow_MetropolisBlue,
        .dxgvStatusSelectedRow_MetropolisBlue
        {
            height: 20px !important;
        }
    </style>
    <script type="text/javascript">

         function showPanel(show)
        {
            if (show)
            {
                $(".panel").show(300);
                $("#hideExSearchButton").show(0);
                $("#showExSearchButton").hide(0);
            }
            else
            {
                $(".panel").hide("fast");
                $("#showExSearchButton").show(0);
                $("#hideExSearchButton").hide(0);
            }
        };

        function doSearch() {

            var temp;
            var items = [];
            var args = [];

            temp = QueryTextBox.GetText();
            if (temp) {
                args.push("q=" + temp);
            };

            temp = CuratorTextBox.GetText();
            if (temp) {
                args.push("c=" + temp);
            }

            temp = AuthorTextBox.GetText();
            if (temp) {
                args.push("a=" + temp);
            }

            temp = [];
            items = PlanTypeListBox.GetSelectedValues();
            if (items.length > 0) {
                temp.push (items.join(','));
            }
            items = PreliminaryTypeListBox.GetSelectedValues();
            if (items.length > 0) {
                temp.push (items.join(','));
            }
            items = Plan2012TypeListBox.GetSelectedValues();
            if (items.length > 0) {
                temp.push (items.join(','));
            }
            items = TypeListBox.GetSelectedValues();
            if (items.length > 0) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i] == 2000) {
                        items[i] = "2001,2002,2003,2004,2005,2008,2010,2017"; // типы файлов
                    }
                }
                temp.push (items.join(','));
            }

            if (temp.length > 0) {
                args.push('t=' + temp.join(','));
            }

            items = DepartmentListBox.GetSelectedValues();
            if (items.length > 0) {
                args.push('d=' + items.join(','));
            }

            items = IKTGrid.GetSelectedKeysOnPage();
            if (items.length > 0) {
                args.push('i=' + items.join(','));
            }

            items = StatusGrid.GetSelectedKeysOnPage();
            if (items.length > 0) {
                args.push('s=' + items.join(','));
            }

            temp = NumberSpinEdit.GetText();
            if (temp) {
                args.push('n=' + temp);
            }

            temp = Year1SpinEdit.GetText();
            if (temp) {
                args.push('y1=' + temp);
            }

            temp = Year2SpinEdit.GetText();
            if (temp) {
                args.push('y2=' + temp);
            }


            temp = LastMod1DateEdit.GetDate();
            if (temp) {
                args.push('d1=' + temp.getDate() + '.' + (temp.getMonth() + 1) + '.' + temp.getFullYear());
            }

            temp = LastMod2DateEdit.GetDate();
            if (temp) {
                args.push('d2=' + temp.getDate() + '.' + (temp.getMonth() + 1) + '.' + temp.getFullYear());
            }

            temp = sF1SpinEdit.GetText(); if (temp) args.push('f1=' + temp);
            temp = sF2SpinEdit.GetText(); if (temp) args.push('f2=' + temp);
            temp = sR1SpinEdit.GetText(); if (temp) args.push('r1=' + temp);
            temp = sR2SpinEdit.GetText(); if (temp) args.push('r2=' + temp);
            temp = sV1SpinEdit.GetText(); if (temp) args.push('v1=' + temp);
            temp = sV2SpinEdit.GetText(); if (temp) args.push('v2=' + temp);
            temp = sSum1SpinEdit.GetText(); if (temp) args.push('s1=' + temp);
            temp = sSum2SpinEdit.GetText(); if (temp) args.push('s2=' + temp);


            temp = sY01SpinEdit.GetText(); if (temp) args.push('y01=' + temp);
            temp = sY02SpinEdit.GetText(); if (temp) args.push('y02=' + temp);
            temp = sY11SpinEdit.GetText(); if (temp) args.push('y11=' + temp);
            temp = sY12SpinEdit.GetText(); if (temp) args.push('y12=' + temp);
            temp = sY21SpinEdit.GetText(); if (temp) args.push('y21=' + temp);
            temp = sY22SpinEdit.GetText(); if (temp) args.push('y22=' + temp);

            temp = sAddY01SpinEdit.GetText(); if (temp) args.push('add01=' + temp);
            temp = sAddY02SpinEdit.GetText(); if (temp) args.push('add02=' + temp);
            temp = sAddY11SpinEdit.GetText(); if (temp) args.push('add11=' + temp);
            temp = sAddY12SpinEdit.GetText(); if (temp) args.push('add12=' + temp);
            temp = sAddY21SpinEdit.GetText(); if (temp) args.push('add21=' + temp);
            temp = sAddY22SpinEdit.GetText(); if (temp) args.push('add22=' + temp);

            temp = sSumY01SpinEdit.GetText(); if (temp) args.push('sum01=' + temp);
            temp = sSumY02SpinEdit.GetText(); if (temp) args.push('sum02=' + temp);
            temp = sSumY11SpinEdit.GetText(); if (temp) args.push('sum11=' + temp);
            temp = sSumY12SpinEdit.GetText(); if (temp) args.push('sum12=' + temp);
            temp = sSumY21SpinEdit.GetText(); if (temp) args.push('sum21=' + temp);
            temp = sSumY22SpinEdit.GetText(); if (temp) args.push('sum22=' + temp);


            temp = document.getElementById('extContent')
            if (temp && temp.style.display != 'none') {
                args.push("x=1");
            }

            temp = 'Search.aspx';

            if (args.length > 0) {
                temp = temp + "?" + args.join('&');
            }

            window.open(temp, '_self');
        }

        function OnRowClick(s, e) {
            e.cancel = true;
            s.SelectRowOnPage(e.visibleIndex, !s.IsRowSelectedOnPage(e.visibleIndex));
        }
    </script>

    <div class="page" style="margin: 0 auto;">

    <table style="width:1000px">
        <tr>
            <td colspan="2" align="center">
                <asp:Panel runat="server" DefaultButton="SearchButton" EnableViewState="false">
                    <table>
                        <tr>
                            <td style="vertical-align:top">
                                <dxe:ASPxTextBox runat="server" ID="QueryTextBox" ClientInstanceName="QueryTextBox"
                                    Width="800px">
                                </dxe:ASPxTextBox>
                            </td>
                            <td style="vertical-align:top">
                                <dxe:ASPxButton runat="server" ID="SearchButton" AutoPostBack="false" Text="Найти"
                                    Height="17px" Width="120px">
                                    <ClientSideEvents Click="function(s,e){ doSearch(); }" />
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td style="text-align:right">
                                <div id="hideExSearchButton" <%=!showExtendedSearch ? "style=\"display: none\"": null %> >
                                    <a class="btn-slide" style="font-size: 0.8em; display: block;" href="javascript:void(0);" onclick="showPanel(false);">Cкрыть</a>
                                </div>
                                <div id="showExSearchButton" <%=showExtendedSearch ? "style=\"display: none\"": null %> >
                                    <a class="btn-slide" style="font-size: 0.8em; display: block;" href="javascript:void(0);" onclick="showPanel(true);">Расширенный поиск</a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <table style="width:1000px">
        <tr>
            <td valign="top" align="left" style="width: 99%;  vertical-align: top;">
                <ul style="margin-top: 1.5em; width: 700px">
                    <asp:PlaceHolder runat="server" ID="NoResultMessage" EnableViewState="false" Visible="false">
                        <li><span class="no-result">По Вашему запросу не найдено ни одного документа.</span></li>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="NoSearch" EnableViewState="false" Visible="false">
                        <li><span class="no-result">Поиск временно недоступен.</span></li>
                    </asp:PlaceHolder>
                    <asp:Repeater runat="server" ID="result" EnableViewState="false">
                        <ItemTemplate>
                            <li style="margin-bottom: 20px;">
                                <%#getItem(Container.DataItem)%>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            <%#getPager()%>
                        </FooterTemplate>
                    </asp:Repeater>
                </ul>
            </td>
            <td valign="top" align="left" style="padding-top:33px; width: 1%;">
                <div style="display:<%=showExtendedSearch ? "block" : "none"%>; overflow: hidden;" class="panel">
                    <table width="100%">
                        <tr>
                            <td>
                                <dxnb:ASPxNavBar ID="TypesNavBar" ClientInstanceName="TypesNavBar" runat="server"
                                    AllowSelectItem="True" EnableAnimation="True" Width="450">
                                    <GroupHeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <Groups>
                                        <dxnb:NavBarGroup Expanded="true" Text="Планы информатизации:">
                                            <Items>
                                                <dxnb:NavBarItem Name="Plan">
                                                    <Template>
                                                        <dxe:ASPxListBox runat="server" ID="PlanTypeListBox" ClientInstanceName="PlanTypeListBox"
                                                            SelectionMode="CheckColumn" Height="170px" Width="404px" ValueField="Id" ValueType="System.Int32"
                                                            TextField="Name">
                                                            <ItemStyle Wrap="True" />
                                                        </dxe:ASPxListBox>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Формы сведений:">
                                            <Items>
                                                <dxnb:NavBarItem Name="Preliminary">
                                                    <Template>
                                                        <dxe:ASPxListBox runat="server" ID="PreliminaryTypeListBox" ClientInstanceName="PreliminaryTypeListBox"
                                                            SelectionMode="CheckColumn" Height="60px" Width="404px" ValueField="Id" ValueType="System.Int32"
                                                            TextField="Name">
                                                            <ItemStyle Wrap="True" />
                                                        </dxe:ASPxListBox>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Уточненные планы:">
                                            <Items>
                                                <dxnb:NavBarItem Name="Plan2012">
                                                    <Template>
                                                        <dxe:ASPxListBox runat="server" ID="Plan2012TypeListBox" ClientInstanceName="Plan2012TypeListBox"
                                                            SelectionMode="CheckColumn" Height="115px" Width="404px" ValueField="Id" ValueType="System.Int32"
                                                            TextField="Name">
                                                            <ItemStyle Wrap="True" />
                                                        </dxe:ASPxListBox>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Другие объекты:">
                                            <Items>
                                                <dxnb:NavBarItem Name="OtherTypes">
                                                    <Template>
                                                        <dxe:ASPxListBox runat="server" ID="TypeListBox" ClientInstanceName="TypeListBox"
                                                            SelectionMode="CheckColumn"  Height="235px" Width="404px" ValueField="Id" ValueType="System.Int32"
                                                            TextField="Name">
                                                            <ItemStyle Wrap="True" />
                                                        </dxe:ASPxListBox>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Ведомства:">
                                            <Items>
                                                <dxnb:NavBarItem Name="Departments">
                                                    <Template>
                                                        <dxe:ASPxListBox runat="server" ID="DepartmentListBox" ClientInstanceName="DepartmentListBox"
                                                            ValueField="Id" ValueType="System.Int32" TextField="Name" SelectionMode="CheckColumn"
                                                            Width="404px">
                                                            <ItemStyle Wrap="True" />
                                                        </dxe:ASPxListBox>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="ИКТ компоненты:">
                                            <Items>
                                                <dxnb:NavBarItem Name="IKT">
                                                    <Template>
                                                        <dxwgv:ASPxGridView runat="server" ID="IKTGrid" ClientInstanceName="IKTGrid" Width="404px"
                                                                AutoGenerateColumns="False" KeyFieldName="Id" EnableRowsCache="false" EnableViewState="false"> 
                                                                <Columns>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Название" FieldName="Name" 
                                                                        ShowInCustomizationForm="True" VisibleIndex="1">
                                                                        <DataItemTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <%#string.Format("<td style=\"width:{0}px\"></td>", Eval("Indent")) %>
                                                                                    <td><%#Eval("Name")%></td>
                                                                                </tr>
                                                                            </table>
                                                                        </DataItemTemplate>
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn FieldName="Year"
                                                                        GroupIndex="0" ShowInCustomizationForm="True" SortIndex="0"
                                                                        SortOrder="Ascending" VisibleIndex="2">
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="20px">
                                                                        <ClearFilterButton Visible="False">
                                                                        </ClearFilterButton>
                                                                    </dxwgv:GridViewCommandColumn>
                                                                </Columns>
                                                                <ClientSideEvents RowClick="OnRowClick" />
                                                                <Styles>
                                                                    <GroupRow BackColor="#c0c0c0"></GroupRow>
                                                                    <SelectedRow CssClass="dxgvStatusSelectedRow_MetropolisBlue"></SelectedRow>
                                                                    <Row CssClass="dxgvStatusRow_MetropolisBlue"></Row>                                                                    
                                                                </Styles>
                                                                <SettingsBehavior AllowDragDrop="False" 
                                                                    AllowSort="False" AutoExpandAllGroups="True" EnableRowHotTrack="True" 
                                                                    AllowSelectByRowClick="True" />
                                                                <SettingsPager Visible="False" Mode="ShowAllRecords">
                                                                </SettingsPager>
                                                                <Settings ShowColumnHeaders="False" ShowGroupButtons="False" GridLines="None"
                                                                    GroupFormat="{1} {2}" ShowVerticalScrollBar="True" />
                                                                    <SettingsPager></SettingsPager>
                                                            </dxwgv:ASPxGridView>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Статусы:">
                                            <Items>
                                                <dxnb:NavBarItem Name="Statuses">
                                                    <Template>
                                                            <dxwgv:ASPxGridView runat="server" ID="StatusGrid" ClientInstanceName="StatusGrid" Width="404px"
                                                                AutoGenerateColumns="False" KeyFieldName="Id" EnableRowsCache="false" EnableViewState="false"> 
                                                                <Columns>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Название" FieldName="Name" 
                                                                        ShowInCustomizationForm="True" VisibleIndex="1">
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn FieldName="StatusGroup.Name" 
                                                                        GroupIndex="0" ShowInCustomizationForm="True" SortIndex="0" 
                                                                        SortOrder="Ascending" VisibleIndex="2">
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="20px">
                                                                        <ClearFilterButton Visible="False">
                                                                        </ClearFilterButton>
                                                                    </dxwgv:GridViewCommandColumn>
                                                                </Columns>
                                                                <ClientSideEvents RowClick="OnRowClick" />
                                                                <Styles>
                                                                    <GroupRow BackColor="#c0c0c0"></GroupRow>
                                                                    <SelectedRow CssClass="dxgvStatusSelectedRow_MetropolisBlue"></SelectedRow>
                                                                    <Row CssClass="dxgvStatusRow_MetropolisBlue"></Row>                                                                    
                                                                </Styles>
                                                                <SettingsBehavior AllowDragDrop="False" 
                                                                    AllowSort="False" AutoExpandAllGroups="True" EnableRowHotTrack="True" 
                                                                    AllowSelectByRowClick="True" />
                                                                <SettingsPager Visible="False" Mode="ShowAllRecords">
                                                                </SettingsPager>
                                                                <Settings ShowColumnHeaders="False" ShowGroupButtons="False" GridLines="None"
                                                                    GroupFormat="{1} {2}" ShowVerticalScrollBar="True" />
                                                                    <SettingsPager></SettingsPager>
                                                            </dxwgv:ASPxGridView>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Дата изменения:">
                                            <Items>
                                                <dxnb:NavBarItem Name="LastMod">
                                                    <Template>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    с:&nbsp;<dxe:ASPxDateEdit runat="server" ID="LastMod1DateEdit" ClientInstanceName="LastMod1DateEdit">
                                                                    </dxe:ASPxDateEdit>
                                                                </td>
                                                                <td>
                                                                    по:&nbsp;<dxe:ASPxDateEdit runat="server" ID="LastMod2DateEdit" ClientInstanceName="LastMod2DateEdit">
                                                                    </dxe:ASPxDateEdit>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Год:">
                                            <Items>
                                                <dxnb:NavBarItem Name="Year">
                                                    <Template>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    с:&nbsp;<dxe:ASPxSpinEdit runat="server" ID="Year1SpinEdit" ClientInstanceName="Year1SpinEdit"
                                                                        MinValue="2009" MaxValue="3000">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    по:&nbsp;<dxe:ASPxSpinEdit runat="server" ID="Year2SpinEdit" ClientInstanceName="Year2SpinEdit"
                                                                        MinValue="2009" MaxValue="3000">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Другое:">
                                            <Items>
                                                <dxnb:NavBarItem Name="Other">
                                                    <Template>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    Номер:&nbsp;<dxe:ASPxSpinEdit runat="server" ID="NumberSpinEdit" ClientInstanceName="NumberSpinEdit"
                                                                        MinValue="0" MaxValue="1000000000">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Куратор:&nbsp;<dxe:ASPxTextBox runat="server" ID="CuratorTextBox" ClientInstanceName="CuratorTextBox"
                                                                        Width="404px">
                                                                    </dxe:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Автор:&nbsp;<dxe:ASPxTextBox runat="server" ID="AuthorTextBox" ClientInstanceName="AuthorTextBox"
                                                                        Width="404px">
                                                                    </dxe:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Финансовое обеспечение за счет средств:">
                                            <Items>
                                                <dxnb:NavBarItem Name="Plan2010Sum">
                                                    <Template>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    федерального бюджета от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sF1SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sF1SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sF2SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sF2SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    регионального бюджета, местных бюджетов от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sR1SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sR1SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sR2SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sR2SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    государственных внебюджетных фондов от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sV1SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sV1SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sV2SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sV2SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Итого от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sSum1SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sSum1SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sSum2SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sSum2SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Основной объём бюджетных ассигнований:">
                                            <Items>
                                                <dxnb:NavBarItem Name="Plan2011Sum">
                                                    <Template>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    На очередной финансовый год от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sY01SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sY01SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sY02SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sY02SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    На первый год планового периода от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sY11SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sY11SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sY12SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sY12SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    На второй год планового периода от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sY21SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sY21SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sY22SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sY22SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Дополнительная потребность:">
                                            <Items>
                                                <dxnb:NavBarItem Name="Plan2011AddSum">
                                                    <Template>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    На очередной финансовый год от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sAddY01SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sAddY01SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sAddY02SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sAddY02SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    На первый год планового периода от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sAddY11SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sAddY11SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sAddY12SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sAddY12SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    На второй год планового периода от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sAddY21SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sAddY21SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sAddY22SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sAddY22SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                        <dxnb:NavBarGroup Expanded="false" Text="Итого:">
                                            <Items>
                                                <dxnb:NavBarItem Name="Plan2011TotalSum">
                                                    <Template>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    На очередной финансовый год от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sSumY01SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sSumY01SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sSumY02SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sSumY02SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    На первый год планового периода от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sSumY11SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sSumY11SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sSumY12SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sSumY12SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    На второй год планового периода от:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sSumY21SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sSumY21SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    до:
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit runat="server" ID="sSumY22SpinEdit" DecimalPlaces="2" NumberType="Float" ClientInstanceName="sSumY22SpinEdit"
                                                                        MinValue="0" MaxValue="1000000000" Width="100px">
                                                                        <SpinButtons ShowIncrementButtons="False"></SpinButtons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    тыс.руб.
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Template>
                                                </dxnb:NavBarItem>
                                            </Items>
                                        </dxnb:NavBarGroup>
                                    </Groups>
                                </dxnb:ASPxNavBar>
                            </td>
                            <td style="background-color: #c4df9b; width:2px;" valign="middle">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>

    </div>
</asp:Content>
