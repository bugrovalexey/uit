﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class IKTObjectProvisionTypeMap : ClassMapBase<IKTObjectProvisionType>
    {
        public IKTObjectProvisionTypeMap()
            : base("IKT_Object_Provision_Type")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "IKT_Object_Provision_Type_Name");
        }
    }
}