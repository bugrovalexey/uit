﻿using System;
using System.Runtime.Serialization;

namespace Core.Import.ESKI
{
    /// <summary>План</summary>
    [DataContract]
    public class Plan : IEntity
    {
        public string EntityName { get { return "Plan"; } }

        ///<summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string  ExtId { get; set; }
        
        /// <summary>Год</summary>
        [DataMember(Order = 2, IsRequired = true)]
        [Annotation("Год")]
        public int Year { get; set; }

        /// <summary>Код ОГВ</summary>
        [DataMember(Order = 3, IsRequired = true)]
        [Annotation("Код ОГВ")]
        public string OGVCode { get; set; }

        /// <summary>Логин пользователя</summary>
        [DataMember(Order = 4, IsRequired = true)]
        [Annotation("Логин пользователя")]
        public string UserLogin { get; set; }


        //// <summary>Документы</summary>
        //[DataMember(Order = 5)]
        //[Annotation("Документы")]
        //public Document[] DocumentList { get; set; }

        /// <summary>Список мероприятий</summary>
        [DataMember(Order = 6)]
        [Annotation("Список мероприятий")]
        public Activity[] ActivityList { get; set; }

        [DataMember(Order = 7)]
        [Annotation("Список мероприятий для муниципальных объектов")]
        public Plans_Activity_MU[] MUList { get; set; }

        //[DataMember(Order = 8)]
        //[Annotation("Список планов - график")]
        //public Schedule[] ScheduleList { get; set; }

        //[DataMember(Order = 9)]
        //[Annotation("Список систем проектного управления")]
        //public ProjectManagementSystem[] ProjectManagementSystemtList { get; set; }

        //[DataMember(Order = 10)]
        //[Annotation("Список сведений о структуре органа управления")]
        //public GovStruct[] GovStructList { get; set; }

        //[DataMember(Order = 11)]
        //[Annotation("Список должностных лиц")]
        //public Official[] OfficialList { get; set; }

        //[DataMember(Order = 12)]
        //[Annotation("Список отчётных форм по проектной деятельности")]
        //public ReportForm[] ReportFormList { get; set; }

        ///// <summary>Письма(печатные формы)</summary>
        //[DataMember(Order = 13)]
        //[Annotation("Письма(печатные формы)")]
        //public PaperObject[] PaperObjectList { get; set; }
    }

    /// <summary>Мероприятие</summary>
    [DataContract]
    [Annotation("Мероприятие")]
    public class Activity : IEntity
    {
        public string EntityName { get { return "Activity"; } }
        
        /// <summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Код</summary>
        [Annotation("Код")]
        public string Code { get; set; }

        /// <summary>Наименование</summary>
        [DataMember(Order = 3)]
        [Annotation("Наименование")]
        public string Name { get; set; }

        /// <summary>Аннотация</summary>
        [DataMember(Order = 4)]
        [Annotation("Аннотация")]
        public string Annotation { get; set; }

        /// <summary>Номер</summary>
        [Annotation("Номер")]
        public int Num { get; set; }

        /// <summary>Ид типа мероприятия 0-Не задано, 1-Создание, 2-Развитие, 3-Модернизация, 4-Эксплуатация</summary>
        [DataMember(Order = 6)]
        [Annotation("Ид типа мероприятия")]
        public int TypeID { get; set; }

        ///// <summary>Ид номера рееста ФГИС</summary>
        //[DataMember(Order = 7)]
        //[Annotation("Ид номера рееста ФГИС")]
        //public int FGISInfoID { get; set; }

        ///// <summary>Наименование ФГИС</summary>
        //[DataMember(Order = 8)]
        //[Annotation("Наименование ФГИС")]
        //public string FGISName { get; set; }        

        /// <summary>Номер реестра ФГИС</summary>
        [DataMember(Order = 9)]
        [Annotation("Номер реестра ФГИС")]
        public string FGISInfoText { get; set; }

        /// <summary>Код классификационной категории ИС или компонента ИТКИ</summary>
        [DataMember(Order = 10)]
        [Annotation("Код классификационной категории ИС или компонента ИТКИ")]
        public string IKTCode { get; set; }

        ///// <summary>Ид паспорта в аис учёта</summary>
        //[DataMember(Order = 11)]
        //[Annotation("Ид паспорта в аис учёта")]
        //public int RegistryImportID { get; set; }

        ///// <summary>Ид ИКТ компонента</summary>
        //[DataMember(Order = 12)]
        //[Annotation("Ид ИКТ компонента")]
        //public int RegistryImportIKTID { get; set; }

        /// <summary>Приоритетное</summary>
        [DataMember(Order = 13)]
        [Annotation("Приоритетное")]
        public string Priority { get; set; }

        /// <summary>ФИО ответственного</summary>
        [DataMember(Order = 14)]
        [Annotation("ФИО ответственного")]
        public string ResponsibleName { get; set; }

        /// <summary>Должность ответственного</summary>
        [DataMember(Order = 15)]
        [Annotation("Должность ответственного")]
        public string ResponsibleJob { get; set; }

        /// <summary>Телефон ответственного</summary>
        [DataMember(Order = 16)]
        [Annotation("Телефон ответственного")]
        public string ResponsiblePhone { get; set; }

        /// <summary>Email ответственного</summary>
        [DataMember(Order = 17)]
        [Annotation("Email ответственного")]
        public string ResponsibleEmail { get; set; }

        /// <summary>Код по БК ГРБС</summary>
        [DataMember(Order = 18)]
        [Annotation("Код по БК ГРБС")]
        public string GRBSCode { get; set; }

        /// <summary>Код по БК ПЗ/РЗ</summary>
        [DataMember(Order = 19)]
        [Annotation("Код по БК ПЗ/РЗ")]
        public string SectionKBKCode { get; set; }

        /// <summary>Код по БК ЦСР</summary>
        [DataMember(Order = 20)]
        [Annotation("Код по БК ЦСР")]
        public string ExpenseItemCode { get; set; }

        /// <summary>Код по БК ВР</summary>
        [DataMember(Order = 21)]
        [Annotation("Код по БК ВР")]
        public string WorkFormCode { get; set; }

        /// <summary>Подпрограмма</summary>
        [DataMember(Order = 22)]
        [Annotation("Подпрограмма")]
        public string Subentry { get; set; }

        /// <summary>Величина фактических расходов по состоянию на дату</summary>
        [DataMember(Order = 23)]
        [Annotation("Величина фактических расходов по состоянию на дату")]
        public DateTime? FactDate { get; set; }

        /// <summary>Объем планируемых бюджетных ассигнований на очередной финансовый год</summary>
        [DataMember(Order = 24)]
        [Annotation("Объем планируемых бюджетных ассигнований на очередной финансовый год")]
        public double VolY0 { get; set; }

        /// <summary>Объем планируемых бюджетных ассигнований на первый год планового периода</summary>
        [DataMember(Order = 25)]
        [Annotation("Объем планируемых бюджетных ассигнований на первый год планового периода")]
        public double VolY1 { get; set; }

        /// <summary>Объем планируемых бюджетных ассигнований на второй год планового периода</summary>
        [DataMember(Order = 26)]
        [Annotation("Объем планируемых бюджетных ассигнований на второй год планового периода")]
        public double VolY2 { get; set; }

        ///// <summary>Дополнительная потребность на очередной финансовый год</summary>
        //[DataMember(Order = 27)]
        //[Annotation("Дополнительная потребность на очередной финансовый год")]
        //public double AddVolY0 { get; set; }

        ///// <summary>Дополнительная потребность на первый год планового периода</summary>
        //[DataMember(Order = 28)]
        //[Annotation("Дополнительная потребность на первый год планового периода")]
        //public double AddVolY1 { get; set; }

        ///// <summary>Дополнительная потребность на второй год планового периода</summary>
        //[DataMember(Order = 29)]
        //[Annotation("Дополнительная потребность на второй год планового периода")]
        //public double AddVolY2 { get; set; }

        /// <summary>Объем фактически израсходованных бюджетных ассигнований на очередной финансовый год</summary>
        [DataMember(Order = 30)]
        [Annotation("Объем фактически израсходованных бюджетных ассигнований на очередной финансовый год")]
        public double FactVolY0 { get; set; }

        /// <summary>Объем фактически израсходованных бюджетных ассигнований на первый год планового периода</summary>
        [DataMember(Order = 31)]
        [Annotation("Объем фактически израсходованных бюджетных ассигнований на первый год планового периода")]
        public double FactVolY1 { get; set; }

        /// <summary>Объем фактически израсходованных бюджетных ассигнований на второй год планового периода</summary>
        [DataMember(Order = 32)]
        [Annotation("Объем фактически израсходованных бюджетных ассигнований на второй год планового периода")]
        public double FactVolY2 { get; set; }

        /// <summary>Список целевых индикаторов</summary>
        [DataMember(Order = 33)]
        [Annotation("Список целевых индикаторов")]
        public Goal[] GoalList { get; set; }

        /// <summary>Список оснований для реализации мероприятия</summary>
        [DataMember(Order = 34)]
        [Annotation("Список оснований для реализации мероприятия")]
        public Reason[] ReasonList { get; set; }

        ///// <summary>Список наименований программных решений в ФАП</summary>
        //[DataMember(Order = 35)]
        //[Annotation("Список наименований программных решений в ФАП")]
        //public ApfInfo[] ApfInfoList { get; set; }

        ///// <summary>Список функций и полномочий</summary>
        //[DataMember(Order = 36)]
        //[Annotation("Список функций и полномочий")]
        //public Function[] FunctionList { get; set; }

        /// <summary>Список заключенных государственных контрактов</summary>
        [DataMember(Order = 37)]
        [Annotation("Список заключенных государственных контрактов")]
        public PlansStateContract[] PlansStateContractList { get; set; }

        /// <summary>Список показателей / индикаторов</summary>
        [DataMember(Order = 38)]
        [Annotation("Список показателей / индикаторов")]
        public Indicator[] IndicatorList { get; set; }

        /// <summary>Список спецификаций</summary>
        [DataMember(Order = 39)]
        [Annotation("Список спецификаций")]
        public Spec[] SpecList { get; set; }

        /// <summary>Список работ</summary>
        [DataMember(Order = 40)]
        [Annotation("Список работ")]
        public Work[] WorkList { get; set; }

        ///// <summary>Список ЦОД'ов</summary>
        //[DataMember(Order = 41)]
        //[Annotation("Список ЦОД'ов")]
        //public Zod[] ZodList { get; set; }

        /// <summary>Список направлений расходов по мероприятию Формы сведений</summary>
        [DataMember(Order = 42)]
        [Annotation("Список направлений расходов по мероприятию Формы сведений")]
        public ExpenseDirection[] ExpenseDirectionList { get; set; }   
    }

    /// <summary>Цель мероприятия</summary>
    [DataContract]
    [Annotation("Цель мероприятия")]
    public class Goal : IEntity
    {
        public string EntityName { get { return "Goal"; } }

        /// <summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Наименование</summary>
        [DataMember(Order = 2)]
        [Annotation("Наименование")]
        public string Name { get; set; }
    }

    /// <summary>Наименование и реквизиты основания реализации мероприятия</summary>
    [DataContract]
    [Annotation("Наименование и реквизиты основания реализации мероприятия")]
    public class Reason : IEntity
    {
        public string EntityName { get { return "Reason"; } }

        /// <summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        ///// <summary>Вид документа НПА</summary>
        //[DataMember(Order = 2)]
        //[Annotation("Вид документа НПА")]
        //public int NPATypeID { get; set; }

        ///// <summary>Тип документа НПА</summary>
        //[DataMember(Order = 3)]
        //[Annotation("Тип документа НПА")]
        //public int NPAKindID { get; set; }

        /// <summary>Наименование НПА</summary>
        [DataMember(Order = 4)]
        [Annotation("Наименование НПА")]
        public string NPAName { get; set; }

        /// <summary>Номер НПА</summary>
        [DataMember(Order = 5)]
        [Annotation("Номер НПА")]
        public string NPANum { get; set; }

        /// <summary>Дата НПА</summary>
        [DataMember(Order = 6)]
        [Annotation("Дата НПА")]
        public DateTime? NPADate { get; set; }

        ///// <summary>Пункт, статья документа НПА</summary>
        //[DataMember(Order = 7)]
        //[Annotation("Пункт, статья документа НПА")]
        //public string NPAItem { get; set; }

        ///// <summary>Список документов - оснований</summary>
        //[DataMember(Order = 8)]
        //[Annotation("Список документов - оснований")]
        //public Document[] DocumentList { get; set; }

        /// <summary>Ссылка на документ НПА</summary>
        [DataMember(Order = 9)]
        [Annotation("Ссылка на документ НПА")]
        public string NPALink { get; set; }

    }

   

    /// <summary>Группа направлений расходов по мероприятию Формы сведений</summary>
    [DataContract]
    [Annotation("Группа направлений расходов по мероприятию Формы сведений")]
    public class ExpenseDirection : IEntity
    {
        public string EntityName { get { return "ExpenseDirection"; } }

        /// <summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Ид направления расходов</summary>
        [DataMember(Order = 2)]
        [Annotation("Код направления расходов")]
        public string ExpenseDirectionCode { get; set; }

        /// <summary>Основной объем Очередной год</summary>
        [DataMember(Order = 3)]
        [Annotation("Основной объем Очередной год")]
        public Decimal ExpenseVolY0 { get; set; }

        /// <summary>Основной объем Первый год</summary>
        [DataMember(Order = 4)]
        [Annotation("Основной объем Первый год")]
        public Decimal ExpenseVolY1 { get; set; }

        /// <summary>Основной объем Второй год</summary>
        [DataMember(Order = 5)]
        [Annotation("Основной объем Второй год")]
        public Decimal ExpenseVolY2 { get; set; }

        ///// <summary>Дополнительная потребность Очередной год</summary>
        //[DataMember(Order = 6)]
        //[Annotation("Дополнительная потребность Очередной год")]
        //public Decimal ExpenseAddVolY0 { get; set; }

        ///// <summary>Дополнительная потребность Первый год</summary>
        //[DataMember(Order = 7)]
        //[Annotation("Дополнительная потребность Первый год")]
        //public Decimal ExpenseAddVolY1 { get; set; }

        ///// <summary>Дополнительная потребность Второй год</summary>
        //[DataMember(Order = 8)]
        //[Annotation("Дополнительная потребность Второй год")]
        //public Decimal ExpenseAddVolY2 { get; set; }
    }

    /// <summary>Заключенные государственные контракты</summary>
    [DataContract]
    [Annotation("Заключенные государственные контракты")]
    public class PlansStateContract : IEntity
    {
        public string EntityName { get { return "PlansStateContract"; } }

        /// <summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Реестровый номер госконтракта</summary>
        [DataMember(Order = 2)]
        [Annotation("Реестровый номер госконтракта")]
        public string ContractCode { get; set; }

        /// <summary>Описание ранее достигнутых результатов</summary>
        [DataMember(Order = 3)]
        [Annotation("Описание ранее достигнутых результатов")]
        public string ResultDescription { get; set; }
    }

    /// <summary>Целевой показатель</summary>
    [DataContract]
    [Annotation("Целевой показатель")]
    public class Indicator : IEntity
    {
        public string EntityName { get { return "Indicator"; } }
        
        /// <summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }
        
        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }
        
        ///// <summary>Дополнительное финансирование</summary>
        //[DataMember(Order = 1)]
        //[Annotation("   ")]
        //public string AddFounding { get; set; }
        ///// <summary>Ссылка на функцию и полномочия</summary>
        //[DataMember(Order = 2)]
        //[Annotation("Ссылка на функцию и полномочия")]
        //public string PlansActivityFuncRef { get; set; }
        ///// <summary>Ссылка на цель мероприятия</summary>
        //[DataMember(Order = 3)]
        //[Annotation("Ссылка на цель мероприятия")]
        //public string PlansActivityGoalRef { get; set; }
        
        /// <summary>Тип, 1 - индикатор или 0 - показатель</summary>
        [DataMember(Order = 4)]
        [Annotation("Тип, 1 - индикатор или 0 - показатель")]
        public int FeatureType { get; set; }
        
        /// <summary>Наименование</summary>
        [DataMember(Order = 5)]
        [Annotation("Наименование")]
        public string Name { get; set; }
        
        /// <summary>Год</summary>
        [DataMember(Order = 6)]
        [Annotation("Год")]
        public int Year { get; set; }
        
        /// <summary>Единица измерения</summary>
        [DataMember(Order = 7)]
        [Annotation("   ")]
        public string UOM { get; set; }
        
        /// <summary>Фактическое значение</summary>
        [DataMember(Order = 8)]
        [Annotation("   ")]
        public string PRSFactVal { get; set; }
        
        /// <summary>Алгоритм</summary>
        [DataMember(Order = 9)]
        [Annotation("   ")]
        public string Algorithm { get; set; }

        /// <summary>Ссылка на родительский целевой показатель (Indicator.ExtId)</summary>
        [DataMember(Order = 10)]
        [Annotation("Ссылка на родительский объект (ExtId)")]
        public string ParentRef { get; set; }

        //public string FeatureTypeStr { get; set; }
        //public string ParentName { get; set; }
        // public string IsNotCurrent { get; set; }
        // public string Func { get; set; }        
        //public string Goal { get; set; }
    }

    /// <summary>Работа</summary>
    [DataContract]
    [Annotation("Работа")]
    public class Work : IEntity
    {
        public string EntityName { get { return "Work"; } }
        
        /// <summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }
        
        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }
        
        /// <summary>Наименование</summary>
        [DataMember(Order = 1)]
        [Annotation("Наименование")]
        public string Name { get; set; }
        
        /// <summary>Номер</summary>
        [Annotation("Номер")]
        public int Num { get; set; }
        
        /// <summary>Код вида расходов</summary>
        [DataMember(Order = 3)]
        [Annotation("Код вида расходов")]
        public string ExpenseDirectionCode { get; set; }

        ///// <summary>Ид ОКДП</summary>
        //[DataMember(Order = 4)]
        //[Annotation("Ид ОКДП")]
        //public int OKDPID { get; set; }
        ///// <summary>название ОКДП</summary>
        //[DataMember(Order = 5)]
        //[Annotation("название ОКДП")]
        //public string OKDPText { get; set; }
        
        /// <summary>код ОКДП</summary>
        [DataMember(Order = 5)]
        [Annotation("Код ОКДП")]
        public string OKDPCode { get; set; }

        ///// <summary>Ид формы работы</summary>
        //[DataMember(Order = 6)]
        //[Annotation("Ид формы работы")]
        //public int WorkFormID { get; set; }

        /// <summary>Код типа расходов</summary>
        [DataMember(Order = 7)]
        [Annotation("Код типа расходов")]
        public string ExpenseTypeCode { get; set; }

        /// <summary>Список типов работ</summary>
        [DataMember(Order = 8)]
        [Annotation("Список типов работ")]
        public WorkType[] WorkTypeList { get; set; }

        ///// <summary>Ид секции КБК</summary>
        //[DataMember(Order = 0)]
        //[Annotation("   ")]
        //public int SectionKBKID { get; set; }
        ///// <summary>Ид элемента расходов</summary>
        //[DataMember(Order = 0)]
        //[Annotation("   ")]
        //public int ExpenseItemID { get; set; }
        ///// <summary>Ид ГРБС</summary>
        //[DataMember(Order = 0)]
        //[Annotation("   ")]
        //public int GRBSID { get; set; }
        ///// <summary>Ид номера в реестре ФГИС</summary>
        //[DataMember(Order = 0)]
        //[Annotation("   ")]
        //public int FgisInfoID { get; set; }
        ///// <summary>Ид номера программного решения ФАП</summary>
        //[DataMember(Order = 0)]
        //[Annotation("   ")]
        //public int ApfInfoID { get; set; }
        ///// <summary>Ид статуса работ</summary>
        //[DataMember(Order = 0)]
        //[Annotation("   ")]
        //public int StatusID { get; set; }
        ///// <summary>Номер для кода</summary>
        //[DataMember(Order = 0)]
        //[Annotation("   ")]
        //public string WorkForm_CodeName { get; set; }
        //public int OrigNum { get; set; }
        //public string CodeName { get; set; }
        //public string CodeNum { get; set; }
        //public string OKDP_CodeName { get; set; }

    }

    /// <summary>Тип работы</summary>
    [DataContract]
    [Annotation("Тип работы")]
    public class WorkType : IEntity
    {
        public string EntityName { get { return "WorkType"; } }
        /// <summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }
     
        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Код типа затрат</summary>
        [DataMember(Order = 1)]
        [Annotation("Код типа затрат")]
        public string ExpenseTypeCode { get; set; }
        
        /// <summary>Расходы на очередной финансовый год</summary>
        [DataMember(Order = 2)]
        [Annotation("Расходы на очередной финансовый год")]
        public double ExpY0 { get; set; }

        //public string ExpenseTypeName { get; set; }

    }

    /// <summary>Спецификация</summary>
    [DataContract]
    [Annotation("Спецификация")]
    public class Spec : IEntity
    {
        public string EntityName { get { return "Spec"; } }
        
        /// <summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }
        
        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Номер</summary>
        [Annotation("Номер")]
        public int Num { get; set; }
        
        /// <summary>Наименование</summary>
        [DataMember(Order = 2)]
        [Annotation("Наименование")]
        public string Name { get; set; }

        /// <summary>Код по БК ВР</summary>
        [DataMember(Order = 3)]
        [Annotation("Код по БК ВР")]
        public string WorkFormCode { get; set; }
        
        /// <summary>Код направления расходов</summary>
        [DataMember(Order = 4)]
        [Annotation("Код направления расходов")]
        public string ExpenseDirectionCode { get; set; }
        
        /// <summary>Аналог</summary>
        [DataMember(Order = 5)]
        [Annotation("Аналог")]
        public string Analog { get; set; }
        
        /// <summary>Количество</summary>
        [DataMember(Order = 6)]
        [Annotation("Количество")]
        public string Quantity { get; set; }
        
        ///// <summary>Цена аналога, тыс. руб.</summary>
        //[DataMember(Order = 7)]
        //[Annotation("Цена аналога, тыс. руб.")]
        //public double AnalogPrice { get; set; }
        
        /// <summary>Стоимость</summary>
        [DataMember(Order = 8)]
        [Annotation("Стоимость")]
        public double Cost { get; set; }
        
        /// <summary>Код типа оборудования</summary>
        [DataMember(Order = 9)]
        [Annotation("Код типа оборудования")]
        public string QualifierCode { get; set; }

        ///// <summary>Ид ОДКП</summary>
        //[DataMember(Order = 10)]
        //[Annotation("Ид ОДКП")]
        //public int OKDPID { get; set; }
        ///// <summary>Название ОДКП</summary>
        //[DataMember(Order = 11)]
        //[Annotation("Название ОДКП")]
        //public string OKDPText { get; set; }
        
        /// <summary>Код ОКДП</summary>
        [DataMember(Order = 11)]
        [Annotation("Код ОКДП")]
        public string OKDPCode { get; set; }

        ///// <summary>Ид ГРБС</summary>
        //[DataMember(Order = 12)]
        //[Annotation("Код ГРБС")]
        //public string GRBSCode { get; set; }
        
        ///// <summary>Код раздела/подраздела КБК</summary>
        //[DataMember(Order = 13)]
        //[Annotation("Код раздела/подраздела КБК")]
        //public string SectionKBKCode { get; set; }

        ///// <summary>Код по БК ЦСР</summary>
        //[DataMember(Order = 14)]
        //[Annotation("Код по БК ЦСР")]
        //public string ExpenseItemCode { get; set; }
        
        /// <summary>Номер для кода</summary>
        [Annotation("Номер для кода")]
        public int OrigNum { get; set; }
        
        /// <summary>Список типов затрат</summary>
        [DataMember(Order = 16)]
        [Annotation("Список типов затрат")]
        public ExpenseType[] ExpenseTypeList { get; set; }

        /// <summary>Список типов спецификаций</summary>
        [DataMember(Order = 17)]
        [Annotation("Список типов спецификаций")]
        public SpecType[] SpecTypeList { get; set; }

        //public string CodeNum { get; set; }
        //public string Qualifier { get; set; }
        //public string WorkForm_CodeName { get; set; }
        //public string ExpenseDirection_CodeName { get; set; }
        //public string SpecExpenseType { get; set; }
        //public int ExpenseTypeID { get; set; }
    }

    /// <summary>Тип затрат (связь спецификации с типом затрат)</summary>
    [DataContract]
    [Annotation("Тип затрат (связь спецификации с типом затрат)")]
    public class ExpenseType
    {
        public string EntityName { get { return "ExpenseType"; } }
        
        /// <summary>Идентификатор типа затрат</summary>
        [Annotation("Идентификатор типа затрат")]
        public int Id { get; set; }

        /// <summary>Название</summary>
        [Annotation("Название")]
        public string Name { get; set; }
    }

    /// <summary>Тип спецификации</summary>
    [DataContract]
    [Annotation("Тип спецификации")]
    public class SpecType : IEntity
    {
        public string EntityName { get { return "SpecType"; } }
        
        /// <summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }
        
        /// <summary>Наименование характеристики</summary>
        [DataMember(Order = 1)]
        [Annotation("Наименование характеристики")]
        public string CharName { get; set; }
        
        /// <summary>Единица измерения</summary>
        [DataMember(Order = 2)]
        [Annotation("Единица измерения")]
        public string CharUnit { get; set; }
        
        /// <summary>Значение характеристики</summary>
        [DataMember(Order = 3)]
        [Annotation("Значение характеристики")]
        public string CharValue { get; set; }
    }


    /// <summary>Мероприятие для муниципальных объектов</summary>
    [DataContract]
    [Annotation("Мероприятие для муниципальных объектов")]
    public class Plans_Activity_MU : IEntity
    {
        public string EntityName { get { return "Plans_Activity_MU"; } }
     
        /// <summary>Идентификатор в УВиРИ</summary>
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1, IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Код по БК ПЗ/РЗ</summary>
        [DataMember(Order = 1)]
        [Annotation("Код по БК ПЗ/РЗ")]
        public string SectionKBKCode { get; set; }

        /// <summary>Код по БК ГРБС</summary>
        [DataMember(Order = 2)]
        [Annotation("Код по БК ГРБС")]
        public string GRBSCode { get; set; }

        /// <summary>Код по БК ЦСР</summary>
        [DataMember(Order = 3)]
        [Annotation("Код по БК ЦСР")]
        public string ExpenseItemCode { get; set; }

        /// <summary>Код по БК ВР</summary>
        [DataMember(Order = 4)]
        [Annotation("Код по БК ВР")]
        public string WorkFormCode { get; set; }

        /// <summary>Наименование</summary>
        [DataMember(Order = 5)]
        [Annotation("Наименование")]
        public string Name { get; set; }

        /// <summary>Аннотация</summary>
        [DataMember(Order = 6)]
        [Annotation("Аннотация")]
        public string Annotation { get; set; }

        /// <summary>Объем фактически израсходованных бюджетных ассигнований на год, предшествующий отчетному финансовому году</summary>
        [DataMember(Order = 7)]
        [Annotation("Объем фактически израсходованных бюджетных ассигнований на год, предшествующий отчетному финансовому году")]
        public double Vol_FB_Y0 { get; set; }

        /// <summary>Объем фактически израсходованных бюджетных ассигнований на отчетный финансовый год</summary>
        [DataMember(Order = 8)]
        [Annotation("Объем фактически израсходованных бюджетных ассигнований на отчетный финансовый год")]
        public double Vol_FB_Y1 { get; set; }

        /// <summary>Объем фактически израсходованных бюджетных ассигнований на текущий финансовый год</summary>
        [DataMember(Order = 9)]
        [Annotation("Объем фактически израсходованных бюджетных ассигнований на текущий финансовый год")]
        public double Vol_FB_Y2 { get; set; }

        /// <summary>Объем фактически израсходованных  бюджетных ассигнований на очередной финансовый год</summary>
        [DataMember(Order = 10)]
        [Annotation("Объем фактически израсходованных  бюджетных ассигнований на очередной финансовый год")]
        public double Vol_B_Y0 { get; set; }

        /// <summary>Объем фактически израсходованных  бюджетных ассигнований на первый год планового периода</summary>
        [DataMember(Order = 11)]
        [Annotation("Объем фактически израсходованных  бюджетных ассигнований на первый год планового периода")]
        public double Vol_B_Y1 { get; set; }

        /// <summary>Объем фактически израсходованных  бюджетных ассигнований на второй год планового периода</summary>
        [DataMember(Order = 12)]
        [Annotation("Объем фактически израсходованных  бюджетных ассигнований на второй год планового периода")]
        public double Vol_B_Y2 { get; set; }

        //[DataMember(Order = 13)]
        //[Annotation("Дополнительная потребность на очередной финансовый год")]
        //public double Plans_Activity_MU_Vol_Add_Y0 { get; set; }
        //[DataMember(Order = 14)]
        //[Annotation("Дополнительная потребность на первый год планового периода")]
        //public double Plans_Activity_MU_Vol_Add_Y1 { get; set; }
        //[DataMember(Order = 15)]
        //[Annotation("Дополнительная потребность на второй год планового периода")]
        //public double Plans_Activity_MU_Vol_Add_Y2 { get; set; }

        /// <summary>Объем софинансирования на очередной финансовый год</summary>
        [DataMember(Order = 16)]
        [Annotation("Объем софинансирования на очередной финансовый год")]
        public double Vol_JF_Y0 { get; set; }

        /// <summary>Объем софинансирования на первый год, предшествующий очередному финансовому</summary>
        [DataMember(Order = 17)]
        [Annotation("Объем софинансирования на первый год, предшествующий очередному финансовому")]
        public double Vol_JF_Y1 { get; set; }

        /// <summary>Объем софинансирования на второй год, предшествующий очередному финансовому</summary>
        [DataMember(Order = 18)]
        [Annotation("Объем софинансирования на второй год, предшествующий очередному финансовому")]
        public double Vol_JF_Y2 { get; set; }

        /// <summary>Подпрограмма</summary>
        [DataMember(Order = 19)]
        [Annotation("Подпрограмма")]
        public string Subprogram { get; set; }

        /// <summary>Список регионов</summary>
        [DataMember(Order = 20)]
        [Annotation("Список регионов")]
        public Region[] RegionList { get; set; }
    }

    /// <summary>Связь региона с мероприятием для муниципальных объектов</summary>
    [DataContract]
    [Annotation("Связь региона с мероприятием для муниципальных объектов")]
    public class Region
    {
        public string EntityName { get { return "Region"; } }

        /// <summary>Код региона</summary>
        [DataMember(Order = 0)]
        [Annotation("Код региона")]
        public string OGVCode { get; set; }
    }

    

    ///// <summary>Документ (файл)</summary>
    //[DataContract]
    //[Annotation("Документ (файл)")]
    //public class Document : IEntity
    //{
    //    public string EntityName { get { return "Document"; } }

    //    /// <summary>Идентификатор в УВиРИ</summary>
    //    [Annotation("Идентификатор в УВиРИ")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Ид типа документа</summary>
    //    [Annotation("Ид типа документа")]
    //    public int DocTypeId { get; set; }

    //    /// <summary>Ид пользователя, загрузившего документ</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Ид пользователя, загрузившего документ")]
    //    public int UserId { get; set; }
        
    //    /// <summary>Номер</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Номер")]
    //    public string Num { get; set; }

    //    /// <summary>Дата создания</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Дата создания")]
    //    public DateTime DateCreate { get; set; }

    //    /// <summary>Название файла</summary>
    //    [DataMember(Order = 6)]
    //    [Annotation("Название файла")]
    //    public string FileName { get; set; }

    //    /// <summary>Комментарий</summary>
    //    [Annotation("Комментарий")]
    //    public string Comment { get; set; }

    //    /// <summary>Ссылка на файл</summary>
    //    [DataMember(Order = 8)]
    //    [Annotation("Ссылка на файл")]
    //    public string URL { get; set; }
    //}
}



/*
 Мусорка
 * 
 
    /// <summary>Функции и полномочия</summary>
    [DataContract]
    [Annotation("Функции и полномочия")]
    public class Function : IEntity
    {
        public string EntityName { get { return ""; } }

        /// <summary>Идентификатор в УВиРИ</summary>
        //[DataMember(Order = 0)]
        //[Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        //[DataMember(Order = 1)]
        //[Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Наименование</summary>
        [DataMember(Order = 2)]
        [Annotation("Наименование")]
        public string Name { get; set; }

        /// <summary>Ид вида</summary>
        [DataMember(Order = 3)]
        [Annotation("Ид вида")]
        public int FuncTypeID { get; set; }

        /// <summary>Ид типовой деятельности</summary>
        [DataMember(Order = 4)]
        [Annotation("Ид типовой деятельности")]
        public int TypicalActivitiesID { get; set; }

        /// <summary>Ид гос. услуги</summary>
        [DataMember(Order = 5)]
        [Annotation("Ид гос. услуги")]
        public int GosServiceId { get; set; }

        /// <summary>Флаг отсутствия функции в реестре гос услуг</summary>
        [DataMember(Order = 6)]
        [Annotation("Флаг отсутствия функции в реестре гос услуг")]
        public string IsMissing { get; set; }

        /// <summary>Название отсутствующей функции</summary>
        [DataMember(Order = 7)]
        [Annotation("Название отсутствующей функции")]
        public string MissingName { get; set; }

        /// <summary>Оказание государственной услуги (выполнение государственной функции) не осуществлялось до момента формирования мероприятия</summary>
        [DataMember(Order = 8)]
        [Annotation("Оказание государственной услуги (выполнение государственной функции) не осуществлялось до момента формирования мероприятия")]
        public string IsUsedBefore { get; set; }
        
        /// <summary>Тип НПА</summary>
        //[DataMember(Order = 9)]
        //[Annotation("Тип НПА")]
        public int NPATypeID { get; set; }

        /// <summary>Тип документа</summary>
        //[DataMember(Order = 10)]
        //[Annotation("Тип документа")]
        public int NPAKindID { get; set; }

        /// <summary>Наименование НПА</summary>
        [DataMember(Order = 11)]
        [Annotation("Наименование НПА")]
        public string NPAName { get; set; }

        /// <summary>Номер НПА</summary>
        [DataMember(Order = 12)]
        [Annotation("Номер НПА")]
        public string NPANum { get; set; }

        /// <summary>Дата НПА</summary>
        [DataMember(Order = 13)]
        [Annotation("Дата НПА")]
        public DateTime? NPADate { get; set; }

        /// <summary>Пункт, статья документа</summary>
        //[DataMember(Order = 14)]
        //[Annotation("Пункт, статья документа")]
        public string NPAItem { get; set; }

        /// <summary>Идентификатор НПА</summary>
        [DataMember(Order = 15)]
        [Annotation("Идентификатор НПА")]
        public int NPADocumentID { get; set; }
    }


    /// <summary>Связь мероприятия и программного решения в ФАП</summary>
    //[DataContract]
    //[Annotation("Связь мероприятия и программного решения в ФАП")]
    public class ApfInfo : IEntity
    {
        public string EntityName { get { return ""; } }

        /// <summary>Идентификатор связи</summary>
        //[DataMember(Order = 0)]
        //[Annotation("Идентификатор связи")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        //[DataMember(Order = 1)]
        //[Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Идентификатор программного решения</summary>
        //[DataMember(Order = 2)]
        //[Annotation("Идентификатор программного решения")]
        public int APFInfoID { get; set; }
        
        /// <summary>Комментарий</summary>
        //[DataMember(Order = 3)]
        //[Annotation("Комментарий")]
        public string Comment { get; set; }
    }

    [DataContract]
    [Annotation("План-график")]
    public class Schedule : IEntity
    {
        public string EntityName { get { return ""; } }
        /// <summary>Идентификатор в УВиРИ</summary>
        [DataMember(Order = 0)]
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }
        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }
        [DataMember(Order = 1)]
        [Annotation("Ссылка на мероприятие")]
        public string Plans_Activity_Ref { get; set; }
        [DataMember(Order = 2)]
        [Annotation("Ссылка на работу")]
        public string Plans_Work_Ref { get; set; }
        [DataMember(Order = 3)]
        [Annotation("Ссылка на спецификацию")]
        public string Plans_Spec_Ref { get; set; }
        [DataMember(Order = 4)]
        [Annotation("Плановый срок исполнения  контракта")]
        public DateTime? Plans_Schedule_Exec_Time { get; set; }
        [DataMember(Order = 5)]
        [Annotation("Ориентировочная начальная стоимость контракта, тыс. руб.")]
        public double Plans_Schedule_Cost { get; set; }
        [DataMember(Order = 6)]
        [Annotation("Наименование предмета государственного контракта")]
        public string Plans_Schedule_Subject_Name { get; set; }
        [DataMember(Order = 7)]
        [Annotation("ОКДП введенное пользователем (менее приоритетное по отношению к OKDP)")]
        public string Plans_Schedule_OKDP_Value { get; set; }
    }

    [DataContract]
    [Annotation("Система проектного управления мероприятиями по информатизации")]
    public class ProjectManagementSystem : IEntity
    {
        public string EntityName { get { return ""; } }
        /// <summary>Идентификатор в УВиРИ</summary>
        [DataMember(Order = 0)]
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }
        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }
        [DataMember(Order = 1)]
        [Annotation("Порядковый номер")]
        public int Num { get; set; }
        [DataMember(Order = 2)]
        [Annotation("Номер ведомственного акта")]
        public string ActNum { get; set; }
        [DataMember(Order = 3)]
        [Annotation("Наименование ведомственного акта")]
        public string ActName { get; set; }
        [DataMember(Order = 4)]
        [Annotation("Дата акта/ГОСТ")]
        public DateTime? ActDate { get; set; }
        [DataMember(Order = 5)]
        [Annotation("Наименование ИС")]
        public string ISName { get; set; }
        [DataMember(Order = 6)]
        [Annotation("Перечень процедур этапов управления проектами и их результатов, для автоматизации которых используется ИС")]
        public string Procedures { get; set; }
    }
  
 
    [DataContract]
    [Annotation("Сведения о структуре органа управления мероприятиями по информатизации государственного органа")]
    public class GovStruct : IEntity
    {
        public string EntityName { get { return ""; } }
        [DataMember(Order = 0)]
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }
        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }
        [DataMember(Order = 1)]
        [Annotation("Уровни управления")]
        public int Gov_Struct_Lvl_ID { get; set; }
        [DataMember(Order = 2)]
        [Annotation("Коллегиальный элемент органа управления")]
        public string Gov_Struct_Element { get; set; }
        [DataMember(Order = 3)]
        [Annotation("Периодичность совещаний коллегиального элемента управления")]
        public string Gov_Struct_Period { get; set; }
        [DataMember(Order = 4)]
        [Annotation("Комментарии")]
        public string Gov_Struct_Comment { get; set; }
        [DataMember(Order = 5)]
        [Annotation("Фамилия руководителя")]
        public string Gov_Struct_Manager_Last_Name { get; set; }
        [DataMember(Order = 6)]
        [Annotation("Имя руководителя")]
        public string Gov_Struct_Manager_First_Name { get; set; }
        [DataMember(Order = 7)]
        [Annotation("Отчество руководителя")]
        public string Gov_Struct_Manager_Surname { get; set; }
        [DataMember(Order = 8)]
        [Annotation("Должность руководителя")]
        public string Gov_Struct_Manager_Post { get; set; }

        [DataMember(Order = 9)]
        [Annotation("Список ролей управления")]
        public GovStructRole[] GovStructRoleList { get; set; }
    }

    [DataContract]
    [Annotation("Роль управления")]
    public class GovStructRole : IEntity
    {
        public string EntityName { get { return ""; } }
        [DataMember(Order = 0)]
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }
        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }
        [DataMember(Order = 1)]
        [Annotation("Название")]
        public string Gov_Struct_Role_Name { get; set; }
        [DataMember(Order = 2)]
        [Annotation("Управляющая роль")]
        public string Gov_Struct_Role_Management { get; set; }
        [DataMember(Order = 3)]
        [Annotation("Функция")]
        public string Gov_Struct_Role_Function { get; set; }
    }

    /// <summary>Должностное лицо</summary>
    [DataContract]
    [Annotation("Должностное лицо")]
    public class Official : IEntity
    {
        public string EntityName { get { return ""; } }
        /// <summary>Идентификатор в УВиРИ</summary>
        [DataMember(Order = 0)]
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }
        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }
        /// <summary>Фамилия</summary>
        [DataMember(Order = 1)]
        [Annotation("Фамилия")]
        public string LastName { get; set; }
        /// <summary>Имя</summary>
        [DataMember(Order = 2)]
        [Annotation("Имя")]
        public string FirstName { get; set; }
        /// <summary>Отчество</summary>
        [DataMember(Order = 3)]
        [Annotation("Отчество")]
        public string SurName { get; set; }
        /// <summary>Должность</summary>
        [DataMember(Order = 4)]
        [Annotation("Должность")]
        public string Post { get; set; }
        /// <summary>Квалификация</summary>
        [DataMember(Order = 5)]
        [Annotation("Квалификация")]
        public string Qualification { get; set; }
        /// <summary>Проектный опыт</summary>
        [DataMember(Order = 6)]
        [Annotation("Проектный опыт")]
        public string Experience { get; set; }
        /// <summary>Ссылка на роль управления</summary>
        [DataMember(Order = 7)]
        [Annotation("Ссылка на роль управления")]
        public string RoleRef { get; set; }

    }
 
    /// <summary>Отчётные формы по проектной деятельности</summary>
    [DataContract]
    [Annotation("Отчётные формы по проектной деятельности")]
    public class ReportForm : IEntity
    {
        public string EntityName { get { return ""; } }
        /// <summary>Идентификатор в УВиРИ</summary>
        [DataMember(Order = 0)]
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }
        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }
        /// <summary>Наименование</summary>
        [DataMember(Order = 1)]
        [Annotation("Наименование")]
        public string Name { get; set; }
        /// <summary>Описание</summary>
        [DataMember(Order = 2)]
        [Annotation("Описание")]
        public string Description { get; set; }
        /// <summary>Периодичность формирования</summary>
        [DataMember(Order = 3)]
        [Annotation("Периодичность формирования")]
        public string Period { get; set; }
        /// <summary>Ссылка на роль ответственного за согласование</summary>
        [DataMember(Order = 4)]
        [Annotation("Ссылка на роль ответственного за согласование")]
        public string CoordinationRoleRef { get; set; }
        /// <summary>Ссылка на роль ответственного за подготовку</summary>
        [DataMember(Order = 5)]
        [Annotation("Ссылка на роль ответственного за подготовку")]
        public string PreparationRoleRef { get; set; }
        /// <summary>Ссылка на роль ответственного за утверждение</summary>
        [DataMember(Order = 6)]
        [Annotation("Ссылка на роль ответственного за утверждение")]
        public string StatementRoleRef { get; set; }
    }

    /// <summary>Бумажная форма</summary>
    [DataContract]
    [Annotation("Бумажная форма")]
    public class PaperObject : IEntity
    {
        public string EntityName { get { return ""; } }
        /// <summary>Идентификатор в УВиРИ</summary>
        [DataMember(Order = 0)]
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }
        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }
        /// <summary>Номер исходящего (огв)</summary>
        [DataMember(Order = 1)]
        [Annotation("Номер исходящего (огв)")]
        public string NumberOutgoing { get; set; }
        /// <summary>Дата исходящего (огв)</summary>
        [DataMember(Order = 2)]
        [Annotation("Дата исходящего (огв)")]
        public DateTime? DateOutgoing { get; set; }
        /// <summary>Номер входящего МКС (огв)</summary>
        [DataMember(Order = 3)]
        [Annotation("Номер входящего МКС (огв)")]
        public string NumberInboxMKS { get; set; }
        /// <summary>Дата входящего МКС (огв)</summary>
        [DataMember(Order = 4)]
        [Annotation("Дата входящего МКС (огв)")]
        public DateTime? DateInboxMKS { get; set; }
        /// <summary>Номер исходящего (мкс)</summary>
        [DataMember(Order = 5)]
        [Annotation("Номер исходящего (мкс)")]
        public virtual string NumberOutgoingMKS { get; set; }
        /// <summary>Дата исходящего (мкс)</summary>
        [DataMember(Order = 6)]
        [Annotation("Дата исходящего (мкс)")]
        public virtual DateTime? DateOutgoingMKS { get; set; }
        /// <summary>Дата начала экспертной оценки (факт.)</summary>
        [DataMember(Order = 7)]
        [Annotation("Дата начала экспертной оценки (факт.)")]
        public virtual DateTime? ExpStartDateF { get; set; }
        /// <summary>Дата окончания экспертной оценки (факт.)</summary>
        [DataMember(Order = 8)]
        [Annotation("Дата окончания экспертной оценки (факт.)")]
        public virtual DateTime? ExpEndDateF { get; set; }
        
        /// <summary>Дата отправки плана на доработку</summary>
        [DataMember(Order = 9)]
        [Annotation("Дата отправки плана на доработку")]
        public virtual DateTime? ExpReturnDate { get; set; }
        
        /// <summary>Комментарий</summary>
        [DataMember(Order = 10)]
        [Annotation("Комментарий")]
        public string Comment { get; set; }
        
        /// <summary>Ид статуса</summary>
        [DataMember(Order = 11)]
        [Annotation("Ид статуса")]
        public int StatusId { get; set; }

        /// <summary>Тип бумаги</summary>
        [DataMember(Order = 12)]
        [Annotation("Тип бумаги")]
        public int PaperTypeId { get; set; }

        ///// <summary>Документ привязаный к бумажной форме (огв)</summary>
        //[DataMember(Order = 0)]
        //[Annotation("   ")]
        //public int DocumentId { get; set; }

        ///// <summary>Документ привязаный к бумажной форме (мкс)</summary>
        //[DataMember(Order = 0)]
        //[Annotation("   ")]
        //public int DocumentMKSId { get; set; }

        /// <summary>Документы бумажной формы</summary>
        public Document[] Documents { get; set; }
    }
 
    /// <summary>ЦОД</summary>
    [DataContract]
    [Annotation("ЦОД")]
    public class Zod : IEntity
    {
        public string EntityName { get { return ""; } }

        /// <summary>Идентификатор в УВиРИ</summary>
        [DataMember(Order = 0)]
        [Annotation("Идентификатор в УВиРИ")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        [DataMember(Order = 1)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Ид государственного органа, совместно использующего ИС или компоненты ИКТ</summary> 
        [DataMember(Order = 2)]
        [Annotation("Ид государственного органа, совместно использующего ИС или компоненты ИКТ")]
        public int OGVID { get; set; }

        /// <summary>Ид уполномоченного государственного органа</summary> 
        [DataMember(Order = 3)]
        [Annotation("Ид уполномоченного государственного органа")]
        public int OGVAuthorizedID { get; set; }

        /// <summary>Ид номера реестра ФГИС</summary>
        [DataMember(Order = 4)]
        [Annotation("Ид номера реестра ФГИС")]
        public int FgisInfoID { get; set; }

        //public string FgisInfo { get; set; }
        /// <summary>Наименование ИС/ИКТ</summary>
        [DataMember(Order = 5)]
        [Annotation("Наименование ИС/ИКТ")]
        public string IKTComponent { get; set; }
    }

 */