﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
    public class SupportMap : ClassMapBase<Support>
    {
        public SupportMap()
            : base("PRS_8_8")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_8_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_8_8_CA_1 = ? 
//                          ,@PRS_8_8_TP_1 = ? 
//                          ,@PRS_8_8_CA_2 = ? 
//                          ,@PRS_8_8_TP_2 = ? 
//                          ,@PRS_8_8_CA_3 = ? 
//                          ,@PRS_8_8_TP_3 = ? 
//                          ,@PRS_8_8_CA_4 = ? 
//                          ,@PRS_8_8_TP_4 = ? 
//                          ,@Plans_ID = ? 
//						  ,@PRS_8_8_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_8_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_8_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_8_Delete @PRS_8_8_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Development_CA, "PRS_8_8_CA_1");
			Map(x => x.Development_TP, "PRS_8_8_TP_1");
			Map(x => x.TechSupport_CA, "PRS_8_8_CA_2");
			Map(x => x.TechSupport_TP, "PRS_8_8_TP_2");
			Map(x => x.SoftSupport_CA, "PRS_8_8_CA_3");
			Map(x => x.SoftSupport_TP, "PRS_8_8_TP_3");
			Map(x => x.LanSupport_CA, "PRS_8_8_CA_4");
			Map(x => x.LanSupport_TP, "PRS_8_8_TP_4");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
        }
    }
}