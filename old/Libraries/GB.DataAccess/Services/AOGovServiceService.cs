﻿using AutoMapper;
using GB.Data.AccountingObjects;
using GB.Data.Frgu;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class AOGovServiceService : ServiceBase<AOGovService>, IService<AOGovService, AOGovServiceArgs>
    {
        public AOGovService Create(AOGovServiceArgs args)
        {
            var entity = new AOGovService();
            var result = SaveOrUpdate(entity, args);

            new AOGovServiceNPAService().SetAOGovServiceForAOGovServicesNPA(
                args.AOGovServicesNPAIds, result);

            return result;
        }

        public AOGovService Update(AOGovServiceArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private AOGovService SaveOrUpdate(AOGovService entity, AOGovServiceArgs args)
        {
            Mapper.Map(args, entity);

            if (args.GovServiceId.HasValue)
            {
                var govService =
                    new Repository.Repository<GovService>().Get(
                        q => q.Fetch(x => x.Documents).Eager.Where(x => x.Id == args.GovServiceId.Value));

                entity.GovService = govService;
                SaveOrUpdate(entity);
                new AOGovServiceNPAService().CopyGovServiceNPAInGovService(govService.Documents, entity.Id);
            }
            else
            {
                SaveOrUpdate(entity);
            }

            return entity;
        }

        public void Delete(AOGovServiceArgs args)
        {
            Delete(args.Id);
        }
    }
}
