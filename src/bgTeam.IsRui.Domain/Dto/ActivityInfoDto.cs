﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Domain.Entities.Workflow;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityInfoDto
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int ResponsibleDepartmentId { get; set; }

        public string ResponsibleDepartmentName { get; set; }

        //public int? ResponsibleInformationId { get; set; }

        //public string ResponsibleInformationName { get; set; }

        public int? AoLinkId { get; set; }

        public string AoLinkName { get; set; }

        public int? SigningId { get; set; }

        public string SigningName { get; set; }

        public int? TypeId { get; set; }

        public string TypeName { get; set; }

        public int? StatusId { get; set; }

        public int Year { get; set; }

        public string StatusName { get; set; }

        public int WorkflowId { get; set; }

        public WorkflowUserConclusionDto Conclusion { get; set; }

        public IEnumerable<ActivityDocumentDto> Documents { get; set; }

        public ActivityCostDto Cost { get; set; }

        public IEnumerable<ActivitiesServiceDto> Services { get; set; }

        public IEnumerable<ActivityIndicatorDto> Indicator { get; set; }

        public IEnumerable<ActivitysMarksDto> Marks { get; set; }

        public IEnumerable<ActivityWorksGetDto> Works { get; set; }

        public IEnumerable<ActivityGoodsGetDto> Goods { get; set; }

        public IEnumerable<StatusDto> StatusGetNext { get; set; }

        public IEnumerable<ActivityContractDto> Contracts { get; set; }

        public IEnumerable<ActivityPurchaseDto> Purchases { get; set; }

        public IEnumerable<ActivityActDto> Acts { get; set; }

        public IEnumerable<InformationInteractionDto> InformationInteractions { get; set; }
    }
}
