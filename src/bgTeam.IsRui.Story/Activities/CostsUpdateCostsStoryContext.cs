﻿namespace bgTeam.IsRui.Story.Activities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

     public class CostsUpdateCostsStoryContext
    {
        public int? ActivityId { get; set; }

        /*public decimal? VolY0 { get; set; }

        public decimal? VolY1 { get; set; }

        public decimal? VolY2 { get; set; }*/

        public int BudgetItemId { get; set; }

        public decimal? PlanQuarter1 { get; set; }

        public decimal? PlanQuarter2 { get; set; }

        public decimal? PlanQuarter3 { get; set; }

        public decimal? PlanQuarter4 { get; set; }

        public decimal? ForecastQuarter1 { get; set; }

        public decimal? ForecastQuarter2 { get; set; }

        public decimal? ForecastQuarter3 { get; set; }

        public decimal? ForecastQuarter4 { get; set; }
    }
}
