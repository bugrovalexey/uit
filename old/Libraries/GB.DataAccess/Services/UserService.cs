﻿using System.Collections.Generic;
using System.Linq;
using GB.Data;
using GB.Data.Common;
using GB.DataAccess.Repository;
using GB.DataAccess.Services.Args;
using GB.Extentions;

namespace GB.DataAccess.Services
{
    public class UserService : ServiceBase<User>, IService<User, UserArgs>
    {
        public User Create(UserArgs args)
        {
            return SaveOrUpdate(new User(), args);
        }

        public User Update(UserArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        public void Delete(UserArgs args)
        {
            base.Delete(args.Id);
        }

        public User Update(UserCardArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            entity.FillUser(args);

            base.SaveOrUpdate(entity);
            return entity;
        }

        private User SaveOrUpdate(User entity, UserArgs args)
        {
            entity.FillUser(args);

            base.SaveOrUpdate(entity);

            SaveRoles(entity, args.RoleIds);

            return entity;
        }

        private void SaveRoles(User user, IEnumerable<int> newRolesIds)
        {
            newRolesIds = newRolesIds ?? new List<int>();

            if (user.Id != EntityBase.UnsavedId)
            {
                //InternalStaticMethods.Get(user.Id) - непонятно зачем так было, заменил на просто user
                user.Roles.Return(roles => roles.Select(r => r.Id)).Do(roles =>
                {
                    var deletedRoles = roles.Except(newRolesIds).ToList();
                    RepDeleteRoles(user, deletedRoles);
                });
            }
            else
            {
                SaveOrUpdate(user);
            }

            RepSaveRoles(user, newRolesIds);
            SaveOrUpdate(user);
            NHibernateStaticMethods.Refresh(user);
        }

        private void RepDeleteRoles(User user, List<int> rolesIds)
        {
            var deleteObj = new Repository<UserToRole>().GetAllEx(q => q
                .Where(x => x.User.Id == user.Id)
                .WhereRestrictionOn(x => x.Role.Id)
                .IsIn(rolesIds));

            if (deleteObj.Any())
            {
                NHibernateStaticMethods.DeleteList(deleteObj);
            }
        }

        private void RepSaveRoles(User user, IEnumerable<int> rolesIds)
        {
            foreach (var roleId in rolesIds)
            {
                var userToRole = new Repository<UserToRole>().Get(q => q.Where(utr => utr.User.Id == user.Id && utr.Role.Id == roleId));
                if (userToRole == null)
                {
                    userToRole = new UserToRole();
                    userToRole.User = user;
                    userToRole.Role = new Repository<Role>().Get(roleId);
                    NHibernateStaticMethods.Insert(userToRole);
                }
            }
        }
    }
}