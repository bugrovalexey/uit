﻿namespace bgTeam.IsRui.Domain._Mapping.Claims
{
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Domain.Entities.Workflow;

    internal class ClaimMap : ClassMapHiLo<Claim>
    {
        public ClaimMap()
            : base("Claim")
        {
        }

        protected override void InitMap()
        {
            References<AccountingObject>(x => x.AccountingObject, "Claim_OU_ID").Nullable();
            //References<Status>(x => x.Status, "Claim_Status_ID").Nullable();
            References<PlansActivity>(x => x.PlansActivity, "Claim_Plans_Activity_ID").Not.Nullable();
            References<Entities.Common.Workflow>(x => x.Workflow, "WorkflowId");
            References<ClaimConclusion>(x => x.Conclusion, "Claim_Conclusion_ID");
            References<User>(x => x.InitiatorUser, "Claim_InitiatorUser_ID");

            Map(x => x.Number, "Claim_Number").Not.Nullable();
            Map(x => x.SubDivisionName, "Claim_SubDivisionName").Nullable();
            Map(x => x.Cost, "Claim_Cost").Nullable();
            Map(x => x.CreateDate, "CreateDate").Not.Nullable();
            Map(x => x.Justification, "Claim_Justification").Nullable();
            Map(x => x.Content, "Claim_Content").Nullable();
            Map(x => x.TimeStamp, "TimeStamp").Not.Nullable();
            Map(x => x.ImplementationCost, "Claim_ImplementationCost").Nullable();
            Map(x => x.Name, "Claim_Name").Nullable();

            HasMany(x => x.Marks).KeyColumns.Add("Claim_ID").Cascade.Delete().Inverse();
            HasMany(x => x.Indicators).KeyColumns.Add("Claim_ID").Cascade.Delete().Inverse();
            //HasMany(x => x.Documents).KeyColumns.Add("ClaimDocument_Claim_ID").Cascade.Delete().Inverse();
            //HasMany(x => x.Acts).KeyColumns.Add("ClaimAct_Claim_ID").Cascade.Delete().Inverse();
            HasMany(x => x.ScopeOfWork).KeyColumns.Add("ClaimScopeOfWork_Claim_ID").Cascade.Delete().Inverse();
        }
    }
}
