﻿
namespace GB.DataAccess.Command.AccountingObjects.List
{
    public class AddInFavoritesContext : ICommandContext
    {
        /// <summary>
        /// Id объекта учета
        /// </summary>
        public int AccountingObjectId { get; set; }
    }
}
