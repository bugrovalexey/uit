﻿namespace bgTeam.IsRui.MKRF.Logic.MasterEdit
{
    class MasterItemEnumDescription
    {
        internal static string GetTitleDescription(MasterItemEnum item)
        {
            switch (item)
            {
                case MasterItemEnum.None:
                    break;
                case MasterItemEnum.CardForRead:
                    break;
                case MasterItemEnum.DocumentBase:
                    break;
                case MasterItemEnum.GoalsAndPurpose:
                    break;
                case MasterItemEnum.ResponsibleService:
                    break;
                case MasterItemEnum.SpecialActivity:
                    break;
                case MasterItemEnum.ActivityCreate:
                    break;
                case MasterItemEnum.TechnicalSupport:
                    break;
                case MasterItemEnum.Software:
                    break;
                case MasterItemEnum.WorkAndService:
                    break;
                case MasterItemEnum.CharacteristicsCreate:
                    break;
                case MasterItemEnum.InformationAboutSecurity:
                    break;
                case MasterItemEnum.InformationInteractionCreate:
                    break;
                case MasterItemEnum.ResponsiblePurchase:
                    break;
                case MasterItemEnum.GovernmentContract:
                    break;
                case MasterItemEnum.ActsOfAcceptance:
                    break;
                case MasterItemEnum.FactualLocation:
                    break;
                case MasterItemEnum.Commissioning:
                    break;
                case MasterItemEnum.AdoptionBudget:
                    break;
                case MasterItemEnum.ResponsibleExploitation:
                    break;
                case MasterItemEnum.Decommissioning:
                    break;
            }
            
            return "Описание не найдено";
        }

        internal static string GetStepDescription(MasterItemEnum item)
        {
            switch (item)
            {
                //case MasterItemEnum.CommonDataCreate:
                //    break;
                case MasterItemEnum.DocumentBase:
                    return "Решение о создании (закупке) объекта учета";
                case MasterItemEnum.GoalsAndPurpose:
                    return "Цели, назначение и область применения объекта учета";
                case MasterItemEnum.ResponsibleService:
                    return "Ответственные за ведение объекта учета";
                case MasterItemEnum.SpecialActivity:
                    return "Специальная деятельность (функции)";
                case MasterItemEnum.ActivityCreate:
                    return "Мероприятие по информатизации";
                case MasterItemEnum.ActivityList:
                    return "Cведения о мероприятиях";
                case MasterItemEnum.TechnicalSupport:
                    return "Техническое обеспечение";
                case MasterItemEnum.Software:
                    return "Программное обеспечение";
                case MasterItemEnum.WorkAndService:
                    return "Работы (услуги)";
                case MasterItemEnum.CharacteristicsCreate:
                    return "Характеристики объекта учета";
                case MasterItemEnum.InformationAboutSecurity:
                    return "Cведения о защите информации";
                case MasterItemEnum.InformationInteractionCreate:
                    return "Информационное взаимодействие";
                case MasterItemEnum.ResponsiblePurchase:
                    return "Ответственные за организацию закупок";
                case MasterItemEnum.GovernmentContract:
                    return "Государственные контракты заключенные в рамках объекта учета";
                case MasterItemEnum.ActsOfAcceptance:
                    return "Сведения о результатах исполнения государственных контрактов";
                case MasterItemEnum.FactualLocation:
                    return "Фактическое месторасположение объекта учета";
                case MasterItemEnum.Commissioning:
                    return "Cведения о вводе в эксплуатацию объекта учета";
                case MasterItemEnum.AdoptionBudget:
                    return "Cведения о принятии объекта учета к бюджетному учету";
                case MasterItemEnum.ResponsibleExploitation:
                    return "Ответственные за эксплуатацию объекта учета";
                case MasterItemEnum.Decommissioning:
                    return "Cведения о выводе из эксплуатации объекта учета";
                case MasterItemEnum.ReasonsDecommissioning:
                    return "Причины вывода из эксплуатации объекта учета";

                case MasterItemEnum.KindMasterEdit:
                    return "Классификационная категория";
                case MasterItemEnum.Purchase:
                    return "Закупки";
            }

            return null;
        }
    }
}
