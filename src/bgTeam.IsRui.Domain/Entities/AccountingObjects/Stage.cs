﻿namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    public class Stage : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }
    }
}
