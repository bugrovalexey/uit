﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    internal class ProductGroupMap : ClassMapHiLo<ProductGroup>
    {
        public ProductGroupMap()
            : base("Product_Group")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Product_Group_Name").Nullable();
        }
    }
}