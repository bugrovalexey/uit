﻿namespace bgTeam.IsRui.Story.Common
{
    using bgTeam.IsRui.Domain;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;

    public class FileAddStoryContext
    {
        public IEntity Entity { get; set; }

        public DocKindEnum Kind { get; set; }

        public DocTypeEnum Type { get; set; }

        public Guid? TempGuid { get; set; }

        public string Name { get; set; }

        public string Extension { get; set; }

        public int? Size { get; set; }

        public string Url { get; set; }

        public string MimeType { get; set; }
    }
}