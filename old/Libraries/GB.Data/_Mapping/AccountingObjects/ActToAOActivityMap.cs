﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.AccountingObjects;

namespace GB._Mapping.AccountingObjects
{
    internal class ActToAOActivityMap : ClassMapHiLo<ActToAOActivity>
    {
        public ActToAOActivityMap()
            : base("Act_To_OU_Activity")
        {
        }

        protected override void InitMap()
        {
            References(x => x.ActOfAcceptance, "Act_ID");
            References(x => x.AoActivity, "OU_Plans_Activity_ID").Not.Nullable();
        }
    }
}
