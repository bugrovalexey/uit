﻿using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AccessToPageError()
        {
            return View();
        }

        public ActionResult AccessToObjectError()
        {
            return View();
        }

        public ActionResult FileError()
        {
            return View();
        }

        public ActionResult NotFound()
        {
            return HttpNotFound();
        }
    }
}