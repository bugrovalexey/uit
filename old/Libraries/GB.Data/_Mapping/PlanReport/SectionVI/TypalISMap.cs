﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.PlanReport.SectionVI;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVI
{
    public class TypalISMap : ClassMapBase<TypalIS>
	{
		public TypalISMap()
            : base("PRS_6a")
		{
//            Table("w_");

//            Id(x => x.Id, "PRS_6a_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string args = @" @PRS_6a_Num = ? 
//							,@PRS_6a_Is_Exists_IS = ? 
//							,@PRS_6a_User_Cnt = ? 
//							,@PRS_6a_Department = ? 
//							,@PRS_6a_Soft = ? 
//							,@PRS_6a_Executor = ? 
//							,@Plans_ID = ? 
//							,@IKT_Component_ID = ? 
//							,@APF_Info_ID = ? 
//							,@PRS_6a_Year = ? 
//							,@PRS_6_Support_ID = ? 
//							,@PRS_6a_ID = ? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_6a_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_6a_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_6a_Delete
//						  @PRS_6a_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_6a_Num");
			Map(x => x.ISExist, "PRS_6a_Is_Exists_IS");
			Map(x => x.UsersCount, "PRS_6a_User_Cnt");
			Map(x => x.Unit, "PRS_6a_Department");
			Map(x => x.Software, "PRS_6a_Soft");
			Map(x => x.Executor, "PRS_6a_Executor");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
			References<IKTComponent>(x => x.IKTComponent, "IKT_Component_ID");
			References<ApfInfo>(x => x.APFInfo, "APF_Info_ID");
			References<Years>(x => x.Year, "PRS_6a_Year");
			References<TechSupport>(x => x.TechSupport, "PRS_6_Support_ID");
        }
    }
}