﻿using Uviri.MKRF.Entities.Prognosis;
using Uviri.MKRF.Entities.Prognosis.SectionIII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionIII
{
    public class SiteInfoMap : ClassMapBase<SiteInfo>
	{
		public SiteInfoMap()
            : base("MF3_14")
        {
//            Table("w_");

//            Id(x => x.Id, "MF3_14_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_MF3_14_Insert
//                          @MF3_14_Num = ?
//                         ,@MF3_14_1 = ?
//                         ,@MF3_14_2 = ?
//                         ,@MF3_14_3 = ?
//						 ,@MF3_14_4 = ?
//						 ,@MF3_14_5 = ?
//						 ,@MF3_14_6 = ?
//						 ,@MF3_14_7 = ?
//						 ,@MF3_14_8 = ?
//						 ,@Mon_Form_ID = ?
//                         ,@MF3_14_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF3_14_Update
//                          @MF3_14_Num = ?
//                         ,@MF3_14_1 = ?
//                         ,@MF3_14_2 = ?
//                         ,@MF3_14_3 = ?
//						 ,@MF3_14_4 = ?
//						 ,@MF3_14_5 = ?
//						 ,@MF3_14_6 = ?
//						 ,@MF3_14_7 = ?
//						 ,@MF3_14_8 = ?
//						 ,@Mon_Form_ID = ?
//                         ,@MF3_14_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF3_14_Delete
//						  @MF3_14_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "MF3_14_Num");
			Map(x => x.Address, "MF3_14_1");
			Map(x => x.StartYear, "MF3_14_2");
			Map(x => x.AvgDayVisitors, "MF3_14_3");
			Map(x => x.PubSystemName, "MF3_14_4");
			Map(x => x.InfoUpdatePeriod, "MF3_14_5");
			Map(x => x.ActFOIVInfoList, "MF3_14_6");
			Map(x => x.AuthPersMechanism, "MF3_14_7");
			Map(x => x.ServicesList, "MF3_14_8");

			References<Prognos>(x => x.Prognos, "Mon_Form_ID").Not.Nullable();
        }
    }
}