﻿namespace GB.MKRF.Services.Args
{
    /// <summary>
    /// Ответственные
    /// </summary>
    public class AccountingObjectResponsiblesArgs : BaseArgsNew
    {
        /// <summary>
        /// Государственный орган, уполномоченный на создание,
        /// развитие ОУ
        /// </summary>
        public int? GrbsId { get; set; }

        #region Раздел  "Государственный орган или иное юридическое лицо, уполномоченное на эксплуатацию"

        /// <summary>
        /// Наименование организации
        /// </summary>
        public string ExploitationOrgName { get; set; }

        /// <summary>
        /// Структурное подразделение
        /// </summary>
        public string ExploitationOrgDepartment { get; set; }

        /// <summary>
        /// ФИО ответственного
        /// </summary>
        public string ExploitationFioResponsible { get; set; }

        /// <summary>
        /// Должность ответственного
        /// </summary>
        public string ExploitationJobResponsible { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string ExploitationOrgAddress { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string ExploitationOrgPhone { get; set; }

        #endregion Раздел  "Государственный орган или иное юридическое лицо, уполномоченное на эксплуатацию"

        #region Раздел "Сведения об ответственных должностных лицах"

        /// <summary>
        /// Ответственный за координацию мероприятий
        /// по информатизации
        /// </summary>
        public int? ResponsibleCoordId { get; set; }

        /// <summary>
        /// Ответственный за обеспечение
        /// эксплуатации
        /// </summary>
        public int? ResponsibleExploitationId { get; set; }

        /// <summary>
        /// Ответственный за организацию
        /// закупок
        /// </summary>
        public int? ResponsiblePurchaseId { get; set; }

        /// <summary>
        /// Ответственный за размещение сведений
        /// об объекте учета
        /// </summary>
        public int? ResponsibleInformationId { get; set; }

        #endregion Раздел "Сведения об ответственных должностных лицах"

        #region Раздел "Сведения о фактическом месторасположении объекта учета"

        /// <summary>
        /// Наименование организации
        /// </summary>
        public string LocationOrgName { get; set; }

        /// <summary>
        /// ФИО ответственного
        /// </summary>
        public string LocationFioResponsible { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string LocationOrgAddress { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string LocationOrgPhone { get; set; }

        #endregion Раздел "Сведения о фактическом месторасположении объекта учета"
    }
}