﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace bgTeam.IsRui.WebApp.Helpers
{
    public class ReportHelper
    {
        public const string RepType = "RepType";
        public const string RepTypeMVC = "type";

        public static string CreateLink(string name, object rname, params object[] parameters)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<a href='");
            builder.Append(UrlHelper.Resolve("~/Report.aspx"));
            builder.Append("?");
            builder.Append(RepType);
            builder.Append("=");
            builder.Append(rname);

            if (parameters != null)
            {
                int count = 1;
                foreach (object par in parameters)
                {
                    builder.Append("&p");
                    builder.Append(count);
                    builder.Append("=");
                    builder.Append(par);
                    count++;
                }
            }

            builder.Append("' target='_blank'>");
            builder.Append(name);
            builder.Append("</a>");

            return builder.ToString();
        }

        public static System.Web.Mvc.MvcHtmlString CreateLinkMVC(string name, object rname, params object[] parameters)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<a href='");
            builder.Append(UrlHelper.Resolve("~/Report"));
            builder.Append("?");
            builder.Append(RepTypeMVC);
            builder.Append("=");
            builder.Append(rname);

            if (parameters != null)
            {
                int count = 1;
                foreach (object par in parameters)
                {
                    builder.Append("&p");
                    builder.Append(count);
                    builder.Append("=");
                    builder.Append(par);
                    count++;
                }
            }

            builder.Append("' target='_blank'>");
            builder.Append(name);
            builder.Append("</a>");

            return new System.Web.Mvc.MvcHtmlString(builder.ToString());
        }

        public static object[] GetParams(NameValueCollection parameters)
        {
            List<object> list = new List<object>();

            for (int i = 1; i < parameters.Count; i++)
            {
                object obj = parameters[string.Format("p{0}", i)];

                if (obj != null)
                {
                    list.Add(obj);
                }
            }

            return list.ToArray();
        }
    }
}