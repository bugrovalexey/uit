﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="OU.aspx.cs" Inherits="GB.MKRF.Web.Selectors.OU" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">
        function RowClick(s, e) {
            var id = s.GetRowKey(e.visibleIndex);
            s.GetRowValues(e.visibleIndex, 'Id;Name', function (values) {
                var result = id + ';' + values[1];

                window.parent.PopupClose('<% =Request.QueryString["type"] %>', result);
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <dxwgv:ASPxGridView runat="server" ID="Grid" KeyFieldName="Id" Width="100%"
        ClientInstanceName="Grid" AutoGenerateColumns="False"
        OnBeforePerformDataSelect="Grid_BeforePerformDataSelect">
        <Columns>
            <dxwgv:GridViewDataTextColumn Caption="Краткое наименование объекта учета" FieldName="Name" />
            <dxwgv:GridViewDataTextColumn Caption="Тип ОУ" FieldName="Type" />
            <dxwgv:GridViewDataTextColumn Caption="Вид ОУ" FieldName="Kind" />
        </Columns>
        <ClientSideEvents RowClick="RowClick" />
    </dxwgv:ASPxGridView>
</asp:Content>
