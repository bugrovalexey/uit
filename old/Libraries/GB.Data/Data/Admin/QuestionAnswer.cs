﻿using System.ComponentModel;

namespace Uviri.MKRF.Entities.Admin
{
    /// <summary>
    /// Стандартные ответы на вопросы анкеты
    /// </summary>
    public class QuestionAnswer : EntityBase
    {
        protected QuestionAnswer() {}

        public QuestionAnswer(QuestionnaireQuestion question)
        {
            this.QuestionnaireQuestion = question;
        }

        /// <summary>Текст ответа</summary>
        public virtual string Name { get; set; }

        /// <summary>Порядковый номер</summary>
        public virtual int Num { get; set; }

        /// <summary>Тип ответа</summary>
        public virtual AnswerTypeEnum Type { get; set; }

        /// <summary>Вопрос</summary>
        public virtual QuestionnaireQuestion QuestionnaireQuestion { get; set; }
    }

    public enum AnswerTypeEnum
    {
        [Description("Радиокнопка")]
        RadioButton = 0,

        [Description("Чекбокс")]
        Checkbox = 1,

        [Description("Свободный ответ c радиокнопкой")]
        FreeWithRadio = 2,

        [Description("Свободный ответ с чекбоксом")]
        FreeWithCheckbox = 3,

        [Description("Свободный ответ")]
        Free = 4
    }
}
