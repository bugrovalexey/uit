﻿using System;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Selectors
{
    public partial class Services : System.Web.UI.Page
    {
        private object dataSourceInternal;
        protected object dataSource
        {
            get
            {
                if (dataSourceInternal == null)
                {
                    dataSourceInternal = GovServiceRepositary.GetList();
                }
                return dataSourceInternal;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ServicesGrid.DataBind();
            base.OnPreRender(e);
        }

        protected void ServicesGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var source = dataSource;

            ServicesGrid.DataSource = source;
        }

        protected void ServicesGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "$(this).addClass(\"grid_mouseover\")");
            e.Row.Attributes.Add("onmouseout", "$(this).removeClass(\"grid_mouseover\")");
        }

        protected string getIndent(string parentId, string value)
        {
            if (string.IsNullOrEmpty(parentId))
            {
                return value;
            }
            else
            {
                return string.Format("<table style='width:100%'><tr><td style='width:30px'>&nbsp;</td><td>{0}</td></tr></table>", value);
            }

        }
    }
}