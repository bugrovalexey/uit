﻿using FluentNHibernate.Mapping;
using GB.Data;
using GB.Data.Common;

namespace GB._Mapping.Common
{
    internal class AccessToEntityMap : ClassMap<AccessToEntity>
    {
        public AccessToEntityMap()
        {
            Table("Access_To_Entity");

            Id(x => x.Id, "Access_To_Entity_ID");
            //.GeneratedBy.Increment();  - Ошибка IDENTITY_INSERT имеет значение OFF, так как явно пытаемся вставить значение
            //.GeneratedBy.Custom(typeof(IdGenerator)).UnsavedValue((int)EntityBase.UnsavedId);

            InitMap();
        }

        protected void InitMap()
        {
            Map(x => x.AccessMask, "Access_To_Entity_Access").CustomType(typeof(AccessActionEnum)).Not.Nullable();
            Map(x => x.EntityType, "Access_To_Entity_Type").CustomType(typeof(EntityType)).Not.Nullable();
            Map(x => x.Note, "Access_To_Entity_Note");

            References(x => x.Role, "Role_ID").Not.Nullable();
            References(x => x.Status, "Status_ID");
        }
    }
}