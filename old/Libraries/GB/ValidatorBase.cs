﻿using GB.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GB
{
    public abstract class ValidatorBase
    {
        /// <summary>
        /// Проверяет, есть ли у текущего пользователя доступ к странице
        /// </summary>
        public static bool UserAccessToPage(string page)
        {
            UserBase currentUser = UserRepositoryBase.GetCurrentEx();

            return Singleton<GbSiteMapProvider>.Instance.СheckAccessUser(page, currentUser);
        }
    }
}
