﻿namespace bgTeam.IsRui.Common.Extentions
{
    using System;
    using System.Globalization;
    using bgTeam.Extensions;

    public static class DateExtensions
    {
        public static string ToStringOrDefault(this DateTime? date, string defaultResult = null, string format = "dd.MM.yyyy")
        {
            return date.Return(d => d.ToString(format), defaultResult);
        }


        public static string ToDisplay(this DateTime? date)
        {
            return date.Return(d => $"{d.ToString("ddd", CultureInfo.CreateSpecificCulture("ru-RU"))}, {d.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))}", default);
        }
    }
}
