﻿namespace GB.DataAccess.Services.Args
{
    public class WorkAndServiceArgs : BaseArgsNew, IWorkAndGoodsArgs
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Объект учета
        /// </summary>
        public int? AccountingObjectId { get; set; }

        /// <summary>
        /// Классификационная категория
        /// </summary>
        public int? CategoryKindOfSupportId { get; set; }

        /// <summary>
        /// Сведения по арендуемой инфраструктуре и условиям ее использования
        /// </summary>
        public string InformationLeasedInfrastructure { get; set; }

        /// <summary>
        /// ОКВЭД
        /// </summary>
        public int? OkvedId { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public decimal Summa { get; set; }
    }
}