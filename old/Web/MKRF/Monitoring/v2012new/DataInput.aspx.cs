﻿using System;
using System.Collections.Generic;
using System.Data;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;

namespace GB.MKRF.Web.Monitoring.v2012new
{
	public partial class DataInput : System.Web.UI.Page
	{
		private DataSet _data = null;

		int year = DateTime.Now.Year;
        
        private int currentYear = 0;
        protected int CurrentYear
        {
            get
            {
                if (currentYear == 0)
                {

                    var ys = Request.QueryString["Year"];
                    int yi = 0;
                    if (!string.IsNullOrWhiteSpace(ys) && int.TryParse(ys, out yi))
                    {
                        currentYear = yi;
                    }
                    else
                    {
                        currentYear = DateTime.Now.Year;
                    }
                }
                return currentYear;
            }
        }

        //private int currentDepType = 0;
        //protected int CurrentDepType
        //{
        //    get
        //    {
        //        if (currentDepType == 0)
        //        {

        //            var ds = Request.QueryString["DepType"];
        //            int di = 0;
        //            if (!string.IsNullOrWhiteSpace(ds) && int.TryParse(ds, out di))
        //            {
        //                currentDepType = di;
        //            }
        //            else
        //            {
        //                currentDepType = 0;
        //            }
        //        }
        //        return currentDepType;
        //    }
        //}

		private DataSet data 
		{ 
			get
			{
				if(_data == null)
					GetValues();
				return _data;
			} 
		} 

		private Dictionary<string, int> _values = null;

		protected override void OnPreRender(EventArgs e)
		{
            YearsComboBox.DataBind();
            //OGVTypeComboBox.Value = CurrentDepType;

			GetValues();
			InputDataWithPlansGrid.DataBind();
			PlansSendToExpertiseGrid.DataBind();
			PlansNotExistGrid.DataBind();
			//PlansSendToExpertise_addGrid.DataBind();
            PlansAccordGrid.DataBind();

            Chart.Series[0].Points.Clear();
			Chart.Series[0].Points.Add(new SeriesPoint(" ", _values["PlansNotExist"]));
            Chart.Series[0].Name = "По планам информатизации не вносились данные";
            
            Chart.Series[1].Points.Clear();
			Chart.Series[1].Points.Add(new SeriesPoint(" ", _values["InputDataWithPlans"]));
            Chart.Series[1].Name = "Вносятся данные по планам информатизации";

			Chart.Series[2].Points.Clear();
			Chart.Series[2].Points.Add(new SeriesPoint(" ", _values["PlansSendToExpertise"]));
            Chart.Series[2].Name = "Планы, направленные на согласование";

            Chart.Series[3].Points.Clear();
            Chart.Series[3].Points.Add(new SeriesPoint(" ", _values["PlansAccord"]));
            Chart.Series[3].Name = "Согласованные планы информатизации";
		}

		protected void Page_Load(object sender, EventArgs e)
		{
            InputDataWithPlansGrid.Columns["DepartmentName"].Caption = "Наименование департамента";
            //InputDataWithPlansGrid.Columns["DepartmentCode"].Caption = "Код ОГВ";
            //InputDataWithPlansGrid.Columns["DepartmentNameSmall"].Caption = "ОГВ (краткое)";

            PlansSendToExpertiseGrid.Columns["DepartmentName"].Caption = "Наименование департамента";
            //PlansSendToExpertiseGrid.Columns["DepartmentCode"].Caption = "Код ОГВ";
            //PlansSendToExpertiseGrid.Columns["DepartmentNameSmall"].Caption = "ОГВ (краткое)";

            PlansNotExistGrid.Columns["DepartmentName"].Caption = "Наименование департамента";
            //PlansNotExistGrid.Columns["DepartmentCode"].Caption = "Код ОГВ";
            //PlansNotExistGrid.Columns["DepartmentNameSmall"].Caption = "ОГВ (краткое)";

            PlansAccordGrid.Columns["DepartmentName"].Caption = "Наименование департамента";
            //PlansAccordGrid.Columns["DepartmentCode"].Caption = "Код ОГВ";
            //PlansAccordGrid.Columns["DepartmentNameSmall"].Caption = "ОГВ (краткое)";
		}

        protected void YearsComboBox_DataBinding(object sender, System.EventArgs e)
        {
            IList<Years> data = RepositoryBase<Years>.GetAll(x => x.Value >= 1900);
            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = data;
            box.Value = CurrentYear;
        }
        

		private void GetValues()
		{
			_values = new Dictionary<string, int>();
            _data = MonitoringRepository.GetFullDataForDataInputMKRF(CurrentYear, 0 /*CurrentDepType*/, out _values);
		}

		protected void InputDataWithPlansGrid_DataBinding(object sender, EventArgs e)
		{
			InputDataWithPlansGrid.DataSource = data.Tables["InputDataWithPlans"];
		}

		protected void PlansSendToExpertiseGrid_DataBinding(object sender, EventArgs e)
		{
			PlansSendToExpertiseGrid.DataSource = data.Tables["PlansSendToExpertise"];
		}

		protected void PlansNotExistGrid_DataBinding(object sender, EventArgs e)
		{
			PlansNotExistGrid.DataSource = data.Tables["PlansNotExist"];
		}

        protected void PlansAccordGrid_DataBinding(object sender, EventArgs e)
		{
            PlansAccordGrid.DataSource = data.Tables["PlansAccord"];
		}


        //protected void PlansSendToExpertise_addGrid_DataBinding(object sender, EventArgs e)
        //{
        //    DataTable dtBase = data.Tables["PlansSendToExpertise"];

        //    if (dtBase == null)
        //        return;

        //    DataTable tempDT = dtBase.Clone();

        //    double min1 = double.MaxValue;
        //    double min2 = double.MaxValue;
        //    double max1 = double.MinValue;
        //    double max2 = double.MinValue;
        //    double sum = 0;
        //    double sumFB = 0;
        //    double sumRB = 0;
        //    double sumVB = 0;
        //    foreach (DataRow dr in dtBase.Rows)
        //    {
        //        double val1 = Convert.ToDouble(dr["PlanActivity2012Total"]);

        //        if (val1 < min1)
        //            min1 = val1;

        //        if (val1 > max1)
        //            max1 = val1;

        //        double val2 = Convert.ToDouble(dr["AvgPlanActivity2012"]);

        //        if (val2 < min2)
        //            min2 = val2;

        //        if (val2 > max2)
        //            max2 = val2;

        //        sum += val1;

        //        sumFB += Convert.ToDouble(dr["Plans_Expense_Vol_FB"]);
        //        sumRB += Convert.ToDouble(dr["Plans_Expense_Vol_RB"]);
        //        sumVB += Convert.ToDouble(dr["Plans_Expense_Vol_VB"]);
        //    }

        //    if (min1 == double.MaxValue)
        //        min1 = 0;
        //    if (min2 == double.MaxValue)
        //        min2 = 0;
        //    if (max1 == double.MinValue)
        //        max1 = 0;
        //    if (max2 == double.MinValue)
        //        max2 = 0;

        //    DataRow drNew1 = tempDT.NewRow();
        //    drNew1["DepartmentNameCode"] = string.Format("Минимальный планируемый объем расходов на мероприятия по  информатизации одного ОГВ, тыс. руб (данные по {0} ОГВ)", dtBase.Rows.Count);
        //    drNew1["Plans_Activity_Sum"] = min1;
        //    tempDT.Rows.Add(drNew1);

        //    DataRow drNew2 = tempDT.NewRow();
        //    drNew2["DepartmentNameCode"] = string.Format("Максимальный  планируемый объем расходов на мероприятия информатизации одного ОГВ, тыс. руб (данные по {0} ОГВ)", dtBase.Rows.Count);
        //    drNew2["Plans_Activity_Sum"] = max1;
        //    tempDT.Rows.Add(drNew2);

        //    DataRow drNew4 = tempDT.NewRow();
        //    drNew4["DepartmentNameCode"] = string.Format("Максимальный планируемый объем расходов на одно мероприятие по  информатизации, тыс. руб  (данные по {0} ОГВ)", dtBase.Rows.Count);
        //    drNew4["Plans_Activity_Sum"] = max2;
        //    tempDT.Rows.Add(drNew4);
            
        //    DataRow drNew8 = tempDT.NewRow();
        //    drNew8["DepartmentNameCode"] = string.Format("Всего планируется расходов на мероприятия информатизации ОГВ, тыс. руб");
        //    drNew8["Plans_Activity_Sum"] = sum;
        //    tempDT.Rows.Add(drNew8);

        //    PlansSendToExpertise_addGrid.DataSource = tempDT;
        //}

		protected void InputDataWithPlansToExcelButton_Click(object sender, EventArgs e)
		{
			ToExcelExporter.GridViewID = "InputDataWithPlansGrid";
			DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions(); options.SheetName = "Вносятся данные";
			options.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value; ToExcelExporter.WriteXlsToResponse("Vnosiatsa_dannie", options);
		}

		protected void PlansSendToExpertiseToExcelButton_Click(object sender, EventArgs e)
		{
			ToExcelExporter.GridViewID = "PlansSendToExpertiseGrid";
			DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions(); options.SheetName = "Отправленно на экспертизу";
			options.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value; ToExcelExporter.WriteXlsToResponse("Otpravleno_na_expertizu", options);
		}

		protected void PlansNotExistToExcelButton_Click(object sender, EventArgs e)
		{
			ToExcelExporter.GridViewID = "PlansNotExistGrid";
			DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions(); options.SheetName = "Не вносились данные";
			options.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value; ToExcelExporter.WriteXlsToResponse("Ne_vnosilis_dannie", options);
		}

        protected void PlansAccordToExcelButton_Click(object sender, EventArgs e)
        {
            ToExcelExporter.GridViewID = "PlansAccordGrid";
            DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions(); options.SheetName = "Отправленно на экспертизу";
            options.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value; ToExcelExporter.WriteXlsToResponse("Otpravleno_na_expertizu", options);
        }
	}
}