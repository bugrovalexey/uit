﻿using Uviri.MKRF.Entities.PlanReport.SectionVII;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVII
{
    public class PRS7Map : ClassMapBase<PRS7>
    {
        public PRS7Map()
            : base("PRS_7")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_7_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@PRS_7_URL = ?
//                          ,@PRS_7_Year = ?
//                          ,@PRS_7_User_Cnt = ?
//                          ,@PRS_7_Sys_Publ = ?
//                          ,@PRS_7_Period = ?
//                          ,@PRS_7_Sved = ?
//                          ,@PRS_7_Auth = ?
//                          ,@PRS_7_Uslugi = ?
//                          ,@Plans_ID = ?
//						  ,@PRS_7_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_7_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_7_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_7_Delete @PRS_7_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Url, "PRS_7_URL");
            Map(x => x.Year, "PRS_7_Year");
            Map(x => x.VisitCount, "PRS_7_User_Cnt");
            Map(x => x.Publication, "PRS_7_Sys_Publ");
            Map(x => x.PeriodUpdate, "PRS_7_Period");
            Map(x => x.Svedenia, "PRS_7_Sved");
            Map(x => x.Authorization, "PRS_7_Auth");
            Map(x => x.Service, "PRS_7_Uslugi");

            References<Plan>(x => x.Plan, "Plans_ID");
        }
    }
}
