﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxEditors;
using System.Web.UI;
using System.Web;
using System.Web.Security;
using System.ComponentModel;
using DevExpress.Web.ASPxCallback;
using GB.ESIA.Web;

namespace GB.Controls
{
    public class LoginControl : CompositeControl, IPostBackEventHandler
    {
        /// <summary>
        /// Основной контейнер для всех компонентов
        /// </summary>
        protected HtmlGenericControl container;
        /// <summary>
        /// Контрол для добавления стиля
        /// </summary>
        protected HtmlTable table;
        /// <summary>
        /// Поле ввода имени пользователя
        /// </summary>
        protected TextBox LoginTextBox;
        /// <summary>
        /// Поле ввода пароля
        /// </summary>
        protected TextBox PasswordTextBox;
        
        /// <summary>
        /// Название кнопки
        /// </summary>
        public string ButtonText { get; set; }
        /// <summary>
        /// Применяемый стиль
        /// </summary>
        public string ButtonCssClass { get; set; }
        /// <summary>
        /// Идентификатор панели для вывода ошибок
        /// </summary>
        
        public string ErrorPanelID { get; set; }
        /// <summary>
        /// Шаблон для коллбэка
        /// </summary>
        const string _pattern = "LoginUserCallback:";


        public LoginControl()
        {
            container = new HtmlGenericControl("div");
            table = new HtmlTable();
        }


        protected override void CreateChildControls()
        {
            #region Создание - задание свойств

            container.Attributes.Add("class", "form_auth");
            container.ID = "test";
            
            table.Attributes.Add("class", "form_auth_table");

            HtmlTableRow rowLoginText = new HtmlTableRow();
            HtmlTableCell cell = new HtmlTableCell();
            cell.Attributes.Add("class", "form_auth_text");
            cell.Attributes.Add("colspan", "2");
            cell.Controls.Add(new LiteralControl("Логин"));
            rowLoginText.Cells.Add(cell);

            HtmlTableRow rowLoginControl = new HtmlTableRow();
            HtmlTableCell cell2 = new HtmlTableCell();
            cell2.Attributes.Clear();
            cell2.Attributes.Add("class", "form_auth_input");
            cell2.Attributes.Add("colspan", "2");

            /* input login */
            LoginTextBox = new TextBox();
            LoginTextBox.TabIndex = 1;
            LoginTextBox.ID = "LoginTextBox";
            LoginTextBox.MaxLength = 100;
            //LoginTextBox.Attributes.Add("placeholder", "Логин");
            
            cell2.Controls.Add(LoginTextBox);
            rowLoginControl.Cells.Add(cell2);

            HtmlTableRow rowErrorLogin = new HtmlTableRow();
            cell = new HtmlTableCell();
            cell.Attributes.Clear();
            cell.Attributes.Add("class", "form_auth_desc");
            cell.Attributes.Add("colspan", "2");
            //cell.Controls.Add(new LiteralControl("Не может быть пустым."));
            rowErrorLogin.Cells.Add(cell);

            HtmlTableRow rowPassText = new HtmlTableRow();
            cell = new HtmlTableCell();
            cell.Attributes.Clear();
            cell.Attributes.Add("class", "form_auth_text");
            cell.Attributes.Add("colspan", "2");
            cell.Controls.Add(new LiteralControl("Пароль"));
            rowPassText.Cells.Add(cell);

            HtmlTableRow rowPassControl = new HtmlTableRow();
            cell = new HtmlTableCell();
            cell.Attributes.Clear();
            cell.Attributes.Add("class", "form_auth_input");
            cell.Attributes.Add("colspan", "2");

            /* input password */
            PasswordTextBox = new TextBox();
            PasswordTextBox.ID = "PasswordTextBox";
            PasswordTextBox.TextMode = TextBoxMode.Password;
            PasswordTextBox.TabIndex = 2;
            PasswordTextBox.MaxLength = 100;
            //PasswordTextBox.Attributes.Add("placeholder", "Пароль");
            PasswordTextBox.Attributes.Add("autocomplete", "off");
            
            cell.Controls.Add(PasswordTextBox);
            rowPassControl.Cells.Add(cell);

            HtmlTableRow rowErrorPass = new HtmlTableRow();
            cell = new HtmlTableCell();
            cell.Attributes.Clear();
            cell.Attributes.Add("class", "form_auth_desc");
            cell.Attributes.Add("colspan", "2");
            //cell.Controls.Add(new LiteralControl("Не может быть пустым."));
            rowErrorPass.Cells.Add(cell);

            HtmlTableRow rowButton = new HtmlTableRow();
            cell = new HtmlTableCell();
            cell.Attributes.Clear();
            cell.Attributes.Add("width", "1%");
            //cell.Attributes.Add("align", "center");
            //cell.Attributes.Add("style", "padding-top: 10px");

            //Ok_Button = new ASPxButton();
            //Ok_Button.AutoPostBack = false;
            //Ok_Button.ID = "Ok_Button";
            //Ok_Button.Text = ButtonText;
            //Ok_Button.Width = ButtonWidth;
            //Ok_Button.Height = ButtonHeight;
            //Ok_Button.Native = true;
            //Ok_Button.CssClass = ButtonCssClass;
            //Ok_Button.Attributes.Add("onclick", "LoginSys();return false;");
            //cell.Controls.Add(Ok_Button);

            cell.InnerHtml = string.Concat(
                "<input id=\"LoginForm_Ok_Button\" class=\"", ButtonCssClass, "\" onclick=\"LoginSys();return false;\" value=\"", ButtonText ?? "Войти", "\" type=\"submit\">");

            rowButton.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Attributes.Clear();
            cell.Style.Add("padding-left", "10px");
            cell.Controls.Add(new ESIALoginControl());

            rowButton.Cells.Add(cell);
            

            #endregion

            table.Rows.Add(rowLoginText);
            table.Rows.Add(rowLoginControl);
            table.Rows.Add(rowErrorLogin);
            table.Rows.Add(rowPassText);
            table.Rows.Add(rowPassControl);
            table.Rows.Add(rowErrorPass);
            table.Rows.Add(rowButton);
            container.Controls.Add(table);
            Controls.Add(container);
            Page.ClientScript.GetPostBackEventReference(this, string.Empty);
        }

        // Проверяем есть ли нужный паттерн при загрузке и вызываем событие
        protected override void OnLoad(EventArgs e)
        {
            if (Page.Request.Form["__EVENTTARGET"] == _pattern)
            {
                string eventArgument = Page.Request.Form["__EVENTARGUMENT"];
                string userpass = eventArgument.Replace(_pattern, "");
                string[] split = userpass.Split(new Char[] { ',' });
                string username = split[0];
                string pass = split[1];

                var loginParams = new LoginControlEventArgs(username, pass);
                this.OnAuthorize(loginParams);

                if (loginParams.Cancel)
                    PasswordTextBox.Focus();
            }

            base.OnLoad(e);
        }

        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            base.RenderBeginTag(writer);

            writer.Write("<script type=\"text/javascript\">");
            writer.Write(
            @"// Показать панель с ошибкой
            function ErrorPanelShow(text) {
                SetDivProperties('" + ErrorPanelID + @"', 'curved', text);
            }

            //Найти Error панель и задать ей текст 
            function SetDivProperties(component, cl, text) {
                var diverr = document.getElementById(component);

                if (diverr) {
                    diverr.className = cl;
                    diverr.innerHTML = text;
                }
            }

            function LoginSys() {
            var loginEl = document.getElementById('" + LoginTextBox.ClientID + @"');
            var login = loginEl.value;
            var passEl = document.getElementById('" + PasswordTextBox.ClientID + @"');
            var password = passEl.value;

            if (login)
            {
                if (password)
                 {
                    __doPostBack('" + _pattern + @"', '" + _pattern + @"' + login + ',' + password);
                    return;
                 }
            }

            var errtext = '';

            if (!login) { errtext += 'Не заполнено поле <b>Логин</b>. Пожалуйста, заполните его.'; loginEl.focus(); }
            if (!password) {
                if (errtext) errtext += '<br>'; errtext += 'Не заполнено поле <b>Пароль</b>. Пожалуйста, заполните его.';
                if (login) { passEl.focus() }
            }

            ErrorPanelShow(errtext); 
            }");
            writer.Write("</script>");
        }

        // добавляем скрипты на страницу, на первой загрузке очищаем авторизационные куки
        protected override void OnPreRender(System.EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoginTextBox.Focus();

                // очистить куки
                HttpResponse response = HttpContext.Current.Response;
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName) { Expires = DateTime.Now.AddDays(-1) };
                response.Cookies.Set(cookie);
            }

            base.OnPreRender(e);
        }

        // нужно для IpostBackEventhandler
        public void RaisePostBackEvent(string eventArgument)
        {

        }


        [Category("AuthorizeAction"), Description("Возникает в момент авторизации")]
        public event AuthorizeEventHandler Authorize;

        protected virtual void OnAuthorize(LoginControlEventArgs e)
        {
            if (Authorize != null)
            {
                Authorize(this, e);
            }
        }
    }

    public delegate void AuthorizeEventHandler(object sender, LoginControlEventArgs e);
    
    /// <summary>
    /// Данные для событий компонента LoginControl
    /// </summary>
    public class LoginControlEventArgs : EventArgs
    {
        /// <summary>/// Имя пользователя /// </summary>
        public string Login { get; protected set; }
        /// <summary>/// Пароль /// </summary>
        public string Password { get; protected set; }
        /// <summary>/// Признак отмены /// </summary>
        public bool Cancel { get; set; }

        public LoginControlEventArgs(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }
}


