﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class WebPageMap : ClassMapBase<WebPage>
	{
        public WebPageMap()
            : base("Web_Page")
		{
//            Table("w_");

//            Id(x => x.Id, "Web_Page_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            SqlInsert(@"exec ZZZ_Prc_Web_Page_Insert
//						  @Web_Page_Http_Name = ?
//						 ,@Web_Page_Name = ?
//						 ,@Web_Page_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Web_Page_Update
//						  @Web_Page_Http_Name = ?
//						 ,@Web_Page_Name = ?
//						 ,@Web_Page_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Web_Page_Delete
//						  @Web_Page_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.HttpName, "Web_Page_Http_Name").Nullable();
			Map(x => x.Name, "Web_Page_Name").Nullable();
        }
    }
}
