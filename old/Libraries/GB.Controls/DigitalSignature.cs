﻿using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using GB.Helpers;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxHiddenField;
using DevExpress.Web.ASPxCallback;
using System;
using System.Security.Cryptography;

namespace GB.Controls
{
    /// <summary>Параметры, неободимые для электронной подписи данных</summary>
    public class GetSourceEventArgs
    {
        /// <summary>Описание (дескриптор) данных</summary>
        public String Info { get; set; }
        /// <summary>Исходные данные</summary>
        public byte[] Data { get; set; }
    }

    public class VerificationEventArgs
    {
        public ReturnSignedDataEventArgs data {get; protected set;}

        public int Code { get; set; }

        public string Description { get; set; }

        public VerificationEventArgs(ReturnSignedDataEventArgs e)
        {
            this.data = e;
        }
        
        protected VerificationEventArgs()
        {
        }
    }

    /// <summary>Параметры содержащие результат подписания. 
    /// После внедрения плагина IFC часть полей стала Obsolete</summary>
    public class ReturnSignedDataEventArgs
    {
        /// <summary>Номер криптопровайдера</summary>
        [Obsolete]
        public int ProvNum { get; set; }

        /// <summary>Id ключа</summary>
        [Obsolete]
        public string Uuid { get; set; }

        /// <summary>Номер токена</summary>
        [Obsolete]
        public int SlotNum { get; set; }

        /// <summary>Id ключа</summary>
        public string Id { get; set; }

        /// <summary>PIN-код</summary>
        [Obsolete]
        public string Pin { get; set; }

        /// <summary>Описание (дескриптор) данных</summary>
        public string Info { get; protected set; }
        
        /// <summary>Электронная подпись(токен)</summary>        
        public string Sign { get; protected set; }

        /// <summary>То, что подписывали</summary>
        public byte[] SourceMessage { get; protected set; }

        /// <summary>Электронная подпись, полученная от IFCPlugin</summary>
        public byte[] CMS { get; protected set; }

        [Obsolete]
        public int SignType { get; set; }

        /// <summary>true = отсоединённая подпись (не содержит в себе хэш)</summary>
        public bool IsDetached { get; set; }

        /// <summary>Информация о сертификате</summary>
        public string Certificate { get; set; }

        public ReturnSignedDataEventArgs(byte[] SourceMessage, byte[] CMS, string Info, string Sign, bool IsDetached = true)
        {
            this.SourceMessage = SourceMessage;
            this.CMS = CMS;
            this.Info = Info;
            this.Sign = Sign;
            this.IsDetached = IsDetached;
        }
    }

    public class DigitalSignature : CompositeControl
    {
        ASPxHiddenField shf;
        ASPxCallback packageCallback;
        ASPxCallback signCallback;
        ASPxComboBox CertListComboBox;
        const string errorMessage = "При подписании объекта возникла ошибка";

        string jsMain =
@"
$(document).ready(function () {
    $('<object id=""IFCPlugin"" type=""application/x-ifcplugin"" width=""1"" height=""1""><param name=""onload"" value=""pluginLoaded"" /></object>').appendTo('body');
});

var package = '';
var signedPackage = '';
var ifcPlugin;
var ifcPluginError = 'Не найден плагин для работы с ЭЦП. Пожалуйста установите его.';

function pluginLoaded() {
    ifcPlugin = new IFCPlugin(document.getElementById('IFCPlugin'));
};

function " + GetClientSideReloadListMethod() + @" {

    if (typeof CertListComboBox =='undefined')
        return;

    CertListComboBox.ClearItems();
        
    refreshCertList();
        
    if (CertListComboBox.GetItemCount() > 0) {
        CertListComboBox.SetSelectedIndex(0);
    }
}

function " + GetClientSidePerformSignMethod() + @" {

    shf.Clear();
    package = null;

    if (!CertListComboBox.GetValue())
    {
        handleError('Необходимо выбрать сертификат');
        return;
    }

   packageCallBack.PerformCallback('');
}

function signPackage() {

    var errorMessage = null;
    var containerId = CertListComboBox.GetValue();
    var cert =  CertListComboBox.GetText();
    var pin = PinTextBox.GetText(); 

    shf.Set('containerId', containerId);
    shf.Set('cert', cert);
    
    errorMessage = signSave(containerId, pin);
    
    if (errorMessage) {
        handleError(errorMessage);
    }
    else {
        signCallBack.PerformCallback(signedPackage);
    }
}

function refreshCertList() {

    if (ifcPlugin != null) {
        var certificatesList = ifcPlugin.getCertificateList();
        if (ifcPlugin.getErrorCode() == 0) {
            for (var i = 0; i < certificatesList.length; i++) {
                var certificate = certificatesList[i];
                CertListComboBox.AddItem(certificate.getSubjectDN().getCommonName() + '; Издатель: ' + certificate.getIssuerDN().getCommonName() + '; Действителен с ' + certificate.getValidFrom() + ' по ' + certificate.getValidTo(), 
                                         certificate.getContainerId());                }
        } else {
            //addToLog('getCertificateList result: ' + ifcPlugin.getErrorDescription());
            //addError('Ошибка ' + ifcPlugin.getErrorCode() + ': ' + ifcPlugin.getErrorDescription());
        }
    }

}


function signSave(containerId, pin) {
    if (ifcPlugin != null) {
        if (!containerId) {
            return 'Необходимо выбрать сертификат';
        }
        
        signedPackage = ifcPlugin.signDataCmsDetached(containerId, pin, package);
        var errorCode = ifcPlugin.getErrorCode();
        if (errorCode != 0) {
            if (errorCode == IFCError.IFC_P11_LOGIN_ERROR) {
                return 'Пакет не удалось подписать. Неверный пин-код.';
            } else {
                return ifcPlugin.getErrorDescription();
            }
        }

        // package = null;
    }
    else {
        return 'Не установлен плагин';
    }
}   

function packageCallBackComplete(s, e) {
    if (e.result)
    {
        var splitted = e.result.split(';', 2);
        shf.Set('packageInfo', splitted[0]);
        shf.Set('packageData', splitted[1]);
        package = splitted[1];
    }
    else
    {
        package = null;
    }
        
    signPackage();
}

function signCallBackComplete(s, e) {
    certType = null;
    handleError(e.result);
}

function callBackError(s, e) {
    certType = null;
    handleError('Ошибка при подписании');
}";

        /// <summary>Клиентский обработчик ошибки ( по умолчанию вызывается alert('error') )</summary>
        public string ClientSideErrorEvent
        {
            get;
            set;
        }

        /// <summary>Заполнять сертификаты при загрузке страницы?</summary>
        public bool FillCertificatesOnLoad
        {
            get;
            set;
        }

        /// <summary>Название функции обновления списка сертификатов</summary>
        public static string GetClientSideReloadListMethod()
        {
            return "tryUpdateCertList()";
        }

        public static string GetClientSidePerformSignMethod()
        {
            return "signClick()";
        }


        public string PluginDownloadUrl { get; set; }

        /// <summary>JS функция которая будет вызвана после подписания</summary>
        public string CustomSignCallbackCompleteJS
        {
            get;
            set;
        }
    
        /// <summary>Делегат для получения исходного файла</summary>
        public delegate void GetSourceDelegate(object sender, GetSourceEventArgs e);

         /// <summary>Событие получения исходного файла</summary>
        public event GetSourceDelegate GetSource;


        /// <summary>Делегат для возврата подписанного файла (Sign - информация об эп)</summary>
        public delegate void ReturnSignedDataDelegate(object sender, ReturnSignedDataEventArgs e);

        /// <summary>событие возврата подписанного файла</summary>
        public event ReturnSignedDataDelegate ReturnSignedData;


        /// <summary>Делегат для проверки ЭЦП</summary>
        public delegate void VerifySignatureDelegate(object sender, VerificationEventArgs e);

        /// <summary>Проверка ЭП в УЦ</summary>
        public event VerifySignatureDelegate VerifySignature;

        public DigitalSignature()
        {
            shf = new ASPxHiddenField() { ClientInstanceName = "shf" };

            packageCallback = new ASPxCallback() { ClientInstanceName = "packageCallBack" };
            packageCallback.Callback += new CallbackEventHandler(packageCallback_Callback);
            packageCallback.ClientSideEvents.CallbackComplete = "function(s, e) { packageCallBackComplete(s, e); }";
            packageCallback.ClientSideEvents.CallbackError = "function(s,e){ callBackError(s,e); }";

            signCallback = new ASPxCallback() { ClientInstanceName = "signCallBack" };
            signCallback.Callback += new CallbackEventHandler(signCallback_Callback);

            signCallback.ClientSideEvents.CallbackComplete = "function(s, e) { }";  // signCallBackComplete(s, e); 
            signCallback.ClientSideEvents.CallbackError = "function(s,e){ callBackError(s,e); }";
        }

        /// <summary>Запрос на сохранение подписанного пакета</summary>
        void signCallback_Callback(object source, CallbackEventArgs e)
        {
            if (ReturnSignedData == null)
            {
                e.Result = errorMessage;
                return;
            }

            try
            {
                ReturnSignedDataEventArgs args = new ReturnSignedDataEventArgs(
                    shf.Contains("packageData") ? Convert.FromBase64String((string)shf["packageData"]) : null,
                    Convert.FromBase64String(e.Parameter),
                    shf.Contains("packageInfo") ? (string)shf["packageInfo"] : null,
                    CertListComboBox.Text);

                if (shf.Contains("cert") && shf["cert"] != null)
                    args.Certificate = Convert.ToString(shf["cert"]);

                args.Id = Convert.ToString(shf["containerId"]);
                
                e.Result = savePackage(args);
            }
            catch (Exception exc)
            {
                // TODO: запись в лог сообщения об ошибке
                throw exc;
            }
        }

        /// <summary>Запрос исходных данных</summary>
        void packageCallback_Callback(object source, CallbackEventArgs e)
        {
            string hash = null;
            GetSourceEventArgs args = new GetSourceEventArgs();

            if (GetSource != null)
            {
                GetSource(this, args);
                hash = CryptoHelper.ComputeHash(args.Data);
            }

            if (hash != null)
                e.Result = args.Info + ';' + hash;
            else
                e.Result = null;
        }

        protected override void CreateChildControls()
        {
            if (!string.IsNullOrEmpty(CustomSignCallbackCompleteJS))
            {
                signCallback.ClientSideEvents.CallbackComplete = "function(s, e) { " + CustomSignCallbackCompleteJS + " }";
            }

            this.Controls.Add(shf);
            this.Controls.Add(packageCallback);
            this.Controls.Add(signCallback);

            HtmlTable table = new HtmlTable();
            table.Width = this.Width.ToString();
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableCell cell = new HtmlTableCell();
            cell.Attributes.Add("class", "");
            cell.InnerHtml = "Сертификат:";
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Attributes.Add("class", "");
            HtmlTable innerTable = new HtmlTable();
            innerTable.Style.Add(HtmlTextWriterStyle.BorderCollapse, "collapse");
            innerTable.CellSpacing = 0;
            innerTable.Width = "100%";
            HtmlTableRow innerRow = new HtmlTableRow();
            HtmlTableCell innerCell = new HtmlTableCell();
            CertListComboBox = new ASPxComboBox()
            {
                ID = "CertListComboBox",
                ClientInstanceName = "CertListComboBox",
                TextField = "Name",
                ValueField = "ID",
                ValueType = typeof(string),
                EnableCallbackMode = true,
                EncodeHtml = false,
                Width = new Unit("100%")
            };
            innerCell.Controls.Add(CertListComboBox);
            innerRow.Cells.Add(innerCell);

            innerCell = new HtmlTableCell();
            ASPxButton RefreshButton = new ASPxButton()
            {
                ID = "RefreshButton",
                ClientInstanceName = "RefreshButton",
                AutoPostBack = false
            };
            RefreshButton.ClientSideEvents.Click = "function(s,e){ " + GetClientSideReloadListMethod() +"; }";
            RefreshButton.Text = "Обновить список";
            innerCell.Controls.Add(RefreshButton);
            innerRow.Cells.Add(innerCell);
            innerTable.Rows.Add(innerRow);

            cell.Controls.Add(innerTable);
            row.Cells.Add(cell);
            table.Rows.Add(row);

            row = new HtmlTableRow();
            cell = new HtmlTableCell();
            cell.Attributes.Add("class", "");
            cell.InnerHtml = "ПИН-код:";
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Attributes.Add("class", "form_edit_input");
            ASPxTextBox PinTextBox = new ASPxTextBox()
            {
                ID = "PinTextBox",
                ClientInstanceName = "PinTextBox",
                Width = new Unit(100),
                Password = true
            };
            cell.Controls.Add(PinTextBox);
            row.Cells.Add(cell);
            table.Rows.Add(row);


            row = new HtmlTableRow();
            cell = new HtmlTableCell();
            cell.Attributes.Add("class", "");
            cell.InnerHtml = "Тип подписи:";
            row.Cells.Add(cell);

            //cell = new HtmlTableCell();
            //cell.Attributes.Add("class", "form_edit_input");
            //ASPxComboBox TypeComboBox = new ASPxComboBox()
            //{
            //    ID = "SignTypeComboBox",
            //    ClientInstanceName = "SignTypeComboBox",
            //};
            //TypeComboBox.ValueType = typeof(int);
            //TypeComboBox.Items.Add("Подпись ЭП государственного органа", 1);
            //TypeComboBox.Items.Add("Подпись ЭП должностного лица", 2);
            //TypeComboBox.SelectedIndex = 0;

            //cell.Controls.Add(TypeComboBox);
            //row.Cells.Add(cell);
            //table.Rows.Add(row);



            this.Controls.Add(table);
        }

        protected override void OnPreRender(System.EventArgs e)
        {
            string ifcScript = "/Content/GB/Scripts/ifcplugin-lib.js";
            this.Page.ClientScript.RegisterClientScriptInclude(ifcScript, ifcScript);


            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "dsScript", jsMain, true);
            
            string js = @"
function handleError(message) {
    var errorFunc = " + (ClientSideErrorEvent ?? "null") + @";
    if (errorFunc) {
        errorFunc(message);
    }
    else {
        alert(message);
    }
};";
            if (FillCertificatesOnLoad)
            {
                js += (@" $(document).ready(function () { setTimeout(function(){ " + GetClientSideReloadListMethod() + @"; }, 300); });");
            }

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "dsScriptErr", js, true);

          
            
            base.OnPreRender(e);
        }

        /// <summary>Сохранить пакет</summary>
        private string savePackage(ReturnSignedDataEventArgs args)
        {
            string validationErrorMessage = null;
            int code = 0;
            if (VerifySignature != null)
            {
                VerificationEventArgs va = new VerificationEventArgs(args);
                VerifySignature(this, va);
                validationErrorMessage = va.Description;
                code = va.Code;
            }
            if (code != 0)
            {
                throw new Exception(validationErrorMessage);
                //return validationErrorMessage;
            }
            else
            {
                ReturnSignedData(this, args);
                return "Объект успешно подписан";
            }
            
        }
    }
}
