﻿using GB.Data.Documents;
using GB.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.Documents
{
    public class SaveDocumentCommand : ICommand<SaveDocumentContext>
    {
        ISessionFactoryGb _factory;

        public SaveDocumentCommand(ISessionFactoryGb factory)
        {
            _factory = factory;
        }

        public void Execute(SaveDocumentContext context)
        {
            Dictionary<string, string> newFiles = StringHelper.FileListToDictionary(context.fileList);

            var files2Remove = new List<Files>();

            foreach (Files file in context.listDocs)
            {
                string id = file.Id.ToString();
                if (newFiles.ContainsKey(id))
                {
                    newFiles.Remove(id);
                }
                else
                {                    
                    files2Remove.Add(file);
                }
            }

            if (files2Remove.Any())
            {
                FilesRemove(files2Remove);
            }

            if (context.Session != null)
            {
                FilesAdd(context, newFiles, context.Session);
            }
            else
            {
                using (var session = context.Session ?? _factory.Create())
                {
                    FilesAdd(context, newFiles, session);
                    session.Commit();
                }
            }
        }

        private static void FilesAdd(SaveDocumentContext context, Dictionary<string, string> newFiles, SessionHibernate session)
        {
            foreach (string key in newFiles.Keys)
            {
                Files file = new Files(context.type, context.owner);
                file.FileName = file.Name = newFiles[key];
                session.Insert(file);

                string guidString = key;
                try
                {
                    int slashIndex = guidString.IndexOf('/');
                    if (slashIndex >= 0)
                    {
                        guidString = guidString.Substring(0, slashIndex);
                    }
                    Guid guid = new Guid(guidString);
                    FileProvider.MoveToGeneric(guid, file.Id);
                }
                catch (Exception e)
                {
                    session.Delete(file);
                    Logger.Fatal(string.Format("Не удалось переместить файл из temp в хранилище. key={0} guidString={1} Document.Id={2}",
                        key, guidString, file.Id), e);
                }
            }
        }

        private void FilesRemove(List<Files> files)
        {
            using (var session = _factory.Create())
            {
                foreach (var fl in files)
                {
                    FileProvider.DeleteFile(fl.Id);
                    session.Delete(fl);
                }
                session.Commit();
            }
        }
    }
}
