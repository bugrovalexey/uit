﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class FilesDto
    {
        public int Id { get; set; }

        public int EntityType { get; set; }

        public string EntityOwnerId { get; set; }

        public int Type { get; set; }

        public int Kind { get; set; }

        public string FileName { get; set; }

        public string Extension { get; set; }

        public int? Size { get; set; }

        public string Url { get; set; }

        public string MimeType { get; set; }
    }
}
