﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess
{
    /// <summary>
    /// Интерфейс для команды.
    /// </summary>
    /// <typeparam name="TCommandContext">Контекст команды</typeparam>
    public interface ICommand<in TCommandContext> where TCommandContext : ICommandContext
    {
        /// <summary>
        /// Выполняет действия команды.
        /// </summary>
        /// <param name="commandContext">Контекст команды</param>
        void Execute(TCommandContext context);
    }

    public interface ICommand<in TCommandContext, out TCommandResult> where TCommandContext : ICommandContext
    {
        /// <summary>
        /// Выполняет действия команды.
        /// </summary>
        /// <param name="commandContext">Контекст команды</param>
        TCommandResult Execute(TCommandContext context);
    }
}
