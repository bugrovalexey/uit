﻿using System;
using System.ServiceModel.Configuration;

namespace GB.SmevBehaviorExtension
{
    public class ServiceBehaviorExtension : BehaviorExtensionElement
    {
        private static readonly Object syncRoot = new Object();
        private static volatile string CERTIFICATE_NAME = null; // "03799DC86B03323FAF7C88AFA9148799C22DF7CD"

        public static string CertificateName
        {
            get
            {
                return CERTIFICATE_NAME;
            }
        }

        public static void SetCertificateName(string name)
        {
            if (CERTIFICATE_NAME == null)
            {
                lock (syncRoot)
                {
                    if (CERTIFICATE_NAME == null)
                    {
                        CERTIFICATE_NAME = name;
                    }
                }
            }
        }

        public override Type BehaviorType
        {
            get { return typeof(ExtendedServiceBehavior); }
        }

        protected override object CreateBehavior()
        {
            return new ExtendedServiceBehavior() { CertificateName = CERTIFICATE_NAME };
        }
    }
}
