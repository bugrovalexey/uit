﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    class ExternalIsAndComponentsItkiMap : ClassMapHiLo<ExternalIsAndComponentsItki>
    {
        public ExternalIsAndComponentsItkiMap()
            : base("OU_Externel_IS")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Externel_IS_Name").Not.Nullable();
            Map(x => x.Purpose, "OU_Externel_IS_Goal");
            Map(x => x.Smev, "OU_Externel_IS_SMEV");
            Map(x => x.Url, "OU_Externel_IS_URL");

            References(x => x.Owner, "OU_ID").Not.Nullable();
        }

    }
}
