﻿using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using GB;
using GB.Helpers;
using DevExpress.Web.ASPxHiddenField;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxCallback;
using System.IO;

//using Uviri.Registry.Entities;
//using Uviri.Registry.Entities.Documents;
//using Uviri.Registry.Repository;
//using Uviri.Registry.Web.Code;

namespace GB.Controls
{
    [Obsolete("Использовать FileUploadControl2")]
    public class FileUploadControl : CompositeControl
    {
        protected HtmlTable table;
        protected HtmlAnchor link;
        protected HtmlGenericControl blocklink;
        protected HtmlGenericControl container;
        protected ASPxHiddenField hf;
        protected ASPxUploadControl uploader;
        protected ASPxCallback callback;
        protected CustomValidator validator;

        protected string ContainerItem;
        public string ContainerItemFormat { get; set; }

        //public DocumentTypeEnum DocumentType { get; set; }
        //public List<Document> Documents { get; protected set; }
        
        //public EntityBase Owner;
        public bool ReadOnly;
        public bool MultiLine;

        public FileUploadControl()
        {
            table = new HtmlTable();
            link = new HtmlAnchor();
            blocklink = new HtmlGenericControl();
            container = new HtmlGenericControl("div");
            hf = new ASPxHiddenField();
            uploader = new ASPxUploadControl();
            //uploader.ValidationSettings.MaxFileSize = 32777216;// int.MaxValue;
            //uploader.UploadMode = UploadControlUploadMode.Advanced;
            callback = new ASPxCallback();
            validator= new CustomValidator();

            //Documents = new List<Document>();
            
            uploader.FileUploadComplete += uploader_FileUploadComplete;
            callback.Callback += callback_Callback;
        }

        /// <summary>
        /// Справочник файлов. Ключ - путь к файлу, значение - имя (подпись) файла.
        /// </summary>
        /// <returns></returns>
        public ICollection<KeyValuePair<string, object>> GetValue()
        {
            return hf;
        }

        protected override void CreateChildControls()
        {
            Controls.Add(hf);
            if (!ReadOnly)
            {
                Controls.Add(callback);
                blocklink.Controls.Add(link);
                Controls.Add(uploader);
                Controls.Add(blocklink);

                var validatorContainer = new HtmlGenericControl("div");
                validatorContainer.Controls.Add(validator);
                Controls.Add(validatorContainer);
            }
            Controls.Add(container);

            container.Style.Add("clear", "both");

            //foreach (var item in Documents)
            //{
            //    hf.Add(item.Id.ToString(), item.FileName);
            //}

            blocklink.Style.Add("margin-top", "4px");
            blocklink.Style.Add("display", "inline-block");
            blocklink.Attributes.Add("class", "fileupload_none");
            
            link.Attributes.Add("class", "command fileupload_sep");
            link.Attributes.Add("onclick", uploader.ClientID + ".Upload();");
            link.InnerText = "Загрузить";

            //uploader.UploadMode = UploadControlUploadMode.Advanced;
            uploader.ShowProgressPanel = true;
            uploader.BrowseButton.Text = "Обзор...";
            uploader.Style.Add("float", "left");

            uploader.ClientSideEvents.TextChanged = string.Concat(
                "\n", "function(s, e) {",
                "\n", "var text = s.GetText();",
                "\n", "if (text){",
                "\n", "$(\"#", blocklink.ClientID, "\").removeClass(\"fileupload_none\");",
                "\n", "}else{",
                "\n", "$(\"#", blocklink.ClientID, "\").addClass(\"fileupload_none\");",
                "\n", "}}");

            uploader.ClientSideEvents.FileUploadStart = string.Concat(
                "\n", "function(s, e) {",
                "\n", "$(\"#", blocklink.ClientID, "\").addClass(\"fileupload_none\");",
                "\n", "}");

            uploader.ClientSideEvents.FileUploadComplete = string.Concat(
                "\n", "function(s, e) {",
                "\n", "if (e.isValid) {",
                "\n", "var pair = e.callbackData.split(';', 2);",
                "\n", "$('#",validator.ClientID, "').hide();",
                "\n", uploader.ClientID, "AddContainer(pair[0], pair[1]);",
                "}}");

            validator.Display = ValidatorDisplay.Dynamic;
            validator.ErrorMessage = "Данное поле не может быть пустым";
            validator.ClientValidationFunction = "ValidateFile";
            //validator.ServerValidate += File_FileUploadValidator_Validate;
            validator.CssClass = "dxeErrorCell_Aqua";

            callback.ClientSideEvents.CallbackComplete = string.Concat(
                "\n", "function(s, e) {",
                "\n", hf.ClientID, ".Remove(e.parameter);",
                "\n", this.ClientID, "UpdateContainer();",
                "}");

            string deleteScript = null;

            if (!ReadOnly)
                deleteScript = string.Concat(
                    "<div class=\"fileupload_sep fileupload_item_delete\" title=\"Удалить\" onclick=\"",
                    uploader.ClientID, "DeleteItem(", "\\'", "' + id + '", "\\'", ")",
                    "\"></div>");

            ContainerItem = string.Concat(
                "'<table class=\"table_not_border\"><tr><td><a href=\"", UrlHelper.Resolve("~/file.aspx"), "/'+ id +'\">' + name + '</a></td>'",
                //"'<table class=\"table_not_border\"><tr><td><span>' + name + '</span></td>'",
                " + ", "'<td>", deleteScript, "</td></tr></table>'");
            ContainerItemFormat = "'<div class=\"fileupload_item\">' + {0} + '</div>'";
        }

        private void File_FileUploadValidator_Validate(object source, ServerValidateEventArgs e)
        {
            ICollection<KeyValuePair<string, object>> files = GetValue();

            if (files.Count == 0)
            {
                e.IsValid = false;
            }
        }

        public override void RenderBeginTag(System.Web.UI.HtmlTextWriter writer)
        {
            base.RenderBeginTag(writer);

            writer.Write("<script type=\"text/javascript\">");

            if (!ReadOnly)
            {
                writer.Write(string.Concat(
                    "\n", "function ", uploader.ClientID, "AddContainer(id, name) { ",
                    "\n", hf.ClientID, ".properties[ASPxClientHiddenField.TopLevelKeyPrefix + id] = name;",
                    "\n", this.ClientID, "UpdateContainer();",
                    "\n", "}"));

                writer.Write(string.Concat(
                    "\n", "function ", uploader.ClientID, "DeleteItem(id) {",
                    callback.ClientID, ".PerformCallback(id);",
                    "\n", "}"));
            }

            writer.Write(string.Concat(
                "\n", "function ", this.ClientID, "UpdateContainer() {",
                "\n", "var pref = ASPxClientHiddenField.TopLevelKeyPrefix;",
                "\n", "var properties = ", hf.ClientID, ".properties;",
                "\n", "var length = 0;",
                "\n", "$(\"#", container.ClientID, "\").empty();",
                "\n", "for (var i in properties) {",
                "\n", "var id = i.substring(pref.length);",
                "\n", "var name = properties[i];",
                "\n", "$(\"#", container.ClientID, "\").append(", string.Format(ContainerItemFormat, ContainerItem), ");",
                "\n", "length++; }",
                "\n", this.ClientID, "UpdateMultiLine(length);",
                "\n", "}"));

            writer.Write(string.Format(@"
                function ValidateFile(source, arguments)
                    {{
                        var properties = {0}.properties;
                        var length = 0;
                        for (var i in properties) {{
                            length++;
                        }}
                        arguments.IsValid = length > 0;
                    }}
                ", hf.ClientID));

            if (!MultiLine && !ReadOnly)
            {
                writer.Write(string.Concat(
                    "\n", "function ", this.ClientID, "UpdateMultiLine(length) {",
                    "\n", "if(length > 0){",
                    "\n", uploader.ClientID, ".SetVisible(false);}",
                    "\n", "else{",
                    "\n", uploader.ClientID, ".SetVisible(true);}",
                    "\n", "}"));
            }
            else
            {
                writer.Write(string.Concat(
                    "\n", "function ", this.ClientID, "UpdateMultiLine(length) {",
                    "\n", "}"));
            }


            writer.Write("</script>");
        }

        public override void RenderEndTag(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write(string.Concat(
                "<script type=\"text/javascript\">",
                this.ClientID, "UpdateContainer();",
                "</script>"));

            base.RenderEndTag(writer);
        }

        protected void uploader_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            try
            {
                if (FileUploadComplete != null)
                {
                    FileUploadEventArgs ef = new FileUploadEventArgs();

                    ef.FileName = FileHelper.TranslateFileName(e.UploadedFile.FileName);
                    ef.ContentType = e.UploadedFile.ContentType;

                    FileUploadComplete(sender, ef);
                    
                    Logger.Info(ef.FileName);
                    Logger.Info(ef.ContentType);
                    Logger.Info(ef.Id.ToString());
                    Logger.Info(ef.FilePath);

                    using (Stream file = File.OpenWrite(ef.FilePath))
                    {
                        CopyStream(e.UploadedFile.PostedFile.InputStream, file);
                    }

                        //{

                    //e.UploadedFile.SaveAs(ef.FilePath);

                    //using (MemoryStream ms = new MemoryStream(e.UploadedFile.FileBytes))
                    //{
                    //    ef.Stream = ms;
                    //    ef.FileName = Core.Helpers.FileHelper.TranslateFileName(e.UploadedFile.FileName);
                    //    ef.ContentType = e.UploadedFile.ContentType;

                    //    FileUploadComplete(sender, ef);
                    //}

                    e.CallbackData = string.Concat(ef.Id, ";", ef.FileName);
                }
                else
                    throw new ArgumentNullException("Не задано событие FileUploadComplete");
            }
            catch (Exception exp)
            {
                Logger.Fatal(exp);
                throw exp;
            }
        }

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }

        protected void callback_Callback(object source, CallbackEventArgs e)
        {
            int id;

            if (int.TryParse(e.Parameter, out id))
            {
                //RepositoryBase<Document>.Delete(id);
            }
        }

        public void SetValue(string fileName, string filePath)
        {
            hf.Add(filePath, fileName);
        }

        /// <summary>
        /// Пользователь загрузил файл
        /// </summary>
        public event FileUpload FileUploadComplete;
    }


    public enum FileUploadMode
    {
        OneFile,
        MultiFile
    }

    public delegate void FileUpload(object sender, FileUploadEventArgs e);

    public class FileUploadEventArgs : EventArgs
    {
        public Guid Id { get; set; }
        public Stream FileContent { get; set; }

        public string FileName { get; set; }
        public string ContentType { get; set; }

        //public bool Cancel { get; set; }

        public string FilePath { get; set; }
    }
}