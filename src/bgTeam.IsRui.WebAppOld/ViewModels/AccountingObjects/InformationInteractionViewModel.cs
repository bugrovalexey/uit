﻿using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class InformationInteractionViewModel : AccountingObjectBase
    {
        public InformationInteractionViewModel(int? id)
            : base(id)
        {
            InformationInteractions =
                /*new InformationInteractionService().Repository*/new InformationInteractionRepository().GetAllByAccountingObject(id).Select(x => new
                    TableInformationInteraction(x));
        }

        public IEnumerable<TableInformationInteraction> InformationInteractions { get; set; }
    }
}