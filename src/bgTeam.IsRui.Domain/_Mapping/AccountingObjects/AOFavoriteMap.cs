﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    internal class AOFavoriteMap : ClassMapHiLo<AOFavorite>
    {
        public AOFavoriteMap()
            : base("OU_Favorites") { }

        protected override void InitMap()
        {
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
            References(x => x.User, "Users_ID");
        }
    }
}
