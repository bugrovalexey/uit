﻿using bgTeam.IsRui.Domain;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SiteMapNode = bgTeam.IsRui.Domain.Entities.Admin.SiteMapNode;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin.SiteMap
{
    [MetadataType(typeof(SiteMapEditViewModel_Valid))]
    public class SiteMapEditViewModel : ViewModelBase
    {
        private const string TitleCaption = "Наименование";
        private const string ParentIdCaption = "Родитель";

        public SiteMapEditViewModel(int? id)
        {
            var siteMapNode = id.HasValue
                ? new RepositoryDc<SiteMapNode>().Get(q =>
                    q.Fetch(x => x.Parent).Eager.Where(x => x.Id == Id))
                : null;
            siteMapNode.Do(Fill);
        }

        public void Fill(SiteMapNode node)
        {
            Id = node.Id;
            Title = node.Title;
            Url = node.Url;
            Parent = node.Parent.Return(x => new List<IEntityName>() { new SelectEntityInfo { Id = x.Id, Name = x.Url } }, default(List<IEntityName>));
        }

        public int Id { get; set; }

        [DisplayName(TitleCaption)]
        public string Title { get; set; }

        public string Url { get; set; }

        [DisplayName(ParentIdCaption)]
        public List<IEntityName> Parent { get; set; }
    }

    internal abstract class SiteMapEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Title { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string Url { get; set; }
    }
}