﻿using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Caching;
using System.Web.UI;

namespace GB.ESIA
{
	public class Util
	{
		/// <summary>
		/// Возвращает абсолютный URL.
		/// </summary>
		/// <param name="page">Страница.</param>
		/// <param name="relativeUrl">Относительный URL.</param>
		/// <returns>Абсолютный URL.</returns>
		public static string GetAbsoluteUrl(Page page, string relativeUrl)
		{
			return new Uri(page.Request.Url, page.ResolveUrl(relativeUrl)).ToString();
		}

		/// <summary>
		/// Вставляет объект в кэш приложения (System.Web.Caching.Cache) с указанием срока действия и приоритета.
		/// </summary>
		/// <param name="key">Ключ, по которому можно однозначно идентифицировать объект в кэше.</param>
		/// <param name="value">Объект, размещаемый в кэше.</param>
		/// <param name="slidingExpiration">
		/// Интервал между временем последнего доступа к объекту и временем, когда объект будет автоматически удалён из кэша.
		/// </param>
		public static void InsertIntoCache(string key, object value, TimeSpan slidingExpiration)
		{
			HttpContext.Current.Cache.Insert(key, value, null, Cache.NoAbsoluteExpiration, slidingExpiration, CacheItemPriority.Normal, null);
		}

		/// <summary>
		/// Удаляет объект из кэша приложения (System.Web.Caching.Cache).
		/// </summary>
		/// <param name="key">Ключ, по которому можно однозначно идентифицировать объект в кэше.</param>
		/// <returns>Объект, удалённый из кэша. Если объект не найден, возвращается null.</returns>
		public static object RemoveFromCache(string key)
		{
			return HttpContext.Current.Cache.Remove(key);
		}

		/// <summary>
		/// Определяет, есть ли информация в коллекции.
		/// </summary>
		/// <param name="collection">Коллекция объектов.</param>
		/// <returns>true - коллекция пуста, false - в противном случае.</returns>
		public static bool IsCollectionEmptyOrNull(ICollection collection)
		{
			return (collection == null) || (collection.Count == 0);
		}
        
        /// <summary>
        /// Пишем в лог в корень проекта, данные перезаписываются
        /// потоконебезопасно
        /// </summary>
        /// <param name="fileName">Имя файла в проекте</param>
        /// <param name="data">данные для логирования</param>
	    public static void WriteLog(string fileName, string data)
	    {
	        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, fileName);
	        File.WriteAllText(filePath, data);
	    }
	}
}