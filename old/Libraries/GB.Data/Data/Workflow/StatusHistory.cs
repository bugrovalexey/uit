﻿using GB.Data.Common;
using System;

namespace GB.Data.Workflow
{
    /// <summary>История изменения статуса</summary>
    public class StatusHistory : EntityBase
    {
        /// <summary>Пользователь</summary>
        public virtual User User { get; set; }

        /// <summary>Исходный статус</summary>
        public virtual Status From { get; set; }

        /// <summary>Конечный статус</summary>
        public virtual Status In { get; set; }

        /// <summary>Дата смены статуса</summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>Комментарий</summary>
        public virtual string Comment { get; set; }

        /// <summary>Id объекта</summary>
        public virtual int EntityId { get; set; }

        /// <summary>Тип объекта</summary>
        public virtual EntityType EntityType { get; set; }
    }
}