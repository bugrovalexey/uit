﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.DataAccess.Repository;
using NHibernate.Criterion;
using System;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Analytics
{
    public class AnalyticsViewModel : ViewModelBase
    {
        public SelectList DepartmentList { get; set; }

        public int? DepartmentId { get; set; }

        public AnalyticsViewModel()
        {
            DepartmentList = new SelectList(new RepositoryDc<Department>().GetAllEx(q => q.OrderBy(x => x.Code)), "Id", "Name");
        }

        public AnalyticsDataViewModel GetChartData(int? departmentId)
        {
            var rootIKTs = new RepositoryDc<IKTComponent>().GetAllEx(q => q                
                .Fetch(x => x.AccountingObjects).Eager                                
                .TransformUsing(CriteriaSpecification.DistinctRootEntity)
                .Where(x => x.Parent == null && x.Id != 0)).ToList();

            Func<AccountingObject, bool> funcFilterDep;
            if (departmentId == 0)
                funcFilterDep = (obj) => true;
            else
                funcFilterDep = (obj) => obj.Department.Id == departmentId;

            //берем только те объекты, у которых есть объекты учета(напрямую или через детей) выбранного подразделения и их сумма больше 0
            rootIKTs = rootIKTs.Where(r => (r.AccountingObjects.Any(funcFilterDep) && r.AccountingObjects.Where(funcFilterDep).Sum(it => it.ActivitiesCost) > 0) ||
                                           (r.Child.Any(ch => ch.AccountingObjects.Any(funcFilterDep)) && r.Child.Sum(c => c.AccountingObjects.Where(funcFilterDep).Sum(ao => ao.ActivitiesCost)) > 0)).ToList();
                
            var data = new AnalyticsDataViewModel();
            data.IKTNames = new string[rootIKTs.Count];
            data.Data = new IKTObjectData[rootIKTs.Count];
            
            for (int i = 0; i < rootIKTs.Count; i++)
            {
                data.IKTNames[i] = rootIKTs[i].Name;

                var acountingObjs = rootIKTs[i].AccountingObjects.Where(funcFilterDep).ToList();

                int rootObjCount = acountingObjs.Count;
                int rootChild = rootIKTs[i].Child.Count;

                var iktData = new IKTObjectData();
                iktData.BalanceCost = acountingObjs.Sum(a => a.ActivitiesCost) + rootIKTs[i].Child.SelectMany(c => c.AccountingObjects).Where(funcFilterDep).Sum(ob => ob.ActivitiesCost);
                iktData.Name = rootIKTs[i].Name;                
                iktData.Childs = new ChildData[rootObjCount + rootChild];

                for (int j = 0; j < rootObjCount; j++)
                {                    
                    iktData.Childs[j] = new ChildData {
                                                        Name = acountingObjs[j].ShortName,
                                                        Cost = acountingObjs[j].ActivitiesCost,
                                                        ObjectsNames = new string[] { acountingObjs[j].ShortName },
                                                        BalanceValues = new decimal[] { acountingObjs[j].ActivitiesCost }
                                                      };
                }

                for (int k = 0; k < rootChild; k++)
                {
                    var childAcountingObjs = rootIKTs[i].Child[k].AccountingObjects.Where(funcFilterDep).ToList();

                    iktData.Childs[k + rootObjCount] = new ChildData
                    {
                        Name = rootIKTs[i].Child[k].Name,
                        Cost = childAcountingObjs.Sum(a => a.ActivitiesCost)
                    };

                    iktData.Childs[k + rootObjCount].ObjectsNames = new string[childAcountingObjs.Count];
                    iktData.Childs[k + rootObjCount].BalanceValues = new decimal[childAcountingObjs.Count];

                    for (int n = 0; n < childAcountingObjs.Count; n++)
                    {
                        iktData.Childs[k + rootObjCount].ObjectsNames[n] = childAcountingObjs[n].ShortName;
                        iktData.Childs[k + rootObjCount].BalanceValues[n] = childAcountingObjs[n].ActivitiesCost;
                    }
                }

                data.Data[i] = iktData;
            }

            return data;
        }
    }

    public class AnalyticsDataViewModel
    {
        public string[] IKTNames { get; set; }

        public IKTObjectData[] Data { get; set; } 
        
    }

    public class IKTObjectData
    {
        public decimal BalanceCost { get; set; }

        public string Name { get; set; }        

        public ChildData[] Childs { get; set; }
    }

    public class ChildData
    {
        public string[] ObjectsNames { get; set; }

        public decimal[] BalanceValues { get; set; }

        public decimal Cost { get; set; }

        public string Name { get; set; }
    }
}