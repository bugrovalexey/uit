﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class ClassComponentIKTMap : ClassMapBase<ClassComponentIKT>
    {
        public ClassComponentIKTMap() :
            base("Class_Component_IKT")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Class_Component_IKT_Name");
        }
    }
}