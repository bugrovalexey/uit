﻿namespace GB.DataAccess.Command.AccountingObjects.ActsToAOActivities
{
    public class DeleteActivityFromActCommand : ICommand<DeleteActivityFromActContext>
    {
        ISessionFactoryGb factory;

        public DeleteActivityFromActCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteActivityFromActContext context)
        {
            using (var session = factory.Create())
            {
                session.Delete<Data.AccountingObjects.ActToAOActivity>(context.Id);
                session.Commit();
            }
        }
    }
}
