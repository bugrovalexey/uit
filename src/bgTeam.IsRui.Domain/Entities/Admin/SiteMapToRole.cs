﻿using bgTeam.IsRui.Domain.Entities.Common;

namespace bgTeam.IsRui.Domain.Entities.Admin
{
    public class SiteMapToRole : EntityBase
    {
        public virtual Role Role { get; set; }

        public virtual string RoleName
        {
            get
            {
                return Role.Name;
            }
        }

        public virtual SiteMapNode SiteMap { get; set; }
    }
}