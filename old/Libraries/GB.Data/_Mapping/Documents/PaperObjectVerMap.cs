﻿using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Entities.Statuses;

//using Uviri.MKRF.Entities.OtherExpObjects;

namespace GB.MKRF.Mapping.Documents
{
    class PaperObjectVerMap : ClassMapBase<PaperObjectVer>
    {
        public PaperObjectVerMap()
            : base("Plans_Paper_Ver")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.NumberOutgoing, "Plans_Paper_Ver_Ved_Out_Num");
            Map(x => x.DateOutgoing, "Plans_Paper_Ver_Ved_Out_Date");
            Map(x => x.NumberInboxMKS, "Plans_Paper_Ver_MKC_In_Num");
            Map(x => x.DateInboxMKS, "Plans_Paper_Ver_MKC_In_Date");
            Map(x => x.NumberOutgoingMKS, "Plans_Paper_Ver_MKC_Out_Num");
            Map(x => x.DateOutgoingMKS, "Plans_Paper_Ver_MKC_Out_Date");

            Map(x => x.ExpStartDateF, "Plans_Paper_Ver_Exp_Start_Date_F");
            Map(x => x.ExpEndDateF, "Plans_Paper_Ver_Exp_End_Date_F");
            Map(x => x.ExpReturnDate, "Plans_Paper_Ver_Exp_Return_Date");
            Map(x => x.Comment, "Plans_Paper_Ver_Comm");
            Map(x => x.CreateDT, "Plans_Paper_Ver_Timestamp").ReadOnly().LazyLoad();

            References<Status>(x => x.Status, "Status_ID").Not.Nullable();
            References<Document>(x => x.Document, "Document_Ver_ID");
            References<Document>(x => x.DocumentMKS, "Document_Ver_ID_MKC");
            References<PaperType>(x => x.PaperType, "Papper_Type_ID").Not.Nullable();

            ReferencesAny(x => x.Owner)
               .AddMetaValue<ExpDemand>(((int)TableTypeEnum.Exp_Demand).ToString())
               .AddMetaValue<Plan>(((int)TableTypeEnum.Plan).ToString())
                //.AddMetaValue<ExpObjectDemand>(((int)TableTypeEnum.ExpObjectDemand).ToString())
               .EntityTypeColumn("Portal_Table_ID")
               .EntityIdentifierColumn("Plans_Paper_Ver_Obj_ID").IdentityType<int>();

            HasMany<DocToPaper>(x => x.Docs)
                .KeyColumn("Plans_Paper_Ver_ID")
                .Cascade.AllDeleteOrphan()
                .Inverse();
        }
    }
}