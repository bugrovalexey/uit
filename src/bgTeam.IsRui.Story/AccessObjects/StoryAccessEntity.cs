﻿namespace bgTeam.IsRui.Story.AccessObjects
{
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess.Repository;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class StoryAccessEntity : IStoryAccess
    {
        public StoryAccessEntity()
        {
        }

        public void CheckAccess<TStoryContext, TResult>(IStory<TStoryContext, TResult> story)
        {
            CheckAccessAsync(story).Wait();
        }

        public async Task CheckAccessAsync<TStoryContext, TResult>(IStory<TStoryContext, TResult> story)
        {
        }
    }
}
