﻿using System;

namespace GB.MKRF.Extensions
{
    public static class ExceptionExtensions
    {
        public static Exception GetFirstException(this Exception exp)
        {
            if (exp.InnerException != null)
            {
                return GetFirstException(exp.InnerException);
            }

            return exp;
        }
    }
}