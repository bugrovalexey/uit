﻿namespace bgTeam.IsRui.Common.Services.Impl
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    public class FileProviderRui : IFileProviderRui
    {
        private readonly string _tempFolder;
        private readonly string _filesFolder;
        private readonly IAppLogger _logger;
        private readonly IHostingEnvironment _hostingEnviroment;
        private object _lock = new object();

        public FileProviderRui(IAppLogger logger, IHostingEnvironment environment, IFileProviderRuiSetting setting)
        {
            _logger = logger;
            _hostingEnviroment = environment;
            _tempFolder = InitPhysicalFolder(setting.TempFolderPhysicalPath ?? "Temp");
            _filesFolder = InitPhysicalFolder(setting.FilesFolderPhysicalPath ?? "Files");
        }

        public string GenerateFileName()
        {
            return Guid.NewGuid().ToString();
        }

        public void DeleteFile(object id)
        {
            File.Delete(GetPhysicalPath(id));
        }

        public string GetPhysicalPath(object id)
        {
            switch (id)
            {
                case int value:
                    return Path.Combine(_filesFolder, value.ToString());
                case Guid value:
                    return Path.Combine(_tempFolder, value.ToString());
                default:
                    return null;
            }
        }

        public void MoveToGeneric(Guid guid, int id, bool replace = false)
        {
            var src = GetPhysicalPath(guid);
            var dest = GetPhysicalPath(id);

            var srcFile = new FileInfo(src);
            var destFile = new FileInfo(dest);

            lock (_lock)
            {
                if (replace && destFile.Exists)
                {
                    destFile.Delete();
                }

                srcFile.MoveTo(dest);
            }
        }

        public async Task<string> SaveAsync(IFormFile file)
        {
            var fileName = GenerateFileName();

            using (var fs = File.OpenWrite(GetPhysicalPath(Guid.Parse(fileName))))
            {
                await file.CopyToAsync(fs);
            }

            return fileName;
        }

        public Stream OpenFileToRead(int id)
        {
            return File.OpenRead(GetPhysicalPath(id));
        }

        public async Task<byte[]> ReadFileAsync(int id)
        {
            var bytes = Enumerable.Empty<byte>();
            var buffer = new byte[1024];
            int readBytes;

            using (Stream stream = OpenFileToRead(id))
            {
                do
                {
                    readBytes = await stream.ReadAsync(buffer, 0, buffer.Length);
                    bytes = bytes.Concat(buffer.Take(readBytes).ToArray()).ToArray();
                }
                while (readBytes > 0);
            }

            return bytes.ToArray();
        }

        private string InitPhysicalFolder(string directory)
        {
            var absDirectory = GetAbsoluteDirectory(directory);
            _logger.Info($"Инициализация директории - {absDirectory}");

            if (!Directory.Exists(absDirectory))
            {
                lock (_lock)
                {
                    try
                    {
                        Directory.CreateDirectory(absDirectory);
                    }
                    catch (Exception exp)
                    {
                        _logger.Error(exp);
                    }
                }
            }

            return absDirectory;
        }

        private string GetAbsoluteDirectory(string directory)
        {
            return Path.Combine(_hostingEnviroment.WebRootPath, directory);
        }
    }
}
