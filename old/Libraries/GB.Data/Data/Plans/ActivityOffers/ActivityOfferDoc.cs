﻿using System;

namespace Uviri.MKRF.Entities.Plans.ActivityOffers
{
    public class ActivityOfferDoc : EntityBase
    {
        /// <summary>Номер</summary>
        public virtual string Num { get; set; }

        /// <summary>Дата</summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Пункты, статья</summary>
        public virtual string Points { get; set; }

        /// <summary>
        /// Мероприятие плана
        /// </summary>
        public virtual ActivityOffer Activity { get; set; }
    }
}
