﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807090151)]
    public class Migration_201807090151 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "Information_Exchange" });
        }
    }
}
