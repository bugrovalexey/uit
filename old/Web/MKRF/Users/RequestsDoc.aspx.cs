﻿using System;
using GB.Helpers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Documents;
using GB.Repository;

namespace GB.MKRF.Web.Users
{
    public partial class RequestsDoc : System.Web.UI.Page
    {
        private const string DocIdKey = "DocId";
        
        private bool loaded;
        private UserRequest current;
        
        public UserRequest UserR
        {
            get
            {
                if (!loaded)
                {
                    int id = 0;
                    if (int.TryParse(Request.QueryString[DocIdKey], out id))
                    {
                        loaded = true;
                        current = RepositoryBase<UserRequest>.Get(id);
                    }
                }
                return current;
            }
        }

        public static string GetUrl(int id)
        {
            return string.Concat(UrlHelper.Resolve("~/Users/RequestsDoc.aspx"), "?", DocIdKey, "=", id);
        }

        protected override void OnInit(EventArgs e)
        {
            dc.DocType = DocTypeEnum.UserRequest;
            dc.Owner = UserR;
            
            base.OnInit(e);
        }
    }
}