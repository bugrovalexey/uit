﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Validator
{
    class Reference
    {
        public static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create)
            {
                Entities.Plans.Reference entity = Entity as Entities.Plans.Reference;

                if (entity.Demand != null)
                    return "0;" + entity.Demand.Id.ToString();
                else
                    return "0";
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
