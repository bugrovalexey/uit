﻿using GB;
using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Validation
{
    internal class UserRequest
    {
        /// <summary>Параметры для валидации запроса на добавление пользователя</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create)
            {
                Entities.Admin.UserRequest Item = (Entities.Admin.UserRequest)Entity;
                return "0;" + (Item.Department != null ? Item.Department.Id.ToString() : "0");
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
