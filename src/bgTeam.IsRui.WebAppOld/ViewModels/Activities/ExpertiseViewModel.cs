﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Extensions;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Domain.Entities.Workflow;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.DataAccess.Services;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using bgTeam.IsRui.Common.Helpers;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public class ExpertiseViewModel : ActivitiesCardBase
    {
        public int ConclusionId
        {
            get { return CurrentActivity.ExpertiseConclusion.Id; }
        }

        public IEnumerable<ExpertTableModel> ExpertList { get; set; }

        public ExpertiseViewModel(IUserRepository userRepository, IStatusRepository statusRepository, int id)
            : base(statusRepository, id)
        {
            //var service = new ActivityExpertiseConclusionService();

            ReadOnly = !(RoleEnum.CA.IsEq(userRepository.GetCurrent().Roles) //редактировать может только Куратор
                         && StatusEnum.ActOnExpertise.IsEq(CurrentActivity)  //в статусе На экспертизе в ЦА
                         && StatusEnum.ExpAppointment.IsEq(CurrentActivity.ExpertiseConclusion) //в суб-статусе Назначаются эксперты
                        );

            ExpertList = new ActivityExpertService().Repository.GetAllEx(q => q
                .Where(x => x.Conclusion == CurrentActivity.ExpertiseConclusion))
                .Select(x => new ExpertTableModel(x));
        }
    }

    public class ExpertTableModel : NotificationBaseViewModel
    {
        private const string NameCaption = "ФИО";
        private const string RoleCaption = "Роль эксперта";

        public ExpertTableModel(ActivityExpert e)
        {
            Id = e.Id;
            Name = e.Expert.FullName;
            Role = EnumHelper.GetDescription<ActivityExpertEnum>(e.Role);
        }

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public object Name { get; set; }

        [DisplayName(RoleCaption)]
        public object Role { get; set; }

        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("ExpertDelete", "Activities", new { id = Id }, "GridExpertise"); }
        }
    }
}