﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class IKTTypicalFuncRequirementMap : ClassMapBase<IKTTypicalFuncRequirement>
    {
        public IKTTypicalFuncRequirementMap()
            : base("IKT_Typical_Func_Req")
        {
            //Table("w_");

            //Id(x => x.Id, "IKT_Typical_Func_Req_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "IKT_Typical_Func_Req_Name");

            HasManyToMany(x => x.IKTComponents)
                .Table("IKT_Component_TFR")
                .ParentKeyColumn("IKT_Typical_Func_Req_ID")
                .ChildKeyColumn("IKT_Component_ID");
        }
    }
}
