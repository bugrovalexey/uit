﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Threading.Tasks;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;

    public class AoSoftwareDeleteSoftwareStory : IStory<AoSoftwareDeleteSoftwareStoryContext, bool>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;


        public AoSoftwareDeleteSoftwareStory(IMapperBase mapper, IRepositoryRui repository, ISessionNHibernateFactory factory)
        {
            _mapper = mapper;
            _repository = repository;
            _factory = factory;
        }

        public bool Execute(AoSoftwareDeleteSoftwareStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(AoSoftwareDeleteSoftwareStoryContext context)
        {

            var characteristicOfLicense = _repository.Get<CharacteristicOfLicense>(
                q => q.Where(x => x.Software.Id == context.Id));

            if (characteristicOfLicense != null)
            {
                using (var session = _factory.OpenSession())
                {
                    await session.DeleteAsync<CharacteristicOfLicense>(characteristicOfLicense.Id);
                }
            }

            using (var session = _factory.OpenSession())
            {
                await session.DeleteAsync<Software>(context.Id);
            }

            return true;
        }
}
}
