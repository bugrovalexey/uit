﻿using GB.Data.AccountingObjects;
using GB.Data.Documents;
using GB.MKRF.Helpers;
using System;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    public class DocumentAccountingObjectService : ServiceBase<DocumentAccountingObject>, IService<DocumentAccountingObject, DocumentAccountingObjectArgs>
    {
        public DocumentAccountingObject Create(DocumentAccountingObjectArgs args)
        {
            return SaveOrUpdate(new DocumentAccountingObject(), args);
        }

        public DocumentAccountingObject Update(DocumentAccountingObjectArgs args)
        {
            //SetCurrentEdit(args.Id);
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private DocumentAccountingObject SaveOrUpdate(DocumentAccountingObject entity, DocumentAccountingObjectArgs args)
        {
            entity = ConvertDtoToEntity.FillDocumentAccountingObjectFromDto(entity, args);

            base.SaveOrUpdate(entity);
            base.Refresh(entity);

            new DocumentService()
                .Save(Convert.ToString(args.Files), entity.Documents, entity, DocTypeEnum.DecisionToCreateAccountingObject);

            base.SaveOrUpdate(entity);
            base.Refresh(entity);

            return entity;
        }

        public void Delete(DocumentAccountingObjectArgs args)
        {
            base.Delete((int)args.Id);
        }
    }
}