﻿using GB.Data.Documents;
using GB.Data.Workflow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Комментарий пользователя к ОУ
    /// </summary>
    public class AccountingObjectUserComment : EntityBase
    {
        public AccountingObjectUserComment()
        {
            Documents = new List<Files>();            
        }

        /// <summary>Комментарий</summary>
        public virtual string Comment { get; set; }            

        /// <summary>ОУ</summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>Пользователь</summary>
        public virtual GB.Data.Common.User User { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public virtual DateTime CreateDate { get; set; }

        /// <summary>
        /// Документ 
        /// </summary>
        public virtual IList<Files> Documents { get; set; }

        //public virtual int? StatusId { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual Status Status { get; set; }
    }
}
