﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class OrderWayMap : ClassMapBase<OrderWay>
    {
        public OrderWayMap()
            : base("Order_Way")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Order_Way_Name");
        }
    }
}