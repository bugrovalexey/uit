﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GoodsGetAllGoodsStory : IStory<GoodsGetAllGoodsStoryContext, IEnumerable<ActivityGoodsDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public GoodsGetAllGoodsStory(IRepositoryRui repository, IMapperBase mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<ActivityGoodsDto> Execute(GoodsGetAllGoodsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivityGoodsDto>> ExecuteAsync(GoodsGetAllGoodsStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var goods = _repository.GetAllEx<ActivityGoods>(
                q => q.Where(x => x.PlansActivityId == context.ActivityId))
                .Select(x => _mapper.Map(x, new ActivityGoodsDto()));

            return goods;
        }
    }
}
