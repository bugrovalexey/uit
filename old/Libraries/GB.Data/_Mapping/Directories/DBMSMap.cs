﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class DBMSMap : ClassMapBase<DBMSType>
    {
        public DBMSMap()
            : base("DBMS_Type")
        {
            //Table("w_");

            //Id(x => x.Id, "DBMS_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "DBMS_Type_Name");
            Map(x => x.NameSmall, "DBMS_Type_SName");
            Map(x => x.Version, "DBMS_Type_Ver");
            Map(x => x.Developer, "DBMS_Type_Dev");
            Map(x => x.Comment, "DBMS_Type_Comm");
        }
    }
}
