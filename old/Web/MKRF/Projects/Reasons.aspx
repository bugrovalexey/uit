﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Projects/EditCard.Master" AutoEventWireup="true" CodeBehind="Reasons.aspx.cs" Inherits="GB.MKRF.Web.Projects.Reasons" %>
<%--Основания--%>
<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <style>
        table.a_cd .form_edit_desc {
            width: 320px;
        }
    </style>

    <script type="text/javascript">

        function FormSave() {
        }

        /*
        function ReasonsSave() {
            hf.Set('ReasonName', ReasonNPANameTextBox.GetText());
            hf.Set('ReasonTypeId', ReasonNPATypeComboBox.GetValue());
            hf.Set('ReasonNum', ReasonNPANumTextBox.GetText());
            hf.Set('ReasonDate', ReasonNPADateDateEdit.GetValue());
            hf.Set('ReasonItem', ReasonItemTextBox.GetText());
            hf.Set('ReasonFiles', uploader2.GetFiles());
        }*/

        function ShowConfirm(isChange) {
            if (isChange) {
                window.onbeforeunload = function (e) {
                    return "Внесенные на странице данные не сохранены.";
                };
            } else {
                window.onbeforeunload = null;
            }
        }

        $(document).ready(function () {
            //UpdateButtonClear('ResponsibleFinancial');
        });
    </script>

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" SyncWithServer="true" />
    <dxpc:ASPxPopupControl ID="PopupControl" runat="server" AllowDragging="True" AllowResize="True"
        CloseAction="CloseButton" Modal="True" EnableViewState="False"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="600px"
        ClientInstanceName="PopupControl" EnableHierarchyRecreation="True">
    </dxpc:ASPxPopupControl>

    <div class="panel_button">
        <dxe:ASPxButton ID="saveButton" runat="server" AutoPostBack="false" Text="Сохранить" ValidationGroup="CommonData" CssClass="fleft topButton save_button">
            <ClientSideEvents Click="function(s,e) { FormSave(); callBack.PerformCallback(''); MainLoadingPanel.Show(); ShowConfirm(false); }" />
        </dxe:ASPxButton>
    </div>
    <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" OnCallback="callBack_Callback">
        <ClientSideEvents
            CallbackComplete="function(s, e) { MainLoadingPanel.Hide(); }"
            CallbackError="function(s,e) { MainLoadingPanel.Hide(); }" />
    </dxc:ASPxCallback>

    <table class="form_edit">
        <tr>
            <td class="form_edit_desc">Государственная программа</td>
            <td class="form_edit_input">
                <dxe:ASPxComboBox ID="StateProgramComboBox" runat="server" ClientIDMode="Static" ClientInstanceName="StateProgramComboBox"
                    OnDataBinding="StateProgramComboBox_DataBinding" OnValueChanged="StateProgramComboBox_ValueChanged"
                    TextField="Name" ValueField="Id" ValueType="System.Int32">
                </dxe:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">Подпрограмма государственной программы</td>
            <td class="form_edit_input">
                <dxe:ASPxComboBox ID="StateSubProgramComboBox" runat="server" ClientIDMode="Static" ClientInstanceName="StateSubProgramComboBox"
                    OnDataBinding="StateSubProgramComboBox_DataBinding"
                    TextField="Name" ValueField="Id" ValueType="System.Int32">
                </dxe:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">Основное мероприятие государственной программы</td>
            <td class="form_edit_input">
                <dxe:ASPxComboBox ID="StateMainEventComboBox" runat="server" ClientIDMode="Static" ClientInstanceName="StateMainEventComboBox"
                    OnDataBinding="StateMainEventComboBox_DataBinding"
                    TextField="Name" ValueField="Id" ValueType="System.Int32">
                </dxe:ASPxComboBox>
            </td>
        </tr>
    </table>

   <p class="header_grid">Другие основания проекта</p>
    <dxe:ASPxButton ID="AddReasonsButton" runat="server" 
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {ReasonsGrid.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="ReasonsGrid" Width="100%" ClientInstanceName="ReasonsGrid"
        KeyFieldName="Id"
        OnBeforePerformDataSelect="ReasonsGrid_BeforePerformDataSelect"
        OnRowInserting="ReasonsGrid_RowInserting"
        OnRowUpdating="ReasonsGrid_RowUpdating"
        OnStartRowEditing="ReasonsGrid_StartRowEditing"
        OnRowDeleting="ReasonsGrid_RowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False"
                AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Id" ShowInCustomizationForm="False" Visible="False" />
            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Наименование документа" />
            <dxwgv:GridViewDataTextColumn FieldName="Num" Caption="Номер документа" />
            <dxwgv:GridViewDataTextColumn FieldName="SignerName" Caption="ФИО подписавшего" />
            <dxwgv:GridViewDataTextColumn FieldName="SignerPosition" Caption="Должность подписавшего" />
            <dxwgv:GridViewDataTextColumn FieldName="URL" Caption="URL документа" />
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">

                    <tr>
                        <td class="form_edit_desc required">Наименование документа:</td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo runat="server" ID="ReasonNPANameTextBox" ClientInstanceName="ReasonNPANameTextBox" Rows="5"
                                OnDataBinding="ReasonNPANameTextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>

                            </dxe:ASPxMemo>
                        </td>
                    </tr>

                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>Номер документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="ReasonNPANumTextBox" ClientInstanceName="ReasonNPANumTextBox"
                                OnDataBinding="ReasonNPANumTextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Дата документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="ReasonNPADateDateEdit" ClientInstanceName="ReasonNPADateDateEdit"
                                OnDataBinding="ReasonNPADateDateEdit_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxDateEdit>
                        </td>
                    </tr>

                    <tr>
                        <td class="form_edit_desc">Пункт, статья документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="ReasonItemTextBox" ClientInstanceName="ReasonItemTextBox"
                                OnDataBinding="ReasonItemTextBox_DataBinding" />
                        </td>
                    </tr>

                    <tr id="Tr1" runat="server">
                        <td class="form_edit_desc">
                            <div id="docLabel">Файлы:</div>
                        </td>
                        <td class="form_edit_input">
                            <cc:FileUploadControl2 runat="server" ID="FileUploader" Mode="MultiFile" ClientInstanceName="FileUploader"
                                OnDataBinding="FileUploader_DataBinding" AutoUpload="true" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') ReasonsSave(); }" />
    </dxwgv:ASPxGridView>
</asp:Content>