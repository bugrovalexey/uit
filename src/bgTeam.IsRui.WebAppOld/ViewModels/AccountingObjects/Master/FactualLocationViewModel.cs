﻿using System.ComponentModel;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class FactualLocationViewModel : StepBaseViewModel
    {
        public FactualLocationViewModel(int id)
            : base(id, MasterItemEnum.FactualLocation)
        {
        }

        [DisplayName(ResponsiblesViewModel.OrgNameCaption)]
        public string LocationOrgName { get { return Current.LocationOrgName; } }

        [DisplayName(ResponsiblesViewModel.FioResponsibleCaption)]
        public string LocationFioResponsible { get { return Current.LocationFioResponsible; } }

        [DisplayName(ResponsiblesViewModel.OrgAddressCaption)]
        public string LocationOrgAddress { get { return Current.LocationOrgAddress; } }

        [DisplayName(ResponsiblesViewModel.OrgPhoneCaption)]
        public string LocationOrgPhone { get { return Current.LocationOrgPhone; } }
    }
}