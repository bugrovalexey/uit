﻿using GB.MKRF.Logic.MasterEdit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Logic.AccountingObjectActions.Handler
{
    class ActionCreationHandler : ActionBaseHandler
    {
        protected override void Init()
        {
            //Add("Добавить мероприятие", MasterItemEnum.ActivityCreate, a_action_template);
            //Add("Добавить закупку", MasterItemEnum.Purchase, a_action_template);
            Add("Добавить госконтракт", MasterItemEnum.GovernmentContract, a_action_template);
            Add("Добавить акт сдачи-приемки", MasterItemEnum.ActsOfAcceptance, a_action_template);

            Add("Сменить стадию", null, "<button onclick='StageChangeOpen()'>{0}</button>");
        }

    }
}
