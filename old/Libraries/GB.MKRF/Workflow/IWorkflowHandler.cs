﻿using System;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Statuses;

namespace GB.MKRF.Workflow
{
    public interface IWorkflowHandler
    {
        void ReadyForNewStatus(IWorkflowObject workflowObject, Status newStatus);
    }
}