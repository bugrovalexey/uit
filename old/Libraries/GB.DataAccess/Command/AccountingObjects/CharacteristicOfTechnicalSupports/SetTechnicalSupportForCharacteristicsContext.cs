﻿using GB.Data.AccountingObjects.WorksAndGoods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.CharacteristicOfTechnicalSupports
{
    public class SetTechnicalSupportForCharacteristicsContext : ICommandContext
    {
        public List<int> CharacteristicsOfTechnicalSupportIds { get; set; }
        public TechnicalSupport TechnicalSupport { get; set; }
    }
}
