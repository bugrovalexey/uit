﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class QualifierMap : ClassMapBase<Qualifier>
    {
        public QualifierMap()
            : base("Qualifier") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "Qualifier_Type");
            Map(x => x.Descr, "Qualifier_Descr");

            References<QualifierGroup>(x => x.Group, "Qualifier_Group_ID");
        }
    }
}