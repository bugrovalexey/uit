﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class WorksGetAllWorkStory : IStory<WorksGetAllWorkStoryContext, IEnumerable<ActivityWorksDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public WorksGetAllWorkStory(IRepositoryRui repository, IMapperBase mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<ActivityWorksDto> Execute(WorksGetAllWorkStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivityWorksDto>> ExecuteAsync(WorksGetAllWorkStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var works = _repository.GetAllEx<ActivityWorks>(
                q => q.Where(x => x.PlansActivityId == context.ActivityId))
                .Select(x => _mapper.Map(x, new ActivityWorksDto()));

            return works;
        }
    }
}
