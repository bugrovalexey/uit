﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.AccountingObjects;

namespace GB.DataAccess.Command.AccountingObjects.Activities
{
    public class AddActivityContext : ICommandContext
    {
        public int? xId { get; set; }

        public string Name { get; set; }
        public short Year { get; set; }
        public int? Type { get; set; }
        public decimal? CostY0 { get; set; }
        public decimal? CostY1 { get; set; }
        public decimal? CostY2 { get; set; }

        public int? ActivityId { get; set; }
        public int AccountingObjectId { get; set; }
    }
}
