﻿<%@ Page Language="C#" MasterPageFile="~/AccountingObjects/EditCard.Master" AutoEventWireup="true" CodeBehind="CommonData.aspx.cs" Inherits="GB.MKRF.Web.AccountingObjects.CommonData" %>


<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function validateComboBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && e.value != 0;
        }

        function validateKindComboBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && (e.value != 0 || cbxKind.GetItemCount() == 1);
        }

        function validateMaskBox(s, e) {
            e.isValid = e.value == null || (e.value != null && e.value.length == 9);
        }

        function validateUrl(s, e) {
            var pattern = new RegExp(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/);
            e.isValid = e.value == null || pattern.test(e.value);
        }

        function SaveButtonClick(s, e) {
            if (ASPxClientEdit.ValidateGroup('CommonData')) {
                callBack.PerformCallback('');
                MainLoadingPanel.Show();
            }
        }

        function GridSave(e) {
            hf.Set('Name', txtDocName.GetValue());
            hf.Set('Number', txtDocNumber.GetValue());
            hf.Set('FioSigner', txtFioSigner.GetValue());
            hf.Set('PostSigner', txtPostSigner.GetValue());
            hf.Set('Url', txtUrl.GetValue());
            hf.Set('Files', fuFiles.GetFiles());
        }

        function OnEndCallbackCbxKind(s, e) {
            cbxKind.SetEnabled(cbxKind.GetItemCount() != 1);
        }

        $(function () {
            cbxKind.SetEnabled(cbxKind.GetItemCount() != 1);
        });
    </script>

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />

    <div class="panel_button">
        <dxe:ASPxButton runat="server" ID="saveButton" ClientInstanceName="saveButton" AutoPostBack="false"
            Text="Сохранить" ValidationGroup="CommonData" CssClass="fleft topButton save_button">
            <ClientSideEvents Click="SaveButtonClick" />
        </dxe:ASPxButton>
    </div>

    <table class="form_edit">
        <tr>
            <td class="form_edit_desc required">Текущий статус объекта учета
            </td>
            <td class="form_edit_input">
                <dxe:ASPxComboBox ID="cbxStatus" runat="server" ClientInstanceName="cbxStatus"
                    TextField="Name" ValueField="Id" ValueType="System.Int32" 
                    OnDataBinding="cbxStatus_OnDataBinding">
                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData">
                        <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                    </ValidationSettings>
                    <ClientSideEvents Validation="function(s,e){ validateComboBox(s, e); }" />
                </dxe:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">Уникальный идентификационный номер ОУ
            </td>
            <td class="form_edit_input">
                <div>
                    <dxe:ASPxTextBox ID="txtNumber" runat="server" ClientInstanceName="txtNumber"
                        OnDataBinding="txtNumber_OnDataBinding">
                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData" />
                        <MaskSettings Mask="99\.9999999" ErrorText="Неверное значение" IncludeLiterals="None"></MaskSettings>
                        <ClientSideEvents Validation="function(s,e){ validateMaskBox(s, e); }" />
                    </dxe:ASPxTextBox>
                </div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc required">Тип ОУ
            </td>
            <td class="form_edit_input">
                <div>
                    <dxe:ASPxComboBox ID="cbxType" runat="server" ClientInstanceName="cbxType"
                        TextField="Name" ValueField="Id" ValueType="System.Int32" 
                        OnDataBinding="cbxType_OnDataBinding" EnableSynchronization="False">
                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData">
                            <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                        </ValidationSettings>
                        <ClientSideEvents Validation="function(s,e){ validateComboBox(s, e); }"
                            SelectedIndexChanged="function(s, e) { cbxKind.PerformCallback(s.GetValue()); }" />
                    </dxe:ASPxComboBox>
                </div>
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                <span class="required"></span>
                Вид ОУ
            </td>
            <td class="form_edit_input">
                <dxe:ASPxComboBox ID="cbxKind" runat="server" ClientInstanceName="cbxKind"
                    TextField="Name" ValueField="Id" ValueType="System.Int32" 
                    OnDataBinding="cbxKind_OnDataBinding" OnCallback="cbxKind_OnCallback" EnableSynchronization="False">
                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData">
                        <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                    </ValidationSettings>
                    <ClientSideEvents
                        Validation="function(s,e){ validateKindComboBox(s, e); }"
                        EndCallback="OnEndCallbackCbxKind" />
                </dxe:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">Полное наименование ОУ
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox ID="txtFullName" runat="server" ClientInstanceName="txtFullName"
                    ClientIDMode="Static" OnDataBinding="txtFullName_OnDataBinding">
                </dxe:ASPxTextBox>
            </td>
        </tr>
    </table>

    <p class="header_grid">
        Решение о создании (закупке) ОУ
   
   
   
   
   
    </p>

    <dxe:ASPxButton ID="bntAddDocument" runat="server" ClientInstanceName="bntAddDocument" CssClass="fleft topButton"
        AutoPostBack="false" Text="Добавить">
        <ClientSideEvents Click="function(s,e) {gridDocuments.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="gridDocuments" Width="100%" ClientInstanceName="gridDocuments"
        KeyFieldName="Id" ViewStateMode="Disabled" AutoGenerateColumns="False"
        OnBeforePerformDataSelect="gridDocuments_OnBeforePerformDataSelect"
        OnRowInserting="gridDocuments_OnRowInserting"
        OnRowUpdating="gridDocuments_OnRowUpdating"
        OnStartRowEditing="gridDocuments_OnStartRowEditing"
        OnRowDeleting="gridDocuments_OnRowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Наименование документа" FieldName="Name" />
            <dxwgv:GridViewDataTextColumn Caption="Номер документа" FieldName="Number" />
            <dxwgv:GridViewDataTextColumn Caption="ФИО подписавшего" FieldName="FioSigner" />
            <dxwgv:GridViewDataTextColumn Caption="Должность подписавшего" FieldName="PostSigner" />
            <dxwgv:GridViewDataTextColumn Caption="URL на документ">
                <DataItemTemplate>
                    <a href='<%#Eval("Url")%>'><%#Eval("Url")%></a>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Документ-основание">
                <Settings AllowDragDrop="False" />
                <DataItemTemplate>
                    <%#GetDownloadUrl((string)Eval("FileInfo"))%>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc required">Наименование документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtDocName" ClientInstanceName="txtDocName"
                                runat="server" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                OnDataBinding="txtDocName_OnDataBinding">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Номер документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtDocNumber" ClientInstanceName="txtDocNumber"
                                runat="server"
                                OnDataBinding="txtDocNumber_OnDataBinding">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">ФИО подписавшего:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtFioSigner" ClientInstanceName="txtFioSigner"
                                runat="server"
                                OnDataBinding="txtFioSigner_OnDataBinding">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Должность подписавшего:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtPostSigner" ClientInstanceName="txtPostSigner"
                                runat="server"
                                OnDataBinding="txtPostSigner_OnDataBinding">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">URL на документ:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtUrl" ClientInstanceName="txtUrl"
                                runat="server" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                OnDataBinding="txtUrl_OnDataBinding">
                                <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData" ErrorText="Неверное значение" />
                                <ClientSideEvents Validation="validateUrl"></ClientSideEvents>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td class="form_edit_desc">
                            <div id="docLabel">
                                Файл:
                           
                            </div>
                        </td>
                        <td class="form_edit_input">
                            <cc:FileUploadControl2 runat="server" ID="fuFiles" Mode="OneFile"
                                AllowedFileExtensions=".pdf, .tiff, .tif, .jpg, .rar, .zip, .doc, .xls, .docx, .xlsx"
                                ClientInstanceName="fuFiles"
                                OnDataBinding="fuFiles_OnDataBinding" AutoUpload="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents
            BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') {GridSave(e);} }" />
    </dxwgv:ASPxGridView>


    <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" OnCallback="callBack_OnCallback">
        <ClientSideEvents CallbackComplete="function(s, e) { MainLoadingPanel.Hide(); }"
            CallbackError="function(s,e) { MainLoadingPanel.Hide(); }" />
    </dxc:ASPxCallback>
</asp:Content>
