﻿namespace GB.MKRF.Logic.Tabs.Handler
{
    internal class MainTabsHandler : TabsBaseHandler
    {
        public MainTabsHandler()
        {
            Add("Объекты учета", "Index", "AccountingObjects");
            Add("Мероприятия", "Index", "Activities");
            //Add("Планирование", "Index", "Plans");
            //Add("Аналитика", "Index", "Analytics");
        }
    }
}