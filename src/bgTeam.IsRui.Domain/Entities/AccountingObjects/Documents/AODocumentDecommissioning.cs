﻿using bgTeam.IsRui.Domain.Entities.Documents;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects.Documents
{
    /// <summary>
    ///  Документ вывода из эксплуатации ОУ
    /// </summary>
    public class AODocumentDecommissioning : AODocumentBase
    {
        internal static readonly DocTypeEnum DocType = DocTypeEnum.DecommissioningAccountingObject;    

        protected override DocTypeEnum GetTypeDocument()
        {
            return DocType;
        }

    }
}
