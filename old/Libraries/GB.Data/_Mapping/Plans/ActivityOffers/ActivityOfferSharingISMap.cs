﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans.ActivityOffers;

namespace Uviri.MKRF.Mapping.Plans.ActivityOffers
{
    public class ActivityOfferSharingISMap : ClassMapBase<ActivityOfferSharingIS>
    {
        public ActivityOfferSharingISMap()
            : base("Activity_Offer_Sharing")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Activity_Offer_Sharing_Name");

            References<FgisInfo>(x => x.FGIS, "FGIS_Info_ID");
            References<Department>(x => x.Department, "Govt_Organ_ID");
            References<ActivityOffer>(x => x.Offer, "Activity_Offer_Inform_ID");
        }
    }
}