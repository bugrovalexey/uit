﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using GB.Entity;
using GB.Repository;

namespace GB.Providers
{
    public class GbSiteMapProvider
    {
        const char _separator = '/';
        private readonly object _lock = new object();
        private Dictionary<string, string> _accessList;
        
        //Путь к информационным страницам
        static readonly string  ERROR_PATH = "~/Error/";


        /// <summary>
        /// Использовать через Singleton
        /// </summary>
        protected GbSiteMapProvider()
        {
            BuildSiteMap();
        }

        /// <summary>
        /// Проверить есть ли у пользователя доступ к странице.
        /// </summary>
        /// <param name="page">Страница.</param>
        /// <param name="user">Пользователь.</param>
        internal bool СheckAccessUser(string page, UserBase user)
        {
            var mass = page.Split(_separator);

            for (int i = mass.Length - 1; i > 0; i--)
            {
                var str = string.Empty;

                for (int j = i; j > 0; j--)
                {
                    str = string.Concat(_separator, mass[j], str);
                }

                var access = CheckAccess(string.Concat(mass[0], str), user);

                if (access.HasValue)
                    return access.Value;
            }

            return false;
        }

        private bool? CheckAccess(string path, UserBase user)
        {
            if (path.Contains(ERROR_PATH))
            {
                //Доступ на страницы информации открыт всем
                return true;
            }

            if (!_accessList.ContainsKey(path))
                return null;

            foreach (var item in user.Roles)
            {
                if (_accessList[path].Contains(item.Name))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Обновляет список прав
        /// </summary>
        public void Reset()
        {
            BuildSiteMap();
        }


        /// <summary>
        /// Заполняет список прав
        /// </summary>
        /// <returns></returns>
        public void BuildSiteMap()
        {
            lock (_lock)
            {
                _accessList = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

                foreach (var item in GetSiteMapData())
                {
                    if (string.IsNullOrEmpty(item.Roles))
                        continue;

                    string url = item.Url.Trim();
                    if (_accessList.ContainsKey(url))
                        continue;

                    _accessList.Add(url, item.Roles);
                }
            }
        }

        /// <summary>
        /// Получить данные об узлах проекта.
        /// </summary>
        /// <returns></returns>
        private IList<SiteMapData> GetSiteMapData()
        {
            var sql = @"
                SELECT Site_Map_Node_ID as Id,
                       Site_Map_Node_Par_ID as ParId,
                       Site_Map_Node_Url as Url,
                       Site_Map_Node_Title as Title,
                       Site_Map_Node_Descr as Description,
                       Site_Map_Node_Roles as Roles,
                       Site_Map_Node_Visible as Visible
                FROM v_Site_Map_Node z
                ORDER BY Site_Map_Node_Level,
                         Site_Map_Node_Order,
                         Site_Map_Node_Title";

            return RepositoryBase.ExecuteSqlToObject<SiteMapData>(sql);
        }

        private class SiteMapData
        {
            public int Id { get; set; }
            public int? ParId { get; set; }
            public string Url { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string Roles { get; set; }
            public bool Visible { get; set; }
        }
    }
}
