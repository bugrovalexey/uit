﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace bgTeam.IsRui.Domain.Entities.Purchase
{
    /// <summary>Контракт</summary>
    public class Contract : EntityBase
    {
        /// <summary>Номер контракт</summary>
        public virtual string Number { get; set; }

        /// <summary>Дата заключения контракта</summary>
        public virtual DateTime Sign_Date { get; set; }//null

        /// <summary>Год исполнения контракта</summary>
        public virtual int Execution_Year { get; set; }

        /// <summary>Месяц исполнения контракта</summary>
        public virtual int Execution_Month { get; set; }

        /// <summary>Цена контракта</summary>
        public virtual decimal Price { get; set; }//null

        /// <summary> Код заказчика </summary>
        public virtual string Customer_Reg_Num { get; set; }

        /// <summary> Заказчик </summary>
        public virtual string Customer_Name { get; set; }

        /// <summary>Наименование бюджета</summary>
        public virtual string Budget_Name { get; set; }

        /// <summary> Уровень бюджета </summary>
        public virtual string Budget_Level { get; set; }

        /// <summary>Код бюджетной классификации</summary>
        public virtual string KBK { get; set; }

        /// <summary>максимальная сумма контракта</summary>
        public virtual decimal MaxSum { get; set; }

        /// <summary>максимальная сумма контракта</summary>
        public virtual string NotificationNumber { get; set; }

        /// <summary>Статус</summary>
        public virtual string Status { get; set; }

        /// <summary> Номер реестровой записи </summary>
        public virtual string Reg_Num { get; set; }

        /// <summary> Предмет контракта </summary>
        public virtual IList<ContractProduct> Products { get; set; }

        /// <summary> Предмет контракта </summary>
        //public virtual IList<Suppler> Supplers { get; set; }

        public virtual string ProductName
        {
            get
            {
                var result = Products.Aggregate(string.Empty, (current, product) => current + (product.Name + "<br/>"));
                result = result.TrimEnd("\n".ToCharArray());
                return result;
            }
        }
    }
}