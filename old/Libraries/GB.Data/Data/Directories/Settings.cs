﻿using System;
using System.Globalization;

namespace GB.MKRF.Entities.Directories
{
    /// <summary>Настройки</summary>
    public class SettingsMKRF : EntityBase
    {
        /// <summary>Название</summary>
        public virtual string Name { get; set; }

        /// <summary>Тип данных</summary>
        public virtual SettingsMKRFTypeEnum Type { get; set; }

        /// <summary>Значение</summary>
        public virtual string Value { get; set; }
    }

    public enum SettingsMKRFKeys
    {
        /// <summary> Дата последнего обновления по закупкам </summary>
        ZakupkiLastDate = 1,
    }

    /// <summary>
    /// Типы значений настроек
    /// </summary>
    public enum SettingsMKRFTypeEnum
    {
        /// <summary>Строчный</summary>
        String = 0,

        /// <summary>Целочисленный</summary>
        Int = 1,

        /// <summary>Булевский</summary>
        Bool = 2,

        /// <summary>Дата</summary>
        Date = 3
    }

    public static class SettingsMKRFExtension
    {
        /// <summary>
        /// Сериализация значения
        /// </summary>
        /// <param name="option"></param>
        /// <param name="value"></param>
        public static void SetValue(this SettingsMKRF option, object value)
        {
            switch (option.Type)
            {
                case SettingsMKRFTypeEnum.Bool:
                    {
                        option.Value = (bool)value ? "true" : "false";
                    }
                    break;

                case SettingsMKRFTypeEnum.Date:
                    {
                        var dt = (DateTime)value;

                        option.Value = string.Format("{0}-{1}-{2}", dt.Year, dt.Month, dt.Day);
                    }
                    break;

                case SettingsMKRFTypeEnum.Int:
                    {
                        option.Value = ((int)value).ToString();
                    }
                    break;

                case SettingsMKRFTypeEnum.String:
                    {
                        option.Value = (string)value;
                    }
                    break;

                default:
                    throw new ArgumentException(option.Type.ToString());
            }
        }

        /// <summary>
        /// Десериализация значения
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public static object GetValue(this SettingsMKRF option)
        {
            if (option.Value == null)
                return null;

            object val = null;

            switch (option.Type)
            {
                case SettingsMKRFTypeEnum.Bool:
                    {
                        val = (string.Compare(option.Value, "true", true) == 0);
                    }
                    break;

                case SettingsMKRFTypeEnum.Date:
                    {
                        DateTime date;
                        CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
                        DateTimeStyles styles = DateTimeStyles.None;

                        if (DateTime.TryParse(option.Value, culture, styles, out date))
                            val = date;
                    }
                    break;

                case SettingsMKRFTypeEnum.Int:
                    {
                        int num = 0;
                        if (int.TryParse(option.Value, out num))
                            val = num;
                    }
                    break;

                case SettingsMKRFTypeEnum.String:
                    {
                        val = option.Value;
                    }
                    break;

                default:
                    throw new ArgumentException(option.Type.ToString());
            }

            return val;
        }
    }
}