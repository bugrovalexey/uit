﻿using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Mapping.Plans
{
    class FavoriteActivityMap : ClassMapBase<FavoriteActivity>
    {
        public FavoriteActivityMap() :
            base("Users_Activity")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.IsFavorite, "Users_Activity_Is_Favourite").Not.Nullable();
            References<PlansActivity>(x => x.PlansActivity, "Plans_Activity_ID").Not.Nullable();
            References<User>(x => x.User, "Users_ID").Not.Nullable();
        }
    }
}