﻿using System.Collections.Generic;

namespace bgTeam.IsRui.Domain.Entities.Frgu
{
    /// <summary>
    /// Государственная услуга (функция) ГУ/ГФ
    /// </summary>
    public class GovService : EntityBase
    {
        public GovService()
        {
            Documents = new List<GovServiceNPA>();
        }

        /// <summary>Наименование услуги, данное поле заполняется из serviceTargets/serviceTarget</summary>
        public virtual string Name { get; set; }

        /// <summary>Наименование услуги из описания услуги</summary>
        public virtual string Title { get; set; }

        /// <summary>Уникальный реестровый номер услуги</summary>
        public virtual string Number { get; set; }

        /// <summary>Описание результата предоставления услуги</summary>
        public virtual string Result { get; set; }

        ///// <summary> Пасспорт госуслуг </summary>
        //public virtual Gov_Passport Passport { get; set; }

        /// <summary> родительская услуга </summary>
        //public virtual Gov_Service Parent { get; set; }

        /// <summary> НПА документы </summary>
        public virtual IList<GovServiceNPA> Documents { get; protected set; }
    }
}
