﻿namespace bgTeam.IsRui.Story.Admin
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Users;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UserGetAllUsersStory : IStory<UserGetAllUsersStoryContext, IEnumerable<UsersDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public UserGetAllUsersStory(IRepositoryRui repository, IMapperBase mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<UsersDto> Execute(UserGetAllUsersStoryContext context)
        {
             return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<UsersDto>> ExecuteAsync(UserGetAllUsersStoryContext context)
        {
            var users = _repository.GetAllEx<UsersEntity>(q => q.Where(x => x.Id != 0)).Select(x => _mapper.Map(x, new UsersDto()));

            return users;
        }
    }
}
