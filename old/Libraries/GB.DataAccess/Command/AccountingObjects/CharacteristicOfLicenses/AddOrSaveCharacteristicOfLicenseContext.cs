﻿using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.WorksAndGoods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.CharacteristicOfLicenses
{
    public class AddOrSaveCharacteristicOfLicenseContext : BaseAddOrSaveContext
    {
        public int SoftwareId { get; set; }

        public string Name { get; set; }

        public string Model { get; set; }

        public TypeOfConnectionToServerEnum? TypeOfConnectionToServer { get; set; }

        public int? NumberOnlineSameTime { get; set; }

        public string ProductNumber { get; set; }

        public int? NumberAllocatedSameTime { get; set; }

        public string Licensor { get; set; }
    }
}
