﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoContractAddOrUpdateStoryContext
    {
        public int? Id { get; set; }

        public int? AoId { get; set; }

        public string Number { get; set; }

        public string Name { get; set; }

        public DateTime? BeginDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
