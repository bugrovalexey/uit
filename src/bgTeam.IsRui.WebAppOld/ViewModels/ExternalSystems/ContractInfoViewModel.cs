﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master;
using bgTeam.Extensions;
using bgTeam.IsRui.Zakupki360.Dto;

namespace bgTeam.IsRui.WebApp.ViewModels.ExternalSystems
{
    public class ContractInfoViewModel
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Order { get; set; }
        public string Number { get; set; }
        public string NumberLink { get; set; }
        public int Status { get; set; }
        public string StatusStr { get; set; }
        public string Price { get; set; }
        public string Executer { get; set; }


        public ContractInfoViewModel(ContractInfo item)
        {
            Id = item.Id;
            Date = item.Date.Value.ToString("dd.MM.yyyy");
            Order = item.OrderNumber;
            Number = item.Number;
            NumberLink = LinkBuilder.Link(item.Number, string.Concat(TableGovernmentContract.contract_url, item.Number), inNewTab: true);
            Status = (int)item.Status;
            StatusStr = item.Status.GetDescription();
            Price = item.PricePlan.ToString("N2");
            Executer = item.Supplier;
        }
    }
}           
            
            
            