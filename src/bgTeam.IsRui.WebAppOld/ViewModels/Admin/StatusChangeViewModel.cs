﻿using bgTeam.IsRui.Domain.Entities.Workflow;
using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    public class StatusChangeViewModel : ViewModelBase
    {
        public const string RoleCaption = "Роль";
        public const string StatusFromCaption = "Начальный статус";
        public const string StatusToCaption = "Конечный статус";

        public IList<TableStatusChange> Items { get; private set; }

        public StatusChangeViewModel()
        {
            Items =
                new RepositoryDc<StatusList>().GetAllEx(
                    q =>
                        q.Fetch(x => x.From).Eager
                        .Fetch(x => x.To).Eager
                        .Fetch(x => x.Role).Eager.Clone())
                    .Select(x => new TableStatusChange(x))
                    .ToList();
        }
    }
}