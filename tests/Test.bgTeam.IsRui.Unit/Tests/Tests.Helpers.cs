﻿using bgTeam.IsRui.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Test.bgTeam.IsRui.Unit.Tests
{
    public class TestsHelpers
    {
        [Theory]
        [InlineData("get default mime", "text/plain")]
        [InlineData(null, null)]
        [InlineData("test.txt", "text/plain")]
        [InlineData("test.png", "image/png")]
        [InlineData("TEST.PNG", "image/png")]
        [InlineData("TEST.DOC", "application/msword")]
        [InlineData("TEST.Doc", "application/msword")]
        [InlineData("TEST.xls", "application/excel")]
        [InlineData("TEST.more dots.xls", "application/excel")]
        public void Helpers_MimeTypeHelper(string mime, string expected)
        {
            var result = MimeTypeHelper.GetMimeTypeFromFileName(mime);
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Helpers_CryptoHelper()
        {
            var res = CryptoHelper.GetMD5Hash("1234");
            Assert.Equal("81dc9bdb52d04dc20036dbd8313ed055", res);
        }
    }
}
