﻿using System.Collections.Generic;
using Atp.Saml2;

namespace GB.ESIA
{
	/// <summary>
	/// Пользователь.
	/// Предоставляет доступ к идентификационным атрибутам пользователя, передаваемым из СИА.
	/// </summary>
	public class User
	{
		/// <summary>
		/// Фамилия.
		/// </summary>
		public string LastName { get; set; }

		/// <summary>
		/// Имя.
		/// </summary>
		public string FirstName { get; set; }

		/// <summary>
		/// Отчество.
		/// </summary>
		public string MiddleName { get; set; }

		/// <summary>
		/// Наименование организации.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Идентификатор пользователя.
		/// </summary>
		public string UserId { get; set; }

		/// <summary>
		/// ИНН.
		/// </summary>
		public string Inn { get; set; }

		/// <summary>
		/// СНИЛС.
		/// </summary>
		public string Snils { get; set; }

		/// <summary>
		/// Номер миграционной карты.
		/// </summary>
		public string Migration { get; set; }

		/// <summary>
		/// Тип пользователя.
		/// P – физ. лицо
		/// F – иностранный гражданин
		/// O – организация
		/// B – Индивидуальный предприниматель (ИП)
		/// </summary>
		public string UserType { get; set; }

		/// <summary>
		/// Токен безопасности.
		/// </summary>
		public string AuthToken { get; set; }

		/// <summary>
		/// Код ошибки.
		/// </summary>
		public string ErrorCode { get; set; }

        /// <summary> ИНН. </summary>
        public string OrgInn { get; set; }

        /// <summary> Тип организации </summary>
        public string OrgType { get; set; }

        /// <summary> Тип организации </summary>
        public string GlobalRole { get; set; }
		/// <summary>
		/// Конструктор.
		/// </summary>
		public User()
		{
		}

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="attributeStatement">Первая секция AttributeStatement из ответа на запрос аутентификации.</param>
		public User(AttributeStatement attributeStatement)
		{
			var attributes = attributeStatement.Attributes;
			if (Util.IsCollectionEmptyOrNull(attributes))
				return;
			foreach (var attribute in attributes)
			{
				var saml2Attribute = (Atp.Saml2.Attribute)attribute;
				switch (saml2Attribute.FriendlyName)
				{
					case "lastName":
						LastName = GetFirstAttributeValue(saml2Attribute.Values);
						break;
					case "firstName":
						FirstName = GetFirstAttributeValue(saml2Attribute.Values);
						break;
					case "middleName":
						MiddleName = GetFirstAttributeValue(saml2Attribute.Values);
						break;
					case "name":
						MiddleName = GetFirstAttributeValue(saml2Attribute.Values);
						break;
					case "userId":
						UserId = GetFirstAttributeValue(saml2Attribute.Values);
						break;
                    case "personINN":
						Inn = GetFirstAttributeValue(saml2Attribute.Values);
						break;
                    case "personSNILS":
						Snils = GetFirstAttributeValue(saml2Attribute.Values);
						break;
					case "migration":
						Migration = GetFirstAttributeValue(saml2Attribute.Values);
						break;
					case "usertype":
						UserType = GetFirstAttributeValue(saml2Attribute.Values);
						break;
					case "authToken":
						AuthToken = GetFirstAttributeValue(saml2Attribute.Values);
						break;
					case "errorCode":
						ErrorCode = GetFirstAttributeValue(saml2Attribute.Values);
						break;
                    case "orgINN":
                        OrgInn = GetFirstAttributeValue(saml2Attribute.Values);
				        break;
                    case "orgType":
                        OrgType = GetFirstAttributeValue(saml2Attribute.Values);
				        break;
                    case "globalRole":
                        GlobalRole = GetFirstAttributeValue(saml2Attribute.Values);
                        break;
				}
			}
		}

		/// <summary>
		/// Возвращает самое первое значение из всех значений атрибута в списке.
		/// </summary>
		/// <param name="items">Список значений атрибута.</param>
		/// <returns>Самое первое значение из всех значений атрибута в списке.</returns>
		private static string GetFirstAttributeValue(IList<AttributeValue> items)
		{
			return (items == null) || (items.Count == 0) ? string.Empty : items[0].ToString();
		}
	}
}
