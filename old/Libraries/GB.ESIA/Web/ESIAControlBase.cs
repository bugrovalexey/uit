﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using GB.ESIA.Web.Helpers;

namespace GB.ESIA.Web
{
    public class ESIAControlsBase : CompositeControl
    {
        protected Button _Button;
        public string CssClass { get; set; }
        public string LinkButtonText { get; set; }

        public ESIAControlsBase()
        {
            _Button = new Button();

            _Button.Text = "ЕСИА";
            _Button.Attributes.Add("class", "big white");
        }

        protected override void CreateChildControls()
        {
            if (ConfigAccessor.VisibleControl)
            {
                SetValues();

                _Button.Click += LinkButtonClick;
                Controls.Add(_Button);
            }
        }

        protected virtual void SetValues()
        {
            throw new NotImplementedException("Set Values");
        }

        protected virtual void LinkButtonClick(object sender, EventArgs e)
        {
            throw new NotImplementedException("Event click");
        }

        public static User GetESIAUser(HttpRequest request, ref string urlForSuccess, ref string error)
        {
            var authenticationResponse = new AuthenticationResponse(request, CertificateAccessor.IdentityProviderCert, CertificateAccessor.ServiceProviderCert);
            if (ConfigAccessor.DebugMode)
                Util.WriteLog("Temp/LastAuthResponse.xml", authenticationResponse.Xml.OuterXml);

            if (!authenticationResponse.IsSuccess)
            {
                error = authenticationResponse.StatusMessage;
                return null;
            }

            urlForSuccess = (string)Util.RemoveFromCache(authenticationResponse.AuthenticationRequestId);

            var user = authenticationResponse.User;

            //сохраняем NameIdentifier, который потребуется для формирования пакета LogOut.
            Util.InsertIntoCache(user.Snils, authenticationResponse.NameIdentifier, new TimeSpan(12, 0, 0));

            // Определение пользовательских данных (в частности, имени пользователя).
            return user;
        }
    }
}