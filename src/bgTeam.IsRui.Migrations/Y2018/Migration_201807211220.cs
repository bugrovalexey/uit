﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807211220)]
    public class Migration_201807211220 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("ArticleGroup")
                .WithColumn("AG_ID").AsInt32().PrimaryKey()
                .WithColumn("AG_Name").AsString().Nullable()

                .WithColumn("AG_CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("AG_TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "ArticleGroup" });

            Create.Table("ArticleBudget")
                .WithColumn("AB_ID").AsInt32().PrimaryKey()
                .WithColumn("AB_Name").AsString().Nullable()
                .WithColumn("AB_Group_ID").AsInt32().Nullable()

                .WithColumn("AB_CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("AB_TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "ArticleBudget" });

            Create.ForeignKey("FK_AB_Group")
               .FromTable("ArticleBudget").ForeignColumn("AB_Group_ID")
               .ToTable("ArticleGroup").PrimaryColumn("AG_ID");
        }
    }
}
