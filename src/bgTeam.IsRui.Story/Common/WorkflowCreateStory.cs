﻿namespace bgTeam.IsRui.Story.Common
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class WorkflowCreateStory : IStory<WorkflowCreateStoryContext, Workflow>
    {
        private readonly IStatusRepository _statusRepository;

        public WorkflowCreateStory(
            IStatusRepository statusRepository)
        {
            _statusRepository = statusRepository;
        }

        public Workflow Execute(WorkflowCreateStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<Workflow> ExecuteAsync(WorkflowCreateStoryContext context)
        {
            var status = _statusRepository.GetDefault(context.Entity);

            var workflow = new Workflow()
            {
                Status = status,
                EntityOwner = context.Entity,
            };

            await context.Session.InsertAsync(workflow);

            context.Entity.Workflow = workflow;

            await context.Session.UpdateAsync(context.Entity);

            return workflow;
        }
    }
}
