﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Mapping.Admin
{
    public class UsersRightMap : ClassMapBase<UsersRight>
    {
        public UsersRightMap()
            : base("Users_Rights")
        {
            //Table("w_");

            //Id(x => x.Id, "Users_Rights_ID").GeneratedBy.Custom(typeof(IdGenerator));

			
        }

        protected override void InitMap()
        {
            References<User>(x => x.User, "Users_ID").Not.Nullable();
			References<Right>(x => x.Right, "Rights_ID").Not.Nullable();
        }
    }
}
