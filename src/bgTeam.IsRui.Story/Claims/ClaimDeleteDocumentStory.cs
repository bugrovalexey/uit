﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Story.AccountingObjects;
    using System.Threading.Tasks;

    public class ClaimDeleteDocumentStory : ClaimDeleteBaseStory<ClaimDocument, ClaimDeleteDocumentStoryContext>
    {
        private readonly IFileProviderRui _fileProviderRui;

        public ClaimDeleteDocumentStory(
            IRepositoryRui repositoryRui,
            ISessionNHibernateFactory sessionNHibernateFactory,
            IFileProviderRui fileProviderRui)
            : base(repositoryRui, sessionNHibernateFactory)
        {
            _fileProviderRui = fileProviderRui;
        }

        protected override async Task DeleteReferenceAsync(ClaimDocument entity)
        {
            if (entity.File == null)
            {
                return;
            }

            var story = new FileDeleteStory(_repository, _fileProviderRui, _factory);

            await story.ExecuteAsync(new FileDeleteStoryContext { Id = entity.File.Id });
        }
    }
}
