﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PlanHtmlVersion.aspx.cs" Inherits="GB.MKRF.Web.Plans.PlanHtmlVersion" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="GB.Repository" %>
<%@ Import Namespace="GB.MKRF.Entities.Directories" %>
<%@ Import Namespace="GB.MKRF.Entities.Plans" %>
<%@ Import Namespace="GB.MKRF.Repository" %>
<%@ Import Namespace="GB.MKRF.Web.Plans.v2012new" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <title></title>
    <style type="text/css">
        body
        {
            width: 1024px;
            font: 0.8em/1.7em Tahoma, Arial, Helvetica, sans-serif;
            margin: auto;
        }
        ul
        {
            margin: 1px;
        }

            ul li
            {
                padding: 1px 0 1px 5px;
                font-size: 12px;
            }

        a, .arrow
        {
            color: rgb(0, 101, 197);
        }

        .arrow
        {
            font-size: 1.4em;
        }
    </style>
</head>
<body>
    <h1><%=CurrentPlan.Name %></h1>
    <div>
        <span class="headers_fieldnames">ОГВ:</span> <span class="headers_values"><%=CurrentPlan.Department != null ? CurrentPlan.Department.Name : "" %></span>
    </div>
    <div>
        <span class="headers_fieldnames">Год:</span> <span class="headers_values"><%=CurrentPlan.Year %></span>
    </div>
    <div>
        <span class="headers_fieldnames">Статус заявки:</span> <span class="headers_values"><%=CurrentPlan.Demand != null ? CurrentPlan.Demand.Status.Name : "Заявка отсутствует" %></span>
    </div>
    <div>
        <span class="headers_fieldnames">Ответственный:</span> <span class="headers_values"><%=GetResponsibleInfo() %></span>
    </div>
    <br />
    <div>
        <b>Объем бюджетных ассигнований, тыс. руб.</b>
        <% foreach (DataRow row in GetPlanSummary().Rows)
           { %>
        <br />
        <div>
            <span style="display: inline-block; width: 220px;"><%=row["Category"] %></span> <span><%=string.Format("{0:0.0000}", row["Summary"]) %></span>
        </div>
        <div>
            <span style="display: inline-block; width: 220px;">очередной финансовый год:</span> <span><%=string.Format("{0:0.0000}", row["ExpenseVolume0"]) %></span>
        </div>
        <div>
            <span style="display: inline-block; width: 220px;">первый год планового периода:</span> <span><%=string.Format("{0:0.0000}", row["ExpenseVolume1"]) %></span>
        </div>
        <div>
            <span style="display: inline-block; width: 220px;">второй год планового периода:</span> <span><%=string.Format("{0:0.0000}", row["ExpenseVolume2"]) %></span>
        </div>
        <%} %>
    </div>
    <br />
<%--    <div>
        <%= GetExpense242() %>
    </div>--%>
    <div>
        <h2><a name="Acitivities">Мероприятия</a></h2>
        <table class="readmode_table">
            <thead>
                <tr>
                    <td rowspan="2">Код</td>
                    <td rowspan="2">Наименование</td>
                    <td rowspan="2">Тип</td>
                    <td rowspan="2">Классификационный признак ИС или компонента ИТ-инфраструктуры</td>
                    <td colspan="3">Объем фактически израсходованных бюджетных ассигнований, тыс. руб</td>
                    <td colspan="3">Объем планируемых бюджетных ассигнований, тыс. руб</td>
                    <td colspan="3">Дополнительная потребность, тыс. руб</td>
                    <td colspan="3">Итого, тыс. руб</td>
                </tr>
                <tr>
                    <td>текущий финансовый год</td>
                    <td>первый год, предшествующий очередному финансовому (текущий год - 1)</td>
                    <td>второй год, предшествующий очередному финансовому (текущий год - 2)</td>
                    <td>очередной финансовый год</td>
                    <td>первый год планового периода</td>
                    <td>второй год планового периода</td>
                    <td>очередной финансовый год</td>
                    <td>первый год планового периода</td>
                    <td>второй год планового периода</td>
                    <td>очередной финансовый год</td>
                    <td>первый год планового периода</td>
                    <td>второй год планового периода</td>
                </tr>
            </thead>
            <tbody>
                <% var activitiesRows = GetActivityList().Select(string.Empty, "IsNotPriority, Num");
                   var activitiesAll = new List<PlansActivity>();
                   foreach (DataRow row in activitiesRows)
                   {
                       activitiesAll.Add(RepositoryBase<PlansActivity>.Get(row["Id"]));
                %>
                <tr>
                    <td>
                        <%=row["ActCode"] %>
                    </td>
                    <td>
                        <a href='#<%= row["Id"]%>'><%=row["Name"] %></a>
                    </td>
                    <td>
                        <%=row["Type"] %>
                    </td>
                    <td>
                        <%=row["IKT_Component_Code_Name"] %>
                    </td>
                    <td>
                        <%=row["PlansActivityFactVolY0"] %>
                    </td>
                    <td>
                        <%=row["PlansActivityFactVolY1"] %>
                    </td>
                    <td>
                        <%=row["PlansActivityFactVolY2"] %>
                    </td>
                    <td>
                        <%=row["PlansActivityVolY0"] %>
                    </td>
                    <td>
                        <%=row["PlansActivityVolY1"] %>
                    </td>
                    <td>
                        <%=row["PlansActivityVolY2"] %>
                    </td>
                    <td>
                        <%=row["PlansActivityAddVolY0"] %>
                    </td>
                    <td>
                        <%=row["PlansActivityAddVolY1"] %>
                    </td>
                    <td>
                        <%=row["PlansActivityAddVolY2"] %>
                    </td>
                    <td>
                        <%=row["VolY0"] %>
                    </td>
                    <td>
                        <%=row["VolY1"] %>
                    </td>
                    <td>
                        <%=row["VolY2"] %>
                    </td>
                </tr>
                <% } %>
                <% if (activitiesRows.Length == 0)
                   { %>
                <tr>
                    <td colspan="16"  class="emptyrow">Нет данных</td>
                </tr>
                <% } %>
            </tbody>
        </table>
    </div>
    <br />

    <% foreach (PlansActivity activity in activitiesAll.OrderBy(x => !x.IsPriority).ThenBy(x => x.Num))
       {%>
    <div style="margin-top: 80px;">
        <h1 style="margin-bottom: 30px;"><a name="<%= activity.Id %>"><%= activity.Code %> <%= activity.Name %></a></h1>
        <%= GetActivityReadMode(activity.Id) %>
    </div>

    <br />
    <span class="arrow">&uarr;&nbsp;</span><a style="font-size: 1.2em" href="#Acitivities">К списку мероприятий</a>
    <%
       } %>
</body>
</html>
