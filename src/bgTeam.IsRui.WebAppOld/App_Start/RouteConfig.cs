﻿using System.Web.Mvc;
using System.Web.Routing;

namespace bgTeam.IsRui.WebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.html/{*pathInfo}");

            //routes.IgnoreRoute("files/{*pathInfo}");
            //routes.IgnoreRoute("image/{*pathInfo}");
            //routes.IgnoreRoute("video/{*pathInfo}");

            routes.MapRoute("Login", "login", new { controller = "Account", action = "Login" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Index", id = UrlParameter.Optional });
        }
    }
}