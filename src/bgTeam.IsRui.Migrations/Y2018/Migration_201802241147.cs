﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201802241148)]
    public class Migration_201802241147 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("OU_Article")
                .WithColumn("OU_Article_ID").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("OU_Article_Name").AsString().NotNullable()

                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Execute.Sql("INSERT INTO OU_Article (OU_Article_ID, OU_Article_Name) VALUES (0, N'Не задано')");

            Create.Column("OU_Article_ID").OnTable("OU").AsInt32().Nullable();

            Execute.Sql("UPDATE OU SET OU_Article_ID = 0");

            Alter.Column("OU_Article_ID").OnTable("OU").AsInt32().NotNullable();

            Create.ForeignKey("FK_OU_OU_Article")
               .FromTable("OU").ForeignColumn("OU_Article_ID")
               .ToTable("OU_Article").PrimaryColumn("OU_Article_ID");
        }
    }
}
