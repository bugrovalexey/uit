﻿using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.DataAccess.Repository;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport
{
    [MetadataType(typeof (ManufacturerViewModel_Valid))]
    public class ManufacturerViewModel
    {
        private const string ManufacturerCaption = "Производитель";
        private const string NotListedCaption = "Отсутствует в списке";
        private const string ManufacturerNameCaption = "Наименование производителя";

        public ManufacturerViewModel()
        {
            Manufacturers = new SelectList(
                new RepositoryDc<Manufacturer>().GetAll(),
                "Id", "Name");
        }

        [DisplayName(ManufacturerCaption)]
        public int? ManufacturerId { get; set; }

        [DisplayName(ManufacturerNameCaption)]
        public string ManufacturerName { get; set; }

        [DisplayName(NotListedCaption)]
        public bool NotListed { get; set; }

        public SelectList Manufacturers { get; set; }
    }

    internal class ManufacturerViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = NotZeroValueText)]
        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int? ManufacturerId { get; set; }

    }

}