﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.ExternalSystems.Zakupki360.Dto
{
    class ResponseGetConracts
    {
        public string Error { get; set; }
        public IEnumerable<ContractInfo> Contracts { get; set; }
    }
}
