﻿using GB.Data.Common;

namespace GB._Mapping.Common
{
    internal class ResponsibleMap : ClassMapHiLo<Responsible>
    {
        public ResponsibleMap()
            : base("Responsible")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Responsible_FIO");
            //Map(x => x.Job, "Responsible_Job");
            Map(x => x.Phone, "Responsible_Phone");
            Map(x => x.Email, "Responsible_Email");
            Map(x => x.IsActive, "Responsible_Is_Active");

            References<Department>(x => x.Department, "Govt_Organ_ID");
            References<ResponsibleJob>(x => x.Job, "Responsible_Job_ID");
        }
    }

    internal class ResponsibleJobMap : ClassMapHiLo<ResponsibleJob>
    {
        public ResponsibleJobMap()
            : base("Responsible_Job")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Responsible_Job_Name");
        }
    }
}