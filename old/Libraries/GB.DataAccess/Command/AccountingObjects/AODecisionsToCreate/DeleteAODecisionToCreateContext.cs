﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AODecisionsToCreate
{
    public class DeleteAODecisionToCreateContext : ICommandContext
    {
        public int? xId { get; set; }
    }
}
