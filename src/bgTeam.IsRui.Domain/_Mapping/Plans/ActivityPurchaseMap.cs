﻿namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class ActivityPurchaseMap : ClassMapHiLo<ActivityPurchase>
    {
        public ActivityPurchaseMap()
            : base("ActivityPurchase")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.AcceptanceEndDate, "AP_AcceptanceEndDate").Nullable();
            Map(x => x.Month, "AP_Month").Nullable();
            Map(x => x.Number, "AP_Number").Nullable();
            Map(x => x.PublishDate, "AP_PublishDate").Nullable();
            Map(x => x.ResultDate, "AP_ResultDate").Nullable();
            Map(x => x.TimeStamp, "AP_TimeStamp").Not.Nullable();

            References(x => x.Activity, "Plans_Activity_ID").Not.Nullable();
        }
    }
}
