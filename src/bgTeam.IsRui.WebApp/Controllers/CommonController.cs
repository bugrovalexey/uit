﻿namespace bgTeam.IsRui.WebApp.Controllers
{
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Story.Activities;
    using bgTeam.IsRui.Story.Common;
    using bgTeam.IsRui.WebApp.Model;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [Authorize]
    [Route("api/[controller]/[action]")]
    public class CommonController : Controller
    {
        private readonly IStoryBuilder _builder;

        public CommonController(IStoryBuilder builder)
        {
            _builder = builder;
        }

        /// <summary>
        /// Загрузить данные для справочника
        /// </summary>
        /// <param name="context">
        /// - Name наименование справочника
        /// - Search строка поиска
        /// </param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IEnumerable<DictionaryDto>> GetDictionary(GetDictionaryStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<DictionaryDto>>();
        }

        /// <summary>
        /// Получить иерархию подразделений
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<IEnumerable<DepartmentDtoProc>> GetDepartmentsAllChildrens([FromBody] GetDepartmentsAllChildrensByIdsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<DepartmentDtoProc>>();
        }
    }
}
