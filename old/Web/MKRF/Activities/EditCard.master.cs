﻿using GB;
using GB.Helpers;
using GB.MKRF.Entities.Plans;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.Controls;

namespace GB.MKRF.Web.Activities
{
    public partial class EditCard : System.Web.UI.MasterPage
    {
        public const string ActivityIdKey = "ActivityId";

        private PlansActivity _activity;


        public int? ActivityId
        {
            get
            {
                int activityId;

                if (int.TryParse(Request.QueryString[ActivityIdKey], out activityId))
                {
                    return activityId;
                }

                return null;
            }
        }


        public PlansActivity CurrentActivity
        {
            get
            {
                if (_activity == null)
                {
                    try
                    {
                        _activity = RepositoryBase<PlansActivity>.Get(ActivityId);

                        if(_activity == null)
                            ResponseHelper.DenyAccess(this.Response);
                    }
                    catch (Exception exp)
                    {
                        Logger.Fatal("Ошибка получения CurrentActivity", exp);
                        ResponseHelper.Error(this.Response);
                    }
                }

                return _activity;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (string.IsNullOrEmpty(Request.QueryString[ActivityIdKey]))
                ResponseHelper.Error(Response);

            base.OnInit(e);
        }


        protected override void OnPreRender(EventArgs e)
        {
            IList<string> urlParameters = new List<string>();

            urlParameters.Clear();
            urlParameters.Add(ActivityIdKey + "=" + Request.QueryString[ActivityIdKey]);
            
            // Настройка ссылок
            foreach (DevExpress.Web.ASPxTabControl.Tab t in tc.GetVisibleTabs())
            {
                t.Visible = GB.MKRF.Validation.Validator.UserAccessToPage(t.NavigateUrl);

                t.NavigateUrl += (t.NavigateUrl.Contains("?") ? "&" : "?") + string.Join("&", urlParameters);
            }
            
            ActivityLabel.DataBind();

            base.OnPreRender(e);
        }


        protected void ActivityLabel_DataBinding(object sender, EventArgs e)
        {
            (sender as ASPxLabel).Text = CurrentActivity.Name;
        }

    }
}