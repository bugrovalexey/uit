﻿using System;
using GB.Data.Plans;
using GB.DataAccess.Repository;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class ActivityCommentService : ServiceBase<ActivityComment>, IService<ActivityComment, ActivityCommentArgs>
    {
        public ActivityComment Create(ActivityCommentArgs args)
        {
            var ac = new ActivityComment(new Repository<PlansActivity>().Get(args.Activity));

            ac.Text = args.Text;
            ac.Page = args.Page;
            ac.User = UserRepository.GetCurrent();

            return base.SaveOrUpdate(ac);
        }

        public ActivityComment Update(ActivityCommentArgs args)
        {
            throw new NotImplementedException();
        }

        public void Delete(ActivityCommentArgs args)
        {
            throw new NotImplementedException();
        }
    }

    public class ActivityCommentArgs : IServiceArgs
    {
        public int Activity { get; set; }

        public string Text { get; set; }

        public string Page { get; set; }
    }
}