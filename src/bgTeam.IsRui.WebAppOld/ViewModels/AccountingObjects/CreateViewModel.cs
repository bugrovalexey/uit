﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    [MetadataType(typeof(CreateViewModel_Valid))]
    public class CreateViewModel
    {
        [DisplayName("Краткое наименование ОУ")]
        public string ShortName { get; set; }
    }

    public class CreateViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string ShortName { get; set; }
    }
}