﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
    public class LanMap : ClassMapBase<Lan>
    {
        public LanMap()
            : base("PRS_8_2")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_2_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_8_2_1 = ? 
//                          ,@PRS_8_2_2 = ? 
//                          ,@PRS_8_2_3 = ? 
//                          ,@PRS_8_2_4 = ? 
//                          ,@PRS_8_2_5 = ? 
//                          ,@PRS_8_2_6 = ? 
//                          ,@PRS_8_2_7 = ? 
//                          ,@PRS_8_2_8 = ? 
//                          ,@PRS_8_2_9 = ? 
//                          ,@PRS_8_2_10 = ? 
//                          ,@PRS_8_2_11 = ? 
//                          ,@Plans_ID = ? 
//						  ,@PRS_8_2_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_2_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_2_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_2_Delete @PRS_8_2_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.UnitCount, "PRS_8_2_1");
			Map(x => x.AvgSpeed, "PRS_8_2_2");
			Map(x => x.InternetSpeed, "PRS_8_2_3");
			Map(x => x.CountSpeed_100, "PRS_8_2_4");
			Map(x => x.CountSpeed_10_100, "PRS_8_2_5");
			Map(x => x.CountSpeed_1_10, "PRS_8_2_6");
			Map(x => x.CountSpeed_1, "PRS_8_2_7");
			Map(x => x.CountSpeed_0, "PRS_8_2_8");
			Map(x => x.CountPersAccess, "PRS_8_2_9");
			Map(x => x.CountSharedAccess, "PRS_8_2_10");
			Map(x => x.CountHardware, "PRS_8_2_11");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
        }
    }
}