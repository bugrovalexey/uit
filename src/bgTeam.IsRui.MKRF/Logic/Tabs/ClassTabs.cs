﻿namespace bgTeam.IsRui.MKRF.Logic.Tabs
{
    /// <summary>
    /// Вкладки главного меню
    /// </summary>
    public class MainTabs
    {
    }

    /// <summary>
    /// Вкладки раздела мероприятия
    /// </summary>
    public class ActivityTabs
    {
    }

    /// <summary>
    /// Вкладки карточки мероприятия
    /// </summary>
    public class ActivityCardTabs
    {
    }

    /// <summary>
    /// Вкладки раздела ОУ
    /// </summary>
    public class AccountingObjectsTabs
    {
    }

    /// <summary>
    /// Аналитика
    /// </summary>
    public class AnalyticsTabs
    {
    }

    /// <summary>
    /// Вкладки карточки ОУ
    /// </summary>
    public class AccountingObjectsCardTabs
    {
    }
}