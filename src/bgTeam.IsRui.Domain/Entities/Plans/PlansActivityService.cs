﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain.Entities.Plans
{
    /// <summary>
    /// Услуги плана
    /// </summary>
    public class PlansActivityService : EntityBase
    {
        protected PlansActivityService()
        {
        }

        public PlansActivityService(PlansActivity pa)
        {
            Activity = pa;
        }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Год</summary>
        public virtual YearEnum Year { get; set; }

        /// <summary>Количество</summary>
        public virtual int? Count { get; set; }

        /// <summary>Стоимость</summary>
        public virtual double Cost { get; set; }

        /// <summary>БК КОСГУ </summary>
        public virtual ExpenditureItem KOSGU { get; set; }

        /// <summary>Мероприятие</summary>
        public virtual PlansActivity Activity { get; set; }
    }
}