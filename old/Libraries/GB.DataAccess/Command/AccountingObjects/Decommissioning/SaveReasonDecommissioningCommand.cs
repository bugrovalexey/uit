﻿using AutoMapper;
using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.Documents;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;

namespace GB.DataAccess.Command.AccountingObjects.Decommissioning
{
    /// <summary>
    /// Сохранить причину вывода из эксплуатации
    /// </summary>
    public class SaveReasonDecommissioningCommand : ICommand<SaveReasonDecommissioningContext>
    {
        ISessionFactoryGb factory;

        public SaveReasonDecommissioningCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(SaveReasonDecommissioningContext context)
        {
            var entity = new Repository<AccountingObject>().GetExisting(context.xId);

            entity.DecommissioningReasons = context.Reason;

            using (var session = factory.Create())
            {
                session.Update(entity);
                session.Commit();
            }
        }
    }
}
