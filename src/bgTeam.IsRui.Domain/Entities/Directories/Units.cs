﻿namespace bgTeam.IsRui.Domain.Entities.Directories
{
    /// <summary>
    /// Единицы измерения
    /// </summary>
    public class Units : EntityDirectories
    {
        /// <summary>Сокращение</summary>
        public virtual string Abbreviation { get; set; }

        public virtual string FullName { get { return string.Concat(Name, " ", Abbreviation); } }
    }
}