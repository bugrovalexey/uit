﻿using System.Collections.Generic;
using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Entities.Plans.ActivityOffers
{
    /// <summary>
    /// Предложение по мероприятиям
    /// </summary>
    public class ActivityOffer : NumerableEntity
    {
        protected ActivityOffer()
        {
        }

        public ActivityOffer(Plan plan)
        {
            this.Plan = plan;
        }

        /// <summary>План</summary>
        public virtual Plan Plan { get; protected set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Плановый год оканчания</summary>
        public virtual short YearEnd { get; set; }

        /// <summary>Ответственный</summary>
        public virtual User Responsible { get; set; }

        /// <summary>Подпрограммы государственной программы "Информационное общество"</summary>
        public virtual string Subprograms { get; set; }

        /// <summary>Перечень субъектов РФ, участвующих в реализации мероприятия</summary>
        public virtual string Subjects  { get; set; }

        /// <summary>
        /// Фонд алгоритмов и программ
        /// </summary>
        public virtual IList<ActivityOfferApf> OfferApf { get; set; }

        /// <summary>
        /// Документ-основание реализации мероприятия
        /// </summary>
        public virtual IList<ActivityOfferDoc> OfferDoc { get; set; }

        /// <summary>
        /// Функции и полномочия гос. органа
        /// </summary>
        public virtual IList<ActivityOfferFunction> OfferFunction { get; set; }

        /// <summary>
        /// ИС И ИКТ-компоненты
        /// </summary>
        public virtual IList<ActivityOfferIS> OfferIS { get; set; }

        /// <summary>
        /// Совместное использование ИС и ИКТ-компонентов
        /// </summary>
        public virtual IList<ActivityOfferSharingIS> OfferSharingIS { get; set; }
    }
}
