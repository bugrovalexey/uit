﻿using System.Linq;
using System.Management.Instrumentation;
using GB.Data;
using GB.Data.AccountingObjects.WorksAndGoods;
using GB.Data.Directories;
using GB.DataAccess.Services.Args;
using GB.DataAccess.Command.AccountingObjects.CharacteristicOfTechnicalSupports;


namespace GB.DataAccess.Services
{
    public class TechnicalSupportService : ServiceBase<TechnicalSupport>, IService<TechnicalSupport, TechnicalSupportArgs>
    {
        public TechnicalSupport Create(TechnicalSupportArgs args)
        {
            var entity = new TechnicalSupport();
            var result = SaveOrUpdate(entity, args);            

            new SetTechnicalSupportForCharacteristicsCommand().Execute(new SetTechnicalSupportForCharacteristicsContext { CharacteristicsOfTechnicalSupportIds = args.CharacteristicsOfTechnicalSupportIds,
                                                                                                                          TechnicalSupport = result    });

            args.Id = result.Id;
            new WorksAndGoodsService().Create(args);

            return result;
        }

        public TechnicalSupport Update(TechnicalSupportArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            var result = SaveOrUpdate(entity, args);

            new WorksAndGoodsService().Update(args);

            return result;
        }

        private TechnicalSupport SaveOrUpdate(TechnicalSupport entity, TechnicalSupportArgs args)
        {
            Manufacturer man;

            if (args.ManufacturerId.HasValue == false && string.IsNullOrWhiteSpace(args.ManufacturerName) == false)
            {
                var мanufacturerName = args.ManufacturerName.Trim();
                man =
                    new Repository.Repository<Manufacturer>().GetAllEx(
                        q => q.Where(x => x.Name == мanufacturerName)).FirstOrDefault();

                if (man == null)
                {
                    man = new CrudService<Manufacturer, EntityDirectoriesArgs>().Create(new EntityDirectoriesArgs()
                    {
                        Name = мanufacturerName
                    });
                }

                ConvertDtoToEntity.FillTechnicalSupportFromArgs(entity, args);
                entity.Manufacturer = man;
            }
            else
            {
                ConvertDtoToEntity.FillTechnicalSupportFromArgs(entity, args);                
            }

            SaveOrUpdate(entity);

            return entity;
        }

        public void Delete(TechnicalSupportArgs args)
        {
            Delete(args.Id);
            new WorksAndGoodsService().Delete(args);
        }
    }
}
