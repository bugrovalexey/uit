﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    internal class CharacteristicOfTechnicalSupportMap : ClassMapHiLo<CharacteristicOfTechnicalSupport>
    {
        public CharacteristicOfTechnicalSupportMap()
            : base("Charact_TO")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Charact_TO_Name").Not.Nullable();
            References(x => x.Unit, "MUnit_ID").Not.Nullable();
            References(x => x.TechnicalSupport, "OU_Technical_ID");
            Map(x => x.Value, "Charact_TO_Value");
        }
    }
}