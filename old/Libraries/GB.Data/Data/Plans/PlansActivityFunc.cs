﻿//using GB.Entity.Directories.FPGU;
using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Entities.Plans
{
    /// <summary>
    /// Функции и полномочия
    /// </summary>
    public class PlansActivityFunc : EntityBase
    {
        protected PlansActivityFunc()
        {
        }

        public PlansActivityFunc(PlansActivity Activity)
        {
            this.Activity = Activity;
        }

        /// <summary>Мероприятие плана</summary>
        public virtual PlansActivity Activity { get; protected set; }

        /// <summary>Название</summary>
        public virtual string Name { get; set; }

        /// <summary> Пункт, статья документа </summary>
        public virtual string Comment { get; set; }

        /// <summary>Вид</summary>
        public virtual FuncType Type { get; set; }

        /// <summary>НПА</summary>
        public virtual NPA NPA { get; set; }

        /// <summary> Ссылка на госуслугу </summary>
        //public virtual Gov_Service Service { get; set; }

        /// <summary> Ссылка на НПА документ </summary>
        //public virtual NPA_Document Document { get; set; }

        /// <summary>
        /// Мероприятие направлено на эксплуатацию информационной системы
        /// </summary>
        public virtual bool DirectedToExpl { get; set; }

        /// <summary>
        /// казание государственной услуги (выполнение государственной функции) не осуществлялось до момента формирования мероприятия
        /// </summary>
        public virtual bool IsUsedBefore { get; set; }

        public virtual TypicalActivities TypicalActivity { get; set; }

        /// <summary> Флаг, что данная ф-ия отсутствует в реестре гос услуг </summary>
        public virtual bool IsMissingFunc { get; set; }

        /// <summary> Ф-ия </summary>
        public virtual string MissingFuncName { get; set; }
    }
}