﻿using GB.Data.AccountingObjects;
using GB.Data.Workflow;
using GB.DataAccess.Command.AccountingObjects.Workflow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Logic.Status.Handler
{
    class AccountingObjectWorkflowHandler : WorkflowHandlerBase<AccountingObject>
    {
        protected override bool CheckNewStatus(AccountingObject @object, Data.Workflow.Status status, out string errorMessage, out string confirmMessage)
        {
            errorMessage = null;
            confirmMessage = null;

            return true;
        }

        public override void ChangeAfter(AccountingObject obj)
        {
            StatusEnum status = (StatusEnum)obj.Status.Id;

            switch (status)
            {
                case StatusEnum.AoDraft:
                    break;
                case StatusEnum.AoAgreed:
                    break;
                case StatusEnum.AoOnCompletion:
                    break;
                case StatusEnum.AoAccordHigher:
                    //if (obj.Parent == null)
                    //{
                    //    var command = new CreateMultiCommand();
                    //    command.Execute(new CreateMultiContext() { AO = obj });
                    //}
                    break;
                default:
                    throw new Exception("Пришел не обрабатываемый статус");
            }

            var commandUpdate = new UpdateCommentStatusCommand();
            commandUpdate.Execute(new UpdateCommentStatusContext() { AO = obj });
        }
    }
}
