﻿using bgTeam.IsRui.Domain.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.WebApp.Admin.Models
{
    public class GetAllUsersViewModel
    {
        public IEnumerable<UserViewModel> UsersList { get; set; }

        public GetAllUsersViewModel(IEnumerable<UsersDto> users)
        {
            UsersList = users.Select(x => new UserViewModel(x));
        }
    }
}
