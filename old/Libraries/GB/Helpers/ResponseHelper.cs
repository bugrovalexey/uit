﻿using System;
using System.Web;

namespace GB.Helpers
{
    public class ResponseHelper
    {
        /// <summary>
        /// Запрет доступа( прекращается построение страницы, клиенту шлется 403 ошибка)
        /// </summary>
        /// <param name="response"></param>
        public static void DenyAccessToPage(HttpResponseBase response)
        {
            response.Redirect("~/Error/AccessToPageError");
        }

        public static void DenyAccessToPage(HttpResponse response)
        {
            response.Redirect("~/Error/AccessToPageError");
        }

        /// <summary>
        /// Запрет доступа( прекращается построение страницы, клиенту шлется 403 ошибка)
        /// </summary>
        /// <param name="response"></param>
        public static void DenyAccessToObject(HttpResponseBase response)
        {
            response.Redirect("~/Error/AccessToObjectError");
        }

        public static void Error(HttpResponse response)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Отсутствие пользователя
        /// </summary>
        /// <param name="response"></param>
        public static void IncorrectUser(HttpResponse response)
        {
            throw new NotImplementedException();
        }

        public static void FileNotFound(HttpResponse response, string LogMessage = null)
        {
            throw new NotImplementedException();
        }

        public static void AccessDenied(HttpResponse response, string LogMessage = null)
        {
            EndResponse(response, 403, "403 Access Denied", LogMessage);
        }

        public static void EndResponse(HttpResponse response, int StatusCode, string Status, string LogMessage = null)
        {
            if (!string.IsNullOrWhiteSpace(LogMessage))
            {
                Logger.Error(LogMessage);
            }
            response.StatusCode = StatusCode;
            response.Status = Status;
            //response.TrySkipIisCustomErrors = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            //response.End();
        }
    }
}