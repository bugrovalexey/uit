﻿using GB.Data.Directories;
using GB.Data.Plans;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    public class PlansSpecCharacteristicsService : ServiceBase<PlansSpecCharacteristic>, IService<PlansSpecCharacteristic, PlansSpecCharacteristicArgs>
    {
        public PlansSpecCharacteristic Create(PlansSpecCharacteristicArgs args)
        {
            var product = args.Owner ?? GetEntity<PlansSpec>(args.OwnerId);
            PlansSpecCharacteristic entity = new PlansSpecCharacteristic { Owner = product };

            return SaveOrUpdate(entity, args);
        }

        public PlansSpecCharacteristic Update(PlansSpecCharacteristicArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            return SaveOrUpdate(entity, args);
        }

        private PlansSpecCharacteristic SaveOrUpdate(PlansSpecCharacteristic entity, PlansSpecCharacteristicArgs args)
        {
            entity.Name = args.Name;
            entity.Unit = GetEntity<Units>(args.Unit);
            entity.Value = args.Value;

            base.SaveOrUpdate(entity);

            return entity;
        }

        public void Delete(PlansSpecCharacteristicArgs args)
        {
            base.Delete(args.Id);
        }
    }

    public class PlansSpecCharacteristicArgs : BaseArgsNew
    {
        public string Name { get; set; }

        public int Unit { get; set; }

        public int? Value { get; set; }

        public PlansSpec Owner { get; set; }

        public int OwnerId { get; set; }
    }
}