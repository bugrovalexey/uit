﻿using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Documents;

namespace GB.MKRF.Mapping.Documents
{
    class DocumentAccessMap : ClassMapBase<DocumentAccess>
    {
        public DocumentAccessMap()
            : base("Doc_Role_Access")
        {
        }

        protected override void InitMap()
        {
            References<Role>(x => x.Role, "Role_ID").Nullable();
            References<Document>(x => x.Document, "Document_ID").Nullable();
        }
    }
}