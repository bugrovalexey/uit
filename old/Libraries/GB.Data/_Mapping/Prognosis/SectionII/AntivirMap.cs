﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Prognosis.SectionII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionII
{
	public class AntivirMap : ClassMapBase<Antivir>
	{
        public AntivirMap()
            : base("MF2_Antivir")
        {
//            Table("w_");

//            Id(x => x.Id, "MF2_Antivir_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF2_Antivir_Insert
//                          @MF2_Antivir_CA = ?
//                         ,@MF2_Antivir_TP = ?
//                         ,@MF2_Antivir_Comm = ?
//                         ,@MF2_ID = ?
//                         ,@Antivir_Type_ID = ?
//                         ,@MF2_Antivir_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF2_Antivir_Update
//                          @MF2_Antivir_CA = ?
//                         ,@MF2_Antivir_TP = ?
//                         ,@MF2_Antivir_Comm = ?
//                         ,@MF2_ID = ?
//                         ,@Antivir_Type_ID = ?
//                         ,@MF2_Antivir_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF2_Antivir_Delete
//						  @MF2_Antivir_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.CountCA, "MF2_Antivir_CA");
            Map(x => x.CountTP, "MF2_Antivir_TP");
            Map(x => x.Comment, "MF2_Antivir_Comm");

            References<TechInfo>(x => x.TechInfo, "MF2_ID").Not.Nullable();
			References<AntivirType>(x => x.AntivirType, "Antivir_Type_ID").Not.Nullable();
        }
    }
}