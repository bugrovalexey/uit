﻿using bgTeam.IsRui.Domain.Entities.Documents;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects.Documents
{
    /// <summary>
    /// Основания для создания ОУ 
    /// </summary>
    public class AODocumentGroundsForCreation : AODocumentBase
    {
        internal static readonly DocTypeEnum DocType = DocTypeEnum.GroundsForCreationAccountingObject;    

        protected override DocTypeEnum GetTypeDocument()
        {
            return DocType;
        }
    }
}
