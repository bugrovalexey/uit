﻿using System;
using System.Collections.Generic;
using bgTeam.IsRui.Domain.Entities.Documents;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects.Documents
{
    /// <summary>
    /// Документ ОУ
    /// </summary>
    public abstract class AODocumentBase : EntityBase
    {

        public AODocumentBase()
        {
            Type = GetTypeDocument();
            Documents = new List<Files>();
        }

        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        /// Дата принятия документа
        /// </summary>
        public virtual DateTime? DateAdoption { get; set; }

        /// <summary>
        /// Пункт, статья документа
        /// </summary>
        public virtual string Paragraph { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public virtual DocTypeEnum Type { get; protected set; }

        /// <summary>
        /// Документ 
        /// </summary>
        public virtual IList<Files> Documents { get; set; }

        /// <summary>
        /// Получение типа документа
        /// </summary>
        /// <returns></returns>
        protected abstract DocTypeEnum GetTypeDocument();
    }
}
