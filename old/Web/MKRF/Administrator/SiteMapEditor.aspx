﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="SiteMapEditor.aspx.cs" Inherits="GB.MKRF.Web.Administrator.SiteMapEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function hfSave(e) {
            hf.Set('Title', TextBox_Title.GetText());
            hf.Set('Url', TextBox_Url.GetText());
            hf.Set('Parent', ParentComboBox.GetValue());
            hf.Set('Visible', Visible_CheckBox.GetChecked());
            hf.Set('Order', OrderNumberEdit.GetValue());
        }

        function hfSaveGrid(e) {
            hfg.Set('Role', Role_ComboBox.GetValue());
        }
    </script>
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
    <dxhf:ASPxHiddenField runat="server" ID="hfg" ClientInstanceName="hfg" />
    <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
        Modal="True" ContainerElementID="panel">
    </dxlp:ASPxLoadingPanel>
    <asp:Panel runat="server" ID="panel">
    <table>
        <tr>
            <td>
                <table width="100%" style="padding: 10px">
                    <tr>
                        <td colspan="2">
                            <dxe:ASPxButton ID="AddParentButton" runat="server" AutoPostBack="false"
                                Text="Добавить в корень" Width="160px" >
                                <ClientSideEvents Click="function(s,e) { hf.Set('Root', true); TreeList.StartEditNewNode(); }" />
                            </dxe:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dxe:ASPxButton ID="AddChildButton" runat="server" AutoPostBack="false"
                                Text="Добавить дочерний" Width="160px" >
                                <ClientSideEvents Click="function(s,e) { hf.Set('Root', false); TreeList.StartEditNewNode(TreeList.GetFocusedNodeKey()); }" />
                            </dxe:ASPxButton>
                        </td>
                        <td align="right">
                            <dxe:ASPxButton ID="UpdateButton" runat="server" AutoPostBack="false" Text="Обновить меню" Width="160px">
                                <ClientSideEvents Click="function(s,e) {  LoadingPanel.Show();  callBack.PerformCallback(''); }" />
                            </dxe:ASPxButton>
                            <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" oncallback="callBack_Callback" >
                                <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
                            </dxc:ASPxCallback>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="bottom">
                <table width="100%" style="padding: 10px">
                    <tr>
                        <td>
                            <dxe:ASPxButton ID="_AddGrid_Button" runat="server" ClientInstanceName="Add_Button"
                                AutoPostBack="false" Text="Добавить" >
                                <ClientSideEvents Click="function(s,e) { Grid.AddNewRow(); }" />
                            </dxe:ASPxButton>
                        </td>
                        <td width="10px"/>
                        <td>
                            <dxe:ASPxButton ID="_CopyRole_Button" runat="server" ClientInstanceName="CopyRole_Button"
                                Text="Скопировать дочерним" OnClick="_CopyRole_Button_Click">
                            </dxe:ASPxButton>
                        </td>
                    </tr>
                    </table>
            </td>
        </tr>
        <tr>
            <td>
                <dxwtl:ASPxTreeList ID="_TreeList" ClientInstanceName="TreeList" runat="server" OnDataBinding="_TreeList_DataBinding"
                    KeyFieldName="ID" ParentFieldName="ParentID" AutoGenerateColumns="False"
                    OnInitNewNode="_TreeList_InitNewNode"
                    OnNodeDeleting="_TreeList_NodeDeleting" 
                    OnNodeInserting="_TreeList_NodeInserting"
                    OnNodeUpdating="_TreeList_NodeUpdating" 
                    OnStartNodeEditing="_TreeList_StartNodeEditing">
                    <Columns>
                        <dxwtl:TreeListTextColumn FieldName="Id" VisibleIndex="0" Visible="false" />
                        <dxwtl:TreeListTextColumn FieldName="ParentId" VisibleIndex="1" Visible="false" />
                        <dxwtl:TreeListTextColumn FieldName="Title" VisibleIndex="2" />
                        <dxwtl:TreeListTextColumn FieldName="Url" VisibleIndex="3" />
                        <dxwtl:TreeListCheckColumn FieldName="Visible" VisibleIndex="4" />
                        <dxwtl:TreeListTextColumn FieldName="OrderIndex" VisibleIndex="5" />
                        <dxwtl:TreeListCommandColumn Caption=" " Width="150px">
                            <EditButton Visible="true" />
                            <DeleteButton Visible="true" />
                        </dxwtl:TreeListCommandColumn>
                    </Columns>
                    <SettingsBehavior AllowFocusedNode="True" />
                    <SettingsEditing Mode="EditForm" />
                    <Templates>
                        <EditForm>
                            <table>
                                <tr>
                                    <td class="userCapt">
                                        Наименование:
                                    <td>
                                        <dxe:ASPxTextBox ID="_TextBox_Title" runat="server" ClientInstanceName="TextBox_Title"
                                            Width="700px" OnDataBinding="_TextBox_Title_DataBinding">
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td class="userCapt">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="userCapt">
                                        Url:
                                    <td>
                                        <dxe:ASPxTextBox ID="_TextBox_Url" runat="server" ClientInstanceName="TextBox_Url"
                                            Width="700px" OnDataBinding="_TextBox_Url_DataBinding">
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td class="userCapt">
                                    </td>
                                </tr>
                                <%--<%if (!isNewNode) {%>--%>
                                <tr>
                                    <td class="userCapt">
                                        Родитель:
                                    </td>
                                    <td>
                                        <dxe:ASPxComboBox ID="ParentComboBox" runat="server" 
                                            ClientInstanceName="ParentComboBox" ondatabinding="ParentComboBox_DataBinding" 
                                            TextField="Title" ValueField="Id" ValueType="System.Int32" Width="700px">
                                            <Columns>
                                                <dxe:ListBoxColumn Caption="Id" FieldName="Id" />
                                                <dxe:ListBoxColumn Caption="Название" FieldName="Title" />
                                                <dxe:ListBoxColumn Caption="Url" FieldName="Url" />
                                            </Columns>
                                        </dxe:ASPxComboBox>
                                    </td>
                                    <td />
                                </tr>
                                <%--<%} %>--%>
                                <tr>
                                    <td class="userCapt">
                                        Показывать в меню:
                                        <td>
                                            <dxe:ASPxCheckBox ID="_Visible_CheckBox" runat="server" 
                                                ClientInstanceName="Visible_CheckBox" 
                                                OnDataBinding="_Visible_CheckBox_DataBinding" Width="700px">
                                            </dxe:ASPxCheckBox>
                                        </td>
                                        <td class="userCapt">
                                        </td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Сортировка:
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="OrderNumberEdit" runat="server" ClientInstanceName="OrderNumberEdit"
                                            Width="100%" DisplayFormatString="N0" MaxValue="999999999999" 
                                            AllowMouseWheel="False" NumberType="Integer" 
                                            ondatabinding="OrderNumberEdit_DataBinding" >
                                            <SpinButtons ShowIncrementButtons="False">
                                            </SpinButtons>
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 98%">
                                                    &nbsp;
                                                </td>
                                                <td style="width: 1%">
                                                    <dxwtl:ASPxTreeListTemplateReplacement ID="ASPxTreeListTemplateReplacement1" 
                                                        runat="server" ReplacementType="UpdateButton" />
                                                </td>
                                                <td style="width: 1%">
                                                    <dxwtl:ASPxTreeListTemplateReplacement ID="ASPxTreeListTemplateReplacement2" 
                                                        runat="server" ReplacementType="CancelButton" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="userCapt">
                                    </td>
                                </tr>
                            </table>
                        </EditForm>
                    </Templates>
                    <Border BorderStyle="Solid" />
                    <ClientSideEvents 
                        BeginCallback="function(s,e) { if (e.command=='UpdateEdit') hfSave(e); }" 
                        FocusedNodeChanged="function(s, e) { Grid.Refresh(); }" />
                </dxwtl:ASPxTreeList>
            </td>
            <td valign="top" >
                <dxwgv:ASPxGridView runat="server" ID="_Grid" ClientInstanceName="Grid" KeyFieldName="Id" Width="100%"
                    OnDataBinding="Grid_DataBinding" 
                    OnRowDeleting="_Grid_RowDeleting" 
                    OnRowInserting="_Grid_RowInserting" >
                    <SettingsPager Mode="ShowAllRecords" >
                    </SettingsPager>
                    <Columns>
                        <dxwgv:GridViewDataTextColumn FieldName="Id" VisibleIndex="0" Visible="false" />
                        <dxwgv:GridViewDataTextColumn Caption="Роль" FieldName="RoleName" VisibleIndex="1" Width="200px" />
                        <dxwgv:GridViewCommandColumn Caption=" " VisibleIndex="2" Width="100px">
                            <DeleteButton  Visible="True" />
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <Templates>
                        <EditForm>
                            <table>
                                <tr>
                                    <td class="userCapt">
                                        Роль:
                                    <td>
                                        <dxe:ASPxComboBox ID="_Role_ComboBox" runat="server" ClientInstanceName="Role_ComboBox"
                                            Width="150px" ValueField="Id" TextField="Name"
                                            ondatabinding="_Role_ComboBox_DataBinding" >
                                        </dxe:ASPxComboBox>
                                    </td>
                                    <td class="userCapt">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 98%">
                                                    &nbsp;
                                                </td>
                                                <td style="width: 1%"  class="form_edit_buttons" >
                                                    <span class="ok">
                                                        <dxwgv:ASPxGridViewTemplateReplacement ID="ASPxGridViewTemplateReplacement1" runat="server"
                                                            ReplacementType="EditFormUpdateButton">
                                                        </dxwgv:ASPxGridViewTemplateReplacement>
                                                    </span>
                                                </td>
                                                <td style="width: 1%"  class="form_edit_buttons" >
                                                    <span class="cancel">
                                                        <dxwgv:ASPxGridViewTemplateReplacement ID="ASPxGridViewTemplateReplacement2" runat="server"
                                                            ReplacementType="EditFormCancelButton">
                                                        </dxwgv:ASPxGridViewTemplateReplacement>
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="userCapt">
                                    </td>
                                </tr>
                            </table>
                        </EditForm>
                    </Templates>
                    <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') hfSaveGrid(e); }" />
                </dxwgv:ASPxGridView>
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
