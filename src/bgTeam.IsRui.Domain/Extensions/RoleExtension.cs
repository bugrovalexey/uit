﻿using bgTeam.IsRui.Domain.Entities.Common;
using System.Collections.Generic;
using System.Linq;

namespace bgTeam.IsRui.Domain.Extensions
{
    public static class RoleExtension
    {
        public static bool IsEq(this RoleEnum e, IList<Role> r)
        {
            return r.Any(x => x.Id == (int)e);
        }

        public static bool IsEq(this IList<Role> r, RoleEnum e)
        {
            return r.Any(x => x.Id == (int)e);
        }
    }
}