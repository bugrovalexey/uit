﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ParticipantMap : ClassMapBase<Participant>
	{
        public ParticipantMap()
            : base("Participant")
		{
//            Table("w_");

//            Id(x => x.Id, "Participant_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            SqlInsert(@"exec ZZZ_Prc_Participant_Insert
//						  @Participant_Contact = ?
//						 ,@Participant_Name = ?
//						 ,@Participant_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Participant_Update
//						  @Participant_Contact = ?
//						 ,@Participant_Name = ?
//						 ,@Participant_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Participant_Delete
//						  @Participant_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Contact, "Participant_Contact").Not.Nullable();
			Map(x => x.Name, "Participant_Name").Not.Nullable();
        }
    }
}
