﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.PlanReport.SectionV;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionV
{
    public class InfoCancelingMap : ClassMapBase<InfoCanceling>
	{
        public InfoCancelingMap()
            : base("PRS_5g")
		{
//            Table("w_");

//            Id(x => x.Id, "PRS_5g_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string args = @" @PRS_5g_Num = ? 
//							,@PRS_5g_Func = ? 
//							,@PRS_5g_IS = ? 
//							,@PRS_5g_Task = ? 
//							,@PRS_5g_Reason = ? 
//							,@Plans_ID = ? 
//							,@FGIS_Info_ID = ? 
//							,@PRS_5g_ID = ? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_5g_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_5g_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_5g_Delete
//						  @PRS_5g_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_5g_Num");
			Map(x => x.Func, "PRS_5g_Func");
			Map(x => x.IS, "PRS_5g_IS");
			Map(x => x.Tasks, "PRS_5g_Task");
			Map(x => x.Reasons, "PRS_5g_Reason");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
			References<FgisInfo>(x => x.FgisInfo, "FGIS_Info_ID");
        }
    }
}