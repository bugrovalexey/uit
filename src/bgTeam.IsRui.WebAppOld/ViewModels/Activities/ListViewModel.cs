﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Domain.Entities.Workflow;
using bgTeam.IsRui.MKRF.Logic.Access;
using bgTeam.IsRui.MKRF.Logic.Tabs;
using bgTeam.IsRui.WebApp.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public abstract class ListBaseViewModel
    {
        public string AddNew { get; set; }

        public IList<string> Tabs { get; private set; }

        public IEnumerable<TableActivities> Items { get; set; }

        public ListBaseViewModel()
        {
            Load();

            var handler = Singleton<TabsManager>.Instance.GetHandler<ActivityTabs>();

            Tabs = handler.GetTabsList(LinkBuilder.ActionLink);
        }

        protected abstract void Load();
    }

    /// <summary>Мои мероприятия - список мероприятий для роли "Организация"</summary>
    public class ListModel : ListBaseViewModel
    {
        private readonly IUserRepository _userRepository;

        public ListModel(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        protected override void Load()
        {
            Items = new RepositoryDc<PlansActivity>().GetAllEx(query => query
                .Where(x => x.Department == _userRepository.GetCurrent().Department)
                .OrderBy(x => x.CreateDate).Desc
                .Fetch(f => f.Status).Eager
                .Fetch(f => f.ActivityType2).Eager
                .Fetch(f => f.AccountingObject).Eager.Clone())
                .Select(x => new TableActivities(x));

            AddNew = LinkBuilder.Link("<i class='material-icons'>add</i>", "#", onclick: "CreateNew();", @class: "btn-floating btn-large waves-effect waves-light");

            if (ManagerAccess.CanCreate(EntityType.PlansActivity) == false)
                AddNew = string.Empty;
        }
    }

    /// <summary>На согласовании - список мероприятий для роли "Куратор"</summary>
    public class ListApproveModel : ListBaseViewModel
    {
        private readonly IUserRepository _userRepository;

        public ListApproveModel(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        protected override void Load()
        {
            bgTeam.IsRui.Domain.Entities.Common.Department dep = null;
            ActivityExpertiseConclusion ec = null;
            ActivityType at = null;

            //TODO это примерный вариант, надо уточнить
            Items = new RepositoryDc<PlansActivity>().GetAllEx(query => query
                .JoinAlias(x => x.Department, () => dep)
                .JoinAlias(x => x.ExpertiseConclusion, () => ec)
                .JoinAlias(x => x.ActivityType2, () => at)
                .Where(x => x.Status.Id == (int)StatusEnum.ActOnExpertise)
                .Where(() => dep.Parent == _userRepository.GetCurrent().Department)
                .Where(() => ec.Status.Id == (int)StatusEnum.ExpAppointment || ec.Status.Id == (int)StatusEnum.ExpAgreed)
                .Fetch(f => f.AccountingObject).Eager
                .OrderBy(x => x.CreateDate).Desc.Clone())
                .Select(x => new TableActivities(x));
        }
    }

    /// <summary>На экспертизе - список мероприятий для ролей "Тех.специалист" и "Экономист"</summary>
    public class ListExpertiseModel : ListBaseViewModel
    {
        protected override void Load()
        {
            //TODO это примерный вариант, надо уточнить
            Items = new RepositoryDc<PlansActivity>().GetAllEx(query =>
                query.Where(x => x.Status.Id == (int)StatusEnum.ActOnExpertise)
                .OrderBy(x => x.CreateDate).Desc
                     .JoinQueryOver(x => x.ExpertiseConclusion)
                     .JoinQueryOver(x => x.Status)
                     .Where(x => x.Id == (int)StatusEnum.ExpTechnician
                                 || x.Id == (int)StatusEnum.ExpEconomist)
                    //.Fetch(f => f.ActivityType2).Eager
                     .Fetch(f => f.AccountingObject).Eager.Clone())
                .Select(x => new TableActivities(x));
        }
    }

    public class TableActivities : ViewModelBase
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string AO { get; set; }

        public string Status { get; set; }

        public string StatusCA { get; set; }

        public double Y0 { get; set; }

        public double Y1 { get; set; }

        public double Y2 { get; set; }

        public string DeleteLink { get; set; }

        public bool CanDelete { get; set; }

        public string Department { get; set; }

        public TableActivities(PlansActivity x)
        {
            Fill(x);
        }

        private void Fill(PlansActivity x)
        {
            Id = x.Id;
            Name = x.NameSmall;
            Department = x.Department.Name;
            Type = x.ActivityType2.Return(s => s.Name, default(string));
            AO = x.AccountingObject.Return(s => s.ShortName, default(string));
            Status = x.Status.Name;
            StatusCA = string.Concat(x.Status.Name, " \u2192 ", x.ExpertiseConclusion.Status.Name);
            Y0 = x.PlansActivityVolY0.Value;
            Y1 = x.PlansActivityVolY1.Value;
            Y2 = x.PlansActivityVolY2.Value;

            CanDelete = ManagerAccess.CanDelete(x); //права доступа устанавливаются в ListModel
            ReadOnly = !CanDelete;
            DeleteLink = ReadOnly ? string.Empty : LinkBuilder.AjaxDeleteLink("Delete", "Activities", new { id = x.Id }, "Grid");
        }

        
    }
}