﻿using GB.Data.Common;

namespace GB._Mapping.Common
{
    internal class UserToRoleMap : ClassMapHiLo<UserToRole>
    {
        public UserToRoleMap()
            : base("Users_To_Role")
        {
        }

        protected override void InitMap()
        {
            References(x => x.Role, "Role_ID").Not.Nullable();
            References(x => x.User, "Users_ID").Not.Nullable();
        }
    }
}