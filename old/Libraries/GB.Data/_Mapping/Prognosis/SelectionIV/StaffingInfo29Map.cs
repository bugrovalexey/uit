﻿using Uviri.MKRF.Entities.Prognosis.SelectionIV;

namespace Uviri.MKRF.Mapping.Prognosis.SelectionIV
{
    public class StaffingInfo29Map : ClassMapBase<StaffingInfo29> 
    {
        public StaffingInfo29Map()
            : base("MF4_29")
        {
//            Table("w_");

//            Id(x => x.Id, "MF4_29_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF4_29_Insert
//                          @MF4_29_Num = ?
//                         ,@MF4_29_Name = ?
//                         ,@MF4_ID = ?
//                         ,@MF4_29_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF4_29_Update
//                          @MF4_29_Num = ?
//                         ,@MF4_29_Name = ?
//                         ,@MF4_ID = ?
//                         ,@MF4_29_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF4_29_Delete
//						  @MF4_29_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "MF4_29_Num");
            Map(x => x.Name, "MF4_29_Name");

            References<StaffingInfo>(x => x.StaffingInfo, "MF4_ID").Not.Nullable();
        }
    }
}
