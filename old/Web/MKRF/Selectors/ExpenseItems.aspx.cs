﻿using System;
using DevExpress.Web.ASPxGridView;
using Uviri.Entity.Directories;
using Uviri.MKRF.Repository;

namespace Uviri.MKRF.Web.Selectors
{
    public partial class ExpenseItems : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            SelectorsExpenseItemsGrid.DataBind();

            base.OnPreRender(e);
        }

        protected void SelectorsExpenseItemsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var val = Request.QueryString["year"];
            int year = DateTime.Now.Year;
            Int32.TryParse(val, out year);

            SelectorsExpenseItemsGrid.DataSource = DirectoriesRepositary.GetDirectories<ExpenseItem>(year);
        }

        protected void SelectorsExpenseItemsGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "$(this).addClass(\"grid_mouseover\")");
            e.Row.Attributes.Add("onmouseout", "$(this).removeClass(\"grid_mouseover\")");
        }
    }
}