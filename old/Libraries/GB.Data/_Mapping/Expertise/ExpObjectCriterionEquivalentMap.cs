﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.OtherExpObjects;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpObjectCriterionEquivalentMap : ClassMapBase<ExpObjectCriterionEquivalent>
	{
        public ExpObjectCriterionEquivalentMap()
            : base("Exp_Criterion_Equivalent_Other")
		{
		}

        protected override void InitMap()
        {
            Map(x => x.Comm, "Exp_Criterion_Equivalent_Other_Comm");

            References<ExpObjectConclAct>(x => x.ExpConclAct, "Exp_Concl_Act_Other_ID");
            References<ExpCriterion>(x => x.ExpCriterion, "Exp_Criterion_ID");
            References<ExpConformity>(x => x.ExpConformity, "Exp_Conformity_ID");
        }
    }
}