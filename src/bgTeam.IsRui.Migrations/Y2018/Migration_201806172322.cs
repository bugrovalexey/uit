﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201806172324)]
    public class Migration_201806172324 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("SpecialistCategoryType")
                .WithColumn("SpecialistCategoryType_ID").AsInt32().ForeignKey().Unique()
                .WithColumn("SCT_Name").AsString(255).Nullable();
        }
    }
}
