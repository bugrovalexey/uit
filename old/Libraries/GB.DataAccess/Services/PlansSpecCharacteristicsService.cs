﻿using System.Collections;
using GB.Data.AccountingObjects;
using GB.Data.Directories;
using GB.Data.Plans;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class PlansSpecCharacteristicsService : ServiceBase<PlansSpecCharacteristic>, IService<PlansSpecCharacteristic, PlansSpecCharacteristicArgs>
    {
        public PlansSpecCharacteristic Create(PlansSpecCharacteristicArgs args)
        {
            var goods = GetEntity<PlansSpec>(args.GoodsId);
            PlansSpecCharacteristic entity = new PlansSpecCharacteristic { Goods = goods };

            return SaveOrUpdate(entity, args);
        }

        public PlansSpecCharacteristic Update(PlansSpecCharacteristicArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            return SaveOrUpdate(entity, args);
        }

        private PlansSpecCharacteristic SaveOrUpdate(PlansSpecCharacteristic entity, PlansSpecCharacteristicArgs args)
        {
            entity.Name = args.Name;
            entity.Unit = GetEntity<Units>(args.Unit);
            entity.Value = args.Value;

            base.SaveOrUpdate(entity);

            return entity;
        }

        public void SetGoodsForCharacteristics(ICollection characteristicsIds,
            PlansSpec goods)
        {
            var characteristics = Repository.GetAllEx(q => q.WhereRestrictionOn(x => x.Id).IsIn(characteristicsIds));

            foreach (var item in characteristics)
            {
                item.Goods = goods;
                SaveOrUpdate(item);
            }
        }


        public void Delete(PlansSpecCharacteristicArgs args)
        {
            base.Delete(args.Id);
        }
    }

    public class PlansSpecCharacteristicArgs : BaseArgsNew
    {
        public string Name { get; set; }

        public int Unit { get; set; }

        public int? Value { get; set; }

        public int GoodsId { get; set; }
    }
}