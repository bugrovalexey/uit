﻿namespace GB.MKRF.Services.Args
{
    /// <summary>
    /// Базовый Args ОУ
    /// </summary>
    public class AccountingObjectArgs : BaseArgsNew
    {
        /// <summary>
        /// Краткое наименование
        /// </summary>
        public string ShortName { get; set; }
    }
}