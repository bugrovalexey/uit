﻿using System;

namespace Uviri.MKRF.Entities.Admin
{
	/// <summary>Доп. право пользователя</summary>
	public class UsersAnswer : EntityBase
	{
		/// <summary>Пользователь</summary>
        public virtual UserQuestionnaire UserQuestionnaire { get; set; }

		/// <summary>Ответ</summary>
		public virtual QuestionAnswer Answer { get; set; }

        /// <summary>Текст ответа</summary>
        public virtual string Text { get; set; }

        /// <summary>Дата ответа</summary>
        public virtual DateTime Date { get; set; }
	}
}
