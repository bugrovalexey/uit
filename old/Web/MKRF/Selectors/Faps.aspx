﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="Faps.aspx.cs" Inherits="GB.MKRF.Web.Selectors.Faps" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript">
        function rowClick(s, e) {
            var id = s.GetRowKey(e.visibleIndex);
            s.GetRowValues(e.visibleIndex, 'Id;NAME;FGISNumber', function (values) {
                var result = id + ';' + values[1] + ';' + values[2];

                window.parent.PopupClose(result);
            });
        }
    </script>
    <dxwgv:ASPxGridView ID="SelectorFapGrid" ClientInstanceName="SelectorFapGrid" runat="server" AutoGenerateColumns="False" KeyFieldName="Id" 
        OnBeforePerformDataSelect="SelectorFapGrid_BeforePerformDataSelect" OnHtmlRowCreated="SelectorFapGrid_HtmlRowCreated" >
        <Columns>
            <dxwgv:GridViewDataColumn FieldName="Id" Visible="false" >
            </dxwgv:GridViewDataColumn>
            <dxwgv:GridViewDataTextColumn FieldName="NAME" Visible="true" Caption="Наименование ФАП">
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="FGISNumber" Visible="true" Caption="Номер ФГИС">
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Styles>
            <AlternatingRow Enabled="true" />
            <Header Wrap="True" />
        </Styles>
        <ClientSideEvents RowClick="rowClick" />
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="15" />
    </dxwgv:ASPxGridView>
</asp:Content>
