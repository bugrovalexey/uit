﻿namespace GB.Data.Frgu
{
    /// <summary>
    /// Нормативный правовой акт
    /// </summary>
    public class GovServiceNPA : EntityBase
    {
        /// <summary>Hаименование</summary>
        public virtual string Name { get; set; }

        ///// <summary>SN</summary>
        //public virtual string Sn { get; set; }

        ///// <summary>Дата утверждения </summary>
        //public virtual DateTime? ApprovalDate { get; set; }

        /// <summary>
        /// Госуслуга
        /// </summary>
        public virtual GovService GovService { get; set; }
    }
}
