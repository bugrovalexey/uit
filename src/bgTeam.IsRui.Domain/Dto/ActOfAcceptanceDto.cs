﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Акт сдачи-приемки
    /// </summary>
    public class ActOfAcceptanceDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Госконтракт. Id
        /// </summary>
        public string GovContractId { get; set; }

        /// <summary>
        /// Госконтракт. Number
        /// </summary>
        public string GovContractNumber { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string DocName { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string DocNumber { get; set; }

        /// <summary>
        /// Дата подписания документа
        /// </summary>
        public DateTime? DocDateAdoption { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public ActivityTypeEnum? ActivityType { get; set; }
    }
}
