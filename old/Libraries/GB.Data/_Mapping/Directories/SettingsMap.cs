﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class SettingsMKRFMap : ClassMapBase<SettingsMKRF>
    {
        public SettingsMKRFMap()
            : base("Settings")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Settings_Name").ReadOnly();
            Map(x => x.Value, "Settings_Value");
            Map(x => x.Type, "Settings_Type").CustomType(typeof(SettingsMKRFTypeEnum)).Not.Nullable().ReadOnly();
        }
    }
}