﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.Admin;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin.SiteMap
{
    public class TableSiteMapToRole : NotificationBaseViewModel
    {
        private const string NameCaption = "Роль";

        public TableSiteMapToRole(SiteMapToRole siteMapToRole)
        {
            Id = siteMapToRole.Id;
            Name = siteMapToRole.RoleName;
        }

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteSiteMapToRole", "Admin", new { id = Id }, "GridSiteMapToRole");
            }
        }
    }
}