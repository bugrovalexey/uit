﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;

namespace GB.Controls
{
    /// <summary>
    /// Контрол заменяющий стандартный HtmlForm для сохранения значения аттрибута name
    /// </summary>
    public class FormControl : HtmlForm
    {
        public string OwnUniqueID { get; set; }

        public override string UniqueID
        {
            get
            {
                return this.OwnUniqueID;
            }
        }
    }
}
