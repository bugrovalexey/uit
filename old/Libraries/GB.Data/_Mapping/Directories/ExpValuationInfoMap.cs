﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
	public class ExpValuationInfoMap : ClassMapBase<ExpValuationInfo>
	{
        public ExpValuationInfoMap()
            : base("Exp_Valuation_Info")
		{
		}

        protected override void InitMap()
        {
            Map(x => x.SQL, "Exp_Valuation_Info_SQL");
            Map(x => x.Comm, "Exp_Valuation_Info_Comm");
        }
    }
}
