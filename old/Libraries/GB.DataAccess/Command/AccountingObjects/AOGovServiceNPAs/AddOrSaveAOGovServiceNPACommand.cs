﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServiceNPAs
{
    public class AddOrSaveAOGovServiceNPACommand : ICommand<AddOrSaveAOGovServiceNPAContext, AOGovServiceNPA>
    {
        ISessionFactoryGb factory;

        public AddOrSaveAOGovServiceNPACommand()
        {
            factory = new SessionFactory();
        }

        public AOGovServiceNPA Execute(AddOrSaveAOGovServiceNPAContext context)
        {
            AOGovServiceNPA aoGServNPA;

            if (context.xId.HasValue && context.xId != 0)
                aoGServNPA = new Repository<AOGovServiceNPA>().Get(context.xId.Value);
            else
            {
                aoGServNPA = new AOGovServiceNPA();                
            }

            AutoMapper.Mapper.Map(context, aoGServNPA);

            using (var session = factory.Create())
            {
                session.SaveOrUpdate(aoGServNPA);
                session.Commit();
            }

            return aoGServNPA;
        }
    }
}
