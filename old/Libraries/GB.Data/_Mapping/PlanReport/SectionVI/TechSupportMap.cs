﻿using Uviri.MKRF.Entities.PlanReport.SectionVI;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVI
{
    public class TechSupportMap : ClassMapBase<TechSupport>
	{
        public TechSupportMap()
            : base("PRS_6_Support")
		{
//            Table("w_");

//            Id(x => x.Id, "PRS_6_Support_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string args = @" @PRS_6_Support_Name = ? 
//							,@PRS_6_Support_ID = ? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_6_Support_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_6_Support_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_6_Support_Delete
//						  @PRS_6_Support_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Name, "PRS_6_Support_Name");
        }
    }
}