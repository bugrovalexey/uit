﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Workflow;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimUserCommentDto
    {
        public int Id { get; set; }

        /// <summary>ОУ</summary>
        public int ClaimId { get; set; }

        /// <summary>Пользователь</summary>
        public UserDto User { get; set; }

        /// <summary>Комментарий</summary>
        public string Comment { get; set; }

        /// <summary> Дата создания </summary>
        public DateTime CreateDate { get; set; }

        /// <summary> Документ </summary>
        public IList<FilesDto> Documents { get; set; }

        /// <summary> Статус </summary>
        public StatusDto Status { get; set; }
    }
}
