﻿
namespace GB.DataAccess.Command.AccountingObjects.Documents
{
    public class AddDocumentContext : ICommandContext
    {        
        public int? xId { get; set; }

        public string CommentFiles { get; set; }

        public int AccountingObjectId { get; set; }
    }
}
