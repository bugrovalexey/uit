﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpCriterionEquivalentSubsidizeMap : ClassMapBase<ExpCriterionEquivalentSubsidize>
	{
        public ExpCriterionEquivalentSubsidizeMap()
            : base("Exp_Criterion_Equivalent_Subsidize")
		{
		}

        protected override void InitMap()
        {
            Map(x => x.Comm, "Exp_Criterion_Equivalent_Subsidize_Comm");

            References<ExpConclSubsidizeAct>(x => x.ExpConclSubsidizeAct, "Exp_Concl_Subsidize_Act_ID");
            References<ExpCriterion>(x => x.ExpCriterion, "Exp_Criterion_ID");
            References<ExpConformity>(x => x.ExpConformity, "Exp_Conformity_ID");
        }
    }
}