﻿using bgTeam.IsRui.Domain.Entities.Admin;
using bgTeam.IsRui.Domain.Entities.Common;

namespace bgTeam.IsRui.Domain._Mapping.Admin
{
    internal class SiteMapToRoleMap : ClassMapHiLo<SiteMapToRole>
    {
        public SiteMapToRoleMap()
            : base("Site_Map_Role") { }

        protected override void InitMap()
        {
            //Table("Site_Map_Role");

            //Id(x => x.Id, "Site_Map_Role_ID").GeneratedBy.Increment();
            //.GeneratedBy.Custom(typeof(IdGenerator)).UnsavedValue((int)EntityBase.UnsavedId);

            References<SiteMapNode>(x => x.SiteMap, "Site_Map_Node_ID");
            References<Role>(x => x.Role, "Role_ID");
        }
    }
}