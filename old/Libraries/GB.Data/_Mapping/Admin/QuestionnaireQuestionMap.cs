﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Mapping.Admin
{
    class QuestionnaireQuestionMap : ClassMapBase<QuestionnaireQuestion>
    {
        public QuestionnaireQuestionMap()
            : base("Questions")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "Questions_Num");
            Map(x => x.Text, "Questions_Text");

            References<Questionnaire>(x => x.Questionnaire, "Quests_ID").Not.Nullable();

            HasMany<QuestionAnswer>(x => x.Answers).KeyColumns.Add("Questions_ID").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
