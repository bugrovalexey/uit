﻿using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.OtherExpObjects;
using Uviri.MKRF.Entities.Statuses;

namespace Uviri.MKRF.Mapping.OtherExpObjects
{
    public class ExpObjectConclusionMap : ClassMapBase<ExpObjectConclusion>
	{
		public ExpObjectConclusionMap()
            : base("Exp_Obj_Conclusion")
		{
		}

        protected override void InitMap()
        {
            Map(x => x.ExpConclusionNum1, "Exp_Obj_Conclusion_Num1");
            Map(x => x.ExpConclusionNum2, "Exp_Obj_Conclusion_Num2");
            Map(x => x.ExpConclusionNum3, "Exp_Obj_Conclusion_Num3");
            Map(x => x.ExpConclusionNum4, "Exp_Obj_Conclusion_Num4");
            Map(x => x.ExpConclusionNum5, "Exp_Obj_Conclusion_Num5");

            Map(x => x.IsActive, "Exp_Obj_Conclusion_Is_Active");
            Map(x => x.CustInfo, "Exp_Obj_Conclusion_Cust_Info");
            Map(x => x.GetDate, "Exp_Obj_Conclusion_Get_Date");
            Map(x => x.Conflict, "Exp_Obj_Conclusion_Conflict");
            Map(x => x.Recom, "Exp_Obj_Conclusion_Recom");
            Map(x => x.Content, "Exp_Obj_Conclusion_Content");
            Map(x => x.IsPublic, "Exp_Obj_Conclusion_Is_Public");
            Map(x => x.Decision, "Exp_Obj_Conclusion_Decision");

            Map(x => x.Agreement, "Exp_Obj_Conclusion_Agreement");
            Map(x => x.Additional, "Exp_Obj_Conclusion_Additional");
            Map(x => x.Reason, "Exp_Obj_Conclusion_Reason");

            References<Status>(x => x.Status, "Status_ID").Not.Nullable();
            References<ExpObjectDemand>(x => x.Demand, "Exp_Obj_Demand_ID").Not.Nullable();
			References<User>(x => x.User, "Users_ID");
            References<ExpDocSufficiency>(x => x.ExpDocSufficiency, "Exp_Doc_Sufficiency_ID");
            References<ExpDocEquivalent>(x => x.ExpDocEquivalent, "Exp_Doc_Equivalent_ID");
            References<ExpDocFinalEvaluation>(x => x.ExpDocFinalEvaluation, "Exp_Doc_Final_Evaluation_ID");

            HasMany<ExpObjectConclAct>(x => x.Acts).KeyColumns.Add("Exp_Obj_Conclusion_ID").Inverse();
        }
    }
}