﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans.ActivityOffers;

namespace Uviri.MKRF.Mapping.Plans.ActivityOffers
{
    public class ActivityOfferISMap : ClassMapBase<ActivityOfferIS>
    {
        public ActivityOfferISMap()
            : base("Activity_Offer_IS")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Activity_Offer_IS_Name");
            Map(x => x.Indication, "Activity_Offer_IS_Indication");
            Map(x => x.FGIS_Text, "Activity_Offer_IS_FGIS_Text");

            References<FgisInfo>(x => x.FGIS, "FGIS_Info_ID");
            References<ActivityOffer>(x => x.Offer, "Activity_Offer_Inform_ID");
            References<IKTComponent>(x => x.IKT, "IKT_Component_ID");
        }
    }
}