﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class GRBSMap : ClassMapHiLo<GRBS>
    {
        public GRBSMap()
            : base("GRBS")
        {
            //            Table("w_");

            //            Id(x => x.Id, "GRBS_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_GRBS_Insert
            //                          @GRBS_Code = ?
            //                         ,@GRBS_Name = ?
            //                         ,@GRBS_SName = ?
            //                         ,@GRBS_Addr = ?
            //                         ,@GRBS_Phone = ?
            //					     ,@GRBS_Web = ?
            //                         ,@GRBS_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_GRBS_Update
            //                          @GRBS_Code = ?
            //                         ,@GRBS_Name = ?
            //                         ,@GRBS_SName = ?
            //                         ,@GRBS_Addr = ?
            //                         ,@GRBS_Phone = ?
            //					     ,@GRBS_Web = ?
            //                         ,@GRBS_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_GRBS_Delete
            //						  @GRBS_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "GRBS_Code");
            Map(x => x.Name, "GRBS_Name");
            Map(x => x.SName, "GRBS_SName");
            Map(x => x.Addr, "GRBS_Addr");
            Map(x => x.Phone, "GRBS_Phone");
            Map(x => x.Web, "GRBS_Web");
            Map(x => x.DateStart, "GRBS_Start_Date");
            Map(x => x.DateEnd, "GRBS_End_Date");
        }
    }
}