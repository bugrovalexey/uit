﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MailsControl.ascx.cs" Inherits="GB.MKRF.Web.Common.MailsControl" %>
<%@ Import Namespace="GB.MKRF.Web.Selectors" %>
<script type="text/javascript">
    function hfSave(e) {

        var mode = hf.Get('editMode');

        if (mode == "ogv") {

            hf.Set('NumberOutgoing', typeof (NumberOutgoingTextBox) != 'undefined' ? NumberOutgoingTextBox.GetValue() : null);
            hf.Set('DateOutgoing', typeof (DateOutgoingDateEdit) != 'undefined' ? DateOutgoingDateEdit.GetValue() : null);
        }
        else if (mode == "mks") {

            hf.Set('NumberInboxMKS', typeof (NumberInboxMKSTextBox) != 'undefined' ? NumberInboxMKSTextBox.GetValue() : null);
            hf.Set('DateInboxMKS', typeof (DateInboxMKSDateEdit) != 'undefined' ? DateInboxMKSDateEdit.GetValue() : null);
            hf.Set('State', typeof (StateComboBox) != 'undefined' ? StateComboBox.GetValue() : null);
            hf.Set('NumberOutgoingMKS', typeof (NumberOutgoingMKSTextBox) != 'undefined' ? NumberOutgoingMKSTextBox.GetValue() : null);
            hf.Set('DateOutgoingMKS', typeof (DateOutgoingMKSDateEdit) != 'undefined' ? DateOutgoingMKSDateEdit.GetValue() : null);
            hf.Set('Comment', typeof (CommentMemo) != 'undefined' ? CommentMemo.GetValue() : null);
        }

        hf.Set('Files', grid.cpFiles);
        hf.Set('SelectedFiles', filesGrid.cpSelected);
    }

    function SetContentUrl(s, e) {
        s.SetContentUrl('<%=PlanPaperDocuments.GetUrl(CurrentPlan.Demand) %>');
    }

    function PopupClose(idList, textList) {
        grid.cpFiles = idList;
        FileChoosePopupControl.Hide();
        filesGrid.PerformCallback(idList + "#" + filesGrid.cpSelected);
    }
    function GetIds() {
        return grid.cpFiles;
    }
    function deleteRow(rowNum) {
        if (confirm('Вы подтверждаете удаление?'))
            grid.DeleteRow(rowNum);
    }
    function editRow(mode, rowNum) {
        hf.Clear();
        hf.Set('editMode', mode);
        grid.StartEditRow(rowNum);
    }
    function updateSelectedFiles(docId, isChecked) {

        var arr = filesGrid.cpSelected ? filesGrid.cpSelected.split(';') : new Array();

        if (!isChecked)
            arr = jQuery.grep(arr, function (value) { return value != docId; });
        else if (jQuery.inArray(docId, arr) < 0) {
            arr.push(docId);
        }

        filesGrid.cpSelected = arr.join(';');
    }
</script>
<dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
<dxe:ASPxButton runat="server" ID="AddButton" ClientInstanceName="AddButton" EnableClientSideAPI="true"
    Text="Создать письмо" Style="margin-bottom: 20px;" AutoPostBack="False">
    <ClientSideEvents Click="function(s,e) { hf.Set('editMode', 'ogv'); grid.AddNewRow();}" />
</dxe:ASPxButton>
<table width="100%" style="text-align: center; font-weight: bold; font-size: 1.5em; padding: 15px 0;">
    <tr>
        <td width="50%"><%=CurrentPlan.Department.Type.Name %></td>
        <td width="50%">Министерство культуры РФ</td>
    </tr>
</table>
<dxwgv:ASPxGridView runat="server" ID="Grid" KeyFieldName="Id" Width="100%" ClientInstanceName="grid"
    OnBeforePerformDataSelect="grid_BeforePerformDataSelect" OnStartRowEditing="grid_StartRowEditing"
    OnCustomJSProperties="grid_CustomJSProperties" OnRowDeleting="grid_RowDeleting"
    OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating">
    <Border BorderWidth="0px" />
    <SettingsEditing Mode="PopupEditForm" PopupEditFormModal="true" PopupEditFormVerticalAlign="Middle" PopupEditFormHorizontalAlign="Center" />
    <Settings ShowColumnHeaders="false" />
    <SettingsBehavior ConfirmDelete="True" />
    <Columns>
        <dxwgv:GridViewDataTextColumn Caption="Исходящий номер" FieldName="NumberOutgoing" />
        <dxwgv:GridViewDataTextColumn Caption="Дата исходящего" FieldName="DateOutgoing" />
        <dxwgv:GridViewDataTextColumn Caption="Входящий номер" FieldName="NumberInboxMKS" />
        <dxwgv:GridViewDataDateColumn Caption="Дата входящего" FieldName="DateInboxMKS" />
        <dxwgv:GridViewDataTextColumn Caption="Статус" FieldName="State" />
        <dxwgv:GridViewDataTextColumn Caption="Исходящий номер ОГВ" FieldName="NumberOutgoing" />
        <dxwgv:GridViewDataTextColumn Caption="Исходящий номер Минкомсвязь" FieldName="NumberOutgoingMKS" />
        <dxwgv:GridViewDataDateColumn Caption="Дата исходящего" FieldName="DateOutgoingMKS" />
        <dxwgv:GridViewDataTextColumn Caption="Комментарий" FieldName="Comment" />
    </Columns>
    <Templates>
        <EditForm>
            <table class="form_edit" style="width: 900px">
                <%if (editMode == editModeEnum.ogv) {%>
                
                <tr>
                    <td class="form_edit_desc required">Исходящий номер</td>
                    <td class="form_edit_input">
                        <dxe:ASPxTextBox runat="server" ID="NumberOutgoingTextBox" ClientInstanceName="NumberOutgoingTextBox" 
                            OnDataBinding="Box_DataBinding" >
                            <ClientSideEvents Init="function(s,e) { s.validationGroup = grid.name; }" />
                            <ValidationSettings ErrorDisplayMode="Text" >
                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                            </ValidationSettings>
                        </dxe:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="form_edit_desc required">Дата исходящего</td>
                    <td class="form_edit_input">
                        <dxe:ASPxDateEdit runat="server" ID="DateOutgoingDateEdit" ClientInstanceName="DateOutgoingDateEdit"
                            OnDataBinding="Box_DataBinding" DisplayFormatString="dd.MM.yyyy HH:mm" >
                            <ClientSideEvents Init="function(s,e) { s.validationGroup = grid.name; }" />
                            <TimeSectionProperties Visible="true" />
                            <ValidationSettings ErrorDisplayMode="Text">
                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                            </ValidationSettings>
                        </dxe:ASPxDateEdit>
                    </td>
                </tr>
                
                <%} else if (editMode == editModeEnum.mks) {%>
                
                <td class="form_edit_section top" colspan="2">
                    Входящее
                    <div class="line"></div>
                </td>
                <tr>
                    <td class="form_edit_desc">Номер
                    </td>
                    <td class="form_edit_input">
                        <dxe:ASPxTextBox runat="server" ID="NumberInboxMKSTextBox" ClientInstanceName="NumberInboxMKSTextBox"
                            OnDataBinding="Box_DataBinding">
                        </dxe:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="form_edit_desc">Дата
                    </td>
                    <td class="form_edit_input">
                        <dxe:ASPxDateEdit runat="server" ID="DateInboxMKSDateEdit" ClientInstanceName="DateInboxMKSDateEdit"
                            OnDataBinding="Box_DataBinding" DisplayFormatString="dd.MM.yyyy HH:mm">
                            <TimeSectionProperties Visible="true"></TimeSectionProperties>
                        </dxe:ASPxDateEdit>
                    </td>
                </tr>
                <tr>
                    <td class="form_edit_desc">Статус
                    </td>
                    <td class="form_edit_input">
                        <dxe:ASPxComboBox runat="server" ID="StateComboBox" ClientInstanceName="StateComboBox"
                            OnDataBinding="Box_DataBinding" TextField="Name" ValueField="Id" ValueType="System.Int32">
                        </dxe:ASPxComboBox>
                    </td>
                </tr>
                <td class="form_edit_section" colspan="2">
                    Исходящее
                    <div class="line"></div>
                </td>
                <tr>
                    <td class="form_edit_desc required">Номер</td>
                    <td class="form_edit_input">
                        <dxe:ASPxTextBox runat="server" ID="NumberOutgoingMKSTextBox" ClientInstanceName="NumberOutgoingMKSTextBox"
                            OnDataBinding="Box_DataBinding">
                            <ClientSideEvents Init="function(s,e) { s.validationGroup = grid.name; }" />
                            <ValidationSettings ErrorDisplayMode="Text" >
                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                            </ValidationSettings>
                        </dxe:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="form_edit_desc required">Дата</td>
                    <td class="form_edit_input">
                        <dxe:ASPxDateEdit runat="server" ID="DateOutgoingMKSDateEdit" ClientInstanceName="DateOutgoingMKSDateEdit"
                            OnDataBinding="Box_DataBinding" DisplayFormatString="dd.MM.yyyy HH:mm" >
                            <ClientSideEvents Init="function(s,e) { s.validationGroup = grid.name; }" />
                            <TimeSectionProperties Visible="true"></TimeSectionProperties>
                            <ValidationSettings ErrorDisplayMode="Text" >
                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                            </ValidationSettings>
                        </dxe:ASPxDateEdit>
                    </td>
                </tr>
                <tr>
                    <td class="form_edit_desc">Комментарий</td>
                    <td class="form_edit_input">
                        <dxe:ASPxMemo runat="server" ID="CommentMemo" ClientInstanceName="CommentMemo" Rows="5"
                            OnDataBinding="Box_DataBinding">
                        </dxe:ASPxMemo>
                    </td>
                </tr>

                <%} %>
                
                <tr>
                    <td class="form_edit_desc">Файлы</td>
                    <td class="form_edit_input">
                        <dxwgv:ASPxGridView runat="server" ID="filesGrid" ClientInstanceName="filesGrid"
                            KeyFieldName="Id" OnBeforePerformDataSelect="filesGrid_BeforePerformDataSelect"
                            OnCustomCallback="filesGrid_Callback" OnCustomJSProperties="filesGrid_CustomJSProperties" >
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Название"  FieldName="Name" />
                                <dxwgv:GridViewDataCheckColumn Caption="Экспертное заключение" Visible="false" Name="Checked" FieldName="Checked" Width="20px">
                                    <DataItemTemplate>
                                        <dxe:ASPxCheckBox runat="server" Checked='<%#(bool)Eval("Checked")%>' OnInit="chk_Init" />
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataCheckColumn>
                            </Columns>
                        </dxwgv:ASPxGridView>
                        <div id="Td1" runat="server" valign="top" visible='<%#(editMode == editModeEnum.ogv && userSide == userSideEnum.ogv) || (editMode == editModeEnum.mks && userSide == userSideEnum.mks) %>'>
                            <a href="javascript:void()" id="fileChooseLink" class="command">Выбрать</a>
                        </div>
                        <dxpc:ASPxPopupControl ID="FileChoosePopupControl" runat="server" AllowDragging="True" AllowResize="True"
                            CloseAction="CloseButton" Modal="True" EnableViewState="False" PopupElementID="fileChooseLink"
                            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="650px"
                            HeaderText="Выберите файл" ClientInstanceName="FileChoosePopupControl" EnableHierarchyRecreation="True">
                            <ClientSideEvents Init="SetContentUrl"></ClientSideEvents>
                        </dxpc:ASPxPopupControl>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="form_edit_buttons">
                        <span class="ok"><dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" /></span>
                        <span class="cancel"><dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" /></span>
                    </td>
                </tr>
            </table>
        </EditForm>
        <DataRow>
            <table width="100%">
                <tr valign="top">
                    <td style="width: 50%">
                        <div class="mails">
                            <div class="text">Исходящее:</div>
                            <div class="number">
                                <%#GetMailText(Eval("NumberOutgoing") as string, Eval("DateOutgoing") as string) %>
                            </div>
                            <%#Eval("FilesOgv")%>
                        </div>
                        <div class="form_edit_buttons">
                            <span class="cancel" id="s1" runat="server" visible='<%#((userSide == userSideEnum.ogv) && Eval("CanEdit") != null && (bool)Eval("CanEdit")) ? true : false%>'>
                                <a class="command" href="javascript:editRow('ogv', <%# Container.VisibleIndex %>);">Изменить</a>
                            </span>
                            <span class="cancel" id="s2" runat="server" visible='<%#(userSide == userSideEnum.ogv && Eval("CanDelete") != null && (bool)Eval("CanDelete")) ? true : false%>'>
                                 <a class="command" href="javascript:deleteRow(<%# Container.VisibleIndex %>);">Удалить</a>
                             </span>
                        </div>
                    </td>
                    <td style="vertical-align: middle">
                        <div class="mails">
                            <div class="direction">
                                <%#Eval("Direction")%>
                            </div>
                        </div>
                    </td>
                    <td style="width: 50%">
                        <div class="mails">
                            <div class="text">Входящее:</div>
                            <div class="number">
                                <%#GetMailText(Eval("NumberInboxMKS") as string, Eval("DateInboxMKS") as string, Eval("State") as string) %>
                            </div>
                            <div class="text">Исходящее:</div>
                            <div class="number">
                                <%#GetMailText(Eval("NumberOutgoingMKS") as string, Eval("DateOutgoingMKS") as string) %>
                            </div>
                            <div class="comment">
                                <%#Eval("Comment")%>
                            </div>
                            <%#Eval("FilesMks")%>
                        </div>
                        <table width="100%">
                            <tr id="Tr2" runat="server" visible='<%#(userSide == userSideEnum.mks && Eval("CanEdit") != null && (bool)Eval("CanEdit")) ? true : false%>'>
                                <td>&nbsp;
                                </td>
                                <td align="center" style="padding-top: 15px">
                                    <a class="command" href="javascript:editRow('mks', <%# Container.VisibleIndex %>);">Изменить</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </DataRow>
    </Templates>
    <ClientSideEvents BeginCallback="function(s,e) { if (e.command=='UPDATEEDIT') hfSave(e); }" />
</dxwgv:ASPxGridView>

