﻿namespace bgTeam.IsRui.Story.Activities
{
    using System.Threading.Tasks;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using bgTeam.IsRui.Domain.Entities.Plans;

    public class GoodsAddOrUpdateGoodsCharacteristicStory : IStory<GoodsAddOrUpdateGoodsCharacteristicStoryContext, GoodsCharacteristicDto>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public GoodsAddOrUpdateGoodsCharacteristicStory(ISessionNHibernateFactory factory, IRepositoryRui repository, IMapperBase mapper)
        {
            _factory = factory;
            _repository = repository;
            _mapper = mapper;
        }

        public GoodsCharacteristicDto Execute(GoodsAddOrUpdateGoodsCharacteristicStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<GoodsCharacteristicDto> ExecuteAsync(GoodsAddOrUpdateGoodsCharacteristicStoryContext context)
        {
            GoodsCharacteristic characteristic;

            var units = _repository.Get<Units>(context.UnitId);

            ActivityGoods goods;

            using (var session = _factory.OpenSession())
            {
                if (context.GoodsId.HasValue)
                {
                    goods = _repository.Get<ActivityGoods>(context.GoodsId);
                }
                else
                {
                    goods = null;
                }

                if (context.Id.HasValue)
                {
                    characteristic = _repository.Get<GoodsCharacteristic>(context.Id);
                }
                else
                {
                    characteristic = new GoodsCharacteristic();
                }

                _mapper.Map(context, characteristic);

                characteristic.Unit = units;

                characteristic.Goods = goods;

                await session.SaveOrUpdateAsync(characteristic);
            }

            return _mapper.Map(characteristic, new GoodsCharacteristicDto());
        }
    }
}
