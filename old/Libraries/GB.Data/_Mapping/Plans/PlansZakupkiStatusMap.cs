﻿using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Mapping.Plans
{
    class PlansZakupkiStatusMap : ClassMapBase<PlansZakupkiStatus>
    {
        public PlansZakupkiStatusMap()
            : base("Plans_Zakupki_Status")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Zakupki_Status_Name");
        }
    }
}