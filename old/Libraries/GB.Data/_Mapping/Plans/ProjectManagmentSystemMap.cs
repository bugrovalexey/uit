﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ProjectManagmentSystemMap : ClassMapBase<ProjectManagementSystem>
    {
        public ProjectManagmentSystemMap()
            : base("Management_PA")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "Management_PA_Numb");
            Map(x => x.ActNum, "Management_PA_Act_Num");
            Map(x => x.ActName, "Management_PA_Act_Name");
            Map(x => x.ActDate, "Management_PA_Act_Date").Nullable();
            Map(x => x.FIO, "Management_PA_FIO");
            Map(x => x.Post, "Management_PA_Post");
            Map(x => x.Role, "Management_PA_Role_Name");
            Map(x => x.Func, "Management_PA_Func");
            Map(x => x.Exp, "Management_PA_Exp");
            Map(x => x.ISName, "Management_PA_IS_Name");
            Map(x => x.Procedures, "Management_PA_Proc");

            References<PlansActivity>(x => x.PlansActivity, "Plans_Activity_ID");
            References<Plan>(x => x.Plan, "Plans_ID");
        }
    }
}
