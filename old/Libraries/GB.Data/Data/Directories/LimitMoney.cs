﻿using GB.Data.Common;

namespace GB.Data.Directories
{
    /// <summary>Планируемые расходы организации на год</summary>
    public class LimitMoney : EntityBase
    {
        /// <summary>Ведомство</summary>
        public virtual Department Department { get; set; }

        /// <summary>Год</summary>
        public virtual Years Year { get; set; }

        /// <summary>Расходы</summary>
        public virtual double Money { get; set; }
    }
}