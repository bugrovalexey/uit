﻿using System;
using System.Linq;
using GB.Controls;
using GB.Extentions;
using GB.Handlers;
using GB.Helpers;
using DevExpress.Web.ASPxEditors;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Projects;
using GB.MKRF.Repository;
using GB.Controls.Extensions;

namespace GB.MKRF.Web.Projects
{
    //Проект - Общие сведения
    public partial class CommonData : System.Web.UI.Page
    {
        readonly ProjectController controller = new ProjectController();

        public static string GetUrl(object projectId)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/Projects/CommonData.aspx"), EditCard.ProjectIdKey, projectId);
        }

        public Project CurrentProject
        {
            get { return ((EditCard)Master).CurrentProject; }
        }


        protected override void OnPreRender(EventArgs e)
        {
            controller.CurrentEdit = CurrentProject;

            NameTextBox.DataBind();
            ShortNameTextBox.DataBind();
            DescriptionTextBox.DataBind();
            SubdivisionTextBox.DataBind();
            DepartmentComboBox.DataBind();
            ResponsibleUserTextBox.DataBind();
            SignerTextBox.DataBind();
            FileUploader.DataBind();

            UsersGrid.DataBind();

            base.OnPreRender(e);
        }

        #region Handlers

        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            var args = new ProjectControllerArgs
            {
                Id = CurrentProject.Id,
                FullName = hf["FullName"] as string,
                ShortName = hf["ShortName"] as string,
                DepartmentId = hf["DepartmentId"],
                ResponsibleId = hf["ResponsibleUserId"],
                Signer = hf["SignerId"],
                Description = hf["Description"] as string,
                Subdivision = hf["Subdivision"] as string,
                Logo = hf["Logo"] as string
            };
            controller.UpdateCommonData(args);
        }

        protected void DepartmentComboBox_DataBinding(object sender, EventArgs e)
        {
            var data = controller.GetDepartments();
            sender.SetDataComboBox(data).SetValueComboBox<int>(controller.CurrentEdit
                  .Return(x => x.Department, UserRepository.GetCurrent().Department)
                  .Return(x => x.Id));
        }

        protected void ResponsibleUserTextBox_DataBinding(object sender, EventArgs e)
        {
            BindUserTextBox((ASPxTextBox)sender, CurrentProject.ResponsibleUser, "ResponsibleUser");
        }

        protected void SignerTextBox_DataBinding(object sender, EventArgs e)
        {
            BindUserTextBox((ASPxTextBox)sender, CurrentProject.Signer, "Signer");
        }

        protected void NameTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(CurrentProject.Name);
        }

        protected void ShortNameTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(CurrentProject.ShortName);
        }

        protected void DescriptionTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(CurrentProject.Description);
        }

        protected void SubdivisionTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(CurrentProject.Subdivision);
        }

        protected void FileUploader_DataBinding(object sender, EventArgs e)
        {
            //TODO FileUploaderDataBinding(sender as FileUploadControl2, CurrentProject.Logo);
            var uploader = (FileUploadControl2)sender;
            uploader.FileItemTemplate = "<a href=\"" + FileHandler.GetUrl(string.Empty) + "{id}\">{name}</a>";
            var logo = CurrentProject.Logo.FirstOrDefault();
            if (logo != null)
                uploader.SetValue(logo.FileName, logo.Id.ToString());
        }

        #endregion

        #region  UsersGrid

        protected void UsersGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            UsersGrid.DataSource = CurrentProject.InvolvedEmployees.Select(u => new { Id = u.Id, Name = u.FullName }).ToArray();
        }

        protected void UsersGrid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            int id = Convert.ToInt32(hf["InvolvedId"]);
            controller.CurrentEdit = CurrentProject;
            controller.AddInvolvedUser(id);
            UsersGrid.GridCancelEdit(e);
        }

        protected void UsersGrid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            int id = Convert.ToInt32(e.Keys[0]);
            controller.CurrentEdit = CurrentProject;
            controller.DeleteInvolvedUser(id);
            UsersGrid.GridCancelEdit(e);
        }

        protected void InvolvedComboBox_DataBinding(object sender, EventArgs e)
        {
            var data = controller.GetUsers();
            sender.SetDataComboBox(data).SetValueComboBox<int>(controller.CurrentEditUser.Return(u => u.Id));
        }

        #endregion

        private void BindUserTextBox(ASPxTextBox box, Entities.Admin.User user, string field)
        {
            if (user == null)
            {
                box.Text = "Не задано";
                hf.Set(field + "Id", -1);
                hf.Set(field + "Name", "Не задано");
            }
            else
            {
                box.Text = user.FullName;
                hf.Set(field + "Id", user.Id);
                hf.Set(field + "Name", user.Name);
            }
        }

        protected string GetResponsibleUrl()
        {
            return String.Format("../Selectors/UsersOne.aspx?id={0}&type=ResponsibleUser&department=all",
                CurrentProject.ResponsibleUser != null ? CurrentProject.ResponsibleUser.Id : 0);
        }

        protected string GetSignerUrl()
        {
            return String.Format("../Selectors/UsersOne.aspx?id={0}&type=Signer&department=all",
                CurrentProject.Signer != null ? CurrentProject.Signer.Id : 0);
        }
    }
}