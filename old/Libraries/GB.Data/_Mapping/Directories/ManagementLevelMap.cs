﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ManagementLevelMap : ClassMapBase<ManagementLevel>
	{
        public ManagementLevelMap()
            : base("Gov_Struct_Lvl")
		{
		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Gov_Struct_Lvl_Name").Nullable();
        }
    }
}