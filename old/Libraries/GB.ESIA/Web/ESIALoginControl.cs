﻿using System;
using System.Web.Security;
using GB.ESIA.Web.Helpers;

namespace GB.ESIA.Web
{
    public class ESIALoginControl : ESIAControlsBase
    {
        protected override void SetValues()
        {
            _Button.Text = LinkButtonText ?? "Авторизация через ЕСИА";
            if (!string.IsNullOrEmpty(CssClass))
                _Button.CssClass = CssClass;
            
        }

        protected override void LinkButtonClick(object sender, EventArgs e)
        {
            // Создание запроса на аутентификацию.
            var authenticationRequest = new AuthenticationRequest(Page, CertificateAccessor.ServiceProviderCert, ConfigAccessor.SingleSignOnServiceUrl, ConfigAccessor.AssertionServiceUrl(Page.Request));

            if (ConfigAccessor.DebugMode)
                Util.WriteLog("Temp/LastAuthRequest.xml", authenticationRequest.Xml.OuterXml);
            
            // Определение URL'а, на который следует перейти в случае успешной аутентификации.
            string urlToRedirect = Util.GetAbsoluteUrl(Page, FormsAuthentication.GetRedirectUrl(string.Empty, false));
            
            // Сохранение URL'а в кэше.
            Util.InsertIntoCache(authenticationRequest.Id, urlToRedirect, new TimeSpan(1, 0, 0));
            // Отправка запроса.
            authenticationRequest.Send();
        }
    }
}
