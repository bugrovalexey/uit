﻿using System;
using GB.Data.AccountingObjects;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class AccountingObjectCharacteristicsService : ServiceBase<AccountingObjectCharacteristics>,
        IService<AccountingObjectCharacteristics, AccountingObjectCharacteristicsArgs>
    {
        public AccountingObjectCharacteristics Create(AccountingObjectCharacteristicsArgs args)
        {
            var ao = new AccountingObjectCharacteristics(args.AccountingObject ??
                        GetEntity<AccountingObject>(args.AccountingObjectId));

            ao.IsRegister = args.IsRegister;

            return SaveOrUpdate(ao, args);
        }       

        public AccountingObjectCharacteristics Update(AccountingObjectCharacteristicsArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private AccountingObjectCharacteristics SaveOrUpdate(AccountingObjectCharacteristics charact, AccountingObjectCharacteristicsArgs args)
        {
            charact = ConvertDtoToEntity.FillAccountingObjectCharacteristicsFromDto(charact, args);
            base.SaveOrUpdate(charact);

            return charact;
        }

        public AccountingObjectCharacteristics Create(IAccountingObjectCharacteristicsActivityArgs args)
        {
            var ao = new AccountingObjectCharacteristics(GetEntity<AccountingObject>(args.AccountingObjectId));

            ao.IsRegister = args.IsRegister;

            return SaveOrUpdate(ao, args);
        }

        public AccountingObjectCharacteristics Update(IAccountingObjectCharacteristicsActivityArgs args)
        {
            var ao = Repository.GetExisting(args.CharacteristicsId);

            return SaveOrUpdate(ao, args);
        }

        private AccountingObjectCharacteristics SaveOrUpdate(AccountingObjectCharacteristics ao, IAccountingObjectCharacteristicsActivityArgs args)
        {
            ao.Fill(args);

            base.SaveOrUpdate(ao);

            return ao;
        }

        public void Delete(AccountingObjectCharacteristicsArgs args)
        {
            base.Delete(args.Id);
        }        
    }

    public interface IAccountingObjectCharacteristicsActivityArgs
    {
        string Name { get; set; }

        int? Type { get; set; }

        int? Unit { get; set; }

        bool IsRegister { get; set; }

        int AccountingObjectId { get; set; }

        int? CharacteristicsId { get; set; }
    }

    public class AccountingObjectCharacteristicsArgs : BaseArgsNew
    {
        public string Name { get; set; }

        public int? TypeId { get; set; }

        public int? UnitId { get; set; }

        /// <summary>Номинальное значение</summary>
        public int Norm { get; set; }

        /// <summary>Максимальное значение</summary>
        public int? Max { get; set; }

        public int? Fact { get; set; }

        public DateTime? DateBeginning { get; set; }

        public DateTime? DateEnd { get; set; }

        /// <summary>
        /// Зарегистрирована в ОУ
        /// </summary>
        public bool IsRegister { get; set; }

        public AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Id объекта учета
        /// </summary>
        public int AccountingObjectId { get; set; }
    }
}