﻿using GB.Data.AccountingObjects;


namespace GB.DataAccess.Services.Args
{
    public class SpecialCheckArgs : BaseArgsNew
    {
        /// <summary>
        /// Id объекта учета
        /// </summary>
        public int AccountingObjectId { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public string Files { get; set; }
    }
}
