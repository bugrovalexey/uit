﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201806230240)]
    public class Migration_201806230240 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("ClaimAct")
                .AddColumn("ClaimDocument_DateAdoption").AsDateTime().Nullable()
                .AddColumn("ClaimDocument_Cost").AsDecimal().Nullable();
        }
    }
}
