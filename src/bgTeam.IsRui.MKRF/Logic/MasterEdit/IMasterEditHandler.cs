﻿using System.Collections.Generic;

namespace bgTeam.IsRui.MKRF.Logic.MasterEdit
{
    public interface IMasterEditHandler
    {
        MasterItem First();

        MasterItem GetItem(MasterItemEnum e);
        IEnumerable<MasterItem> GetItems();
    }
}
