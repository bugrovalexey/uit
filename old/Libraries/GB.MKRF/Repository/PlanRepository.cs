﻿using GB.Data.Plans;

namespace GB.MKRF.Repository
{
    /// <summary>
    ///
    /// </summary>
    public class PlanRepository : Repository<Plan>
    {
        //        public static Plan GetPlan(int id)
        //        {
        //            Plan plan = null;
        //            Years year = null;
        //            Department department = null;
        //            var query = Session.QueryOver<Plan>(() => plan)
        //                               .JoinAlias(() => plan.Year, () => year)
        //                               .JoinAlias(() => plan.Department, () => department)
        //                               .Where(x => x.Id == id);
        //            return query.List<Plan>().FirstOrDefault();
        //        }

        //        public static PlansActivity GetActivity(int id)
        //        {
        //            PlansActivity planActivity = null;
        //            Plan plan = null;
        //            Years year = null;
        //            var query = Session.QueryOver<PlansActivity>(() => planActivity)
        //                               .JoinAlias(() => planActivity.Plan, () => plan)
        //                               .JoinAlias(() => plan.Year, () => year)
        //                               .Where(x => x.Id == id);
        //            return query.List<PlansActivity>().FirstOrDefault();
        //        }

        //        /// <summary>
        //        /// Получить список заявок дочерних ОГВ для сводного плана
        //        /// </summary>
        //        public static DataTable GetDepExpDemandList(int demandId)
        //        {
        //            string sql = @"DECLARE @Year SMALLINT
        //
        //SELECT  @Year = a.[Year]
        //FROM    dbo.w_Plans a
        //        JOIN dbo.w_Exp_Demand b ON b.Plans_ID = a.Plans_ID
        //WHERE   b.Exp_Demand_ID = @Obj_ID
        //
        //SELECT  z4.Exp_Demand_ID AS Id
        //FROM    dbo.w_Plans z1
        //        JOIN dbo.v_Govt_Organ_Tree (71) g ON g.Govt_Organ_ID = z1.Govt_Organ_ID
        //        JOIN dbo.w_Exp_Demand z4 ON z1.Plans_ID = z4.Plans_ID
        //WHERE   z1.[Year] = @Year";

        //            Dictionary<string, object> args = new Dictionary<string, object>();

        //            args.Add("@Users_ID", UserRepository.GetCurrent().Id);
        //            args.Add("@Obj_ID", demandId);

        //            return GetTableViaSQL(sql, args); ;
        //        }

        //        /// <summary>
        //        /// Возвращает список мероприятий плана без наложения секьюры
        //        /// </summary>
        //        /// <param name="plan"></param>
        //        /// <returns></returns>
        //        public static IList<PlansActivity> GetActivityListWithoutSecurity(Plan plan)
        //        {
        //            SecurityFilter.DisableFilter(Session);
        //            IList<PlansActivity> result = RepositoryBase<PlansActivity>.GetAll(x => x.Plan == plan);
        //            SecurityFilter.EnableFilter(Session, UserRepository.GetCurrent().Id);
        //            return result;
        //        }

        //        /// <summary>
        //        /// Получение списка мероприятий Плана информатизации (версия 2012 г.)
        //        /// </summary>
        //        /// <param name="plan"></param>
        //        public static DataTable GetActivityList2012new(Plan plan, int userId)
        //        {
        //            var sql = "SELECT * FROM [Get_My_All_Plans_Activity] (@UserId, @PlanId)";

        //            if (plan.PlanType == ExpObjectTypeEnum.Plan2012new)
        //                sql = string.Concat(sql, " ORDER BY Num");
        //            else if (plan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new)
        //                sql = string.Concat(sql, " ORDER BY Id");

        //                return RepositoryBase.GetTableViaSQL(sql,
        //                    new Dictionary<string, object>()
        //                {
        //                    { "@UserId", userId },
        //                    { "@PlanId", plan.Id }
        //                },
        //                "Activities");
        //        }

        //        public static DataTable GetSummaryActivityList(Plan plan, int userId)
        //        {
        //            var sql = "SELECT * FROM dbo.Get_My_All_Plans_Activity_Total(@UserId, @PlanId)";

        //            return RepositoryBase.GetTableViaSQL(sql,
        //                new Dictionary<string, object>()
        //                {
        //                    { "@UserId", userId },
        //                    { "@PlanId", plan.Id }
        //                });
        //        }

        //        public static DataTable GetSummaryByActivityTypes(Plan plan)
        //        {
        //            #region sql
        //            string sql = @"WITH    t1
        //          AS ( SELECT   t1.Plans_Activity_Type2_Name AS [Type] ,
        //                        ISNULL(t2.IsPriority, 0) AS IsPriority ,
        //                        SUM(ISNULL(Plans_Activity_P_Fact_Vol_Y0, 0)) AS Plans_Activity_P_Fact_Vol_Y0 ,
        //                        SUM(ISNULL(Plans_Activity_P_Fact_Vol_Y1, 0)) AS Plans_Activity_P_Fact_Vol_Y1 ,
        //                        SUM(ISNULL(Plans_Activity_P_Fact_Vol_Y2, 0)) AS Plans_Activity_P_Fact_Vol_Y2 ,
        //                        SUM(ISNULL(Plans_Activity_P_Vol_Y0, 0)) AS Plans_Activity_P_Vol_Y0 ,
        //                        SUM(ISNULL(Plans_Activity_P_Vol_Y1, 0)) AS Plans_Activity_P_Vol_Y1 ,
        //                        SUM(ISNULL(Plans_Activity_P_Vol_Y2, 0)) AS Plans_Activity_P_Vol_Y2 ,
        //                        SUM(ISNULL(Plans_Activity_P_Add_Vol_Y0, 0)) AS Plans_Activity_P_Add_Vol_Y0 ,
        //                        SUM(ISNULL(Plans_Activity_P_Add_Vol_Y1, 0)) AS Plans_Activity_P_Add_Vol_Y1 ,
        //                        SUM(ISNULL(Plans_Activity_P_Add_Vol_Y2, 0)) AS Plans_Activity_P_Add_Vol_Y2 ,
        //                        SUM(ISNULL(VolY0, 0)) AS VolY0 ,
        //                        SUM(ISNULL(VolY1, 0)) AS VolY1 ,
        //                        SUM(ISNULL(VolY2, 0)) AS VolY2
        //               FROM     dbo.w_Plans_Activity_Type2 t1
        //                        LEFT JOIN ( SELECT  b.Plans_Activity_Type2_ID AS ID ,
        //                                            a.Plans_Activity_Is_Priority AS IsPriority ,
        //                                            a.Plans_Activity_P_Fact_Vol_Y0 AS Plans_Activity_P_Fact_Vol_Y0 ,
        //                                            a.Plans_Activity_P_Fact_Vol_Y1 AS Plans_Activity_P_Fact_Vol_Y1 ,
        //                                            a.Plans_Activity_P_Fact_Vol_Y2 AS Plans_Activity_P_Fact_Vol_Y2 ,
        //                                            a.Plans_Activity_P_Vol_Y0 AS Plans_Activity_P_Vol_Y0 ,
        //                                            a.Plans_Activity_P_Vol_Y1 AS Plans_Activity_P_Vol_Y1 ,
        //                                            a.Plans_Activity_P_Vol_Y2 AS Plans_Activity_P_Vol_Y2 ,
        //                                            a.Plans_Activity_P_Add_Vol_Y0 AS Plans_Activity_P_Add_Vol_Y0 ,
        //                                            a.Plans_Activity_P_Add_Vol_Y1 AS Plans_Activity_P_Add_Vol_Y1 ,
        //                                            a.Plans_Activity_P_Add_Vol_Y2 AS Plans_Activity_P_Add_Vol_Y2 ,
        //                                            a.Plans_Activity_P_Vol_Y0
        //                                            + a.Plans_Activity_P_Add_Vol_Y0 AS VolY0 ,
        //                                            a.Plans_Activity_P_Vol_Y1
        //                                            + a.Plans_Activity_P_Add_Vol_Y1 AS VolY1 ,
        //                                            a.Plans_Activity_P_Vol_Y2
        //                                            + a.Plans_Activity_P_Add_Vol_Y2 AS VolY2
        //                                    FROM    dbo.w_Plans_Activity a
        //                                            JOIN dbo.w_Plans_Activity_Type2 b ON b.Plans_Activity_Type2_ID = a.Plans_Activity_Type2_ID
        //                                            JOIN dbo.w_Plans c ON c.Plans_ID = a.Plans_ID
        //                                    WHERE   c.Exp_Obj_Type_ID = 11
        //                                            AND c.Plans_ID = @PlanID
        //                                  ) t2 ON t2.ID = t1.Plans_Activity_Type2_ID
        //               WHERE    t1.Plans_Activity_Type2_ID != 0
        //               GROUP BY t2.IsPriority ,
        //                        t1.Plans_Activity_Type2_Name ,
        //                        t1.Plans_Activity_Type2_ID
        //               UNION
        //               SELECT   Plans_Activity_Type2_Name ,
        //                        0 AS Plans_Activity_Is_Priority ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0
        //               FROM     dbo.w_Plans_Activity_Type2
        //               WHERE    Plans_Activity_Type2_ID != 0
        //               UNION
        //               SELECT   Plans_Activity_Type2_Name ,
        //                        1 AS Plans_Activity_Is_Priority ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0 ,
        //                        0
        //               FROM     dbo.w_Plans_Activity_Type2
        //               WHERE    Plans_Activity_Type2_ID != 0
        //             )
        //    SELECT  [Type] ,
        //            CAST (IsPriority AS BIT) AS IsPriority ,
        //            SUM(Plans_Activity_P_Fact_Vol_Y0) AS Plans_Activity_P_Fact_Vol_Y0 ,
        //            SUM(Plans_Activity_P_Fact_Vol_Y1) AS Plans_Activity_P_Fact_Vol_Y1 ,
        //            SUM(Plans_Activity_P_Fact_Vol_Y2) AS Plans_Activity_P_Fact_Vol_Y2 ,
        //            SUM(Plans_Activity_P_Vol_Y0) AS Plans_Activity_P_Vol_Y0 ,
        //            SUM(Plans_Activity_P_Vol_Y1) AS Plans_Activity_P_Vol_Y1 ,
        //            SUM(Plans_Activity_P_Vol_Y2) AS Plans_Activity_P_Vol_Y2 ,
        //            SUM(Plans_Activity_P_Add_Vol_Y0) AS Plans_Activity_P_Add_Vol_Y0 ,
        //            SUM(Plans_Activity_P_Add_Vol_Y1) AS Plans_Activity_P_Add_Vol_Y1 ,
        //            SUM(Plans_Activity_P_Add_Vol_Y2) AS Plans_Activity_P_Add_Vol_Y2 ,
        //            SUM(VolY0) AS VolY0 ,
        //            SUM(VolY1) AS VolY1 ,
        //            SUM(VolY2) AS VolY2
        //    FROM    t1
        //    GROUP BY [Type] ,
        //            IsPriority
        //    ORDER BY [Type]";
        //            #endregion

        //            Dictionary<string, object> args = new Dictionary<string, object>();

        //            args.Add("@Users_ID", UserRepository.GetCurrent().Id);
        //            args.Add("@PlanID", plan.Id);

        //            DataTable dt = GetTableViaSQL(sql, args, "GetSummaryByActivityTypes");

        //            return dt;
        //        }

        //        public static DataTable GetCommonActivityList2012new(Plan plan)
        //        {
        //            #region sqlQuery
        //            string sql = @"DECLARE @Year SMALLINT
        //
        //                            SELECT  @Year = [Year]
        //                            FROM    dbo.w_Plans
        //                            WHERE   Plans_ID = @PlansID
        //
        //                            SELECT  a.Plans_Activity_ID AS Id ,
        //                                    a.Plans_Activity_Type_Name AS ActType ,
        //                                    ActCode = CASE WHEN a.Govt_Organ_Type_ID = 3
        //                                                   THEN REPLACE(a.Govt_Organ_SName + '.'
        //                                                                + a.Plans_Activity_Code, 'Минкомсвязь ',
        //                                                                '')
        //                                                   ELSE a.Plans_Activity_Code
        //                                              END ,
        //                                    a.Plans_Activity_Name AS Name ,
        //                                    a.Plans_Activity_Num AS Num ,
        //                                    a.Plans_Activity_Is_Priority AS IsPriority ,
        //                                    Plans_Activity_P_Vol_Y0_00 AS PlansActivityVolY0 ,
        //                                    Plans_Activity_P_Vol_Y1_01 AS PlansActivityVolY1 ,
        //                                    Plans_Activity_P_Vol_Y2_02 AS PlansActivityVolY2 ,
        //                                    Plans_Activity_P_Add_Vol_Y0_00 AS PlansActivityAddVolY0 ,
        //                                    Plans_Activity_P_Add_Vol_Y1_01 AS PlansActivityAddVolY1 ,
        //                                    Plans_Activity_P_Add_Vol_Y2_02 AS PlansActivityAddVolY2 ,
        //                                    Plans_Activity_P_Fact_Vol_Y0 AS PlansActivityFactVolY0 ,
        //                                    Plans_Activity_P_Fact_Vol_Y1 AS PlansActivityFactVolY1 ,
        //                                    Plans_Activity_P_Fact_Vol_Y2 AS PlansActivityFactVolY2 ,
        //                                    Plans_Activity_P_Vol_Y0_00 + Plans_Activity_P_Add_Vol_Y0_00 AS VolY0 ,
        //                                    Plans_Activity_P_Vol_Y1_01 + Plans_Activity_P_Add_Vol_Y1_01 AS VolY1 ,
        //                                    Plans_Activity_P_Vol_Y2_02 + Plans_Activity_P_Add_Vol_Y2_02 AS VolY2 ,
        //                                    CASE WHEN ae.Obj_ID IS NULL THEN 0
        //                                         ELSE 1
        //                                    END + CASE WHEN ad.Obj_ID IS NULL THEN 0
        //                                               ELSE 2
        //                                          END AS Composite
        //                            FROM    dbo.v_Plans_Activity a
        //                                    JOIN dbo.w_Plans d ON d.Plans_ID = a.Plans_ID
        //                                    JOIN dbo.v_Govt_Organ_Tree (71) c ON c.Govt_Organ_ID = d.Govt_Organ_ID
        //                                    JOIN dbo.Get_Object_List(@Users_ID, 8, 'r') ar ON a.Plans_Activity_ID = ar.Obj_ID
        //                                    LEFT JOIN dbo.Get_Object_List(@Users_ID, 8, 'e') ae ON a.Plans_Activity_ID = ae.Obj_ID
        //                                    LEFT JOIN dbo.Get_Object_List(@Users_ID, 8, 'd') ad ON a.Plans_Activity_ID = ad.Obj_ID
        //                            WHERE   d.[Year] = @Year
        //                            ORDER BY a.Plans_Activity_Num";
        //            #endregion

        //            Dictionary<string, object> args = new Dictionary<string, object>();

        //            args.Add("@Users_ID", UserRepository.GetCurrent().Id);
        //            args.Add("@PlansID", plan.Id);

        //            DataTable dt = GetTableViaSQL(sql, args, "Activities");

        //            NormalizeActivitiNum(dt);

        //            return dt;
        //        }

        //        /// <summary>
        //        /// Получить список мероприятий дочерних ОГВ для сводного плана
        //        /// </summary>
        //        public static DataTable GetDepActivityList2012new(Plan plan)
        //        {
        //            #region sqlQuery
        //            string sql = @"DECLARE @Year SMALLINT
        //
        //SELECT  @Year = [Year]
        //FROM    dbo.w_Plans
        //WHERE   Plans_ID = @PlansID
        //
        //SELECT  a.Plans_Activity_ID AS Id ,
        //        a.Plans_Activity_Num AS ActivityNum ,
        //        dbo.Get_Activity_Status(a.Plans_Activity_ID) AS fillStatus ,
        //        ActCode = CASE WHEN a.Govt_Organ_Type_ID = 3
        //                       THEN REPLACE(a.Govt_Organ_SName + '.'
        //                                    + a.Plans_Activity_Code, 'Минкомсвязь ',
        //                                    '')
        //                       ELSE a.Plans_Activity_Code
        //                  END ,
        //        a.Plans_Activity_Type_Name AS ActivityType ,
        //        a.Plans_Activity_Name AS Name ,
        //        a.Plans_Activity_Num AS Num ,
        //        ~a.Plans_Activity_Is_Priority AS IsNotPriority ,
        //        a.Plans_Activity_Is_Priority AS IsPriority ,
        //        a.Plans_Activity_Expense_Vol_FB AS ExpenseVolumeFB ,
        //        a.Plans_Activity_Expense_Vol_RB AS ExpenseVolumeRB ,
        //        a.Plans_Activity_Expense_Vol_VB AS ExpenseVolumeVB ,
        //        a.Plans_Spec_Expense_Vol_FB AS ExpenseVolumeFB_S ,
        //        a.Plans_Spec_Expense_Vol_RB AS ExpenseVolumeRB_S ,
        //        a.Plans_Spec_Expense_Vol_VB AS ExpenseVolumeVB_S ,
        //        a.Plans_Work_Expense_Vol_FB AS ExpenseVolumeFB_W ,
        //        a.Plans_Work_Expense_Vol_RB AS ExpenseVolumeRB_W ,
        //        a.Plans_Work_Expense_Vol_VB AS ExpenseVolumeVB_W ,
        //        a.Plans_Activity_Expense_Vol_Itogo AS SummaryExpense ,
        //        a.Govt_Organ_Name AS DepartmentName ,
        //        Plans_Activity_P_Vol_Y0_00 AS PlansActivityVolY0 ,
        //        Plans_Activity_P_Vol_Y1_01 AS PlansActivityVolY1 ,
        //        Plans_Activity_P_Vol_Y2_02 AS PlansActivityVolY2 ,
        //        Plans_Activity_P_Add_Vol_Y0_00 AS PlansActivityAddVolY0 ,
        //        Plans_Activity_P_Add_Vol_Y1_01 AS PlansActivityAddVolY1 ,
        //        Plans_Activity_P_Add_Vol_Y2_02 AS PlansActivityAddVolY2 ,
        //        Plans_Activity_P_Fact_Vol_Y0 AS PlansActivityFactVolY0 ,
        //        Plans_Activity_P_Fact_Vol_Y1 AS PlansActivityFactVolY1 ,
        //        Plans_Activity_P_Fact_Vol_Y2 AS PlansActivityFactVolY2 ,
        //        Plans_Activity_P_Vol_Y0_00 + Plans_Activity_P_Add_Vol_Y0_00 AS VolY0 ,
        //        Plans_Activity_P_Vol_Y1_01 + Plans_Activity_P_Add_Vol_Y1_01 AS VolY1 ,
        //        Plans_Activity_P_Vol_Y2_02 + Plans_Activity_P_Add_Vol_Y2_02 AS VolY2 ,
        //        Plans_Spec_Assgn_Vol_Y0 AS PlansSpecAssgnVolY0 ,
        //        Plans_Spec_Assgn_Vol_Y1 AS PlansSpecAssgnVolY1 ,
        //        Plans_Spec_Assgn_Vol_Y2 AS PlansSpecAssgnVolY2 ,
        //        Plans_Work_Assgn_Vol_Y0 AS PlansWorkAssgnVolY0 ,
        //        Plans_Work_Assgn_Vol_Y1 AS PlansWorkAssgnVolY1 ,
        //        Plans_Work_Assgn_Vol_Y2 AS PlansWorkAssgnVolY2 ,
        //        CASE WHEN ae.Obj_ID IS NULL THEN 0
        //             ELSE 1
        //        END + CASE WHEN ad.Obj_ID IS NULL THEN 0
        //                   ELSE 2
        //              END AS Composite ,
        //        c.Status_Name AS StatusName ,
        //        t.Plans_Activity_Type2_Name AS Type ,
        //        i.IKT_Component_Code + ' ' + i.IKT_Component_Name AS IKT_Component_Code_Name ,
        //        ISNULL(wua.Users_Activity_Is_Favourite, 0) AS IsFavor ,
        //        CASE WHEN s.Show_Objects_ID IS NULL THEN CAST(0 AS BIT)
        //             ELSE CAST(1 AS BIT)
        //        END AS IsShow ,
        //        ( SELECT    SUM(ISNULL(b1.Plans_Activity_ED_VB_Y0, 0)
        //                        + ISNULL(b1.Plans_Activity_ED_Add_Y0, 0))
        //          FROM      dbo.w_Plans_Activity_ED b1
        //          WHERE     b1.Plans_Activity_ID = a.Plans_Activity_ID
        //        ) AS ED_Y0 ,
        //        ( SELECT    SUM(ISNULL(b1.Plans_Activity_ED_VB_Y1, 0)
        //                        + ISNULL(b1.Plans_Activity_ED_Add_Y1, 0))
        //          FROM      dbo.w_Plans_Activity_ED b1
        //          WHERE     b1.Plans_Activity_ID = a.Plans_Activity_ID
        //        ) AS ED_Y1 ,
        //        ( SELECT    SUM(ISNULL(b1.Plans_Activity_ED_VB_Y2, 0)
        //                        + ISNULL(b1.Plans_Activity_ED_Add_Y2, 0))
        //          FROM      dbo.w_Plans_Activity_ED b1
        //          WHERE     b1.Plans_Activity_ID = a.Plans_Activity_ID
        //        ) AS ED_Y2
        //FROM    dbo.v_Plans_Activity a
        //        JOIN dbo.w_Plans d ON d.Plans_ID = a.Plans_ID
        //        JOIN dbo.w_Exp_Demand b ON a.Plans_ID = b.Plans_ID
        //        JOIN dbo.w_Status c ON c.Status_ID = b.Status_ID
        //        JOIN dbo.v_Govt_Organ_Tree (71) g ON g.Govt_Organ_ID = d.Govt_Organ_ID
        //        LEFT JOIN dbo.w_Users_Activity wua ON wua.Plans_Activity_ID = a.Plans_Activity_ID
        //                                              AND wua.Users_ID = @Users_ID
        //        JOIN dbo.w_IKT_Component i ON i.IKT_Component_ID = a.IKT_Component_ID
        //        LEFT JOIN dbo.w_Plans_Activity_Type2 t ON t.Plans_Activity_Type2_ID = a.Plans_Activity_Type2_ID
        //        JOIN dbo.Get_Object_List(@Users_ID, 8, 'r') ar ON a.Plans_Activity_ID = ar.Obj_ID
        //        LEFT JOIN dbo.Get_Object_List(@Users_ID, 8, 'e') ae ON a.Plans_Activity_ID = ae.Obj_ID
        //        LEFT JOIN dbo.Get_Object_List(@Users_ID, 8, 'd') ad ON a.Plans_Activity_ID = ad.Obj_ID
        //        LEFT JOIN dbo.w_Show_Objects s ON a.Plans_Activity_ID = s.Show_Objects_Entity
        //                                          AND s.Show_Objects_Entity_Type = 1
        //                                          AND s.Users_ID = @Users_ID
        //WHERE   d.[Year] = @Year
        //ORDER BY Plans_Activity_Num
        //";
        //            #endregion

        //            Dictionary<string, object> args = new Dictionary<string, object>();

        //            args.Add("@Users_ID", UserRepository.GetCurrent().Id);
        //            args.Add("@PlansID", plan.Id);

        //            DataTable dt = GetTableViaSQL(sql, args, "Activities");

        //            //NormalizeActivitiNum(dt);

        //            return dt;
        //        }

        //        /// <summary>
        //        /// Нормализовать номер мероприятия
        //        /// </summary>
        //        [Obsolete]
        //        public static void NormalizeActivitiNum(DataTable dt)
        //        {
        //            if (dt != null)
        //            {
        //                DataColumn column = dt.Columns.Add("ActivityNum");
        //                DataColumn numColumn = dt.Columns["Num"];
        //                DataColumn priorityColumn = dt.Columns["IsPriority"];

        //                foreach (DataRow row in dt.Rows)
        //                {
        //                    int Num = (int)row[numColumn];
        //                    row[column] = priorityColumn != null && !(row[priorityColumn] is DBNull) ? GetActivityNumber((bool)row[priorityColumn], Num) : Num.ToString();
        //                    //row[column] = GetActivityNumber((bool)row[priorityColumn], Num);
        //                }
        //            }
        //        }

        //        /// <summary>
        //        /// Собрать обозначение мероприятия из приоритета и номера
        //        /// </summary>
        //        /// <param name="priority"></param>
        //        /// <param name="num"></param>
        //        /// <returns></returns>
        //        static string GetActivityNumber(bool priority, int num)
        //        {
        //            return String.Concat(priority ? "1." : "2.", num.ToString());
        //        }

        //        #region History
        //        /// <summary>
        //        /// Получить список версий объекта экспертизы
        //        /// </summary>
        //        /// <param name="Id">Ид объекта</param>
        //        /// <returns>Версии</returns>
        //        public static DataTable GetHistory(Plan plan, ExpObjectTypeEnum type)
        //        {
        //            #region sql
        //            string sql =
        //@"SELECT  Num ,
        //        Date_Time ,
        //        DT ,
        //        Status_Name ,
        //        Plans_Expense_Vol_FB ,
        //        Plans_Expense_Vol_RB ,
        //        Plans_Expense_Vol_VB ,
        //        Plans_Expense_Vol_Itogo ,
        //        User_Full_Info ,
        //        Plans_Ver_Act_P_Vol_FB_Y0 ,
        //        Plans_Ver_Act_P_Vol_VB_Y0 ,
        //        Plans_Ver_Act_P_Add_Vol_FB_Y0 ,
        //        Plans_Ver_Act_P_Add_Vol_VB_Y0 ,
        //        Plans_Ver_Act_P_Vol_FB_Y1 ,
        //        Plans_Ver_Act_P_Vol_VB_Y1 ,
        //        Plans_Ver_Act_P_Add_Vol_FB_Y1 ,
        //        Plans_Ver_Act_P_Add_Vol_VB_Y1 ,
        //        Plans_Ver_Act_P_Vol_FB_Y2 ,
        //        Plans_Ver_Act_P_Vol_VB_Y2 ,
        //        Plans_Ver_Act_P_Add_Vol_FB_Y2 ,
        //        Plans_Ver_Act_P_Add_Vol_VB_Y2 ,
        //        Plans_Ver_Act_P_Vol_Itogo ,
        //        Plans_Ver_Calc_Act_P_Vol_FB_Y0 ,
        //        Plans_Ver_Calc_Act_P_Vol_VB_Y0 ,
        //        Plans_Ver_Calc_Act_P_Add_Vol_FB_Y0 ,
        //        Plans_Ver_Calc_Act_P_Add_Vol_VB_Y0 ,
        //        Plans_Ver_Calc_Act_P_Vol_FB_Y1 ,
        //        Plans_Ver_Calc_Act_P_Vol_VB_Y1 ,
        //        Plans_Ver_Calc_Act_P_Add_Vol_FB_Y1 ,
        //        Plans_Ver_Calc_Act_P_Add_Vol_VB_Y1 ,
        //        Plans_Ver_Calc_Act_P_Vol_FB_Y2 ,
        //        Plans_Ver_Calc_Act_P_Vol_VB_Y2 ,
        //        Plans_Ver_Calc_Act_P_Add_Vol_FB_Y2 ,
        //        Plans_Ver_Calc_Act_P_Add_Vol_VB_Y2 ,
        //        Plans_Ver_Calc_Act_P_Vol_Itogo
        //FROM    v_Plans_Ver a
        //        JOIN dbo.w_Exp_Demand b ON b.Plans_ID = a.Plans_ID
        //WHERE   a.Plans_ID = @PlanId
        //        AND Exp_Obj_Type_ID = @ExpType
        //        AND ( Govt_Organ_ID = @Govt_Organ_ID
        //              OR Status_ID = 610)";
        //            #endregion

        //            User currentUser = UserRepository.GetCurrent();
        //            Dictionary<string, object> args = new Dictionary<string, object>();

        //            args.Add("@PlanId", plan.Id);
        //            args.Add("@ExpType", (int)type);

        //            // TODO: Вынести в валидатор!
        //            if (currentUser.Department.Type.Id == (int)DepartmentTypeEnum.Department)
        //            {
        //                // для департаментов отображаем историю только по своему огв
        //                args.Add("@Govt_Organ_ID", currentUser.Department.Id);
        //            }
        //            else
        //            {
        //                // для остальных - историю по огв плана
        //                args.Add("@Govt_Organ_ID", plan.Department.Id);
        //            }
        //            return GetTableViaSQL(sql, args);
        //        }

        //        /// <summary>
        //        /// Получение история мероприятий плана 2012new
        //        /// </summary>
        //        /// <param name="plan">План</param>
        //        /// <param name="date">Дата</param>
        //        /// <returns></returns>
        //        public static DataTable GetHistoryNewActivityList(Plan plan, DateTime date)
        //        {
        //            string sql = @"
        //SELECT  ZZZ_ID_Plans_Activity AS HistoryId ,
        //        a.Plans_Activity_ID AS Id ,
        //        a.Plans_Activity_Name AS Name ,
        //        Plans_Activity_ID ,
        //        Plans_Activity_Expense_Vol_FB ,
        //        Plans_Activity_Expense_Vol_RB ,
        //        Plans_Activity_Expense_Vol_VB ,
        //        Plans_Activity_Expense_Vol_Itogo ,
        //        a.Plans_Activity_Num AS Num ,
        //        a.Plans_Activity_Code AS Code ,
        //        IKT_Component_Name AS IKTComponent ,
        //        STUFF((SELECT   ISNULL(CHAR(10) + CHAR(10) + c1.Expense_Direction_Name,
        //                               '')
        //               FROM     dbo.wh_Plans_Activity_ED(@dt) b1
        //                        JOIN dbo.w_Expense_Direction c1 ON b1.Expense_Direction_ID = c1.Expense_Direction_ID
        //               WHERE    a.Plans_Activity_ID = b1.Plans_Activity_ID
        //               ORDER BY 1
        //        FOR   XML PATH('') ,
        //                  TYPE).value('.', 'varchar(max)'), 1, 1, '') AS ExpenseDirection ,
        //        STUFF((SELECT   ISNULL(CHAR(10) + CHAR(10) + d2.Func_Type_Name
        //                               + ISNULL(','
        //                                        + b2.Plans_Activity_Func_Functionality,
        //                                        '') + ',' + c2.NPA_Info, '')
        //               FROM     dbo.wh_Plans_Activity_Func(@dt) b2
        //                        JOIN dbo.vh_NPA(@dt) c2 ON b2.NPA_ID = c2.NPA_ID
        //                        JOIN dbo.w_Func_Type d2 ON b2.Func_Type_ID = d2.Func_Type_ID
        //               WHERE    a.Plans_Activity_ID = b2.Plans_Activity_ID
        //               ORDER BY 1
        //        FOR   XML PATH('') ,
        //                  TYPE).value('.', 'varchar(max)'), 1, 1, '') AS ActivityFuncs ,
        //       -- STUFF((SELECT   ISNULL(CHAR(10) + CHAR(10) + c3.NPA_Info, '')
        //       --        FROM     dbo.wh_Plans_Activity_Reason(@dt) b3
        //       --                 JOIN dbo.vh_NPA(@dt) c3 ON b3.NPA_ID = c3.NPA_ID
        //       --        WHERE    a.Plans_Activity_ID = b3.Plans_Activity_ID
        //       --        ORDER BY 1
        //       -- FOR   XML PATH('') ,
        //       --           TYPE).value('.', 'varchar(max)'), 1, 1, '') AS ActivityReasons ,
        //        a.FGIS_Status_ID
        //                                   --,a.FGIS_Status_Name as FGISStatus
        //        ,
        //        PlansActivityVolY0 ,
        //        PlansActivityVolY1 ,
        //        PlansActivityVolY2 ,
        //        PlansActivityAddVolY0 ,
        //        PlansActivityAddVolY1 ,
        //        PlansActivityAddVolY2 ,
        //        PlansActivityFactVolY0 ,
        //        PlansActivityFactVolY1 ,
        //        PlansActivityFactVolY2 ,
        //        VolY0 ,
        //        VolY1 ,
        //        VolY2 ,
        //        ~IsNotPriority AS IsPriority ,
        //        [Type]
        //FROM    dbo.vh_Plans_Activity(@dt) a
        //WHERE   a.plans_id = @PlanId
        //ORDER BY a.Plans_Activity_Num";

        //            //                            join    dbo.Get_Object_List(@UsersId, 8, 'r') ar
        //            //                                    on a.Plans_Activity_ID = ar.Obj_ID

        //            Dictionary<string, object> args = new Dictionary<string, object>();

        //            args.Add("@UsersId", UserRepository.GetCurrent().Id);
        //            args.Add("@PlanId", plan.Id);
        //            args.Add("@dt", date);

        //            //return GetTableViaSQL(sql, args);

        //            DataTable dt = GetTableViaSQL(sql, args);

        //            NormalizeActivitiNum(dt);

        //            return dt;
        //        }
        //        #endregion

        //        public static bool IsExceed(Plan plan)
        //        {
        //            object result = RepositoryBase<EntityBase>.GetValueViaSQL("select dbo.Get_Limit (@PlanID)",
        //                                                          new Dictionary<string, object>()
        //                                                                {
        //                                                                    {"@PlanID", plan.Id },
        //                                                                });

        //            return (bool)result;
        //        }

        //        public static XmlDocument GetActivityListXml(int PlanId)
        //        {
        //            Dictionary<string, object> pars = new Dictionary<string, object>()
        //             {
        //                 {"@PlanId", PlanId}
        //             };
        //            return RepositoryBase.GetXmlViaSQL("exec dbo.Get_Plans_Export_XML @PlanId", pars);
        //        }

        //        public static XmlDocument GetSummaryActivityListXml(int PlanId, int? priority = null, string sourceFundingIds = null, string activityTypeIds = null)
        //        {
        //            Dictionary<string, object> pars = new Dictionary<string, object>()
        //             {
        //                 {"@PlanId", PlanId},
        //                 {"@isPriority", (object) priority ?? DBNull.Value},
        //                 {"@Source", (object) sourceFundingIds ?? DBNull.Value},
        //                 {"@ActivityType", (object) activityTypeIds ?? DBNull.Value}
        //             };
        //            return RepositoryBase.GetXmlViaSQL("exec dbo.Get_Plans_Svod_Export_XML @PlanId, @isPriority, @Source, @ActivityType", pars);
        //        }

        //        public static XmlDocument GetActivityMUListXml(int PlanId)
        //        {
        //            Dictionary<string, object> pars = new Dictionary<string, object>()
        //             {
        //                 {"@PlanId", PlanId}
        //             };
        //            return RepositoryBase.GetXmlViaSQL("exec dbo.Get_MU_Export_XML @PlanId", pars);
        //        }

        //        public static XmlDocument GetPlansScheduleListXml(int PlanId)
        //        {
        //            Dictionary<string, object> pars = new Dictionary<string, object>()
        //             {
        //                 {"@PlanId", PlanId}
        //             };
        //            return RepositoryBase.GetXmlViaSQL("exec dbo.Get_Schedule_Export_XML @PlanId", pars);
        //        }

        //        public static XmlDocument GetPlansGovStructRolesListXml(int PlanId)
        //        {
        //            Dictionary<string, object> pars = new Dictionary<string, object>()
        //             {
        //                 {"@PlanId", PlanId}
        //             };
        //            return RepositoryBase.GetXmlViaSQL("exec dbo.Get_Gov_Struct_Export_XML @PlanId", pars);
        //        }

        //        /// <summary>
        //        /// Перевод мероприятия в новый департамент.
        //        /// Мероприятие переводится только в тот же год.
        //        /// Если в новом департаменте нет плана на этот год, то создаётся новый план с ответственным, который передаётся.
        //        /// </summary>
        //        /// <param name="activityId">Мероприятие</param>
        //        /// <param name="newDepartamentId">Департамент</param>
        //        /// <param name="userId">Ответственный</param>
        //        /// <returns>План в который переводится мероприятие</returns>
        //        public static int ChangeDepartament(int activityId, int newDepartamentId, int userId)
        //        {
        //            Dictionary<string, object> pars = new Dictionary<string, object>()
        //             {
        //                 {"@Plans_Activity_ID", activityId},
        //                 {"@Govt_Organ_ID", newDepartamentId},
        //                 {"@Users_ID", userId}
        //             };
        //            var obj = GetValueViaSQL("exec dbo.Prc_Copy_Plans_Activity @Plans_Activity_ID, @Govt_Organ_ID, @Users_ID", pars);
        //            int result = 0;
        //            if(obj == null || !Int32.TryParse(obj.ToString(), out result))
        //                throw new ApplicationException("Ошибка перевода мероприятия в другой департамент процедура вернула " + obj);

        //            return result;
        //        }

        //        internal static DataTable GetListUtvergdeno(int depId)
        //        {
        //            var sql = "SELECT * FROM dbo.w_Get_My_All_Plans_Utvergdeno where DepartmentId != @depId";

        //            Dictionary<string, object> pars = new Dictionary<string, object>()
        //            {
        //                {"@depId", depId}
        //            };

        //            return RepositoryBase.GetTableViaSQL(sql, pars);
        //        }
    }
}