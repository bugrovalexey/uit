﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class IKTObjectMap : ClassMapBase<IKTObject>
    {
        public IKTObjectMap()
            : base("IKT_Object")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "IKT_Object_Name");
            Map(x => x.Number, "IKT_Object_Number");
            Map(x => x.ShortName, "IKT_Object_Short_Name");
            Map(x => x.CreationDate, "IKT_Object_Creation_Date");
            Map(x => x.DateStart, "IKT_Object_Start_Date");
            Map(x => x.DateEnd, "IKT_Object_End_Date");

            References(x => x.Status, "IKT_Object_Status_ID");
        }
    }
}