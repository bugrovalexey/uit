﻿namespace bgTeam.IsRui.Domain.Entities.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Workflow;

    public class Workflow : EntityBase
    {
        public virtual Status Status { get; set; }

        public virtual int EntityType { get; set; }

        public virtual IEntity EntityOwner { get; set; }
    }
}
