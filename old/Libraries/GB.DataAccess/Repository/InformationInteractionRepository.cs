﻿using System.Collections.Generic;
using GB.Data.AccountingObjects;

namespace GB.DataAccess.Repository
{
    public class InformationInteractionRepository : Repository<InformationInteraction>
    {
        public IList<InformationInteraction> GetAllByAccountingObject(int? accountingObjectId)
        {
            return GetAllEx(q => q
                .Fetch(x => x.Type).Eager
                .Where(x => x.AccountingObject.Id == accountingObjectId));
        }
    }
}