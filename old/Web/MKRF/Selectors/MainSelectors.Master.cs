﻿using System;

namespace GB.MKRF.Web.Selectors
{
    public partial class MainSelectors : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // По умолчанию к страницам выбора значений справочника доступ открыт всем ролям.
            // Если требуется ограничить доступ, это нужно делать на каждой странице отдельно и желательно путём фильтрации данных.
        }
    }
}