﻿using GB.Data.AccountingObjects.WorksAndGoods;

namespace GB._Mapping.AccountingObjects.WorksAndGoods
{
    internal class TechnicalSupportMap : ClassMapHiLo<TechnicalSupport>
    {
        public TechnicalSupportMap()
            : base("OU_Technical")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Technical_Name").Not.Nullable();
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
            References(x => x.CategoryKindOfSupport, "VO_Category_ID").Not.Nullable();
            References(x => x.Manufacturer, "Producer_ID").Not.Nullable();
            Map(x => x.Amount, "OU_Technical_Count").Not.Nullable();
            Map(x => x.Summa, "OU_Technical_Sum").Not.Nullable();

            HasMany(x => x.Characteristics).KeyColumn("OU_Technical_ID").Inverse().Cascade.Delete();
        }
    }
}