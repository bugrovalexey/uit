﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    using System.ComponentModel;

    public enum ActivityTypeEnum
    {
        [Description("Не задано")]
        None = 0,

        [Description("Создание")]
        Create = 1,

        [Description("Развитие")]
        Develop = 2,

        [Description("Эксплуатация")]
        Exploitation = 4
    }
}