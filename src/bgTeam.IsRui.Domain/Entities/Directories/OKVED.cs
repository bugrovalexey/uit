﻿namespace bgTeam.IsRui.Domain.Entities.Directories
{
    /// <summary>
    /// ОКВЭД
    /// </summary>
    public class OKVED : EntityDirectories, IEntityCodeName
    {
        /// <summary>Код</summary>
        public virtual string Code { get; set; }

        /// <summary>Средняя зарплата</summary>
        public virtual decimal Salary { get; set; }

        /// <summary>Код и наименование</summary>
        public virtual string CodeName { get { return EntityHelper.GetCodeName(this); } }
    }
}