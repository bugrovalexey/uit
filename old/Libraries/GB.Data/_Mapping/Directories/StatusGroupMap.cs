﻿using Core;
using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class StatusGroupMap : ClassMapBase<StatusGroup>
	{
        public StatusGroupMap()
            : base("Status_Group")
		{
//            Table("w_");

//            Id(x => x.Id, "Status_Group_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            SqlInsert(@"exec ZZZ_Prc_Status_Group_Insert
//						  @Status_Group_Name = ?
//						 ,@Status_Group_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Status_Group_Update
//						  @Status_Group_Name = ?
//						 ,@Status_Group_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Status_Group_Delete
//						  @Status_Group_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Status_Group_Name").Nullable();
        }
    }
}
