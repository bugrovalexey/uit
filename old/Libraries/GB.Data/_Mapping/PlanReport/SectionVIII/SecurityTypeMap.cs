﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
    public class SecurityTypeMap : ClassMapBase<SecurityType>
    {
        public SecurityTypeMap()
            : base("PRS_8_5_Type")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_5_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_8_5_Type_Name = ? 
//						  ,@PRS_8_5_Type_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_5_Type_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_5_Type_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_5_Type_Delete @PRS_8_5_Type_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "PRS_8_5_Type_Name");
        }
    }
}