﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using GB;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.AccountingObjects;
using GB.MKRF.Repository;
using GB.Repository;

namespace GB.MKRF.Web.AccountingObjects
{
    public class BasePageAccountingObject : Page
    {
        #region Fields

        private AccountingObjectService _accountingObjectService;
        private IKTComponentRepository _iktComponentRepository;
        private DocumentAccountingObjectController _documentController;

        #endregion Fields

        #region Props
        public int? AccountingObjectId
        {
            get { return (Master as EditCard).AccountingObjectId; }
        }

        public AccountingObjectService AccountingObjectService
        {
            get
            {
                if (_accountingObjectService == null)
                {
                    _accountingObjectService = new AccountingObjectService();
                }

                return _accountingObjectService;
            }
        }

        public DocumentAccountingObjectController DocumentController
        {
            get
            {
                if (_documentController == null)
                {
                    _documentController = new DocumentAccountingObjectController();
                }

                return _documentController;
            }
        }

        /// <summary>
        /// Текущий объект учета
        /// </summary>
        public AccountingObject CurrentAccountingObject
        {
            get { return (Master as EditCard).CurrentAccountingObject; }
        }

        public IKTComponentRepository IKTComponentRepository
        {
            get
            {
                if (_iktComponentRepository == null)
                {
                    _iktComponentRepository = new IKTComponentRepository();
                }

                return _iktComponentRepository;
            }
        }


        #endregion Props



        #region Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            CheckSecurity();
        }

        #endregion Handlers

        #region  Helpers

        private void CheckSecurity()
        {
            if (AccountingObjectId.HasValue == false)
            {
                Logger.Info("Попытка редактировать информацию об объекте учета с некорректным Id = <{0}>",
                    Request.QueryString[EditCard.AccountingObjectIdKey]);
                ResponseHelper.DenyAccess(this.Response);
            }

            if (CurrentAccountingObject == null)
            {
                Logger.Info("Не найден AccountingObject с Id = <{0}>",
                     Request.QueryString[EditCard.AccountingObjectIdKey]);
                ResponseHelper.DenyAccess(this.Response);
            }
        }

        #endregion Helpers

    }
}