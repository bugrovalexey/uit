﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace GB.Extentions
{
    public static class DateExtensions
    {
        public static string ToStringOrDefault(this DateTime? date, string defaultResult = null,
            string format = "dd.MM.yyyy")
        {
            return date.Return(d => d.ToString(format), defaultResult);
        }


        public static string ToDisplay(this DateTime? date)
        {
            return date.Return(
                    d =>
                        string.Format("{0}, {1}",
                            d.ToString("ddd", CultureInfo.CreateSpecificCulture("ru-RU")),
                                d.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))));
        }
    }
}
