﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.Extensions;

namespace GB.DataAccess.Command.AccountingObjects.UserComments
{
    public class AddUserCommentCommand : ICommand<AddUserCommentContext, int>
    {
        ISessionFactoryGb factory;

        public AddUserCommentCommand()
        {
            factory = new SessionFactory();
        }

        public int Execute(AddUserCommentContext context)
        {
            AccountingObjectUserComment comment;

            if (context.xId.HasValue)
            {
                comment = new Repository<AccountingObjectUserComment>()
                    .Get(context.xId.Value);
            }
            else
            {
                var currentUser = UserRepository.GetCurrent();

                comment = new AccountingObjectUserComment();
                comment.User = currentUser;
                comment.CreateDate = DateTime.Now;
            }

            AutoMapper.Mapper.Map(context, comment);

            using (var session = factory.Create())
            {
                session.SaveOrUpdate(comment);
                session.Commit();
            }

            return comment.Id;
        }
    }
}