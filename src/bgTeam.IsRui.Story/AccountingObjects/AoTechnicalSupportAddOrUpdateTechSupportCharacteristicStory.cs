﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Threading.Tasks;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
    using bgTeam.IsRui.Domain.Entities.Directories;

    public class AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStory : IStory<AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStoryContext, TechnicalSupportAddOrUpdateTechSupportCharacteristicDto>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStory(ISessionNHibernateFactory factory, IRepositoryRui repository, IMapperBase mapper)
        {
            _factory = factory;
            _repository = repository;
            _mapper = mapper;
        }

        public TechnicalSupportAddOrUpdateTechSupportCharacteristicDto Execute(AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<TechnicalSupportAddOrUpdateTechSupportCharacteristicDto> ExecuteAsync(AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStoryContext context)
        {
            CharacteristicOfTechnicalSupport characteristic;

            Units units = _repository.Get<Units>(context.UnitId);

            TechnicalSupport technicalSupport;

            using (var session = _factory.OpenSession())
            {
                    if (context.TechnicalSupportId.HasValue)
                    {
                        technicalSupport = _repository.Get<TechnicalSupport>(context.TechnicalSupportId);
                    }
                    else
                    {
                        technicalSupport = null;
                    }

                    if (context.Id.HasValue)
                    {
                        characteristic = _repository.Get<CharacteristicOfTechnicalSupport>(context.Id);
                    }
                    else
                    {
                        characteristic = new CharacteristicOfTechnicalSupport();
                    }

                    _mapper.Map(context, characteristic);

                    characteristic.Unit = units;

                    characteristic.TechnicalSupport = technicalSupport;

                    await session.SaveOrUpdateAsync(characteristic);
            }

            return _mapper.Map(characteristic, new TechnicalSupportAddOrUpdateTechSupportCharacteristicDto());
        }
    }
}
