﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class ManufacturerMap : ClassMapHiLo<Manufacturer>
    {
        public ManufacturerMap()
            : base("Producer")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Producer_Name");
        }
    }
}