﻿using System.Collections.Specialized;
using System.Web;

namespace GB.Helpers
{
    public class UrlHelper
    {
        /// <summary>
        /// Преобразует виртуальный путь в абсолютный путь приложения.
        /// </summary>
        /// <param name="url">Относительный путь</param>
        public static string ToAbsolute(string url)
        {
            return VirtualPathUtility.ToAbsolute(url);
        }

        public static string Resolve(string RelativeUrl)
        {
            return UrlHelper.Current.resolveInternal(RelativeUrl);
        }

        public static string Link(string name, string relativeUrl, string par = null, string style = null)
        {
            return string.Concat("<a style='", style, "' href='", UrlHelper.Current.resolveInternal(relativeUrl), "?", par, "'>", name, "</a>");
        }

        private readonly object mapLock = new object();
        protected StringDictionary map = new StringDictionary();

        protected static UrlHelper Current
        {
            get
            {
                return Nested.instance;
            }
        }

        protected string resolveInternal(string RelativeUrl)
        {
            string absoluteUrl = null;

            if (!this.map.ContainsKey(RelativeUrl))
            {
                absoluteUrl = VirtualPathUtility.ToAbsolute(RelativeUrl);
                lock (this.mapLock)
                {
                    this.map[RelativeUrl] = absoluteUrl;
                }
            }
            else
            {
                absoluteUrl = this.map[RelativeUrl];
            }

            return absoluteUrl;
        }

        private class Nested
        {
            static Nested()
            {
            }

            internal static readonly UrlHelper instance = new UrlHelper();
        }
    }
}