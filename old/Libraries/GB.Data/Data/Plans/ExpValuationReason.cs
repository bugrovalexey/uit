﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
	/// <summary>Основание для оценивания по критерию</summary>
    public class ExpValuationReason : EntityBase // ExpValuationReasonBase
	{
        ///// <summary>Соответствие критериям</summary>
        //public virtual new ExpCriterionEquivalent ExpCriterionEquivalent
        //{
        //    //get { return (ExpCriterionEquivalent)base.ExpCriterionEquivalent; }
        //    //protected set { base.ExpCriterionEquivalent = value; }
        //    get;
        //    protected set;
        //}
        /// <summary>Соответствие критериям</summary>
        public virtual ExpCriterionEquivalent ExpCriterionEquivalent { get; set; }

        /// <summary>Основание</summary>
        public virtual ExpReason ExpReason { get; set; }

        /// <summary>Соответствие</summary>
        public virtual ExpConformity ExpConformity { get; set; }

        /// <summary>Комментарий</summary>
        public virtual string Comm { get; set; }//null
	}
}