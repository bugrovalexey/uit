﻿using System;

namespace bgTeam.IsRui.WebApp.ViewModels.Shared
{
    public class DateViewModel
    {
        public string Name { get; set; }

        public DateTime? Value { get; set; }

        public bool ReadOnly { get; set; }
    }
}