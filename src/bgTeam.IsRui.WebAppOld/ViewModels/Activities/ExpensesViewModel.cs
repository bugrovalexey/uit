﻿using bgTeam.IsRui.Domain;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.Extensions;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    [MetadataType(typeof(ExpensesViewModel_Valid))]
    public class ExpensesViewModel : ActivitiesCardBase
    {
        [DisplayName("БК ГРБС")]
        public IList<IEntityName> GRBS { get; private set; }

        [DisplayName("БК ПЗ/РЗ")]
        public IList<IEntityName> SectionKBK { get; private set; }

        [DisplayName("БК ЦСР")]
        public IList<IEntityName> CSR { get; private set; }

        [DisplayName("БК ВР")]
        public IList<IEntityName> WorkForm { get; private set; }

        [DisplayName("КОСГУ")]
        public IList<IEntityName> KOSGU { get; private set; }

        [DisplayName("Очередной финансовый год, руб.")]
        public object VolY0 { get; set; }

        [DisplayName("Первый год планового периода, руб.")]
        public object VolY1 { get; set; }

        [DisplayName("Второй год планового периода, руб.")]
        public object VolY2 { get; set; }

        //public ExpensesViewModel() { }

        public ExpensesViewModel(IStatusRepository statusRepository, int id)
            : base(statusRepository, id)
        {
            GRBS = CurrentActivity.GRBS.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });
            SectionKBK = CurrentActivity.SectionKBK.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });
            CSR = CurrentActivity.ExpenseItem.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });
            WorkForm = CurrentActivity.WorkForm.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });
            KOSGU = CurrentActivity.ExpenditureItem.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });

            VolY0 = CurrentActivity.PlansActivityVolY0;
            VolY1 = CurrentActivity.PlansActivityVolY1;
            VolY2 = CurrentActivity.PlansActivityVolY2;
        }
    }

    public class ExpensesArgs : IPlansActivityExpensesArgs
    {
        public int Id { get; set; }

        public int? GRBS { get; set; }

        public int? SectionKBK { get; set; }

        public int? WorkForm { get; set; }

        public int? CSR { get; set; }

        public int? KOSGU { get; set; }

        public object VolY0 { get; set; }

        public object VolY1 { get; set; }

        public object VolY2 { get; set; }

        public string tVolY0 { get; set; }

        public string tVolY1 { get; set; }

        public string tVolY2 { get; set; }
    }

    public class ExpensesViewModel_Valid : ValidateViewModel
    {
        [Range(MoneyMin, MoneyMax, ErrorMessage = RangeMoneyText)]
        public object VolY0 { get; set; }

        [Range(MoneyMin, MoneyMax, ErrorMessage = RangeMoneyText)]
        public object VolY1 { get; set; }

        [Range(MoneyMin, MoneyMax, ErrorMessage = RangeMoneyText)]
        public object VolY2 { get; set; }
    }
}