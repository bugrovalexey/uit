﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.Extensions;
using GB.Data.Plans;

namespace GB.DataAccess.Command.AccountingObjects.Activities
{
    public class AddActivityCommand : ICommand<AddActivityContext, AoActivity>
    {
        ISessionFactoryGb factory;

        public AddActivityCommand()
        {
            factory = new SessionFactory();
        }

        public AoActivity Execute(AddActivityContext context)
        {
            AoActivity activity;

            if (context.xId.HasValue)
                activity = new Repository<AoActivity>().Get(context.xId.Value);
            else
            {
                var ao = new Repository<AccountingObject>().Get(context.AccountingObjectId);

                activity = new AoActivity();
                activity.AccountingObject = ao;
                activity.Stage = ao.Stage;
                activity.ActivityType = ao.Stage.ToActivityTypeEnum().First();
            }

            AutoMapper.Mapper.Map(context, activity);

            if (context.Type.HasValue)
                activity.ActivityType = (ActivityTypeEnum)context.Type;

            using (var session = factory.Create())
            {
                session.SaveOrUpdate(activity);
                session.Commit();
            }

            return activity;
        }
    }
}
