﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="EditCard.Master" CodeBehind="Activities.aspx.cs" Inherits="GB.MKRF.Web.AccountingObjects.Activities" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
        <div class="page" style="position: relative">
        <dxwgv:ASPxGridView runat="server" ID="Grid" KeyFieldName="Id" Width="100%" AutoGenerateColumns="False"
            ClientInstanceName="Grid" OnBeforePerformDataSelect="Grid_OnBeforePerformDataSelect">
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Наименование">
                    <DataItemTemplate>
                        <%#GetLinkToActivityCard(Eval("Id"),Eval("Name")) %>
                    </DataItemTemplate>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Тип" FieldName="ActivityType2.Name" />               
                <dxwgv:GridViewDataTextColumn Caption="Ответственное учреждение" FieldName="Department.Name" />                
<%--                <dxwgv:GridViewDataTextColumn Caption="Статус мероприятия" FieldName="" />                --%>
                <dxwgv:GridViewBandColumn Caption="Расходы на мероприятия по информатизации, тыс. руб" Visible="true">
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="PlansActivityVolY0" Width="150px">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="PlansActivityVolY1" Width="150px">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="PlansActivityVolY2" Width="150px">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                </dxwgv:GridViewBandColumn>
            </Columns>
        </dxwgv:ASPxGridView>
    </div>    
</asp:Content>

