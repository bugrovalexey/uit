﻿using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.Documents;

namespace GB._Mapping.AccountingObjects
{
    internal class AODecisionToCreateMap : ClassMapHiLo<AODecisionToCreate>
    {
        public AODecisionToCreateMap()
            : base("OU_Purchase_Decision")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Purchase_Decision_Name").Not.Nullable();
            Map(x => x.Number, "OU_Purchase_Decision_Number");
            Map(x => x.DateAdoption, "OU_Purchase_Decision_Date");
            Map(x => x.ParticleOfDocument, "OU_Purchase_Document_Paragraph");
            Map(x => x.Url, "OU_Purchase_Decision_URL");

            References(x => x.AccountingObject, "OU_ID").Not.Nullable();

            HasMany(x => x.Documents).KeyColumns.Add("Document_Owner_ID")
                .Where("Doc_Type_ID = " + (int)DocTypeEnum.DecisionToCreateAccountingObject)
                .Where("Portal_Table_ID = " + (int)EntityType.DocumentAccountingObject)
                .Inverse();
        }
    }
}