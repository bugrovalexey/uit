﻿using GB.Data.Extensions;
using GB.Data.Plans;
using GB.Data.Workflow;
using System.Linq;
using System.Text;
using GB.DataAccess.Repository;
using GB.DataAccess.Command.AccountingObjects;
using GB.DataAccess.Command.Workflow;
using StatusSt = GB.Data.Workflow.Status;

namespace GB.MKRF.Logic.Status.Handler
{
    public class ActivityWorkflowHandler : WorkflowHandlerBase<PlansActivity>
    {
        protected override bool CheckNewStatus(PlansActivity a, Data.Workflow.Status status, out string errorMessage, out string confirmMessage)
        {
            confirmMessage = null;
            var sb = new StringBuilder();
            var se = (StatusEnum)status.Id;
            switch (se)
            {
                case StatusEnum.ActOnExpertise:
                    if (a.ActivityType2.IsEq(ActivityTypeEnum.Develop) || a.ActivityType2.IsEq(ActivityTypeEnum.Exploitation))
                    {
                        // В случае если мероприятие по эксплуатации или развитию,
                        // то необходимо проверять что на вкладке "Общие сведения",
                        // в поле "Объект учета" выбрано значение из списка объектов учета.
                        if (a.AccountingObject == null)
                        {
                            sb.AppendLine("Пожалуйста, заполните сведения об объекте учета на вкладке Общие сведения.");
                        }
                    }
                    else if (a.ActivityType2.IsEq(ActivityTypeEnum.Create))
                    {
                        // Если мероприятие направлено на создание ОУ,
                        // то необходимо проверять что все поля в разделе "Объект учета"
                        // (Полное и краткое наименования, тип и вид ОУ) заполнены.
                        if (a.AccountingObject == null
                            || string.IsNullOrWhiteSpace(a.AccountingObject.FullName)
                            || string.IsNullOrWhiteSpace(a.AccountingObject.ShortName)
                            || a.AccountingObject.Kind == null)
                        {
                            sb.AppendLine("Пожалуйста, заполните сведения об объекте учета на вкладке Общие сведения.");
                        }
                    }
                    else
                    {
                        errorMessage = "Неизвестный тип мероприятия: " + a.ActivityType2;
                        return false;
                    }

                    //Вне зависимости от типа мероприятия, необходимо проверять наличие хотя бы одной характеристики,
                    //заполненной на вкладке "Характеристики ОУ"
                    //TODO закрыто для показа
                    //if (AccountingObjectCharacteristicsValueRepository.GetAllForActivity(a).Count == 0)
                    //{
                    //    sb.AppendLine("Пожалуйста, заполните сведения на вкладке Характеристики ОУ.");
                    //}

                    //На вкладке "Основания" должны быть заполнены, либо три поля "Государственная программа",
                    //"Подпрограмма государственной программы" и "Основное мероприятие государственной программы",
                    //либо в списке "Другие основания реализации" должен быть добавлен хотя бы один элемент.
                    if ((a.StateProgram.IsNullOrDefault() || a.StateSubProgram.IsNullOrDefault() || a.StateMainEvent.IsNullOrDefault())
                        && (!a.OtherReasons.Any()))
                    {
                        sb.AppendLine("Пожалуйста, заполните основания реализации мероприятия на вкладке Основания.");
                    }

                    //В случае если на вкладке расходы, все поля "Очередной финансовый год, руб.", "Первый год планового периода, руб.", "Второй год планового периода, руб."
                    //содержат значение 0,00, то не блокировать перевод статуса, но при попытке его совершить, выводить предупреждающее сообщение с двумя кнопками - продолжить и отмена.
                    if (a.PlansActivityVolY0 + a.PlansActivityAddVolY1 + a.PlansActivityAddVolY2 == 0)
                    {
                        confirmMessage = "Внимание! " +
                            "Сумма расходов, указанная на очередной и планируемый период реализации, равна 0,00 руб. " +
                            "Вы уверены что хотите продолжить?";
                    }
                    break;

                case StatusEnum.ActOnCompletion:
                    break;

                case StatusEnum.ActAgreed:
                    if (!StatusEnum.ExpAgreed.IsEq(a.ExpertiseConclusion.Status))
                    {
                        sb.AppendLine("Мероприятие должно пройти экспертизу, и быть  согласовано экспертами.");
                    }
                    break;

                case StatusEnum.ActSendedToMKS:
                    break;

                case StatusEnum.ActRejectedAtMKS:
                    /* В случае если мероприятие отклонено МКС, пользователю для того, чтобы перевести мероприятие в соответствующий статус
                     * необходимо приложить документ с замечаниями Минкомсвязи. Для этого после нажатия на кнопку "Отклонено МКС",
                     * должно появляться выпадающее окно - Прикрепите замечания МКС, с возможностью загрузки документа
                     * в форматах pdf, tiff, tif, jpg, rar, zip, doc, xls, docx, xlsx. */
                    break;

                case StatusEnum.ActIncludedInPlan:
                    break;

                default:
                    errorMessage = "Мероприятие не может иметь статус: " + status;
                    return false;
            }
            errorMessage = sb.ToString().TrimEnd('\r', '\n');
            return string.IsNullOrEmpty(errorMessage);
        }

        public override void ChangeAfter(PlansActivity obj)
        {
            StatusEnum status = (StatusEnum)obj.Status.Id;

            switch (status)
            {
                //case StatusEnum.None:
                //    break;
                //case StatusEnum.ActCreated:
                //    break;
                //case StatusEnum.ActOnExpertise:
                //    break;
                case StatusEnum.ActOnCompletion:
                    var command = new StatusChangePostCommand();

                    var conclusion = obj.ExpertiseConclusion;
                    conclusion.Status = new Repository<StatusSt>().Get((int)StatusEnum.ExpAppointment);

                    command.Execute(new GB.DataAccess.Command.Workflow.StatusChangePostContext() { Obj = conclusion });
                    break;
                //case StatusEnum.ActAgreed:
                //    break;
                //case StatusEnum.ActSendedToMKS:
                //    break;
                //case StatusEnum.ActRejectedAtMKS:
                //    break;
                //case StatusEnum.ActIncludedInPlan:
                //    break;
                //case StatusEnum.ExpAppointment:
                //    break;
                //case StatusEnum.ExpTechnician:
                //    break;
                //case StatusEnum.ExpEconomist:
                //    break;
                //case StatusEnum.ExpAgreed:
                //    break;
                //case StatusEnum.AoDraft:
                //    break;
                //case StatusEnum.AoAgreed:
                //    break;
                //case StatusEnum.AoOnCompletion:
                //    break;
                //case StatusEnum.AoAccordHigher:
                //    break;
                //default:
                //    break;
            }
        }
    }
}