﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class WorkAndServiceAddOrUpdateWorkDto
    {
        public int? Id { get; set; }

        public int AoId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Классификационная категория
        /// </summary>
        public int CategoryKindOfSupportId { get; set; }

        /// <summary>
        /// Сведения по арендуемой инфраструктуре и условиям ее использования
        /// </summary>
        public string InformationLeasedInfrastructure { get; set; }

        /// <summary>
        /// ОКВЭД
        /// </summary>
        public int OkvedId { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public virtual decimal Summa { get; set; }
    }
}
