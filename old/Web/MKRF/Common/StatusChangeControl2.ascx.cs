﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Entities;
using GB.MKRF.Repository;
using GB.MKRF.Entities.Statuses;
using GB.Extentions;
using GB.Controls.Extensions;
using GB;
using DevExpress.Web.ASPxClasses;

namespace GB.MKRF.Web.Common
{
    public partial class StatusChangeControl2 : System.Web.UI.UserControl
    {
        public IWorkflowObject Object { get; set; }

        private StatusRepository repository = new StatusRepository();


        protected override void OnPreRender(EventArgs e)
        {
            StatusComboBox.DataBind();

            base.OnPreRender(e);
        }

        protected void StatusComboBox_DataBinding(object sender, EventArgs e)
        {
            var data = repository.GetNext(Object as IWorkflowObject);

            if (data.Count > 0)
            {
                sender.SetDataComboBox(data)
                    .SetIndexComboBox<int>(null, 0);
            }
            else
                ChangeStatusButton.ClientEnabled = false;
        }

        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            repository.Change(Object, 
                StatusComboBox.Value,
                CommentMemo.Text);

            ASPxWebControl.RedirectOnCallback(Request.RawUrl);
        }
    }
}