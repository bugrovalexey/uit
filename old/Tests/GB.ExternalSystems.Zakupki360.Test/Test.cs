﻿using GB.ExternalSystems.Zakupki360.Dto;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.ExternalSystems.Zakupki360.Test
{
    [TestFixture]
    class Test
    {
        [Test]
        public void GetConractsTest()
        {
            string error = null;
            var cs = new ClientSystemsFactory().Create();

            var res = cs.GetConracts(
                new SearchParametersContracts()
                {
                    //Status = 1,
                    //Number = "0148300008214000049",
                    //Order = "0148300008214000048",
                    Performer = "Регион-Снаб",
                    FromDate = new DateTime(2014, 1, 1),
                    ToDate = new DateTime(2015, 1, 1),
                },
                out error);

            Assert.IsNull(error);
            Assert.IsTrue(res.Count() > 0);
        }
    }
}
