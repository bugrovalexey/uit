﻿namespace GB.Data.Common
{
    /// <summary>
    /// Типы значений настроек
    /// </summary>
    public enum OptionsTypeEnum
    {
        /// <summary>Строчный</summary>
        String = 0,

        /// <summary>Целочисленный</summary>
        Int = 1,

        /// <summary>Булевский</summary>
        Bool = 2,

        /// <summary>Дата</summary>
        Date = 3
    }
}