﻿using System;

namespace GB.DataAccess.Services.Args
{
    public class AOGovServiceNPAArgs : BaseArgsNew
    {
        public int AOGovServiceId { get; set; }

        public string Name { get; set; }
        
        public string DocumentNumber { get; set; }

        public DateTime? Date { get; set; }
        
        public string ParticleOfDocument { get; set; }
    }
}
