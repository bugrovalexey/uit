﻿using GB.ExternalSystems.Zakupki360.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.ExternalSystems.Zakupki360
{
    public interface IClientSystems
    {
        void Test();

        IEnumerable<OrderInfo> GetOrders(SearchParametersOrders filter, out string error);

        IEnumerable<ContractInfo> GetConracts(SearchParametersContracts filter, out string error);
    }
}
