﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201808070040)]
    public class Migration_201808070040 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("Okei")
                .WithColumn("Okei_ID").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Okei_Name").AsString().NotNullable()
                .WithColumn("Okei_Code").AsInt32().Nullable()
                .WithColumn("Okei_FullName").AsString().Nullable()
                .WithColumn("Okei_NationalCode").AsString().Nullable()
                .WithColumn("Okei_InternationalCode").AsString().Nullable()
                .WithColumn("Okei_InternationalName").AsString().Nullable();

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 460, TableKey = "Okei" });
        }
    }
}
