﻿using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Extensions;
using NHibernate;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace bgTeam.IsRui.DataAccess.Repository
{
    /// <summary>
    /// Общий репозиторий
    /// </summary>
    /// <typeparam name="T">Тип объекта</typeparam>
    public class RepositorySt
    {
        private ISession _session;

        public RepositorySt(ISessionNHibernate session)
        {
            _session = session.GetSession();
        }


        protected const int CommandTimeout = 300;

        protected internal NHibernate.ISession Session
        {
            get { return _session; }
        }

        /// <summary>
        /// Выполнить sql запрос через NHibernate
        /// </summary>
        /// <typeparam name="TOutput"></typeparam>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        /*public static IList<TOutput> ExecuteSqlToObject<TOutput>(string sql, Action<IQuery> query = null)
        {
            IQuery _query = Session.CreateSQLQuery(sql);

            if (query != null)
                query(_query);

            return _query
                .SetResultTransformer(Transformers.AliasToBean<TOutput>())
                .List<TOutput>();
        }*/

        /// <summary>
        /// Выполнить хранимую процедуру через NHibernate
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        /*public static void ExecuteStoredProcedure(string procedureName, Dictionary<string, object> parameters )
        {
            var paramStr = string.Join(", ", parameters.Keys.Select(key => string.Format("@{0}=:{0}", key)));
            ISQLQuery query = Session.CreateSQLQuery(string.Format("exec {0} {1}", procedureName, paramStr));

            foreach (var pr in parameters)
            {
                query.SetParameter(pr.Key, pr.Value);
            }
            
            query.ExecuteUpdate();
        }*/


        /// <summary>
        /// Получает DataTable по запросу с параметрами
        /// </summary>
        /// <param name="sql">SQL запрос</param>
        /// <param name="pars">Параметры запроса</param>
        /// <param name="tableName">Наименование DataTable который вернётся.</param>
        /// <param name="primaryKey">ColumnName в качестве primaryKey, если нужно указать</param>
        /// <returns></returns>
        /*protected static DataTable GetTableViaSQL(string sql, Dictionary<string, object> pars = null, string tableName = null, string primaryKey = null)
        {
            DataTable dt = new DataTable();

            if (!string.IsNullOrEmpty(tableName))
                dt.TableName = tableName;

            SqlCommand command = new SqlCommand(sql, (SqlConnection)Session.Connection);
            command.CommandTimeout = CommandTimeout;
            Session.Transaction.Enlist(command);
            SqlDataAdapter adapter = new SqlDataAdapter(command);

            if (pars != null)
            {
                foreach (string parameterName in pars.Keys)
                {
                    IDbDataParameter parameter = command.CreateParameter();
                    parameter.ParameterName = parameterName;
                    parameter.Value = pars[parameterName];
                    command.Parameters.Add(parameter);
                }
            }

            adapter.Fill(dt);

            if (!string.IsNullOrEmpty(primaryKey))
                dt.PrimaryKey = new DataColumn[] { dt.Columns[primaryKey] };

            return dt;
        }*/

        /*public static IEntity Get(EntityType type, int id)
        {
            var t = EntityExtension.GetObjType(type);

            return Session.Get(t, id) as IEntity;
        }*/
    }
}