﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.CharacteristicOfTechnicalSupports
{
    public class AddOrSaveCharacteristicOfTechnicalSupportContext : BaseAddOrSaveContext
    {
        public int TechnicalSupportId { get; set; }

        public string Name { get; set; }

        public int UnitId { get; set; }

        public double? Value { get; set; }
    }
}
