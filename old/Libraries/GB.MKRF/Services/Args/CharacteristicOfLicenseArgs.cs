﻿using GB.Data.AccountingObjects;

namespace GB.MKRF.Services.Args
{
    public class CharacteristicOfLicenseArgs : BaseArgsNew, IServiceArgs
    {
        public int SoftwareId { get; set; }

        public string Name { get; set; }

        public string Model { get; set; }

        public TypeOfConnectionToServerEnum? TypeOfConnectionToServer { get; set; }

        public int? NumberOnlineSameTime { get; set; }

        public string ProductNumber { get; set; }

        public int? NumberAllocatedSameTime { get; set; }

        public string Licensor { get; set; }
    }
}