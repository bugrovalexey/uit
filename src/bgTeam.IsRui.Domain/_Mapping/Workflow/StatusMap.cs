﻿using bgTeam.IsRui.Domain.Entities.Workflow;

namespace bgTeam.IsRui.Domain._Mapping.Workflow
{
    internal class StatusMap : ClassMapHiLo<Status>
    {
        public StatusMap()
            : base("Status") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "Status_Name");
            Map(x => x.ActionName, "Status_Action_Name");
            Map(x => x.Css, "Status_Css");

            References<StatusGroup>(x => x.StatusGroup, "Status_Group_ID").Not.Nullable();
        }
    }
}