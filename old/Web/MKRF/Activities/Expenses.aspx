﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Activities/EditCard.master" AutoEventWireup="true" CodeBehind="Expenses.aspx.cs" Inherits="GB.MKRF.Web.Activities.Expenses" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function FormSave(s, e) {

        }

        function validateComboBox(s, e) {
            var cbValue = s.GetValue();
            e.isValid = (cbValue != undefined || cbValue) && cbValue != 0;
        }
        function validateMemo(s, e) {
            var cbValue = s.GetValue();
            e.isValid = (cbValue != undefined || cbValue) && cbValue != '- Не задано';
        }

        function ShowConfirm(isChange) {
            if (isChange) {
                window.onbeforeunload = function (e) {
                    return "Внесенные на странице данные не сохранены.";
                }
            } else {
                window.onbeforeunload = null;
            }
        }

        $(document).ready(function () {
            VolY0TextBox.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            VolY1TextBox.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            VolY2TextBox.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });

            <% if(!EnableEdit) {%>
            $('a .command, .catalogButton').each(function () {
                $(this).css("display", "none");
            });
            <% }%>

            UpdateButtonClear('GRBS');
            UpdateButtonClear('ExpenseItem');
            UpdateButtonClear('SectionKBK');
            UpdateButtonClear('WorkForm');
            UpdateButtonClear('ExpenditureItems');
        });

        function UpdateButtonClear(name) {
            var val = hf.Get(name + 'Id');

            if (val && val != 0) {
                var btName = '#' + name + 'Memo';
                var bt = $(btName).parents('td.form_edit_input').find('div.ButtonClear');

                bt.css('display', '');
            }
        }


        function SelectValue(e) {
            PopupControl.SetContentHtml('');
            var title;
            switch (e) {
                case 'ExpenseItem':
                    title = 'Выберите БК ЦСР';
                    break;
                case 'GRBS':
                    title = 'Выберите БК ГРБС';
                    break;
                case 'SectionKBK':
                    title = 'Выберите БК ПЗ/РЗ';
                    break;
                case 'WorkForm':
                    title = 'Выберите БК ВР';
                    break;

                case 'ExpenditureItems':
                    title = 'Выбирите БК КОСГУ';
                    break;
                default:
                    alert('error SelectValue');
            }

            PopupControl.SetContentUrl("../Selectors/CatalogItems.aspx?type=" + e);
            PopupControl.RefreshContentUrl();
            PopupControl.SetHeaderText(title);
            PopupControl.Show();
        };

        function CatalogPopupClose(type, result) {
            PopupControl.Hide();
            var mass = result.split(';');
            if (mass[0]) hf.Set(type + 'Id', mass[0]);
            if (mass[1]) hf.Set(type + 'Code', mass[1]);
            if (mass[2]) hf.Set(type + 'Name', mass[2]);
            var Memo = '#' + type + 'Memo';
            $(Memo).find('input').val(hf.Get(type + 'Code') + ' ' + hf.Get(type + 'Name'));
            UpdateButtonClear(type);
            ShowConfirm(true);

            PopupControl.SetContentUrl("../Selectors/CatalogItems.aspx?type=-999");
            PopupControl.RefreshContentUrl();
        };


        function ClearValue(s, name) {
            if (hf.Contains(name + 'Id')) {
                ShowConfirm(true);
            }

            hf.Set(name + 'Id', '0');
            hf.Set(name + 'Name', '');
            hf.Set(name + 'Code', '');

            var Memo = '#' + name + 'Memo';
            $(Memo).find('input').val('- Не задано');

            $(s).css('display', 'none');
        };

    </script>

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" SyncWithServer="true" />
    <div class="panel_button">
        <dxe:ASPxButton runat="server" AutoPostBack="false" Text="Сохранить" ID="saveButton" CausesValidation="true"
            ValidationGroup="Expenses" CssClass="fleft topButton save_button">
            <ClientSideEvents Click="function(s,e){ FormSave(); callBack.PerformCallback(''); MainLoadingPanel.Show(); ShowConfirm(false); }" />
        </dxe:ASPxButton>
    </div>
    <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" OnCallback="callBack_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { MainLoadingPanel.Hide(); }" 
            CallbackError="function(s,e) { MainLoadingPanel.Hide(); }" />
    </dxc:ASPxCallback>

    
    <dxpc:ASPxPopupControl ID="PopupControl" runat="server" AllowDragging="True" AllowResize="True"
        CloseAction="CloseButton" Modal="True" EnableViewState="False" 
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="600px" 
        HeaderText="Выберите БК ГРБС" ClientInstanceName="PopupControl" EnableHierarchyRecreation="True">
    </dxpc:ASPxPopupControl>
    
    <p class="header_grid">
        Код бюджетной классификации
    </p>
    <table class="form_edit">
        <tr>
            <td class="form_edit_desc">
                БК ГРБС
            </td>
            <td class="form_edit_input">
                <div style="position:relative">
                    <dxe:ASPxTextBox ID="GRBSMemo" runat="server" ClientInstanceName="GRBSMemo" ClientIDMode="Static" ReadOnly="true" OnDataBinding="Memo_DataBinding">
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('GRBS'); return false;" ></div>
                    <div class="ButtonClear catalogButton" style="display:none;" onclick="ClearValue(this, 'GRBS'); return false;" ></div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                БК ПЗ/РЗ
            </td>
            <td class="form_edit_input" >
                <div style="position:relative">
                    <dxe:ASPxTextBox ID="SectionKBKMemo" runat="server" ClientInstanceName="SectionKBKMemo" ClientIDMode="Static" ReadOnly="true" OnDataBinding="Memo_DataBinding">
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('SectionKBK'); return false;" ></div>
                    <div class="ButtonClear catalogButton" style="display:none;" onclick="ClearValue(this, 'SectionKBK'); return false;" ></div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                БК ЦСР
            </td>
            <td class="form_edit_input">
                <div style="position:relative">
                    <dxe:ASPxTextBox ID="ExpenseItemMemo" runat="server" ClientInstanceName="ExpenseItemMemo" ClientIDMode="Static" ReadOnly="true" OnDataBinding="Memo_DataBinding">
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('ExpenseItem'); return false;" ></div>
                    <div class="ButtonClear catalogButton" style="display:none;" onclick="ClearValue(this, 'ExpenseItem'); return false;" ></div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                БК ВР
            </td>
            <td class="form_edit_input">
                <div style="position:relative">
                    <dxe:ASPxTextBox ID="WorkFormMemo" runat="server" ClientInstanceName="WorkFormMemo" ClientIDMode="Static" Rows="2" ReadOnly="true" OnDataBinding="Memo_DataBinding">
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('WorkForm'); return false;" ></div>
                    <div class="ButtonClear catalogButton" style="display:none;" onclick="ClearValue(this, 'WorkForm'); return false;" ></div>   
                </div>      
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                БК КОСГУ
            </td>
            <td class="form_edit_input">
                <div style="position:relative">
                    <dxe:ASPxTextBox ID="ExpenditureItemsMemo" runat="server" ClientInstanceName="ExpenditureItemsMemo" ClientIDMode="Static" Rows="2" ReadOnly="true" OnDataBinding="Memo_DataBinding">
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('ExpenditureItems'); return false;" ></div>
                    <div class="ButtonClear catalogButton" style="display:none;" onclick="ClearValue(this, 'ExpenditureItems'); return false;" ></div>   
                </div> 
            </td>
        </tr>
    </table>

    <p class="header_grid">
        Расходы на мероприятия по информатизации
    </p>

    <table>
        <tr>
            <td class="form_edit_desc">Очередной финансовый год, руб.
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="VolY0TextBox" ClientInstanceName="VolY0TextBox" 
                    MaskSettings-Mask="<0..999999999g>.<00..99>" OnDataBinding="VolTextBox_DataBinding" />
            </td>
            
        </tr>
        <tr>
            <td class="form_edit_desc">Первый год планового периода, руб.
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="VolY1TextBox" ClientInstanceName="VolY1TextBox" 
                    MaskSettings-Mask="<0..999999999g>.<00..99>" OnDataBinding="VolTextBox_DataBinding" />
            </td>
           
        </tr>
        <tr>
            <td class="form_edit_desc">Второй год планового периода, руб.
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox runat="server" ID="VolY2TextBox" ClientInstanceName="VolY2TextBox" 
                    MaskSettings-Mask="<0..999999999g>.<00..99>" OnDataBinding="VolTextBox_DataBinding" />
            </td>
        </tr>
    </table>
</asp:Content>
