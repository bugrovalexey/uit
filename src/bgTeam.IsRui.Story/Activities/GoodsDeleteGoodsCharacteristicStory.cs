﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GoodsDeleteGoodsCharacteristicStory : IStory<GoodsDeleteGoodsCharacteristicStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;

        public GoodsDeleteGoodsCharacteristicStory(ISessionNHibernateFactory factory)
        {
            _factory = factory;
        }

        public bool Execute(GoodsDeleteGoodsCharacteristicStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(GoodsDeleteGoodsCharacteristicStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            bool result;

            using (var session = _factory.OpenSession())
            {
                    await session.DeleteAsync<GoodsCharacteristic>(context.Id.Value);

                    result = true;
            }

            return result;
        }
    }
}
