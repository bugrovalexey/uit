﻿namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;

    internal class ActOfAcceptanceMap : ClassMapHiLo<ActOfAcceptance>
    {
        public ActOfAcceptanceMap()
            : base("Act") { }

        protected override void InitMap()
        {
            Map(x => x.DocName, "Act_Name");
            Map(x => x.DocNumber, "Act_Number");
            Map(x => x.DocDateAdoption, "Act_Date");
            Map(x => x.Cost, "Act_Cost").Not.Nullable();
            Map(x => x.ReasonsWaitingResultsDeviation, "Act_Reason");
            Map(x => x.ActivityType, "Act_Activity_Type").CustomType<ActivityTypeEnum>();
            
            References(x => x.GovContract, "OU_Gos_Contract_ID").Not.Nullable();

            //Нельзя ставить каскад на удаление,так как удаление Работ и Товаров
            //возможно только при удалении соответсвующих ПО, ТО и т.д.
            //Нельзя добавлять Inverse - тоже удаляет.
            HasMany(x => x.Works).KeyColumns.Add("Act_ID");

            HasMany(x => x.ActToAOActivities).KeyColumns.Add("Act_ID").Cascade.Delete().Inverse();

            HasMany(x => x.Documents).KeyColumns.Add("EntityOwnerId")
                .Where("EntityType = " + (int)EntityType.ActOfAcceptance)
                .Where("Type = " + (int)DocTypeEnum.ActOfAcceptance)
                .Cascade.Delete().Inverse();
        }
    }
}
