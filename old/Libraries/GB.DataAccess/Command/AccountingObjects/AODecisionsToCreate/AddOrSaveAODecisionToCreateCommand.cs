﻿using GB.Data.AccountingObjects;
using GB.Data.Documents;
using GB.DataAccess.Command.Documents;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AODecisionsToCreate
{
    public class AddOrSaveAODecisionToCreateCommand : ICommand<AddOrSaveAODecisionToCreateContext, AODecisionToCreate>
    {
        ISessionFactoryGb factory;

        public AddOrSaveAODecisionToCreateCommand()
        {
            factory = new SessionFactory();
        }

        public AODecisionToCreate Execute(AddOrSaveAODecisionToCreateContext context)
        {
            AODecisionToCreate aoDecision;

            if (context.xId.HasValue && context.xId != 0)
                aoDecision = new Repository<AODecisionToCreate>().Get(context.xId.Value);
            else
            {
                aoDecision = new AODecisionToCreate();                
            }

            AutoMapper.Mapper.Map(context, aoDecision);

            using (var session = factory.Create())
            {
                try
                {
                    session.SaveOrUpdate(aoDecision);
                    //session.Commit();
                    //}

                    //using (var session = factory.Create())
                    //{
                    session.Refresh(aoDecision);
                    //session.Commit();
                    //}

                    new SaveDocumentCommand(factory)
                        .Execute(new SaveDocumentContext
                    {
                        fileList = context.Files,
                        listDocs = aoDecision.Documents,
                        owner = aoDecision,
                        type = DocTypeEnum.DecisionToCreateAccountingObject,
                        Session = session,
                    });

                    //using (var session = factory.Create())
                    //{
                    //session.Refresh(aoDecision);

                    session.Commit();
                }
                catch (Exception)
                {
                    session.Rollback();
                    throw;
                }
            }

            return aoDecision;
        }
    }
}
