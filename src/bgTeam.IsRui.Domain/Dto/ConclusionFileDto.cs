﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ConclusionFileDto
    {
        public int? ActivityId { get; set; }

        public int? ConclusionId { get; set; }

        public int? FileId { get; set; }

        public string FileName { get; set; }
    }
}
