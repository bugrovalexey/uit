﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class RoleDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string WebPage { get; set; }

        public string Code { get; set; }
    }
}
