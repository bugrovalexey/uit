﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Logic.MasterEdit
{
    public interface IMasterEditHandler
    {
        MasterItem First();

        MasterItem GetItem(MasterItemEnum e);
        IEnumerable<MasterItem> GetItems();
    }
}
