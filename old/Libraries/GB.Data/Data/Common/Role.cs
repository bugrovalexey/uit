﻿namespace GB.Data.Common
{
    /// <summary>Роль</summary>
    public class Role : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Стартовая страница для роли</summary>
        public virtual string WebPage { get; set; }

        public override string ToString()
        {
            return string.IsNullOrEmpty(Name) ? base.ToString() : this.Name;
        }
    }
}