﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Получить список контрактов мероприятий привязанных к ОУ
    /// </summary>
    public class AoContractsGetAllContractsStory : IStory<AoContractsGetAllContractsStoryContext, IEnumerable<ActivityContractDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;
        private readonly IStory<AoGetLinkedActivitiesStoryContext, IEnumerable<ActivityDto>> _getLinkedActivityStory;

        public AoContractsGetAllContractsStory(
            IRepositoryRui repository,
            IMapperBase mapper,
            IStory<AoGetLinkedActivitiesStoryContext, IEnumerable<ActivityDto>> getLinkedActivityStory)
        {
            _mapper = mapper;
            _repository = repository;
            _getLinkedActivityStory = getLinkedActivityStory;
        }

        public IEnumerable<ActivityContractDto> Execute(AoContractsGetAllContractsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivityContractDto>> ExecuteAsync(AoContractsGetAllContractsStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            var res = await _getLinkedActivityStory
                .ExecuteAsync(new AoGetLinkedActivitiesStoryContext() { AoId = context.AoId });

            var ids = res.Select(x => x.Id).ToArray();

            var contracts = _repository.GetAllEx<ActivityContract>(q =>
                q.WhereRestrictionOn(x => x.PlansActivity).IsIn(ids));

            return _mapper.Map(contracts, new List<ActivityContractDto>());
        }
    }
}
