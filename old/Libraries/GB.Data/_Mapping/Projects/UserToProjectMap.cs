﻿using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Projects;

namespace GB.MKRF.Mapping.Projects
{
    class UserToProjectMap : ClassMapBase<UserToProject>
    {
        public UserToProjectMap()
            : base("Project_Users")
        {
        }

        protected override void InitMap()
        {
            References<User>(x => x.User, "Users_ID").Not.Nullable();
            References<Project>(x => x.Project, "Project_ID").Not.Nullable();
        }
    }
}