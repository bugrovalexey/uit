﻿using System;
using System.Collections.Generic;

namespace GB.MKRF.Logic
{
    public abstract class ManagerBase<THandler, TType> : IManager
    {
        private readonly object syncRoot = new object();
        private readonly Dictionary<object, THandler> handlers = new Dictionary<object, THandler>();

        public THandler GetHandler<T>() where T : TType
        {
            THandler handler;
            lock (syncRoot)
            {
                handlers.TryGetValue(typeof(T), out handler);
            }
            return handler;
        }

        public THandler GetHandler<T>(T type) //where T : object
        {
            THandler handler;
            lock (syncRoot)
            {
                handlers.TryGetValue(type, out handler);
            }
            return handler;
        }

        internal void RegisterHandler<T>(THandler handler) where T : TType
        {
            lock (syncRoot)
            {
                handlers[typeof(T)] = handler;
            }
        }

        internal void RegisterHandler<T>(T obj, THandler handler) where T : TType
        {
            lock (syncRoot)
            {
                handlers[obj] = handler;
            }
        }
    }
}