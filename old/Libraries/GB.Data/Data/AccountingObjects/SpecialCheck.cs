﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.Documents;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Сведения о прохождении специальных проверок
    /// </summary>
    public class SpecialCheck : EntityBase
    {
        /// <summary>
        /// ОУ
        /// </summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Наименование 
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Документ 
        /// </summary>
        public virtual IList<Files> Documents { get; set; }

    }
}
