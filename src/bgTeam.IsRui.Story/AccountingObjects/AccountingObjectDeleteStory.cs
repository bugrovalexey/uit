﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Workflow;
    using bgTeam.IsRui.Story.Activities;

    /// <summary>
    /// Удалить ОУ
    /// </summary>
    public class AccountingObjectDeleteStory : IStory<AccountingObjectDeleteStoryContext, bool>
    {
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IMapperBase _mapper;
        private readonly IFileProviderRui _fileProvider;

        public AccountingObjectDeleteStory(
            IRepositoryRui repository,
            ISessionNHibernateFactory factory,
            IFileProviderRui fileProvider,
            IMapperBase mapper)
        {
            _repository = repository;
            _factory = factory;
            _fileProvider = fileProvider;
            _mapper = mapper;
        }

        public bool Execute(AccountingObjectDeleteStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(AccountingObjectDeleteStoryContext context)
        {
            if (!context.Id.HasValue)
            {
                throw new ArgumentNullException(nameof(context.Id));
            }

            var ao = _repository.Get<AccountingObject>(context.Id);
            if (ao == null)
            {
                throw new ArgumentException($"Object not find - {context.Id}");
            }

            //Удаляем ОУ - который создается организациями
            if (ao.Mode == AccountingObjectEnum.One)
            {
                using (var session = _factory.OpenSession())
                {
                    await DeleteReference(ao);

                    await session.DeleteAsync<AccountingObject>(ao.Id);
                    //await session.CommitAsync();
                }
            }

            //Удаляем ОУ - который создается автоматически
            if (ao.Mode == AccountingObjectEnum.Multi)
            {
                var list = _repository.GetAllEx<AccountingObject>(q => q.Where(x => x.Parent == ao));

                foreach (var item in list)
                {
                    item.Parent = null;
                    item.Status = _repository.Get<Status>((int)StatusEnum.AoOnCompletion);
                }

                using (var session = _factory.OpenSession())
                {
                    await DeleteReference(ao);

                    await session.UpdateAsync(list);
                    await session.DeleteAsync<AccountingObject>(ao.Id);
                    //await session.CommitAsync();
                }
            }

            return true;
        }

        public async Task<bool> DeleteReference(AccountingObject ao)
        {
            var documentsDelete = new AoDocumentBaseDeleteDocumentStory(_repository, _factory, _mapper, _fileProvider);
            foreach (AODecisionToCreate document in ao.Documents)
            {
               await documentsDelete.ExecuteAsync(new AoDocumentBaseDeleteDocumentStoryContext() { Id = document.Id });
            }

            var characteristicsDelete = new AoCharacteristicsDeleteCharacteristicStory(_mapper, _repository, _factory);
            foreach (AccountingObjectCharacteristics characteristic in ao.Characteristics)
            {
               await characteristicsDelete.ExecuteAsync(new AoCharacteristicsDeleteCharacteristicStoryContext() { Id = characteristic.Id });
            }

            //var activityDelete = new DeleteActivityStory(_repository, _factory);
            //foreach (AoActivity activity in ao.AoActivities)
            //{
            //    await activityDelete.ExecuteAsync(new DeleteActivityStoryContext() { Id = activity.Id });
            //}

            var softwareDelete = new AoSoftwareDeleteSoftwareStory(_mapper, _repository, _factory);
            var softwareGet = _repository.GetAllEx<Software>(q => q.Where(x => x.AccountingObject.Id == ao.Id));
            foreach (Software software in softwareGet)
                {
                    await softwareDelete.ExecuteAsync(new AoSoftwareDeleteSoftwareStoryContext() { Id = software.Id });
                }

            var technicalSupportDelete = new AoTechnicalSupportDeleteTechSupportStory(_factory);
            foreach (TechnicalSupport technicalSupport in ao.TechnicalSupports)
            {
                await technicalSupportDelete.ExecuteAsync(new AoTechnicalSupportDeleteTechSupportStoryContext() { Id = technicalSupport.Id });
            }

            var workAndServiceDelete = new AoWorkAndServiceDeleteWorkStory(_factory);
            foreach (WorkAndService workAndService in ao.WorksAndServices)
            {
                await workAndServiceDelete.ExecuteAsync(new AoWorkAndServiceDeleteWorkStoryContext() { Id = workAndService.Id });
            }

            var contractsDelete = new AoContractsDeleteContractStory(_factory);
            var contractsGet = _repository.GetAllEx<AccountingObjectContract>(q => q.Where(x => x.AccountingObject.Id == ao.Id));
            foreach (AccountingObjectContract contract in contractsGet)
                {
                    await contractsDelete.ExecuteAsync(new AoContractsDeleteContractStoryContext() { Id = contract.Id });
                }

            var actsDel = new AoActsDeleteActStory(_mapper, _repository, _factory, _fileProvider);
            var actsGet = _repository.GetAllEx<AccountingObjectActs>(q => q.Where(x => x.AccountingObject.Id == ao.Id));
            foreach (AccountingObjectActs acts in actsGet)
            {
                await actsDel.ExecuteAsync(new AoActsDeleteActStoryContext() { Id = acts.Id });
            }

            //var filesDel = new FileDeleteStory(_fac.repositoryRui, _fac.sessionFactory);
            //foreach ()
            //story.Execute(new FileDeleteStoryContext() { Id =  });

            return true;
        }
    }
}
