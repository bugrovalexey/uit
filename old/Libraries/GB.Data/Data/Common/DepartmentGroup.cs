﻿
namespace GB.Data.Common
{
    /// <summary>
    /// Группа органа государственной власти (ведомства)
    /// </summary>
    public class DepartmentGroup : EntityBase
    {
        /// <summary>
        /// Орган государственной власти (ведомство)
        /// </summary>
        public virtual Department Department { get; set; }

        /// <summary>
        /// Год
        /// </summary>
        public virtual DepartmentGroupYearEnum Year { get; set; }
    }
}
