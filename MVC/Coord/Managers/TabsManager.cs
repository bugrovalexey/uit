﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GB.Helpers;
using GB.Infrastructure;
using GB.Providers;

namespace GB.Coord.Managers
{
    public class TabsManager
    {
        protected Dictionary<string, string> Tabs { get; set; }

        protected string ControllerName { get; set; }

        protected object RouteValues { get; set; }

        public TabsManager(IEnumerable<KeyValuePair<string, string>> actionTextList,  string controllerName, object routeValues = null )
        {
            ControllerName = controllerName;
            RouteValues = routeValues;
            Tabs = new Dictionary<string, string>();
            FillTabs(actionTextList);
        }

        public Dictionary<string, string> GetAccessibleTabs()
        {
            ApplyAccessByTabs();
            return Tabs;
        }

        protected void FillTabs(IEnumerable<KeyValuePair<string, string>> actionTextList)
        {
            foreach (var keyValuePair in actionTextList)
            {
                Add(keyValuePair.Key, keyValuePair.Value);
            }
        }

        protected void Add(string action, string text)
        {
            Tabs.Add(string.Format("~/{0}/{1}", ControllerName, action),
                LinkBuilder.ActionLink(text, action, ControllerName, RouteValues));
        }

        protected void ApplyAccessByTabs()
        {
            Dictionary<string, string> notAccessToTab = new Dictionary<string, string>();

            foreach (var tab in Tabs)
            {
                //if (Singleton<TabsProvider>.Instance..СheckAccessCurrentUser(
                //filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                //filterContext.ActionDescriptor.ActionName))
                //    ResponseHelper.DenyAccessToPage(filterContext.HttpContext.Response);

                //if (ValidatorBase.UserAccessToPage(tab.Key) == false)
                //{
                //    notAccessToTab.Add(tab.Key, tab.Value);
                //}
            }

            Tabs = Tabs.Except(notAccessToTab).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

    }
}