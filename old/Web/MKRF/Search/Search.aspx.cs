﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using Core;
using Core.Handlers;
using Core.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxNavBar;
using Uviri.MKRF.Entities;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Statuses;
using Uviri.MKRF.Repository;
using Uviri.MKRF.Web.Helpers;

namespace Uviri.MKRF.Web.Search
{
    public partial class Search : System.Web.UI.Page
    {
        public class itemInfo
        {
            public string url { get; set; }
            public string itemName { get; set; }

            public itemInfo(string itemType, int id, int ownerId)
            {
                switch (itemType)
                {
                    case "1": url = Plans.PlanCard.GetUrl(id, ExpObjectTypeEnum.Plan); itemName = "Планы информатизации"; break;
                    

                    case "8": url = Plans.PlanCard.GetUrl(id, ExpObjectTypeEnum.PreliminaryPlan); itemName = "Формы сведений"; break;
                    

                    case "9": url = Plans.PlanCard.GetUrl(id, ExpObjectTypeEnum.Plan2012); itemName = "Уточнённые планы"; break;
                    

                    case "10": break; // сводный план
                    //case "1001": break;
                    //case "1002": break;
                    //case "1003": break;

                    case "11": url = Plans.PlanCard.GetUrl(id, ExpObjectTypeEnum.Plan2012new); itemName = "Планы информатизации (версия 2012 г.)"; break;
                    case "1101": url = Plans.v2012new.Works.GetUrl(id); itemName = "Мероприятия планов (версия 2012 г.)"; break;
                    case "1102": url = Plans.v2012new.Works.GetUrl(ownerId); itemName = "Работы планов (версия 2012 г.)"; break;
                    case "1103": url = Plans.v2012new.Specifications.GetUrl(ownerId); itemName = "Спецификации планов (версия 2012 г.)"; break;
                    case "1104": url = Plans.v2012new.GoalsResults.GetUrl(ownerId); itemName = "Достигнутые результаты (версия 2012 г.)"; break;
                    case "1105": url = Plans.v2012new.GoalsResults.GetUrl(ownerId); itemName = "Целевые показатели (версия 2012 г.)"; break;

                    case "12": break;
                    case "1201": break;
                    case "1202": break;
                    case "1203": break;

                    
                    case "2001": url = FileHandler.GetUrl(id); itemName = "Реквизиты исходящих"; break;
                    case "2004": url = FileHandler.GetUrl(id); itemName = "Реквизиты входящих"; break;
                    case "2002": url = FileHandler.GetUrl(id); itemName = "Документ к заявке"; break;
                    case "2003": url = FileHandler.GetUrl(id); itemName = "Опись"; break;
                    case "2005": url = FileHandler.GetUrl(id); itemName = "Особое мнение"; break;
                    case "2008": url = FileHandler.GetUrl(id); itemName = "Нормативная документация"; break;
                    case "2010": url = FileHandler.GetUrl(id); itemName = "Заседание экспертной комиссии (протоколы и решения)"; break;
                    case "2017": url = FileHandler.GetUrl(id); itemName = "Часто задаваемые вопросы"; break;
                }
            }

        }
        SearchArgs args = null;
        SearchEngine engine = null;

        protected override void OnPreRender(EventArgs e)
        {
            if (!DesignMode)
            {
                args = new SearchArgs(Request.QueryString);
                args.PageSize = 10;

                doSearch();

				setUpFilter();
            }

            base.OnPreRender(e);
        }

        protected void doSearch()
        {
            engine = new SearchEngine(args);

            string physicalPathToIndex = ConfigurationManager.AppSettings["PathToSearchIndex"];

            if (string.IsNullOrEmpty(physicalPathToIndex) || !System.IO.Directory.Exists(physicalPathToIndex))
            {
                NoSearch.Visible = true;
                Logger.Fatal("Не указана директория индексной базы (PathToSearchIndex) или к ней нет доступа. " + physicalPathToIndex);                
                return;
            }

            DataTable searchResult = engine.DoSearch(physicalPathToIndex);

            if (searchResult == null || searchResult.Rows.Count <= 0)
            {
                NoResultMessage.Visible = true;
            }
            else
            {
                NoResultMessage.Visible = false;
                result.DataSource = searchResult;
                result.DataBind();
            }
        }

        protected void setUpFilter()
        {
            NavBarItem item = null;

            this.QueryTextBox.Text = args.Query;

            item = (NavBarItem)TypesNavBar.Items.FindByName("Plan");
			ASPxListBox PlanTypeListBox = (ASPxListBox)item.FindControl("PlanTypeListBox");
			PlanTypeListBox.DataSource = getTypes(ExpObjectTypeEnum.Plan);
			PlanTypeListBox.DataBind();
            item.Group.Expanded = checkListBox(PlanTypeListBox, args.Type);


            item = (NavBarItem)TypesNavBar.Items.FindByName("Preliminary");
			ASPxListBox PreliminaryTypeListBox = (ASPxListBox)item.FindControl("PreliminaryTypeListBox");
			PreliminaryTypeListBox.DataSource = getTypes(ExpObjectTypeEnum.PreliminaryPlan);
			PreliminaryTypeListBox.DataBind();
            item.Group.Expanded = checkListBox(PreliminaryTypeListBox, args.Type);


            item = (NavBarItem)TypesNavBar.Items.FindByName("Plan2012");
            ASPxListBox Plan2012TypeListBox = (ASPxListBox)item.FindControl("Plan2012TypeListBox");
			Plan2012TypeListBox.DataSource = getTypes(ExpObjectTypeEnum.Plan2012);
			Plan2012TypeListBox.DataBind();
            item.Group.Expanded = checkListBox(Plan2012TypeListBox, args.Type);


            //item = (NavBarItem)TypesNavBar.Items.FindByName("OtherTypes");
            //ASPxListBox TypeListBox = (ASPxListBox)item.FindControl("TypeListBox");
            //TypeListBox.DataSource = getTypes(ExpObjectTypeEnum.Other);
            //TypeListBox.DataBind();
            //item.Group.Expanded = checkListBox(TypeListBox, args.Type);


            item = (NavBarItem)TypesNavBar.Items.FindByName("Departments");
			ASPxListBox DepartmentListBox = (ASPxListBox)item.FindControl("DepartmentListBox");
			DepartmentListBox.DataSource = RepositoryBase<Department>.GetAll(null, x => x.Name);
			DepartmentListBox.DataBind();
            item.Group.Expanded = checkListBox(DepartmentListBox, args.Department);


            item = (NavBarItem)TypesNavBar.Items.FindByName("IKT");
            ASPxGridView IKTGrid = (ASPxGridView)item.FindControl("IKTGrid");
            IKTGrid.DataSource = createIKTPseudoTree(RepositoryBase<IKTComponent>.GetAll(null, x => x.Name));
            IKTGrid.DataBind();
            item.Group.Expanded = checkGrid(IKTGrid, args.IKT);


            item = (NavBarItem)TypesNavBar.Items.FindByName("Statuses");
            ASPxGridView StatusGrid = (ASPxGridView)item.FindControl("StatusGrid");
            StatusGrid.DataSource = RepositoryBase<Status>.GetAll();
            StatusGrid.DataBind();
            item.Group.Expanded = checkGrid(StatusGrid, args.Status);


            item = TypesNavBar.Items.FindByName("LastMod");
			ASPxDateEdit LastMod1DateEdit = (ASPxDateEdit)item.FindControl("LastMod1DateEdit");
			LastMod1DateEdit.Value = args.Date1;
            ASPxDateEdit LastMod2DateEdit = (ASPxDateEdit)item.FindControl("LastMod2DateEdit");
			LastMod2DateEdit.Value = args.Date2;
            item.Group.Expanded = args.Date1.HasValue || args.Date2.HasValue;


            item = TypesNavBar.Items.FindByName("Year");
			ASPxSpinEdit Year1SpinEdit = (ASPxSpinEdit)item.FindControl("Year1SpinEdit");
			Year1SpinEdit.Value = args.Year1;
            ASPxSpinEdit Year2SpinEdit = (ASPxSpinEdit)item.FindControl("Year2SpinEdit");
			Year2SpinEdit.Value = args.Year2;
            item.Group.Expanded = args.Year1.HasValue || args.Year2.HasValue;

            item = TypesNavBar.Items.FindByName("Other");
			ASPxSpinEdit NumberSpinEdit = (ASPxSpinEdit)item.FindControl("NumberSpinEdit");
			NumberSpinEdit.Value = args.Num;
            ASPxTextBox CuratorTextBox = (ASPxTextBox)item.FindControl("CuratorTextBox");
			CuratorTextBox.Text = args.Curator;
            ASPxTextBox AuthorTextBox = (ASPxTextBox)item.FindControl("AuthorTextBox");
			AuthorTextBox.Text = args.Author;
            item.Group.Expanded = args.Num.HasValue || !string.IsNullOrEmpty(args.Curator) || !string.IsNullOrEmpty(args.Author);


            item = TypesNavBar.Items.FindByName("Plan2010Sum");
            item.Group.Expanded |= setSpinValue(item, "sF1SpinEdit", args.sF_1);
            item.Group.Expanded |= setSpinValue(item, "sF2SpinEdit", args.sF_2);
            item.Group.Expanded |= setSpinValue(item, "sR1SpinEdit", args.sR_1);
            item.Group.Expanded |= setSpinValue(item, "sR2SpinEdit", args.sR_2);
            item.Group.Expanded |= setSpinValue(item, "sV1SpinEdit", args.sV_1);
            item.Group.Expanded |= setSpinValue(item, "sV2SpinEdit", args.sV_2);
            item.Group.Expanded |= setSpinValue(item, "sSum1SpinEdit", args.sSum_1);
            item.Group.Expanded |= setSpinValue(item, "sSum2SpinEdit", args.sSum_2);
            
            
            item = TypesNavBar.Items.FindByName("Plan2011Sum");
            item.Group.Expanded |= setSpinValue(item, "sY01SpinEdit", args.sY0_1);
            item.Group.Expanded |= setSpinValue(item, "sY02SpinEdit", args.sY0_2);
            item.Group.Expanded |= setSpinValue(item, "sY11SpinEdit", args.sY1_1);
            item.Group.Expanded |= setSpinValue(item, "sY12SpinEdit", args.sY1_2);
            item.Group.Expanded |= setSpinValue(item, "sY21SpinEdit", args.sY2_1);
            item.Group.Expanded |= setSpinValue(item, "sY22SpinEdit", args.sY2_2);

            
            item = TypesNavBar.Items.FindByName("Plan2011AddSum");
            item.Group.Expanded |= setSpinValue(item, "sAddY01SpinEdit", args.sAddY0_1);
            item.Group.Expanded |= setSpinValue(item, "sAddY02SpinEdit", args.sAddY0_2);
            item.Group.Expanded |= setSpinValue(item, "sAddY11SpinEdit", args.sAddY1_1);
            item.Group.Expanded |= setSpinValue(item, "sAddY12SpinEdit", args.sAddY1_2);
            item.Group.Expanded |= setSpinValue(item, "sAddY21SpinEdit", args.sAddY2_1);
            item.Group.Expanded |= setSpinValue(item, "sAddY22SpinEdit", args.sAddY2_2);


            item = TypesNavBar.Items.FindByName("Plan2011TotalSum");
            item.Group.Expanded |= setSpinValue(item, "sSumY01SpinEdit", args.sSumY0_1);
            item.Group.Expanded |= setSpinValue(item, "sSumY02SpinEdit", args.sSumY0_2);
            item.Group.Expanded |= setSpinValue(item, "sSumY11SpinEdit", args.sSumY1_1);
            item.Group.Expanded |= setSpinValue(item, "sSumY12SpinEdit", args.sSumY1_2);
            item.Group.Expanded |= setSpinValue(item, "sSumY21SpinEdit", args.sSumY2_1);
            item.Group.Expanded |= setSpinValue(item, "sSumY22SpinEdit", args.sSumY2_2);
        }

		private IList<NamedEntity> getTypes(ExpObjectTypeEnum type)
		{
			List<NamedEntity> types = new List<NamedEntity>();

			switch (type)
			{
				case ExpObjectTypeEnum.Plan:
					types.Add(new NamedEntity(1, "Планы информатизации"));
					types.Add(new NamedEntity(101, "Мероприятия"));
					types.Add(new NamedEntity(102, "Работы"));
					types.Add(new NamedEntity(103, "Спецификации"));
					types.Add(new NamedEntity(104, "Цели и задачи"));
					types.Add(new NamedEntity(105, "Целевые индикаторы"));
					break;
				case ExpObjectTypeEnum.Plan2012:
					types.Add(new NamedEntity(9, "Уточнённые планы"));
					types.Add(new NamedEntity(901, "Мероприятия"));
					types.Add(new NamedEntity(902, "Работы"));
					types.Add(new NamedEntity(903, "Спецификации"));
					break;
				case ExpObjectTypeEnum.PreliminaryPlan:
					types.Add(new NamedEntity(8, "Формы сведений"));
					types.Add(new NamedEntity(801, "Мероприятия"));
					break;
				default:
					types.Add(new NamedEntity(3, "Ведомственные программы информатизации"));
					types.Add(new NamedEntity(5, "Федеральные целевые программы"));
					types.Add(new NamedEntity(4, "Ведомственные целевые программы"));
					types.Add(new NamedEntity(6, "Проекты стратегий, концепций, иных долгосрочных документов"));
					types.Add(new NamedEntity(2, "Проекты решений о создании ФГИС"));
					types.Add(new NamedEntity(7, "Иные документы, содержащие мероприятия по ИКТ"));
					types.Add(new NamedEntity(1000, "Экспертные заключения"));
					types.Add(new NamedEntity(2000, "Файлы"));
					break;
			}

			return types;
		}

        bool checkListBox(ASPxListBox box, HashSet<int> set)
        {
            bool containsCheckedItem = false;
            ListEditItem item = null;
            foreach(int id in set)
            {

                if (id >= 2000 && id < 3000)
                {
                    // в индексной базе файлы разделены по типам, на форме - только 1 чекбокс
                    item = box.Items.FindByValue(2000);
                }
                else
                {
                    item = box.Items.FindByValue(id);
                }

                if (item != null && !item.Selected)
                {
                    item.Selected = true;
                    containsCheckedItem = true;
                }
            }
            return containsCheckedItem;
        }

        bool checkGrid(ASPxGridView grid, HashSet<int> set)
        {
            foreach (int id in set)
            {
                grid.Selection.SelectRowByKey(id);
            }
            return (set != null && set.Count > 0);
        }

        bool setSpinValue(NavBarItem item, string spinName, double? value)
        {
            if (!value.HasValue)
                return false;
            ASPxSpinEdit spin = (ASPxSpinEdit)item.FindControl(spinName);
            spin.Value = value.Value;
            return true;
        }

        protected string getItem(object container)
        {
            DataRow row = (container as DataRowView).Row;

            if (row != null)
            {
                itemInfo info = new itemInfo(row["Type"].ToString(), int.Parse(row["Id"].ToString()), int.Parse(row["OwnerId"].ToString()));


                if (string.IsNullOrEmpty(info.url))
                    info.url = "javascript:void(0);";

                StringBuilder builder = new StringBuilder();
                builder.Append("<span class='header-link'><a href=\"" + info.url + "\">");
                builder.Append(row["Name"]);
                builder.Append("</a></span><br />");
                builder.Append("<span class='info'>" + info.itemName + "</span>");
                builder.Append("<span class='time'>" + row["ModDate"] + "</span>");
                itemInfo ownerInfo = getOwnerInfo(row[SearchRepository.DocField.OwnerInfo.ToString()].ToString());
                if (ownerInfo != null && !string.IsNullOrEmpty(ownerInfo.url))
                {
                    builder.Append("<span class='owner'>");
                    builder.Append("<a href=\"" + ownerInfo.url + "\">" + ownerInfo.itemName + "</a>");
                    builder.Append("</span>");
                }

                string text = row["Text"].ToString();
                if (!string.IsNullOrEmpty(text))
                {
                    builder.Append("<br />");
                    builder.Append(text);
                }
                return builder.ToString();
            }
            return null;
        }

        protected itemInfo getOwnerInfo(string ownerInfo)
        {
            if (string.IsNullOrEmpty(ownerInfo))
                return null;

            int index1 = ownerInfo.IndexOf(';');
            if (index1 <=0 )
                return null;
            string ownerType = ownerInfo.Substring(0, index1);

            index1++;

            int index2 = ownerInfo.IndexOf(';', index1);
            if (index2 <= 0)
                return null;
            int ownerId = 0;

            if (!int.TryParse(ownerInfo.Substring(index1, index2 - index1), out ownerId) && ownerId <= 0)
                return null;

            string ownerName = ownerInfo.Substring(index2 + 1);

            itemInfo info = new itemInfo(ownerType, ownerId, ownerId);
            info.itemName = ownerName;

            return info;
        }
        
        protected string getPager()
        {
            const int PageLinksToShow = 10;

            if (engine == null || engine.PageSize <= 0)
                return null;

            int PageCount = engine.Total / engine.PageSize;

            if (PageCount <= 0)
                return null;

            int CurrentPage = engine.CurrentPage;
            StringBuilder builder = new StringBuilder();

            int min = Math.Min(Math.Max(0, CurrentPage - (PageLinksToShow / 2)), Math.Max(0, PageCount - PageLinksToShow + 1));
            int max = Math.Min(PageCount, min + PageLinksToShow);

            StringBuilder urlBuilder = new StringBuilder();
            string url = Request.RawUrl;
            int index = url.IndexOf("?");
            if (index >= 0)
            {
                urlBuilder.Append(url.Substring(0, index));
                urlBuilder.Append("?");
                int keyCount = 0;
                foreach (string key in Request.QueryString.AllKeys)
                {
                    if (key.Equals("p", StringComparison.InvariantCultureIgnoreCase))
                        continue;

                    if (keyCount++ > 0)
                        urlBuilder.Append("&");

                    urlBuilder.Append(key);
                    urlBuilder.Append("=");
                    urlBuilder.Append(Request.QueryString[key]);
                }

                if (keyCount > 0)
                    urlBuilder.Append("&");
            }
            else
            {
                urlBuilder.Append(url);
                urlBuilder.Append("?");
            }
            urlBuilder.Append("p=");
            url = urlBuilder.ToString();

            if (min > 0)
            {
                int prev10 = CurrentPage - PageLinksToShow;
                if (prev10 < 0)
                    prev10 = 0;

                builder.Append(getPagerLink(url, prev10, "&hellip;"));
            }

            for (int i = min; i <= max; i++)
            {
                if (i == CurrentPage)
                    builder.Append("<b class='current-page'>" + (i + 1).ToString() + "</b>");
                else
                    builder.Append(getPagerLink(url, i, (i + 1).ToString()));
            }

            if (max < PageCount)
            {
                int next10 = CurrentPage + PageLinksToShow;
                if (next10 > PageCount)
                    next10 = PageCount;

                builder.Append(getPagerLink(url, next10, "&hellip;"));
            }

            return builder.ToString();
        }

        string getPagerLink(string url, int page, string text)
        {
            return "<a class='pager-link' href='" + url + (page + 1).ToString() + "'>" + text + "</a>";
        }

        protected bool showExtendedSearch
        {
            get
            {
                return args != null && args.ExSearch;
            }
        }

        protected IList<pseudoTreeItem> createIKTPseudoTree(IList<IKTComponent> IKTList)
        {
            //TODO: Пока закоментировал т.к. поис не актуален
            //var nodeDict = IKTList.Select(x => new pseudoTreeItem() { Id = x.Id, Name = x.Name, Year = x.Year < 1900 ? "Год не указан" : (x.Year.ToString() +  " год") })
            //                      .ToDictionary(x => x.Id, x => x);

            //foreach (var ikt in IKTList)
            //{
            //    if (ikt.Parent != null && nodeDict.ContainsKey(ikt.Parent.Id))
            //    {
            //        nodeDict[ikt.Parent.Id].Childs.Add(nodeDict[ikt.Id]);
            //        nodeDict[ikt.Id].isChild = true;
            //    }
            //}

            var treeItems = new List<pseudoTreeItem>();

            //var topLevel = nodeDict.Values.Where(x=>x.isChild == false)
            //                              .OrderBy(x => x.Name)
            //                              .GroupBy(x => x.Year);

            //foreach (var g in topLevel)
            //{
            //    var orderedRoot = g.OrderBy(x=>x.Name);
            //    foreach (var child in orderedRoot)
            //    {
            //        processItem(0, child, ref treeItems, ref nodeDict);
            //    }
            //}
            return treeItems;
        }

        private void processItem(int level, pseudoTreeItem current, ref List<pseudoTreeItem> treeItems, ref Dictionary<int, pseudoTreeItem> nodeDict)
        {
            if (!nodeDict.ContainsKey(current.Id))
                return;

            current.Name = new String(' ', level) + current.Name;
            current.Indent = level * 20;
            treeItems.Add(current);
            nodeDict.Remove(current.Id);

            if (current.Childs.Count > 0)
            {
                var orderredItems = current.Childs.OrderBy(x => x.Name);
                foreach(var childItem in orderredItems)
                {
                    processItem(level+1, childItem, ref treeItems, ref nodeDict);
                }
            }
        }

        protected class pseudoTreeItem
        {
            public pseudoTreeItem() { Childs = new List<pseudoTreeItem>(); }

            public int Id { get; set; }
            public string Name { get; set; }
            public string Year { get; set; }
            public int Indent { get; set; }
            public bool isChild { get; set; }
            public IList<pseudoTreeItem> Childs { get; protected set; }
        }
    }
}