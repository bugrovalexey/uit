﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Entities.Admin;
using GB.Repository;

namespace GB.MKRF.Web
{
    public partial class MainEmpty : System.Web.UI.MasterPage
    {
        protected string PageTitle
        {
            get 
            {
                return (SiteMap.Provider as GB.Providers.DbSiteMapProvider).GetTitlePage(Request.AppRelativeCurrentExecutionFilePath) ?? 
                    RepositoryBase<Option>.Get(OptionsEnum.PageTitle).Value; 
            }
        }
    }
}