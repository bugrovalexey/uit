﻿using System;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ControlHelpMap: ClassMapBase<ControlHelp>
    {
        public ControlHelpMap():
            base("Control_Help")
        {

        }

        protected override void InitMap()
        {
            Map(x => x.PageName, "Control_Help_Page");
            Map(x => x.ControlId, "Control_Help_Control");
            Map(x => x.Example, "Control_Help_Example").CustomSqlType("nvarchar(max)").Length(Int32.MaxValue);
            Map(x => x.Help, "Control_Help_Help").CustomSqlType("nvarchar(max)").Length(Int32.MaxValue);
        }
    }
}
