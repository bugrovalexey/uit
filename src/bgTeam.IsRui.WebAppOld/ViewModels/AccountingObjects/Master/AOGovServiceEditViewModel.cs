﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Frgu;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    [MetadataType(typeof(AOGovServiceEditViewModel_Valid))]
    public class AOGovServiceEditViewModel : ViewModelBase
    {
        public AOGovServiceEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;

            GovServicesNPA = new List<TableAOGovServicesNPA>();



            //GovServiceId = 

            GovServicesList = new SelectList(new RepositoryDc<GovService>().GetAll(), "Id", "Name");

            var entity = id.HasValue
                ? new RepositoryDc<AOGovService>().Get(id)
                : null;

            entity.Do(Fill);
        }

        #region Methods

        protected void Fill(AOGovService entity)
        {
            Id = entity.Id;
            AccountingObjectId = entity.AccountingObject.Id;
            GovServiceId = entity.GovService.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.Name } });

            GovServicesNPA =
                new RepositoryDc<AOGovServiceNPA>().GetAllEx(q => q.Where(x => x.GovService.Id == entity.Id))
                    .Select(x => new TableAOGovServicesNPA(x));

            PercentOfUseResource = entity.PercentOfUseResource;
        }

        #endregion Methods

        #region Props

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        [DisplayName("Государственная услуга/функция")]
        public IList<IEntityName> GovServiceId { get; set; }

        [DisplayName(TableAOGovServices.PercentOfUseResourceCaption)]
        public object PercentOfUseResource { get; set; }

        public IEnumerable<TableAOGovServicesNPA> GovServicesNPA { get; set; }

        public SelectList GovServicesList { get; set; }

        #endregion Props

    }

    public class AOGovServiceEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public object GovServiceId { get; set; }
    }
}