﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/PlanCard.master" AutoEventWireup="true" CodeBehind="MonitoringActivities.aspx.cs" Inherits="GB.MKRF.Web.Plans.MonitoringActivities" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <div class="page">
        <dxwgv:ASPxGridView runat="server" ID="grid" ClientInstanceName="grid" AutoGenerateColumns="false"
            Width="100%" KeyFieldName="Id" OnBeforePerformDataSelect="grid_BeforePerformDataSelect">
            <Settings ShowFilterRow="True" />
            <Columns>
                <dxwgv:GridViewCommandColumn Caption=" " ButtonType="Image" HeaderStyle-CssClass="bottomborder" Width="20px">
                    <ClearFilterButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/clear_16.png" Image-ToolTip="Очистить" />
                </dxwgv:GridViewCommandColumn>
                <dxwgv:GridViewDataTextColumn Caption="Код мероприятия" FieldName="Code" />
                <dxwgv:GridViewDataTextColumn Caption="Наименование мероприятия" FieldName="Name" >
                    <DataItemTemplate>
                        <%#GetLinkToActivityCard(Eval("Id"), Eval("Name"))%>
                    </DataItemTemplate>
                    </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewBandColumn Caption="Закупки">
                    <Columns>
                        <dxwgv:GridViewDataDateColumn Caption="Планируемая дата публикации" FieldName="PublishDate" Width="100px" />
                        <dxwgv:GridViewDataDateColumn Caption="Дата подведения итогов" FieldName="ItogDate" Width="100px" />
                        <dxwgv:GridViewDataTextColumn Caption="Статус" FieldName="Status" >
                            <Settings AllowHeaderFilter="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Наименование предмета контракта" FieldName="ProductName" />
                    </Columns>
                </dxwgv:GridViewBandColumn>
                <dxwgv:GridViewDataTextColumn Caption="Ответственное лицо" FieldName="Responsible" Width="200px" />
            </Columns>
            <SettingsPager PageSize="30" />
        </dxwgv:ASPxGridView>
    </div>
</asp:Content>
