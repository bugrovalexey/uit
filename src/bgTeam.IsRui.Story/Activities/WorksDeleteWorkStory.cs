﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class WorksDeleteWorkStory : IStory<WorksDeleteWorkStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;

        public WorksDeleteWorkStory(ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _factory = sessionNHibernateFactory;
        }

        public bool Execute(WorksDeleteWorkStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(WorksDeleteWorkStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            bool result;

            using (var session = _factory.OpenSession())
            {
                await session.DeleteAsync<ActivityWorks>(context.Id.Value);

                result = true;
            }

            return result;
        }
    }
}
