﻿using GB.Data.AccountingObjects;
using GB.Data.Directories;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateKindAccountingObjectCommand : ICommand<UpdateKindAccountingObjectContext>
    {
        ISessionFactoryGb factory;

        public UpdateKindAccountingObjectCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(UpdateKindAccountingObjectContext context)
        {
            var entity = new Repository<AccountingObject>().GetExisting(context.xId);

            entity.Kind = new Repository<IKTComponent>().Get(context.KindId);

            using (var session = factory.Create())
            {
                session.Update(entity);
                session.Commit();
            }
        }
    }
}
