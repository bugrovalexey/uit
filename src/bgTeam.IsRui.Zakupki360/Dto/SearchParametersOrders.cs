﻿namespace bgTeam.IsRui.Zakupki360.Dto
{
    public class SearchParametersOrders
    {
        public string OrderNumber { get; set; }

        public int? ResCount { get; set; }
    }
}
