﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="GB.MKRF.Web.Activities.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        ActiveMenuItem = 2;

        function validateComboBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && e.value != 0;
        }

        function GridSave() {
            hf.Set('Name', NameMemo.GetText());
            hf.Set('Type', TypeComboBox.GetValue());
        }
    </script>

    <div class="page" style="position: relative">
        <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
        <div class="detailTop">
            <dxe:ASPxButton ID="addButton" runat="server" ClientInstanceName="addButton" AutoPostBack="false"
                Text="Добавить" CssClass="topButton fleft">
                <ClientSideEvents Click="function(s,e) { Grid.AddNewRow(); }" />
            </dxe:ASPxButton>
        </div>
        <dxwgv:ASPxGridView runat="server" ID="Grid" KeyFieldName="Id" Width="100%" AutoGenerateColumns="False"
            ClientInstanceName="Grid" OnBeforePerformDataSelect="Grid_BeforePerformDataSelect" OnStartRowEditing="Grid_StartRowEditing"
            OnRowInserting="Grid_RowInserting" OnRowUpdating="Grid_RowUpdating" OnRowDeleting="Grid_RowDeleting" OnCustomButtonCallback="Grid_CustomButtonCallback">
            <Settings ShowFooter="True" ShowFilterRow="true" />
            <%--<Settings ShowFooter="True" ShowColumnHeaders="false" />
            <Border BorderStyle="None" />--%>
            <Columns>
                <%--<dxwgv:GridViewCommandColumn Caption=" " Name="copyColumn" ShowInCustomizationForm="False"
                    AllowDragDrop="False" CellStyle-VerticalAlign="Middle" ButtonType="Image" HeaderStyle-CssClass="bottomborder">
                    <CustomButtons>
                        <dxwgv:GridViewCommandColumnCustomButton ID="copyButton" Image-ToolTip="Копировать" Image-Url="/Content/GB/Styles/Imgs/copy_16.png" />
                    </CustomButtons>
                </dxwgv:GridViewCommandColumn>--%>
                <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False"
                    AllowDragDrop="False" ButtonType="Image" CellStyle-VerticalAlign="Middle">
                    <EditButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/edit_16.png" Image-ToolTip="Изменить" />
                    <ClearFilterButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/clear_16.png" Image-ToolTip="Очистить" />
                </dxwgv:GridViewCommandColumn>
                <dxwgv:GridViewDataTextColumn FieldName="ActivityType2.Name" Caption="Тип">
                    <Settings AutoFilterCondition="Contains" AllowSort="True" SortMode="Value" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Наименование">
                    <DataItemTemplate>
                        <%#GetLinkToActivityCard(Eval("Id"),Eval("Name")) %>
                    </DataItemTemplate>
                    <Settings AutoFilterCondition="Contains" AllowSort="True" SortMode="Value" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="AccountingObject.ShortName" Caption="Наименование ОУ">
                    <Settings AutoFilterCondition="Contains" AllowSort="True" SortMode="Value" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewBandColumn Caption="Расходы на мероприятия по информатизации, тыс. руб" Visible="true">
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="PlansActivityVolY0" Width="150px">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="PlansActivityVolY1" Width="150px">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="PlansActivityVolY2" Width="150px">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                </dxwgv:GridViewBandColumn>
                <dxwgv:GridViewCommandColumn Caption=" " Name="delColumn" ShowInCustomizationForm="False" AllowDragDrop="False" ButtonType="Image">
                    <DeleteButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/delete_16.png" Image-ToolTip="Удалить" />
                </dxwgv:GridViewCommandColumn>
            </Columns>
            <TotalSummary>
                <dxwgv:ASPxSummaryItem ShowInColumn="Name" DisplayFormat="Количество: {0}" SummaryType="Count" />
                <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY0" ShowInColumn="PlansActivityVolY0" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY1" ShowInColumn="PlansActivityVolY1" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY2" ShowInColumn="PlansActivityVolY2" SummaryType="Sum" />
            </TotalSummary>
            <Templates>
                <EditForm>
                    <table class="form_edit">
                        <%--<tr>
                            <td class="form_edit_desc">Организация:
                            </td>
                            <td class="form_edit_input">
                                <dxe:ASPxLabel runat="server" ID="DepCheckBox" OnDataBinding="DepCheckBox_DataBinding" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td class="form_edit_desc required">Наименование:
                            </td>
                            <td class="form_edit_input">
                                <dxe:ASPxMemo ID="NameMemo" runat="server" ClientInstanceName="NameMemo" OnDataBinding="NameMemo_DataBinding"
                                    ValidationSettings-ValidationGroup="<%# Container.ValidationGroup%>">
                                    <ValidationSettings ErrorDisplayMode="Text">
                                        <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                    </ValidationSettings>
                                </dxe:ASPxMemo>
                            </td>
                        </tr>
                        <tr>
                            <td class="form_edit_desc required">Тип мероприятия:
                            </td>
                            <td class="form_edit_input">
                                <dxe:ASPxLabel ID="TypeLabel" runat="server" OnDataBinding="TypeLabel_DataBinding" />
                                <dxe:ASPxComboBox ID="TypeComboBox" runat="server" ClientInstanceName="TypeComboBox"
                                    TextField="Name" ValueField="Id" ValueType="System.Int32" 
                                    OnDataBinding="TypeComboBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup%>">
                                    <ValidationSettings ErrorDisplayMode="Text" />
                                    <ClientSideEvents Validation="function(s,e){ validateComboBox(s, e); }" />
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="form_edit_buttons">
                                <span class="ok"><a onclick="Grid.UpdateEdit();">Сохранить</a></span>
                                <span class="cancel"><a onclick="Grid.CancelEdit();">Отмена</a></span>
                            </td>
                        </tr>
                    </table>
                </EditForm>
                <%--<DataRow>
                    <div><%#GetLinkToActivityCard(Eval("Id"),Eval("Name")) %></div>
                    <div class="form_edit_buttons">
                        <span class="ok"><a onclick="Grid.StartEditRow(<%# Container.VisibleIndex%>);">Редактировать</a></span>
                        <span class="cancel"><a onclick="Grid.DeleteRow(<%# Container.VisibleIndex%>);">Удалить</a></span>
                    </div>
                </DataRow>--%>
            </Templates>
            <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') GridSave(); }" />
        </dxwgv:ASPxGridView>
    </div>
</asp:Content>
