﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityGoodsDto
    {
        public int? Id { get; set;}

        public int? ActivityId { get; set; }

        public virtual string Name { get; set; }

        //public virtual int? OKPDId { get; set; }

        //public virtual int? ProductGroupId { get; set; }

        //public virtual int? ExpenseDirectionId { get; set; }

        //public virtual int? ExpenseTypeId { get; set; }

        //public virtual int? KOSGUId { get; set; }

        //public virtual YearEnum Year { get; set; }

        public virtual int? Quantity { get; set; }

        //public virtual int? Duration { get; set; }

        public virtual int? Cost { get; set; }

        public virtual decimal? Summ { get; set; }

        public QuarterEnum Quarter { get; set; }

        public string AnalogName { get; set; }

        public decimal AnalogCost { get; set; }

        public IList<GoodsCharacteristicDto> Characteristic { get; set; }
    }
}
