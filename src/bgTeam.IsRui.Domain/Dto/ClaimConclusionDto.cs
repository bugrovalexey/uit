﻿namespace bgTeam.IsRui.Domain.Dto
{
    public class ClaimConclusionDto
    {
        public int Id { get; set; }

        public int ClaimId { get; set; }

        public string ClaimName { get; set; }

        public string Comment { get; set; }
    }
}
