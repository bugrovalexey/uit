﻿using Uviri.MKRF.Entities.Plans.ActivityOffers;

namespace Uviri.MKRF.Mapping.Plans.ActivityOffers
{
    public class ActivityOfferDocMap : ClassMapBase<ActivityOfferDoc>
    {
        public ActivityOfferDocMap()
            : base("Activity_Offer_Doc")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "Activity_Offer_Doc_Num");
            Map(x => x.Date, "Activity_Offer_Doc_Date");
            Map(x => x.Name, "Activity_Offer_Doc_Name");
            Map(x => x.Points, "Activity_Offer_Doc_Points");

            References<ActivityOffer>(x => x.Activity, "Activity_Offer_Inform_ID");
        }
    }
}