﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;

    public class AoAllCharacteristicsDto
    {
        //public IList<AccountingObjectCharacteristics> Characteristics { get; set; }
        /// <summary>ОУ</summary>
        public int AoId { get; set; }

        /// <summary>Ид</summary>
        public int Id { get; set; }

        /// <summary>Наименование</summary>
        public string Name { get; set; }

        /// <summary>Тип</summary>
        public int TypeId { get; set; }

        /// <summary>Единица измерения</summary>
        public int UnitId { get; set; }

        /// <summary>Номинальное значение</summary>
        public int Norm { get; set; }

        /// <summary>Максимальное значение</summary>
        public virtual int? Max { get; set; }

        /// <summary>Фактическое значение</summary>
        public int? Fact { get; set; }

        /// <summary>Дата начала</summary>
        public DateTime DateBeginning { get; set; }

        /// <summary>Дата конца</summary>
        public DateTime? DateEnd { get; set; }
    }
}
