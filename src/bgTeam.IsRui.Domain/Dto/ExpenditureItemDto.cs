﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;

    public class ExpenditureItemDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Код
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Дата начала действия справочника
        /// </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary>
        /// Дата окончания действия справочника
        /// </summary>
        public virtual DateTime? DateEnd { get; set; }
    }
}
