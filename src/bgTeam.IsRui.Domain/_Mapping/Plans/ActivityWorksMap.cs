﻿namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityWorksMap
    {
        internal class ActivityWorksMapMap : ClassMapHiLo<ActivityWorks>
        {
            public ActivityWorksMapMap()
                : base("Plans_Activity_Works")
            {
            }

            protected override void InitMap()
            {
                Map(x => x.Name, "Plans_Activity_Works_Name");
                /*Map(x => x.Year, "Plans_Activity_Works_Year");
                Map(x => x.SpecialistsCount, "Plans_Activity_Works_SpecialistsCount");
                Map(x => x.Salary, "Plans_Activity_Works_Salary");*/
                Map(x => x.Duration, "Plans_Activity_Works_Duration");
                Map(x => x.Summ, "Plans_Activity_Works_Summ");
                Map(x => x.ChangeDate, "TimeStamp");
                Map(x => x.PlansActivityId, "Plans_Activity_ID");
                Map(x => x.Quarter, "Plans_Activity_Works_Quarter");

                /*References(x => x.OKPD, "Plans_Activity_Works_OKPDId");
                References(x => x.ExpenseDirection, "Plans_Activity_Works_ExpenseDirectionId");
                References(x => x.ExpenseType, "Plans_Activity_Works_ExpenseTypeId");
                References(x => x.KOSGU, "Plans_Activity_Works_KOSGUId");
                References(x => x.OKVED, "Plans_Activity_Works_OKVEDId");*/
            }
        }
    }
}
