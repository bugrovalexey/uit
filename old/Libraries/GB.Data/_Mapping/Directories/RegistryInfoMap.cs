﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class RegistryInfoMap : ClassMapBase<RegistryInfo>
    {
        public RegistryInfoMap()
            : base("Registry_Info")
        {
//            Table("w_");

//            Id(x => x.Id, "Registry_Info_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"
//                @Registry_Info_Name = ?,
//                @Registry_Info_Orig_ID = ?,
//                @Govt_Organ_ID = ?,
//                @Registry_Info_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Registry_Info_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Registry_Info_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Registry_Info_Delete @Registry_Info_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Registry_Info_Name").Nullable();
            Map(x => x.OridId, "Registry_Info_Orig_ID");

            References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
        }
    }
}
