﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201803131424)]
    public class Migration_201803131424 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Role").AddColumn("Role_Code").AsString(50).Nullable();
        }
    }
}
