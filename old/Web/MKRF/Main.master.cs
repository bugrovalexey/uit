﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.Helpers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Providers;

namespace GB.MKRF.Web
{
    public partial class Main : System.Web.UI.MasterPage
    {
        User currentUserInternal = null;
        protected User currentUser
        {
            get
            {
                if (currentUserInternal == null)
                {
                    currentUserInternal = UserRepository.GetCurrent();
                }
                return currentUserInternal;
            }
        }

        protected string CurrentName
        {
            get
            {
                string userName = currentUser.FullName.Trim();

                if (string.IsNullOrEmpty(userName))
                    userName = currentUser.Login;

                return string.Concat("<a href='", Page.ResolveClientUrl("~/PrivateOffice/UserMainData.aspx"), "'>", userName, "</a>");
            }
        }

        protected string GetMenu()
        {
            string str = string.Empty;

            Role role = UserRepository.GetCurrent().Role as Role;

            if (!role.IsEq(RoleEnum.Admin))
            {
                str = string.Format(
                    @"<li><a href='{0}'>Планирование</a></li>
                    <li><a href='{1}'>Мероприятия</a></li>
                    <li><a href='{2}'>Объекты учета</a></li>",
                    UrlHelper.Resolve("~/Plans/List.aspx"),
                    UrlHelper.Resolve("~/Activities/List.aspx"),
                    UrlHelper.Resolve("~/AccountingObjects/List.aspx"));
            }
//            else if (role.IsEq(RoleEnum.MKRF))
//            {
//                str = string.Format(
//                    @"<li><a href='{0}'>Планирование</a></li>
//                    <li><a href='{1}'>Реализация</a></li>
//                    <li><a href='{2}'>Отчеты</a></li>",
//                    UrlHelper.Resolve("~/Plans/v2012new/List.aspx"),
//                    UrlHelper.Resolve("~/Plans/v2012rel/List.aspx"),
//                    UrlHelper.Resolve("~/Monitoring/v2012new/List.aspx"));
//            }

            return str;
        }

        /// <summary>Ссылка на скачивание руководства пользователя</summary>
        public string ManualDownloadUrl
        {
            get
            {
                return ResolveClientUrl(System.Configuration.ConfigurationManager.AppSettings["UsersManualDownloadUrl"]);
            }
        }

        protected void menu_ItemDataBound(object source, DevExpress.Web.ASPxMenu.MenuItemEventArgs e)
        {
            if (e.Item.NavigateUrl.EndsWith("#"))
                e.Item.NavigateUrl = string.Empty;

            //открывать раздел справочная информация в новом окне
            if (e.Item.Parent != null &&
                e.Item.Parent.DataItem != null &&
                (e.Item.Parent.DataItem as System.Web.SiteMapNode).Title.Equals("Справочная информация"))
                e.Item.Target = "_blank";
        }

        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);

            if (!Validation.Validator.UserAccessToPage(Request.AppRelativeCurrentExecutionFilePath))
                ResponseHelper.DenyAccess(this.Response);

            lbtnLogout.Visible = currentUser != null;
        }

        protected void lbtnLogout_Click(object sender, System.EventArgs args)
        {
            FormsAuthentication.SignOut();
            Response.Redirect(FormsAuthentication.GetRedirectUrl(UserRepository.GetCurrent().Login, false));
        }
    }
}