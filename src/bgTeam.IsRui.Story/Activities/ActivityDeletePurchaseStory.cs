﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.DataAccess;
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityDeletePurchaseStory : IStory<ActivityDeletePurchaseStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;

        public ActivityDeletePurchaseStory(
            ISessionNHibernateFactory sessionNHibernateFactory,
            IRepositoryRui repository)
        {
            _factory = sessionNHibernateFactory;
            _repository = repository;
        }

        public bool Execute(ActivityDeletePurchaseStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(ActivityDeletePurchaseStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            var activityPurchase = _repository.Get<ActivityPurchase>(context.Id);

            if (activityPurchase == null)
            {
                throw new IsRuiException("Не найдена закупка");
            }

            using (var session = _factory.OpenSession())
            {
                await session.DeleteAsync(activityPurchase);
            }

            return true;
        }
    }
}
