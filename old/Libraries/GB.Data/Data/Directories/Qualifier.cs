﻿namespace GB.MKRF.Entities.Directories
{
    /// <summary>Год</summary>
    public class Qualifier : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null

        /// <summary>Описание</summary>
        public virtual string Descr { get; set; }//null

        public virtual QualifierGroup Group { get; set; }
    }
}