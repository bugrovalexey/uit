﻿<%@ Page Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="Notifications.aspx.cs" Inherits="GB.MKRF.Web.Selectors.Notifications" %>

<asp:Content ID="Head" ContentPlaceHolderID="Head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
     <script type="text/javascript">
     // <![CDATA[
         function rowClick(s, e) {
             var id = s.GetRowKey(e.visibleIndex);
             window.parent.PopupClose(id);
         }
      // ]]> 
    </script>
    <dxwgv:ASPxGridView ID="SelectorsNotificationsGrid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" KeyFieldName="Id"
        OnBeforePerformDataSelect="SelectorsNotificationsGrid_BeforePerformDataSelect" Width="100%" OnHtmlRowCreated="SelectorsNotificationsGrid_HtmlRowCreated">
        <Columns>
            <dxwgv:GridViewDataTextColumn Caption="Название" FieldName="Name" />
            <dxwgv:GridViewDataTextColumn Caption="Тип" FieldName="Type" />
        </Columns>
        <Styles>
            <AlternatingRow Enabled="true" />
            <Header Wrap="True" />
        </Styles> 
        <ClientSideEvents RowClick="rowClick" />
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="15" />
    </dxwgv:ASPxGridView>
</asp:Content>
