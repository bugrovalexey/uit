﻿using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Domain.Entities.Workflow;

namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    internal class ActivityExpertiseConclusionMap : ClassMapHiLo<ActivityExpertiseConclusion>
    {
        public ActivityExpertiseConclusionMap()
            : base("Activity_Expertise_Conclusion") { }

        protected override void InitMap()
        {
            References<Status>(x => x.Status, "Status_ID").Not.Nullable();

            HasMany(x => x.Experts).KeyColumns.Add("Activity_Expertise_Conclusion_ID").Inverse();
        }
    }
}