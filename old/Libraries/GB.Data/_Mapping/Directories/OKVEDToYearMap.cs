﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class OKVEDToYearMap : ClassMapBase<OKVEDToYear>
    {
        public OKVEDToYearMap()
            : base("OKVED_Years", "v_", true)
        {
            //Table("v_");

            //Id(x => x.Id, "OKVED_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "OKVED_Code");
            Map(x => x.CodeName, "OKVED_CodeName");
            Map(x => x.Year, "Year");
        }
    }
}
