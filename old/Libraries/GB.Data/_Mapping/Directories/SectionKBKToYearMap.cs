﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class SectionKBKToYearMap : ClassMapBase<SectionKBKToYear>
    {
        public SectionKBKToYearMap()
            : base("Section_KBK_Years", "v_", true)
        {
            //Table("v_");

            //Id(x => x.Id, "Section_KBK_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "Section_KBK_Code");
            Map(x => x.CodeName, "Section_KBK_CodeName");
            Map(x => x.Year, "Year");
        }
    }
}
