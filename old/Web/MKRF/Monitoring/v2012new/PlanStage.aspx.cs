﻿using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;

namespace GB.MKRF.Web.Monitoring.v2012new
{
    public partial class PlanStage : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            YearsComboBox.DataBind();
            DepartmentComboBox.DataBind();

            ChartDataBinding();
        }

        protected void YearsComboBox_DataBinding(object sender, EventArgs e)
        {
            IList<Years> years = RepositoryBase<Years>.GetAll(x => x.Value >= 2012).Take(10).ToList();

            ASPxComboBox box = sender as ASPxComboBox;
            
            box.DataSource = years;

            var year = Request.QueryString["year"];

            if (string.IsNullOrEmpty(year))
            {
                box.Value = DateTime.Now.Year;
            }
            else
            {
                box.Value = int.Parse(year);
            }
        }

        protected void DepartmentComboBox_DataBinding(object sender, EventArgs e)
        {
            var data = DepartmentRepository.GetAllWithPlan();

            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = data;

            var dep = Request.QueryString["dep"];

            if (string.IsNullOrEmpty(dep))
            {
                box.SelectedIndex = 0;
            }
            else
            {
                box.Value = int.Parse(dep);
            }
        }

        protected void ChartDataBinding()
        {
            var Data = MonitoringRepository.GetPlanStage(DepartmentComboBox.Value, YearsComboBox.Value);

            int i = Chart.Series.Add(string.Empty, ViewType.Bar);
            Chart.Series[i].LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            Chart.Series[i].Label.ResolveOverlappingMode = ResolveOverlappingMode.HideOverlapped;
            Chart.Series[i].CrosshairLabelPattern = "{S} : {V:#,##0.0000}";
            Chart.Series[i].Label.TextPattern = "{V:#,##0.0000}";
            Chart.Series[i].Points.Clear();

            Chart.Series[i].Points.Add(new SeriesPoint("Этап 1", Data.Rows[0][0]));
            Chart.Series[i].Points.Add(new SeriesPoint("Этап 2", Data.Rows[0][1]));
            Chart.Series[i].Points.Add(new SeriesPoint("Этап 3", Data.Rows[0][2]));
        }
    }
}