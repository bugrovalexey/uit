﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class UnitsMap : ClassMapHiLo<Units>
    {
        public UnitsMap()
            : base("MUnit")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "MUnit_Name");
            Map(x => x.Abbreviation, "MUnit_SName");
        }
    }
}