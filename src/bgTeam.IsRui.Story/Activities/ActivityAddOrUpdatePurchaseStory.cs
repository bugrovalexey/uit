﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityAddOrUpdatePurchaseStory : IStory<ActivityAddOrUpdatePurchaseStoryContext, ActivityPurchaseDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;
        private readonly ISessionNHibernateFactory _factory;

        public ActivityAddOrUpdatePurchaseStory(
            IRepositoryRui repository,
            IMapperBase mapper,
            ISessionNHibernateFactory factory)
        {
            _mapper = mapper;
            _repository = repository;
            _factory = factory;
        }

        public ActivityPurchaseDto Execute(ActivityAddOrUpdatePurchaseStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityPurchaseDto> ExecuteAsync(ActivityAddOrUpdatePurchaseStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var activity = _repository.Get<PlansActivity>(context.ActivityId);

            if (activity == null)
            {
                throw new IsRuiException("Не найдено мероприятие");
            }

            var purchase = _mapper.Map(context, new ActivityPurchase());
            purchase.Activity = activity;
            purchase.TimeStamp = DateTime.Now;

            using (var session = _factory.OpenSession())
            {
                if (context.Id.HasValue)
                {
                    await session.UpdateAsync(purchase);
                }
                else
                {
                    await session.InsertAsync(purchase);
                }
            }

            return _mapper.Map(purchase, new ActivityPurchaseDto());
        }
    }
}
