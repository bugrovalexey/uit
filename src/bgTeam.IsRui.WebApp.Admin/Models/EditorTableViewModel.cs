﻿namespace bgTeam.IsRui.WebApp.Admin.Models
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using bgTeam.IsRui.WebApp.Admin.Domain;

    public class EditorTableViewModel
    {
        public int DictId { get; private set; }

        public IEnumerable<TableRowDataView> Data { get; private set; }

        public IEnumerable<MetaColumn> Columns { get; private set; }

        //public bool DeleteEnable { get; set; }
        public bool ReadOnly { get; private set; }

        public EditorTableViewModel(MetaDictionary dict, IEnumerable<MetaColumn> columns, IEnumerable<dynamic> data)
        {
            DictId = dict.Id;
            ReadOnly = dict.ReadOnly;

            if (AppSettings.PRIMARY_KEY_SHOW)
            {
                Columns = columns;
            }
            else
            {
                Columns = columns.Where(x => x.Name != AppSettings.PRIMARY_KEY);
            }

            var colPK = columns.Single(x => x.Name == AppSettings.PRIMARY_KEY);

            Data = data.Select(x => new TableRowDataView(x, colPK.ColumnName));
        }
    }
}