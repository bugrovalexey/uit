﻿using AutoMapper;
using GB.Data.AccountingObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.GovContracts
{
    public class AddGovContractContext : ICommandContext
    {
        public int IdAO { get; set; }
        public int IdZK { get; set; }

        public int Status { get; set; }
        public string Number { get; set; }
        public string Order { get; set; }
        public DateTime? Date { get; set; }
        public string Executer { get; set; }
        public decimal Price { get; set; }
        public string RcDeviation { get; set; }
    }
}
