﻿using GB.Infrastructure;
using GB.MKRF.Repository;
using GB.Providers;

namespace GB
{
    public abstract class ValidatorBase
    {
        /// <summary>
        /// Проверяет, есть ли у текущего пользователя доступ к странице
        /// </summary>
        public static bool UserAccessToPage(string page)
        {
            var currentUser = UserRepository.GetCurrent();

            return Singleton<AccessControllerProvider>.Instance.СheckAccessUser(page, currentUser);
        }
    }
}
