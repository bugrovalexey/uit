﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Uviri.MKRF.Web.Search
{
    public class SearchArgs
    {
            public string Query { get; set; }

            // отображать панель расширенного поиска
            public bool ExSearch { get; set; }

            public DateTime? Date1 { get; set; }
            public DateTime? Date2 { get; set; }

            public int? Year1 { get; set; }
            public int? Year2 { get; set; }

            public int? Num { get; set; }

            public HashSet<int> IKT { get; set; }
            public HashSet<int> Department { get; set; }
            public HashSet<int> Type { get; set; }
            public HashSet<int> Status { get; set; }

            public string Author { get; set; }
            public string Curator { get; set; }


            public double? sF_1 { get; set; }
            public double? sF_2 { get; set; }

            public double? sR_1 { get; set; }
            public double? sR_2 { get; set; }

            public double? sV_1 { get; set; }
            public double? sV_2 { get; set; }

            public double? sSum_1 { get; set; }
            public double? sSum_2 { get; set; }

            public double? sY0_1 { get; set; }
            public double? sY0_2 { get; set; }

            public double? sY1_1 { get; set; }
            public double? sY1_2 { get; set; }

            public double? sY2_1 { get; set; }
            public double? sY2_2 { get; set; }

            public double? sAddY0_1 { get; set; }
            public double? sAddY0_2 { get; set; }

            public double? sAddY1_1 { get; set; }
            public double? sAddY1_2 { get; set; }

            public double? sAddY2_1 { get; set; }
            public double? sAddY2_2 { get; set; }

            public double? sSumY0_1 { get; set; }
            public double? sSumY0_2 { get; set; }

            public double? sSumY1_1 { get; set; }
            public double? sSumY1_2 { get; set; }

            public double? sSumY2_1 { get; set; }
            public double? sSumY2_2 { get; set; }


            public int Page { get; set; }
            public int PageSize { get; set; }


            public SearchArgs(NameValueCollection QueryString)
            {
                DateTime tempDate;
                int tempInt;
                string temp;

                Query = QueryString["q"];
                Curator = QueryString["c"];
                Author = QueryString["a"];

                ExSearch = QueryString["x"] == "1";

                Type = splitInt(QueryString["t"]);
                Department = splitInt(QueryString["d"]);
                Status = splitInt(QueryString["s"]);
                IKT = splitInt(QueryString["i"]);
                

                
                

                temp = QueryString["d1"];
                if (!string.IsNullOrEmpty(temp))
                {
                    if (DateTime.TryParse(temp, out tempDate))
                        Date1 = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 0, 0, 0);
                }

                temp = QueryString["d2"];
                if (!string.IsNullOrEmpty(temp))
                {
                    if (DateTime.TryParse(temp, out tempDate))
                        Date2 = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 23, 59, 59);
                }

                temp = QueryString["y1"];
                if (!string.IsNullOrEmpty(temp))
                {
                    if (int.TryParse(temp, out tempInt))
                        Year1 = tempInt;
                }

                temp = QueryString["y2"];
                if (!string.IsNullOrEmpty(temp))
                {
                    if (int.TryParse(temp, out tempInt))
                        Year2 = tempInt;
                }

                temp = QueryString["n"];
                if (!string.IsNullOrEmpty(temp))
                {
                    if (int.TryParse(temp, out tempInt))
                        Num = tempInt;
                }

                sF_1 = getDecimal(QueryString["f1"]);
                sF_2 = getDecimal(QueryString["f2"]);
                sR_1 = getDecimal(QueryString["r1"]);
                sR_2 = getDecimal(QueryString["r2"]);
                sV_1 = getDecimal(QueryString["v1"]);
                sV_2 = getDecimal(QueryString["v2"]);
                sSum_1 = getDecimal(QueryString["s1"]);
                sSum_2 = getDecimal(QueryString["s2"]);
                sY0_1 = getDecimal(QueryString["y01"]);
                sY0_2 = getDecimal(QueryString["y02"]);
                sY1_1 = getDecimal(QueryString["y11"]);
                sY1_2 = getDecimal(QueryString["y12"]);
                sY2_1 = getDecimal(QueryString["y21"]);
                sY2_2 = getDecimal(QueryString["y22"]);
                sAddY0_1 = getDecimal(QueryString["add01"]);
                sAddY0_2 = getDecimal(QueryString["add02"]);
                sAddY1_1 = getDecimal(QueryString["add11"]);
                sAddY1_2 = getDecimal(QueryString["add12"]);
                sAddY2_1 = getDecimal(QueryString["add21"]);
                sAddY2_2 = getDecimal(QueryString["add22"]);
                sSumY0_1 = getDecimal(QueryString["sum01"]);
                sSumY0_2 = getDecimal(QueryString["sum02"]);
                sSumY1_1 = getDecimal(QueryString["sum11"]);
                sSumY1_2 = getDecimal(QueryString["sum12"]);
                sSumY2_1 = getDecimal(QueryString["sum21"]);
                sSumY2_2 = getDecimal(QueryString["sum22"]);          


                temp = QueryString["p"];
                if (!string.IsNullOrEmpty(temp))
                {
                    if (int.TryParse(temp, out tempInt) && tempInt > 0)
                        Page = tempInt - 1;
                }

                PageSize = 10;

                ExSearch =
                        Date1.HasValue ||
                        Date2.HasValue ||
                        Year1.HasValue ||
                        Year2.HasValue ||
                        Num.HasValue ||
                        IKT.Count > 0 ||
                        Department.Count > 0 ||
                        Type.Count > 0 ||
                        Status.Count > 0 ||
                        !string.IsNullOrEmpty(Author) ||
                        !string.IsNullOrEmpty(Curator) ||
                        sF_1.HasValue ||
                        sF_2.HasValue ||
                        sR_1.HasValue ||
                        sR_2.HasValue ||
                        sV_1.HasValue ||
                        sV_2.HasValue ||
                        sSum_1.HasValue ||
                        sSum_2.HasValue ||
                        sY0_1.HasValue ||
                        sY0_2.HasValue ||
                        sY1_1.HasValue ||
                        sY1_2.HasValue ||
                        sY2_1.HasValue ||
                        sY2_2.HasValue ||
                        sAddY0_1.HasValue ||
                        sAddY0_2.HasValue ||
                        sAddY1_1.HasValue ||
                        sAddY1_2.HasValue ||
                        sAddY2_1.HasValue ||
                        sAddY2_2.HasValue ||
                        sSumY0_1.HasValue ||
                        sSumY0_2.HasValue ||
                        sSumY1_1.HasValue ||
                        sSumY1_2.HasValue ||
                        sSumY2_1.HasValue ||
                        sSumY2_2.HasValue;
            }

            private double? getDecimal(string queryArg)
            {
                if (string.IsNullOrEmpty(queryArg))
                    return null;
                double temp = 0.0;

                string decimalSeparator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
                queryArg = queryArg.Replace(".", decimalSeparator).Replace(",", decimalSeparator);


                if (!double.TryParse(queryArg, out temp))
                    return null;

                return temp;
            }

            public int? GetId(HashSet<int> set)
            {
                if (set != null && set.Count > 0)
                {
                    IEnumerator<int> enumerator = set.GetEnumerator();
                    if (enumerator.MoveNext())
                        return enumerator.Current;
                }
                return null;
            }

            public HashSet<int> splitInt(string joinedDigits)
            {
                HashSet<int> result = new HashSet<int>();

                if (!string.IsNullOrEmpty(joinedDigits))
                {
                    int tempInt = 0;
                    string[] digits = joinedDigits.Split(',');
                    foreach (string d in digits)
                    {
                        if (int.TryParse(d, out tempInt) && !result.Contains(tempInt))
                        {
                            result.Add(tempInt);
                        }
                    }
                }
                return result;
            }
    }
}