﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServiceNPAs
{
    public class DeleteAOGovServiceNPAContext : ICommandContext
    {
        public int? xId { get; set; }
    }
}
