﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace GB.Helpers
{
    public class CryptoHelper
    {
        /// <summary>Вычислить хеш исходного массива</summary>
        /// <param name="File">Массив</param>
        /// <returns>Хэш</returns>
        public static string ComputeHash(byte[] File)
        {
            if (File == null)
                return null;

            MD5 md5Hasher = MD5.Create();
            return Convert.ToBase64String(md5Hasher.ComputeHash(File));
        }

        public static string GetMD5Hash(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }
}