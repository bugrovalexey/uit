﻿using GB.Data.AccountingObjects;

namespace GB.MKRF.Services.Args
{
    public class SoftwareArgs : BaseArgsNew, IServiceArgs
    {
        public int AccountingObjectId { get; set; }

        public string Name { get; set; }

        public int? CategoryKindOfSupportId { get; set; }

        public int? ManufacturerId { get; set; }

        public int Amount { get; set; }

        public decimal Summa { get; set; }

        public RightsOnSoftwareEnum Right { get; set; }
    }
}