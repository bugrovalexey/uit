﻿namespace bgTeam.IsRui.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Extensions;

    public class AccessEntityProvider
    {
        private readonly IUserRepository _userRepository;

        public AccessEntityProvider(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        private readonly object _lock = new object();
        private readonly ManualResetEvent _mre = new ManualResetEvent(false);
        private IList<AccessToEntity> _accessRights;

        /// <summary>
        /// Использовать через Singleton
        /// </summary>
        protected AccessEntityProvider()
        {
            LoadData();
        }

        /// <summary>
        /// Обновляет список прав
        /// </summary>
        public void Reset()
        {
            LoadData();
        }

        /// <summary>
        /// Возвращает наличие права на создание объекта указанного типа
        /// </summary>
        /// <returns></returns>
        public bool CanCreate(EntityType entityType)
        {
            return true; 

            var roles = _userRepository.GetCurrent().Roles.Select(x => x.Id);
            var rights = GetRights(r =>
                r.Where(x => x.EntityType == entityType
                          && x.Status == null
                          && roles.Contains(x.Role.Id)));

            var access = rights.Any() ? rights.Max(r => r.AccessMask) : AccessActionEnum.None;
            return access.CanDo(AccessActionEnum.Create);
        }

        /// <summary>
        /// Выставляет права доступа к объекту
        /// </summary>
        public void SetAccess(IWorkflowObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");

            var entityType = obj.GetTableType();
            var roles = _userRepository.GetCurrent().Roles.Select(x => x.Id);
            var rights = GetRights(r =>
                r.Where(x => x.EntityType == entityType
                          && x.Status.IsEq(obj.Status)
                          && roles.Contains(x.Role.Id)));

            obj.Access = rights.Any() ? rights.Max(r => r.AccessMask) : AccessActionEnum.None;
        }

        private AccessToEntity[] GetRights(Func<IList<AccessToEntity>, IEnumerable<AccessToEntity>> expression)
        {
            _mre.WaitOne(); //ждем окончания работы LoadAccessRights
            return expression(_accessRights).ToArray();
        }

        private void LoadData()
        {
            lock (_lock)
            {
                _mre.Reset(); //блокируем читающие потоки
                _accessRights = new RepositoryDc<AccessToEntity>().GetAll();
                _mre.Set(); //разблокируем читающие потоки
            }
        }
    }
}