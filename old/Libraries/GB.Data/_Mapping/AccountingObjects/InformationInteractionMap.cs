﻿using GB.Data.AccountingObjects;

namespace GB._Mapping.AccountingObjects
{
    internal class InformationInteractionMap : ClassMapHiLo<InformationInteraction>
    {
        public InformationInteractionMap()
            : base("Information_Exchange")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Information_Exchange_Name").Not.Nullable();
            Map(x => x.Purpose, "Information_Exchange_Goal");
            Map(x => x.SMEV, "Information_Exchange_SMEV");
            Map(x => x.Title, "Information_Exchange_Heading");
            Map(x => x.Url, "Information_Exchange_URL");

            References(x => x.Type, "Information_Exchange_Type_ID");
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
        }
    }
}