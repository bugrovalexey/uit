﻿<%@ Register Src="GridColumnManager.ascx" TagName="GridColumnManager" TagPrefix="gcm" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatusChangeControl.ascx.cs" Inherits="GB.MKRF.Web.Common.StatusChangeControl" %>
<script type="text/javascript">
    function moveStatus() {
        var newStatusName = StatusComboBox.GetText();
        var msg = "Перевести заявку в статус '" + newStatusName + "'?";
        if (ASPxClientEdit.ValidateGroup("statGroup") && confirm(msg)) {
            LoadingPanel.Show();

            //if(SignaturePanel.GetVisible())
            //{
            //    <%=GB.Controls.DigitalSignature.GetClientSidePerformSignMethod()%>;
            //}   
            //else
            //{
                callBack.PerformCallback('');
            //}            
        }
    }
    
    function RefreshOpener() {
        window.parent.PopupClose();        
    }
</script>
<style>
    /*

    #ErrorText span
    {
        color: orange;
    }
     td.form_edit_input table.dxeButtonEdit,
    td.form_edit_input table.dxeTextBox,
    td.form_edit_input table.dxeMemo,
    td.form_edit_input table.dxucControl,
    td.form_edit_input table.dxeButtonEdit_Aqua,
    td.form_edit_input table.dxeTextBox_Aqua,
    td.form_edit_input table.dxeMemo_Aqua,
    td.form_edit_input table.dxucControl_Aqua,
    td.form_edit_input table.dxeListBox_Aqua
    {
        width: 290px;
    }
        */

    div.sopanel {
        border: 1px solid #A8D5E0;
        background-color: white;
        width: 470px;
        padding: 10px 15px;
        margin-bottom: 20px;
        background-color: #E7FCE5;
    }

    div.sopanel span {
        display: block;
        padding-bottom: 10px;
    }

    div.spanel {
        border: 1px solid #A8D5E0;
        background-color: white;
        padding: 10px 15px;
        width: 470px;
    }
</style>
<dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" OnCallback="callBack_Callback">
    <ClientSideEvents CallbackError="function(s,e) { LoadingPanel.Hide(); alert('В процессе перевода статуса произошла ошибка. Пожалуйста, обратитесь к администратору.'); e.handled = true; }" />
</dxc:ASPxCallback>
<dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
    Modal="True">
</dxlp:ASPxLoadingPanel>

<table class="form_edit">
    <% if (allowChange)
       { %>
    <tr>
        <td class="form_edit_desc">Перевести в статус:
        </td>
        <td>
            <dxe:ASPxComboBox runat="server" ID="StatusComboBox" Width="500px" ValueType="System.Int32"
                ValueField="Key" TextField="Value.Name" ClientInstanceName="StatusComboBox"
                ValidationSettings-ValidationGroup="statGroup">
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                </ValidationSettings>
                <ClientSideEvents SelectedIndexChanged="function(s,e){ activateButton(s); }" />
            </dxe:ASPxComboBox>
            <script type="text/javascript">
                var arr = [];
                <%=errorCodes%>
                function activateButton(s) {
                    var msg = arr[s.GetValue()];
                    if (msg && msg=='ok') {
                        document.getElementById('ErrorText').innerHTML = '';
                        ChangeStatusButton.SetEnabled(true);
                        CommentMemo.SetEnabled(true);
                    }
                    else {
                        if (msg) {
                            document.getElementById('ErrorText').innerHTML = msg;
                        }
                        ChangeStatusButton.SetEnabled(false);
                        CommentMemo.SetEnabled(false);
                    }

                    //cpPreview.PerformCallback(s.GetValue());
                }
            </script>

        </td>
    </tr>
    <tr runat="server" id="CommentRow">
        <td class="form_edit_desc">Комментарий:
        </td>
        <td>
            <dxe:ASPxMemo runat="server" ID="CommentMemo" Rows="7" Width="500px" ClientInstanceName="CommentMemo"
                ClientEnabled="false">
            </dxe:ASPxMemo>
        </td>
    </tr>

   <%-- <tr>
        <td class="form_edit_desc">Электронная подпись:
        </td>
        <td>
            <dxe:ASPxCheckBox runat="server" Text="Добавить ЭП к переводу статуса" ID="ApplySignatureCheckBox">
                <ClientSideEvents CheckedChanged="function(s,e){ ToggleSignaturePanel(s.GetChecked()); }" />
            </dxe:ASPxCheckBox>
            <dxp:ASPxPanel runat="server" ID="SignaturePanel" ClientInstanceName="SignaturePanel"
                ClientVisible="false">
                <PanelCollection>
                    <dxp:PanelContent>
                        <dxcp:ASPxCallbackPanel runat="server" ID="cpPreview" ClientInstanceName="cpPreview"
                            OnCallback="cpPreview_Callback" ShowLoadingPanel="false">
                            <PanelCollection>
                                <dxhe:HtmlEditorRoundPanelContent>
                                    <%#signPreview%>
                                </dxhe:HtmlEditorRoundPanelContent>
                            </PanelCollection>
                        </dxcp:ASPxCallbackPanel>

                        <div class="spanel">
                            <p style="margin: 0">
                                Воспользуйтесь данным блоком, если ваша электронно-цифровая подпись записана на
                                eToken (токен) или на сменном носителе (USB-флеш-накопитель, дискета 3,5" и т.п.).
                                <br />
                                В большинстве случаев требуется вводить PIN-код.
                            </p>
                            <p>
                                <b>Для работы со сменным носителем вам необходимо дополнительно установить <a href="<%=pluginUrl%>">плагин</a> к браузеру</b>
                            </p>
                            <cc:DigitalSignature ID="dsc" runat="server" Width="100%" OnGetSource="dsc_GetSource"
                                OnReturnSignedData="dsc_ReturnSignedData" ClientSideErrorEvent="function(message){ LoadingPanel.Hide(); alert(message); }"
                                FillCertificatesOnLoad="true" />
                        </div>
                    </dxp:PanelContent>
                </PanelCollection>
            </dxp:ASPxPanel>
        </td>
    </tr>--%>
    <tr>
        <td class="form_edit_desc"></td>
        <td>
            <dxe:ASPxButton runat="server" ID="ChangeStatusButton" Text="Перевести" AutoPostBack="false"
                ValidationGroup="statGroup" ClientEnabled="false" ClientInstanceName="ChangeStatusButton">
                <ClientSideEvents Click="function(s,e){moveStatus();}" />
            </dxe:ASPxButton>

        </td>
    </tr>

    <%} %>

    <tr>
        <td class="form_edit_desc"></td>
        <td>
            <%=warningMessage %>
            <div id="ErrorText" style="font-size: 1em; line-height: 18px; padding-top: 20px;"></div>

            <script type="text/javascript">activateButton(StatusComboBox);</script>
        </td>
    </tr>
</table>

<p class="header_grid">
    История переводов статусов
</p>
<%--<gcm:GridColumnManager ID="_GridColumnManager" runat="server" GridName="statusGrid" />--%>
<dxwgv:ASPxGridView runat="server" ID="statusGrid" KeyFieldName="Id" Width="100%"
    AutoGenerateColumns="False" ClientInstanceName="statusGrid" OnBeforePerformDataSelect="statusGrid_BeforePerformDataSelect">
    <Columns>
        <dxwgv:GridViewDataDateColumn Caption="Дата-время" FieldName="ChangeDateTime" VisibleIndex="0"
            SortIndex="0" SortOrder="Descending">
            <PropertiesDateEdit DisplayFormatString="">
            </PropertiesDateEdit>
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataTextColumn Caption="Автор" FieldName="UserFullName" VisibleIndex="1"
            Name="author" ShowInCustomizationForm="False" Visible="false">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Из статуса" FieldName="BeginStatusName" VisibleIndex="2">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="В статус" FieldName="EndStatusName" VisibleIndex="3">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataMemoColumn Caption="Комментарий" FieldName="Comm" VisibleIndex="4" />
    </Columns>
    <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
    <Styles>
        <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
        </Header>
    </Styles>
    <SettingsPager PageSize="20">
    </SettingsPager>
</dxwgv:ASPxGridView>
