﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.ExternalSystems.Zakupki360.Dto
{
    class RequestGetOrders
    {
        public SearchParametersOrders Filter { get; set; }

        public string ApiKey { get; set; }
    }
}
