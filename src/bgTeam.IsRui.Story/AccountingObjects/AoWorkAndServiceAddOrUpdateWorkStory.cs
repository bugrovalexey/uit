﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoWorkAndServiceAddOrUpdateWorkStory : IStory<AoWorkAndServiceAddOrUpdateWorkStoryContext, WorkAndServiceAddOrUpdateWorkDto>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public AoWorkAndServiceAddOrUpdateWorkStory(ISessionNHibernateFactory factory, IRepositoryRui repository, IMapperBase mapper)
        {
            _factory = factory;
            _repository = repository;
            _mapper = mapper;
        }

        public WorkAndServiceAddOrUpdateWorkDto Execute(AoWorkAndServiceAddOrUpdateWorkStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<WorkAndServiceAddOrUpdateWorkDto> ExecuteAsync(AoWorkAndServiceAddOrUpdateWorkStoryContext context)
        {
            WorkAndService workAndService;

            using (var session = _factory.OpenSession())
            {
                try
                {
                    CategoryKindOfSupport categoryKindOfSupport = _repository.Get<CategoryKindOfSupport>(context.CategoryKindOfSupportId);

                    OKVED okved = _repository.Get<OKVED>(context.OkvedId);

                    if (context.Id.HasValue)
                    {
                        workAndService = _repository.Get<WorkAndService>(context.Id);

                        _mapper.Map(context, workAndService);

                        workAndService.CategoryKindOfSupport = categoryKindOfSupport;

                        workAndService.Okved = okved;

                        await session.UpdateAsync(workAndService);
                    }
                    else
                    {
                        workAndService = new WorkAndService();

                        AccountingObject ao = _repository.Get<AccountingObject>(context.AoId);

                        _mapper.Map(context, workAndService);

                        workAndService.AccountingObject = ao;

                        workAndService.CategoryKindOfSupport = categoryKindOfSupport;

                        workAndService.Okved = okved;

                        await session.InsertAsync(workAndService);
                    }
                }
                catch (Exception)
                {
                    await session.RollbackAsync();

                    throw;
                }
            }

                return _mapper.Map(workAndService, new WorkAndServiceAddOrUpdateWorkDto());
        }
    }
}
