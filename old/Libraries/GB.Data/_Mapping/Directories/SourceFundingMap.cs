﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class SourceFundingMap : ClassMapBase<SourceFunding>
    {
        public SourceFundingMap()
            : base("Source_Funding")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "Source_Funding_Code");
            Map(x => x.Name, "Source_Funding_Name");
        }
    }
}