﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Monitoring/Monitoring.master" AutoEventWireup="true" CodeBehind="DepartmentStatFull.aspx.cs" Inherits="GB.MKRF.Web.Monitoring.v2012new.DepartmentStatFull" %>

<asp:Content ID="Content1" ContentPlaceHolderID="buttons" runat="server">

    <script type="text/javascript">

        function OnValueChanged(s, e) {
            if (!CallbackPanel.InCallback())
                CallbackPanel.PerformCallback();
        }

    </script>


            <table style="width: 100%">
                <tr>
                    <td style="width: 1%; white-space: nowrap">Департамент:</td>
                    <td style="width: 1%">
                        <dxe:ASPxComboBox runat="server" ID="DepComboBox" Width="400px" ValueType="System.Int32"
                            ValueField="Id" TextField="Name" ClientInstanceName="DepComboBox" OnDataBinding="DepComboBox_DataBinding">
                            <ClientSideEvents ValueChanged="OnValueChanged" />
                        </dxe:ASPxComboBox>
                    </td>
                    <td style="width: 98%"></td>
                </tr>
            </table>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="center" runat="server">
    <div style="font-family: tahoma; font-size: 14pt; text-align: center; width: 100%;">Статистика по департаментам</div>
    <dxcp:ASPxCallbackPanel runat="server" ID="CallbackPanel" ClientInstanceName="CallbackPanel" OnCallback="CallbackPanel_Callback">
        <PanelCollection>
            <dxp:PanelContent>
                <p class="header_grid">Общие сведения по планам информатизации</p>
                <dxwgv:ASPxGridView runat="server" ID="Grid1" Width="100%" OnDataBinding="Grid1_DataBinding" KeyFieldName="ID">
                    <Settings ShowFooter="true" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Год" FieldName="Year" Width="100px" CellStyle-HorizontalAlign="Center" />
                        <dxwgv:GridViewBandColumn Caption="Текущий год">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Основной объем" FieldName="Y0" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                                <dxwgv:GridViewDataTextColumn Caption="Доп. потребность" FieldName="AddY0" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                        <dxwgv:GridViewBandColumn Caption="Плановый (+1 год)">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Основной объем" FieldName="Y1" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                                <dxwgv:GridViewDataTextColumn Caption="Доп. потребность" FieldName="AddY1" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                        <dxwgv:GridViewBandColumn Caption="Плановый (+2 год)">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Основной объем" FieldName="Y2" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                                <dxwgv:GridViewDataTextColumn Caption="Доп. потребность" FieldName="AddY2" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                    </Columns>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="Итого:" FieldName="Year" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y0" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="AddY0" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y1" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="AddY1" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y2" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="AddY2" SummaryType="Sum" />
                    </TotalSummary>
                </dxwgv:ASPxGridView>

                <p class="header_grid">Расходы по типам мероприятий</p>
                <dxwgv:ASPxGridView runat="server" ID="Grid2" Width="100%" OnDataBinding="Grid2_DataBinding" KeyFieldName="ID">
                    <Settings ShowFooter="true" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Год" FieldName="Year" Width="100px" CellStyle-HorizontalAlign="Center" />
                        <dxwgv:GridViewBandColumn Caption="Не задано">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Кол-во мероприятий" FieldName="NeZadano" />
                                <dxwgv:GridViewDataTextColumn Caption="Расходы на текущий год" FieldName="Y0NZ" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                        <dxwgv:GridViewBandColumn Caption="Создание">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Кол-во мероприятий" FieldName="Sozdanie" />
                                <dxwgv:GridViewDataTextColumn Caption="Расходы на текущий год" FieldName="Y0S" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                        <dxwgv:GridViewBandColumn Caption="Развитие">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Кол-во мероприятий" FieldName="Razvitie" />
                                <dxwgv:GridViewDataTextColumn Caption="Расходы на текущий год" FieldName="Y0R" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                        <dxwgv:GridViewBandColumn Caption="Модернизация">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Кол-во мероприятий" FieldName="Modernizacia" />
                                <dxwgv:GridViewDataTextColumn Caption="Расходы на текущий год" FieldName="Y0M" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                        <dxwgv:GridViewBandColumn Caption="Эксплуатация">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Кол-во мероприятий" FieldName="Expluatacia" />
                                <dxwgv:GridViewDataTextColumn Caption="Расходы на текущий год" FieldName="Y0E" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                    </Columns>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="Итого:" FieldName="Year" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="NeZadano" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y0NZ" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="Sozdanie" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y0S" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="Razvitie" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y0R" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="Modernizacia" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y0M" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="Expluatacia" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y0E" SummaryType="Sum" />

                    </TotalSummary>
                </dxwgv:ASPxGridView>

                <p class="header_grid">Расходы по приоритетным/не приоритетным мероприятиям</p>
                <dxwgv:ASPxGridView runat="server" ID="Grid3" Width="100%" OnDataBinding="Grid3_DataBinding" KeyFieldName="ID">
                    <Settings ShowFooter="true" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Год" FieldName="Year" Width="100px" CellStyle-HorizontalAlign="Center" />
                        <dxwgv:GridViewBandColumn Caption="Приоритетные">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Кол-во мероприятий" FieldName="PriorCount" />
                                <dxwgv:GridViewDataTextColumn Caption="Расходы на текущий год" FieldName="Y0P" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                        <dxwgv:GridViewBandColumn Caption="Неприоритетные">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Кол-во мероприятий" FieldName="NoPriorCount" />
                                <dxwgv:GridViewDataTextColumn Caption="Расходы на текущий год" FieldName="Y0NP" PropertiesTextEdit-DisplayFormatString="{0:#,##0.0000}" />
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                    </Columns>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="Итого:" FieldName="Year" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="PriorCount" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y0P" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="NoPriorCount" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y0NP" SummaryType="Sum" />
                    </TotalSummary>
                </dxwgv:ASPxGridView>
            </dxp:PanelContent>
        </PanelCollection>
        </dxcp:ASPxCallbackPanel>
</asp:Content>
