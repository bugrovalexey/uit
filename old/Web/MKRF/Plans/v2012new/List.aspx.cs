﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Report;
using GB.MKRF.Repository;
using GB.MKRF.Validation;
using ICSharpCode.SharpZipLib.GZip;
using GB.MKRF.Helpers;
using GB.MKRF.Web.Helpers;
//using Core.Repository;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class List : System.Web.UI.Page
    {
        PlanControllerOld planController = new PlanControllerOld();

        //bool isNewRow = false;

        //const int canPublishMask = 4;
        //const int activeDepartmentMask = 8;

        //bool isMKS
        //{
        //    get
        //    {
        //        User user = UserRepository.GetCurrent();
        //        if (RoleEnum.MKS.IsEq(user.Role))
        //            return true;
        //        else return false;
        //    }

        //}

        private DataTable table = null;
        private DataTable GetPlanList
        {
            get
            {
                if (table == null)
                {
                    User user = UserRepository.GetCurrent();

                    if(RoleEnum.CA.IsEq(user.Role))
                        table = planController.GetList(false, 1);
                    else
                        table = planController.GetList(true, 1);
                }

                return table;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridColumnManager.Grid = plansGrid;
        }

        protected override void OnPreRender(EventArgs e)
        {
            User user = UserRepository.GetCurrent();
            addButton.Visible = Validation.Validator.Validate(
                new ExpDemand() { ExpObject = new Plan(null) { PlanType = ExpObjectTypeEnum.Plan2012new } },
                ObjActionEnum.create);

            plansGrid.DataBind();
            base.OnPreRender(e);
        }

        //private DataTable DataTableCustomFilter(DataTable dt, string filterExpression)
        //{
        //    var rows = dt.Select(filterExpression);
        //    if (rows.Any())
        //    {
        //        return rows.CopyToDataTable();
        //    }
        //    else
        //    {
        //        dt.Rows.Clear();
        //        return dt;
        //    }
        //}

        protected void plansGrid_Init(object sender, EventArgs e)
        {
            ASPxGridView gridView = (ASPxGridView)sender;
            foreach (GridViewColumn column in gridView.Columns)
            {
                var c = column as GridViewDataColumn;

                if (c != null)
                {
                    c.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                    c.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.False;
                    c.Settings.ShowFilterRowMenu = DevExpress.Utils.DefaultBoolean.False;

                    //switch (c.FieldName)
                    //{
                    //    case "StatusChangeDate":
                    //        c.Settings.ShowFilterRowMenu = DevExpress.Utils.DefaultBoolean.True;
                    //        break;
                    //    case "Status":
                    //        c.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;
                    //        break;
                    //    case "Year":
                    //        c.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;
                    //        break;
                    //        //case "ExpStatus":
                    //        //    c.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;
                    //        //break;
                    //    default:
                    //        c.Settings.ShowFilterRowMenu = DevExpress.Utils.DefaultBoolean.False;
                    //        break;
                    //}
                }
            }
        }

        protected void plansGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            DataTable table = null;

            //Включаем группировку для МКС и Подведомственных
            //var filterMode = hfFilterMode.Contains("filterMode") ? hfFilterMode["filterMode"].ToString() : string.Empty;
            if (!IsCallback)
            {
                plansGrid.ClearSort();
                if (UserRepository.IsMks)
                {
                    plansGrid.GroupBy(plansGrid.Columns["FieldForGrouping"]);

                    //Если подведомтсвенные, то добавляем группировку по категориям
                    //if (filterMode == "subordinate")
                    //    plansGrid.GroupBy(plansGrid.Columns["GovtOrganVedName"]);

                    //                    plansGrid.ClientSideEvents.RowClick =
                    //                        @"function(s, e) {
                    //if (plansGrid.IsGroupRowExpanded(e.visibleIndex))
                    //    plansGrid.CollapseRow(e.visibleIndex);
                    //else
                    //    plansGrid.ExpandRow(e.visibleIndex);
                    //}";
                }
            }

            if (hfCache.Contains("t") && hfCache["t"] != null)
            {
                object result = null;
                var b64 = Convert.FromBase64String((string)hfCache["t"]);
                using (var memory = new MemoryStream(b64))
                {
                    GZipInputStream gzipIn = new GZipInputStream(memory);
                    result = new BinaryFormatter().Deserialize(gzipIn);

                }
                table = (DataTable)result;
            }
            else
            {
                table = GetPlanList;

                //if (ChBExpired.Checked)
                //{
                //    table = DataTableCustomFilter(table, string.Format("DecisionDate <'{0:o}'", DateTime.Today));
                //}

                //switch (filterMode)
                //{
                //    case "departments":
                //        table = DataTableCustomFilter(table, string.Format("Govt_Organ_Type_ID ={0}", (int)DepartmentTypeEnum.Department));
                //        break;
                //    case "subordinate":
                //        table = DataTableCustomFilter(table, string.Format("Govt_Organ_Type_ID ={0} AND Govt_Organ_Ved_ID > 0", (int)DepartmentTypeEnum.Establishment));
                //        break;
                //    case "region":
                //        table = DataTableCustomFilter(table, string.Format("Govt_Organ_Type_ID ={0}", (int)DepartmentTypeEnum.Region));
                //        break;
                //    default:
                //        break;
                //}

                table.RemotingFormat = SerializationFormat.Binary;

                byte[] bytes = null;
                using (var memory = new MemoryStream())
                {
                    GZipOutputStream gzipOut = new GZipOutputStream(memory);
                    gzipOut.IsStreamOwner = false;

                    new BinaryFormatter().Serialize(gzipOut, table);

                    gzipOut.Close();

                    memory.Flush();
                    bytes = memory.ToArray();
                }

                hfCache["t"] = Convert.ToBase64String(bytes);
            }

            plansGrid.DataSource = table;
        }



        protected void plansGrid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            planController.Create(new PlanControllerArgs()
            {
                PlanTypeId = ExpObjectTypeEnum.Plan2012new,
                YearValue = hf["Year"],
                OwnerUserId = hf["User"],
                CopyPlanId = hf["Copy"],
            });

            table = null;
            hfCache.Clear();
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void plansGrid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            planController.SaveOrUpdate(new PlanControllerArgs()
            {
                Id = e.Keys[0],
                YearValue = hf["Year"],
                OwnerUserId = hf["User"]
            });

            table = null;
            hfCache.Clear();

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void plansGrid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            planController.Delete(new PlanControllerArgs()
            {
                Id = e.Keys[0]
            });

            table = null;
            hfCache.Clear();
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        //protected void plansGrid_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        //{
        //    isNewRow = true;
        //}

        protected void plansGrid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            planController.SetCurrentDemand(new PlanControllerArgs() { Id = e.EditingKeyValue });
        }

        protected void UserComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = (ASPxComboBox)sender;

            var usersData = planController.GetUsers();
            box.DataSource = usersData.Users;

            if (planController.CurrentEdit == null)
                return;

            box.Value = usersData.SelectedUser.Id;
        }

        protected void DepartmentTextBox_DataBinding(object sender, EventArgs e)
        {
            //if (planController.CurrentEdit == null)
            //    return;

            ASPxLabel box = (ASPxLabel)sender;

            box.Text = UserRepository.GetCurrent().Department.Name;//planController.GetDepartmentName();
        }

        protected void YearComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = (ASPxComboBox)sender;

            var years = planController.GetYears();
            box.DataSource = years.YearList;

            if (planController.CurrentEdit == null)
                return;

            box.Value = years.SelectedValue;
        }

        protected void PlansComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = (ASPxComboBox)sender;

            var demands = planController.GetSourceDemands();

            box.DataSource = demands.DemandList;
            box.Value = demands.SelectedValue;
        }

        protected void plansGrid_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;

            int? composite = (int?)grid.GetRowValues(e.VisibleIndex, "Composite");

            if (e.ButtonType == ColumnCommandButtonType.Edit)
            {
                e.Visible = (composite.HasValue && (composite.Value & (int)CompositeEnum.Edit) != 0);
            }

            //Всегда показываем кнопку удалить
#if !DEBUG 
            else if (e.ButtonType == ColumnCommandButtonType.Delete)
            {
                e.Visible = (composite.HasValue && (composite.Value & (int)CompositeEnum.Delete) != 0);
            }
#endif
        }

        protected void plansGrid_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;

            int? composite = (int?)grid.GetRowValues(e.VisibleIndex, "Composite");

            if (e.CellType == GridViewTableCommandCellType.Data && e.ButtonID == "copyButton")
            {
                e.Visible = (DevExpress.Utils.DefaultBoolean)Convert.ToInt16(!(composite.HasValue && (composite.Value & (int)CompositeEnum.Edit) != 0));
            }
        }

        protected void plansGrid_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;

            switch (e.ButtonID)
            {
                case "copyButton":
                    hfCache.Clear();

                    planController.Copy(new PlanControllerArgs()
                    {
                        Id = grid.GetRowValues(e.VisibleIndex, "Id")
                    });

                    grid.DataBind();
                    break;
            }
        }

        protected void plansGrid_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
        {
            if (IsCallback)
            {
                e.Properties["cpCache"] = hfCache["t"];
            }
        }

        protected string getViewLinks(int Id)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append(ReportPage.GetLink("P 1.1", ReportType.Plan2012NewSectionOne_1, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 1.2", ReportType.Plan2012NewSectionOne_2, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 1.3", ReportType.Plan2012NewSectionOne_3, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 1.4", ReportType.Plan2012NewSectionOne_4, Id)); sb.Append(" ");

            sb.Append("<br />");

            sb.Append(ReportPage.GetLink("P 2.1", ReportType.Plan2012NewSectionTwo_1, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 2.2", ReportType.Plan2012NewSectionTwo_2, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 2.3", ReportType.Plan2012NewSectionTwo_3, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 2.4", ReportType.Plan2012NewSectionTwo_4, Id)); sb.Append(" ");

            sb.Append("<br />");

            sb.Append(ReportPage.GetLink("P 3.1", ReportType.Plan2012NewSectionThree_1, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 3.2", ReportType.Plan2012NewSectionThree_2, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 3.3", ReportType.Plan2012NewSectionThree_3, Id)); sb.Append(" ");

            sb.Append("<br />");

            sb.Append(ReportPage.GetLink("P 4.1 P 4.2", ReportType.Plan2012NewSectionFour, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 4.3", ReportType.Plan2012NewSectionFour_3, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 4.4", ReportType.Plan2012NewSectionFour_4, Id)); sb.Append(" ");

            return sb.ToString();
        }


        //protected bool isReadOnlyPublishCheckBox(int Index)
        //{
        //    if (Index < 0)
        //        return false;

        //    int? Composite = (int?)plansGrid.GetRowValues(Index, "Composite");

        //    return (Composite.HasValue && (Composite.Value & canPublishMask) != 0);
        //}


        //protected void plansGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        //{
        //    if (e.RowType != GridViewRowType.Data)
        //        return;

        //    int id = (int)plansGrid.GetRowValues(e.VisibleIndex, "Id");

        //    ASPxCheckBox cb = plansGrid.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataCheckColumn)plansGrid.Columns["IsPublic"], "cb") as ASPxCheckBox;

        //    if (cb != null)
        //    {
        //        cb.Checked = (bool)plansGrid.GetRowValues(e.VisibleIndex, "IsPublic");
        //        cb.Enabled = isReadOnlyPublishCheckBox(e.VisibleIndex);

        //        if (cb.Enabled)
        //        {
        //            cb.ClientSideEvents.CheckedChanged = "function(s, e) { togglePublish(s, '" + id + "'); }";
        //        }
        //    }
        //}

        //private static System.Drawing.Color ColorRowDepartment = System.Drawing.Color.FromArgb(180, 180, 180);

        //protected void plansGrid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        //{
        //    if (e.RowType == GridViewRowType.Data)
        //    {
        //        int? Composite = (int?)plansGrid.GetRowValues(e.VisibleIndex, "Composite");

        //        if (Composite.HasValue && (Composite.Value & activeDepartmentMask) == 0)
        //        {
        //            e.Row.BackColor = ColorRowDepartment;
        //        }
        //    }
        //}

        //protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        //{
        //    try
        //    {
        //        string[] items = e.Parameter.Split('#');

        //        bool val = (items[0] == "1");
        //        int id = int.Parse(items[1]);

        //        ExpDemand demand = DemandRepository<ExpDemand>.Get(id);
        //        if (!Validator.Validator.Validate(demand.ExpObject, ObjActionEnum.CanPublish))
        //            throw new Exception("Публикация запрещена");

        //        //                demand.ExpObject.IsPublic = val;

        //        RepositoryBase<Plan>.SaveOrUpdate(demand.ExpObject);
        //    }
        //    catch
        //    {
        //        throw new Exception("При публикации плана произошла ошибка");
        //    }
        //}

        protected void ExportReportControl_BeforeDataBind(Common.ExportReportControl sender, Common.ExportDataEventArgs e)
        {
            for (int i = 0; i < plansGrid.VisibleRowCount; i++)
            {
                if (plansGrid.Selection.IsRowSelected(i))
                {
                    e.Data.Add(new object[] { plansGrid.GetRowValues(i, "PlanId") });
                }
            }

            //if (e.Data.Count == 0)
            //    SetErrorText("Не выбрано ни одной записи.");
        }

        //protected void ExportReportControl_AddExportReportType(Common.ExportReportControl sender, ListEditItemCollection item)
        //{
        //    //item.Add("В обычном виде", ReportTypeExport.Plan2012NewSectionFull);
        //}

        protected void ExportButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions(); options.SheetName = "Планы информатизации (версия 2012г.)";
            options.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value; GridViewExporter.WriteXlsToResponse("Планы информатизации", options);
        }

        //ASPxComboBox plansComboBox;

        //protected void PlansComboBox_Init(object sender, EventArgs e)
        //{
        //    plansComboBox = sender as ASPxComboBox;
        //}

        protected string GetNameLink(object id, object name, object isnew)
        {
            return string.Format("<a class='gridLink' {2} href='{0}'>{1}</a>",
                PlanCard.GetUrl((int)id, ExpObjectTypeEnum.Plan2012new),
                name.ToString(),
                isnew.ToString().Equals("False", StringComparison.InvariantCultureIgnoreCase) ? " style='font-weight: bold' " : string.Empty);
        }
    }
}