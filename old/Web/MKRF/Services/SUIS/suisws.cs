﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18052
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GB.MKRF.Web.Services.SUIS
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://ws.suisws.iteco.ru", ConfigurationName="suisws")]
    public interface suisws
    {
    
        // CODEGEN: Generating message contract since the operation GetAsset is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="GetAsset", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        GetAssetResponse GetAsset(GetAssetRequest request);
    
        // CODEGEN: Generating message contract since the operation GetModel is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="GetModel", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        GetModelResponse GetModel(GetModelRequest request);
    
        // CODEGEN: Generating message contract since the operation GetPortfolio is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="GetPortfolio", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        GetPortfolioResponse GetPortfolio(GetPortfolioRequest request);
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
    public partial class HeaderType
    {
    
        private string nodeIdField;
    
        private string messageIdField;
    
        private System.DateTime timeStampField;
    
        private MessageClassType messageClassField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string NodeId
        {
            get
            {
                return this.nodeIdField;
            }
            set
            {
                this.nodeIdField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string MessageId
        {
            get
            {
                return this.messageIdField;
            }
            set
            {
                this.messageIdField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public System.DateTime TimeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public MessageClassType MessageClass
        {
            get
            {
                return this.messageClassField;
            }
            set
            {
                this.messageClassField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
    public enum MessageClassType
    {
    
        /// <remarks/>
        REQUEST,
    
        /// <remarks/>
        RESPONSE,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://output.suisws.iteco.ru")]
    public partial class PortfolioReturnMsgType
    {
    
        private MessageType messageField;
    
        private MessageDataType messageDataField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=0)]
        public MessageType Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=1)]
        public MessageDataType MessageData
        {
            get
            {
                return this.messageDataField;
            }
            set
            {
                this.messageDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
    public partial class MessageType
    {
    
        private orgExternalType senderField;
    
        private orgExternalType recipientField;
    
        private orgExternalType originatorField;
    
        private TypeCodeType typeCodeField;
    
        private StatusType statusField;
    
        private System.DateTime dateField;
    
        private string exchangeTypeField;
    
        private string requestIdRefField;
    
        private string originRequestIdRefField;
    
        private string serviceCodeField;
    
        private string caseNumberField;
    
        private string testMsgField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public orgExternalType Sender
        {
            get
            {
                return this.senderField;
            }
            set
            {
                this.senderField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public orgExternalType Recipient
        {
            get
            {
                return this.recipientField;
            }
            set
            {
                this.recipientField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public orgExternalType Originator
        {
            get
            {
                return this.originatorField;
            }
            set
            {
                this.originatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public TypeCodeType TypeCode
        {
            get
            {
                return this.typeCodeField;
            }
            set
            {
                this.typeCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public StatusType Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public System.DateTime Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string ExchangeType
        {
            get
            {
                return this.exchangeTypeField;
            }
            set
            {
                this.exchangeTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=7)]
        public string RequestIdRef
        {
            get
            {
                return this.requestIdRefField;
            }
            set
            {
                this.requestIdRefField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=8)]
        public string OriginRequestIdRef
        {
            get
            {
                return this.originRequestIdRefField;
            }
            set
            {
                this.originRequestIdRefField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=9)]
        public string ServiceCode
        {
            get
            {
                return this.serviceCodeField;
            }
            set
            {
                this.serviceCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=10)]
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=11)]
        public string TestMsg
        {
            get
            {
                return this.testMsgField;
            }
            set
            {
                this.testMsgField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
    public partial class orgExternalType
    {
    
        private string codeField;
    
        private string nameField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
    public enum TypeCodeType
    {
    
        /// <remarks/>
        GSRV,
    
        /// <remarks/>
        GFNC,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
    public enum StatusType
    {
    
        /// <remarks/>
        REQUEST,
    
        /// <remarks/>
        RESULT,
    
        /// <remarks/>
        REJECT,
    
        /// <remarks/>
        INVALID,
    
        /// <remarks/>
        ACCEPT,
    
        /// <remarks/>
        PING,
    
        /// <remarks/>
        PROCESS,
    
        /// <remarks/>
        NOTIFY,
    
        /// <remarks/>
        FAILURE,
    
        /// <remarks/>
        CANCEL,
    
        /// <remarks/>
        STATE,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
    public partial class MessageDataType
    {
    
        private AppDataType appDataField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public AppDataType AppData
        {
            get
            {
                return this.appDataField;
            }
            set
            {
                this.appDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
    public partial class AppDataType
    {
    
        private string xMLDataField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://output.suisws.iteco.ru", Order=0)]
        public string XMLData
        {
            get
            {
                return this.xMLDataField;
            }
            set
            {
                this.xMLDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://input.suisws.iteco.ru")]
    public partial class GetPortfolioMsgType
    {
    
        private MessageType messageField;
    
        private MessageDataType messageDataField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=0)]
        public MessageType Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=1)]
        public MessageDataType MessageData
        {
            get
            {
                return this.messageDataField;
            }
            set
            {
                this.messageDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://output.suisws.iteco.ru")]
    public partial class ModelReturnMsgType
    {
    
        private MessageType messageField;
    
        private MessageDataType messageDataField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=0)]
        public MessageType Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=1)]
        public MessageDataType MessageData
        {
            get
            {
                return this.messageDataField;
            }
            set
            {
                this.messageDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://input.suisws.iteco.ru")]
    public partial class GetModelMsgType
    {
    
        private MessageType messageField;
    
        private MessageDataType messageDataField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=0)]
        public MessageType Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=1)]
        public MessageDataType MessageData
        {
            get
            {
                return this.messageDataField;
            }
            set
            {
                this.messageDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://output.suisws.iteco.ru")]
    public partial class AssetReturnMsgType
    {
    
        private MessageType messageField;
    
        private MessageDataType messageDataField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=0)]
        public MessageType Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=1)]
        public MessageDataType MessageData
        {
            get
            {
                return this.messageDataField;
            }
            set
            {
                this.messageDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://input.suisws.iteco.ru")]
    public partial class GetAssetMsgType
    {
    
        private MessageType messageField;
    
        private MessageDataType messageDataField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=0)]
        public MessageType Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://smev.gosuslugi.ru/rev111111", Order=1)]
        public MessageDataType MessageData
        {
            get
            {
                return this.messageDataField;
            }
            set
            {
                this.messageDataField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetAssetRequest
    {
    
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
        public HeaderType Header;
    
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://input.suisws.iteco.ru", Order=0)]
        public GetAssetMsgType GetAsset;
    
        public GetAssetRequest()
        {
        }
    
        public GetAssetRequest(HeaderType Header, GetAssetMsgType GetAsset)
        {
            this.Header = Header;
            this.GetAsset = GetAsset;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetAssetResponse
    {
    
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
        public HeaderType Header;
    
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://output.suisws.iteco.ru", Order=0)]
        public AssetReturnMsgType AssetReturn;
    
        public GetAssetResponse()
        {
        }
    
        public GetAssetResponse(HeaderType Header, AssetReturnMsgType AssetReturn)
        {
            this.Header = Header;
            this.AssetReturn = AssetReturn;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetModelRequest
    {
    
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
        public HeaderType Header;
    
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://input.suisws.iteco.ru", Order=0)]
        public GetModelMsgType GetModel;
    
        public GetModelRequest()
        {
        }
    
        public GetModelRequest(HeaderType Header, GetModelMsgType GetModel)
        {
            this.Header = Header;
            this.GetModel = GetModel;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetModelResponse
    {
    
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
        public HeaderType Header;
    
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://output.suisws.iteco.ru", Order=0)]
        public ModelReturnMsgType ModelReturn;
    
        public GetModelResponse()
        {
        }
    
        public GetModelResponse(HeaderType Header, ModelReturnMsgType ModelReturn)
        {
            this.Header = Header;
            this.ModelReturn = ModelReturn;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetPortfolioRequest
    {
    
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
        public HeaderType Header;
    
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://input.suisws.iteco.ru", Order=0)]
        public GetPortfolioMsgType GetPortfolio;
    
        public GetPortfolioRequest()
        {
        }
    
        public GetPortfolioRequest(HeaderType Header, GetPortfolioMsgType GetPortfolio)
        {
            this.Header = Header;
            this.GetPortfolio = GetPortfolio;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetPortfolioResponse
    {
    
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://smev.gosuslugi.ru/rev111111")]
        public HeaderType Header;
    
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://output.suisws.iteco.ru", Order=0)]
        public PortfolioReturnMsgType PortfolioReturn;
    
        public GetPortfolioResponse()
        {
        }
    
        public GetPortfolioResponse(HeaderType Header, PortfolioReturnMsgType PortfolioReturn)
        {
            this.Header = Header;
            this.PortfolioReturn = PortfolioReturn;
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface suiswsChannel : suisws, System.ServiceModel.IClientChannel
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class suiswsClient : System.ServiceModel.ClientBase<suisws>, suisws
    {
    
        public suiswsClient()
        {
        }
    
        public suiswsClient(string endpointConfigurationName) : 
            base(endpointConfigurationName)
        {
        }
    
        public suiswsClient(string endpointConfigurationName, string remoteAddress) : 
            base(endpointConfigurationName, remoteAddress)
        {
        }
    
        public suiswsClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(endpointConfigurationName, remoteAddress)
        {
        }
    
        public suiswsClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(binding, remoteAddress)
        {
        }
    
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        GetAssetResponse suisws.GetAsset(GetAssetRequest request)
        {
            return base.Channel.GetAsset(request);
        }
    
        public AssetReturnMsgType GetAsset(ref HeaderType Header, GetAssetMsgType GetAsset1)
        {
            GetAssetRequest inValue = new GetAssetRequest();
            inValue.Header = Header;
            inValue.GetAsset = GetAsset1;
            GetAssetResponse retVal = ((suisws)(this)).GetAsset(inValue);
            Header = retVal.Header;
            return retVal.AssetReturn;
        }
    
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        GetModelResponse suisws.GetModel(GetModelRequest request)
        {
            return base.Channel.GetModel(request);
        }
    
        public ModelReturnMsgType GetModel(ref HeaderType Header, GetModelMsgType GetModel1)
        {
            GetModelRequest inValue = new GetModelRequest();
            inValue.Header = Header;
            inValue.GetModel = GetModel1;
            GetModelResponse retVal = ((suisws)(this)).GetModel(inValue);
            Header = retVal.Header;
            return retVal.ModelReturn;
        }
    
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        GetPortfolioResponse suisws.GetPortfolio(GetPortfolioRequest request)
        {
            return base.Channel.GetPortfolio(request);
        }
    
        public PortfolioReturnMsgType GetPortfolio(ref HeaderType Header, GetPortfolioMsgType GetPortfolio1)
        {
            GetPortfolioRequest inValue = new GetPortfolioRequest();
            inValue.Header = Header;
            inValue.GetPortfolio = GetPortfolio1;
            GetPortfolioResponse retVal = ((suisws)(this)).GetPortfolio(inValue);
            Header = retVal.Header;
            return retVal.PortfolioReturn;
        }
    }
}