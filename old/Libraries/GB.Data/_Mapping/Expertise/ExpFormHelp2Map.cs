﻿using Uviri.MKRF.Entities.Expertise;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpFormHelp2Map : ClassMapBase<ExpFormHelp2>
    {
        public ExpFormHelp2Map()
            : base("Exp_Concl_Sprv_2")
        {
//            Table("w_");

//            Id(x => x.Id, "Exp_Concl_Sprv_2_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@Exp_Concl_Sprv_2_Inf_Prov = ? 
//						  ,@Exp_Concl_Sprv_2_Comm = ?
//                          ,@Plans_Activity_ID = ?
//                          ,@Exp_Concl_Sprv_ID = ?
//						  ,@Exp_Concl_Sprv_2_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Exp_Concl_Sprv_2_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Concl_Sprv_2_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Exp_Concl_Sprv_2_Delete @Exp_Concl_Sprv_2_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.ProvisionInfrastructure, "Exp_Concl_Sprv_2_Inf_Prov");
            Map(x => x.Comment, "Exp_Concl_Sprv_2_Comm");

            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID").Not.Nullable();
            References<ExpFormHelp>(x => x.FormHelp, "Exp_Concl_Sprv_ID").Not.Nullable();
        }
    }
}
