﻿namespace bgTeam.IsRui.Web
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using bgTeam.IsRui.WebApp;
    using bgTeam.IsRui.WebApp.Common;
    using Microsoft.AspNetCore.Antiforgery;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Swashbuckle.AspNetCore.Swagger;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // установка конфигурации подключения
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    //options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/api/account/index");

                    // Выкидвывть 401 ошибку вместо редиректа
                    options.Events.OnRedirectToLogin = context =>
                    {
                        context.Response.StatusCode = 401;
                        return Task.CompletedTask;
                    };
                });

            services.AddMvc();

            // KITVLG-245
            //services.AddMvc(options =>
            //    options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute()));

            services.AddSwaggerGen(x =>
            {
                var basePath = AppContext.BaseDirectory;
                x.SwaggerDoc("v1", new Info { Title = "IsRui API", Version = "v1" });
                x.OperationFilter<FileUploadOperation>();
                x.IncludeXmlComments(Path.Combine(basePath, "bgTeam.IsRui.WebApp.xml"));
                x.IncludeXmlComments(Path.Combine(basePath, "bgTeam.IsRui.WebApp.Admin.xml"));
                x.IncludeXmlComments(Path.Combine(basePath, "bgTeam.IsRui.Story.xml"));
            });

            DIContainer.Configure(services);
            WebApp.Admin.DIContainer.Configure(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IAntiforgery antiforgery)
        {
            if (env.IsDevelopment() || env.IsEnvironment("Debug") || env.IsEnvironment("Release"))
            {
                app.UseSwaggerUI(x =>
                {
                    x.SwaggerEndpoint("/swagger/v1/swagger.json", "IsRui API V1");
                });

                app.UseDeveloperExceptionPage();
                //app.UseBrowserLink();
                //app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/home/error");
            }

            app.UseSwagger();

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMiddleware<MiddlewareException>();

            // задача KITVLG-245
            //app.UseMiddleware<XcsrfProtectionMiddleware>();

            app.UseMvc(x =>
            {
                x.MapRoute(name: "default", template: "api/{controller=Home}/{action=Index}");
                x.MapRoute(name: "admin", template: "admin/{controller=AdminDic}/{action=Index}");
            });

            // раскомментить когда займёмся задачей KITVLG-245
            //app.Use(next => context =>
            //{
            //    string path = context.Request.Path.Value;

            //    if (
            //        string.Equals(path, "/", StringComparison.OrdinalIgnoreCase) ||
            //        string.Equals(path, "/index.html", StringComparison.OrdinalIgnoreCase))
            //    {
            //        // The request token can be sent as a JavaScript-readable cookie, 
            //        // and Angular uses it by default.
            //        var tokens = antiforgery.GetAndStoreTokens(context);
            //        context.Response.Cookies.Append("XSRF-TOKEN", tokens.RequestToken,
            //            new CookieOptions() { HttpOnly = false });
            //    }

            //    return next(context);
            //});

            //app.UseCors("AllowAllOrigin");

            //app.UseMvc(x =>
            //{
            //    x.MapRoute(name: "default", template: "{controller=Home}/{action=Index}");
            //});
        }
    }
}
