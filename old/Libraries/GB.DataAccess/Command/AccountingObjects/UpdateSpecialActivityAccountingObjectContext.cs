﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateSpecialActivityAccountingObjectContext : ICommandContext
    {
        public bool InformAboutActivityGovOrgan { get; set; }

        public int xId { get; set; }
    }
}
