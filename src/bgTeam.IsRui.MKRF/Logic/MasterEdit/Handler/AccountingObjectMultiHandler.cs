﻿namespace bgTeam.IsRui.MKRF.Logic.MasterEdit.Handler
{
    class AccountingObjectMultiHandler : MasterEditBaseHandler
    {
        protected override MasterInItem[] GetListPage()
        {
            return new MasterInItem[] 
            {
                new MasterInItem(MasterItemEnum.None),
                new MasterInItem(MasterItemEnum.GoalsAndPurpose),
                new MasterInItem(MasterItemEnum.KindMasterEdit),
                new MasterInItem(MasterItemEnum.CardForRead)
            };
        }
    }
}
