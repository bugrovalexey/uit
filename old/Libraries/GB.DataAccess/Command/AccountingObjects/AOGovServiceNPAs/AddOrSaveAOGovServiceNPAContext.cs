﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServiceNPAs
{
    public class AddOrSaveAOGovServiceNPAContext : ICommandContext
    {
        public int? xId { get; set; }

        public int AOGovServiceId { get; set; }

        public string Name { get; set; }

        public string DocumentNumber { get; set; }

        public DateTime? Date { get; set; }

        public string ParticleOfDocument { get; set; }
    }
}
