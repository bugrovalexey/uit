﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.WorkAndGood
{
    public class AddOrSaveActToWorkAndGoodsContext : ICommandContext
    {
        public int? ActsOfAcceptanceId { get; set; }

        public int? NewId { get; set; }

        public int Id { get; set; }
    }
}
