﻿using bgTeam.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class ResponsibleServiceViewModel : StepBaseViewModel
    {
        #region consts

        private const string DepartmentCaption = "Учреждение, уполномоченное на создание, развитие ОУ";
        private const string ResponsibleCoordCaption = "Ответственный за координацию мероприятий по информатизации";
        private const string ResponsibleInformationCaption = "Ответственный за размещение сведений об объекте учета";

        #endregion


        [DisplayName(DepartmentCaption)]
        public int DepartmentId { get; set; }

        public SelectList DepartmentList { get; set; }

        [DisplayName(ResponsibleCoordCaption)]
        public IList<IEntityName> ResponsibleCoord { get; private set; }

        [DisplayName(ResponsibleInformationCaption)]
        public IList<IEntityName> ResponsibleInformation { get; private set; }


        public ResponsibleServiceViewModel(IUserRepository userRepository, int id)
            : base(id, MasterItemEnum.ResponsibleService)
        {
            DepartmentId = Current.ResponsibleDepartment.Return(d => d.Id, userRepository.GetCurrent().Department.Return(dep => dep.Id, default(int)));

            DepartmentList = new SelectList(new RepositoryDc<Department>().GetAllEx(q => q.OrderBy(x => x.Code)), "Id", "Name");

            ResponsibleCoord =
                Current.ResponsibleCoord.Return(
                    x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.Name } }, default(List<IEntityName>));

            ResponsibleInformation =
                Current.ResponsibleInformation.Return(
                    x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.Name } }, default(List<IEntityName>));
        }
    }
}