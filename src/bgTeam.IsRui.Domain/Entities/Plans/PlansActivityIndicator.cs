﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    public class PlansActivityIndicator : BaseIndicator
    {
        /// <summary>
        /// Алгоритм расчета
        /// </summary>
        public virtual string AlgorithmOfCalculating { get; set; }

        /// <summary>
        /// Обоснование
        /// </summary>
        public virtual string Justification { get; set; }
    }
}