﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.PlanReport.SectionV;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionV
{
    public class ISGetAccessMap : ClassMapBase<ISGetAccess>
	{
        public ISGetAccessMap()
            : base("PRS_5b")
		{
//            Table("w_");

//            Id(x => x.Id, "PRS_5b_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string args = @" @PRS_5b_Num = ? 
//							,@PRS_5b_IS = ? 
//							,@PRS_5b_Access = ? 
//							,@PRS_5b_Comm = ? 
//							,@Plans_ID = ? 
//							,@Govt_Organ_ID = ? 
//							,@FGIS_Info_ID = ? 
//							,@NPA_ID = ? 
//							,@PRS_5b_ID = ? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_5b_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_5b_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_5b_Delete
//						  @PRS_5b_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_5b_Num");
			Map(x => x.IS, "PRS_5b_IS");
			Map(x => x.Access, "PRS_5b_Access");
			Map(x => x.Comment, "PRS_5b_Comm");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
			References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
			References<FgisInfo>(x => x.FgisInfo, "FGIS_Info_ID");
			References<NPA>(x => x.NPA, "NPA_ID");
        }
    }
}
