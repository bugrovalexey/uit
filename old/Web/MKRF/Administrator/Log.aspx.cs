﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GB.MKRF.Web.Selectors
{
    public partial class Log : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var file = Request.QueryString["file"];
            if (!string.IsNullOrEmpty(file))
            {
                var appSetting = Server.MapPath("~/Logs");
                var fileName = Path.Combine(appSetting, file);
                if (File.Exists(fileName))
                {
                    var text = System.IO.File.ReadAllText(fileName);
                    Response.Write(text.Replace(Environment.NewLine, "<br />"));
                }
            }
        }
    }
}