﻿namespace bgTeam.IsRui.Domain.Dto
{
    public class KindDto
    {
        public int AoId { get; set; }

        public int KindId { get; set; }
    }
}
