﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
    public class SoftwareTypeMap : ClassMapBase<SoftwareType>
    {
        public SoftwareTypeMap()
            : base("PRS_8_7_Soft_Type")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_7_Soft_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_8_7_Soft_Type_Name = ? 
//						  ,@PRS_8_7_Soft_Type_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_7_Soft_Type_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_7_Soft_Type_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_7_Soft_Type_Delete @PRS_8_7_Soft_Type_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "PRS_8_7_Soft_Type_Name");
        }
    }
}