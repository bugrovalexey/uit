﻿using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    [MetadataType(typeof(SpecCheckEditViewModel_Valid))]
    public class SpecCheckEditViewModel : ViewModelBase
    {
        #region ctor

        public SpecCheckEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;

            FilesData = new List<SelectEntityInfo>();

            var entity = id.HasValue
                ? new RepositoryDc<SpecialCheck>().Get(
                    q => q.Fetch(x => x.Documents).Eager
                        .Where(x => x.Id == id))
                : null;

            entity.Do(Fill);
        }

        #endregion ctor

        #region methods

        protected void Fill(SpecialCheck entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            FilesData = entity.Documents.Return(docs => docs.Select(x => new SelectEntityInfo() { Id = x.Id, Name = x.Name }), default(IEnumerable<SelectEntityInfo>));
        }

        #endregion methods

        #region Props

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        [DisplayName(TableSpecCheck.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableSpecCheck.FilesDataCaption)]
        public IEnumerable<IEntityName> FilesData { get; set; }

        #endregion Props

    }


    public class SpecCheckEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }
    }

}