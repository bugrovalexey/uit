﻿namespace bgTeam.IsRui.Common
{
    using System;

    /// <summary>
    /// Нет доступа к объекту
    /// </summary>
    [Serializable]
    public class ObjectAccessException : Exception
    {
    }
}