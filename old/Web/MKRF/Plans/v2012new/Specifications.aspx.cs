﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GB;
using GB.Helpers;
using DevExpress.Utils;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.Data;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Helpers;
using GB.MKRF.Repository;
using GB.MKRF.Web.Helpers;
using DevExpress.Data;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class Specifications : PlansActivityEditPage
    {
        protected PlansSpecController controller = new PlansSpecController();

        public override bool EnableEdit
        {
            get
            {
                return base.EnableEdit ||
                        Validation.Validator.Validate(new PlansSpec{ PlanActivity = CurrentActivity }, ObjActionEnum.create);
            }
        }
        
        public static string GetUrl(int ActId)
        {
            return UrlHelper.Resolve("~/Plans/v2012new/Specifications.aspx") + "?" + ActivityCard.ActivityIdKey + "=" + ActId;
        }

        protected override void OnInit(EventArgs e)
        {
            dc.Owner = this.CurrentActivity;

            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            addSpecTypeButton.Visible = EnableEdit;

            treeList.Columns["cmdColumn"].Visible = EnableEdit;
            treeList.Columns["updownColumn"].Visible = EnableEdit;
            treeList.Columns["copyColumn"].Visible = EnableEdit;

            base.OnPreRender(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            treeList.DataBind();
        }

        protected void memo_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            ASPxMemo memo = (ASPxMemo)sender;

            switch (memo.ID)
            {
                case "EquipmentTypeMemo":
                    memo.Text = controller.CurrentEdit.EquipmentType;
                    break;
                case "NameMemo":
                    memo.Text = controller.CurrentEdit.Name;
                    break;
                case "AnalogMemo":
                    memo.Text = controller.CurrentEdit.Analog;
                    break;
                
                default:
                    throw new ArgumentException(memo.ID.ToString());
            }

        }

        protected void QualifierComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = (ASPxComboBox)sender;

            var list = controller.GetQualifierList();

            box.DataSource = list.List;
            if (controller.CurrentEdit != null)
            {
                box.Value = list.Selected;
            }
            else
            {
                box.SelectedIndex = -1;
            }

        }

        protected void OKDPTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null) 
                return;

            ASPxTextBox tb = sender as ASPxTextBox;
            tb.Value = String.IsNullOrWhiteSpace(controller.CurrentEdit.OKDPText) ? "" : controller.CurrentEdit.OKDPText;
        }

        protected void ExpenseDirectionComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;
            if (box == null) return;

            var expenseDirections = controller.GetExpenseDirections(CurrentActivity);

            box.DataSource = expenseDirections.List;
            box.Value = expenseDirections.Selected;
        }

        protected void STType_Combo_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = controller.GetExpenseTypeItems();

            if (controller.CurrentEdit != null && controller.CurrentEdit.SpecExpenseType.Count > 0)
            {
                box.Value = controller.CurrentEdit.SpecExpenseType[0].ExpenseType.Id;
            }
            else
            {
                box.Value = EntityBase.Default;
            }
        }

        protected void STType_Combo_Callback(object sender, CallbackEventArgsBase e)
        {
            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = controller.GetExpenseTypeItems(e.Parameter);
            box.DataBind();
        }

        protected void OKDPComboBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            var data = controller.GetOKDPList();

            var s = sender as ASPxComboBox;

            s.DataSource = data.List;
            s.Value = data.Selected;
        }

        protected void Quantity_SpinEdit_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null) return;

            var spin = sender as ASPxSpinEdit;
            spin.Value = controller.CurrentEdit.Quantity;
        }

        protected void spinEdit_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            ASPxSpinEdit spin = (ASPxSpinEdit)sender;

            switch (spin.ID)
            {
                case "CostSpinEdit":
                    spin.Value = controller.CurrentEdit.Cost;
                    break;
                case "ExpenseVolumeCurrentYearSpinEdit":
                    spin.Value = controller.CurrentEdit.ExpenseVolumeCurrentYear;
                    break;
                case "ExpenseVolumeFirstYearSpinEdit":
                    spin.Value = controller.CurrentEdit.ExpenseVolumeFirstYear;
                    break;
                case "ExpenseVolumeSecondYearSpinEdit":
                    spin.Value = controller.CurrentEdit.ExpenseVolumeSecondYear;
                    break;
                case "AnalogPriceSpinEdit":
                    spin.Value = controller.CurrentEdit.AnalogPrice;
                    break;
                default:
                    throw new ArgumentException(spin.ID.ToString());
            }

        }

        public string GetTdStyle()
        {
            //if (RoleExtension.IsEq(RoleEnum.Expert, UserRepository.GetCurrent().Role))
            //{
            //    return "style=\"display:none\"";
            //}
            return string.Empty;
        }

        #region treeList
        protected void treeList_DataBinding(object sender, EventArgs e)
        {
            treeList.DataSource = controller.GetSpecList(CurrentActivity);
        }

        protected void treeList_NodeUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            controller.Update(new PlansSpecControllerArgs
                {
                    PlansSpecId = e.Keys[0],
                    Values = hfSpec.ToDictionary(x => x.Key, x => x.Value)
                });

            RefreshActivity();
            treeList.TreeCancelEdit(e);
        }

        protected void treeList_NodeInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            controller.Create(new PlansSpecControllerArgs
                {
                    ParentActivity = CurrentActivity,
                    Values = hfSpec.ToDictionary(x => x.Key, x => x.Value)
                });

            RefreshActivity();
            treeList.TreeCancelEdit(e);
        }

        protected void treeList_StartNodeEditing(object sender, TreeListNodeEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.NodeKey);
        }

        protected void treeList_CommandColumnButtonInitialize(object sender, TreeListCommandColumnButtonEventArgs e)
        {
            ASPxTreeList tl = sender as ASPxTreeList;
            TreeListNode node = tl.FindNodeByKeyValue(e.NodeKey);
            if (node.Level > 1 && e.ButtonType != TreeListCommandColumnButtonType.Delete)
                e.Visible = DefaultBoolean.False;
        }

        protected void treeList_NodeDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            TreeListNode currentNd = ((ASPxTreeList)sender).FindNodeByKeyValue(e.Keys[0].ToString());

            if (EnableEdit)
            {
                if (currentNd.Level == 1)
                {
                    controller.Delete((int)e.Keys[0]);
                }
                else if (currentNd.Level == 2)
                {
                    controller.DeleteSpecType((int)e.Keys[0]);
                }
            }

            RefreshActivity();
            NumHelper<PlansSpec>.OrderNums(CurrentActivity.Specifications);
            treeList.DataBind();
        }


        protected void treeList_NodeDeleted(object sender, ASPxDataDeletedEventArgs e)
        {
            e.ExceptionHandled = true;
        }

        protected void treeList_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            var parameters = e.Argument.Split(new []{";"}, StringSplitOptions.RemoveEmptyEntries);
            if (parameters.Length != 2)
                throw new ArgumentException("Некорректные данные. Пожалуйста, напишите в техподдержку.");

            switch (parameters[1])
            {
                case "TreeListDownButton":
                case "TreeListUpButton":
                    controller.ChangeOrder(new PlansSpecControllerArgs
                        {
                            ParentActivity = CurrentActivity,
                            PlansSpecId = parameters[0]
                        }, parameters[1] == "TreeListUpButton");
                    break;

                case "TreeListCloneButton":
                    controller.CopySpec(parameters[0]);
                    break;
            }

            treeList.DataBind();
            RefreshActivity();
        }        
        #endregion

        protected void treeList_CustomSummaryCalculate(object sender, TreeListCustomSummaryEventArgs e)
        {
            switch (e.SummaryProcess)
            {
                case CustomSummaryProcess.Start:
                    e.Value = (decimal)0;
                    break;
                case CustomSummaryProcess.Calculate:
                    if (e.Node.Level == 1)
                        e.Value = (decimal)e.Value + (decimal)e.Node["Cost"];
                    break;
            }
        }

        protected void ExpenditureItemsMemo_DataBinding(object sender, EventArgs e)
        {
            var memo = sender as ASPxTextBox;
            if (memo == null)
                return;
            
            if (controller.CurrentEdit == null || controller.CurrentEdit.ExpenditureItem == null)
            {
                memo.Text = "- Не задано";
            }
            else
            {
                memo.Text = controller.CurrentEdit.ExpenditureItem.Code + " " + controller.CurrentEdit.ExpenditureItem.Name;
            }
        }

        protected void ExpenditureItemsComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = DirectoriesRepositary.GetDirectories<ExpenditureItem>(DateTime.Now.Year);

            if (controller.CurrentEdit == null || controller.CurrentEdit.ExpenditureItem == null)
            {
                box.Value = GB.EntityBase.Default;
            }
            else
            {
                box.Value = controller.CurrentEdit.ExpenditureItem.Id;
            }
        }
    }
}