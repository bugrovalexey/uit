﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using GB.Helpers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.MKRF.Web.Common;
using GB.Repository;

namespace GB.MKRF.Web.Plans
{
    public partial class PlanHtmlVersion : System.Web.UI.Page
    {
        protected int CurrentId { get; set; }

        private Plan _currentPlan;
        protected Plan CurrentPlan
        {
            get
            {
                return _currentPlan ??
                       (_currentPlan = RepositoryBase<Plan>.Get(CurrentId));
            }
        }

        public const string PlanIdKey = "Id";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.Request.QueryString[PlanIdKey] != null)
            {
                CurrentId = Convert.ToInt32(Page.Request.QueryString[PlanIdKey]);
            }
        }

        protected string GetResponsibleInfo()
        {
            if (CurrentPlan.User != null)
            {
                var infolist = CurrentPlan.User.Surname;
                if (!string.IsNullOrWhiteSpace(CurrentPlan.User.Name))
                    infolist += " " + CurrentPlan.User.Name;
                if (!string.IsNullOrWhiteSpace(CurrentPlan.User.Middlename))
                    infolist += " " + CurrentPlan.User.Middlename;
                if (!string.IsNullOrWhiteSpace(CurrentPlan.User.Job))
                    infolist += ", " + CurrentPlan.User.Job;
                if (!string.IsNullOrWhiteSpace(CurrentPlan.User.WorkPhone))
                    infolist += ", " + CurrentPlan.User.WorkPhone;
                if (!string.IsNullOrWhiteSpace(CurrentPlan.User.MobPhone))
                    infolist += ", " + CurrentPlan.User.MobPhone;
                if (!string.IsNullOrWhiteSpace(CurrentPlan.User.Fax))
                    infolist += ", " + CurrentPlan.User.Fax;
                if (!string.IsNullOrWhiteSpace(CurrentPlan.User.Email))
                    infolist += ", " + CurrentPlan.User.Email;

                return infolist;
            }
            return string.Empty;
        }

        protected DataTable GetActivityList()
        {
            return CurrentPlan.PlanType != ExpObjectTypeEnum.SummaryPlanMKS2012new
                       ? PlanRepository.GetActivityList2012new(CurrentPlan, UserRepository.GetCurrent().Id)
                       : PlanRepository.GetDepActivityList2012new(CurrentPlan);
        }


        struct moneyAndIndex
        {
            public decimal money;
            public int index;
        }

        protected DataTable GetPlanSummary()
        {
            DataTable table = PlanRepository.GetActivityList2012new(CurrentPlan, UserRepository.GetCurrent().Id);
            DataTable tableSummary = null;

            const int PlansSpecAssgnVolY0 = 0;
            const int PlansSpecAssgnVolY1 = 1;
            const int PlansSpecAssgnVolY2 = 2;
            const int PlansWorkAssgnVolY0 = 3;
            const int PlansWorkAssgnVolY1 = 4;
            const int PlansWorkAssgnVolY2 = 5;

            moneyAndIndex[] money = new moneyAndIndex[6];

            if (table != null)
            {
                money[PlansSpecAssgnVolY0].index = table.Columns.IndexOf("PlansSpecCost");
                money[PlansSpecAssgnVolY1].index = table.Columns.IndexOf("PlansSpecAssgnVolY1");
                money[PlansSpecAssgnVolY2].index = table.Columns.IndexOf("PlansSpecAssgnVolY2");
                money[PlansWorkAssgnVolY0].index = table.Columns.IndexOf("PlansWorkAssgnVolY0");
                money[PlansWorkAssgnVolY1].index = table.Columns.IndexOf("PlansWorkAssgnVolY1");
                money[PlansWorkAssgnVolY2].index = table.Columns.IndexOf("PlansWorkAssgnVolY2");
            }

            foreach (DataRow row in table.Rows)
            {
                for (int i = 0; i < money.Length; i++)
                {
                    money[i].money += (decimal)row[money[i].index];
                }
            }

            tableSummary = new DataTable();
            tableSummary.Columns.Add("Category", typeof(string));
            tableSummary.Columns.Add("ExpenseVolume0", typeof(decimal));
            tableSummary.Columns.Add("ExpenseVolume1", typeof(decimal));
            tableSummary.Columns.Add("ExpenseVolume2", typeof(decimal));
            tableSummary.Columns.Add("Summary", typeof(double));

            tableSummary.Rows.Add("Итого по работам:",
                                    money[PlansWorkAssgnVolY0].money,
                                    money[PlansWorkAssgnVolY1].money,
                                    money[PlansWorkAssgnVolY2].money,
                                    money[PlansWorkAssgnVolY0].money + money[PlansWorkAssgnVolY1].money + money[PlansWorkAssgnVolY2].money);

            tableSummary.Rows.Add("Итого по спецификациям:",
                                    money[PlansSpecAssgnVolY0].money,
                                    money[PlansSpecAssgnVolY1].money,
                                    money[PlansSpecAssgnVolY2].money,
                                    money[PlansSpecAssgnVolY0].money + money[PlansSpecAssgnVolY1].money + money[PlansSpecAssgnVolY2].money);
            return tableSummary;
        }

        //protected string GetExpense242()
        //{
        //    Limit242Expenses type242Expense = RepositoryBase<Limit242Expenses>.Get(x => x.Department.Id == CurrentPlan.Department.Id);
        //    var result = string.Empty;

        //    if (type242Expense != null &&
        //        ((CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012new) || (CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new))
        //        && CurrentPlan.Year.Year < 2016 && CurrentPlan.Year.Year > 2012)
        //    {
        //        double sumY0 = 0;
        //        double sumY1 = 0;
        //        double sumY2 = 0;
        //        foreach (PlansActivity act in CurrentPlan.Activities)
        //        {
        //            sumY0 += act.PlansActivityVolY0;
        //            sumY1 += act.PlansActivityVolY1;
        //            sumY2 += act.PlansActivityVolY2;
        //        }

        //        result = "Лимит финансирования по 242 виду расходов согласно бюджету ";

        //        if (CurrentPlan.Year.Year == 2013)
        //        {
        //            result += String.Format("<b>на 2013 год:</b> <label style='{1}'>{0}</label> ", type242Expense.Y2013, (sumY0 > type242Expense.Y2013) ? "color:red" : "color:green");
        //            result += String.Format("<b>на 2014 год:</b> <label style='{1}'>{0}</label> ", type242Expense.Y2014, (sumY1 > type242Expense.Y2014) ? "color:red" : "color:green");
        //            result += String.Format("<b>на 2015 год:</b> <label style='{1}'>{0}</label> ", type242Expense.Y2015, (sumY2 > type242Expense.Y2015) ? "color:red" : "color:green");
        //        }
        //        if (CurrentPlan.Year.Year == 2014)
        //        {
        //            result += String.Format("<b>на 2014 год:</b> <label style='{1}'>{0}</label> ", type242Expense.Y2014, (sumY0 > type242Expense.Y2013) ? "color:red" : "color:green");
        //            result += String.Format("<b>на 2015 год:</b> <label style='{1}'>{0}</label> ", type242Expense.Y2015, (sumY1 > type242Expense.Y2014) ? "color:red" : "color:green");
        //        }
        //        if (CurrentPlan.Year.Year == 2015)
        //        {
        //            result += String.Format("<b>на 2015 год:</b> <label style='{1}'>{0}</label> ", type242Expense.Y2015, (sumY0 > type242Expense.Y2013) ? "color:red" : "color:green");
        //        }
        //    }

        //    return result;
        //}

        protected override void Render(HtmlTextWriter writer)
        {
            StringBuilder sbOut = new StringBuilder();
            StringWriter swOut = new StringWriter(sbOut);
            HtmlTextWriter htwOut = new HtmlTextWriter(swOut);
            base.Render(htwOut);
            string sOut = sbOut.ToString();

//            Encoding encoding = Encoding.GetEncoding(1251);
            UTF8Encoding encoding = new UTF8Encoding();
            var filename = FileHelper.SaveInTemp(encoding.GetBytes(sOut));

            FileHelper.SendFile(HttpContext.Current, filename, string.Format("{0}.html", CurrentPlan.Name));
            writer.Write(sbOut);
        }

        //protected void GetHtmlVersion(object sender, EventArgs e)
        //{
        //    WebClient myClient = new WebClient();
        //    string myPageHTML = null;
        //    byte[] requestHTML;
        //    // Gets the url of the page
        //    string currentPageUrl = Request.Url.ToString();

        //    UTF8Encoding utf8 = new UTF8Encoding();

        //    requestHTML = myClient.DownloadData(currentPageUrl);

        //    myPageHTML = utf8.GetString(requestHTML);

        //    Response.Write(myPageHTML);
        //}

        protected object GetActivityReadMode(int id)
        {
            var control = (ActivityReadMode)Page.LoadControl("~/Common/ActivityReadMode.ascx");
            control.CurrentId = id.ToString();
            control.EnableEdit = false;
            StringBuilder sb = new StringBuilder();
            StringWriter tw = new StringWriter(sb);
            HtmlTextWriter hw = new HtmlTextWriter(tw);

            control.RenderControl(hw);
            return sb.ToString();
        }
    }
}