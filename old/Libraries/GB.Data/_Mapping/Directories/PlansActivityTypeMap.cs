﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class PlansActivityTypeMap : ClassMapBase<ActivityType>
	{
        public PlansActivityTypeMap()
            : base("Plans_Activity_Type")
		{
//            Table("w_");

//            Id(x => x.Id, "Plans_Activity_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            SqlInsert(@"exec ZZZ_Prc_Plans_Activity_Type_Insert
//						  @Plans_Activity_Type_Code = ?
//						 ,@Plans_Activity_Type_Name = ?
//						 ,@Plans_Activity_Type_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Plans_Activity_Type_Update
//						  @Plans_Activity_Type_Code = ?
//						 ,@Plans_Activity_Type_Name = ?
//						 ,@Plans_Activity_Type_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Plans_Activity_Type_Delete
//						  @Plans_Activity_Type_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Code, "Plans_Activity_Type_Code").Nullable();
			Map(x => x.Name, "Plans_Activity_Type_Name").Nullable();
        }
    }
}
