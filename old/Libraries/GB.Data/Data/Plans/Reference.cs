﻿using System.Collections.Generic;
using Uviri.MKRF.Entities.Documents;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Справка по комплексу взаимосвязанных мероприятий по информатизации
    /// </summary>
    public class Reference : EntityBase
    {
        public Reference(ExpDemand Demand)
        {
            this.Demand = Demand;
            Documents = new List<Document>();
            Influences = new List<Influence>();
            ActivityList = new List<PlansActivity>();
        }

        protected Reference()
        {
        }

        /// <summary>
        /// Заявка на план
        /// </summary>
        public virtual ExpDemand Demand { get; protected set; }

        /// <summary>
        /// Код проекта
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Наименование проекта
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Цели проекта
        /// </summary>
        public virtual string Purpose { get; set; }
        
        /// <summary>Документы-основания</summary>
        public virtual IList<Document> Documents { get; protected set; }

        /// <summary>Влияние мероприятия на индикаторы</summary>
        public virtual IList<Influence> Influences { get; protected set; }

        public virtual IList<PlansActivity> ActivityList { get; protected set; }

        public virtual string Docs { get; set; }

    }
}
