﻿using System.ComponentModel;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    public enum AccountingObjectsStageEnum
    {
        //[Description("Не задано")]
        //None = 0,

        /// <summary>
        /// Подготовка к созданию
        /// </summary>
        [Description("Подготовка к созданию")]
        Planned = 1,
        
        [Description("Создание")]
        Creation = 2,

        [Description("Эксплуатация")]
        Exploitation = 3,

        //Более не используется
        //[Description("Развитие")]
        //Evolution = 4,
        
        [Description("Выведен из эксплуатации")]
        OutExploitation = 4
    }
}
