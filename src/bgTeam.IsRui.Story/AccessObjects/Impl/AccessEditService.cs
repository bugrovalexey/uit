﻿namespace bgTeam.IsRui.Story.AccessObjects.Impl
{
    using bgTeam.DataAccess;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AccessEditService : IAccessEditService
    {
        private readonly IAppLogger _appLogger;
        private readonly IRepositoryRui _repository;
        private readonly ICacheManager _cacheManager;
        private readonly IUserRepository _userRepository;

        public AccessEditService(
            IAppLogger appLogger,
            IRepositoryRui repository,
            ICacheManager cacheManager,
            IUserRepository userRepository)
        {
            _appLogger = appLogger;
            _repository = repository;
            _cacheManager = cacheManager;
            _userRepository = userRepository;
        }

        public void CheckAccessEdit<TWorkflowEntity>(TWorkflowEntity entity)
            where TWorkflowEntity : class, IEntityWorkflow, IEntity
        {
            CheckAccessEditAsync(entity).Wait();
        }

        public async Task CheckAccessEditAsync<TWorkflowEntity>(TWorkflowEntity entity)
            where TWorkflowEntity : class, IEntityWorkflow, IEntity
        {
            if (entity.Workflow == null)
            {
                return;
            }

            var user = _userRepository.GetCurrent();
            var permissions = await GetAccessPermissions(user);

            var perm = permissions
                .Where(x => (int)x.EntityType == entity.Workflow.EntityType && x.Status.Id == entity.Workflow.Status.Id);

            if (perm.Any(x => (x.AccessMask & AccessActionEnum.Edit) != AccessActionEnum.Edit))
            {
                throw new IsRuiEditException();
            }
        }

        private async Task<IEnumerable<AccessToEntity>> GetAccessPermissions(User user)
        {
            var result = new List<AccessToEntity>();

            foreach (var role in user.Roles)
            {
                result.AddRange(await GetAccessPermissions(role));
            }

            return result;
        }

        private async Task<IEnumerable<AccessToEntity>> GetAccessPermissions(Role role)
        {
            if (_cacheManager.TryGetValue<int, IEnumerable<AccessToEntity>>(role.Id, out IEnumerable<AccessToEntity> accesses))
            {
                return accesses;
            }

            var result = _repository.GetAllEx<AccessToEntity>(q => q.Where(x => x.Role.Id == role.Id));

            if (!_cacheManager.TrySetValue<int, IEnumerable<AccessToEntity>>(role.Id, result))
            {
                _appLogger.Error($"Не удалось добавить данные прав доступа на редактирование для роли: {role.Id}");
            }

            return result;
        }
    }
}
