﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActivityReadMode.ascx.cs" Inherits="GB.MKRF.Web.Common.ActivityReadMode" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="GB.Helpers" %>
<%@ Import Namespace="GB.MKRF.Entities.Directories" %>
<%@ Import Namespace="GB.MKRF.Entities.Documents" %>
<%@ Import Namespace="GB.MKRF.Entities.Plans" %>
<%@ Import Namespace="GB.MKRF.Repository" %>
<%@ Import Namespace="GB.MKRF.Web.Plans.v2012new" %>
<%@ Import Namespace="GB.Repository" %>

<style>
    .item_title
    {
        /*background: #F3F3F3;*/
        padding: 5px 10px;
        cursor: pointer;
        font-size: 1.3em;
        position: relative;
        /*font-family: 'Cuprum','Helvetica';*/
        /*text-transform: uppercase;*/
        border-bottom: 1px solid #C0C0C0;
        min-height: 22px;
    }

        .item_title span
        {
            position: absolute;
            top: 6px;
        }

    .item
    {
        /*padding-left: 12px;
        padding-top: 10px;
        padding: 10px 5px 5px 10px;
        
        border-color: #C0C0C0;
        border-image: none;
        border-style: solid;
        border-width: 1px;
        border-top: none;*/
        margin-bottom: 50px;
    }


    /*.item_title.expanded:before
    {
        content: url('/content/style/all/img/minus_16.png');
        margin-right: 8px;
        width: 12px;
        height: 12px;
        position: relative;
        top: 2px;
    }

    .item_title.collapsed:before
    {
        content: url('/content/style/all/img/plus_16.png');
        margin-right: 8px;
        width: 12px;
        height: 12px;
        position: relative;
        top: 2px;
    }*/

    .commondataitem
    {
        display: inline-block;
        vertical-align: top;
    }

    .dataitem
    {
        padding-bottom: 5px;
    }

    .headers_fieldnames
    {
        color: #999;
        /*font-family: Cuprum;*/
        font-size: 1.1em;
        margin-right: 10px;
        margin-top: 5px;
        padding-bottom: 2px;
        vertical-align: top;
    }

    .headers_values
    {
        font: 1.25em Arial,Helvetica,sans-serif;
    }

    table.readmode_table
    {
        font: 11px/24px Verdana, Arial, Helvetica, sans-serif;
        border-collapse: collapse;
    }

    .readmode_table thead
    {
        background: #ededed;
        font-size: 1em;
        font-weight: bold;
        text-align: Center;
        vertical-align: Middle;
        white-space: normal;
    }

        .readmode_table thead tr td
        {
            padding: 5px;
        }

    .readmode_table td
    {
        border: 1px solid #c0c0c0;
        vertical-align: middle;
        padding: 3px 6px 4px;
        line-height: 14px;
    }

        .readmode_table td.center_cell
        {
            text-align: center;
        }

        .readmode_table td.emptyrow
        {
            text-align: center;
            vertical-align: middle;
            padding-top: 15px;
            padding-bottom: 15px;
        }

    .tables_name
    {
        color: #808080;
        font-family: Cuprum;
        font-size: 1.15em;
    }

    .values_list
    {
        margin: 0px;
    }

    .values_list_item
    {
        margin-bottom: 5px;
        padding: 5px 10px 5px;
        position: relative;
    }

        .values_list_item:nth-child(2n+1)
        {
            background-color: #EDEDED;
        }

        .listitem div.list2
        {
            margin-top: 5px;
        }

            .listitem div.list2:before
            {
                content: '—';
                margin-right: 5px;
            }

    .listitem
    {
        padding: 10px;
    }

        .listitem:nth-child(2n+1)
        {
            background-color: #EDEDED;
        }

        .listitem:nth-child(2n):last-of-type
        {
            border-bottom: 2px solid #EDEDED;
        }

    .listitem_sublistitem
    {
        margin-top: 8px;
        padding-bottom: 8px;
        border-bottom: 1px dotted gray;
    }

        .listitem_sublistitem:last-of-type
        {
            border-bottom: none;
        }

    .listitem_title
    {
        font-size: 1.25em;
        text-decoration: underline;
    }

    .details_link
    {
        display: inline-block;
        padding: 5px;
    }

    .details
    {
        padding: 10px 5px 10px;
    }

    .details_link.active,
    .details
    {
        background-color: #E0E7EF;
    }
</style>

<%if (DefaultShowCollapseLink) { %>
<div style="margin-bottom: 5px; text-align: right">
    <div>
        <a href="javascript://" style="font-size: x-small; white-space: nowrap;" class="showAll">Показать все</a>
        <a href="javascript://" style="font-size: x-small; white-space: nowrap;" class="collapseAll">Свернуть все</a>
    </div>
</div>
<% } %>
<div id="CommonData" class="item_title <%=ItemTitleStyle %>">
    <span>Общие сведения&nbsp;&nbsp;
    <% if (DefaultShowEditLinks)
       {%>
        <a href="<%= GetEditUrl("~/Plans/v2012new/CommonData.aspx") %>">
            <img height="14" title="Редактировать" alt="Править" src="/Content/GB/Styles/Imgs/edit_16.png" /></a>
        <% } %>
    </span>
</div>
<div class="item <%=ItemStyle %>">
    <div class="dataitem">
        <div class="headers_fieldnames">Тип мероприятия:</div>
        <div class="headers_values"><%= ShowValue(CurrentActivity.ActivityType2_Name) %></div>
    </div>
    <div class="dataitem">
        <div class="headers_fieldnames">Наименование ИС или объекта ИКТ-инфраструктуры:</div>
        <div class="headers_values"><%= ShowValue(CurrentActivity.IKTObject) %></div>
    </div>
    <div class="dataitem">
        <div class="headers_fieldnames">Классификационный признак ИС или компонента ИТ-инфраструктуры:</div>
        <div class="headers_values"><%= ShowValue(CurrentActivity.IKTComponentCodeName) %></div>
    </div>
    <div class="dataitem">
        <div class="headers_fieldnames">Аннотация:</div>
        <div class="headers_values"><%= ShowValue(CurrentActivity.AnnotationData) %></div>
    </div>
    <div class="dataitem">
        <div class="headers_fieldnames">Ответственный за мероприятие:</div>
        <div class="headers_values"><%= ShowValue(CurrentActivity.ResponsibleInfo) %></div>
    </div>
    <div class="dataitem">
        <div class="headers_fieldnames">Источник финансирования:</div>
        <div class="headers_values"><%= ShowValue(CurrentActivity.SourceFunding) %></div>
    </div>
    <div class="dataitem">
        <div class="headers_fieldnames">
            Основания реализации мероприятия:
        </div>
        <ul class="values_list headers_values">
            <% foreach (var reason in CurrentActivity.ActivityReasons)
               {%>
            <li class="values_list_item">
                <div>
                    <span>
                        <%= string.Format("{0} \"{1}\"", reason.NPAType_Name, reason.Name) %>
                    </span>
                    <span>
                        <%= string.Format("{2}№{0} {1}", reason.Num,
                                                          reason.Date.HasValue 
                                                                ? ("от " + reason.Date.Value.ToShortDateString())
                                                                : null, 
                                                         !string.IsNullOrEmpty(reason.Num) ? ", " : string.Empty) %>
                    </span>
                    <span>
                        <%= string.Format("{1}{0}", reason.Item, !string.IsNullOrEmpty(reason.Item) ? ", " : string.Empty) %>
                    </span>
                </div>
                <div>
                    <%= getReasonDownloadUrl(string.Join(";", reason.Documents.Select(x => (x.Id.ToString() + ":" + x.FileName)).ToArray()))%>
                </div>
            </li>
            <% } %>
        </ul>
        <% if (CurrentActivity.ActivityReasons.Count == 0)
           { %>
        <div class="headers_values">—</div>
        <% } %>
    </div>
    <div>
        <div class="headers_fieldnames">Цели мероприятия:</div>
        <ul class="values_list headers_values">
            <%
                foreach (var goals in CurrentActivity.Goals)
                {%>
            <li class="values_list_item">
                <%= goals.Name %>
            </li>
            <% } %>
        </ul>
        <% if (CurrentActivity.Goals.Count == 0)
           { %>
        <div class="headers_values">—</div>
        <% } %>
    </div>
</div>
<div id="Functions" class="item_title <%=ItemTitleStyle %>">
    <span>Функции&nbsp;&nbsp;
    <% if (DefaultShowEditLinks)
       {%>
        <a href="<%= GetEditUrl("~/Plans/v2012new/Functions.aspx") %>">
            <img height="14" title="Редактировать" src="/Content/GB/Styles/Imgs/edit_16.png" /></a>
        <% } %>
    </span>
</div>
<div class="item <%=ItemStyle %>">
    <div style="padding: 0px;">
        <% foreach (var func in CurrentActivity.Functions)
           {%>
        <div class="listitem">
            <div class="dataitem">
                <div class="headers_fieldnames">Вид деятельности:</div>
                <div class="headers_values"><%= ShowValue(func.Type) %></div>
            </div>
            <div class="dataitem">
                <div class="headers_fieldnames">Выполняемые функции органом государственной власти:</div>
                <div class="headers_values"><%= ShowValue(func.Name) %></div>
            </div>
            <% var acts = func.Acts.Where(x => !string.IsNullOrEmpty(x));
               if (acts.Any())
               {%>
            <div style="margin-bottom: 10px;"></div>
            <div class="details_link">
                <a href="javascript://" class="command" onclick="ToogleDetails(this)">Подробнее</a>
            </div>
            <div class="details dataitem" style="display: none;">
                <div class="headers_fieldnames">Наименование и реквизиты регулирующих НПА:</div>
                <div class="headers_values">
                    <%foreach (var doc in func.Acts.Where(x => !string.IsNullOrEmpty(x)))
                      { %>
                    <div class="list2"><%= doc%></div>
                    <% } %>
                </div>
            </div>
            <% } %>
        </div>
        <% } %>
        <% if (!CurrentActivity.Functions.Any())
           { %>
        <div class="headers_values">—</div>
        <% } %>
    </div>
</div>
<div id="Indicators" class="item_title <%=ItemTitleStyle %>">
    <span>Показатели&nbsp;&nbsp;
    <% if (DefaultShowEditLinks)
       {%>
        <a href="<%= GetEditUrl("~/Plans/v2012new/Indicators.aspx") %>">
            <img height="14" title="Редактировать" alt="Править" src="/Content/GB/Styles/Imgs/edit_16.png" /></a>
        <% } %>
    </span>
</div>
<div class="item <%=ItemStyle %>">
    <div>
        <%
            foreach (var index in CurrentActivity.Indexes)
            {
        %>
        <div class="listitem">
            <div class="listitem_title">
                <%= index.Name %>
            </div>
            <div class="dataitem">
                <div class="headers_fieldnames">Госуслуга/госфункция:</div>
                <div class="headers_values"><%= index.ActivityFunc %></div>
            </div>
            <div class="dataitem">
                <div class="headers_fieldnames">Цель мероприятия:</div>
                <div class="headers_values"><%=  ShowValue(index.Goal) %></div>
            </div>
            <div class="dataitem">
                <div style="width: 20%; margin-right: 20px; display: inline-block; vertical-align: top;">
                    <div class="headers_fieldnames">Базовое значение:</div>
                    <div class="headers_values" style="margin-top: 5px;"><%= string.Format("{0} {1}", index.PRSFactVal, index.UOM) %></div>
                </div>
                <div style="width: 70%; display: inline-block; vertical-align: top;">
                    <div>
                        <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; display: inline-block;">Плановые значения</div>
                        <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; display: inline-block;"><%= CurrentActivity.Plan_Year %> год</div>
                        <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; display: inline-block;"><%= CurrentActivity.Plan_Year + 1 %> год</div>
                        <div class="headers_fieldnames" style="width: 20%; display: inline-block;"><%= CurrentActivity.Plan_Year + 2 %> год</div>
                    </div>
                    <div style="margin-top: 5px;">
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block; vertical-align: top;">Основной объем</div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= string.Format("{0} {1}", index.FirstPRSFactVal, index.FirstUOM) %></div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= string.Format("{0} {1}", index.SecondPRSFactVal, index.SecondUOM) %></div>
                        <div class="headers_values" style="width: 20%; display: inline-block;"><%= string.Format("{0} {1}", index.ThridPRSFactVal, index.ThridUOM) %></div>
                    </div>
                    <div style="margin-top: 5px;">
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block; vertical-align: top;">Дополнительный</div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.IsNullOrWhiteSpace(index.FirstAddFounding) ? null : string.Format("{0} {1}", index.FirstAddFounding, index.FirstUOM)) %></div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.IsNullOrWhiteSpace(index.SecondAddFounding) ? null : string.Format("{0} {1}", index.SecondAddFounding, index.SecondUOM)) %></div>
                        <div class="headers_values" style="width: 20%; display: inline-block;"><%= ShowValue(string.IsNullOrWhiteSpace(index.ThridAddFounding) ? null : string.Format("{0} {1}", index.ThridAddFounding, index.ThridUOM)) %></div>
                    </div>
                </div>
            </div>
        </div>
        <% } %>
        <% if (!CurrentActivity.Indexes.Any())
           { %>
        <div style="margin-left: 30px; font-size: 1.2em;">Нет данных</div>
        <% } %>
    </div>
</div>
<div class="item_title <%=ItemTitleStyle %>">
    <span>Индикаторы&nbsp;&nbsp;
    <% if (DefaultShowEditLinks)
       {%>
        <a href="<%= GetEditUrl("~/Plans/v2012new/Indicators.aspx") %>">
            <img height="14" title="Редактировать" alt="Править" src="/Content/GB/Styles/Imgs/edit_16.png" /></a>
        <% } %>
    </span>
</div>
<div class="item <%=ItemStyle %>">
    <div>
        <%
            foreach (var indicator in CurrentActivity.Indicators)
            {
        %>
        <div class="listitem">
            <div class="listitem_title">
                <%= indicator.Name %>
            </div>
            <div class="dataitem">
                <div class="headers_fieldnames">Госуслуга/госфункция:</div>
                <div class="headers_values"><%= indicator.ActivityFunc %></div>
            </div>
            <div class="dataitem">
                <div class="headers_fieldnames">Алгоритм расчета:</div>
                <div class="headers_values"><%= ShowValue(indicator.Algorythm) %></div>
            </div>
            <div class="dataitem">
                <div style="width: 70%; display: inline-block; vertical-align: top;">
                    <div>
                        <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; display: inline-block;">Плановые значения</div>
                        <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; display: inline-block;"><%= CurrentActivity.Plan_Year %> год</div>
                        <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; display: inline-block;"><%= CurrentActivity.Plan_Year + 1 %> год</div>
                        <div class="headers_fieldnames" style="width: 20%; display: inline-block;"><%= CurrentActivity.Plan_Year + 2 %> год</div>
                    </div>
                    <div style="margin-top: 5px;">
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block; vertical-align: top;">Основной объем</div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= string.Format("{0} {1}", indicator.FirstPRSFactVal, indicator.FirstUOM) %></div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= string.Format("{0} {1}", indicator.SecondPRSFactVal, indicator.SecondUOM) %></div>
                        <div class="headers_values" style="width: 20%; display: inline-block;"><%= string.Format("{0} {1}", indicator.ThridPRSFactVal, indicator.ThridUOM) %></div>
                    </div>
                    <div style="margin-top: 5px;">
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block; vertical-align: top;">Дополнительный</div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.IsNullOrWhiteSpace(indicator.FirstAddFounding) ? null : string.Format("{0} {1}", indicator.FirstAddFounding, indicator.FirstUOM)) %></div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.IsNullOrWhiteSpace(indicator.SecondAddFounding) ? null : string.Format("{0} {1}", indicator.SecondAddFounding, indicator.SecondUOM)) %></div>
                        <div class="headers_values" style="width: 20%; display: inline-block;"><%= ShowValue(string.IsNullOrWhiteSpace(indicator.ThridAddFounding) ? null : string.Format("{0} {1}", indicator.ThridAddFounding, indicator.ThridUOM)) %></div>
                    </div>
                </div>
            </div>
        </div>
        <% } %>
        <% if (!CurrentActivity.Indicators.Any())
           { %>
        <div style="margin-left: 30px; font-size: 1.2em;">Нет данных</div>
        <% } %>
    </div>
</div>
<div id="Expenses" class="item_title <%=ItemTitleStyle %>">
    <span>Расходы&nbsp;&nbsp;
    <% if (DefaultShowEditLinks)
       {%><a href="<%= GetEditUrl("~/Plans/v2012new/Expenses.aspx") %>"><img height="14" title="Редактировать" alt="Править" src="/Content/GB/Styles/Imgs/edit_16.png" /></a>
        <% } %>
    </span>
</div>
<div class="item <%=ItemStyle %>">
    <div>
        <div class="dataitem">
            <div class="headers_fieldnames">Код БК:</div>
            <div class="headers_values">
                <span title="Код по БК ГРБС" style="margin-right: 5px;"><%= CurrentActivity.GRBS_Code %></span>
                <span title="Код по БК ПЗ/РЗ" style="margin-right: 5px;"><%= CurrentActivity.SectionKBK_Code %></span>
                <span title="Код по БК ЦС" style="margin-right: 5px;"><%= CurrentActivity.ExpenseItem_Code %></span>
                <span title="Код по БК ВР" style="margin-right: 5px;"><%= CurrentActivity.WorkForm_Code %></span>
            </div>
            <div class="details_link">
                <a href="javascript://" class="command" onclick="ToogleDetails(this)">Подробнее</a>
            </div>
            <div class="details dataitem" style="display: none;">
                <div class="dataitem">
                    <div class="headers_fieldnames">Код по БК ГРБС:</div>
                    <div class="headers_values"><%= ShowValue(CurrentActivity.GRBS_Code_Name)  %></div>
                </div>
                <div class="dataitem">
                    <div class="headers_fieldnames">Код по БК ПЗ/РЗ:</div>
                    <div class="headers_values"><%= ShowValue(CurrentActivity.SectionKBK_Code_Name) %></div>
                </div>
                <div class="dataitem">
                    <div class="headers_fieldnames">Код по БК ЦСР:</div>
                    <div class="headers_values"><%= ShowValue(CurrentActivity.ExpenseItem_Code_Name) %></div>
                </div>
                <div class="dataitem">
                    <div class="headers_fieldnames">Подпрограмма:</div>
                    <div class="headers_values"><%= ShowValue(CurrentActivity.Subentry) %></div>
                </div>
                <div class="dataitem">
                    <div class="headers_fieldnames">Код по БК ВР:</div>
                    <div class="headers_values"><%= ShowValue(CurrentActivity.WorkForm_Code_Name)  %></div>
                </div>
            </div>
        </div>
    </div>

    <div class="dataitem">
        <div style="width: 70%;">
            <div class="headers_fieldnames" style="padding-bottom: 0px;">
                Объем планируемых бюджетных ассигнований, тыс. руб:
            </div>
            <div>
                <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; margin-top: 0px; display: inline-block;"></div>
                <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; margin-top: 0px; display: inline-block;"><%= CurrentActivity.Plan_Year %> год</div>
                <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; margin-top: 0px; display: inline-block;"><%= CurrentActivity.Plan_Year + 1 %> год</div>
                <div class="headers_fieldnames" style="width: 20%; margin-top: 0px; display: inline-block;"><%= CurrentActivity.Plan_Year + 2 %> год</div>
            </div>
            <div style="margin-top: 5px;">
                <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block; vertical-align: top;">Основной объем</div>
                <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}",CurrentActivity.PlansActivityVolY0)) %></div>
                <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}",CurrentActivity.PlansActivityVolY1)) %></div>
                <div class="headers_values" style="width: 20%; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}",CurrentActivity.PlansActivityVolY2)) %></div>
            </div>
            <div style="margin-top: 5px;">
                <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block; vertical-align: top;">Дополнительный</div>
                <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}", CurrentActivity.PlansActivityAddVolY0)) %></div>
                <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}", CurrentActivity.PlansActivityAddVolY1)) %></div>
                <div class="headers_values" style="width: 20%; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}", CurrentActivity.PlansActivityAddVolY2)) %></div>
            </div>
        </div>
    </div>

    <div class="dataitem" style="margin-top: 20px;">
        <div style="width: 70%;">
            <div class="headers_fieldnames" style="padding-bottom: 0px;">
                Объем фактически израсходованных бюджетных ассигнований
                 <% if (CurrentActivity.PlansActivityFactDate.HasValue)
                    {%>
                по состоянию на <%= CurrentActivity.PlansActivityFactDate.Value.ToString("dd.MM.yyyy")%>
                <% } %>
                , тыс. руб:
            </div>
            <div>
                <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; margin-top: 0px; display: inline-block;"></div>
                <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; margin-top: 0px; display: inline-block;"><%= CurrentActivity.Plan_Year %> год</div>
                <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; margin-top: 0px; display: inline-block;"><%= CurrentActivity.Plan_Year - 1 %> год</div>
                <div class="headers_fieldnames" style="width: 20%; margin-top: 0px; display: inline-block;"><%= CurrentActivity.Plan_Year - 2 %> год</div>
            </div>
            <div style="margin-top: 5px;">
                <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block; vertical-align: top;">Фактически израсходовано</div>
                <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}",CurrentActivity.PlansActivityFactVolY0)) %></div>
                <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}",CurrentActivity.PlansActivityFactVolY1)) %></div>
                <div class="headers_values" style="width: 20%; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}",CurrentActivity.PlansActivityFactVolY2)) %></div>
            </div>
        </div>
    </div>

    <div class="dataitem" style="margin-top: 20px;">
        <div></div>
        <div class="headers_fieldnames">
                Направления расходования средств на ИКТ:
        </div>
        <% var expenseDirectionses = CurrentActivity.ExpenseDirections.OrderBy(x => x.Code);
           foreach (var ed in CurrentActivity.ExpenseDirections)
           {%>
        <div class="listitem">
            <div class="listitem_title">
                <%= ed.CodeName %>
            </div>
            <div class="dataitem">
                <div style="width: 70%;">
                    <div class="headers_fieldnames" style="padding-bottom: 0px;">
                        Объем планируемых бюджетных ассигнований, тыс. руб:
                    </div>
                    <div>
                        <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; margin-top: 0px; display: inline-block;"></div>
                        <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; margin-top: 0px; display: inline-block;"><%= CurrentActivity.Plan_Year %> год</div>
                        <div class="headers_fieldnames" style="width: 20%; margin-right: 10px; margin-top: 0px; display: inline-block;"><%= CurrentActivity.Plan_Year + 1 %> год</div>
                        <div class="headers_fieldnames" style="width: 20%; margin-top: 0px; display: inline-block;"><%= CurrentActivity.Plan_Year + 2 %> год</div>
                    </div>
                    <div style="margin-top: 5px;">
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block; vertical-align: top;">Основной объем</div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}",ed.ExpenseVolY0)) %></div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}",ed.ExpenseVolY1)) %></div>
                        <div class="headers_values" style="width: 20%; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}",ed.ExpenseVolY2)) %></div>
                    </div>
                    <div style="margin-top: 5px;">
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block; vertical-align: top;">Дополнительный</div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}", ed.ExpenseAddVolY0)) %></div>
                        <div class="headers_values" style="width: 20%; margin-right: 10px; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}", ed.ExpenseAddVolY1)) %></div>
                        <div class="headers_values" style="width: 20%; display: inline-block;"><%= ShowValue(string.Format("{0:#,##0.0000}", ed.ExpenseAddVolY2)) %></div>
                    </div>
                </div>
            </div>
        </div>
        <% } %>
        <% if (!expenseDirectionses.Any())
                   { %>
        <div style="margin-left: 30px; font-size: 1.2em;">Нет данных</div>
                <% } %>
    </div>
</div>
<div id="Results" class="item_title <%=ItemTitleStyle %>">
    <span>Результаты&nbsp;&nbsp;
    <% if (DefaultShowEditLinks)
       {%><a href="<%= GetEditUrl("~/Plans/v2012new/GoalsResults.aspx") %>"><img height="14" title="Редактировать" alt="Править" src="/Content/GB/Styles/Imgs/edit_16.png" /></a>
        <% } %>
    </span>
</div>
<div class="item <%=ItemStyle %>">
    <div>
        <% foreach (var plansContract in CurrentActivity.Contracts)
           {%>
        <div class="listitem">
            <div class="dataitem">
                <div class="headers_fieldnames">Номер контракта:</div>
                <div class="headers_values"><%= ShowValue(plansContract.Number) %></div>
            </div>
            <div class="dataitem">
                <div class="headers_fieldnames">Предмет контракта:</div>
                <div class="headers_values"><%= plansContract.Products.Any() ? string.Join("<br />", plansContract.Products) : "Не задано" %></div>
            </div>
            <div class="dataitem">
                <div class="headers_fieldnames">Дата заключения контракта:</div>
                <div class="headers_values"><%= plansContract.Sign_Date.HasValue ? plansContract.Sign_Date.Value.ToShortDateString() : "Не задано" %></div>
            </div>
            <div class="dataitem">
                <div class="headers_fieldnames">Описание ранее достигнутых результатов:</div>
                <div class="headers_values"><%= plansContract.DescriptionPreviousResult %></div>
            </div>
        </div>
        <%} %>
        <%if (!CurrentActivity.Contracts.Any())
          { %>
        <div style="margin-left: 30px; font-size: 1.2em;">Нет данных</div>
        <% } %>
    </div>
</div>
<div id="Works" class="item_title <%=ItemTitleStyle %>">
    <span>Работы&nbsp;&nbsp;
    <% if (DefaultShowEditLinks)
       {%><a href="<%= GetEditUrl("~/Plans/v2012new/Works.aspx") %>"><img height="14" title="Редактировать" alt="Править" src="/Content/GB/Styles/Imgs/edit_16.png" /></a>
        <% } %>
    </span>
</div>
<div class="item <%=ItemStyle %>">
    <div>
        <% foreach (var work in CurrentActivity.Works)
           {%>
        <div class="listitem">
            <div title="Наименование" class="listitem_title">
                <%= work.Name %>
            </div>
            <div style="margin-top: 10px;">
                <div class="commondataitem" style="width: 8%; margin-right: 20px; margin-left: 5px;">
                    <div class="headers_fieldnames">Номер:</div>
                    <div class="headers_values"><%= work.ActivityWorkCode %></div>
                </div>
                <div class="commondataitem" style="width: 8%; margin-right: 20px;">
                    <div class="headers_fieldnames" style="">Код ОКДП:</div>
                    <div class="headers_values"><%= ShowValue(work.OKDP_CodeName) %></div>
                </div>
                <div class="commondataitem" style="width: 15%; margin-right: 20px;">
                    <div class="headers_fieldnames" style="">Код по БК КОСГУ:</div>
                    <div class="headers_values"><%= ShowValue(work.ExpeditureItem) %></div>
                </div>
                <div class="commondataitem" style="width: 47%; margin-right: 20px;">
                    <div class="headers_fieldnames">Вид затрат:</div>
                    <div class="headers_values"><%= work.ExpenseDirection_CodeName %></div>
                </div>
                <div class="commondataitem" style="width: 28%;">
                    <div class="headers_fieldnames" style="">Объем бюджетных ассигнований, тыс. руб:</div>
                    <div class="headers_values"><%= string.Format("{0:#,##0.0000}", work.ExpenseVolumeYear0) %></div>
                </div>
                <% if (work.WorkTypes.Any())
                   {%>
                <div style="margin-bottom: 10px;"></div>
                <div class="details_link">
                    <a href="javascript://" class="command" onclick="ToogleDetails(this)">Подробнее</a>
                </div>
                <div class="details" style="vertical-align: top; display: none; width: 69%">
                    <div>
                        <div class="headers_fieldnames" style="width: 62%; margin-right: 20px; display: inline-block;">Тип затрат:</div>
                        <div class="headers_fieldnames" style="width: 30%; display: inline-block; text-align: center;">Объем бюджетных ассигнований, тыс. руб:</div>
                    </div>
                    <% foreach (var workType in work.WorkTypes)
                       {%>
                    <div class="listitem_sublistitem">
                        <div class="headers_values" style="width: 62%; margin-right: 20px; display: inline-block;"><%= workType.ExpenseType_Name %></div>
                        <div class="headers_values" style="width: 30%; display: inline-block; text-align: center; vertical-align: top;"><%= string.Format("{0:#,##0.0000}", workType.ExpenseVolumeYear0) %></div>
                    </div>
                    <% } %>
                </div>
                <% } %>
            </div>
        </div>
        <% } %>
        <%if (!CurrentActivity.Works.Any())
          { %>
        <div style="margin-left: 30px; font-size: 1.2em;">Нет данных</div>
        <% } %>
    </div>
    <div style="padding: 10px;">
        <% if (CurrentActivity.WorkDocuments.Any())
          {%>
        <div class="details_link">
            <a href="javascript://" class="command" onclick="ToogleDetails(this)" style="">Документы</a>
        </div>
        <div class="details" style="vertical-align: top; display: none;">
            <div>
                <div class="headers_fieldnames" style="width: 25%; margin-right: 20px; display: inline-block;">Наименование:</div>
                <div class="headers_fieldnames" style="width: 25%; margin-right: 20px; display: inline-block;">Номер:</div>
                <div class="headers_fieldnames" style="width: 25%; margin-right: 20px; display: inline-block;">Дата добавления:</div>
                <div class="headers_fieldnames" style="width: 15%; display: inline-block;"></div>
            </div>
            <%foreach (var doc in CurrentActivity.WorkDocuments)
              {%>
            <div class="listitem_sublistitem">
                <div class="headers_values" style="width: 25%; margin-right: 20px; display: inline-block;"><%= ShowValue(doc.Name) %></div>
                <div class="headers_values" style="width: 25%; margin-right: 20px; display: inline-block;"><%= ShowValue(doc.Num) %></div>
                <div class="headers_values" style="width: 25%; margin-right: 20px; display: inline-block;"><%= GetDate(doc.Date) %></div>
                <div class="headers_values" style="width: 15%; display: inline-block;"><%= GetDocDownloadUrl(doc.Id) %></div>
            </div>
            <% } %>
        </div>
        <% }%>
    </div>
</div>
<div id="Specification" class="item_title <%=ItemTitleStyle %>">
    <span>Товары&nbsp;&nbsp;
    <% if (DefaultShowEditLinks)
       {%><a href="<%= GetEditUrl("~/Plans/v2012new/Specifications.aspx") %>"><img height="14" title="Редактировать" alt="Править" src="/Content/GB/Styles/Imgs/edit_16.png" /></a>
        <% } %>
    </span>
</div>
<div class="item <%=ItemStyle %>">
    <div>
        <% foreach (var spec in CurrentActivity.Specs)
           {
        %>
        <div class="listitem">
            <div title="Наименование" class="listitem_title">
                <%= spec.Name %>
            </div>
            <div style="margin-top: 10px;">
                <div class="commondataitem" style="width: 8%; margin-right: 20px; margin-left: 5px;">
                    <div class="headers_fieldnames">Номер:</div>
                    <div class="headers_values"><%= spec.ActivitySpecCode %></div>
                </div>
                <div class="commondataitem" style="width: 8%; margin-right: 20px;">
                    <div class="headers_fieldnames" style="">Код ОКДП:</div>
                    <div class="headers_values"><%= ShowValue(spec.OKDP_Text) %></div>
                </div>
                <div class="commondataitem" style="width: 15%; margin-right: 20px;">
                    <div class="headers_fieldnames" style="">Код по БК КОСГУ:</div>
                    <div class="headers_values"><%= ShowValue(spec.ExpeditureItem) %></div>
                </div>
                <div class="commondataitem" style="width: 35%; margin-right: 20px;">
                    <div class="headers_fieldnames">Тип оборудования:</div>
                    <div class="headers_values"><%= ShowValue(spec.QT) %></div>
                </div>
                <div class="commondataitem" style="width: 10%; margin-right: 20px;">
                    <div class="headers_fieldnames">Количество, ед.:</div>
                    <div class="headers_values"><%= spec.Quantity %></div>
                </div>
                <div class="commondataitem" style="width: 25%;">
                    <div class="headers_fieldnames" style="">Стоимость, тыс. руб.:</div>
                    <div class="headers_values"><%= ShowValue(spec.Cost) %></div>
                </div>
                <div style="margin-bottom: 10px;"></div>
                <div class="details_link">
                    <a href="javascript://" class="command" onclick="ToogleDetails(this)">Подробнее</a>
                </div>
                <div class="details" style="vertical-align: top; display: none; width: 69%">
                    <div class="dataitem">
                        <div class="headers_fieldnames">Вид затрат:</div>
                        <div class="headers_values"><%= ShowValue(spec.ExpenseDirection_CodeName)%></div>
                    </div>
                    <div class="dataitem">
                        <div class="headers_fieldnames">Тип затрат:</div>
                        <div class="headers_values"><%= ShowValue(spec.ExpName)%></div>
                    </div>
                    <div class="dataitem">
                        <div class="headers_fieldnames">Аналог:</div>
                        <div class="headers_values"><%= ShowValue(spec.Analog)%></div>
                    </div>
                    <div class="dataitem">
                        <div class="headers_fieldnames">Цена аналога:</div>
                        <div class="headers_values"><%= ShowValue(spec.AnalogPrice)%></div>
                    </div>
                    <% if (spec.SpecTypes.Any())
                       {%>
                    <div>
                        <div>
                            <div class="headers_fieldnames" style="width: 30%; margin-right: 20px; display: inline-block;">Наименование характеристики:</div>
                            <div class="headers_fieldnames" style="width: 30%; margin-right: 20px; display: inline-block;">Единица измерения характеристики:</div>
                            <div class="headers_fieldnames" style="width: 30%; display: inline-block;">Значение характеристики:</div>
                        </div>
                        <% foreach (var specType in spec.SpecTypes)
                           {%>
                        <div class="listitem_sublistitem">
                            <div class="headers_values" style="width: 30%; margin-right: 20px; display: inline-block;"><%= specType.CharName %></div>
                            <div class="headers_values" style="width: 30%; margin-right: 20px; display: inline-block;"><%= specType.CharUnit %></div>
                            <div class="headers_values" style="width: 30%; display: inline-block;"><%= specType.CharValue %></div>
                        </div>
                        <% } %>
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
        <%} %>
        <%if (!CurrentActivity.Specs.Any())
          { %>
        <div style="margin-left: 30px; font-size: 1.2em;">Нет данных</div>
        <% } %>
    </div>
    <div style="padding: 10px;">
        <% if (CurrentActivity.SpecDocuments.Any())
          {%>
        <div class="details_link">
            <a href="javascript://" class="command" onclick="ToogleDetails(this)" style="">Документы</a>
        </div>
        <div class="details" style="vertical-align: top; display: none;">
            <div>
                <div class="headers_fieldnames" style="width: 25%; margin-right: 20px; display: inline-block;">Наименование:</div>
                <div class="headers_fieldnames" style="width: 25%; margin-right: 20px; display: inline-block;">Номер:</div>
                <div class="headers_fieldnames" style="width: 25%; margin-right: 20px; display: inline-block;">Дата добавления:</div>
                <div class="headers_fieldnames" style="width: 15%; display: inline-block;"></div>
            </div>
            <%foreach (var doc in CurrentActivity.SpecDocuments)
              {%>
            <div class="listitem_sublistitem">
                <div class="headers_values" style="width: 25%; margin-right: 20px; display: inline-block;"><%= ShowValue(doc.Name) %></div>
                <div class="headers_values" style="width: 25%; margin-right: 20px; display: inline-block;"><%= ShowValue(doc.Num) %></div>
                <div class="headers_values" style="width: 25%; margin-right: 20px; display: inline-block;"><%= GetDate(doc.Date) %></div>
                <div class="headers_values" style="width: 15%; display: inline-block;"><%= GetDocDownloadUrl(doc.Id) %></div>
            </div>
            <% } %>
        </div>
        <% }%>
    </div>
</div>
<div id="InitialActivity" class="item_title <%=ItemTitleStyle %>">
    <span>Исходные меропритятия&nbsp;&nbsp;
    <% if (DefaultShowEditLinks)
       {%><a href="<%= GetEditUrl("~/Plans/v2012new/InitialActivity.aspx") %>"><img height="14" title="Редактировать" alt="Править" src=/Content/GB/Styles/Imgs/edit_16.png" /></a><%}%>
    </span>
</div>
<div class="item <%=ItemStyle %>">
    <div>
        <% foreach (var a in CurrentActivity.InitialActivity)
           {%>
        <div class="listitem">
            <div class="commondataitem">
                <div class="headers_fieldnames">Рег. №:</div>
                <div class="headers_values"><%= ShowValue(a.RegNum) %></div>
            </div>
            <div class="commondataitem">
                <div class="headers_fieldnames" >Наименование мероприятия:</div>
                <div class="headers_values" ><%= ShowValue(a.Name) %></div>
            </div>
            <div class="commondataitem">
                <div class="headers_fieldnames">БК ВР:</div>
                <div class="headers_values"><%= ShowValue(a.WorkForm) %></div>
            </div>
            <div class="commondataitem">
                <div class="headers_fieldnames">БК КОСГУ:</div>
                <div class="headers_values"><%= ShowValue(a.ExpenditureItem) %></div>
            </div>
            <div class="commondataitem">
                <div class="headers_fieldnames">Примечание</div>
                <div class="headers_values"><%= ShowValue(a.Comment) %></div>
            </div>
            <div class="commondataitem">
                <div class="headers_fieldnames">Сумма, тыс. руб.:</div>
                <div class="headers_values"><%= ShowValue(a.Summ) %></div>
            </div>
        </div>
        <%} %>
        <%if (!CurrentActivity.InitialActivity.Any())
          { %>
        <div style="margin-left: 30px; font-size: 1.2em;">Нет данных</div>
        <% } %>
    </div>
</div>
<% if(CurrentActivity.Plan_Type == (int)GB.MKRF.Entities.Directories.ExpObjectTypeEnum.Plan2012rel) { %>

<div id="Purchases" class="item_title <%=ItemTitleStyle %>">
    <span>Закупки&nbsp;&nbsp;
    <% if (DefaultShowEditLinks)
       {%><a href="<%= GetEditUrl("~/Plans/v2012new/Purchases.aspx") %>"><img height="14" title="Редактировать" alt="Править" src="/Content/GB/Styles/Imgs/edit_16.png" /></a>
        <% } %>
    </span>
</div>
<div class="item <%=ItemStyle %>">    
    <div>    
    <% foreach (var item in CurrentActivity.PlansZakupki) { %>
        <div class="listitem">
        <div title="Наименование" class="listitem_title">
            <%= item.NumberName %>
        </div>
        <div style="margin-top: 10px;">
            <div class="commondataitem" style="width: 12%; margin-right: 20px; margin-left: 5px;">
                <div class="headers_fieldnames">Номер:</div>
                <div class="headers_values"><%= item.Number %></div>
            </div>
            <div class="commondataitem" style="width: 26%; margin-right: 20px; margin-left: 5px;">
                <div class="headers_fieldnames">Наименование предмета контракта:</div>
                <div class="headers_values"><%= item.Name %></div>
            </div>

            <div class="commondataitem" style="width: 13%; margin-right: 20px; margin-left: 5px;">
                <div class="headers_fieldnames">Планируемая дата публикации:</div>
                <div class="headers_values"><%= GetDate(item.PublishDate) %></div>
            </div>
            <div class="commondataitem" style="width: 16%; margin-right: 20px; margin-left: 5px;">
                <div class="headers_fieldnames">Способ размещения заказа:</div>
                <div class="headers_values"><%= item.OrderWay_Name %></div>
            </div>
            <div class="commondataitem" style="width: 10%; margin-right: 20px; margin-left: 5px;">
                <div class="headers_fieldnames">Статус:</div>
                <div class="headers_values"><%= item.Status_Name %></div>
            </div>
            <div class="commondataitem" style="width: 12%; margin-right: 20px; margin-left: 5px;">
                <div class="headers_fieldnames">Номер извещения:</div>
                <div class="headers_values"><%= item.IzvNumb %></div>
            </div>
            <div class="commondataitem" style="width: 12%; margin-right: 20px; margin-left: 5px;">
                <div class="headers_fieldnames">Дата извещения:</div>
                <div class="headers_values"><%= GetDate(item.IzvDate) %></div>
            </div>
            <div class="commondataitem" style="width: 11%; margin-right: 20px; margin-left: 5px;">
                <div class="headers_fieldnames">Дата вскрытия конвертов:</div>
                <div class="headers_values"><%= GetDate(item.OpenDate) %></div>
            </div>
            <div class="commondataitem" style="width: 13%; margin-right: 20px; margin-left: 5px;">
                <div class="headers_fieldnames">Дата рассмотрения заявок:</div>
                <div class="headers_values"><%= GetDate(item.ConsDate) %></div>
            </div>
            <div class="commondataitem" style="width: 12%; margin-right: 20px; margin-left: 5px;">
                <div class="headers_fieldnames">Дата подведения итогов:</div>
                <div class="headers_values"><%= GetDate(item.ItogDate) %></div>
            </div>
            <div style="padding: 20px 0">
                <div class="details_link">
                    <a href="javascript://" class="command" onclick="ToogleDetails(this)">Лоты:</a>
                </div>
                <div class="details" style="vertical-align: top; display: none; width: 99%">

                    <div>
                        
                        <div class="headers_fieldnames" style="width: 8%; margin-right: 20px; display: inline-block;">Номер:</div>
                        <div class="headers_fieldnames" style="width: 15%; margin-right: 20px; display: inline-block;">Код ОКДП:</div>
                        <div class="headers_fieldnames" style="width: 40%; margin-right: 20px; display: inline-block;">Наименование:</div>
                        <div class="headers_fieldnames" style="width: 10%; margin-right: 20px; display: inline-block;">Единица измерения:</div>
                        <div class="headers_fieldnames" style="width: 10%; margin-right: 20px; display: inline-block;">Количество:</div>
                        <div class="headers_fieldnames" style="width: 1%; display: inline-block;"></div>
                    </div>

                    <% foreach (var lot in item.Lots) { %>

                        <div class="listitem_sublistitem">
                            <div class="headers_values" style="width: 8%; margin-right: 20px; display: inline-block;"><%= lot.Number %></div>
                            <div class="headers_values" style="width: 15%; margin-right: 20px; display: inline-block;"><%= lot.Code %></div>
                            <div class="headers_values" style="width: 40%; margin-right: 20px; display: inline-block;"><%= lot.Name %></div>
                            <div class="headers_values" style="width: 10%; margin-right: 20px; display: inline-block;"><%= lot.Unit %></div>
                            <div class="headers_values" style="width: 10%; margin-right: 20px; display: inline-block;"><%= lot.Count %></div>
                            <div class="headers_values" style="width: 1%; display: inline-block;"></div>
                        </div>
                           
                    <% } %>
                </div>
            </div>
        </div>
        </div>
    <% } %>
     <%if (!CurrentActivity.PlansZakupki.Any())
        { %>
    <div style="margin-left: 30px; font-size: 1.2em;">Нет данных</div>
    <% } %>
    </div>
</div>

<% } %>

<script type="text/javascript" src="/Content/GB/Scripts/ActivityReadMode.js">
</script>
