﻿using System.Collections.Generic;

namespace bgTeam.IsRui.Zakupki360.Dto
{
    class ResponseGetConracts
    {
        public string Error { get; set; }
        public IEnumerable<ContractInfo> Contracts { get; set; }
    }
}
