﻿namespace GB.Data.Workflow
{
    /// <summary>Группа статусов</summary>
    public class StatusGroup : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }
    }
}