﻿using System;
using GB.Data.Directories;
using GB.Data.Frgu;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Нормативный правовой акт ОУ
    /// </summary>
    public class AOGovServiceNPA : EntityBase
    {
        /// <summary>
        /// Ссылка на реализуемые государственные услуги и функции объекта учета
        /// </summary>
        public virtual AOGovService GovService { get; set; }

        /// <summary>
        /// Если копировали с NPA
        /// </summary>
        public virtual GovServiceNPA GovServiceNpa { get; set; }

        /// <summary>
        ///// Вид документа
        ///// </summary>
        //public virtual NPAKind Kind { get; set; }

        /// <summary>
        /// Hаименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>       
        public virtual string DocumentNumber { get; set; }

        /// <summary>
        /// Дата документа
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Пункт, статья документа
        /// </summary>
        public virtual string ParticleOfDocument { get; set; }
    }
}
