﻿using GB.MKRF.Entities.Documents;

namespace GB.MKRF.Mapping.Documents
{
    class PaperTypeMap : ClassMapBase<PaperType>
    {
        public PaperTypeMap()
            : base("Papper_Type")
        {
            //Table("w_");

            //Id(x => x.Id, "Papper_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Papper_Type_Name");
        }
    }
}