﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class QualifierGroupMap : ClassMapBase<QualifierGroup>
    {
        public QualifierGroupMap()
            : base("Qualifier_Group") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "Qualifier_Group_Name");
        }
    }
}