﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;

namespace GB.DataAccess.Command.AccountingObjects.List
{
    public class AddInFavoritesCommand : ICommand<AddInFavoritesContext, bool>
    {
        ISessionFactoryGb factory;

        public AddInFavoritesCommand()
        {
            factory = new SessionFactory();
        }

        public bool Execute(AddInFavoritesContext context)
        {
            bool res = false;
            var currentUser = UserRepository.GetCurrent();

            //TODO сделать синхронизацию потоков
            //чтобы быстро щелкая нельзя было добавть несколько записей

            var entity =
                new Repository<AOFavorite>().Get(
                    q => q
                        .Where(x => x.AccountingObject.Id == context.AccountingObjectId)
                        .Where(x => x.User == currentUser)
                    );

            using (var session = factory.Create())
            {
                if (entity == null)
                {
                    var accountingObject = new Repository<AccountingObject>().GetExisting(context.AccountingObjectId);
                    entity = new AOFavorite();
                    entity.AccountingObject = accountingObject;
                    entity.User = currentUser;
                    session.Insert(entity);
                    res = true;
                }
                else
                {
                    session.Delete(entity);
                    res = false;
                }

                session.Commit();
                return res;
            }
        }
    }
}
