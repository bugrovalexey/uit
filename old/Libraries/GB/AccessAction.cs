﻿using System;
using System.ComponentModel;

namespace GB
{
    [Flags]
    public enum AccessAction
    {
        [Description("Запрещен")]
        None = 0,

        [Description("Чтение")]
        Read = 1,

        [Description("Редактирование")]
        Edit = 2,

        [Description("Удаление")]
        Delete = 4,

        [Description("Создание")]
        Create = 8,
        
        //!!!Внимание!!!
        //При добавлении новых значений, добавить их в All
        [Description("Полный доступ")]
        All = Read | Edit | Delete | Create,
    }
}