﻿using System;
using Uviri.Entity.Import;
using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// ЦОД, отчет по плану
    /// </summary>
    public class ReportZod : EntityBase
    {
        protected ReportZod()
        {
        }

        public ReportZod(PlansActivity activity)
        {
            this.PlansActivity = activity;
        }

        public ReportZod(Zod zod)
        {
            this.Zod = zod;
            this.PlansActivity = zod.PlansActivity;
        }


        /// <summary>Мероприятия плана</summary>
        public virtual PlansActivity PlansActivity { get; protected set; }

        /// <summary>Наименование государственного органа, совместно использующего ИС или компоненты ИКТ</summary>
        public virtual Department OGV { get; set; }

        /// <summary>Наименование уполномоченного государственного органа</summary>
        public virtual Department OGVAuthorized { get; set; }

        /// <summary>Наименование ИС/ИКТ </summary>
        public virtual String IKTComponent { get; set; }

        /// <summary>Был изменён </summary>
        public virtual bool IsChange  { get; set; }

        /// <summary>ЦОД</summary>
        public virtual Zod Zod { get; protected set; }

        public virtual RegistryImport RegistryImport { get; set; }
    }
}
