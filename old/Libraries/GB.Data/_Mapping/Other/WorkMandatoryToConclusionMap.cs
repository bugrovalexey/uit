﻿using Uviri.MKRF.Entities.Other;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Other
{
    public class WorkMandatoryToConclusionMap : ClassMapBase<WorkMandatoryToConclusion>
	{
		public WorkMandatoryToConclusionMap()
            : base("Exp_Concl_Work_Mand")
		{
//            Table("");

//            Id(x => x.Id, "Exp_Concl_Work_Mand_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            string args = @" @Exp_Concl_Work_Mand_Is_Complete = ?
//							,@Exp_Concl_Work_Mand_Comm = ? 
//							,@Exp_Conclusion_ID = ? 
//							,@Work_Mandatory_ID = ? 
//							,@Exp_Concl_Work_Mand_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Exp_Concl_Work_Mand_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Exp_Concl_Work_Mand_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Concl_Work_Mand_Delete
//						  @Exp_Concl_Work_Mand_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.IsComplete, "Exp_Concl_Work_Mand_Is_Complete").Nullable();
			Map(x => x.Comment, "Exp_Concl_Work_Mand_Comm").Nullable();

			References<ExpConclusion>(x => x.ExpConclusion, "Exp_Conclusion_ID").Not.Nullable();
			References<WorkMandatory>(x => x.WorkMandatory, "Work_Mandatory_ID").Not.Nullable();
        }
    }
}