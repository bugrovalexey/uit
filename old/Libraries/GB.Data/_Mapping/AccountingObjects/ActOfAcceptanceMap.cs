﻿using GB.Data.AccountingObjects;
using GB.Data.Documents;
using GB.Data;
using FluentNHibernate.Mapping;
using GB.Data.Plans;

namespace GB._Mapping.AccountingObjects
{
    class ActOfAcceptanceMap : ClassMapHiLo<ActOfAcceptance>
    {
        public ActOfAcceptanceMap()
            : base("Act") { }

        protected override void InitMap()
        {
            Map(x => x.DocName, "Act_Name");
            Map(x => x.DocNumber, "Act_Number");
            Map(x => x.DocDateAdoption, "Act_Date");
            Map(x => x.Cost, "Act_Cost").Not.Nullable();
            Map(x => x.ReasonsWaitingResultsDeviation, "Act_Reason");
            Map(x => x.ActivityType, "Act_Activity_Type").CustomType<ActivityTypeEnum>();
            
            References(x => x.GovContract, "OU_Gos_Contract_ID").Not.Nullable();

            //Нельзя ставить каскад на удаление,так как удаление Работ и Товаров
            //возможно только при удалении соответсвующих ПО, ТО и т.д.
            //Нельзя добавлять Inverse - тоже удаляет.
            HasMany(x => x.Works).KeyColumns.Add("Act_ID");

            HasMany(x => x.ActToAOActivities).KeyColumns.Add("Act_ID").Cascade.Delete().Inverse();

            HasMany(x => x.Documents).KeyColumns.Add("Document_Owner_ID")
                .Where("Doc_Type_ID = " + (int)DocTypeEnum.ActOfAcceptance)
                .Where("Portal_Table_ID = " + (int)EntityType.ActOfAcceptance)
                .Inverse();
        }

    }
}
