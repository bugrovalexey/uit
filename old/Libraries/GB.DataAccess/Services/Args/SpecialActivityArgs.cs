﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Services.Args
{
    public class SpecialActivityArgs : BaseArgsNew
    {
        public bool InformAboutActivityGovOrgan { get; set; }
    }
}
