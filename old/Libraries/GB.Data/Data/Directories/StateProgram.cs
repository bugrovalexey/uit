﻿using GB.Data.Common;
using System.Collections.Generic;

namespace GB.Data.Directories
{
    /// <summary>Государственная программа</summary>
    public class StateProgram : EntityBase, IEntityName
    {
        /// <summary>Наименование показателя</summary>
        public virtual string Name { get; set; }

        /// <summary>Номер уровня иерархии (NOT NULL)</summary>
        //public virtual int Level { get; set; }

        /// <summary>Родительская программа</summary>
        public virtual StateProgram Parent { get; set; }

        /// <summary>
        /// Дочерние программы
        /// </summary>
        public virtual IList<StateProgram> Children { get; protected set; }

        /// <summary>ОГВ</summary>
        public virtual Department Owner { get; set; }

        /// <summary>
        /// Показатели программы
        /// </summary>
        public virtual IList<StateProgramMark> Marks { get; set; }
    }
}