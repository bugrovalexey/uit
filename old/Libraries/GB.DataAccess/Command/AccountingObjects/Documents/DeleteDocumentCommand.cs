﻿using GB.Data.AccountingObjects;
using GB.Data.Documents;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;
using GB.Helpers;
using System;
using System.Collections.Generic;

namespace GB.DataAccess.Command.AccountingObjects.Documents
{
    public class DeleteDocumentCommand : ICommand<DeleteDocumentContext>
    {
        ISessionFactoryGb _factory;

        public DeleteDocumentCommand(ISessionFactoryGb factory)
        {
            _factory = factory;
        }

        public void Execute(DeleteDocumentContext context)
        {            
            if (context.xId.HasValue)
            {
                var entity = new Repository<Files>().Get(context.xId.Value);

                if (entity != null)
                {
                    if (context.Session != null)
                    {
                        FileProvider.DeleteFile(context.xId.Value);
                        context.Session.Delete(entity);
                        //session.Commit();
                    }
                    else
                    {

                        using (var session = context.Session ?? _factory.Create())
                        {
                            FileProvider.DeleteFile(context.xId.Value);
                            session.Delete(entity);
                            session.Commit();
                        }
                    }
                }
            }
        }
    }
}
