﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GB.Helpers;
using GB.MKRF.Entities;
using GB.Data.Extensions;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Entities.Statuses;
using GB.MKRF.Repository;
using GB.MKRF.Workflow;
using GB.Data.Plans;

namespace GB.Coord.Workflow
{
    public class ActivityWorkflowHandler : WorkflowHandlerBase<PlansActivity>
    {
        protected override void CheckNewStatus(PlansActivity a, Status newStatus)
        {
            StatusEnum status = (StatusEnum)newStatus.Id;
            switch (status)
            {
                case StatusEnum.ActOnExpertise:
                    if (a.ActivityType2.IsEq(ActivityTypeEnum.Develop) || a.ActivityType2.IsEq(ActivityTypeEnum.Exploitation))
                    {
                        // В случае если мероприятие по эксплуатации или развитию,
                        // то необходимо проверять что на вкладке "Общие сведения",
                        // в поле "Объект учета" выбрано значение из списка объектов учета.
                        if (a.AccountingObject == null)
                        {
                            throw new IncompleteInfoException(
                                "Пожалуйста, заполните сведения об объекте учета на вкладке Общие сведения",
                                "CommonDataSingle",
                                "Activities",
                                new { a.Id });
                        }
                    }
                    else if (a.ActivityType2.IsEq(ActivityTypeEnum.Create))
                    {
                        // Если мероприятие направлено на создание ОУ,
                        // то необходимо проверять что все поля в разделе "Объект учета"
                        // (Полное и краткое наименования, тип и вид ОУ) заполнены.
                        if (a.AccountingObject == null
                            || string.IsNullOrWhiteSpace(a.AccountingObject.FullName)
                            || string.IsNullOrWhiteSpace(a.AccountingObject.ShortName)
                            || a.AccountingObject.Type == null
                            || a.AccountingObject.Kind == null)
                        {
                            throw new IncompleteInfoException(
                                "Пожалуйста, заполните сведения об объекте учета на вкладке Общие сведения",
                                "CommonDataSingle",
                                "Activities",
                                new { a.Id });
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException("Неизвестный тип мероприятия: " + a.ActivityType2);
                    }

                    //Вне зависимости от типа мероприятия, необходимо проверять наличие хотя бы одной характеристики,
                    //заполненной на вкладке "Характеристики ОУ"
                    if (AccountingObjectCharacteristicsValueRepository.GetAllForActivity(a).Count == 0)
                    {
                        throw new IncompleteInfoException(
                            "Пожалуйста, заполните сведения на вкладке Характеристики ОУ",
                            "CharacteristicsSingle",
                            "Activities",
                            new { a.Id });
                    }

                    //На вкладке "Основания" должны быть заполнены, либо три поля "Государственная программа",
                    //"Подпрограмма государственной программы" и "Основное мероприятие государственной программы",
                    //либо в списке "Другие основания реализации" должен быть добавлен хотя бы один элемент.
                    if (!((a.StateProgram != null && a.StateSubProgram != null && a.StateMainEvent != null) || (a.OtherReasons.Any())))
                    {
                        throw new IncompleteInfoException(
                            "Пожалуйста, заполните основания реализации мероприятия на вкладке Основания",
                            "ReasonsSingle",
                            "Activities",
                            new { a.Id });
                    }

                    //В случае если на вкладке расходы, все поля "Очередной финансовый год, руб.", "Первый год планового периода, руб.", "Второй год планового периода, руб." 
                    //содержат значение 0,00, то не блокировать перевод статуса, но при попытке его совершить, выводить предупреждающее сообщение с двумя кнопками - продолжить и отмена.
                    if (a.PlansActivityVolY0 + a.PlansActivityAddVolY1 + a.PlansActivityAddVolY2 == 0)
                        throw new OperationConfirmException(
                            "Внимание! Сумма расходов, указанная на очередной и планируемый период реализации, равна 0,00 руб. Вы уверены что хотите продолжить?");
                    break;
                case StatusEnum.ActOnCompletion:
                    break;
                case StatusEnum.ActAgreed:
                    break;
                case StatusEnum.ActSendedToMKS:
                    break;
                case StatusEnum.ActRejectedAtMKS:
                    /* В случае если мероприятие отклонено МКС, пользователю для того, чтобы перевести мероприятие в соответствующий статус 
                     * необходимо приложить документ с замечаниями Минкомсвязи. Для этого после нажатия на кнопку "Отклонено МКС", 
                     * должно появляться выпадающее окно - Прикрепите замечания МКС, с возможностью загрузки документа 
                     * в форматах pdf, tiff, tif, jpg, rar, zip, doc, xls, docx, xlsx. */
                    break;
                case StatusEnum.ActIncludedInPlan:
                    break;
                
                //default:
                //    throw new InvalidOperationException("Мероприятие не может иметь статус: " + status);
            }
        }
    }
}