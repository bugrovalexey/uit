﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB;
using GB.Extentions;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.AccountingObjects;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Extensions;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.Data;
using GB.Controls;
using GB.Controls.Extensions;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.AccountingObjects
{
    public partial class Characteristics : BasePageAccountingObject
    {
        #region Fields

        private AccountingObjectCharacteristicsService _accountingObjectCharacteristicsService;

        #endregion Fields


        #region Prop

        public AccountingObjectCharacteristicsService AccountingObjectCharacteristicsService
        {
            get
            {
                if (_accountingObjectCharacteristicsService == null)
                {
                    _accountingObjectCharacteristicsService = new AccountingObjectCharacteristicsService();
                }

                return _accountingObjectCharacteristicsService;
            }
        }


        #endregion Prop


        #region Handlers

        #region PageHandlers
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            gridCharacteristics.DataBind();
        }

        #endregion PageHandlers

        #region Control Handlers

        #region Grid

        #region EditFom

        protected void txtName_OnDataBinding(object sender, EventArgs e)
        {
            AccountingObjectCharacteristicsService.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Name));
        }

        protected void txtFact_OnDataBinding(object sender, EventArgs e)
        {
            AccountingObjectCharacteristicsService.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Fact));
        }

        protected void cbxType_OnDataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(RepositoryBase<AccountingObjectCharacteristicsType>.GetAll())
                .SetValueComboBox(
                    AccountingObjectCharacteristicsService.With(x => x.CurrentEdit)
                        .With(x => x.Type)
                        .Return(x => x.Id, EntityBase.Default));
        }

        protected void cbxUnit_OnDataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(RepositoryBase<Units>.GetAll())
                .SetValueComboBox(
                    AccountingObjectCharacteristicsService.With(x => x.CurrentEdit)
                        .With(x => x.Unit)
                        .Return(x => x.Id, EntityBase.Default));
        }

        protected void deDate_OnDataBinding(object sender, EventArgs e)
        {
            AccountingObjectCharacteristicsService.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Date));
        }

        protected void txtPeriod_OnDataBinding(object sender, EventArgs e)
        {
            AccountingObjectCharacteristicsService.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Period));
        }

        #endregion EditForm


        protected void gridCharacteristics_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            gridCharacteristics.DataSource = new AccountingObjectCharacteristicsRepository()
                .GetAllByAccountingObject(AccountingObjectId);
        }

        protected void gridCharacteristics_OnRowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            AccountingObjectCharacteristicsService.Create(GetAccountingObjectCharacteristicsArgs());
            gridCharacteristics.GridCancelEdit(e);
        }

        protected void gridCharacteristics_OnRowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            AccountingObjectCharacteristicsService.Update(GetAccountingObjectCharacteristicsArgs((int)e.Keys[0]));
            gridCharacteristics.GridCancelEdit(e);
        }

        protected void gridCharacteristics_OnStartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            AccountingObjectCharacteristicsService.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void gridCharacteristics_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            AccountingObjectCharacteristicsService.Delete(new AccountingObjectCharacteristicsArgs(){Id = (int)e.Keys[0]});
            gridCharacteristics.GridCancelEdit(e);
        }

        #endregion Grid

        #endregion Control Handlers

        #endregion Handlers


        #region Helpers

        public static string GetUrl(object accountingObjectId)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/AccountingObjects/Characteristics.aspx"), EditCard.AccountingObjectIdKey, accountingObjectId);
        }

        private AccountingObjectCharacteristicsArgs GetAccountingObjectCharacteristicsArgs(int id = 0)
        {
            var result = new AccountingObjectCharacteristicsArgs()
            {
                Id = id,
                AccountingObject = CurrentAccountingObject,
                Name = hf["Name"].ToStringOrDefault(),
                Period = hf["Period"].ToStringOrDefault(),
                TypeId = hf["TypeId"].ToIntNull(),
                UnitId = hf["UnitId"].ToIntNull(),
                Fact = hf["Fact"].ToIntNull(),
                Date = hf["Date"].ToDateTimeNull(),
                IsRegister = true,
            };

            return result;
        }


        protected int? GetSumMax(object values)
        {
           var valuesList =
            values as IList<AccountingObjectCharacteristicsValue>;

            return valuesList.Return(v => v.Sum(x => x.Max), 0);
        }

        protected int? GetSumNorm(object values)
        {
            var valuesList =
             values as IList<AccountingObjectCharacteristicsValue>;

            return valuesList.Return(v => v.Sum(x => x.Norm), 0);
        }

        #endregion Helpers


    }
}