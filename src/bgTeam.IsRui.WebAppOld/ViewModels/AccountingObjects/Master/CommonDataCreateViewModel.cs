﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    [MetadataType(typeof(CommonDataCreateViewModel_Valid))]
    public class CommonDataCreateViewModel
    {
        #region Consts

        private const string ShortNameCaption = "Краткое наименование";
        private const string FullNameCaption = "Полное наименование";
        private const string KindCaption = "Классификационная категория ОУ";

        #endregion Consts

        public int Id { get; set; }

        [DisplayName(ShortNameCaption)]
        public string ShortName { get; set; }

        [DisplayName(FullNameCaption)]
        public string FullName { get; set; }

        [DisplayName(KindCaption)]
        public int? KindId { get; set; }

        public SelectList KindList { get; set; }


        public CommonDataCreateViewModel(int? id)
        {
            Id = id.GetValueOrDefault();

            KindList = new SelectList(new IKTComponentRepository().GetAllWhoNotHaveChildren(), "Id", "Name");
            
            var entity = new RepositoryDc<AccountingObject>().Get(id);
            entity.Do(Fill);
        }

        private void Fill(AccountingObject entity)
        {
            ShortName = entity.ShortName;
            FullName = entity.FullName;
            KindId = entity.Kind.Return(k => k.Id, default(int));
        }
    }

    public class CommonDataCreateViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string ShortName { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string FullName { get; set; }

        [Required(ErrorMessage = RequiredText)]
        [Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        public int? KindId { get; set; }
    }
}