﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    /// <summary>
    /// Объект запроса, содержащий наименование объекта учёта
    /// </summary>
    public class AccountingObjectCreateStoryContext
    {
        /// <summary>
        /// Полное наименование объекта учёта
        /// </summary>
        public string Name { get; set; }

        public int? ArticleId { get; set; }

        public int? DepartmentId { get; set; }

        public int? KindId { get; set; }

        public int? kindOfActivityId { get; set; }
    }
}
