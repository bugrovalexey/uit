﻿namespace bgTeam.IsRui.Common.Services
{
    using Microsoft.AspNetCore.Http;
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public interface IFileProviderRui
    {
        string GenerateFileName();

        void DeleteFile(object id);

        Task<string> SaveAsync(IFormFile file);

        void MoveToGeneric(Guid guid, int id, bool replace = false);

        Stream OpenFileToRead(int id);

        Task<byte[]> ReadFileAsync(int id);
    }
}
