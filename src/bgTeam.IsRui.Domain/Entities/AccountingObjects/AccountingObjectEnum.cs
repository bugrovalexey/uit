﻿namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    using System.ComponentModel;

    public enum AccountingObjectEnum
    {
        [Description("")]
        One = 0,

        [Description("Создано на основании согласованного")]
        Multi = 1
    }
}
