﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityGoods : EntityBase
    {
        public virtual string Name { get; set; }

        //public virtual OKPD OKPD { get; set; }

        //public virtual ProductGroup ProductGroup { get; set; }

        //public virtual ExpenseDirection ExpenseDirection { get; set; }

        //public virtual ExpenseType ExpenseType { get; set; }

        //public virtual ExpenseItem KOSGU { get; set; }

        //public virtual YearEnum Year { get; set; }

        public virtual int? Quantity { get; set; }

        //public virtual int? Duration { get; set; }

        public virtual int? Cost { get; set; }

        public virtual decimal? Summ { get; set; }

        public virtual int? PlansActivityId { get; set; }

        public virtual IList<GoodsCharacteristic> Characteristic { get; set; }

        public virtual DateTime? ChangeDate { get; set; }

        public virtual QuarterEnum Quarter { get; set; }

        public virtual string AnalogName { get; set; }

        public virtual decimal AnalogCost { get; set; }
    }
}
