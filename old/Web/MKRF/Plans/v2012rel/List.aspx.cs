﻿using DevExpress.Web.ASPxGridView;
using ICSharpCode.SharpZipLib.GZip;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Uviri.MKRF.Controllers;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Report;
using Uviri.MKRF.Repository;

namespace Uviri.MKRF.Web.Plans.v2012rel
{
    public partial class List : System.Web.UI.Page
    {
        PlanController planController = new PlanController();

        private DataTable table = null;
        private DataTable GetPlanList
        {
            get
            {
                if (table == null)
                {
                    table = planController.GetList(false, 2);
                }

                return table;
            }
        }


        protected void plansGrid_Init(object sender, EventArgs e)
        {
            var gridView = sender as ASPxGridView;
            foreach (GridViewColumn column in gridView.Columns)
            {
                var c = column as GridViewDataColumn;

                if (c != null)
                {
                    c.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                    c.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.False;
                    c.Settings.ShowFilterRowMenu = DevExpress.Utils.DefaultBoolean.False;

                    //switch (c.FieldName)
                    //{
                    //    case "StatusChangeDate":
                    //        c.Settings.ShowFilterRowMenu = DevExpress.Utils.DefaultBoolean.True;
                    //        break;
                    //    case "Status":
                    //        c.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;
                    //        break;
                    //    case "Year":
                    //        c.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;
                    //        break;
                    //        //case "ExpStatus":
                    //        //    c.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;
                    //        //break;
                    //    default:
                    //        c.Settings.ShowFilterRowMenu = DevExpress.Utils.DefaultBoolean.False;
                    //        break;
                    //}
                }
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            plansGrid.DataBind();
            
            base.OnPreRender(e);
        }

        protected void plansGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            DataTable table = null;

            //Включаем группировку для МКС и Подведомственных
            //var filterMode = hfFilterMode.Contains("filterMode") ? hfFilterMode["filterMode"].ToString() : string.Empty;
            if (!IsCallback)
            {
                plansGrid.ClearSort();
                if (UserRepository.IsMks)
                {
                    plansGrid.GroupBy(plansGrid.Columns["FieldForGrouping"]);

                    //Если подведомтсвенные, то добавляем группировку по категориям
                    //if (filterMode == "subordinate")
                    //    plansGrid.GroupBy(plansGrid.Columns["GovtOrganVedName"]);

                    //                    plansGrid.ClientSideEvents.RowClick =
                    //                        @"function(s, e) {
                    //if (plansGrid.IsGroupRowExpanded(e.visibleIndex))
                    //    plansGrid.CollapseRow(e.visibleIndex);
                    //else
                    //    plansGrid.ExpandRow(e.visibleIndex);
                    //}";
                }
            }

            if (hfCache.Contains("t") && hfCache["t"] != null)
            {
                object result = null;
                var b64 = Convert.FromBase64String((string)hfCache["t"]);
                using (var memory = new MemoryStream(b64))
                {
                    GZipInputStream gzipIn = new GZipInputStream(memory);
                    result = new BinaryFormatter().Deserialize(gzipIn);

                }
                table = (DataTable)result;
            }
            else
            {
                table = GetPlanList;

                //if (ChBExpired.Checked)
                //{
                //    table = DataTableCustomFilter(table, string.Format("DecisionDate <'{0:o}'", DateTime.Today));
                //}

                //switch (filterMode)
                //{
                //    case "departments":
                //        table = DataTableCustomFilter(table, string.Format("Govt_Organ_Type_ID ={0}", (int)DepartmentTypeEnum.Department));
                //        break;
                //    case "subordinate":
                //        table = DataTableCustomFilter(table, string.Format("Govt_Organ_Type_ID ={0} AND Govt_Organ_Ved_ID > 0", (int)DepartmentTypeEnum.Establishment));
                //        break;
                //    case "region":
                //        table = DataTableCustomFilter(table, string.Format("Govt_Organ_Type_ID ={0}", (int)DepartmentTypeEnum.Region));
                //        break;
                //    default:
                //        break;
                //}

                table.RemotingFormat = SerializationFormat.Binary;

                byte[] bytes = null;
                using (var memory = new MemoryStream())
                {
                    GZipOutputStream gzipOut = new GZipOutputStream(memory);
                    gzipOut.IsStreamOwner = false;

                    new BinaryFormatter().Serialize(gzipOut, table);

                    gzipOut.Close();

                    memory.Flush();
                    bytes = memory.ToArray();
                }

                hfCache["t"] = Convert.ToBase64String(bytes);
            }

            plansGrid.DataSource = table;
        }

        protected string GetNameLink(object id, object name, object isnew)
        {
            return string.Format("<a class='gridLink' {2} href='{0}'>{1}</a>",
                PlanCard.GetUrl((int)id, ExpObjectTypeEnum.Plan2012rel),
                name.ToString(),
                isnew.ToString().Equals("False", StringComparison.InvariantCultureIgnoreCase) ? " style='font-weight: bold' " : string.Empty);
        }

        protected string GetViewLinks(int Id)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append(ReportPage.GetLink("P 1.1", ReportType.Plan2012NewSectionOne_1, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 1.2", ReportType.Plan2012NewSectionOne_2, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 1.3", ReportType.Plan2012NewSectionOne_3, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 1.4", ReportType.Plan2012NewSectionOne_4, Id)); sb.Append(" ");

            sb.Append("<br />");

            sb.Append(ReportPage.GetLink("P 2.1", ReportType.Plan2012NewSectionTwo_1, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 2.2", ReportType.Plan2012NewSectionTwo_2, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 2.3", ReportType.Plan2012NewSectionTwo_3, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 2.4", ReportType.Plan2012NewSectionTwo_4, Id)); sb.Append(" ");

            sb.Append("<br />");

            sb.Append(ReportPage.GetLink("P 3.1", ReportType.Plan2012NewSectionThree_1, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 3.2", ReportType.Plan2012NewSectionThree_2, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 3.3", ReportType.Plan2012NewSectionThree_3, Id)); sb.Append(" ");

            sb.Append("<br />");

            sb.Append(ReportPage.GetLink("P 4.1 P 4.2", ReportType.Plan2012NewSectionFour, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 4.3", ReportType.Plan2012NewSectionFour_3, Id)); sb.Append(" ");
            sb.Append(ReportPage.GetLink("P 4.4", ReportType.Plan2012NewSectionFour_4, Id)); sb.Append(" ");

            return sb.ToString();
        }

        
    }
}