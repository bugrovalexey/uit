﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;

namespace bgTeam.IsRui.DataSetExtensions
{
    /// <summary>
    /// This static class defines the DataTable extension methods.
    /// </summary>
    public static class DataTableExtensions
    {
        /// <summary>
        /// This method takes an input sequence of DataRows and produces a DataTable object
        /// with copies of the source rows.
        /// Also note that this will cause the rest of the query to execute at this point in time
        /// (e.g. there is no more delayed execution after this sequence operator).
        /// </summary>
        /// <param name="source">The input sequence of DataRows</param>
        /// <returns>DataTable containing copies of the source DataRows. Properties for the DataTable table will be taken from first DataRow in the source.</returns>
        /// <exception cref="ArgumentNullException">if source is null</exception>
        /// <exception cref="InvalidOperationException">if source is empty</exception>
        public static DataTable CopyToDataTable<T>(this IEnumerable<T> source)
            where T : DataRow
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            return LoadTableFromEnumerable(source, table: null, options: null, errorHandler: null);
        }

        private static DataTable LoadTableFromEnumerable<T>(IEnumerable<T> source, DataTable table, LoadOption? options, FillErrorEventHandler errorHandler)
            where T : DataRow
        {
            if (options.HasValue)
            {
                switch (options.Value)
                {
                    case LoadOption.OverwriteChanges:
                    case LoadOption.PreserveChanges:
                    case LoadOption.Upsert:
                        break;
                    default:
                        throw new ArgumentException("Invalid load options", nameof(options));
                }
            }


            using (IEnumerator<T> rows = source.GetEnumerator())
            {
                // need to get first row to create table
                if (!rows.MoveNext())
                {
                    return table ?? throw new InvalidOperationException("Empty data row source");
                }

                DataRow current;
                if (table == null)
                {
                    current = rows.Current;
                    if (current == null)
                    {
                        throw new InvalidOperationException("data row is null");
                    }

                    table = new DataTable()
                    {
                        Locale = CultureInfo.CurrentCulture
                    };

                    // We do not copy the same properties that DataView.ToTable does.
                    // If user needs that functionality, use other CopyToDataTable overloads.
                    // The reasoning being, the IEnumerator<DataRow> can be sourced from
                    // different DataTable, so we just use the "Default" instead of resolving the difference.

                    foreach (DataColumn column in current.Table.Columns)
                    {
                        table.Columns.Add(column.ColumnName, column.DataType);
                    }
                }

                table.BeginLoadData();
                try
                {
                    do
                    {
                        current = rows.Current;
                        if (current == null)
                        {
                            continue;
                        }

                        object[] values = null;
                        try
                        {
                            // 'recoverable' error block
                            switch (current.RowState)
                            {
                                case DataRowState.Detached:
                                    if (!current.HasVersion(DataRowVersion.Proposed))
                                    {
                                        throw new InvalidOperationException("Cannot load detached row");
                                    }
                                    goto case DataRowState.Added;
                                case DataRowState.Unchanged:
                                case DataRowState.Added:
                                case DataRowState.Modified:
                                    values = current.ItemArray;
                                    if (options.HasValue)
                                    {
                                        table.LoadDataRow(values, options.Value);
                                    }
                                    else
                                    {
                                        table.LoadDataRow(values, fAcceptChanges: true);
                                    }
                                    break;
                                case DataRowState.Deleted:
                                    throw new InvalidOperationException("Cannot load deleted row");
                                default:
                                    throw new InvalidOperationException($"Invalid data row state {current.RowState}");
                            }
                        }
                        catch (Exception e)
                        {
                            if (!IsCatchableExceptionType(e))
                            {
                                throw;
                            }

                            FillErrorEventArgs fillError = null;
                            if (null != errorHandler)
                            {
                                fillError = new FillErrorEventArgs(table, values)
                                {
                                    Errors = e
                                };
                                errorHandler.Invoke(rows, fillError);
                            }
                            if (null == fillError)
                            {
                                throw;
                            }
                            else if (!fillError.Continue)
                            {
                                if (ReferenceEquals(fillError.Errors ?? e, e))
                                {
                                    // if user didn't change exception to throw (or set it to null)
                                    throw;
                                }
                                else
                                {
                                    // user may have changed exception to throw in handler
                                    throw fillError.Errors;
                                }
                            }
                        }
                    } while (rows.MoveNext());
                }
                finally
                {
                    table.EndLoadData();
                }
            }
            return table;
        }

        // only StackOverflowException & ThreadAbortException are sealed classes
        private static readonly Type s_stackOverflowType = typeof(StackOverflowException);
        private static readonly Type s_outOfMemoryType = typeof(OutOfMemoryException);
        private static readonly Type s_threadAbortType = typeof(System.Threading.ThreadAbortException);
        private static readonly Type s_nullReferenceType = typeof(NullReferenceException);
        private static readonly Type s_accessViolationType = typeof(AccessViolationException);
        private static readonly Type s_securityType = typeof(System.Security.SecurityException);

        static internal bool IsCatchableExceptionType(Exception e)
        {
            // a 'catchable' exception is defined by what it is not.
            Type type = e.GetType();

            return ((type != s_stackOverflowType) &&
                     (type != s_outOfMemoryType) &&
                     (type != s_threadAbortType) &&
                     (type != s_nullReferenceType) &&
                     (type != s_accessViolationType) &&
                     !s_securityType.IsAssignableFrom(type));
        }
    }
}
