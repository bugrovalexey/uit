﻿using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Documents;
using Uviri.MKRF.Entities.OtherExpObjects;
using Uviri.MKRF.Entities.Statuses;
using Uviri.MKRF.Helpers;

namespace Uviri.MKRF.Mapping.OtherExpObjects
{
    public class ExpObjectDemandMap : ClassMapBase<ExpObjectDemand>
    {
        public ExpObjectDemandMap()
            : base("Exp_Obj_Demand")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Exp_Obj_Demand_Name");
            Map(x => x.Date, "Exp_Obj_Demand_Doc_Date");
            Map(x => x.Num, "Exp_Obj_Demand_Doc_Num");
            Map(x => x.Basis, "Exp_Obj_Demand_Basis");
           // Map(x => x.SummaryConclusionId, "Exp_Obj_Summary_Conclusion_ID");
            Map(x => x.CreateDateTime, "Exp_Obj_Demand_Create_DT").ReadOnly();


            References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
            References<User>(x => x.User, "Users_ID").Not.Nullable();
            References<Status>(x => x.Status, "Status_ID").Not.Nullable();
			References<Years>(x => x.Year, "Year");
            References<Years>(x => x.YearEnd, "Year_End");
			References<ExpObjectType>(x => x.ObjectType, "Exp_Obj_Type_ID").Not.Nullable();
            References<OtherDocType>(x => x.Type, "Other_Doc_Type_ID");
            References<ExpObjectConclusion>(x => x.SummaryConclusion, "Exp_Obj_Summary_Conclusion_ID");


            HasMany<Document>(x => x.DocumentList)
                   .KeyColumns.Add("Document_Owner_ID")
                   .Where("Doc_Type_ID = " + (int)DocTypeEnum.demand_document)
                   .Where("Portal_Table_ID = " + (int)TableTypeEnum.ExpObjectDemand)
                   .Inverse();

            HasMany<Document>(x => x.DocumentInventoryList)
                    .KeyColumns.Add("Document_Owner_ID")
                    .Where("Doc_Type_ID = " + (int)DocTypeEnum.demand_inventory)
                    .Where("Portal_Table_ID = " + (int)TableTypeEnum.ExpObjectDemand)
                    .Inverse();

            //HasMany<ExpObjectConclusion>(x => x.ConclusionList)
            //        .KeyColumns.Add("Exp_Obj_Demand_ID")
            //        .Inverse()
            //        .ApplyFilter<SecurityFilter>("Exp_Obj_Conclusion_ID in (select * from Get_Object_List(:UserId, " + (int)TableTypeEnum.Exp_Conclusion + ", 'r'))");

            ApplyFilter<SecurityFilter>("Exp_Obj_Demand_ID in (select * from Get_Object_List(:UserId, " + (int)TableTypeEnum.ExpObjectDemand + ", 'r'))");
        }
    }
}