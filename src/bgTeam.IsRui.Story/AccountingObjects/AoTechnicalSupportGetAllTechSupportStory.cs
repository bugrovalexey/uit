﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoTechnicalSupportGetAllTechSupportStory : IStory<AoTechnicalSupportGetAllTechSupportStoryContext, IEnumerable<AllTechnicalSupportDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;

        public AoTechnicalSupportGetAllTechSupportStory(IMapperBase mapper, IRepositoryRui repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public IEnumerable<AllTechnicalSupportDto> Execute(AoTechnicalSupportGetAllTechSupportStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<AllTechnicalSupportDto>> ExecuteAsync(AoTechnicalSupportGetAllTechSupportStoryContext context)
        {
            var technicalSupport = _repository.GetAllEx<TechnicalSupport>(
                  q => q.Where(x => x.AccountingObject.Id == context.AoId))
                  .Select(x => _mapper.Map(x, new AllTechnicalSupportDto()));

            return technicalSupport;
        }
    }
}
