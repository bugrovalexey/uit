﻿using GB.Data.Directories;
using GB.Data.Plans;
using GB.MKRF.Helpers;
using System.Collections;
using System.Collections.Generic;

namespace GB.MKRF.Repository
{
    public class YearRepository : Repository<Years>
    {
        /// <summary>
        /// Список годов, по которым можно создавать планы
        /// </summary>
        public IEnumerable GetYearsForPlan(Plan plan)
        {
            return GetAll(x => x.Value > 2013);

            //return new NonUsedYears()
            //{
            //    YearList = YearRepository.GetAvailableForPlan(ExpObjectTypeEnum.Plan2012new)
            //                             .Where(y => y.Year >= 2013)
            //                             .ToList(),
            //    SelectedValue = CurrentEdit != null ? CurrentEdit.ExpObject.Year : null
            //};
        }

        private static Dictionary<YearEnum, string> yearList;

        public static Dictionary<YearEnum, string> YearList
        {
            get
            {
                return yearList ?? (yearList = EnumHelper.ToDictionary<YearEnum>());
            }
        }

        public static string GetYearText(YearEnum year)
        {
            string text;
            YearList.TryGetValue(year, out text);
            return text;
        }
    }
}