﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
	public class CODMap : ClassMapBase<COD>
    {
        public CODMap()
            : base("PRS_8_3")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_3_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_8_3_1 = ? 
//                          ,@PRS_8_3_2 = ? 
//                          ,@PRS_8_3_3 = ? 
//                          ,@PRS_8_3_4 = ? 
//                          ,@PRS_8_3_5 = ? 
//                          ,@PRS_8_3_6 = ? 
//                          ,@PRS_8_3_7 = ? 
//                          ,@PRS_8_3_8 = ? 
//                          ,@PRS_8_3_9 = ? 
//                          ,@PRS_8_3_10 = ? 
//                          ,@PRS_8_3_11 = ? 
//                          ,@PRS_8_3_12 = ? 
//                          ,@PRS_8_3_13 = ? 
//                          ,@PRS_8_3_14 = ? 
//                          ,@Plans_ID = ? 
//						  ,@PRS_8_3_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_3_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_3_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_3_Delete @PRS_8_3_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_8_3_1");
			Map(x => x.CODName, "PRS_8_3_2");
			Map(x => x.CapacitySummary, "PRS_8_3_3");
			Map(x => x.ChanelCount, "PRS_8_3_4");
			Map(x => x.CabinetCount, "PRS_8_3_5");
			Map(x => x.UnitSummary, "PRS_8_3_6");
			Map(x => x.UnitUsed, "PRS_8_3_7");
			Map(x => x.ServerCount, "PRS_8_3_8");
			Map(x => x.HardwareCount, "PRS_8_3_9");
			Map(x => x.SecurityCount, "PRS_8_3_10");
			Map(x => x.SHDCount, "PRS_8_3_11");
			Map(x => x.OtherHDCount, "PRS_8_3_12");
			Map(x => x.UPSCount, "PRS_8_3_13");
			Map(x => x.UPSPower, "PRS_8_3_14");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
        }
    }
}