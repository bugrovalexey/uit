﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
    public class SecurityMap : ClassMapBase<Security>
    {
        public SecurityMap()
            : base("PRS_8_5")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_5_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_8_5_1 = ? 
//                          ,@PRS_8_5_2 = ? 
//                          ,@PRS_8_5_4 = ? 
//                          ,@PRS_8_5_5 = ? 
//                          ,@PRS_8_5_6 = ? 
//                          ,@PRS_8_5_7 = ? 
//                          ,@PRS_8_5_8 = ? 
//                          ,@PRS_8_5_9 = ? 
//                          ,@Plans_ID = ? 
//                          ,@PRS_8_5_Type_ID = ? 
//						  ,@PRS_8_5_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_5_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_5_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_5_Delete @PRS_8_5_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_8_5_1");
			Map(x => x.Name, "PRS_8_5_2");
			Map(x => x.Developer, "PRS_8_5_4");
			Map(x => x.Count, "PRS_8_5_5");
			Map(x => x.CertGovN, "PRS_8_5_6");
			Map(x => x.CertGovTime, "PRS_8_5_7");
			Map(x => x.CertFSBN, "PRS_8_5_8");
			Map(x => x.CertFSBTime, "PRS_8_5_9");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
			References<SecurityType>(x => x.Type, "PRS_8_5_Type_ID");
        }
    }
}