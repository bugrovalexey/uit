﻿namespace bgTeam.IsRui.WebApp.Admin
{
    using bgTeam.IsRui.WebApp.Admin.Controllers;
    using bgTeam.Core;
    using bgTeam.Core.Impl;
    using bgTeam.DataAccess;
    using bgTeam.DataAccess.Impl.Dapper;
    using bgTeam.DataAccess.Impl.MsSql;
    using bgTeam.Impl;
    using bgTeam.Infrastructure;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using System.Reflection;
    using bgTeam.IsRui.DataAccess.Repository;

    public static class DIContainer
    {
        public static void Configure(IServiceCollection services)
        {
            services.Scan(scan => scan
                .FromExecutingAssembly()
                .AddClasses(classes => classes.AssignableTo(typeof(IStory<,>)))
                .AsImplementedInterfaces()
                .WithTransientLifetime());

            services.Scan(scan => scan
                .FromExecutingAssembly()
                .AddClasses(classes => classes.AssignableTo<Controller>())
                .AsImplementedInterfaces()
                .WithTransientLifetime());

            var storyfactory = new StoryFactory(services);

            services.TryAddSingleton<IAppConfiguration, AppConfigurationDefault>();
            services.TryAddSingleton<IAppLogger, AppLoggerDefault>();
            services.TryAddSingleton<IStoryFactory>(storyfactory);
            services.TryAddSingleton<IStoryBuilder, StoryBuilder>();
            services.TryAddSingleton<IConnectionSetting, AppSettings>();
            services.TryAddSingleton<IConnectionFactory, ConnectionFactoryMsSql>();
            services.TryAddTransient<ICrudService, CrudServiceDapper>();
            services.TryAddTransient<IRepository, RepositoryDapper>();
            services.TryAddTransient<IBuilderQuery, BuilderQueryMsSql>();
            services.TryAddTransient<IRepositoryRui, Repository>();
        }
    }
}
