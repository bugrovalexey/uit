﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans.ActivityOffers
{
    public class ActivityOfferIS : EntityBase
    {
        /// <summary>ФГИС</summary>
        public virtual FgisInfo FGIS { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Признак</summary>
        public virtual string Indication { get; set; }

        /// <summary>ИС или ИКТ объект</summary>
        public virtual IKTComponent IKT { get; set; }

        /// <summary>
        /// ИС/ИКТ при отсутствии во ФГИС
        /// </summary>
        public virtual string FGIS_Text { get; set; }

        /// <summary>
        /// Мероприятие плана
        /// </summary>
        public virtual ActivityOffer Offer { get; set; }
    }
}
