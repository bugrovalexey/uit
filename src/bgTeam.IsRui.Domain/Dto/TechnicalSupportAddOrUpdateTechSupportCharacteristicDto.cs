﻿namespace bgTeam.IsRui.Domain.Dto
{
    public class TechnicalSupportAddOrUpdateTechSupportCharacteristicDto
    {
        public int? Id { get; set; }

        /// <summary>
        /// Техническое обеспечение ОУ
        /// </summary>
        public int? TechnicalSupportId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public int UnitId { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        public double? Value { get; set; }
    }
}
