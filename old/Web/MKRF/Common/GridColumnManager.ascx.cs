﻿using System;
using System.Text;
using System.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTreeList;

namespace GB.MKRF.Web.Common
{
    public partial class GridColumnManager : System.Web.UI.UserControl
    {
        public const string CommandClearCookies = "ClearCookies";

        public string UserStyle { get; set; }

        public string GridName
        {
            get
            {
                return _hfGridName.Get("GridName") as string;
            }
            set
            {
                _hfGridName.Set("GridName", value);
            }
        }


        public ASPxGridView Grid { get; set; }

        public string OnPreClickClearButtom { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Grid != null)
            {
                Grid.ClientLayout += new DevExpress.Web.ASPxClasses.ASPxClientLayoutHandler(Grid_ClientLayout);
                Grid.CustomCallback += new ASPxGridViewCustomCallbackEventHandler(Grid_CustomCallback);

                Grid.SettingsCookies.Enabled = true;
                Grid.SettingsCustomizationWindow.Enabled = true;
                Grid.SettingsCustomizationWindow.PopupHorizontalAlign = DevExpress.Web.ASPxClasses.PopupHorizontalAlign.Center;
                Grid.SettingsCustomizationWindow.PopupVerticalAlign = DevExpress.Web.ASPxClasses.PopupVerticalAlign.WindowCenter;
            }
        }

        void Grid_ClientLayout(object sender, DevExpress.Web.ASPxClasses.ASPxClientLayoutArgs e)
        {
            if (e.LayoutMode == DevExpress.Web.ASPxClasses.ClientLayoutMode.Loading)
            {
                ASPxGridView grid = sender as ASPxGridView;

                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("page1");

                if (grid.GroupCount > 0)
                {
                    sb.AppendFormat("|group{0}", grid.GroupCount);
                    sb.AppendFormat("|sort{0}", grid.SortCount);

                    var groups = grid.GetGroupedColumns();

                    int i = 0;
                    foreach (GridViewColumn column in grid.AllColumns)
                    {
                        if (groups.Contains(column))
                        {
                            sb.AppendFormat("|a{0}", i);
                        }
                        i++;
                    }
                }

                sb.AppendFormat("|visible{0}", grid.AllColumns.Count);

                foreach (GridViewColumn column in grid.AllColumns)
                {
                    sb.AppendFormat("|{0}{1}", column.Visible ? 't' : 'f', column.VisibleIndex);
                }

                _hfGridName.Set("DefSetting", sb.ToString());
            }
        }

        void Grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (string.Compare(e.Parameters, Web.Common.GridColumnManager.CommandClearCookies) == 0)
            {
                (sender as ASPxGridView).LoadClientLayout(_hfGridName["DefSetting"] as string);
            }
        }
    }
}