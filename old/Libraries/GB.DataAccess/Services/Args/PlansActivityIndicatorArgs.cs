﻿namespace GB.DataAccess.Services.Args
{
    public class PlansActivityIndicatorArgs : BaseIndicatorArgs
    {
        /// <summary>
        /// Алгоритм расчета
        /// </summary>
        public string AlgorithmOfCalculating { get; set; }
    }
}