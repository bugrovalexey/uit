﻿using System;
using System.Collections.Generic;
using System.Data;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;

namespace GB.MKRF.Web.Monitoring.v2012new
{
    public partial class ExpenseStructure : System.Web.UI.Page
    {
        const int YearMin = 1900;

        private int currentYear = 0;
        protected int CurrentYear
        {
            get
            {
                if (currentYear == 0)
                {
                 
                    var ys = Request.QueryString["Year"];
                    int yi = 0;
                    if (!string.IsNullOrWhiteSpace(ys) && int.TryParse(ys, out yi))
                    {
                        currentYear = yi;
                    }
                    else
                    {
                        currentYear = DateTime.Now.Year;
                    }
                }
                return currentYear;
            }
        }

        //private int currentDepType = 0;
        //protected int CurrentDepType
        //{
        //    get
        //    {
        //        if (currentDepType == 0)
        //        {

        //            var ds = Request.QueryString["DepType"];
        //            int di = 0;
        //            if (!string.IsNullOrWhiteSpace(ds) && int.TryParse(ds, out di))
        //            {
        //                currentDepType = di;
        //            }
        //            else
        //            {
        //                currentDepType = 0;
        //            }
        //        }
        //        return currentDepType;
        //    }
        //}


        private DataTable _data = null;
        private DataTable Data
        {
            get
            {
                if (_data == null)
                {
                    //_data = MonitoringRepository.GetExpenseStructure(CurrentYear, CurrentDepType);
                    _data = MonitoringRepository.GetExpenseStructure(CurrentYear, 0);
                }

                return _data;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            YearsComboBox.DataBind();
           // OGVTypeComboBox.Value = CurrentDepType;
            ChartDataBinding();
            grid.DataBind();
        }   

        protected void ChartCallbackPanel_OnCallback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            ChartDataBinding();
        }

        private void ChartDataBinding()
        {
            double[] sum = new double[6];

            foreach(DataRow row in Data.Rows)
            {
                sum[0] += Convert.ToDouble(row["Y0"]);
                sum[1] += Convert.ToDouble(row["Y1"]);
                sum[2] += Convert.ToDouble(row["Y2"]);
                sum[3] += Convert.ToDouble(row["AddY0"]);
                sum[4] += Convert.ToDouble(row["AddY1"]);
                sum[5] += Convert.ToDouble(row["AddY2"]);
            }

            Chart.Series.Clear();

            DateTime currentYearDT = new DateTime(CurrentYear, 1, 1);

            int i = Chart.Series.Add("Основной объем", ViewType.Bar);
            Chart.Series[i].LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            Chart.Series[i].Label.ResolveOverlappingMode = ResolveOverlappingMode.HideOverlapped;
            Chart.Series[i].Points.Clear();
            Chart.Series[i].Points.Add(new SeriesPoint(currentYearDT, sum[0]));
            Chart.Series[i].Points.Add(new SeriesPoint(currentYearDT.AddYears(1), sum[1]));
            Chart.Series[i].Points.Add(new SeriesPoint(currentYearDT.AddYears(2), sum[2]));


            i = Chart.Series.Add("Дополнительный объем", ViewType.Bar);
            Chart.Series[i].LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            Chart.Series[i].Label.ResolveOverlappingMode = ResolveOverlappingMode.HideOverlapped;
            Chart.Series[i].Points.Add(new SeriesPoint(currentYearDT, sum[3]));
            Chart.Series[i].Points.Add(new SeriesPoint(currentYearDT.AddYears(1), sum[4]));
            Chart.Series[i].Points.Add(new SeriesPoint(currentYearDT.AddYears(2), sum[5]));

            if (Chart.Diagram != null)
            {
                XYDiagram diagram = Chart.Diagram as XYDiagram;

                diagram.AxisX.DateTimeScaleOptions.GridAlignment = DateTimeGridAlignment.Year;
                diagram.AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Year;
                diagram.AxisX.Label.DateTimeOptions.Format = DateTimeFormat.Custom;
                diagram.AxisX.Label.DateTimeOptions.FormatString = "yyyy";
            }
        }

       

     

        protected void YearsComboBox_DataBinding(object sender, System.EventArgs e)
        {
            IList<Years> data = RepositoryBase<Years>.GetAll(x => x.Value >= 1900);
			ASPxComboBox box = (ASPxComboBox)sender;
			box.DataSource = data;
			box.Value = CurrentYear;
        }

        protected void grid_DataBinding(object sender, EventArgs e)
        {
            grid.DataSource = this.Data;
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            grid.Columns["Y0"].Caption = grid.Columns["AddY0"].Caption = currentYear.ToString();
            grid.Columns["Y1"].Caption = grid.Columns["AddY1"].Caption = (currentYear + 1).ToString();
            grid.Columns["Y2"].Caption = grid.Columns["AddY2"].Caption = (currentYear + 2).ToString();
        }

        protected void Chart_CustomDrawSeriesPoint(object sender, CustomDrawSeriesPointEventArgs e)
        {
            e.LabelText = Convert.ToDouble(e.LabelText).ToString("#,##0.0000");
        }


    }
}