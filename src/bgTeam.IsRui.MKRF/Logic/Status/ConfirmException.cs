﻿using System;

namespace bgTeam.IsRui.MKRF.Logic.Status
{
    public class OperationConfirmException : Exception
    {
        public OperationConfirmException(string message)
            : base(message)
        {
        }
    }
}