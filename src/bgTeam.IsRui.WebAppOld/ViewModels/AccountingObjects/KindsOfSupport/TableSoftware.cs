﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.WebApp.Helpers;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport
{
    public class TableSoftware : NotificationBaseViewModel
    {
        #region consts

        public const string NameCaption = "Наименование";
        public const string CategoryKindOfSupportCaption = "Вид программного обеспечения";
        public const string ManufacturerCaption = "Производитель";
        public const string AmountCaption = "Количество";
        public const string SummaCaption = "Стоимость";
        public const string RightCaption = "Права на ПО";

        #endregion consts

        public TableSoftware(Software entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            CategoryKindOfSupportName = entity.CategoryKindOfSupport.Return(cat => cat.Name, default(string));
            ManufacturerName = entity.Manufacturer.Return(man => man.Name, default(string));
            Amount = entity.Amount;
            Summa = entity.Summa.ToMoneyString();
            Right = EnumHelper.GetDescription(entity.Right);

            entity.CharacteristicsOfLicense.Do(FillLicenseData);
        }


        private void FillLicenseData(CharacteristicOfLicense license)
        {
            ModelOfLicense = license.Model;
            TypeOfConnectionToServer = license.TypeOfConnectionToServer.Return<TypeOfConnectionToServerEnum, string>(x => x.GetDescription(), default(string));
            NumberOnlineSameTime = license.NumberOnlineSameTime;
            ProductNumberOfLicense = license.ProductNumber;
            NumberAllocatedSameTime = license.NumberAllocatedSameTime;
            Licensor = license.Licensor;
        }

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(CategoryKindOfSupportCaption)]
        public string CategoryKindOfSupportName { get; set; }

        [DisplayName(ManufacturerCaption)]
        public string ManufacturerName { get; set; }

        [DisplayName(AmountCaption)]
        public int? Amount { get; set; }

        [DisplayName(SummaCaption)]
        public string Summa { get; set; }

        [DisplayName(RightCaption)]
        public string Right { get; set; }

        #region License

        public string ModelOfLicense { get; set; }

        public string TypeOfConnectionToServer { get; set; }

        public int? NumberOnlineSameTime { get; set; }

        public string ProductNumberOfLicense { get; set; }

        public int? NumberAllocatedSameTime { get; set; }

        public string Licensor { get; set; }

        #endregion License

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteSoftware", "AccountingObjects", new { id = Id }, "GridSoftwares");
            }
        }
    }
}