﻿namespace bgTeam.IsRui.Web
{
    using Microsoft.AspNetCore.Http;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class XcsrfProtectionMiddleware
    {
        private readonly IAppLogger _logger;
        private readonly RequestDelegate _next;

        public XcsrfProtectionMiddleware(RequestDelegate next, IAppLogger logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var path = context.Request.Path.Value;

            await _next(context);
        }
    }
}
