﻿using System;

namespace GB.DataAccess.Services.Args
{
    public class AODecisionToCreateArgs : BaseArgsNew
    {
        /// <summary>
        /// Id объекта учета
        /// </summary>
        public int AccountingObjectId { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Дата принятия документа
        /// </summary>
        public DateTime? DateAdoption { get; set; }

        /// <summary>
        /// Пункт, статья документа
        /// </summary>
        public string ParticleOfDocument { get; set; }

        /// <summary>
        /// URL на документ
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public string Files { get; set; }
    }
}