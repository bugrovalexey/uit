﻿namespace bgTeam.IsRui.WebApp.Helpers
{
    /// <summary>
    /// Содержит различные константы, которые нужны в приложении
    /// Осторожно!!! При использовании const в других сборках, нужен ребилд!
    /// </summary>
    public static class ConstatntHelper
    {
        public const string UrlRegEx = @"((([A-Za-z]{3,9}:\/\/)(?:[-;:&=\+\$,\w]+@)?[:A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)";
    }
}