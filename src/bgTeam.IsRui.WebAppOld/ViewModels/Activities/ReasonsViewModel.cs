﻿using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.Extensions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public class ReasonsViewModel : ActivitiesCardBase
    {
        private const string StateProgramCaption = "Государственная программа";
        private const string StateSubProgramCaption = "Подпрограмма государственной программы";
        private const string StateMainEventCaption = "Основное мероприятие государственной программы";

        public const string EditActionLink = "ReasonsSingleEdit";

        #region StateProgram

        public IEntityName StateProgram { get; set; }

        public SelectList StateProgramList { get; set; }

        [DisplayName(StateProgramCaption)]
        public int StateProgramId
        {
            get { return StateProgram.Return(x => x.Id, default(int)); }
        }

        #endregion StateProgram

        #region StateSubProgram

        public IEntityName StateSubProgram { get; set; }

        public SelectList StateSubProgramList { get; set; }

        [DisplayName(StateSubProgramCaption)]
        public int StateSubProgramId
        {
            get { return StateSubProgram.Return(x => x.Id, default(int)); }
        }

        #endregion StateSubProgram

        #region StateMainEvent

        public IEntityName StateMainEvent { get; set; }

        public SelectList StateMainEventList { get; set; }

        [DisplayName(StateMainEventCaption)]
        public int StateMainEventId
        {
            get { return StateMainEvent.Return(x => x.Id, default(int)); }
        }

        #endregion StateMainEvent

        public IEnumerable<TableOtherReason> OtherReasons { get; private set; }

        public ReasonsViewModel(IStatusRepository statusRepository, int id)
            : base(statusRepository, id)
        {
            StateProgram = CurrentActivity.StateProgram;
            StateSubProgram = CurrentActivity.StateSubProgram;
            StateMainEvent = CurrentActivity.StateMainEvent;

            OtherReasons = new RepositoryDc<Reason>()
                .GetAllEx(q => q.Where(x => x.Owner == CurrentActivity))
                .Select(x => new TableOtherReason(x));

            var repo = new RepositoryDc<StateProgram>();

            StateProgramList = new SelectList(
                repo.GetAllEx(q => q.Where(x => x.Id == EntityBase.Default
                                                || (x.Parent == null || x.Parent.Id == 0))), //первый уровень
                "Id", "Name",
                StateProgramId);

            StateSubProgramList = new SelectList(
                repo.GetAllEx(q => q.Where(x => x.Id == EntityBase.Default
                                                || (x.Parent != null && x.Parent == StateProgram))), //второй уровень
                "Id", "Name",
                StateSubProgramId);

            StateMainEventList = new SelectList(
                repo.GetAllEx(q => q.Where(x => x.Id == EntityBase.Default
                                                || (x.Parent != null && x.Parent == StateSubProgram))), //третий уровень
                "Id", "Name",
                StateMainEventId);
        }
    }

    public class ActivityReasonsArgs : IPlansActivityReasonsArgs
    {
        public int Id { get; set; }

        public int StateProgram { get; set; }

        public int StateSubProgram { get; set; }

        public int StateMainEvent { get; set; }
    }
}