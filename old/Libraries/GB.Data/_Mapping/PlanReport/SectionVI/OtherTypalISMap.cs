﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.PlanReport.SectionVI;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVI
{
    public class OtherTypalISMap : ClassMapBase<OtherTypalIS>
	{
        public OtherTypalISMap()
            : base("PRS_6b")
		{
//            Table("w_");

//            Id(x => x.Id, "PRS_6b_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string args = @" @PRS_6b_Num = ? 
//							,@PRS_6b_Name = ? 
//							,@PRS_6b_Task = ? 
//							,@PRS_6b_User_Cnt = ? 
//							,@PRS_6b_Department = ? 
//							,@PRS_6b_Soft = ? 
//							,@PRS_6b_Executor = ? 
//							,@Plans_ID = ? 
//							,@PRS_6b_Year = ? 
//							,@PRS_6_Support_ID = ? 
//							,@PRS_6b_ID = ? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_6b_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_6b_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_6b_Delete
//						  @PRS_6b_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_6b_Num");
			Map(x => x.Name, "PRS_6b_Name");
			Map(x => x.Tasks, "PRS_6b_Task");
			Map(x => x.UsersCount, "PRS_6b_User_Cnt");
			Map(x => x.Unit, "PRS_6b_Department");
			Map(x => x.Software, "PRS_6b_Soft");
			Map(x => x.Executor, "PRS_6b_Executor");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
			References<Years>(x => x.Year, "PRS_6b_Year");
			References<TechSupport>(x => x.TechSupport, "PRS_6_Support_ID");
        }
    }
}