﻿namespace GB.Data.Directories
{
    /// <summary>Таблица системы</summary>
    public class PortalTable : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Наименование по-русски</summary>
        public virtual string RusName { get; set; }
    }
}