﻿using GB.Data.Directories;
using System.Collections.Generic;

namespace GB.MKRF.Repository
{
    public class StateProgramMarkRepository : Repository<StateProgramMark>
    {
        public IEnumerable<StateProgramMark> GetAllMarksByStateProgram(int stateProgramId)
        {
            var programs = new Repository<StateProgram>().GetAllEx(
                q =>
                    q.Where(sp => sp.Id == stateProgramId));

            var marks = new List<StateProgramMark>();
            GetAllMarks(programs, marks);

            return marks;
        }

        private void GetAllMarks(IEnumerable<StateProgram> programs, List<StateProgramMark> marks)
        {
            foreach (var program in programs)
            {
                marks.AddRange(program.Marks);
                GetAllMarks(program.Children, marks);
            }
        }
    }
}