﻿using System;
using System.Collections.Generic;
using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Statuses;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>Экспертное заключение</summary>
    public class ExpConclusionVersion : EntityBase //  ExpConclusionBase
    {
        protected ExpConclusionVersion()
        {
        }


        /// <summary>
        /// Акты
        /// </summary>
        public virtual IList<ExpConclAct> Acts
        {
            get;
            protected set;
        }

        /// <summary></summary>
        public virtual string ExpConclusionNum1 { get; set; }//null

        /// <summary></summary>
        public virtual string ExpConclusionNum2 { get; set; }//null

        /// <summary></summary>
        public virtual string ExpConclusionNum3 { get; set; }//null

        /// <summary></summary>
        public virtual string ExpConclusionNum4 { get; set; }//null

        /// <summary></summary>
        public virtual string ExpConclusionNum5 { get; set; }//null

        /// <summary>Заявка на экспертизу</summary>
        public virtual ExpDemand Demand { get; protected set; }

        /// <summary>Пользователь</summary>
        public virtual User User { get; protected set; }//null

        /// <summary>Cтатус</summary>
        public virtual Status Status { get; set; } // TODO: Статус вынести в отдельную сущность (статус + коммент)

        /// <summary>Достаточность</summary>
        public virtual ExpDocSufficiency ExpDocSufficiency { get; set; }

        /// <summary>Соответствие</summary>
        public virtual ExpDocEquivalent ExpDocEquivalent { get; set; }

        /// <summary></summary>
        public virtual string CustInfo { get; set; }//null

        /// <summary></summary>
        public virtual DateTime? GetDate { get; set; }//null

        /// <summary>Признак наличия конфликта</summary>
        public virtual bool Conflict { get; set; }

        /// <summary>Рекомендации</summary>
        public virtual string Recom { get; set; }//null

        /// <summary></summary>
        public virtual string Content { get; set; }//null

        /// <summary>Признак публичности</summary>
        public virtual bool IsPublic { get; set; }

        /// <summary>Решение</summary>
        public virtual string Decision { get; set; }//null

        /// <summary>Признак активности (участие заключения и эксперта в экспертизе)</summary>
      //  public virtual bool IsActive { get; set; }

                /// <summary>
        /// Предложение по доработке
        /// </summary>
        public virtual string Completion { get; set; }

        /// <summary>
        /// Итоговая оценка документа по информатизации
        /// </summary>
        public virtual Valuation Valuation { get; set; }

        /// <summary>
        /// Наименования государственных органов, с которыми необходимо согласовать документ по информатизации
        /// </summary>
        public virtual string Agreement { get; set; }

        /// <summary>
        /// Состав материалов, прилагаемый к документу по информатизации
        /// </summary>
        public virtual string Additional { get; set; }

        /// <summary>
        /// Основания проведения оценки по информатизации
        /// </summary>
        public virtual string Reason { get; set; }
    }
}
