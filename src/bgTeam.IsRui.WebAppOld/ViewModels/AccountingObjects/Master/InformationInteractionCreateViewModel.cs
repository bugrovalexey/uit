﻿using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using System.Collections.Generic;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class InformationInteractionCreateViewModel : StepBaseViewModel
    {
        public InformationInteractionCreateViewModel(int id)
            : base(id, MasterItemEnum.InformationInteractionCreate, x => x.ExternalIsAndComponentsItki)
        {
            InternalIsAndComponentsItki =
                new RepositoryDc<InternalIsAndComponentsItki>().GetAllEx(
                    q =>
                        q.Fetch(x => x.Object).Eager
                        .Fetch(x => x.Object.ResponsibleDepartment).Eager
                        .Fetch(x => x.Object.Kind).Eager
                        .Where(x => x.Owner.Id == id)).Select(x => new TableInternalIsAndComponentItki(x));

            ExternalIsAndComponentsItki = Current.ExternalIsAndComponentsItki.Select(x => new TableExternalIsAndComponentItki(x));

            ExternalUserInterfacesOfIs =
                new RepositoryDc<ExternalUserInterfaceOfIs>().GetAllEx(
                    q => q.Fetch(x => x.TypeOfInterface).Eager.Where(x => x.Owner.Id == id))
                    .Select(x => new TableExternalUserInterfaceOfIs(x));
        }

        public IEnumerable<TableInternalIsAndComponentItki> InternalIsAndComponentsItki { get; set; }

        public IEnumerable<TableExternalIsAndComponentItki> ExternalIsAndComponentsItki { get; set; }

        public IEnumerable<TableExternalUserInterfaceOfIs> ExternalUserInterfacesOfIs { get; set; }

    }
}