﻿namespace bgTeam.AdminDB.Web.Domain
{
    public enum MetaColumnTypeEnum
    {
        TString,
        TBigString,
        TInt,
        TDecimal,
        TRef,
        TChild,
        TBool,
        TDateTime
    }
}