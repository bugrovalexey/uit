﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class FgisInfoMap : ClassMapBase<FgisInfo>
    {
        public FgisInfoMap()
            : base("FGIS_Info")
        {
            //Table("w_");

            //Id(x => x.Id, "FGIS_Info_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //SqlInsert(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlUpdate(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlDelete(@"raiserror ('Unsupported',10,1)").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "FGIS_Info_Num").Nullable();
            Map(x => x.Name, "FGIS_Info_Name").Nullable();
            Map(x => x.OriginId, "FGIS_Info_Orig_ID").Nullable();

            References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
        }
    }
}