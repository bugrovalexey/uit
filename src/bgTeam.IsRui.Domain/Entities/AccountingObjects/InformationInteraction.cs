﻿using bgTeam.IsRui.Domain.Entities.Plans;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    /// <summary>
    /// Информационное взаимодействие
    /// </summary>
    public class InformationInteraction : EntityBase
    {
        /// <summary>
        /// Объект учета
        /// </summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Мероприятие
        /// </summary>
        public virtual PlansActivity PlansActivity { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        public virtual string Purpose { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public virtual InformationInteractionType Type { get; set; }

        /// <summary>
        /// СМЭВ
        /// </summary>
        public virtual string SMEV { get; set; }

        /// <summary>
        /// Заголовок
        /// </summary>
        public virtual string Title { get; set; }

        /// <summary>
        /// URL
        /// </summary>
        public virtual string Url { get; set; }
    }
}