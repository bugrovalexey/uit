﻿namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Plans;

    internal class ActivityDocumentsMap : ClassMapHiLo<ActivityDocuments>
    {
        public ActivityDocumentsMap()
       : base("Plans_Activity_Docs") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Docs_Name");
            Map(x => x.Number, "Plans_Activity_Docs_Number");
            Map(x => x.DateAdoption, "Plans_Activity_Docs_Date_Adoption");
            Map(x => x.ParticleOfDocument, "Plans_Activity_Docs_ParticleOfDocument");
            Map(x => x.FileId, "OU_Acts_FileId");
            Map(x => x.FileName, "OU_Acts_FileName");
            Map(x => x.ChangeDate, "TimeStamp");

            References(x => x.Activity, "Plans_Activity_ID").Not.Nullable();
        }
    }
}
