﻿using Uviri.MKRF.Entities.Prognosis;
using Uviri.MKRF.Entities.Prognosis.SectionIII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionIII
{
    public class SecurityInfoMap : ClassMapBase<SecurityInfo>
	{
        public SecurityInfoMap()
            : base("MF3_15")
        {
//            Table("w_");

//            Id(x => x.Id, "MF3_15_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF3_15_Insert
//                          @MF3_15_Sub_Item = ?
//                         ,@MF3_15_Num = ?
//                         ,@MF3_15_Name = ?
//                         ,@MF3_15_Maker = ?
//						 ,@MF3_15_Amount = ?
//						 ,@Mon_Form_ID = ?
//                         ,@MF3_15_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF3_15_Update
//                          @MF3_15_Sub_Item = ?
//                         ,@MF3_15_Num = ?
//                         ,@MF3_15_Name = ?
//                         ,@MF3_15_Maker = ?
//						 ,@MF3_15_Amount = ?
//						 ,@Mon_Form_ID = ?
//                         ,@MF3_15_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF3_15_Delete
//						  @MF3_15_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.SubItem, "MF3_15_Sub_Item").CustomType(typeof(SubItemEnum));

			Map(x => x.Num, "MF3_15_Num");
			Map(x => x.Name, "MF3_15_Name");
			Map(x => x.Maker, "MF3_15_Maker");
			Map(x => x.Count, "MF3_15_Amount");

			References<Prognos>(x => x.Prognos, "Mon_Form_ID").Not.Nullable();
        }
    }
}