﻿namespace GB.MKRF.Logic.MasterEdit
{
    public enum MasterItemEnum
    {
        None,
        /// <summary>Карточка для чтения</summary>
        CardForRead,


        DocumentBase,
        GoalsAndPurpose,
        ResponsibleService,
        SpecialActivity,

        /// <summary>Cведения о мероприятие по информатизации направленном на создание данного ОУ</summary>
        ActivityCreate,
        TechnicalSupport,
        Software,
        WorkAndService,
        CharacteristicsCreate,
        InformationAboutSecurity,
        InformationInteractionCreate,



        /// <summary>Должностное лицо, ответственное за организацию закупок</summary>
        ResponsiblePurchase,
        /// <summary>Cведения о заключенных государственных контрактах</summary>
        GovernmentContract,
        /// <summary>Cведения о результатах исполнения государственных контрактов</summary>
        ActsOfAcceptance,
        /// <summary>Фактическое месторасположение объекта учета</summary>
        FactualLocation,



        /// <summary>Cведения о вводе в эксплуатацию объекта учета</summary>
        Commissioning,
        /// <summary>Cведения о принятии объекта учета к бюджетному учету</summary>
        AdoptionBudget,
        /// <summary>Должностное лицо, ответственное за эксплуатацию объекта учета</summary>
        ResponsibleExploitation,
        /// <summary>Cведения о выводе из эксплуатации объекта учета</summary>
        Decommissioning,
        /// <summary>Классификационная категория ОУ</summary>
        KindMasterEdit,
        /// <summary>Закупки</summary>
        Purchase,
        /// <summary>Список мероприятий</summary>
        ActivityList,
        /// <summary>Причины вывода из эксплуатации</summary>
        ReasonsDecommissioning,
    }
}
