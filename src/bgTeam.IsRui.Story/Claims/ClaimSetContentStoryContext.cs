﻿namespace bgTeam.IsRui.Story.Claims
{
    public class ClaimSetContentStoryContext
    {
        public int? ClaimId { get; set; }

        public string Content { get; set; }
    }
}
