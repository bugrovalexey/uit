﻿namespace bgTeam.IsRui.Story.Admin
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Users;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UserGetUserInfoStory : IStory<UserGetUserInfoStoryContext, UsersDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public UserGetUserInfoStory(IRepositoryRui repository, IMapperBase mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public UsersDto Execute(UserGetUserInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<UsersDto> ExecuteAsync(UserGetUserInfoStoryContext context)
        {
            var user = _repository.Get<UsersEntity>(context.Id);

            return _mapper.Map(user, new UsersDto());
        }
    }
}
