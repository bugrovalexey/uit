﻿namespace bgTeam.IsRui.Story.Activities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CommonInfoUpdateInfoStoryContext
    {
        public int? ActivityId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int? ResponsibleDepartmentId { get; set; }

        //public int? ResponsibleInformationId { get; set; }

        public int? SigningId { get; set; }
    }
}
