﻿namespace bgTeam.IsRui.Domain.Entities.Common
{
    /// <summary>
    /// Настройки
    /// </summary>
    public enum OptionsEnum
    {
        ///// <summary>
        ///// Признак продуктива
        ///// </summary>
        //SystemIsProductive = 0,

        ///// <summary>Имя SMTP сервера</summary>
        //SmtpServer = 1001,
        ///// <summary>SMTP порт</summary>
        //SmtpPort = 1002,
        ///// <summary>Логин</summary>
        //SmtpLogin = 1003,
        ///// <summary>Пароль</summary>
        //SmtpPass = 1004,
        ///// <summary>Использовать SSL</summary>
        //SmtpUseSSL = 1005,
        ///// <summary>Белый список почтовых адресов</summary>
        //SmtpWhiteList = 1006,
        ///// <summary>Периодичность рассылки</summary>
        //SmtpTimeout = 1007,
        ///// <summary>Пользователь от имени которого запущен робот</summary>
        //SmtpRobotLogin = 1008,
        ///// <summary>email пользователей, получающих все письма системы</summary>
        //SmtpObservers = 1009,

        ///// <summary>Специалист</summary>
        //TechSpecialistName = 2001,
        ///// <summary>Телефон cпециалиста</summary>
        //TechSpecialistPhone = 2002,
        ///// <summary>Почта cпециалиста</summary>
        //TechSpecialistEmail = 2003,
        ///// <summary>Пользователи, получающие обращения в техподдержку</summary>
        //TechSupportObservers = 2004,

        ///// <summary>Кому оповещать</summary>
        //ConflictEmailTo = 3001,
        ///// <summary>Включить оповещение о ходе экспертизы</summary>
        //ConflictEnableNotification = 3002,

        ///// <summary>Периодичность индексации</summary>
        //IndexerTimeout = 4001,
        ///// <summary>Дата последнего документа</summary>
        //IndexerLastDocumentDate = 4002,

        ///// <summary>Карточка АИС Учёта</summary>
        //RegistryCard = 5001,
        ///// <summary>Карточка АИС Учёта для создания нового паспорта</summary>
        //RegistryCardNew = 5002,
        ///// <summary>Интеграция с АИС Учета включена</summary>
        //IsRegistryIntegration = 5003,

        ///// <summary>Разрешить копирование мероприятий из другого плана </summary>
        //EnablePlanActivityCopy = 3003,

        ///// <summary>Показать сообщение для целевых показателей </summary>
        //ShowMessageForGoalIndicators = 3004,

        ///// <summary>Идентификатор файла помощи ИКТ </summary>
        //IKTHelpDownloadID = 3005,

        // <summary>Идентификатор файла помощи ИКТ </summary>
        ExportImportXmlEnabled = 3006,

        // <summary>Заголовок страниц сайта </summary>
        PageTitle = 6001,

        // <summary>Дней до планируемой даты публикации</summary>
        DaysUpToPlansZakupkiPublishDate = 7001,

        // <summary>Дней до даты вскрытия ковертов</summary>
        DaysUpToPlansZakupkiOpenDate = 7002,

        // <summary>Дней до даты рассмотрения заявок</summary>
        DaysUpToPlansZakupkiConsDate = 7003,

        // <summary>Дней до даты подведения итогов</summary>
        DaysUpToPlansZakupkiItogDate = 7004,

        // <summary>Пользователь, от имени которого производить изменения в АИС УВИРИ </summary>
        UVIRIUser = 8001,

        UviriIntegrationEnabled = 8002,
    }
}