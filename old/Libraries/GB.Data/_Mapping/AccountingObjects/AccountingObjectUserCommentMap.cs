﻿using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.Documents;
using GB.Data.Documents;

namespace GB._Mapping.AccountingObjects
{
    internal class AccountingObjectUserCommentMap : ClassMapHiLo<AccountingObjectUserComment>
    {
        public AccountingObjectUserCommentMap()
            : base("OU_User_Comment")
        {
             
        }

        protected override void InitMap()
        {
            Map(x => x.CreateDate, "OU_User_Comment_Create_DT").Not.Nullable();
            Map(x => x.Comment, "Comment");            
            References(x => x.Status, "Status_ID").Nullable();

            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
            References(x => x.User, "Users_ID").Not.Nullable();

            HasMany(x => x.Documents).KeyColumns.Add("Document_Owner_ID")
                .Where("Doc_Type_ID = " + (int)DocTypeEnum.AccountingObjectCommentDocument)
                .Where("Portal_Table_ID = " + (int)EntityType.AccountingObjectCommentDocument)
                .Cascade.Delete().Inverse();                

            //HasMany(x => x.Documents).KeyColumns.Add("OU_ID").Cascade.Delete().Inverse();            
        }
    }
}