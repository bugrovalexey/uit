﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/v2012new/ActivityCard.master"
    AutoEventWireup="true" CodeBehind="CommonData.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.CommonData" %>

<%@ Import Namespace="GB.MKRF.Web.Plans.v2012new" %>

<asp:Content ID="contentTop" ContentPlaceHolderID="detailTopContent" runat="server">

    <style>
        .SaveButton td.dxbButton_MetropolisBlue {
            background: #399330;
            border: 1px solid #399330;
            color: #FFF;
        }
    </style>
    <dxe:ASPxButton runat="server" AutoPostBack="false" Text="Сохранить" ValidationGroup="CommonData" CssClass="fleft topButton SaveButton"
        CausesValidation="true" ID="saveButton">
        <ClientSideEvents Click="function(s,e){ hfSave(); callBack.PerformCallback(''); LoadingPanel.Show(); ShowConfirm(false);}" />
    </dxe:ASPxButton>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" SyncWithServer="true" />
    <dxhf:ASPxHiddenField runat="server" ID="hfPlansActivityGoal" ClientInstanceName="hfPlansActivityGoal" />
    <dxpc:ASPxPopupControl ID="PopupControl" runat="server" AllowDragging="True" AllowResize="True"
        CloseAction="CloseButton" Modal="True" EnableViewState="False"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="600px"
        HeaderText="Выберите БК ГРБС" ClientInstanceName="PopupControl" EnableHierarchyRecreation="True">
    </dxpc:ASPxPopupControl>

    <script type="text/javascript">

        function hfReasonsSave(e) {
            hfReasons.Set('NPAName', ReasonNPANameTextBox.GetText());
            hfReasons.Set('NPATypeId', ReasonNPATypeComboBox.GetValue());
            hfReasons.Set('NPANum', ReasonNPANumTextBox.GetText());
            hfReasons.Set('NPADate', ReasonNPADateDateEdit.GetValue());
            hfReasons.Set('NPAItem', ReasonItemTextBox.GetText());
            hfReasons.Set('NPAFiles', uploader2.GetFiles());
        }


        function fileUploaded(fileInfo) {

            var info = fileInfo.split('+');

            hfDoc.Set('FilePath', info[0]);
            hfDoc.Set('FileName', info[1]);
            hfDoc.Set('ContentType', info[2]);

            showDocContainer(info[1]);
        }

        function showDocContainer(fileName) {
            var div = document.getElementById("docDiv");
            div.innerHTML = "<b>Файл " + fileName + " загружен</b>";
            div = document.getElementById("docLabel");
            div.innerHTML = "";
        }

        function hfSave() {
            hf.Set('Annotation', AnnotationMemo.GetText());
            hf.Set('Type', TypeComboBox.GetValue());
            hf.Set('Funding', FundingComboBox.GetValue());
            //            hf.Set('IKTObject', IKTObjectComboBox.GetValue());
            hf.Set('DatePlacement', PlansActivityDatePlacement.GetValue());

            //добавил, потому что при добавлении нового элемента контрол срабатывает на фильтр
            //            IKTObjectComboBox.AddItem(IKTObjectComboBox.GetValue());
        }

        function validateTextBox(s, e) {
            var cbValue = s.GetValue();
            e.isValid = (cbValue != undefined || cbValue) && cbValue != 'Не задано';
        }
        function validateComboBox(s, e) {
            e.isValid = s.GetValue() != 0;
        }

        function hfPlansActivityGoalSave() {
            hfPlansActivityGoal.Set('GoalName', PlansActivityGoalTextBox.GetValue());
        }

        function ShowConfirm(isChange) {
            if (isChange) {
                window.onbeforeunload = function (e) {
                    return "Внесенные на странице данные не сохранены.";
                };
            } else {
                window.onbeforeunload = null;
            }
        }

        $(document).ready(function () {
            TypeComboBox.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            //            IKTObjectComboBox.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            AnnotationMemo.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            FundingComboBox.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });

            <% if (!(Master as ActivityCard).EnableEdit)
               {%>
            $('a .command, .catalogButton').each(function () {
                $(this).css("display", "none");
            });
            <% }%>

            UpdateButtonClear('IKTSelection');
            UpdateButtonClear('Responsible');
            UpdateButtonClear('IKTObject');
        });

        function UpdateButtonClear(name) {
            var val = hf.Get(name + 'Id');

            if (val && val != 0) {
                var btName = '#' + name + 'TextBox';
                var bt = $(btName).parents('td.form_edit_input').find('div.ButtonClear');

                bt.css('display', '');
            }
        }

        SelectValue = function (e) {
            PopupControl.SetContentHtml("");
            switch (e) {
                case 'IKTSelection':
                    PopupControl.SetHeaderText('Выберите ИКТ/ИС');
                    PopupControl.SetContentUrl("<%=GetIKTSelectorUrl()%>");
                    break;
                case 'Responsible':
                    PopupControl.SetHeaderText('Выберите ответственного');
                    PopupControl.SetContentUrl("<%=GetRespSelectorUrl()%>");
                    break;
                case 'IKTObject':
                    PopupControl.SetHeaderText('Выберите ИС или объект ИКТ-инфраструктуры');
                    PopupControl.SetContentUrl("<%=GetIKTObjectSelectorUrl()%>");
                    break;
                default:
                    alert('error');
            }

            PopupControl.RefreshContentUrl();
            PopupControl.Show();
        };

        PopupClose = function (type, result) {
            PopupControl.Hide();
            var mass = result.split(';');
            if (mass[0]) hf.Set(type + 'Id', mass[0]);
            if (mass[1]) hf.Set(type + 'Name', mass[1]);
            var Memo = '#' + type + 'TextBox';
            $(Memo).find('input').val(hf.Get(type + 'Name'));
            UpdateButtonClear(type);
            ShowConfirm(true);

            PopupControl.SetContentHtml("");
        };


        function ClearValue(s, name) {
            if (hf.Contains(name + 'Id')) {
                ShowConfirm(true);
            }

            hf.Set(name + 'Id', '0');
            hf.Set(name + 'Name', '');

            var Memo = '#' + name + 'TextBox';
            $(Memo).find('input').val('Не задано');

            $(s).css('display', 'none');
        };
    </script>
    <p class="header_grid" style="">
        Основная информация
        <span class="subtitle">Заполняется на первом этапе
        </span>
    </p>

                <table class="form_edit" style="float: left; margin-top: 0px;">
                    <tr>
                        <td class="form_edit_desc">Тип мероприятия
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="TypeComboBox" runat="server" ClientInstanceName="TypeComboBox"
                                TextField="Name" ValueField="Id" ValueType="System.Int32" 
                                OnDataBinding="TypeComboBox_DataBinding">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Наименование ИС или объекта ИКТ-инфраструктуры
                        </td>
                        <td class="form_edit_input">
                            <%--                    <dxe:ASPxComboBox ID="IKTObjectComboBox" runat="server" EnableCallbackMode="true" ClientInstanceName="IKTObjectComboBox"
                         ValueType="System.String" ValueField="Name" TextField="Name" DropDownStyle="DropDown"
                        OnDataBinding="IKTObjectComboBox_DataBinding" >
                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData">
                            <RequiredField IsRequired="true" ErrorText="Обязательное поле" />
                        </ValidationSettings>                        
                    </dxe:ASPxComboBox>--%>
                            <div style="position: relative">
                                <dxe:ASPxTextBox ID="IKTObjectTextBox" runat="server" ClientInstanceName="IKTObjectTextBox" ClientIDMode="Static" OnDataBinding="TextBox_DataBinding" ReadOnly="true">
                                    <ClientSideEvents Validation="function(s,e){validateTextBox(s,e);}" />
                                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData">
                                        <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                        <RegularExpression ErrorText="Обязательное поле" ValidationExpression="^[Не задано]" />
                                    </ValidationSettings>
                                </dxe:ASPxTextBox>
                                <div class="ButtonSelected catalogButton" onclick="SelectValue('IKTObject'); return false;"></div>
                                <div class="ButtonClear catalogButton" style="display: none;" onclick="ClearValue(this, 'IKTObject'); return false;"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Классификационный признак ИС или компонента ИТ-инфраструктуры
                        </td>
                        <td class="form_edit_input">
                            <div style="position: relative">
                                <dxe:ASPxTextBox ID="IKTSelectionTextBox" runat="server" ClientInstanceName="IKTSelectionTextBox" ClientIDMode="Static" OnDataBinding="TextBox_DataBinding" ReadOnly="true">
                                    <ClientSideEvents TextChanged="function (s, e) { alert('t'); }" />
                                </dxe:ASPxTextBox>
                                <div class="ButtonSelected catalogButton" onclick="SelectValue('IKTSelection'); return false;"></div>
                                <div class="ButtonClear catalogButton" style="display: none;" onclick="ClearValue(this, 'IKTSelection'); return false;"></div>
                            </div>

                            <%--<a id="iktselecthref" class="command" href="SelectValue('IKTSystem'); return false;" >Выбрать</a>
                    <a id="iktclearhref" class="command" href="javascript:void(0)" onclick="IKTClear(); return false;">Очистить</a>
                    <dxpc:ASPxPopupControl ID="IKTPopupControl" runat="server" AllowDragging="True" AllowResize="True"
                        CloseAction="CloseButton" Modal="True" EnableViewState="False" PopupElementID="iktselecthref" 
                        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="600px" 
                        HeaderText="Выберите ИКТ/ИС" ClientInstanceName="IKTPopupControl" EnableHierarchyRecreation="True">
                    </dxpc:ASPxPopupControl>--%>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Аннотация
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo ID="AnnotationMemo" runat="server" ClientInstanceName="AnnotationMemo">
                                <ClientSideEvents TextChanged="function (s, e) { }" />
                            </dxe:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc"><span class="required"></span>Ответственный
                        </td>
                        <td class="form_edit_input">
                            <div style="position: relative">
                                <dxe:ASPxTextBox ID="ResponsibleTextBox" runat="server" ClientInstanceName="ResponsibleTextBox" ClientIDMode="Static" OnDataBinding="TextBox_DataBinding" ReadOnly="true">
                                    <ClientSideEvents Validation="function(s,e){validateTextBox(s,e);}" />
                                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData">
                                        <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                        <RegularExpression ErrorText="Обязательное поле" ValidationExpression="^[Не задано]" />
                                    </ValidationSettings>
                                </dxe:ASPxTextBox>
                                <div class="ButtonSelected catalogButton" onclick="SelectValue('Responsible'); return false;"></div>
                                <div class="ButtonClear catalogButton" style="display: none;" onclick="ClearValue(this, 'Responsible'); return false;"></div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="form_edit_desc">Источник финансирования
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="FundingComboBox" runat="server" ClientInstanceName="FundingComboBox"
                                TextField="Name" ValueField="Id" ValueType="System.Int32" 
                                OnDataBinding="FundingComboBox_DataBinding">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>

                    <tr style="display: none">
                        <td class="form_edit_desc">Планируемый срок размещения
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="PlansActivityDatePlacement" ClientInstanceName="PlansActivityDatePlacement" />
                        </td>
                    </tr>

                </table>

    <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" OnCallback="callBack_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }"
            CallbackError="function(s,e) { LoadingPanel.Hide(); }" />
    </dxc:ASPxCallback>

    <p class="header_grid">
        Наименование и реквизиты основания реализации мероприятия и (или) решения о создании
                        системы с указанием пунктов, статей (федеральный закон, акт Президента Российской
                        Федерации, Правительства Российской Федерации)
        <span class="subtitle">Заполняется на первом этапе
        </span>
    </p>

    <dxhf:ASPxHiddenField runat="server" ID="hfReasons" ClientInstanceName="hfReasons" />
    <dxhf:ASPxHiddenField runat="server" ID="hfDoc" ClientInstanceName="hfDoc" />
    <dxe:ASPxButton ID="AddReasonsButton" runat="server" ClientInstanceName="AddReasonsButton"
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {ReasonsGrid.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="ReasonsGrid" Width="100%" ClientInstanceName="ReasonsGrid"
        KeyFieldName="Id"
        OnBeforePerformDataSelect="ReasonsGrid_BeforePerformDataSelect"
        OnRowInserting="ReasonsGrid_RowInserting"
        OnRowUpdating="ReasonsGrid_RowUpdating"
        OnStartRowEditing="ReasonsGrid_StartRowEditing"
        OnRowDeleting="ReasonsGrid_RowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False"
                AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" />
            <dxwgv:GridViewDataTextColumn FieldName="Id" ShowInCustomizationForm="False" Visible="False" />
            <dxwgv:GridViewDataTextColumn FieldName="Rekvisit" Caption="Реквизиты документа" />
            <dxwgv:GridViewDataTextColumn FieldName="Item" Caption="Пункт, статья документа" />
            <dxwgv:GridViewDataTextColumn ShowInCustomizationForm="False">
                <Settings AllowDragDrop="False" />
                <DataItemTemplate>
                    <%#getDownloadUrl((string)Eval("FileInfo"))%>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
        <SettingsEditing Mode="EditForm" />
        <Styles>
            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
        </Styles>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Вид документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox runat="server" ID="ReasonNPATypeComboBox" ClientInstanceName="ReasonNPATypeComboBox"
                                OnDataBinding="ReasonNPATypeComboBox_DataBinding" TextField="Value" ValueField="Key"
                                ValueType="System.Int32" EncodeHtml="false">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Наименование документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo runat="server" ID="ReasonNPANameTextBox" ClientInstanceName="ReasonNPANameTextBox" Rows="5"
                                OnDataBinding="ReasonNPANameTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Номер документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="ReasonNPANumTextBox" ClientInstanceName="ReasonNPANumTextBox"
                                OnDataBinding="ReasonNPANumTextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Дата документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="ReasonNPADateDateEdit" ClientInstanceName="ReasonNPADateDateEdit"
                                OnDataBinding="ReasonNPADateDateEdit_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Пункт, статья документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="ReasonItemTextBox" ClientInstanceName="ReasonItemTextBox"
                                OnDataBinding="ReasonItemTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td class="form_edit_desc">
                            <div id="docLabel">
                                Файлы:
                            </div>
                        </td>
                        <td class="form_edit_input">
                            <cc:FileUploadControl2 runat="server" ID="uploader2" Mode="MultiFile" ClientInstanceName="uploader2"
                                OnDataBinding="uploader2_DataBinding" AutoUpload="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') hfReasonsSave(e); }" />
    </dxwgv:ASPxGridView>

    <p class="header_grid">
        Цели мероприятия
        <span class="subtitle">Заполняется на первом этапе
        </span>
    </p>

    <dxe:ASPxButton ID="AddPlansActivityGoalButton" runat="server" ClientInstanceName="AddPlansActivityGoalButton"
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {PlansActivityGoalGrid.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="PlansActivityGoalGrid" Width="100%" ClientInstanceName="PlansActivityGoalGrid"
        KeyFieldName="Id"
        OnBeforePerformDataSelect="PlansActivityGoalGrid_BeforePerformDataSelect"
        OnRowInserting="PlansActivityGoalGrid_RowInserting"
        OnRowUpdating="PlansActivityGoalGrid_RowUpdating"
        OnStartRowEditing="PlansActivityGoalGrid_StartRowEditing"
        OnRowDeleting="PlansActivityGoalGrid_RowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Цель мероприятия" />
        </Columns>
        <SettingsBehavior ConfirmDelete="True" />
        <Styles>
            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
        </Styles>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Цель мероприятия:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo ID="PlansActivityGoalTextBox" runat="server" ClientInstanceName="PlansActivityGoalTextBox"
                                OnLoad="PlansActivityGoalTextBox_Load" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') hfPlansActivityGoalSave(e); }" />
    </dxwgv:ASPxGridView>
</asp:Content>
