﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using GB.Handlers;
using GB.Helpers;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Plans.v2012new
{
    public class YellowTabTemplate : System.Web.UI.ITemplate
    {
        string text;
        bool isYellow;
        public YellowTabTemplate(string text, bool isYellow)
        {
            this.text = text;
            this.isYellow = isYellow;
        }
        public void InstantiateIn(System.Web.UI.Control container)
        {
            var div = new HtmlGenericControl("div");
            div.Style.Add(HtmlTextWriterStyle.WhiteSpace, "nowrap");
            if (this.isYellow)
            {
                div.Attributes["class"] = "circleYellow";
                //div.Attributes["title"] = "Данные изменены";
            }
            div.InnerHtml = text;
            
            container.Controls.Add(div);
        }
    }

    public partial class HistoryActionDetails : System.Web.UI.Page
    {
        public const string ActivityIdKey = "Activity";

        DateTime? dateInternal;
        protected DateTime date1
        {
            get
            {
                if (!dateInternal.HasValue)
                {
                    dateInternal = parseDate(Request.QueryString[HistoryAction.historyActionTicksKey], true);
                    if (dateInternal == DateTime.MinValue)
                    {
                        throw new ArgumentException();
                    }
                }
                return dateInternal.Value;
            }
        }

        DateTime? date2Internal;
        protected DateTime? date2
        {
            get
            {
                if (!date2Internal.HasValue)
                {
                    date2Internal = parseDate(Request.QueryString["t2"], false);
                }

                return (date2Internal != DateTime.MinValue) ? date2Internal : null;
            }
        }

        int? currentActivityIdInternal;
        protected int CurrentActivityId
        {
            get
            {
                if (!currentActivityIdInternal.HasValue)
                {
                    int id;
                    if (int.TryParse(Request.QueryString[ActivityIdKey], out id))
                    {
                        currentActivityIdInternal = id;
                    }
                    else
                    {
                        ResponseHelper.DenyAccess(this.Response);
                    }
                }
                return currentActivityIdInternal.Value;
            }
        }

        HistoryData historyDataInternal;
        protected HistoryData hd
        {
            get
            {
                if (historyDataInternal == null)
                {
                    historyDataInternal = new HistoryData(CurrentActivityId, date1, date2);
                }
                return historyDataInternal;
            }
        }



        protected DateTime? parseDate(string ticksValue, bool isRequired)
        {
            DateTime? tempDate = null;

            long ticks = 0;
            if (long.TryParse(ticksValue, out ticks))
            {
                tempDate = new DateTime(ticks);
                if (tempDate > new DateTime(9999, 1, 1) || tempDate < new DateTime(1900, 1, 1))
                    tempDate = null;
            }

            if (tempDate == null)
            {
                if (isRequired)
                    ResponseHelper.DenyAccess(this.Response);
                else
                    tempDate = DateTime.MinValue;
            }
            return tempDate;
        }

        public static string GetUrl(int ActionId, DateTime date)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(UrlHelper.Resolve("~/Plans/v2012new/HistoryActionDetails.aspx"));
            builder.Append("?");
            builder.Append(HistoryAction.historyActionKey);
            builder.Append("=");
            builder.Append(ActionId);
            builder.Append("&");
            builder.Append(HistoryAction.historyActionTicksKey);
            builder.Append("=");
            builder.Append(date.Ticks);
            return builder.ToString();
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.DataBind();

            if (date2.HasValue)
            {
                var selectedItem = cb.Items.FindByValue(date2.Value.Ticks.ToString());
                if (selectedItem != null)
                    cb.SelectedIndex = selectedItem.Index;

                var cmp = hd.Compare();

                if (!string.IsNullOrEmpty(cmp))
                {
                    var diff = new HashSet<string> (cmp.Split(','));

                    var yellowTabSet = new HashSet<string>(
                        new Dictionary<string, string>()
                            {
                                {"gridResponsible", "Common"},
                                {"gridResponsible2", "Common"},
                                {"gridReason", "Common"},
                                {"gridPlansActivityGoal", "Common"},
                                {"gridFunc", "Functions"},
                                {"gridGoalIndicator", "GoalIndicators"},
                                //{"gridAPFInfo", "Special"},
                                {"gridZOD", "Special"},
                                {"gridExpense1", "Expenses"},
                                {"gridExpense2", "Expenses"},
                                {"gridExpense3", "Expenses"},
                                {"gridExpenseDirection", "Expenses"},
                                {"gridStateContract", "GoalsResults"},
                                {"gridWork", "Works"},
                                {"gridTypesWork", "Works"},
                                {"gridSpecification", "Specifications"},
                                {"gridTypesSpec", "Specifications"}
                            }
                            .Where(x => diff.Contains(x.Key))
                            .Select(x => x.Value));

                    

                    foreach (TabPage tab in tc.TabPages)
                    {
                        tab.TabTemplate = new YellowTabTemplate(tab.Text, yellowTabSet.Contains(tab.Name));
                    }
                }

                
            }
            else
            {
                cb.SelectedIndex = 0;
            }

            base.OnPreRender(e);
        }

        protected string GetLegend()
        {
            if (this.date2.HasValue)
            {
                return string.Format(HistoryData.LegendTemplate, date1, date2);
            }
            else
            {
                return null;
            }
        }

        protected void grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            object ownerKey = getOwnerKey(grid);
            grid.DataSource = hd.getDataSource(grid.ID, ownerKey);
            if (date2.HasValue)
            {
                switchGridToCompareMode(grid);
            }
        }

        protected void grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (!date2.HasValue)
                return;

            ASPxGridView grid = (ASPxGridView)sender;

            string id = grid.GetRowValues(e.VisibleIndex, HistoryData.rowIdColumnName).ToString();

            e.Cell.Font.Bold =     (id.IndexOf("__1") >= 0)
                                && (grid.GetRowValues(e.VisibleIndex, HistoryData.captionColumnName).ToString().IndexOf("_cmp") >= 0);

            

            if (string.IsNullOrEmpty(e.DataColumn.FieldName))
                return;

            if (currentChanges.Key != e.VisibleIndex)
            {
                // актуализируем информацию об изменениях
                string[] splitted = id.Split('@');

                string changesRaw = (splitted.Length > 1) ? splitted[1] : "";
                currentChanges = new KeyValuePair<int, HashSet<string>>(e.VisibleIndex, new HashSet<string>(changesRaw.Split(';')));
            }


            if (currentChanges.Value != null &&
                currentChanges.Value.Contains(e.DataColumn.FieldName) &&
                e.DataColumn.FieldName != HistoryData.keyColumnName &&
                e.Cell.CssClass.IndexOf("compareYellow") < 0)
            {
                e.Cell.CssClass += " compareYellow";
            }
        }

        protected void grid_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;

            if (e.RowType != GridViewRowType.Group)
                return;

            if (!grid.IsRowExpanded(e.VisibleIndex))
                return;

            int level = grid.GetRowLevel(e.VisibleIndex);
            if (e.Row.Cells.Count <= level)
                return;

            e.Row.Font.Bold = true;

            if (e.Row.Cells[0].HasControls())
                e.Row.Cells[0].Controls[0].Visible = false;
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            grid.DetailRows.ExpandAllRows();
            grid.SettingsDetail.ShowDetailButtons = false;
        }

        protected void switchGridToCompareMode(ASPxGridView grid)
        {
            if (grid == gridReason || 
                grid == gridExpenseDirection ||
                grid == gridPlansActivityGoal ||
                grid == gridStateContract ||
                grid == gridGoalIndicator ||
                grid == gridWork ||
                grid == gridSpecification ||
                grid.ID == "gridTypesWork" ||
                grid.ID == "gridTypesSpec")
            {
                grid.KeyFieldName = HistoryData.rowIdColumnName;
                GridViewDataColumn captionColumn = (GridViewDataColumn)grid.Columns["Caption"];
                captionColumn.Visible = true;
                captionColumn.GroupIndex = 0;
            }

        }

        protected object getOwnerKey(ASPxGridView grid)
        {
            if (grid.Parent is GridViewDetailRowTemplateContainer)
            {
                GridViewDetailRowTemplateContainer c = grid.Parent as GridViewDetailRowTemplateContainer;
                return c.Grid.GetRowValues(c.VisibleIndex, "Id");
            }
            else
            {
                return null;
            }
        }

       

        KeyValuePair<int, HashSet<string>> currentChanges = new KeyValuePair<int, HashSet<string>>(-1, new HashSet<string>());

        protected string getActivityInfo(string key)
        {
            DataTable activityTable = hd.extractActivity(date1);
            if (activityTable.Rows.Count <= 0 && date2.HasValue)
            {
                activityTable = hd.extractActivity(date2.Value); 
            }

            if (activityTable != null && activityTable.Rows.Count > 0)
                return activityTable.Rows[0][key].ToString();
            else
                return null;
        }

        protected void cb_DataBinding(object sender, EventArgs e)
        {
            DataTable versions = HistoryRepository.GetVersionsByActivity(CurrentActivityId, date1);
            versions.Columns.Add("Id");
            versions.Columns.Add("Name");
            if (versions.Rows.Count > 0)
            {
                foreach (DataRow row in versions.Rows)
                {
                    DateTime d = (DateTime)row["DT"];
                    row["Id"] = d.Ticks;
                    row["Name"] = d.ToString();
                }

                DataRow r = versions.NewRow();
                r["Id"] = 0;
                r["Name"] = "Выберите версию";
                versions.Rows.InsertAt(r, 0);
            }
            else
            {
                DataRow r = versions.NewRow();
                r["Id"] = -1;
                r["Name"] = "Не с чем сравнивать";
                versions.Rows.InsertAt(r, 0);
                cb.Enabled = false;
            }

            cb.DataSource = versions;
        }

        protected string getActivityListUrl()
        {
            string plansId = null;

            DataTable table = hd.extractActivity(date1);
            if (table.Rows.Count > 0)
            {
                plansId = table.Rows[0]["Plans_ID"].ToString();
            }
            else if (date2.HasValue)
            {
                table = hd.extractActivity(date2.Value);
                if (table.Rows.Count > 0)
                {
                    plansId = table.Rows[0]["Plans_ID"].ToString();
                }
            }
            if (plansId != null)
            {
                string url = "HistoryAction.aspx?" + PlanCard.plansIdKey + "=" + plansId + "&t=" + date1.Ticks;
                return (url + (date2.HasValue ? ("&t2=" + date2.Value.Ticks) : null));
            }
            else
            {
                return null;
            }
        }

        protected string getDocumentUrl(object DocumentId)
        {
            int id = 0;
            return (DocumentId != null && int.TryParse(DocumentId.ToString(), out id) && id > 0) ?
                        ("<a href='" + FileHandler.GetUrl(id) + "'>Скачать</a>") :
                        null;
        }
        
    }
}