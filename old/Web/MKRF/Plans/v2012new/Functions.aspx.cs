﻿using GB.Helpers;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.MKRF.Web.Helpers;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class Functions : PlansActivityEditPage
    {
        PlansActivityFuncController controller = new PlansActivityFuncController();

        protected override void OnPreRender(EventArgs e)
        {
            FuncGrid.DataBind();
            FuncGrid.Columns["cmdColumn"].Visible = AddFuncButton.Visible = EnableEdit;

            base.OnPreRender(e);
        }

        public static string GetUrl(int activityId)
        {
            return UrlHelper.Resolve("~/Plans/v2012new/Functions.aspx?ActivityId=" + activityId.ToString());
        }

        #region FuncGrid

        protected void FuncGrid_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            if (!EnableEdit)
                throw new Exception("Добавление запрещено");
        }

        protected void FuncGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            if (CurrentActivity != null)
                FuncGrid.DataSource = controller.GetList(CurrentActivity);
        }

        protected void FuncGrid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void FuncGrid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            controller.Create(new PlansActivityFuncControllerArgs
            {
                ParentActivity = CurrentActivity,
                Values = hfFunc.ToDictionary(x => x.Key, x => x.Value)
            });

            hfFunc["ServiceId"] = null;

            RefreshActivity();
            FuncGrid.GridCancelEdit(e);
        }

        protected void FuncGrid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            controller.Update(new PlansActivityFuncControllerArgs
            {
                    PlansActivityFuncId = e.Keys[0],
                    Values = hfFunc.ToDictionary(x => x.Key, x => x.Value)
            });

            hfFunc["ServiceId"] = null;

            RefreshActivity();
            FuncGrid.GridCancelEdit(e);
        }

        protected void FuncGrid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            controller.Delete(new PlansActivityFuncControllerArgs
            {
                PlansActivityFuncId = e.Keys[0]
            });

            RefreshActivity();
            FuncGrid.GridCancelEdit(e);
        }

        protected void FuncGrid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            var itemId = (int)FuncGrid.GetRowValues(e.VisibleIndex, FuncGrid.KeyFieldName);
            controller.DeleteAllByService(new PlansActivityFuncControllerArgs
            {
                PlansActivityFuncId = itemId,
                ParentActivity = CurrentActivity
            });

            RefreshActivity();
            FuncGrid.CancelEdit();
            FuncGrid.DataBind();
        }

        protected void FuncTypeComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = (ASPxComboBox)sender;

            //Если мероприятие содержит какую-либо функцию, то он может содержать функции того же вида, поэтому
            //в данном случае комбобокс будет содержать только такие функции
            //изменять тип функции будет нельзя (компобокс будет дизейблиться)
            if (controller.HasFunctions(CurrentActivity))
            {
                box.ClientEnabled = false;
            }

            var types = controller.GetFuncTypes(CurrentActivity);
            box.DataSource = types.Types;
            if (controller.CurrentEdit == null)
            {
                box.SelectedIndex = 0;
            }
            else
            {
                box.Value = types.SelectedValue;
            }
        }

        protected void TextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null || controller.CurrentEdit.Service == null)
                return;

            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = controller.CurrentEdit.Service.Name;
        }

        protected void CFComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            var taList = controller.GetTypicalActivitiesList();

            box.DataSource = taList.List;
            box.Value = taList.SelectedTypicalActivities.Id;
        }

        protected void UsedBeforeASPxCheckBox_DataBinding(object sender, EventArgs e)
        {
            ASPxCheckBox box = (ASPxCheckBox)sender;

            box.Checked = controller.CurrentEdit != null && controller.CurrentEdit.IsUsedBefore;
        }

        protected void DirectASPxCheckBox_DataBinding(object sender, EventArgs e)
        {
            ASPxCheckBox box = (ASPxCheckBox)sender;

            box.Checked = controller.CurrentEdit != null && controller.CurrentEdit.DirectedToExpl;
        }

        protected void MissFuncASPxCheckBox_DataBinding(object sender, EventArgs e)
        {
            ASPxCheckBox box = (ASPxCheckBox)sender;

            box.Checked = controller.CurrentEdit != null && controller.CurrentEdit.IsMissingFunc;
        }

         protected void MissFuncNameTextBox_DataBinding(object sender, EventArgs e)
        {

            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = controller.CurrentEdit == null ? string.Empty : controller.CurrentEdit.MissingFuncName;
        }

        protected void FuncNPATypeComboBox_DataBinding(object sender, EventArgs e)
        {
            var types = controller.GetNPATypes();
            ASPxComboBox box = (ASPxComboBox)sender;
            
            box.DataSource = types.Types;
            box.Value = types.SelectedValue;

            if (controller.CurrentEdit == null)
            {
                box.SelectedIndex = -1;
            }
        }

        protected void FuncNPAKindComboBox_DataBinding(object sender, EventArgs e)
        {
            var kinds = controller.GetNPAKinds();
            ASPxComboBox box = (ASPxComboBox)sender;

            box.DataSource = kinds.Kinds;
            box.Value = kinds.SelectedValue;

            if (controller.CurrentEdit == null)
            {
                box.SelectedIndex = -1;
            }
        }

        protected void FuncNPANumTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = controller.CurrentEdit == null || controller.CurrentEdit.NPA == null ? string.Empty : controller.CurrentEdit.NPA.Num;
        }

        protected void FuncNPADateDateEdit_DataBinding(object sender, EventArgs e)
        {
            ASPxDateEdit box = (ASPxDateEdit)sender;

            box.Value = controller.CurrentEdit == null || controller.CurrentEdit.NPA == null ? null : controller.CurrentEdit.NPA.Date;
        }

        protected void FuncItemTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = (ASPxTextBox)sender;

            if (controller.CurrentEdit != null)
            {
                if (controller.CurrentEdit.NPA != null)
                    box.Text = controller.CurrentEdit.NPA.Item;

                if (!string.IsNullOrEmpty(controller.CurrentEdit.Comment))
                    box.Text = controller.CurrentEdit.Comment;
            }
        }

        protected void FuncNPANameTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = controller.CurrentEdit == null || controller.CurrentEdit.NPA == null ? string.Empty : controller.CurrentEdit.NPA.Name;
        }

        protected void FuncGrid_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Edit)
            {
                var editable = (bool)FuncGrid.GetRowValues(e.VisibleIndex, "Editable");
                e.Enabled = editable;
                e.Visible = editable;
            }
        }

        //protected object GetServiceUrl()
        //{
        //    return "../../Selectors/Services.aspx";
        //}

        

        //protected void ServicePanel_Callback(object sender, CallbackEventArgsBase e)
        //{
        //    var id = Convert.ToInt32(e.Parameter);
        //    var service = RepositoryBase<Gov_Service>.Get(id);

        //    hfFunc["Name"] = service.Name;
        //    hfFunc["ServiceId"] = service.Id;

        //    //hfContract["Number"] = contract.Number;
        //    //hfContract["Product"] = string.Join("<br />", contract.Products.Select(i => i.Name).ToArray());
        //    //hfContract["SignDate"] = contract.Sign_Date.ToShortDateString();
        //}
        #endregion
    }
}