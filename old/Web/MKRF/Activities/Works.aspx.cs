﻿using System;
using System.Collections.Generic;
using System.Linq;
using GB.Entity.Directories;
using GB.Extentions;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using GB.Controls;
using GB.Controls.Extensions;

namespace GB.MKRF.Web.Activities
{
    public partial class Works : System.Web.UI.Page
    {
        readonly PlansWorkController controller = new PlansWorkController();

        public PlansActivity CurrentActivity
        {
            get { return (Master as EditCard).CurrentActivity; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ((GridViewDataComboBoxColumn)Grid.Columns["yearColumn"]).PropertiesComboBox.DataSource = PlanYearsRepository.YearList;
            Grid.DataBind();

            base.OnPreRender(e);
        }

        #region Grid

        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var data = controller.Repository.GetAll(x => x.PlanActivity == CurrentActivity);
            ((ASPxGridView)sender).DataSource = data.Select(w =>
                 new
                 {
                     w.Id,
                     w.Name,
                     OKPD = w.OKPD.Return(x => x.CodeName),
                     ExpenseDirection = w.ExpenseDirection.Return(x => x.Name),
                     ExpenseType = w.ExpenseType.Return(x => x.Name),
                     ExpenditureItem = w.ExpenditureItem.Return(x => x.CodeName),
                     OKVED = w.OKVED.Return(x => x.CodeName),
                     w.Year,
                     w.SpecialistsCount,
                     w.Salary,
                     w.Duration
                 }).ToArray();
        }

        protected void Grid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void Grid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            var args = new PlansWorkArgs { Activity = CurrentActivity };
            FillPlansWorkArgs(args, hf);
            controller.Create(args);

            ((ASPxGridView)sender).GridCancelEdit(e);
        }

        protected void Grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            var args = new PlansWorkArgs { Id = Convert.ToInt32(e.Keys[0]) };
            FillPlansWorkArgs(args, hf);
            controller.Update(args);

            ((ASPxGridView)sender).GridCancelEdit(e);
        }

        protected void Grid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            controller.Delete(new PlansWorkArgs { Id = Convert.ToInt32(e.Keys[0]) });

            ((ASPxGridView)sender).GridCancelEdit(e);
        }

        #endregion

        #region Handlers

        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            e.Result = new RepositoryBaseNew<OKVED>().Get(e.Parameter).Return(x => x.Salary.ToString());
        }

        protected void NameTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(controller.CurrentEdit.With(x => x.Name));
        }

        protected void SpecCountTextBox_DataBinding(object sender, EventArgs e)
        {
            controller.CurrentEdit.With(x => x.SpecialistsCount).Do(sender.SetValueTextBoxVal<int>);
        }

        protected void DurationTextBox_DataBinding(object sender, EventArgs e)
        {
            controller.CurrentEdit.With(x => x.Duration).Do(sender.SetValueTextBoxVal<int>);
        }

        protected void SalaryTextBox_DataBinding(object sender, EventArgs e)
        {
            controller.CurrentEdit.With(x => x.Salary).DoVal(sender.SetValueTextBoxVal<decimal>);
        }

        protected void OKPDComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(DirectoriesRepositary.GetDirectories<OKPD>(DateTime.Now.Year).OrderBy(x => x.Code))
                  .SetValueComboBox<int>(controller.CurrentEdit.With(x => x.OKPD).Return(x => x.Id));
        }

        protected void ExpenseDirectionComboBox_DataBinding(object sender, EventArgs e)
        {
            // Необходимо чтобы в поле "Вид затрат" отображались только актуальные значения на текущую дату.
            DateTime date = DateTime.Now;
            sender.SetDataComboBox(new RepositoryBaseNew<ExpenseDirection>().GetAll(x => (x.Type == ExpenseTypeEnum.Works && x.DateStart <= date && date <= x.DateEnd) || x.Id == GB.EntityBase.Default))
                  .SetValueComboBox<int>(controller.CurrentEdit.With(x => x.ExpenseDirection).Return(x => x.Id));
        }

        protected void ExpenseTypeComboBox_DataBinding(object sender, EventArgs e)
        {
            int directionId = controller.CurrentEdit.With(x => x.ExpenseDirection).Return(x => x.Id);
            //Перечень доступных значений зависит от выбранного значения в поле виды затрат
            FillExpenseTypes(sender, directionId, controller.CurrentEdit.With(x => x.ExpenseType).Return(x => x.Id));
        }

        protected void ExpenditureComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(DirectoriesRepositary.GetDirectories<ExpenditureItem>(DateTime.Now.Year).OrderBy(x => x.Code))
                  .SetValueComboBox<int>(controller.CurrentEdit.Return(x => x.ExpenditureItem.Id));
        }

        protected void OKVEDComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(new RepositoryBaseNew<OKVED>().GetAll())
                  .SetValueComboBox<int>(controller.CurrentEdit.With(x => x.OKVED).Return(x => x.Id));
        }

        protected void YearComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(PlanYearsRepository.YearList)
                  .SetValueComboBox<int>(controller.CurrentEdit.Return(x => (int)x.Year, 1));
        }

        protected void ExpenseTypeComboBox_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            FillExpenseTypes(sender, Convert.ToInt32(e.Parameter), GB.EntityBase.Default).DataBind();
        }

        #endregion

        private static ASPxComboBox FillExpenseTypes(object sender, int directionId, int typeId)
        {
            var data = new RepositoryBaseNew<ExpenseType>().GetAll(x => (x.ExpenseDirection != null && x.ExpenseDirection.Id == directionId) || x.Id == GB.EntityBase.Default);
            return sender.SetDataComboBox(data)
                         .SetValueComboBox<int>(typeId);
        }

        private static void FillPlansWorkArgs(PlansWorkArgs args, IDictionary<string, object> values)
        {
            args.Name = (string)values["Name"];
            args.OKPD = Convert.ToInt32(values["OKPD"]);
            args.ExpenseDirection = Convert.ToInt32(values["ExpenseDirection"]);
            args.ExpenseType = Convert.ToInt32(values["ExpenseType"]);
            args.KOSGU = Convert.ToInt32(values["KOSGU"]);
            args.OKVED = Convert.ToInt32(values["OKVED"]);
            args.Year = (YearEnum)Convert.ToInt32(values["Year"]);
            args.SpecCount = Convert.ToInt32(values["SpecCount"]);
            args.Salary = Convert.ToDecimal(values["Salary"]);
            args.Duration = Convert.ToInt32(values["Duration"]);
        }

    }
}