﻿using GB.Data;
using GB.DataAccess.Repository;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess
{
    public interface IService<TEntity, TArgs>
        where TEntity : EntityBase
        where TArgs : IServiceArgs
    {
        TEntity Create(TArgs args);

        TEntity Update(TArgs args);

        void Delete(TArgs args);

        Repository<TEntity> Repository { get; }
    }
}