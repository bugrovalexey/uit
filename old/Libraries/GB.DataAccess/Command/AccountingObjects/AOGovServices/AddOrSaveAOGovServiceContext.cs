﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServices
{
    public class AddOrSaveAOGovServiceContext : ICommandContext
    {
        public AddOrSaveAOGovServiceContext()
        {
            AOGovServicesNPAIds = new List<int>();
        }

        public int? AccountingObjectId { get; set; }

        public int? GovServiceId { get; set; }
        
        public int? PercentOfUseResource { get; set; }

        public List<int> AOGovServicesNPAIds { get; set; }

        public int? xId { get; set; }
    }
}
