﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Monitoring/Monitoring.master" AutoEventWireup="true" CodeBehind="SummaryAskedReport.aspx.cs" Inherits="GB.MKRF.Web.Monitoring.v2012new.SummaryAskedReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="buttons" runat="server">

    <script type="text/javascript">

        function OnValueChanged(s, e) {
            //if (!CallbackPanel.InCallback())
            //    CallbackPanel.PerformCallback();
        }

    </script>

    <table style="width: 100%">
        <tr>
            <td style="width: 1%; white-space: nowrap">Департамент:</td>
            <td style="width: 1%">
                <dxe:ASPxComboBox runat="server" ID="DepComboBox" Width="400px" ValueType="System.Int32"
                    ValueField="Id" TextField="Name" ClientInstanceName="DepComboBox" OnDataBinding="DepComboBox_DataBinding">
                    <ClientSideEvents ValueChanged="OnValueChanged" />
                </dxe:ASPxComboBox>
            </td>
            <td style="padding: 0 10px;"></td>
            <td style="width: 1%; white-space: nowrap">Год:</td>
            <td style="width: 1%">
                <dxe:ASPxComboBox runat="server" ID="YearComboBox" Width="130px" ValueType="System.Int32"
                    ValueField="Key" TextField="Value" ClientInstanceName="YearComboBox" OnDataBinding="YearComboBox_DataBinding">
                    <ClientSideEvents ValueChanged="OnValueChanged" />
                </dxe:ASPxComboBox>
            </td>
            <td style="width: 98%"></td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="center" runat="server">
    <dxwpg:ASPxPivotGrid ID="PivotGrid" runat="server" OnBeforePerformDataSelect="PivotGrid_BeforePerformDataSelect">
        <Fields>
            <dxwpg:PivotGridField  FieldName="Code" Caption="Код мероприятия" />
            <dxwpg:PivotGridField  FieldName="Name" Caption="Наименование мероприятия" />
            <dxwpg:PivotGridField  FieldName="Type" Caption="Тип мероприятия" />
            <dxwpg:PivotGridField  FieldName="IsIkt" Caption="Классификационный признак ИС или компонента ИТ-инфраструктуры" />
            <dxwpg:PivotGridField  FieldName="NameIsIkt" Caption="Наименование ИС или объекта ИКТ-инфраструктуры" />
            <dxwpg:PivotGridField  FieldName="Annotation" Caption="Аннотация" />
            <dxwpg:PivotGridField  FieldName="Source" Caption="Источник финансирования" />
            <dxwpg:PivotGridField  FieldName="Responsible" Caption="ФИО ответственного сотрудника" />
            <dxwpg:PivotGridField  FieldName="Goals" Caption="Цель мероприятия" />
            <dxwpg:PivotGridField  FieldName="SectionKBK" Caption="Код по БК ПЗ/РЗ" />
            <dxwpg:PivotGridField  FieldName="ExpenseItem" Caption="Код по БК ЦСР" />
            <dxwpg:PivotGridField  FieldName="WorkForm" Caption="Код по БК ВР" />
            <dxwpg:PivotGridField  FieldName="ExpenditureItem" Caption="Код по БК КОСГУ" />
        </Fields>
        <Styles HeaderStyle-Wrap="True"/>
    </dxwpg:ASPxPivotGrid>
</asp:Content>
