﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class IKTComponentToYearMap : ClassMapBase<IKTComponentToYear>
    {
        public IKTComponentToYearMap()
            : base("IKT_Component_Years")
        {
            //Table("w_");

            //Id(x => x.Id, "IKT_Component_Years_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Year, "Year");
        }
    }
}
