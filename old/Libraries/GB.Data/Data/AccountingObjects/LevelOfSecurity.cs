﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Уровень защищенности
    /// </summary>
    public enum LevelOfSecurity
    {
        [Description("Не определен")]
        None = 0,

        [Description("1")]
        First = 1,

        [Description("2")]
        Second = 2,

        [Description("3")]
        Third = 3,

        [Description("4")]
        Fourth = 4,

        [Description("Не является ИСПдн")]
        IsNotISPDn = 5,
    }
}
