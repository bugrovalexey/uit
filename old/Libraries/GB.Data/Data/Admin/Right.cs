﻿namespace Uviri.MKRF.Entities.Admin
{
	/// <summary>Доп. право пользователя</summary>
	public class Right : EntityBase
	{
		/// <summary>Наименование</summary>
		public virtual string Name { get; set; }
	}

	public enum RightsEnum
	{
		/// <summary>Может назначать ответственных от МКС</summary>
		CanSetMksResponsible = 1,
	}
}
