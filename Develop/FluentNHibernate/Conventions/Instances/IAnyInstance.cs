using FluentNHibernate.Conventions.Inspections;

namespace FluentNHibernate.Conventions.Instances
{
    public interface IAnyInstance : IAnyInspector
    {
        new IAccessInstance Access { get; }
    }
}