﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.ExternalSystems.Zakupki360.Dto
{
    public class SearchParametersOrders
    {
        public string OrderNumber { get; set; }

        public int? ResCount { get; set; }
    }
}
