﻿<%@ Page Title="Письма" Language="C#" MasterPageFile="~/Plans/PlanCard.master" AutoEventWireup="true"
    CodeBehind="Mails.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.Mails" %>

<%@ Register src="../../Common/MailsControl.ascx" tagname="MailsControl" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <div class="page">
        <uc1:MailsControl ID="mc" runat="server"/>    
    </div>
</asp:Content>