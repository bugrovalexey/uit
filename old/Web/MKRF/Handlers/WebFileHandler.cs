﻿using GB.Handlers;
using GB.MKRF.Entities.Documents;
using GB.Repository;
using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Web.Handlers
{
    public class WebFileHandler : FileHandler
    {
        protected override string GetFileName(File_Info info)
        {
            Document document = RepositoryBase<Document>.Get(info.Id);

            return document.FileName;

            //TODO : Востановить проверку на скачивание файла
            //if (Validator.Validator.Validate(document, ObjActionEnum.view))
            //{
            //    return document.FileName;
            //}
            //else
            //{
            //    return ""; // файл запрещён
            //}
        }

        protected override StorageTypeEnum GetStorageType(FileHandler.File_Info info)
        {
            return StorageTypeEnum.FileServer;
        }
    }
}