﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.Controls.Extensions;
using GB.Extentions;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Extensions;
using GB.MKRF.Repository;
using GB.Providers;
using GB.Repository;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.Data;
using GB.Controls;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Web.Plans.v2012new;
using NHibernate.Criterion;

namespace GB.MKRF.Web.Administrator
{
    public partial class Users : System.Web.UI.Page
    {
       
        
        protected override void OnPreRender(EventArgs e)
        {
            UsersGrid.DataBind();
            base.OnPreRender(e);
        }

        protected bool isNewRow = false;
        protected void UsersGrid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
        {
            isNewRow = true;
        }

        protected void UsersGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            UsersGrid.DataSource = UserRepository.GetAll().OfType<User>().Select(u => new
            {
                u.Id,
                u.Login,
                Roles = GetRolesNames(u.Roles),
                u.Name,
                Department = u.Department.Name,
                u.WorkPhone,
                u.MobPhone,
                IsEsia = u.Snils.Return(shils => shils.Length == 14, false),
                u.IsActive
            }
            ); 
        }

        protected User currentUser = null;
        protected void UsersGrid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            currentUser = RepositoryBase<User>.Get(e.EditingKeyValue);
        }

        protected void UsersGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            User user = new User();
            saveUser(user);
            UsersGrid.GridCancelEdit(e);
        }

        protected void UsersGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            User user = RepositoryBase<User>.Get((int)e.Keys[0]);
            saveUser(user);
            UsersGrid.GridCancelEdit(e);
        }

        protected void UsersGrid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            RepositoryBase<PlansActivityReason>.Delete((int)e.Keys[0]);
            UsersGrid.GridCancelEdit(e);
        }

        void saveUser(User user)
        {
            if (user.Id == EntityBase.UnsavedId)
            {
                var login = Convert.ToString(hf["Login"]);
                if (string.IsNullOrEmpty(login))
                    throw new ApplicationException("Укажите логин пользователя");
                user.Login = login;
                user.IsActive = true;
            }

            user.IsActive = Convert.ToBoolean(hf["Active"]);
            user.Name = Convert.ToString(hf["FirstName"]);
            user.Middlename = Convert.ToString(hf["MiddleName"]);
            user.Surname = Convert.ToString(hf["SurName"]);
            user.Email = Convert.ToString(hf["Email"]);
            user.WorkPhone = Convert.ToString(hf["WorkPhone"]);
            user.MobPhone = Convert.ToString(hf["MobPhone"]);

            var roles = hf["Roles"] as IEnumerable;

            var rolesIds = roles.ToListInt();


            int id;
            Int32.TryParse(hf["Department"].ToString(), out id);
            user.Department = RepositoryBase<Department>.Get(id);

            if (hf.Contains("Password") && !string.IsNullOrEmpty(Convert.ToString(hf["Password"])))
            {
                user.Password = UviriProvider.GetMD5Hash(hf["Password"].ToString());
            }

            UserRepository.Save(user, rolesIds);
        }

        protected void TextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox tbox = (ASPxTextBox)sender;
            if (isNewRow)
            {
                switch (tbox.ID)
                {
                    case "Login_TextBox":
                        tbox.ReadOnly = false;
                        break;
                }

                return;
            }


            if (currentUser == null)
                return;

            switch (tbox.ID)
            {
                case "Email_TextBox":
                    tbox.Text = currentUser.Email;
                    break;

                case "SurName_TextBox":
                    tbox.Text = currentUser.Surname;
                    break;

                case "FirstName_TextBox":
                    tbox.Text = currentUser.Name;
                    break;

                case "MiddleName_TextBox":
                    tbox.Text = currentUser.Middlename;
                    break;

                case "Login_TextBox":
                    tbox.Text = currentUser.Login;
                    break;

                case "Pass_TextBox":
                    tbox.Text = "************************";
                    break;

                case "Snils_TextBox":
                    tbox.Text = currentUser.Snils;
                    break;

                case "WorkPhone_TextBox":
                    tbox.Text = currentUser.WorkPhone;
                    break;

                case "MobPhone_TextBox":
                    tbox.Text = currentUser.MobPhone;
                    break;

                default:
                    throw new ArgumentException(tbox.ID);
            }
        }

        protected void IsActive_CheckBox_DataBinding(object sender, EventArgs e)
        {
            if (currentUser == null)
            {
                (sender as ASPxCheckBox).Checked = true;
                return;
            }

            (sender as ASPxCheckBox).Checked = currentUser.IsActive;
        }

        protected void Role_ListBox_OnDataBinding(object sender, EventArgs e)
        {
            sender.SetDataListBox(RepositoryBase<Role>.GetAll());
        }

        protected void Role_ListBox_OnDataBound(object sender, EventArgs e)
        {
            ASPxListBox box = (ASPxListBox)sender;

            if (isNewRow)
            {
                return;
            }

            currentUser.With(currUser => currUser.Roles).Do(roles =>
            {
                var rolesIds = roles.Select(r => r.Id);
                int value;

                foreach (ListEditItem item in box.Items)
                {
                    if (int.TryParse(item.Value.ToString(), out value) 
                        && rolesIds.Contains(value))
                    {
                        item.Selected = true;
                    }
                }
            });
        }

        protected void comboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = (ASPxComboBox)sender;

            switch (box.ID)
            {
                case "Department_ComboBox":
                    box.DataSource = RepositoryBase<Department>.GetAll();
                    if (!isNewRow && currentUser != null && currentUser.Department != null)
                        box.Value = currentUser.Department.Id;
                    break;

                default:
                    throw new ArgumentException(box.ID);
            }
        }

        protected string GetRolesNames(IList<GB.Entity.RoleBase> roles)
        {
            var result = string.Empty;

            roles.Do(items =>
            {
                var names = items.Select(role => role.Name);
                result = string.Join(", ", names);
            });

            return result;
        }

    }
}