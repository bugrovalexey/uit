﻿using Uviri.MKRF.Entities.Prognosis;
using Uviri.MKRF.Entities.Prognosis.SectionIII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionIII
{
	public class AccessNeedMap : ClassMapBase<AccessNeed>
	{
        public AccessNeedMap()
            : base("MF3_17")
        {
//            Table("w_");

//            Id(x => x.Id, "MF3_17_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_MF3_17_Insert
//                          @MF3_17_Num = ?
//                         ,@MF3_17_1 = ?
//                         ,@MF3_17_2 = ?
//                         ,@MF3_17_3 = ?
//						 ,@MF3_17_4 = ?
//						 ,@MF3_17_5 = ?
//						 ,@Mon_Form_ID = ?
//                         ,@MF3_17_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF3_17_Update
//                          @MF3_17_Num = ?
//                         ,@MF3_17_1 = ?
//                         ,@MF3_17_2 = ?
//                         ,@MF3_17_3 = ?
//						 ,@MF3_17_4 = ?
//						 ,@MF3_17_5 = ?
//						 ,@Mon_Form_ID = ?
//                         ,@MF3_17_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF3_17_Delete
//						  @MF3_17_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "MF3_17_Num");
			Map(x => x.FOIVFunc, "MF3_17_1");
			Map(x => x.AccessOrder, "MF3_17_2");
            Map(x => x.Department, "MF3_17_3");
			Map(x => x.ISName, "MF3_17_4");
			Map(x => x.Condition, "MF3_17_5");

			References<Prognos>(x => x.Prognos, "Mon_Form_ID").Not.Nullable();
			//References<Department>(x => x.Department, "Govt_Organ_ID_17_3").Not.Nullable();
        }
    }
}