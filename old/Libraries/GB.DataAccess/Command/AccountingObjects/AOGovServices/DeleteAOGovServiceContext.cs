﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServices
{
    public class DeleteAOGovServiceContext : ICommandContext
    {
        public int? xId { get; set; }
    }
}
