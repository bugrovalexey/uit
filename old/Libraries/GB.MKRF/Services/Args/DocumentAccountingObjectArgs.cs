﻿using GB.Data.AccountingObjects;

namespace GB.MKRF.Services.Args
{
    public class DocumentAccountingObjectArgs : BaseArgsNew
    {
        /// <summary>
        /// Объект учета
        /// </summary>
        public AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Id объекта учета
        /// </summary>
        public int AccountingObjectId { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// ФИО подписавшего
        /// </summary>
        public string FioSigner { get; set; }

        /// <summary>
        /// Должность подписавшего
        /// </summary>
        public string PostSigner { get; set; }

        /// <summary>
        /// URL на документ
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public string Files { get; set; }
    }
}