﻿INSERT INTO ClassificationСategory (ClassificationСategory_ID, CC_Name, CC_Parent_ID)
VALUES
(1,'Управляющие ИТ-сервисы',null),
(2,'Основные ИТ-сервисы',null),
(3,'Обеспечивающие ИТ-сервисы',null),

(4,'Корпоративное управоение',1),
(5,'Бизнес-аналитика',1),
(6,'Системы бюджетного управления',1),

(7,'Управление продажами',2),
(8,'Управление исполнением заказа',2),
(9,'Управление логистикой',2),

(10,'ИТ-обеспечение',3),
(11,'Эксплуатация парка вагонов и контейнеров',3),
(12,'Эксплуатация имущества',3),
(13,'Управление персоналом',3),
(14,'Системы регламентированного учёта',3),
(15,'Правовые информационные системы',3),
(16,'Добавить сети и т.д.',3);