﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class StatusPMap : ClassMapBase<StatusP>
	{
        public StatusPMap()
            : base("StatusP")
		{
//            Table("w_");

//            Id(x => x.Id, "StatusP_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            SqlInsert(@"exec ZZZ_Prc_StatusP_Insert
//						  @StatusP_Name = ?
//						 ,@StatusP_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_StatusP_Update
//						  @StatusP_Name = ?
//						 ,@StatusP_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_StatusP_Delete
//						  @StatusP_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Name, "StatusP_Name").Nullable();
        }
    }
}
