﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.ExternalSystems.Zakupki360
{
    interface IClientSystemsFactory
    {
        IClientSystems Create();
    }
}
