﻿///Наша обёртка над родным гридом datatables.net
///требует подключеные библиотеки jquery.js и jquery.dataTables.js
///Переопределены некоторые "родные" свойства, которые будут применятся у всех таблиц, например вс надписи
///Также реализована возможность переопределения некоторых индивидуальных параметров для конкретной таблицы по средством конфига
///Функция BuildDataTable обёртывает стандартный html-элемент
///Параметры table - jquery объект нужной таблицы; 
///advConfig - конфиг, расширяющий функционал. см. jquery.dataTables.js объект DataTable.defaults (стр. 7831) 
///также имеет расширенные свойства:
/// toolbar - jquery селектор, описывающий элемент страницы, который нужно поместить в контейнер непосредственно над таблицей
/// multiSelect - если true то добавляет выбранным строкам css класс row_selected, что позволяет определить выбранные строки
///               если false то по клику по строке начинается обработка события OnRowClick (по умолчанию переход на страницу редактирования записи)
///               по умолчанию false
/// OnRowClick - событие на клик строки в таблице
/// OnMouseOver - событие на наведение курсора на строку в таблице
/// OnMouseOut - событие на выход курсора со строки в таблице
/// см. примеры вызова
///
///
///Примеры вызова:
///Грид по умолчанию: BuildDataTable($("#grid")); - где grid это id аттрибут нужной таблицы
///Грид с параметрами (Таблица справочников):
//BuildDataTable($('#dictGrid'), {
//    "OnRowClick": function (nRow, aData, iDisplayIndex, iDisplayIndexFull, clickLink) {
//        if (clickLink)
//            window.location = clickLink;
//    }
//});
/// Таблица выбора значений справочника:
//BuildDataTable($('#chooseGrid'), {
//    "bAutoWidth": false,
//    "iDisplayLength": 10,
//    "OnRowClick": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//        var tr = $(nRow);
//        var id = tr.data('id');
//        var primaryKeyName = tr.data("name");
//        $('input[name=hf_' + primaryKeyName + ']').val(id);
//        var tds = tr.find('td');
//        $.each(tds, function (i, e) {
//            var name = $(e).data("name");
//            if (i == 0)
//                $('input[name=' + primaryKeyName + ']').val(aData[i]);
//            else
//                $('input[name=' + name + ']').val(aData[i]);
//        });
//        popup.close();
//    },
//    "OnMouseOver": function (nRow) {
//        $(nRow).css('cursor', 'pointer');
//    },
//    "OnMouseOut": function (nRow) {
//        $(nRow).css('cursor', '');
//    }
//});

function BuildDataTable(table, advConfig) {

    var filters = "";

    var aoColumns = [];
    $.each($(table).find('tr').first().find('th'), function (i, e) {
        var column = {
            "bSortable": $(e).hasClass('not-sortable') ? false : true,
            "bSearchable": $(e).hasClass('not-searchable') ? false : true,
            "sSortDataType": $(e).hasClass('checkboxColumn') ? "dom-checkbox" : ""
        };
        aoColumns.push(column);
    });


    $.fn.dataTableExt.oPagination.custom_buttons = {
        "fnInit": function (oSettings, nPaging, fnCallbackDraw) {
            var oLang = oSettings.oLanguage.oPaginate;
            var oClasses = oSettings.oClasses;
            var fnClickHandler = function (e) {
                if (oSettings.oApi._fnPageChange(oSettings, e.data.action)) {
                    fnCallbackDraw(oSettings);
                }
            };

            $(nPaging).append(
                '<a  tabindex="' + oSettings.iTabIndex + '" class="' + oClasses.sPageButton + " " + oClasses.sPagePrevious + '"><div class="arrow_back">←</div>' + oLang.sPrevious + '</a>' +
                '<span style="margin: 0 5px"></span>' +
                '<a tabindex="' + oSettings.iTabIndex + '" class="' + oClasses.sPageButton + " " + oClasses.sPageNext + '">' + oLang.sNext + '<div class="arrow_back">→</div></a>'
            );
            var els = $('a', nPaging);
            var nPrev = els[0],
                nNext = els[1];

            oSettings.oApi._fnBindAction(nPrev, { action: "previous" }, fnClickHandler);
            oSettings.oApi._fnBindAction(nNext, { action: "next" }, fnClickHandler);

            /* ID the first elements only */
            if (!oSettings.aanFeatures.p) {
                nPaging.id = oSettings.sTableId + '_paginate';
                nPrev.id = oSettings.sTableId + '_previous';
                nNext.id = oSettings.sTableId + '_next';
            }
        },
        "fnUpdate": $.fn.dataTableExt.oPagination.full_numbers.fnUpdate
    };

    $.fn.dataTableExt.oApi.fnGetColumnData = function (oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty) {
        // check that we have a column id
        if (typeof iColumn == "undefined") return new Array();

        // by default we only want unique data
        if (typeof bUnique == "undefined") bUnique = true;

        // by default we do want to only look at filtered data
        if (typeof bFiltered == "undefined") bFiltered = true;

        // by default we do not want to include empty values
        if (typeof bIgnoreEmpty == "undefined") bIgnoreEmpty = true;

        // list of rows which we're going to loop through
        var aiRows;

        // use only filtered rows
        if (bFiltered == true) aiRows = oSettings.aiDisplay;
            // use all rows
        else aiRows = oSettings.aiDisplayMaster; // all row numbers

        // set up data array   
        var asResultData = new Array();

        for (var i = 0, c = aiRows.length; i < c; i++) {
            iRow = aiRows[i];
            var aData = this.fnGetData(iRow);
            var sValue = aData[iColumn];

            // ignore empty values?
            if (bIgnoreEmpty == true && sValue.length == 0) continue;

                // ignore unique values?
            else if (bUnique == true && jQuery.inArray(sValue, asResultData) > -1) continue;

                // else push the value onto the result data array
            else asResultData.push(sValue);
        }

        return asResultData;
    };

    var aSelected = [];

    var defaultConfig = {
        "aoColumns": aoColumns,
        "aaSorting": [],
        "bLengthChange": false,
        "sPaginationType": "custom_buttons",
        "iDisplayLength": 10,
        "bInfo": false,
        "bPaginate": false,
        "sDom": '<"toolbar">rtip',
        "oLanguage": {
            "oAria": {
                "sSortAscending": ": сортировка по возрастанию",
                "sSortDescending": ": сортировка по убыванию"
            },
            "oPaginate": {
                "sFirst": "Первая",
                "sLast": "Последняя",
                "sNext": "Следующая",
                "sPrevious": "Предыдущая"
            },
            "sEmptyTable": "Нет данных для отображения",
            "sInfo": "Показано с _START_ по _END_ запись из _TOTAL_",
            "sInfoEmpty": "Показано 0 записей",
            "sInfoFiltered": "(выбрано из _MAX_ записей)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Показывать по _MENU_ записей",
            "sLoadingRecords": "Загрузка...",
            "sProcessing": "Обработка...",
            "sSearch": "Фильтр:",
            "sUrl": "",
            "sZeroRecords": "Не найдено данных"
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var $nRow = $(nRow);

            $nRow.off(); //сначала отписываемся от всех событий, которые были до этого (нужно для того, чтобы не дублировались хэндлеры при сортировке например)
            $nRow.live('mouseover', function (e) {
                var link = $nRow.data('clicklink');
                if (advConfig !== undefined && advConfig.OnMouseOver) {
                    advConfig.OnMouseOver(nRow, aData, iDisplayIndex, iDisplayIndexFull, link);
                }
                else {
                    if (link) $nRow.css("cursor", "pointer");
                }
            });

            $nRow.live('mouseout', function (e) {
                var link = $nRow.data('clicklink');
                if (advConfig !== undefined && advConfig.OnMouseOut) {
                    advConfig.OnMouseOut(nRow, aData, iDisplayIndex, iDisplayIndexFull, link);
                }
                else {
                    if (link) $nRow.css("cursor", "");
                }
            });

            if (advConfig !== undefined && advConfig.multiSelect) {
                if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
                    $nRow.addClass('row_selected');
                }

                $nRow.live('click', function (e) {
                    var id = this.id;
                    var index = $.inArray(id, aSelected);

                    if (index === -1) {
                        aSelected.push(id);
                    } else {
                        aSelected.splice(index, 1);
                    }

                    $(this).toggleClass('row_selected');
                    if (advConfig.OnMultiSelectRowClick)
                        advConfig.OnMultiSelectRowClick(nRow, aData, iDisplayIndex, iDisplayIndexFull);
                    e.preventDefault();
                });
            }
            else {
                var link = $nRow.data('clicklink');
                $nRow.live('click', function (e) {
                    if (e.target.nodeName != 'A') {
                        if (advConfig.OnRowClick)
                            advConfig.OnRowClick(nRow, aData, iDisplayIndex, iDisplayIndexFull, link);
                        else {
                            if (link)
                                window.location = link;
                        }
                    }
                });
            }
        }
    };

    var config = defaultConfig;
    if (advConfig)
        config = $.extend(true, defaultConfig, advConfig);
    
    if(advConfig.showFooter) {
        config.bInfo = true,
        config.bPaginate = true
    }

    var dataTable = $(table).dataTable(config);

    var toolbarContainer = $(table).siblings('div.toolbar');
    //toolbarContainer.css('margin-top','20px');
    var toolbarContent = "<div style='position: relative;'>";

    if (advConfig && advConfig.toolbar) {
        var toolbarOuter = $(advConfig.toolbar);
        if (toolbarOuter.length != 0) {
            toolbarContent += toolbarOuter.html();
            toolbarOuter.remove();
        }
    }


    $.each($(table).find('tr').first().find('th'), function (i, e) {
        if (!$(e).hasClass('not-searchable')) {
            var text = $(e).text();
            if ($(e).hasClass('selectable')) {
                filters += fnCreateSelect(dataTable.fnGetColumnData(i), text);
                $('select[name="' + text + '"]').live('change', function () {
                    var $this = $(this);
                    if ($this.val() != '') {
                        $this.removeClass("search_init");
                    }
                    else {
                        $this.addClass("search_init");
                    }
                    dataTable.fnFilter($(this).val(), i);
                    TogglerToClearButton();
                });
            }
            else {
                filters += "<input type='text' name='" + text + "' value='" + text + "' class='search_init filtercontrol'/>";
            }
            //if ((i + 1) % 3 == 0)
            //    filters += "<br/>";
        } else {
            filters += "<input type='hidden' class='filtercontrol'/>";
        }
    });

    //инициализируемый контейнер с фильтрами
    toolbarContent += "<div class='filterToggler' style='position: absolute; right: 0; bottom: 0;" +
        "'><a class='command'>Подробный фильтр</a></div></div>" +
        //"<div style='clear: both;'></div>" +
        "<div class='filter_panel' style='display: none;'></div>";

    toolbarContainer.append(toolbarContent);
    
    if (!advConfig.showDefaultFilter) {
        toolbarContainer.find('.filterToggler').hide();
    }

    var filterPanel = toolbarContainer.find('.filter_panel');

    //TODO : Нигде не используется
    if (advConfig.FilterContent) {
        toolbarContainer.find('.filter_panel').append(advConfig.FilterContent);
    }
    else {
        toolbarContainer.find('.filter_panel').append(filters);
    }

    var filterinputs = filterPanel.find('input');
    var filterselects = filterPanel.find('select');
    var filtercontrols = filterPanel.find('.filtercontrol');
    filterinputs.keyup(function (e) {
        /* Filter on the column (the index) of this element */
        dataTable.fnFilter(this.value, filtercontrols.index(this));
        TogglerToClearButton();
    });

    var asInitVals = new Array();
    filtercontrols.each(function (i) {
        asInitVals[i] = this.value;
    });

    filterinputs.change(function () {
        if ($(this).val() == '')
            FilterInputDefaultify(this);
        TogglerToClearButton();
    });

    filterinputs.focus(function () {
        var $this = $(this);
        if ($this.hasClass("search_init")) {
            $this.removeClass("search_init");
            $this.val("");
        }
    });

    filterinputs.blur(function (i) {
        if (this.value == "") {
            FilterInputDefaultify(this);
        }
    });

    toolbarContainer.find('.filterToggler a').click(function () {
        var $this = $(this);
        if ($this.text() == "Подробный фильтр") {
            $this.text('Cкрыть фильтр');
            $this.parent().css('background-color', '#EEEECC');
            filterPanel.slideDown(400);
        } else if ($this.text() == "Сбросить фильтр") {
            $this.text('Cкрыть фильтр');
            $.each(filterinputs, function (i, e) {
                if ($(this).is(':not(:disabled)'))
                    FilterInputDefaultify(this);
            });
            $.each(filterselects, function (i, e) {
                if ($(this).is(':not(:disabled)'))
                    FilterSelectDefaultify(this);
            });
        } else {//else if (this.text == "Cкрыть фильтр") {
            $this.text('Подробный фильтр');
            $this.parent().css('background-color', 'transparent');
            filterPanel.slideUp(400);
        }

        if (advConfig.OnFilterTogglerClick)
            advConfig.OnFilterTogglerClick();
    });

    function TogglerToClearButton() {
        if (filterPanel.is(':hidden')) {
            return;
        }

        var toClearButton = false;
        $.each(filterinputs, function (e, i) {
            if ($(this).val() != '' && !$(this).hasClass('search_init')) {
                toClearButton = true;
                return;
            }
        });
        $.each(filterselects, function (e, i) {
            if ($(this).val() != '') {
                toClearButton = true;
                return;
            }
        });

        if (toClearButton) {
            toolbarContainer.find('.filterToggler a').text('Сбросить фильтр');
        }
        else {
            toolbarContainer.find('.filterToggler a').text('Скрыть фильтр');
        }
    }
    function FilterInputDefaultify(filter) {
        var $filter = $(filter);
        $filter.addClass("search_init");
        $filter.val(asInitVals[filtercontrols.index(filter)]);
        dataTable.fnFilter('', filtercontrols.index(filter));
    }
    function FilterSelectDefaultify(filter) {
        var $filter = $(filter);
        $filter.addClass("search_init");
        $filter.find("option:first").attr("selected", "selected");
        dataTable.fnFilter('', filtercontrols.index(filter));
    }

    function fnCreateSelect(aData, text) {
        var r = '<select name="' + text + '" style="width: 32%; margin: 6px;" class="search_init filtercontrol"><option value="" selected="selected">' + text + '...</option>', i, iLen = aData.length;
        for (i = 0 ; i < iLen ; i++) {
            r += '<option value="' + aData[i] + '">' + aData[i] + '</option>';
        }
        return r + '</select>';
    }

    return dataTable;
}

function ClearFilters(grid) {
    $.each($('.filter_panel .filtercontrol', grid.parent()), function (i, e) {
        $(e).attr('disabled', false);
        $(e).css('visibility', 'visible');
        $(e).addClass('search_init');
        $(e).val('');
        $(e).change();
        grid.fnFilter('', i);
    });
    grid.fnFilter('');
}

function ToogleActiveToolbarLink(link) {
    $('.gridToolbarLink').removeClass('active');
    $(link).addClass('active');
}

function SetFilterStaticValue(filter, value) {
    $(filter).val(value);
    $(filter).removeClass('search_init');
    $(filter).attr('disabled', true);
}

function SetFilterInvisible(filter) {
    $(filter).removeClass('search_init');
    $(filter).attr('disabled', true);
    $(filter).css('visibility', 'hidden');
}


///
///Функция для группирования в гриде
///
function GroupRow(gridName, oSettings, indexColumnGroup, indexColumnName) {

    if (oSettings.aiDisplay.length == 0) {
        return;
    }

    var nCell;
    var groupIndex = 0;
    var nTrs = $('#' + gridName + ' tbody tr');
    var iColspan = nTrs[0].getElementsByTagName('td').length;
    var sLastGroup = '';

    for (var i = 0 ; i < nTrs.length ; i++) {
        var iDisplayIndex = oSettings._iDisplayStart + i;
        var sGroup = oSettings.aoData[oSettings.aiDisplay[iDisplayIndex]]._aData[indexColumnGroup];

        if (sGroup != sLastGroup) {
            groupIndex++;
            var nGroup = document.createElement('tr');
            nCell = document.createElement('td');
            nCell.colSpan = iColspan;
            nCell.className = 'group';
            nCell.innerHTML = oSettings.aoData[oSettings.aiDisplay[iDisplayIndex]]._aData[indexColumnName];
            nGroup.appendChild(nCell);
            nTrs[i].parentNode.insertBefore(nGroup, nTrs[i]);
            sLastGroup = sGroup;

        }

        $(nTrs[i]).addClass('rowgroup' + groupIndex);
        //$(nTrs[i]).attr('style', 'display: none');
    }
}