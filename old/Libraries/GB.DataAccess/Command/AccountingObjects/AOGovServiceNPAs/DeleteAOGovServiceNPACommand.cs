﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServiceNPAs
{
    public class DeleteAOGovServiceNPACommand : ICommand<DeleteAOGovServiceNPAContext>
    {
        ISessionFactoryGb factory;

        public DeleteAOGovServiceNPACommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteAOGovServiceNPAContext context)
        {            
            if (context.xId.HasValue && context.xId != 0)
            {
                var entity = new Repository<AOGovServiceNPA>().Get(context.xId.Value);

                if (entity != null)
                {
                    using (var session = factory.Create())
                    {                        
                        session.Delete(entity);
                        session.Commit();
                    }
                }
            }
        }
    }
}
