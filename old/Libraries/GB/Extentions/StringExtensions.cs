﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GB.Extentions
{
    public static class StringExtensions
    {
        public static string GetExtension(this string fileName)
        {
            return Path.GetExtension(fileName).Return(ext => ext.Replace(".", String.Empty));
        }


        public static string ReturnThisOrDefault(this string str, string @default)
        {
            if (string.IsNullOrWhiteSpace(str))
                return @default;

            return str;
        }

    }
}
