﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    internal class IKTComponentMap : ClassMapHiLo<IKTComponent>
    {
        public IKTComponentMap()
            : base("IS_or_IKT")
        {
            //Table("w_");

            //Id(x => x.Id, "IKT_Component_ID").GeneratedBy.Custom(typeof(IdGenerator));
        }

        protected override void InitMap()
        {
            Map(x => x.Name, " IS_or_IKT_Name");
            Map(x => x.Code, " IS_or_IKT_Code");
            Map(x => x.DateStart, "IS_or_IKT_Start_Date");
            Map(x => x.DateEnd, "IS_or_IKT_End_Date");
            References(x => x.Parent, " IS_or_IKT_Parent").Nullable();

            //HasMany(x => x.Child).KeyColumns.Add("IS_or_IKT_Parent").Inverse();
            HasMany(x => x.AccountingObjects).KeyColumns.Add("OU_Kind_ID").Inverse();
        }
    }
}