﻿using bgTeam.IsRui.Domain;
using System.Collections.Generic;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.Shared
{
    public class SelectEntityViewModel
    {
        public string Name { get; set; }

        public string SelectUrl { get; set; }

        public string NameUrl
        {
            get { return string.Concat(Name, "_URL"); }
        }

        private IList<IEntityName> _data;

        public IList<IEntityName> Data
        {
            get
            {
                if (_data == null)
                    _data = new List<IEntityName>();

                return _data;
            }
            set
            {
                _data = value;
            }
        }

        public string Value
        {
            get
            {
                return string.Join(",", Data.Select(s => s.Id));
            }
        }

        public bool IsMultipleSelection { get; set; }

        public bool ReadOnly { get; set; }

        public bool Required { get; set; }

        public bool IsRequired { get; set; }

        public string SelectEntityCallback { get; set; }
    }
}