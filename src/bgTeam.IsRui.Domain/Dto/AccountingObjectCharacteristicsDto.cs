﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AccountingObjectCharacteristicsDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public virtual AccountingObjectCharacteristicsTypeDto Type { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public virtual UnitsDto Unit { get; set; }

        /// <summary>
        /// Номинальное значение
        /// </summary>
        public virtual int Norm { get; set; }

        /// <summary>
        /// Максимальное значение
        /// </summary>
        public virtual int? Max { get; set; }

        /// <summary>
        /// Фактическое значение
        /// </summary>
        public virtual int? Fact { get; set; }

        /// <summary>
        /// Дата начала
        /// </summary>
        public virtual DateTime? DateBeginning { get; set; }

        /// <summary>
        /// Дата конца
        /// </summary>
        public virtual DateTime? DateEnd { get; set; }

        /// <summary>
        /// Зарегистрирована в ОУ
        /// </summary>
        public virtual bool IsRegister { get; set; }
    }
}
