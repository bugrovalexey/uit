﻿namespace GB.MKRF.Services.Args
{
    public class PlansActivityMarkArgs : BaseIndicatorArgs
    {
        /// <summary>
        /// True - когда источник формирования показателя - Госпрограмма
        /// False - Другой источник
        /// </summary>
        public virtual bool SourceIsStateProgram { get; set; }

        /// <summary>
        /// Наименование источника формирования
        /// </summary>
        public virtual string SourceName { get; set; }

        /// <summary>
        /// Пояснение связи с показателем госпрограммы
        /// </summary>
        public virtual string ExplanationRelationshipWithStateProgramMark { get; set; }

        /// <summary>
        /// Обоснование
        /// </summary>
        public string Justification { get; set; }

        /// <summary>
        /// ИД показателя гос. программы.
        /// </summary>
        public int? StateProgramMarkId { get; set; }
    }
}