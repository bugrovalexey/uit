﻿using GB.MKRF.Entities.Directories;
using System;
using System.Collections.Generic;

namespace GB.MKRF.Entities.Plans
{
    public class PlansZakupki : EntityBase
    {
        public PlansZakupki()
        {
        }

        public PlansZakupki(PlansActivity activity)
        {
            Activity = activity;
        }

        public virtual int Number { get; protected set; }

        public virtual PlansZakupkiStatus Status { get; set; }

        public virtual OrderWay OrderWay { get; set; }

        public virtual PlansActivity Activity { get; protected set; }

        public virtual DateTime? PublishDate { get; set; }

        public virtual string Name { get; set; }

        public virtual string IzvNumb { get; set; }

        public virtual DateTime? IzvDate { get; set; }

        public virtual DateTime? OpenDate { get; set; }

        public virtual DateTime? ConsDate { get; set; }

        public virtual DateTime? ItogDate { get; set; }

        public virtual string Site { get; set; }

        public virtual IList<PlansLots> Lots { get; set; }

        public virtual string NumberName
        {
            get
            {
                return string.Concat(Number, " - ", Name);
            }
        }
    }
}