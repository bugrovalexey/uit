﻿using System;
using System.Collections.Generic;
using System.Data;

namespace GB.MKRF.DictyonaryEdit
{
    /// <summary>
    /// Предоставляет метаданные описывающие справочник
    /// </summary>
    public class MetaDictionary
    {
        //системный столбец определяющий разрешение на удаление записи
        internal const string INUSE = "In_Use";

        //столбец определяющий первичный ключ
        public const string PRIMARY_KEY_NAME = "ID";

        //public MetaDictionaryColumn PrimaryKey { get; protected set; }

        public Dictionary<int, MetaDictionaryColumn> Columns { get; set; }

        public MetaDictionaryColumn PrimaryKey { get; set; }

        public DataTable Data { get; set; }

        public int ID { get; protected set; }

        public string Name { get; protected set; }

        public string ViewName { get; protected set; }

        public bool IsUse_InUse { get; protected set; }

        public string ProcedureInsert { get; protected set; }

        public string ProcedureUpdate { get; protected set; }

        public string ProcedureDelete { get; protected set; }

        public bool Visible { get; protected set; }

        public bool AddEnable { get { return (AccessType & (int)EnableAccessType.Add) == (int)EnableAccessType.Add; } }

        public bool EditEnable { get { return (AccessType & (int)EnableAccessType.Edit) == (int)EnableAccessType.Edit; } }

        public bool DeleteEnable { get { return (AccessType & (int)EnableAccessType.Delete) == (int)EnableAccessType.Delete; } }

        public bool ReadOnly { get { return !(AddEnable || EditEnable || DeleteEnable); } }

        public int AccessType { get; set; }
    }

    public enum ColumnType
    {
        TString,
        TBigString,
        TInt,
        TDecimal,
        TRef,
        TChild,
        TBool,
        TDateTime
    }

    [Flags]
    public enum EnableAccessType
    {
        None = 0,
        Add = 1,
        Edit = 2,
        Delete = 4
    }

    /// <summary>
    /// Предоставляет метаданные описывающие столбец справочника
    /// </summary>
    public class MetaDictionaryColumn
    {
        public int ID { get; set; }

        public int Dictionary_ID { get; protected set; }

        public string Name { get; protected set; }

        public string ColumnName { get; protected set; }

        public bool Visible { get; protected set; }

        public ColumnType Type 
        {
            get 
            {
                return (ColumnType)Enum.Parse(typeof(ColumnType), TypeString);
            }
        }

        public string TypeString { protected get; set; }

        public int RefId { get; protected set; }

        public int RefColumn { get; protected set; }

    }
}