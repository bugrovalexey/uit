﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class TypicalActivitiesMap : ClassMapBase<TypicalActivities>
    {
        public TypicalActivitiesMap() :
            base("Typical_Activities")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Typical_Activities_Name");
        }
    }
}