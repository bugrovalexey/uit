﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.WebApp.Helpers;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class TableAOGovServices : NotificationBaseViewModel
    {
        #region consts

        public const string NameCaption = "Наименование государственной услуги (функции)";
        public const string PercentOfUseResourceCaption = "Процент использования ресурсов ИС при автоматизации исполнения государственной услуги (функции)";

        #endregion consts

        public TableAOGovServices(AOGovService entity)
        {
            Id = entity.Id;
            Name = entity.GovService.Return(gs => gs.Name, default(string));
            PercentOfUseResource = entity.PercentOfUseResource.ToPercentString();

            GovServicesNPA =
                new RepositoryDc<AOGovServiceNPA>().GetAllEx(q => q.Where(x => x.GovService.Id == entity.Id))
                    .Select(x => new TableAOGovServicesNPA(x));

        }

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(PercentOfUseResourceCaption)]
        public string PercentOfUseResource { get; set; }

        public IEnumerable<TableAOGovServicesNPA> GovServicesNPA { get; set; }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteAOGovServices", "AccountingObjects", new { id = Id }, "GridGovServices");
            }
        }

    }
}