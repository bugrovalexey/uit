﻿using FluentNHibernate.Mapping;
using GB.Data;
using GB.Data.Admin;

namespace GB._Mapping.Admin
{
    internal class SiteMapNodeMap : ClassMapHiLo<SiteMapNode>
    {
        public SiteMapNodeMap()
            : base("Site_Map_Node") { }

        protected override void InitMap()
        {
            //Table("Site_Map_Node");

            //Id(x => x.Id, "Site_Map_Node_ID");
            //.GeneratedBy.Increment(); - Ошибка IDENTITY_INSERT имеет значение OFF, так как явно пытаемся вставить значение
            //.GeneratedBy.Custom(typeof(IdGenerator)).UnsavedValue((int)EntityBase.UnsavedId);

            Map(x => x.Title, "Site_Map_Node_Title");
            Map(x => x.Controller, "Site_Map_Node_Controller");
            Map(x => x.Action, "Site_Map_Node_Action");
            Map(x => x.Description, "Site_Map_Node_Descr");
            Map(x => x.Url, "Site_Map_Node_Url");
            Map(x => x.Visible, "Site_Map_Node_Visible");
            Map(x => x.OrderIndex, "Site_Map_Node_Order");

            References<SiteMapNode>(x => x.Parent, "Site_Map_Node_Par_ID");

            HasMany<SiteMapNode>(x => x.Children).KeyColumns.Add("Site_Map_Node_Par_ID").Inverse().Cascade.AllDeleteOrphan();
            HasMany<SiteMapToRole>(x => x.Roles).KeyColumns.Add("Site_Map_Node_ID").Inverse();
        }
    }
}