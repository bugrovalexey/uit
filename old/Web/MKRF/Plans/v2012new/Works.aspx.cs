﻿using System;
using System.Collections.Generic;
using System.Linq;
using GB.Entity.Directories;
using GB.Helpers;
using DevExpress.Utils;
using DevExpress.Web.ASPxCallback;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.Data;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Helpers;
using GB.MKRF.Repository;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Web.Helpers;
using DevExpress.Data;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class Works : PlansActivityEditPage
    {
        protected PlansWorkController controller = new PlansWorkController();

        public override bool EnableEdit
        {
            get
            {
                return base.EnableEdit ||
                        Validation.Validator.Validate(new PlansWork() { PlanActivity = CurrentActivity }, ObjActionEnum.create);
            }
        }
        
        Dictionary<int, bool> _ListExpense;
        Dictionary<int, bool> ListExpense
        {
            get { return _ListExpense ?? (_ListExpense = controller.GetExpenseError(CurrentActivity)); }
        }

        public static string GetUrl(int ActId)
        {
            return UrlHelper.Resolve("~/Plans/v2012new/Works.aspx") + "?" + ActivityCard.ActivityIdKey + "=" + ActId;
        }

        protected override void OnPreRender(EventArgs e)
        {
            addWorksButton.Visible = EnableEdit;

            treeList.Columns["cmdColumn"].Visible = EnableEdit;
            treeList.Columns["copyColumn"].Visible = EnableEdit;

            base.OnPreRender(e);
        }


        protected override void OnInit(EventArgs e)
        {
            dc.Owner = CurrentActivity;

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            treeList.DataBind();
        }

        protected void OKDPTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null) 
                return;

            ASPxTextBox tb = sender as ASPxTextBox;
            tb.Value = String.IsNullOrWhiteSpace(controller.CurrentEdit.OKDPText) ? "" : controller.CurrentEdit.OKDPText;
        }

        protected void ExpenseDirection_Callback(object sender, CallbackEventArgsBase e)
        {
            IList<ExpenseType> list = controller.GetExpenseTypes(Convert.ToInt32(e.Parameter));

            var pxCallback = sender as ASPxCallback;
            if (!pxCallback.JSProperties.ContainsKey("cp_expenseTypeList"))
                pxCallback.JSProperties.Add("cp_expenseTypeList", string.Empty);
            pxCallback.JSProperties["cp_expenseTypeList"] = string.Format("[{0}]", string.Join(", ", list.Select(x => string.Format("{{\"Key\": \"{0}\", \"Value\": \"{1}\"}}", x.Id, x.Name))));
        }

        protected void treeList_DataBinding(object sender, EventArgs e)
        {
            treeList.DataSource = controller.GetWorkList(CurrentActivity);
        }

        protected void treeList_NodeUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            controller.Update(new PlansWorkControllerArgs
            {
                PlansWorkId = e.Keys[0],
                Values = hfWorks.ToDictionary(x => x.Key, x => x.Value)
            });

            RefreshActivity();
            treeList.TreeCancelEdit(e);
        }

        protected void treeList_NodeInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            controller.Create(new PlansWorkControllerArgs
                {
                    ParentActivity = CurrentActivity,
                    Values = hfWorks.ToDictionary(x => x.Key, x => x.Value)
                });

            RefreshActivity();
            treeList.TreeCancelEdit(e);
        }

        protected void treeList_StartNodeEditing(object sender, TreeListNodeEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.NodeKey);
        }

        protected void treeList_CommandColumnButtonInitialize(object sender, TreeListCommandColumnButtonEventArgs e)
        {
            ASPxTreeList tl = sender as ASPxTreeList;
            TreeListNode node = tl.FindNodeByKeyValue(e.NodeKey);
            if (node.Level > 1 && e.ButtonType != TreeListCommandColumnButtonType.Delete)
                e.Visible = DefaultBoolean.False;
        }

        protected void treeList_NodeDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            TreeListNode currentNd = ((ASPxTreeList)sender).FindNodeByKeyValue(e.Keys[0].ToString());

            if (EnableEdit)
            {
                if (currentNd.Level == 1)
                {
                    controller.Delete((int)e.Keys[0]);
                }
                else if (currentNd.Level == 2)
                {
                    controller.DeleteWorkType((int)e.Keys[0]);
                }
            }

            RefreshActivity();
            NumHelper<PlansWork>.OrderNums(CurrentActivity.Works);

//            treeList.TreeCancelEdit(e);
            treeList.DataBind();
        }


        protected void treeList_NodeDeleted(object sender, ASPxDataDeletedEventArgs e)
        {
            e.ExceptionHandled = true;
        }

        protected void treeList_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            controller.CopyWork(e.Argument);
            
            treeList.DataBind();
            RefreshActivity();
        }

        protected void treeList_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (e.RowKind == TreeListRowKind.Data)
            {
                var id = (int)e.GetValue("ExpenseDirectionID");

                if (ListExpense.ContainsKey(id))
                {
                    var index = (sender as ASPxTreeList).Columns["ExpenseVolumeYear0"].VisibleIndex;

                    if (e.Row.Cells.Count > index + 1)
                        e.Row.Cells[index + 1].Style.Add("color", "red");
                }
            }
        }

        protected void ExpenseDirectionComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;
            //if (box == null) return;

            var expenseDirections = controller.GetExpenseDirections(CurrentActivity);

            box.DataSource = expenseDirections.List;
            box.Value = expenseDirections.Selected;
        }

        protected void ExpenditureItemsComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = DirectoriesRepositary.GetDirectories<ExpenditureItem>(DateTime.Now.Year);

            if (controller.CurrentEdit == null || controller.CurrentEdit.ExpenditureItem == null)
            {
                box.Value = GB.EntityBase.Default;
            }
            else
            {
                box.Value = controller.CurrentEdit.ExpenditureItem.Id;
            }
        }

        protected void NameMemo_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            (sender as ASPxMemo).Text = controller.CurrentEdit.Name;
        }

        protected void treelest_CustomSummaryCalculate(object sender, TreeListCustomSummaryEventArgs e)
        {
            switch (e.SummaryProcess)
            {
                case CustomSummaryProcess.Start:
                    e.Value = (decimal)0;
                    break;
                case CustomSummaryProcess.Calculate:
                    if (e.Node.Level == 2)
                        e.Value = (decimal)e.Value + (decimal)e.Node["ExpenseVolumeYear0"];
                    break;
            }
        }

        protected void Memo_DataBinding(object sender, EventArgs e)
        {
            var memo = sender as ASPxTextBox;
            if (memo == null)
                return;
            
            if (controller.CurrentEdit == null || controller.CurrentEdit.ExpenditureItem == null)
            {
                memo.Text = "- Не задано";
//                hfWorks.Set("ExpenditureItemsId", 0);
//                hfWorks.Set("ExpenditureItemsName", string.Empty);
//                hfWorks.Set("ExpenditureItemsCode", string.Empty);
            }
            else
            {
                memo.Text = controller.CurrentEdit.ExpenditureItem.Code + " " + controller.CurrentEdit.ExpenditureItem.Name;
//                hfWorks.Set("ExpenditureItemsId", controller.CurrentEdit.ExpenditureItem.Id);
//                hfWorks.Set("ExpenditureItemsName", controller.CurrentEdit.ExpenditureItem.Name);
//                hfWorks.Set("ExpenditureItemsCode", controller.CurrentEdit.ExpenditureItem.Code);
            }
        }

        
    }
}