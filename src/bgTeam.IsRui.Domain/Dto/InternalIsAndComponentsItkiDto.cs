﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class InternalIsAndComponentsItkiDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование ИС
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Классификационная категория ОУ
        /// </summary>
        public string KindName { get; set; }

        /// <summary>
        /// Учреждение
        /// </summary>
        public string DepartmentName { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        public string Purpose { get; set; }
    }
}
