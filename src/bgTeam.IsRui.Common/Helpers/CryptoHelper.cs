﻿namespace bgTeam.IsRui.Common.Helpers
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    public static class CryptoHelper
    {
        /// <summary>Вычислить хеш исходного массива</summary>
        /// <param name="file">Массив</param>
        /// <returns>Хэш</returns>
        public static string ComputeHash(byte[] file)
        {
            if (file == null)
            {
                return null;
            }

            MD5 md5Hasher = MD5.Create();
            return Convert.ToBase64String(md5Hasher.ComputeHash(file));
        }

        public static string GetMD5Hash(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(value));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }
}