﻿using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Plans;
using Uviri.MKRF.Repository;

namespace Uviri.MKRF.Validator
{
    internal class Conclusion
    {
        /// <summary>Параметры для валидации заключения</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            ExpConclusion Item = (ExpConclusion)Entity;
            User CurrentUser = UserRepository.GetCurrent();
            if (Action == ObjActionEnum.create)
            {
                if (Item.Demand != null)
                    return "0;" + Item.Demand.Id.ToString();
                else
                    return null;
            }
            else if (Action == ObjActionEnum.CanChangeConclusionStatus)
            {
                return Action.ToString() + ";" + Entity.Id.ToString();
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
