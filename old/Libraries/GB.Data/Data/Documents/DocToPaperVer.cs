﻿namespace GB.MKRF.Entities.Documents
{
    /// <summary>Связь бумажной формы с документом</summary>
    public class DocToPaperVer : EntityBase
    {
        protected DocToPaperVer()
        {
        }

        public DocToPaperVer(PaperObjectVer PaperObject, DocumentVer Document, DocToPaperType Type)
        {
            this.PaperObject = PaperObject;
            this.Document = Document;
            this.Type = Type;
        }

        /// <summary>Бумажный план</summary>
        public virtual PaperObjectVer PaperObject { get; set; }

        /// <summary>Документ</summary>
        public virtual DocumentVer Document { get; set; }

        /// <summary>Тип связи</summary>
        public virtual DocToPaperType Type { get; set; }

        /// <summary>Файл является экспертным заключением</summary>
        public virtual bool IsExpertConclusion { get; set; }
    }
}