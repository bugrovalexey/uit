﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Threading.Tasks;

    public class WorkflowUserConclusionSaveOrUpdateStory : IStory<WorkflowUserConclusionSaveOrUpdateStoryContext, WorkflowUserConclusionDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IStatusRepository _statusRepository;

        private static object _lock = new object();

        public WorkflowUserConclusionSaveOrUpdateStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IUserRepository userRepository,
            ISessionNHibernateFactory factory,
            IStatusRepository statusRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _userRepository = userRepository;
            _factory = factory;
            _statusRepository = statusRepository;
        }

        public WorkflowUserConclusionDto Execute(WorkflowUserConclusionSaveOrUpdateStoryContext context)
        {
            return ExecuteAsync(context).GetAwaiter().GetResult();
        }

        public async Task<WorkflowUserConclusionDto> ExecuteAsync(WorkflowUserConclusionSaveOrUpdateStoryContext context)
        {
            if (context.ActivityId.HasValue)
            {
                WorkflowUserConclusion entity;

                lock (_lock) // TODO: зачем здесь лок?
                {
                    var curUser = _userRepository.GetCurrent();
                    var activity = _repository.GetExisting<PlansActivity>(context.ActivityId);

                    if (activity.Workflow == null)
                    {
                        using (var session = _factory.OpenSession())
                        {
                            new WorkflowCreateStory(_statusRepository)
                            .Execute(new WorkflowCreateStoryContext()
                            {
                                Entity = activity,
                                Session = session,
                            });
                        }
                    }

                    entity = _repository.Get<WorkflowUserConclusion>(q => q
                        .Where(x => x.WorkflowId == activity.Workflow.Id &&
                            x.StatusId == activity.Workflow.Status.Id &&
                            x.UserId == curUser.Id));

                    if (entity == null)
                    {
                        entity = new WorkflowUserConclusion()
                        {
                            WorkflowId = activity.Workflow.Id,
                            StatusId = activity.Workflow.Status.Id,
                            UserId = curUser.Id,
                            Text = context.Text,
                        };

                        using (var session = _factory.OpenSession())
                        {
                            session.Insert(entity);
                        }
                    }
                    else
                    {
                        using (var session = _factory.OpenSession())
                        {
                            entity.Text = context.Text;

                            session.Update(entity);
                        }
                    }
                }

                return _mapper.Map(entity, new WorkflowUserConclusionDto());
            }
            else if (context.Id.HasValue)
            {
                var entity = _repository.GetExisting<WorkflowUserConclusion>(context.Id.Value);

                using (var session = _factory.OpenSession())
                {
                    entity.Text = context.Text;

                    await session.UpdateAsync(entity);
                }

                return _mapper.Map(entity, new WorkflowUserConclusionDto());
            }

            throw new ArgumentNullException($"{nameof(context.ActivityId)} or {nameof(context.Id)}");

            //context.EntityType.CheckNull(nameof(context.EntityType));
            //context.EntityOwnerId.CheckNull(nameof(context.EntityOwnerId));
            //context.StatusId.CheckNull(nameof(context.StatusId));

            //WorkflowUserConclusion entity;
            //if (context.Id.HasValue)
            //{
            //    entity = _repository.Get<WorkflowUserConclusion>(context.Id.Value);
            //}
            //else
            //{
            //    var curUser = _userRepository.GetCurrent();

            //    entity = new WorkflowUserConclusion();
            //    entity.UserId = curUser.Id;
            //    //entity.EntityOwner = Domain.Extensions.EntityExtension.GetObj(context.EntityType, context.EntityOwnerId);
            //}

            //_mapper.Map(context, entity);
            //entity.TimeStamp = DateTime.Now;

            //using (var session = _factory.OpenSession())
            //{
            //    await session.SaveOrUpdateAsync(entity);
            //}

            //return _mapper.Map(entity, new WorkflowUserConclusionDto());
        }
    }
}
