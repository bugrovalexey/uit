﻿using FluentNHibernate.Mapping;
using GB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB._Mapping
{
    internal abstract class ClassMapHiLo<T> : ClassMap<T> where T : EntityBase
    {
        public ClassMapHiLo(string table)
        {
            Table(table);            

            Id(x => x.Id, table + "_ID")
                .GeneratedBy.HiLo("NH_HiLo", "NextHi", "1", "TableKey = '" + table + "'");

            InitMap();
        }

        /// <summary>
        /// Инициализировать маппинг
        /// </summary>
        protected abstract void InitMap();
    }
}
