﻿namespace bgTeam.IsRui.Story.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Common.Helpers;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Users;

    public class UserChangePasswordStory : IStory<UserChangePasswordStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;

        public UserChangePasswordStory(
            ISessionNHibernateFactory factory,
            IRepositoryRui repository)
        {
            _factory = factory;
            _repository = repository;
        }

        public bool Execute(UserChangePasswordStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(UserChangePasswordStoryContext context)
        {
            var user = _repository.Get<UsersEntity>(context.Id);

            user.Password = CryptoHelper.GetMD5Hash(context.NewPassword);

            using (var session = _factory.OpenSession())
            {
                await session.UpdateAsync(user);
            }

            return true;
        }
    }
}
