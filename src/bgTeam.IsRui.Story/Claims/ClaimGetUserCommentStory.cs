﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimGetUserCommentStory : IStory<ClaimGetUserCommentStoryContext, WorkflowUserConclusionDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;

        public ClaimGetUserCommentStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IUserRepository userRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _userRepository = userRepository;
        }

        public WorkflowUserConclusionDto Execute(ClaimGetUserCommentStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<WorkflowUserConclusionDto> ExecuteAsync(ClaimGetUserCommentStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка с идентификатором: {context.ClaimId}");
            }

            var curUser = _userRepository.GetCurrent();

            var comment = _repository.Get<WorkflowUserConclusion>(q => q
                    .Where(x => x.WorkflowId == claim.Workflow.Id &&
                        x.StatusId == claim.Workflow.Status.Id &&
                        x.UserId == curUser.Id));

            return _mapper.Map(comment, new WorkflowUserConclusionDto());
        }
    }
}
