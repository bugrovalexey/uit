﻿using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using GB.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IocTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = InitializeContainer();

            var t = container.Resolve<ITest>();

            t.Execute();

            Console.Write("OK");
            Console.ReadKey();
        }

        private static WindsorContainer InitializeContainer()
        {
            var container = new WindsorContainer();

            container.AddFacility<TypedFactoryFacility>();

            var command = Classes.FromAssemblyNamed("GB.DataAccess")
                                  .BasedOn(typeof(ICommand<>))
                                  .WithService.AllInterfaces()
                                  .LifestyleTransient();
            //.Configure().
            //.Configure(x => x.LifeStyle.Transient);

            container.Register(
                command,
                Component.For<ICommandBuilder>()
                         .ImplementedBy(typeof(CommandBuilder)),
                Component.For<ICommandFactory>().AsFactory().LifestyleSingleton());

            container.Register(Component.For<ITest>().ImplementedBy<Test2>());

            return container;
        }

        
    }
}
