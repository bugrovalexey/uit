﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.Report
{
    public enum ExportFormat
    {
        Pdf = 0,
        Rtf = 1,
        Xls = 2
    }
}
