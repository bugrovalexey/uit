param(
[Parameter(Mandatory=$true)]
[String]$serverName,
[Parameter(Mandatory=$true)]
[String]$packagePath,
[Parameter(Mandatory=$true)]
[String]$packageName,
[Parameter(Mandatory=$true)]
[String]$version,
[Parameter(Mandatory=$true)]
[String]$destinationPath,
[Parameter(Mandatory=$true)]
[String]$user,
[Parameter(Mandatory=$true)]
[String]$password,
[Parameter(Mandatory=$true)]
[String]$apppool
)

$pass = ConvertTo-SecureString -AsPlainText $password -Force
$cred = New-Object System.Management.Automation.PSCredential -ArgumentList $user,$pass
$zipName = $packageName+ '.' + $version + '.zip'

Write-Host "ZipName is $zipName"

Invoke-Command -ComputerName $serverName -Credential $cred -ScriptBlock {
    param([string]$pPath,
    [string]$zName,
    [string]$pName,
    [string]$dPath,
	[string]$applicationPoolName)
	
    Add-Type -AssemblyName System.IO.Compression.FileSystem
	import-module WebAdministration

	# shatdown apppool
	if((Get-WebAppPoolState -Name $applicationPoolName).Value -ne 'Stopped'){
		Write-Output ('Stopping Application Pool: {0}' -f $applicationPoolName)
		Stop-WebAppPool -Name $applicationPoolName
	}
	
	Start-Sleep -Seconds 3
	
    $packageToDeploy = [io.path]::Combine($pPath, $zName)
    Write-Host "Package to deploy is $packageToDeploy"
    $unpackFolder = [io.path]::Combine($pPath, $pName)
    Write-Host "Unpack folder is $unpackFolder"
    Write-Host "Destination path is $dPath"

    if (!(Test-Path($packageToDeploy)))
    {
        throw "package to deploy not found"
    }

    if (Test-Path($unpackFolder))
    {
        Remove-Item $unpackFolder -Force -Recurse
    }

    New-Item -Path $unpackFolder -type directory

    [System.IO.Compression.ZipFile]::ExtractToDirectory($packageToDeploy, $unpackFolder)

    if (Test-Path($dPath))
    {
        Remove-Item $dPath -Force -Recurse
    }
    
    # случай, когда не всё удалилось
    if (Test-Path($dPath))
    {
        $parent = (get-item $dPath).parent.FullName
        Copy-Item $unpackFolder -Destination $parent -Force -Recurse
    }
    else
    {
        Copy-Item $unpackFolder -Destination $dPath -Force -Recurse
    }

    Remove-Item $unpackFolder -Force -Recurse
    Remove-Item $packageToDeploy -Force -Recurse
	
	# start apppool
	if((Get-WebAppPoolState -Name $applicationPoolName).Value -ne 'Started'){
		Write-Output ('Starting Application Pool: {0}' -f $applicationPoolName)
		Start-WebAppPool -Name $applicationPoolName
	}

} -ArgumentList $packagePath, $zipName, $packageName, $destinationPath, $apppool

