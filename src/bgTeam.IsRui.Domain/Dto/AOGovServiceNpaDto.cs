﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Нормативный правовой акт ОУ
    /// </summary>
    public class AOGovServiceNpaDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Hаименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public virtual string DocumentNumber { get; set; }

        /// <summary>
        /// Дата документа
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Пункт, статья документа
        /// </summary>
        public virtual string ParticleOfDocument { get; set; }
    }
}
