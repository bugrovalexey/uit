﻿using bgTeam.IsRui.Domain.Entities.Plans;

namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    internal class PlansActivitySynchronizationMap : ClassMapHiLo<PlansActivitySynchronization>
    {
        public PlansActivitySynchronizationMap()
            : base("Plans_Activity_Synchronization_Coord")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.SynchronizationDate, "Plans_Activity_Synchronization_Coord_Date").Not.Nullable();
            References<PlansActivity>(x => x.PlanActivity, "Plans_Activity_ID").Not.Nullable();
        }
    }
}