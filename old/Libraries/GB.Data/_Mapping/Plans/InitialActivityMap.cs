﻿using GB.Entity.Directories;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Mapping.Plans
{
    class InitialActivityMap : ClassMapBase<InitialActivity>
    {
        public InitialActivityMap() :
            base("Plans_Activity_Initial")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.RegNum, "Plans_Activity_Initial_RegNum");
            Map(x => x.Name, "Plans_Activity_Initial_Name");
            Map(x => x.Comment, "Plans_Activity_Initial_Comment");
            Map(x => x.Summ, "Plans_Activity_Initial_Summ");

            References<WorkForm>(x => x.WorkForm, "Work_Form_ID").Not.Nullable();
            References<ExpenditureItem>(x => x.ExpenditureItem, "Expenditure_Items_ID").Not.Nullable();
            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID").Not.Nullable();
        }
    }
}