﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    /// <summary>
    /// Характеристики лицензии
    /// </summary>
    public class CharacteristicOfLicense : EntityBase
    {
        /// <summary>
        /// Наименование типовой лицензии
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Модель лицензирования
        /// </summary>
        public virtual string Model { get; set; }

        /// <summary>
        /// Программное обеспечение ОУ
        /// </summary>
        public virtual Software Software { get; set; }

        /// <summary>
        /// Тип соединения с сервером
        /// </summary>
        public virtual TypeOfConnectionToServerEnum? TypeOfConnectionToServer { get; set; }

        /// <summary>
        /// Количество одновременных подключений
        /// </summary>
        public virtual int? NumberOnlineSameTime { get; set; }

        /// <summary>
        /// Продуктовый номер лицензии
        /// </summary>
        public virtual string ProductNumber { get; set; }

        /// <summary>
        /// Количество единовременно распределяемых лицензий
        /// </summary>
        public virtual int? NumberAllocatedSameTime { get; set; }

        /// <summary>
        /// Лицензиар
        /// </summary>
        public virtual string Licensor { get; set; }
    }
}