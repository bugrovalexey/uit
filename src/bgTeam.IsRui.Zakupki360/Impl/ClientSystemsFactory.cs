﻿using System.Configuration;

namespace bgTeam.IsRui.Zakupki360.Impl
{
    public class ClientSystemsFactory
    {
        public IClientSystems Create()
        {
            var host = ConfigurationManager.AppSettings["Zakupki360.Host"];
            var key = ConfigurationManager.AppSettings["Zakupki360.Key"];

            return new ClientSystems(host, key);
        }
    }
}
