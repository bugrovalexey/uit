﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using bgTeam.IsRui.WebApp.ViewModels.Shared;
using bgTeam.IsRui.Domain.Extensions;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.IsRui.DataAccess.Services.Args;
using bgTeam.Extensions;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    [MetadataType(typeof(CommonDataModel_Valid))]
    public class CommonDataModel : ActivitiesCardBase
    {

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Описание")]
        public string Description { get; set; }

        [DisplayName("Ответственное структурное подразделение")]
        public string ResponsibleDepartment { get; set; }

        [DisplayName("Ответственный за размещение сведений")]
        public IList<IEntityName> ResponsibleInformation { get; private set; }

        [DisplayName("Объект учета")]
        public IList<IEntityName> AccountingObject { get; private set; }

        [DisplayName("Сотрудник, подписывающий мероприятие")]
        public IList<IEntityName> Signing { get; private set; }

        public AOTypeAndKindViewModel AOTypeAndKindViewModel { get; set; }

        public int AO { get; set; }

        [DisplayName("Полное наименование")]
        public string AOName { get; set; }

        [DisplayName("Краткое наименование")]
        public string AOShortName { get; set; }

        [DisplayName("Приказ №")]
        public int? Order { get; set; }

        [DisplayName("Дата приказа")]
        public string OrderDate { get; set; }

        [DisplayName("Предпосылки реализации мероприятия")]
        public string Prerequisites { get; set; }

        [DisplayName("Цели мероприятия")]
        public string Goal { get; set; }

        [DisplayName("Задачи мероприятия")]
        public string Task { get; set; }

        [DisplayName("Логотип")]
        public IEnumerable<IEntityName> Logo { get; set; }

        [DisplayName("Дополнительные материалы в рамках ФЭО (Анализ рынка)")]
        public IEnumerable<IEntityName> FilesData { get; set; }

        [DisplayName("Концепция")]
        public IEnumerable<IEntityName> Conception { get; set; }

        public bool IsEditAO { get { return CurrentActivity.With(x => x.ActivityType2.IsEq(ActivityTypeEnum.Create)); } }

        public CommonDataModel(IStatusRepository statusRepository, int id)
            : base(statusRepository, id, x => x.ResponsibleInformation, x => x.Signing, x => x.AccountingObject)
        {
            Name = CurrentActivity.Name;
            Description = CurrentActivity.Description;
            ResponsibleDepartment = CurrentActivity.ResponsibleDepartment;
            ResponsibleInformation = CurrentActivity.ResponsibleInformation.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.FullName } });
            Signing = CurrentActivity.Signing.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.FullName } });

            Order = CurrentActivity.Order;
            OrderDate = CurrentActivity.OrderDate.HasValue ? CurrentActivity.OrderDate.Value.ToString("dd.MM.yyyy") : null;
            Prerequisites = CurrentActivity.Prerequisites;
            Goal = CurrentActivity.Goal;
            Task = CurrentActivity.Task;

            Logo = CurrentActivity.Logo.Select(x => new SelectEntityInfo() { Id = x.Id, Name = x.Name });
            FilesData = CurrentActivity.Documents.Select(x => new SelectEntityInfo() { Id = x.Id, Name = x.Name });
            Conception = CurrentActivity.Conception.Select(x => new SelectEntityInfo() { Id = x.Id, Name = x.Name });

            var ao = CurrentActivity.AccountingObject;
            AOTypeAndKindViewModel = new AOTypeAndKindViewModel(ao.Return(x =>  x.Id, default(int)), ReadOnly);

            if (ao != null)
            {
                AccountingObject = new List<IEntityName>() { new SelectEntityInfo() { Id = ao.Id, Name = ao.ShortName } };

                AO = ao.Id;
                AOName = ao.FullName;
                AOShortName = ao.ShortName;
            }

        }
    }

    public class CommonDataArgs : IPlansActivityArgs, IAccountingObjectControllerArgs
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ResponsibleDepartment { get; set; }

        public int? ResponsibleInformation { get; set; }

        public int? ResponsibleTechnical { get; set; }

        public int? ResponsibleFinancial { get; set; }

        public int? Signing { get; set; }

        public int? AccountingObject { get; set; }

        public string Logo { get; set; }

        public string Files { get; set; }

        public int AO { get; set; }

        public string AOName { get; set; }

        public string AOShortName { get; set; }

        public int AOType { get; set; }

        public int AOKind { get; set; }

        public int? Order { get; set; }

        public DateTime? OrderDate { get; set; }

        public string Prerequisites { get; set; }

        public string Goal { get; set; }

        public string Task { get; set; }

        public string Conception { get; set; }

        public bool IsEditAO { get; set; }

        #region IAccountingObjectControllerArgs

        int IAccountingObjectControllerArgs.Id
        {
            get { return AO; }
            set { AO = value; }
        }

        int IAccountingObjectControllerArgs.Type
        {
            get { return AOType; }
            set { AOType = value; }
        }

        int IAccountingObjectControllerArgs.Kind
        {
            get { return AOKind; }
            set { AOKind = value; }
        }

        string IAccountingObjectControllerArgs.ShortName
        {
            get { return AOShortName; }
            set { AOShortName = value; }
        }

        string IAccountingObjectControllerArgs.FullName
        {
            get { return AOName; }
            set { AOName = value; }
        }

        #endregion IAccountingObjectControllerArgs
    }

    public class CommonDataModel_Valid : ValidateViewModel
    {
        //TODO Вернуть. Не работает валидация, ошибка JS, поэтому отключили
        //[Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        //[Required(ErrorMessage = RequiredText)]
        //public object ResponsibleInformation { get; set; }

        //[Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        //[Required(ErrorMessage = RequiredText)]
        //public object Signing { get; set; }

        //[Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        //[Required(ErrorMessage = RequiredText)]
        //public object AccountingObject { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public object AOShortName { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string Goal { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string Task { get; set; }

        [Required(ErrorMessage = RequiredText)]
        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int? Order { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string OrderDate { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string Prerequisites { get; set; }

    }
}