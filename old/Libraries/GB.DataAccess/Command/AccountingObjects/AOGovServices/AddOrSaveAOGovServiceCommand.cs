﻿using GB.Data.AccountingObjects;
using GB.Data.Frgu;
using GB.DataAccess.Command.AccountingObjects.AOGovServiceNPAs;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServices
{
    public class AddOrSaveAOGovServiceCommand : ICommand<AddOrSaveAOGovServiceContext, AOGovService>
    {
        ISessionFactoryGb factory;

        public AddOrSaveAOGovServiceCommand()
        {
            factory = new SessionFactory();
        }

        public AOGovService Execute(AddOrSaveAOGovServiceContext context)
        {
            AOGovService aoGovServ;

            if (context.xId.HasValue && context.xId != 0)
                aoGovServ = new Repository<AOGovService>().Get(context.xId.Value);
            else
            {
                aoGovServ = new AOGovService();                
            }

            AutoMapper.Mapper.Map(context, aoGovServ);

            using (var session = factory.Create())
            {
                session.SaveOrUpdate(aoGovServ);
                session.Commit();
            }
            
            if (context.GovServiceId.HasValue)
            {                
                new CopyGovServiceNPAInGovServiceCommand().Execute(new CopyGovServiceNPAInGovServiceContext() 
                { 
                    AOGovServiceId = aoGovServ.Id,
                    Npas = aoGovServ.GovService.Documents
                });
            }            

            if (!context.xId.HasValue || context.xId == 0)
            {                
                new SetAOGovServiceForAOGovServicesNPACommand().Execute(new SetAOGovServiceForAOGovServicesNPAContext() 
                {
                    AOGovService = aoGovServ,
                    AOGovServicesNpaIds = context.AOGovServicesNPAIds
                });
            }            

            return aoGovServ;
        }
    }
}
