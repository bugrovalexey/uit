﻿using System;
using GB.ESIA.Web.Helpers;

namespace GB.ESIA.Web
{
    public class ESIALogoutPage : System.Web.UI.Page
    {
        protected bool IsLogoutCompleted()
        {
            var redirect = false;
            string logoutRequestId = GetLogoutRequestIdFromIdp();
            if (string.IsNullOrEmpty(logoutRequestId))
            {
                logoutRequestId = GetLogoutResponseIdFromIdp();
                redirect = true;
            }
            // Создание запроса, подтверждающего успешное отключение на стороне ServiceProvider'а.
            var logoutResponse = new LogoutResponse(Page, CertificateAccessor.ServiceProviderCert, ConfigAccessor.SingleLogoutServiceResponseUrl, logoutRequestId);
            if (ConfigAccessor.DebugMode)
                Util.WriteLog("Temp/LastLogoutResponse.xml", logoutResponse.Xml.OuterXml);

            logoutResponse.Send();
            return redirect;
        }

        /// <summary>
        /// Возвращает идентификатор запроса на подтверждение отключения от IdentityProvider'а.
        /// </summary>
        /// <returns>Идентификатор запроса или, если не удалось обработать запрос, null.</returns>
        private string GetLogoutRequestIdFromIdp()
        {
            try
            {
                // Расшифровка запроса на подтверждение отключения от IdentityProvider'а.
                var logoutRequestFromIdp = new LogoutRequest(Page, CertificateAccessor.IdentityProviderCert);
                if (ConfigAccessor.DebugMode)
                    Util.WriteLog("Temp/LastLogoutRequestFromIdp.xml", logoutRequestFromIdp.Xml.OuterXml);

                return logoutRequestFromIdp.Id;
            }
            catch (Exception ex)
            {
                if (ConfigAccessor.DebugMode)
                    Util.WriteLog("Temp/LastLogoutRequestFromIdp.xml", ex.ToString());

                return null;
            }
        }

        /// <summary>
        /// Возвращает идентификатор ответа от IdentityProvider'а.
        /// </summary>
        /// <returns>Идентификатор ответа или, если не удалось обработать ответ, null.</returns>
        private string GetLogoutResponseIdFromIdp()
        {
            try
            {
                // Расшифровка ответа от IdentityProvider'а.
                var logoutResponseFromIdp = new LogoutResponse(Page, CertificateAccessor.IdentityProviderCert);
                if (ConfigAccessor.DebugMode)
                    Util.WriteLog("Temp/LastLogoutRequestFromIdp.xml", logoutResponseFromIdp.Xml.OuterXml);

                return logoutResponseFromIdp.Id;
            }
            catch (Exception ex)
            {
                if (ConfigAccessor.DebugMode)
                    Util.WriteLog("Temp/LastLogoutResponseFromIdp.xml", ex.ToString());

                return null;
            }
        }
    }
}
