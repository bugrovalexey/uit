﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.AccountingObjects;

namespace GB.DataAccess.Services.Args
{
    public class AccountingObjectDataProtectionArgs : BaseArgsNew
    {
        public LevelOfSecurity LevelOfSecurity { get; set; } 
    }
}
