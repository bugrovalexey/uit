﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.Domain.Dto;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimGetBaseInfoStory : IStory<ClaimGetBaseInfoStoryContext, ClaimBaseInfoDto>
    {
        private readonly ISessionNHibernateFactory _factory;

        public ClaimGetBaseInfoStory(ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _factory = sessionNHibernateFactory;
        }

        public ClaimBaseInfoDto Execute(ClaimGetBaseInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ClaimBaseInfoDto> ExecuteAsync(ClaimGetBaseInfoStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            using (var s = _factory.OpenSession())
            {
                var result = await s.GetSession()
                    .CreateSQLQuery("SELECT c.Claim_Number FROM Claim c WHERE c.Claim_ID = :Number")
                    .SetParameter("Number", context.ClaimId)
                    .UniqueResultAsync<string>();

                return new ClaimBaseInfoDto { Number = result };
            }
        }
    }
}
