﻿using bgTeam.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using System.Collections.Generic;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class ActivitiesViewModel : AccountingObjectBase
    {
        public ActivitiesViewModel(int id)
            : base(id)
        {
            Items = new PlansActivityRepository().GetActivitiesByAccountingObject(id)
                .Select(x => new
                {
                    x.Id,
                    x.NameSmall,
                    ActivityType = x.ActivityType2.Name,
                    Department = x.Department.Name,
                    Status = x.Status.Name,
                    PlansActivityVolY0 = x.PlansActivityVolY0.Return(d => d.ToString("N2"), default(string)),
                    PlansActivityVolY1 = x.PlansActivityVolY1.Return(d => d.ToString("N2"), default(string)),
                    PlansActivityVolY2 = x.PlansActivityVolY2.Return(d => d.ToString("N2"), default(string))
                });
        }

        public IEnumerable<object> Items { get; set; }
    }
}