﻿using bgTeam.IsRui.Domain.Entities.Plans;

namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    internal class PlansActivityMarkMap : ClassMapHiLo<PlansActivityMark>
    {
        public PlansActivityMarkMap()
            : base("Plans_Activity_Mark")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Mark_Name");
            Map(x => x.ExplanationRelationshipWithProjectMark, "Plans_Activity_Mark_Explan");
            Map(x => x.BaseValue, "Plans_Activity_Mark_Basic_Value");
            Map(x => x.TargetedValue, "Plans_Activity_Mark_Goal_Value");
            Map(x => x.DateToAchieveGoalValue, "Plans_Activity_Mark_Date");
            Map(x => x.Justification, "Plans_Activity_Mark_Justif");
            Map(x => x.SourceIsStateProgram, "Plans_Activity_Mark_Source_Is_State_Program").Not.Nullable();
            Map(x => x.SourceName, "Plans_Activity_Mark_Source_Name");
            Map(x => x.ExplanationRelationshipWithStateProgramMark, "Plans_Activity_Mark_Explanation");
            Map(x => x.AlgorithmOfCalculating, "Plans_Activity_Mark_AlgorithmOfCalculating");

            References(x => x.Unit, "MUnit_ID");
            References(x => x.PlansActivity, "Plans_Activity_ID");
            References(x => x.StateProgramMark, "State_Programme_Mark_ID");
        }
    }
}