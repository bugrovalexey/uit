﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ChangePasswordDto
    {
        public bool Result { get; set; }

        public string CurrentPassword { get; set; }
    }
}
