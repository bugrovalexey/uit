﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Other;

namespace Uviri.MKRF.Mapping.Other
{
    public class WorkMandatoryMap : ClassMapBase<WorkMandatory>
    {
        public WorkMandatoryMap()
            : base("Work_Mandatory")
        {
//            Table("w_");

//            Id(x => x.Id, "Work_Mandatory_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@Work_Mandatory_Name = ?
//                          ,@Work_Mandatory_Reason = ?
//                          ,@Work_Mandatory_Num = ?
//                          ,@Work_Mandatory_DT = ?
//                          ,@Work_Mandatory_Comm = ?
//                          ,@Work_Mandatory_Guarantor = ?
//                          ,@Year = ?
//                          ,@Govt_Organ_ID = ?
//                          ,@IKT_Component_ID = ?
//                          ,@Work_Mandatory_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Work_Mandatory_Insert " + sql).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Work_Mandatory_Update " + sql).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Work_Mandatory_Delete @Work_Mandatory_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Work_Mandatory_Name");
            Map(x => x.Reason, "Work_Mandatory_Reason");
            Map(x => x.Number, "Work_Mandatory_Num");
            Map(x => x.Date, "Work_Mandatory_DT");
            Map(x => x.Comment, "Work_Mandatory_Comm");
			Map(x => x.Guarantor, "Work_Mandatory_Guarantor");

            References<Years>(x => x.Year, "Year").Not.Nullable();
            References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
            References<IKTComponent>(x => x.IKTComponent, "IKT_Component_ID").Not.Nullable();
        }
    }
}
