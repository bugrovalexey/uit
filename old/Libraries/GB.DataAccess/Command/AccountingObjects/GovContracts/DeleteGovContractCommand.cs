﻿using AutoMapper;
using GB.Data.AccountingObjects;
using GB.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.GovContracts
{
    public class DeleteGovContractCommand : ICommand<DeleteGovContractContext>
    {
        ISessionFactoryGb factory;

        public DeleteGovContractCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteGovContractContext context)
        {
            using (var session = factory.Create())
            {
                session.Delete<GovContract>(context.Id);
                session.Commit();
            }
        }
    }
}
