﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class ExpenseItemMap : ClassMapHiLo<ExpenseItem>
    {
        public ExpenseItemMap()
            : base("Expense_Item")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Expense_Item_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_Expense_Item_Insert
            //						  @Expense_Item_Code = ?
            //						 ,@Expense_Item_Name = ?
            //						 ,@Expense_Item_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Expense_Item_Update
            //						  @Expense_Item_Code = ?
            //						 ,@Expense_Item_Name = ?
            //						 ,@Expense_Item_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Expense_Item_Delete
            //						  @Expense_Item_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "Expense_Item_Code").Nullable();
            Map(x => x.Name, "Expense_Item_Name").Nullable();
            Map(x => x.DateStart, "Expense_Item_Start_Date").Nullable();
            Map(x => x.DateEnd, "Expense_Item_End_Date").Nullable();
        }
    }
}