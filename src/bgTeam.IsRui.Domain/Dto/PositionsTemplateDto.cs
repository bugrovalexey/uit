﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class PositionsTemplateDto
    {
        public int Id { get; set; }

        public string SubDivision { get; set; }

        public string Article { get; set; }

        public string AccountingObject { get; set; }

        public string Name { get; set; }

        public decimal Quart1 { get; set; }

        public decimal Quart2 { get; set; }

        public decimal Quart3 { get; set; }

        public decimal Quart4 { get; set; }

        //public decimal Summ { get; set; }
    }
}
