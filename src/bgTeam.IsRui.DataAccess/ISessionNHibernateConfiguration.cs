﻿namespace bgTeam.IsRui.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.Domain;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using FluentNHibernate.Conventions.Helpers;
    using NHibernate.Cfg;

    public interface ISessionNHibernateConfiguration
    {
        Configuration GetConfiguration(string connectionString);
    }

    public class SessionNHibernateConfigurationMsSql : ISessionNHibernateConfiguration
    {
        public Configuration GetConfiguration(string connectionString)
        {
            var dbConfigurer = MsSqlConfiguration.MsSql2012.ConnectionString(cs => cs.Is(connectionString));

            var configuration = Fluently.Configure()
                .Database(dbConfigurer)
                .Mappings(x =>
                {
                    x.FluentMappings.AddFromAssembly(GetAssemblyMappings());
                    x.FluentMappings.Conventions.Setup(f => f.Add(AutoImport.Never()));
                });
                //.ExposeConfiguration(BuildSchema);

#if CACHING
            configuration.Cache(c => c.UseSecondLevelCache()
                    .UseQueryCache()
                    .ProviderClass<NHibernate.Caches.SysCache2.SysCacheProvider>());
#endif

            return configuration.BuildConfiguration();
        }

        //public static ISession BuildSchema(bool script, bool export)
        //{
        //    ISessionFactory session = GetConfig()
        //        .ExposeConfiguration(c => new NHibernate.Tool.hbm2ddl.SchemaExport(c).Create(script, export))
        //        .BuildSessionFactory();
        //    //.BuildConfiguration();

        //    return session.OpenSession();
        //}

        //private static void BuildSchema(NHibernate.Cfg.Configuration config)
        //{
        //    //Creates database structure
        //    new NHibernate.Tool.hbm2ddl.SchemaExport(config).Create(false, true);
        //}

        //TODO : Переделать метод
        private static IList<Assembly> GetAssemblyMappings()
        {
            return new Assembly[] { typeof(IEntity).Assembly };

            //Assembly.Load( )

            //return new List<Assembly>(AppDomain.CurrentDomain.GetAssemblies()
            //    .Where(x => x.FullName.GetCustomAttributes<AssemblyMappings>()
            //    .Any()));
        }
    }
}
