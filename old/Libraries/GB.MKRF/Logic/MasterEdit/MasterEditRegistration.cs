﻿using GB.Data.AccountingObjects;
using GB.MKRF.Logic.MasterEdit.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Logic.MasterEdit
{
    /// <summary>
    /// Регистрация менеджера, логика по построению мастера редактирования
    /// для различных объектов
    /// </summary>
    public class MasterEditRegistration : ManagerRegistrationBase<MasterEditManager>
    {
        protected override void RegisterHandlers(MasterEditManager manager)
        {
            manager.RegisterHandler<AccountingObject>(new AccountingObjectPlannedHandler(), MasterEditTypeEnum.AOPlanned);
            manager.RegisterHandler<AccountingObject>(new AccountingObjectPlannedSAHandler(), MasterEditTypeEnum.AOPlannedSA);

            manager.RegisterHandler<AccountingObject>(new AccountingObjectCreateHandler(), MasterEditTypeEnum.AOCreate);
            //manager.RegisterHandler<AccountingObject>(new AccountingObjectCreateWithoutContractHandler(), MasterEditTypeEnum.AOCreateWithoutContract);

            manager.RegisterHandler<AccountingObject>(new AccountingObjectExploitationHandler(), MasterEditTypeEnum.AOExploitation);
            //manager.RegisterHandler<AccountingObject>(new AccountingObjectExploitationWithoutContractHandler(), MasterEditTypeEnum.AOExploitationWithoutContract);

            //manager.RegisterHandler<AccountingObject>(new AccountingObjectEvolutionHandler(), MasterEditTypeEnum.AOEvolution);
            //manager.RegisterHandler<AccountingObject>(new AccountingObjectEvolutionWithoutContractHandler(), MasterEditTypeEnum.AOEvolutionWithoutContract);

            manager.RegisterHandler<AccountingObject>(new AccountingObjectOutExploitationHandler(), MasterEditTypeEnum.AOOutExploitation);
            //manager.RegisterHandler<AccountingObject>(new AccountingObjectOutExploitationWithoutContractHandler(), MasterEditTypeEnum.AOOutExploitationWithoutContract);


            manager.RegisterHandler<AccountingObject>(new AccountingObjectMultiHandler(), MasterEditTypeEnum.AOMulti);

            //manager.RegisterHandler<ActivityTabs>(new ActivityHandler());
            //manager.RegisterHandler<ActivityCardTabs>(new ActivityCardHandler());
            //manager.RegisterHandler<AccountingObjectsTabs>(new AccountingObjectsHandler());
            //manager.RegisterHandler<AccountingObjectsCardTabs>(new AccountingObjectsCardHandler());
        }
    }
}
