﻿namespace bgTeam.IsRui.Common.Helpers
{
    using System.Collections.Generic;

    public static class CollectionHelper
    {
        public static IEnumerable<object> GetEmptyForSelectList()
        {
            return new[]
            {
                new
                {
                    Id = (int?)null,
                    Name = "Не задано"
                }
            };
        }
    }
}
