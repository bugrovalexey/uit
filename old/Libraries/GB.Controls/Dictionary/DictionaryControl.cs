﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxCallback;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxHiddenField;
using DevExpress.Web.Data;

namespace GB.Controls.Dictionary
{
    public class DictionaryControl : CompositeControl
    {
        public string ConnectionString { get; set; }

        #region ChildsControl
        protected HtmlTable table;
        protected ASPxButton ASPxRefreshDictionaries;
        protected ASPxButton ASPxButtonAdd;
        protected ASPxHiddenField ASPxHiddenField;
        //protected ASPxLabel ErrLabel;
        //protected ASPxListBox ListBox;
        protected ASPxComboBox ComboBox;
        protected ASPxGridView Grid;
        protected ASPxCallback Callback;
        #endregion

        #region Properties
        MetaDictionary SelectDictionary
        {
            get
            {
                return MetaDictionaries.ListDictionaryItem[Convert.ToInt32(ComboBox.SelectedItem.Value)];
            }
        }

        public int UserId { get; set; }
        #endregion

        public DictionaryControl()
        {
            table = new HtmlTable();

            ASPxRefreshDictionaries = new ASPxButton { ID = "ASPxRefreshDictionaries", Text = "Обновить" };
            ASPxRefreshDictionaries.Click += ASPxRefreshDictionaries_Click;

            ASPxButtonAdd = new ASPxButton { ID = "ASPxButtonAdd", Text = "Добавить", ClientInstanceName = "ASPxButtonAdd" };
            ASPxButtonAdd.Click += ASPxButtonAdd_Click;
            ASPxButtonAdd.Style.Add("float", "right");

            ASPxHiddenField = new ASPxHiddenField { ID = "hfState", ClientInstanceName = "hfState" };

            //ErrLabel = new ASPxLabel { ID = "ErrLabel", ClientInstanceName = "ErrLabel" };

            ComboBox = new ASPxComboBox()
            {
                ID = "ComboBox",
                ClientInstanceName = "ComboBox",
                Width = new Unit("500px"),
                TextField = "Name",
                ValueField = "Id",
                ValueType = typeof(System.Int32)
            };
            ComboBox.Style.Add("float", "left");
            ComboBox.ClientSideEvents.SelectedIndexChanged =
@"function(s, e) {
    hfState.Set('ClearFilter', true);
    Grid.GotoPage(0);
}";


            Grid = new ASPxGridView { ID = "Grid", ClientInstanceName = "Grid", AutoGenerateColumns = false, Width = new Unit("100%")};

            Grid.RowDeleting += Grid_RowDeleting;
            Grid.StartRowEditing += Grid_StartRowEditing;
            Grid.CommandButtonInitialize += Grid_CommandButtonInitialize;
            Grid.BeforePerformDataSelect += Grid_BeforePerformDataSelect;
            var ecolumn = new GridViewCommandColumn { Caption = " ", Name = "cmdColumn", VisibleIndex = 100, Width = new Unit("150px") };
#if DX14
            ecolumn.ShowEditButton = true;
            ecolumn.ShowDeleteButton = true;
            ecolumn.ShowClearFilterButton = true;
#else
            ecolumn.EditButton.Visible = true;
            ecolumn.DeleteButton.Visible = true;
            ecolumn.ClearFilterButton.Visible = true;
#endif
            Grid.Columns.Add(ecolumn);
            Grid.ClientSideEvents.EndCallback = @"function(s, e) {
                                                    ASPxButtonAdd.SetEnabled(!s.cpReadOnly);
                                                    if(s.cpError)
                                                    {
                                                        alert(s.cpError);
                                                        s.cpError = null;
                                                    }

                                                    hfState.Set('ClearFilter', false);
                                                }";
            Grid.Settings.ShowFilterRow = true;
            Grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
            Grid.SettingsPager.PageSize = 100;
        }

        protected override void  CreateChildControls()
        {
            base.CreateChildControls();
            table.Width = "100%";
            var row = new HtmlTableRow();
            row.Height = "45px";
            
            var cell11 = new HtmlTableCell();
            cell11.Controls.Add(ComboBox);
            cell11.Controls.Add(ASPxRefreshDictionaries);
            cell11.Controls.Add(ASPxHiddenField);
            row.Cells.Add(cell11);

            var cell1 = new HtmlTableCell();
            cell1.Width = "30%";
            cell1.Controls.Add(ASPxButtonAdd);
            row.Cells.Add(cell1);
            
            var row6 = new HtmlTableRow();



            var row4 = new HtmlTableRow();
            var cell4 = new HtmlTableCell();
            cell4.ColSpan = 2;
            cell4.Controls.Add(Grid);
            row4.Cells.Add(cell4);


            table.Rows.Add(row);
            
            table.Rows.Add(row6);
            
            table.Rows.Add(row4);

            Controls.Add(table);

            MetaDictionaries.ConnectionString = ConnectionString;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            int DictionaryID = Convert.ToInt32(Page.Request.QueryString[DictionaryEditControl.DictIdKey]);

            if (MetaDictionaries.ListDictionaryItem.Values.Count == 0)
            {
                ASPxButtonAdd.Enabled = ComboBox.SelectedItem != null;
                return;
            }

            ComboBoxDataBind();
            //ListBoxDataBind();

            if (DictionaryID > 0)
            {
                ComboBox.Value = DictionaryID;
                ComboBox.SelectedIndex = ComboBox.SelectedItem.Index;
            }
            else
            {
                ComboBox.SelectedIndex = 0;
            }

            UpdateTableColums();
            ASPxButtonAdd.ClientEnabled = !SelectDictionary.ReadOnly;
            Grid.DataBind();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            if (Page.IsPostBack)
            {
                if (ComboBox.SelectedItem != null)
                {
                    UpdateTableColums();
                }
            }
        }

        private void ComboBoxDataBind()
        {
            ComboBox.DataSource = MetaDictionaries.ListDictionaryItem.Values.Where(md => md.Visible).ToList();
            ComboBox.DataBind();
        }

        //private void ListBoxDataBind()
        //{
        //    ListBox.DataSource = MetaDictionaries.ListDictionaryItem.Values.Where(md => md.Visible).ToList();
        //    ListBox.DataBind();
        //}

        public static string GetUrl(int Id)
        {
            return string.Concat(VirtualPathUtility.ToAbsolute("~/Administrator/Dictionary.aspx"), "?", DictionaryEditControl.DictIdKey, "=", Id);
        }

        private void GridCancelEdit(ASPxGridView grid, System.ComponentModel.CancelEventArgs e)
        {
            grid.CancelEdit();
            e.Cancel = true;

            grid.DataBind();
        }

        private void UpdateTableColums()
        {
            if (ComboBox.SelectedItem == null) return;
            DictionaryHelper.CreareGridColumns(SelectDictionary, Grid);

            Grid.JSProperties["cpReadOnly"] = SelectDictionary.ReadOnly;
        }

        protected void ASPxButtonAdd_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect(DictionaryEditControl.GetUrl((int)ComboBox.SelectedItem.Value));
        }

        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            if (ASPxHiddenField.Contains("ClearFilter") && (bool)ASPxHiddenField["ClearFilter"])
            {
                Grid.FilterExpression = string.Empty;
            }
            Grid.DataSource = DictionaryHelper.GetDataDictionary(SelectDictionary);
        }

        protected void Grid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            GridCancelEdit(sender as ASPxGridView, e);

            DevExpress.Web.ASPxGridView.ASPxGridView.RedirectOnCallback(DictionaryEditControl.GetUrl((int)ComboBox.SelectedItem.Value, Convert.ToInt32(e.EditingKeyValue)));
        }

        protected void Grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            Grid.JSProperties["cpError"] = DictionaryHelper.DeleteDictionaryItem(SelectDictionary, (int)e.Keys[0], UserId);

            GridCancelEdit(sender as ASPxGridView, e);
        }

        protected void Grid_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Delete && Grid.Columns[MetaDictionary.INUSE] != null)
            {
                if (Convert.ToInt32(Grid.GetRowValues(e.VisibleIndex, MetaDictionary.INUSE)) == 1)
                {
                    e.Visible = false;
                }
            }
        }

        protected void ASPxRefreshDictionaries_Click(object sender, EventArgs e)
        {
            MetaDictionaries.Clear();

            //ListBoxDataBind();
            ComboBoxDataBind();

            UpdateTableColums();
        }

    }
}
