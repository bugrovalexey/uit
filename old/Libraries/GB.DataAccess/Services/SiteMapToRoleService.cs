﻿using GB.Data.Admin;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class SiteMapToRoleService : ServiceBase<SiteMapToRole>, IService<SiteMapToRole, SiteMapToRoleArgs>
    {
        public SiteMapToRole Create(SiteMapToRoleArgs args)
        {
            return SaveOrUpdate(new SiteMapToRole(), args);
        }

        public SiteMapToRole Update(SiteMapToRoleArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private SiteMapToRole SaveOrUpdate(SiteMapToRole entity, SiteMapToRoleArgs args)
        {
            entity.FillSiteToRoleMap(args);

            base.SaveOrUpdate(entity);

            return entity;
        }

        public void Delete(SiteMapToRoleArgs args)
        {
            base.Delete(args.Id);
        }
    }
}