﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201802261034)]
    public class Migration_201802261033 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Column("OU_Software_ID").OnTable("License_Charact").AsInt32().Unique().NotNullable();
        }
    }
}
