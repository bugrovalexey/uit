﻿using System;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Administrator
{
    public partial class DictionaryEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ctlDictionaryEdit.UserId = UserRepository.GetCurrent().Id;
        }
    }
}