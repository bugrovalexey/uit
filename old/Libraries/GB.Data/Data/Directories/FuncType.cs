﻿namespace GB.MKRF.Entities.Directories
{
    /// <summary>Тип функций ведомства</summary>
    public class FuncType : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null
    }

    public enum FuncTypeEnum
    {
        /// <summary>
        /// Типовые
        /// </summary>
        AddFunc = 1,

        /// <summary>
        /// Основные
        /// </summary>
        MainFunc = 2,
    }
}