﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Services.Args
{
    public class InternalIsAndComponentItkiArgs : BaseArgsNew
    {
        /// <summary>
        /// Id объекта учета
        /// </summary>
        public int AccountingObjectId { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// Внутренний ИС и компоненты ИТКИ (ОУ)
        /// </summary>
        public int? ObjectId { get; set; }

    }
}
