﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.PlanReport.SectionV;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionV
{
    public class SupplyISInfoMap : ClassMapBase<SupplyISInfo>
    {
        public SupplyISInfoMap()
            : base("PRS_5")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_5_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string args = @" @PRS_5_Num = ? 
//							,@PRS_5_Func = ? 
//							,@PRS_5_IS = ? 
//							,@PRS_5_User_Count = ? 
//							,@PRS_5_Need_Complete = ? 
//							,@PRS_5_Need_Create = ? 
//							,@Plans_ID = ? 
//							,@FGIS_Info_ID = ? 
//							,@PRS_5_ID = ? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_5_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_5_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_5_Delete
//						  @PRS_5_ID = ?").Check.None();

        }

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_5_Num");
			Map(x => x.Func, "PRS_5_Func");
			Map(x => x.IS, "PRS_5_IS");
			Map(x => x.UsersCount, "PRS_5_User_Count");
			Map(x => x.NeedUpgrade, "PRS_5_Need_Complete");
			Map(x => x.NeedCreate, "PRS_5_Need_Create");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
        	References<FgisInfo>(x => x.FgisInfo, "FGIS_Info_ID");
        }
    }
}