﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.GovContracts
{
    public class SaveGovContractContext : ICommandContext
    {
        public int Id { get; set; }

        public string RcDeviation { get; set; }
    }
}
