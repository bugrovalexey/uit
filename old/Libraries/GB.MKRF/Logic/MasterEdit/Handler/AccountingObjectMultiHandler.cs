﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Logic.MasterEdit.Handler
{
    class AccountingObjectMultiHandler : MasterEditBaseHandler
    {
        protected override MasterInItem[] GetListPage()
        {
            return new MasterInItem[] 
            {
                new MasterInItem(MasterItemEnum.None),
                new MasterInItem(MasterItemEnum.GoalsAndPurpose),
                new MasterInItem(MasterItemEnum.KindMasterEdit),
                new MasterInItem(MasterItemEnum.CardForRead)
            };
        }
    }
}
