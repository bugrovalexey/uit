﻿using GB.Data.Documents;
using GB.Data.Plans;
using GB.DataAccess.Command.Documents;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class OtherReasonsService : ServiceBase<Reason>, IService<Reason, ProjectReasonArgs>
    {
        ISessionFactoryGb factory;

        public OtherReasonsService()
        {
            factory = new SessionFactory();
        }

        public Reason Create(ProjectReasonArgs args)
        {
            Reason reason = new Reason { Owner = args.Owner ?? GetEntity<PlansActivity>(args.OwnerId) };

            return SaveOrUpdate(reason, args);
        }

        public Reason Update(ProjectReasonArgs args)
        {
            return SaveOrUpdate(Repository.GetExisting(args.Id), args);
        }

        public void Delete(ProjectReasonArgs args)
        {
            base.Delete((int)args.Id);
        }

        private Reason SaveOrUpdate(Reason reason, ProjectReasonArgs args)
        {
            reason.Num = args.Num;
            reason.Name = args.Name;
            reason.SignerName = args.SignerName;
            reason.SignerPosition = args.SignerPosition;
            reason.URL = args.URL;

            base.SaveOrUpdate(reason);

            //new DocumentService()
            //    .Save(args.Files as string, reason.Documents, reason, DocTypeEnum.OtherReason);

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = args.Files,
                listDocs = reason.Documents,
                owner = reason,
                type = DocTypeEnum.OtherReason
            });

            base.Refresh(reason);
            return reason;
        }
    }

    public class ProjectReasonArgs : BaseArgsNew
    {
        public string Name { get; set; }

        public string Num { get; set; }

        public string SignerName { get; set; }

        public string SignerPosition { get; set; }

        public string URL { get; set; }

        public string Files { get; set; }

        public PlansActivity Owner { get; set; }

        public int OwnerId { get; set; }
    }
}