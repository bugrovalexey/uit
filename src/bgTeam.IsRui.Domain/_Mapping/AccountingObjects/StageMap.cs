﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    class StageMap : ClassMapHiLo<Stage>
    {
        public StageMap()
            : base("OU_Stage") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Stage_Name").Not.Nullable();
        }
    }
}
