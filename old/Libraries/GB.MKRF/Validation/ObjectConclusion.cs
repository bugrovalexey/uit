﻿using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.OtherExpObjects;
using Uviri.MKRF.Repository;

namespace Uviri.MKRF.Validator
{
    internal class ObjectConclusion
    {
        /// <summary>Параметры для валидации заключения</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            ExpObjectConclusion Item = (ExpObjectConclusion)Entity;
            User CurrentUser = UserRepository.GetCurrent();
            if (Action == ObjActionEnum.create)
            {
                if (Item.Demand != null)
                    return "0;" + Item.Demand.Id.ToString();
                else
                    return null;
            }
            else if (Action == ObjActionEnum.CanChangeConclusionStatus)
            {
                return Action.ToString() + ";" + Entity.Id.ToString();
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
