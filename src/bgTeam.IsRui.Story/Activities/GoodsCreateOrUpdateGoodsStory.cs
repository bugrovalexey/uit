﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GoodsCreateOrUpdateGoodsStory : IStory<GoodsCreateOrUpdateGoodsStoryContext, ActivityGoodsDto>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public GoodsCreateOrUpdateGoodsStory(ISessionNHibernateFactory factory, IRepositoryRui repository, IMapperBase mapper)
        {
            _factory = factory;
            _repository = repository;
            _mapper = mapper;
        }

        public ActivityGoodsDto Execute(GoodsCreateOrUpdateGoodsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityGoodsDto> ExecuteAsync(GoodsCreateOrUpdateGoodsStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var result = new ActivityGoodsDto();

            using (var session = _factory.OpenSession())
            {
                ActivityGoods goods;

                if (context.Id.HasValue)
                {
                    goods = _repository.Get<ActivityGoods>(context.Id);
                }
                else
                {
                    goods = new ActivityGoods();

                    goods.PlansActivityId = context.ActivityId.Value;
                }

                _mapper.Map(context, goods);

                goods.ChangeDate = DateTime.Now;

                //if (context.ExpenseDirectionId != null)
                //{
                //    goods.ExpenseDirection = _repository.Get<ExpenseDirection>(context.ExpenseDirectionId);
                //}

                //if (context.ExpenseTypeId != null)
                //{
                //    goods.ExpenseType = _repository.Get<ExpenseType>(context.ExpenseTypeId);
                //}

                //if (context.KOSGUId != null)
                //{
                //    goods.KOSGU = _repository.Get<ExpenseItem>(context.KOSGUId);
                //}

                //if (context.OKPDId != null)
                //{
                //    goods.OKPD = _repository.Get<OKPD>(context.OKPDId);
                //}

                //if (context.ProductGroupId != null)
                //{
                //    goods.ProductGroup = _repository.Get<ProductGroup>(context.ProductGroupId);
                //}

                await session.SaveOrUpdateAsync(goods);

                await SaveCharacteristics(context, goods);

                _mapper.Map(goods, result);

                return result;
            }
        }

        private async Task<CharacteristicOfTechnicalSupportGetDto> SaveCharacteristics(GoodsCreateOrUpdateGoodsStoryContext context, ActivityGoods goods)
        {
            if (context.Characteristic != null)
            {
                var characteristicStory = new GoodsAddOrUpdateGoodsCharacteristicStory(_factory, _repository, _mapper);

                foreach (GoodsCharacteristic characteristicNew in context.Characteristic)
                {
                    var characteristicData = _repository.GetExisting<GoodsCharacteristic>(characteristicNew.Id);

                    await characteristicStory.ExecuteAsync(new GoodsAddOrUpdateGoodsCharacteristicStoryContext()
                    {
                        Id = characteristicNew.Id,
                        Name = characteristicData.Name,
                        GoodsId = goods.Id,
                        UnitId = characteristicData.Unit.Id,
                        Value = characteristicData.Value
                    });
                }

                foreach (GoodsCharacteristic characteristicMap in goods.Characteristic)
                {
                    var characteristicDataMap = _repository.GetExisting<GoodsCharacteristic>(characteristicMap.Id);

                    characteristicMap.Goods = characteristicDataMap.Goods;
                    characteristicMap.Unit = characteristicDataMap.Unit;
                }
            }

            return null;
        }
    }
}
