﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace GB.ExternalSystems.Zakupki360.Dto
{
    public enum PurchaseTypeEnum
    {
         
        [Description("Открытый конкурс")]
        Open = 1,

        [Description("Электронный аукцион")]
        Auction = 2
    }
}
