﻿using System;
using GB.Extentions;

namespace GB.DataAccess
{
    internal static class ObjectExtensions
    {
        public static bool IsEmpty(this object Id)
        {
            return (Id == null ||
                    Id == System.DBNull.Value ||
                    string.IsNullOrWhiteSpace(Id.ToString()));
        }

        public static int? ToIntNull(this object n)
        {
            if (n is int)
                return (int)n;

            if (!string.IsNullOrWhiteSpace(n as string))
                return Convert.ToInt32(n);

            return null;
        }

        public static double? ToDoubleNull(this object n)
        {
            if (n is double)
                return (double)n;

            if (!string.IsNullOrWhiteSpace(n as string))
                return Convert.ToDouble(n);

            return null;
        }

        public static DateTime? ToDateTimeNull(this object d)
        {
            if (d is DateTime)
                return (DateTime)d;

            if (d is string)
                if (!string.IsNullOrWhiteSpace(d as string))
                    return Convert.ToDateTime(d);

            return null;
        }

        public static string ToStringOrDefault(this object value)
        {
            return value.Return(Convert.ToString);
        }
    }
}