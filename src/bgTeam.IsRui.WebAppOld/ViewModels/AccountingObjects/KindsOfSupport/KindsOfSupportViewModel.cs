﻿using System.Collections.Generic;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport
{
    public class KindsOfSupportViewModel : AccountingObjectBase
    {
        public KindsOfSupportViewModel(int id)
            : base(id, x => x.WorksAndServices)
        {
            //TODO доставать списки одним запросом с Current (см Future), нельзя запихивать в Fetch, только 1 список можно с Get
            TechnicalSupports = Current.TechnicalSupports.Select(x => new TableTechnicalSupport(x));
            Softwares = Current.Softwares.Select(x => new TableSoftware(x));
            WorksAndServices = Current.WorksAndServices.Select(x => new TableWorkAndService(x));
        }

        public IEnumerable<TableTechnicalSupport> TechnicalSupports { get; set; }

        public IEnumerable<TableSoftware> Softwares { get; set; }

        public IEnumerable<TableWorkAndService> WorksAndServices { get; set; }
    }
}