﻿using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport;
using System.Collections.Generic;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class WorkAndServiceViewModel : StepBaseViewModel
    {
        public WorkAndServiceViewModel(int id)
            : base(id, MasterItemEnum.WorkAndService, x => x.WorksAndServices)
        {
            WorksAndServices = Current.WorksAndServices.Select(x => new TableWorkAndService(x));
        }

        public IEnumerable<TableWorkAndService> WorksAndServices { get; set; }

    }
}