﻿using System;
using System.Collections.Generic;
using System.Text;
using GB.Controls;
using GB.Handlers;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Repository;
using GB.MKRF.Web.Helpers;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;


namespace GB.MKRF.Web.Common
{
    /// <summary>
    /// Компонент для работы со документами заявки.
    /// Отображаются документы, типы которых перечислены в DisplayedDocTypes
    /// </summary>
    public partial class DocControl : System.Web.UI.UserControl
    {
        private ExpDemandBase demand = null;
        /// <summary>
        /// Заявка. Дефолтный владелец
        /// </summary>
        public ExpDemandBase Demand
        {
            get
            {
                return demand;
            }
            set
            {
                demand = value;
            }
        }


        /// <summary>
        /// Типы документов, которые надо отобразить
        /// null = документы не отображаются
        /// </summary>
        public DocTypeEnum[] DisplayedDocTypes
        {
            get;
            set;
        }

        /// <summary>
        /// Тип присваеваемый новому документу
        /// null = demand_document
        /// </summary>
        public DocTypeEnum? NewDocumentDocType
        {
            get;
            set;
        }

        /// <summary>
        /// Отображать столбец "Тип документа"
        /// </summary>
        public bool ShowDocType { get; set; }

        /// <summary>
        /// Отображать комментарии
        /// </summary>
        public bool ShowComment { get; set; }

        /// <summary>
        /// Отображать столбец "Решение прав. комиссии"
        /// </summary>
        //public bool ShowIsDecision { get; set; }

        protected Document currentDoc = null;
        protected bool isNewRow = false;


        /// <summary>true переводит компонент в режим выбора значений</summary>
        public bool SelectionMode { get; set; }

        /// <summary>
        /// Клиентское имя таблицы
        /// </summary>
        public string GridClientName
        {
            get
            {
                return docsGrid.ClientInstanceName;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            //ShowIsDecision = true;
            callBack.ClientInstanceName = "callBack" + this.ID;
            docsGrid.ClientInstanceName = "docsGrid" + this.ID;
            hfDoc.ClientInstanceName = "hfDoc" + this.ID;
            LoadingPanel.ClientInstanceName = "LoadingPanel" + this.ID;


            callBack.ClientSideEvents.BeginCallback = @"
function(s, e){
    "+LoadingPanel.ClientInstanceName+@".Show();
}";
            callBack.ClientSideEvents.CallbackComplete = @"
function(s, e){
    "+LoadingPanel.ClientInstanceName+@".Hide();
    alert(e.result);
}";


            docsGrid.ClientSideEvents.BeginCallback = @"
function(s,e) { 
    if (e.command=='STARTEDIT') 
        " + hfDoc.ClientInstanceName + @".Clear(); 
    if (e.command=='UPDATEEDIT') 
        hfSave(e, " + hfDoc.ClientInstanceName + @"); 
}";

            docsGrid.ClientSideEvents.EndCallback = @"
function(s, e) {
    " + hfDoc.ClientInstanceName + @".Set('FilePath', '');
    " + hfDoc.ClientInstanceName + @".Set('FileName', '');
}";


            base.OnInit(e);
        }

//        protected void uploader_DataBinding(object sender, EventArgs e)
//        {
//            ASPxUploadControl uc = (ASPxUploadControl)sender;
//            uc.ClientSideEvents.FileUploadComplete = @"
//function(s, e) { 
//    "+LoadingPanel.ClientInstanceName+@".SetText('Проверка подписи&amp;hellip;'); 
//    "+LoadingPanel.ClientInstanceName+@".Hide(); 
//    fileUploaded (e.callbackData, "+hfDoc.ClientInstanceName+ @"); 
//    s.SetVisible(false); 
//}";


//            uc.ClientSideEvents.TextChanged = @"
//function(s, e) { 
//    if (s.GetVisible()){
//        "+LoadingPanel.ClientInstanceName+@".SetText('Загрузка файла'); 
//        "+LoadingPanel.ClientInstanceName+@".Show(); 
//        s.Upload();
//    }
//}";
//        }

        protected override void OnPreRender(EventArgs e)
        {
            if (SelectionMode)
            {
                docsGrid.Columns["cmdCheckBox"].Visible = true;
                docsGrid.Columns["cmdColumn"].Visible = false;
                //docsGrid.Settings.ShowVerticalScrollBar = true;
                docsGrid.ClientSideEvents.Init="OnInit";
                docsGrid.SettingsPager.PageSize = 6;
                docsGrid.SettingsEditing.Mode = GridViewEditingMode.PopupEditForm;
                docsGrid.SettingsPopup.EditForm.Modal = true;
                docsGrid.SettingsPopup.EditForm.HorizontalAlign = DevExpress.Web.ASPxClasses.PopupHorizontalAlign.WindowCenter;
                docsGrid.SettingsPopup.EditForm.VerticalAlign = DevExpress.Web.ASPxClasses.PopupVerticalAlign.WindowCenter;

                //docsGrid.ClientSideEvents.EndCallback = "OnEndCallback";

                string js = @"
function OnInit(s, e) {
    SelectRows(s);
}
";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "gridJs", js, true); 
            }

            docsGrid.DataBind();

            addButton.Visible = Validation.Validator.Validate(new Document(NewDocumentDocType ?? DocTypeEnum.demand_document, Demand), ObjActionEnum.create);
            if (addButton.Visible)
            {
                addButton.ClientSideEvents.Click = "function(s,e) {" + docsGrid.ClientInstanceName + ".AddNewRow();}";
            }

            base.OnPreRender(e);
        }

        protected void docsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            docsGrid.Columns["DocTypeText"].Visible = ShowDocType;
            docsGrid.Columns["Comment"].Visible = ShowComment;
            //docsGrid.Columns["IsDecision"].Visible = ShowIsDecision;

            //IList<Document> docList = null;

            if (DisplayedDocTypes == null)
                throw new ArgumentException("Необходимо указать один или несколько типов документов");


            var crud = new AccessAction[] { AccessAction.Read, AccessAction.Edit, AccessAction.Delete, AccessAction.Create };
            var dt = DocumentRepositoryOld.GetAllDemandDocuments(this.Demand, DisplayedDocTypes, UserRepository.GetCurrent().Id, crud);
            docsGrid.DataSource = dt;

            return;


            //if (DisplayedDocTypes != null)
            //{
            //    docList = DocumentRepository.GetDemandDocuments(this.Demand, DisplayedDocTypes);
            //}
            //else
            //{
            //    throw new ArgumentException("Необходимо указать один или несколько типов документов");
            //}
            

            //AccessRepository.FillAccess<Document>(docList, AccessEnum.Read, AccessEnum.Edit, AccessEnum.Delete, AccessEnum.ConfigureAccess);

            //var dataSource = docList.Select(x => new
            //{
            //    Id = x.Id,
            //    Name = x.Name,
            //    Num = x.Num,
            //    DateDoc = x.Date,
            //    Comment = x.Comm,
            //    DocTypeText = x.DocTypeText,
            //    Date = x.DateCreate,
            //    Responsible = x.Author == null ? null : string.Join(", ", new string[] { x.Author.Role.Name, x.Author.FullName }),
            //    IsSigned = x.IsSigned,
            //    IsDecision = x.DecisionGovComission ? "Да" : "Нет",
            //    Access = (int)(x.Access ?? 0),
            //}).Where(z => (z.Access & (int)AccessEnum.Read) == (int)AccessEnum.Read).OrderByDescending(x => x.Date);

            //docsGrid.DataSource = dataSource;
        }

        protected void docsGrid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            currentDoc = RepositoryBase<Document>.Get(e.EditingKeyValue);
        }

        protected void docsGrid_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            isNewRow = true;
        }

        protected void docsGrid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            Document doc = new Document(NewDocumentDocType ?? DocTypeEnum.demand_document, Demand);
            saveRow(doc);

            docsGrid.CancelEdit();
            e.Cancel = true;
        }

        protected void docsGrid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            Document doc = RepositoryBase<Document>.Get((int)e.Keys[0]);
            saveRow(doc);

            docsGrid.CancelEdit();
            e.Cancel = true;
        }

        protected void docsGrid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            DocumentController.Delete((int)e.Keys[0]);
            
            docsGrid.CancelEdit();
            e.Cancel = true;
        }

        protected void NameTextBox_DataBinding(object sender, EventArgs e)
        {
            if (!isNewRow && currentDoc == null)
                return;

            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = isNewRow ? string.Empty : currentDoc.Name;
        }

        protected void NumTextBox_DataBinding(object sender, EventArgs e)
        {
            if (!isNewRow && currentDoc == null)
                return;

            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = isNewRow ? string.Empty : currentDoc.Num;
        }

        protected void DocTypeTextTextBox_DataBinding(object sender, EventArgs e)
        {
            if (!ShowDocType || !isNewRow && currentDoc == null)
                return;

            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = isNewRow ? string.Empty : currentDoc.DocTypeText;
        }

        protected void CommentMemo_DataBinding(object sender, EventArgs e)
        {
            if (!ShowComment || !isNewRow && currentDoc == null)
                return;

            ASPxMemo box = (ASPxMemo)sender;

            box.Text = isNewRow ? string.Empty : currentDoc.Comm;
        }
        

        protected void DateDateEdit_DataBinding(object sender, EventArgs e)
        {
            if (!isNewRow && currentDoc == null)
                return;

            ASPxDateEdit box = (ASPxDateEdit)sender;

            box.Value = isNewRow ? null : currentDoc.Date;
        }


        //protected void DecisionGovComissionCheckBox_DataBinding(object sender, EventArgs e)
        //{
        //    if (isNewRow || currentDoc == null)
        //        return;

        //    ASPxCheckBox box = (ASPxCheckBox)sender;
        //    box.Checked = currentDoc.DecisionGovComission;
        //}

        void saveRow(Document doc)
        {
            DocumentController.SaveOrUpdate(doc, new DocumentArgs()
                {
                    Author = UserRepository.GetCurrent(),
                    FileInfo = hfDoc["File"],
                    Name = hfDoc["Name"],
                    Num = hfDoc["Num"],
                    DocTypeText = ShowDocType ? hfDoc["DocTypeText"] : null,
                    Comment = ShowComment ? hfDoc["Comment"] : null,
                    Date = hfDoc["Date"]        
                });            
        }

        protected void docsGrid_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            int? composite = null;

            composite = (int?)grid.GetRowValues(e.VisibleIndex, "Composite");
            
            if (e.ButtonType == ColumnCommandButtonType.Edit)
            {
                e.Visible = (composite.HasValue && ((composite.Value & (int)AccessAction.Edit) == (int)AccessAction.Edit));
            }
            else if (e.ButtonType == ColumnCommandButtonType.Delete)
            {
                e.Visible = (composite.HasValue && ((composite.Value & (int)AccessAction.Delete) == (int)AccessAction.Delete));
            }
        }

        protected void docsGrid_CancelRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            docsGrid.DataBind();
        }

        protected string getDownloadUrl(int docId, bool IsSigned, int Access)
        {
            StringBuilder sb = new StringBuilder();

            if (docId > 0)
                sb.Append("<a href='" + FileHandler.GetUrl(docId) + "'>Скачать</a>");

            if (IsSigned)
            {
                // TODO: Сделать модуль проверки эцп.
                // Сейчас выполняется фиктивная проверка
                sb.Append("<br/><a href='javascript:" + callBack.ClientInstanceName + ".PerformCallback(\"" + docId + "\")'>Проверить ЭП</a>");
            }


            if ((Access & (int)AccessAction.Create) == (int)AccessAction.Create)
                sb.Append("<br/><br/><a href='javascript:RoleGrid.PerformCallback(" + docId + ");'>Настройка доступа</a>");

            return sb.ToString();
        }

        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            StringBuilder Log = new StringBuilder();

            try
            {
                Document doc = RepositoryBase<Document>.Get(e.Parameter);                
                if (DigitalSignatureHelper.Verify(doc, Log))
                    e.Result = "Подпись действительна";
                else
                    e.Result = "Подпись не действительна";
            }
            catch
            {
                e.Result = "Проверить подпись не удалось. Сервис проверки недоступен.";
            }

            e.Result += ("\n\n" + Log.ToString());
        }

        protected void RoleGrid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {

            int Id = Convert.ToInt32(e.Parameters);
            ASPxGridView grid = (sender as ASPxGridView);

            grid.DataSource = EntityHelper.ConvertToAnonymous<Role>(RepositoryBase<Role>.GetAll(x => (x.Id != (int)RoleEnum.CA && x.Id != (int)RoleEnum.Admin)),
                delegate(Role r)
                {
                    return new
                    {
                        Id = r.Id,
                        Name = r.Name,
                        DocId = Id
                    };
                });



            grid.DataBind();

            DocumentAccessList list = new DocumentAccessList(Id);

            for (int i = 0; i < grid.VisibleRowCount; i++)
            {
                int roleId = (int)grid.GetRowValues(i, "Id");

                if (list.Find(roleId) == null)
                {
                    grid.Selection.SelectRow(i);
                }
            }
        }

        class DocumentAccessList
        {
            System.Collections.Generic.IList<DocumentAccess> list;

            public DocumentAccessList(int id)
            {
                list = RepositoryBase<DocumentAccess>.GetAll(x => x.Document.Id == id);
            }

            public DocumentAccess Find(int roleId)
            {
                foreach (DocumentAccess item in list)
                {
                    if (item.Role.Id == roleId)
                    {
                        return item;
                    }
                }

                return null;
            }

            public void DeleteAll()
            {
                foreach (DocumentAccess item in list)
                {
                    RepositoryBase<DocumentAccess>.Delete(item.Id);
                }
            }
        }

        protected void RoleCallBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            int Id = (int)RoleGrid.GetRowValues(0, "DocId");

            //удалить все записи
            DocumentAccessList list = new DocumentAccessList(Id);

            list.DeleteAll();

            for (int i = 0; i < RoleGrid.VisibleRowCount; i++)
            {
                if (!RoleGrid.Selection.IsRowSelected(i))
                {
                    DocumentAccess da = new DocumentAccess();

                    da.Document = RepositoryBase<Document>.Get(Id);
                    da.Role = RepositoryBase<Role>.Get((int)RoleGrid.GetRowValues(i, "Id"));

                    RepositoryBase<DocumentAccess>.SaveOrUpdate(da);
                }
            }
        }


        protected void DocGroupComboBox_DataBinding(object sender, EventArgs e)
        {
            if (!isNewRow && currentDoc == null)
                return;

            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = DocumentRepositoryOld.GetDocGroups();

            box.Value = isNewRow ? (int)DocGroupEnum.None : (int)currentDoc.DocGroup;
        }

        protected void uploader2_DataBinding(object sender, EventArgs e)
        {
            FileUploadControl2 uploader = (FileUploadControl2)sender;

            if (currentDoc == null)
                return;

            uploader.SetValue(currentDoc.FileName, currentDoc.Id.ToString());
            uploader.FileItemTemplate = "<a href=\"" + FileHandler.GetUrl("") + "{id}\">{name}</a>";
        }

        //protected void DecisionGovComissionCheckBox_Init(object sender, EventArgs e)
        //{
        //    var c = sender as ASPxCheckBox;

        //    var role = UserRepository.GetCurrent().Role;
        //    var allow = !(RoleEnum.Department.IsEq(role) || RoleEnum.DepartmentMKS.IsEq(role));

        //    c.ReadOnly = allow;
        //    c.ClientEnabled = !allow;
        //}
    }
}