﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateGoalAndPurposeAccountingObjectCommand: ICommand<UpdateGoalAndPurposeAccountingObjectContext>
    {
        ISessionFactoryGb factory;

        public UpdateGoalAndPurposeAccountingObjectCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(UpdateGoalAndPurposeAccountingObjectContext context)
        {
            var entity = new Repository<AccountingObject>().GetExisting(context.xId);

            entity.Targets = context.Targets;
            entity.PurposeAndScope = context.PurposeAndScope;            

            using (var session = factory.Create())
            {
                session.Update(entity);
                session.Commit();
            }
        }
    }
}
