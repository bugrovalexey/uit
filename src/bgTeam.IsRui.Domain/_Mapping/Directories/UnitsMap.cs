﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    internal class UnitsMap : ClassMapHiLo<Units>
    {
        public UnitsMap()
            : base("MUnit")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "MUnit_Name");
            Map(x => x.Abbreviation, "MUnit_SName");
        }
    }
}