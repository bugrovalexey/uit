﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.WebApp.Helpers;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport
{
    public class TableTechnicalSupport : NotificationBaseViewModel
    {
        #region consts

        public const string NameCaption = "Наименование";
        public const string CategoryKindOfSupportCaption = "Вид оборудования";
        public const string ManufacturerCaption = "Производитель";
        public const string AmountCaption = "Количество";
        public const string SummaCaption = "Cтоимость";

        #endregion consts

        public TableTechnicalSupport(TechnicalSupport entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            CategoryKindOfSupportName = entity.CategoryKindOfSupport.Return(cat => cat.Name, default(string));
            ManufacturerName = entity.Manufacturer.Return(man => man.Name, default(string));
            Amount = entity.Amount;
            Summa = entity.Summa.ToMoneyString();
            Characteristics = entity.Characteristics
                .Return(ch => ch.Select(x => new TableCharacteristicOfTechnicalSupport(x)), new List<TableCharacteristicOfTechnicalSupport>());
        }

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(CategoryKindOfSupportCaption)]
        public string CategoryKindOfSupportName { get; set; }

        [DisplayName(ManufacturerCaption)]
        public string ManufacturerName { get; set; }

        [DisplayName(AmountCaption)]
        public int? Amount { get; set; }

        [DisplayName(SummaCaption)]
        public string Summa { get; set; }

        public IEnumerable<TableCharacteristicOfTechnicalSupport> Characteristics { get; set; }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteTechnicalSupport", "AccountingObjects", new { id = Id }, "GridTechnicalSupports");
            }
        }
    }
}