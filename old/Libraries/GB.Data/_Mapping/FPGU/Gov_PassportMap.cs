﻿using GB;
using GB.Entity.Directories.FPGU;

namespace GB.MKRF.Mapping.FPGU
{
    public class Gov_PassportMap : ClassMapBase<Gov_Passport>
    {
        public Gov_PassportMap()
            : base("Gov_Passport")
        {
        }
        protected override void InitMap()
        {
            Map(x => x.Name, "Gov_Passport_Name");
            Map(x => x.SName, "Gov_Passport_SName");
            Map(x => x.Number, "Gov_Passport_Number");
            Map(x => x.OGV, "Gov_Passport_OGV");
            Map(x => x.Orig_Id, "Gov_Passport_Orig_ID");
            Map(x => x.Classifier, "Gov_Passport_Classifier");

            HasMany<Gov_Passport>(x => x.Services).KeyColumns.Add("Gov_Passport_ID").Inverse().Cascade.AllDeleteOrphan();
        }
    }
}