﻿using System;

namespace bgTeam.IsRui.Story.AccountingObjects
{
    public class AoCharacteristicsAddOrUpdateCharacteristicStoryContext
    {
        public int AoId { get; set; }

        public int? Id { get; set; }

        public string Name { get; set; }

        public int? TypeId { get; set; }

        public int? UnitId { get; set; }

        public int? Norm { get; set; }

        public int? Max { get; set; }

        public int? Fact { get; set; }

        public DateTime? DateBeginning { get; set; }

        public DateTime? DateEnd { get; set; }
    }
}
