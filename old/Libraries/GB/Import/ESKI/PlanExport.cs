﻿using System;
using System.Runtime.Serialization;

namespace Core.Import.ESKI
{
    [DataContract]
    public class PlanExport
    {
        [DataMember(Order = 0, IsRequired = true)]
        public int Id { get; set; }
        [DataMember(Order = 2, IsRequired = true)]
        public int Year { get; set; }
        [DataMember(Order = 3, IsRequired = true)]
        public string OGVCode { get; set; }        
        [DataMember(Order = 4, IsRequired = true)]
        public string UserLogin { get; set; }
        [DataMember(Order = 6)]
        public ActivityExport[] ActivityList { get; set; }
    }

    [DataContract]
    public class ActivityExport
    {
        [DataMember(Order = 0, IsRequired = true)]
        public int Id { get; set; }
        [DataMember(Order = 1)]
        public string ExtId { get; set; }
        [DataMember(Order = 2)]
        public string Name { get; set; }
        [DataMember(Order = 3)]
        public string Annotation { get; set; }
        [DataMember(Order = 4)]
        public int TypeId { get; set; }
        [DataMember(Order = 5)]
        public int FGISInfoID { get; set; }
        [DataMember(Order = 6)]
        public string FGISName { get; set; }
        [DataMember(Order = 7)]
        public string FGISInfoText { get; set; }
        [DataMember(Order = 8)]
        public int IKTID { get; set; }
        [DataMember(Order = 9)]
        public int RegistryImportID { get; set; }
        [DataMember(Order = 9)]
        public string RegistryImportCode { get; set; }
        [DataMember(Order = 10)]
        public int RegistryImportIKTID { get; set; }
        [DataMember(Order = 11)]
        public bool Priority { get; set; }
        [DataMember(Order = 12)]
        public string ResponsibleName { get; set; }
        [DataMember(Order = 13)]
        public string ResponsibleJob { get; set; }
        [DataMember(Order = 14)]
        public string ResponsiblePhone { get; set; }
        [DataMember(Order = 15)]
        public string ResponsibleEmail { get; set; }
        [DataMember(Order = 16)]
        public string GRBSCode { get; set; }
        [DataMember(Order = 17)]
        public string SectionKBKCode { get; set; }
        [DataMember(Order = 18)]
        public string ExpenseItemCode { get; set; }
        [DataMember(Order = 19)]
        public string WorkFormCode { get; set; }
        [DataMember(Order = 20)]
        public string Subentry { get; set; }
        [DataMember(Order = 21)]
        public DateTime? FactDate { get; set; }
        [DataMember(Order = 22)]
        public double VolY0 { get; set; }
        [DataMember(Order = 23)]
        public double VolY1 { get; set; }
        [DataMember(Order = 24)]
        public double VolY2 { get; set; }
        [DataMember(Order = 25)]
        public double AddVolY0 { get; set; }
        [DataMember(Order = 26)]
        public double AddVolY1 { get; set; }
        [DataMember(Order = 27)]
        public double AddVolY2 { get; set; }
        [DataMember(Order = 28)]
        public double FactVolY0 { get; set; }
        [DataMember(Order = 29)]
        public double FactVolY1 { get; set; }
        [DataMember(Order = 30)]
        public double FactVolY2 { get; set; }
        [DataMember(Order = 31)]
        public GoalExport[] ArrayOfGoal { get; set; }
        [DataMember(Order = 32)]
        public ReasonExport[] ArrayOfReason { get; set; }
        [DataMember(Order = 33)]
        public SpecExport[] ArrayOfSpec { get; set; }
        [DataMember(Order = 34)]
        public WorkExport[] ArrayOfWork { get; set; }
    }

    [DataContract]
    public class SpecExport
    {
        [DataMember(Order = 0, IsRequired = true)]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public string Name { get; set; }
        [DataMember(Order = 3)]
        public string OKDPCode { get; set; }
        [DataMember(Order = 4)]
        public int ExpenseTypeID { get; set; }
        [DataMember(Order = 5)]
        public int ExpenseDirectionID { get; set; }
        [DataMember(Order = 6)]
        public string Analog { get; set; }
        [DataMember(Order = 7)]
        public string Quantity { get; set; }
        [DataMember(Order = 8)]
        public double? Cost { get; set; }
        [DataMember(Order = 9)]
        public int QualifierID { get; set; }
        [DataMember(Order = 10)]
        public SpecTypeExport[] ArrayOfSpecType { get; set; }
    }

    [DataContract]
    public class SpecTypeExport
    {
        [DataMember(Order = 0, IsRequired = true)]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public string CharName { get; set; }
        [DataMember(Order = 3)]
        public string CharUnit { get; set; }
        [DataMember(Order = 4)]
        public string CharValue { get; set; }
    }

    [DataContract]
    public class WorkExport
    {
        [DataMember(Order = 0, IsRequired = true)]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public string Name { get; set; }
        [DataMember(Order = 3)]
        public string OKDPCode { get; set; }
        [DataMember(Order = 4)]
        public string OKDPText { get; set; }
        [DataMember(Order = 5)]
        public int ExpenseDirectionID { get; set; }
        [DataMember(Order = 6)]
        public int ExpenseTypeID { get; set; }
        [DataMember(Order = 7)]
        public WorkTypeExport[] ArrayOfWorkType { get; set; }
    }

    [DataContract]
    public class WorkTypeExport
    {
        [DataMember(Order = 0, IsRequired = true)]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public int ExpenseTypeId { get; set; }
        [DataMember(Order = 3)]
        public double ExpY0 { get; set; }
    }

    [DataContract]
    public class ReasonExport
    {
        [DataMember(Order = 0, IsRequired = true)]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public string Name { get; set; }
        [DataMember(Order = 3)]
        public string NPANum { get; set; }
        [DataMember(Order = 4)]
        public DateTime? NPADate { get; set; }
        [DataMember(Order = 5)]
        public string NPALink { get; set; }
    }

    [DataContract]
    public class GoalExport
    {
        [DataMember(Order = 0, IsRequired = true)]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public string Name { get; set; }
    }

}