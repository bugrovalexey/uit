﻿using System.Collections.Generic;

namespace GB.MKRF.Logic.Tabs
{
    public interface ITabsHandler
    {
        /// <summary>
        /// Получить первый доступный action
        /// </summary>
        string GetAction();

        /// <summary>
        /// Получит список доступных табов
        /// </summary>
        IList<string> GetTabsList(ReturnTabsText fun);
    }

    public delegate string ReturnTabsText(string text, string actionName, string controllerName);
}