﻿namespace bgTeam.IsRui.Domain.Entities.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Domain.Entities.Documents;

    public class WorkflowUserConclusion : EntityBase
    {
        public virtual int WorkflowId { get; set; }

        public virtual int UserId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual string Text { get; set; }

        /// <summary>
        /// Документы
        /// </summary>
        public virtual IList<Files> Documents { get; set; }
    }
}
