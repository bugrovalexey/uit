﻿using FluentNHibernate.Mapping;
using bgTeam.IsRui.Domain.Entities;

namespace bgTeam.IsRui.Domain._Mapping
{
    internal abstract class ClassMapIdentity<T> : ClassMap<T> where T : EntityBase
    {
        public ClassMapIdentity(string table)
        {
            Table(table);

            Id(x => x.Id, table+ "_ID").GeneratedBy.Identity();
        }
    }
}
