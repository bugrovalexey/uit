﻿namespace bgTeam.IsRui.Domain._Mapping.Documents
{
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.Documents;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;

    internal class FilesMap : ClassMapHiLoNew<Files>
    {
        public FilesMap()
            : base("BG_Files")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Type, "Type").CustomType<DocTypeEnum>();
            Map(x => x.Kind, "Kind").CustomType<DocKindEnum>();

            Map(x => x.Name, "Name");
            Map(x => x.Extension, "Extension");
            Map(x => x.Size, "Size");
            Map(x => x.Url, "Url");
            Map(x => x.MimeType, "MimeType");

            Map(x => x.EntityType, "EntityType").ReadOnly();
            ReferencesAny(x => x.EntityOwner)
                .AddMetaValue<PlansActivity>(((int)EntityType.PlansActivity).ToString())
                .AddMetaValue<AccountingObject>(((int)EntityType.AccountingObject).ToString())
                .AddMetaValue<AODecisionToCreate>(((int)EntityType.DocumentAccountingObject).ToString())
                .AddMetaValue<Reason>(((int)EntityType.OtherReason).ToString())
                .AddMetaValue<SpecialCheck>(((int)EntityType.SpecialCheck).ToString())
                .AddMetaValue<AODocumentCommissioning>(((int)EntityType.AODocumentCommissioning).ToString())
                .AddMetaValue<AODocumentDecommissioning>(((int)EntityType.AODocumentDecommissioning).ToString())
                .AddMetaValue<AODocumentGroundsForCreation>(((int)EntityType.AODocumentGroundsForCreation).ToString())
                .AddMetaValue<ActOfAcceptance>(((int)EntityType.ActOfAcceptance).ToString())
                .AddMetaValue<AccountingObjectUserComment>(((int)EntityType.AccountingObjectCommentDocument).ToString())
                .AddMetaValue<WorkflowUserConclusion>(((int)EntityType.WorkflowUserConclusion).ToString())
                .AddMetaValue<WorkflowUserConclusionHistory>(((int)EntityType.WorkflowUserConclusionHistory).ToString())
                .AddMetaValue<ClaimDocument>(((int)EntityType.ClaimDocument).ToString())
                .AddMetaValue<ClaimAct>(((int)EntityType.ClaimAct).ToString())
                .AddMetaValue<ClaimUserComment>(((int)EntityType.ClaimUserComment).ToString())
                .AddMetaValue<PlansActivityAct>(((int)EntityType.ActivityAct).ToString())
                .EntityTypeColumn("EntityType")
                .EntityIdentifierColumn("EntityOwnerId").IdentityType<int>();
        }
    }
}