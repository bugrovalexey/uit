﻿namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using bgTeam.IsRui.Domain.Entities.Directories;

    internal class OkeiMap : ClassMapHiLo<Okei>
    {
        public OkeiMap()
            : base("Okei")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Okei_Name");
            Map(x => x.Code, "Okei_Code");
            Map(x => x.FullName, "Okei_FullName");
            Map(x => x.NationalCode, "Okei_NationalCode");
            Map(x => x.InternationalCode, "Okei_InternationalCode");
            Map(x => x.InternationalName, "Okei_InternationalName");
        }
    }
}
