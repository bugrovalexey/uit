﻿using GB.Data.Common;
using GB.MKRF.Infrastructure;
using System.Runtime.Remoting.Messaging;

namespace GB.MKRF.Repository
{
    public class UserRepository : Repository<User>
    {
        private const string KEY_CURRENT_USER = "CurrentUser";

        private static User GetByLoginEx(string login)
        {
            var user = new Repository<User>().Get(q => q
                .Fetch(x => x.Department).Eager
                .Fetch(x => x.Roles).Eager //в Get так можно, чтобы одним запросов всё считать
                .Where(x => x.Login == login));
            return user;
        }

        private static User GetCurrentEx()
        {
            var user = (User)CallContext.GetData(KEY_CURRENT_USER);

            if (user == null)
            {
                user = GetByLoginEx(NHSession.CurrentUserLogin);
                CallContext.SetData(KEY_CURRENT_USER, user);
            }

            return user;
        }

        /// <summary>
        /// Возвращает текущего пользователя
        /// </summary>
        public static User GetCurrent()
        {
            return GetCurrentEx();
        }

        public static int CurrentUserId
        {
            get
            {
                var user = GetCurrentEx();
                return user != null ? user.Id : -1;
            }
        }

        public static bool IsAuthorized
        {
            get { return GetCurrentEx() != null; }
        }
    }
}