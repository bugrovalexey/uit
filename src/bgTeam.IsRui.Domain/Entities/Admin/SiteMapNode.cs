﻿using System.Collections.Generic;

namespace bgTeam.IsRui.Domain.Entities.Admin
{
    /// <summary>
    /// Узел карты сайта
    /// </summary>
    public class SiteMapNode : EntityBase
    {
        /// <summary>Название</summary>
        public virtual string Title { get; set; }

        /// <summary>Контроллер</summary>
        public virtual string Controller { get; set; }

        /// <summary>Действие</summary>
        public virtual string Action { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// URL страницы
        /// </summary>
        public virtual string Url { get; set; }

        /// <summary>
        /// Порядковый номер
        /// </summary>
        public virtual int OrderIndex { get; set; }

        /// <summary>
        /// Показывать в меню
        /// </summary>
        public virtual bool Visible { get; set; }

        /// <summary>
        /// Связь узла с ролью
        /// </summary>
        public virtual IList<SiteMapToRole> Roles { get; set; }

        /// <summary>
        /// Родительский узел
        /// </summary>
        public virtual SiteMapNode Parent { get; set; }

        /// <summary>
        /// Родительский узел
        /// </summary>
        public virtual int? ParentId
        {
            get
            {
                if (Parent != null)
                {
                    return Parent.Id;
                }

                return null;
            }
        }

        /// <summary>
        /// Дочерние узлы
        /// </summary>
        public virtual IList<SiteMapNode> Children { get; protected set; }

        ///// <summary>
        ///// Список ролей
        ///// </summary>
        //public virtual IList<Role> GetRoles()
        //{
        //    IList<Role> roles = new List<Role>();
        //    foreach(NodeToRole r in NodeToRole)
        //    {
        //        roles.Add(r.Role);
        //    }
        //    return roles;
        //}

        ///// <summary>
        ///// Список ролей
        ///// </summary>
        //public virtual void AddRole(Role role)
        //{
        //    foreach (NodeToRole r in NodeToRole)
        //    {
        //        if (r.Role == role)
        //            return;
        //    }
        //    NodeToRole.Add(new NodeToRole() { Node = this, Role = role });
        //}
    }
}