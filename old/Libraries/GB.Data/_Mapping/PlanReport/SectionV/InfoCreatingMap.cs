﻿using Uviri.MKRF.Entities.PlanReport.SectionV;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionV
{
    public class InfoCreatingMap : ClassMapBase<InfoCreating>
	{
        public InfoCreatingMap()
            : base("PRS_5v")
		{
//            Table("w_");

//            Id(x => x.Id, "PRS_5v_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string args = @" @PRS_5v_Num = ? 
//							,@PRS_5v_Func = ? 
//							,@PRS_5v_IS = ? 
//							,@PRS_5v_Task = ? 
//							,@PRS_5v_Soft = ? 
//							,@PRS_5v_Executor = ? 
//							,@PRS_5v_Department = ? 
//							,@PRS_5v_content = ? 
//							,@Plans_ID = ? 
//							,@PRS_5v_ID = ? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_5v_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_5v_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_5v_Delete
//						  @PRS_5v_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_5v_Num");
			Map(x => x.Func, "PRS_5v_Func");
			Map(x => x.IS, "PRS_5v_IS");
			Map(x => x.Tasks, "PRS_5v_Task");
			Map(x => x.Software, "PRS_5v_Soft");
			Map(x => x.Executor, "PRS_5v_Executor");
			Map(x => x.Unit, "PRS_5v_Department");
			Map(x => x.Content, "PRS_5v_content");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
        }
    }
}