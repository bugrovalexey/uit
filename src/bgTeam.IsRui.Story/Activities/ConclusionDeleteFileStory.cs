﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ConclusionDeleteFileStory : IStory<ConclusionDeleteFileStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;

        public ConclusionDeleteFileStory(ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _factory = sessionNHibernateFactory;
        }

        public bool Execute(ConclusionDeleteFileStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(ConclusionDeleteFileStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            bool result;

            using (var session = _factory.OpenSession())
            {
                await session.DeleteAsync<Files>(context.Id.Value);

                result = true;
            }

            return result;
        }
    }
}
