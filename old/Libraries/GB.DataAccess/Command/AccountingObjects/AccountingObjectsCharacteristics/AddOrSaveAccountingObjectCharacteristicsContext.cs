﻿using GB.Data.AccountingObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AccountingObjectsCharacteristics
{
    public class AddOrSaveAccountingObjectCharacteristicsContext : BaseAddOrSaveContext
    {
        public string Name { get; set; }

        public int? TypeId { get; set; }

        public int? UnitId { get; set; }
        
        public int Norm { get; set; }
       
        public int? Max { get; set; }

        public int? Fact { get; set; }

        public DateTime? DateBeginning { get; set; }

        public DateTime? DateEnd { get; set; }
        
        public bool IsRegister { get; set; }        

        public int AccountingObjectId { get; set; }
    }
}
