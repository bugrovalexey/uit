﻿namespace bgTeam.IsRui.Domain._Mapping.Users
{
    using bgTeam.IsRui.Domain.Entities.Users;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class UsersMap : ClassMapHiLo<UsersEntity>
    {
        public UsersMap()
           : base("Users") { }

        protected override void InitMap()
        {
            Map(x => x.Login, "Users_Login");
            Map(x => x.Password, "Users_Password");
            Map(x => x.IsActive, "Users_Is_Active");
            Map(x => x.Surname, "Users_Surname");
            Map(x => x.Name, "Users_Name");
            Map(x => x.Middlename, "Users_Middlename");
            Map(x => x.Job, "Users_Job");
            Map(x => x.WorkPhone, "Users_Work_Phone");
            Map(x => x.MobPhone, "Users_Mob_Phone");
            Map(x => x.Fax, "Users_Fax");
            Map(x => x.Email, "Users_Email");
            Map(x => x.Work, "Users_Work");
            Map(x => x.Education, "Users_Education");
            Map(x => x.ReceiveMail, "Users_Receive_Mail");
            Map(x => x.IsResponsible, "Users_Is_Responsible");
            Map(x => x.Snils, "Users_Snils");
            Map(x => x.IsESIA, "Users_Is_ESIA");
            Map(x => x.Token, "Users_Token");

            References(x => x.GovtOrgan, "Govt_Organ_ID");
        }
    }
}
