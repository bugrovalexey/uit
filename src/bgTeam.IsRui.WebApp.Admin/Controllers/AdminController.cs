﻿namespace bgTeam.IsRui.WebApp.Controllers
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Story.Admin;
    using bgTeam.IsRui.WebApp.Admin.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Управление учётными записями
    /// </summary>
    [Route("admin/[controller]/[action]")]
    //[Authorize(Roles = "administrator")]
    public class AdminController : Controller
    {
        private readonly IStoryBuilder _builder;
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public AdminController(IStoryBuilder storyBuilder, IRepositoryRui repository, IMapperBase mapper)
        {
            _builder = storyBuilder;
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// Index page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            return RedirectToAction("GetAllUsers");
        }

        /// <summary>
        /// Представление всех пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            var users = await _builder
                .Build(new UserGetAllUsersStoryContext() { })
                .ReturnAsync<IEnumerable<UsersDto>>();

            return View(new GetAllUsersViewModel(users));
        }

        [HttpGet]
        public async Task<IActionResult> GetUserInfo(int id)
        {
            UsersDto userDto = null;

            if (id > 0)
            {
                userDto = await _builder
                .Build(new UserGetUserInfoStoryContext() { Id = id })
                .ReturnAsync<UsersDto>();

                return View(new UserViewModel(userDto));
            }
            else
            {
                return View(new UserViewModel());
            }
        }

        /// <summary>
        /// Добавить/редактировать данные пользователя
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UserAddOrUpdateUser(UserAddOrUpdateUserStoryContext context)
        {
            var user = await _builder
                .Build(context)
                .ReturnAsync<UsersDto>();
            return RedirectToAction("GetAllUsers");
        }

        /// <summary>
        /// Изменить пароль пользователя
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> UserChangePassword(int id)
        {
            var userInfo = await _builder
                .Build(new UserGetUserInfoStoryContext() { Id = id })
                .ReturnAsync<UsersDto>();
            return View(new UserChangePasswordViewModel(id, userInfo.Name));
        }

        /// <summary>
        /// Изменить пароль пользователя
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UserChangePassword(UserChangePasswordViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.NewPassword))
            {
                model.Message = "Введите пароль";
                return View(model);
            }

            if (string.IsNullOrWhiteSpace(model.NewPasswordConfirm))
            {
                model.Message = "Введите подтверждение пароля";
                return View(model);
            }

            if (model.NewPassword != model.NewPasswordConfirm)
            {
                model.Message = "Подтверждение пароля введено не верно";
                return View(model);
            }

            var result = await _builder
                .Build(new UserChangePasswordStoryContext()
                {
                    Id = model.Id,
                    NewPassword = model.NewPassword
                })
                .ReturnAsync<bool>();

            return RedirectToAction("GetAllUsers");
        }

        /// <summary>
        /// Удалить пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<bool> UserDeleteUser(int id)
        {
            return await _builder
                .Build(new UserDeleteUserStoryContext() { Id = id })
                .ReturnAsync<bool>();
        }
    }
}
