﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Prognosis.SectionII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionII
{
    public class SoftOfficeMap : ClassMapBase<SoftOffice>
	{
        public SoftOfficeMap()
            : base("MF2_Soft_Office")
        {
//            Table("w_");

//            Id(x => x.Id, "MF2_Soft_Office_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_MF2_Soft_Office_Insert
//                          @MF2_Soft_Office_CA = ?
//                         ,@MF2_Soft_Office_TP = ?
//                         ,@MF2_Soft_Office_Comm = ?
//                         ,@MF2_ID = ?
//                         ,@Soft_Office_Type_ID = ?
//                         ,@MF2_Soft_Office_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF2_Soft_Office_Update
//                          @MF2_Soft_Office_CA = ?
//                         ,@MF2_Soft_Office_TP = ?
//                         ,@MF2_Soft_Office_Comm = ?
//                         ,@MF2_ID = ?
//                         ,@Soft_Office_Type_ID = ?
//                         ,@MF2_Soft_Office_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF2_Soft_Office_Delete
//						  @MF2_Soft_Office_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.CountCA, "MF2_Soft_Office_CA");
			Map(x => x.CountTP, "MF2_Soft_Office_TP");
            Map(x => x.Comment, "MF2_Soft_Office_Comm");

			References<TechInfo>(x => x.TechInfo, "MF2_ID").Not.Nullable();
			References<SoftOfficeType>(x => x.SoftOfficeType, "Soft_Office_Type_ID").Not.Nullable();
        }
    }
}