﻿using bgTeam.IsRui.Common.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.bgTeam.IsRui.Unit.Common
{
    class FileProviderRuiMock : IFileProviderRui
    {
        public void DeleteFile(object id)
        {
        }

        public string GenerateFileName()
        {
            return "Mock file";
        }

        public void MoveToGeneric(Guid guid, int id, bool replace = false)
        {
        }

        public Stream OpenFileToRead(int id)
        {
            return null;
        }

        public async Task<byte[]> ReadFileAsync(int id)
        {
            return new byte[] { 1, 2, 3, 4, 5 };
        }

        public async Task<string> SaveAsync(IFormFile file)
        {
            return GenerateFileName();
        }
    }
}
