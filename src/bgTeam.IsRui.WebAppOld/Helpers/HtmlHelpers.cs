﻿using bgTeam.IsRui.Domain;
using bgTeam.IsRui.WebApp.Helpers.Control;
using bgTeam.IsRui.WebApp.ViewModels.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace bgTeam.IsRui.WebApp.Helpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString Json(this HtmlHelper helper, object value)
        {
            return MvcHtmlString.Create(System.Web.Helpers.Json.Encode(value));
        }

        #region Table

        public static MvcHtmlString Table(this HtmlHelper helper, string name, IEnumerable items,
            DataTableSettingsBase settings, DataTablesConfig config, IDictionary<string, object> attributes = null)
        {
            return Table(helper, name, items, settings, config.ToString(), attributes);
        }

        public static MvcHtmlString Table(this HtmlHelper helper, string name, IEnumerable items,
            DataTableSettingsBase settings, string dataTableConfig, IDictionary<string, object> attributes = null)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Не задано имя таблицы"); //return new MvcHtmlString(string.Empty);

            return MvcHtmlString.Create(BuildTable(name, items, settings, dataTableConfig, attributes));
        }

        private static string BuildTable(string name, IEnumerable items, DataTableSettingsBase settings,
            string dataTableConfig, IDictionary<string, object> attributes)
        {
            StringBuilder sb = new StringBuilder();

            var columns = settings.GetColumnsList();

            BuildTableHeader(sb, columns);

            if (!string.IsNullOrEmpty(settings.FooterTemplateText))
            {
                if (settings.FooterTemplateText == DataTableSettingsBase.DefaultFooter)
                    BuildTableFooter(sb, columns);
                else
                    sb.AppendLine(settings.FooterTemplateText);
            }

            if (items != null)
            {
                if (settings.IsTreeTableFlag)
                {
                    var treeItems = items.OfType<ITree>();

                    BuildTableRowsForTreeTable(sb, treeItems.Where(i => i.ParentId.HasValue == false), treeItems,
                        settings, columns);
                }
                else
                {
                    foreach (var item in items)
                    {
                        BuildTableRow(sb, item, settings, columns);
                    }
                }
            }

            TagBuilder builder = new TagBuilder("table");
            builder.MergeAttributes(attributes);
            builder.MergeAttribute("name", name);
            builder.MergeAttribute("id", name);
            builder.MergeAttribute("class", "display striped");
            builder.InnerHtml = sb.ToString();

            var result = builder.ToString(TagRenderMode.Normal);

            if (settings.IsWrapTable)
            {
                result += JSWrapTable(name, settings, dataTableConfig, "response");
            }
            return result;
        }

        private static void BuildTableRowsForTreeTable(StringBuilder sb, IEnumerable<ITree> parentItems,
            IEnumerable<ITree> allItems, DataTableSettingsBase settings, IList<DataTablesColumn> colunms)
        {
            foreach (var item in parentItems)
            {
                BuildTableRow(sb, item, settings, colunms);
                BuildTableRowsForTreeTable(sb, allItems.Where(i => i.ParentId == item.Id), allItems, settings, colunms);
            }
        }

        private static void BuildTableRow(StringBuilder sb, object obj, DataTableSettingsBase settings,
            IList<DataTablesColumn> colunms)
        {
            //Если в качества параметра obj приходит DataRow,
            //то пробегаем все колонки из settings, используя FieldName каждой column, достаём значения из переданного obj
            //Если же приходит объект какой-либо сущности,
            //то пробегаем все колонки из settings и из объекта достаем значения соответствующих свойств объекта

            var row = obj as DataRow; // obj - DataRow ?
            Type objType = obj.GetType(); //достаем тип объекта (используется если приходит объект сущности)
            object id = 0; //идентификатор записи
            object customvalue = null;
            if (row != null)
            {
                id = row[settings.KeyField];
                customvalue = !string.IsNullOrWhiteSpace(settings.CustomAttributeField)
                    ? row[settings.CustomAttributeField]
                    : null;
            }
            else
            {
                var keyPoperty = objType.GetProperty(settings.KeyField);
                var customAttributeField = !string.IsNullOrWhiteSpace(settings.CustomAttributeField)
                    ? objType.GetProperty(settings.CustomAttributeField)
                    : null;
                id = keyPoperty != null ? keyPoperty.GetValue(obj, null) : 0;
                customvalue = customAttributeField != null ? customAttributeField.GetValue(obj, null) : null;
            }
            sb.AppendFormat("\t<tr data-id='{0}' {1} {2} {3}>", id,
                !string.IsNullOrWhiteSpace(settings.ItemEditPage)
                    ? string.Format("data-clicklink='{0}{1}'", settings.ItemEditPage,
                        settings.ItemEditPage.Contains("?") ? "&id=" + id : "/" + id)
                    : string.Empty,
                !(string.IsNullOrWhiteSpace(settings.CustomAttributeField) || customvalue == null)
                    ? string.Format("data-{0}={1}", settings.CustomAttributeField, customvalue)
                    : string.Empty,
                GetTreeTableAttributes(settings.IsTreeTableFlag, obj));

            foreach (var column in colunms)
            {
                if (column == null) continue;
                var commandColumn = column as DataTablesCommandColumn;
                if (commandColumn == null)
                {
                    object value = (row != null)
                        ? row[column.FieldName] //если DataRow
                        : column.GetValue(obj); //если объект сущности
                    string cellContent;
                    if (column.CheckBoxColumn)
                    {
                        cellContent = string.Format("<input type='checkbox' disabled='disabled' {0}/>",
                            Convert.ToBoolean(value) ? "checked='checked'" : string.Empty);
                    }
                    else if (value is IFormattable)
                    {
                        cellContent = ((IFormattable) value).ToString(column.Format, null);
                    }
                    else
                    {
                        cellContent = string.Format("{0}", value);
                    }
                    sb.AppendFormat("\t\t<td class='{1}'>{0}</td>\n", cellContent.Replace("\n", "<br/>"),
                        column.CssClass);
                }
                else //если команда
                {
                    //сначала достаем значения параметров из полей объекта сущности, и создаём строку с параметрами
                    //далее парсим строку с ссылкой и достаем оттуда href
                    //конкатенируем общий href со строкой параметров и записываем в строку с ссылкой новый href
                    var param = "&";
                    foreach (string key in commandColumn.Parameters.Keys)
                    {
                        var propName = commandColumn.Parameters[key];
                        var paramValue = row != null
                            ? row[propName]
                            : objType.GetProperty(propName).GetValue(obj, null);
                        param += string.Format("{0}={1}", key, paramValue);
                    }
                    param = param.TrimStart('&');
                    var commonHref =
                        commandColumn.CommandLink.ToString().Split(new[] {"href"}, StringSplitOptions.None)[1].Split('"')
                            [1];
                    sb.AppendFormat("\t\t<td>{0}</td>\n",
                        commandColumn.CommandLink.ToString().Replace(commonHref,
                            string.Format("{0}?{1}", commonHref, param)));
                }
            }

            sb.AppendLine("\t</tr>");
        }

        private static string GetTreeTableAttributes(bool isTreeTable, object obj)
        {
            var result = new StringBuilder();
            var treeObj = obj as ITree;

            if (isTreeTable == false || treeObj == null)
            {
                return result.ToString();
            }

            if (treeObj.ParentId.HasValue)
            {
                result.AppendFormat("data-parent-id='{0}'", treeObj.ParentId);
            }

            return result.ToString();
        }

        private static void BuildTableHeader(StringBuilder sb, IEnumerable<DataTablesColumn> columns)
        {
            sb.AppendLine("\t<thead><tr>");

            foreach (DataTablesColumn column in columns)
            {
                if (column == null) continue;

                var cssClass = new StringBuilder();
                if (!column.Sortable)
                    cssClass.Append("not-sortable ");
                if (!column.Searchable)
                    cssClass.Append("not-searchable ");
                if (column.Selectable)
                    cssClass.Append("selectable ");
                if (column.CheckBoxColumn)
                    cssClass.Append("checkboxColumn ");

                sb.AppendFormat("\t\t<th class='{1}' width='{2}'>{0}</th>\n", column.Caption, cssClass,
                    !string.IsNullOrEmpty(column.Width)
                        ? column.Width
                        : string.IsNullOrWhiteSpace(column.Caption) ? "5%" : string.Empty);
            }

            sb.AppendLine("\t</tr></thead>");
        }

        private static void BuildTableFooter(StringBuilder sb, IList<DataTablesColumn> columns)
        {
            sb.AppendLine("\t<tfoot><tr>");

            foreach (DataTablesColumn column in columns)
            {
                if (column == null || !column.Visible) continue;

                var cssClass = string.Empty;
                sb.Append("\t\t<th");
                if (!string.IsNullOrEmpty(cssClass))
                    sb.AppendFormat(" class='{0}'", cssClass);
                var width = !string.IsNullOrEmpty(column.Width)
                    ? column.Width
                    : string.IsNullOrWhiteSpace(column.Caption) ? "5%" : string.Empty;
                if (!string.IsNullOrEmpty(width))
                    sb.AppendFormat(" width='{0}'", width);
                sb.Append("></th>\n");
                //sb.AppendFormat("\t\t<th class='{0}' width='{1}'></th>\n",
                //    cssClass,
                //    !string.IsNullOrEmpty(column.Width) ? column.Width : string.IsNullOrWhiteSpace(column.Caption) ? "5%" : string.Empty);
            }

            sb.AppendLine("\t</tr></tfoot>");
        }

        private static string JSWrapTable(string name, DataTableSettingsBase settings, string config,
            string responseName)
        {
            var columns = settings.GetColumnsList();
            var initTreeTable = settings.IsTreeTableFlag
                ? string.Format(
                    "$('#{0}').treetable({{ expandable: true, nodeIdAttr: 'id', parentIdAttr: 'parentId' }});", name)
                : string.Empty;

            string values = string.Join(", ", columns.Select(c =>
                string.IsNullOrEmpty(c.FieldName)
                    ? string.Empty
                    : (c.FieldName.StartsWith("'")
                        ? c.FieldName
                        : string.Format("{0}.{1}", responseName, c.FieldName))
                ));

            var indexesInvisibleColumns = columns.Where(x => x.Visible == false)
                .Select(x => columns.ToList().IndexOf(x));

            //todo реализовать возможность описывать обёртку таблицы сверху
            return string.Format(@"
<script type='text/javascript'>
    var {0} = null;
    $(function () {{
        {0} = BuildDataTable($('#{0}'), {1});

        {0}.DataTable().columns([{5}]).visible( false );

        {4}
    }});

    function {0}_onDelete({2}, status, data) {{
        if (!{2}.success) {{ notify.showWarning({2}.message); }}
        else {{
            var _tr = $('#{0} tr[data-id=""' + {2}.id + '""]');
            {0}.fnDeleteRow(_tr[0]);
            notify.showSuccess({2}.message);
        }}

        if(typeof({0}_DeleteRow) === ""function"") {{
                {0}_DeleteRow();
        }}
}}

    function {0}_afterEdit({2}) {{
	    var values = [ {3} ];
	    var id = {2}.Id;
	    var tr = $('#{0} tr[data-id=""' + id + '""]');
	    if (tr.length == 0) {{
	        var index = {0}.fnAddData(values);
	        var node = {0}.fnGetNodes(index);
	        $(node).attr('data-clicklink', ""/"" + id);
	        $(node).attr('data-id', id);
            if(typeof({0}_AddRow) === ""function"") {{
                    {0}_AddRow();
            }}
	    }} else {{
	        {0}.fnUpdate(values, tr[0]);
            if(typeof({0}_UpdateRow) === ""function"") {{
                    {0}_UpdateRow();
            }}
	    }}
    }}
</script>
", name, config, responseName, values, initTreeTable, string.Join(",", indexesInvisibleColumns));
        }

        #endregion Table

        #region Labels

        public static MvcHtmlString LabelReq<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string text)
        {
            return htmlHelper.LabelFor(expression, text, new { @required = true });
        }

        public static MvcHtmlString LabelReq<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.LabelFor(expression, new { @required = true });
        }

        #endregion Labels


        public static MvcHtmlString TextBoxMD<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, bool required = false, bool readOnly = false)
        {
            var attribute = readOnly ? new { @readonly = "readonly" } : null;

            var label = htmlHelper.LabelFor(expression);
            var req = required ? "<i class='material-icons red-text prefix'>star</i>" : null;
            var textbox = htmlHelper.TextBoxFor(expression, attribute);
            var vltext = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(string.Concat("<div class='row'><div class='input-field col s12'>", req,
                label.ToString(), textbox.ToString(), vltext.ToString(),
                "</div></div>"));
        }

        public static MvcHtmlString DropDownMD<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, SelectList selectList, bool required = false, bool readOnly = false)
        {
            var label = htmlHelper.LabelFor(expression);
            var req = required ? "<i class='material-icons red-text prefix'>star</i>" : null;
            var dropdown = htmlHelper.DropDownListFor(expression, selectList);
            var vltext = htmlHelper.ValidationMessageFor(expression);

            if (readOnly)
            {
                dropdown = DropDownListReadOnly(htmlHelper, expression, selectList);
            }

            return new MvcHtmlString(string.Concat("<div class='row'><div class='input-field col s12'>", req,
                dropdown.ToString(), label.ToString(), vltext.ToString(), "</div></div>"));
        }

        public static MvcHtmlString TextAreaMD<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, bool required = false, bool readOnly = false)
        {
            object attribute = null;
            if (readOnly)
                attribute = new { @class = "materialize-textarea", @readonly = "readonly" };
            else
                attribute = new { @class = "materialize-textarea" };

            var label = htmlHelper.LabelFor(expression);
            var req = required ? "<i class='material-icons red-text prefix'>star</i>" : null;
            var textarea = htmlHelper.TextAreaFor(expression, attribute);
            var vltext = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(string.Concat("<div class='row'><div class='input-field col s12'>", req,
                label.ToString(), textarea.ToString(), vltext.ToString(),
                "</div></div>"));
        }

        public static MvcHtmlString DateBoxMD<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, bool required = false, bool readOnly = false)
        {
            object attribute;
            if(readOnly)
                attribute = new {@readonly = "readonly"};
            else
                attribute = new { @class = "datepicker" };

            var label = htmlHelper.LabelFor(expression);
            var req = required ? "<i class='material-icons red-text prefix'>star</i>" : null;
            var datebox = htmlHelper.TextBoxFor(expression, attribute); //"<input type='text' class='datepciker'>";//htmlHelper.DateBox(expression);
            var vltext = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(string.Concat("<div class='row'><div class='input-field col s12'>", req,
                label.ToString(), datebox.ToString(), vltext.ToString(),
                "</div></div>"));
        }

        public static MvcHtmlString FileUploadMD<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, FileUploadViewModel fileUploadVM, bool required = false, bool readOnly = false)
        {
            var label = htmlHelper.LabelFor(expression);
            var req = required ? "<i class='material-icons red-text prefix'>star</i>" : null;
            var fileUpload = htmlHelper.FileUpload(expression, fileUploadVM, readOnly);
            var vltext = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(string.Concat("<div class='row'>", req, label, "<div class='input-field col s12'>", fileUpload.ToString(), vltext.ToString(),
                "</div></div>"));
        }

        public static MvcHtmlString SelectEntityMD<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string url, bool required = false, bool readOnly = false)
        {
            var label = htmlHelper.LabelFor(expression, new { @class = "active" });
            var req = required ? "<i class='material-icons red-text prefix'>star</i>" : null;
            var selectentity = htmlHelper.SelectEntity(expression, url, false, readOnly);
            var vltext = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(string.Concat("<div class='row'><div class='input-field col s12'>", req,
                label.ToString(), selectentity.ToString(), vltext.ToString(),
                "</div></div>"));
        }

        public static MvcHtmlString CheckBoxMD<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression)
        {
            var label = htmlHelper.LabelFor(expression);

            var modelMeta = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var checkbox = string.Concat("<input type='checkbox' class='filled-in' id='", modelMeta.PropertyName, "'", (bool)modelMeta.Model ? "checked" : string.Empty, "/>");
            var vltext = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(string.Concat("<p>", 
                 checkbox.ToString(), label.ToString(), vltext.ToString(),
                "</p>"));
        }

        public static MvcHtmlString RadioButtonMD<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, string text, object value)
        {
            var label = htmlHelper.LabelFor(expression, text);
            var radio = htmlHelper.RadioButtonFor(expression, value);
            var vltext = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(string.Concat(radio.ToString(), label.ToString(), vltext.ToString()));
        }

        public static MvcHtmlString PasswordMD<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, bool required = false)
        {
            var label = htmlHelper.LabelFor(expression);
            var req = required ? "<i class='material-icons red-text prefix'>star</i>" : null;
            var textbox = htmlHelper.PasswordFor(expression);
            var vltext = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(string.Concat("<div class='row'><div class='input-field col s12'>", req,
                label.ToString(), textbox.ToString(), vltext.ToString(),
                "</div></div>"));
        }
        #region Text

        
        /// <summary>
        /// Обертка над TextBoxFor
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper">HtmlHelper</param>
        /// <param name="expression">Выражение, описывающее отображаемое свойство модели</param>
        /// <param name="readOnly">Только для чтения</param>
        /// <param name="validationRequired">Добавлять сообщение о валидации для поля</param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString TextBox<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            bool readOnly = false,
            bool validationRequired = true,
            IDictionary<string, object> htmlAttributes = null)
        {
            if (readOnly)
            {
                return htmlHelper.TextBoxFor(expression, new { @readonly = "readonly" });
            }

            var textbox = htmlHelper.TextBoxFor(expression, htmlAttributes);

            var validation = new MvcHtmlString(string.Empty);
            if (validationRequired)
            {
                validation = htmlHelper.ValidationMessageFor(expression);
            }

            return new MvcHtmlString(textbox.ToString() + validation.ToString());
        }

        /// <summary>
        /// Обертка над PasswordFor
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper">HtmlHelper</param>
        /// <param name="expression">Выражение, описывающее отображаемое свойство модели</param>
        /// <param name="readOnly">Только для чтения</param>
        /// <param name="validationRequired">Добавлять сообщение о валидации для поля</param>
        /// <returns></returns>
        public static MvcHtmlString PasswordBox<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, bool readOnly = false, bool validationRequired = true)
        {
            if (readOnly)
                return htmlHelper.DisplayTextFor(expression);
            var textbox = htmlHelper.PasswordFor<TModel, TProperty>(expression);
            var validation = new MvcHtmlString(string.Empty);
            if (validationRequired)
                validation = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(textbox.ToString() + validation.ToString());
        }

        /// <summary>
        /// Обертка над TextAreaFor
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper">HtmlHelper</param>
        /// <param name="expression">Выражение, описывающее отображаемое свойство модели</param>
        /// <param name="readOnly">Только для чтения</param>
        /// <param name="validationRequired">Добавлять сообщение о валидации для поля</param>
        /// <returns></returns>
        public static MvcHtmlString TextArea<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, bool readOnly = false, bool validationRequired = true)
        {
            if (readOnly)
            {
                return htmlHelper.TextAreaFor<TModel, TProperty>(expression, new { @readonly = "readonly" });
            }
            var textarea = htmlHelper.TextAreaFor<TModel, TProperty>(expression);
            var validation = new MvcHtmlString(string.Empty);
            if (validationRequired)
                validation = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(textarea.ToString() + validation.ToString());
        }

        /// <summary>
        /// Обертка над TextBoxFor
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper">HtmlHelper</param>
        /// <param name="expression">Выражение, описывающее отображаемое свойство модели</param>
        /// <param name="readOnly">Только для чтения</param>
        /// <param name="validationRequired">Добавлять сообщение о валидации для поля</param>
        /// <param name="htmlAttributes"></param>
        /// <param name="source">JSON источник</param>
        /// <param name="urlSource">URL на источник</param>
        /// <param name="functionSelect">JS функция, которая вызывается по событию Select</param>
        /// <returns></returns>
        public static MvcHtmlString AutocompleteTextBox<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            bool readOnly = false,
            bool validationRequired = true,
            IDictionary<string, object> htmlAttributes = null,
            string source = null,
            string urlSource = null,
            string functionSelect = null)
        {
            var textBox = htmlHelper.TextBox(expression, readOnly, validationRequired, htmlAttributes);

            var js = new StringBuilder("<script type='text/javascript'>  $(function() {");

            if (string.IsNullOrWhiteSpace(source) == false)
            {
                js.AppendFormat(" InitAutocomplete('#{0}', {1});", ExpressionHelper.GetExpressionText(expression), htmlHelper.Raw(source));
            }
            else if (string.IsNullOrWhiteSpace(urlSource) == false)
            {
                js.AppendFormat(
                    @" $('#{0}').autocomplete({{
                            source: function(request, response) {{
                                $.ajax({{
                                    url: '{1}',
                                    data: {{
                                        term: request.term
                                    }},
                                    success: function(data) {{
                                        response(data);
                                    }}
                                }});
                            }}", ExpressionHelper.GetExpressionText(expression), urlSource
                    );

                if (string.IsNullOrWhiteSpace(functionSelect) == false)
                {
                    js.AppendFormat(@",
                            select: function(event, ui){{ {0}(event, ui); }}
                        }});", functionSelect);
                }
                else
                {
                    js.Append("});");
                }
            }
            else
            {
                throw new ArgumentException("AutocompleteTextBox. Не указан источник данных.");
            }

            js.Append(@" }); </script>");


            return new MvcHtmlString(textBox.ToString() + js.ToString());
        }


        #endregion Text

        #region DDL

        /// <summary>
        /// Обертка над DropDownListFor
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper">HtmlHelper</param>
        /// <param name="expression">Выражение, описывающее отображаемое свойство модели</param>
        /// <param name="readOnly">Только для чтения</param>
        /// <param name="validationRequired">Добавлять сообщение о валидации для поля</param>
        /// <param name="classes">Классы</param>
        /// <param name="htmlAttributes">Атрибуты</param>
        /// <returns></returns>
        public static MvcHtmlString DropDownList<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            SelectList selectList,
            bool readOnly = false,
            bool validationRequired = true,
            string optionLabel = "-Выберите значение-",
            IDictionary<string, object> htmlAttributes = null)
        {

            if (selectList == null)
                throw new ArgumentNullException("SelectList is null");

            if (readOnly)
            {
                return DropDownListReadOnly(htmlHelper, expression, selectList);
            }

            var dropdown = htmlHelper.DropDownListFor(expression, selectList, htmlAttributes);

            var validation = new MvcHtmlString(string.Empty);
            if (validationRequired)
                validation = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(dropdown.ToString() + validation.ToString());
        }

        public static MvcHtmlString DropDownList<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            SelectList selectList,
            string label = null,
            bool readOnly = false,
            bool validationRequired = true,
            string optionLabel = "-Выберите значение-",
            IDictionary<string, object> htmlAttributes = null)
        {

            if (selectList == null)
                throw new ArgumentNullException("SelectList is null");

            if (readOnly)
            {
                return DropDownListReadOnly(htmlHelper, expression, selectList);
            }

            var dropdown = htmlHelper.DropDownListFor(expression, selectList, htmlAttributes);

            var validation = new MvcHtmlString(string.Empty);
            if (validationRequired)
                validation = htmlHelper.ValidationMessageFor(expression);

            return string.IsNullOrWhiteSpace(label) ? new MvcHtmlString(dropdown.ToString() + validation.ToString()) : new MvcHtmlString(dropdown.ToString() + "<label>" + label + "</label>" + validation.ToString());
        }

        /// <summary>
        /// Multi DDL
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper">HtmlHelper</param>
        /// <param name="expression">Выражение, описывающее отображаемое свойство модели</param>
        /// <param name="selectList"></param>
        /// <param name="readOnly">Только для чтения</param>
        /// <param name="validationRequired">Добавляет сообщение о валидации для поля</param>
        /// <returns></returns>
        public static MvcHtmlString DropDownListMulti<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            SelectList selectList,
            bool readOnly = false,
            bool validationRequired = true)
        {
            var htmlAttributes = new Dictionary<string, object>();
            htmlAttributes.Add("class", "chosen-select");
            htmlAttributes.Add("multiple", "multiple");
            htmlAttributes.Add("data-placeholder", "Выберите значение");

            if (selectList == null)
                throw new ArgumentNullException("SelectList is null");

            if (readOnly)
            {
                return DropDownListReadOnly(htmlHelper, expression, selectList);
            }

            var dropdown = htmlHelper.ListBoxFor(expression, selectList, htmlAttributes);

            var validation = new MvcHtmlString(string.Empty);
            if (validationRequired)
                validation = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(dropdown.ToString() + validation.ToString());
        }

        private static MvcHtmlString DropDownListReadOnly<TModel, TProperty>(HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            SelectList selectList)
        {
            var modelMeta = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            string value = modelMeta.Model != null ? modelMeta.Model.ToString() : string.Empty;

            string str = null;

            foreach (var item in selectList)
                if (item.Value.Equals(value))
                {
                    str = item.Text;
                    break;
                }

            return new MvcHtmlString(htmlHelper.HiddenFor<TModel, TProperty>(expression).ToString() +
                                     htmlHelper.TextBox(string.Concat(modelMeta.PropertyName, "_RO"), str,
                                         new { @readonly = "readonly" }).ToString());
        }


        #endregion DDL

        #region Buttons

        /// <summary>
        /// Кнопка "Сохранить"
        /// </summary>
        public static MvcHtmlString SaveButton(this HtmlHelper htmlHelper, string onclick, bool readOnly = false)
        {
            return Button("Сохранить", onclick, "waves-effect waves-light btn", null, readOnly);
        }

        /// <summary>
        /// Кнопка "Добавить"
        /// </summary>
        public static MvcHtmlString AddButton(this HtmlHelper htmlHelper, string onclick, bool readOnly = false)
        {
            return Button("Добавить", onclick, "waves-effect waves-light btn", readOnly: readOnly, notSubmit: true);
        }

        public static MvcHtmlString Button(this HtmlHelper htmlHelper, string text, string onclick, string @class = null, string title = null, bool readOnly = false, bool notSubmit = false)
        {
            return Button(text, onclick, "waves-effect waves-light btn", title, readOnly, notSubmit: notSubmit);
        }

        private static MvcHtmlString Button(string text, string onclick, string @class = null, string title = null, bool readOnly = false, bool notSubmit = false)
        {
            var builder = new TagBuilder("button");
            builder.InnerHtml = text;
            builder.Attributes.Add("onclick", onclick);

            if (@class != null)
                builder.AddCssClass(@class);

            if (title != null)
                builder.Attributes.Add("title", title);

            if (readOnly)
                builder.Attributes.Add("disabled", "true");

            if (notSubmit)
                builder.Attributes.Add("type", "button");


            return MvcHtmlString.Create(builder.ToString());
        }

        #endregion Buttons

        #region AjaxLink

        public static MvcHtmlString AjaxLink(this HtmlHelper htmlHelper, string text, string callbackFunctionName, string confirmMessage, string @class = null, string title = null, bool readOnly = false)
        {
            return AjaxLink(text, callbackFunctionName, confirmMessage, @class, title, readOnly);
        }

        public static MvcHtmlString AjaxLink(this HtmlHelper htmlHelper, AjaxLinkViewModel viewModel)
        {
            return AjaxLink(viewModel.Text, viewModel.Callback, viewModel.ConfirmMessage, viewModel.Class, viewModel.Title, viewModel.Disabled);
        }

        private static MvcHtmlString AjaxLink(string text, string callbackFunctionName, string confirmMessage, string @class = null, string title = null, bool readOnly = false)
        {
            var builder = new TagBuilder("a");
            builder.InnerHtml = text;
            builder.Attributes.Add("href", "#");
            builder.Attributes.Add("data-ajax", "true");

            if (callbackFunctionName != null)
                builder.Attributes.Add("data-ajax-success", callbackFunctionName);
            if (confirmMessage != null)
                builder.Attributes.Add("data-ajax-confirm", confirmMessage);

            if (@class != null)
                builder.AddCssClass(@class);
            if (title != null)
                builder.Attributes.Add("title", title);
            if (readOnly)
                builder.Attributes.Add("disabled", "true");

            return MvcHtmlString.Create(builder.ToString());
        }

        #endregion AjaxLink

        #region Other

        /// <summary>
        /// Обертка над CheckBoxFor
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper">HtmlHelper</param>
        /// <param name="expression">Выражение, описывающее отображаемое свойство модели</param>
        /// <param name="readOnly">Только для чтения</param>
        /// <param name="validationRequired">Добавлять сообщение о валидации для поля</param>
        /// <returns></returns>
        public static MvcHtmlString CheckBox<TModel>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, bool>> expression, bool readOnly = false, bool validationRequired = true)
        {
            if (readOnly)
                return htmlHelper.CheckBoxFor(expression, new { disabled = "disabled" });
            var textbox = htmlHelper.CheckBoxFor(expression);
            var validation = new MvcHtmlString(string.Empty);
            if (validationRequired)
                validation = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(textbox.ToString() + validation.ToString());
        }

        /// <summary>
        /// Обертка над частичной вьюхой Date
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper">HtmlHelper</param>
        /// <param name="expression">Выражение, описывающее отображаемое свойство модели</param>
        /// <param name="readOnly">Только для чтения</param>
        /// <param name="validationRequired">Добавлять сообщение о валидации для поля</param>
        /// <returns></returns>
        public static MvcHtmlString DateBox<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, bool readOnly = false, bool validationRequired = true)
        {
            var modelMeta = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var value = (DateTime?)modelMeta.Model;
            var name = modelMeta.PropertyName;

            var partial = htmlHelper.Partial("Date", new DateViewModel { Name = name, Value = value, ReadOnly = readOnly });
            var validation = new MvcHtmlString(string.Empty);
            if (validationRequired)
                validation = htmlHelper.ValidationMessageFor(expression);

            return new MvcHtmlString(partial.ToString() + validation.ToString());
        }

        /// <summary>
        /// Обертка над частичной вьюхой SelectEntity
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper">HtmlHelper</param>
        /// <param name="expression">Выражение, описывающее отображаемое свойство модели</param>
        /// <param name="url">Строка, содержащая адрес вьюхи отображаемого списка</param>
        /// <param name="readOnly">Только для чтения</param>
        /// <param name="validationRequired">Добавлять сообщение о валидации для поля</param>
        /// <returns></returns>
        public static MvcHtmlString SelectEntity<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, string url, bool multiple = false, bool readOnly = false,
            bool validationRequired = true, string jsCallback = "")
        {
            var modelMeta = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var value = modelMeta.Model as List<IEntityName>;
            var name = modelMeta.PropertyName;

            var partial = htmlHelper.Partial("SelectEntity", new SelectEntityViewModel()
            {
                Name = name,
                Data = value,
                SelectUrl = url,
                IsMultipleSelection = multiple,
                ReadOnly = readOnly,
                IsRequired = modelMeta.IsRequired,
                SelectEntityCallback = jsCallback
            });

            var validation = new MvcHtmlString(string.Empty);
            if (validationRequired)
                validation = htmlHelper.ValidationMessage(string.Format("hf_{0}", name));

            return new MvcHtmlString(partial.ToString() + validation.ToString());
        }

        /// <summary>
        /// Обертка на частичной вьюхой FileUpload
        /// </summary>
        /// <param name="htmlHelper">HtmlHelper</param>
        /// <param name="name">Имя контрола</param>
        /// <returns></returns>
        public static MvcHtmlString FileUpload<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, FileUploadViewModel model, bool readOnly = false)
        {
            var modelMeta = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var value = modelMeta.Model as IEnumerable<IEntityName>;
            var name = modelMeta.PropertyName;

            model.Name = model.Name ?? name;
            model.Files = value;
            model.ReadOnly = readOnly;

            return htmlHelper.Partial("FileUpload", model);
        }

        /// <summary>
        /// обертка над BeginForm
        /// внутрь тега form добавляет div с классом edit_form или edit_form_readonly в зависимости от параметра
        /// </summary>
        /// <param name="htmlHelper">HtmlHelper</param>
        /// <param name="readOnly">параметр, в соотвествии с которым выставляет нужный класс контейнеру</param>
        /// <returns></returns>
        public static MvcFormExtensions FormBegin(this HtmlHelper htmlHelper, bool readOnly = false)
        {
            htmlHelper.BeginForm();
            var form = new MvcFormExtensions(htmlHelper.ViewContext, htmlHelper, readOnly);
            return form;
        }

        #endregion Other

    }
}