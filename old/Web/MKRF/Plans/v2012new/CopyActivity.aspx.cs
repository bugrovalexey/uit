﻿using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxTreeView;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Controllers;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class CopyActivity : System.Web.UI.Page
    {
        CopyActivityController controller_internal = null;
        public CopyActivityController Controller
        {
            get { return controller_internal; }
        }

        protected override void OnInit(EventArgs e)
        {
            controller_internal = new CopyActivityController(Request.QueryString["id"]);
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            treeView.ExpandToLevel(1);

            if (Controller.IsFirstLoad)
            {
                Controller.Clone();
                treeView.RefreshVirtualTree();
                treeView.ExpandToLevel(1);
            }
            base.OnPreRender(e);
        }


        protected void treeView_VirtualModeNodeCreating(object sender, TreeListVirtualModeNodeCreatingEventArgs e)
        {
            var dataItem = (e.NodeObject as TreeViewVirtualNode).DataItem as CopyActivityController.TreeItemViewModel;
            e.SetNodeValue("Key", dataItem.Key);
            e.SetNodeValue("ParentKey", dataItem.ParentKey);
            e.SetNodeValue("Name", dataItem.Name);
            e.SetNodeValue("Y0", dataItem.Y0);
            e.SetNodeValue("Y1", dataItem.Y1);
            e.SetNodeValue("Y2", dataItem.Y2);
            e.SetNodeValue("AddY0", dataItem.AddY0);
            e.SetNodeValue("AddY1", dataItem.AddY1);
            e.SetNodeValue("AddY2", dataItem.AddY2);

            e.SetNodeValue("JSON", dataItem.JSON);

            e.NodeKeyValue = dataItem.Key;
        }

        protected void treeView_VirtualModeCreateChildren(object sender, TreeListVirtualModeCreateChildrenEventArgs e)
        {
            var nodeObject = (e.NodeObject as TreeViewVirtualNode);

            var data = Controller.GetChildrenNodes(nodeObject != null ? nodeObject.Name : null);

            var children = (data == null) ? null :
                                            data.Select(x => new TreeViewVirtualNode(x.Key, x.Name)
                                            {
                                                DataItem = x,
                                                IsLeaf = false,
                                            }).ToList();

            e.Children = children;
        }

        protected void treeView_NodeInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            updateValues(null, e.NewValues);
            Controller.SaveOrUpdate(e.NewValues);
            if (CopyActivityController.IsActivity(currentNodeKey))
            {
                treeView.ExpandToLevel(1);
            }   
        }

        protected void treeView_NodeUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            updateValues(e.Keys[0], e.NewValues);
            Controller.SaveOrUpdate(e.NewValues);
        }

        protected void treeView_NodeDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            //updateValues(e.Keys[0], e.Values);
            updateKeys(e.Keys[0]);
            e.Values["currentNodeKey"] = currentNodeKey;
            e.Values["parentNodeKey"] = parentNodeKey;
            Controller.Delete(e.Values);
        }

        protected void treeView_CustomJSProperties(object sender, TreeListCustomJSPropertiesEventArgs e)
        {
            e.Properties["cpActivityList"] = string.Join(",", Controller.GetAllActivities().Select(x => x.Key));
            e.Properties["cpCurrentForm"] = Controller.GetNodeType(currentNodeKey);            
            Controller.SaveJSON();
        }

        protected T getValue<T>(object json, string fieldName)
        {
            return CopyActivityController.getValue<T>(json, fieldName);
        }

        protected object GetNameText(object DataItem)
        {
            var json = DataBinder.Eval(DataItem, "JSON");
            var name = Convert.ToString(DataBinder.Eval(DataItem, "Name"));

            var key = getValue<string>(json, "Key");
            var isPriority = getValue<bool>(json, "IsPriority");

            if (CopyActivityController.IsActivity(key))
            {
                return string.Format("<span class='itemText activityText'>{0}</span>", name);
            }
            else if (CopyActivityController.IsIkt(key))
            {
                //<span style='font-size:0.8em'>Направления расходования средств на ИКТ</span><br>
                return string.Format("<span class='itemText iktText'>{0}</span>", name);
            }
            else if (CopyActivityController.IsWork(key))
            {
                var okdp = getValue<string>(json, "OKDP");
                var code = getValue<string>(json, "Code");
                return string.Format("<span class='itemText'><b>{0}</b> {1}</span>", code, name);  // , string.IsNullOrWhiteSpace(okdp) ? null : ("<br/><span class='subText'>Код ОКДП: " + okdp + "</span>")
            }
            else if (CopyActivityController.IsSpec(key))
            {
                var code = getValue<string>(json, "Code");
                return string.Format("<span class='itemText'><b>{0}</b> {1}</span><br/>", code, name);
            }
            else if (CopyActivityController.IsSpecType(key))
            {
                var cname = getValue<string>(json, "CharName");
                var cunit = getValue<string>(json, "CharUnit");
                var cvalue = getValue<string>(json, "CharValue");

                var ctext = string.Join(" - ", (new string[] { cname, (cvalue + " " + cunit) })
                                                  .Where(x => !string.IsNullOrWhiteSpace(x))
                                                  .ToArray());
                
                return string.Format("<span class='itemText'>{0}</span>", ctext);
            }
            else
            {
                return string.Format("<span class='itemText'>{0}</span>", name);
            }
        }

        protected string getHeaderText(string template, int year, object sum, decimal remain)
        {
            return string.Format(template, year) + string.Format("<span class='headerSum {0}'>({1:#,##0.0000})<span class='headerNote'>*</span></span>", remain < decimal.Zero ? "sumRed" : "sumGreen", remain);
        }

        protected void treeView_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            bool isPriority = CopyActivityController.IsPriority(e.NodeKey);

            switch(e.Column.Name)
            {
                case "Y0":
                    {
                        if (CopyActivityController.IsSpecType(e.NodeKey))
                        {
                            e.Cell.Text = "-";
                        }
                        else if (isPriority)
                        {
                            e.Cell.Text = "";
                        }
                        else if (CopyActivityController.IsActivity(e.NodeKey) || CopyActivityController.IsIkt(e.NodeKey))
                        {
                            var json = treeView.FindNodeByKeyValue(e.NodeKey).GetValue("JSON");
                            var redValues = getValue<string>(json, "RedValues");
                            if (!string.IsNullOrEmpty(redValues))
                            {
                                if (redValues.IndexOf(e.Column.FieldName + "@") >= 0)
                                {
                                    e.Cell.ForeColor = System.Drawing.Color.Red;
                                }

                            }

                        }
                    }
                    break;

                case "Y1":
                case "Y2":
                case "AddY0":
                case "AddY1":
                case "AddY2":
                    {
                        if ( CopyActivityController.IsWork(e.NodeKey) || 
                            CopyActivityController.IsWorkType(e.NodeKey) || 
                            CopyActivityController.IsSpec(e.NodeKey) || 
                            CopyActivityController.IsSpecType(e.NodeKey))
                        {
                            e.Cell.Text = "-";
                        }
                        else if (isPriority)
                        {
                            e.Cell.Text = "";
                        }
                        else if (CopyActivityController.IsActivity(e.NodeKey))
                        {
                            var json = treeView.FindNodeByKeyValue(e.NodeKey).GetValue("JSON");
                            var redValues = getValue<string>(json, "RedValues");
                            if (!string.IsNullOrEmpty(redValues))
                            {
                                if (redValues.IndexOf(e.Column.FieldName + "@") >= 0)
                                {
                                    e.Cell.ForeColor = System.Drawing.Color.Red;
                                }

                            }

                        }
                    }
                    break;

                default:

                    break;
            }
        }

        protected string getButtons(object key)
        {
            string buttonRow = Controller.GetCustomButtons(key);

            if (string.IsNullOrEmpty(buttonRow))
                return null;

            var button = buttonRow.Split(' ');
            var result = new StringBuilder();

            foreach (var buttonCode in button)
            {
                switch (buttonCode)
                {
                    case "add_ikt": result.AppendFormat("<a class='subCommand' href=\"javascript:StartEditNewNode('{0}', '{1}')\">Добавить вид затрат</a> ", CopyActivityController.CreateKeyForNewObject(CopyActivityController.NodeTypeIkt), key); break;
                    case "add_work": result.AppendFormat("<a class='subCommand' href=\"javascript:StartEditNewNode('{0}','{1}')\">Добавить работу</a> ", CopyActivityController.CreateKeyForNewObject(CopyActivityController.NodeTypeWork), key); break;
                    case "add_spec": result.AppendFormat("<a class='subCommand' href=\"javascript:StartEditNewNode('{0}','{1}')\">Добавить товар</a> ", CopyActivityController.CreateKeyForNewObject(CopyActivityController.NodeTypeSpec), key); break;
                    case "add_worktype": result.AppendFormat("<a class='subCommand' href=\"javascript:StartEditNewNode('{0}','{1}')\">Добавить затраты</a> ", CopyActivityController.CreateKeyForNewObject(CopyActivityController.NodeTypeWorkType), key); break;
                    case "add_spectype": result.AppendFormat("<a class='subCommand' href=\"javascript:StartEditNewNode('{0}','{1}')\">Добавить характеристику</a> ", CopyActivityController.CreateKeyForNewObject(CopyActivityController.NodeTypeSpecType), key); break;
                }
            }

            return result.ToString();
        }

        private string currentnodeKey_internal = null;
        protected string currentNodeKey
        {
            get
            {
                return currentnodeKey_internal;
            }
            set
            {
                currentnodeKey_internal = value;
                if (CopyActivityController.IsActivity(currentnodeKey_internal)) { treeView.SettingsPopupEditForm.Caption = "Мероприятие"; }
                else if (CopyActivityController.IsIkt(currentnodeKey_internal)) { treeView.SettingsPopupEditForm.Caption = "Направления расходования средств на ИКТ "; }
                else if (CopyActivityController.IsWork(currentnodeKey_internal)) { treeView.SettingsPopupEditForm.Caption = "Работа"; }
                else if (CopyActivityController.IsWorkType(currentnodeKey_internal)) { treeView.SettingsPopupEditForm.Caption = "Затраты"; }
                else if (CopyActivityController.IsSpec(currentnodeKey_internal)) { treeView.SettingsPopupEditForm.Caption = "Товар"; }
                else if (CopyActivityController.IsSpecType(currentnodeKey_internal)) { treeView.SettingsPopupEditForm.Caption = "Характеристика товара"; }
            }
        }

        protected string parentNodeKey = null;

        protected void treeView_StartNodeEditing(object sender, TreeListNodeEditingEventArgs e)
        {
            updateKeys(e.NodeKey);
        }

        protected void treeView_InitNewNode(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            updateKeys();
        }

        void updateValues(object keys, OrderedDictionary Values)
        {
            updateKeys(keys);

            if (CopyActivityController.IsActivity(currentNodeKey))
            {
                Values["Name"] = (treeView.FindEditFormTemplateControl("ActivityNameMemo") as ASPxMemo).Text;
                Values["IsPriority"] = (treeView.FindEditFormTemplateControl("PriorityCheckBox") as ASPxCheckBox).Checked;
                Values["Y0"] = (treeView.FindEditFormTemplateControl("PlansActivityVolY0SpinEdit") as ASPxSpinEdit).Number;
                Values["Y1"] = (treeView.FindEditFormTemplateControl("PlansActivityVolY1SpinEdit") as ASPxSpinEdit).Number;
                Values["Y2"] = (treeView.FindEditFormTemplateControl("PlansActivityVolY2SpinEdit") as ASPxSpinEdit).Number;
                Values["AddY0"] = (treeView.FindEditFormTemplateControl("PlansActivityAddVolY0SpinEdit") as ASPxSpinEdit).Number;
                Values["AddY1"] = (treeView.FindEditFormTemplateControl("PlansActivityAddVolY1SpinEdit") as ASPxSpinEdit).Number;
                Values["AddY2"] = (treeView.FindEditFormTemplateControl("PlansActivityAddVolY2SpinEdit") as ASPxSpinEdit).Number;
            }
            else if (CopyActivityController.IsIkt(currentNodeKey))
            {
                Values["ExpId"] = (treeView.FindEditFormTemplateControl("ExpenseDirectionComboBox") as ASPxComboBox).Value;
                Values["Y0"] = (treeView.FindEditFormTemplateControl("IktY0") as ASPxSpinEdit).Number;
                Values["Y1"] = (treeView.FindEditFormTemplateControl("IktY1") as ASPxSpinEdit).Number;
                Values["Y2"] = (treeView.FindEditFormTemplateControl("IktY2") as ASPxSpinEdit).Number;
                Values["AddY0"] = (treeView.FindEditFormTemplateControl("IktAddY0") as ASPxSpinEdit).Number;
                Values["AddY1"] = (treeView.FindEditFormTemplateControl("IktAddY1") as ASPxSpinEdit).Number;
                Values["AddY2"] = (treeView.FindEditFormTemplateControl("IktAddY2") as ASPxSpinEdit).Number;
            }
            else if (CopyActivityController.IsWork(currentNodeKey))
            {
                Values["Name"] = (treeView.FindEditFormTemplateControl("WorkNameMemo") as ASPxMemo).Text;
                Values["OKDP"] = (treeView.FindEditFormTemplateControl("WorkOKDPTextBox") as ASPxTextBox).Text;
                Values["ExpenditureId"] = (treeView.FindEditFormTemplateControl("ExpenditureItemsComboBox") as ASPxComboBox).Value;
            }
            else if (CopyActivityController.IsWorkType(currentNodeKey))
            {
                Values["ExpTypeId"] = (treeView.FindEditFormTemplateControl("WorkExpTypeComboBox") as ASPxComboBox).Value;
                Values["Y0"] = (treeView.FindEditFormTemplateControl("WorkTypeY0") as ASPxTextBox).Text;
            }
            else if (CopyActivityController.IsSpec(currentNodeKey))
            {
                Values["Name"] = (treeView.FindEditFormTemplateControl("SpecNameMemo") as ASPxMemo).Text;
                Values["QualifierId"] = (treeView.FindEditFormTemplateControl("QualifierComboBox") as ASPxComboBox).Value;
                Values["OKDP"] = (treeView.FindEditFormTemplateControl("SpecOKDPTextBox") as ASPxTextBox).Text;
                Values["ExpTypeId"] = (treeView.FindEditFormTemplateControl("SpecExpTypeComboBox") as ASPxComboBox).Value;

                Values["Analog"] = (treeView.FindEditFormTemplateControl("AnalogMemo") as ASPxMemo).Text;
                Values["Quantity"] = (treeView.FindEditFormTemplateControl("Quantity_SpinEdit") as ASPxSpinEdit).Number;
                Values["AnalogPrice"] = (treeView.FindEditFormTemplateControl("AnalogPriceSpinEdit") as ASPxSpinEdit).Number;
                Values["Cost"] = (treeView.FindEditFormTemplateControl("CostSpinEdit") as ASPxSpinEdit).Number;
            }
            else if (CopyActivityController.IsSpecType(currentNodeKey))
            {
                Values["CharName"] = (treeView.FindEditFormTemplateControl("STCharNameTextBox") as ASPxTextBox).Text;
                Values["CharUnit"] = (treeView.FindEditFormTemplateControl("STCharUnitTextBox") as ASPxTextBox).Text;
                Values["CharValue"] = (treeView.FindEditFormTemplateControl("STCharValueTextBox") as ASPxTextBox).Text;
            }

            Values["currentNodeKey"] = currentNodeKey;
            Values["parentNodeKey"] = parentNodeKey;
        }

        void updateKeys(object key = null)
        {
            //currentNodeKey = Convert.ToString(hf.Contains("newNodeKey") ? hf["newNodeKey"] : null);
            //parentNodeKey = Convert.ToString(hf.Contains("parentNodeKey") ? hf["parentNodeKey"] : null);

            currentNodeKey = Convert.ToString(key);
            parentNodeKey = null;

            if (string.IsNullOrEmpty(currentNodeKey))
            {
                // новая строка
                currentNodeKey = Convert.ToString(hf.Contains("newNodeKey") ? hf["newNodeKey"] : null);
            }

            if (string.IsNullOrEmpty(currentNodeKey))
            {
                return;
            }

            var currentNode = treeView.FindNodeByKeyValue(currentNodeKey);
            if (currentNode != null)
            {
                parentNodeKey = currentNode.ParentNode != null ? currentNode.ParentNode.Key : null;
            }
            else
            {
                // новая строка
                parentNodeKey = Convert.ToString(hf.Contains("parentNodeKey") ? hf["parentNodeKey"] : null);
            }
        }

        protected void ExpenseDirectionComboBox_DataBinding(object sender, EventArgs e)
        {
            var comboBox = (sender as ASPxComboBox);

            if (comboBox.Visible == false)
                return;

            comboBox.DataSource = Controller.GetExpenseDirections(); //parentNodeKey

            //box.Value = isNewExpenseDirectionsRow ? listStr.FirstOrDefault().Key : currentDirection.ExpenseDirection.Id;


        }

        protected void WorkExpTypeComboBox_DataBinding(object sender, EventArgs e)
        {
            var comboBox = (sender as ASPxComboBox);

            if (comboBox.Visible == false)
                return;

            comboBox.DataSource = Controller.GetExpenseTypes(parentNodeKey);
        }

        protected void QualifierComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = (ASPxComboBox)sender;

            if (box.Visible == false)
                return;

            box.DataSource = Controller.GetQualifierList(); // parentNodeKey
        }

        protected void SpecExpTypeComboBox_DataBinding(object sender, EventArgs e)
        {
            var comboBox = (sender as ASPxComboBox);

            if (comboBox.Visible == false)
                return;

            comboBox.DataSource = Controller.GetExpenseTypes(parentNodeKey);
        }

        protected void ExpenditureItemsComboBox_DataBinding(object sender, EventArgs e)
        {
            var comboBox = (sender as ASPxComboBox);

            if (comboBox.Visible == false)
                return;

            comboBox.DataSource = Controller.GetExpenditures();
        }

        protected void copyCallback_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            if (e.Parameter.StartsWith("doCopy"))
            {
                var activityKey = e.Parameter.Substring(6);
                var deleteSource = deleteActivityCheckBox.Checked;
                Controller.DoCopy(activityKey, deleteSource);
            }
            else if (e.Parameter.StartsWith("clone"))
            {
                Controller.Clone();
            }

        }

        protected void treeView_CommandColumnButtonInitialize(object sender, TreeListCommandColumnButtonEventArgs e)
        {
            if (CopyActivityController.IsPriority(e.NodeKey))
            {
                e.Visible = DevExpress.Utils.DefaultBoolean.False;
            }
        }

        protected void pnl_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            treeView.RefreshVirtualTree();
        }        
    }
}