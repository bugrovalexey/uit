﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Directories
{
    class ReportSpecTypeMap : ClassMapBase<ReportSpecType>
    {
        public ReportSpecTypeMap() :
            base("Report_Spec_Type")
        {

        }

        protected override void InitMap()
        {
            Map(x => x.CharName, "Report_Spec_Type_Char_Name");
            Map(x => x.CharUnit, "Report_Spec_Type_Char_Unit");
            Map(x => x.CharValue, "Report_Spec_Type_Char_Value");
            Map(x => x.CharFactValue, "Report_Spec_Type_Char_Fact_Value");
            Map(x => x.IsChange, "Report_Spec_Type_Is_Change");

            References<PlansSpec>(x => x.PlanSpec, "Plans_Spec_ID");
            References<SpecType>(x => x.SpecType, "Spec_Exp_Type_ID");
        }
    }
}