﻿using System;
using System.Collections.Generic;
using System.Web;

namespace bgTeam.IsRui.WebApp.Helpers
{
    public class CookieHelper
    {
        public static HttpCookie MakeCookie(string cookieName, Dictionary<string, string> values, DateTime expires)
        {
            HttpCookie cookie = new HttpCookie(cookieName);

            foreach (string key in values.Keys)
            {
                cookie[key] = values[key];
            }

            cookie.Expires = expires;
            return cookie;
        }

        public static void ClearCookie(string cookieName)
        {
            HttpResponse response = HttpContext.Current.Response;
            HttpCookie cookie = new HttpCookie(cookieName) { Expires = DateTime.Now.AddDays(-1) };
            response.Cookies.Set(cookie);
        }

        public static void SaveCookie(string cookieName, Dictionary<string, string> values, DateTime expires)
        {
            //HttpCookie cookie = new HttpCookie(cookieName);
            HttpResponse response = HttpContext.Current.Response;
            response.Cookies.Add(CookieHelper.MakeCookie(cookieName, values, expires));
        }
    }
}