﻿namespace bgTeam.IsRui.Domain.Entities.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Workflow;

    public class WorkflowUserConclusionHistory : EntityBase
    {
        public virtual int? Id_Ref { get; set; }

        public virtual int WorkflowId { get; set; }

        public virtual User User { get; set; }

        public virtual Status Status { get; set; }

        public virtual string Text { get; set; }

        public virtual DateTime TimeStamp { get; set; }

        /// <summary>
        /// Документы
        /// </summary>
        public virtual IList<Files> Documents { get; set; }
    }
}
