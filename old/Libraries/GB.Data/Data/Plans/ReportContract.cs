﻿using Uviri.Entity.Directories.Zakupki;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Контракты для отчета по плану раздел 2
    /// </summary>
    public class ReportContract: EntityBase
    {
        public ReportContract()
        {
        }

        public ReportContract(PlansStateContract pContract)
        {
            PlansStateContract = pContract;
            PlansActivity = pContract.PlanActivity;
        }

        public ReportContract(PlansActivity activity)
        {
            this.PlansActivity = activity;
        }

        /// <summary>Был изменён </summary>
        public virtual bool IsChange { get; set; }
        
        public virtual Contract Contract { get; set; }

        /// <summary></summary>
        public virtual PlansStateContract PlansStateContract { get; protected set; }

        /// <summary>Мероприятия плана</summary>
        public virtual PlansActivity PlansActivity { get; protected set; }
    }
}
