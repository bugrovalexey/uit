﻿namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class ClassificationСategoryMap : ClassMapHiLo<ClassificationCategory>
    {
        public ClassificationСategoryMap()
            : base("ClassificationСategory")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "CC_Name");

            References(x => x.Parent, "CC_Parent_ID").Nullable();

            HasMany(x => x.Childs).KeyColumns.Add("CC_Parent_ID").Inverse();
        }
    }
}
