﻿namespace Uviri.MKRF.Entities.Plans
{
    /// <summary> роли управления </summary>
    public class ManagementRole : EntityBase
    {   
        protected ManagementRole()
        {
        }
        public ManagementRole(ManagementStructure Structure)
        {
            this.Structure = Structure;
        }

        /// <summary> Роль управления <summary>
        public virtual string Role { get; set; }

        /// <summary> Управляющая роль </summary>
        public virtual string Managment { get; set; }

        /// <summary> Функции </summary>
        public virtual string Function { get; set; }
        
        /// <summary>Структура органа управления</summary>
        public virtual ManagementStructure Structure { get; protected set; }
    }
}
