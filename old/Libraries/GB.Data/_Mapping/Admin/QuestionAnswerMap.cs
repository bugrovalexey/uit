﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Mapping.Admin
{
    class QuestionAnswerMap : ClassMapBase<QuestionAnswer>
    {
        public QuestionAnswerMap()
            : base("Answers")
        {
        }


        protected override void InitMap()
        {
            Map(x => x.Name, "Answers_Name");
            Map(x => x.Num, "Answers_Num");
            Map(x => x.Type, "Answers_Type").CustomType(typeof(AnswerTypeEnum)).Not.Nullable();

            References<QuestionnaireQuestion>(x => x.QuestionnaireQuestion, "Questions_ID").Not.Nullable();
        }
    }
}
