﻿using System;

namespace Uviri.MKRF.Entities.Admin
{
    /// <summary>
    /// Сообщение лога
    /// </summary>
    public class LogMessage : EntityBase
    {
        public virtual DateTime Date 
        {
            get;
            set;
        }

        public virtual string Username
        {
            get;
            set;
        }

        public virtual string Identity
        {
            get;
            set;
        }

        public virtual string Thread
        {
            get;
            set;
        }

        public virtual string Level
        {
            get;
            set;
        }

        public virtual LogLevel LevelEx
        {
            get
            {
                try
                {
                    return (LogLevel)Enum.Parse(typeof(LogLevel), Level, true);
                }
                catch (Exception)
                {
                    return LogLevel.None;
                }
            }
        }

        public virtual string Message
        {
            get;
            set;
        }

        public virtual string Exception
        {
            get;
            set;
        }
    }

    public enum LogLevel
    {
        None = 0,
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5
    }
}
