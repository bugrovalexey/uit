﻿namespace GB.DataAccess.Command
{
    public class StatusChangeCommand : ICommand<StatusChangeContext>
    {
        ISessionFactoryGb factory;

        public StatusChangeCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(StatusChangeContext context)
        {
            using (var session = factory.Create())
            {
                session.Update(context.Object);
                context.funChangeAfter(context.Object);
                session.Commit();
            }
        }
    }
}
