﻿using System;
using System.Web.Security;
using Uviri.ESIA.Web;

namespace Uviri.MKRF.Web
{
    public partial class Logout : ESIALogoutPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(IsLogoutCompleted())
            {
                FormsAuthentication.SignOut();
                Response.Redirect(FormsAuthentication.LoginUrl);
            }
        }
    }
}