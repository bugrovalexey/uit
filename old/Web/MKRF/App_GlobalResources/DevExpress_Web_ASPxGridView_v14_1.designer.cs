//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "12.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class DevExpress_Web_ASPxGridView_v14_1 {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal DevExpress_Web_ASPxGridView_v14_1() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.DevExpress_Web_ASPxGridView_v14_1", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to [Свернуть].
        /// </summary>
        internal static string ASPxGridViewStringId_Alt_Collapse {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Alt_Collapse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Скрыть.
        /// </summary>
        internal static string ASPxGridViewStringId_Alt_DragAndDropHideColumnIcon {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Alt_DragAndDropHideColumnIcon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to [Раскрыть].
        /// </summary>
        internal static string ASPxGridViewStringId_Alt_Expand {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Alt_Expand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to [Условие].
        /// </summary>
        internal static string ASPxGridViewStringId_Alt_FilterRowButton {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Alt_FilterRowButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to [Фильтр].
        /// </summary>
        internal static string ASPxGridViewStringId_Alt_HeaderFilterButton {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Alt_HeaderFilterButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to [Отфильтровано].
        /// </summary>
        internal static string ASPxGridViewStringId_Alt_HeaderFilterButtonActive {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Alt_HeaderFilterButtonActive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to По возрастанию.
        /// </summary>
        internal static string ASPxGridViewStringId_Alt_SortedAscending {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Alt_SortedAscending", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to По убыванию.
        /// </summary>
        internal static string ASPxGridViewStringId_Alt_SortedDescending {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Alt_SortedDescending", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Начинается с.
        /// </summary>
        internal static string ASPxGridViewStringId_AutoFilterBeginsWith {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.AutoFilterBeginsWith", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Содержит.
        /// </summary>
        internal static string ASPxGridViewStringId_AutoFilterContains {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.AutoFilterContains", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Заканчивается на.
        /// </summary>
        internal static string ASPxGridViewStringId_AutoFilterEndsWith {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.AutoFilterEndsWith", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Равно.
        /// </summary>
        internal static string ASPxGridViewStringId_AutoFilterEquals {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.AutoFilterEquals", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Больше чем.
        /// </summary>
        internal static string ASPxGridViewStringId_AutoFilterGreater {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.AutoFilterGreater", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Больше чем или равно.
        /// </summary>
        internal static string ASPxGridViewStringId_AutoFilterGreaterOrEqual {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.AutoFilterGreaterOrEqual", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Меньше чем.
        /// </summary>
        internal static string ASPxGridViewStringId_AutoFilterLess {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.AutoFilterLess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Меньше чем или равно.
        /// </summary>
        internal static string ASPxGridViewStringId_AutoFilterLessOrEqual {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.AutoFilterLessOrEqual", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Не равно.
        /// </summary>
        internal static string ASPxGridViewStringId_AutoFilterNotEqual {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.AutoFilterNotEqual", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Отмечено.
        /// </summary>
        internal static string ASPxGridViewStringId_Checked {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Checked", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Отмена.
        /// </summary>
        internal static string ASPxGridViewStringId_CommandCancel {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.CommandCancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Очистить.
        /// </summary>
        internal static string ASPxGridViewStringId_CommandClearFilter {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.CommandClearFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Удалить.
        /// </summary>
        internal static string ASPxGridViewStringId_CommandDelete {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.CommandDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Изменить.
        /// </summary>
        internal static string ASPxGridViewStringId_CommandEdit {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.CommandEdit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Новый.
        /// </summary>
        internal static string ASPxGridViewStringId_CommandNew {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.CommandNew", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выбрать.
        /// </summary>
        internal static string ASPxGridViewStringId_CommandSelect {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.CommandSelect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сохранить.
        /// </summary>
        internal static string ASPxGridViewStringId_CommandUpdate {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.CommandUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Подтверждаете удаление?.
        /// </summary>
        internal static string ASPxGridViewStringId_ConfirmDelete {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.ConfirmDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выбор колонок.
        /// </summary>
        internal static string ASPxGridViewStringId_CustomizationWindowCaption {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.CustomizationWindowCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Нет данных для отображения.
        /// </summary>
        internal static string ASPxGridViewStringId_EmptyDataRow {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.EmptyDataRow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Перетяните сюда колонку....
        /// </summary>
        internal static string ASPxGridViewStringId_EmptyHeaders {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.EmptyHeaders", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to (Продолжено на следующей странице).
        /// </summary>
        internal static string ASPxGridViewStringId_GroupContinuedOnNextPage {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.GroupContinuedOnNextPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Перетяните заголовок колонки, чтобы сгруппировать.
        /// </summary>
        internal static string ASPxGridViewStringId_GroupPanel {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.GroupPanel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to (Все).
        /// </summary>
        internal static string ASPxGridViewStringId_HeaderFilterShowAllItem {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.HeaderFilterShowAllItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Позднее следущего месяца.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_BeyondNextMonth {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_BeyondNextMonth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ранее этого месяца.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_EarlierThisMonth {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_EarlierThisMonth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Последний месяц.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_LastMonth {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_LastMonth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Последняя неделя.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_LastWeek {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_LastWeek", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Позднее этого месяца.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_LaterThisMonth {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_LaterThisMonth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Следующий месяц.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_NextMonth {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_NextMonth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Следующая неделя.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_NextWeek {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_NextWeek", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Старше.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_Older {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_Older", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Три недели назад.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_ThreeWeeksAgo {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_ThreeWeeksAgo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Через Три недели.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_ThreeWeeksAway {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_ThreeWeeksAway", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сегодня.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_Today {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_Today", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Завтра.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_Tomorrow {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_Tomorrow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Две недели назад.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_TwoWeeksAgo {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_TwoWeeksAgo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Через Две недели.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_TwoWeeksAway {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_TwoWeeksAway", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вчера.
        /// </summary>
        internal static string ASPxGridViewStringId_Outlook_Yesterday {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Outlook_Yesterday", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Форма редактирования.
        /// </summary>
        internal static string ASPxGridViewStringId_PopupEditFormCaption {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.PopupEditFormCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Не отмечено.
        /// </summary>
        internal static string ASPxGridViewStringId_Unchecked {
            get {
                return ResourceManager.GetString("ASPxGridViewStringId.Unchecked", resourceCulture);
            }
        }
    }
}
