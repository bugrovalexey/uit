﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using GB;
using GB.Helpers;
using DevExpress.Web.ASPxCallback;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxClasses.Internal;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxHiddenField;
using DevExpress.Web.ASPxUploadControl;

namespace GB.Controls
{
    public class FileUploadValidator : ASPxEdit
    {
        public string ClientOwner
        {
            get
            {
                return ((FileUploadValidatorProperties)this.Properties).ClientOwner;
            }

            set
            {
                ((FileUploadValidatorProperties)this.Properties).ClientOwner = value;
            }
        }

        protected override void CreateControlHierarchy()
        {
            base.CreateControlHierarchy();
            this.Native = true;
            FileUploadValidatorControl ctrl = new FileUploadValidatorControl(this);
            base.Controls.Add(ctrl);
        }

        protected override void GetCreateClientObjectScript(StringBuilder stb, string localVarName, string clientName)
        {
            base.GetCreateClientObjectScript(stb, localVarName, clientName);
            stb.Append(localVarName + ".FindInputElement = function(){return this.GetMainElement();}; \n");
            stb.Append(localVarName + ".test = function(){alert('test');}; \n");
        }


        protected override EditPropertiesBase CreateProperties()
        {
            return new FileUploadValidatorProperties();
        }
    }

    public class FileUploadValidatorControl : ASPxInternalWebControl
    {
        private ASPxEdit edit;
        private WebControl inputControl;
        protected ASPxEdit Edit
        {
            get
            {
                return this.edit;
            }
        }
        protected WebControl InputControl
        {
            get
            {
                return this.inputControl;
            }
        }
        public FileUploadValidatorControl(ASPxEdit edit)
        {
            this.edit = edit;
        }
        protected override void ClearControlFields()
        {
            this.inputControl = null;
        }
        protected override void CreateControlHierarchy()
        {
            this.inputControl = RenderUtils.CreateWebControl(HtmlTextWriterTag.Input);
            this.Controls.Add(this.InputControl);
        }
        protected override void PrepareControlHierarchy()
        {
            RenderUtils.AssignAttributes(this.Edit, this.InputControl);
            this.Edit.GetControlStyle().AssignToControl(this.InputControl);
            this.Edit.GetPaddings().AssignToControl(this.InputControl);
            this.InputControl.Height = this.Edit.Height;
            this.InputControl.Width = this.Edit.Width;
            RenderUtils.SetStringAttribute(this.InputControl, "type", "hidden");
            if (!string.IsNullOrEmpty(this.Edit.UniqueID))
            {
                RenderUtils.SetStringAttribute(this.InputControl, "name", this.Edit.UniqueID);
            }
        }
    }

    public class FileUploadValidatorProperties : EditProperties
    {
        public string ClientOwner { get; set; }

        protected override ASPxEditBase CreateEditInstance()
        {
            return new FileUploadValidator();
        }

        protected override EditClientSideEventsBase CreateClientSideEvents()
        {
            EditClientSideEventsBase cse = base.CreateClientSideEvents();
            if (!string.IsNullOrEmpty(this.ClientOwner))
            {
                cse.SetEventHandler("Validation", string.Format("function(s,e){{ e.isValid = {0}.Validate(); }}", this.ClientOwner));
            }

            return cse;
        }
    }

    public class UploadedFileInfo
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string FileName { get; set; }
    }

    public class FileUploadControl2 : CompositeControl
    {
        protected HtmlAnchor link;
        protected HtmlGenericControl blocklink;
        protected HtmlGenericControl container;
        protected ASPxHiddenField hf;
        protected ASPxUploadControl uploader;
        protected FileUploadValidator validator;

        /// <summary>Допустимые типы файлов</summary>
        [TypeConverter(typeof(StringListConverter))]
        public virtual string[] AllowedFileExtensions { get; set; } //AllowedFileExtensions=".jpg,.jpeg,.gif,.png" 

        /// <summary>Шаблон строки с сылкой на файл и кнопкой "удалить". 
        /// Можно использовать {file} и {delete} для подстановки шаблонов FileItemTemplate и DeleteFileButtonTemplate</summary>
        public string FileRowTemplate { get; set; }

        /// <summary>Шаблон ссылки на файл. Можно использовать {id} и {name} для подстановки ид и имени файла</summary>
        public string FileItemTemplate { get; set; }

        /// <summary>Шаблон строки файла. Можно использовать {command} для подстановки JS метода</summary>
        public string DeleteFileButtonTemplate { get; set; }

        /// <summary>Javascript-код, выполняющийся на клиентское событие FileUploadComplete</summary>
        public string ClientFileUploadComplete { get; set; }

        /// <summary>Только просмотр файлов</summary>
        public bool ReadOnly;

        /// <summary>Необходимо указать хотябы один файл</summary>
        public bool IsRequired { get; set; }

        /// <summary>Необходимо указать хотябы один файл</summary>
        public bool AutoUpload { get; set; }

        /// <summary>ValidationGroup</summary>
        public string ValidationGroup
        {
            get
            {
                return validator.ValidationSettings.ValidationGroup;
            }
            set
            {
                validator.ValidationSettings.ValidationGroup = value;
            }
        }

        /// <summary>Множественная загрузка</summary>
        public FileUploadMode Mode { get; set; }

        protected string ClientInstanceNameInternal = null;
        public string ClientInstanceName
        {
            get
            {
                if (ClientInstanceNameInternal == null)
                {
                    ClientInstanceNameInternal = this.ClientID + "_object";
                }
                return ClientInstanceNameInternal;
            }
            set
            {
                ClientInstanceNameInternal = value;
            }
        }

        public FileUploadControl2()
        {
            link = new HtmlAnchor();
            blocklink = new HtmlGenericControl();
            container = new HtmlGenericControl("div");
            hf = new ASPxHiddenField();
            uploader = new ASPxUploadControl();
            validator = new FileUploadValidator();
            uploader.FileUploadComplete += uploader_FileUploadComplete;
        }

        /// <summary>Список файлов</summary>
        public IList<UploadedFileInfo> GetValue()
        {

            List<UploadedFileInfo> infoList = new List<UploadedFileInfo>();
            int id = 0;
            Guid guid;
            string key;
            foreach (var item in hf)
            {
                key = item.Key.ToLower();

                // TODO: Данный код есть в новом FileHandler - ParseFileUrl
                int endOfId = 0;
                while (endOfId < key.Length)
                {
                    if ("-0123456789abcdefgh".IndexOf(key[endOfId]) >= 0)
                        endOfId++;
                    else
                        break;
                }

                if (endOfId > 0)
                {
                    key = key.Substring(0, endOfId);
                }

                UploadedFileInfo info = new UploadedFileInfo();
                info.FileName = (item.Value as string);


                if (int.TryParse(key, out id))
                {
                    info.Id = id;
                }
                else if (Guid.TryParse(key, out guid))
                {
                    info.Guid = key;
                }
                else
                {
                    continue;
                }

                infoList.Add(info);
            }
            return infoList;
        }

        protected override void CreateChildControls()
        {
            hf.ID = "hf";
            Controls.Add(hf);

            container.ID = "container";
            container.Style.Add("clear", "both");
            Controls.Add(container);

            if (!ReadOnly)
            {
                validator.ID = "validator";
                blocklink.ID = "blocklink";
                uploader.ID = "uploader";

                uploader.ShowProgressPanel = true;
                uploader.BrowseButton.Text = "Обзор...";
                uploader.Style.Add("float", "left");
                if (AutoUpload)
                    uploader.ClientSideEvents.TextChanged = string.Format("function(s, e) {{   {0}.ShowUploadButton(false); {0}.Upload();  }}", this.ClientInstanceName);
                else
                    uploader.ClientSideEvents.TextChanged = string.Format("function(s, e) {{   {0}.ShowUploadButton(s.GetText());  }}", this.ClientInstanceName);
                uploader.ClientSideEvents.FileUploadStart = string.Format("function(s, e) {{   {0}.ShowUploadButton(false);  }}", this.ClientInstanceName);
                uploader.ClientSideEvents.FileUploadComplete = string.Format("function(s, e) {{ if (e.isValid) {{ {0}.AddFileContainer(e.callbackData); {1}}} }}", this.ClientInstanceName, ClientFileUploadComplete ?? string.Empty);
                uploader.ValidationSettings.AllowedFileExtensions = AllowedFileExtensions;

                link.Attributes.Add("class", "command fileupload_sep");
                link.Attributes.Add("onclick", string.Format("{0}.Upload()", this.ClientInstanceName));
                link.InnerText = "Загрузить";

                blocklink.Controls.Add(link);
                blocklink.Style.Add("margin-top", "4px");
                blocklink.Style.Add("display", "inline-block");
                blocklink.Attributes.Add("class", "fileupload_none");

                //validator.Display = ValidatorDisplay.Dynamic;
                validator.ValidationSettings.ErrorText = "Данное поле не может быть пустым";
                //validator.ClientValidationFunction = string.Format("{0}.ValidateFile()", this.objectName);
                validator.CssClass = "dxeErrorCell_Aqua";

                Controls.Add(uploader);
                if (IsRequired)
                {
                    validator.ClientOwner = this.ClientInstanceName;
                    Controls.Add(validator);

                }
                Controls.Add(blocklink);
            }
        }

        public override void RenderEndTag(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write(string.Format(@"
<script {0} type='text/javascript'>
    var objName = '{1}';
    var obj = new Uploader('{1}', {2}, '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}');
    window[objName] = obj;
    obj.UpdateContainer();
</script>",

Page.IsCallback ? " id='dxss_FileUploadStartup'" : "",

this.ClientInstanceName,

this.hf.ClientID,
this.uploader.ClientID,
this.validator.ClientID,
this.Mode.ToString(),
this.container.ClientID,
this.blocklink.ClientID,
FileRowTemplate,
FileItemTemplate,
DeleteFileButtonTemplate
          ));

            base.RenderEndTag(writer);
        }

        protected void uploader_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            try
            {
                FileUploadEventArgs ef = new FileUploadEventArgs();

                ef.Id = Guid.NewGuid();
                ef.FileName = FileHelper.TranslateFileName(e.UploadedFile.FileName);
                ef.ContentType = e.UploadedFile.ContentType;
                ef.FilePath = FileHelper.GetPhysicalPath(ef.Id);
                ef.FileContent = e.UploadedFile.FileContent;

                Logger.Info(ef.FileName);
                Logger.Info(ef.ContentType);
                Logger.Info(ef.Id.ToString());
                Logger.Info(ef.FilePath);

                if (SaveFile != null)
                {
                    SaveFile(this, ef);
                }
                else
                {
                    using (Stream file = File.OpenWrite(ef.FilePath))
                    {
                        CopyStream(e.UploadedFile.FileContent, file);
                    }
                }

                e.CallbackData = string.Format("{0}/{1};{1}", ef.Id, ef.FileName);

                if (FileUploadComplete != null)
                {
                    FileUploadComplete(sender, ef);
                }
            }
            catch (Exception exp)
            {
                Logger.Fatal(exp);
                throw exp;
            }
        }

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }

        public void SetValue(string fileName, string filePath)
        {
            hf.Add(filePath, fileName);
        }

        /// <summary>
        /// Пользователь загрузил файл
        /// </summary>
        public event FileUpload FileUploadComplete;

        /// <summary>
        /// Сохранение файла
        /// </summary>
        public event FileUpload SaveFile;

        /// <summary>
        /// Пользователь загрузил файл
        /// </summary>
        public static Dictionary<string, string> ConvertToDictionary(string RawFileList)
        {
            Dictionary<string, string> fileDict = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(RawFileList))
            {
                string[] fileItems = RawFileList.Split(';');
                foreach (string item in fileItems)
                {
                    string[] pair = item.Split(':');
                    if (pair.Length < 2)
                        continue;

                    string id = pair[0].Trim();
                    if (fileDict.ContainsKey(id))
                        continue;

                    fileDict.Add(id, pair[1].Trim());
                }
            }
            return fileDict;
        }


    }
}