﻿namespace bgTeam.IsRui.Common.Extentions
{
    using System;
    using System.IO;
    using bgTeam.Extensions;

    public static class StringExtensions
    {
        public static string GetExtension(this string fileName)
        {
            return Path.GetExtension(fileName).Return(ext => ext.Replace(".", string.Empty), default);
        }

        public static string ReturnThisOrDefault(this string str, string @default)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return @default;
            }

            return str;
        }
    }
}
