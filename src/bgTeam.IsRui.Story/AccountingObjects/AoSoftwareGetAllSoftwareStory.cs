﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoSoftwareGetAllSoftwareStory : IStory<AoSoftwareGetAllSoftwareStoryContext, IEnumerable<AllSoftwareDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;

        public AoSoftwareGetAllSoftwareStory(IMapperBase mapper, IRepositoryRui repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public IEnumerable<AllSoftwareDto> Execute(AoSoftwareGetAllSoftwareStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<AllSoftwareDto>> ExecuteAsync(AoSoftwareGetAllSoftwareStoryContext context)
        {
            var software = _repository.GetAllEx<Software>(
                q => q.Where(x => x.AccountingObject.Id == context.AoId))
                .Select(x => _mapper.Map(x, new AllSoftwareDto()));

            return software;
        }
    }
}
