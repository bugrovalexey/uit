﻿
namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.MKRF.Logic.Access;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class AccountingObjectListStory : IStory<AccountingObjectListStoryContext, IEnumerable<AccountingObjectDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IManagerAccess _managerAccess;
        private readonly IRepositoryRui _repositoryRui;
        private readonly IUserRepository _userRepository;

        public AccountingObjectListStory(
            IMapperBase mapper,
            IManagerAccess managerAccess,
            IRepositoryRui repositoryRui,
            IUserRepository userRepository)
        {
            _mapper = mapper;
            _managerAccess = managerAccess;
            _repositoryRui = repositoryRui;
            _userRepository = userRepository;
        }

        public IEnumerable<AccountingObjectDto> Execute(AccountingObjectListStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<AccountingObjectDto>> ExecuteAsync(AccountingObjectListStoryContext context)
        {
            return _repositoryRui.GetAllEx<AccountingObject>(q =>
            {
                var departament = _userRepository.GetCurrent().Department;

                q.Fetch(x => x.Kind).Eager
                    .Fetch(x => x.Type).Eager
                    .Fetch(x => x.Favorites).Eager
                    .Fetch(x => x.Status).Eager
                    .OrderBy(x => x.CreateDate).Desc
                    .Where(x => x.Department == departament);
            }).Select(ConvertObject);
        }

        private AccountingObjectDto ConvertObject(AccountingObject obj)
        {
            var result = _mapper.Map(obj, new AccountingObjectDto());

            result.CanDelete = _managerAccess.CanDelete(obj);

            /*var currentActivities = _repositoryRui.GetAllEx<AoActivity>(query => query
               .Where(a => a.AccountingObject == obj));*/

            var finance = 0M;

            if (obj.FinancingQuarter1 != null)
            {
                finance = finance + obj.FinancingQuarter1.Value;
            }

            if (obj.FinancingQuarter2 != null)
            {
                finance = finance + obj.FinancingQuarter2.Value;
            }

            if (obj.FinancingQuarter3 != null)
            {
                finance = finance + obj.FinancingQuarter3.Value;
            }

            if (obj.FinancingQuarter4 != null)
            {
                finance = finance + obj.FinancingQuarter4.Value;
            }

            result.CostValue = finance;//obj.FinancingQuarter1.Value + obj.FinancingQuarter2.Value + obj.FinancingQuarter3.Value + obj.FinancingQuarter4.Value;   //currentActivities.Sum(a => a.CostY0);
            result.Cost = result.CostValue.ToString("N2");      //result.CostValue.ToString("N2");

            var childs = _repositoryRui.GetAllEx<AccountingObject>(q => q.Where(a => a.Parent == obj));

            if (obj.Department == _userRepository.GetCurrent().Department)
            {
                if (childs.Count == 1)
                {
                    result.Description = childs[0].Department.Name;
                }

                if (childs.Count > 1)
                {
                    result.Description = "Консолидированное";
                }
            }
            else
            {
                result.Description = obj.Department.Name;

                if (childs.Count == 1)
                {
                    result.Description = childs[0].Department.Name;
                }
            }

            return result;
        }
    }
}
