﻿namespace Uviri.MKRF.Entities.Admin
{
	/// <summary>Доп. право пользователя</summary>
	public class UsersRight : EntityBase
	{
		/// <summary>Пользователь</summary>
		public virtual User User { get; set; }

		/// <summary>Право</summary>
		public virtual Right Right { get; set; }
	}
}
