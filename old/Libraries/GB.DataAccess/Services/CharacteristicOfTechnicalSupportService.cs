﻿using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.WorksAndGoods;
using GB.DataAccess.Services.Args;
using System.Collections;

namespace GB.DataAccess.Services
{
    public class CharacteristicOfTechnicalSupportService : ServiceBase<CharacteristicOfTechnicalSupport>, IService<CharacteristicOfTechnicalSupport, CharacteristicOfTechnicalSupportArgs>
    {
        public CharacteristicOfTechnicalSupport Create(CharacteristicOfTechnicalSupportArgs args)
        {
            var entity = new CharacteristicOfTechnicalSupport();
            return SaveOrUpdate(entity, args);
        }

        public CharacteristicOfTechnicalSupport Update(CharacteristicOfTechnicalSupportArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private CharacteristicOfTechnicalSupport SaveOrUpdate(CharacteristicOfTechnicalSupport entity, CharacteristicOfTechnicalSupportArgs args)
        {
            ConvertDtoToEntity.FillCharacteristicOfTechnicalSupportFromArgs(entity, args);
            SaveOrUpdate(entity);
            return entity;
        }

        public void SetTechnicalSupportForCharacteristics(ICollection characteristicsIds,
            TechnicalSupport technicalSupport)
        {
            var characteristics = Repository.GetAllEx(q => q.WhereRestrictionOn(x => x.Id).IsIn(characteristicsIds));

            foreach (var item in characteristics)
            {
                item.TechnicalSupport = technicalSupport; 
                SaveOrUpdate(item);
            }
        }

        public void Delete(CharacteristicOfTechnicalSupportArgs args)
        {
            Delete(args.Id);
        }
    }
}