﻿using System;
using System.Collections.Generic;

namespace Uviri.MKRF.Entities.Admin
{
    /// <summary>
    /// Данные описывающие анкету
    /// </summary>
    public class Questionnaire : EntityBase
    {
        public Questionnaire()
        {
            UserList = new List<UserQuestionnaire>();
            QuestionsList = new List<QuestionnaireQuestion>();
        }
        /// <summary>
        /// Название анкеты
        /// </summary>
        public virtual string Caption { get; set; }
        /// <summary>
        /// Дата создания
        /// </summary>
        public virtual DateTime DateCreation { get; set; }
        /// <summary>
        /// Опубликовать?
        /// </summary>
        public virtual bool IsPublic { get; set; }
        /// <summary>
        /// Список пользователей
        /// </summary>
        public virtual IList<UserQuestionnaire> UserList { get; protected set; }
        /// <summary>
        /// Список вопросов
        /// </summary>
        public virtual IList<QuestionnaireQuestion> QuestionsList { get; protected set; }
    }

    public class UserQuestionnaire : EntityBase
    {
        /// <summary>
        /// Идентификатор анкеты
        /// </summary>
        public virtual Questionnaire Questionnaire { get; set; }
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public virtual User User { get; set; }
    }
}
