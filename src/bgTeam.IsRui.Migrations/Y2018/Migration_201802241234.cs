﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201802241234)]
    public class Migration_201802241234 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 3, TableKey = "OU_Contract" });
        }
    }
}
