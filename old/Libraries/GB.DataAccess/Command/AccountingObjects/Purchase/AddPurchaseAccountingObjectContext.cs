﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.Purchase
{
    public class AddPurchaseAccountingObjectContext : ICommandContext
    {
        public int IdAO { get; set; }
        public int IdZakupki360 { get; set; } 
        public string Number { get; set; } 
        public int Type { get; set; } 
        public DateTime? Date { get; set; }  
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Summ { get; set; } 
    }
}
