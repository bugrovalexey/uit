﻿using System;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Ожидаемые результаты мероприятия
    /// </summary>
    public class ExpectedResult : EntityBase
    {
        protected ExpectedResult()
        { 
        }

        public ExpectedResult(PlansActivity PlansActivity)
        {
            this.PlansActivity = PlansActivity;
        }

        /// <summary>Мероприятия плана</summary>
        public virtual PlansActivity PlansActivity { get; protected set; }

        /// <summary> Год </summary>
        public virtual int Year { get; set; }

        /// <summary>Описание</summary>
        public virtual String Description { get; set; }

        /// <summary>Значение</summary>
        public virtual String Value { get; set; }
    }
}
