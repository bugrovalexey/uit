﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ServicesCreateOrUpdateServiceStory : IStory<ServicesCreateOrUpdateServiceStoryContext, ActivitiesServiceDto>
    {
        private readonly IMapperBase _mapper;
        private readonly ISessionNHibernateFactory _sessionNHibernateFactory;
        private readonly IRepositoryRui _repositoryRui;

        public ServicesCreateOrUpdateServiceStory(IMapperBase mapper, ISessionNHibernateFactory factory, IRepositoryRui repository)
        {
            _mapper = mapper;
            _sessionNHibernateFactory = factory;
            _repositoryRui = repository;
        }

        public ActivitiesServiceDto Execute(ServicesCreateOrUpdateServiceStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivitiesServiceDto> ExecuteAsync(ServicesCreateOrUpdateServiceStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            using (var session = _sessionNHibernateFactory.OpenSession())
            {
                PlansActivityService activitiesService;

                if (context.Id.HasValue)
                {
                    activitiesService = _repositoryRui.Get<PlansActivityService>(context.Id);
                }
                else
                {
                    var activity = _repositoryRui.Get<PlansActivity>(context.ActivityId);

                    activitiesService = new PlansActivityService(activity);
                }

                _mapper.Map(context, activitiesService);

                activitiesService.KOSGU = _repositoryRui.Get<ExpenditureItem>(context.KOSGUId);

                await session.SaveOrUpdateAsync(activitiesService);

                return _mapper.Map(activitiesService, new ActivitiesServiceDto());
            }
        }
    }
}
