﻿namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AccountingObjectActs : EntityBase
    {
        public virtual string Name { get; set; }

        public virtual string Number { get; set; }

        public virtual DateTime? DateAdoption { get; set; }

        public virtual int Cost { get; set; }

        // public virtual ActsFileDto File { get; set; }

        public virtual int? FileId { get; set; }

        public virtual string FileName { get; set; }

        public virtual AccountingObject AccountingObject { get; set; }

        public virtual DateTime? ChangeDate { get; set; }
    }
}
