﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/v2012new/ActivityCard.master" AutoEventWireup="true" CodeBehind="InitialActivity.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.InitialActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="detailTopContent" runat="server">
    <script type="text/javascript">
        function hfSave(e) {
            hf.Set('Num', NumTextBox.GetText());
            hf.Set('Name', NameTextBox.GetText());
            hf.Set('WorkForm', WorkFormComboBox.GetValue());
            hf.Set('ExpenditureItems', ExpenditureItemsComboBox.GetValue());
            hf.Set('Comment', CommentTextBox.GetText());
            hf.Set('Summ', SummTextBox.GetText());
        }
    </script>

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="center" runat="server">
    <p class="header_grid" style="">
        Список исходных мероприятий
        <span class="subtitle">Заполняется на третьем этапе</span>
    </p>

    <dxe:ASPxButton ID="ActivityAddButton" runat="server" 
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) { ActivityGrid.AddNewRow(); }" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="ActivityGrid" Width="100%" ClientInstanceName="ActivityGrid"
        KeyFieldName="Id"
        OnBeforePerformDataSelect="ActivityGrid_BeforePerformDataSelect"
        OnRowInserting="ActivityGrid_RowInserting"
        OnRowUpdating="ActivityGrid_RowUpdating"
        OnStartRowEditing="ActivityGrid_StartRowEditing"
        OnRowDeleting="ActivityGrid_RowDeleting"
        OnSummaryDisplayText="ActivityGrid_SummaryDisplayText">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn FieldName="RegNum" Caption="Рег. №" />
            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Наименование мероприятия" />
            <dxwgv:GridViewDataTextColumn FieldName="WorkForm.Code" Caption="БК ВР" />
            <dxwgv:GridViewDataTextColumn FieldName="ExpenditureItem.Code" Caption="БК КОСГУ" />
            <dxwgv:GridViewDataTextColumn FieldName="Comment" Caption="Примечание" />
            <dxwgv:GridViewDataTextColumn FieldName="Summ" Caption="Сумма, тыс. руб." >
                <PropertiesTextEdit DisplayFormatString="#,##0.00000" />
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Settings ShowFooter="True" />
        <SettingsBehavior ConfirmDelete="True" />
        <Styles>
            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
        </Styles>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Рег. №
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="NumTextBox" ClientInstanceName="NumTextBox"
                                OnDataBinding="NumTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Наименование мероприятия
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo ID="NameTextBox" runat="server" ClientInstanceName="NameTextBox"
                                OnDataBinding="NameTextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">БК ВР
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="WorkFormComboBox" runat="server" ClientInstanceName="WorkFormComboBox" 
                                OnDataBinding="WorkFormComboBox_DataBinding" TextField="CodeName" ValueField="Id" ValueType="System.Int32" EncodeHtml="false" DropDownHeight="300px">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Код по БК КОСГУ
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ExpenditureItemsComboBox" runat="server" ClientInstanceName="ExpenditureItemsComboBox" 
                                OnDataBinding="ExpenditureItemsComboBox_DataBinding" TextField="CodeName" ValueField="Id" ValueType="System.Int32" EncodeHtml="false" DropDownHeight="300px">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Примечание
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="CommentTextBox" ClientInstanceName="CommentTextBox"
                                OnDataBinding="CommentTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Сумма
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="SummTextBox" ClientInstanceName="SummTextBox" MaskSettings-Mask="<0..999999999g>.<00000..99999>"
                                OnDataBinding="SummTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <TotalSummary>
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00000}" FieldName="Summ" SummaryType="Sum" />
        </TotalSummary>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') hfSave(e); }" />
    </dxwgv:ASPxGridView>
</asp:Content>
