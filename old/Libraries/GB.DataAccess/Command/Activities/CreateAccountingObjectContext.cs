﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.DataAccess.Services;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Command.Activities
{
    public class CreateAccountingObjectContext : ICommandContext, IPlansActivityArgs, IAccountingObjectControllerArgs
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ResponsibleDepartment { get; set; }

        public int? ResponsibleInformation { get; set; }

        public int? ResponsibleTechnical { get; set; }

        public int? ResponsibleFinancial { get; set; }

        public int? Signing { get; set; }

        public int? AccountingObject { get; set; }

        public string Logo { get; set; }

        public string Files { get; set; }

        public int AO { get; set; }

        public string AOName { get; set; }

        public string AOShortName { get; set; }

        public int AOType { get; set; }

        public int AOKind { get; set; }

        public int? Order { get; set; }

        public DateTime? OrderDate { get; set; }

        public string Prerequisites { get; set; }

        public string Goal { get; set; }

        public string Task { get; set; }

        public string Conception { get; set; }

        /// <summary>
        /// Если мероприятие находится в стадии Create, есть возможность
        //  менять поля ОУ
        /// </summary>
        public bool IsEditAO { get; set; }

        #region IAccountingObjectControllerArgs

        int IAccountingObjectControllerArgs.Id
        {
            get { return AO; }
            set { AO = value; }
        }

        int IAccountingObjectControllerArgs.Type
        {
            get { return AOType; }
            set { AOType = value; }
        }

        int IAccountingObjectControllerArgs.Kind
        {
            get { return AOKind; }
            set { AOKind = value; }
        }

        string IAccountingObjectControllerArgs.ShortName
        {
            get { return AOShortName; }
            set { AOShortName = value; }
        }

        string IAccountingObjectControllerArgs.FullName
        {
            get { return AOName; }
            set { AOName = value; }
        }

        #endregion IAccountingObjectControllerArgs

    }
}
