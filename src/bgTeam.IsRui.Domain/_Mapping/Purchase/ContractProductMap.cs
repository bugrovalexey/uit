﻿using bgTeam.IsRui.Domain.Entities.Purchase;

namespace bgTeam.IsRui.Domain._Mapping.Purchase
{
    /// <summary>
    /// Предмет контракта
    /// </summary>
    internal class ContractProductMap : ClassMapHiLo<ContractProduct>
    {
        public ContractProductMap()
            : base("Contract_Product") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "Contract_Product_Name");

            Map(x => x.OKDP_Code, "Contract_Product_OKDP_Code");
            Map(x => x.OKDP_Name, "Contract_Product_OKDP_Name");
            Map(x => x.OKEI_Code, "Contract_Product_OKEI_Code");
            Map(x => x.OKEI_Name, "Contract_Product_OKEI_Name");

            Map(x => x.Quantity, "Contract_Product_Quantity");
            Map(x => x.Price, "Contract_Product_Price").Not.Nullable();
            Map(x => x.Sum, "Contract_Product_Sum");

            References<Contract>(x => x.Contract, "Contract_ID").Not.Nullable();
        }
    }
}