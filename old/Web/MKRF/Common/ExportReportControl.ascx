﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExportReportControl.ascx.cs"
    Inherits="GB.MKRF.Web.Common.ExportReportControl" %>
<div>
    <table>
        <tr>
            <td>
                <dxe:ASPxLabel runat="server" ID="DepartmentChooseLabel" ClientInstanceName="DepartmentChooseLabel"
                    Text="Экспортировать выделенные: " Width="200px" Font-Bold="True">
                </dxe:ASPxLabel>
            </td>
            <%--<td>
                <dxe:ASPxComboBox ID="TypeReportComboBox" runat="server" Width="145px">
                </dxe:ASPxComboBox>
            </td>--%>
            <td>
                <dxe:ASPxComboBox ID="FormatReportComboBox" runat="server" Width="70px">
                </dxe:ASPxComboBox>
            </td>
            <td>
                <dxe:ASPxButton runat="server" ID="ExportButton" OnClick="ExportButton_Click" Text="OK">
                </dxe:ASPxButton>
            </td>
        </tr>
    </table>
</div>
