﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System.Threading.Tasks;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;

    public class AoWorkAndServiceDeleteWorkStory : IStory<AoWorkAndServiceDeleteWorkStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;

        public AoWorkAndServiceDeleteWorkStory(ISessionNHibernateFactory factory)
        {
            _factory = factory;
        }

        public bool Execute(AoWorkAndServiceDeleteWorkStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(AoWorkAndServiceDeleteWorkStoryContext context)
        {
            bool result = false;

            using (var session = _factory.OpenSession())
            {
                try
                {
                    await session.DeleteAsync<WorkAndService>(context.Id);

                    result = true;
                }
                catch
                {
                    await session.RollbackAsync();

                    throw;
                }
            }

                return result;
        }
    }
}
