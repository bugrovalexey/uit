﻿using System;
using System.ComponentModel;
using System.Linq;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    //[MetadataType(typeof(ActivityCreate_Valid))]
    public class ActivityCreateViewModel : StepBaseViewModel
    {
        #region consts

        public const string NameSmallCaption = "Наименование";
        public const string TypeCaption = "Тип мероприятия";
        public const string YearCaption = "Год реализации";
        public const string VolY0Caption = "Очередной финансовый год, руб.";
        public const string VolY1Caption = "Первый год планового периода, руб.";
        public const string VolY2Caption = "Второй год планового периода, руб.";

        #endregion

        public int? xId { get; set; }

        [DisplayName(NameSmallCaption)]
        public string Name { get; set; }

        [DisplayName(TypeCaption)]
        public string Type { get; set; }

        [DisplayName(YearCaption)]
        public int Year { get; set; }

        [DisplayName(VolY0Caption)]
        public object VolY0 { get; set; }

        [DisplayName(VolY1Caption)]
        public object VolY1 { get; set; }

        [DisplayName(VolY2Caption)]
        public object VolY2 { get; set; }

        public System.Web.Mvc.SelectList StageList { get; set; }
        public System.Web.Mvc.SelectList YearList { get; set; }

        public ActivityCreateViewModel(int id)
            : base(id, MasterItemEnum.ActivityCreate)
        {
            var activity = new RepositoryDc<AoActivity>().GetAllEx(query => query
                .Where(x => x.AccountingObject == Current)
                .Where(x => x.Stage == Current.Stage)).FirstOrDefault();

            Type = Current.Stage.ToActivityTypeEnum().First().GetDescription();
            Year = DateTime.Now.Year;

            if(activity != null)
            {
                xId = activity.Id;
                Name = activity.Name;
                Type = activity.ActivityType.GetDescription();
                Year = activity.Year;
                VolY0 = activity.CostY0;
                VolY1 = activity.CostY1;
                VolY2 = activity.CostY2;
            }

            StageList = new System.Web.Mvc.SelectList(EnumHelper.ToDictionary<AccountingObjectsStageEnum>(), "Key", "Value");
            YearList = new System.Web.Mvc.SelectList(new YearRepository().GetAll(), "Id", "Value");
        }
    }

    //public class ActivityCreate_Valid : ValidateViewModel
    //{
    //    [Required(ErrorMessage = RequiredText)]
    //    public string Name { get; set; }

    //    //[Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
    //    ////[Required(ErrorMessage = RequiredText)]
    //    //public int Type { get; set; }
    //}
}