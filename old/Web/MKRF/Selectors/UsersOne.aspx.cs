﻿using System;
using System.Collections.Generic;
using System.Linq;
using GB.Extentions;
using GB.Helpers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxGridView;

namespace GB.MKRF.Web.Selectors
{
    public partial class UsersOne : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            UsersGrid.DataBind();

            base.OnPreRender(e);
        }

        protected void UsersGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            int departmentId;
            string depFilter = Request.QueryString["department"];
            if (depFilter == "all") // все департаменты
            {
                departmentId = 0;
            }
            else
            {
                if (!int.TryParse(depFilter, out departmentId)) // фильтр по указанному департаменту
                    departmentId = UserRepository.GetCurrent().Department.Id; // поведение по-умолчанию: фильтр по департаменту текущему пользователя
            }

            IList<User> list = RepositoryBase<User>.GetAll(
                new GetAllArgs<User>()
                {
                    Expression = (x => x.IsActive == true
                        && x.Role.Id != 0 // Из всех полей, ссылающихся на справочник пользователей, надо убрать админа
                        && (departmentId == 0 || x.Department.Id == departmentId)),
                    OrderByAsc = (x => x.Surname),
                    Fetch = new System.Linq.Expressions.Expression<Func<User, object>>[] 
                    { 
                        x => x.Role, 
                        x => x.Department 
                    }
                });

            (sender as ASPxGridView).DataSource = list
                .Select(user => new
                {
                    user.Id,
                    Name = user.FullName,
                    Role = user.Role.Name,
                    Department = user.Department.Name, //string.Concat(user.Department.Code, " - ", user.Department.Name),
                    Job = user.Job,
                    Email = user.Email,
                    Phone = string.IsNullOrWhiteSpace(user.WorkPhone) ? user.MobPhone : user.WorkPhone
                }).ToList();
        }

        protected void UsersGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "$(this).addClass(\"grid_mouseover\")");
            e.Row.Attributes.Add("onmouseout", "$(this).removeClass(\"grid_mouseover\")");
        }
    }
}