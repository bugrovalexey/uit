﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain.Entities.Plans
{
    /// <summary>Тип работы</summary>
    public class WorkType : EntityBase
    {
        public WorkType()
        {
        }

        public WorkType(PlansWork work)
        {
            PlanWork = work;
        }

        ///// <summary>ID параметра</summary>
        //public virtual int ParID { get; set; }//null
        // TODO: Сделать Childs

        /// <summary>Код</summary>
        public virtual string Code { get; set; }//null

        ///// <summary>Наименование</summary>
        //public virtual string Name { get; set; }//null

        /// <summary>
        /// Работа плана
        /// </summary>
        public virtual PlansWork PlanWork { get; set; }

        /// <summary>
        /// Тип расходов
        /// </summary>
        public virtual ExpenseType ExpenseType { get; set; }

        /// <summary>
        /// а очередной финансовый год
        /// </summary>
        public virtual double ExpenseVolumeYear0 { get; set; }

        /// <summary>
        /// на первый год планового периода
        /// </summary>
        public virtual double ExpenseVolumeYear1 { get; set; }

        /// <summary>
        /// на второй год планового периода
        /// </summary>
        public virtual double ExpenseVolumeYear2 { get; set; }
    }
}