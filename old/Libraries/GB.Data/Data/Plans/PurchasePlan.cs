﻿using System.Collections.Generic;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// План закупок
    /// </summary>
    public class PurchasePlan : NumerableEntity
    {
        public PurchasePlan() { }

        public PurchasePlan(Plan plan)
        {
            Plan = plan;
        }

        /// <summary>
        /// Наименование работ в плане закупок
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Статус работ
        /// </summary>
        public virtual string State { get; set; }

        /// <summary>
        /// Планируемый объем финансирования по плану закупок
        /// </summary>
        public virtual double AmountFunding { get; set; }

        /// <summary>
        /// План
        /// </summary>
        public virtual Plan Plan { get; set; }

        public virtual IList<PurchasePlanActivity> PurchasePlanActivity { get; set; }
    }
}
