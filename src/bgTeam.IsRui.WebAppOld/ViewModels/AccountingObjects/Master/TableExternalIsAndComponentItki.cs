﻿using System.ComponentModel;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class TableExternalIsAndComponentItki : NotificationBaseViewModel
    {
        #region Consts

        public const string NameCaption = "Наименование";
        public const string PurposeCaption = "Назначение";
        public const string SmevCaption = "СМЭВ";
        public const string UrlCaption = "URL адрес";

        #endregion Consts

        #region Ctors

        public TableExternalIsAndComponentItki(ExternalIsAndComponentsItki entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            Purpose = entity.Purpose;
            Smev = entity.Smev;
            Url = entity.Url.Return(url => LinkBuilder.Link(url, url), default(string));
        }

        #endregion Ctors

        #region Props

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(PurposeCaption)]
        public string Purpose { get; set; }

        [DisplayName(SmevCaption)]
        public string Smev { get; set; }

        [DisplayName(UrlCaption)]
        public string Url { get; set; }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteExternalIsAndComponentItki", "AccountingObjects", new { id = Id }, "GridExternalIsAndComponentsItki");
            }
        }

        #endregion Props
    }
}