﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Workflow;
using bgTeam.IsRui.WebApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    [MetadataType(typeof(CommonDataViewModel_Valid))]
    public class CommonDataViewModel : AccountingObjectBase
    {
        #region consts

        private const string StageCaption = "Стадия ОУ";
        private const string StatusCaption = "Текущий статус ОУ";
        private const string NumberCaption = "Уникальный идентификационный номер ОУ";
        private const string NameCaption = "Полное наименование ОУ";
        private const string TargetsCaption = "Цели создания ОУ";
        private const string PurposeAndScopeCaption = "Назначение и область применения объекта учета";
        private const string TotalCostCaption = "Общая стоимость ОУ, руб";
        private const string TotalCostCreateCaption = "Общая стоимость создания ОУ, руб";
        private const string TotalCostGrowthCaption = "Общая стоимость развития ОУ, руб";
        private const string TotalCostExploitationCaption = "Общая стоимость эксплуатации ОУ, руб";
        private const string AdoptionBudgetDateCaption = "Дата принятия к бюджетному учету";
        private const string ExpenditureItemCaption = "Код по БК КОСГУ";
        private const string BalanceCostCaption = "Балансовая стоимость, руб";
        private const string AdoptionBudgetBasisCaption = "Основания принятия к бюджетному учету";
        public const string DocNameCaption = "Наименование документа";
        public const string DocNumberCaption = "Номер документа";
        public const string DocDateAdoptionCaption = "Дата принятия документа";
        public const string DocParagraphCaption = "Пункт, статья документа";
        private const string CommissioningDateCaption = "Введен в эксплуатацию";
        public const string FilesDataCaption = "Документ";
        private const string DecommissioningDateCaption = "Выведен из эксплуатации";

        #endregion consts

        #region ctor

        public CommonDataViewModel(IStatusRepository statusRepository, int id)
            : base(id, x => x.Documents)
        {
            Documents = Current.Documents.Select(doc => new TableAODecisionToCreate(doc));

            StageList = new SelectList(
               EnumHelper.ToDictionary<AccountingObjectsStageEnum>(),
                "Key", "Value");

            StausList = new SelectList(
                statusRepository.GetAllEx(q => q.Where(st => st.StatusGroup.Id == (int)StatusGroupEnum.AccountingObject)), "Id", "Name");

            KOSGUList = new SelectList(
                DirectoriesRepositary.GetDirectories<ExpenditureItem>(DateTime.Now.Year).OrderBy(x => x.Code),
                "Id", "CodeName");

            CommissioningFilesData =
                Current.DocumentCommissioning.With(dc => dc.Documents).Return(
                    docs => docs.Select(x => new SelectEntityInfo() { Id = x.Id, Name = x.Name }), new List<SelectEntityInfo>());

            DecommissioningFilesData =
                Current.DocumentDecommissioning.With(dc => dc.Documents).Return(
                    docs => docs.Select(x => new SelectEntityInfo() { Id = x.Id, Name = x.Name }), new List<SelectEntityInfo>());

            AOTypeAndKindViewModel = new AOTypeAndKindViewModel(id, ReadOnly);
        }

        #endregion ctor

        public IEnumerable<TableAODecisionToCreate> Documents { get; set; }

        public SelectList StageList { get; set; }

        public SelectList StausList { get; set; }

        public SelectList KOSGUList { get; set; }

        [DisplayName(StageCaption)]
        public AccountingObjectsStageEnum Stage
        {
            get { return Current.Stage; }
        }

        [DisplayName(StatusCaption)]
        public int? StatusId
        {
            get { return Current.Status.Return(st => st.Id, default(int)); }
        }

        public AOTypeAndKindViewModel AOTypeAndKindViewModel { get; set; }

        [DisplayName(NameCaption)]
        public string Name
        {
            get { return Current.FullName; }
        }

        [DisplayName(NumberCaption)]
        public string Number
        {
            get { return Current.Number; }
        }

        [DisplayName(TargetsCaption)]
        public string Targets
        {
            get { return Current.Targets; }
        }

        [DisplayName(PurposeAndScopeCaption)]
        public string PurposeAndScope
        {
            get { return Current.PurposeAndScope; }
        }

        //TODO: Вычислять из вкладки Закупки (Будет в другой задаче)
        [DisplayName(TotalCostCaption)]
        public decimal? TotalCost
        {
            get { return null; }
        }

        //TODO: Вычислять из вкладки Закупки (Будет в другой задаче)
        [DisplayName(TotalCostCreateCaption)]
        public decimal? TotalCostCreate
        {
            get { return null; }
        }

        //TODO: Вычислять из вкладки Закупки (Будет в другой задаче)
        [DisplayName(TotalCostGrowthCaption)]
        public decimal? TotalCostGrowth 
        {
            get { return null; }
        }

        //TODO: Вычислять из вкладки Закупки (Будет в другой задаче)
        [DisplayName(TotalCostExploitationCaption)]
        public decimal? TotalCostExploitation
        {
            get { return null; }
        }

        [DisplayName(AdoptionBudgetDateCaption)]
        public string AdoptionBudgetDate
        {
            get { return Current.AdoptionBudgetDate.ToStringOrDefault(); }
        }

        [DisplayName(ExpenditureItemCaption)]
        public int? ExpenditureItemId
        {
            get { return Current.BK_KOSGU.Return(ex => ex.Id, default(int)); }
        }

        [DisplayName(BalanceCostCaption)]
        public decimal BalanceCost
        {
            get { return Current.BalanceCost; }
        }

        [DisplayName(AdoptionBudgetBasisCaption)]
        public string AdoptionBudgetBasis
        {
            get { return Current.AdoptionBudgetBasis; }
        }

        [DisplayName(DocNameCaption)]
        public string CommissioningDocName
        {
            get { return Current.DocumentCommissioning.Return(dc => dc.Name, default(string)); }
        }

        [DisplayName(DocNumberCaption)]
        public string CommissioningDocNumber
        {
            get { return Current.DocumentCommissioning.Return(dc => dc.Number, default(string)); }
        }

        [DisplayName(DocDateAdoptionCaption)]
        public string CommissioningDocDateAdoption
        {
            get { return Current.DocumentCommissioning.Return(dc => dc.DateAdoption.ToStringOrDefault(), default(string)); }
        }

        [DisplayName(CommissioningDateCaption)]
        public string CommissioningDate
        {
            get { return Current.CommissioningDate.ToStringOrDefault(); }
        }

        [DisplayName(FilesDataCaption)]
        public IEnumerable<IEntityName> CommissioningFilesData { get; set; }

        [DisplayName(DocNameCaption)]
        public string DecommissioningDocName
        {
            get { return Current.DocumentDecommissioning.Return(dc => dc.Name, default(string)); }
        }

        [DisplayName(DocNumberCaption)]
        public string DecommissioningDocNumber
        {
            get { return Current.DocumentDecommissioning.Return(dc => dc.Number, default(string)); }
        }

        [DisplayName(DocDateAdoptionCaption)]
        public string DecommissioningDocDateAdoption
        {
            get { return Current.DocumentDecommissioning.Return(dc => dc.DateAdoption.ToStringOrDefault(), default(string)); }
        }

        [DisplayName(DecommissioningDateCaption)]
        public string DecommissioningDate
        {
            get { return Current.DecommissioningDate.ToStringOrDefault(); }
        }

        [DisplayName(FilesDataCaption)]
        public IEnumerable<IEntityName> DecommissioningFilesData { get; set; }
    }

    public class CommonDataViewModel_Valid : ValidateViewModel
    {
        [Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        [Required(ErrorMessage = RequiredText)]
        public int? StatusId { get; set; }

        [RegularExpression(pattern: @"(^\d{2}\.\d{7}$)|(^__\._______$)", ErrorMessage = NotMatchMask)]
        public string Number { get; set; }
    }
}