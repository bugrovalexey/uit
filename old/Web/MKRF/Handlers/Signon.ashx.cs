﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Uviri.ESIA.Web;
using Uviri.Helpers;
using Uviri.MKRF.Repository;

namespace Uviri.MKRF.Web.Handlers
{
    public class Signon : IHttpHandler, IRequiresSessionState
    {
        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; } 
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string urlForSuccess = string.Empty;
                string error = string.Empty;
                var esiaUser = ESIAControlsBase.GetESIAUser(context.Request, ref urlForSuccess, ref error);
                
                if(!string.IsNullOrEmpty(error))
                {
                    context.Response.Redirect(FormsAuthentication.LoginUrl + "?error=" + error);
                    return;
                }
                
                var rep = new UserRepository();
                var user = rep.GetUserBySNILS(esiaUser.Snils);
                
                if(user == null)
                {
                    ResponseHelper.IncorrectUser(context.Response);
                    return;
                }
                
                user.IsESIAUser = true;
                UserRepository.SaveOrUpdate(user);

                if (string.IsNullOrEmpty(urlForSuccess))
                {
                    urlForSuccess = "~/Default.aspx";
                }

                // Уведомление FormsAuthentication об успешной аутентификации.
                FormsAuthentication.SetAuthCookie(user.Login, false);
                // Переход на URL, который изначально хотел открыть пользователь.                
                context.Response.Redirect(urlForSuccess, false);
            }
            catch (Exception ex)
            {
                string message = "Ошибка при обработке результата аутентификации.";
                throw new Exception(message, ex);
            }
        }

        #endregion
    }
}
