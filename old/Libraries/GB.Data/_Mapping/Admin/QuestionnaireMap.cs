﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Mapping.Admin
{
    public class QuestionnaireMap : ClassMapBase<Questionnaire>
    {
        public QuestionnaireMap()
            : base("Quests")
        {
//            Table("w_");

//            Id(x => x.Id, "Quests_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string argsList =
//                       @"@Quests_Is_Publish = ?
//                        ,@Quests_Caption = ?
//                        ,@Quests_ID = ?";
            
//            SqlInsert(@"exec ZZZ_Prc_Quests_Insert " + argsList).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Quests_Update " + argsList).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Quests_Delete @Quests_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.IsPublic, "Quests_Is_Publish");
            Map(x => x.Caption, "Quests_Caption");
            Map(x => x.DateCreation, "Quests_Create_DT").ReadOnly();

            HasMany<UserQuestionnaire>(x => x.UserList).KeyColumns.Add("Quests_ID").Cascade.AllDeleteOrphan().Inverse();
            HasMany<QuestionnaireQuestion>(x => x.QuestionsList).KeyColumns.Add("Quests_ID").Cascade.AllDeleteOrphan().Inverse();
        }
    }

    public class UserListMap : ClassMapBase<UserQuestionnaire>
    {
        public UserListMap()
            : base("Quest_User_List")
        {
//            Table("w_Quest_User_List");

//            Id(x => x.Id, "Quest_User_List_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string argsList = 
//                        @"@Users_ID = ?
//                         ,@Quests_ID = ?
//                         ,@Quest_User_List_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Quest_User_List_Insert " + argsList).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Quest_User_List_Update " + argsList).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Quest_User_List_Delete @Quest_User_List_ID = ?").Check.None();
        }


        protected override void InitMap()
        {
            References<User>(x => x.User, "Users_ID").Not.Nullable();
            References<Questionnaire>(x => x.Questionnaire, "Quests_ID").Not.Nullable();
        }
    }
}
