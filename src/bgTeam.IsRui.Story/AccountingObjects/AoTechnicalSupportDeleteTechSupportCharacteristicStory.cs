﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoTechnicalSupportDeleteTechSupportCharacteristicStory : IStory<AoTechnicalSupportDeleteTechSupportCharacteristicStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;

        public AoTechnicalSupportDeleteTechSupportCharacteristicStory(ISessionNHibernateFactory factory)
        {
            _factory = factory;
        }

        public bool Execute(AoTechnicalSupportDeleteTechSupportCharacteristicStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(AoTechnicalSupportDeleteTechSupportCharacteristicStoryContext context)
        {
            bool result = false;

            using (var session = _factory.OpenSession())
            {
                try
                {
                    await session.DeleteAsync<CharacteristicOfTechnicalSupport>(context.Id);

                    result = true;
                }
                catch (Exception)
                {
                    await session.RollbackAsync();

                    throw;
                }
            }

                return result;
        }
    }
}
