﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class IKTTypeMap : ClassMapBase<IKTType>
    {
        public IKTTypeMap()
            : base("IKT_Type")
        {
            //Table("w_");

            //Id(x => x.Id, "IKT_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

            ///// <summary>Код для сортировки</summary>
            //public string OrderIndex { get; set; }

            ///// <summary>Признак актуальности</summary>
            //public string IsActual { get; set; }
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "IKT_Type_Name");
            Map(x => x.ExternalKey, "IKT_Type_External_Key");
        }
    }
}