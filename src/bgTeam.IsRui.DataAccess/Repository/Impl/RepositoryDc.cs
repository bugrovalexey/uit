﻿namespace bgTeam.IsRui.DataAccess.Repository
{
    using System;
    using System.Collections.Generic;
    using bgTeam.IsRui.DataAccess.Impl;
    using bgTeam.IsRui.Domain.Entities;
    using NHibernate;

    /// <summary>
    /// Общий репозиторий
    /// </summary>
    /// <typeparam name="T">Тип объекта</typeparam>
    public class RepositoryDc<T> : RepositorySt, IRepositoryDc<T>
        where T : EntityBase
    {
        public RepositoryDc()
            : base(new SessionNHibernate(null))
        {
        }

        public RepositoryDc(ISessionNHibernateFactory factory)
            : base(factory.OpenSession())
        {
        }

        /// <summary>
        /// Возвращает объект по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Объект</returns>
        public T Get(object id)
        {
            if (id == null)
                return default(T);
            else if (id is double)
                id = Convert.ToInt32(id);
            else if (id is Enum)
                id = (int)id;
            else if (id is string)
            {
                int value = 0;
                if (!int.TryParse(id.ToString(), out value))
                    return default(T);
                id = value;
            }

            return Session.Get<T>(id);
        }

        public T Get(Action<IQueryOver<T, T>> query)
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.SingleOrDefault();
        }

        /// <summary>
        /// Возвращает существующую запись
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Запись, либо ArgumentException, если ее неудалось найти</returns>
        public T GetExisting(object id)
        {
            var entity = Get(id);

            if (entity == null)
            {
                throw new ArgumentException(String.Format("Объект <{0}> не найден в системе", id));
            }

            return entity;
        }

        public T GetDefault()
        {
            return Session.Get<T>(EntityBase.Default);
        }

        public T GetOrDefault(object id)
        {
            T ob = Get(id);

            if (ob == null)
                return GetDefault();

            return ob;
        }

        #region GetAll

        /// <summary>
        /// Возвращает все объекты
        /// </summary>
        /// <returns>Список объектов</returns>
        public IList<T> GetAll()
        {
            return Session.CreateCriteria(typeof(T)).List<T>();
        }

        /// <summary>
        /// Возвращает все объекты
        /// </summary>
        /// <returns>Список объектов</returns>
        public IList<T> GetAllEx(Action<IQueryOver<T, T>> query)
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.List<T>();
        }

        /// <summary>
        /// Можно вытаскивать типы отличные от Root типа
        /// (конкретное поле, несколько полей, другой объект, используя TransformUsing)
        /// </summary>
        /// <typeparam name="TReturn">Возвращаемый тип</typeparam>
        /// <param name="query">Запрос</param>
        /// <returns>Список объектов возращаемых запросом</returns>
        public IList<TReturn> Select<TReturn>(Action<IQueryOver<T, T>> query)
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.List<TReturn>();
        }

        /// <summary>
        /// Можно вытаскивать типы отличные от Root типа
        /// (конкретное поле, другой объект, используя TransformUsing)
        /// </summary>
        /// <typeparam name="TReturn">Возвращаемый тип</typeparam>
        /// <param name="query">Запрос</param>
        /// <returns>Объект возращаемый запросом</returns>
        public TReturn SelectSingle<TReturn>(Action<IQueryOver<T, T>> query)
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.SingleOrDefault<TReturn>();
        }

        /// <summary>
        /// Возвращает количество строк по запросу
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public int RowCount(Action<IQueryOver<T, T>> query)
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.RowCount();
        }

        public void Refresh(object obj)
        {
            Session.Refresh(obj);
        }
        #endregion GetAll
    }
}
