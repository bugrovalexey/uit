﻿using GB.Entity.Directories.Zakupki;

namespace GB.MKRF.Entities.Plans
{
    /// <summary> Заключенные государственные контракты </summary>
    public class PlansStateContract : EntityBase
    {
        protected PlansStateContract()
        {
        }

        public PlansStateContract(PlansActivity PlansActivity)
        {
            this.PlanActivity = PlansActivity;
        }

        /// <summary>Описание ранее достигнутых результато</summary>
        public virtual string DescriptionPreviousResult { get; set; }

        /// <summary> Год </summary>
        public virtual int Year { get; set; }

        /// <summary> год, предшествующий текущему финансовому году </summary>
        public virtual double ImplementationIndicatorY0 { get; set; }

        /// <summary>отчетный финансовый год </summary>
        public virtual double ImplementationIndicatorY1 { get; set; }

        /// <summary> текущий финансовый год </summary>
        public virtual double ImplementationIndicatorY2 { get; set; }

        /// <summary> Госконтракт </summary>
        public virtual Contract Contract { get; set; }

        /// <summary>Мероприятие Плана</summary>
        public virtual PlansActivity PlanActivity { get; protected set; }

        /// <summary> Цель </summary>
        //public virtual PlansActGT PlansActGT { get; set; }

        /// <summary>Целевой показатель</summary>
        public virtual PlansGoalIndicator PlansGoalIndicator { get; set; }
    }
}