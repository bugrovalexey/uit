﻿namespace bgTeam.IsRui.WebApp
{
    using bgTeam.Core;
    using bgTeam.Core.Impl;
    using bgTeam.Infrastructure;
    using bgTeam.Infrastructure.Logger;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.Common.Services.Impl;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Impl;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.MKRF.Logic.Status;
    using bgTeam.IsRui.Story;
    using bgTeam.IsRui.Story.AccessObjects;
    using bgTeam.IsRui.Story.AccessObjects.Impl;
    using bgTeam.IsRui.Story.StoryMapping;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;
    using System.Reflection;

    public static class DIContainer
    {
        public static void Configure(IServiceCollection services)
        {
            services.Scan(scan => scan
                    .FromAssemblyOf<IStoryLibrary>()
                    .AddClasses(classes => classes.AssignableTo(typeof(IStory<,>)))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime());

            services.Scan(scan => scan
                    .FromAssemblies(Assembly.GetExecutingAssembly())
                    .AddClasses(classes => classes.AssignableTo<Controller>())
                    .AsImplementedInterfaces()
                    .WithTransientLifetime());

            services.Scan(scan => scan
                    .FromAssemblyOf<IAppSettings>()
                    .AddClasses(classes => classes.AssignableTo(typeof(IAppSettings)))
                    .AddClasses(classes => classes.AssignableTo(typeof(IFileProviderRuiSetting)))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime());

            var storyfactory = new StoryFactory(services);
            var cacheManager = new CacheManager(new System.TimeSpan(0, 20, 0));

            services.AddSingleton<IAppConfiguration, AppConfigurationDefault>()
                .AddSingleton<IAppLoggerConfig, AppLoggerSerilogConfig>()
                .AddSingleton<IAppLogger, AppLoggerSerilog>()
                .AddSingleton<ICacheManager>(cacheManager)
                .AddSingleton<IMapperBase, AutoMapperMap>()
                .AddSingleton<IStoryFactory>(storyfactory)
                .AddSingleton<IStoryBuilder, StoryBuilder>()
                .AddSingleton<IStoryAccess, StoryAccessEntity>()
                .AddSingleton<ISessionNHibernateConfiguration, SessionNHibernateConfigurationMsSql>()
                .AddScoped<ISessionNHibernateFactory, SessionNHibernateFactory>()
                .AddSingleton<MKRF.Logic.Access.IManagerAccess, MKRF.Logic.Access.Impl.ManagerAccess>()
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                .AddScoped<IRepositoryRui, Repository>()
                .AddScoped<IUserRepository, UserRepository>()
                .AddScoped<IStatusRepository, StatusRepository>()
                .AddSingleton<IAccessEditService, AccessEditService>()
                .AddSingleton<IFileProviderRui, FileProviderRui>()
                .AddSingleton<IConvertHtmlToXls, ConvertHtmlToXls>()
                .AddSingleton<IConvertHtmlToPdf, ConvertHtmlToPdf>();
        }
    }
}
