﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="EditCard.Master" CodeBehind="InformationInteraction.aspx.cs" Inherits="GB.MKRF.Web.AccountingObjects.InformationInteraction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function GridSave(e) {
            hf.Set('Name', txtName.GetValue());
            hf.Set('Purpose', txtPurpose.GetValue());
            hf.Set('TypeId', cbxType.GetValue());
            hf.Set('SMEV', txtSMEV.GetValue());
            hf.Set('Title', txtTitle.GetValue());
            hf.Set('URL', txtUrl.GetValue());
        }

        function validateUrl(s, e) {
            var pattern = new RegExp(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/);

            e.isValid = e.value == null || pattern.test(e.value);
        }

    </script>

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />

    <p class="header_grid">
        Информационное взаимодействие
   
   
   
   
   
    </p>

    <dxe:ASPxButton ID="bntAddInformationInteraction" runat="server" ClientInstanceName="bntAddInformationInteraction" CssClass="fleft topButton"
        AutoPostBack="false" Text="Добавить">
        <ClientSideEvents Click="function(s,e) {gridInformationInteraction.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="gridInformationInteraction" Width="100%" ClientInstanceName="gridInformationInteraction"
        KeyFieldName="Id" ViewStateMode="Disabled" AutoGenerateColumns="False"
        OnBeforePerformDataSelect="gridInformationInteraction_OnBeforePerformDataSelect"
        OnRowInserting="gridInformationInteraction_OnRowInserting"
        OnRowUpdating="gridInformationInteraction_OnRowUpdating"
        OnStartRowEditing="gridInformationInteraction_OnStartRowEditing"
        OnRowDeleting="gridInformationInteraction_OnRowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" />
            <dxwgv:GridViewDataTextColumn Caption="Назначение" FieldName="Purpose" />
            <dxwgv:GridViewDataTextColumn Caption="Тип" FieldName="Type.Name" />
            <dxwgv:GridViewDataTextColumn Caption="СМЭВ" FieldName="SMEV" />
            <dxwgv:GridViewDataTextColumn Caption="Заголовок" FieldName="Title" />
            <dxwgv:GridViewDataTextColumn Caption="URL">
                <DataItemTemplate>                
                    <a href='<%#Eval("Url")%>'><%#Eval("Url")%></a>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc required">Наименование:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtName" ClientInstanceName="txtName"
                                runat="server" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                OnDataBinding="txtName_OnDataBinding">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Назначение:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtPurpose" ClientInstanceName="txtPurpose"
                                runat="server"
                                OnDataBinding="txtPurpose_OnDataBinding">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Тип:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="cbxType" runat="server" ClientInstanceName="cbxType"
                                TextField="Name" ValueField="Id" ValueType="System.Int32" 
                                OnDataBinding="cbxType_OnDataBinding" EnableSynchronization="False">
                            </dxe:ASPxComboBox>

                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">СМЭВ:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtSMEV" ClientInstanceName="txtSMEV"
                                runat="server"
                                OnDataBinding="txtSMEV_OnDataBinding">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Заголовок:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtTitle" ClientInstanceName="txtTitle"
                                runat="server"
                                OnDataBinding="txtTitle_OnDataBinding">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">URL:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtUrl" ClientInstanceName="txtUrl"
                                runat="server" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                OnDataBinding="txtUrl_OnDataBinding">
                                <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData" ErrorText="Неверное значение" />
                                <ClientSideEvents Validation="validateUrl"></ClientSideEvents>
                            </dxe:ASPxTextBox>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents
            BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') {GridSave(e);} }" />
    </dxwgv:ASPxGridView>
</asp:Content>
