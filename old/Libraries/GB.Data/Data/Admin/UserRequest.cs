﻿using GB.MKRF.Entities.Directories;
using System;

namespace GB.MKRF.Entities.Admin
{
    /// <summary>Запрос на добавление пользователя</summary>
    public class UserRequest : EntityBase
    {
        /// <summary>Ведомство</summary>
        public virtual Department Department { get; set; }

        /// <summary>Автор заявки</summary>
        public virtual User Author { get; set; }

        /// <summary>Фамилия</summary>
        public virtual string Surname { get; set; }

        /// <summary>Имя</summary>
        public virtual string Name { get; set; }

        /// <summary>Отчество</summary>
        public virtual string Middlename { get; set; }

        /// <summary>Рабочий телефон</summary>
        public virtual string PhoneWork { get; set; }

        /// <summary>Мобильный телефон/Факс</summary>
        public virtual string PhoneMob { get; set; }

        /// <summary>Почтовый адрес</summary>
        public virtual string Email { get; set; }

        /// <summary>Должность</summary>
        public virtual string Job { get; set; }

        /// <summary>Комментарий</summary>
        public virtual string Comment { get; set; }

        /// <summary>Пользователь добавлен</summary>
        public virtual bool IsComplete { get; set; }

        /// <summary>Дата обработки заявки</summary>
        public virtual DateTime? CompletionDate { get; set; }

        /// <summary>Дата обработки заявки</summary>
        public virtual DateTime CreationDate { get; set; }
    }
}