﻿namespace GB.MKRF.Entities.Directories
{
    public class SourceFunding : EntityBase, IEntityCodeDirectories
    {
        /// <summary>Код</summary>
        public virtual string Code { get; set; }//null

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null

        public virtual string CodeName
        {
            get
            {
                return string.Format("{0} - {1}", Code, Name);
            }
        }
    }
}