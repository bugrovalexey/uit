﻿using System.Collections.Generic;

namespace GB.DataAccess.Services.Args
{
    public class AOGovServiceArgs : BaseArgsNew, IServiceArgs
    {
        public AOGovServiceArgs()
        {
            AOGovServicesNPAIds = new List<int>();
        }

        public int? AccountingObjectId { get; set; }

        public int? GovServiceId { get; set; }
        
        public int? PercentOfUseResource { get; set; }

        public List<int> AOGovServicesNPAIds { get; set; }

    }
}
