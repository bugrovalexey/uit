﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Story.AccountingObjects;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimCreateOrUpdateDocumentStory : IStory<ClaimCreateOrUpdateDocumentStoryContext, ClaimDocumentDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapperBase;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IFileProviderRui _fileProvider;

        public ClaimCreateOrUpdateDocumentStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase,
            ISessionNHibernateFactory sessionNHibernateFactory,
            IFileProviderRui fileProviderRui)
        {
            _repository = repositoryRui;
            _mapperBase = mapperBase;
            _factory = sessionNHibernateFactory;
            _fileProvider = fileProviderRui;
        }

        public ClaimDocumentDto Execute(ClaimCreateOrUpdateDocumentStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ClaimDocumentDto> ExecuteAsync(ClaimCreateOrUpdateDocumentStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка: {context.ClaimId}");
            }

            ClaimDocument document;
            IEnumerable<Files> files = Enumerable.Empty<Files>();

            if (context.Id.HasValue && context.Id.Value > 0)
            {
                document = _repository.Get<ClaimDocument>(context.Id.Value);
                files = _repository.GetAllEx<Files>(q =>
                    q.Where(x => x.EntityOwner == document && x.Type == DocTypeEnum.ClaimDocument));

                foreach (var f in files)
                {
                    if (context.File == null || f.Id != context.File.Id)
                    {
                        await new FileDeleteStory(_repository, _fileProvider, _factory)
                            .ExecuteAsync(new FileDeleteStoryContext
                            {
                                Id = f.Id
                            });
                    }
                }

                document = _mapperBase.Map(context, document);
            }
            else
            {
                document = _mapperBase.Map(context, new ClaimDocument());
            }

            document.File = null;

            using (var session = _factory.OpenSession())
            {
                await session.SaveOrUpdateAsync(document);
                await session.CommitAsync();
            }

            var result = _mapperBase.Map(document, new ClaimDocumentDto());

            if (!files.Any(x => x.Id == context.File.Id))
            {
                result.File = _mapperBase.Map(await SaveFile(context, document), new FilesDto());
            }

            return result;
        }

        private async Task<Files> SaveFile(ClaimCreateOrUpdateDocumentStoryContext context, ClaimDocument document)
        {
            if (context.File == null)
            {
                return null;
            }

            var addFile = new FileAddStory(_mapperBase, _repository, _fileProvider, _factory);

            if (context.File.Url == null)
            {
                var result = await addFile.ExecuteAsync(new FileAddStoryContext()
                {
                    Type = DocTypeEnum.ClaimDocument,
                    Entity = document,
                    Name = context.File.FileName,
                    TempGuid = context.File.TempGuid,
                    MimeType = context.File.MimeType,
                    Kind = context.File.Kind,
                    Size = context.File.Size,
                    Extension = context.File.Extension,
                });
                return result;
            }
            else
            {
                var result = await addFile.ExecuteAsync(new FileAddStoryContext()
                {
                    Type = DocTypeEnum.ClaimDocument,
                    Entity = document,
                    Name = context.File.FileName,
                    Url = context.File.Url,
                    MimeType = context.File.MimeType,
                    Kind = context.File.Kind,
                    Size = context.File.Size,
                    Extension = context.File.Extension,
                });
                return result;
            }
        }
    }
}
