﻿using GB;
using GB.Helpers;
using GB.MKRF.Entities.Plans;
using GB.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GB.MKRF.Web.Activities
{
    public class BasePagePlansActivity : System.Web.UI.Page
    {
        public int? ActivityId
        {
            get { return (Master as EditCard).ActivityId; }
        }


        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as EditCard).CurrentActivity;
            }
        }

        public bool EnableEdit
        {
            get
            {
                return true;
            }
        }
    }
}