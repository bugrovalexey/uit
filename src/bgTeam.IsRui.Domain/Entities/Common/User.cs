﻿using System.Collections.Generic;

namespace bgTeam.IsRui.Domain.Entities.Common
{
    /// <summary>Пользователь</summary>
    public class User : EntityBase
    {
        /// <summary>Признак активности</summary>
        public virtual bool IsActive { get; set; }

        /// <summary>Ведомство</summary>
        public virtual Department Department { get; set; }

        /// <summary>Логин</summary>
        public virtual string Login { get; set; }

        /// <summary>Пароль</summary>
        public virtual string Password { get; set; }

        /// <summary>Фамилия</summary>
        public virtual string Surname { get; set; }

        /// <summary>Имя</summary>
        public virtual string Name { get; set; }

        /// <summary>Отчество</summary>
        public virtual string Middlename { get; set; }

        /// <summary>Роли</summary>
        public virtual IList<Role> Roles { get; set; }

        /// <summary>Место работы</summary>
        public virtual string Work { get; set; }//null

        /// <summary>Образование</summary>
        public virtual string Education { get; set; }//null

        /// <summary>E-mail</summary>
        public virtual string Email { get; set; }//null

        /// <summary>Должность</summary>
        public virtual string Job { get; set; }//null

        /// <summary>Рабочий телефон</summary>
        public virtual string WorkPhone { get; set; }//null

        /// <summary>Мобильный телефон</summary>
        public virtual string MobPhone { get; set; }//null

        /// <summary>Факс</summary>
        public virtual string Fax { get; set; }//null

        /// <summary>Получать письма</summary>
        public virtual bool ReceiveMail { get; set; }

        /// <summary>Снилс</summary>
        public virtual string Snils { get; set; }

        /// <summary> Пользователь авторизовался через ЕСИА </summary>
        public virtual bool IsESIAUser { get; set; }

        /// <summary> Информация о токене пользователя
        /// Наличие данных говорит о необходимости проверить сертификат перед подписанием</summary>
        public virtual string TokenInfo { get; set; }

        /// <summary>Подключенные рассылки</summary>
        //public virtual IList<UserToNotification> NotificationList { get; set; }

        /// <summary>Дополнительные права</summary>
        //public virtual IList<UsersRight> RightList { get; set; }

        /// <summary>Дополнительные права</summary>
        //public virtual IList<UsersMsg> MessageList { get; set; }

        /// <summary>Ответственный от ОГВ</summary>
        public virtual bool IsResponsible { get; set; }

        /// <summary>JSON строка с настройками пользователя</summary>
        public virtual string Options { get; set; }

        /// <summary>
        /// Ф + И + О
        /// </summary>
        public virtual string FullName
        {
            get
            {
                return string.Concat(Surname, " ", Name, " ", Middlename).Trim();
            }
        }

        /// <summary>
        /// Ф + И + О - Роль
        /// </summary>
        //public virtual string FullNameRole
        //{
        //    get
        //    {
        //        return string.Concat(Surname, " ", Name, " ", Middlename, " - (", Role.Name, ")").Trim();
        //    }
        //}

        ///// <summary>
        ///// Роли
        ///// </summary>
        //public virtual IList<Role> Roles { get; set; }
    }
}