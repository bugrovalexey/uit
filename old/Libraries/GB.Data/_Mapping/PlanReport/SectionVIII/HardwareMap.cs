﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
    public class HardwareMap : ClassMapBase<Hardware>
    {
        public HardwareMap()
            : base("PRS_8_1")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_1_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_8_1_CA_1 = ? 
//                          ,@PRS_8_1_TP_1 = ? 
//                          ,@PRS_8_1_CA_2 = ? 
//                          ,@PRS_8_1_TP_2 = ? 
//                          ,@PRS_8_1_CA_3 = ? 
//                          ,@PRS_8_1_TP_3 = ? 
//                          ,@PRS_8_1_CA_4 = ? 
//                          ,@PRS_8_1_TP_4 = ? 
//                          ,@PRS_8_1_CA_5 = ? 
//                          ,@PRS_8_1_TP_5 = ? 
//                          ,@PRS_8_1_CA_6 = ? 
//                          ,@PRS_8_1_TP_6 = ? 
//                          ,@PRS_8_1_CA_7 = ? 
//                          ,@PRS_8_1_TP_7 = ? 
//                          ,@PRS_8_1_CA_8 = ? 
//                          ,@PRS_8_1_TP_8 = ? 
//                          ,@PRS_8_1_CA_9 = ? 
//                          ,@PRS_8_1_TP_9 = ? 
//                          ,@PRS_8_1_CA_10 = ? 
//                          ,@PRS_8_1_TP_10 = ? 
//                          ,@PRS_8_1_CA_11 = ? 
//                          ,@PRS_8_1_TP_11 = ? 
//                          ,@PRS_8_1_CA_12 = ? 
//                          ,@PRS_8_1_TP_12 = ? 
//                          ,@PRS_8_1_CA_13 = ? 
//                          ,@PRS_8_1_TP_13 = ? 
//                          ,@Plans_ID = ? 
//						  ,@PRS_8_1_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_1_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_1_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_1_Delete @PRS_8_1_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.DesktopCount_5_CA, "PRS_8_1_CA_1");
			Map(x => x.DesktopCount_5_TP, "PRS_8_1_TP_1");
			Map(x => x.DesktopCount_0_5_CA, "PRS_8_1_CA_2");
			Map(x => x.DesktopCount_0_5_TP, "PRS_8_1_TP_2");
			Map(x => x.LaptopCount_5_CA, "PRS_8_1_CA_3");
			Map(x => x.LaptopCount_5_TP, "PRS_8_1_TP_3");
			Map(x => x.LaptopCount_0_5_CA, "PRS_8_1_CA_4");
			Map(x => x.LaptopCount_0_5_TP, "PRS_8_1_TP_4");
			Map(x => x.TabletCount_CA, "PRS_8_1_CA_5");
			Map(x => x.TabletCount_TP, "PRS_8_1_TP_5");
			Map(x => x.ServerCount_CA, "PRS_8_1_CA_6");
			Map(x => x.ServerCount_TP, "PRS_8_1_TP_6");
			Map(x => x.Reserve_CA, "PRS_8_1_CA_7");
			Map(x => x.Reserve_TP, "PRS_8_1_TP_7");
			Map(x => x.ReserveVol_CA, "PRS_8_1_CA_8");
			Map(x => x.ReserveVol_TP, "PRS_8_1_TP_8");
			Map(x => x.MFUCount_CA, "PRS_8_1_CA_9");
			Map(x => x.MFUCount_TP, "PRS_8_1_TP_9");
			Map(x => x.XeroxCount_CA, "PRS_8_1_CA_10");
			Map(x => x.XeroxCount_TP, "PRS_8_1_TP_10");
			Map(x => x.LanPrinterCount_CA, "PRS_8_1_CA_11");
			Map(x => x.LanPrinterCount_TP, "PRS_8_1_TP_11");
			Map(x => x.PrinterCount_CA, "PRS_8_1_CA_12");
			Map(x => x.PrinterCount_TP, "PRS_8_1_TP_12");
			Map(x => x.ScanerCount_CA, "PRS_8_1_CA_13");
			Map(x => x.ScanerCount_TP, "PRS_8_1_TP_13");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
        }
    }
}