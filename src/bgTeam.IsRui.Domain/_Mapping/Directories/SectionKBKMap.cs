﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    internal class SectionKBKMap : ClassMapHiLo<SectionKBK>
    {
        public SectionKBKMap()
            : base("Section_KBK")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Section_KBK_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_Section_KBK_Insert
            //						  @Section_KBK_Code = ?
            //						 ,@Section_KBK_Name = ?
            //						 ,@Section_KBK_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Section_KBK_Update
            //						  @Section_KBK_Code = ?
            //						 ,@Section_KBK_Name = ?
            //						 ,@Section_KBK_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Section_KBK_Delete
            //						  @Section_KBK_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "Section_KBK_Code").Nullable();
            Map(x => x.Name, "Section_KBK_Name").Nullable();
            Map(x => x.DateStart, "Section_KBK_Start_Date");
            Map(x => x.DateEnd, "Section_KBK_End_Date");
        }
    }
}