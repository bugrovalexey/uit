﻿using GB.Data.Directories;
using GB.Data.Purchase;
using System;
using System.Collections.Generic;

namespace GB.Data.Plans
{
    /// <summary>Спецификация Плана</summary>
    public class PlansSpec : EntityNumerable
    {
        public PlansSpec()
        {
            SpecType = new List<SpecType>();
        }

        /// <summary>Мероприятие Плана</summary>
        public virtual PlansActivity PlanActivity { get; set; }

        /// <summary>Тип оборудования</summary>
        public virtual string EquipmentType { get; set; }

        /// <summary>Тип оборудования</summary>
        //public virtual Qualifier Qualifier { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null

        /// <summary>ОКПД</summary>
        public virtual OKPD OKPD { get; set; }//null

        /// <summary>Группа товаров</summary>
        public virtual ProductGroup ProductGroup { get; set; }

        /// <summary>Направление расходов</summary>
        public virtual ExpenseDirection ExpenseDirection { get; set; }

        /// <summary>Тип расходов</summary>
        public virtual ExpenseType ExpenseType { get; set; }

        /// <summary> Код по БК КОСГУ </summary>
        public virtual ExpenditureItem ExpenditureItem { get; set; }

        /// <summary>Год</summary>
        public virtual YearEnum Year { get; set; }

        /// <summary>Срок поставки (в днях)</summary>
        public virtual int? Duration { get; set; }

        /// <summary>Количество</summary>
        public virtual int? Quantity { get; set; }

        /// <summary>Цена за единицу в рублях</summary>
        public virtual decimal Cost { get; set; }

        /// <summary>Раздел/подраздел КБК</summary>
        public virtual SectionKBK SectionKBK { get; set; }

        /// <summary>Программа</summary>
        public virtual ExpenseItem ExpenseItem { get; set; }

        /// <summary>Форма работы</summary>
        public virtual WorkForm WorkForm { get; set; }

        /// <summary>ГРБС</summary>
        public virtual GRBS GRBS { get; set; }

        /// <summary>Номер, склеенный с номером мероприятия</summary>
        public virtual string ActivitySpecNum
        {
            get
            {
                string activityNum = PlanActivity.ActivityNum.Remove(2);
                string specNum = "";
                if (OrigNum < 10)
                    specNum = "0" + OrigNum;
                if (OrigNum >= 10)
                    specNum = OrigNum.ToString();
                return string.Format("{0}{1}", activityNum, specNum);
            }
        }

        /// <summary>Аналог</summary>
        public virtual string Analog { get; set; }//null

        /// <summary>Объем расходов из Федерального Бюджета</summary>
        public virtual double ExpenseVolumeFB { get; set; }

        /// <summary>Объем расходов из Регионального Бюджета</summary>
        public virtual double ExpenseVolumeRB { get; set; }

        /// <summary>Объем расходов из Внебюджетных источников</summary>
        public virtual double ExpenseVolumeVB { get; set; }

        /// <summary>Фактический Объем расходов из Федерального Бюджета</summary>
        public virtual double ExpenseVolumeFB_Fact { get; set; }

        /// <summary>Фактический Объем расходов из Регионального Бюджета</summary>
        public virtual double ExpenseVolumeRB_Fact { get; set; }

        /// <summary>Фактический Объем расходов из Внебюджетных источников</summary>
        public virtual double ExpenseVolumeVB_Fact { get; set; }

        /// <summary>Подпрограмма</summary>
        public virtual string SubEntry { get; set; }//null

        /// <summary>Комментарий</summary>
        public virtual string Comm { get; set; }//null

        /// <summary>Основной объем/Федеральный бюджет/Очередной год</summary>
        public virtual double ExpenseVolFBY0 { get; set; }

        /// <summary>Основной объем/Федеральный бюджет/Первый год</summary>
        public virtual double ExpenseVolFBY1 { get; set; }

        /// <summary>Основной объем/Федеральный бюджет/Второй год</summary>
        public virtual double ExpenseVolFBY2 { get; set; }

        /// <summary>Дополнительная потребность/Федеральный бюджет/Очередной год</summary>
        public virtual double ExpenseAddVolFBY0 { get; set; }

        /// <summary>Дополнительная потребность/Федеральный бюджет/Первый год</summary>
        public virtual double ExpenseAddVolFBY1 { get; set; }

        /// <summary>Дополнительная потребность/Федеральный бюджет/Второй год</summary>
        public virtual double ExpenseAddVolFBY2 { get; set; }

        /// <summary>Основной объем/Внебюджетный фонд/Очередной год</summary>
        public virtual double ExpenseVolVBY0 { get; set; }

        /// <summary>Основной объем/Внебюджетный фонд/Первый год</summary>
        public virtual double ExpenseVolVBY1 { get; set; }

        /// <summary>Основной объем/Внебюджетный фонд/Второй год</summary>
        public virtual double ExpenseVolVBY2 { get; set; }

        /// <summary>Дополнительная потребность/Внебюджетный фонд/Очередной год</summary>
        public virtual double ExpenseAddVolVBY0 { get; set; }

        /// <summary>Дополнительная потребность/Внебюджетный фонд/Первый год</summary>
        public virtual double ExpenseAddVolVBY1 { get; set; }

        /// <summary>Дополнительная потребность/Внебюджетный фонд/Второй год</summary>
        public virtual double ExpenseAddVolVBY2 { get; set; }

        /// <summary>Стоимость услуг технической поддержки для СПО/Текущий год/Бюджетные</summary>
        public virtual double SupportPriceY0 { get; set; }

        /// <summary>Стоимость услуг технической поддержки для СПО/Первый год/Бюджетные</summary>
        public virtual double SupportPriceY1 { get; set; }

        /// <summary>Стоимость услуг технической поддержки для СПО/Второй год/Бюджетные</summary>
        public virtual double SupportPriceY2 { get; set; }

        /// <summary>Стоимость услуг технической поддержки для СПО/Текущий год/Внебюджетные</summary>
        public virtual double SupportPriceVBY0 { get; set; }

        /// <summary>Стоимость услуг технической поддержки для СПО/Первый год/Внебюджетные</summary>
        public virtual double SupportPriceVBY1 { get; set; }

        /// <summary>Стоимость услуг технической поддержки для СПО/Второй год/Внебюджетные</summary>
        public virtual double SupportPriceVBY2 { get; set; }

        /// <summary>Количество/Текущий год/Бюджетные</summary>
        public virtual int QuantityY0 { get; set; }

        /// <summary>Количество/Первый год/Бюджетные</summary>
        public virtual int QuantityY1 { get; set; }

        /// <summary>Количество/Второй год/Бюджетные</summary>
        public virtual int QuantityY2 { get; set; }

        /// <summary>Количество/Текущий год/Внебюджетные</summary>
        public virtual int QuantityVBY0 { get; set; }

        /// <summary>Количество/Первый год/Внебюджетные</summary>
        public virtual int QuantityVBY1 { get; set; }

        /// <summary>Количество/Второй год/Внебюджетные</summary>
        public virtual int QuantityVBY2 { get; set; }

        /// <summary>Согласованные Основной объем/ФБ/Очередной год</summary>
        public virtual double AgreedExpenseVolY0 { get; set; }

        /// <summary>Согласованные Основной объем/ФБ/Первый год</summary>
        public virtual double AgreedExpenseVolY1 { get; set; }

        /// <summary>Согласованные Основной объем/ФБ/Второй год</summary>
        public virtual double AgreedExpenseVolY2 { get; set; }

        /// <summary>Согласованные Основной объем/ВБ/Очередной год</summary>
        public virtual double AgreedExpenseVolVBY0 { get; set; }

        /// <summary>Согласованные Основной объем/ВБ/Первый год</summary>
        public virtual double AgreedExpenseVolVBY1 { get; set; }

        /// <summary>Согласованные Основной объем/ВБ/Второй год</summary>
        public virtual double AgreedExpenseVolVBY2 { get; set; }

        //=======================PlanReport========================

        /// <summary>Номер реестровой записи</summary>
        public virtual string ReestrNumber { get; set; }

        /// <summary>Реквизиты актов приемки</summary>
        public virtual string DetailsActs { get; set; }

        /// <summary>Наименование фактически закупленного оборудования</summary>
        public virtual string FactName { get; set; }

        /// <summary>Стоимость фактически закупленного оборудования, программного обеспечения, тыс.руб.</summary>
        public virtual double FactCost { get; set; }

        /// <summary>Реквизиты госконтракта</summary>
        public virtual string PRSContract { get; set; }

        /// <summary>Наименование подрядчика</summary>
        public virtual string PRSContractor { get; set; }

        //===================================================Plan2012new=================================================================

        /// <summary>Объем бюджетных ассигнований очередной финансовый год</summary>
        public virtual double ExpenseVolumeCurrentYear { get; set; }

        /// <summary>Объем бюджетных ассигнований на первый год планового периода</summary>
        public virtual double ExpenseVolumeFirstYear { get; set; }

        /// <summary>Объем бюджетных ассигнований на второй год планового периода</summary>
        public virtual double ExpenseVolumeSecondYear { get; set; }

        /// <summary>Цена аналога, тыс. руб.</summary>
        public virtual double AnalogPrice { get; set; }

        public virtual Contract Contract { get; set; }

        /// <summary>Тип спецификации</summary>
        public virtual IList<SpecType> SpecType { get; set; }

        /// <summary>Характеристики</summary>
        public virtual IList<PlansSpecCharacteristic> Characteristics { get; protected set; }

        /// <summary>
        /// комментарий, раздел 3.2, отчет по плану
        /// </summary>
        public virtual string Comment32 { get; set; }

        /// <summary>
        /// номер
        /// </summary>
        public virtual int OrigNum { get; set; }

        /// <summary>
        /// Вид расходов
        /// </summary>
        public virtual ExpenseDirection ReportExpenseDirection { get; set; }

        /// <summary> Типы затрат  </summary>
        public virtual ExpenseType ReportExpenseType { get; set; }

        /// <summary> Фактическая стоимость в отчёте </summary>
        public virtual double ReportCost { get; set; }//null

        /// <summary>Количество в отчётах </summary>
        public virtual string ReportQuantity { get; set; }//null
    }
}