﻿namespace bgTeam.IsRui.Domain.Dto
{
    public class ClaimContentDto
    {
        public int Id { get; set; }

        public string Content { get; set; }
    }
}
