﻿using GB.Data.AccountingObjects.WorksAndGoods;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.WorkAndGood
{
    public class DeleteWorkAndGoodsByActCommand : ICommand<DeleteWorkAndGoodsByActContext>
    {
        ISessionFactoryGb factory;

        public DeleteWorkAndGoodsByActCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteWorkAndGoodsByActContext context)
        {            
            if (context.xId.HasValue && context.xId != 0)
            {
                var worksAndGoods = new Repository<WorkAndGoods>().GetAllEx(q => q.Where(x => x.ActOfAcceptance.Id == context.xId.Value));                

                if (worksAndGoods.Any())
                {
                    using (var session = factory.Create())
                    {
                        foreach (var wag in worksAndGoods)
                        {
                            wag.ActOfAcceptance = null;
                            session.SaveOrUpdate(wag);                            
                        }
                        session.Commit();
                    }
                }
            }
        }
    }
}
