﻿using GB.Data.Plans;
using GB.MKRF.Helpers;
using GB.MKRF.Repository;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    public class PlansActivityIndicatorService : ServiceBase<PlansActivityIndicator>, IService<PlansActivityIndicator, PlansActivityIndicatorArgs>
    {
        public PlansActivityIndicator Create(PlansActivityIndicatorArgs args)
        {
            return SaveOrUpdate(new PlansActivityIndicator(), args);
        }

        public PlansActivityIndicator Update(PlansActivityIndicatorArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private PlansActivityIndicator SaveOrUpdate(PlansActivityIndicator entity, PlansActivityIndicatorArgs args)
        {
            entity = ConvertDtoToEntity.FillPlansActivityIndicatorFromDto(entity, args);
            base.SaveOrUpdate(entity);

            return entity;
        }

        public void Delete(PlansActivityIndicatorArgs args)
        {
            base.Delete((int)args.Id);
        }

        private BaseIndicatorRepository<PlansActivityIndicator> _repository = new BaseIndicatorRepository<PlansActivityIndicator>();

        public new BaseIndicatorRepository<PlansActivityIndicator> Repository
        {
            get { return _repository; }
        }
    }
}