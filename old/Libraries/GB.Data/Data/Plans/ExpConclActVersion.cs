﻿using System.Collections.Generic;
using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>Версия акта экспертного заключения</summary>
    public class ExpConclActVersion : EntityBase
    {
        /// <summary>Соответствие критериям</summary>
        public virtual IList<ExpCriterionEquivalent> ExpCriterionEquivalent
        {
            get;
            protected set;
        }

        /// <summary>Экспертное заключение</summary>
        public virtual ExpConclusion ExpConclusion { get; set; }

        /// <summary>Cоответствие</summary>
        public virtual ExpActEquivalent ExpActEquivalent { get; set; }

        /// <summary>Несоответствие</summary>
        public virtual string Disagreement { get; set; }//null

        /// <summary>
        /// Проведение мероприятий с точки зрения эффективности
        /// </summary>
        public virtual Appropriate GoalEffective { get; set; }

        /// <summary>
        /// Финансирование мероприятий в запрашиваемых объемах
        /// </summary>
        public virtual Appropriate OrderFinance { get; set; }

        /// <summary>
        /// Целесообразный объем финансирования, текущий год
        /// </summary>
        public virtual double AppropriateVolumeFinanceY0 { get; set; }

        /// <summary>
        /// Целесообразный объем финансирования, первый год
        /// </summary>
        public virtual double AppropriateVolumeFinanceY1 { get; set; }

        /// <summary>
        /// Целесообразный объем финансирования, второй год
        /// </summary>
        public virtual double AppropriateVolumeFinanceY2 { get; set; }

        /// <summary>
        /// Предложение по доработке
        /// </summary>
        public virtual string Completion { get; set; }

        /// <summary>
        /// Код мероприятия
        /// </summary>
        public virtual string Code { get; set; }
    }
}