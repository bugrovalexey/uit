﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities.Goods
{
    public class TableCharacteristic : NotificationBaseViewModel
    {
        public const string NameCaption = "Наименование характеристики";
        public const string UnitCaption = "Единица измерения";
        public const string ValueCaption = "Значение";

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(ValueCaption)]
        public int? Value { get; set; }

        [DisplayName(UnitCaption)]
        public string UnitText { get; set; }

        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("CharacteristicDelete", "Activities", new { id = Id }, "GridGoodsCharacteristics"); }
        }

        public TableCharacteristic(PlansSpecCharacteristic value)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            Id = value.Id;
            Name = value.Name;
            UnitText = value.Unit.Return(x => x.Name, default(string));
            Value = value.Value;
        }
    }

    [MetadataType(typeof(CharacteristicViewModel_Validation))]
    public class CharacteristicEditViewModel : ActivitiesCardBase
    {
        public const string EditActionLink = "CharacteristicEdit";

        /// <summary>ИД товара</summary>
        public int GoodsId { get; set; }

        public int CharId { get; set; }

        [DisplayName(TableCharacteristic.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableCharacteristic.ValueCaption)]
        public int? Value { get; set; }

        #region Unit

        public SelectList UnitList { get; set; }

        [DisplayName(TableCharacteristic.UnitCaption)]
        public int UnitId { get; set; }

        #endregion Unit

        public CharacteristicEditViewModel(IStatusRepository statusRepository, int activityId, int goodsId, int? id)
            : base(statusRepository, activityId)
        {
            GoodsId = goodsId;

            UnitList = new SelectList(
                new RepositoryDc<Units>().GetAll(),
                "Id", "FullName", UnitId);

            var entity = new RepositoryDc<PlansSpecCharacteristic>().Get(id);
            entity.Do(FillData);
        }

        private void FillData(PlansSpecCharacteristic entity)
        {
            CharId = entity.Id;
            Name = entity.Name;
            UnitId = entity.Unit.Return(x => x.Id, default(int));
            Value = entity.Value;
            GoodsId = entity.Goods.Return(x => x.Id, default(int));
        }
    }

    public class CharacteristicViewModel_Validation : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        public int UnitId { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int? Value { get; set; }
    }
}