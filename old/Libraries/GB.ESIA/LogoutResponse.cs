﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Web.UI;
using System.Xml;
using Atp.Saml2;

namespace GB.ESIA
{
	/// <summary>
	/// Результат отсоединения от SSO на стороне ServiceProvider'а, который обязательно следует отправить IdentityProvider'у.
	/// Если информацию о результате не отправить, то IdentityProvider будет считать, что отсоединение от SSO не выполнено.
	/// </summary>
	public class LogoutResponse
	{
		private readonly string relayState = Guid.NewGuid().ToString();
		/// <summary>
		/// GUID-идентификатор запроса.
		/// </summary>
		public string RelayState
		{
			get { return relayState; }
		}

		/// <summary>
		/// Идентификатор запроса.
		/// </summary>
		public string Id
		{
			get { return logoutResponse.Id; }
		}

		/// <summary>
		/// Представление запроса в виде XML.
		/// </summary>
		/// <remarks>Пример использования: [EasyLogoutResponse].Xml.OuterXml</remarks>
		public XmlNode Xml
		{
			get { return logoutResponse.GetXml().ParentNode; }
		}

		/// <summary>
		/// Страница, с которой инициирован запрос.
		/// </summary>
		private readonly Page pageFrom;

		/// <summary>
		/// Сертификат, которым должен быть подписан отправляемый запрос.
		/// </summary>
		private readonly X509Certificate2 spX509Certificate;

		/// <summary>
		/// Запрос с результатом отсоединения от SSO.
		/// </summary>
		private readonly Atp.Saml2.LogoutResponse logoutResponse;

	    /// <summary>
        /// Конструктор для MVC
	    /// </summary>
	    /// <param name="page">Страница, с которой инициирован запрос.</param>
	    /// <param name="absoluteUrl">URL</param>
	    /// <param name="spCertificate">Сертификат, которым должен быть подписан отправляемый запрос.</param>
	    /// <param name="singleLogoutServiceResponseUrl">URL, на который должно быть отправлено подтверждение успешного выхода.</param>
	    /// <param name="logoutRequestId">Идентификатор запроса, на который посылается ответ.</param>
	    public LogoutResponse(Page page, string absoluteUrl, X509Certificate2 spCertificate, string singleLogoutServiceResponseUrl, string logoutRequestId)
		{
			logoutResponse = new Atp.Saml2.LogoutResponse();
			logoutResponse.Destination = singleLogoutServiceResponseUrl;
            logoutResponse.Issuer = new Issuer(absoluteUrl);
			logoutResponse.Status = new Status("urn:oasis:names:tc:SAML:2.0:status:Success", string.Empty);
			logoutResponse.InResponseTo = logoutRequestId;
			spX509Certificate = spCertificate;
			pageFrom = page;
		}

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="page">Страница, с которой инициирован запрос.</param>
        /// <param name="spCertificate">Сертификат, которым должен быть подписан отправляемый запрос.</param>
        /// <param name="singleLogoutServiceResponseUrl">URL, на который должно быть отправлено подтверждение успешного выхода.</param>
        /// <param name="logoutRequestId">Идентификатор запроса, на который посылается ответ.</param>
        public LogoutResponse(Page page, X509Certificate2 spCertificate, string singleLogoutServiceResponseUrl, string logoutRequestId)
            : this(page, Util.GetAbsoluteUrl(page, "~/"), spCertificate, singleLogoutServiceResponseUrl, logoutRequestId) { }

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <remarks>Используется для обработки ответа (запроса на подтверждение отключения) от IdentityProvider'а.</remarks>
		/// <param name="page">Страница, на которую поступил ответ (запрос на подтверждение отключения).</param>
		/// <param name="idpCertificate">Сертификат, которым поступивший ответ должен быть подписан.</param>
		public LogoutResponse(Page page, X509Certificate2 idpCertificate)
		{
			logoutResponse = Atp.Saml2.LogoutResponse.Create(page.Request, idpCertificate.PublicKey.Key);
		}

		/// <summary>
		/// Отправляет запрос на выход.
		/// </summary>
		public void Send()
		{
			logoutResponse.Redirect(pageFrom.Response, logoutResponse.Destination, relayState, spX509Certificate.PrivateKey);
		}
	}
}
