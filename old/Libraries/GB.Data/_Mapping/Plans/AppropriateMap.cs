﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class AppropriateMap : ClassMapBase<Appropriate>
    {
        public AppropriateMap() :
            base("Appropriate")
        {

        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Appropriate_Name");
        }


    }
}
