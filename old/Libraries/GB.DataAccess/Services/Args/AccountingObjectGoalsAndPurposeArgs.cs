﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Services.Args
{
    public class AccountingObjectGoalsAndPurposeArgs: BaseArgsNew
    {
        /// <summary>
        /// Цели создания ОУ
        /// </summary>
        public string Targets { get; set; }

        /// <summary>
        /// Назначение и область применения объекта учета
        /// </summary>
        public string PurposeAndScope { get; set; }
    }
}
