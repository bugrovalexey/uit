﻿using System;
using GB.Helpers;
using GB.MKRF.Entities.Documents;

namespace GB.MKRF.Web.Plans
{
    /// <summary>
    /// Документы заявки
    /// </summary>
    public partial class Documents : System.Web.UI.Page
    {
        public static string GetUrl(int PlanId)
        {
            string url = UrlHelper.Resolve("~/Plans/Documents.aspx");
            return (PlanId > 0) ? url + "?" + PlanCard.plansIdKey + "=" + PlanId : url;
        }

        protected override void OnInit(EventArgs e)
        {
            dc.DocType = DocTypeEnum.demand_document;
            dc.Owner = (Master as PlanCard).CurrentPlan.Demand;
            
            base.OnInit(e);
        }
    }
}