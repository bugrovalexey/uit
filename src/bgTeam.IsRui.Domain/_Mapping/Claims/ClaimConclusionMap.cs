﻿namespace bgTeam.IsRui.Domain._Mapping.Claims
{
    using bgTeam.IsRui.Domain.Entities.Claims;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class ClaimConclusionMap : ClassMapHiLo<ClaimConclusion>
    {
        public ClaimConclusionMap()
            : base("ClaimConclusion")
        {
        }

        protected override void InitMap()
        {
            References<Claim>(x => x.Claim, "ClaimConclusion_Claim_ID").Not.Nullable();

            Map(x => x.Comment, "ClaimConclusion_Comment");
        }
    }
}
