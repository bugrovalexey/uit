﻿using Uviri.MKRF.Entities.Expertise;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpConslCheckMap : ClassMapBase<ExpConslCheck>
    {
        public ExpConslCheckMap()
            : base("Exp_Concl_Check")
        {
//            Table("w_");

//            Id(x => x.Id, "Exp_Concl_Check_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@Exp_Concl_Check_Res = ? 
//						  ,@Exp_Concl_Check_Comm = ?
//                          ,@Exp_Conclusion_ID = ?
//                          ,@Exp_Demand_Check_ID = ?
//						  ,@Exp_Concl_Check_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Exp_Concl_Check_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Concl_Check_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Exp_Concl_Check_Delete @Exp_Concl_Check_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Check, "Exp_Concl_Check_Res");
            Map(x => x.Comment, "Exp_Concl_Check_Comm");

            References<ExpConclusion>(x => x.Conclusion, "Exp_Conclusion_ID").Not.Nullable();
            References<ExpDemandCheck>(x => x.DemandCheck, "Exp_Demand_Check_ID").Not.Nullable();
        }
    }
}
