﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ServicesGetAllServicesStory : IStory<ServicesGetAllServicesStoryContext, IEnumerable<ActivitiesServiceDto>>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IMapperBase _mapper;

        public ServicesGetAllServicesStory(IRepositoryRui repositoryRui, IMapperBase mapperBase)
        {
            _repositoryRui = repositoryRui;
            _mapper = mapperBase;
        }

        public IEnumerable<ActivitiesServiceDto> Execute(ServicesGetAllServicesStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivitiesServiceDto>> ExecuteAsync(ServicesGetAllServicesStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var activitiesServises = _repositoryRui.GetAllEx<PlansActivityService>(
                q => q.Where(x => x.Activity.Id == context.ActivityId))
                .Select(x => _mapper.Map(x, new ActivitiesServiceDto()));

            return activitiesServises;
        }
    }
}
