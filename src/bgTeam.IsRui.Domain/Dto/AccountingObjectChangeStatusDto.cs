﻿namespace bgTeam.IsRui.Domain.Dto
{
    public class AccountingObjectChangeStatusDto
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }
    }
}
