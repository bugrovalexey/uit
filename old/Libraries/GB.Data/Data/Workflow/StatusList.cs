﻿using GB.Data.Common;

namespace GB.Data.Workflow
{
    /// <summary>Изменение статуса</summary>
    public class StatusList : EntityBase
    {
        /// <summary>Роль</summary>
        public virtual Role Role { get; set; }

        /// <summary>Текущий статус</summary>
        public virtual Status From { get; set; }

        /// <summary>Следующий статус</summary>
        public virtual Status To { get; set; }
    }
}