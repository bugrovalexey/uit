﻿using System;

namespace bgTeam.IsRui.MKRF.Logic.Status
{
    public class IncompleteInfoException : Exception
    {
        public string ActionName { get; private set; }
        public string ControllerName { get; private set; }
        public object RouteValues { get; private set; }

        public IncompleteInfoException(string text, string actionName, string controllerName, object routeValues)
            : base(text)
        {
            ActionName = actionName;
            ControllerName = controllerName;
            RouteValues = routeValues;
        }
    }
}
