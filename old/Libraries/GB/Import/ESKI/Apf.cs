﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Core.Import.ESKI
{
    /// <summary>
    /// Импорт данных из ЕСКИ в ФАП
    /// </summary>
    [DataContract]
    public class FundImport : IEntity
    {
        public string EntityName { get { return "Fund object import"; } }

        ///<summary>Идентификатор в ФАП</summary>
        [Annotation("Идентификатор в ФАП")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        [DataMember(IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Название решения</summary>
        [DataMember]
        [Annotation("Название решения")]
        public string Name { get; set; }

        /// <summary>Реестровый номер записи госконтракта</summary>
        [DataMember]
        [Annotation("Реестровый номер записи госконтракта")]
        public string ContractRegNum { get; set; }
        
        /// <summary>Логин пользователя</summary>
        [DataMember(IsRequired = true)]
        [Annotation("Логин пользователя")]
        public string UserLogin { get; set; }
    }

    /// <summary>
    /// Экспорт данных из ФАП в ЕСКИ 
    /// </summary>
    [DataContract]
    public class FundExport : IEntity
    {
        public string EntityName { get { return "Fund object export "; } }

        ///<summary>Идентификатор в ФАП</summary>
        [Annotation("Идентификатор в ФАП")]
        public int Id { get; set; }

        /// <summary>Идентификатор в СКП</summary>
        [DataMember(IsRequired = true)]
        [Annotation("Идентификатор в СКП")]
        public string ExtId { get; set; }

        /// <summary>Вид решения</summary>
        [DataMember]
        [Annotation("Вид решения")]
        public string KindSolution { get; set; }

        /// <summary>Аннотация</summary>
        [DataMember]
        [Annotation("Аннотация")]
        public string Annotation{ get; set; }

        /// <summary>Возможность использования по модели SaaS</summary>
        [DataMember]
        [Annotation("Возможность использования по модели SaaS")]
        public bool IsSAAS { get; set; }

        /// <summary>Тиражируемое решение</summary>
        [DataMember]
        [Annotation("Тиражируемое решение")]
        public bool IsDuplicated { get; set; }

        /// <summary>Рейтинг </summary>
        [DataMember]
        [Annotation("Рейтинг ")]
        public double Rating { get; set; }
        
        /// <summary>Список назначений программного решения </summary>
        [DataMember]
        [Annotation("Список назначений программного решения ")]
        public List<string> Purpose { get; set; }

        /// <summary>Область применения </summary>
        [DataMember]
        [Annotation("Область применения ")]
        public List<string> Scope { get; set; }

        /// <summary>Перечень ОГВ, использующих решение </summary>
        [DataMember]
        [Annotation("Перечень ОГВ, использующих решение")]
        public List<string> GovtOrgan { get; set; }
    }

    ///// <summary>
    ///// Импорт данных из ЕСКИ в ФАП
    ///// </summary>
    //[DataContract]
    //public class Apf : IEntity
    //{
    //    public string EntityName { get { return "Fund object"; } }

    //    ///<summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Краткое наименование</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Краткое наименование")]
    //    public string ShortName { get; set; }

    //    /// <summary>Полное наименование</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Полное наименование")]
    //    public string FullName { get; set; }

    //    /// <summary>Классификация</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Классификация")]
    //    public string Clasification { get; set; }

    //    /// <summary>Статус подписания ЭП</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Статус подписания ЭП")]
    //    public bool IsSign { get; set; }

    //    /*Вкладка Общая информация */
    //    /// <summary>Дата регистрации</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Дата регистрации")]
    //    public DateTime RegistrationDate { get; set; }

    //    /// <summary>Цели создания</summary>
    //    [DataMember(Order = 6)]
    //    [Annotation("Цели создания")]
    //    public string Goals { get; set; }

    //    /// <summary>Статус регистровой записи</summary>
    //    [DataMember(Order = 7)]
    //    [Annotation("Статус регистровой записи")]
    //    public string StatusRegistration { get; set; }

    //    /// <summary>Текущий статус</summary>
    //    [DataMember(Order = 8)]
    //    [Annotation("Текущий статус")]
    //    public string CurrentStatus { get; set; }

    //    /// <summary>Назначение и область применения</summary>
    //    [DataMember(Order = 9)]
    //    [Annotation("Назначение и область применения")]
    //    public string AreaOfApplication { get; set; }

    //    /// <summary>Расходы на создание до 2013 года (тыс. руб.)</summary>
    //    [DataMember(Order = 10)]
    //    [Annotation("Расходы на создание до 2013 года (тыс. руб.)")]
    //    public string CreationExpenses { get; set; }

    //    /// <summary>Расходы на эксплуатацию до 2013 года (тыс. руб.)</summary>
    //    [DataMember(Order = 11)]
    //    [Annotation("Расходы на эксплуатацию до 2013 года (тыс. руб.)")]
    //    public string ExploitationExpenses { get; set; }


    //    /// <summary>Входит в состав комплексной ИС</summary>
    //    [DataMember(Order = 12)]
    //    [Annotation("Входит в состав комплексной ИС")]
    //    public bool IsComplexIS { get; set; }

    //    /// <summary>Входит в состав межведомственной ИС</summary>
    //    [DataMember(Order = 13)]
    //    [Annotation("Входит в состав межведомственной ИС")]
    //    public bool IsInterDepartmentIS{ get; set; }

    //    /// <summary>Список нормативных документов </summary>
    //    [DataMember(Order = 14)]
    //    [Annotation("Список нормативных документов")]
    //    public NormativeDocument[] NormativeDocument { get; set; }

    //    /// <summary>Государственный орган, уполномоченный на создание, развитие, модернизацию ИС или компонента ИТКИ</summary>
    //    [DataMember(Order = 15)]
    //    [Annotation("Государственный орган, уполномоченный на создание, развитие, модернизацию ИС или компонента ИТКИ")]
    //    public GovtOrgan[] CreationGovtOrgan { get; set; }

    //    /// <summary>Государственный орган или иное юридическое лицо, уполномоченный на эксплуатацию ИС или компонента ИТКИ</summary>
    //    [DataMember(Order = 16)]
    //    [Annotation("Государственный орган или иное юридическое лицо, уполномоченный на эксплуатацию ИС или компонента ИТКИ")]
    //    public GovtOrgan[] ExploitationGovtOrgan { get; set; }

    //    /// <summary>Ответственные должностные лица</summary>
    //    [DataMember(Order = 17)]
    //    [Annotation("Ответственные должностные лица")]
    //    public Responsible[] Responsible { get; set; }

    //    /// <summary>Компоненты ИТКИ</summary>
    //    [DataMember(Order = 18)]
    //    [Annotation("Компоненты ИТКИ")]
    //    public ITKIComponent[] ITKIComponent { get; set; }

    //    /// <summary>Сведения о видах обеспечения</summary>
    //    [DataMember(Order = 19)]
    //    [Annotation("Сведения о видах обеспечения")]
    //    public KindGuarantee[] KindGuarantee { get; set; }

    //    /// <summary>Реализуемые системой государственные услуги и функции </summary>
    //    [DataMember(Order = 20)]
    //    [Annotation("Реализуемые системой государственные услуги и функции ")]
    //    public Services[] Services { get; set; }
        
    //    /// <summary>Обязательные функциональные характеристики </summary>
    //    [DataMember(Order = 21)]
    //    [Annotation("Обязательные функциональные характеристики ")]
    //    public RequiredCharacteristics[] RequiredCharacteristics { get; set; }

    //    /// <summary>Прочие функциональные характеристики</summary>
    //    [DataMember(Order = 22)]
    //    [Annotation("Прочие функциональные характеристики")]
    //    public OtherCharacteristics[] OtherCharacteristics { get; set; }

    //    /// <summary>Информационная безопасность</summary>
    //    [DataMember(Order = 23)]
    //    [Annotation("Информационная безопасность")]
    //    public Security[] Security { get; set; }

    //    /// <summary>Мероприятия по информатизации</summary>
    //    [DataMember(Order = 24)]
    //    [Annotation("Мероприятия по информатизации")]
    //    public InformatizationActivity[] InformatizationActivity { get; set; }

    //    /// <summary>Сведения о мероприятиях по созданию, развитию, модернизации и эксплуатации </summary>
    //    [DataMember(Order = 25)]
    //    [Annotation("Сведения о мероприятиях по созданию, развитию, модернизации и эксплуатации ")]
    //    public OperationActivity[] OperationActivity { get; set; }
        
    //    /// <summary>Сведения об объявлении торгов и заключенных ГК </summary>
    //    [DataMember(Order = 26)]
    //    [Annotation("Сведения об объявлении торгов и заключенных ГК ")]
    //    public Contract[] Contract { get; set; }

    //    /// <summary>Сведения об актах сдачи-приемки</summary>
    //    [DataMember(Order = 27)]
    //    [Annotation("Сведения об актах сдачи-приемки")]
    //    public Act[] Act { get; set; }

    //    /// <summary>Сведения о принятии к бюджетному учету</summary>
    //    [DataMember(Order = 28)]
    //    [Annotation("Сведения о принятии к бюджетному учету")]
    //    public Accounting[] Accounting { get; set; }

    //    /// <summary>Ввод в эксплуатацию</summary>
    //    [DataMember(Order = 29)]
    //    [Annotation("Ввод в эксплуатацию")]
    //    public Commissioning[] Commissioning  { get; set; }

    //    /// <summary>Вывод из эксплуатации</summary>
    //    [DataMember(Order = 30)]
    //    [Annotation("Вывод из эксплуатации")]
    //    public Decommissioning[] Decommissioning { get; set; }

    //    /// <summary>Пользовательские веб интерфейсы </summary>
    //    [DataMember(Order = 31)]
    //    [Annotation("Пользовательские веб интерфейсы ")]
    //    public WebInteface[] WebInteface { get; set; }

    //    /// <summary>Веб-сервисы (машинные интерфейсы) системы</summary>
    //    [DataMember(Order = 31)]
    //    [Annotation("Веб-сервисы (машинные интерфейсы) системы")]
    //    public WebService[] WebService { get; set; }

    //    /// <summary>Прочие интерфейсы</summary>
    //    [DataMember(Order = 33)]
    //    [Annotation("Прочие интерфейсы")]
    //    public OtherInteface[] OtherInteface { get; set; }
    //}

    ///// <summary>Прочие интерфейсы</summary>
    //[DataContract]
    //[Annotation("Прочие интерфейсы")]
    //public  class OtherInteface : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "OtherInteface"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Наименование</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование")]
    //    public string Name { get; set; }

    //    /// <summary>Описание</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Описание")]
    //    public string Description { get; set; }
    //}
   
    ///// <summary>Веб-сервисы (машинные интерфейсы) системы</summary>
    //[DataContract]
    //[Annotation("Веб-сервисы (машинные интерфейсы) системы")]
    //public class WebService : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "WebService"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }
        
    //    /// <summary>Назначение</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Назначение")]
    //    public string Purpose { get; set; }

    //    /// <summary>Описание</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Описание")]
    //    public string Description { get; set; }

    //    /// <summary>Идентификатор сервиса</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Идентификатор сервиса")]
    //    public string ServiceId { get; set; }

    //    /// <summary>Ссылка на описание сервиса с СМЭВ</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Ссылка на описание сервиса с СМЭВ")]
    //    public string URL { get; set; }
    //}
   
    ///// <summary>Пользовательские веб интерфейсы </summary>
    //[DataContract]
    //[Annotation("Пользовательские веб интерфейсы ")]
    //public class WebInteface : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "WebInteface"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }
        
    //    /// <summary>Наименование</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование")]
    //    public string Name { get; set; }

    //    /// <summary>Назначение</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Назначение")]
    //    public string Purpose { get; set; }
        
    //    /// <summary>Тип интерфейса</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Тип интерфейса")]
    //    public string Type { get; set; }

    //    /// <summary>URL</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("URL")]
    //    public string URL { get; set; }
    //}

    ///// <summary>Вывод из эксплуатации</summary>
    //[DataContract]
    //[Annotation("Вывод из эксплуатации")]
    //public class Decommissioning : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "Decommissioning"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Наименование</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование")]
    //    public string Name { get; set; }

    //    /// <summary>Номер документа</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Номер документа")]
    //    public string Number { get; set; }

    //    /// <summary>Дата принятия</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Дата принятия")]
    //    public DateTime AdoptionDate { get; set; }

    //    /// <summary>Дата вывода из эксплуатации</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Дата вывода из эксплуатации")]
    //    public DateTime DecommissioningDate { get; set; }
    //}
    
    ///// <summary>Ввод в эксплуатацию</summary>
    //[DataContract]
    //[Annotation("Ввод в эксплуатацию")]
    //public class Commissioning : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "Commissioning"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Наименование</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование")]
    //    public string Name { get; set; }

    //    /// <summary>Номер документа</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Номер документа")]
    //    public string Number { get; set; }

    //    /// <summary>Дата принятия</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Дата принятия")]
    //    public DateTime AdoptionDate { get; set; }

    //    /// <summary>Дата ввода в эксплуатацию</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Дата ввода в эксплуатацию")]
    //    public DateTime CommissioningDate { get; set; }
    //}
    
    ///// <summary>Сведения о принятии к бюджетному учету</summary>
    //[DataContract]
    //[Annotation("Сведения о принятии к бюджетному учету")]
    //public class Accounting : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "Accounting"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Дата учета</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Дата учета")]
    //    public DateTime AccountingDate { get; set; }

    //    /// <summary>Код БК</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Код БК")]
    //    public string Code { get; set; }

    //    /// <summary>Балансовая стоимость (тыс. руб.)</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Балансовая стоимость (тыс. руб.)")]
    //    public string Cost { get; set; }

    //    /// <summary>Основания</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Основания")]
    //    public string Reason { get; set; }

    //    /// <summary>Иные операции с объектами учета</summary>
    //    [DataMember(Order = 6)]
    //    [Annotation("Иные операции с объектами учета")]
    //    public string Operations { get; set; }
    //}

    ///// <summary>Сведения об актах сдачи-приемки</summary>
    //[DataContract]
    //[Annotation("Сведения об актах сдачи-приемки")]
    //public class Act : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "Act"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Наименование акта</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование акта")]
    //    public string Name { get; set; }

    //    /// <summary>Дата заключения акта</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Дата заключения акта")]
    //    public DateTime ActDate { get; set; }

    //    /// <summary>Сумма по акту</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Сумма по акту")]
    //    public string ActSum { get; set; }
    //}

    ///// <summary>Сведения об объявлении торгов и заключенных ГК </summary>
    //[DataContract]
    //[Annotation("Сведения об объявлении торгов и заключенных ГК ")]
    //public class Contract : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "Contract"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Тип записи</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Тип записи")]
    //    public string Type { get; set; }

    //    /// <summary>Наименование</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Наименование")]
    //    public string Name { get; set; }

    //    /// <summary>Реестровый номер закупки</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Реестровый номер закупки")]
    //    public string PurchaseNumber { get; set; }

    //    /// <summary>Реестровый номер ГК</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Реестровый номер ГК")]
    //    public string ContractNumber { get; set; }

    //    /// <summary>Номер лота</summary>
    //    [DataMember(Order = 6)]
    //    [Annotation("Номер лота")]
    //    public string LotNumber { get; set; }

    //    /// <summary>Заказчик</summary>
    //    [DataMember(Order = 7)]
    //    [Annotation("Заказчик")]
    //    public string Customer { get; set; }
    //}

    ///// <summary>Сведения о мероприятиях по созданию, развитию, модернизации и эксплуатации </summary>
    //[DataContract]
    //[Annotation("Сведения о мероприятиях по созданию, развитию, модернизации и эксплуатации ")]
    //public class OperationActivity : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "OperationActivity"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Наименование</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование")]
    //    public string Name { get; set; }

    //    /// <summary>Год исполнения</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Год исполнения")]
    //    public int Year { get; set; }

    //    /// <summary>На очередной финансовый год (тыс. руб.)</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("На очередной финансовый год (тыс. руб.)")]
    //    public string Sum { get; set; }
    //}

    ///// <summary>Мероприятия по информатизации</summary>
    //[DataContract]
    //[Annotation("Мероприятия по информатизации")]
    //public class InformatizationActivity : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "InformatizationActivity"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Мероприятия по информатизации</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Мероприятия по информатизации")]
    //    public string Name { get; set; }

    //    /// <summary>Сроки создания</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Сроки создания")]
    //    public DateTime Creation { get; set; }

    //    /// <summary>Сведения о сроках создания</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Сведения о сроках создания")]
    //    public string CreationInfo { get; set; }

    //    /// <summary>Дата начала</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Дата начала")]
    //    public DateTime DateStart { get; set; }

    //    /// <summary>Дата завершения</summary>
    //    [DataMember(Order = 6)]
    //    [Annotation("Дата завершения")]
    //    public DateTime DateEnd { get; set; }

    //    /// <summary>План информатизации</summary>
    //    [DataMember(Order = 7)]
    //    [Annotation("План информатизации")]
    //    public string Plan { get; set; }
    //}

    ///// <summary>Информационная безопасность</summary>
    //[DataContract]
    //[Annotation("Информационная безопасность")]
    //public class Security : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "Services"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Сведения о защите информации</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Сведения о защите информации")]
    //    public string SecurityInfo { get; set; }

    //    /// <summary>Уровень защищенности</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Уровень защищенности")]
    //    public string SecurityLevel { get; set; }

    //    /// <summary>Сведения о прохождении специальных проверок</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Сведения о прохождении специальных проверок")]
    //    public string SecurityTest { get; set; }
    //}

    ///// <summary>Прочие функциональные характеристики </summary>
    //[DataContract]
    //[Annotation("Прочие функциональные характеристики ")]
    //public class OtherCharacteristics : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "OtherCharacteristics"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Наименование показателя</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование показателя")]
    //    public string Name { get; set; }

    //    /// <summary>Единицы измерения</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Единицы измерения")]
    //    public string Measurement { get; set; }

    //    /// <summary>Номинальное (плановое) значение</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Номинальное (плановое) значение")]
    //    public string PlanValue { get; set; }

    //    /// <summary>Максимальное (расчетное) значение</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Максимальное (расчетное) значение")]
    //    public string MaxValue { get; set; }

    //    /// <summary>Фактическое значение</summary>
    //    [DataMember(Order = 6)]
    //    [Annotation("Фактическое значение")]
    //    public string FactValue { get; set; }

    //    /// <summary>Дата измерения</summary>
    //    [DataMember(Order = 7)]
    //    [Annotation("Дата измерения")]
    //    public DateTime DateMeasurement { get; set; }

    //    /// <summary>Период измерения</summary>
    //    [DataMember(Order = 8)]
    //    [Annotation("Период измерения")]
    //    public string Period { get; set; }
    //}

    ///// <summary>Обязательные функциональные характеристики </summary>
    //[DataContract]
    //[Annotation("Обязательные функциональные характеристики ")]
    //public class RequiredCharacteristics : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "RequiredCharacteristics"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }
        
    //    /// <summary>Наименование показателя</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование показателя")]
    //    public string Name { get; set; }

    //    /// <summary>Единицы измерения</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Единицы измерения")]
    //    public string Measurement { get; set; }

    //    /// <summary>Номинальное (плановое) значение</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Номинальное (плановое) значение")]
    //    public string PlanValue { get; set; }

    //    /// <summary>Максимальное (расчетное) значение</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Максимальное (расчетное) значение")]
    //    public string MaxValue { get; set; }

    //    /// <summary>Фактическое значение</summary>
    //    [DataMember(Order = 6)]
    //    [Annotation("Фактическое значение")]
    //    public string FactValue { get; set; }

    //    /// <summary>Дата измерения</summary>
    //    [DataMember(Order = 7)]
    //    [Annotation("Дата измерения")]
    //    public DateTime DateMeasurement { get; set; }

    //    /// <summary>Период измерения</summary>
    //    [DataMember(Order = 8)]
    //    [Annotation("Период измерения")]
    //    public string Period { get; set; }
    //}

    ///// <summary>Реализуемые системой государственные услуги и функции </summary>
    //[DataContract]
    //[Annotation("Реализуемые системой государственные услуги и функции ")]
    //public class Services : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "Services"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Наименование ГУ/ГФ</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование ГУ/ГФ")]
    //    public string NameService { get; set; }

    //    /// <summary>Наименование</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Наименование")]
    //    public string Name { get; set; }

    //    /// <summary>Процент использования</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Процент использования")]
    //    public string Percent { get; set; }
    //}

    ///// <summary>Сведения о видах обеспечения</summary>
    //[DataContract]
    //[Annotation("Сведения о видах обеспечения")]
    //public class KindGuarantee : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "KindGuarantee"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }
        
    //    /// <summary>Наименование</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование")]
    //    public string Name { get; set; }

    //    /// <summary>Классификация</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Классификация")]
    //    public string Classification { get; set; }

    //    /// <summary>Количество</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Количество")]
    //    public int Count { get; set; }

    //    /// <summary>Сумма (тыс. руб.)</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Сумма (тыс. руб.)")]
    //    public string Cost { get; set; }

    //    /// <summary>Вид обеспечения</summary>
    //    [DataMember(Order = 6)]
    //    [Annotation("Вид обеспечения")]
    //    public string KindOfGuarantee { get; set; }
    //}

    ///// <summary>Компоненты ИТКИ</summary>
    //[DataContract]
    //[Annotation("Компоненты ИТКИ")]
    //public class ITKIComponent : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "ITKIComponent"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Уникальный идентификационный номер ОУ</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Уникальный идентификационный номер ОУ")]
    //    public string UniqueNumber { get; set; }

    //    /// <summary>Краткое наименование</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Краткое наименование")]
    //    public string Name { get; set; }

    //    /// <summary>Классификация</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Классификация")]
    //    public string Classification { get; set; }
    //}

    ///// <summary>Ответственные должностные лица</summary>
    //[DataContract]
    //[Annotation("Ответственные должностные лица")]
    //public class Responsible : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "Responsible"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Имя</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Имя")]
    //    public string FirstName { get; set; }

    //    /// <summary>Фамилия</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Фамилия")]
    //    public string LastName { get; set; }

    //    /// <summary>Отчество</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Отчество")]
    //    public string MiddleName { get; set; }

    //    /// <summary>Подразделение</summary>
    //    [DataMember(Order = 5)]
    //    [Annotation("Подразделение")]
    //    public string Department { get; set; }

    //    /// <summary>Телефон</summary>
    //    [DataMember(Order = 6)]
    //    [Annotation("Телефон")]
    //    public string Phone { get; set; }

    //    /// <summary>Почта</summary>
    //    [DataMember(Order = 7)]
    //    [Annotation("Почта")]
    //    public string Email { get; set; }

    //    /// <summary>Сфера ответственности</summary>
    //    [DataMember(Order = 8)]
    //    [Annotation("Сфера ответственности")]
    //    public string Responsibility { get; set; }

    //    /// <summary>Комментарии</summary>
    //    [DataMember(Order = 9)]
    //    [Annotation("Комментарии")]
    //    public string Comment { get; set; }
    //}

    ///// <summary>Государственный орган</summary>
    //[DataContract]
    //[Annotation("Государственный орган")]
    //public class GovtOrgan : IEntity
    //{
    //    public string EntityName
    //    {
    //        get { return "Govt Organ"; }
    //    }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Наименование</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование")]
    //    public string Name { get; set; }
    //}

    ///// <summary>Нормативные документы </summary>
    //[DataContract]
    //[Annotation("Нормативные документы")]
    //public class NormativeDocument : IEntity
    //{
    //    public string EntityName { get { return "Normative Document"; } }

    //    /// <summary>Идентификатор в ФАП</summary>
    //    [Annotation("Идентификатор в ФАП")]
    //    public int Id { get; set; }

    //    /// <summary>Идентификатор в СКП</summary>
    //    [DataMember(Order = 1, IsRequired = true)]
    //    [Annotation("Идентификатор в СКП")]
    //    public string ExtId { get; set; }

    //    /// <summary>Наименование</summary>
    //    [DataMember(Order = 2)]
    //    [Annotation("Наименование")]
    //    public string Name { get; set; }

    //    /// <summary>Номер документа</summary>
    //    [DataMember(Order = 3)]
    //    [Annotation("Номер документа")]
    //    public string Number { get; set; }

    //    /// <summary>Дата принятия</summary>
    //    [DataMember(Order = 4)]
    //    [Annotation("Дата принятия")]
    //    public DateTime AcceptDate { get; set; }
    //}
}
