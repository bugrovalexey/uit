INSERT INTO Role (Role_ID, Role_Name, Role_Web_Page, Role_Code)
VALUES(62, 'Функциональный заказчик', '~\\', 'func_customer'),
      (63, 'Ответственный менеджер', '~\\', 'resp_manager'),
      (64, 'Финансовый менеджер', '~\\', 'fin_manager');
  
INSERT INTO Status_Group (Status_Group_ID, Status_Group_Name)
VALUES(4, 'Статусы заявок');

INSERT INTO Status (Status_ID, Status_Name, Status_Action_Name, Status_Css, Status_Group_ID)
VALUES(200, 'Создание', 'Создание', NULL, 4),
      (201, 'На рассмотрении', 'На рассмотрении', NULL, 4),
      (202, 'Оценка', 'Оценка', NULL, 4),
      (203, 'На доработке', 'На доработке', NULL, 4),
      (204, 'Экспертная группа', 'Экспертная группа', NULL, 4),
      (205, 'Реализация', 'Реализация', NULL, 4),
      (206, 'Завершено', 'Завершено', NULL, 4);
	  
INSERT INTO Status_Change_List (Status_Change_List_ID, Role_ID, Status_From_ID, Status_To_ID)
VALUES(5,  62, 200, 201),
      (6,  62, 203, 201),
	  
      (7,  63, 201, 203),
      (8,  63, 201, 202),
      (9,  63, 204, 203),
      (10, 63, 204, 205),
      (11, 63, 205, 206),
	  
      (12, 64, 202, 203),
      (13, 64, 202, 204);