﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/PlanCard.master" AutoEventWireup="true" CodeBehind="Documents.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.Documents" %>

<%@ Register src="../../Common/DocControl.ascx" tagname="DocControl" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <div class="page">
        <uc1:DocControl ID="dc" runat="server"/>
    </div>
</asp:Content>
