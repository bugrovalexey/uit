﻿using AutoMapper;
using GB.Data.AccountingObjects.WorksAndGoods;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class WorkAndServiceService : ServiceBase<WorkAndService>, IService<WorkAndService, WorkAndServiceArgs>
    {
        public WorkAndService Create(WorkAndServiceArgs args)
        {
            var entity = new WorkAndService();
            var result = SaveOrUpdate(entity, args);

            args.Id = result.Id;
            new WorksAndGoodsService().Create(args);

            return result;
        }

        public WorkAndService Update(WorkAndServiceArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            var result = SaveOrUpdate(entity, args);

            new WorksAndGoodsService().Update(args);

            return result;
        }

        private WorkAndService SaveOrUpdate(WorkAndService entity, WorkAndServiceArgs args)
        {
            Mapper.Map(args, entity);
            SaveOrUpdate(entity);
            
            return entity;
        }

        public void Delete(WorkAndServiceArgs args)
        {
            Delete(args.Id);
            new WorksAndGoodsService().Delete(args);
        }

    }
}
