﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UserDto
    {
        public int Id { get; set; }

        /// <summary>Признак активности</summary>
        public bool IsActive { get; set; }

        /// <summary>Ведомство</summary>
        public int DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        /// <summary>Логин</summary>
        public string Login { get; set; }

        /// <summary>Пароль</summary>
       // public string Password { get; set; }

        /// <summary>Имя</summary>
        public string Name { get; set; }

        /// <summary>Фамилия</summary>
        public string Surname { get; set; }

        /// <summary>Отчество</summary>
        public string Middlename { get; set; }

        public string FullName { get; set; }

        /// <summary>Место работы</summary>
        public string Work { get; set; }//null

        /// <summary>Образование</summary>
        public string Education { get; set; }//null

        /// <summary>E-mail</summary>
        public string Email { get; set; }//null

        /// <summary>Должность</summary>
        public string Job { get; set; }//null

        /// <summary>Рабочий телефон</summary>
        public string WorkPhone { get; set; }//null

        /// <summary>Мобильный телефон</summary>
        public string MobPhone { get; set; }//null

        /// <summary>Факс</summary>
        public string Fax { get; set; }//null

        /// <summary>Получать письма</summary>
        public bool ReceiveMail { get; set; }

        /// <summary>Снилс</summary>
        public string Snils { get; set; }

        /// <summary> Пользователь авторизовался через ЕСИА </summary>
        public bool IsESIAUser { get; set; }

        /// <summary> Информация о токене пользователя
        /// Наличие данных говорит о необходимости проверить сертификат перед подписанием</summary>
        public string TokenInfo { get; set; }

        /// <summary>Ответственный от ОГВ</summary>
        public bool IsResponsible { get; set; }

        /// <summary>JSON строка с настройками пользователя</summary>
        public string Options { get; set; }

        /// <summary>Роли</summary>
        public IList<RoleDto> Roles { get; set; }
    }
}
