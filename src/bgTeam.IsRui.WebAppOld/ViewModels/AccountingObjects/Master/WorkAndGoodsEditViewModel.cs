﻿using bgTeam.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.MKRF.Logic.Access;
using System.Collections.Generic;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class WorkAndGoodsEditViewModel : ViewModelBase
    {
        private const string WorksAndGoodsCaption = "Работа/Товар";

        public WorkAndGoodsEditViewModel(int actsOfAcceptanceId, int accountingObjectId, int? id)
        {
            ActsOfAcceptanceId = actsOfAcceptanceId;
            AccountingObjectId = accountingObjectId;
            ReadOnly = ManagerAccess.CanEdit(new RepositoryDc<AccountingObject>().GetExisting(accountingObjectId)) ==
                       false;

            var entity = new RepositoryDc<WorkAndGoods>().Get(id);
            entity.Do(Fill);
        }

        public void Fill(WorkAndGoods entity)
        {
            Id = entity.Id;
            WorksAndGoods = new List<IEntityName>() { new SelectEntityInfo { Id = entity.Id, Name = entity.Name } };
        }

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        public int ActsOfAcceptanceId { get; set; }

        [DisplayName(WorksAndGoodsCaption)]
        public IList<IEntityName> WorksAndGoods { get; private set; }
    }
}