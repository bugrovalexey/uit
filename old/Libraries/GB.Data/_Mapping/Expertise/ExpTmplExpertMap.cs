﻿using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Expertise;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpTmplExpertMap : ClassMapBase<ExpTmplExpert>
	{
		public ExpTmplExpertMap()
            : base("Exp_Tmpl_Expert")
		{
//            Table("w_");

//            Id(x => x.Id, "Exp_Tmpl_Expert_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_Exp_Tmpl_Expert_Insert
//						  @Exp_Tmpl_Expert_Name = ?
//						 ,@Users_ID = ?
//						 ,@Exp_Tmpl_Expert_ID = ?").Check.None();
			
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Tmpl_Expert_Update
//						  @Exp_Tmpl_Expert_Name = ?
//						 ,@Users_ID = ?
//						 ,@Exp_Tmpl_Expert_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Tmpl_Expert_Delete
//						  @Exp_Tmpl_Expert_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Exp_Tmpl_Expert_Name");

			References<User>(x => x.User, "Users_ID");

            HasMany(x => x.ExpertList).KeyColumns.Add("Exp_Tmpl_Expert_ID").Inverse().Cascade.AllDeleteOrphan();
        }
    }
}
