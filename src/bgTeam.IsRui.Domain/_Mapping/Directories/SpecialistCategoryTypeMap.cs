﻿namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    using bgTeam.IsRui.Domain.Entities.Directories;

    internal class SpecialistCategoryTypeMap : ClassMapHiLo<SpecialistCategoryType>
    {
        public SpecialistCategoryTypeMap()
            : base("SpecialistCategoryType")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "SCT_Name").Nullable();
        }
    }
}
