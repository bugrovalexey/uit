﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class FuncTypeMap : ClassMapBase<FuncType>
    {
        public FuncTypeMap()
            : base("Func_Type")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Func_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_Func_Type_Insert
            //						  @Activity_Level_Name = ?
            //						 ,@Func_Type_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Func_Type_Update
            //						  @Activity_Level_Name = ?
            //						 ,@Func_Type_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Func_Type_Delete
            //						  @Func_Type_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Func_Type_Name").Nullable();
        }
    }
}