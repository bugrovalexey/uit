﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class TypingIndicatorGovFunctionMap : ClassMapBase<TypingIndicatorGovFunction>
    {
        public TypingIndicatorGovFunctionMap() :
            base("Typical_Indicator_Gov_Function")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Typical_Indicator_Gov_Function_Name");
            Map(x => x.Algoritm, "Typical_Indicator_Gov_Function_Algoritm");

            HasManyToMany(x => x.ActivityTypes)
                .Table("Typical_Indicator_Type")
                .ParentKeyColumn("Typical_Indicator_Gov_Function_ID")
                .ChildKeyColumn("Plans_Activity_Type2_ID")
                .ReadOnly();

            HasManyToMany(x => x.IKTComponents)
                .Table("Typical_Indicator_IKT_Component")
                .ParentKeyColumn("Typical_Indicator_Gov_Function_ID")
                .ChildKeyColumn("IKT_Component_ID")
                .ReadOnly();
        }
    }
}