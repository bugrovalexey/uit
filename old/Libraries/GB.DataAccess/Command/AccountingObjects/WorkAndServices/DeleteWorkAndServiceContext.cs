﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.WorkAndServices
{
    public class DeleteWorkAndServiceContext : ICommandContext
    {
        public int? xId { get; set; }
    }
}
