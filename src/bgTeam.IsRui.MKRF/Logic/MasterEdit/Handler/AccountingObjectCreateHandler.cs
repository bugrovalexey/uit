﻿namespace bgTeam.IsRui.MKRF.Logic.MasterEdit.Handler
{
    class AccountingObjectCreateHandler : MasterEditBaseHandler
    {
        protected override MasterInItem[] GetListPage()
        {
            return new MasterInItem[] 
            {
                new MasterInItem(MasterItemEnum.None),
                new MasterInItem(MasterItemEnum.ResponsiblePurchase),
                new MasterInItem(MasterItemEnum.GovernmentContract)
                {
                    Title = "Укажите сведения о заключенных государственных контрактах",
                },
                new MasterInItem(MasterItemEnum.ActsOfAcceptance)
                {
                    Title = "Заполните сведения о результатах исполнения государственных контрактов"
                },
                new MasterInItem(MasterItemEnum.Software)
                {
                    Title = "Заполните перечень программного обеспечения" +
                            " необходимого для функционирования объекта учета",
                },
                new MasterInItem(MasterItemEnum.TechnicalSupport)
                {
                    Title = "Заполните перечень технического " +
                            "оборудования необходимого для функционирования объекта учета",
                },
                new MasterInItem(MasterItemEnum.WorkAndService)
                {
                    Title = "Заполните перечень работ",
                },
                new MasterInItem(MasterItemEnum.FactualLocation),
                new MasterInItem(MasterItemEnum.CardForRead)
            };
        }
    }   
}
