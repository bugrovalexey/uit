﻿using GB.Data.AccountingObjects;
using GB.Data.Documents;
using GB.DataAccess.Command.AccountingObjects.ActsToAOActivities;
using GB.DataAccess.Command.Documents;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.ActsOfAcceptance
{
    public class AddOrSaveActOfAcceptanceCommand : ICommand<AddOrSaveActOfAcceptanceContext, ActOfAcceptance>
    {
        ISessionFactoryGb factory;

        public AddOrSaveActOfAcceptanceCommand()
        {
            factory = new SessionFactory();
        }

        public ActOfAcceptance Execute(AddOrSaveActOfAcceptanceContext context)
        {
            ActOfAcceptance act;

            if (context.xId.HasValue && context.xId != 0)
                act = new Repository<ActOfAcceptance>().Get(context.xId.Value);
            else
            {
                act = new ActOfAcceptance();                
            }

            AutoMapper.Mapper.Map(context, act);
            
            using (var session = factory.Create())
            {
                session.SaveOrUpdate(act);
                session.Commit();
            }

            if (!context.xId.HasValue || context.xId == 0)
            {                
                new UpdateActToAOActivityCommand().Execute(new UpdateActToAOActivityContext()
                {
                    ActId = act.Id,
                    ActToActivityIds = context.ActToActivityIds                        
                });                
            }

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = context.Files,
                listDocs = act.Documents,
                owner = act,
                type = DocTypeEnum.ActOfAcceptance
            });
            
            using (var session = factory.Create())
            {
                session.Refresh(act);
                session.Commit();
            }

            return act;
        }        
    }
}
