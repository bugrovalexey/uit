﻿using GB.Data.Plans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.ActsOfAcceptance
{
    public class AddOrSaveActOfAcceptanceContext : ICommandContext
    {
        public AddOrSaveActOfAcceptanceContext()
        {            
            ActToActivityIds = new List<int>();
        }
        
        public string DocName { get; set; }
        
        public string DocNumber { get; set; }
        
        public DateTime? DocDateAdoption { get; set; }
        
        public string Files { get; set; }
        
        public decimal Cost { get; set; }
        
        public int GovContractId { get; set; }
        
        public ActivityTypeEnum? ActivityType { get; set; }
        
        public string ReasonsWaitingResultsDeviation { get; set; }

        public List<int> ActToActivityIds { get; set; }

        public int? xId { get; set; }

        //public int AccountingObjectId { get; set; }
    }
}
