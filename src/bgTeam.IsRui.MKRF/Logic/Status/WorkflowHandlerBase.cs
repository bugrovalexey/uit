﻿using bgTeam.IsRui.Domain.Entities;

namespace bgTeam.IsRui.MKRF.Logic.Status
{
    public abstract class WorkflowHandlerBase<T> : IWorkflowHandler where T : IWorkflowObject
    {
        protected abstract bool CheckNewStatus(T @object, bgTeam.IsRui.Domain.Entities.Workflow.Status status, out string errorMessage, out string confirmMessage);

        bool IWorkflowHandler.ReadyForNewStatus(IWorkflowObject @object, bgTeam.IsRui.Domain.Entities.Workflow.Status status, out string errorMessage, out string confirmMessage)
        {
            return CheckNewStatus((T)@object, status, out errorMessage, out confirmMessage);
        }
        
        public abstract void ChangeAfter(T obj);

        public void ChangeAfter(IWorkflowObject obj)
        {
            ChangeAfter((T)obj);
        }
    }
}