﻿using System;
using System.Collections.Generic;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    /// <summary>
    /// Госконтрак (ГК)
    /// </summary>
    public class GovContract : EntityBase
    {
        public GovContract() { }

        public GovContract(AccountingObject accountingObject)
        {
            this.AccountingObject = accountingObject;
        }

        /// <summary>
        /// ОУ
        /// </summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Реестровый номер
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        /// Дата заключения
        /// </summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Статус госконтракта
        /// </summary>
        public virtual GovContractStatusEnum Status { get; set; }

        /// <summary>
        /// Исполнитель 
        /// </summary>
        public virtual string Executer { get; set; }

        /// <summary>
        /// Стоимость 
        /// </summary>
        public virtual decimal Price { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public virtual string Currency { get; set; }

        /// <summary>
        /// Ссылка на сайт госзакупок
        /// </summary>
        public virtual string UrlOnZakupki { get; set; }

        /// <summary>
        /// Закупка (поле состоит из реестрового номера закупки, в рамках которой заключен ГК.)
        /// </summary>
        public virtual string Order { get; set; }

        /// <summary>
        /// Причины отклонения от ожидаемых результатов
        /// </summary>
        public virtual string ReasonsCostDeviation { get; set; }

        /// <summary>
        /// Id из системы - закупки 360
        /// </summary>
        public virtual int IdZakupki360 { get; set; }

        /// <summary>
        /// Определяет на какой стадии был добавлен
        /// </summary>
        public virtual AccountingObjectsStageEnum Stage { get; set; }

        public virtual IList<ActOfAcceptance> Acts { get; protected set; }
    }
}
