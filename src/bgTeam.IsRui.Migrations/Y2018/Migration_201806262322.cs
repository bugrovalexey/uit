﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201806262323)]
    public class Migration_201806262323 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("ClaimUserComment")
                .WithColumn("ClaimUserComment_ID").AsInt32().NotNullable()
                .WithColumn("ClaimUserComment_Claim_ID").AsInt32().NotNullable()
                .WithColumn("ClaimUserComment_User_ID").AsInt32().NotNullable()
                .WithColumn("ClaimUserComment_Status_ID").AsInt32().Nullable()
                .WithColumn("ClaimUserComment_Comment").AsString(2000).Nullable()
                .WithColumn("ClaimUserComment_CreateDate").AsDateTime().Nullable()
                .WithColumn("ClaimUserComment_TimeStamp").AsDateTime().Nullable();

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "ClaimUserComment" });
        }
    }
}
