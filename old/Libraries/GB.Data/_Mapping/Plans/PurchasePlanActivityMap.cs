﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class PurchasePlanActivityMap : ClassMapBase<PurchasePlanActivity>
    {
        public PurchasePlanActivityMap()
            : base("Plan_Purchase_Work")
        {
//            Table("w_");

//            Id(x => x.Id, "Plan_Purchase_Work_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_Plan_Purchase_Work_Insert
//                          @Plan_Purchase_ID = ?
//                         ,@Plans_Activity_ID = ?
//                         ,@Plans_Work_ID = ?
//                         ,@Plan_Purchase_Work_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Plan_Purchase_Work_Update
//                          @Plan_Purchase_ID = ?
//                         ,@Plans_Activity_ID = ?
//                         ,@Plans_Work_ID = ?
//                         ,@Plan_Purchase_Work_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Plan_Purchase_Work_Delete
//						  @Plan_Purchase_Work_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            References<PurchasePlan>(x => x.PurchasePlan, "Plan_Purchase_ID").Not.Nullable();
            References<PlansActivity>(x => x.PlansActivity, "Plans_Activity_ID").Not.Nullable();
            References<PlansWork>(x => x.PlansWork, "Plans_Work_ID");
        }
    }
}
