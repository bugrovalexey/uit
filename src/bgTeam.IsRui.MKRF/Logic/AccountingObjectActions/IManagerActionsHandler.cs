﻿using System.Collections.Generic;

namespace bgTeam.IsRui.MKRF.Logic.AccountingObjectActions
{
    public interface IManagerActionsHandler
    {
        IEnumerable<ActionDescription> GetActionsList();
    }
}
