﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityDocuments : EntityBase
    {
        public virtual string Name { get; set; }

        public virtual string Number { get; set; }

        public virtual DateTime? DateAdoption { get; set; }

        public virtual string ParticleOfDocument { get; set; }

        public virtual int? FileId { get; set; }

        public virtual string FileName { get; set; }

        public virtual PlansActivity Activity { get; set; }

        public virtual DateTime? ChangeDate { get; set; }
    }
}
