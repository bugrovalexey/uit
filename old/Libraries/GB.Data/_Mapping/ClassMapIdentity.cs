﻿using FluentNHibernate.Mapping;
using GB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB._Mapping
{
    internal abstract class ClassMapIdentity<T> : ClassMap<T> where T : EntityBase
    {
        public ClassMapIdentity(string table)
        {
            Table(table);

            Id(x => x.Id, table+ "_ID").GeneratedBy.Identity();
        }
    }
}
