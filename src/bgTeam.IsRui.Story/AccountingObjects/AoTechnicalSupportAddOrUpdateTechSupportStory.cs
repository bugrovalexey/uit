﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Common.Helpers;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
    using bgTeam.IsRui.Domain.Entities.Directories;

    public class AoTechnicalSupportAddOrUpdateTechSupportStory : IStory<AoTechnicalSupportAddOrUpdateTechSupportStoryContext, TechnicalSupportAddOrUpdateDto>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public AoTechnicalSupportAddOrUpdateTechSupportStory(ISessionNHibernateFactory factory, IRepositoryRui repository, IMapperBase mapper)
        {
            _factory = factory;
            _repository = repository;
            _mapper = mapper;
        }

        public TechnicalSupportAddOrUpdateDto Execute(AoTechnicalSupportAddOrUpdateTechSupportStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<TechnicalSupportAddOrUpdateDto> ExecuteAsync(AoTechnicalSupportAddOrUpdateTechSupportStoryContext context)
        {
            var result = new TechnicalSupportAddOrUpdateDto();

            using (var session = _factory.OpenSession())
            {
                TechnicalSupport technicalSupport;

                if (context.Id.HasValue)
                {
                    technicalSupport = _repository.Get<TechnicalSupport>(context.Id);
                }
                else
                {
                    technicalSupport = new TechnicalSupport();

                    technicalSupport.AccountingObject = new AccountingObject() { Id = context.AoId.Value };

                    technicalSupport.CategoryKindOfSupport = new CategoryKindOfSupport() { Id = context.CategoryKindOfSupportId.Value };
                }

                _mapper.Map(context, technicalSupport);

                if (context.ManufacturerId.HasValue)
                {
                    technicalSupport.Manufacturer = _repository.Get<Manufacturer>(context.ManufacturerId);
                }

                await session.SaveOrUpdateAsync(technicalSupport);

                await SaveCharacteristics(context, technicalSupport);

                _mapper.Map(technicalSupport, result);

                return result;
            }

        }

        private async Task<CharacteristicOfTechnicalSupportGetDto> SaveCharacteristics(AoTechnicalSupportAddOrUpdateTechSupportStoryContext context, TechnicalSupport technicalSupport)
        {
            if (context.Characteristics != null)
            {
                var characteristicStory = new AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStory(_factory, _repository, _mapper);

                foreach (CharacteristicOfTechnicalSupport characteristicNew in context.Characteristics)
                {
                    var characteristicData = _repository.GetExisting<CharacteristicOfTechnicalSupport>(characteristicNew.Id);

                    await characteristicStory.ExecuteAsync(new AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStoryContext()
                    {
                        Id = characteristicNew.Id,
                        Name = characteristicData.Name,
                        TechnicalSupportId = technicalSupport.Id,
                        UnitId = characteristicData.Unit.Id,
                        Value = characteristicData.Value
                    });
                }

                foreach (CharacteristicOfTechnicalSupport characteristicMap in technicalSupport.Characteristics)
                {
                    var characteristicDataMap = _repository.GetExisting<CharacteristicOfTechnicalSupport>(characteristicMap.Id);

                    characteristicMap.TechnicalSupport = characteristicDataMap.TechnicalSupport;
                    characteristicMap.Unit = characteristicDataMap.Unit;
                }

                //var oldCharacteristic = _repository.GetAllEx<CharacteristicOfTechnicalSupport>(
                //    q => q.Where(x => x.TechnicalSupport.Id == technicalSupport.Id));

                    //var characteristicStoryDel = new AoTechnicalSupportDeleteTechSupportCharacteristicStory(_factory);

                    //foreach (CharacteristicOfTechnicalSupport characteristicDel in oldCharacteristic)
                    //{
                    //    if (context.Characteristics.Contains(characteristicDel) == false)
                    //    {
                    //        await characteristicStoryDel.ExecuteAsync(new AoTechnicalSupportDeleteTechSupportCharacteristicStoryContext { Id = characteristicDel.Id });
                    //    }
                    //}
            }

            return null;
        }
    }
}
