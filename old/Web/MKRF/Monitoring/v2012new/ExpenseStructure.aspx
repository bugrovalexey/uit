﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ExpenseStructure.aspx.cs" Inherits="GB.MKRF.Web.Monitoring.v2012new.ExpenseStructure" %>
<%@ Import Namespace="GB.MKRF.Entities.Directories" %>
<%@ Import Namespace="GB.MKRF.Web.Plans" %>

<asp:Content ID="Content2" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function reloadPage() {

            pageUrl = 'ExpenseStructure.aspx';

            var args = [];
            args.push('Year=' + YearsComboBox.GetValue());
           //args.push('DepType=' + OGVTypeComboBox.GetValue());

            window.open(pageUrl + "?" + args.join('&'), '_self');
        }

    </script>
    <table style="width: 100%">
        <tr>
            <td align="center">
                <table style="width: 800px">
                    <tr>
                        <td style="width: 1%; white-space:nowrap">Год плана:</td>
                        <td style="width: 1%">
                            <dxe:ASPxComboBox runat="server" ID="YearsComboBox" Width="80px" ValueType="System.Int32"
                                ValueField="Year" TextField="Year" ClientInstanceName="YearsComboBox" OnDataBinding="YearsComboBox_DataBinding">
                                <ClientSideEvents SelectedIndexChanged="function(s,e) { reloadPage();  }" />
                            </dxe:ASPxComboBox>
                        </td>
                        <td style="width: 1%; white-space:nowrap; padding-left:30px">
                            <%--Тип департамента:--%>
                        </td>
                        <td style="width: 97%;">
                            <%--<dxe:ASPxComboBox runat="server" ID="OGVTypeComboBox" Width="180px" ValueType="System.Int32"
                                ClientInstanceName="OGVTypeComboBox">
                                <ClientSideEvents SelectedIndexChanged="function(s,e) { reloadPage();  }" />
                                <Items>
                                    <dxe:ListEditItem Text="Все" Value="0" />
                                    <dxe:ListEditItem Text="Департаменты" Value="3" />
                                    <dxe:ListEditItem Text="Тер. органы" Value="2" />
                                    <dxe:ListEditItem Text="Подведомственные" Value="1" />
                                </Items>
                            </dxe:ASPxComboBox>--%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br />
                <dxchartsui:WebChartControl ID="Chart" Width="1000px"
                    runat="server" SideBySideEqualBarWidth="True" Height="670px" AppearanceName="In A Fog"
                    PaletteName="Origin" OnCustomDrawSeriesPoint="Chart_CustomDrawSeriesPoint">
	                <seriestemplate>
		                <ViewSerializable>
			                <dxcharts:SideBySideBarSeriesView/>
		                </ViewSerializable>
		                <LabelSerializable>
			                <dxcharts:SideBySideBarSeriesLabel LineVisible="True">
				                <FillStyle>
					                <OptionsSerializable>
						                <dxcharts:SolidFillOptions/>
					                </OptionsSerializable>
				                </FillStyle>
			                </dxcharts:SideBySideBarSeriesLabel>
		                </LabelSerializable>
		                <PointOptionsSerializable>
			                <dxcharts:PointOptions/>
		                </PointOptionsSerializable>
		                <LegendPointOptionsSerializable>
			                <dxcharts:PointOptions/>
		                </LegendPointOptionsSerializable>
	                </seriestemplate>
	                <palettewrappers>
		                <dxchartsui:PaletteWrapper Name="Palette 1" ScaleMode="Repeat">
			                <Palette>
				                <dxcharts:PaletteEntry Color="79, 129, 189" Color2="51, 90, 136"/>
				                <dxcharts:PaletteEntry Color="192, 80, 77" Color2="139, 52, 49"/>
				                <dxcharts:PaletteEntry Color="155, 187, 89" Color2="111, 137, 56"/>
				                <dxcharts:PaletteEntry Color="128, 100, 162" Color2="89, 69, 115"/>
				                <dxcharts:PaletteEntry Color="75, 172, 198" Color2="46, 124, 145"/>
				                <dxcharts:PaletteEntry Color="247, 150, 70" Color2="213, 101, 9"/>
			                </Palette>
		                </dxchartsui:PaletteWrapper>
	                </palettewrappers>
	                <fillstyle>
		                <OptionsSerializable>
			                <dxcharts:SolidFillOptions/>
		                </OptionsSerializable>
	                </fillstyle>
	                <padding bottom="0" left="0" right="0" top="0"/>
	                <legend alignmentvertical="Center" equallyspaceditems="False" font="Tahoma, 10pt"/>
	                <titles>
		                <dxcharts:ChartTitle WordWrap="True" MaximumLinesCount="4" Text="Структура планируемых расходов" Font="Tahoma, 14pt"/>
	                </titles>
	                <borderoptions visible="False" />
	                <backimage imageurl="/Content/Style/Coord/backgrounds/diagram_back.jpg"/>
	                <borderoptions visible="False"/>
	                <diagramserializable>
		                <dxcharts:XYDiagram>
			                <AxisX Title-Text="Год" VisibleInPanesSerializable="-1">
				                <Tickmarks Visible="False" MinorVisible="False"/>
				                <Label Staggered="False" Antialiasing="True"/>
				                <Range SideMarginsEnabled="True"/>
			                </AxisX>

			                <AxisY Title-Text="Объем расходов тыс. руб." Title-Visible="True"  VisibleInPanesSerializable="-1">
				                <Tickmarks MinorVisible="False" CrossAxis="True"/>
				                <Label Antialiasing="True"/>
				                <Range SideMarginsEnabled="True"/>
			                </AxisY>
			                <DefaultPane BackColor="White" EnableAxisXScrolling="False" EnableAxisYScrolling="False" EnableAxisXZooming="False" EnableAxisYZooming="False"/>
		                </dxcharts:XYDiagram>
	                </diagramserializable>
                </dxchartsui:WebChartControl>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br />
                <dxwgv:ASPxGridView runat="server" ID="grid" Width="1000px" OnDataBinding="grid_DataBinding" OnDataBound="grid_DataBound" KeyFieldName="ID">
                     <Settings ShowFilterRow="True" ShowFooter="true" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Наименование департамента" FieldName="OGV">
                            <Settings AutoFilterCondition="Contains" />
                            <DataItemTemplate>
                                <%#string.Format("<a class='gridLink' href='{0}'>{1}</a>", PlanCard.GetUrl((int)Eval("PlanId"), ExpObjectTypeEnum.Plan2012new), Eval("OGV").ToString()) %>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewBandColumn Caption="Основной объём">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="2014" FieldName="Y0" Name="Y0" VisibleIndex="1">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="2015" FieldName="Y1" Name="Y1" VisibleIndex="2">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="2016" FieldName="Y2" Name="Y2" VisibleIndex="3">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                        <dxwgv:GridViewBandColumn Caption="Дополнительный объём">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="2014" FieldName="AddY0" Name="AddY0" VisibleIndex="10">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="2015" FieldName="AddY1" Name="AddY1" VisibleIndex="20">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="2016" FieldName="AddY2" Name="AddY2" VisibleIndex="30">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                    </Columns>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="Итого:" SummaryType="Sum" FieldName="OGV" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y0" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y1" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="Y2" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="AddY0" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="AddY1" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="AddY2" SummaryType="Sum" />
                    </TotalSummary>
                </dxwgv:ASPxGridView>

            </td>
        </tr>
    </table>

</asp:Content>
