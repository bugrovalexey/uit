﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.PlanReport.SectionV;

namespace Uviri.MKRF.Mapping.PlanReport.SectionV
{
    public class UsingISByOthersMap : ClassMapBase<UsingISByOthers>
	{
		public UsingISByOthersMap()
            : base("PRS_5_9")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_5_9_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string args = @" @PRS_5_ID =? 
//							,@Govt_Organ_ID =? 
//							,@PRS_5_9_ID =? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_5_9_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_5_9_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_5_9_Delete
//						  @PRS_5_9_ID = ?").Check.None();

        }

        protected override void InitMap()
        {
            References<SupplyISInfo>(x => x.SupplyISInfo, "PRS_5_ID").Not.Nullable();
			References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
        }
    }
}