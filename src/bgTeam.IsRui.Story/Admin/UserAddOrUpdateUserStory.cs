﻿namespace bgTeam.IsRui.Story.Admin
{
    using bgTeam.IsRui.Common.Helpers;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Users;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UserAddOrUpdateUserStory : IStory<UserAddOrUpdateUserStoryContext, UsersDto>
    {
        private readonly IMapperBase _mapper;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;

        public UserAddOrUpdateUserStory(
            IMapperBase mapper,
            ISessionNHibernateFactory factory,
            IRepositoryRui repository)
        {
            _mapper = mapper;
            _factory = factory;
            _repository = repository;
        }

        public UsersDto Execute(UserAddOrUpdateUserStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<UsersDto> ExecuteAsync(UserAddOrUpdateUserStoryContext context)
        {
            UsersEntity user;

            if (context.Id.HasValue)
            {
                user = _repository.Get<UsersEntity>(context.Id);
            }
            else
            {
                user = new UsersEntity();
            }

            _mapper.Map(context, user);

            if (context.GovtOrganId.HasValue)
            {
                user.GovtOrgan = _repository.Get<Department>(context.GovtOrganId);
            }

            if (context.Password != null)
            {
                user.Password = CryptoHelper.GetMD5Hash(context.Password);
            }

            using (var session = _factory.OpenSession())
            {
                await session.InsertAsync(user);
            }

            return _mapper.Map(user, new UsersDto());

        }
    }
}
