﻿using System;
using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.Plans;
using GB.DataAccess;
using GB.MKRF.Logic.Access.Handlers;

namespace GB.MKRF.Logic.Access
{
    public static class ManagerAccess
    {
        public static bool CanRead<T>(T entity) where T : EntityBase, IWorkflowObject
        {
            return true;
            var handler = GetHandler(entity);
            return handler.CanRead();
        }

        public static bool CanEdit<T>(T entity) where T : EntityBase, IWorkflowObject
        {
            return true;
            var handler = GetHandler(entity);
            return handler.CanEdit();
        }

        public static bool CanCreate<T>(T entity) where T : EntityBase, IWorkflowObject
        {
            return true;
            var handler = GetHandler(entity);
            return handler.CanCreate();
        }

        public static bool CanCreate(EntityType entityType)
        {
            return true;
            return Singleton<AccessEntityProvider>.Instance.CanCreate(entityType);
        }

        public static bool CanDelete<T>(T entity) where T : EntityBase, IWorkflowObject
        {
            return true;
            var handler = GetHandler(entity);
            return handler.CanDelete();
        }


        private static IAccessHandler GetHandler<T>(T entity) where T : EntityBase, IWorkflowObject
        {
            if(entity is AccountingObject)
                return new AccountingObjectAccessHandler(entity as AccountingObject);
            
            if (entity is PlansActivity)
                return new PlansActivityAccessHandler(entity as PlansActivity);

            throw  new ArgumentOutOfRangeException(string.Format("Тип {0} не имеет обработчика прав.", entity.GetType())); 
        }

       

    }
}
