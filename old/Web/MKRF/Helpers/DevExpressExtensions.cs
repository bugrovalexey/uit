﻿using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTreeList;

namespace GB.MKRF.Web.Helpers
{
    public static class DevExpressExtensions
    {
        public static void GridCancelEdit(this ASPxGridView grid, System.ComponentModel.CancelEventArgs e, bool IgnoreBinding = false)
        {
            grid.CancelEdit();
            e.Cancel = true;

            if (IgnoreBinding == false)
                grid.DataBind();
        }

        /// <summary>
        /// Установить фильтровать на все строки
        /// </summary>
        /// <param name="grid">The grid.</param>
        /// <param name="filter">Фильтр.</param>
        public static void SetAutoFilterCondition(this ASPxGridView grid, AutoFilterCondition filter)
        {
            for (int i = 0; i < grid.Columns.Count; i++)
            {
                GridViewDataColumn col = grid.Columns[i] as GridViewDataColumn;

                if (col != null)
                {
                    col.Settings.AutoFilterCondition = filter;
                }
            }
        }

        public static void TreeCancelEdit(this ASPxTreeList tl, System.ComponentModel.CancelEventArgs e)
        {
            tl.CancelEdit();
            e.Cancel = true;

            tl.DataBind();
        }
    }

    public static class DevExpressValidationExtensions
    {
        public const string EM_NOT_NULL = "Данное поле не может быть пустым";

        public static void SetValidation(this ASPxEdit control)
        {
            if (!control.ReadOnly)
            {
                SetValidation(control, EM_NOT_NULL);
            }
        }

        public static void SetValidation(this ASPxEdit control, string errorText)
        {
            control.ValidationSettings.Display = Display.Dynamic;
            control.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.Text;
            control.ValidationSettings.ErrorFrameStyle.CssClass = "ErorField";

            control.ValidationSettings.RequiredField.IsRequired = true;
            control.ValidationSettings.RequiredField.ErrorText = errorText;
        }

        public static void SetValidationExpression(this ASPxEdit control, string regex, string errorText)
        {
            control.ValidationSettings.RegularExpression.ValidationExpression = regex;
            control.ValidationSettings.RegularExpression.ErrorText = errorText;
        }
    }
}