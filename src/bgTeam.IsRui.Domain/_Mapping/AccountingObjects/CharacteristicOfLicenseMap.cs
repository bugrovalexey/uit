﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    internal class CharacteristicOfLicenseMap : ClassMapHiLo<CharacteristicOfLicense>
    {
        public CharacteristicOfLicenseMap()
            : base("License_Charact")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "License_Charact_Name");
            Map(x => x.Model, "License_Charact_Model");
            References(x => x.Software, "OU_Software_ID").Not.Nullable();
            Map(x => x.TypeOfConnectionToServer, "License_Charact_Serv_Connection_Type").CustomType<TypeOfConnectionToServerEnum>();
            Map(x => x.NumberOnlineSameTime, "License_Charact_Connection_Count");
            Map(x => x.ProductNumber, "License_Charact_Num");
            Map(x => x.NumberAllocatedSameTime, "License_Charact_Count");
            Map(x => x.Licensor, "License_Charact_Licensor");
        }
    }
}