﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="RequestsDoc.aspx.cs" Inherits="GB.MKRF.Web.Users.RequestsDoc" %>

<%@ Register src="../Common/DocumentControl.ascx" tagname="DocumentControl" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <div style="text-align: right;"><a href="javascript:history.go(-1);"><< Назад</a></div>
    <uc1:DocumentControl ID="dc" runat="server"/>
</asp:Content>
