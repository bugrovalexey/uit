﻿using System.ComponentModel;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Directories;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    [MetadataType(typeof(CharacteristicsEditViewModel_Valid))]
    public class CharacteristicsEditViewModel : ActivitiesCardBase
    {
        private AccountingObjectCharacteristics _Characteristics;
        private AccountingObjectCharacteristicsValue _CharacteristicsValue;

        public int? CharacteristicsId { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Тип")]
        public int? Type { get; set; }

        [DisplayName("Единица измерения")]
        public int? Unit { get; set; }

        [DisplayName("Фактическое значение")]
        public int? Fact { get; set; }

        [DisplayName("Номинальное значение")]
        public int? Norm { get; set; }

        [DisplayName("Максимальное значение")]
        public int? Max { get; set; }

        public bool IsRegister { get; set; }

        public int AccountingObjectId { get; set; }

        public System.Web.Mvc.SelectList TypesList { get; set; }

        public System.Web.Mvc.SelectList UnitsList { get; set; }

        //public bool ReadOnly { get; set; }

        public CharacteristicsEditViewModel(IStatusRepository statusRepository, int activityId, int? id)
            : base(statusRepository, activityId)
        {
            _Characteristics = new RepositoryDc<AccountingObjectCharacteristics>().Get(id);

            AccountingObjectId = CurrentActivity.AccountingObject.Id;

            if (_Characteristics != null)
            {
                CharacteristicsId = _Characteristics.Id;
                Name = _Characteristics.Name;
                Type = _Characteristics.Type.Id;
                Unit = _Characteristics.Unit.Id;
                Fact = _Characteristics.Fact;
                IsRegister = _Characteristics.IsRegister;

                _CharacteristicsValue = new RepositoryDc<AccountingObjectCharacteristicsValue>()
                    .GetAllEx(q => q.Where(x => x.Activity == CurrentActivity && x.Characteristics == _Characteristics))
                    .FirstOrDefault();

                if (_CharacteristicsValue != null)
                {
                    Norm = _CharacteristicsValue.Norm;
                    Max = _CharacteristicsValue.Max;
                }
            }
            TypesList = new System.Web.Mvc.SelectList(new RepositoryDc<AccountingObjectCharacteristicsType>().GetAll(), "Id", "Name");
            UnitsList = new System.Web.Mvc.SelectList(new RepositoryDc<Units>().GetAll(), "Id", "Name");
        }
    }

    public class CharacteristicsArgs : IAccountingObjectCharacteristicsValueArgs, IAccountingObjectCharacteristicsActivityArgs
    {
        public int ActivityId { get; set; }

        public int? CharacteristicsId { get; set; }

        public int? Norm { get; set; }

        public int? Max { get; set; }

        public string Name { get; set; }

        public int? Type { get; set; }

        public int? Unit { get; set; }

        public bool IsRegister { get; set; }

        public int AccountingObjectId { get; set; }
    }

    public class CharacteristicsEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public object Name { get; set; }

        [Required(ErrorMessage = RequiredText)]
        [RangeAttribute(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        public object Type { get; set; }

        [Required(ErrorMessage = RequiredText)]
        [RangeAttribute(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        public object Unit { get; set; }

        [RangeAttribute(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public object Norm { get; set; }

        [RangeAttribute(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public object Max { get; set; }
    }
}