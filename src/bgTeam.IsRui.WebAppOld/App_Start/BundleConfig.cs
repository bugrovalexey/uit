﻿using System.Web.Optimization;

namespace bgTeam.IsRui.WebApp
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Нужно чтобы можно было грузить min файлы.
            //TODO: убрать отсюда min, добавить не min, а min будут подгружаться когда не Debug, (EnableOptimizations) 
            //см http://www.asp.net/mvc/overview/performance/bundling-and-minification
            bundles.IgnoreList.Clear();

            BundleTable.EnableOptimizations = false;

            bundles.Add(new ScriptBundle("~/Scripts/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));            

            bundles.Add(new ScriptBundle("~/Scripts/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/Scripts/jquery.validate").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/Scripts/knockout").Include(
                        "~/Scripts/knockout-{version}.js"));

            bundles.Add(new ScriptBundle("~/Scripts/CLEditor").Include(
            "~/Scripts/jquery.cleditor.js"));

            bundles.Add(new ScriptBundle("~/Scripts/All").Include(
                        "~/Scripts/All/jquery.cookie.js",
                        "~/Scripts/All/jquery.validate.min.js",
                        "~/Scripts/All/jquery.validate.unobtrusive.min.js",
                        "~/Scripts/All/jquery-ui-1.10.0.custom.min.js",
                        "~/Scripts/All/jquery-ui.js",
                        "~/Scripts/All/jquery.mousewheel.js",
                        "~/Scripts/All/jquery.maskedinput.min.js",
                        "~/Scripts/All/test/jquery.number.js",
                        "~/Scripts/All/jquery.unobtrusive-ajax.min.js",
                        "~/Scripts/All/jquery.dataTables.min.js",
                        "~/Scripts/All/jquery.json-2.3.js",
                        "~/Scripts/All/jquery.debounce-1.0.5.js",
                        "~/Scripts/All/scripts.mvc.js",
                        "~/Scripts/All/chosen.jquery.min.js",
                        "~/Scripts/All/jquery.treetable.js",
                        "~/Scripts/All/autoNumeric/autoNumeric-1.9.25.js"
                        ));


            bundles.Add(new ScriptBundle("~/Scripts/New").Include(
                "~/static/js/main.js"));
#if !DEBUG
            bundles.Add(new ScriptBundle("~/Scripts/gb").Include(
                        "~/Scripts/gb.global.min.js",
                        "~/Scripts/gb.fileuploader.min.js",
                        "~/Scripts/gb.popup.mvc.min.js",
                        "~/Scripts/gb.notify.min.js",
                        "~/Scripts/gb.dataTable.min.js",
                        "~/Scripts/gb.validate.min.js"));
#else
            bundles.Add(new ScriptBundle("~/Scripts/gb").Include(
                        "~/Scripts/gb.global.js",
                        "~/Scripts/gb.fileuploader.js",
                        "~/Scripts/gb.popup.mvc.js",
                        "~/Scripts/gb.notify.js",
                        "~/Scripts/gb.dataTable.js",
                        "~/Scripts/gb.validate.js"));
#endif

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/Scripts/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/style.css",
                "~/Content/menu.css",
                "~/Content/popup.css",
                "~/Content/jquery.cleditor.css"));

            bundles.Add(new StyleBundle("~/Content/All").Include(
                "~/content/All/Main/form_edit.mvc2.css",
                "~/Content/All/jquery-ui.css",
                "~/Content/All/ms/jquery.dataTables.min.css",
                "~/Content/All/fileuploader.css",
                "~/Content/All/Chosen/chosen.min.css",
                "~/Content/All/TreeTable/jquery.treetable.css",
                "~/Content/All/TreeTable/jquery.treetable.theme.default.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new ScriptBundle("~/Content/New").Include(
                "~/static/css/main.css"));
        }
    }
}