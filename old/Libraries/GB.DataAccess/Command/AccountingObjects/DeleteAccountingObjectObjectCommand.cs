﻿using GB.Data.AccountingObjects;
using GB.Data.Workflow;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class DeleteAccountingObjectObjectCommand : ICommand<DeleteAccountingObjectObjectContext>
    {
        ISessionFactoryGb factory;

        public DeleteAccountingObjectObjectCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteAccountingObjectObjectContext context)
        {
            var ao = new Repository<AccountingObject>().Get(context.Id);

            //Удаляем ОУ - который создается организациями
            if(ao.Mode == AccountingObjectEnum.One)
            {
                using (var session = factory.Create())
                {
                    session.Delete(ao);
                    session.Commit();
                }
            }

            //Удаляем ОУ - который создается автоматически
            if (ao.Mode == AccountingObjectEnum.Multi)
            {
                var list = new Repository<AccountingObject>().GetAllEx(q => q.Where(x => x.Parent == ao));

                foreach (var item in list)
                {
                    item.Parent = null;
                    item.Status = new Repository<Status>().Get((int)StatusEnum.AoOnCompletion);
                }

                using (var session = factory.Create())
                {
                    session.Update(list);
                    session.Delete(ao);
                    session.Commit();
                }
            }
        }
    }
}
