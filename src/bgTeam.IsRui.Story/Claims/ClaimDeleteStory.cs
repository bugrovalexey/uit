﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimDeleteStory : ClaimDeleteBaseStory<Claim, ClaimDeleteStoryContext>
    {
        private readonly IFileProviderRui _fileProviderRui;

        public ClaimDeleteStory(
            IRepositoryRui repositoryRui,
            ISessionNHibernateFactory sessionNHibernateFactory,
            IFileProviderRui fileProviderRui)
            : base(repositoryRui, sessionNHibernateFactory)
        {
            _fileProviderRui = fileProviderRui;
        }

        protected override async Task DeleteReferenceAsync(Claim claim)
        {
            var claimActs = _repository
                .GetAllEx<ClaimAct>(q => q.Where(x => x.Claim.Id == claim.Id));
            var claimConclusion = _repository
                .GetAllEx<ClaimConclusion>(q => q.Where(x => x.Claim.Id == claim.Id));
            var claimDocument = _repository
                .GetAllEx<ClaimDocument>(q => q.Where(x => x.Claim.Id == claim.Id));
            var claimIndicator = _repository
                .GetAllEx<ClaimIndicator>(q => q.Where(x => x.Claim.Id == claim.Id));
            var claimMark = _repository
                .GetAllEx<ClaimMark>(q => q.Where(x => x.Claim.Id == claim.Id));
            var claimScopeOfWork = _repository
                .GetAllEx<ClaimScopeOfWork>(q => q.Where(x => x.Claim.Id == claim.Id));

            var deleteStoryClaimAct = new ClaimDeleteActStory(_repository, _factory, _fileProviderRui);
            var deleteStoryConclusion = new ClaimDeleteBaseStory<ClaimConclusion, ClaimDeleteBaseStoryContext>(_repository, _factory);
            var deleteStoryClaimDocument = new ClaimDeleteDocumentStory(_repository, _factory, _fileProviderRui);
            var deleteStoryClaimIndicator = new ClaimDeleteIndicatorStory(_repository, _factory);
            var deleteStoryClaimMark = new ClaimDeleteMarkStory(_repository, _factory);
            var deleteStoryClaimScopeOfWork = new ClaimDeleteBaseStory<ClaimScopeOfWork, ClaimDeleteBaseStoryContext>(_repository, _factory);

            foreach (var entity in claimActs)
            {
                await deleteStoryClaimAct.ExecuteAsync(new ClaimDeleteActStoryContext { Id = entity.Id });
            }

            foreach (var entity in claimConclusion)
            {
                await deleteStoryConclusion.ExecuteAsync(new ClaimDeleteBaseStoryContext { Id = entity.Id });
            }

            foreach (var entity in claimDocument)
            {
                await deleteStoryClaimDocument.ExecuteAsync(new ClaimDeleteDocumentStoryContext { Id = entity.Id });
            }

            foreach (var entity in claimIndicator)
            {
                await deleteStoryClaimIndicator.ExecuteAsync(new ClaimDeleteIndicatorStoryContext { Id = entity.Id });
            }

            foreach (var entity in claimMark)
            {
                await deleteStoryClaimMark.ExecuteAsync(new ClaimDeleteMarkStoryContext { Id = entity.Id });
            }

            foreach (var entity in claimScopeOfWork)
            {
                await deleteStoryClaimScopeOfWork.ExecuteAsync(new ClaimDeleteBaseStoryContext { Id = entity.Id });
            }
        }
    }
}
