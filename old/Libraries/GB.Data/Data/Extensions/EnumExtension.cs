﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.AccountingObjects;
using GB.Data.Plans;
using GB.Extentions;

namespace GB.Data.Extensions
{
    public static class EnumExtension
    {
        public static List<ActivityTypeEnum> ToActivityTypeEnum(this AccountingObjectsStageEnum stageEnum)
        {
            switch (stageEnum)
            {
                case AccountingObjectsStageEnum.Planned:
                    return new List<ActivityTypeEnum> {ActivityTypeEnum.Create};
                case AccountingObjectsStageEnum.Creation:
                    return new List<ActivityTypeEnum> {ActivityTypeEnum.Create};
                case AccountingObjectsStageEnum.Exploitation:
                    return new List<ActivityTypeEnum> {ActivityTypeEnum.Develop, ActivityTypeEnum.Exploitation};
                case AccountingObjectsStageEnum.OutExploitation:
                    return new List<ActivityTypeEnum> {ActivityTypeEnum.Exploitation};
            }

            throw new ArgumentOutOfRangeException(string.Format("Стадия {0} не имеет соответсвия типа.",
                stageEnum.GetDescription()));
        }


        public static List<AccountingObjectsStageEnum> ToAccountingObjectsStageEnum(this ActivityTypeEnum typeEnum)
        {
            switch (typeEnum)
            {
                case ActivityTypeEnum.Create:
                    return new List<AccountingObjectsStageEnum> { AccountingObjectsStageEnum.Planned, AccountingObjectsStageEnum.Creation };
                case ActivityTypeEnum.Develop:
                    return new List<AccountingObjectsStageEnum> { AccountingObjectsStageEnum.Exploitation };
                case ActivityTypeEnum.Exploitation:
                    return new List<AccountingObjectsStageEnum> { AccountingObjectsStageEnum.Exploitation, AccountingObjectsStageEnum.OutExploitation };
            }

            throw new ArgumentOutOfRangeException(string.Format("Тип {0} не имеет соответсвия стадии.",
                typeEnum.GetDescription()));
        }

    }
}
