﻿namespace bgTeam.IsRui.Domain.Entities.Claims
{
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;

    /// <summary>
    /// Объём работ
    /// </summary>
    public class ClaimScopeOfWork : EntityBase
    {
        public virtual Claim Claim { get; set; }

        /// <summary>
        /// Тип работы
        /// </summary>
        public virtual string WorkType { get; set; }

        /// <summary>
        /// Количество запланированных часов
        /// </summary>
        public virtual int WorkHours { get; set; }

        /// <summary>
        /// Описание работ
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Дата начала работ
        /// </summary>
        public virtual DateTime? DateBegin { get; set; }

        /// <summary>
        /// Плановая дата окончания
        /// </summary>
        public virtual DateTime? DatePlanEnd { get; set; }

        /// <summary>
        /// Трудозатраты, чел/часов
        /// </summary>
        public virtual int LaborCostsHours { get; set; }

        /// <summary>
        /// Категория специалиста
        /// </summary>
        public virtual SpecialistCategoryType Specialist { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public virtual decimal Cost { get; set; }
    }
}
