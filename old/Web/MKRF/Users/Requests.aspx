﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Documentation/Documentation.master" AutoEventWireup="true"
    CodeBehind="Requests.aspx.cs" Inherits="GB.MKRF.Web.Users.Requests" %>

<%@ Register Src="~/Common/GridColumnManager.ascx" TagName="GridColumnManager" TagPrefix="gcm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function hfSave(e) {
            hf.Set('Surname', SurnameTextBox.GetText());
            hf.Set('Name', NameTextBox.GetText());
            hf.Set('Middlename', MiddlenameTextBox.GetText());
            hf.Set('PhoneWork', PhoneWorkTextBox.GetValue());
            hf.Set('PhoneMob', PhoneMobTextBox.GetValue());
            hf.Set('Email', EmailTextBox.GetText());
            hf.Set('Job', JobTextBox.GetText());
            hf.Set('Comment', CommentTextBox.GetText());
            /*hf.Set('Department', DepartmentComboBox.GetValue());*/
        }
    </script>
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
    <br />
    <table width="100%">
        <tr>
            <td>
                <dxe:ASPxButton ID="addButton" runat="server" ClientInstanceName="addButton" AutoPostBack="false"
                    Text="Добавить" Width="80px" Style="margin-top: 10px; margin-left: 10px; margin-bottom: 10px;">
                    <ClientSideEvents Click="function(s,e) { grid.AddNewRow(); }" />
                </dxe:ASPxButton>
            </td>
            <td width="100%">
                <dxe:ASPxButton runat="server" ID="ToExcelButton" Text="Выгрузить в Excel" OnClick="ToExcelButton_Click" />
            </td>
            <td>
                <gcm:GridColumnManager ID="GridColumnManager1" runat="server" GridName="grid" />
            </td>
        </tr>
    </table>
    <dxwgve:ASPxGridViewExporter runat="server" ID="ToExcelExporter" GridViewID="grid" />
    <dxwgv:ASPxGridView runat="server" ID="grid" ClientInstanceName="grid" AutoGenerateColumns="False"
        Width="100%" KeyFieldName="Id" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
        OnInitNewRow="grid_InitNewRow" OnRowDeleting="grid_RowDeleting"
        OnRowUpdating="grid_RowUpdating" OnStartRowEditing="grid_StartRowEditing" OnRowInserting="grid_RowInserting"
        OnCommandButtonInitialize="grid_CommandButtonInitialize">
        <Columns>
            <dxwgv:GridViewCommandColumn VisibleIndex="0" Caption=" ">
                <EditButton Visible="true" />
                <DeleteButton Visible="true" />
                <ClearFilterButton Visible="true" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataCheckColumn VisibleIndex="1" Caption="Добавлен" FieldName="IsComplete"
                Visible="false" ShowInCustomizationForm="false">
                <PropertiesCheckEdit DisplayTextChecked="Да" DisplayTextUnchecked="Нет">
                </PropertiesCheckEdit>
            </dxwgv:GridViewDataCheckColumn>
            <dxwgv:GridViewDataDateColumn VisibleIndex="2" Caption="Дата заполнения" FieldName="CompletionDate">
            </dxwgv:GridViewDataDateColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="3" Caption="Фамилия" FieldName="Surname">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="4" Caption="Имя" FieldName="Name">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="5" Caption="Отчество" FieldName="Middlename">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="6" Caption="ОГВ" FieldName="DepartmentNameCode">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="6" Caption="Код ОГВ" FieldName="DepartmentCode" Visible="false">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="6" Caption="ОГВ (краткое)" FieldName="DepartmentNameSmall" Visible="false">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="6" Caption="Куратор" FieldName="Curator" Visible="false">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="7" Caption="Должность" FieldName="Job">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="8" Caption="Раб. тел." FieldName="PhoneWork">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="9" Caption="Факс" FieldName="PhoneMob">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="10" Caption="Email" FieldName="Email">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="11" Caption="Примечание" FieldName="Comment">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="12" Caption="Исполнитель" FieldName="Author" ShowInCustomizationForm="True">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="13" Caption=" " ReadOnly="true" ShowInCustomizationForm="True"
                Width="100px">
                <Settings AllowDragDrop="False" />
                <DataItemTemplate>
                    <%#getDocLink((int)Eval("Id"))%>
                </DataItemTemplate>
                <EditItemTemplate>
                </EditItemTemplate>
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Settings ShowFilterRow="True" ShowHeaderFilterBlankItems="False" ShowFooter="True" />
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsPager Mode="ShowAllRecords" PageSize="10000" />
        <SettingsEditing EditFormColumnCount="1" />
        <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
        <Templates>
            <EditForm>
                <table style="margin: 10px">
                    <tr>
                        <td class="userCapt">Фамилия
                        </td>
                        <td>
                            <dxe:ASPxTextBox runat="server" ID="SurnameTextBox" ClientInstanceName="SurnameTextBox"
                                OnDataBinding="TextBox_DataBinding" Width="500px" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="userCapt">Имя
                        </td>
                        <td>
                            <dxe:ASPxTextBox runat="server" ID="NameTextBox" ClientInstanceName="NameTextBox"
                                OnDataBinding="TextBox_DataBinding" Width="500px" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="userCapt">Отчество
                        </td>
                        <td>
                            <dxe:ASPxTextBox runat="server" ID="MiddlenameTextBox" ClientInstanceName="MiddlenameTextBox"
                                OnDataBinding="TextBox_DataBinding" Width="500px" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <%--                    <tr>
                        <td class="userCapt">
                            ОГВ
                        </td>
                        <td>
                            <dxe:ASPxComboBox runat="server" ID="DepartmentComboBox" ValueField="Id" TextField="Name"
                                Width="500px" ClientInstanceName="DepartmentComboBox" ValueType="System.Int32"
                                OnDataBinding="ComboBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="userCapt">Должность
                        </td>
                        <td>
                            <dxe:ASPxTextBox runat="server" ID="JobTextBox" ClientInstanceName="JobTextBox" OnDataBinding="TextBox_DataBinding"
                                Width="500px" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="userCapt">Раб. тел.
                        </td>
                        <td>
                            <dxe:ASPxTextBox runat="server" ID="PhoneWorkTextBox" ClientInstanceName="PhoneWorkTextBox"
                                OnDataBinding="TextBox_DataBinding" Width="500px" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <MaskSettings Mask="+0 (000) 000-00-00" IncludeLiterals="None" />
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="userCapt">Факс
                        </td>
                        <td>
                            <dxe:ASPxTextBox runat="server" ID="PhoneMobTextBox" ClientInstanceName="PhoneMobTextBox"
                                OnDataBinding="TextBox_DataBinding" Width="500px">
                                <MaskSettings Mask="+0 (000) 000-00-00" IncludeLiterals="None"/>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="userCapt">Email
                        </td>
                        <td>
                            <dxe:ASPxTextBox runat="server" ID="EmailTextBox" ClientInstanceName="EmailTextBox"
                                OnDataBinding="TextBox_DataBinding" Width="500px" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true">
                                    <RegularExpression ErrorText="Неправильный e-mail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="userCapt">Примечание
                        </td>
                        <td>
                            <dxe:ASPxTextBox runat="server" ID="CommentTextBox" ClientInstanceName="CommentTextBox"
                                OnDataBinding="TextBox_DataBinding" Width="500px">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table>
                                <td class="form_edit_buttons" >
                                    <span class="cancel">
                                        <dxwgv:ASPxGridViewTemplateReplacement ID="ASPxGridViewTemplateReplacement1" runat="server"
                                            ReplacementType="EditFormUpdateButton"></dxwgv:ASPxGridViewTemplateReplacement>
                                    </span>
                                </td>
                                <td  class="form_edit_buttons" >
                                    <span class="cancel">
                                        <dxwgv:ASPxGridViewTemplateReplacement ID="ASPxGridViewTemplateReplacement2" runat="server"
                                            ReplacementType="EditFormCancelButton"></dxwgv:ASPxGridViewTemplateReplacement>
                                    </span>
                                </td>
                            </table>
                        y</td>
                    </tr>
                </table>
                </td> </tr> </table>
            </EditForm>
        </Templates>
        <TotalSummary>
            <dxwgv:ASPxSummaryItem SummaryType="Count" FieldName="DepartmentNameCode"
                ShowInColumn="DepartmentNameCode" DisplayFormat="Количество ОГВ: {0}"></dxwgv:ASPxSummaryItem>
        </TotalSummary>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') hfSave(e); }" />
    </dxwgv:ASPxGridView>
</asp:Content>
