﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans.ActivityOffers
{
    public class ActivityOfferApf : EntityBase
    {
        /// <summary>ФАП</summary>
        public virtual ApfInfo APF { get; set; }

        /// <summary>Обоснование невозможности использования</summary>
        public virtual string Usage { get; set; }//null

        /// <summary>
        /// Номер системы во ФГИС
        /// </summary>
        public virtual string FGIS_Number { get; set; }

        /// <summary>
        /// Мероприятие плана
        /// </summary>
        public virtual ActivityOffer Offer { get; set; }
    }
}
