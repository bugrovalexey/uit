﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201803041925)]
    public class Migration_201803041925 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Plans_Activity_Mark").AddColumn("Plans_Activity_Mark_AlgorithmOfCalculating").AsString().Nullable();
        }
    }
}
