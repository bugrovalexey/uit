﻿namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Documents;

     internal class SpecialCheckMap : ClassMapHiLo<SpecialCheck>
    {
        public SpecialCheckMap()
            : base("OU_Special_Check")
        {
        }

        protected override void InitMap()
        {
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();

            Map(x => x.Name, "OU_Special_Check_Name").Not.Nullable();

            HasMany(x => x.Documents).KeyColumns.Add("EntityOwnerId")
                .Where("EntityType = " + (int)EntityType.SpecialCheck)
                .Where("Type = " + (int)DocTypeEnum.SpecialCheck)
                .Cascade.Delete().Inverse();
        }
    }
}
