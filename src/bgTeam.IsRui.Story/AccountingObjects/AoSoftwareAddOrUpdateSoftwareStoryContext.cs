﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;

    public class AoSoftwareAddOrUpdateSoftwareStoryContext
    {
        /// <summary>
        /// Ид
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// ОУ
        /// </summary>
        public int? AoId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Классификационная категория
        /// </summary>
        public int? CategoryKindOfSupportId { get; set; }

        /// <summary>
        /// Id - Производителя
        /// </summary>
        public int? ManufacturerId { get; set; }

        /// <summary>
        /// Производитель
        /// </summary>
        public string ManufacturerCustomName { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int? Amount { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public decimal? Summa { get; set; }

        /// <summary>
        /// Права на ПО
        /// </summary>
        public RightsOnSoftwareEnum Right { get; set; }

        /// <summary>
        /// Характеристики лицензи
        /// </summary>
        public int? CharacteristicOfLicenseId { get; set; }
        public string CharacteristicOfLicenseName { get; set; }

        public string CharacteristicOfLicenseModel { get; set; }

        public TypeOfConnectionToServerEnum? CharacteristicOfLicenseTypeOfConnectionToServer { get; set; }

        public int CharacteristicOfLicenseNumberOnlineSameTime { get; set; }

        public string CharacteristicOfLicenseProductNumber { get; set; }

        public int CharacteristicOfLicenseNumberAllocatedSameTime { get; set; }

        public string CharacteristicOfLicenseLicensor { get; set; }
    }
}
