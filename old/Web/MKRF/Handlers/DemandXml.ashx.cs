﻿using System.Web;
using System;
using Uviri.Coord.Entities.Plans;
using Uviri.Repository;
using Uviri.Coord.Entities.Admin;
using Uviri.Helpers;
using Uviri.Coord.Repository;
using System.Xml;
using Uviri.Coord.Entities.OtherExpObjects;
namespace Uviri.Coord.Web.Handlers
{
    public class DemandXml : IHttpHandler
    {
        public static string GetUrl(EntityBase Demand)
        {
            string url = UrlHelper.Resolve("~/Handlers/DemandXml.ashx");

            if(Demand is ExpDemand)
            {
                url+= "?Demand=";
            }
            else if(Demand is ExpObjectDemand)
            {
                url+= "?ExpObject=";
            }
            else
            {
                return null;
            }

            return url + Demand.Id;
        }
        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                int id;
                XmlDocument xdoc = null;


                bool? isPlan = null;
                string sId = context.Request.QueryString["Demand"];
                if (!string.IsNullOrEmpty(sId))
                {
                    isPlan = true;
                }
                else 
                {
                    sId = context.Request.QueryString["ExpObject"];
                    if (!string.IsNullOrEmpty(sId))
                    {
                        isPlan = false;
                    }
                }

                if (!isPlan.HasValue || !int.TryParse(sId, out id))
                    throw new ArgumentException(context.Request.RawUrl);


                if (isPlan.Value)
                {
                    ExpDemand currentDemand = RepositoryBase<ExpDemand>.Get(id);

                    if (currentDemand == null || !Validator.Validator.Validate(currentDemand, ObjActionEnum.view))
                    {
                        ResponseHelper.DenyAccess(context.Response);
                    }

                    xdoc = StatusRepository.GetPlanXml(currentDemand.ExpObject.Id);

                    context.Response.Clear();
                    context.Response.ContentType = "text/xml";
                    context.Response.Charset = "UTF-8";
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    context.Response.Cache.SetAllowResponseInBrowserHistory(true);
                    xdoc.Save(context.Response.Output);
                    context.Response.End();
                }
                else
                {
                    ExpObjectDemand currentDemand = RepositoryBase<ExpObjectDemand>.Get(id);

                    if (currentDemand == null || !Validator.Validator.Validate(currentDemand, ObjActionEnum.view))
                    {
                        ResponseHelper.DenyAccess(context.Response);
                    }

                    xdoc = StatusRepository.GetExpObjectXml(id);

                    context.Response.Clear();
                    context.Response.ContentType = "text/xml";
                    context.Response.Charset = "UTF-8";
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    context.Response.Cache.SetAllowResponseInBrowserHistory(true);
                    xdoc.Save(context.Response.Output);
                    context.Response.End();
                }
               
            }
            catch (Exception ex)
            {
                string message = "Ошибка при получении XML файла";
                throw new Exception(message, ex);
            }
        }

        #endregion
    }
}
