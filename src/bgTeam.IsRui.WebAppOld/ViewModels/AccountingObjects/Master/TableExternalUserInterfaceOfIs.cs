﻿using System.ComponentModel;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class TableExternalUserInterfaceOfIs : NotificationBaseViewModel
    {
        #region Consts

        public const string NameCaption = "Наименование";
        public const string PurposeCaption = "Назначение";
        public const string TypeCaption = "Тип";
        public const string UrlCaption = "Начальный URL";

        #endregion Consts

        #region Ctors

        public TableExternalUserInterfaceOfIs(ExternalUserInterfaceOfIs entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            Purpose = entity.Purpose;
            Type = entity.TypeOfInterface.Return(t => t.Name, default(string));
            Url = entity.Url.Return(url => LinkBuilder.Link(url, url), default(string));
        }

        #endregion Ctors

        #region Props

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(PurposeCaption)]
        public string Purpose { get; set; }

        [DisplayName(TypeCaption)]
        public string Type { get; set; }

        [DisplayName(UrlCaption)]
        public string Url { get; set; }


        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteExternalUserInterfaceOfIs", "AccountingObjects", new { id = Id }, "GridExternalUserInterfacesOfIs");
            }
        }

        #endregion Props

    }
}