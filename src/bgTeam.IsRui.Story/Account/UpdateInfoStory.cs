﻿namespace bgTeam.IsRui.Story.Account
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UpdateInfoStory : IStory<UpdateInfoStoryContext, UserDto>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IUserRepository _userRepository;
        private readonly IMapperBase _mapper;

        public UpdateInfoStory(ISessionNHibernateFactory factory, IUserRepository userRepository, IMapperBase mapper)
        {
            _factory = factory;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public UserDto Execute(UpdateInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<UserDto> ExecuteAsync(UpdateInfoStoryContext context)
        {
            var user = _userRepository.GetCurrent();

            _mapper.Map(context, user);

            using (var session = _factory.OpenSession())
            {
                await session.UpdateAsync(user);
            }

            return _mapper.Map(user, new UserDto());
        }
    }
}
