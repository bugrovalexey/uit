﻿namespace bgTeam.IsRui.Story.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.Common;

    public class WorkflowUserConclusionGetByIdStory : IStory<WorkflowUserConclusionGetByIdStoryContext, WorkflowUserConclusionDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;

        public WorkflowUserConclusionGetByIdStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IUserRepository userRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _userRepository = userRepository;
        }

        public WorkflowUserConclusionDto Execute(WorkflowUserConclusionGetByIdStoryContext context)
        {
            return ExecuteAsync(context).GetAwaiter().GetResult();
        }

        public async Task<WorkflowUserConclusionDto> ExecuteAsync(WorkflowUserConclusionGetByIdStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            var res = _repository.GetExisting<WorkflowUserConclusion>(context.Id);

            return _mapper.Map(res, new WorkflowUserConclusionDto());
        }
    }
}
