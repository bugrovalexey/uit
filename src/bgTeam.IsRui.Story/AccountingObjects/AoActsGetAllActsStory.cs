﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class AoActsGetAllActsStory : IStory<AoActsGetAllActsStoryContext, IEnumerable<ActsDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;

        public AoActsGetAllActsStory(IMapperBase mapper, IRepositoryRui repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public IEnumerable<ActsDto> Execute(AoActsGetAllActsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActsDto>> ExecuteAsync(AoActsGetAllActsStoryContext context)
        {
            var acts = _repository.GetAllEx<AccountingObjectActs>(
                q => q.Where(x => x.AccountingObject.Id == context.AoId));
            //.Select(x => _mapper.Map(x, new ActsDto()));

            var actsMap = _mapper.Map(acts, new List<ActsDto>());

            foreach (ActsDto act in actsMap)
            {
                var file = _repository.Get<Files>(act.File.Id);

                act.File.Url = file.Url;
                act.File.MimeType = file.MimeType;
                act.File.Kind = file.Kind;
                act.File.Size = file.Size;
                act.File.Extension = file.Extension;
            }

            return actsMap;
        }
    }
}
