﻿namespace bgTeam.IsRui.Story.Activities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Plans;

    public class CommonInfoUpdateInfoStory : IStory<CommonInfoUpdateInfoStoryContext, ActivityCommonInfoDto>
    {
        private readonly IMapperBase _mapper;
        private readonly ISessionNHibernateFactory _session;
        private readonly IRepositoryRui _repository;

        public CommonInfoUpdateInfoStory(IMapperBase mapper, ISessionNHibernateFactory factory, IRepositoryRui repository)
        {
            _mapper = mapper;
            _session = factory;
            _repository = repository;
        }

        public ActivityCommonInfoDto Execute(CommonInfoUpdateInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityCommonInfoDto> ExecuteAsync(CommonInfoUpdateInfoStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var activity = _repository.Get<PlansActivity>(context.ActivityId);

            if (activity == null)
            {
                throw new IsRuiException("Не найдено мероприятие");
            }

            _mapper.Map(context, activity);

            activity.Department = _repository.Get<Department>(context.ResponsibleDepartmentId.Value);

            using (var session = _session.OpenSession())
            {
                //if (context.ResponsibleInformationId.HasValue)
                //{
                //    activity.ResponsibleInformation = _repository.Get<User>(context.ResponsibleInformationId);
                //}

                if (context.SigningId.HasValue)
                {
                    activity.Signing = _repository.Get<User>(context.SigningId);
                }

                await session.SaveOrUpdateAsync(activity);

                return _mapper.Map(activity, new ActivityCommonInfoDto());
            }
        }
    }
}
