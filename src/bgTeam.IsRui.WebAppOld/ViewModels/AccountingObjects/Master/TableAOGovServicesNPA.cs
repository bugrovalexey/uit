﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Frgu;
using bgTeam.IsRui.WebApp.Helpers;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class TableAOGovServicesNPA : NotificationBaseViewModel
    {
         #region consts

        public const string NameCaption = "Наименование документа";
        public const string DocumentNumberCaption = "Номер документа";
        public const string DateCaption = "Дата документа";
        public const string ParticleOfDocumentCaption = "Пункт, статья документа";

        #endregion consts

        public TableAOGovServicesNPA(AOGovServiceNPA entity)
        {
            Id = entity.Id;
            GovServicesNPAId = entity.GovServiceNpa.Return(x => (int?) x.Id, default(int?));
            Name = entity.Name;
            DocumentNumber = entity.DocumentNumber;
            DateStr = entity.Date.ToStringOrDefault();
            ParticleOfDocument = entity.ParticleOfDocument;
        }

        public TableAOGovServicesNPA(GovServiceNPA entity)
        {
            Name = entity.Name;
        }

        public int Id { get; set; }

        public int? GovServicesNPAId { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(DocumentNumberCaption)]
        public string DocumentNumber { get; set; }

        [DisplayName(DateCaption)]
        public string DateStr { get; set; }

        [DisplayName(ParticleOfDocumentCaption)]
        public string ParticleOfDocument { get; set; }


        public string DeleteLink
        {
            get
            {
                if (GovServicesNPAId.HasValue)
                    return string.Empty;

                return LinkBuilder.AjaxDeleteLink("DeleteAOGovServicesNPA", "AccountingObjects", new { id = Id }, "GridGovServicesNPA");
            }
        }
    }
}