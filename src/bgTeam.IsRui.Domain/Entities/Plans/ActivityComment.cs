﻿using bgTeam.IsRui.Domain.Entities.Common;
using System;

namespace bgTeam.IsRui.Domain.Entities.Plans
{
    /// <summary>
    /// Комментарии по мероприятию
    /// </summary>
    public class ActivityComment : EntityBase
    {
        protected ActivityComment()
        {
        }

        public ActivityComment(PlansActivity activity)
        {
            this.Activity = activity;
        }

        /// <summary>Страница на которой был оставлен комментарий</summary>
        public virtual string Page { get; set; }

        /// <summary>Комментарий</summary>
        public virtual string Text { get; set; }

        /// <summary>Дата комментария</summary>
        public virtual DateTime Date { get; set; }

        /// <summary>Пользователь оставивший комментарий</summary>
        public virtual User User { get; set; }

        public virtual PlansActivity Activity { get; set; }
    }
}