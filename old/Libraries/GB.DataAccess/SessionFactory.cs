﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GB.DataAccess
{
    class SessionFactory : ISessionFactoryGb
    {
        public SessionHibernate Create(IsolationLevel isolationLevel = IsolationLevel.ReadUncommitted)
        {
            return new SessionHibernate(isolationLevel);
        }

        internal SessionHibernate CreateInternal()
        {
            return new SessionHibernate();
        }
    }
}
