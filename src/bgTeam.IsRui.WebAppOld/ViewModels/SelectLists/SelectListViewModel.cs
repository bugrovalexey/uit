﻿using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace bgTeam.IsRui.WebApp.ViewModels.SelectLists
{
    public class SelectListViewModel<T> : SelectListViewModelBase where T : ViewModelBase
    {
        public IEnumerable<T> Data { get; set; }
    }

    public class DataTableSelectListViewModel : SelectListViewModelBase
    {
        public DataTable Data { get; set; }
    }

    public class SelectListViewModel : SelectListViewModelBase
    {
        public IEnumerable Data { get; set; }
    }

    public class SelectListViewModelBase
    {
        public bool IsMultipleSelect { get; set; }

        public string Target { get; set; }

        public string ActionUrl { get; set; }

        public string SelectListCallback { get; set; }

        public object Params { get; set; }
    }
}