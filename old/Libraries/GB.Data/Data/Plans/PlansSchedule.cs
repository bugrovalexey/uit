﻿using System;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>План-график</summary>
    public class PlansSchedule : NumerableEntity
    {
        protected PlansSchedule()
        {
        }

        public PlansSchedule(Plan Plan)
        {
            this.Plan = Plan;
        }

		/// <summary>План</summary>
		public virtual Plan Plan { get; set; }

        /// <summary>Мероприятие</summary>
        public virtual PlansActivity Activity {get; set;}

        /// <summary>
        /// Наименование работы (услуги)
        /// </summary>
        public virtual PlansWork Work {get; set;}

        /// <summary> ОКДП введенное пользователем (менее приоритетное по отношению к OKDP)</summary>
        public virtual string OKDP_Value { get; set; }

        /// <summary>Наименование предмета государственного контракта</summary>
        public virtual string ContractSubject { get; set; }

        /// <summary>Спецификация - Наименование оборудования, лицензий </summary>
        public virtual PlansSpec PlansSpec { get; set; }

        /// <summary> Плановый срок исполнения  контракта</summary>
        public virtual DateTime? ExecTime { get; set; }

        /// <summary> Ориентировочная начальная стоимость контракта, тыс. руб. </summary>
        public virtual double Cost { get; set; }
    }
}
