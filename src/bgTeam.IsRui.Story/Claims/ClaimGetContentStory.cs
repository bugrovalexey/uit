﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimGetContentStory : IStory<ClaimGetContentStoryContext, ClaimContentDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapperBase;

        public ClaimGetContentStory(IRepositoryRui repositoryRui, IMapperBase mapperBase)
        {
            _repository = repositoryRui;
            _mapperBase = mapperBase;
        }

        public ClaimContentDto Execute(ClaimGetContentStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ClaimContentDto> ExecuteAsync(ClaimGetContentStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка: {context.ClaimId}");
            }

            return _mapperBase.Map(claim, new ClaimContentDto());
        }
    }
}
