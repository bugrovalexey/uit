﻿using Uviri.MKRF.Entities.PlanReport.SectionIX;

namespace Uviri.MKRF.Mapping.PlanReport.SectionIX
{
    public class OutFuncGKMap : ClassMapBase<OutFuncGK>
    {
        public OutFuncGKMap()
            : base("PRS_9b_Rekv_GK")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_9b_Rekv_GK_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"  @PRS_9b_Rekv_GK_DT = ? 
//							,@PRS_9b_Rekv_GK_Num = ? 
//							,@PRS_9b_Rekv_GK_Predmet = ? 
//							,@PRS_9b_Rekv_GK_Executor = ? 
//							,@PRS_9b_ID = ? 
//							,@PRS_9b_Rekv_GK_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_9b_Rekv_GK_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_9b_Rekv_GK_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_9b_Rekv_GK_Delete @PRS_9b_Rekv_GK_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Date, "PRS_9b_Rekv_GK_DT");
			Map(x => x.Num, "PRS_9b_Rekv_GK_Num");
			Map(x => x.Subject, "PRS_9b_Rekv_GK_Predmet");
			Map(x => x.Executor, "PRS_9b_Rekv_GK_Executor");

			References<OutFunc>(x => x.OutFunc, "PRS_9b_ID").Not.Nullable();
        }
    }
}