﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.AccountingObjects;

namespace GB._Mapping.AccountingObjects
{
    class InternalIsAndComponentsItkiMap : ClassMapHiLo<InternalIsAndComponentsItki>
    {
        public InternalIsAndComponentsItkiMap()
            : base("OU_Internel_IS")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Purpose, "OU_Internel_IS_Goal");

            References(x => x.Object, "OU_Object_ID");
            References(x => x.Owner, "OU_ID").Not.Nullable();
        }
    }
}
