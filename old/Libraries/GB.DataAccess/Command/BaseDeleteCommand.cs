﻿using GB.Data;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command
{
    public class BaseDeleteCommand<TContext, T> : ICommand<TContext>
        where T : EntityBase, new()
        where TContext : BaseAddOrSaveContext
    {
        ISessionFactoryGb factory;

        public BaseDeleteCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(TContext context)
        {            
            if (context.xId.HasValue && context.xId != 0)
            {
                var entity = new Repository<T>().Get(context.xId.Value);

                if (entity != null)
                {
                    using (var session = factory.Create())
                    {                        
                        session.Delete(entity);
                        session.Commit();
                    }
                }
            }
        }
    }
}
