﻿using System.ComponentModel;

namespace GB.Data.Directories
{
    /// <summary>
    /// Типы обеспечения
    /// </summary>
    public enum CategoryTypeOfSupportEnum
    {
        [Description("Не задано")]
        None = 0,

        [Description("Программное обеспечение")]
        Software = 1,

        [Description("Техническое обеспечение")]
        TechnicalSupport = 2,

        [Description("Работы/Услуги")]
        WorksAndServices = 3
    }
}