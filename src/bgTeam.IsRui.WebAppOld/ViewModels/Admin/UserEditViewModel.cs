﻿using bgTeam.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Common;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    [MetadataType(typeof(UserEditViewModel_Valid))]
    public class UserEditViewModel : ViewModelBase
    {
        private const string IsActiveCaption = "Активный";
        private const string LoginCaption = "Логин";
        private const string PasswordCaption = "Пароль";
        private const string ConfirmPasswordCaption = "Подтверждение пароля";
        private const string RoleIdsCaption = "Роли";
        private const string SurnameCaption = "Фамилия";
        private const string NameCaption = "Имя";
        private const string MiddlenameCaption = "Отчество";
        private const string DepartmentCaption = "Учреждение";
        private const string EmailCaption = "Email";
        private const string WorkPhoneCaption = "Рабочий телефон";
        private const string MobPhoneCaption = "Сотовый телефон";

        public UserEditViewModel(int? id)
        {
            var user = id.HasValue ? new RepositoryDc<User>().Get(
               q => q.Fetch(x => x.Department).Eager.Fetch(x => x.Roles).Eager.Where(x => x.Id == id))
               : null;

            Departments = new SelectList(
                new DepartmentRepository().GetAllEx(q => q.OrderBy(x => x.Id)),
                "Id",
                "Name", EntityBase.Default);

            Roles = new SelectList(
                new RoleService().Repository.GetAll(), "Id", "Name");

            //Значение по умолчанию
            IsActive = true;

            user.Do(Fill);
        }

        public void Fill(User user)
        {
            Id = user.Id;
            IsActive = user.IsActive;
            Login = user.Login;
            RoleIds = user.Roles.Return(roles => roles.Select(r => r.Id), default(IEnumerable<int>));
            Surname = user.Surname;
            Name = user.Name;
            Middlename = user.Middlename;
            DepartmentId = user.Department.Return(dep => (int?)dep.Id, default(int?));
            Email = user.Email;
            WorkPhone = user.WorkPhone;
            MobPhone = user.MobPhone;
        }

        public int Id { get; set; }

        [DisplayName(IsActiveCaption)]
        public bool IsActive { get; set; }

        [DisplayName(LoginCaption)]
        public string Login { get; set; }

        [DisplayName(PasswordCaption)]
        public string NewPassword { get; set; }

        [DisplayName(ConfirmPasswordCaption)]
        public string ConfirmNewPassword { get; set; }

        [DisplayName(RoleIdsCaption)]
        public IEnumerable<int> RoleIds { get; set; }

        [DisplayName(SurnameCaption)]
        public string Surname { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(MiddlenameCaption)]
        public string Middlename { get; set; }

        [DisplayName(DepartmentCaption)]
        public int? DepartmentId { get; set; }

        [DisplayName(EmailCaption)]
        public string Email { get; set; }

        [DisplayName(WorkPhoneCaption)]
        public string WorkPhone { get; set; }

        [DisplayName(MobPhoneCaption)]
        public string MobPhone { get; set; }

        public SelectList Departments { get; set; }

        public SelectList Roles { get; set; }
    }

    internal abstract class UserEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Login { get; set; }

        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        //[DataType(DataType.Password)]
        //[Compare("NewPassword", ErrorMessage = NotMatchPasswords)]
        //public string ConfirmNewPassword { get; set; }

        [Required(ErrorMessage = NotZeroValueText)]
        public IEnumerable<int> RoleIds { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string Surname { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Required(ErrorMessage = RequiredText)]
        [Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        public int? DepartmentId { get; set; }
    }
}