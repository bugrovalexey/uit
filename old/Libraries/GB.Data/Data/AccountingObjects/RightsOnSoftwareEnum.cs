﻿using System.ComponentModel;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Права на ПО
    /// </summary>
    public enum RightsOnSoftwareEnum
    {
        [Description("Объект исключительного права")]
        Exclusive = 1,

        [Description("Права использования на неисключительной основе")]
        NotExclusive = 2
    }
}