﻿namespace GB.MKRF.Entities.Plans
{
    /// <summary>
    /// Предложение по приоритетным направлениям развития ИТ на очередной финансовый год
    /// </summary>
    public class OfferByPlan : EntityBase
    {
        /// <summary>
        /// Индикаторы
        /// </summary>
        public virtual string Indicator { get; set; }

        /// <summary>
        /// Показатель
        /// </summary>
        public virtual string Index { get; set; }

        /// <summary>
        /// Перечень мероприятий по информатизации
        /// </summary>
        public virtual string Activities { get; set; }

        /// <summary>
        /// Предложение
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Задачи
        /// </summary>
        public virtual string Task { get; set; }

        /// <summary>
        /// Цели
        /// </summary>
        public virtual string Purpose { get; set; }

        /// <summary>
        /// аннотация
        /// </summary>
        public virtual string Annotation { get; set; }

        public virtual Plan Plan { get; set; }
    }
}