﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class CommonFunctionMap : ClassMapBase<CommonFunction>
    {
        public CommonFunctionMap()
            : base("Common_Functions")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Common_Functions_Name");
        }
    }
}
