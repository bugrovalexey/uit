﻿using System.ComponentModel;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    public enum GovPurchaseTypeEnum
    {
        [Description("Открытый конкурс")]
        Open = 1,

        [Description("Электронный аукцион")]
        Auction = 2
    }
}
