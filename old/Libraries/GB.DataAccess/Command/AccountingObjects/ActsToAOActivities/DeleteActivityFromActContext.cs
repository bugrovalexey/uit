﻿namespace GB.DataAccess.Command.AccountingObjects.ActsToAOActivities
{
    public class DeleteActivityFromActContext : ICommandContext
    {
        public int Id { get; set; }
    }
}
