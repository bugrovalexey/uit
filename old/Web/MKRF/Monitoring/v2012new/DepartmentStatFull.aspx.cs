﻿using GB;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GB.MKRF.Web.Monitoring.v2012new
{
    public partial class DepartmentStatFull : System.Web.UI.Page
    {
        Department CurrentDep
        {
            get
            {
                return RepositoryBase<Department>.Get(DepComboBox.Value);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            DepComboBox.DataBind();
            Grid1.DataBind();
            Grid2.DataBind();
            Grid3.DataBind();

            base.OnPreRender(e);
        }

        protected void DepComboBox_DataBinding(object sender, EventArgs e)
        {
            var data = RepositoryBase<Department>.GetAll(x => x.Id != EntityBase.Default).OrderBy(x => x.Name);

            ASPxComboBox box = sender as ASPxComboBox;
            
            box.DataSource = data;
            box.SelectedIndex = 0;
        }

        protected void Grid1_DataBinding(object sender, EventArgs e)
        {
            var grid = sender as ASPxGridView;

            grid.DataSource = MonitoringRepository.GetDepartmentStatFull1(CurrentDep);
        }

        protected void Grid2_DataBinding(object sender, EventArgs e)
        {
            var grid = sender as ASPxGridView;

            grid.DataSource = MonitoringRepository.GetDepartmentStatFull2(CurrentDep);
        }

        protected void Grid3_DataBinding(object sender, EventArgs e)
        {
            var grid = sender as ASPxGridView;

            grid.DataSource = MonitoringRepository.GetDepartmentStatFull3(CurrentDep);
        }

        protected void CallbackPanel_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            Grid1.DataBind();
            Grid2.DataBind();
            Grid3.DataBind();
        }
    }
}