﻿namespace bgTeam.IsRui.Common.Services.Impl
{
    using NReco.PdfGenerator;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ConvertHtmlToPdf : IConvertHtmlToPdf
    {
        public void ConvertHtmlToPdfFile(string htmlCode)
        {

            var pathOutPdf = Path.Combine(Environment.CurrentDirectory, @"wwwroot\Temp\ReportPDF.pdf");

            var htmlToPdf = new HtmlToPdfConverter();

            var pdfBytesFromHtmlString = htmlToPdf.GeneratePdf(htmlCode, null);

            using (FileStream fs = File.Create(pathOutPdf))
            {
                fs.Write(pdfBytesFromHtmlString, 0, pdfBytesFromHtmlString.Length);
            }

            //Process.Start(pathOutPdf);
        }
    }
}
