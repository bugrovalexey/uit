﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.SpecialChecks
{
    public class AddOrSaveSpecialCheckContext : ICommandContext
    {        
        public int AccountingObjectId { get; set; }
        
        public string Name { get; set; }
        
        public string Files { get; set; }

        public int? xId { get; set; }
    }
}
