﻿using System;
using System.Collections.Generic;
using System.Linq;
using GB;
using GB.Entity.Directories;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.MKRF.Web.Helpers;

namespace GB.MKRF.Web.Activities
{
    public partial class Expenses : BasePagePlansActivity
    {
        public static string GetUrl(int activityId)
        {
            return UrlHelper.Resolve("~/Plans/v2012new/Expenses.aspx?ActivityId=" + activityId.ToString());
        }

        protected override void OnPreRender(EventArgs e)
        {
            ExpenseItemMemo.DataBind();
            GRBSMemo.DataBind();
            SectionKBKMemo.DataBind();
            WorkFormMemo.DataBind();
            ExpenditureItemsMemo.DataBind();

            VolY0TextBox.DataBind();
            VolY1TextBox.DataBind();
            VolY2TextBox.DataBind();

            base.OnPreRender(e);
        }

        protected void Memo_DataBinding(object sender, EventArgs e)
        {
            var memo = sender as ASPxTextBox;

            switch (memo.ID)
            {
                case "GRBSMemo":
                    SetHF(memo, "GRBS", CurrentActivity.GRBS);
                    break;
                case "SectionKBKMemo":
                    SetHF(memo, "SectionKBK", CurrentActivity.SectionKBK);
                    break;
                case "ExpenseItemMemo":
                    SetHF(memo, "ExpenseItem", CurrentActivity.ExpenseItem);
                    break;
                case "WorkFormMemo":
                    SetHF(memo, "WorkForm", CurrentActivity.WorkForm);
                    break;
                case "ExpenditureItemsMemo":
                    SetHF(memo, "ExpenditureItems", CurrentActivity.ExpenditureItem);
                    break;
                default:
                    throw new ApplicationException("Отсутствие элемента");
            }
        }

        protected void VolTextBox_DataBinding(object sender, EventArgs e)
        {
            var box = sender as ASPxTextBox;

            switch (box.ID)
            {
                case "VolY0TextBox":
                    box.Text = CurrentActivity.PlansActivityVolY0.ToString();
                    break;
                case "VolY1TextBox":
                    box.Text = CurrentActivity.PlansActivityVolY1.ToString();
                    break;
                case "VolY2TextBox":
                    box.Text = CurrentActivity.PlansActivityVolY2.ToString();
                    break;
            }
        }
        
        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            var controller = new PlansActivityController();
            controller.Update4(
                new PlansActivityArgs()
                {
                    Id = CurrentActivity.Id,

                    KOSGU = Convert.ToInt32(hf.Get("ExpenditureItemsId")),
                    GRBS = Convert.ToInt32(hf.Get("GRBSId")),
                    SectionKBK = Convert.ToInt32(hf.Get("SectionKBKId")),
                    WorkForm = Convert.ToInt32(hf.Get("WorkFormId")),
                    CSR = Convert.ToInt32(hf.Get("ExpenseItemId")),

                    VolY0 = VolY0TextBox.Value,
                    VolY1 = VolY1TextBox.Value,
                    VolY2 = VolY2TextBox.Value,
                });
        }

        private void SetHF(ASPxTextBox memo, string name, IEntityCodeDirectories entity)
        {
            if ((CurrentActivity == null) || (entity == null))
            {
                memo.Text = "- Не задано";
                hf.Set(name + "Id", 0);
                hf.Set(name + "Name", string.Empty);
                hf.Set(name + "Code", string.Empty);
            }
            else
            {
                memo.Text = entity.Code.ToString() + " " + entity.Name;
                hf.Set(name + "Id", entity.Id);
                hf.Set(name + "Name", entity.Name);
                hf.Set(name + "Code", entity.Code);
            }
        }

        

        //private object SaveValue(string name)
        //{
        //    if (hf.Contains(name + "Id"))
        //    {
        //        return hf[name + "Id"];
        //    }
        //    return null;
        //}

        
    }
}