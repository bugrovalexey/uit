﻿using bgTeam.IsRui.MKRF.Providers;

namespace bgTeam.IsRui.MKRF.Logic.Tabs.Handler
{
    internal class AccountingObjectsCardHandler : TabsBaseHandler
    {
        public AccountingObjectsCardHandler(IAccessControllerProvider accessControllerProvider)
            : base("AccountingObjects", accessControllerProvider)
        {
            Add("Общие сведения", "CommonData");
            Add("Информационное взаимодействие", "InformationInteraction");
            Add("Характеристики", "Characteristics");
            Add("Мероприятия по информатизации", "Activities");
            Add("Ответственные", "Responsibles");
            Add("Виды обеспечения", "KindsOfSupport");
        }
    }
}