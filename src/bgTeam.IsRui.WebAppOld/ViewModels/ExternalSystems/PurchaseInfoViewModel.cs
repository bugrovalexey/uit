﻿namespace bgTeam.IsRui.WebApp.ViewModels.ExternalSystems
{
    public class PurchaseInfoViewModel
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public int Type { get; set; }
        public string TypeStr { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public object Price { get; set; }

        
    }
}