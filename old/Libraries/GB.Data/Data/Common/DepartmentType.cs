﻿namespace GB.Data.Common
{
    /// <summary>Тип ОГВа</summary>
    public class DepartmentType : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }
    }
}