﻿using Uviri.MKRF.Entities.Expertise;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpFormHelp6WorkMap : ClassMapBase<ExpFormHelp6Work>
    {
        public ExpFormHelp6WorkMap()
            : base("Exp_Concl_Sprv_6_Work")
        {
//            Table("w_");

//            Id(x => x.Id, "Exp_Concl_Sprv_6_Work_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@Exp_Concl_Sprv_6_Work_Equiv = ?
//                          ,@Exp_Concl_Sprv_6_Work_Comm = ?
//                          ,@Plans_Work_ID = ?
//                          ,@Exp_Concl_Sprv_ID = ?
//						  ,@Exp_Concl_Sprv_6_Work_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Exp_Concl_Sprv_6_Work_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Concl_Sprv_6_Work_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Exp_Concl_Sprv_6_Work_Delete @Exp_Concl_Sprv_6_Work_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Flag, "Exp_Concl_Sprv_6_Work_Equiv");
            Map(x => x.Comment, "Exp_Concl_Sprv_6_Work_Comm");

            References<PlansWork>(x => x.PlansWork, "Plans_Work_ID").Not.Nullable();
            References<ExpFormHelp>(x => x.FormHelp, "Exp_Concl_Sprv_ID").Not.Nullable();
        }
    }
}
