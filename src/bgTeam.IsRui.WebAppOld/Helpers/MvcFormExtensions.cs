﻿using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace bgTeam.IsRui.WebApp.Helpers
{
    public class MvcFormExtensions : MvcForm
    {
        readonly HtmlHelper _helper;

        public MvcFormExtensions(ViewContext viewContext, HtmlHelper helper, bool readOnly)
            : base(viewContext)
        {
            _helper = helper;
            _helper.ViewContext.Writer.Write("<div class='{0}'>", readOnly ? "edit_form_readonly" : "edit_form");
        }

        protected override void Dispose(bool disposing)
        {
            _helper.ViewContext.Writer.Write(string.Format("</div>"));
            base.Dispose(disposing);
        }
    }
}