﻿using GB.Data;
using GB.MKRF.Infrastructure;
using GB.MKRF.Repository;
using NHibernate.Impl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace GB.MKRF.Services
{
    public abstract class ServiceBase<T> where T : EntityBase
    {
        internal static class InternalStaticMethods
        {
            private static readonly object _lock = new object();

            public static NHibernate.ISession Session
            {
                get { return NHSession.CurrentSession; }
            }

            #region Register User

            /// <summary>
            /// Регистрирует в БД текущего пользователя
            /// </summary>
            public static void RegUser()
            {
                RegUserById(UserRepository.CurrentUserId);
            }

            /// <summary>
            /// Регистрирует в БД пользователя по-умолчанию
            /// </summary>
            public static void RegUserDefault()
            {
                RegUserById(EntityBase.Default);
            }

            /// <summary>
            /// Снимает регистрацию текущего пользователя
            /// </summary>
            public static void UnRegUser()
            {
                UnRegUser(UserRepository.CurrentUserId);
            }

            /// <summary>
            /// Снимает регистрацию в БД пользователя по-умолчанию
            /// </summary>
            public static void UnRegUserDefault()
            {
                UnRegUser(EntityBase.Default);
            }

            /// <summary>
            /// Регистрирует в БД пользователя по id
            /// </summary>
            private static void RegUserById(int id)
            {
                SessionImpl si = Session as SessionImpl;

                IDbCommand command = Session.Connection.CreateCommand();

                si.Transaction.Enlist(command);

                command.CommandText = @"dbo.User_Reg";
                command.CommandType = CommandType.StoredProcedure;

                IDbDataParameter seqVal = command.CreateParameter();
                seqVal.ParameterName = "User_ID";
                seqVal.Value = id;
                command.Parameters.Add(seqVal);

                IDbDataParameter dateVal = command.CreateParameter();
                dateVal.ParameterName = "User_DT";
                dateVal.DbType = DbType.DateTime;
                dateVal.Value = DBNull.Value;
                command.Parameters.Add(dateVal);

                IDbDataParameter compName = command.CreateParameter();
                compName.ParameterName = "Comp_Name";
                compName.DbType = DbType.String;
                compName.Value = !string.IsNullOrEmpty(NHSession.ClientComputerName) ? (object)NHSession.ClientComputerName : DBNull.Value;
                command.Parameters.Add(compName);

                command.ExecuteNonQuery();
            }

            /// <summary>
            /// Снимает регистрацию пользователя по id
            /// </summary>
            private static void UnRegUser(int id)
            {
                SessionImpl si = Session as SessionImpl;

                IDbCommand command = Session.Connection.CreateCommand();
                si.Transaction.Enlist(command);

                command.CommandText = @"dbo.User_UnReg";
                command.CommandType = CommandType.StoredProcedure;

                IDbDataParameter seqVal = command.CreateParameter();
                seqVal.ParameterName = "User_ID";
                seqVal.Value = id;
                command.Parameters.Add(seqVal);

                command.ExecuteNonQuery();
            }

            #endregion Register User

            /// <summary>
            /// Удаляет объект
            /// </summary>
            /// <param name="Id"></param>
            public static void Delete(object obj)
            {
                using (Session.BeginTransaction())
                {
                    RegUser();

                    Session.Delete(obj);

                    FlushInternal();

                    UnRegUser();

                    Session.Transaction.Commit();
                }
            }

            public static void DeleteList(IEnumerable list)
            {
                using (Session.BeginTransaction())
                {
                    RegUser();

                    foreach (var item in list)
                    {
                        Session.Delete(item);
                    }

                    FlushInternal();

                    UnRegUser();

                    Session.Transaction.Commit();
                }
            }

            /// <summary>
            /// Вставляем объект
            /// </summary>
            /// <param name="Id"></param>
            public static void Insert(object obj)
            {
                lock (_lock)
                {
                    using (Session.BeginTransaction())
                    {
                        try
                        {
                            RegUser();

                            Session.Save(obj);

                            FlushInternal();

                            UnRegUser();

                            Session.Transaction.Commit();
                        }
                        catch (Exception exp)
                        {
                            UnRegUser();
                            Session.Evict(obj);
                            Session.Transaction.Rollback();

                            throw exp;
                        }
                    }
                }
            }

            /// <summary>
            /// Обновляем объект
            /// </summary>
            /// <param name="Id"></param>
            public static void Update(object obj)
            {
                lock (_lock)
                {
                    using (Session.BeginTransaction())
                    {
                        try
                        {
                            RegUser();

                            Session.Update(obj);

                            FlushInternal();

                            UnRegUser();

                            Session.Transaction.Commit();
                        }
                        catch (Exception exp)
                        {
                            UnRegUser();
                            Session.Evict(obj);
                            Session.Transaction.Rollback();

                            throw exp;
                        }
                    }
                }
            }

            /// <summary>
            /// Обновление объекта в кэше NH
            /// </summary>
            /// <param name="Id"></param>
            public static void Refresh(object obj)
            {
                if (obj == null)
                    return;

                Session.Refresh(obj);
            }

            /// <summary>
            /// Примененяет изменения
            /// </summary>
            private static void FlushInternal()
            {
                try
                {
                    Session.Flush();
                }
                catch (Exception exp)
                {
                    throw exp.InnerException ?? exp;
                }
            }

            //RepositoryBaseNew

            public static T Get(int id)
            {
                return Session.Get<T>(id);
            }
        }

        #region Кандидаты на вылет

        /*
        public T CurrentEdit { get; set; }
        public virtual void SetCurrentEdit(object entityId)
        {
            CurrentEdit = null;

            if (entityId.IsEmpty())
            {
                throw new ArgumentException("Не указан ID");
            }

            CurrentEdit = Repository.GetExisting(entityId);

            //if (!Validator.Validator.Validate(CurrentEdit, ObjActionEnum.view))
            //{
            //    CurrentEdit = null;
            //    throw new AccessViolationException(String.Format("Нет доступа к объекту <{0}>", entityId));
            //}
        }
        */

        #endregion Кандидаты на вылет

        private readonly Repository<T> _r = new Repository<T>();

        public Repository<T> Repository
        {
            get { return _r; }
        }

        /// <summary>
        /// Удаляет объект
        /// </summary>
        public void Delete(int id)
        {
            T obj = InternalStaticMethods.Get(id);
            if (obj != null)
            {
                InternalStaticMethods.Delete(obj);
            }
        }

        /// <summary>
        /// Удаляет объект
        /// </summary>
        protected void Delete(T entity)
        {
            InternalStaticMethods.Delete(entity);
        }

        /// <summary>
        /// Удаляет список объектов
        /// </summary>
        protected void Delete(IList<T> list)
        {
            InternalStaticMethods.DeleteList(list);
        }

        /// <summary>
        /// Создаёт / сохраняет объект
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="insert">Вызвать процедуру вставки в любом случае</param>
        protected T SaveOrUpdate(T obj, bool insert = false)
        {
            if (insert || obj.Id <= 0)
            {
                InternalStaticMethods.Insert(obj);
            }
            else
            {
                InternalStaticMethods.Update(obj);
            }
            return obj;
        }

        /// <summary>
        /// Обновляет объект в кэше NH
        /// </summary>
        /// <param name="Id"></param>
        protected void Refresh(T obj)
        {
            InternalStaticMethods.Refresh(obj);
        }

        protected K GetEntity<K>(object val) where K : EntityBase
        {
            if (val != null)
            {
                var id = 0;
                if (Int32.TryParse(val.ToString(), out id))
                {
                    if (id < 0)
                        return null;

                    return new Repository<K>().Get(id);
                }
            }
            return null;
        }
    }
}