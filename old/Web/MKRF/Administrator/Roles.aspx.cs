﻿using System;
using GB.MKRF.Entities.Admin;
using GB.Repository;

namespace GB.MKRF.Web.Administrator
{
    public partial class Roles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            grid.DataBind();
        }

        protected void grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            grid.DataSource = RepositoryBase<Role>.GetAll();
        }
    }
}