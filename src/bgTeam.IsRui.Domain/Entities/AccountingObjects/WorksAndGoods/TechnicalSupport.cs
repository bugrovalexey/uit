﻿using bgTeam.IsRui.Domain.Entities.Directories;
using System.Collections.Generic;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods
{
    /// <summary>
    /// Техническое обеспечение ОУ
    /// </summary>
    public class TechnicalSupport : EntityBase
    {
        /// <summary>
        /// Вид обеспечения
        /// </summary>
        public static readonly CategoryTypeOfSupportEnum Type = CategoryTypeOfSupportEnum.TechnicalSupport;    

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Классификационная категория
        /// </summary>
        public virtual CategoryKindOfSupport CategoryKindOfSupport { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public virtual decimal Summa { get; set; }

        /// <summary>
        /// Объект учета
        /// </summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Производитель
        /// </summary>
        public virtual Manufacturer Manufacturer { get; set; }

        /// <summary>
        /// Пользовательское наименование производителя
        /// </summary>
        public virtual string ManufacturerCustomName { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public virtual int Amount { get; set; }

        /// <summary>
        /// Характеристики
        /// </summary>
        public virtual IList<CharacteristicOfTechnicalSupport> Characteristics { get; set; }

    }
}