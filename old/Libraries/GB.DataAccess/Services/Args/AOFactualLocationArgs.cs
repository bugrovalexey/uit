﻿namespace GB.DataAccess.Services.Args
{
    public class AOFactualLocationArgs : BaseArgsNew
    {
        /// <summary>
        /// Наименование организации
        /// </summary>
        public string LocationOrgName { get; set; }

        /// <summary>
        /// ФИО ответственного
        /// </summary>
        public string LocationFioResponsible { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string LocationOrgAddress { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string LocationOrgPhone { get; set; }
    }
}
