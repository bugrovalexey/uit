﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class TreeDictionaryDto : DictionaryDto
    {
        public DictionaryDto Parent { get; set; }

        public IEnumerable<DictionaryDto> Childrens { get; set; }
    }
}
