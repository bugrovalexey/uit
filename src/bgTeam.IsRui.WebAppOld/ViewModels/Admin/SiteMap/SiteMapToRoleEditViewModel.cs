﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin.SiteMap
{
    [MetadataType(typeof(SiteMapToRoleEditViewModel_Valid))]
    public class SiteMapToRoleEditViewModel : ViewModelBase
    {
        private const string RoleIdCaption = "Роль";

        public SiteMapToRoleEditViewModel(int siteMapId)
        {
            SiteMapId = siteMapId;
            Roles = new SelectList(
                new RoleService().Repository.GetAll(), "Id", "Name");
        }

        public int Id { get; set; }

        public int SiteMapId { get; set; }

        [DisplayName(RoleIdCaption)]
        public int? RoleId { get; set; }

        public SelectList Roles { get; set; }
    }

    internal abstract class SiteMapToRoleEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = NotZeroValueText)]
        public IEnumerable<int> RoleId { get; set; }
    }
}