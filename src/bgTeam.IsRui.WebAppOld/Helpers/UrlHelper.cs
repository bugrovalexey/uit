﻿using bgTeam.IsRui.Common;
using System.Collections.Specialized;
using System.Web;

namespace bgTeam.IsRui.WebApp.Helpers
{
    public class UrlHelper
    {
        private readonly object mapLock = new object();
        protected StringDictionary map = new StringDictionary();

        protected UrlHelper()
        {
            
        }

        protected string ResolveInternal(string relativeUrl)
        {
            string absoluteUrl;

            if (map.ContainsKey(relativeUrl))
            {
                absoluteUrl = map[relativeUrl];
            }
            else
            {
                absoluteUrl = ToAbsolute(relativeUrl);
                lock (mapLock)
                {
                    map[relativeUrl] = absoluteUrl;
                }
            }

            return absoluteUrl;
        }

        /// <summary>
        /// Преобразует виртуальный путь в абсолютный путь приложения.
        /// </summary>
        /// <param name="url">Относительный путь</param>
        protected static string ToAbsolute(string url)
        {
            return VirtualPathUtility.ToAbsolute(url);
        }

        #region  public static

        public static string Resolve(string relativeUrl)
        {
            return Singleton<UrlHelper>.Instance.ResolveInternal(relativeUrl);
        }

        #endregion  public static
    }
}