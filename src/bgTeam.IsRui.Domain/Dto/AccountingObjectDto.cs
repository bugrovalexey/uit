﻿namespace bgTeam.IsRui.Domain.Dto
{
    public class AccountingObjectDto
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        public string Number { get; set; }

        public string ShortName { get; set; }

        public string TypeName { get; set; }

        public string KindName { get; set; }

        public string Stage { get; set; }

        public string StatusName { get; set; }

        public bool CanDelete { get; set; }

        public bool Favorite { get; set; }

        public string Mode { get; set; }

        public string Cost { get; set; }

        public decimal CostValue { get; set; }

        public string Description { get; set; }

        public string KindOfActivityName { get; set; }
    }
}
