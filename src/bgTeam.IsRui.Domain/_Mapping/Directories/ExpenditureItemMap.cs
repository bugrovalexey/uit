﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    internal class ExpenditureItemMap : ClassMapHiLo<ExpenditureItem>
    {
        public ExpenditureItemMap()
            : base("Expenditure_Items")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Expenditure_Items_Name").Nullable();
            Map(x => x.Code, "Expenditure_Items_Code");
            Map(x => x.DateStart, "Expenditure_Items_Start_Date").Nullable();
            Map(x => x.DateEnd, "Expenditure_Items_End_Date").Nullable();
        }
    }
}