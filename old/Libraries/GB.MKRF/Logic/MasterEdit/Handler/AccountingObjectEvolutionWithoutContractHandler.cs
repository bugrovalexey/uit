﻿
namespace GB.MKRF.Logic.MasterEdit.Handler
{
    class AccountingObjectEvolutionWithoutContractHandler : MasterEditBaseHandler
    {
        protected override MasterInItem[] GetListPage()
        {
            return new MasterInItem[] 
            {
                new MasterInItem(MasterItemEnum.None),
                new MasterInItem(MasterItemEnum.ActivityCreate)
                {
                    Title = "Заполните сведения о мероприятие по информатизации направленном на развитие данного ОУ",
                },
                new MasterInItem(MasterItemEnum.TechnicalSupport)
                {
                    Title = "Дополните перечень технического оборудования, " +
                            "средствами необходимыми для развития объекта учета",
                },
                new MasterInItem(MasterItemEnum.Software)
                {
                    Title = "Дополните перечень программного обеспечения, " +
                            "средствами необходимыми для развития объекта учета",
                },
                new MasterInItem(MasterItemEnum.WorkAndService)
                {
                    Title = "Дополните перечень работами, осуществляемыми в рамках развития объекта учета",
                },
                new MasterInItem(MasterItemEnum.CharacteristicsCreate)
                {
                    Title = "Скорректируйте функциональные характеристики объекта учета",
                },
                new MasterInItem(MasterItemEnum.GovernmentContract)
                {
                    Title = "Укажите сведения о заключенных государственных контрактах на развитие объекта учета"
                },
                new MasterInItem(MasterItemEnum.CardForRead)
            };
        }
    }
}
