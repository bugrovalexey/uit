﻿using System;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class ActivityReadOnly : System.Web.UI.Page
    {
        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as ActivityCardCommon).CurrentActivity;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            var master = Master as ActivityCardCommon;

            if (Page.Request.QueryString[ActivityCard.ActivityIdKey] != null)
            {
                PlansActivityController controller = new PlansActivityController();
                ActivityReadMode.SetViewModel(controller.GetActivityViewModel(master.CurrentActivity));
                ActivityReadMode.EnableEdit = master.EnableEdit;
            }

            master.SetBtnViewTopChecked();

            ActivityReadMode.DefaultShowEditLinks = master.EnableEdit;
            
            if (master.CurrentActivity != null) ShowObjectsRepository.SaveVisitedObjects(master.CurrentActivity.Id, ObjectsType.Activity, UserRepository.GetCurrent().Id);
        }

        public static string GetUrl(int ActId)
        {
            return UrlHelper.Resolve("~/Plans/v2012new/ActivityReadOnly.aspx") + "?" + ActivityCard.ActivityIdKey + "=" + ActId;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
    }
}