﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.PlanReport.SectionV;

namespace Uviri.MKRF.Mapping.PlanReport.SectionV
{
    public class InfoCreatingDepsMap : ClassMapBase<InfoCreatingDeps>
    {
        public InfoCreatingDepsMap()
            : base("PRS_5v_GO")
		{
//            Table("w_");

//            Id(x => x.Id, "PRS_5v_GO_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string args = @" @PRS_5v_ID = ? 
//							,@Govt_Organ_ID = ? 
//							,@PRS_5v_GO_ID = ? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_5v_GO_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_5v_GO_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_5v_GO_Delete
//						  @PRS_5v_GO_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            References<InfoCreating>(x => x.InfoCreating, "PRS_5v_ID").Not.Nullable();
			References<Department>(x => x.Department, "Govt_Organ_ID");
        }
    }
}