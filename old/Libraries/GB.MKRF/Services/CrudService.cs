﻿using GB.Data;
using System;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    public class CrudService<T, Targs> : ServiceBase<T>, IService<T, Targs>
        where T : EntityBase, new()
        where Targs : BaseArgsNew//, IServiceArgs
    {
        protected Action<T, Targs> Fill { get; set; }

        public CrudService(Action<T, Targs> fill)
        {
            Fill = fill;
        }

        public T Create(Targs args)
        {
            return SaveOrUpdate(new T(), args);
        }

        public T Update(Targs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private T SaveOrUpdate(T entity, Targs args)
        {
            Fill(entity, args);

            base.SaveOrUpdate(entity);
            return entity;
        }

        public void Delete(Targs args)
        {
            base.Delete(args.Id);
        }
    }
}