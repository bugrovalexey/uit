﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateBalanceCostCommand : ICommand<UpdateBalanceCostContext>
    {
        ISessionFactoryGb factory;

        public UpdateBalanceCostCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(UpdateBalanceCostContext context)
        {
            var entity = new Repository<AccountingObject>().GetExisting(context.id);
            
            GovContract contr = null;

            var balanceCost = new Repository<ActOfAcceptance>()
                                                .SelectSingle<decimal>(q => q.JoinAlias(x => x.GovContract, () => contr)
                                                                                    .Where(x => contr.AccountingObject.Id == context.id)
                                                                                    .Select(Projections.Sum<ActOfAcceptance>(x => x.Cost)));

            entity.BalanceCost = balanceCost;            

            using (var session = factory.Create())
            {
                session.Update(entity);
                session.Commit();
            }
        }
    }
}
