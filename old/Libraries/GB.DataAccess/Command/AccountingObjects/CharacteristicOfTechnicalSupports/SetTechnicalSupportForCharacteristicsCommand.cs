﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.DataAccess.Repository;
using GB.Data.AccountingObjects;

namespace GB.DataAccess.Command.AccountingObjects.CharacteristicOfTechnicalSupports
{
    public class SetTechnicalSupportForCharacteristicsCommand : ICommand<SetTechnicalSupportForCharacteristicsContext>
    {
        ISessionFactoryGb factory;

        public SetTechnicalSupportForCharacteristicsCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(SetTechnicalSupportForCharacteristicsContext context)
        {
            var characteristics = new Repository<CharacteristicOfTechnicalSupport>().GetAllEx(q => q.WhereRestrictionOn(x => x.Id).IsIn(context.CharacteristicsOfTechnicalSupportIds));

            using (var session = factory.Create())
            {
                foreach (var item in characteristics)
                {
                    item.TechnicalSupport = context.TechnicalSupport;
                    session.SaveOrUpdate(item);
                }
                session.Commit();
            }
        }
    }
}
