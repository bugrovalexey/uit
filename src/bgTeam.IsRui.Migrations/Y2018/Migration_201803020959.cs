﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201803020959)]
    public class Migration_201803020959 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Plans_Activity_Indicators").AddColumn("Plans_Activity_Indicators_Justification").AsString().Nullable();
        }
    }
}
