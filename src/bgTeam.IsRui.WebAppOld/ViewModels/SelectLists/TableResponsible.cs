﻿using bgTeam.Extensions;
using bgTeam.IsRui.Domain.Entities.Common;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.SelectLists
{
    public class TableResponsible : ViewModelBase
    {
        #region consts

        private const string FioCaption = "ФИО";
        private const string DepartmentCaption = "Структурное подразделение";
        private const string JobCaption = "Должность";
        private const string PhoneCaption = "Телефон";
        private const string EmailCaption = "Электронная почта";

        #endregion consts

        public TableResponsible(Responsible responsible)
        {
            Id = responsible.Id;
            Fio = responsible.Name;
            JobName = responsible.Job.Return(job => job.Name, default(string));
            DepartmentName = responsible.Department.Return(dep => dep.Name, default(string));
            Phone = responsible.Phone;
            Email = responsible.Email;
        }

        public int Id { get; set; }

        [DisplayName(FioCaption)]
        public string Fio { get; set; }

        [DisplayName(DepartmentCaption)]
        public string DepartmentName { get; set; }

        [DisplayName(JobCaption)]
        public string JobName { get; set; }

        [DisplayName(PhoneCaption)]
        public string Phone { get; set; }

        [DisplayName(EmailCaption)]
        public string Email { get; set; }
    }
}