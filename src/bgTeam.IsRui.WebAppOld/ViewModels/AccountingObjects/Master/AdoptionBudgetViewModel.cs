﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class AdoptionBudgetViewModel : StepBaseViewModel
    {
        #region consts

        private const string AdoptionBudgetDateCaption = "Дата";
        private const string BalanceCostCaption = "Балансовая стоимость";
        private const string GRBSCaption = "БК ГРБС";
        private const string PZRZCaption = "БК ПЗ/РЗ";
        private const string CSRCaption = "БК ЦСР";
        private const string WorkFormCaption = "БК ВР";

        #endregion consts

        public AdoptionBudgetViewModel(int id)
            : base(id, MasterItemEnum.AdoptionBudget)
        {
            FilesData =
                Current.DocumentGroundsForCreation.With(dc => dc.Documents).Return(
                    docs => docs.Select(x => new SelectEntityInfo() {Id = x.Id, Name = x.Name}),
                    new List<SelectEntityInfo>());

            BK_GRBS = Current.BK_GRBS.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });
            BK_PZRZ = Current.BK_PZRZ.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });
            BK_CSR = Current.BK_CSR.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });
            BK_WR = Current.BK_WR.With(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.CodeName } });
        }

        [DisplayName(AdoptionBudgetDateCaption)]
        public string AdoptionBudgetDate
        {
            get { return Current.AdoptionBudgetDate.ToStringOrDefault(); }
        }

        [DisplayName(BalanceCostCaption)]
        public decimal BalanceCost
        {
            get { return Current.BalanceCost; }
        }

        [DisplayName(CommonDataViewModel.DocNameCaption)]
        public string DocName
        {
            get { return Current.DocumentGroundsForCreation.Return(dc => dc.Name, default(string)); }
        }

        [DisplayName(CommonDataViewModel.DocNumberCaption)]
        public string DocNumber
        {
            get { return Current.DocumentGroundsForCreation.Return(dc => dc.Number, default(string)); }
        }

        [DisplayName(CommonDataViewModel.DocDateAdoptionCaption)]
        public string DocDateAdoption
        {
            get { return Current.DocumentGroundsForCreation.Return(dc => dc.DateAdoption, default(DateTime?)).ToStringOrDefault(); }
        }

        [DisplayName(CommonDataViewModel.DocParagraphCaption)]
        public string DocParagraph
        {
            get { return Current.DocumentGroundsForCreation.Return(dc => dc.Paragraph, default(string)); }
        }

        [DisplayName(CommonDataViewModel.FilesDataCaption)]
        public IEnumerable<IEntityName> FilesData { get; set; }

        [DisplayName(GRBSCaption)]
        public IList<IEntityName> BK_GRBS { get; private set; }

        [DisplayName(PZRZCaption)]
        public IList<IEntityName> BK_PZRZ { get; private set; }

        [DisplayName(CSRCaption)]
        public IList<IEntityName> BK_CSR { get; private set; }

        [DisplayName(WorkFormCaption)]
        public IList<IEntityName> BK_WR { get; private set; }

        public SelectList KOSGUList { get; set; }

    }
}