﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class CommentReportByPlanMap: ClassMapBase<CommentReportByPlan>
    {
        public CommentReportByPlanMap():
            base("Report_By_Plan")
        {

        }

        protected override void InitMap()
        {
            Map(x => x.Comment11, "Report_By_Plan_11");
            Map(x => x.Comment12, "Report_By_Plan_12");
            Map(x => x.Comment2, "Report_By_Plan_2");
            Map(x => x.Comment31, "Report_By_Plan_31");
            Map(x => x.Comment32, "Report_By_Plan_32");
            Map(x => x.Result, "Report_By_Plan_Result");

            Map(x => x.BudgetVolume12, "Report_By_Plan_BV_12")
                .Not.Nullable()
                .Default("0");

            Map(x => x.Cofinance12, "Report_By_Plan_C_12")
                .Not.Nullable()
                .Default("0");

            Map(x => x.BudgetVolume2, "Report_By_Plan_BV_2")
                .Not.Nullable()
                .Default("0");

            References<PlansActivity>(x => x.PlansActivity, "Plans_Activity_ID").Nullable();
            References<PlansActivityMU>(x => x.PlansActivityMU, "Plans_Activity_MU_ID").Nullable();
        }
    }
}
