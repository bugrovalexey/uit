﻿using System;
using AutoMapper;
using GB.Data;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class CrudService<T, Targs> : ServiceBase<T>, IService<T, Targs>
        where T : EntityBase, new()
        where Targs : BaseArgsNew//, IServiceArgs
    {
        public T Create(Targs args)
        {
            return SaveOrUpdate(new T(), args);
        }

        public T Update(Targs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private T SaveOrUpdate(T entity, Targs args)
        {
            Mapper.Map(args, entity);                
            
            SaveOrUpdate(entity);
            return entity;
        }

        public void Delete(Targs args)
        {
            Delete(args.Id);
        }
    }
}