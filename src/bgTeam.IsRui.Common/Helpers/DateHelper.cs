﻿namespace bgTeam.IsRui.Common.Helpers
{
    using System;
    using bgTeam.IsRui.Common.Extentions;

    public static class DateHelper
    {
        public static string GetShortDate(object date)
        {
            if (date is DateTime? && ((DateTime?)date).HasValue)
            {
                return ((DateTime?)date).Value.ToShortDateString();
            }

            return null;
        }

        public static string GetLongDate(object date)
        {
            if (date is DateTime? && ((DateTime?)date).HasValue)
            {
                return ((DateTime?)date).Value.ToLongDateString();
            }

            return null;
        }

        public static string GetPeriodString(DateTime? dateBeginning, DateTime? dateEnd)
        {
            if (!dateEnd.HasValue)
            {
                return dateBeginning.ToStringOrDefault(string.Empty);
            }

            if (!dateBeginning.HasValue)
            {
                return dateEnd.ToStringOrDefault(string.Empty);
            }

            return string.Format("с {0} по {1}", dateBeginning.ToStringOrDefault(string.Empty), dateEnd.ToStringOrDefault(string.Empty));
        }
    }
}