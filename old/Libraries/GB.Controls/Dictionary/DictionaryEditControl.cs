﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallback;

namespace GB.Controls.Dictionary
{
    public class DictionaryEditControl : CompositeControl
    {
        #region ChildsControl
        protected HtmlGenericControl container;
        protected ASPxLabel ErrLabel;
        protected Table Table;
        //protected ASPxButton ASPxButtonOk;
        //protected ASPxButton ASPxButtonCancel;
        protected DictionaryReferenceControl _DictionaryReference;
        protected ASPxCallback Callback;
        #endregion

        #region Constants
        public const string DictIdKey = "DictId";
        public const string ItemIdKey = "ItemId";
        public const string ModeKey = "Mode"; 
        #endregion

        #region Properties
        int DictId
        {
            get
            {
                if (ViewState[DictIdKey] != null)
                {
                    return Convert.ToInt32(ViewState[DictIdKey]);
                }

                return -1;
            }
            set
            {
                ViewState[DictIdKey] = value;
            }
        }

        int ItemId
        {
            get
            {
                if (ViewState[ItemIdKey] != null)
                {
                    return Convert.ToInt32(ViewState[ItemIdKey]);
                }

                return -1;
            }
            set
            {
                ViewState[ItemIdKey] = value;
            }
        }

        DictionaryEditMode Mode
        {
            get
            {
                if (ViewState[ModeKey] != null)
                {
                    return (DictionaryEditMode)ViewState[ModeKey];
                }

                return DictionaryEditMode.Insert;
            }
            set
            {
                ViewState[ModeKey] = value;
            }
        }

        public int UserId { get; set; }

        #endregion

        public DictionaryEditControl()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Callback = new ASPxCallback();
            Callback.Callback += Callback_Callback;

            container = new HtmlGenericControl("div");
            container.Attributes.Add("class", "page");

            ErrLabel = new ASPxLabel { ID = "ErrLabel", Visible = false, ForeColor = Color.Red };

            Table = new Table();
            Table.CssClass = "form_edit";

            //ASPxButtonOk = new ASPxButton { ID = "ASPxButtonOk", Text = "Сохранить" };
            //ASPxButtonOk.Click += ASPxButtonOk_Click;

            //ASPxButtonCancel = new ASPxButton { ID = "ASPxButtonCancel", Text = "Отменить", AutoPostBack = false };

            _DictionaryReference = new DictionaryReferenceControl { ID = "_DictionaryReference", Visible = false, EnableViewState = true };
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            Controls.Add(container);
            Controls.Add(Callback);
            Controls.Add(_DictionaryReference);
            
            ErrLabel.Style.Add("font-weight", "bold");
            container.Controls.Add(ErrLabel);

            container.Controls.Add(Table);

            if (Page.Request.QueryString[DictIdKey] != null)
            {
                DictId = Convert.ToInt32(Page.Request.QueryString[DictIdKey]);
            }

            if (Page.Request.QueryString[ItemIdKey] != null)
            {
                ItemId = Convert.ToInt32(Page.Request.QueryString[ItemIdKey]);
            }

            if (Page.Request.QueryString[ModeKey] != null && Page.Request.QueryString[ModeKey].Length > 0)
            {
                Mode = (DictionaryEditMode)Enum.Parse(typeof(DictionaryEditMode), Page.Request.QueryString[ModeKey]);
            }
            else
            {
                SetError("Не могу определить режим работы.");
                return;
            }

            switch (Mode)
            {
                case DictionaryEditMode.Update:
                    BuildForm();
                    BuildData();
                    break;
                case DictionaryEditMode.Insert:
                    BuildForm();
                    break;
            }

            var row = new TableRow();
            var cell = new TableCell();
            cell.CssClass = "form_edit_buttons";
            cell.ColumnSpan = 2;

            HtmlGenericControl buttomOk = new HtmlGenericControl("span");
            buttomOk.Attributes.Add("class", "ok");
            buttomOk.InnerHtml = string.Concat("<a onclick=\"", Callback.ClientID, ".PerformCallback(); \">Сохранить</a>");
            cell.Controls.Add(buttomOk);

            HtmlGenericControl buttomCancel = new HtmlGenericControl("span");
            buttomCancel.Attributes.Add("class", "cancel");
            buttomCancel.InnerHtml = string.Concat("<a onclick=\"window.location.href ='", DictionaryControl.GetUrl(DictId), "' \">Отмена</a>");
            cell.Controls.Add(buttomCancel);
            
            row.Cells.Add(cell);
            Table.Rows.Add(row);
        }

        //protected override void OnPreRender(EventArgs e)
        //{
        //}

        //protected override void OnLoad(EventArgs e)
        //{
        //    base.OnLoad(e);




        //    base.OnPreRender(e);

        //    //ASPxButtonCancel.ClientSideEvents.Click = string.Concat("function(s, e) { window.location.href = '",
        //    //                                                            (DictionaryControl.GetUrl(DictId)),
        //    //                                                            "'; }");
        //}

        public static string GetUrl(int Id)
        {
            return string.Concat("~/Administrator/DictionaryEdit.aspx?", DictIdKey, "=", Id, "&", ModeKey, "=", DictionaryEditMode.Insert);
        }

        public static string GetUrl(int Id, int itemId)
        {
            return string.Concat("~/Administrator/DictionaryEdit.aspx?", DictIdKey, "=", Id, "&", ItemIdKey, "=", itemId, "&", ModeKey, "=", DictionaryEditMode.Update);
        }

        private void SetError(string error)
        {
            ErrLabel.Visible = true;
            ErrLabel.Text = error;
        }

        private void BuildForm()
        {
            MetaDictionary Dict = MetaDictionaries.ListDictionaryItem[DictId];

            foreach (var item in Dict.ListColumns.Values)
            {
                if (item.Visible)
                {
                    TableRow row = new TableRow();

                    TableCell celll = new TableCell();
                    //ASPxLabel label = new ASPxLabel();
                    //label.Text = item.Name;
                    //celll.Controls.Add(label);
                    celll.Text = item.Name;
                    celll.CssClass = "form_edit_desc";
                    row.Cells.Add(celll);

                    switch (item.Type)
                    {
                        case ColumnType.TBool:
                            TableCell cell6 = new TableCell();
                            ASPxCheckBox checkBox = new ASPxCheckBox();
                            checkBox.ID = item.ColumnName;
                            cell6.CssClass = "form_edit_input";
                            cell6.Controls.Add(checkBox);
                            row.Cells.Add(cell6);
                            break;
                        case ColumnType.TString:
                        case ColumnType.TInt:
                        case ColumnType.TDecimal:
                            TableCell cell2 = new TableCell();
                            ASPxTextBox textbox = new ASPxTextBox();
                            textbox.ID = item.ColumnName;
                            cell2.CssClass = "form_edit_input";
                            cell2.Controls.Add(textbox);
                            row.Cells.Add(cell2);
                            break;
                        case ColumnType.TBigString:
                            TableCell cell5 = new TableCell();
                            ASPxMemo memo = new ASPxMemo();
                            memo.ID = item.ColumnName;
                            cell5.CssClass = "form_edit_input";
                            cell5.Controls.Add(memo);
                            row.Cells.Add(cell5);
                            break;
                        case ColumnType.TDateTime:
                            TableCell celldt = new TableCell();
                            ASPxDateEdit edit = new ASPxDateEdit();
                            edit.ID = item.ColumnName;
                            celldt.CssClass = "form_edit_input";
                            celldt.Controls.Add(edit);
                            row.Cells.Add(celldt);
                            break;
                        case ColumnType.TRef:
                            TableCell cell3 = new TableCell();

                            HiddenField hiddenField = new HiddenField();
                            hiddenField.ID = "HiddenField_" + item.ColumnName;
                            hiddenField.Value = "";
                            cell3.Controls.Add(hiddenField);

                            ASPxTextBox rtextbox = new ASPxTextBox();
                            rtextbox.ID = item.ColumnName;
                            rtextbox.ReadOnly = true;
                            rtextbox.Style.Add("float", "left");
                            cell3.CssClass = "form_edit_input";
                            cell3.Controls.Add(rtextbox);
                            

                            //TableCell cell4 = new TableCell();

                            ASPxButton button = new ASPxButton();
                            button.Click += new EventHandler(button_Click);
                            button.ID = "btnChoise" + item.ColumnName;
                            button.Text = "...";
                            button.Width = 20;
                            button.CommandName = item.ColumnName + "," + item.RefId.ToString() + "," + item.RefColumn;
                            cell3.Controls.Add(button);

                            //row.Cells.Add(cell4);
                            row.Cells.Add(cell3);
                            break;
                        case ColumnType.TChild:
                            row.Cells.Clear();
                            break;
                        default:
                            throw new NotSupportedException("Тип не поддерживается: " + item.Type);
                    }

                    Table.Rows.Add(row);
                }
            }
        }

        void button_Click(object sender, EventArgs e)
        {
            string[] mstr = (sender as ASPxButton).CommandName.Split(',');

            _DictionaryReference.ColumnName = mstr[0];
            _DictionaryReference.DictionaryID = int.Parse(mstr[1]);
            _DictionaryReference.DictionaryFieldID = int.Parse(mstr[2]);

            _DictionaryReference.ClearGrid();
            _DictionaryReference.Visible = true;

            _DictionaryReference.UpdateTableColums();
        }

        private void BuildData()
        {
            MetaDictionary Dict = MetaDictionaries.ListDictionaryItem[DictId];

            string error = string.Empty;

            DataTable dt = DictionaryHelper.GetDataDictionaryItem(Dict, ItemId, ref error);

            if (error.Length > 0)
            {
                SetError(error);
                return;
            }

            foreach (var item in Dict.ListColumns.Values)
            {
                Control contr = DictionaryHelper.FindControl(Table, item.ColumnName);//Table.FindControl(item.ColumnName);

                if (contr != null)
                {
                    switch (item.Type)
                    {
                        case ColumnType.TString:
                            (contr as ASPxTextBox).Text = dt.Rows[0][item.ColumnName].ToString();
                            break;
                        case ColumnType.TBigString:
                            (contr as ASPxMemo).Text = dt.Rows[0][item.ColumnName].ToString();
                            break;
                        case ColumnType.TInt:
                            (contr as ASPxTextBox).Text = dt.Rows[0][item.ColumnName].ToString();
                            break;
                        case ColumnType.TDecimal:
                            (contr as ASPxTextBox).Text = dt.Rows[0][item.ColumnName].ToString();
                            break;
                        case ColumnType.TDateTime:
                            (contr as ASPxDateEdit).Value = dt.Rows[0][item.ColumnName] is DBNull ? null : (DateTime?)Convert.ToDateTime(dt.Rows[0][item.ColumnName]);
                            break;
                        case ColumnType.TRef:
                            Control cont = DictionaryHelper.FindControl(Table, "HiddenField_" + item.ColumnName);
                            (cont as HiddenField).Value = dt.Rows[0][item.ColumnName].ToString();
                            (contr as ASPxTextBox).Text = dt.Rows[0][DictionaryHelper.RefPrefix + item.ColumnName].ToString();
                            break;
                        case ColumnType.TBool:
                            (contr as ASPxCheckBox).Checked = (dt.Rows[0][item.ColumnName].ToString() == "True");
                            break;
                        default:
                            throw new NotSupportedException("Тип не поддерживается: "+ item.Type.ToString()) ;
                    }
                }
            }
        }

        private void Callback_Callback(object source, CallbackEventArgs e)
        {
            MetaDictionary Dict = MetaDictionaries.ListDictionaryItem[DictId];

            string error = string.Empty;

            switch (Mode)
            {
                case DictionaryEditMode.Update:
                    error = DictionaryHelper.UpdateDictionaryItem(Dict, ItemId, Table, UserId);
                    break;
                case DictionaryEditMode.Insert:
                    error = DictionaryHelper.InsertDictionaryItem(Dict, Table, UserId);
                    break;
            }


            if (error.Length > 0)
            {
                SetError(error);
                return;
            }

            DevExpress.Web.ASPxGridView.ASPxGridView.RedirectOnCallback(DictionaryControl.GetUrl(DictId));
        }
    }

    public enum DictionaryEditMode
    {
        Insert,
        Update
    }
}
