﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Repository;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using bgTeam.IsRui.Domain.Entities.Documents;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class CommentListViewModel : ViewModelBase
    {
        public IEnumerable<CommentViewModel> Comments { get; set; }

        public CommentListViewModel(int id)
        {
            Comments = new RepositoryDc<AccountingObjectUserComment>().GetAllEx(q => q
                .OrderBy(x => x.Id).Asc                
                .Fetch(x => x.User).Eager
                .Fetch(x => x.Status).Eager
                .Fetch(x => x.Documents).Eager
                .TransformUsing(CriteriaSpecification.DistinctRootEntity)
                .Where(x => x.AccountingObject.Id == id && x.Status != null))
                .Select(x => new CommentViewModel(x));
        }
    }

    public class CommentViewModel
    {
        public string User { get; set; }

        public string Status { get; set; }

        public string Comment { get; set; }

        public IEnumerable<Files> Documents { get; set; }


        public CommentViewModel(AccountingObjectUserComment x)
        {
            User = x.User.FullName;
            Status = x.Status.Name;
            Comment = x.Comment;

            Documents = x.Documents/*.Select(d => LinkBuilder.Link(d))*/;
        }
    }
}