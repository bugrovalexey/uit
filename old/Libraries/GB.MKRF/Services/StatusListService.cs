﻿using GB.Data.Common;
using GB.Data.Workflow;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    public class StatusListService : ServiceBase<StatusList>, IService<StatusList, StatusListArgs>
    {
        public StatusList Create(StatusListArgs args)
        {
            var entity = new StatusList();
            return SaveOrUpdate(entity, args);
        }

        public StatusList Update(StatusListArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private StatusList SaveOrUpdate(StatusList entity, StatusListArgs args)
        {
            entity.Role = GetEntity<Role>(args.RoleId);
            entity.From = GetEntity<Status>(args.FromId);
            entity.To = GetEntity<Status>(args.ToId);

            base.SaveOrUpdate(entity);
            return entity;
        }

        public void Delete(StatusListArgs args)
        {
            base.Delete(args.Id);
        }
    }
}