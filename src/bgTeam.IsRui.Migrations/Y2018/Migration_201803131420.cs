﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201803131421)]
    public class Migration_201803131420 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("BG_Files")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("EntityType").AsInt32().NotNullable()
                .WithColumn("EntityOwnerId").AsInt32().NotNullable()
                .WithColumn("Type").AsInt32().NotNullable()
                .WithColumn("Kind").AsInt32().NotNullable()
                
                .WithColumn("Name").AsString(1000).NotNullable()
                .WithColumn("Extension").AsString(10).Nullable()
                .WithColumn("Size").AsInt32().Nullable()
                .WithColumn("Url").AsString(1000).Nullable()
                .WithColumn("MimeType").AsString(50).Nullable()

                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "BG_Files" });
        }
    }
}
