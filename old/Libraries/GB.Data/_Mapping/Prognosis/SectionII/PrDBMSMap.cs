﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Prognosis.SectionII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionII
{
    public class PrDBMSMap : ClassMapBase<PrDBMS>
    {
        public PrDBMSMap()
            : base("MF2_DBMS")
        {
//            Table("w_");

//            Id(x => x.Id, "MF2_DBMS_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF2_DBMS_Insert
//                          @MF2_DBMS_CA = ?
//                         ,@MF2_DBMS_TP = ?
//                         ,@MF2_DBMS_Comm = ?
//                         ,@DBMS_Type_ID = ?
//                         ,@MF2_ID = ?
//                         ,@MF2_DBMS_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF2_DBMS_Update
//                          @MF2_DBMS_CA = ?
//                         ,@MF2_DBMS_TP = ?
//                         ,@MF2_DBMS_Comm = ?
//                         ,@DBMS_Type_ID = ?
//                         ,@MF2_ID = ?
//                         ,@MF2_DBMS_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF2_DBMS_Delete
//						  @MF2_DBMS_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.CountCA, "MF2_DBMS_CA");
            Map(x => x.CountTP, "MF2_DBMS_TP");
            Map(x => x.Comment, "MF2_DBMS_Comm");

            References<DBMSType>(x => x.DBMS, "DBMS_Type_ID").Not.Nullable();
            References<TechInfo>(x => x.TechInfo, "MF2_ID").Not.Nullable();
        }
    }
}
