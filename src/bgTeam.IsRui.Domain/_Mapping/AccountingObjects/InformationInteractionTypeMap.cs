﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    internal class InformationInteractionTypeMap : ClassMapHiLo<InformationInteractionType>
    {
        public InformationInteractionTypeMap()
            : base("Information_Exchange_Type")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Information_Exchange_Type_Name");
        }
    }
}