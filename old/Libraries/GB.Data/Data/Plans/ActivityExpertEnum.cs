﻿using System.ComponentModel;

namespace GB.Data.Plans
{
    public enum ActivityExpertEnum
    {
        [Description("Не задано")]
        None = 0,

        /// <summary>Экономист</summary>
        [Description("Экономист")]
        Economist = 1,

        /// <summary>Тех. специалист</summary>
        [Description("Тех. специалист")]
        Technician = 2
    }
}