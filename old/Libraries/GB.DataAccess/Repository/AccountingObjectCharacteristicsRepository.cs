﻿using System.Collections.Generic;
using GB.Data.AccountingObjects;

namespace GB.DataAccess.Repository
{
    public class AccountingObjectCharacteristicsRepository : Repository<AccountingObjectCharacteristics>
    {
        public IList<AccountingObjectCharacteristics> GetAllByAccountingObject(int? accountingObjectId)
        {
            return GetAllEx(q => q
                .Fetch(x => x.Type).Eager
                .Fetch(x => x.Unit).Eager
                .Where(x => x.AccountingObject.Id == accountingObjectId && x.IsRegister));
        }

        public IList<AccountingObjectCharacteristics> GetAllByAccountingObjects(List<int> accountingObjectsIds)
        {
            return GetAllEx(q => q
                .Fetch(x => x.Type).Eager
                .Fetch(x => x.Unit).Eager
                .Where(x => x.IsRegister)
                .WhereRestrictionOn(x => x.AccountingObject.Id).IsIn(accountingObjectsIds));
        }
    }
}