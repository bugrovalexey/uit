﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.ExternalIsAndComponentItkis
{
    public class AddOrSaveExternalIsAndComponentItkiContext : BaseAddOrSaveContext
    {        
        public int AccountingObjectId { get; set; }
        
        public string Name { get; set; }
        
        public string Purpose { get; set; }
        
        public string Smev { get; set; }
        
        public string Url { get; set; }
    }
}
