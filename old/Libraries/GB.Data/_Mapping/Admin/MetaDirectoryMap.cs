﻿using GB.Data.Admin;

namespace GB._Mapping.Admin
{
    internal class MetaDirectoryMap : ClassMapBase<MetaDirectory>
    {
        public MetaDirectoryMap()
            : base("Meta_Dir")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Name");
            Map(x => x.ViewName, "ViewName");
            Map(x => x.OrderIndex, "Sort");
            Map(x => x.ProcInsert, "ProcedureInsert");
            Map(x => x.ProcUpdate, "ProcedureUpdate");
            Map(x => x.ProcDelete, "ProcedureDelete");
            Map(x => x.InUse, "FieldUse_InUse");
            Map(x => x.Visible, "Visible");
            Map(x => x.Readonly, "ReadOnly");

            HasMany(x => x.Columns).KeyColumns.Add("Meta_Dir_ID").Inverse();
        }
    }
}