﻿namespace bgTeam.IsRui.Story.Claims
{
    public class ClaimCreateStoryContext
    {
        /// <summary>
        /// Идентификатор связанного мероприятия
        /// </summary>
        public int? ActivityId { get; set; }
    }
}
