﻿using GB.Data.Directories;
using GB.Data.Plans;

namespace GB._Mapping.Plans
{
    internal class WorkTypeMap : ClassMapHiLo<WorkType>
    {
        public WorkTypeMap()
            : base("Work_Exp_Type")
        {
        }

        protected override void InitMap()
        {
            //Map(x => x.Name, "Work_Exp_Type_Name").Nullable();
            Map(x => x.ExpenseVolumeYear0, "Work_Exp_Type_Expense_Vol_Y0");
            Map(x => x.ExpenseVolumeYear1, "Work_Exp_Type_Expense_Vol_Y1");
            Map(x => x.ExpenseVolumeYear2, "Work_Exp_Type_Expense_Vol_Y2");

            References<PlansWork>(x => x.PlanWork, "Plans_Work_ID");
            References<ExpenseType>(x => x.ExpenseType, "Expense_Type_ID");
        }
    }
}