﻿namespace bgTeam.IsRui.Story.Activities
{
    public class CreateActivityStoryContext
    {
        public string Name { get; set; }

        public int TypeId { get; set; }

        public int? Year { get; set; }

        public int? AoId { get; set; }
    }
}