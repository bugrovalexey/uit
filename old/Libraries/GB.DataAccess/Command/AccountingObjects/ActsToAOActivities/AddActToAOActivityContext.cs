﻿namespace GB.DataAccess.Command.AccountingObjects.ActsToAOActivities
{
    public class AddActToAOActivityContext : ICommandContext
    {
        public int AoActivityId { get; set; }

        public int ActId { get; set; }
    }
}
