﻿using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Statuses;
using System.Collections.Generic;

namespace GB.MKRF.Entities.Projects
{
    /// <summary>Проект по информатизации</summary>
    public class Project : EntityBase
    {
        #region Общие сведения

        /// <summary>Полное наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Краткое наименование (NOT NULL)</summary>
        public virtual string ShortName { get; set; }

        /// <summary>Описание</summary>
        public virtual string Description { get; set; }

        /// <summary>Учреждение ответственное за проект (NOT NULL)</summary>
        public virtual Department Department { get; set; }

        //Структурное подразделение, ответственное за проект
        public virtual string Subdivision { get; set; }

        /// <summary>Сотрудник ответственный за проект (NOT NULL)</summary>
        public virtual User ResponsibleUser { get; set; }

        /// <summary>Сотрудник подразделения, подписывающего проект</summary>
        public virtual User Signer { get; set; }

        /// <summary>Статус</summary>
        public virtual Status Status { get; set; }

        /// <summary>Сумма планируемых расходов</summary>
        public virtual decimal SumOfExpenses { get; set; }

        /// <summary>Сотрудники подразделения, участвующих в проекте</summary>
        public virtual IList<User> InvolvedEmployees { get; set; }

        /// <summary>Логотип</summary>
        public virtual IList<Files> Logo { get; protected set; }

        #endregion Общие сведения

        #region Основания

        /// <summary>Государственная программа</summary>
        public virtual StateProgram StateProgram { get; set; }

        /// <summary>Подпрограмма государственной программы</summary>
        public virtual StateProgram StateSubProgram { get; set; }

        /// <summary>Основное мероприятие государственной программы</summary>
        public virtual StateProgram StateMainEvent { get; set; }

        /// <summary>Другие основания проекта</summary>
        //public virtual IList<Reason> Reasons { get; protected set; }

        #endregion Основания
    }
}