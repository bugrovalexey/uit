﻿namespace bgTeam.IsRui.WebApp.ViewModels
{
    public class AutocompleteInfo
    {
        public int id { get; set; }

        public object  value { get; set; }

        public object label { get; set; }
    }
}