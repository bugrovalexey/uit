﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class PlansScheduleMap : ClassMapBase<PlansSchedule>
    {
        public PlansScheduleMap()
            : base("Plans_Schedule")
        {
//            Table("w_");

//            Id(x => x.Id, "Plans_Schedule_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string args = @"
//                         @Plans_Schedule_Subject_Name = ?
//                        ,@Plans_Schedule_Exec_Time = ?
//                        ,@Plans_Schedule_Num = ?
//                        ,@Plans_Schedule_Cost = ?
//                        ,@Plans_ID = ?
//                        ,@Plans_Activity_ID = ?
//                        ,@Plans_Spec_ID = ?
//                        ,@Plans_Work_ID = ?
//                        ,@OKDP_ID = ?
//                        ,@Plans_Schedule_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Plans_Schedule_Insert" + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Plans_Schedule_Update" + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Plans_Schedule_Delete
//						  @Plans_Schedule_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.ContractSubject, "Plans_Schedule_Subject_Name");
            Map(x => x.ExecTime, "Plans_Schedule_Exec_Time").Nullable();
            Map(x => x.Num, "Plans_Schedule_Num");
            Map(x => x.Cost, "Plans_Schedule_Cost");
            Map(x => x.OKDP_Value, "Plans_Schedule_OKDP_Value");

            References<Plan>(x => x.Plan, "Plans_ID");
            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID");
            References<PlansSpec>(x => x.PlansSpec, "Plans_Spec_ID").Nullable();
            References<PlansWork>(x => x.Work, "Plans_Work_ID").Nullable();
            //References<OKDP>(x => x.OKDP, "OKDP_ID").Nullable();
        }
    }
}
