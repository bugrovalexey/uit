﻿using Uviri.MKRF.Entities.Expertise;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpFormHelp6SpecMap : ClassMapBase<ExpFormHelp6Spec>
    {
        public ExpFormHelp6SpecMap()
            : base("Exp_Concl_Sprv_6_Spec")
        {
//            Table("w_");

//            Id(x => x.Id, "Exp_Concl_Sprv_6_Spec_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@Exp_Concl_Sprv_6_Spec_Equiv = ?
//                          ,@Exp_Concl_Sprv_6_Spec_Comm = ?
//                          ,@Plans_Spec_ID = ?
//                          ,@Exp_Concl_Sprv_ID = ?
//						  ,@Exp_Concl_Sprv_6_Spec_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Exp_Concl_Sprv_6_Spec_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Concl_Sprv_6_Spec_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Exp_Concl_Sprv_6_Spec_Delete @Exp_Concl_Sprv_6_Spec_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Flag, "Exp_Concl_Sprv_6_Spec_Equiv");
            Map(x => x.Comment, "Exp_Concl_Sprv_6_Spec_Comm");

            References<PlansSpec>(x => x.PlansSpec, "Plans_Spec_ID").Not.Nullable();
            References<ExpFormHelp>(x => x.FormHelp, "Exp_Concl_Sprv_ID").Not.Nullable();
        }
    }
}
