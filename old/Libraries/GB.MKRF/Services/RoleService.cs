﻿using GB.Data.Common;
using GB.MKRF.Helpers;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    public sealed class RoleService : ServiceBase<Role>, IService<Role, RoleArgs>
    {
        public Role Create(RoleArgs args)
        {
            return SaveOrUpdate(new Role(), args);
        }

        public Role Update(RoleArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private Role SaveOrUpdate(Role entity, RoleArgs args)
        {
            entity.FillRole(args);

            base.SaveOrUpdate(entity);
            return entity;
        }

        public void Delete(RoleArgs args)
        {
            base.Delete(args.Id);
        }
    }
}