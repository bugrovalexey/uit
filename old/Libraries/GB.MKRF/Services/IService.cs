﻿using GB.Data;
using GB.MKRF.Repository;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    public interface IService<TEntity, TArgs>
        where TEntity : EntityBase
        where TArgs : IServiceArgs
    {
        TEntity Create(TArgs args);

        TEntity Update(TArgs args);

        void Delete(TArgs args);

        Repository<TEntity> Repository { get; }
    }
}