﻿namespace bgTeam.IsRui.Common.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Common.Extentions;
    using Firefly.SimpleXls;

    public interface IBuildXLS
    {
        void ExportToXLS(List<XlsOrderViewModelExtension> orders);
    }
}
