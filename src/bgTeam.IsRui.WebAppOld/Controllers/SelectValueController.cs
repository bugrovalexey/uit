﻿using System.Web.Mvc;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public class SelectValueController : Controller
    {
        /// <summary>
        /// Возвращает среднюю зарплату по выбранному ОКВЭД
        /// </summary>
        /// <param name="id">id ОКВЭД</param>
        /// <remarks>Используется в WorkEdit.cshtml</remarks>
        public ActionResult GetSalaryFromOKVED(int id)
        {
            var okved = new RepositoryDc<OKVED>().GetExisting(id);
            return Json(okved.Salary, JsonRequestBehavior.AllowGet);
        }
    }
}