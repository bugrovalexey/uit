﻿using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Projects;
using GB.MKRF.Entities.Statuses;

namespace GB.MKRF.Mapping.Projects
{
    class ProjectMap : ClassMapBase<Project>
    {
        public ProjectMap()
            : base("Project")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Project_FName").Nullable();
            Map(x => x.ShortName, "Project_SName").Not.Nullable();
            Map(x => x.SumOfExpenses, "Project_Sum").Not.Nullable();
            Map(x => x.Description, "Project_Description").Nullable();
            Map(x => x.Subdivision, "Project_Organ").Nullable();

            //Map(x => x.StateProgram );
            //Map(x => x.StateSubProgram);
            //Map(x => x.StateMainEvent);

            References<Status>(x => x.Status, "Status_ID");
            References<Department>(x => x.Department, "Govt_Organ_ID");
            References<User>(x => x.ResponsibleUser, "Responsible_ID");
            References<User>(x => x.Signer, "Users_Podp_ID");

            HasMany<Files>(x => x.Logo)
                .KeyColumns.Add("Document_Owner_ID")
                .Where("Doc_Type_ID = " + (int)DocTypeEnum.Logo)
                .Where("Portal_Table_ID = " + (int)TableTypeEnum.Project)
                .Inverse();

            //HasMany<Reason>(x => x.Reasons)
            //    .KeyColumns.Add("Project_ID")
            //    .Inverse();

            HasManyToMany(x => x.InvolvedEmployees)
                 .Table("Project_Users")
                 .ParentKeyColumn("Project_ID")
                 .ChildKeyColumn("Users_ID")
                 .Where("Is_Deleted = 0")
                 .LazyLoad();
        }
    }
}