﻿using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Story.Common;
using System.Linq;
using Test.bgTeam.IsRui.Unit.Common;
using Xunit;

namespace Test.bgTeam.IsRui.Unit.Tests
{
    public class TestsWorkflowStory
    {
        private Factory _fac;

        public TestsWorkflowStory()
        {
            _fac = new Factory();
        }

        [Fact]
        public void Story_WorkflowUserConclusionGetByIdStory()
        {
            var story = new WorkflowUserConclusionGetByIdStory(_fac.storyMapper, _fac.repositoryRui, _fac.userRepository);
            var res = story.Execute(new WorkflowUserConclusionGetByIdStoryContext
            {
                Id = 6,
            });

            Assert.NotNull(res);
            Assert.Equal(6, res.Id);
            Assert.Equal(0, res.StatusId);
            Assert.Equal("Test id", res.Text);
        }

        [Fact]
        public void Story_WorkflowConclusionGetByOwnerIdStory()
        {
            var story = new WorkflowUserConclusionGetByOwnerIdStory(_fac.storyMapper, _fac.repositoryRui, _fac.userRepository);
            var res = story.Execute(new WorkflowUserConclusionGetByOwnerIdStoryContext
            {
                EntityType = (int)EntityType.PlansActivity,
                EntityOwnerId = 36,
                StatusId = 1
            });

            Assert.NotNull(res);
            Assert.True(res.Any());

            var res1 = res.First();

            Assert.Equal(6, res1.Id);
            Assert.Equal(0, res1.StatusId);
            Assert.Equal("Test id", res1.Text);
        }

        [Fact]
        public void Story_WorkflowStatusGetNextListStory()
        {
            var story = new WorkflowStatusGetNextListStory(_fac.storyMapper, _fac.repositoryRui, _fac.userRepository);
            var res = story.Execute(new WorkflowStatusGetNextListStoryContext
            {
                WorkflowId = 10
            });

            Assert.NotNull(res);
            Assert.True(res.Any());

            var res1 = res.First();

            Assert.NotEqual(0, res1.Id);
            Assert.NotNull(res1.Name);
        }

        [Fact]
        public void Story_WorkflowStatusChangeStory()
        {
            var story = new WorkflowStatusChangeStory(_fac.storyMapper, _fac.repositoryRui, _fac.userRepository, _fac.sessionFactory);

            try
            {
                var res = story.Execute(new WorkflowStatusChangeStoryContext
                {
                    WorkflowId = 10,
                    StatusId = 101,
                });

                Assert.NotNull(res);
                Assert.NotEqual(0, res.StatusId);
                Assert.NotEqual(0, res.EntityType);
                Assert.NotEqual(0, res.EntityOwnerId);
            }
            catch
            {
                var res102 = story.Execute(new WorkflowStatusChangeStoryContext
                {
                    WorkflowId = 10,
                    StatusId = 102,
                });

                Assert.NotNull(res102);
                Assert.NotEqual(0, res102.StatusId);
                Assert.NotEqual(0, res102.EntityType);
                Assert.NotEqual(0, res102.EntityOwnerId);

            }

            
        }
    }
}