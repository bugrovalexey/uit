﻿using Uviri.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class SettingsMap : ClassMapBase<Settings>
    {
        public SettingsMap2()
            : base("Settings")
		{
		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Settings_Name").ReadOnly();
            Map(x => x.Value, "Settings_Value");
            Map(x => x.Type, "Settings_Type").CustomType(typeof(SettingsTypeEnum)).Not.Nullable().ReadOnly();
        }
    }
}