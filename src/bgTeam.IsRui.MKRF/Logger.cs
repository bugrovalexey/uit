﻿namespace bgTeam.IsRui.Common
{
    using System;
    using System.Linq;
    using log4net;
    using log4net.Config;

    //ToDo: remove this classes, switch to DI logging

    /// <summary>
    /// Данный класс нужен для именования логгера, который описан в web.config
    /// <remarks>Если класс изменил namespace, но надо исправить полное имя логера в web.config </remarks>
    /// </summary>
    public class LoggerBase
    {
    }

    public class Logger : Logger<LoggerBase>
    {
    }

    public class Logger<T>
        where T : class
    {
        private static string _level = "\t\t\t";
        private static ILog _logger = Init();

        private static ILog Init()
        {
            var assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.FullName.StartsWith("Coord"));
            var logRepository = LogManager.GetRepository(assembly ?? typeof(LoggerBase).Assembly);
            if (assembly != null)
            {
                XmlConfigurator.Configure(logRepository, assembly.GetManifestResourceStream("GB.Coord.Configuration.log4net.config"));
            }

            return LogManager.GetLogger(typeof(T));
        }

        public static void Debug(string message)
        {
            _logger.Debug(string.Concat(_level, message));
        }

        public static void Info(string message, params object[] args)
        {
            var mess = string.Format(message, args);

            _logger.Info(string.Concat(_level, mess));
        }

        public static void Error(string message)
        {
            _logger.Error(message);
        }

        public static void Error(Exception exp)
        {
            _logger.Error(exp.Message, exp);
        }

        public static void Error(string message, Exception exp)
        {
            _logger.Error(message, exp);
        }

        public static void Fatal(string message)
        {
            _logger.Fatal(message);
        }

        public static void Fatal(Exception exp)
        {
            _logger.Fatal(exp.Message, exp);
        }

        public static void Fatal(string message, Exception exp)
        {
            _logger.Fatal(message, exp);
        }

        //private static string LogEx(Exception exception)
        //{
        //    return string.Format("{0}Error: {1}; {0} StackTrace: {2}; {0} InnerException: {3} {0}",
        //                               Environment.NewLine,
        //                               exception.Message,
        //                               exception.StackTrace,
        //                               exception.InnerException == null ? "none" : LogEx(exception.InnerException));
        //}
    }
}