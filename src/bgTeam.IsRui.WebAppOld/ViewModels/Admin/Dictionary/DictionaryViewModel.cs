﻿using bgTeam.IsRui.MKRF.DictyonaryEdit;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin.Dictionary
{
    public class DictionaryViewModel : AdminViewModel
    {
        public SelectList Dictionaries { get; set; }

        public MetaDictionary Dictionary { get; set; }
    }
}