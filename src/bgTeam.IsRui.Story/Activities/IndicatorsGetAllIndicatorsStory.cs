﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class IndicatorsGetAllIndicatorsStory : IStory<IndicatorsGetAllIndicatorsStoryContext, IEnumerable<ActivityIndicatorDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public IndicatorsGetAllIndicatorsStory(IRepositoryRui repository, IMapperBase mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<ActivityIndicatorDto> Execute(IndicatorsGetAllIndicatorsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivityIndicatorDto>> ExecuteAsync(IndicatorsGetAllIndicatorsStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var indicators = _repository.GetAllEx<PlansActivityIndicator>(
                q => q.Where(x => x.PlansActivity.Id == context.ActivityId))
                .Select(x => _mapper.Map(x, new ActivityIndicatorDto()));

            return indicators;
        }
    }
}
