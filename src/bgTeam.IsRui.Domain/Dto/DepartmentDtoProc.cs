﻿namespace bgTeam.IsRui.Domain.Dto
{
    public class DepartmentDtoProc
    {
        public int Id { get; set; }

        /// <summary>
        /// Код
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Addr { get; set; }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        public string NameSmall { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }

        /// <summary
        /// >Веб-сайт
        /// </summary>
        public string Web { get; set; }

        /// <summary>
        /// Признак активности
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Сводный перечень заказчиков
        /// </summary>
        public string SPZ { get; set; }

        public int? ParentId { get; set; }
    }
}
