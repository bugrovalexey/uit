﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GB.Data.Directories;
using GB.Data.Plans;
using GB.DataAccess.Repository;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class PlansSpecService : ServiceBase<PlansSpec>, IService<PlansSpec, PlansSpecArgs>
    {

        private static int GetNextNum(PlansActivity activity)
        {
            if (activity.Specifications.Count == 0)
                return 1;

            return activity.Specifications.OrderByDescending(i => i.OrigNum).First().OrigNum + 1;
        }

        public PlansSpec Create(PlansSpecArgs args)
        {
            var plansSpec = new PlansSpec();
            plansSpec.PlanActivity = GetEntity<PlansActivity>(args.ActivityId);
            plansSpec.Num = plansSpec.PlanActivity.Specifications.Count > 0 ? NumHelper<PlansSpec>.GetMinAvailableNum(plansSpec.PlanActivity.Specifications) : 1;
            plansSpec.OrigNum = GetNextNum(plansSpec.PlanActivity);

            var result = SaveOrUpdate(plansSpec, args);

            new PlansSpecCharacteristicsService().SetGoodsForCharacteristics(
                args.GoodsCharacteristicIds, result);

            return result;
        }

        public PlansSpec Update(PlansSpecArgs args)
        {
            return SaveOrUpdate(Repository.GetExisting(args.Id), args);
        }

        private PlansSpec SaveOrUpdate(PlansSpec plansSpec, PlansSpecArgs args)
        {
            if (args.ExpenseDirection == 0)
                throw new ArgumentException("Укажите Вид затрат");

            Mapper.Map(args, plansSpec);
            plansSpec.GRBS = new Repository<GRBS>().GetDefault();
            plansSpec.WorkForm = new Repository<WorkForm>().GetDefault();
            plansSpec.SectionKBK = new Repository<SectionKBK>().GetDefault();
            plansSpec.ExpenseItem = new Repository<ExpenseItem>().GetDefault();
            
            SaveOrUpdate(plansSpec);

            return plansSpec;
        }

        public void Delete(PlansSpecArgs args)
        {
            base.Delete(args.Id);
        }
    }

    public class PlansSpecArgs : BaseArgsNew
    {
        public PlansSpecArgs()
        {
            GoodsCharacteristicIds = new List<int>();
        }

        public string Name { get; set; }

        public int OKPD { get; set; }

        public int ProductGroup { get; set; }

        public int ExpenseDirection { get; set; }

        public int ExpenseType { get; set; }

        public int ExpenditureItem { get; set; }

        public YearEnum Year { get; set; }

        public int Quantity { get; set; }

        public int Duration { get; set; }

        public decimal Cost { get; set; }

        public int ActivityId { get; set; }

        public List<int> GoodsCharacteristicIds { get; set; } 
    }
}