﻿using System;
using System.Collections.Generic;
using GB.Data.Documents;
using GB.Data.AccountingObjects.WorksAndGoods;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Акт сдачи-приемки
    /// </summary>
    public class ActOfAcceptance : EntityBase
    {
        public ActOfAcceptance()
        {
            Documents = new List<Files>();            
        }

        /// <summary>
        /// Госконтракт 
        /// </summary>
        public virtual GovContract GovContract { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public virtual string DocName { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public virtual string DocNumber { get; set; }

        /// <summary>
        /// Дата подписания документа
        /// </summary>
        public virtual DateTime? DocDateAdoption { get; set; }

        /// <summary>
        /// Документ 
        /// </summary>
        public virtual IList<Files> Documents { get; set; }

        /// <summary>
        /// Стоимость 
        /// </summary>
        public virtual decimal Cost { get; set; }

        /// <summary>
        /// Причины отклонений от ожидаемых результатов
        /// </summary>
        public virtual string ReasonsWaitingResultsDeviation { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public virtual Plans.ActivityTypeEnum? ActivityType { get; set; }

        /// <summary>
        /// Связь с мероприятиями. Нужно для удаления каскадом, для маппинга.
        /// </summary>
        protected internal virtual IList<ActToAOActivity> ActToAOActivities { get; set; }  

        public virtual IList<WorkAndGoods> Works { get; protected set; }
    }
}
