﻿// -----------------------------------------------------------------------
// <copyright file="ElementRevisionInfo.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------


using System;
using System.Xml;

namespace Core.Import.NSIservice.Publish
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ElementRevisionInfo
    {
        /// <summary>
        /// Код Компонента НСИ.
        /// </summary>
        public string ComponentCode { get; set; }

        /// <summary>
        /// Код элемента справочника.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Дата начала действия версии элемента справочника.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Данные версии элемента справочника, передаваемые в Эталонный Банк.
        /// </summary>
        public XmlElement ElementXml { get; set; }
    }
}
