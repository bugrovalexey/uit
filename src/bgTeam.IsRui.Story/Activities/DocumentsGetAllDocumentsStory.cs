﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class DocumentsGetAllDocumentsStory : IStory<DocumentsGetAllDocumentsStoryContext, IEnumerable<ActivityDocumentDto>>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IMapperBase _mapper;

        public DocumentsGetAllDocumentsStory(IRepositoryRui repositoryRui, IMapperBase mapperBase)
        {
            _repositoryRui = repositoryRui;
            _mapper = mapperBase;
        }

        public IEnumerable<ActivityDocumentDto> Execute(DocumentsGetAllDocumentsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivityDocumentDto>> ExecuteAsync(DocumentsGetAllDocumentsStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var documents = _repositoryRui.GetAllEx<ActivityDocuments>(
                q => q.Where(x => x.Activity.Id == context.ActivityId));
            //.Select(x => _mapper.Map(x, new ActivityDocumentDto()));

            var documentsMap = _mapper.Map(documents, new List<ActivityDocumentDto>());

            foreach (ActivityDocumentDto doc in documentsMap)
            {
                var file = _repositoryRui.Get<Files>(doc.file.Id);

                doc.file.Url = file.Url;
                doc.file.MimeType = file.MimeType;
                doc.file.Kind = file.Kind;
                doc.file.Size = file.Size;
                doc.file.Extension = file.Extension;
            }

            return documentsMap;
        }
    }
}
