﻿using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class AoActivitiesViewModel
    {
        public AoActivitiesViewModel(int actId, int aoId, ActivityTypeEnum type, string selectListCallback)
        {
            var list = new RepositoryDc<AoActivity>().GetAllEx(q => q
                    .Where(x => x.AccountingObject.Id == aoId && x.ActivityType == type));
            var exludeIds =
                new RepositoryDc<ActToAOActivity>().Select<int>(
                    q => q.Where(x => x.ActOfAcceptance.Id == actId).Select(x => x.AoActivity.Id));

            list = list.Except(list.Where(x => exludeIds.Contains(x.Id))).ToList();
            Data = list.Select(x => new TableActivity(x));

            SelectListCallback = selectListCallback;
        }


        public IEnumerable<TableActivity> Data { get; set; }

        public string SelectListCallback { get; set; }
    }
}