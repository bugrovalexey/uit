﻿namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Целесообразность
    /// </summary>
    public class Appropriate: EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }
    }
}
