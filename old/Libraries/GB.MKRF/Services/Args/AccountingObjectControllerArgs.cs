﻿namespace GB.MKRF.Services.Args
{
    public interface IAccountingObjectControllerArgs
    {
        int Id { get; set; }

        int Type { get; set; }

        int Kind { get; set; }

        string ShortName { get; set; }

        string FullName { get; set; }
    }

    public class AccountingObjectControllerArgs : IAccountingObjectControllerArgs// : BaseArgs
    {
        public int Id { get; set; }

        public int Type { get; set; }

        public int Kind { get; set; }

        public string ShortName { get; set; }

        public string FullName { get; set; }
    }
}