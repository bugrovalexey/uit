﻿using System;

namespace bgTeam.IsRui.Domain.Dto
{
    /// <summary>
    /// Мероприятия
    /// </summary>
    public class ActivityDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Стадия
        /// </summary>
        public string StatusName { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// Year create activity
        /// </summary>
        public int Year { get; set; }

        public DateTime? CreateDate { get; set; }
    }
}
