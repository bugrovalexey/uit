﻿
namespace Uviri.MKRF.Entities.Admin
{
	public class Faq : EntityBase
	{
		/// <summary>Вопрос</summary>
		public virtual string Question { get; set; }//null

		/// <summary>Ответ</summary>
		public virtual string Answer { get; set; }//null

		/// <summary>Порядок</summary>
		public virtual int Order { get; set; }//null
	}
}
