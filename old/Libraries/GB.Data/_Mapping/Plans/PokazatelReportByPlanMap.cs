﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class PokazatelReportByPlanMap:
        ClassMapBase<PokazatelReportByPlan>
    {
        public PokazatelReportByPlanMap():
            base("Pokazatel_Report_By_Plan")
        {

        }

        protected override void InitMap()
        {
            Map(x => x.Caption, "Pokazatel_Report_By_Plan_Caption");
            Map(x => x.Algoritm, "Pokazatel_Report_By_Plan_Algorithm");
            Map(x => x.BaseValue, "Pokazatel_Report_By_Plan_Base_Unit");
            Map(x => x.PlanValue, "Pokazatel_Report_By_Plan_Plan_Unit");
            Map(x => x.Unit, "Pokazatel_Report_By_Plan_Unit");
            Map(x => x.FeatureType, "Pokazatel_Report_By_Plan_Type");
            References<OfferByPlan>(x => x.OfferByPlan, "Offer_By_Plan_ID").Not.Nullable();
        }
    }
}
