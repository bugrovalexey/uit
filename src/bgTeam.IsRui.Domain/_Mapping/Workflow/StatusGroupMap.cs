﻿using bgTeam.IsRui.Domain.Entities.Workflow;

namespace bgTeam.IsRui.Domain._Mapping.Workflow
{
    internal class StatusGroupMap : ClassMapHiLo<StatusGroup>
    {
        public StatusGroupMap()
            : base("Status_Group") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "Status_Group_Name");
        }
    }
}