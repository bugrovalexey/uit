﻿namespace bgTeam.IsRui.DataAccess
{
    using bgTeam.IsRui.Domain;
    using NHibernate;
    using NHibernate.Type;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface ISessionNHibernate : IDisposable
    {
        [Obsolete("Используеться для классов которые будут удалены")]
        ISession GetSession();

        void Insert(IEntity entity);

        Task InsertAsync(IEntity entity);

        void Update(IEntity entity);

        Task UpdateAsync(IEntity entity);

        Task UpdateAsync<T>(IList<T> list)
            where T : IEntity;

        Task SaveOrUpdateAsync(IEntity entity);

        Task DeleteAsync(IEntity entity);

        Task DeleteAsync<T>(int id)
            where T : IEntity;

        Task<IEnumerable<T>> ExecuteProcedure<T>(string str, ParameterProcedure[] parameters)
            where T : IEntity;

        Task RefreshAsync(IEntity entity);

        Task CommitAsync();

        Task RollbackAsync();
    }

    public class ParameterProcedure
    {
        public string Name { get; private set; }

        public object Value { get; private set; }

        public IType Type { get; private set; }

        public ParameterProcedure(string name, object value, IType type)
        {
            Name = name;

            Value = value;

            Type = type;
        }
    }
}
