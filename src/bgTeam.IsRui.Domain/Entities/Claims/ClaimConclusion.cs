﻿namespace bgTeam.IsRui.Domain.Entities.Claims
{
    using System;

    /// <summary>
    /// Заключение
    /// </summary>
    public class ClaimConclusion : EntityBase
    {
        public virtual Claim Claim { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public virtual string Comment { get; set; }
    }
}
