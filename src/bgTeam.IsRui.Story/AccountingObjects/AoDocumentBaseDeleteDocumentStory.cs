﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bgTeam;
using bgTeam.IsRui.Common.Services;
using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Dto;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Documents;
using bgTeam.IsRui.Story.AccountingObjects;

public class AoDocumentBaseDeleteDocumentStory : IStory<AoDocumentBaseDeleteDocumentStoryContext, AoDeleteFileDto>
{
    private readonly IMapperBase _mapper;
    private readonly IFileProviderRui _fileProvider;
    private readonly IRepositoryRui _repository;
    private readonly ISessionNHibernateFactory _factory;

    public AoDocumentBaseDeleteDocumentStory(
        IRepositoryRui repository,
        ISessionNHibernateFactory factory,
        IMapperBase mapperBase,
        IFileProviderRui fileProvider)
    {
        _repository = repository;
        _factory = factory;
        _mapper = mapperBase;
        _fileProvider = fileProvider;
    }

    public AoDeleteFileDto Execute(AoDocumentBaseDeleteDocumentStoryContext context)
    {
        return ExecuteAsync(context).Result;
    }

    public async Task<AoDeleteFileDto> ExecuteAsync(AoDocumentBaseDeleteDocumentStoryContext context)
    {
        var deleteflag = new AoDeleteFile();


        var docForDel = _repository.Get<AODecisionToCreate>(context.Id);

        if (docForDel.Documents.Count > 0)
        {

            new FileDeleteStory(_repository, _fileProvider, _factory)
                .Execute(new FileDeleteStoryContext
                {
                    Id = docForDel.Documents[0].Id,
                });
        }

        using (var session = _factory.OpenSession())
        {
            await session.DeleteAsync<AODecisionToCreate>(context.Id);
        }

        deleteflag.deleteflag = true;

        return _mapper.Map(deleteflag, new AoDeleteFileDto());
    }
}
