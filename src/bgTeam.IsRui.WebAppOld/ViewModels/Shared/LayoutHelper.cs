﻿using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.MKRF.Logic.Tabs;
using System.Collections.Generic;
using System.Web;
using System.Reflection;
using Castle.Windsor;
using System;

namespace bgTeam.IsRui.WebApp.ViewModels.Shared
{
    public class LayoutHelper
    {
        private readonly IUserRepository _userRepository;

        public User User { get { return _userRepository.GetCurrent(); } }

        public IList<string> Tabs { get; private set; }

        public LayoutHelper(HttpApplication application)
        {
            //Dirty Hack ¯\_(ツ)_/¯
            if ((application
                ?.GetType()
                ?.BaseType
                ?.GetField("container", BindingFlags.NonPublic | BindingFlags.Static)
                ?.GetValue(application) is IWindsorContainer container) &&
                container.Resolve<IUserRepository>() is IUserRepository repository)
            {
                _userRepository = repository;
            }
            else throw new Exception("Dirty hack failed :(");

            var handler = Singleton<TabsManager>.Instance.GetHandler<MainTabs>();

            Tabs = handler.GetTabsList(LinkBuilder.ActionLink);
        }
    }
}