﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml.Serialization;

namespace GB.Smev.Data
{
    public class Smev
    {
        public const string MEMO_CODE = "MNSV01001";
        public const string MEMO_NAME = "MINSVYAZ";

        public const string NAMESPACE_SMEV = "http://smev.gosuslugi.ru/rev111111";
        public const string NAMESPACE_SERVICE = "https://www.365.minsvyaz.ru/";
        public const string NAMESPACE_SIGN = "http://www.w3.org/2000/09/xmldsig#";
    }

    [MessageContract]
    public class ObjectsInformationResponse
    {
        public ObjectsInformationResponse()
        {
            this.Message = new Message();
            this.MessageData = new MessageData();
        }

        [MessageBodyMember(Namespace = Smev.NAMESPACE_SMEV, Order = 0)]
        public Message Message { get; set; }

        [MessageBodyMember(Namespace = Smev.NAMESPACE_SMEV, Order = 1)]
        public MessageData MessageData { get; set; }
    }

    [MessageContract]
    public class ObjectsInformationRequest
    {
        public ObjectsInformationRequest()
        {
            this.Message = new Message();
            this.ObjectsInformationData = new ObjectsInformationData();
        }

        [MessageBodyMember(Namespace = Smev.NAMESPACE_SMEV, Order = 0)]
        public Message Message;

        [MessageBodyMember(Namespace = Smev.NAMESPACE_SMEV, Order = 1)]
        public ObjectsInformationData ObjectsInformationData;
    }

    [DataContract(Namespace = Smev.NAMESPACE_SMEV)]
    public class ObjectsInformationData
    {
        [DataMember]
        public DateTime Date;
    }

    [MessageContract]
    public class FunctionRequest
    {
        public FunctionRequest()
        {
            this.Message = new Message();
            this.MessageData = new MessageData();
        }

        [MessageBodyMember(Namespace = Smev.NAMESPACE_SMEV, Order = 0)]
        public Message Message;

        [MessageBodyMember(Namespace = Smev.NAMESPACE_SMEV, Order = 1)]
        public MessageData MessageData;
    }

    [MessageContract]
    public class FunctionResponse
    {
        public FunctionResponse()
        {
            this.Message = new Message();
            this.MessageData = new MessageData();
        }

        [MessageBodyMember(Namespace = Smev.NAMESPACE_SMEV, Order = 0)]
        public Message Message { get; set; }

        [MessageBodyMember(Namespace = Smev.NAMESPACE_SMEV, Order = 1)]
        public MessageData MessageData { get; set; }
    }

    [DataContract(Namespace = Smev.NAMESPACE_SMEV)]
    public class Message
    {
        public Message()
        {
            this.Sender = new TalkProcess() { Code = Smev.MEMO_CODE, Name = Smev.MEMO_NAME };
            this.Recipient = new TalkProcess() { Code = Smev.MEMO_CODE, Name = Smev.MEMO_NAME };
            this.Originator = new TalkProcess() { Code = Smev.MEMO_CODE, Name = Smev.MEMO_NAME };
            this.TypeCode = "GFNC";
            this.ExchangeType = "2";
            this.Date = DateTime.Now;
            string guid = Guid.NewGuid().ToString();
            this.RequestIdRef = guid;
            this.OriginRequestIdRef = guid;
            this.Status = "RESULT";
            this.ServiceCode = string.Empty;
            this.CaseNumber = string.Empty;

        }

        [DataMember(Order = 1)]
        public TalkProcess Sender;

        [DataMember(Order = 2)]
        public TalkProcess Recipient;

        [DataMember(Order = 3)]
        public TalkProcess Originator;

        [DataMember(Order = 4)]
        public string TypeCode;

        [DataMember(Order = 5)]
        public string Status;

        [DataMember(Order = 6)]
        public DateTime Date;

        [DataMember(Order = 7)]
        public string ExchangeType;

        [DataMember(Order = 8)]
        public string RequestIdRef;

        [DataMember(Order = 9)]
        public string OriginRequestIdRef;

        [DataMember(Order = 10)]
        public string ServiceCode;

        [DataMember(Order = 11)]
        public string CaseNumber;
    }

    [DataContract(Namespace = Smev.NAMESPACE_SMEV)]
    public class TalkProcess
    {
        [DataMember]
        public string Code;

        [DataMember]
        public string Name;
    }

    [DataContract(Namespace = Smev.NAMESPACE_SMEV)]
    public class MessageData
    {
        public MessageData()
        {
            this.AppData = new MessageAppData();
        }

        [DataMember]
        public MessageAppData AppData;
    }

    [DataContract]
    public class MessageAppData
    {
        [DataMember(Order = 1, EmitDefaultValue = false)]
        public FgisRequest FgisRequest { get; set; }

        [DataMember(Order = 2, EmitDefaultValue = false)]
        public FgisGetRegisterResult FgisGetRegisterResult { get; set; }

        [DataMember(Order = 3, EmitDefaultValue = false)]
        public FgisGetApplicationResult FgisGetApplicationResult { get; set; }

        [DataMember(Order = 4, EmitDefaultValue = false)]
        public CoordGetPlansRequest CoordGetPlansRequest { get; set; }

        [DataMember(Order = 5, EmitDefaultValue = false)]
        public CoordGetPlansResult CoordGetPlansResult { get; set; }

        [DataMember(Order = 6, EmitDefaultValue = false)]
        public CoordGetDepartmentsResult CoordGetDepartmentsResult { get; set; }

        [DataMember(Order = 7, EmitDefaultValue = false)]
        public ApfGetRegisteredResult ApfGetRegisteredResult { get; set; }

        [DataMember(Order = 8, EmitDefaultValue = false)]
        public ApfGetObjectInformationRequest ApfGetObjectInformationRequest { get; set; }

        [DataMember(Order = 9, EmitDefaultValue = false)]
        public ApfGetObjectInformationResult ApfGetObjectInformationResult { get; set; }
    }

    [DataContract]
    public class FgisRequest
    {
        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }
    }

    [DataContract]
    public class FgisGetRegisterResult
    {
        public FgisGetRegisterResult()
        {
            Files = new List<byte[]>();
        }

        [DataMember]
        public List<byte[]> Files { get; set; }
    }

    [DataContract]
    public class FgisGetApplicationResult
    {
        public FgisGetApplicationResult()
        {
            Files = new List<byte[]>();
        }

        [DataMember]
        public List<byte[]> Files { get; set; }
    }

    [DataContract]
    public class CoordGetPlansRequest
    {
        [DataMember]
        public int Year { get; set; }
    }

    [DataContract]
    public class CoordGetPlansResult
    {
        [DataMember(Name = "base64Binary")]
        public byte[] File { get; set; }
    }

    [DataContract]
    public class CoordGetDepartmentsResult
    {
        [DataMember(Name = "base64Binary")]
        public byte[] File { get; set; }
    }

    [DataContract]
    public class ApfGetRegisteredResult
    {
        //public ApfGetRegisteredResult()
        //{
        //    Funds = new List<ApfFund>();
        //}

        [DataMember]
        public ApfFund[] Funds { get; set; }
    }
    
    public class ApfFund
    {
        [DataMember(Order = 1)]
        public int Id;

        [DataMember(Order = 2)]
        public string Num;

        [DataMember(Order = 3)]
        public string Name;

        [DataMember(Order = 4)]
        public string Provider;

        [DataMember(Order = 5)]
        public int Versions;

        [DataMember(Order = 6)]
        public DateTime Date;

        [DataMember(Order = 7)]
        public string RegistryNum;
    }

    [DataContract]
    public class ApfGetObjectInformationRequest
    {
        [DataMember]
        public DateTime Date { get; set; }
    }

    [DataContract]
    public class ApfGetObjectInformationResult
    {
        //public ApfGetObjectInformationResult()
        //{
        //    Objects = new List<ObjectInformation>();
        //}
        [DataMember]
        public ObjectInformation[] Objects;
    }

    [DataContract]
    public class ObjectInformation
    {
        [DataMember]
        public int Id;

        [DataMember]
        public string Name;

        [DataMember]
        public string Month;

        [DataMember]
        public int Count;
    }
}
