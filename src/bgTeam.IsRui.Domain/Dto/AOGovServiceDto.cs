﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Реализуемые государственные услуги и функции объекта учета
    /// </summary>
    public class AOGovServiceDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Если копировали из ГУ/ГФ
        /// </summary>
        public virtual GovServiceDto GovService { get; set; }

        /// <summary>
        /// Процент использования ресурсов ИС при автоматизации исполнения государственной услуги (функции)
        /// </summary>
        public virtual int? PercentOfUseResource { get; set; }

        /// <summary>
        /// Нормативно-правовые акты
        /// </summary>
        public virtual IList<AOGovServiceNpaDto> AoGovServicesNpa { get; set; }
    }
}
