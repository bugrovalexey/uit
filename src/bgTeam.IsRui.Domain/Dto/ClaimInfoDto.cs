﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities;
    using System;
    using System.Collections.Generic;

    public class ClaimInfoDto
    {
        public int Id { get; set; }

        public string ClaimNumber { get; set; }

        public int AccountingObjectId { get; set; }

        public string AccountingObjectName { get; set; }

        public int ActivityId { get; set; }

        public string ActivityName { get; set; }

        public string SubDivisionName { get; set; }

        public decimal Cost { get; set; }

        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Наименование подразделения пользователя
        /// </summary>
        public string ResponsibleDepartment { get; set; }

        /// <summary>
        /// ФИО пользователя, добавившего заявку
        /// </summary>
        public string ResponsibleFullName { get; set; }

        public decimal ImplementationCost { get; set; }

        public int StatusId { get; set; }

        public string StatusName { get; set; }

        public int WorkflowId { get; set; }

        public WorkflowUserConclusionDto Conclusion { get; set; }

        public string Content { get; set; }

        public string Justification { get; set; }

        public IEnumerable<ClaimDocumentDto> Documents { get; set; }

        public IEnumerable<ClaimIndicatorDto> Indicators { get; set; }

        public IEnumerable<ClaimMarkDto> Marks { get; set; }

        public IEnumerable<ClaimActDto> Acts { get; set; }

        public IEnumerable<ClaimScopeOfWorkDto> ScopeOfWork { get; set; }

        /// <summary>
        /// Переход на следующий статус
        /// </summary>
        public IEnumerable<StatusDto> StatusGetNext { get; set; }
    }
}
