﻿using GB.Data.Directories;

namespace GB.Data.AccountingObjects.WorksAndGoods
{
    /// <summary>
    /// Роботы и товары (сущность, в которую отражаются(копируются) данные из сущностей
    /// ПО, ТО, Работы и услуги)
    /// </summary>
    public class WorkAndGoods : EntityBase
    {
        /// <summary>
        /// Акт сдачи-приемки
        /// </summary>
        public virtual ActOfAcceptance ActOfAcceptance { get; set; }

        /// <summary>
        /// Объект учета
        /// </summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Классификационная категория
        /// </summary>
        public virtual CategoryKindOfSupport CategoryKindOfSupport { get; set; }

        /// <summary>
        /// Id объекта, либо ПО, либо ТО, либо Работа/Сервис
        /// Ключ для вычисления объекта будет составной - Id + CategoryTypeOfSupportEnum
        /// </summary>
        public virtual int ObjectId { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public virtual decimal? Summa { get; set; }

        /// <summary>
        /// Вид обеспечения
        /// </summary>
        public virtual CategoryTypeOfSupportEnum Type { get; set; }

    }

}
