﻿using GB;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Validation
{
    internal class Activity
    {
        /// <summary>Параметры для валидации мероприятия плана</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create)
            {
                PlansActivity Item = (PlansActivity)Entity;

                if (Item != null && Item.Plan != null)
                    return "0;" + Item.Plan.Id.ToString();
                else
                    return null;
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }

    internal class Works
    {
        /// <summary>Параметры для валидации мероприятия плана</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create 
                || Action == ObjActionEnum.edit
                || Action == ObjActionEnum.delete
                || Action == ObjActionEnum.view)
            {
                PlansWork Item = (PlansWork)Entity;

                if (Item != null && Item.PlanActivity != null)
                    return "0;" + Item.PlanActivity.Id.ToString();
                else
                    return null;
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }

    internal class Specs
    {
        /// <summary>Параметры для валидации мероприятия плана</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create
                || Action == ObjActionEnum.edit
                || Action == ObjActionEnum.delete
                || Action == ObjActionEnum.view)
            {
                PlansSpec Item = (PlansSpec)Entity;

                if (Item != null && Item.PlanActivity != null)
                    return "0;" + Item.PlanActivity.Id.ToString();
                else
                    return null;
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }

    internal class PlansActivityFuncs
    {
        /// <summary>Параметры для валидации мероприятия плана</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create
                || Action == ObjActionEnum.edit
                || Action == ObjActionEnum.delete
                || Action == ObjActionEnum.view)
            {
                PlansActivityFunc Item = (PlansActivityFunc)Entity;

                if (Item != null && Item.Activity != null)
                    return "0;" + Item.Activity.Id.ToString();
                else
                    return null;
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }

    internal class ActivityGoal
    {
        /// <summary>Параметры для валидации мероприятия плана</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            PlansActivityGoal Item = (PlansActivityGoal)Entity;

            if (Item != null && Item.PlansActivity != null && Item.PlansActivity.Plan != null)
            {
                if (Action == ObjActionEnum.create)
                    return "0;" + Item.PlansActivity.Plan.Id.ToString();
                else
                    return Item.PlansActivity.Id.ToString();
            }
            else
                return null;
        }
    }
    
    internal class ActivityReason
    {
        /// <summary>Параметры для валидации мероприятия плана</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            PlansActivityReason Item = (PlansActivityReason)Entity;

            if (Item != null && Item.Activity != null && Item.Activity.Plan != null)
            {
                if (Action == ObjActionEnum.create)
                    return "0;" + Item.Activity.Plan.Id.ToString();
                else
                    return Item.Activity.Id.ToString();
            }
            else
                return null;
        }
    }

    internal class GoalIndicator
    {
        /// <summary>Параметры для валидации мероприятия плана</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            PlansGoalIndicator Item = (PlansGoalIndicator)Entity;

            if (Item != null && Item.PlanActivity != null && Item.PlanActivity.Plan != null)
            {
                if (Action == ObjActionEnum.create)
                    return "0;" + Item.PlanActivity.Plan.Id.ToString();
                else
                    return Item.PlanActivity.Id.ToString();
            }
            else
                return null;
        }
    }
}
