﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActsFileDto
    {
        public Guid? TempGuid { get; set; }

        public int? Id { get; set; }

        public string FileName { get; set; }

        public DocKindEnum Kind { get; set; }

        public string Extension { get; set; }

        public int? Size { get; set; }

        public string Url { get; set; }

        public string MimeType { get; set; }
    }
}
