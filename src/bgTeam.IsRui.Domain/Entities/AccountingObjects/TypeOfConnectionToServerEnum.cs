﻿using System.ComponentModel;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    /// <summary>
    /// Тип соединения с сервером
    /// </summary>
    public enum TypeOfConnectionToServerEnum
    {
        [Description("Не задано")]
        None = 0,

        [Description("На пользователя")]
        OnUser = 1,

        [Description("На устройство")]
        OnDevice = 2
    }
}