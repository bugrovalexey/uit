﻿using bgTeam.IsRui.Domain.Entities.Workflow;
using System.Collections.Generic;

namespace bgTeam.IsRui.Domain.Entities.Plans
{
    /// <summary>
    /// Экспертное заключение по мероприятию
    /// </summary>
    public class ActivityExpertiseConclusion : EntityBase, IWorkflowObject
    {
        public ActivityExpertiseConclusion() { }

        internal ActivityExpertiseConclusion(int id)
        {
            // TODO: Complete member initialization
            this.Id = id;
        }

        public virtual Status Status { get; set; }

        /// <summary>Эксперты</summary>
        public virtual IList<ActivityExpert> Experts { get; set; }

        
    }
}