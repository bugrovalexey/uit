﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Activities.Goods;
using System.Web.Mvc;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.Controllers
{
    partial class ActivitiesController
    {
        public ActionResult GoodsSingle(int Id)
        {
            return View(new GoodsViewModel(_statusRepository, Id));
        }

        public ActionResult GoodsEdit(int ownerId, int? id)
        {
            return View(new GoodsEditViewModel(_statusRepository, ownerId, id));
        }

        [HttpPost]
        public ActionResult GoodsEdit(PlansSpecArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                bool isNew = args.Id == 0;
                var result = isNew
                    ? new PlansSpecService().Create(args)
                    : new PlansSpecService().Update(args);

                return new TableGoods(result);
            });
        }

        [HttpPost]
        public ActionResult GoodsDelete(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => new PlansSpecService().Delete(new PlansSpecArgs { Id = id }));
        }

        #region Характеристики товара

        public ActionResult CharacteristicEdit(int? id, int ownerId, int activityId)
        {
            return View(new CharacteristicEditViewModel(_statusRepository, activityId, ownerId, id));
        }

        [HttpPost]
        public ActionResult CharacteristicEdit(PlansSpecCharacteristicArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                bool isNew = args.Id == 0;
                var result = isNew
                    ? new PlansSpecCharacteristicsService().Create(args)
                    : new PlansSpecCharacteristicsService().Update(args);

                return new TableCharacteristic(result);
            });
        }

        [HttpPost]
        public ActionResult CharacteristicDelete(int id)
        {
            return NotificationHelper.NotificationDelete(id,
                () => new PlansSpecCharacteristicsService().Delete(new PlansSpecCharacteristicArgs {Id = id}));
        }

        #endregion Характеристики товара
    }
}