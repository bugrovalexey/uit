﻿using GB;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Validation
{
    internal class Demand
    {
        /// <summary>Параметры для валидации плана</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (!(Entity is ExpDemand))
                return null;

            if (Action == ObjActionEnum.CanSignECP ||
                Action == ObjActionEnum.CanRunExpertize)
            {
                return Action.ToString() + ";" + Entity.Id.ToString();
            }
            else if (Action == ObjActionEnum.create &&
                    (((ExpDemand)Entity).ExpObject != null))
            {
                // если указан план - передаём тип
                return "0;" + ((int)((ExpDemand)Entity).ExpObject.PlanType).ToString();
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
