﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GB.Extentions
{
    public static class EnumExtensions
    {
        // Methods
        public static string GetDescription(this Enum member)
        {
            if (!member.GetType().IsEnum)
            {
                throw new ArgumentOutOfRangeException("member", "member is not enum");
            }
            FieldInfo field = member.GetType().GetField(member.ToString());
            if (field == null)
            {
                return null;
            }
            DescriptionAttribute[] customAttributes = field.GetCustomAttributes<DescriptionAttribute>(false);
            if (customAttributes.Length > 0)
            {
                return customAttributes[0].Description;
            }
            return member.ToString();
        }

        public static IEnumerable<KeyValuePair<int, string>> ToKeyValuePairs<TEnum>() where TEnum : struct, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum must be an enumerated type");
            }
            return (from x in Enum.GetValues(typeof(TEnum)).Cast<Enum>() select new KeyValuePair<int, string>(int.Parse(x.ToString("D")), x.GetDescription())).ToList<KeyValuePair<int, string>>();
        }
    }

}
