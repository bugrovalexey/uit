﻿namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    using System.ComponentModel;

    /// <summary>
    /// Права на ПО
    /// </summary>
    public enum RightsOnSoftwareEnum
    {
        [Description("Объект исключительного права")]
        Exclusive = 1,

        [Description("Права использования на неисключительной основе")]
        NotExclusive = 2
    }
}