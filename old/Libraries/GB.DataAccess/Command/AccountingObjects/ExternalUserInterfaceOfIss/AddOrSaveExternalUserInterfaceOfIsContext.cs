﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.ExternalUserInterfaceOfIss
{
    public class AddOrSaveExternalUserInterfaceOfIsContext : BaseAddOrSaveContext
    {        
        public int AccountingObjectId { get; set; }
        
        public string Name { get; set; }
        
        public string Purpose { get; set; }
        
        public int? TypeId { get; set; }
        
        public string Url { get; set; }
    }
}
