﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class DocumentsDeleteDocumentStory : IStory<DocumentsDeleteDocumentStoryContext, bool>
    {
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;

        public DocumentsDeleteDocumentStory(IRepositoryRui repositoryRui, ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _repository = repositoryRui;
            _factory = sessionNHibernateFactory;
        }

        public bool Execute(DocumentsDeleteDocumentStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(DocumentsDeleteDocumentStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            bool result;

            using (var session = _factory.OpenSession())
            {
                var activity = _repository.Get<ActivityDocuments>(context.Id.Value);

                await session.DeleteAsync<Files>(activity.FileId.Value);

                await session.DeleteAsync<ActivityDocuments>(context.Id.Value);

                result = true;
            }

            return result;
        }
    }
}
