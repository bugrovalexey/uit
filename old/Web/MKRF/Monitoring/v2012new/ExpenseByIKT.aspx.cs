﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GB.Entity.Directories;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;

namespace GB.MKRF.Web.Monitoring.v2012new
{
    public partial class ExpenseByIKT : System.Web.UI.Page
    {
        private int currentYear = 0;
        protected int CurrentYear
        {
            get
            {
                if (currentYear == 0)
                {
                 
                    var ys = Request.QueryString["Year"];
                    int yi = 0;
                    if (!string.IsNullOrWhiteSpace(ys) && int.TryParse(ys, out yi))
                    {
                        currentYear = yi;
                    }
                    else
                    {
                        currentYear = DateTime.Now.Year;
                    }
                }
                return currentYear;
            }
        }

        private int expDirection = 0;
        protected int ExpDirection
        {
            get
            {
                if (expDirection == 0)
                {

                    var ds = Request.QueryString["type"];
                    int di = 0;
                    if (!string.IsNullOrWhiteSpace(ds) && int.TryParse(ds, out di))
                    {
                        expDirection = di;
                    }
                    else
                    {
                        expDirection = 0;
                    }
                }
                return expDirection;
            }
        }


        private DataTable _data = null;
        private DataTable Data
        {
            get
            {
                if (_data == null)
                {
                    _data = MonitoringRepository.ExpenseByIKT(CurrentYear, ExpDirection);
                }

                return _data;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            YearsComboBox.DataBind();
            ExpenseDirectionComboBox.DataBind();
            ChartDataBinding();
            grid.DataBind();
        }   

        protected void ChartCallbackPanel_OnCallback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            ChartDataBinding();
        }

        private void ChartDataBinding()
        {
            Chart.Series.Clear();
            var expenseVolumeY0Sum = 0d;
            var expenseVolumeY1Sum = 0d;
            var expenseVolumeY2Sum = 0d;
            foreach (DataRow row in Data.Rows)
            {
                expenseVolumeY0Sum += Convert.ToDouble(row["ExpenseVolumeY0"]);
                expenseVolumeY1Sum += Convert.ToDouble(row["ExpenseVolumeY1"]);
                expenseVolumeY2Sum += Convert.ToDouble(row["ExpenseVolumeY2"]);
            }

            AddSeries("Очередной финансовый год", expenseVolumeY0Sum);
            AddSeries("Первый год планового периода", expenseVolumeY1Sum);
            AddSeries("Второй год планового периода", expenseVolumeY2Sum);
        }

        private void AddSeries(string name, double value)
        {
            int i = Chart.Series.Add(name, ViewType.Bar);
            Chart.Series[i].LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            Chart.Series[i].Label.ResolveOverlappingMode = ResolveOverlappingMode.HideOverlapped;
            Chart.Series[i].Points.Clear();
            Chart.Series[i].Points.Add(new SeriesPoint(name, value));
        }


        protected void ExpenseDirectionComboBox_DataBinding(object sender, System.EventArgs e)
        {
            IList<ExpenseDirection> data = DirectoriesRepositary.GetDirectories<ExpenseDirection>(CurrentYear);
			ASPxComboBox box = (ASPxComboBox)sender;
			box.DataSource = data;
            box.Value = ExpDirection;
        }
     

        protected void YearsComboBox_DataBinding(object sender, System.EventArgs e)
        {
            IList<Years> data = RepositoryBase<Years>.GetAll(x => x.Value >= 2010).Take(10).ToList();
			ASPxComboBox box = (ASPxComboBox)sender;
			box.DataSource = data;
			box.Value = CurrentYear;
        }

        protected void grid_DataBinding(object sender, EventArgs e)
        {
            //grid.DataSource = Data;

            var result = Data.AsEnumerable()
               .Select(x =>
               new
               {   
                   OGV = Convert.ToString(x["OGV"]),
                   PlanId = Convert.ToInt32(x["PlanId"]),
                   ExpenseVolumeY0 = Convert.ToDouble(x["ExpenseVolumeY0"]),
                   ExpenseVolumeY1 = Convert.ToDouble(x["ExpenseVolumeY1"]),
                   ExpenseVolumeY2 = Convert.ToDouble(x["ExpenseVolumeY2"]),
               })
               .GroupBy(x => x.OGV)
               .Select(m =>
                   new
                   {
                       OGV = Convert.ToString(m.Key),
                       PlanId = m.Select(n => n.PlanId).First(), // за данный год у одного ОГВ может быть только один план
                       ExpenseVolumeY0 = m.Sum(exp => exp.ExpenseVolumeY0),
                       ExpenseVolumeY1 = m.Sum(exp => exp.ExpenseVolumeY1),
                       ExpenseVolumeY2 = m.Sum(exp => exp.ExpenseVolumeY2)
                   })
               .ToList();

            grid.DataSource = result;

        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            grid.Columns["Y0"].Caption = CurrentYear.ToString();
            grid.Columns["Y1"].Caption = (CurrentYear + 1).ToString();
            grid.Columns["Y2"].Caption = (CurrentYear + 2).ToString();
        }

        protected void Chart_CustomDrawSeriesPoint(object sender, CustomDrawSeriesPointEventArgs e)
        {
            e.LabelText = Convert.ToDouble(e.LabelText).ToString("#,##0.0000");
        }
    }
}