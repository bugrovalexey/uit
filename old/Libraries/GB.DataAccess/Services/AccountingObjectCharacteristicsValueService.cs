﻿using System.Linq;
using GB.Data.AccountingObjects;
using GB.Data.Plans;
using GB.DataAccess.Repository;

namespace GB.DataAccess.Services
{
    public class AccountingObjectCharacteristicsValueService : ServiceBase<AccountingObjectCharacteristicsValue>
    {
        public AccountingObjectCharacteristics CurrentCharacteristics { get; private set; }

        public AccountingObjectCharacteristicsValue Update(IAccountingObjectCharacteristicsValueArgs args)
        {
            var val = GetCharacteristicsValue(args);

            val.Norm = args.Norm;
            val.Max = args.Max;

            base.SaveOrUpdate(val);

            return val;
        }

        public void Delete(object id)
        {
            base.Delete((int)id);
        }

        private AccountingObjectCharacteristicsValue GetCharacteristicsValue(IAccountingObjectCharacteristicsValueArgs args)
        {
            var val = new Repository<AccountingObjectCharacteristicsValue>().GetAllEx(q => q.Where(x =>
                x.Activity.Id == args.ActivityId &&
                x.Characteristics.Id == (int)args.CharacteristicsId)).FirstOrDefault();

            if (val == null)
                val = new AccountingObjectCharacteristicsValue(GetEntity<PlansActivity>(args.ActivityId),
                    GetEntity<AccountingObjectCharacteristics>(args.CharacteristicsId));

            return val;
        }

        internal void DeleteAll(PlansActivity activity)
        {
            var crList = new Repository<AccountingObjectCharacteristics>()
                .GetAllEx(q => q.Where(x => x.AccountingObject == activity.AccountingObject));

            var list = Repository.GetAllEx(q => q
                .Where(x => x.Activity == activity)
                .Where(NHibernate.Criterion.Restrictions.In("Characteristics", crList.Select(x => x.Id).ToList())));

            base.Delete(list);
        }
    }

    public interface IAccountingObjectCharacteristicsValueArgs
    {
        int ActivityId { get; set; }

        int? CharacteristicsId { get; set; }

        int? Norm { get; set; }

        int? Max { get; set; }
    }

    public class AccountingObjectCharacteristicsValueArgs : IAccountingObjectCharacteristicsValueArgs // : BaseArgs
    {
        public int ActivityId { get; set; }

        public int? CharacteristicsId { get; set; }

        public int? Norm { get; set; }

        public int? Max { get; set; }
    }
}