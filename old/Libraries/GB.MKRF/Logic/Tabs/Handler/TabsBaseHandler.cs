﻿using GB.MKRF.Providers;
using System.Collections.Generic;

namespace GB.MKRF.Logic.Tabs.Handler
{
    internal class TabsBaseHandler : ITabsHandler
    {
        private string _Controller;

        private IList<ActivityPage> Tabs { get; set; }

        public TabsBaseHandler()
        {
            Tabs = new List<ActivityPage>();
        }

        public TabsBaseHandler(string controller)
            : this()
        {
            _Controller = controller;
        }

        protected void Add(string name, string action)
        {
            Add(name, action, _Controller);
        }

        protected void Add(string name, string action, string controller)
        {
            Tabs.Add(new ActivityPage()
            {
                Name = name,
                Action = action,
                Controller = controller,
            });
        }

        public string GetAction()
        {
            foreach (var item in Tabs)
            {
                if (Singleton<AccessControllerProvider>.Instance.СheckAccessCurrentUser(item.Controller, item.Action))
                    return item.Action;
            }

            throw new ObjectAccessException();
        }

        public IList<string> GetTabsList(ReturnTabsText fun)
        {
            List<string> res = new List<string>();

            foreach (var item in Tabs)
            {
                if (Singleton<AccessControllerProvider>.Instance.СheckAccessCurrentUser(item.Controller, item.Action))
                    res.Add(fun(item.Name, item.Action, item.Controller));
            }

            return res;
        }
    }

    internal class ActivityPage
    {
        public string Name { get; set; }

        public string Action { get; set; }

        public string Controller { get; set; }
    }
}