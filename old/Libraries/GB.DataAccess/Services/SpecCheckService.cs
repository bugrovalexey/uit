﻿using GB.Data.AccountingObjects;
using GB.Data.Documents;
using GB.DataAccess.Command.Documents;
using GB.DataAccess.Services.Args;
using System;

namespace GB.DataAccess.Services
{
    public class SpecCheckService : ServiceBase<SpecialCheck>, IService<SpecialCheck, SpecialCheckArgs>
    {
        ISessionFactoryGb factory;

        public SpecCheckService()
        {
            factory = new SessionFactory();
        }

        public SpecialCheck Create(SpecialCheckArgs args)
        {
            return SaveOrUpdate(new SpecialCheck(), args);
        }

        public SpecialCheck Update(SpecialCheckArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private SpecialCheck SaveOrUpdate(SpecialCheck entity, SpecialCheckArgs args)
        {
            entity = ConvertDtoToEntity.FillSpecCheckFromDto(entity, args);

            base.SaveOrUpdate(entity);
            base.Refresh(entity);            

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = args.Files,
                listDocs = entity.Documents,
                owner = entity,
                type = DocTypeEnum.SpecialCheck
            });

            base.SaveOrUpdate(entity);
            base.Refresh(entity);

            return entity;
        }

        public void Delete(SpecialCheckArgs args)
        {
            base.Delete((int)args.Id);
        }
    }
}
