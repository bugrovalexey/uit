﻿using System;
using System.Linq;
using System.Web.Security;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.Providers
{
    public class DbRoleProvider : RoleProvider
    {
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            //Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("admin"), null);
            base.Initialize(name, config);
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            var roles = new RepositoryDc<Role>().GetAll();

            return roles.Select(x => x.Name).ToArray();
        }

        public override string[] GetRolesForUser(string username)
        {
            var user = new User() { Login = "test" };//IUserRepository.GetCurrent();

            if (user != null)
                return user.Roles.Select(x => x.Name).ToArray();

            return null;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}