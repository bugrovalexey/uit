﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Story.AccessObjects;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimCreateOrUpdateUserCommentStory : IStory<ClaimCreateOrUpdateUserCommentStoryContext, WorkflowUserConclusionDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapperBase;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IUserRepository _userRepository;
        private readonly IStatusRepository _statusRepository;
        private readonly IAccessEditService _accessEditService;

        private static object _lock = new object();

        public ClaimCreateOrUpdateUserCommentStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase,
            ISessionNHibernateFactory sessionNHibernateFactory,
            IUserRepository userRepository,
            IStatusRepository statusRepository,
            IAccessEditService accessEditService)
        {
            _repository = repositoryRui;
            _mapperBase = mapperBase;
            _factory = sessionNHibernateFactory;
            _userRepository = userRepository;
            _statusRepository = statusRepository;
            _accessEditService = accessEditService;
        }

        public WorkflowUserConclusionDto Execute(ClaimCreateOrUpdateUserCommentStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<WorkflowUserConclusionDto> ExecuteAsync(ClaimCreateOrUpdateUserCommentStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.GetExisting<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка с идентификатором: {context.ClaimId}");
            }

            await _accessEditService.CheckAccessEditAsync(claim);

            WorkflowUserConclusion entity;

            lock (_lock) // TODO: зачем здесь лок?
            {
                var curUser = _userRepository.GetCurrent();

                if (claim.Workflow == null)
                {
                    using (var session = _factory.OpenSession())
                    {
                        new WorkflowCreateStory(_statusRepository)
                        .Execute(new WorkflowCreateStoryContext()
                        {
                            Entity = claim,
                            Session = session,
                        });
                    }
                }

                entity = _repository.Get<WorkflowUserConclusion>(q => q
                    .Where(x => x.WorkflowId == claim.Workflow.Id &&
                        x.StatusId == claim.Workflow.Status.Id &&
                        x.UserId == curUser.Id));

                if (entity == null)
                {
                    entity = new WorkflowUserConclusion()
                    {
                        WorkflowId = claim.Workflow.Id,
                        StatusId = claim.Workflow.Status.Id,
                        UserId = curUser.Id,
                        Text = context.Text,
                    };

                    using (var session = _factory.OpenSession())
                    {
                        session.Insert(entity);
                    }
                }
                else
                {
                    using (var session = _factory.OpenSession())
                    {
                        entity.Text = context.Text;

                        session.Update(entity);
                    }
                }
            }

            return _mapperBase.Map(entity, new WorkflowUserConclusionDto());


            /*if (context.ClaimId < 1)
            {
                throw new IsRuiException("Неверный идентификатор заявки");
            }

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка с идентификатором: {context.ClaimId}");
            }

            ClaimUserComment entity;

            if (context.Id.HasValue)
            {
                entity = _repository.Get<ClaimUserComment>(context.Id.Value);
            }
            else
            {
                var curUser = _userRepository.GetCurrent();

                entity = _repository.Get<ClaimUserComment>(q => q
                    .Where(x => x.Claim.Id == context.ClaimId && x.User.Id == curUser.Id && x.Status == null));

                if (entity == null)
                {
                    entity = new ClaimUserComment();
                    entity.User = curUser;
                    entity.CreateDate = DateTime.Now;
                    entity.TimeStamp = DateTime.Now;
                }
            }

            entity.Claim = new Claim() { Id = context.ClaimId };
            entity.Comment = context.Comment;

            using (var session = _factory.OpenSession())
            {
                await session.SaveOrUpdateAsync(entity);
                await session.CommitAsync();
            }

            return _mapperBase.Map(entity, new ClaimUserCommentDto());*/
        }
    }
}
