﻿using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Directories;
using System.Collections.Generic;

namespace bgTeam.IsRui.Domain.Entities.Plans
{
    /// <summary>План информатизации (План)</summary>
    public class Plan : EntityBase
    {
        protected Plan()
        {
        }

        public Plan(Department dep)
        {
            // TODO: Complete member initialization
            this.Department = dep;
        }

        /// <summary>ЭЦП</summary>
        //		public virtual bool PlanECP { get; set; }

        /// <summary>Тип формы плана</summary>
        //public virtual ExpObjectTypeEnum PlanType { get; set; }

        /// <summary>Год</summary>
        public virtual Years Year { get; set; }

        /// <summary>Мероприятия плана</summary>
        public virtual IList<PlansActivity> Activities
        {
            get;
            protected set;
        }

        /// <summary>План закупок</summary>
        //public virtual IList<PurchasePlan> PurchasePlans { get; set; }

        /// <summary>Название Плана</summary>
        public virtual string Name
        {
            get
            {
                return "План информатизации на " + Year.ToString() + " г., " + Department.Name;
            }
        }

        /// <summary>Название Уточненного плана</summary>
        public virtual string Name2012
        {
            get
            {
                return "Уточненный план на " + Year.ToString() + " г., " + Department.Name;
            }
        }

        /// <summary>Название Формы сведений</summary>
        public virtual string PreName
        {
            get
            {
                return "Форма сведений о планируемых бюджетных ассигнованиях на " + Year.ToString() + " г., " + Department.Name;
            }
        }

        /// <summary>Получить название по типу</summary>
        //public virtual string GetName(ExpObjectTypeEnum type)
        //{
        //    switch (type)
        //    {
        //        case ExpObjectTypeEnum.Plan:
        //            return Name;

        //        case ExpObjectTypeEnum.Plan2012:
        //            return Name2012;

        //        case ExpObjectTypeEnum.PreliminaryPlan:
        //            return PreName;

        //        default:
        //            return "Объект экспертизы на " + Year.ToString() + " г., " + Department.Name; ;
        //    }
        //}

        /// <summary>Ведомство</summary>
        public virtual Department Department { get; set; }

        /// <summary>Пользователь</summary>
        public virtual User User { get; set; }

        /// <summary>Признак публичности</summary>
        //        public virtual bool IsPublic { get; set; }

        /// <summary>Заявка на экспертизу</summary>
        //public virtual ExpDemand Demand
        //{
        //    get
        //    {
        //        return (DemandList != null && DemandList.Count > 0) ? DemandList[0] : null;
        //    }
        //}

        //protected IList<ExpDemand> DemandList = null;

        /// <summary>Распределение предельных объемов по 242 виду расходов</summary>
        //        public virtual decimal VolLim0 { get; set; }
        //        /// <summary>Распределение предельных объемов по 242 виду расходов</summary>
        //        public virtual decimal VolLim1 { get; set; }
        //        /// <summary>Распределение предельных объемов по 242 виду расходов</summary>
        //        public virtual decimal VolLim2 { get; set; }
        //        /// <summary>Дополнительная потребность в бюджетных ассигнованиях федерального бюджета</summary>
        //        public virtual decimal VolAddFB0 { get; set; }
        //        /// <summary>Дополнительная потребность в бюджетных ассигнованиях федерального бюджета</summary>
        //        public virtual decimal VolAddFB1 { get; set; }
        //        /// <summary>Дополнительная потребность в бюджетных ассигнованиях федерального бюджета</summary>
        //        public virtual decimal VolAddFB2 { get; set; }

        /// <summary>Родительский план (форма сведений)</summary>
        public virtual Plan Parent { get; set; }

        //===================================================Plan2012new=================================================================

        //public virtual IList<ProjectManagementSystem> ProjectMngSystems { get; protected set; }

        //public virtual IList<PlansSchedule> PlansSchedules { get; protected set; }
    }

    //public static class PlanExtension
    //{
    //    public static string GetTypeName(this Plan plan)
    //    {
    //        switch (plan.PlanType)
    //        {
    //            case ExpObjectTypeEnum.Plan:
    //            case ExpObjectTypeEnum.Plan2012new:
    //                return "План информатизации";

    //            case ExpObjectTypeEnum.PreliminaryPlan:
    //                return "Форма сведений";

    //            case ExpObjectTypeEnum.Plan2012:
    //                return "Уточненный план";

    //            case ExpObjectTypeEnum.SummaryPlanMKS:
    //            case ExpObjectTypeEnum.SummaryPlanMKS2012new:
    //                return "Сводный план";

    //            default:
    //                return string.Empty;
    //        }
    //    }
    //}
}