﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GoalsAndPurposeInfoDto
    {
        public int AoId { get; set; }

        public string Targets { get; set; }

        public string PurposeAndScope { get; set; }
    }
}
