﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="IKT.aspx.cs" Inherits="GB.MKRF.Web.Selectors.IKT" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript">

        //function RowSelect() {
        //    callBack.PerformCallback(IKTTreeList.GetFocusedNodeKey());
        //}
        
        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        $(document).ready(function() {
            var iktId = getUrlVars()["id"];

            if (iktId != 0) {
                IKTTreeList.SetFocusedNodeKey(iktId);
            }
        });
    </script>
    <style>
        .grid_mouseover
        {
            background-color: #ededed;
            cursor: pointer;
        }
    </style>

    <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" oncallback="callBack_Callback">
        <ClientSideEvents CallbackComplete="function(s,e){ window.parent.PopupClose('IKTSelection', e.result); }" />
    </dxc:ASPxCallback>
    
    <dxwtl:ASPxTreeList ID="IKTTreeList" ClientInstanceName="IKTTreeList" runat="server" Width="100%" AutoGenerateColumns="false" 
        KeyFieldName="Id" ParentFieldName="Parent.Id"
        OnDataBinding="IKTTreeList_DataBinding"
        SettingsPager-AlwaysShowPager="true" SettingsPager-PageSize="18" SettingsPager-Mode="ShowPager" OnHtmlRowPrepared="IKTTreeList_HtmlRowPrepared">
        <Columns>
            <dxwtl:TreeListDataColumn FieldName="Code" Caption="Код" Width="40px" />
            <dxwtl:TreeListDataColumn FieldName="Name" Caption="Наименование" />
        </Columns>
        <ClientSideEvents 
            FocusedNodeChanged="function(s, e) {
                        var key = s.GetFocusedNodeKey();
                        if ( s.GetNodeState(key) != 'Child') {
                            s.SetFocusedNodeKey(false);
                        }
                        else {
                            callBack.PerformCallback(key);
                        }
                }" 
            />
        <Settings ShowTreeLines="False" SuppressOuterGridLines="true" />
        <SettingsBehavior AllowFocusedNode="True" />

        <Border BorderStyle="Solid" />
    </dxwtl:ASPxTreeList>

    <%--<a href="javascript:window.parent.PopupClose(result);" class="command" style="padding-top: 20px; display: inline-block;">Выбрать</a>--%>
    <%--<a href="javascript:RowSelect();" class="command" style="padding-top: 20px; display: inline-block;">Выбрать</a>--%>
</asp:Content>
