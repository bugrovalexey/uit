﻿using Uviri.MKRF.Entities.PlanReport.SectionIX;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionIX
{
    public class EmplSupportMap : ClassMapBase<EmplSupport>
    {
        public EmplSupportMap()
            : base("PRS_9")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_9_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@PRS_9_CA_Stat_Cnt = ?
//                          ,@PRS_9_CA_Fact_Cnt = ?
//                          ,@PRS_9_TP_Cnt = ?
//                          ,@PRS_9_TP_Stat_Cnt = ?
//                          ,@PRS_9_TP_Fact_Cnt = ?
//                          ,@PRS_9_Dig_Sign_Cnt = ?
//                          ,@Plans_ID = ?
//						  ,@PRS_9_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_9_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_9_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_9_Delete @PRS_9_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.CA_StatCount, "PRS_9_CA_Stat_Cnt");
            Map(x => x.CA_FactCount, "PRS_9_CA_Fact_Cnt");
            Map(x => x.TP_UnitCount, "PRS_9_TP_Cnt");
            Map(x => x.TP_StatCount, "PRS_9_TP_Stat_Cnt");
            Map(x => x.TP_FactCount, "PRS_9_TP_Fact_Cnt");
            Map(x => x.DS_UnitCount, "PRS_9_Dig_Sign_Cnt");

            References<Plan>(x => x.Plan, "Plans_ID");
        }
    }
}