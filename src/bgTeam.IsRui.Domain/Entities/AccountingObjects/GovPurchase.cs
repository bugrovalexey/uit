﻿using System;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    public class GovPurchase : EntityBase
    {
        public GovPurchase() { }

        public GovPurchase(AccountingObject ao)
        {
            this.AccountingObject = ao;
        }


        public virtual string Number { get; set; }

        public virtual GovPurchaseTypeEnum Type { get; set; }

        public virtual string Name { get; set; }

        public virtual DateTime? Date { get; set; }

        public virtual decimal Price { get; set; }

        public virtual decimal Summ { get; set; }

        public virtual int IdZakupki360 { get; set; }

        public virtual AccountingObject AccountingObject { get; set; }
    }
}
