﻿namespace bgTeam.IsRui.Domain._Mapping.Claims
{
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class ClaimDocumentMap : ClassMapHiLo<ClaimDocument>
    {
        public ClaimDocumentMap()
            : base("ClaimDocument")
        {
        }

        protected override void InitMap()
        {
            References<Claim>(x => x.Claim, "ClaimDocument_Claim_ID").Not.Nullable();
            References<Files>(x => x.File, "ClaimDocument_Files_ID").Nullable();

            Map(x => x.DateAdoption, "ClaimDocument_DateAdoption").Nullable();
            Map(x => x.Name, "ClaimDocument_Name").Nullable();
            Map(x => x.Number, "ClaimDocument_Number").Nullable();
            Map(x => x.ParticleOfDocument, "ClaimDocument_ParticleOfDocument").Nullable();
        }
    }
}
