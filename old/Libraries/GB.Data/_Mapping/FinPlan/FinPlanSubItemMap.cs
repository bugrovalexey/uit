﻿using Uviri.MKRF.Entities.FinPlan;

namespace Uviri.MKRF.Mapping.FinPlan
{
    public class FinPlanSubItemMap : ClassMapBase<FinPlanSubItem>
	{
		public FinPlanSubItemMap()
            : base("Fin_Plan_Sub_Item")
		{
//            Table("w_");

//            Id(x => x.Id, "Fin_Plan_Sub_Item_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_Fin_Plan_Sub_Item_Insert 
//						  @Fin_Plan_Sub_Item_Name = ?
//						 ,@Fin_Plan_Sub_Item_Current_Year_Vol = ?
//						 ,@Fin_Plan_Sub_Item_First_Year_Vol = ?
//						 ,@Fin_Plan_Sub_Item_Second_Year_Vol = ?
//						 ,@Fin_Plan_Sub_Item_Current_Year_Add_Vol = ?
//						 ,@Fin_Plan_Sub_Item_First_Year_Add_Vol = ?
//						 ,@Fin_Plan_Sub_Item_Second_Year_Add_Vol = ?
//						 ,@Fin_Plan_Item_ID = ?
//                         ,@Fin_Plan_Sub_Item_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Fin_Plan_Sub_Item_Update 
//						  @Fin_Plan_Sub_Item_Name = ?
//						 ,@Fin_Plan_Sub_Item_Current_Year_Vol = ?
//						 ,@Fin_Plan_Sub_Item_First_Year_Vol = ?
//						 ,@Fin_Plan_Sub_Item_Second_Year_Vol = ?
//						 ,@Fin_Plan_Sub_Item_Current_Year_Add_Vol = ?
//						 ,@Fin_Plan_Sub_Item_First_Year_Add_Vol = ?
//						 ,@Fin_Plan_Sub_Item_Second_Year_Add_Vol = ?
//						 ,@Fin_Plan_Item_ID = ?
//                         ,@Fin_Plan_Sub_Item_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Fin_Plan_Sub_Item_Delete 
//						  @Fin_Plan_Sub_Item_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Fin_Plan_Sub_Item_Name");
			Map(x => x.CurrentYearVolume, "Fin_Plan_Sub_Item_Current_Year_Vol");
			Map(x => x.FirstYearVolume, "Fin_Plan_Sub_Item_First_Year_Vol");
			Map(x => x.SecondYearVolume, "Fin_Plan_Sub_Item_Second_Year_Vol");
			Map(x => x.CurrentYearAddVolume, "Fin_Plan_Sub_Item_Current_Year_Add_Vol");
			Map(x => x.FirstYearAddVolume, "Fin_Plan_Sub_Item_First_Year_Add_Vol");
			Map(x => x.SecondYearAddVolume, "Fin_Plan_Sub_Item_Second_Year_Add_Vol");

			References<FinPlanItem>(x => x.FinPlanItem, "Fin_Plan_Item_ID").Not.Nullable();
        }
    }
}