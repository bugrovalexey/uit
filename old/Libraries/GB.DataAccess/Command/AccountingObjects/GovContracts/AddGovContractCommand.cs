﻿using AutoMapper;
using GB.Data.AccountingObjects;
using GB.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.GovContracts
{
    public class AddGovContractCommand : ICommand<AddGovContractContext, GovContract>
    {
        ISessionFactoryGb factory;

        public AddGovContractCommand()
        {
            factory = new SessionFactory();
        }

        public GovContract Execute(AddGovContractContext context)
        {
            var ao = new Repository.Repository<AccountingObject>().Get(context.IdAO);

            var c = new GovContract(ao);

            c.Stage = ao.Stage;

            Mapper.Map(context, c);

            using (var session = factory.Create())
            {
                session.Insert(c);
                session.Commit();
            }

            return c;
        }
    }
}
