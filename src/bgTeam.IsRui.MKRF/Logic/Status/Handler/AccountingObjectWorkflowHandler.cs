﻿namespace bgTeam.IsRui.MKRF.Logic.Status.Handler
{
    using System;
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.IsRui.DataAccess.Command.AccountingObjects.Workflow;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Workflow;

    public class AccountingObjectWorkflowHandler : IWorkflowHandler<AccountingObject>
    {
        private readonly ICommandBuilder _commandBuilder;

        public AccountingObjectWorkflowHandler(ICommandBuilder commandBuilder)
        {
            _commandBuilder = commandBuilder;
        }

        public Task AfterChange(AccountingObject obj)
        {
            var status = (StatusEnum)obj.Status.Id;

            switch (status)
            {
                case StatusEnum.AoDraft:
                case StatusEnum.AoAgreed:
                case StatusEnum.AoOnCompletion:
                case StatusEnum.AoAccordHigher:
                    break;
                default:
                    throw new Exception("Пришел не обрабатываемый статус");
            }

            return _commandBuilder.Build(new UpdateCommentStatusContext { AO = obj }).ExecuteAsync();
        }

        public Task<WorkflowHandlerResult> BeforeChange(AccountingObject obj, Status status)
        {
            return Task.FromResult(new WorkflowHandlerResult { Successful = true });
        }
    }
}
