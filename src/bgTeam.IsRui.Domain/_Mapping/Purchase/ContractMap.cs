﻿using bgTeam.IsRui.Domain.Entities.Purchase;

namespace bgTeam.IsRui.Domain._Mapping.Purchase
{
    /// <summary>Контракт</summary>
    internal class ContractMap : ClassMapHiLo<Contract>
    {
        public ContractMap()
            : base("Contract") { }

        protected override void InitMap()
        {
            Map(x => x.Number, "Contract_Number").Not.Nullable();

            Map(x => x.Sign_Date, "Contract_Sign_Date");
            Map(x => x.Execution_Year, "Contract_Execution_Year");
            Map(x => x.Execution_Month, "Contract_Execution_Month");

            Map(x => x.Price, "Contract_Price");
            Map(x => x.Customer_Reg_Num, "Contract_Customer_Reg_Num");
            Map(x => x.Customer_Name, "Contract_Customer_Name");

            Map(x => x.Budget_Name, "Contract_Budget_Name");
            Map(x => x.Budget_Level, "Contract_Budget_Level");
            Map(x => x.KBK, "Contract_KBK");
            Map(x => x.MaxSum, "Contract_Max_Sum");
            Map(x => x.Reg_Num, "Contract_Reg_Num");

            HasMany<ContractProduct>(x => x.Products).KeyColumns.Add("Contract_ID").Inverse().Cascade.AllDeleteOrphan();
        }
    }
}