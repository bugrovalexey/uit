﻿namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    /// <summary>
    /// Связь Акта и Мероприятия ОУ
    /// </summary>
    public class ActToAOActivity : EntityBase
    {
        /// <summary>
        /// Акт
        /// </summary>
        public virtual ActOfAcceptance ActOfAcceptance { get; set; }

        /// <summary>
        /// Мероприятие
        /// </summary>
        public virtual AoActivity AoActivity { get; set; }
    }
}
