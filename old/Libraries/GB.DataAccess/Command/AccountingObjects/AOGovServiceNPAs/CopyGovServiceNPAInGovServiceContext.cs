﻿using GB.Data.Frgu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServiceNPAs
{
    public class CopyGovServiceNPAInGovServiceContext : ICommandContext
    {
        public IList<GovServiceNPA> Npas { get; set; }
        public int AOGovServiceId { get; set; }
    }
}
