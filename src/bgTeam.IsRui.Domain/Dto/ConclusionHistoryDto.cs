﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System.Collections.Generic;

    public class ConclusionHistoryDto
    {
        public int Id { get; set; }

        public int? AuthorId { get; set; }

        public string Author { get; set; }

        public int? StatusId { get; set; }

        public string Status { get; set; }

        public string Text { get; set; }

        public IList<FilesDto> Files { get; set; }
    }
}
