﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class SetInformationInteractionStory : IStory<SetInformationInteractionStoryContext, InformationInteractionDto>
    {
        private readonly IMapperBase _mapperBase;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;

        public SetInformationInteractionStory(
            IMapperBase mapperBase,
            ISessionNHibernateFactory factory,
            IRepositoryRui repository)
        {
            _mapperBase = mapperBase;
            _factory = factory;
            _repository = repository;
        }

        public InformationInteractionDto Execute(SetInformationInteractionStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<InformationInteractionDto> ExecuteAsync(SetInformationInteractionStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            if (!context.AoId.HasValue && string.IsNullOrWhiteSpace(context.ExternalSystemName))
            {
                throw new IsRuiException("Необходимо указать мероприятие или наименование внешней системы");
            }

            var activity = _repository.Get<PlansActivity>(context.ActivityId);

            if (activity == null)
            {
                throw new IsRuiException("Не найдено мероприятие");
            }

            var res = _repository.GetAllEx<InformationInteraction>(q =>
                    q.Where(x => x.PlansActivity.Id == context.ActivityId.Value))
                .FirstOrDefault();

            if (res == null)
            {
                res = new InformationInteraction
                {
                    PlansActivity = activity
                };
            }

            res.Name = context.ExternalSystemName;

            if (context.AoId.HasValue)
            {
                res.AccountingObject = _repository.Get<AccountingObject>(context.AoId.Value);
            }
            else
            {
                res.AccountingObject = null;
            }

            using (var session = _factory.OpenSession())
            {
                await session.InsertAsync(res);
            }

            return _mapperBase.Map(res, new InformationInteractionDto());
        }
    }
}
