﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="Services.aspx.cs" Inherits="GB.MKRF.Web.Selectors.Services" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
     <script type="text/javascript">
     // <![CDATA[
         function rowClick(s, e) {
             var id = s.GetRowKey(e.visibleIndex);
             s.GetRowValues(e.visibleIndex, 'Id;Service', function (values) {
                 var result = id + ';' + values[1];

                 window.parent.PopupClose(result);
             });
         }
      // ]]> 
    </script>
    <dxwgv:ASPxGridView ID="ServicesGrid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" KeyFieldName="Id"
        OnBeforePerformDataSelect="ServicesGrid_BeforePerformDataSelect" Width="100%" OnHtmlRowCreated="ServicesGrid_HtmlRowCreated">
        <Columns>
            <dxwgv:GridViewDataTextColumn FieldName="ParentId" Visible="false" ShowInCustomizationForm="false">
            </dxwgv:GridViewDataTextColumn>
       <%--     <dxwgv:GridViewDataTextColumn Caption="ОГВ" FieldName="ogv" Settings-AllowSort="False" Visible="False" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
                <DataItemTemplate>
                    <%#getIndent(Eval("ParentId").ToString(), Eval("ogv").ToString())%>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>--%>
            <dxwgv:GridViewDataMemoColumn Caption="Услуга" FieldName="Service" Settings-AllowSort="False" Width="500px">
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
                <DataItemTemplate>
                    <%#getIndent(Eval("ParentId").ToString(), Eval("Service").ToString())%>
                </DataItemTemplate>
                <PropertiesMemoEdit EncodeHtml="False">
                </PropertiesMemoEdit>
            </dxwgv:GridViewDataMemoColumn>
        </Columns>
        
        <Styles>
            <AlternatingRow Enabled="true" />
            <Header Wrap="True" />
        </Styles> 
        <ClientSideEvents RowClick="rowClick" />
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="15" />
    </dxwgv:ASPxGridView>
</asp:Content>
