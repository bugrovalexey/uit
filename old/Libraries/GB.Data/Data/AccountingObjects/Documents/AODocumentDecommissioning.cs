﻿using GB.Data.Documents;

namespace GB.Data.AccountingObjects.Documents
{
    /// <summary>
    ///  Документ вывода из эксплуатации ОУ
    /// </summary>
    public class AODocumentDecommissioning : AODocumentBase
    {
        internal static readonly DocTypeEnum DocType = DocTypeEnum.DecommissioningAccountingObject;    

        protected override DocTypeEnum GetTypeDocument()
        {
            return DocType;
        }

    }
}
