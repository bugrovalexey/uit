﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ObjectLinksMap : ClassMapBase<ObjectLinks>
    {
        public ObjectLinksMap()
            : base("Exp_Object_Link")
        {
//            Table("w_");

//            Id(x => x.Id, "Exp_Object_Link_ID").GeneratedBy.Custom(typeof(IdGenerator));

            


//            string sql =   @"@Portal_Table_ID = ?
//                            ,@Exp_Object_Link_Obj_ID = ?
//                            ,@Portal_Table_Par_ID = ?
//                            ,@Exp_Object_Link_Obj_Par_ID = ?
//                            ,@Exp_Object_Link_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Exp_Object_Link_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Object_Link_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Exp_Object_Link_Delete @Exp_Object_Link_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            ReferencesAny(x => x.Parent)
                .AddMetaValue<Plan>(((int)TableTypeEnum.Plan).ToString())
                .EntityTypeColumn("Portal_Table_ID")
                .EntityIdentifierColumn(" Exp_Object_Link_Obj_ID").IdentityType<int>();

            ReferencesAny(x => x.Child)
                .AddMetaValue<Plan>(((int)TableTypeEnum.Plan).ToString())
                .EntityTypeColumn(" Portal_Table_Par_ID")
                .EntityIdentifierColumn("  Exp_Object_Link_Obj_Par_ID").IdentityType<int>();
        }
    }
}
