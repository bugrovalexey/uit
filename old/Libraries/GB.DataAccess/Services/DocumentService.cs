﻿using System;
using System.Collections.Generic;
using System.Linq;
using GB.Data;
using GB.Data.Common;
using GB.Data.Documents;
using GB.Helpers;

namespace GB.DataAccess.Services
{
    public class DocumentArgs
    {
        public object FileInfo;
        public object Name;
        public object Num;
        public object DocTypeText;
        public object Comment;
        public object Date;
        public User Author;
    }

    public class DocumentService : ServiceBase<Files>
    {
        public void Save(string fileList, IList<Files> listDocs, EntityBase owner, DocTypeEnum type)
        {
            Dictionary<string, string> newFiles = StringHelper.FileListToDictionary(fileList);

            foreach (Files file in listDocs)
            {
                string id = file.Id.ToString();
                if (newFiles.ContainsKey(id))
                {
                    newFiles.Remove(id);
                }
                else
                {
                    Delete(file);
                }
            }

            foreach (string key in newFiles.Keys)
            {
                Files file = new Files(type, owner);
                file.FileName = file.Name = newFiles[key];
                SaveOrUpdate(file);

                string guidString = key;
                try
                {
                    int slashIndex = guidString.IndexOf('/');
                    if (slashIndex >= 0)
                    {
                        guidString = guidString.Substring(0, slashIndex);
                    }
                    Guid guid = new Guid(guidString);
                    FileProvider.MoveToGeneric(guid, file.Id);
                }
                catch (Exception e)
                {
                    base.Delete(file);
                    Logger.Fatal(string.Format("Не удалось переместить файл из temp в хранилище. key={0} guidString={1} Document.Id={2}",
                        key, guidString, file.Id), e);
                }
            }
        }

        public void SaveOrUpdate(Files doc, DocumentArgs args)
        {
            var fileKey = StringHelper.FileListToDictionary((string)args.FileInfo).Select(x => x.Key).FirstOrDefault();
            var fileInfo = FileInfo.FromShortUrl(fileKey);

            if (fileInfo == null ||
                (doc.Id == 0 && !(fileInfo.Id is Guid)) ||
                (doc.Id > 0 && fileInfo.Id == null))
            {
                throw new Exception("Необходимо выбрать файл");
            }

            doc.Name = (string)args.Name;
            doc.Num = (string)args.Num;
            doc.DocTypeText = (string)args.DocTypeText;
            doc.Comm = (string)args.Comment;
            doc.Date = (DateTime?)args.Date;
            doc.Author = args.Author;
            doc.FileName = fileInfo.FileName;
            //doc.File = null;

            // по-хорошему нужно удерживать транзакцию открытой, пока не переместим(удалим) файл.
            SaveOrUpdate(doc);

            try
            {
                if (fileInfo.Id is Guid)
                {
                    FileProvider.MoveToGeneric((Guid)fileInfo.Id, doc.Id, true);
                }
                else if (fileInfo.Id == null)
                {
                    FileProvider.DeleteFile(doc.Id);
                }
            }
            catch (Exception e)
            {
                Logger.Fatal(string.Format("Не удалось выполнить операцию с файлом. key={0} id={1} Document.Id={2}",
                                            fileKey,
                                            (fileInfo.Id != null) ? fileInfo.Id.ToString() : "null",
                                            doc.Id),
                             e);
            }
        }

        public new void Delete(int docId)
        {
            base.Delete(docId);
            FileProvider.DeleteFile(docId);
        }
    }
}