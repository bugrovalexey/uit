﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoContractAddOrUpdateStory : IStory<AoContractAddOrUpdateStoryContext, ContractDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;

        public AoContractAddOrUpdateStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            ISessionNHibernateFactory factory)
        {
            _mapper = mapper;
            _repository = repository;
            _factory = factory;
        }

        public ContractDto Execute(AoContractAddOrUpdateStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ContractDto> ExecuteAsync(AoContractAddOrUpdateStoryContext context)
        {
            AccountingObjectContract contract;

            using (var session = _factory.OpenSession())
            {
                if (context.Id.HasValue)
                {
                    contract = _repository.Get<AccountingObjectContract>(context.Id);
                }
                else
                {
                    contract = new AccountingObjectContract();

                    contract.AccountingObject = new AccountingObject() { Id = context.AoId.Value };
                }

                _mapper.Map(context, contract);

                contract.ChangeDate = DateTime.Now;

                await session.SaveOrUpdateAsync(contract);
            }

                return _mapper.Map(contract, new ContractDto());
        }
    }
}
