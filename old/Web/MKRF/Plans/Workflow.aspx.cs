﻿using System;

namespace GB.MKRF.Web.Plans
{
    public partial class Workflow : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            scc.Owner = (Master as PlanCard).CurrentPlan.Demand;

            base.OnInit(e);
        }
    }
}