﻿using System.Collections.Generic;
using Uviri.Entity.Directories;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>Мероприятие для муниципальных объектов</summary>
    public class PlansActivityMU : EntityBase
    {
        protected PlansActivityMU()
        {
            Regions = new List<PlansActivityMURegion>();
        }

        public PlansActivityMU(Plan Plan)
        {
            this.Plan = Plan;
            Regions = new List<PlansActivityMURegion>();
            ReportRegions = new List<PlansActivityMUReportRegion>();
        }

        /// <summary>План</summary>
        public virtual Plan Plan { get; protected set; }

        /// <summary>Код по БК ГРБС</summary>
        public virtual GRBS GRBS { get; set; }

        /// <summary>Код по БК ПЗ/РЗ</summary>
        public virtual SectionKBK SectionKBK { get; set; }

        /// <summary>Код по БК ЦСР</summary>
        public virtual ExpenseItem ExpenseItem { get; set; }

        /// <summary>Код по БК ВР</summary>
        public virtual WorkForm WorkForm { get; set; }

        /// <summary> Регион  </summary>
        public virtual IList<PlansActivityMURegion> Regions { get; protected set; }

        /// <summary> Регион  </summary>
        public virtual IList<PlansActivityMUReportRegion> ReportRegions { get; protected set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Аннотация</summary>
        public virtual string Annotation { get; set; }//null


        /// <summary>Объем фактически израсходованных бюджетных ассигнований на год, предшествующий отчетному финансовому году</summary>
        public virtual double FactY0 { get; set; }
        /// <summary>Объем фактически израсходованных бюджетных ассигнований на отчетный финансовый год</summary>
        public virtual double FactY1 { get; set; }
        /// <summary>Объем фактически израсходованных бюджетных ассигнований на текущий финансовый год</summary>
        public virtual double FactY2 { get; set; }


        /// <summary>Объем фактически израсходованных  бюджетных ассигнований на очередной финансовый год</summary>
        public virtual double BudgetY0 { get; set; }
        /// <summary>Объем фактически израсходованных  бюджетных ассигнований на первый год планового периода</summary>
        public virtual double BudgetY1 { get; set; }
        /// <summary>Объем фактически израсходованных  бюджетных ассигнований на второй год планового периода</summary>
        public virtual double BudgetY2 { get; set; }
        
        
        /// <summary>Дополнительная потребность на очередной финансовый год</summary>
        public virtual double ExpenseAddY0 { get; set; }
        /// <summary>Дополнительная потребность на первый год планового периода</summary>
        public virtual double ExpenseAddY1 { get; set; }
        /// <summary>Дополнительная потребность на второй год планового периода</summary>
        public virtual double ExpenseAddY2 { get; set; }
        

         /// <summary>Объем софинансирования на очередной финансовый год</summary>
        public virtual double JointFundingY0 { get; set; }
        /// <summary>Объем софинансирования на первый год, предшествующий очередному финансовому</summary>
        public virtual double JointFundingY1 { get; set; }
        /// <summary>Объем софинансирования на второй год, предшествующий очередному финансовому</summary>
        public virtual double JointFundingY2 { get; set; }

        /// <summary>Подпрограмма</summary>
        public virtual string Subprogram { get; set; }
    }
}