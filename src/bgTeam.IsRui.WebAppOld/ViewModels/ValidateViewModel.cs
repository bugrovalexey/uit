﻿namespace bgTeam.IsRui.WebApp.ViewModels
{
    public class ValidateViewModel
    {
        public const string RequiredText = "Введите значение";
        public const string NotZeroValueText = "Выберите значение";
        protected const string NotMatchMask = "Введенное значение не соответствует маске";
        public const string NotUrl = "Неверный формат URL (URL должен начинаться с протокола - http://; ftp:// и т.д.)";
        public const string NotEmail = "Неверный формат Email";

        protected const double MoneyMin = 0;
        protected const double MoneyMax = 99999999999.99d;
        protected const string RangeMoneyText = "Введенная сумма не должна быть меньше {1} и превышать {2}";

        protected const string RangeIntText = "Введенное значение не должно быть меньше {1} и больше {2}";

        protected const string NotMatchPasswords = "Пароль и подтверждение пароля не совпадают";

        public const string FileRequired = "Загрузите файл";
        public const string NotDate = "Введите корректную дату";

        public const string OneOrMoreRows = "Таблица должна содержать одну или более строк";
    }
}