﻿using bgTeam.IsRui.Common;
using bgTeam.IsRui.Domain.Entities;
using FluentNHibernate.Mapping;
using System.Text;

namespace bgTeam.IsRui.Domain._Mapping
{
    /// <summary>
    /// Базовый класс для мэпинга
    /// </summary>
    internal abstract class ClassMapBase<T> : ClassMap<T> where T : EntityBase
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ClassMapBase(string table, bool readOnly)
            : this(table, null, readOnly)
        {
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public ClassMapBase(string table, string prefix = null, bool readOnly = false)
        {
            if (string.IsNullOrEmpty(prefix))
                Table("w_" + table);
            else
                Table(prefix + table);

            Id(x => x.Id, table + "_ID")
                .GeneratedBy.HiLo("NH_HiLo", "NextHi", "1", "TableKey = 'dbo." + table + "'");
            //Id(x => x.Id, table + "_ID");
            //.GeneratedBy.Custom(typeof(IdGenerator)).UnsavedValue((int)EntityBase.UnsavedId);

            InitMap();


            InitProcedure(table, readOnly);

#if CACHING
            Cache.ReadWrite();
#endif
        }

        /// <summary>
        /// Инициализировать маппинг
        /// </summary>
        protected abstract void InitMap();

        /// <summary>
        /// Инициализировать процедуры
        /// </summary>
        private void InitProcedure(string table, bool readOnly)
        {
            if (!readOnly)
            {
                MappingProviderStore providers = this.Provider;

                StringBuilder sb = new StringBuilder();

                foreach (var item in providers.Properties)
                {
                    var c = item.GetPropertyMapping();

                    GetColumnsDescription(c.Columns, sb);
                }

                foreach (var item in providers.References)
                {
                    var c = item.GetManyToOneMapping();

                    GetColumnsDescription(c.Columns, sb);
                }

                //foreach (var item in providers.Components)
                //{
                //    var cols = item.GetComponentMapping();
                //    foreach (var c in cols.Properties)
                //    {
                //        GetColumnsDescription(c.Columns, sb);
                //    }
                //}

                foreach (var item in providers.Anys)
                {
                    var c = item.GetAnyMapping();

                    GetColumnsDescription(c.TypeColumns, sb);
                    GetColumnsDescription(c.IdentifierColumns, sb);
                }

                sb.Append("@");
                sb.Append(table);
                sb.Append("_ID = ?");

                SqlInsert(string.Concat(@"exec ZZZ_Prc_", table, "_Insert\r\n", sb)).Check.None();
                SqlUpdate(string.Concat(@"exec ZZZ_Prc_", table, "_Update\r\n", sb)).Check.None();
                SqlDelete(string.Concat(@"exec ZZZ_Prc_", table, "_Delete @", table, "_ID = ?")).Check.None();
            }
            else
            {
#if CACHING
                ReadOnly();
#endif
                SqlInsert(@"raiserror ('Unsupported',10,1)").Check.None();
                SqlUpdate(@"raiserror ('Unsupported',10,1)").Check.None();
                SqlDelete(@"raiserror ('Unsupported',10,1)").Check.None();
            }
        }

        private static void GetColumnsDescription(System.Collections.Generic.IEnumerable<FluentNHibernate.MappingModel.ColumnMapping> colums, StringBuilder sb)
        {
            foreach (var col in colums)
            {
                if (!string.IsNullOrEmpty(col.Name) && !col.ReadOnly)
                {
                    sb.Append("@");
                    sb.Append(col.Name);
                    sb.Append(" = ?,\r\n");
                }
                else
                    break;
            }
        }

        ///// <summary>
        ///// Получаем объект который содержит в себе маппинг
        ///// </summary>
        ///// <remarks>
        ///// Не используется, сделали его public в проекте FluentNHibernate
        ///// </remarks>
        //private MappingProviderStore GetProvider()
        //{
        //    System.Reflection.FieldInfo fi = typeof(ClasslikeMapBase<T>).GetField("providers", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

        //    if (fi == null)
        //        throw new Exception("Не найден Provider");

        //    MappingProviderStore providers = fi.GetValue(this) as MappingProviderStore;

        //    if (providers == null)
        //        throw new Exception("Не найден Provider");

        //    return providers;
        //}
    }
}