﻿namespace GB.MKRF.Entities.Documents
{
    //public class DocGroup : EntityBase
    //{
    //    /// <summary>Название</summary>
    //    public virtual string Name { get; set; }
    //}

    public enum DocGroupEnum
    {
        None = 0,

        // федеральные законы
        FederalLaws = 1,

        // акты президента рф
        PresidentActs = 2,

        // акты правительства рф
        GovernmentActs = 3,

        // акты Минкомсвязи
        MKSActs = 4,

        // Акты ФОИВ
        FOIVActs = 5,

        // Госты
        GOST = 6,

        // РД
        RD = 7,

        // Международные документы
        ISO = 8,

        // Иные документы
        Other = 9
    }
}