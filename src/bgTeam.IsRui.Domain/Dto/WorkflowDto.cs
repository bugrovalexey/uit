﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class WorkflowDto
    {
        public int StatusId { get; set; }

        public int EntityType { get; set; }

        public int EntityOwnerId { get; set; }
    }
}
