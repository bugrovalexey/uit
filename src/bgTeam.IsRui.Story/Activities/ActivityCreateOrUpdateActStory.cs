﻿namespace bgTeam.IsRui.Story.Activities
{
    using System;
    using System.Threading.Tasks;
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Story.AccountingObjects;
    using bgTeam.IsRui.Story.Common;

    public class ActivityCreateOrUpdateActStory : IStory<ActivityCreateOrUpdateActStoryContext, ActivityActDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IFileProviderRui _fileProvider;

        public ActivityCreateOrUpdateActStory(
            IRepositoryRui repository,
            ISessionNHibernateFactory factory,
            IMapperBase mapperBase,
            IFileProviderRui fileProvider)
        {
            _repository = repository;
            _factory = factory;
            _mapper = mapperBase;
            _fileProvider = fileProvider;
        }

        public ActivityActDto Execute(ActivityCreateOrUpdateActStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityActDto> ExecuteAsync(ActivityCreateOrUpdateActStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            PlansActivityAct act;

            using (var session = _factory.OpenSession())
            {
                if (context.Id.HasValue)
                {
                    act = _repository.Get<PlansActivityAct>(q => q.Where(x => x.Id == context.Id));

                    if (context.File != null && context.File.Id != act.FileId)
                    {
                        await new FileDeleteStory(_repository, _fileProvider, _factory)
                            .ExecuteAsync(new FileDeleteStoryContext
                            {
                                Id = act.FileId,
                            });
                    }
                }
                else
                {
                    act = new PlansActivityAct();
                    act.DateAdoption = DateTime.Now;
                }

                if (context.File != null
                    && ((act.FileId == null && context.File.Id == null)
                        || (context.File.Id != act.FileId && context.File.Id == null)))
                {
                    var addFile = await new FileAddStory(_mapper, _repository, _fileProvider, _factory)
                        .ExecuteAsync(new FileAddStoryContext()
                        {
                            Type = DocTypeEnum.ActivityAct,
                            Entity = _repository.Get<PlansActivity>(context.ActivityId),
                            Name = context.File.FileName,
                            TempGuid = context.File.TempGuid,
                            Url = context.File.Url,
                            MimeType = context.File.MimeType,
                            Kind = context.File.Kind,
                            Size = context.File.Size
                        });

                    context.File.Id = addFile.Id;
                    context.File.FileName = addFile.Name;
                    context.File.Url = addFile.Url;
                    context.File.MimeType = addFile.MimeType;
                    context.File.Kind = addFile.Kind;
                    context.File.Size = addFile.Size;
                    context.File.Extension = addFile.Extension;
                }

                _mapper.Map(context, act);

                act.Activity = new PlansActivity { Id = context.ActivityId.Value };

                act.ChangeDate = DateTime.Now;

                await session.SaveOrUpdateAsync(act);
            }

            var result = _mapper.Map(act, new ActivityActDto());

            if (context.File != null)
            {
                result.File = new ActsFileDto();
                result.File.Id = context.File.Id;
                result.File.FileName = context.File.FileName;
                result.File.Url = context.File.Url;
                result.File.MimeType = context.File.MimeType;
                result.File.Kind = context.File.Kind;
                result.File.Size = context.File.Size;
                result.File.Extension = context.File.Extension;
            }

            return result;
        }
    }
}
