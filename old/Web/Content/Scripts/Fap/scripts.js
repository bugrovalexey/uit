﻿// Показать панель с ошибкой
function ErrorPanelShow(text) {
    SetDivProperties("div_error_panel", "", text);
}


// Спрятать окно с ошибкой
function ErrorPanelHide() {
    SetDivProperties("div_error_panel", "display_none", null);
}


//Найти Error панель и задать ей текст 
function SetDivProperties(component, cl, text) {
    var diverr = document.getElementById(component);

    if (diverr) {
        diverr.className = cl;
        diverr.innerHTML = text;
    }
}

function SetStyle(component_id, style) {
    var component = document.getElementById(component_id);

    if (component) {
        component.className = style;
    }
}

function ValidateRow(component_id, value) {
    if (!value)
        SetStyle(component_id, "not_valid");
    else
        SetStyle(component_id, null);
}

//Возвращает количество выбранных записей на странице таблицы
function GetSelectedRowCountOnPage(grid) {
    var selInput = grid.GetSelectionInput();

    if (!selInput) return 0;

    var selCount = 0;
    var checkList = selInput.value;

    for (var i = 0; i < checkList.length; i++) {
        if (checkList.charAt(i) == "T") selCount++;
    }

    return selCount;
}

//Удалить выбранные строки в таблице
function GridDeleteSelectedRow(grid) {
    if (confirm('Вы действительно хотите удалить запись?')) {
        grid.PerformCallback('Delete');
    }
}

//Выбрать/снять выделение всех строк в таблице
function GridSelectAllRow(grid) {
    if (GetSelectedRowCountOnPage(grid) != grid.pageRowCount)
        grid.SelectAllRowsOnPage(true);
    else
        grid.SelectAllRowsOnPage(false);
}

//Инициализация таблицы
function InitializeTable(table, urlPage, addBackUrl, container, canEdit) {
    var isLink = false;

    $("td .cell_not_redirect", table).bind("click", function (e) {
        isLink = true;
    });

    $("tr.row_redirect td a", table).bind("click", function (e) {
        isLink = true;
    });
    
    if (canEdit) {
        $("tr.row_redirect", table).bind("click", function(e) {
            if (!isLink) {

                var url = GetGridRowEditUrl(urlPage, addBackUrl, $(this).attr("edit"));

                if (container)
                    ShowWindow(container, url);
                else {
                    if (!e.ctrlKey)
                        window.location.assign(url);
                    else
                        window.open(url);
                }
            }

            isLink = false;
        });
    }
}

function ShowWindowNew(table, urlPage, addBackUrl, container) {
    var url = GetGridRowEditUrl(urlPage, addBackUrl, $(this).attr("edit"));

    if (container)
        ShowWindow(container, url);
    else
        window.location.assign(url);
}

function GetGridRowEditUrl(url, addBackUrl, id) {
    var mass = url.split(';');

    url = mass[0] + "?";

    if (id) {
        url += "id=" + id;
    }

    if (mass.length > 1) {
        for (var i = 1; i < mass.length; i++) {
            url += "&" + mass[i];
        }
    }

    if (addBackUrl) {
        url += "&backurl=" + encodeURIComponent(window.location.pathname + window.location.search);
    }

    return url;
}

function ShowWindow(form, url) {
    form.SetContentHtml('');
    
    if (url) {
        url += "&win=true";
        form.SetContentUrl(url);
    }

    form.SetWidth(1000);
    form.SetHeight(document.documentElement.clientHeight - 100);
    form.Show();
}

function HideWindow(form) {
    //form.SetContentHtml('');
    form.Hide();
}

function SaveEditForm(form, grid) {
    var i = form.GetContentIFrame();

    if (i.contentWindow.SaveForm(grid)) {
        form.Hide();
        return true;
    }

    return false;
}

// Валидация данных 
function ValidateForm() {
    var validationGroup = '';
    return RaisePageValidation(validationGroup);
}

function RaisePageValidation(validationGroup) {
    var validationProcs = [RaiseDxValidation, RaiseStandardValidation];
    var result = true;
    for (var index = 0; index < validationProcs.length; index++)
        result = validationProcs[index](validationGroup) && result;
    return result;
}

/* Different validation procs */
function RaiseDxValidation(validationGroup) {
    if (typeof (ASPxClientEdit) != "undefined" && typeof (ASPxClientEdit.ValidateGroup) == "function")
        return ASPxClientEdit.ValidateGroup(validationGroup);
    else
        return true;
}
function RaiseStandardValidation(validationGroup) {
    if (typeof (Page_IsValid) != "undefined" && Page_IsValid != null && typeof (Page_ClientValidate) == "function") {
        Page_ClientValidate(validationGroup);

        for (i = 0; i < Page_Validators.length; i++) {
            if (Page_Validators[i].isvalid) {
                $(Page_Validators[i]).closest('table').removeClass("ErorField");
            }
            else {
                $(Page_Validators[i]).closest('table').addClass("ErorField");
            }
        }

        return Page_IsValid;
    }
    return true;
}