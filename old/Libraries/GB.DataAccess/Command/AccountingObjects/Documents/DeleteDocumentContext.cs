﻿
namespace GB.DataAccess.Command.AccountingObjects.Documents
{
    public class DeleteDocumentContext : ICommandContext
    {
        public int? xId { get; set; }
        internal SessionHibernate Session { get; set; }
    }
}
