﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GB.Infrastructure;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Workflow;

namespace GB.Coord.Workflow
{
    internal static class WorkflowRegistration
    {
        static readonly object syncRoot = new object();
        static bool isRegistered;

        internal static void RegisterHandlers()
        {
            lock (syncRoot)
            {
                if (isRegistered) return;

                var manager = Singleton<WorkflowManager>.Instance;
                manager.RegisterHandler<PlansActivity>(new ActivityWorkflowHandler());
                manager.RegisterHandler<ActivityExpertiseConclusion>(new ExpertiseWorkflowHandler());

                isRegistered = true;
            }
        }
    }
}