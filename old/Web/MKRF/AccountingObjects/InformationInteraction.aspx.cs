﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB;
using GB.Extentions;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Dtos;
using GB.MKRF.Entities.AccountingObjects;
using GB.MKRF.Extensions;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.Data;
using GB.Controls;
using GB.Controls.Extensions;
using GB.MKRF.Repository;
using Entity = GB.MKRF.Entities.AccountingObjects;

namespace GB.MKRF.Web.AccountingObjects
{
    public partial class InformationInteraction : BasePageAccountingObject
    {
        #region Fields

        private InformationInteractionController _informationInteractionController;

        #endregion Fields


        #region Prop

        public InformationInteractionController InformationInteractionController
        {
            get
            {
                if (_informationInteractionController == null)
                {
                    _informationInteractionController = new InformationInteractionController();
                }

                return _informationInteractionController;
            }
        }


        #endregion Prop


        #region Handlers

        #region PageHandlers
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            gridInformationInteraction.DataBind();
        }

        #endregion PageHandlers

        #region Control Handlers

        #region Grid

        #region EditFom

        protected void txtName_OnDataBinding(object sender, EventArgs e)
        {
            InformationInteractionController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Name));
        }

        protected void txtPurpose_OnDataBinding(object sender, EventArgs e)
        {
            InformationInteractionController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Purpose));
        }

        protected void cbxType_OnDataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(RepositoryBase<InformationInteractionType>.GetAll())
                .SetValueComboBox(
                    InformationInteractionController.With(x => x.CurrentEdit)
                        .With(x => x.Type)
                        .Return(x => x.Id, EntityBase.Default));
        }

        protected void txtSMEV_OnDataBinding(object sender, EventArgs e)
        {
            InformationInteractionController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.SMEV));
        }

        protected void txtTitle_OnDataBinding(object sender, EventArgs e)
        {
            InformationInteractionController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Title));
        }

        protected void txtUrl_OnDataBinding(object sender, EventArgs e)
        {
            InformationInteractionController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Url));
        }

        #endregion EditForm


        protected void gridInformationInteraction_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            gridInformationInteraction.DataSource = InformationInteractionController.Repository.GetAllByAccountingObject(AccountingObjectId);
        }

        protected void gridInformationInteraction_OnRowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            InformationInteractionController.Create(GetInformationInteractionArgs());
            gridInformationInteraction.GridCancelEdit(e);
        }

        protected void gridInformationInteraction_OnRowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            InformationInteractionController.Update(GetInformationInteractionArgs((int)e.Keys[0]));
            gridInformationInteraction.GridCancelEdit(e);
        }

        protected void gridInformationInteraction_OnStartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            InformationInteractionController.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void gridInformationInteraction_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            InformationInteractionController.Delete(new InformationInteractionArgs()
            {
                Id = (int)e.Keys[0]
            });

            gridInformationInteraction.GridCancelEdit(e);
        }

        #endregion Grid

        #endregion Control Handlers

        #endregion Handlers


        #region Helpers

        public static string GetUrl(object accountingObjectId)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/AccountingObjects/InformationInteraction.aspx"), EditCard.AccountingObjectIdKey, accountingObjectId);
        }

        private InformationInteractionArgs GetInformationInteractionArgs(int id = 0)
        {
            var result = new InformationInteractionArgs()
            {
                Id = id,
                AccountingObject = CurrentAccountingObject,
                Name = hf["Name"].ToStringOrDefault(),
                Purpose = hf["Purpose"].ToStringOrDefault(),
                TypeId = hf["TypeId"].ToIntNull(),
                SMEV = hf["SMEV"].ToStringOrDefault(),
                Title = hf["Title"].ToStringOrDefault(),
                Url = hf["URL"].ToStringOrDefault(),
            };

            return result;
        }


        #endregion Helpers

    }
}