﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    internal class InformationInteractionMap : ClassMapHiLo<InformationInteraction>
    {
        public InformationInteractionMap()
            : base("Information_Exchange")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Information_Exchange_Name").Nullable();
            Map(x => x.Purpose, "Information_Exchange_Goal");
            Map(x => x.SMEV, "Information_Exchange_SMEV");
            Map(x => x.Title, "Information_Exchange_Heading");
            Map(x => x.Url, "Information_Exchange_URL");

            References(x => x.Type, "Information_Exchange_Type_ID");
            References(x => x.AccountingObject, "OU_ID").Nullable();
            References(x => x.PlansActivity, "PlansActivity_ID").Nullable();
        }
    }
}