﻿using GB.Data.Plans;

namespace GB.MKRF.Logic.Access.Handlers
{
    public class PlansActivityAccessHandler : AccessHandlerBase<PlansActivity>
    {
        public override PlansActivity Entity { get; set; }

        public PlansActivityAccessHandler(PlansActivity pa) : base(pa)
        {
        }
    }
}
