﻿namespace bgTeam.IsRui.Domain.Entities.Claims
{
    public class ClaimIndicator : BaseIndicator
    {
        /// <summary>
        /// Алгоритм расчета
        /// </summary>
        public virtual string AlgorithmOfCalculating { get; set; }

        /// <summary>
        /// Обоснование
        /// </summary>
        public virtual string Justification { get; set; }
    }
}
