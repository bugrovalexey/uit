﻿using bgTeam.IsRui.MKRF.Logic.Tabs.Handler;
using bgTeam.IsRui.MKRF.Providers;

namespace bgTeam.IsRui.MKRF.Logic.Tabs
{
    /// <summary>
    /// Регистрация менеджера, логика по отображению доступных вкладок для пользователя
    /// а также по переходу на доступную страницу
    /// </summary>
    public class TabsRegistration : ManagerRegistrationBase<TabsManager>
    {
        private readonly IAccessControllerProvider m_accessControllerProvider;

        public TabsRegistration(IAccessControllerProvider accessControllerProvider)
        {
            m_accessControllerProvider = accessControllerProvider;
        }

        protected override void RegisterHandlers(TabsManager manager)
        {
            manager.RegisterHandler<MainTabs>(new MainTabsHandler(m_accessControllerProvider));

            manager.RegisterHandler<ActivityTabs>(new ActivityHandler(m_accessControllerProvider));
            manager.RegisterHandler<ActivityCardTabs>(new ActivityCardHandler(m_accessControllerProvider));
            manager.RegisterHandler<AccountingObjectsTabs>(new AccountingObjectsHandler(m_accessControllerProvider));
            manager.RegisterHandler<AnalyticsTabs>(new AccountingObjectsHandler(m_accessControllerProvider));
            manager.RegisterHandler<AccountingObjectsCardTabs>(new AccountingObjectsCardHandler(m_accessControllerProvider));
        }
    }
}