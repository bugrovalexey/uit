﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.GovContracts
{
    public class DeleteGovContractContext : ICommandContext
    {
        public int Id { get; set; }


        public DeleteGovContractContext(int id)
        {
            this.Id = id;
        }
    }
}
