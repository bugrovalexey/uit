﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class GRBSToYearMap : ClassMapBase<GRBSToYear>
    {
        public GRBSToYearMap()
            : base("GRBS_Years", "v_", true)
        {
            //Table("v_");

            //Id(x => x.Id, "GRBS_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "GRBS_Code");
            Map(x => x.CodeName, "GRBS_CodeName");
            Map(x => x.Year, "Year");
        }
    }
}
