﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts.Web;
using GB.MKRF.Entities.Admin;
using DevExpress.XtraCharts;
using DevExpress.Web.ASPxGridView;

namespace GB.MKRF.Web.Monitoring.v2012new
{
    public partial class ActivityTypeStat : System.Web.UI.Page
    {
        private int currentYear = 0;
        protected int CurrentYear
        {
            get
            {
                if (currentYear == 0)
                {

                    var ys = Request.QueryString["Year"];
                    int yi = 0;
                    if (!string.IsNullOrWhiteSpace(ys) && int.TryParse(ys, out yi))
                    {
                        currentYear = yi;
                    }
                    else
                    {
                        currentYear = DateTime.Now.Year;
                    }
                }
                return currentYear;
            }
        }

        private int _dep = 0;
        protected int Department
        {
            get
            {
                if (_dep == 0)
                {

                    var ds = Request.QueryString["dep"];
                    int di = 0;
                    if (!string.IsNullOrWhiteSpace(ds) && int.TryParse(ds, out di))
                    {
                        _dep = di;
                    }
                    else
                    {
                        _dep = 0;
                    }
                }
                return _dep;
            }
        }

        private Series CategoriesSeries { get { return ChartCategories.GetSeriesByName("Categories"); } }

        private DataTable _data = null;
        private DataTable Data
        {
            get { return _data ?? (_data = MonitoringRepository.GetDataForActivityTypeStat(Department, CurrentYear)); }
        }

        protected override void OnPreRender(EventArgs e)
        {
            YearsComboBox.DataBind();
            DepartmentComboBox.DataBind();

            BindCategories();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            GridExport.Grid = Grid;
        }

        protected void ChartCategories_ObjectSelected(object sender, DevExpress.XtraCharts.HotTrackEventArgs e)
        {
            DepartmentComboBox.DataBind();
            BindCategories();

            Series series = e.Object as Series;
            if (series != null)
            {
                ExplodedSeriesPointCollection explodedPoints = ((PieSeriesViewBase)series.View).ExplodedPoints;
                SeriesPoint point = (SeriesPoint)e.AdditionalObject;
                explodedPoints.ToggleExplodedState(point);
            }
            e.Cancel = series == null;
        }

        private void BindCategories()
        {
            if (Data.Rows.Count == 0)
            {
                ChartCategories.Visible = false;
                OverdraftInfoLabel.Text = "Нет данных для отображения";
                return;
            }

            var source = Data.Clone();

            //if (Data.Rows.Count == 0)
            //{
            //    DataRow dr = Data.NewRow();

            //    dr["Name"] = "Не задано";
            //    dr["Y0"] = 0;

            //    Data.Rows.Add(dr);

            //    ExpenseInfoLabel.Text = "У данного департамента не существует плана на выбранный год";
            //}

            //if (Data.Rows.Count > 0)
            //{
            var overdraft = 0.0;
            var expenseSum = 0.0;
            var activityCnt = 0;

            foreach (DataRow row in Data.Rows)
            {
                var r = source.NewRow();
                if (row["Name"].ToString() == "Перерасход")
                {
                    overdraft = Convert.ToDouble(row["Y0"]);
                    continue;
                }
                if (row["Name"].ToString() != "Перерасход" && row["Name"].ToString() != "Остаток")
                {
                    expenseSum += Convert.ToDouble(row["Y0"]);
                }

                r["Name"] = row["Name"];
                r["Y0"] = row["Y0"];
                r["cnt"] = row["cnt"];

                activityCnt += (int)r["cnt"];

                source.Rows.Add(r);
            }

            if (activityCnt == 0)
            {
                ChartCategories.Visible = false;
                OverdraftInfoLabel.Text = "Нет данных для отображения";
                return;
            }

            CategoriesSeries.DataSource = source;
            CategoriesSeries.ArgumentDataMember = "Name";
            CategoriesSeries.ValueDataMembers[0] = "Y0";

            OverdraftInfoLabel.Text = overdraft > 0 ? string.Format("Перерасход на {0:#,##0.0000} тыс. руб.", overdraft) : string.Empty;
            ExpenseInfoLabel.Text = string.Format("Сумма расходов по плану информатизации - {0:#,##0.0000} тыс. руб.", expenseSum != -1 ? expenseSum : 0);
            //}
            //else
            //{
            //    //ChartCategories.Visible = false;
            //    ExpenseInfoLabel.Text = "У данного департамента не существует плана на выбранный год";
            //}

            var limit = RepositoryBase<LimitMoney>.GetAll().SingleOrDefault(x => x.Department.Id == Department && x.Year.Value == CurrentYear);
            LimitInfoLabel.Text = string.Format("Лимит на текущий год - {0:#,##0.0000} тыс. руб.", limit != null ? limit.Money : 0.0);
        }

        protected void YearsComboBox_DataBinding(object sender, EventArgs e)
        {
            IList<Years> years = RepositoryBase<Years>.GetAll(x => x.Value >= 2012).Take(10).ToList();
            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = years;
            box.Value = CurrentYear;
        }

        protected void DepartmentComboBox_DataBinding(object sender, EventArgs e)
        {
            IList<Department> deps =
                RepositoryBase<Department>.GetAll()
                    .Where(x => x.Id != (int)DepartmentEnum.None && x.Id != (int)DepartmentEnum.MKRF)
                    .OrderBy(x => x.Name)
                    .ToList();

            if (Department == 0)
            {
                _dep = deps.First().Id;
            }

            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = deps;
            box.Value = Department;
        }

        protected void Grid_DataBinding(object sender, EventArgs e)
        {
            var grid = sender as ASPxGridView;

            var type = hf["Type"] as string;

            grid.DataSource = MonitoringRepository.GetDataForActivityTypeStatDetail(DepartmentComboBox.Value, CurrentYear, type);
        }
    }
}