﻿using GB.Data.AccountingObjects;
using GB.Data.Plans;
using System;

namespace GB.Data.Extensions
{
    public static partial class EntityExtension
    {
        public static bool Is<T>(this T e, T obj) where T : IEntity
        {
            return e.Id == obj.Id;
        }

        public static bool IsNullOrDefault<T>(this T e) where T : IEntity
        {
            return (e == null || e.Id == EntityBase.Default);
        }

        public static EntityType GetTableType(this IEntity e)
        {
            return GetTableTypeObj(e);
        }

        internal static EntityType GetTableTypeObj(object obj)
        {
            if (obj == null)
                throw new ArgumentNullException();

            if (obj is PlansActivity)
                return EntityType.PlansActivity;

            if (obj is ActivityExpertiseConclusion)
                return EntityType.ActivityExpertise;

            if (obj is AccountingObject)
                return EntityType.AccountingObject;

            throw new NotSupportedException("Не удалось определить тип " + obj.ToString());
        }

        public static Type GetObjType(EntityType type)
        {
            switch (type)
            {
                case EntityType.PlansActivity:
                    return typeof(PlansActivity);
                case EntityType.AccountingObject:
                    return typeof(AccountingObject);
                case EntityType.ActivityExpertise:
                    return typeof(ActivityExpertiseConclusion);

                case EntityType.DocumentAccountingObject:
                case EntityType.OtherReason:
                case EntityType.SpecialCheck:
                case EntityType.AODocumentGroundsForCreation:
                case EntityType.AODocumentCommissioning:
                case EntityType.AODocumentDecommissioning:
                case EntityType.ActOfAcceptance:
                    break;
            }

            throw new NotSupportedException("Не удалось определить тип " + type.ToString());
        }

        //public static IEntity GetObj(EntityType type, int id)
        //{
        //    switch (type)
        //    {
        //        case EntityType.PlansActivity:
        //            return new PlansActivity(id);
        //        case EntityType.AccountingObject:
        //            return new AccountingObject(id);
        //        case EntityType.ActivityExpertise:
        //            return new ActivityExpertiseConclusion(id);

        //        case EntityType.DocumentAccountingObject:
        //        case EntityType.OtherReason:
        //        case EntityType.SpecialCheck:
        //        case EntityType.AODocumentGroundsForCreation:
        //        case EntityType.AODocumentCommissioning:
        //        case EntityType.AODocumentDecommissioning:
        //        case EntityType.ActOfAcceptance:
        //            break;
        //    }

        //    throw new NotSupportedException("Не удалось определить тип " + type.ToString());
        //}

        public static bool CanDo(this AccessActionEnum value, AccessActionEnum access)
        {
            return (value & access) == access;
        }
    }
}