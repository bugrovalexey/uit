﻿using System;

namespace GB.Data.Directories
{
    /// <summary>
    /// Вид затрат
    /// </summary>
    public class ExpenseDirection : EntityBase, IEntityCodeName, IPeriodDirectories
    {
        /// <summary>Код</summary>
        public virtual string Code { get; set; }//null

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null

        /// <summary>Код 3</summary>
        public virtual string Code3 { get; set; }//null

        /// <summary>Группа</summary>
        public virtual int? Group { get; set; }//null

        /// <summary>Группа</summary>
        public virtual string GroupExport { get; set; }//null

        /// <summary> Дата начала действия справочника </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary> Дата окончания действия справочника </summary>
        public virtual DateTime? DateEnd { get; set; }

        /// <summary>Принадлежность (тип затрат, Работы или Товары)</summary>
        public virtual ExpenseDirectionTypeEnum? Type { get; set; }

        public virtual string CodeName { get { return EntityHelper.GetCodeName(this); } }
    }
}