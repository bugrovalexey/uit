﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using GB.Helpers;
using GB.MKRF.Entities.Directories;
using GB.Providers;
using GB.Repository;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Plans
{
	public partial class ObjectsList : System.Web.UI.Page
	{
        const int canDeleteMask = 2;
        protected const string departmentIdKey = "department";
        DataTable table = null;
        Department currentDepartmentInternal = null;
        SortedDictionary<int, int> pivotData = new SortedDictionary<int, int>();

        protected readonly string OGV;

        public ObjectsList()
        {
            OGV = "ОГВ";
        }

        public static string GetUrl(int id)
        {
            string url = "~/Plans/ObjectsList.aspx";
            if (GB.MKRF.Validation.Validator.UserAccessToPage(url))
            {
                return UrlHelper.Resolve(url) + "?" + departmentIdKey + "=" + id;
            }
            else
            {
                return null;
            }
        }

        protected Department CurrentDepartment
        {
            get
            {
                if (currentDepartmentInternal == null)
                {
                    int id = 0;

                    if (int.TryParse(Request.QueryString[departmentIdKey], out id))
                    {
                        currentDepartmentInternal = RepositoryBase<Department>.Get(id);
                    }

                    if (currentDepartmentInternal == null)
                    {
                        ResponseHelper.DenyAccess(this.Response);
                    }
                }
                return currentDepartmentInternal;
            }
        }

		protected string Curator
		{
			get { return RepositoryBase<MksResponsible>.GetAll(r => r.Department.Id == CurrentDepartment.Id).Count > 0 ?RepositoryBase<MksResponsible>.GetAll(r => r.Department.Id == CurrentDepartment.Id)[0].User.FullName : ""; }
		}

        //bool? canManageExecutorsInternal = null;
        //protected bool CanManageExecutors
        //{
        //    get
        //    {
        //        //if (!canManageExecutorsInternal.HasValue)
        //        //    canManageExecutorsInternal = Validator.Validator.Validate(new Executor(CurrentDepartment, null), ObjActionEnum.create);

        //        //return canManageExecutorsInternal.Value;

        //        return true;
        //    }
        //}

        protected override void OnPreRender(EventArgs e)
        {
            //executorsGrid.Columns["DepartnemtName"].Caption = OGV;

            //executorsGrid.DataBind();
            //ObjectsGrid.DataBind();

            //if (CanManageExecutors)
            //{
            //    phExecutors.Visible = true;
            //    DepartmentComboBox.DataBind();
            //    // TODO: вынести в валидатор
            //    DepartmentComboBox.Enabled = (DepartmentComboBox.Items.Count > 0);
            //    AddDepartmentButton.Enabled = (DepartmentComboBox.Items.Count > 0) && RoleEnum.Department.IsEq(UserRepository.GetCurrent().Role);
            //}
            //else
            //{
            //    phExecutors.Visible = false;
            //}
          
            base.OnPreRender(e);
        }

        protected string getPivotData()
        {
            if (table == null)
                return null;

            pivotData.Clear();
            foreach (DataRow row in table.Rows)
            {
                int type = (int)row["ObjType"];
                if (!pivotData.ContainsKey(type))
                    pivotData.Add(type, 0);
                pivotData[type] = pivotData[type] + 1;
            }

            StringBuilder builder = new StringBuilder();
            foreach(int objectType in pivotData.Keys)
            {
                builder.Append("<tr valign='top'><td style='white-space: nowrap; width: 35%; padding-left: 20px' align='right'>");
                string typeName = "";
                switch (objectType) //TODO: избавиться от 1 и вынести строки в ресурсы
                {
                    case (int)ExpObjectTypeEnum.Plan: typeName = "Планов информатизации"; break;
					case (int)ExpObjectTypeEnum.Plan2012: typeName = "Уточненных планов"; break;
					case (int)ExpObjectTypeEnum.PreliminaryPlan: typeName = "Сведений о планируемых бюджетных ассигнованиях"; break;
                    //case (int)ExpObjectTypeEnum.Decision: typeName = "Проектов решений о создании ФГИС:"; break;
                    //case (int)ExpObjectTypeEnum.Federal: typeName = "Федеральных целевых программ:"; break;
                    //case (int)ExpObjectTypeEnum.Other: typeName = "Иных документов, содержащих мероприятия по ИКТ:"; break;
                    //case (int)ExpObjectTypeEnum.Program: typeName = "Ведомственных программ информатизации:"; break;
                    //case (int)ExpObjectTypeEnum.Strategy: typeName = "Проектов стратегий, концепций, иных долгосрочных документов:"; break;
                    //case (int)ExpObjectTypeEnum.Targeted: typeName = "Ведомственных целевых программ:"; break;
					case (int)ExpObjectTypeEnum.Plan2012new: typeName = "Планов информатизации (версия 2012г.)"; break;
                    //case (int)ExpObjectTypeEnum.Plan2012rel: typeName = "Планов информатизации (версия 2012г.)"; break;
                }
                builder.Append(typeName);
                builder.Append("</td><td style='width: 65%; font-weight: bold'>");
                builder.Append(pivotData[objectType]);
                builder.Append("</td></tr>");
            }

            return builder.ToString();
        }

        protected string GetTitle(Department department)
        {
            return string.Concat("<span>", department.Name, "</span>");
        }

        //protected void ObjectsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        //{
        //    table = DemandRepository<ExpDemand>.GetAllByDepartment(CurrentDepartment);
        //    // TODO: проверять в начале. Доступен ли ОГВ текущему польз.
        //    if (table == null || table.Rows.Count <= 0)
        //    {
        //        ResponseHelper.DenyAccess(this.Response);
        //    }
        //    else
        //    {
        //        ObjectsGrid.DataSource = table;
        //    }
        //}

        protected string getActionLink(int ObjectId, int ObjectType)
        {
            string url = "";
            if (ObjectType == (int)ExpObjectTypeEnum.Plan)
            {
                url = PlanCard.GetUrl(ObjectId, ExpObjectTypeEnum.Plan);
            }
			else if (ObjectType == (int)ExpObjectTypeEnum.PreliminaryPlan)
			{
				url = PlanCard.GetUrl(ObjectId, ExpObjectTypeEnum.PreliminaryPlan);
			}
			else if (ObjectType == (int)ExpObjectTypeEnum.Plan2012)
			{
				url = PlanCard.GetUrl(ObjectId, ExpObjectTypeEnum.Plan2012);
			}
			else if (ObjectType == (int)ExpObjectTypeEnum.Plan2012new)
			{
				url = PlanCard.GetUrl(ObjectId, ExpObjectTypeEnum.Plan2012new);
			}
			else{
                throw new Exception("Тип не определен");
			}
            return "<a href='" + url + "'>Карточка</a>";
        }

        protected string getType(int ObjectType)
        {
            switch (ObjectType)
            {
				case (int)ExpObjectTypeEnum.Plan: return "Планы информатизации";
                case (int)ExpObjectTypeEnum.Plan2012: return "Уточненные планы";
				case (int)ExpObjectTypeEnum.PreliminaryPlan: return "Сведения о планируемых бюджетных ассигнованиях";
                //case (int)ExpObjectTypeEnum.Decision: return "Проекты решения о создании ФГИС";
                //case (int)ExpObjectTypeEnum.Federal: return "Федеральная целевая программа";
                //case (int)ExpObjectTypeEnum.Other: return"Иной документ, содержащий мероприятия по ИКТ";
                //case (int)ExpObjectTypeEnum.Program: return "Ведомственная программа информатизации";
                //case (int)ExpObjectTypeEnum.Strategy: return "Проект стратегий, концепций, иных долгосрочных документов";
                //case (int)ExpObjectTypeEnum.Targeted: return "Ведомственная целевая программа";
				case (int)ExpObjectTypeEnum.Plan2012new: return "Планы информатизации (версия 2012г.)";
                //case (int)ExpObjectTypeEnum.Plan2012rel: return "Планы информатизации (версия 2012г.)";
                default: return "";
            }
        }

        //protected void executorsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        //{


        //    executorsGrid.DataSource = EntityHelper.ConvertToAnonymous<User>(DepartmentRepository.GetExecutGetAllByDepartmentors(CurrentDepartment), delegate(User u)
        //                                {
        //                                    return new
        //                                    {
        //                                        Id = u.Id,
        //                                        DepartmentId = u.Department.Id,
        //                                        DepartnemtName = u.Department == CurrentDepartment ? (" " + u.Department.Name) : u.Department.Name,
        //                                        UserName = u.FullName,
        //                                        Job = u.Job,
        //                                        Phone = u.MobPhone + " " + u.WorkPhone,
        //                                    };
        //                                });

        //}
        
        //protected void executorsGrid_DataBound(object sender, EventArgs e)
        //{
        //    executorsGrid.GroupBy(executorsGrid.Columns["DepartnemtName"]);
        //    executorsGrid.ExpandAll();
        //}
        
        //protected void DepartmentComboBox_DataBinding(object sender, EventArgs e)
        //{
        //    IList<Department> depList = DepartmentRepository.GetAllChildren(UserRepository.GetCurrent().Department, DepartmentTypeEnum.Department);

        //    Dictionary<int, Department> depDictionary = new Dictionary<int,Department>();
            
        //    foreach(Executor executor in CurrentDepartment.Executors)
        //    {
        //        if (!depDictionary.ContainsKey(executor.Child.Id))
        //            depDictionary.Add(executor.Child.Id, executor.Child);
        //    }

        //    // исключим департаменты, которые уже есть в списке приглашенных
        //    for (int i = depList.Count - 1; i >= 0; i--)
        //    {
        //        if (depDictionary.ContainsKey(depList[i].Id))
        //            depList.RemoveAt(i);
        //    }

        //    DepartmentComboBox.DataSource = depList;
        //}

        //protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        //{
        //    if (string.IsNullOrEmpty(e.Parameter))
        //        return;

        //    if (e.Parameter.StartsWith("add"))
        //    {
        //        int id = 0;
        //        if (int.TryParse(e.Parameter.Substring(3), out id))
        //        {
        //            Department department = RepositoryBase<Department>.Get(id); 
        //            Executor executor = new Executor(CurrentDepartment, department);

        //            if (Validator.Validator.Validate(executor, ObjActionEnum.create))
        //                RepositoryBase<Executor>.SaveOrUpdate(executor);                        
        //        }
        //    }
        //    else if (e.Parameter.StartsWith("delete"))
        //    {
        //        int id = 0;
        //        if (int.TryParse(e.Parameter.Substring(6), out id))
        //        {
        //            Executor executor = RepositoryBase<Executor>.Get(x => x.Parent == CurrentDepartment && x.Child.Id == id);

        //            if (Validator.Validator.Validate(executor, ObjActionEnum.delete))
        //                RepositoryBase<Executor>.Delete(executor.Id);
        //        }
        //    }
        //    DevExpress.Web.ASPxGridView.ASPxGridView.RedirectOnCallback(Request.RawUrl);
        //}

        //protected string getDeleteLinkButton(int DepartmentId)
        //{
        //    if (DepartmentId == CurrentDepartment.Id || !CanManageExecutors)
        //        return null;
        //    else
        //        return "<a href='javascript:void(0)' onclick='deleteExecutor(" + DepartmentId + ")'>Удалить</a>";
        //}



        //#region DetailGrid
        //int ParentRowId = -1;
        //TableTypeEnum PrtRow = TableTypeEnum.Exp_Demand;

        //protected void DetailGrid_Init(object sender, EventArgs e)
        //{
        //    ASPxGridView grid = sender as ASPxGridView;

        //    grid.ClientInstanceName = string.Concat("DetailGrid_", grid.GetMasterRowKeyValue());
        //}

        //protected void AddDetailButton_Init(object sender, EventArgs e)
        //{
        //    ASPxButton button = sender as ASPxButton;
        //    GridViewDetailRowTemplateContainer c = button.NamingContainer as GridViewDetailRowTemplateContainer;

        //    button.ClientSideEvents.Click = string.Concat("function(s,e){ DetailGrid_", c.KeyValue, ".AddNewRow(); }");
        //}

        //protected void DetailGrid_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        //{
        //    ASPxGridView grid = sender as ASPxGridView;
        //    GridViewDetailRowTemplateContainer c = grid.Parent as GridViewDetailRowTemplateContainer;

        //    PrtRow = (TableTypeEnum)ObjectsGrid.GetRowValues(c.VisibleIndex, "PrtId");
        //    ParentRowId = (int)c.KeyValue;
        //}

        //protected void DetailGrid_BeforePerformDataSelect(object sender, EventArgs e)
        //{
        //    ASPxGridView grid = sender as ASPxGridView;

        //    GridViewDetailRowTemplateContainer c = grid.Parent as GridViewDetailRowTemplateContainer;

        //    DevExpress.Web.Data.WebCachedDataRow row = c.DataItem as DevExpress.Web.Data.WebCachedDataRow;

        //    int prtId = (int)ObjectsGrid.GetRowValues(c.VisibleIndex, "PrtId");

        //    grid.DataSource = PlanRepository.GetLinks((int)c.KeyValue, prtId);
        //}

        //protected void DetailGrid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        //{
        //    ASPxGridView grid = sender as ASPxGridView;
        //    GridViewDetailRowTemplateContainer c = grid.Parent as GridViewDetailRowTemplateContainer;

        //    TableTypeEnum type = (TableTypeEnum)ObjectsGrid.GetRowValues(c.VisibleIndex, "PrtId");

        //    EntityBase parent = type.GetObject((int)c.KeyValue);

        //    ObjectLinks row = new ObjectLinks();

        //    saveDetailRow(row, parent);

        //    GridCancelEdit(grid, e);
        //}

        //protected void DetailGrid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        //{
        //    RepositoryBase<ObjectLinks>.Delete((int)e.Keys[0]);

        //    GridCancelEdit(sender as ASPxGridView, e);
        //}

        //void saveDetailRow(ObjectLinks row, EntityBase parent)
        //{
        //    string str = hf["Object"] as string;

        //    string[] mass = str.Split(';');

        //    TableTypeEnum type = (TableTypeEnum)Convert.ToInt32(mass[1]);

        //    EntityBase child = type.GetObject(Convert.ToInt32(mass[0]));

        //    row.Parent = parent;
        //    row.Child = child;


        //    RepositoryBase<ObjectLinks>.SaveOrUpdate(row);
        //}

        //protected void ObjComboBox_DataBinding(object sender, EventArgs e)
        //{
        //    if (ParentRowId > 0)
        //    {
        //        ASPxComboBox box = sender as ASPxComboBox;

        //        string ignore = ParentRowId.ToString();

        //        DataTable dt = PlanRepository.GetLinks(ParentRowId, (int)PrtRow);

        //        foreach (DataRow item in dt.Rows)
        //        {
        //            ignore = string.Concat(ignore, ",", item["ObjId"].ToString());
        //        }

        //        (sender as ASPxComboBox).DataSource = DemandRepository<ExpDemand>.GetAllByDepartment(CurrentDepartment, ignore);
        //    }
        //}
        //#endregion

        //private static void GridCancelEdit(ASPxGridView grid, System.ComponentModel.CancelEventArgs e)
        //{
        //    grid.CancelEdit();
        //    e.Cancel = true;

        //    grid.DataBind();
        //}
	}
}
