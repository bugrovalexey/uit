﻿namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Должностное лицо
    /// </summary>
    public class Official : EntityBase
    {
        protected Official()
        {
        }
        
        public Official(Plan plan)
        {
            this.Plan= plan;
        }

        /// <summary> фамилия руководителя </summary>
        public virtual string LastName { get; set; }

        /// <summary> имя руководителя </summary>
        public virtual string FirstName { get; set; }

        /// <summary> отчество руководителя </summary>
        public virtual string SurName { get; set; }

        /// <summary>Должность</summary>
        public virtual string Post { get; set; }

        /// <summary>Квалификация</summary>
        public virtual string Qualification { get; set; }

        /// <summary>Проектный опыт</summary>
        public virtual string Experience { get; set; }
        
        /// <summary> План </summary>
        public virtual Plan Plan { get; set; }

        /// <summary> Роль </summary>
        public virtual ManagementRole ManagementRole { get; set; }
    }
}
