﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201806212228)]
    public class Migration_201806212228 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Delete.Column("ClaimDocument_CreateDate").FromTable("ClaimDocument");

            Alter.Table("ClaimDocument")
                .AddColumn("ClaimDocument_Name").AsString(255).Nullable()
                .AddColumn("ClaimDocument_DateAdoption").AsDateTime().Nullable()
                .AddColumn("ClaimDocument_Number").AsString(63).Nullable()
                .AddColumn("ClaimDocument_ParticleOfDocument").AsString(1023).Nullable();
        }
    }
}
