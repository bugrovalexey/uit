﻿namespace bgTeam.IsRui.Common.Services.Impl
{
    using System;
    using System.Collections;
    using System.Collections.Concurrent;

    public class CacheManager : ICacheManager
    {
        private readonly ConcurrentDictionary<int, IDictionary> _container;
        private readonly TimeSpan _expireTime;

        public CacheManager()
        {
            _container = new ConcurrentDictionary<int, IDictionary>();
            _expireTime = new TimeSpan(1, 0, 0);
        }

        public CacheManager(TimeSpan expireTime)
        {
            _container = new ConcurrentDictionary<int, IDictionary>();
            _expireTime = expireTime;
        }

        public bool TryGetValue<TKey, TValue>(TKey key, out TValue value)
        {
            var cache = (ConcurrentDictionary<TKey, CacheValue<TValue>>)GetCache<TKey, CacheValue<TValue>>();
            var res = cache.TryGetValue(key, out CacheValue<TValue> innerValue);

            if (!res || innerValue.IsExpire)
            {
                value = default;
                return false;
            }

            value = innerValue.Value;
            return res;
        }

        public bool TrySetValue<TKey, TValue>(TKey key, TValue value)
        {
            return TrySetValue(key, value, _expireTime);
        }

        public bool TrySetValue<TKey, TValue>(TKey key, TValue value, TimeSpan timeToExpire)
        {
            var cache = (ConcurrentDictionary<TKey, CacheValue<TValue>>)GetCache<TKey, CacheValue<TValue>>();
            var newVal = new CacheValue<TValue>(value, timeToExpire);
            cache.AddOrUpdate(key, newVal, (k, v) => newVal);
            return true;
        }

        private IDictionary GetCache<TKey, TValue>()
        {
            var keyType = StaticType<TKey>.Type;
            var valueType = StaticType<TValue>.Type;
            int hash = 17;

            unchecked
            {
                hash = (hash * 23) + keyType.FullName.GetHashCode();
                hash = (hash * 23) + valueType.FullName.GetHashCode();
            }

            if (_container.TryGetValue(hash, out IDictionary dict))
            {
                return dict;
            }

            var newCache = new ConcurrentDictionary<TKey, TValue>();

            return _container.GetOrAdd(hash, newCache);
        }
    }
}
