﻿using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Mapping.Plans
{
    class OfferByPlanMap : ClassMapBase<OfferByPlan>
    {
        public OfferByPlanMap() :
            base("Offer_By_Plan")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Activities, "Offer_By_Plan_Activity");
            Map(x => x.Annotation, "Offer_By_Plan_Annotation");
            Map(x => x.Name, "Offer_By_Plan_Name");
            Map(x => x.Purpose, "Offer_By_Plan_Purpose");
            Map(x => x.Indicator, "Offer_By_Plan_Indicator");
            Map(x => x.Index, "Offer_By_Plan_Pokazatel");
            Map(x => x.Task, "Offer_By_Plan_Task");

            References<Plan>(x => x.Plan, "Plans_ID");
        }
    }
}