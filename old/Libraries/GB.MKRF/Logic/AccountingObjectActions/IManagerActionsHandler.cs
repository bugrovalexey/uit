﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Logic.AccountingObjectActions
{
    public interface IManagerActionsHandler
    {
        IEnumerable<ActionDescription> GetActionsList();
    }
}
