﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GB.Coord
{
    public class EntityNotFoundException : Exception
    {
        private int id;

        public EntityNotFoundException(int id)
        {
            this.id = id;
        }
    }

    

    public class ObjectAccessException : Exception
    {
    }
}