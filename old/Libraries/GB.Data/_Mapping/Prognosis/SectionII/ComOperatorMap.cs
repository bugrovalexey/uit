﻿using Uviri.MKRF.Entities.Prognosis.SectionII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionII
{
    public class ComOperatorMap : ClassMapBase<ComOperator>
	{
        public ComOperatorMap()
            : base("MF2_11")
        {
//            Table("w_");

//            Id(x => x.Id, "MF2_11_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF2_11_Insert
//                          @MF2_11_CA = ?
//                         ,@MF2_11_TP = ?
//                         ,@MF2_ID = ?
//                         ,@MF2_11_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF2_11_Update
//                          @MF2_11_CA = ?
//                         ,@MF2_11_TP = ?
//                         ,@MF2_ID = ?
//                         ,@MF2_11_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF2_11_Delete
//						  @MF2_11_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.NameCA, "MF2_11_CA");
            Map(x => x.NameTP, "MF2_11_TP");

            References<TechInfo>(x => x.TechInfo, "MF2_ID").Not.Nullable();
        }
    }
}