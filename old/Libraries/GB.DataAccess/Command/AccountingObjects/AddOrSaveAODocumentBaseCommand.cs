﻿using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.Documents;
using GB.DataAccess.Command.Documents;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class AddOrSaveAODocumentBaseCommand : ICommand<AddOrSaveAODocumentBaseContext>
    {
        ISessionFactoryGb factory;

        public AddOrSaveAODocumentBaseCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(AddOrSaveAODocumentBaseContext context)
        {
            var entity = new Repository<AccountingObject>().GetExisting(context.AccountingObjectId);
            entity.CommissioningDate = context.Date;            

            using (var session = factory.Create())
            {
                session.Update(entity);
                session.Commit();
            }

            context.xId = entity.DocumentCommissioning == null ? 0 : entity.DocumentCommissioning.Id;
            var commis = new BaseAddOrSaveCommand<AddOrSaveAODocumentBaseContext, AODocumentCommissioning>().Execute(context);

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = context.Files,
                listDocs = commis.Documents,
                owner = commis,
                type = commis.Type
            });
        }
    }
}
