﻿using System;
using GB.Helpers;
using GB.MKRF.Entities.Documents;

namespace GB.MKRF.Web.Plans.v2012new
{
    /// <summary>
    /// Документы заявки
    /// </summary>
    public partial class Documents : System.Web.UI.Page
    {
        public static string GetUrl(int PlanId)
        {
            string url = UrlHelper.Resolve("~/Plans/v2012new/Documents.aspx");
            return (PlanId > 0) ? url + "?" + PlanCard.plansIdKey + "=" + PlanId : url;
        }

        protected override void OnInit(EventArgs e)
        {
            dc.Demand = (Master as PlanCard).CurrentPlan.Demand;
            dc.DisplayedDocTypes = new DocTypeEnum[] { 
                DocTypeEnum.demand_document, 
                DocTypeEnum.demand_inventory,
                DocTypeEnum.ExpertConclusion,
                DocTypeEnum.PackageExpert,
                DocTypeEnum.PackageOGV,
                DocTypeEnum.PackageMKS
            };
            
            base.OnInit(e);
        }
    }
}