﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoCommonInfoGetStory : IStory<AoCommonInfoGetStoryContext, AoCommonInfoDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public AoCommonInfoGetStory(IRepositoryRui repository, IMapperBase mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public AoCommonInfoDto Execute(AoCommonInfoGetStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<AoCommonInfoDto> ExecuteAsync(AoCommonInfoGetStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            var ao = _repository.Get<AccountingObject>(q => q.Where(x => x.Id == context.AoId));

            return _mapper.Map(ao, new AoCommonInfoDto());
        }
    }
}
