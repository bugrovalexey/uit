﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Внешние ИС и компоненты ИТКИ
    /// </summary>
    public class ExternalIsAndComponentsItkiDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// СМЭВ
        /// </summary>
        public string Smev { get; set; }

        /// <summary>
        /// URL адрес
        /// </summary>
        public string Url { get; set; }
    }
}
