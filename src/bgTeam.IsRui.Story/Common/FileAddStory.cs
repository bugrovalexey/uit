﻿namespace bgTeam.IsRui.Story.Common
{
    using System;
    using System.Threading.Tasks;
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Documents;

    public class FileAddStory : IStory<FileAddStoryContext, Files>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IFileProviderRui _fileProvider;
        private readonly ISessionNHibernateFactory _factory;

        public FileAddStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IFileProviderRui fileProvider,
            ISessionNHibernateFactory factory)
        {
            _mapper = mapper;
            _repository = repository;
            _fileProvider = fileProvider;
            _factory = factory;
        }

        public Files Execute(FileAddStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<Files> ExecuteAsync(FileAddStoryContext context)
        {
            context.Entity.CheckNull(nameof(context.Entity));
            context.Name.CheckNull(nameof(context.Name));

            if (context.Kind == DocKindEnum.File && context.Url == null)
            {
                context.TempGuid.CheckNull(nameof(context.TempGuid));
            }

            var file = _mapper.Map(context, new Files());

            using (var session = _factory.OpenSession())
            {
                await session.InsertAsync(file);

                if (context.Kind == DocKindEnum.File)
                {
                    _fileProvider.MoveToGeneric(context.TempGuid.Value, file.Id, true);
                }
            }

            return file;
        }
    }
}
