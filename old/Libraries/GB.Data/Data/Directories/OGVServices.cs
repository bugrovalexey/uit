﻿using System;

namespace GB.Entity.Directories
{
#warning отказаться от класса PublicService в Registry

    /// <summary>Госуслуга</summary>
    [Serializable]
    public class OGVServices : EntityBase
    {
        /// <summary>Код</summary>
        public virtual string Code { get; set; }

        /// <summary>Название</summary>
        public virtual string Name { get; set; }

        /// <summary> Идентификатор ОГВ </summary>
        public virtual int OGVID { get; set; }

        public virtual string CodeName
        {
            get
            {
                return string.Format("{0} - {1}", Code, Name);
            }
        }
    }
}