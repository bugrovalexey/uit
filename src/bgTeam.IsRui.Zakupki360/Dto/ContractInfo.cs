﻿using System;

namespace bgTeam.IsRui.Zakupki360.Dto
{
    public class ContractInfo
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? FinishDate { get; set; }
        public string OrderNumber { get; set; }
        public string Number { get; set; }
        public ContractStatusEnum Status { get; set; }
        public decimal PricePlan { get; set; }
        public decimal PriceFact { get; set; }
        public string Customer { get; set; }
        public string Supplier { get; set; }
        public string Url { get; set; }
    }
}
