﻿namespace GB._Filter
{
    public class UserFilter : FluentNHibernate.Mapping.FilterDefinition
    {
        private const string keyFilterName = "UserFilter";

        private const string keyUserId = "UserId";

        public UserFilter()
        {
            WithName(keyFilterName)
                .AddParameter(keyUserId, NHibernate.NHibernateUtil.Int32);
        }

        //internal static void EnableFilter(ISession session, int userId)
        //{
        //    session.EnableFilter(keyFilterName).SetParameter(keyUserId, userId);
        //}

        //internal static void DisableFilter(ISession session)
        //{
        //    session.DisableFilter(keyFilterName);
        //}
    }

    //маппинг
    //ApplyFilter<UserFilter>("Exp_Demand_ID in (select * from Get_Object_List(:UserId, " + (int)TableTypeEnum.Exp_Demand + ", 'r'))");

    //использование
    //UserFilter.EnableFilter(Session, UserRepository.GetCurrent().Id);
    //RepositoryBase<Department>.GetAll().OrderBy(x=>x.Id).ToList();
    //UserFilter.DisableFilter(Session);
}