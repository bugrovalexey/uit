﻿using GB.Data.Plans;

namespace GB._Mapping.Plans
{
    internal class PlansSpecCharacteristicMap : ClassMapHiLo<PlansSpecCharacteristic>
    {
        public PlansSpecCharacteristicMap()
            : base("Plans_Spec_Charact")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Spec_Charact_Name");
            Map(x => x.Value, "Plans_Spec_Charact_Value");

            References(x => x.Unit, "MUnit_ID").Not.Nullable();
            References(x => x.Goods, "Plans_Spec_ID");
        }
    }
}