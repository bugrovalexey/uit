﻿using bgTeam.IsRui.MKRF.Providers;

namespace bgTeam.IsRui.MKRF.Logic.Tabs.Handler
{
    internal class ActivityHandler : TabsBaseHandler
    {
        public ActivityHandler(IAccessControllerProvider accessControllerProvider)
            : base("Activities", accessControllerProvider)
        {
            Add("Мои мероприятия", "List");
            Add("На согласовании", "ListApprove");
            Add("На экспертизе", "ListExpertise");
        }
    }
}