﻿using GB.Data.Common;
using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class StateProgramMap : ClassMapHiLo<StateProgram>
    {
        public StateProgramMap() :
            base("State_Programme")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "State_Programme_Name").Nullable();
            //Map(x => x.Level, "State_Programme_Level").Not.Nullable();

            References<StateProgram>(x => x.Parent, "State_Programme_Parent_ID").Nullable();
            References<Department>(x => x.Owner, "Govt_Organ_ID").Nullable();

            HasMany(x => x.Marks).KeyColumn("State_Programme_ID").Inverse();

            HasMany<StateProgram>(x => x.Children).KeyColumns.Add("State_Programme_Parent_ID").Inverse();
        }
    }
}