﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ExpenseItemToYearMap : ClassMapBase<ExpenseItemToYear>
    {
        public ExpenseItemToYearMap()
            : base("Expense_Item_Years", "v_", true)
        {
            //Table("v_");

            //Id(x => x.Id, "Expense_Item_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "Expense_Item_Code");
            Map(x => x.CodeName, "Expense_Item_CodeName");
            Map(x => x.Year, "Year");
        }
    }
}
