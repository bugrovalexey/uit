﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using CryptoPro.Sharpei;
using System.Security.Cryptography;

namespace GB.SmevBehaviorExtension
{
    public enum SignMode
    {
        Request,
        Replay
    }

    public class Signer
    {
        SignMode Mode;

        XmlDocument doc = null;
        XmlElement bodyNode = null;
        XmlElement headerNode = null;
        XmlElement securityNode = null;
        
        XmlElement signatureNode = null;
        XmlElement signedInfoNode = null;

        X509Certificate2 certificate;

        private Signer(XmlDocument Doc, string certificateName, SignMode mode)
        {
            certificate = GetCertificateBySubject(certificateName);

            this.doc = Doc;
            this.Mode = mode;

            AddBodyId();
            AddHeader();
        }

        public static X509Certificate2 GetCertificateBySubject(string certificateName)
        {
            if (string.IsNullOrEmpty(certificateName))
                throw new ArgumentNullException("certificateName");

            X509Certificate2 cert = null;
            X509Store store = new X509Store(StoreName.Root, StoreLocation.LocalMachine);

            try
            {
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

                X509Certificate2Collection certCollection = store.Certificates.Find(X509FindType.FindByThumbprint, certificateName, false);
                cert = (certCollection != null && certCollection.Count > 0) ? certCollection[0] : null;

                if (cert == null)
                {
                    throw new CryptographicException("The certificate could not be found.");
                }
            }
            finally
            {
                store.Close();
            }


            return cert;
        }


        public static XmlDocument SignXml(string SourceXml, string certificateName, SignMode mode)
        {
            
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = false;
            doc.LoadXml(SourceXml);

            Signer signer = new Signer(doc, certificateName, mode);
            
            return doc;
        }

        
        void AddBodyId()
        {
            string smev_ns = "http://smev.gosuslugi.ru/rev111111"; 

            XmlNamespaceManager ns = new XmlNamespaceManager(doc.NameTable);
            ns.AddNamespace("s", "http://schemas.xmlsoap.org/soap/envelope/");

            XmlElement envelopeNode = doc.DocumentElement.SelectSingleNode(@"//s:Envelope", ns) as XmlElement;
            //envelopeNode.SetAttribute("xmlns:smev", smev_ns);

            bodyNode = doc.DocumentElement.SelectSingleNode(@"//s:Body", ns) as XmlElement;
            if (bodyNode == null)
                throw new ApplicationException("No body tag found");
            
            bodyNode.SetAttribute("Id", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "body");
            bodyNode.Attributes["Id"].Prefix = "wsu";
            
            switch (this.Mode)
            {
                case SignMode.Request:

                    XmlNode requestMsg = bodyNode.FirstChild;

                    if (requestMsg.FirstChild.Name.Equals("Message"))
                        return;

                    XmlNode node = doc.CreateElement("Message", smev_ns);

                    XmlElement sender = doc.CreateElement("Sender", smev_ns);
                    XmlNodeAppendChild(sender, "Code", smev_ns, Smev.Data.Smev.MEMO_CODE);
                    XmlNodeAppendChild(sender, "Name", smev_ns, Smev.Data.Smev.MEMO_NAME);
                    node.AppendChild(sender);

                    XmlElement recipient = doc.CreateElement("Recipient", smev_ns);
                    XmlNodeAppendChild(recipient, "Code", smev_ns, Smev.Data.Smev.MEMO_CODE);
                    XmlNodeAppendChild(recipient, "Name", smev_ns, Smev.Data.Smev.MEMO_NAME);
                    node.AppendChild(recipient);

                    XmlElement originator = doc.CreateElement("Originator", smev_ns);
                    XmlNodeAppendChild(originator, "Code", smev_ns, Smev.Data.Smev.MEMO_CODE);
                    XmlNodeAppendChild(originator, "Name", smev_ns, Smev.Data.Smev.MEMO_NAME);
                    node.AppendChild(originator);

                    XmlNodeAppendChild(node, "TypeCode", smev_ns, "GFNC");
                    XmlNodeAppendChild(node, "Status", smev_ns, "REQUEST");
                    XmlNodeAppendChild(node, "Date", smev_ns, DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fffzzz"));
                    XmlNodeAppendChild(node, "ExchangeType", smev_ns, "2");

                    //XmlNodeAppendChild(node, "RequestIdRef", smev_ns, "9072131f-2fa8-418c-8d54-e165507ce649");
                    //XmlNodeAppendChild(node, "OriginRequestIdRef", smev_ns, "9072131f-2fa8-418c-8d54-e165507ce649");

                    node.AppendChild(doc.CreateElement("RequestIdRef", smev_ns));
                    node.AppendChild(doc.CreateElement("OriginRequestIdRef", smev_ns));
                    
                    node.AppendChild(doc.CreateElement("ServiceCode", smev_ns));
                    node.AppendChild(doc.CreateElement("CaseNumber", smev_ns));

                    XmlNode nodeDataR = doc.CreateElement("MessageData", smev_ns);
                    XmlElement nodeAppDataR = doc.CreateElement("AppData", smev_ns);
                    nodeDataR.AppendChild(nodeAppDataR);
                    nodeAppDataR.AppendChild(GetRequest(requestMsg.FirstChild));
                    requestMsg.RemoveAll();
                    requestMsg.AppendChild(nodeDataR);

                    requestMsg.InsertBefore(node, requestMsg.FirstChild);

                    break;
            }
        }

        private XmlNode GetRequest(XmlNode node)
        {
            if (node.Name.Equals("MessageData") || node.Name.Equals("AppData"))
            {
                if (node.FirstChild != null)
                    return GetRequest(node.FirstChild);
            }

            return node;
        }

        void XmlNodeAppendChild(XmlNode node, string name, string ns, string value)
        {
            XmlElement el = doc.CreateElement(name, ns);

            el.InnerText = value;

            node.AppendChild(el);
        }

        void AddHeader()
        {
            XmlNamespaceManager ns = new XmlNamespaceManager(doc.NameTable);
            ns.AddNamespace("s", "http://schemas.xmlsoap.org/soap/envelope/");
            headerNode = doc.DocumentElement.SelectSingleNode(@"//s:Header", ns) as XmlElement;
            if (headerNode != null)
            {
                //headerNode.RemoveAll();
            }
            else
            {
                headerNode = doc.CreateElement("Header", "http://schemas.xmlsoap.org/soap/envelope/");
                headerNode.Prefix = "s";
                bodyNode.ParentNode.InsertAfter(headerNode, null);
                //headerNode.InsertBefore(bodyNode, null);
            }

            AddSecurity();
        }

        void AddSecurity()
        {
            securityNode = doc.CreateElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");

            XmlAttribute attr = doc.CreateAttribute("actor", "http://schemas.xmlsoap.org/soap/envelope/");
            attr.Value = "http://smev.gosuslugi.ru/actors/smev";
            securityNode.Attributes.Append(attr);

            headerNode.AppendChild(securityNode);

            AddSignature();

            AddBinarySecurityToken();
        }

        void AddSignature()
        {
            signatureNode = doc.CreateElement("ds", "Signature", "http://www.w3.org/2000/09/xmldsig#");
            securityNode.AppendChild(signatureNode);

            signedInfoNode = doc.CreateElement("ds", "SignedInfo", "http://www.w3.org/2000/09/xmldsig#");
            signatureNode.AppendChild(signedInfoNode);

            XmlNode node = doc.CreateElement("ds", "CanonicalizationMethod", "http://www.w3.org/2000/09/xmldsig#");
            XmlAttribute attr = doc.CreateAttribute("Algorithm");
            attr.Value = "http://www.w3.org/2001/10/xml-exc-c14n#";
            node.Attributes.Append(attr);
            signedInfoNode.AppendChild(node);

            node = doc.CreateElement("ds", "SignatureMethod", "http://www.w3.org/2000/09/xmldsig#");
            attr = doc.CreateAttribute("Algorithm");
            attr.Value = "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411";
            node.Attributes.Append(attr);
            signedInfoNode.AppendChild(node);

            XmlNode referenceNode = doc.CreateElement("ds", "Reference", "http://www.w3.org/2000/09/xmldsig#");
            attr = doc.CreateAttribute("URI");
            attr.Value = "#body";
            referenceNode.Attributes.Append(attr);
            signedInfoNode.AppendChild(referenceNode);


            XmlNode transformsNode = doc.CreateElement("ds", "Transforms", "http://www.w3.org/2000/09/xmldsig#");
            referenceNode.AppendChild(transformsNode);

            //node = doc.CreateElement("ds", "Transform", "http://www.w3.org/2000/09/xmldsig#");
            //attr = doc.CreateAttribute("Algorithm");
            //attr.Value = "http://www.w3.org/2000/09/xmldsig#enveloped-signature";
            //node.Attributes.Append(attr);
            //transformsNode.AppendChild(node);

            node = doc.CreateElement("ds", "Transform", "http://www.w3.org/2000/09/xmldsig#");
            attr = doc.CreateAttribute("Algorithm");
            attr.Value = "http://www.w3.org/2001/10/xml-exc-c14n#";
            node.Attributes.Append(attr);
            transformsNode.AppendChild(node);


            node = doc.CreateElement("ds", "DigestMethod", "http://www.w3.org/2000/09/xmldsig#");
            attr = doc.CreateAttribute("Algorithm");
            attr.Value = "http://www.w3.org/2001/04/xmldsig-more#gostr3411";
            node.Attributes.Append(attr);
            referenceNode.AppendChild(node);

            node = doc.CreateElement("ds", "DigestValue", "http://www.w3.org/2000/09/xmldsig#");
            node.InnerText = getBodyHash();
            referenceNode.AppendChild(node);

            node = doc.CreateElement("ds", "SignatureValue", "http://www.w3.org/2000/09/xmldsig#");
            node.InnerText = getSignature();
            signatureNode.AppendChild(node);


            XmlNode keyInfoNode = doc.CreateElement("ds", "KeyInfo", "http://www.w3.org/2000/09/xmldsig#");
            signatureNode.AppendChild(keyInfoNode);

            XmlNode securityTokenReferenceNode = doc.CreateElement("wsse", "SecurityTokenReference", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            keyInfoNode.AppendChild(securityTokenReferenceNode);

            node = doc.CreateElement("wsse", "Reference", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            attr = doc.CreateAttribute("URI");
            attr.Value = "#SenderCertificate";
            node.Attributes.Append(attr);
            securityTokenReferenceNode.AppendChild(node);
        }

        void AddBinarySecurityToken()
        {
            XmlNode node = doc.CreateElement("wsse", "BinarySecurityToken", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            XmlAttribute attr = doc.CreateAttribute("EncodingType");
            attr.Value = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary";
            node.Attributes.Append(attr);

            attr = doc.CreateAttribute("ValueType");
            attr.Value = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3";
            node.Attributes.Append(attr);

            attr = doc.CreateAttribute("Id", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            attr.Value = "SenderCertificate";
            node.Attributes.Append(attr);

            string certData = Convert.ToBase64String(certificate.GetRawCertData());

            node.InnerText = certData;

            securityNode.AppendChild(node);            
        }

        byte[] computeHash(XmlNode node, bool canonOnly)
        {
            MemoryStream canonicalStream = (MemoryStream)Canonicalizer.Transform(node);
            byte[] canonData = canonicalStream.ToArray();

            if (canonOnly)
            {
                return canonData;
            }
            else
            {
                Gost3411CryptoServiceProvider GostHash = new Gost3411CryptoServiceProvider();
                canonicalStream.Position = 0;
                byte[] hashValue = GostHash.ComputeHash(canonicalStream);
                return hashValue;
            }
            
        }

        string getBodyHash()
        {
            byte[] hashValue = computeHash(this.bodyNode, false);
            
            return Convert.ToBase64String(hashValue);
        }

        string getSignature()
        {
            byte[] hashValue = computeHash(this.signedInfoNode, false);

            GostSignatureFormatter formatter = new GostSignatureFormatter(certificate.PrivateKey);
            byte[] signedHashValue = formatter.CreateSignature(hashValue);

            return Convert.ToBase64String(signedHashValue);
        }
    }
}
