﻿namespace bgTeam.IsRui.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Common;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;

    public sealed class MiddlewareException
    {
        private readonly IAppLogger _logger;
        private readonly IHostingEnvironment _env;
        private readonly RequestDelegate _next;

        public MiddlewareException(RequestDelegate next, IAppLogger logger, IHostingEnvironment env)
        {
            _next = next;
            _env = env;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (IsRuiEditException exp)
            {
                LogError(context, exp);
                await HandleExceptionAsync(context, exp.GetBaseException(), HttpStatusCode.Forbidden);
            }
            catch (IsRuiException exp)
            {
                LogError(context, exp);
                await HandleExceptionAsync(context, exp.GetBaseException(), HttpStatusCode.BadRequest);
            }
            catch (Exception exp)
            {
                LogError(context, exp);
                await HandleExceptionAsync(context, exp.GetBaseException(), HttpStatusCode.InternalServerError);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exp, HttpStatusCode code)
        {
            var stackTrace = string.Empty;

            if (_env.IsDevelopment() || _env.IsEnvironment("Debug") || _env.IsEnvironment("Release"))
            {
                stackTrace = exp.StackTrace;
            }

            var result = JsonConvert.SerializeObject(new { Code = code, Message = exp.Message, StackTrace = stackTrace });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            return context.Response.WriteAsync(result);
        }

        private void LogError(HttpContext context, Exception exp)
        {
            try
            {
                var builder = new StringBuilder();
                var content = GetContent(context);
                builder.AppendLine("==============================================================");
                builder.AppendLine($"Request path: {context.Request.Path.Value}");
                builder.AppendLine($"Request query: {content}");
                builder.AppendLine("--------------------------------------------------------------");
                builder.AppendLine($"StackTrace: {exp.ToString()}");

                _logger.Error(builder.ToString());
            }
            catch (Exception expFatal)
            {
                _logger.Fatal(expFatal);
            }
        }

        private string GetContent(HttpContext context)
        {
            StringBuilder result = new StringBuilder();

            if (context.Request.Query != null && context.Request.Query.Count > 0)
            {
                //result.AppendJoin("&", context.Request.Query);
                result.Append(string.Join("&", context.Request.Query));
                result.AppendLine();
            }

            if (context.Request.HasFormContentType && context.Request.Form.Count > 0)
            {
                var farr = context.Request.Form.Select(x => $"{x.Key}:{x.Value}");
                //result.AppendJoin("&", farr);
                result.Append(string.Join("&", farr));
            }

            return result.ToString();
        }
    }
}
