﻿namespace bgTeam.IsRui.Web.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class HomeController : Controller
    {
        public string Index()
        {
            return "IS RUI. ASP.NET Core v2.0";
        }
    }
}
