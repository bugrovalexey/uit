﻿namespace bgTeam.AdminDB.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using bgTeam.Core;
    using bgTeam.DataAccess;

    public class AppSettings : IConnectionSetting
    {
        public static readonly string PRIMARY_KEY = "Id";
        public static readonly bool PRIMARY_KEY_SHOW = true;

        public string ConnectionString { get; set; }

        public AppSettings(IAppConfiguration appConfiguration)
        {
            ConnectionString = appConfiguration.GetConnectionString("MAINDB");
        }
    }
}
