﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807180101)]
    public class Migration_201807180101 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("Plans_Activity_Contract")
                .WithColumn("Plans_Activity_Contract_ID").AsInt32().PrimaryKey()
                .WithColumn("Plans_Activity_ID").AsInt32().NotNullable()
                .WithColumn("PA_Contract_Name").AsString().Nullable()
                .WithColumn("PA_Contract_Number").AsString().Nullable()
                .WithColumn("PA_Contract_BeginDate").AsDateTime().Nullable()
                .WithColumn("PA_Contract_EndDate").AsDateTime().Nullable()
                .WithColumn("PA_Contract_ChangeDate").AsDateTime().Nullable();

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "Plans_Activity_Contract" });
        }
    }
}
