﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class AoDocumentBaseGetAllDocumentsStory : IStory<AoDocumentBaseGetAllDocumentsStoryContext, IEnumerable<AODecisionToCreateDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;

        public AoDocumentBaseGetAllDocumentsStory(
            IRepositoryRui repository,
            IMapperBase mapper)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public IEnumerable<AODecisionToCreateDto> Execute(AoDocumentBaseGetAllDocumentsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<AODecisionToCreateDto>> ExecuteAsync(AoDocumentBaseGetAllDocumentsStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            var list = _repository.GetAllEx<AODecisionToCreate>(q =>
            {
                    q.Where(x => x.AccountingObject.Id == context.AoId);
            });

            foreach (AODecisionToCreate str in list)
            {
                var doc = _repository.Get<Files>(q => q.Where(x => x.EntityType == 4 && x.EntityOwner == str));

                str.Documents.Add(doc);
            }

            return _mapper.Map(list, new List<AODecisionToCreateDto>());
        }
    }
}