﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Collections.Generic;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Documents;

    public class AoDocumentBaseCreateOrUpdateDocumentStoryContext
    {
        public int AoId { get; set; }

        public int? Id { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public DateTime? DateAdoption { get; set; }

        public string Particleofdocument { get; set; }

        public CreateDocumentFileData file { get; set; }

    }
}
