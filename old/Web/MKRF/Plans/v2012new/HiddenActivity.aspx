﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="HiddenActivity.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.HiddenActivity" %>

<%@ Register Src="~/Common/ActivityReadMode.ascx" TagPrefix="arm" TagName="ActivityReadMode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <style>
        img.linksPrevNext {
        }

        a.linksPrevNext {
            text-decoration: none;
        }

        #PrevNextActivity {
            width: 98%;
            text-align: right;
            margin-right: 10px;
        }


        #spanFieldsFillIcon {
            width: 16px;
            height: 16px;
        }

        span.spanWrapper {
            position: relative;
            top: 3px;
        }


        /*боковое меню*/
        div.mblock {
            /*background-color: red;
            width: 100%;*/
            font: 12px 'Segoe UI', Helvetica, 'Droid Sans', Tahoma, Geneva, sans-serif;
        }

        div.lblock {
            /*margin-top: 25px;
            background-color: yellow;*/
            float: left;
            min-height: 174px;
            width: 15%;
        }

        .lblock-width {
            width: 150px;
        }

        div.rblock {
            margin-top: 5px;
            width: 85%;
            float: left;
        }

        div.clear {
            clear: both;
        }

        #back-top {
            position: fixed;
            bottom: 70px;
        }

            #back-top a {
                width: 108px;
                display: block;
                text-align: center;
                font: 11px/100% Arial, Helvetica, sans-serif;
                text-transform: uppercase;
                text-decoration: none;
                color: #bbb;
                -webkit-transition: 1s;
                -moz-transition: 1s;
                transition: 1s;
                font-family: Cuprum;
            }

                #back-top a:hover {
                    color: #000;
                }

            #back-top span {
                width: 108px;
                height: 108px;
                display: block;
                margin-bottom: 7px;
                background: #ddd url(/Content/GB/Styles/Imgs/up-arrow.png) no-repeat center center;
                -webkit-border-radius: 15px;
                -moz-border-radius: 15px;
                border-radius: 15px;
                -webkit-transition: 1s;
                -moz-transition: 1s;
                transition: 1s;
            }

            #back-top a:hover span {
                /*background-color: #4B6786;*/
                background-color: #A05050;
                background-color: -webkit-gradient(linear, 0 0, 0 bottom, from(#3c5b7a), to(#5d7594));
                background-color: -moz-linear-gradient(#3c5b7a, #5d7594);
                background-color: linear-gradient(#3c5b7a, #5d7594);
                -pie-background-color: linear-gradient(#3c5b7a, #5d7594);
            }

        #activityMenu {
            list-style: none;
            border-right: 4px #5e1a1a solid;
        }

            #activityMenu li a {
                display: block;
                padding: 10px 12px;
                text-decoration: none;
                text-align: center;
                background-color: #e0e0e0;
                color: #666666;
                margin: 1px 0;
            }

                #activityMenu li a:hover {
                    background-color: #B4C0D0;
                    color: #fff;
                }

                #activityMenu li a.current {
                    background-color: #5e1a1a;
                    color: #fff;
                }

        .detailTopFull {
            z-index: 10;
        }
    </style>
    <script type="text/javascript" src="/Content/GB/Scripts/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Content/GB/Scripts/jquery.nav.js"></script>
    <script type="text/javascript">
        $(function () {
            var menu = $('#activityMenu');
            var detailPanelHeight = $('.detailTopFull').height() + 2;
            menu.onePageNav({ scrollOffset: detailPanelHeight, scrollThreshold: 0.1 });

            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });

            $('#back-top a').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
        });
    </script>
    <div class="page" style="position: relative">
        <div class="arrow_back" style="position: absolute; top: -38px;">
            <%=GetOwner()%>
        </div>
        <div>
            <table style="width: 100%; padding-bottom: 20px; font-size: 1em;">
                <tr>
                    <td style="padding-right: 20px; font-size: 1.5em; color: #333"><b><%=GetActivityTitle()%></b></td>
                    <td style="vertical-align: top; width: 1%; text-align: right;">
                        <% if (this.Links.Any())
                           { %>
                        <table>
                            <tr>
                                <% if (this.Links.Where(x => x.LinkType == 3).Any())
                                   { %>
                                <td>
                                    <dxe:ASPxButton ID="hidButton3" runat="server" ClientInstanceName="hidButton3" AutoPostBack="false" Text="Создано из ▼" CssClass="fleft topButton"></dxe:ASPxButton>
                                    <dxpc:ASPxPopupControl ID="pc3" runat="server" CloseAction="OuterMouseClick" PopupElementID="hidButton3" PopupVerticalAlign="Below" PopupHorizontalAlign="RightSides"
                                        ShowFooter="False" ShowHeader="False" Width="760">
                                        <ContentCollection>
                                            <dxpc:PopupControlContentControl ID="pcc3" runat="server">
                                                <%=GetLinks(3)%>
                                            </dxpc:PopupControlContentControl>
                                        </ContentCollection>
                                    </dxpc:ASPxPopupControl>
                                </td>
                                <% } %>

                                <% if (this.Links.Where(x => x.LinkType == 1).Any())
                                   { %>
                                <td>
                                    <dxe:ASPxButton ID="hidButton1" runat="server" ClientInstanceName="hidButton1" AutoPostBack="false" Text="Разъединено в ▼" CssClass="fleft topButton"></dxe:ASPxButton>
                                    <dxpc:ASPxPopupControl ID="pc1" runat="server" CloseAction="OuterMouseClick" PopupElementID="hidButton1" PopupVerticalAlign="Below" PopupHorizontalAlign="RightSides"
                                        ShowFooter="False" ShowHeader="False" Width="760">
                                        <ContentCollection>
                                            <dxpc:PopupControlContentControl ID="pcc1" runat="server">
                                                <%=GetLinks(1)%>
                                            </dxpc:PopupControlContentControl>
                                        </ContentCollection>
                                    </dxpc:ASPxPopupControl>
                                </td>
                                <% } %>

                                <% if (this.Links.Where(x => x.LinkType == 2).Any())
                                   { %>
                                <td>
                                    <dxe:ASPxButton ID="hidButton2" runat="server" ClientInstanceName="hidButton2" AutoPostBack="false" Text="Объединено из ▼" CssClass="fleft topButton"></dxe:ASPxButton>
                                    <dxpc:ASPxPopupControl ID="pc2" runat="server" CloseAction="OuterMouseClick" PopupElementID="hidButton2" PopupVerticalAlign="Below" PopupHorizontalAlign="RightSides"
                                        ShowFooter="False" ShowHeader="False" Width="760">
                                        <ContentCollection>
                                            <dxpc:PopupControlContentControl ID="pcc2" runat="server">
                                                <%=GetLinks(2)%>
                                            </dxpc:PopupControlContentControl>
                                        </ContentCollection>
                                    </dxpc:ASPxPopupControl>
                                </td>
                                <% } %>

                                 <% if (this.Links.Where(x => x.LinkType == 4).Any()) { %>
                                <td>
                                    <dxe:ASPxButton ID="hidButton4" runat="server" ClientInstanceName="hidButton4" AutoPostBack="false" Text="Объединено в ▼" CssClass="fleft topButton"></dxe:ASPxButton>
                                    <dxpc:ASPxPopupControl ID="pc4" runat="server" CloseAction="OuterMouseClick" PopupElementID="hidButton4" PopupVerticalAlign="Below" PopupHorizontalAlign="RightSides"
                                        ShowFooter="False" ShowHeader="False" Width="760">
                                        <ContentCollection>
                                            <dxpc:PopupControlContentControl ID="pcc4" runat="server">
                                                <%=GetLinks(4)%>
                                            </dxpc:PopupControlContentControl>
                                        </ContentCollection>
                                    </dxpc:ASPxPopupControl>
                                </td>
                                <% } %>
                            </tr>
                        </table>
                        <% } %>
                    </td>
                </tr>
            </table>
        </div>

        <div class="mblock">
            <div class="lblock">
                <ul id="activityMenu" class="lblock-width">
                    <li>
                        <a class="current" href="#CommonData">Общие сведения</a>
                    </li>
                    <li>
                        <a href="#Functions">Функции</a>
                    </li>
                    <li>
                        <a href="#Indicators">Показатели/Индикаторы</a>
                    </li>
                    <li>
                        <a href="#Expenses">Расходы</a>
                    </li>
                    <li>
                        <a href="#Results">Результаты</a>
                    </li>
                    <li>
                        <a href="#Works">Работы</a>
                    </li>
                    <li>
                        <a href="#Specification">Товары</a>
                    </li>
                    <% if (model.Plan_Type == (int)GB.MKRF.Entities.Directories.ExpObjectTypeEnum.Plan2012rel)
                       { %>
                    <li>
                        <a href="#Purchases">Закупки</a>
                    </li>
                    <% } %>
                </ul>
            </div>
            <div class="rblock">
                <arm:ActivityReadMode runat="server" ID="ActivityReadMode" DefaultShowEditLinks="True" DefaultShowCollapseLink="True" />
            </div>
        </div>
        <p id="back-top" style="display: none;">
            <a href="#top">
                <span></span>
                в начало
            </a>
        </p>
        <div class="clear"></div>
    </div>
</asp:Content>
