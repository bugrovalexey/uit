﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.UserComments
{

    public class AddUserCommentContext : ICommandContext
    {
        public int? xId { get; set; }

        public string Comment { get; set; }
        
        public int AccountingObjectId { get; set; }
    }
}
