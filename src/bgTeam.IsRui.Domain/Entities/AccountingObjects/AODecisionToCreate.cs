﻿using System;
using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.Domain.Entities.Documents;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    /// <summary>
    /// Решение о создании (закупки) ОУ
    /// </summary>
    public class AODecisionToCreate : EntityBase
    {
        /// <summary>
        /// Объект учета
        /// </summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        /// Дата принятия документа
        /// </summary>
        public virtual DateTime? DateAdoption { get; set; }

        /// <summary>
        /// Пункт, статья документа
        /// </summary>
        public virtual string ParticleOfDocument { get; set; }

        /// <summary>
        /// URL на документ
        /// </summary>
        public virtual string Url { get; set; }

        /// <summary>
        /// Файлы-основание
        /// </summary>
        public virtual IList<Files> Documents { get; set; }

        //public virtual string FileInfo
        //{
        //    get
        //    {
        //        var result = string.Join(";", Documents.Select(x => (x.Id.ToString() + ":" + x.FileName)).ToArray());
        //        return result;
        //    }
        //}
    }
}