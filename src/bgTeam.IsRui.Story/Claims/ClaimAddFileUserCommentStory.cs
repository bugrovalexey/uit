﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Story.AccessObjects;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimAddFileUserCommentStory : IStory<ClaimAddFileUserCommentStoryContext, FilesDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IFileProviderRui _fileProvider;
        private readonly IStatusRepository _statusRepository;
        private readonly IAccessEditService _accessEditService;

        public ClaimAddFileUserCommentStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IUserRepository userRepository,
            ISessionNHibernateFactory factory,
            IFileProviderRui fileProvider,
            IStatusRepository statusRepository,
            IAccessEditService accessEditService)
        {
            _mapper = mapper;
            _repository = repository;
            _userRepository = userRepository;
            _factory = factory;
            _fileProvider = fileProvider;
            _statusRepository = statusRepository;
            _accessEditService = accessEditService;
        }

        public FilesDto Execute(ClaimAddFileUserCommentStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<FilesDto> ExecuteAsync(ClaimAddFileUserCommentStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException("Не найдена заявка");
            }

            if (!context.Id.HasValue)
            {
                var createStory = new ClaimCreateOrUpdateUserCommentStory(_repository, _mapper, _factory, _userRepository, _statusRepository, _accessEditService);

                var userComment = await createStory.ExecuteAsync(new ClaimCreateOrUpdateUserCommentStoryContext
                {
                    ClaimId = context.ClaimId.Value
                });

                context.Id = userComment.Id;
            }

            WorkflowUserConclusion uc = _repository.Get<WorkflowUserConclusion>(context.Id);

            var addfilecontext = _mapper.Map(context, new FileAddStoryContext());

            addfilecontext.Type = DocTypeEnum.WorkflowUserConclusion;
            addfilecontext.Entity = uc;

            var result = await new FileAddStory(_mapper, _repository, _fileProvider, _factory)
                .ExecuteAsync(addfilecontext);

            return _mapper.Map(result, new FilesDto());
        }
    }
}
