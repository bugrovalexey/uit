﻿namespace bgTeam.IsRui.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Базовый класс сущности
    /// </summary>
    //[Serializable]
    public class EntityBase : IEntity, IAccess //(TODO: используется во ФГИС - надо проверить зачем)
    {
        /// <summary>
        /// Id по умолчанию
        /// </summary>
        public const int Default = 0;

        /// <summary>
        /// Id для несохраненных сущностей
        /// </summary>
        public const int UnsavedId = -1;

        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Битовая маска определением доступа (в маппинге не используется)
        /// </summary>
        public virtual AccessActionEnum? Access { get; set; }

        /// <summary>
        /// Установка идентификатора сущности (не переопределять)
        /// </summary>
        //public virtual void SetId(int id)
        //{
        //    this.Id = id;
        //}

        public EntityBase()
        {
            this.Id = UnsavedId;
            TimeStamp = DateTime.Now;
        }

        //internal EntityBase(int id = UnsavedId)
        //{
        //    this.Id = id;
        //}

        public virtual bool IsNew()
        {
            return Id == UnsavedId;
        }

        public virtual DateTime TimeStamp { get; set; }
    }

    /// <summary>
    /// Базовый класс сущности - справочник
    /// </summary>
    [Serializable]
    public class EntityDirectories : EntityBase, IEntity, IEntityName
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public class EntityTreeDirectories<T> : EntityDirectories
        where T : EntityDirectories
    {
        /// <summary>
        /// Parent category
        /// </summary>
        public virtual T Parent { get; set; }

        /// <summary>
        /// Child categories
        /// </summary>
        public virtual IEnumerable<T> Childs { get; set; }
    }
}