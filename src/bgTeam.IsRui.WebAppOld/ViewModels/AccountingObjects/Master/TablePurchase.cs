﻿using System.ComponentModel;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class TablePurchase : NotificationBaseViewModel
    {
        public TablePurchase(bgTeam.IsRui.Domain.Entities.AccountingObjects.GovPurchase res)
        {
            Id = res.Id;
            this.Number = res.Number;
            this.Type = res.Type.GetDescription();
            this.Date = res.Date.HasValue ? res.Date.Value.ToString("dd.MM.yyyy") : string.Empty;
            this.Name = res.Name;
            this.Price = res.Price.ToString("N2");
            this.Summ = res.Summ.ToString("N2");

            DeleteLink = LinkBuilder.AjaxDeleteLink("PurchaseDelete", "AccountingObjects", new { id = Id }, "GridPurchase");
        }
        public int Id { get; set; }

        [DisplayName("Номер")]
        public string Number { get; set; }

        [DisplayName("Тип")]
        public string Type { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Дата публикации")]
        public string Date { get; set; }

        [DisplayName("Стоимость")]
        public string Price { get; set; }

        [DisplayName("Сумма расходов")]
        public string Summ { get; set; }

        public string DeleteLink { get; private set; }
    }
}