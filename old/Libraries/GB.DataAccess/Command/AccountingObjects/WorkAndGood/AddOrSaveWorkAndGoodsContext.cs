﻿using GB.Data.Directories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.WorkAndGood
{
    public class AddOrSaveWorkAndGoodsContext : ICommandContext
    {
        public int ObjectId { get; set; }

        public int? AccountingObjectId { get; set; }

        public string Name { get; set; }

        public int? CategoryKindOfSupportId { get; set; }

        public decimal Summa { get; set; }

        public CategoryTypeOfSupportEnum Type { get; set; }

        public bool IsNew { get; set; }
    }
}
