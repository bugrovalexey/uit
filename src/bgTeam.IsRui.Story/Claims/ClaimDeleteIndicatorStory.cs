﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Claims;

    public class ClaimDeleteIndicatorStory : ClaimDeleteBaseStory<ClaimIndicator, ClaimDeleteIndicatorStoryContext>
    {
        public ClaimDeleteIndicatorStory(
            IRepositoryRui repositoryRui,
            ISessionNHibernateFactory sessionNHibernateFactory)
            : base(repositoryRui, sessionNHibernateFactory)
        {
        }
    }
}
