﻿using System;
using System.Collections.Generic;
using GB.MKRF.Entities;

namespace GB.MKRF.Workflow
{
    public class WorkflowManager : ManagerBase<IWorkflowHandler, IWorkflowObject>
    {
        //readonly object syncRoot = new object();
        //readonly Dictionary<Type, IWorkflowHandler> handlers = new Dictionary<Type, IWorkflowHandler>();

        ///// <summary>Использовать через Singleton</summary>
        protected WorkflowManager()
        {
        }

        //public IWorkflowHandler GetHandler<T>() where T : IWorkflowObject
        //{
        //    IWorkflowHandler handler;
        //    lock (syncRoot)
        //    {
        //        handlers.TryGetValue(typeof(T), out handler);
        //    }
        //    return handler;
        //}

        //public void RegisterHandler<T>(IWorkflowHandler handler) where T : IWorkflowObject
        //{
        //    lock (syncRoot)
        //    {
        //        handlers[typeof(T)] = handler;
        //    }
        //}
    }
}