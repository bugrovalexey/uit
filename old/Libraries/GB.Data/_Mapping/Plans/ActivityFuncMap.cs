﻿//using GB.Entity.Directories.FPGU;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Mapping.Plans
{
    class ActivityFuncMap : ClassMapBase<PlansActivityFunc>
    {
        public ActivityFuncMap()
            : base("Plans_Activity_Func")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Func_Functionality");
            Map(x => x.DirectedToExpl, "Plans_Activity_Func_Directed");
            Map(x => x.IsUsedBefore, "Plans_Activity_Func_Is_Used_Before");
            Map(x => x.Comment, "Plans_Activity_Func_Comment");

            Map(x => x.IsMissingFunc, "Plans_Activity_Func_Is_Missing");
            Map(x => x.MissingFuncName, "Plans_Activity_Func_Missing_Name");

            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID");
            References<FuncType>(x => x.Type, "Func_Type_ID");
            References<NPA>(x => x.NPA, "NPA_ID");

            //References<Gov_Service>(x => x.Service, "Gov_Service_Id");
            //References<NPA_Document>(x => x.Document, "NPA_Document_id");
            References<TypicalActivities>(x => x.TypicalActivity, "Typical_Activities_ID");
        }
    }
}