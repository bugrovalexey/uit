﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ExpenseDirectionToYearMap : ClassMapBase<ExpenseDirectionToYear>
	{
        public ExpenseDirectionToYearMap()
            : base("Expense_Direction_Years", "v_", true)
		{
            //Table("v_");

            //Id(x => x.Id, "Expense_Direction_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

            //SqlInsert(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlUpdate(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlDelete(@"raiserror ('Unsupported',10,1)").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Code, "Expense_Direction_Code");
			Map(x => x.Name, "Expense_Direction_Name");
			Map(x => x.CodeName, "Expense_Direction_CodeName");
			Map(x => x.Code3, "Expense_Direction_Code3");
			Map(x => x.Code3Name, "Expense_Direction_Code3Name");
			Map(x => x.Year, "Year");
			Map(x => x.Group, "Expense_Direction_Group");
        }
    }
}
