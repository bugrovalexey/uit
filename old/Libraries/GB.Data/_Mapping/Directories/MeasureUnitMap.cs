﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class MeasureUnitMap : ClassMapBase<MeasureUnit>
    {
        public MeasureUnitMap() :
            base("Measure_Unit")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Measure_Unit_Short_Name");
            Map(x => x.FullName, "Measure_Unit_Name");
        }
    }
}