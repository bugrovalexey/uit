﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CostsUpdateCostsStory : IStory<CostsUpdateCostsStoryContext, ActivityCostDto>
    {
        private readonly IMapperBase _mapper;
        private readonly ISessionNHibernateFactory _sessionNHibernateFactory;
        private readonly IRepositoryRui _repositoryRui;

        public CostsUpdateCostsStory(IMapperBase mapper, ISessionNHibernateFactory factory, IRepositoryRui repository)
        {
            _mapper = mapper;
            _sessionNHibernateFactory = factory;
            _repositoryRui = repository;
        }

        public ActivityCostDto Execute(CostsUpdateCostsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityCostDto> ExecuteAsync(CostsUpdateCostsStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            using (var session = _sessionNHibernateFactory.OpenSession())
            {
                var activity = _repositoryRui.Get<PlansActivity>(context.ActivityId);
                activity.BudgetItem = _repositoryRui.Get<ArticleBudget>(context.BudgetItemId);

                _mapper.Map(context, activity);

                await session.SaveOrUpdateAsync(activity);

                return _mapper.Map(activity, new ActivityCostDto());
            }
        }
    }
}
