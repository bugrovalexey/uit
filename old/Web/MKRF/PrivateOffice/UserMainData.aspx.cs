﻿using System;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Repository;
using GB.Providers;
using GB.Repository;
using DevExpress.Web.ASPxEditors;

namespace GB.MKRF.Web.PrivateOffice
{
    /// <summary>
    /// 
    /// </summary>
    public partial class UserMainData : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            User currentUser = UserRepository.GetCurrent();

            if (currentUser != null)
            {
                FTextBox.Text = currentUser.Surname;
                ITextBox.Text = currentUser.Name;
                OTextBox.Text = currentUser.Middlename;
                ETextBox.Text = currentUser.Education;

                WorkTextBox.Text = currentUser.Work;
                JobTextBox.Text = currentUser.Job;

                EmailTextBox.Text = currentUser.Email;
                FaxTextBox.Text = currentUser.Fax;
                MobPhoneTextBox.Text = currentUser.MobPhone;
                WorkPhoneTextBox.Text = currentUser.WorkPhone;

                EmailCheckBox.Checked = currentUser.ReceiveMail;

                PublishDateNtfCheckBox.Checked = RepositoryBase<UserToNotification>.Get(x => x.User == currentUser && x.Notification.Id == (int)NotificationTypeEnum.PlansZakupkiPublishDate) != null;
                OpenDateNtfCheckBox.Checked = RepositoryBase<UserToNotification>.Get(x => x.User == currentUser && x.Notification.Id == (int)NotificationTypeEnum.PlansZakupkiOpenDate) != null;
                ConsDateNtfCheckBox.Checked = RepositoryBase<UserToNotification>.Get(x => x.User == currentUser && x.Notification.Id == (int)NotificationTypeEnum.PlansZakupkiConsDate) != null;
                ItogDateNtfCheckBox.Checked = RepositoryBase<UserToNotification>.Get(x => x.User == currentUser && x.Notification.Id == (int)NotificationTypeEnum.PlansZakupkiItogDate) != null;
                ViolationOfTermsNtfCheckBox.Checked = RepositoryBase<UserToNotification>.Get(x => x.User == currentUser && x.Notification.Id == (int)NotificationTypeEnum.PlansZakupkiViolationOfTerms) != null;
            }
            else
            {
                Response.Redirect("~/");
            }

            base.OnPreRender(e);
        }

        protected void SaveUserCallback_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            User currentUser = UserRepository.GetCurrent();

            if (currentUser == null)
                return;

            currentUser.Surname = FTextBox.Text;
            currentUser.Name = ITextBox.Text;
            currentUser.Middlename = OTextBox.Text;
            currentUser.Education = ETextBox.Text;

            currentUser.Work = WorkTextBox.Text;
            currentUser.Job = JobTextBox.Text;

            currentUser.Email = EmailTextBox.Text;
            currentUser.Fax = FaxTextBox.Text;
            currentUser.MobPhone = MobPhoneTextBox.Text;
            currentUser.WorkPhone = WorkPhoneTextBox.Text;

            currentUser.ReceiveMail = EmailCheckBox.Checked;

            if (currentUser.ReceiveMail)
            {
                SaveOrUpdateUserNotification(PublishDateNtfCheckBox, NotificationTypeEnum.PlansZakupkiPublishDate);
                SaveOrUpdateUserNotification(OpenDateNtfCheckBox, NotificationTypeEnum.PlansZakupkiOpenDate);
                SaveOrUpdateUserNotification(ConsDateNtfCheckBox, NotificationTypeEnum.PlansZakupkiConsDate);
                SaveOrUpdateUserNotification(ItogDateNtfCheckBox, NotificationTypeEnum.PlansZakupkiItogDate);
                SaveOrUpdateUserNotification(ViolationOfTermsNtfCheckBox, NotificationTypeEnum.PlansZakupkiViolationOfTerms);
            }

            RepositoryBase<User>.SaveOrUpdate(currentUser);
        }

        private void SaveOrUpdateUserNotification(ASPxCheckBox chb, NotificationTypeEnum type)
        {
            User currentUser = UserRepository.GetCurrent();
            var utn = RepositoryBase<UserToNotification>.Get(x => x.User == currentUser && x.Notification.Id == (int)type);
            if (chb.Checked && utn == null)
            {
                var userToNotification = new UserToNotification();
                userToNotification.User = currentUser;
                userToNotification.Notification = RepositoryBase<Notification>.Get(type);
                RepositoryBase<UserToNotification>.SaveOrUpdate(userToNotification);
            }
            if (!chb.Checked && utn != null)
            {
                RepositoryBase<UserToNotification>.Delete(utn);
            }
        }

        protected void PassChangeCallBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            string[] mass = e.Parameter.Split(';');

            if (mass.Length == 2)
            {

                mass[0] = UviriProvider.GetMD5Hash(mass[0]);
                mass[1] = UviriProvider.GetMD5Hash(mass[1]);

                User currentUser = UserRepository.GetCurrent();

                if (mass[0] == currentUser.Password)
                {
                    currentUser.Password = mass[1];

                    RepositoryBase<User>.SaveOrUpdate(currentUser);
                    e.Result = "true";
                }
                else
                    e.Result = "Текущий пароль не соответствует значению в поле <b>Старый пароль</b>. Пожалуйста введите его ещё раз.";
            }
            else
                e.Result = "При изменении пароля возникла ошибка.";
        }
    }
}