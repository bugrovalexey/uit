﻿
namespace GB.DataAccess.Services.Args
{
    public interface IWorkAndGoodsArgs : IServiceArgs
    {
        int Id { get; set; }

        int? AccountingObjectId { get; set; }

        string Name { get; set; }

        int? CategoryKindOfSupportId { get; set; }

        decimal Summa { get; set; }
    }
}
