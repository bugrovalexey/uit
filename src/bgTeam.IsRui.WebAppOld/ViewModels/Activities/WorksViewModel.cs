﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public class WorksViewModel : ActivitiesCardBase
    {
        public IList<TableWork> Items { get; private set; }

        public WorksViewModel(IStatusRepository statusRepository, int ownerId)
            : base(statusRepository, ownerId)
        {
            var controller = new PlansWorkService();
            Items = controller.Repository.GetAllEx(q => q
                    .Fetch(x => x.OKPD).Eager
                    .Fetch(x => x.ExpenseDirection).Eager
                    .Fetch(x => x.ExpenseType).Eager
                    .Fetch(x => x.ExpenditureItem).Eager
                    .Fetch(x => x.OKVED).Eager
                    .Where(x => x.PlanActivity == CurrentActivity))
                .Select(w => new TableWork(w))
                .ToList();
        }
    }

    public class TableWork : NotificationBaseViewModel
    {
        public int Id { get; set; }

        [DisplayName(WorkEditViewModel.NameCaption)]
        public string Name { get; set; }

        [DisplayName(WorkEditViewModel.OKPDCaption)]
        public string OKPDName { get; set; }

        [DisplayName(WorkEditViewModel.ExpenseDirectionCaption)]
        public string ExpenseDirectionName { get; set; }

        [DisplayName(WorkEditViewModel.ExpenseTypeCaption)]
        public string ExpenseTypeName { get; set; }

        [DisplayName(WorkEditViewModel.KOSGUCaption)]
        public string KOSGUCodeName { get; set; }

        [DisplayName(WorkEditViewModel.OKVEDCaption)]
        public string OKVEDCodeName { get; set; }

        [DisplayName(WorkEditViewModel.YearCaption)]
        public string YearDescription { get; set; } // Description = YearEnumExtension.GetDescription

        [DisplayName(WorkEditViewModel.SpecialistsCountCaption)]
        public int SpecialistsCount { get; set; }

        [DisplayName(WorkEditViewModel.DurationCaption)]
        public int Duration { get; set; }

        [DisplayName(WorkEditViewModel.SalaryCaption)]
        public decimal Salary { get; set; }


        [DisplayName(WorkEditViewModel.SummCaption)]
        public decimal Summ
        {
            get { return Salary * Duration * SpecialistsCount; }
        }

        //Служебные поля
        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("WorkDelete", "Activities", new { id = Id }, "Grid"); }
        }

        public TableWork(PlansWork work)
        {
            Mapper.Map(work, this);

            //Id = w.Id;
            //Name = w.Name;
            //OKPDName = w.OKPD.Return(x => x.Name);
            //ExpenseDirectionName = w.ExpenseDirection.Return(x => x.Name);
            //ExpenseTypeName = w.ExpenseType.Return(x => x.Name);
            //KOSGUCodeName = w.ExpenditureItem.Return(x => x.CodeName);
            //OKVEDCodeName = w.OKVED.Return(x => x.CodeName);
            //YearDescription = work.Year.Return(y => YearRepository.GetYearText(y));
            //SpecialistsCount = w.SpecialistsCount.Return<int, int>(x => x);
            //Duration = w.Duration.Return<int, int>(x => x);
            //Salary = w.Salary.ToMoneyString();
            //Summ = (w.Salary * Duration * SpecialistsCount).ToMoneyString();
        }
    }

    [MetadataType(typeof(WorkEditViewModel_Validation))]
    public class WorkEditViewModel : ActivitiesCardBase
    {
        public const string EditActionLink = "WorkEdit";

        public const string NameCaption = "Наименование";
        public const string OKPDCaption = "Код ОКПД";
        public const string ExpenseDirectionCaption = "Вид затрат";
        public const string ExpenseTypeCaption = "Тип затрат";
        public const string KOSGUCaption = "КОСГУ";
        public const string OKVEDCaption = "ОКВЭД";
        public const string YearCaption = "Год";
        public const string SpecialistsCountCaption = "Число специалистов";
        public const string SalaryCaption = "Среднемесячная зарплата, руб";
        public const string DurationCaption = "Длительность работ, мес.";
        public const string SummCaption = "Сумма расходов, руб";

        public int WorkId { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        #region OKPD

        public SelectList OKPDList { get; private set; }

        [DisplayName(OKPDCaption)]
        public int OKPDId { get; set; }

        #endregion OKPD

        #region ExpenseDirection

        public SelectList ExpenseDirectionList { get; private set; }

        [DisplayName(ExpenseDirectionCaption)]
        public int ExpenseDirectionId { get; set; }

        #endregion ExpenseDirection

        #region ExpenseType

        public SelectList ExpenseTypeList { get; private set; }

        [DisplayName(ExpenseTypeCaption)]
        public int ExpenseTypeId { get; set; }

        #endregion ExpenseType

        #region KOSGU

        public SelectList KOSGUList { get; private set; }

        [DisplayName(KOSGUCaption)]
        public int KOSGUId { get; set; }

        #endregion KOSGU

        #region OKVED

        public SelectList OKVEDList { get; private set; }

        [DisplayName(OKVEDCaption)]
        public int OKVEDId { get; set; }

        #endregion OKVED

        [DisplayName(YearCaption)]
        public YearEnum Year { get; set; }

        public SelectList YearList { get; private set; }

        [DisplayName(SpecialistsCountCaption)]
        public int SpecialistsCount { get; set; }

        [DisplayName(DurationCaption)]
        public int Duration { get; set; }

        [DisplayName(SalaryCaption)]
        public decimal Salary { get; set; }

        [DisplayName(SummCaption)]
        public decimal Summ
        {
            get { return Salary * Duration * SpecialistsCount; }
        }

        public WorkEditViewModel(IStatusRepository statusRepository, int ownerId, int? id)
            : base(statusRepository, ownerId)
        {
            if (id.HasValue)
            {
                //FillData(new Repository<PlansWork>().GetExisting(id));
                var work = new RepositoryDc<PlansWork>().GetExisting(id);
                Mapper.Map(work, this);
            }

            YearList = new SelectList(
                YearRepository.YearList,
                "Key", "Value", Year.ToString());
            DateTime date = DateTime.Now;
            ExpenseDirectionList = new SelectList(
                new RepositoryDc<ExpenseDirection>().GetAllEx(q => q
                    .Where(x => (x.Type == ExpenseDirectionTypeEnum.Works && x.DateStart <= date && date <= x.DateEnd) || x.Id == EntityBase.Default))
                .OrderBy(x => x.Code),
                "Id", "Name", ExpenseDirectionId);
            ExpenseTypeList = new SelectList(
                new RepositoryDc<ExpenseType>().GetAllEx(q => q
                    .Where(e => (e.ExpenseDirection != null && e.ExpenseDirection.Id == ExpenseDirectionId) || e.Id == EntityBase.Default)),
                "Id", "Name", ExpenseTypeId);
            KOSGUList = new SelectList(
                DirectoriesRepositary.GetDirectories<ExpenditureItem>(DateTime.Now.Year).OrderBy(x => x.Code),
                "Id", "CodeName", KOSGUId);
            OKPDList = new SelectList(
                DirectoriesRepositary.GetDirectories<OKPD>(DateTime.Now.Year).OrderBy(x => x.Code),
                "Id", "CodeName", OKPDId);
            OKVEDList = new SelectList(
                new RepositoryDc<OKVED>().GetAll(),
                "Id", "CodeName", OKVEDId);
        }

        //private void FillData(PlansWork w)
        //{
        //    WorkId = w.Id;
        //    Name = w.Name;
        //    OKPDId = w.OKPD.Return(x => x.Id);
        //    ExpenseDirectionId = w.ExpenseDirection.Return(x => x.Id);
        //    ExpenseTypeId = w.ExpenseType.Return(x => x.Id);
        //    KOSGUId = w.ExpenditureItem.Return(x => x.Id);
        //    OKVEDId = w.OKVED.Return(x => x.Id);
        //    Year = w.Year.Return(y => y, YearEnum.First);
        //    SpecialistsCount = w.SpecialistsCount.Return<int, int>(x => x);
        //    Duration = w.Duration.Return<int, int>(x => x);
        //    Salary = w.Salary;
        //}
    }

    public class WorkEditViewModel_Validation : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        public int ExpenseDirectionId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        public int OKVEDId { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int Duration { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int SpecialistsCount { get; set; }

        [Range(MoneyMin, MoneyMax, ErrorMessage = RangeMoneyText)]
        public object Salary { get; set; }
    }
}