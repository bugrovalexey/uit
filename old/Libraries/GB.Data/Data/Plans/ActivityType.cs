﻿namespace GB.Data.Plans
{
    /// <summary>Тип мероприятия Плана 2012</summary>
    public class ActivityType : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }
    }
}