﻿using GB.Entity.Directories;
using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Entities.Plans
{
    public class InitialActivity : EntityBase
    {
        public InitialActivity()
        {
        }

        public InitialActivity(PlansActivity activity)
        {
            Activity = activity;
        }

        public virtual string RegNum { get; set; }

        public virtual string Name { get; set; }

        public virtual string Comment { get; set; }

        public virtual decimal Summ { get; set; }

        public virtual WorkForm WorkForm { get; set; }

        public virtual ExpenditureItem ExpenditureItem { get; set; }

        public virtual PlansActivity Activity { get; set; }
    }
}