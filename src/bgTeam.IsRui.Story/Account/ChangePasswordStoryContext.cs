﻿namespace bgTeam.IsRui.Story.Account
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ChangePasswordStoryContext
    {
        public string CurrentPassword { get; set; }

        public string NewPassword { get; set; }
    }
}
