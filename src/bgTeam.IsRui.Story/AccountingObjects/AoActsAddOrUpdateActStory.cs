﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoActsAddOrUpdateActStory : IStory<AoActsAddOrUpdateActStoryContext, ActsDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IFileProviderRui _fileProvider;

        public AoActsAddOrUpdateActStory(
            IRepositoryRui repository, 
            ISessionNHibernateFactory factory, 
            IMapperBase mapperBase, 
            IFileProviderRui fileProvider)
        {
            _repository = repository;
            _factory = factory;
            _mapper = mapperBase;
            _fileProvider = fileProvider;
        }

        public ActsDto Execute(AoActsAddOrUpdateActStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActsDto> ExecuteAsync(AoActsAddOrUpdateActStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            AccountingObjectActs act;

            using (var session = _factory.OpenSession())
            {
                if (context.Id.HasValue)
                {
                    act = _repository.Get<AccountingObjectActs>(q => q.Where(x => x.Id == context.Id));

                    if (context.File.Id != act.FileId)
                    {
                        await new FileDeleteStory(_repository, _fileProvider, _factory)
                                .ExecuteAsync(new FileDeleteStoryContext
                                {
                                    Id = act.FileId,
                                });
                    }
                }
                else
                {
                    act = new AccountingObjectActs();
                }

                if ((act.FileId == null && context.File.Id == null) || (context.File.Id != act.FileId && context.File.Id == null))
                {
                    var addFile = await new FileAddStory(_mapper, _repository, _fileProvider, _factory)
                        .ExecuteAsync(new FileAddStoryContext()
                        {
                            Type = DocTypeEnum.ActOfAcceptance,
                            Entity = _repository.Get<AccountingObject>(context.AoId),
                            Name = context.File.FileName,
                            TempGuid = context.File.TempGuid,
                            Url = context.File.Url,
                            MimeType = context.File.MimeType,
                            Kind = context.File.Kind,
                            Size = context.File.Size
                        });

                    context.File.Id = addFile.Id;
                    context.File.FileName = addFile.Name;
                    context.File.Url = addFile.Url;
                    context.File.MimeType = addFile.MimeType;
                    context.File.Kind = addFile.Kind;
                    context.File.Size = addFile.Size;
                    context.File.Extension = addFile.Extension;

                }

                _mapper.Map(context, act);

                act.AccountingObject = new AccountingObject() { Id = context.AoId.Value };

                act.ChangeDate = DateTime.Now;

                await session.SaveOrUpdateAsync(act);
            }

            var result = _mapper.Map(act, new ActsDto());

            result.File.Id = context.File.Id;
            result.File.FileName = context.File.FileName;
            result.File.Url = context.File.Url;
            result.File.MimeType = context.File.MimeType;
            result.File.Kind = context.File.Kind;
            result.File.Size = context.File.Size;
            result.File.Extension = context.File.Extension;

            return result;
        }
    }
}
