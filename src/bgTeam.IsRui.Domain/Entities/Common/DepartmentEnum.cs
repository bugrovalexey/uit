﻿using System.ComponentModel;

namespace bgTeam.IsRui.Domain.Entities.Common
{
    public enum DepartmentEnum
    {
        None = 0,

        [Description("Иное юридическое лицо")]
        OtherLegalEntity = 5096,
    }
}
