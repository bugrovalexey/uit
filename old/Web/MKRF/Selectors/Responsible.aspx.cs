﻿using System;
using GB;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.Data;
using GB.Controls;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Validation;

namespace GB.MKRF.Web.Selectors
{
    public partial class ResponsibleSelector : System.Web.UI.Page
    {
        protected Responsible currentResponsible = null;

        void saveResponsibleRow(Responsible responsible)
        {
            responsible.Name = hfResponsible["Name"].ToString();
            responsible.Job = GetResponsible(hfResponsible["Job"]);
            responsible.Department = UserRepository.GetCurrent().Department;
            responsible.IsActive = true;

            if (hfResponsible["Phone"] != null)
                responsible.Phone = hfResponsible["Phone"].ToString();

            if (hfResponsible["Email"] != null)
                responsible.Email = hfResponsible["Email"].ToString();
            
            RepositoryBase<Responsible>.SaveOrUpdate(responsible);
        }

        private ResponsibleJob GetResponsible(object val)
        {
            if (val is int || val is double)
            {
                return RepositoryBase<ResponsibleJob>.Get(Convert.ToInt32(hfResponsible["Job"]));
            }
            else if (val is string)
            {
                var r = new ResponsibleJob()
                {
                    Name = val as string
                };

                RepositoryBase<ResponsibleJob>.SaveOrUpdate(r);

                return r;
            }

            throw new Exception("ResponsibleSelector. Пришел неизвестный тип.");
        }

        protected void TextBox_DataBinding(object sender, EventArgs e)
        {
            if (currentResponsible == null)
                return;

            ASPxTextBox box = (ASPxTextBox)sender;
            switch (box.ID)
            {
                case "NameTextBox":
                    box.Text = currentResponsible.Name;
                    break;

                case "JobTextBox":
                    box.Text = currentResponsible.Job.Name;
                    break;

                case "PhoneTextBox":
                    box.Text = currentResponsible.Phone;
                    break;

                case "EmailTextBox":
                    box.Text = currentResponsible.Email;
                    break;
            }
        }


        protected void JobComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = RepositoryBase<ResponsibleJob>.GetAll();

            if (currentResponsible != null && currentResponsible.Job != null)
            {
                box.Value = currentResponsible.Job.Id;
            }
            else
            {
                box.Value = EntityBase.Default;
            }
        }

        #region grid
        protected override void OnPreRender(EventArgs e)
        {
            RespGrid.DataBind();

            base.OnPreRender(e);
        }
        protected void RespGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            RespGrid.DataSource = RepositoryBase<Responsible>.GetAll(x => x.Department == UserRepository.GetCurrent().Department && x.IsActive == true);
        }

        protected void RespGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "$(this).addClass(\"grid_mouseover\")");
            e.Row.Attributes.Add("onmouseout", "$(this).removeClass(\"grid_mouseover\")");
        }
        protected void RespGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            var responsible = new Responsible();
            saveResponsibleRow(responsible);
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void RespGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            Responsible responsible = RepositoryBase<Responsible>.Get((int)e.Keys[0]);
            saveResponsibleRow(responsible);
            (sender as ASPxGridView).GridCancelEdit(e);
        }
        protected void RespGrid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            currentResponsible = RepositoryBase<Responsible>.Get(e.EditingKeyValue);
        }
        #endregion

        protected void DepaptmentLabel_DataBinding(object sender, EventArgs e)
        {
            ASPxLabel box = sender as ASPxLabel;

            box.Value = UserRepository.GetCurrent().Department.Name;
        }
    }
}