﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Story.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimDeleteActStory : ClaimDeleteBaseStory<ClaimAct, ClaimDeleteActStoryContext>
    {
        private readonly IFileProviderRui _fileProviderRui;

        public ClaimDeleteActStory(
            IRepositoryRui repositoryRui,
            ISessionNHibernateFactory sessionNHibernateFactory,
            IFileProviderRui fileProviderRui)
            : base(repositoryRui, sessionNHibernateFactory)
        {
            _fileProviderRui = fileProviderRui;
        }

        protected override async Task DeleteReferenceAsync(ClaimAct entity)
        {
            if (entity.File == null)
            {
                return;
            }

            var story = new FileDeleteStory(_repository, null, _factory);

            await story.ExecuteAsync(new FileDeleteStoryContext { Id = entity.File.Id });
        }
    }
}
