﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoConclusionGetStory : IStory<AoConclusionGetStoryContext, AccountingObjectUserCommentDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;

        public AoConclusionGetStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IUserRepository userRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _userRepository = userRepository;
        }

        public AccountingObjectUserCommentDto Execute(AoConclusionGetStoryContext context)
        {
            return ExecuteAsync(context).GetAwaiter().GetResult();
        }

        public async Task<AccountingObjectUserCommentDto> ExecuteAsync(AoConclusionGetStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            var curUser = _userRepository.GetCurrent();

            curUser.CheckNull(nameof(curUser));

            var res = _repository.Get<AccountingObjectUserComment>(q => q
                .Fetch(x => x.Documents).Eager
                .Where(x => x.AccountingObject.Id == context.AoId && x.User.Id == curUser.Id && x.Status == null));

            return _mapper.Map(res, new AccountingObjectUserCommentDto());
        }
    }
}
