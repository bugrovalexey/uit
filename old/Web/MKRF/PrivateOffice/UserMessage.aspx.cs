﻿using System;
using System.Collections.Generic;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;

namespace GB.MKRF.Web.PrivateOffice
{
    public partial class UserMessage : System.Web.UI.Page
    {
        protected string GetMessageList()
        {
            User currentUser = UserRepository.GetCurrent();

            string str = string.Empty;

            IList<UsersMsg> list = RepositoryBase<UsersMsg>.GetAll(x => x.User == currentUser, null, x => x.Date);

            int i = 1;
            foreach (UsersMsg item in list)
            {
                str += AddHTMLRow(item, i);
                i++;
            }

            return str;
        }

        private string AddHTMLRow(UsersMsg item, int index)
        {
            return string.Concat(
                "<tr><td class=\"user_message_text user_message_num\">", index, ".</td><td class=\"user_message_text\">", item.Text, "</td></tr>",
                "<tr><td colspan=\"2\" class=\"user_message_date\">(от ", item.Date.ToLongDateString(), ")</td></tr>",
                "<tr><td></td><td class=\"user_message_answer\">", !string.IsNullOrEmpty(item.AdminText) ? item.AdminText : "Ответа пока нет...", "</td></tr>",
                "<tr><td colspan=\"2\" class=\"user_message_sep\"></td></tr>");
        }

        protected void AddMessageCallBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            try
            {
                UsersMsg message = new UsersMsg();
                message.User = UserRepository.GetCurrent();
                message.Text = e.Parameter;
                message.Date = DateTime.Now;
			    message.UsersMsgStatus = RepositoryBase<UsersMsgStatus>.Get((int)Undefined.value); // "новое"

                RepositoryBase<UsersMsg>.SaveOrUpdate(message);

                e.Result = "true";
            }
            catch (Exception exp)
            {
                e.Result = exp.Message;
            }
        }
    }
}