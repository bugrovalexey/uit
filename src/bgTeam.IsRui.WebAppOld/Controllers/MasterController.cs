﻿using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using bgTeam.IsRui.WebApp.Helpers;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public class MasterController : ControllerBase
    {
        [HttpPost]
        public ActionResult GetNextStepForAO(int id, MasterItemEnum step)
        {
            var ao = new RepositoryDc<AccountingObject>().Get(id);

            var handler = Singleton<MasterEditManager>.Instance.GetHandler(ao);

            MasterItem master = handler.GetItem(step);

            return Json(Url.Action(master.Next, "AccountingObjects", RouteHelper.Merge(new { id }, master.NextParams)));
        }
    }
}