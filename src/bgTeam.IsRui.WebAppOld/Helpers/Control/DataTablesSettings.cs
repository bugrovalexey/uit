﻿using System.Collections.Generic;

namespace bgTeam.IsRui.WebApp.Helpers.Control
{
    /// <summary>
    /// Класс для описания свойств нетипизированной таблицы DataTables
    /// </summary>
    public class DataTablesSettings : DataTableSettingsBase
    {
        public DataTablesSettings()
            : base()
        {
        }

        public DataTablesSettings(string keyId)
            : base(keyId)
        {
        }

        public override IList<DataTablesColumn> GetColumnsList()
        {
            return ColumnsList;
        }

        public DataTablesSettings AddColumn(DataTablesColumn column)
        {
            ColumnsList.Add(column);
            return this;
        }

        public DataTablesSettings AddColumn(string fieldName, string caption)
        {
            ColumnsList.Add(new DataTablesColumn(fieldName, caption));
            return this;
        }

        public DataTablesSettings AddColumn(string fieldName, string caption, bool searchable = false, bool sortable = false, bool selectable = false)
        {
            var column = new DataTablesColumn(fieldName, caption)
                .Search(searchable)
                .Sort(sortable)
                .Select(selectable);
            ColumnsList.Add(column);
            return this;
        }

        public DataTablesSettings EditPage(string url)
        {
            ItemEditPage = url; return this;
        }
    }

    public abstract class DataTableSettingsBase
    {
        public const string DefaultFooter = "default";

        protected List<DataTablesColumn> ColumnsList;

        /// <summary>
        /// Наименование колонки идентификатора (используется при создании аттрибута data-id и формирования страницы редактирования)
        /// По умолчанию - 'Id'
        /// </summary>
        public string KeyField { get; private set; }

        /// <summary>
        /// Пользовательский аттрабут записи (используется при создании аттрибута data-[fieldName])
        /// По умолчанию - ''
        /// </summary>
        public string CustomAttributeField { get; set; }

        /// <summary>
        /// Шаблон тэга &lt;tfoot&gt;
        /// </summary>
        public string FooterTemplateText { get; protected set; }

        /// <summary>
        /// Ссылка на страницу редактирования записей таблицы
        /// Причем идентификатор записи не требуется (подставляется в htmlHelper'e автоматически для каждой записи)
        /// Пример ссылки: /ms/User/Edit
        /// </summary>
        public string ItemEditPage { get; set; }

        /// <summary>
        /// Строка, содержащая javascript код, описывающая функции обертки dataTables
        /// Планируется использование в инливидуальных случаях
        /// По умолчанию в htmlHelper'e происходит стандартная обёртка (см. класс HtmlHelpers метод JSWrapTable)
        /// </summary>
        public bool IsWrapTable { get; set; }

        public string WrapScript { get; set; }

        /// <summary>
        /// В таблице будет дерево
        /// </summary>
        public bool IsTreeTableFlag { get; protected set; }

        public DataTableSettingsBase()
            : this("Id")
        {
        }

        public DataTableSettingsBase(string keyId)
        {
            KeyField = keyId;
            IsWrapTable = true;
            CustomAttributeField = string.Empty;

            ColumnsList = new List<DataTablesColumn>();
        }

        public abstract IList<DataTablesColumn> GetColumnsList();
    }
}