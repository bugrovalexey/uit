﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoContractsDeleteContractStory : IStory<AoContractsDeleteContractStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;

        public AoContractsDeleteContractStory(ISessionNHibernateFactory factory)
        {
            _factory = factory;
        }

        public bool Execute(AoContractsDeleteContractStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(AoContractsDeleteContractStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            bool result = false;

            using (var session = _factory.OpenSession())
            {
                await session.DeleteAsync<AccountingObjectContract>(context.Id.Value);

                result = true;
            }

                return result;
        }
    }
}
