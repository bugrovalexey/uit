﻿using System;
using System.Data;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Web.Services.Fap;

namespace GB.MKRF.Web.Selectors
{
    public partial class Faps : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            SelectorFapGrid.DataBind();

            base.OnPreRender(e);
        }

        protected void SelectorFapGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var service = new FapServiceSoapClient();
            DataTable dt = service.GetAllSolution();
            (sender as ASPxGridView).DataSource = dt;
        }

        protected void SelectorFapGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "$(this).addClass(\"grid_mouseover\")");
            e.Row.Attributes.Add("onmouseout", "$(this).removeClass(\"grid_mouseover\")");
        }
    }
}