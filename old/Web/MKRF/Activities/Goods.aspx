﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Activities/EditCard.master" AutoEventWireup="true" CodeBehind="Goods.aspx.cs" Inherits="GB.MKRF.Web.Activities.Goods" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function GridSave() {
            hf.Set('Name', NameTextBox.GetText());

            hf.Set('OKPD', OKPDComboBox.GetValue());
            hf.Set('Category', CategoryComboBox.GetValue());
            hf.Set('ExpenseDirection', ExpenseDirectionComboBox.GetValue());
            hf.Set('ExpenseType', ExpenseTypeComboBox.GetValue());
            hf.Set('KOSGU', ExpenditureComboBox.GetValue());
            hf.Set('Year', YearComboBox.GetValue());

            hf.Set('Quantity', QuantityTextBox.GetText());
            hf.Set('Duration', DurationTextBox.GetText());
            hf.Set('Cost', CostTextBox.GetText());
        }

        function CharacteristicsGridSave() {
            hfCharacteristic.Set('Name', CharNameTextBox.GetText());
            hfCharacteristic.Set('Value', CharValueTextBox.GetText());
            hfCharacteristic.Set('Unit', UnitComboBox.GetValue());
        }

        function validateComboBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && e.value != 0;
        }

        function validateTextBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && e.value != '0' && e.value != '0.00' && e.value != '0,00';
        }
    </script>
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
    <dxhf:ASPxHiddenField runat="server" ID="hfCharacteristic" ClientInstanceName="hfCharacteristic" />
    <dxe:ASPxButton ID="addButton" runat="server" ClientInstanceName="addButton" AutoPostBack="false"
        Text="Добавить" CssClass="topButton fleft">
        <ClientSideEvents Click="function(s,e) { Grid.AddNewRow(); }" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="Grid" KeyFieldName="Id" Width="100%" AutoGenerateColumns="False"
        ClientInstanceName="Grid" OnBeforePerformDataSelect="Grid_BeforePerformDataSelect"
        OnStartRowEditing="Grid_StartRowEditing" OnRowInserting="Grid_RowInserting"
        OnRowUpdating="Grid_RowUpdating" OnRowDeleting="Grid_RowDeleting">
        <Settings ShowFilterRow="true" />
        <SettingsDetail AllowOnlyOneMasterRowExpanded="true" ShowDetailButtons="true" ShowDetailRow="true" />
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="true" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Наименование">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="OKPD" Caption="Код ОКПД" />
            <dxwgv:GridViewDataTextColumn FieldName="Category" Caption="Группа товаров" />
            <dxwgv:GridViewDataTextColumn FieldName="ExpenseDirection" Caption="Вид затрат" />
            <dxwgv:GridViewDataTextColumn FieldName="ExpenseType" Caption="Тип затрат" />
            <dxwgv:GridViewDataTextColumn FieldName="ExpenditureItem" Caption="КОСГУ" />
            <dxwgv:GridViewDataComboBoxColumn FieldName="Year" Caption="Год" Name="yearColumn">
                <PropertiesComboBox TextField="Value" ValueField="Key" ValueType="System.Int32" />
            </dxwgv:GridViewDataComboBoxColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Quantity" Caption="Количество" />
            <dxwgv:GridViewDataTextColumn FieldName="Duration" Caption="Срок поставки, дней" />
            <dxwgv:GridViewDataTextColumn FieldName="Cost" Caption="Цена за единицу, руб." />
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc required">Наименование
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="NameTextBox" runat="server" ClientInstanceName="NameTextBox" OnDataBinding="NameTextBox_DataBinding"
                                ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Код ОКПД
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="OKPDComboBox" runat="server" ClientInstanceName="OKPDComboBox"
                                TextField="CodeName" ValueField="Id" ValueType="System.Int32" OnDataBinding="OKPDComboBox_DataBinding">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Группа товаров
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="CategoryComboBox" runat="server" ClientInstanceName="CategoryComboBox"
                                OnDataBinding="CategoryComboBox_DataBinding"
                                TextField="Name" ValueField="Id" ValueType="System.Int32">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Вид затрат
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ExpenseDirectionComboBox" runat="server" ClientInstanceName="ExpenseDirectionComboBox" OnDataBinding="ExpenseDirectionComboBox_DataBinding"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                TextField="Name" ValueField="Id" ValueType="System.Int32">
                                <ClientSideEvents Validation="function(s,e) { validateComboBox(s, e); }" SelectedIndexChanged="function(s, e) { ExpenseTypeComboBox.PerformCallback(s.GetValue()); }" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Тип затрат
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ExpenseTypeComboBox" runat="server" ClientInstanceName="ExpenseTypeComboBox"
                                OnDataBinding="ExpenseTypeComboBox_DataBinding" OnCallback="ExpenseTypeComboBox_Callback"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                TextField="Name" ValueField="Id" ValueType="System.Int32">
                                <ClientSideEvents Validation="function(s,e) { validateComboBox(s, e); }" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">КОСГУ
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ExpenditureComboBox" runat="server" ClientInstanceName="ExpenditureComboBox"
                                OnDataBinding="ExpenditureComboBox_DataBinding"
                                TextField="CodeName" ValueField="Id" ValueType="System.Int32">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Год
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="YearComboBox" runat="server" ClientInstanceName="YearComboBox"
                                OnDataBinding="YearComboBox_DataBinding"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                TextField="Value" ValueField="Key" ValueType="System.Int32">
                                <ClientSideEvents Validation="function(s,e) { validateComboBox(s, e); }" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="form_edit_desc required">Количество
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="QuantityTextBox" runat="server" ClientInstanceName="QuantityTextBox"
                                MaskSettings-Mask="<0..999999999>" OnDataBinding="QuantityTextBox_DataBinding"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ClientSideEvents Validation="function(s,e) { validateTextBox(s, e); }" />
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Срок поставки, дней
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="DurationTextBox" runat="server" ClientInstanceName="DurationTextBox"
                                MaskSettings-Mask="<0..999999999g>" OnDataBinding="DurationTextBox_DataBinding">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Цена за единицу, руб.
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="CostTextBox" runat="server" ClientInstanceName="CostTextBox"
                                MaskSettings-Mask="<0..999999999g>.<00..99>"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                OnDataBinding="CostTextBox_DataBinding">
                                <ClientSideEvents Validation="function(s,e) { validateTextBox(s, e); }" />
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok"><a onclick="Grid.UpdateEdit();">Сохранить</a></span>
                            <span class="cancel"><a onclick="Grid.CancelEdit();">Отмена</a></span>
                        </td>
                    </tr>
                </table>
            </EditForm>
            <DetailRow>
                <dxe:ASPxHyperLink runat="server" ID="addTypeLink" ClientInstanceName="addTypeLink"
                    Text="Добавить характеристику"
                    Style="margin-bottom: 5px; margin-left: 10px; cursor: pointer;">
                    <ClientSideEvents Click="function(s,e) { CharacteristicsGrid.AddNewRow(); }" />
                </dxe:ASPxHyperLink>
                <dxwgv:ASPxGridView runat="server" ID="CharacteristicsGrid" KeyFieldName="Id" Width="100%" AutoGenerateColumns="False"
                    ClientInstanceName="CharacteristicsGrid"
                    OnInit="CharacteristicsGrid_Init" OnBeforePerformDataSelect="CharacteristicsGrid_BeforePerformDataSelect"
                    OnStartRowEditing="CharacteristicsGrid_StartRowEditing" OnRowInserting="CharacteristicsGrid_RowInserting"
                    OnRowUpdating="CharacteristicsGrid_RowUpdating" OnRowDeleting="CharacteristicsGrid_RowDeleting">
                    <Columns>
                        <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False" AllowDragDrop="False">
                            <EditButton Visible="True" />
                            <DeleteButton Visible="true" />
                        </dxwgv:GridViewCommandColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Наименование" />
                        <dxwgv:GridViewDataTextColumn FieldName="Unit.Name" Caption="Единица измерения" />
                        <dxwgv:GridViewDataTextColumn FieldName="Value" Caption="Значение" />
                    </Columns>
                    <Templates>
                        <EditForm>
                            <table class="form_edit">
                                <tr>
                                    <td class="form_edit_desc required">Наименование характеристики
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxTextBox ID="CharNameTextBox" runat="server" ClientInstanceName="CharNameTextBox"
                                            OnDataBinding="CharNameTextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                            <ValidationSettings ErrorDisplayMode="Text">
                                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                            </ValidationSettings>
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="form_edit_desc required">Единица измерения
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxComboBox ID="UnitComboBox" runat="server" ClientInstanceName="UnitComboBox"
                                            TextField="Name" ValueField="Id" ValueType="System.Int32"
                                            OnDataBinding="UnitComboBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                            <ValidationSettings ErrorDisplayMode="Text" />
                                            <ClientSideEvents Validation="function(s,e){ validateComboBox(s, e); }" />
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="form_edit_desc">Значение
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxTextBox runat="server" ID="CharValueTextBox" ClientInstanceName="CharValueTextBox"
                                            MaskSettings-Mask="<0..999999999>" OnDataBinding="CharValueTextBox_DataBinding" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="form_edit_buttons">
                                        <span class="ok"><a onclick="CharacteristicsGrid.UpdateEdit();">Сохранить</a></span>
                                        <span class="cancel"><a onclick="CharacteristicsGrid.CancelEdit();">Отмена</a></span>
                                    </td>
                                </tr>
                            </table>
                        </EditForm>
                    </Templates>
                    <ClientSideEvents BeginCallback="function(s,e) { if (e.command=='UPDATEEDIT') CharacteristicsGridSave(); }" />
                </dxwgv:ASPxGridView>
            </DetailRow>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e) { if (e.command=='UPDATEEDIT') GridSave(); }" />
    </dxwgv:ASPxGridView>
</asp:Content>
