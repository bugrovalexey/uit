﻿namespace bgTeam.IsRui.Domain.Entities.Workflow
{
    /// <summary>Группы статусов</summary>
    public enum StatusGroupEnum
    {
        Node = 0,

        /// <summary>
        /// Статусы мероприятий
        /// </summary>
        PlansActivity = 1,

        /// <summary>
        /// Статусы экспертизы по мероприятию
        /// </summary>
        ActivityExpertise = 2,

        /// <summary>
        /// Статусы объекта учета
        /// </summary>
        AccountingObject = 3,

        /// <summary>
        /// Статусы заявок
        /// </summary>
        Claims = 4
    }
}