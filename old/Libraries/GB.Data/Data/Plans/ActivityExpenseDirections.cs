﻿using GB.Data.Directories;

namespace GB.Data.Plans
{
    /// <summary>Группа направлений расходов по мероприятию Формы сведений</summary>
    public class ActivityExpenseDirections : EntityBase
    {
        protected ActivityExpenseDirections()
        {
        }

        public ActivityExpenseDirections(PlansActivity Activity)
        {
            this.Activity = Activity;
        }

        /// <summary>Направление расходов</summary>
        public virtual ExpenseDirection ExpenseDirection { get; set; }

        /// <summary>Мероприятие</summary>
        public virtual PlansActivity Activity { get; protected set; }

        /// <summary>Основной объем Очередной год</summary>
        public virtual decimal ExpenseVolY0 { get; set; }

        /// <summary>Основной объем Первый год</summary>
        public virtual decimal ExpenseVolY1 { get; set; }

        /// <summary>Основной объем Второй год</summary>
        public virtual decimal ExpenseVolY2 { get; set; }

        /// <summary>Дополнительная потребность Очередной год</summary>
        public virtual decimal ExpenseAddVolY0 { get; set; }

        /// <summary>Дополнительная потребность Первый год</summary>
        public virtual decimal ExpenseAddVolY1 { get; set; }

        /// <summary>Дополнительная потребность Второй год</summary>
        public virtual decimal ExpenseAddVolY2 { get; set; }
    }
}