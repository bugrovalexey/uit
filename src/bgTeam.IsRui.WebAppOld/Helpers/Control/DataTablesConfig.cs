﻿using System.Text;

namespace bgTeam.IsRui.WebApp.Helpers.Control
{
    public class DataTablesConfig
    {
        private string onMouseOver;
        private string onMouseOut;
        private string onRowClick;
        private string onMultiSelectRowClick;
        private string footerCallback;

        private bool multiSelect;
        private bool pagingPanel;

        private bool showFilter;
        private int? rowCount;

        private string toolbar;

        public DataTablesConfig()
        {
            pagingPanel = true;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append('{');

            if (!string.IsNullOrWhiteSpace(footerCallback))
                sb.AppendFormat("\"footerCallback\" : function(tfoot, data, start, end, display) {{ {0}; }}, ", footerCallback);

            if (!string.IsNullOrWhiteSpace(onMouseOver))
                sb.AppendFormat("OnMouseOver : function(nRow) {{ {0}; }}, ", onMouseOver);

            if (!string.IsNullOrWhiteSpace(onMouseOut))
                sb.AppendFormat("OnMouseOut : function(nRow) {{ {0}; }}, ", onMouseOut);

            if (!string.IsNullOrWhiteSpace(onRowClick))
                sb.AppendFormat("OnRowClick : function(nRow) {{ {0}; }}, ", onRowClick);

            if (!string.IsNullOrWhiteSpace(onMultiSelectRowClick))
                sb.AppendFormat("OnMultiSelectRowClick : function(nRow) {{ {0}; }}, ", onMultiSelectRowClick);

            if (!string.IsNullOrWhiteSpace(toolbar))
                sb.AppendFormat("toolbar : '#{0}', ", toolbar);
            if (rowCount.HasValue)
                sb.AppendFormat("pageLength : {0}, ", rowCount);

            sb.AppendFormat("multiSelect : {0}, ", multiSelect.ToString().ToLower());
            sb.AppendFormat("pagingPanel : {0}, ", pagingPanel.ToString().ToLower());
            sb.AppendFormat("showDefaultFilter : {0},", showFilter.ToString().ToLower());

            sb.Append('}');

            return sb.ToString();
        }

        public DataTablesConfig FooterCallback(string value)
        {
            footerCallback = value; return this;
        }

        public DataTablesConfig OnMouseOver(string value)
        {
            onMouseOver = value; return this;
        }

        public DataTablesConfig OnMouseOut(string value)
        {
            onMouseOut = value; return this;
        }

        public DataTablesConfig OnRowClick(string value)
        {
            onRowClick = value; return this;
        }

        public DataTablesConfig OnRowClick(string value, bool readOnly)
        {
            if (readOnly == false)
                onRowClick = value;

            return this;
        }

        public DataTablesConfig OnMultiSelectRowClick(string value)
        {
            onMultiSelectRowClick = value; return this;
        }

        public DataTablesConfig MultiSelect(bool value)
        {
            multiSelect = value; return this;
        }

        /// <summary>Показывать панельку с информацией о кол-ве записей и навигацией по страницам.</summary>
        /// <remarks>ex ShowFooter</remarks>
        public DataTablesConfig PagingPanel(bool value)
        {
            pagingPanel = value; return this;
        }

        public DataTablesConfig ShowFilter(bool value)
        {
            showFilter = value; return this;
        }

        public DataTablesConfig RowCount(int value)
        {
            rowCount = value; return this;
        }

        public DataTablesConfig Toolbar(string value)
        {
            toolbar = value; return this;
        }
    }
}