﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="EditCard.Master" CodeBehind="Characteristics.aspx.cs" Inherits="GB.MKRF.Web.AccountingObjects.Characteristics" %>

<%@ Import Namespace="GB.MKRF.Entities.AccountingObjects" %>
<%@ Import Namespace="GB.Extentions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">

        function validateComboBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && e.value != 0;
        }

        function GridSave(e) {
            hf.Set('Name', txtName.GetValue());
            hf.Set('TypeId', cbxType.GetValue());
            hf.Set('UnitId', cbxUnit.GetValue());
            hf.Set('Fact', txtFact.GetValue());
            hf.Set('Date', deDate.GetValue());
            hf.Set('Period', txtPeriod.GetValue());
        }
      </script>

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />

    <p class="header_grid">
        Характеристики
   
   
   
   
   
    </p>

    <dxe:ASPxButton ID="bntAddCharacteristic" runat="server" ClientInstanceName="bntAddCharacteristic" CssClass="fleft topButton"
        AutoPostBack="false" Text="Добавить">
        <ClientSideEvents Click="function(s,e) {gridCharacteristics.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="gridCharacteristics" Width="100%" ClientInstanceName="gridCharacteristics"
        KeyFieldName="Id" ViewStateMode="Disabled" AutoGenerateColumns="False"
        OnBeforePerformDataSelect="gridCharacteristics_OnBeforePerformDataSelect"
        OnRowInserting="gridCharacteristics_OnRowInserting"
        OnRowUpdating="gridCharacteristics_OnRowUpdating"
        OnStartRowEditing="gridCharacteristics_OnStartRowEditing"
        OnRowDeleting="gridCharacteristics_OnRowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" />
            <dxwgv:GridViewDataTextColumn Caption="Тип" FieldName="Type.Name" />
            <dxwgv:GridViewDataTextColumn Caption="Единица измерения" FieldName="Unit.FullName"/>
            <dxwgv:GridViewDataTextColumn Caption="Номинальное значение">
                <DataItemTemplate>
                    <%#GetSumNorm(Eval("Values"))%>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Максимальное значение">
                <DataItemTemplate>
                    <%#GetSumMax(Eval("Values"))%>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Фактическое значение" FieldName="Fact" />
            <dxwgv:GridViewDataTextColumn Caption="Дата" FieldName="Date">
                <PropertiesTextEdit DisplayFormatString="dd.MM.yyyy"/>                
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Период " FieldName="Period" />
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc required">Наименование:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtName" ClientInstanceName="txtName"
                                runat="server" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                OnDataBinding="txtName_OnDataBinding">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Тип:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="cbxType" runat="server" ClientInstanceName="cbxType"
                                TextField="Name" ValueField="Id" ValueType="System.Int32" 
                                OnDataBinding="cbxType_OnDataBinding" EnableSynchronization="False" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                                </ValidationSettings>
                                <ClientSideEvents Validation="function(s,e){ validateComboBox(s, e); }" />
                            </dxe:ASPxComboBox>

                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Единица измерения:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="cbxUnit" runat="server" ClientInstanceName="cbxUnit"
                                TextField="FullName" ValueField="Id" ValueType="System.Int32" 
                                OnDataBinding="cbxUnit_OnDataBinding" EnableSynchronization="False" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                                </ValidationSettings>
                                <ClientSideEvents Validation="function(s,e){ validateComboBox(s, e); }" />
                            </dxe:ASPxComboBox>

                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Фактическое значение:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtFact" ClientInstanceName="txtFact" runat="server"
                                MaskSettings-Mask="<0..999999999>" OnDataBinding="txtFact_OnDataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Дата:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="deDate" ClientInstanceName="deDate"
                                OnDataBinding="deDate_OnDataBinding">
                            </dxe:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Период:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="txtPeriod" ClientInstanceName="txtPeriod"
                                runat="server"
                                OnDataBinding="txtPeriod_OnDataBinding">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents
            BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') {GridSave(e);} }" />
    </dxwgv:ASPxGridView>
</asp:Content>

