﻿using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.Extensions;
using System.ComponentModel;
using System.Linq;
using bgTeam.IsRui.Common.Helpers;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    public class TableAccessToEntity : NotificationBaseViewModel
    {
        private const string RoleCaption = "Роль";
        private const string StatusCaption = "Статус";
        private const string NoteCaption = "Примечание";
        private const string AccessCaption = "Доступ";

        public TableAccessToEntity(AccessToEntity value)
        {
#if DEBUG
            EntityType = value.EntityType;
#endif
            Id = value.Id;
            AccessMask = value.AccessMask;
            Role = value.Role.Return(x => x.Name, default(string));
            Status = value.Status.Return(x => x.Name, default(string));
            Note = value.Note;

            if (AccessMask == AccessActionEnum.None || AccessMask == AccessActionEnum.All)
            {
                AccessText = EnumHelper.GetDescription(AccessMask);
            }
            else
            {
                var access = EnumHelper.ToDictionary<AccessActionEnum>().Where(a =>
                    (a.Key != AccessActionEnum.None && a.Key != AccessActionEnum.All) && (AccessMask & a.Key) == a.Key);
                AccessText = string.Join(", ", access.Select(a => a.Value));
            }
        }

        public int Id { get; private set; }

        public AccessActionEnum AccessMask { get; private set; }

        [DisplayName(AccessCaption)]
        public string AccessText { get; private set; }

        [DisplayName(RoleCaption)]
        public string Role { get; private set; }

        [DisplayName(StatusCaption)]
        public string Status { get; private set; }

        [DisplayName(NoteCaption)]
        public string Note { get; private set; }

        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("AccessRightDelete", "Admin", new { id = Id }, "Grid"); }
        }

#if DEBUG

        public EntityType EntityType { get; set; }

#endif
    }
}