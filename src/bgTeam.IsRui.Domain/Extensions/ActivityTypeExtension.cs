﻿using bgTeam.IsRui.Domain.Entities.Plans;

namespace bgTeam.IsRui.Domain.Extensions
{
    public static class ActivityTypeExtension
    {
        public static bool IsEq(this ActivityTypeEnum e, ActivityType o)
        {
            return o.Id == (int)e;
        }

        public static bool IsEq(this ActivityType o, ActivityTypeEnum e)
        {
            return o.Id == (int)e;
        }
    }
}