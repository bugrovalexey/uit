﻿using Uviri.MKRF.Entities.Prognosis;

namespace Uviri.MKRF.Mapping.Prognosis
{
    public class PriorityProjectsInfoMap : ClassMapBase<PriorityProjectsInfo>
    {
        public PriorityProjectsInfoMap()
            : base("MF6")
        {
//            Table("w_");

//            Id(x => x.Id, "MF6_ID").GeneratedBy.Custom(typeof(IdGenerator));

            


//            string field =
//                        @"@MF6_Num = ?
//                         ,@MF6_32 = ?
//                         ,@MF6_33 = ?
//                         ,@MF6_34 = ?
//                         ,@MF6_35 = ?
//                         ,@MF6_36 = ?
//                         ,@MF6_37 = ?
//                         ,@MF6_38 = ?
//                         ,@MF6_39 = ?
//                         ,@MF6_40 = ?
//                         ,@MF6_41 = ?
//                         ,@MF6_42 = ?
//                         ,@MF6_43 = ?
//                         ,@MF6_44_Cur = ?
//                         ,@MF6_44_Next = ?
//                         ,@MF6_44_Cur_Budg = ?
//                         ,@MF6_44_Next_Budg = ?
//                         ,@MF6_44_Cur_Budg_Cap = ?
//                         ,@MF6_44_Next_Budg_Cap = ?
//                         ,@MF6_44_Cur_Budg_Science = ?
//                         ,@MF6_44_Next_Budg_Science = ?
//                         ,@MF6_44_Cur_Budg_Other = ?
//                         ,@MF6_44_Next_Budg_Other = ?
//                         ,@MF6_44_Cur_VB = ?
//                         ,@MF6_44_Next_VB = ?
//                         ,@MF6_44_Cur_VB_Cred = ?
//                         ,@MF6_44_Next_VB_Cred = ?
//                         ,@MF6_44_Cur_VB_Other = ?
//                         ,@MF6_44_Next_VB_Other = ?
//                         ,@MF6_45_Cur = ?
//                         ,@MF6_45_Next = ?
//                         ,@MF6_45_Cur_Budg = ?
//                         ,@MF6_45_Next_Budg = ?
//                         ,@MF6_45_Cur_Budg_Cap = ?
//                         ,@MF6_45_Next_Budg_Cap = ?
//                         ,@MF6_45_Cur_Budg_Science = ?
//                         ,@MF6_45_Next_Budg_Science = ?
//                         ,@MF6_45_Cur_Budg_Other = ?
//                         ,@MF6_45_Next_Budg_Other = ?
//                         ,@MF6_45_Cur_VB = ?
//                         ,@MF6_45_Next_VB = ?
//                         ,@MF6_45_Cur_VB_Cred = ?
//                         ,@MF6_45_Next_VB_Cred = ?
//                         ,@MF6_45_Cur_VB_Other = ?
//                         ,@MF6_45_Next_VB_Other = ?
//                         ,@MF6_46_Cur = ?
//                         ,@MF6_46_Next = ?
//                         ,@MF6_46_Cur_Budg = ?
//                         ,@MF6_46_Next_Budg = ?
//                         ,@MF6_46_Cur_Budg_Cap = ?
//                         ,@MF6_46_Next_Budg_Cap = ?
//                         ,@MF6_46_Cur_Budg_Science = ?
//                         ,@MF6_46_Next_Budg_Science = ?
//                         ,@MF6_46_Cur_Budg_Other = ?
//                         ,@MF6_46_Next_Budg_Other = ?
//                         ,@MF6_46_Cur_VB = ?
//                         ,@MF6_46_Next_VB = ?
//                         ,@MF6_46_Cur_VB_Cred = ?
//                         ,@MF6_46_Next_VB_Cred = ?
//                         ,@MF6_46_Cur_VB_Other = ?
//                         ,@MF6_46_Next_VB_Other = ?
//                         ,@MF6_47 = ?
//                         ,@MF6_48 = ?
//                         ,@MF6_49 = ?
//                         ,@Mon_Form_ID = ?
//                         ,@MF6_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_MF6_Insert " + field).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF6_Update " + field).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF6_Delete
//						  @MF6_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "MF6_Num");
            Map(x => x.Name, "MF6_32");
            Map(x => x.NameSmall, "MF6_33");
            Map(x => x.PointingProject, "MF6_34");
            Map(x => x.Purpose, "MF6_35");
            Map(x => x.ImplementationPeriod, "MF6_36");
            Map(x => x.Strategy, "MF6_37");
            Map(x => x.Structure, "MF6_38");
            Map(x => x.AchievedResults, "MF6_39");
            Map(x => x.TechInfo, "MF6_40");
            Map(x => x.Developer, "MF6_41");
            Map(x => x.RiskFactors, "MF6_42");
            Map(x => x.Works, "MF6_43");

            Map(x => x.FinanceFact_Curt, "MF6_44_Cur");
            Map(x => x.FinanceFact_Next, "MF6_44_Next");
            Map(x => x.FinanceFactB_Curt, "MF6_44_Cur_Budg");
            Map(x => x.FinanceFactB_Next, "MF6_44_Next_Budg");
            Map(x => x.FinanceFactBCapital_Curt, "MF6_44_Cur_Budg_Cap");
            Map(x => x.FinanceFactBCapital_Next, "MF6_44_Next_Budg_Cap");
            Map(x => x.FinanceFactBScience_Curt, "MF6_44_Cur_Budg_Science");
            Map(x => x.FinanceFactBScience_Next, "MF6_44_Next_Budg_Science");
            Map(x => x.FinanceFactBOther_Curt, "MF6_44_Cur_Budg_Other");
            Map(x => x.FinanceFactBOther_Next, "MF6_44_Next_Budg_Other");
            Map(x => x.FinanceFactVBudget_Curt, "MF6_44_Cur_VB");
            Map(x => x.FinanceFactVBudget_Next, "MF6_44_Next_VB");
            Map(x => x.FinanceFactVBCredit_Curt, "MF6_44_Cur_VB_Cred");
            Map(x => x.FinanceFactVBCredit_Next, "MF6_44_Next_VB_Cred");
            Map(x => x.FinanceFactVBOther_Curt, "MF6_44_Cur_VB_Other");
            Map(x => x.FinanceFactVBOther_Next, "MF6_44_Next_VB_Other");

            Map(x => x.FinanceNeed_Curt, "MF6_45_Cur");
            Map(x => x.FinanceNeed_Next, "MF6_45_Next");
            Map(x => x.FinanceNeedB_Curt, "MF6_45_Cur_Budg");
            Map(x => x.FinanceNeedB_Next, "MF6_45_Next_Budg");
            Map(x => x.FinanceNeedBCapital_Curt, "MF6_45_Cur_Budg_Cap");
            Map(x => x.FinanceNeedBCapital_Next, "MF6_45_Next_Budg_Cap");
            Map(x => x.FinanceNeedBScience_Curt, "MF6_45_Cur_Budg_Science");
            Map(x => x.FinanceNeedBScience_Next, "MF6_45_Next_Budg_Science");
            Map(x => x.FinanceNeedBOther_Curt, "MF6_45_Cur_Budg_Other");
            Map(x => x.FinanceNeedBOther_Next, "MF6_45_Next_Budg_Other");
            Map(x => x.FinanceNeedVBudget_Curt, "MF6_45_Cur_VB");
            Map(x => x.FinanceNeedVBudget_Next, "MF6_45_Next_VB");
            Map(x => x.FinanceNeedVBCredit_Curt, "MF6_45_Cur_VB_Cred");
            Map(x => x.FinanceNeedVBCredit_Next, "MF6_45_Next_VB_Cred");
            Map(x => x.FinanceNeedVBOther_Curt, "MF6_45_Cur_VB_Other");
            Map(x => x.FinanceNeedVBOther_Next, "MF6_45_Next_VB_Other");

            Map(x => x.Finance_Curt, "MF6_46_Cur");
            Map(x => x.Finance_Next, "MF6_46_Next");
            Map(x => x.FinanceB_Curt, "MF6_46_Cur_Budg");
            Map(x => x.FinanceB_Next, "MF6_46_Next_Budg");
            Map(x => x.FinanceBCapital_Curt, "MF6_46_Cur_Budg_Cap");
            Map(x => x.FinanceBCapital_Next, "MF6_46_Next_Budg_Cap");
            Map(x => x.FinanceBScience_Curt, "MF6_46_Cur_Budg_Science");
            Map(x => x.FinanceBScience_Next, "MF6_46_Next_Budg_Science");
            Map(x => x.FinanceBOther_Curt, "MF6_46_Cur_Budg_Other");
            Map(x => x.FinanceBOther_Next, "MF6_46_Next_Budg_Other");
            Map(x => x.FinanceVBudget_Curt, "MF6_46_Cur_VB");
            Map(x => x.FinanceVBudget_Next, "MF6_46_Next_VB");
            Map(x => x.FinanceVBCredit_Curt, "MF6_46_Cur_VB_Cred");
            Map(x => x.FinanceVBCredit_Next, "MF6_46_Next_VB_Cred");
            Map(x => x.FinanceVBOther_Curt, "MF6_46_Cur_VB_Other");
            Map(x => x.FinanceVBOther_Next, "MF6_46_Next_VB_Other");

            Map(x => x.PossibleDamage, "MF6_47");
            Map(x => x.Responsible, "MF6_48");
            Map(x => x.RelationshipInfo, "MF6_49");

            References<Prognos>(x => x.Prognos, "Mon_Form_ID").Not.Nullable();
        }
    }
}
