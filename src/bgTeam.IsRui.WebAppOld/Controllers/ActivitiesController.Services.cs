﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Activities;
using bgTeam.IsRui.Domain.Entities.Plans;
using System.Web.Mvc;
using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.Controllers
{
    partial class ActivitiesController
    {
        private IService<PlansActivityService, PlansActivityServiceArgs> servicesController;

        private IService<PlansActivityService, PlansActivityServiceArgs> ServicesController
        {
            get { return servicesController ?? (servicesController = new ActivityServicesService()); }
        }

        public ActionResult ServicesSingle(int Id)
        {
            return View(new ServicesViewModel(_statusRepository, Id));
        }

        public ActionResult ServiceEdit(int? id, int ownerId)
        {
            return View(new ServiceEditViewModel(_statusRepository, ownerId, id));
        }

        [HttpPost]
        public ActionResult ServiceEdit(PlansActivityServiceArgs args)
        {
            return NotificationHelper.NotificationCreateEdit<TableService>(() =>
            {
                bool isNew = args.Id == 0;
                var result = isNew
                    ? ServicesController.Create(args)
                    : ServicesController.Update(args);

                return new TableService(result)
                {
                    success = true,
                    message = isNew ? "Запись успешно создана" : NotificationHelper.AddTextSuccess
                };
            });
        }

        [HttpPost]
        public ActionResult ServiceDelete(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => ServicesController.Delete(new PlansActivityServiceArgs { Id = id }));
        }
    }
}