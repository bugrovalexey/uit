﻿namespace bgTeam.IsRui.Domain.Dto
{
    public class ClaimJustificationDto
    {
        public int ClaimId { get; set; }

        public string Justification { get; set; }
    }
}
