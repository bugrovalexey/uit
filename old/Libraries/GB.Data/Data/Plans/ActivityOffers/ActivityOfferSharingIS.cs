﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans.ActivityOffers
{
    public class ActivityOfferSharingIS : EntityBase
    {
        /// <summary>Ведомство</summary>
        public virtual Department Department { get; set; }

        /// <summary>ФГИС</summary>
        public virtual FgisInfo FGIS { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Мероприятие плана
        /// </summary>
        public virtual ActivityOffer Offer { get; set; }
    }
}
