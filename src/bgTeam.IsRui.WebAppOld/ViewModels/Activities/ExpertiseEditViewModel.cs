﻿using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.Extensions;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Common.Helpers;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    [MetadataType(typeof(ExpertiseEditViewModel_Valid))]
    public class ExpertiseEditViewModel
    {
        private const string UserCaption = "Эксперт";
        private const string TitleCaption = "Роль эксперта";

        public int ConclusionId { get; set; }

        [DisplayName(UserCaption)]
        public IList<IEntityName> User { get; private set; }

        [DisplayName(TitleCaption)]
        public int Role { get; set; }

        public string ExpertList { get; set; }

        public System.Web.Mvc.SelectList RoleList { get; set; }

        public ExpertiseEditViewModel(int conclusionId, int? id)
        {
            ConclusionId = conclusionId;

            var ae = new RepositoryDc<ActivityExpert>().Get(id);

            if (ae != null)
            {
                User = ae.Expert.Return(x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.FullName } }, default(List<IEntityName>));

                Role = (int)ae.Role;
            }

            ExpertList = string.Join(";", new ActivityExpertService().Repository.GetAllEx(q => q
                .Where(x => x.Conclusion.Id == ConclusionId))
                .Select(x => x.Expert.Id));

            RoleList = new System.Web.Mvc.SelectList(EnumHelper.ToList<ActivityExpertEnum>(), "Key", "Value", ae.Return(x => x.Role, default(ActivityExpertEnum)));
        }

        public bool ReadOnly { get; set; }
    }

    public class ExpertiseEditViewModel_Valid : ValidateViewModel
    {
        [RangeAttribute(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        [Required(ErrorMessage = RequiredText)]
        public object User { get; set; }

        [RangeAttribute(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        [Required(ErrorMessage = RequiredText)]
        public object Role { get; set; }
    }
}