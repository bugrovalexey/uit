﻿using bgTeam.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Shared
{
    [MetadataType(typeof(AOTypeAndKindViewModel_Valid))]
    public class AOTypeAndKindViewModel : ViewModelBase
    {
        #region consts

        private const string TypeCaption = "Тип ОУ";
        private const string KindCaption = "Вид ОУ";

        #endregion consts

        AccountingObject _accountingObject { get; set; }

        public AOTypeAndKindViewModel(int? aoId, bool readOnly)
        {
            ReadOnly = readOnly;

            _accountingObject = new RepositoryDc<AccountingObject>().Get(aoId);

            TypeList = new SelectList(new IKTComponentRepository().GetAllParents(), "Id", "Name");
            KindList = new SelectList(new IKTComponentRepository().GetAll(), "Id", "Name"); ;
        }

        [DisplayName(KindCaption)]
        public int? KindId
        {
            get { return _accountingObject.With(ao => ao.Kind).Return(k => k.Id, default(int)); }
        }

        [DisplayName(TypeCaption)]
        public int? TypeId
        {
            get { return _accountingObject.With(ao => ao.Type).Return(t => t.Id, default(int)); }
        }

        public SelectList TypeList { get; set; }

        public SelectList KindList { get; set; }
    }

    public class AOTypeAndKindViewModel_Valid : ValidateViewModel
    {
        [Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        [Required(ErrorMessage = RequiredText)]
        public int? TypeId { get; set; }
    }

}