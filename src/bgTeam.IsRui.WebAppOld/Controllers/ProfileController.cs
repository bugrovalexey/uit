﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Profile;
using System.Web.Mvc;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.IsRui.DataAccess.Services.Args;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public class ProfileController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public ProfileController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public ActionResult UserCard()
        {
            return View(new UserCardViewModel(_userRepository.GetCurrent()));
        }

        [HttpPost]
        public ActionResult UserCardEdit(UserCardArgs data)
        {
            return NotificationHelper.NotificationCreateEdit(delegate()
            {
                new UserService().Update(data);
            });
        }
    }
}