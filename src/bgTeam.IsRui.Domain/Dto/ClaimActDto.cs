﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;

    public class ClaimActDto
    {
        public int Id { get; set; }

        public int ClaimId { get; set; }

        public string ClaimName { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string Number { get; set; }

        public DateTime? DateAdoption { get; set; }

        public decimal Cost { get; set; }

        /// <summary>
        /// Файл акта
        /// </summary>
        public FilesDto File { get; set; }
    }
}
