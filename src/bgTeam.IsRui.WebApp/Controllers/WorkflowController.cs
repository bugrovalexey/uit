﻿namespace bgTeam.IsRui.WebApp.Controllers
{
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Story.Common;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [Authorize]
    [Route("api/[controller]/[action]")]
    public class WorkflowController : Controller
    {
        private readonly IStoryBuilder _builder;

        public WorkflowController(IStoryBuilder builder)
        {
            _builder = builder;
        }

        /// <summary>
        /// Загрузить доступные статусы
        /// </summary>
        /// <param name="context">
        /// - WorkflowId Идентификатор бизнесс процесса
        /// </param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IEnumerable<StatusDto>> StatusGetNextList(WorkflowStatusGetNextListStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<StatusDto>>();
        }

        /// <summary>
        /// Изменить статус бизнесс процесса
        /// </summary>
        /// <param name="context">
        /// - WorkflowId Идентификатор бизнесс процесса
        /// - StatusId Идентификатор статуса
        /// </param>
        /// <returns></returns>
        [HttpPost]
        public async Task<WorkflowDto> StatusChange(WorkflowStatusChangeStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<WorkflowDto>();
        }
    }
}
