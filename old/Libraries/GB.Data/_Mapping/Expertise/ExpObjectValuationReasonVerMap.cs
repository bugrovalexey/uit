﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.OtherExpObjects;

namespace Uviri.MKRF.Mapping.Expertise
{
	public class ExpObjectValuationReasonVerMap : ClassMapBase<ExpObjectValuationReasonVer>
	{
        public ExpObjectValuationReasonVerMap() : base("Exp_Valuation_Reason_Other_Ver"){}

        protected override void InitMap()
        {
            Map(x => x.Comm, "Exp_Valuation_Reason_Other_Ver_Comm");

            References<ExpObjectCriterionEquivalentVer>(x => x.ExpCriterionEquivalentVer, "Exp_Criterion_Equivalent_Other_Ver_ID");
            References<ExpReason>(x => x.ExpReason, "Exp_Reason_ID");
            References<ExpConformity>(x => x.ExpConformity, "Exp_Conformity_ID");
        }
    }
}