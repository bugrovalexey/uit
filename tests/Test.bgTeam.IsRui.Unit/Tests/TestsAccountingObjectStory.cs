﻿using bgTeam.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Dto;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Story.AccountingObjects;
using bgTeam.IsRui.Story.Activities;
using bgTeam.IsRui.Story.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Test.bgTeam.IsRui.Unit.Common;
using Xunit;

namespace Test.bgTeam.IsRui.Unit.Tests
{
    public class TestsAccountingObjectStory
    {
        private Factory _fac;

        public TestsAccountingObjectStory()
        {
            _fac = new Factory();
        }

        [Fact]
        public void Story_GetSaldoByContractGuidStory()
        {
            //_fac.logger
            //var story = new GetSaldoByContractGuidStory(_fac., _webClient, _repository, _factory);
            //var res = story.Execute(new GetSaldoByContractGuidStoryContext
            //{
            //    ContractGuid = new Guid("e949116b-69c2-4a35-8954-42c1f3452cff")
            //});

            //Assert.NotNull(res);
            //Assert.True(res.Orders.Any());
            //Assert.True(res.Services.Any());
        }

        [Fact]
        public void Story_AccountingObjectCreateStory()
        {
            var sttrep = _fac.statusRepository;

            var story = new AccountingObjectCreateStory(_fac.repositoryRui, _fac.userRepository, sttrep, _fac.sessionFactory);
            var res = story.Execute(new AccountingObjectCreateStoryContext()
            {
                Name = "Full Test Example",
                ArticleId = 1,
                DepartmentId = 1,
                KindId = 1,
                kindOfActivityId = 1
            });

            Assert.True(res.Id > 0);
        }

        [Fact]
        public void Story_AccountingObjectListStory()
        {

            var story = new AccountingObjectListStory(
                _fac.storyMapper,
                _fac.managerAccess,
                _fac.repositoryRui,
                _fac.userRepository);
            var res = story.Execute(new AccountingObjectListStoryContext());

            var lst = res.Select(x => x.Id).ToList();

            Assert.True(lst.Count > 0);
        }

        [Fact]
        public void Story_AccountingObjectFavoriteAddStory()
        {
            var user = _fac.userRepository.GetCurrent();

            var first = _fac.accountingRepository
                .GetAllEx(q => q.Fetch(x => x.Favorites).Eager.Take(1)).First();

            var exists = first.Favorites.Any(x => x.User.Id == user.Id);

            var story = new AccountingObjecFavoriteAddStory(
                _fac.repositoryRui,
                _fac.userRepository,
                _fac.sessionFactory);

            var res = story.Execute(new AccountingObjecFavoriteAddStoryContext
            {
                Id = first.Id
            });

            Assert.True(exists ? !res : res);
        }

        [Fact]
        public void Story_AccountingObjectDeleteStory()
        {
            var iktrep = new RepositoryDc<IKTComponent>();
            var deprep = new RepositoryDc<Department>();
            var sttrep = _fac.statusRepository;

            var story = new AccountingObjectCreateStory(_fac.repositoryRui, _fac.userRepository, sttrep, _fac.sessionFactory);
            var res = story.Execute(new AccountingObjectCreateStoryContext()
            {
                Name = "Full Test Example",
                ArticleId = 1,
                DepartmentId = 1,
                KindId = 1
            });

            Assert.True(res.Id > 0);

            var storyDelete = new AccountingObjectDeleteStory(_fac.repositoryRui, _fac.sessionFactory, _fac.fileProviderRui, _fac.storyMapper);
            var resDelete = storyDelete.Execute(new AccountingObjectDeleteStoryContext()
            {
                Id = res.Id
            });

            Assert.True(resDelete);
        }

        [Fact]
        public void Story_AccountingObjectInfoStory()
        {
            var getLinkedActivityStory = new AoGetLinkedActivitiesStory(_fac.repositoryRui, _fac.storyMapper);
            var getAllContractsStory = new AoContractsGetAllContractsStory(
                _fac.repositoryRui, _fac.storyMapper, getLinkedActivityStory);
            var story = new AccountingObjectInfoStory(
                _fac.repositoryRui,
                _fac.storyMapper,
                getAllContractsStory,
                getLinkedActivityStory);

            var res = story.Execute(new AccountingObjectInfoStoryContext() { Id = 46 });

            Assert.NotNull(res);
            Assert.Equal(46, res.Id);
            Assert.NotEmpty(res.Characteristics);
            Assert.NotEmpty(res.Software);
            Assert.NotEmpty(res.WorksAndServices);
            Assert.NotEmpty(res.Contracts);
            //Assert.NotEmpty(res.InternalIsAndComponentsItki);
            //Assert.NotEmpty(res.ExternalIsAndComponentsItki);
            //Assert.NotEmpty(res.ExternalUserInterfacesOfIs);
            //Assert.NotEmpty(res.GovPurchases);
            //Assert.NotEmpty(res.Documents);
            Assert.NotEmpty(res.Activities);
            Assert.True(res.Activities.All(x => x.Id > 0));
            Assert.True(res.CreateCost > 0M);
            Assert.True(res.DevelopCost > 0M);
            Assert.True(res.ExploitationCost > 0M);
            //Assert.NotEmpty(res.TechnicalSupports);

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(res, Newtonsoft.Json.Formatting.Indented);

            Assert.NotNull(json);
        }

        [Fact]
        public void Story_AccountingObjecGetConclusionHistoryStory()
        {
            var story = new AccountingObjecGetConclusionHistoryStory(
                _fac.storyMapper,
                _fac.repositoryRui);

            var first = _fac.accountingRepository
                .Get(q => q.Fetch(x => x.UserComments).Eager.Take(1));

            var result = story.Execute(new AccountingObjecGetConclusionHistoryStoryContext
            {
                Id = first.Id
            });

            Assert.NotNull(result);

            var all = first.UserComments
                .Where(x => x.Status != null)
                .Select(x => x.Id).ToList();

            var lst = result.Select(x => x.Id).ToList();

            Assert.True(all.Count == lst.Count && all.SequenceEqual(lst));
        }

        [Fact]
        public void Story_AoConclusionGetStory()
        {
            var story = new AoConclusionGetStory(_fac.storyMapper, _fac.repositoryRui, _fac.userRepository);
            var res = story.Execute(new AoConclusionGetStoryContext() { AoId = 46 });

            Assert.NotNull(res);
            //Assert.NotNull(res.Id);
            Assert.NotNull(res.Comment);
        }

        [Fact]
        public void Story_AoConclusionAddOrUpdateStory()
        {
            var story = new AoConclusionAddOrUpdateStory(_fac.storyMapper, _fac.repositoryRui, _fac.userRepository, _fac.sessionFactory);
            var res = story.Execute(new AoConclusionAddOrUpdateStoryContext()
            {
                AoId = 46,
                Comment = $"Авто Тест {DateTime.Now}",
            });

            Assert.NotNull(res);
            Assert.Equal(46, res.AoId);

            var resUpdate = story.Execute(new AoConclusionAddOrUpdateStoryContext()
            {
                AoId = 46,
                Id = res.Id,
                Comment = $"Авто Тест {DateTime.Now}",
            });

            Assert.NotNull(res);
            Assert.Equal(46, res.AoId);
        }

        [Fact]
        public void Story_AoChangeStageStory()
        {
            var story = new AccountingObjectChangeStageStory(
                _fac.repositoryRui,
                _fac.sessionFactory);

            var first = _fac.accountingRepository
                .Get(q => q.Fetch(x => x.Favorites).Eager.Take(1));

            var oldStage = first.Stage;
            var newStage = Enum.GetValues(typeof(AccountingObjectsStageEnum))
                .Cast<AccountingObjectsStageEnum>()
                .Except(new[] { oldStage }).First();

            var result = story.Execute(new AccountingObjectChangeStageStoryContext
            {
                Id = first.Id,
                Stage = newStage.GetDescription()
            });

            first = _fac.accountingRepository
                .Get(q => q.Fetch(x => x.Favorites).Eager.Take(1));

            Assert.NotNull(result);
            Assert.True(newStage == first.Stage);
        }

        [Fact]
        public void Story_AoChangeStatusStory()
        {
            var story = new AccountingObjectChangeStatusStory(
                _fac.repositoryRui,
                _fac.sessionFactory,
                _fac.aoWorkflowHandler);

            var first = _fac.accountingRepository
                .Get(q => q.Fetch(x => x.Favorites).Eager.Take(1));

            var oldStatus = first.Status;
            var newStatus = _fac.statusRepository
                .GetStatuses(EntityType.AccountingObject)
                .First(x => x.Id != oldStatus.Id);

            var result = story.Execute(new AccountingObjectChangeStatusStoryContext
            {
                Id = first.Id,
                Type = EntityType.AccountingObject.GetDescription(),
                Status = newStatus.Id.ToString()
            });

            first = _fac.accountingRepository
                .Get(q => q.Fetch(x => x.Favorites).Eager.Take(1));

            Assert.NotNull(result);
            Assert.True(newStatus.Id == first.Status.Id);
        }

        [Fact(Skip = "")]
        public void Story_FileDeleteStory()
        {
            var story = new FileDeleteStory(_fac.repositoryRui, _fac.fileProviderRui, _fac.sessionFactory);
            var res = story.Execute(new FileDeleteStoryContext() { Id = 66 });

            Assert.True(res);
        }

        [Fact]
        public void Story_DocumentBaseGetAllDocumentsStory()
        {
            var story = new AoDocumentBaseGetAllDocumentsStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new AoDocumentBaseGetAllDocumentsStoryContext() { AoId = 46 });

            Assert.NotEmpty(result);

        }
        [Fact(Skip = "")]
        public void Story_AoDocumentBaseCreateOrUpdateDocument()
        {
            var story = new AoDocumentBaseCreateOrUpdateDocumentStory(_fac.repositoryRui, _fac.sessionFactory, _fac.storyMapper, _fac.fileProviderRui);

            var result = story.Execute(new AoDocumentBaseCreateOrUpdateDocumentStoryContext()
            {
                Name = "имя",
                Number = "123",
                file = new CreateDocumentFileData()
                {
                    FileName = "имя файла",
                    TempGuid = new Guid("ef4c3ab4-58a2-44fa-8f46-3ba38d3e2002")
                },
            });

        }

        [Fact]
        public void Story_FileDownloadStory()
        {
            var story = new FileDownloadStory(_fac.repositoryRui, _fac.fileProviderRui);
            var res = story.Execute(new FileDownloadStoryContext()
            {
                Id = 504
            });

            Assert.NotNull(res);
            Assert.Equal("TendersReport-13.09.2017.xlsx", res.FileName);
            Assert.Equal("application/excel", res.MimeType);
        }

        [Fact(Skip = "")]
        public void Story_AoDocumentBaseDeleteDocumentStory()
        {
            var story = new AoDocumentBaseDeleteDocumentStory(_fac.repositoryRui, _fac.sessionFactory, _fac.storyMapper, _fac.fileProviderRui);

            var result = story.Execute(new AoDocumentBaseDeleteDocumentStoryContext() { Id = 250 });
        }

        [Fact]
        public void Story_AoGoalsAndPurposeGetInfoStory()
        {
            var story = new AoGoalsAndPurposeGetInfoStory(_fac.repositoryRui);

            var result = story.Execute(new AoGoalsAndPurposeGetInfoStoryContext() { AoId = 46 });

            Assert.NotEmpty(result.Targets);
            Assert.NotEmpty(result.PurposeAndScope);
        }

        [Fact]
        public void Story_AoGoalsAndPurposeUpdateInfoStory()
        {
            var story = new AoGoalsAndPurposeUpdateInfoStory(_fac.sessionFactory, _fac.repositoryRui);

            bool result = story.Execute(new AoGoalsAndPurposeUpdateInfoStoryContext() { AoId = 46, PurposeAndScope = "New PurAndSc", Targets = "New Targ" });

            Assert.True(result);
        }

        [Fact]
        public void Story_AoCommonInfoGetStory()
        {
            var story = new AoCommonInfoGetStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new AoCommonInfoGetStoryContext() { AoId = 46 });

            Assert.NotNull(result);
            Assert.NotNull(result.Name);
            Assert.NotNull(result.KindId);
            Assert.NotNull(result.ArticleId);
            Assert.Equal(46, result.Id);
        }

        [Fact]
        public void Story_Story_AoCommonInfoUpdateStory()
        {
            var story = new AoCommonInfoUpdateStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            bool result = story.Execute(new AoCommonInfoUpdateStoryContext() { Id = 46, KindId = 1, ArticleId = 1, Name = "Unit test run" });

            Assert.True(result);
        }

        [Fact]
        public void Story_CharacteristicsGetAllCharacteristics()
        {
            var story = new AoCharacteristicsGetAllCharacteristicsStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new AoCharacteristicsGetAllCharacteristicsStoryContext() { AoId = 46 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_CharacteristicsAddOrUpdateCharacteristic()
        {
            var story = new AoCharacteristicsAddOrUpdateCharacteristicStory(_fac.storyMapper, _fac.repositoryRui, _fac.sessionFactory);

            var result = story.Execute(new AoCharacteristicsAddOrUpdateCharacteristicStoryContext()
            {
                AoId = 46,
                Name = "Имя",
                TypeId = 1,
                UnitId = 1,
                Norm = 50,
                Max = 60,
                Fact = 80,
                DateBeginning = DateTime.Now
            });

            Assert.NotNull(result.Id);
        }

        [Fact]
        public void Story_CharacteristicsDeleteCharacteristic()
        {
            var storyCreate = new AoCharacteristicsAddOrUpdateCharacteristicStory(_fac.storyMapper, _fac.repositoryRui, _fac.sessionFactory);

            var resultCreate = storyCreate.Execute(new AoCharacteristicsAddOrUpdateCharacteristicStoryContext()
            {
                AoId = 46,
                Name = "Имя",
                TypeId = 1,
                UnitId = 1,
                Norm = 50,
                Max = 60,
                Fact = 80,
                DateBeginning = DateTime.Now
            });

            var storyDelete = new AoCharacteristicsDeleteCharacteristicStory(_fac.storyMapper, _fac.repositoryRui, _fac.sessionFactory);

            bool resultDelete = storyDelete.Execute(new AoCharacteristicsDeleteCharacteristicStoryContext() { Id = resultCreate.Id.GetValueOrDefault() });

            Assert.True(resultDelete);
        }

        [Fact]
        public void Story_SoftwareGetAllSoftware()
        {
            var story = new AoSoftwareGetAllSoftwareStory(_fac.storyMapper, _fac.repositoryRui);

            var result = story.Execute(new AoSoftwareGetAllSoftwareStoryContext() { AoId = 46 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_SoftwareAddOrUpdateSoftwareStory()
        {
            var story = new AoSoftwareAddOrUpdateSoftwareStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new AoSoftwareAddOrUpdateSoftwareStoryContext()
            {
                AoId = 46,
                CategoryKindOfSupportId = 1,
                Name = "Name",
                Summa = 1000,
                ManufacturerCustomName = "ManufacturerName",
                //CharacteristicOfLicenseId = 126
                CharacteristicOfLicenseName = "CharacteristicOfLicenseName"
            });

            Assert.NotNull(result.Id);

            var resultUpdate = story.Execute(new AoSoftwareAddOrUpdateSoftwareStoryContext()
            {
                Id = result.Id,
                AoId = 46,
                CategoryKindOfSupportId = 1,
                Name = "Name1",
                Summa = 10000,
                ManufacturerCustomName = "ManufacturerName1",
                //CharacteristicOfLicenseId = 126
                CharacteristicOfLicenseName = "CharacteristicOfLicenseName1"
            });

            Assert.NotNull(resultUpdate.Id);
        }

        [Fact]
        public void Story_SoftwareDeleteSoftwareStory()
        {
            var storyCreate = new AoSoftwareAddOrUpdateSoftwareStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var resultCreate = storyCreate.Execute(new AoSoftwareAddOrUpdateSoftwareStoryContext()
            {
                AoId = 46,
                CategoryKindOfSupportId = 1,
                Name = "Name",
                Summa = 1000,
                ManufacturerCustomName = "ManufacturerName",
                CharacteristicOfLicenseName = "CharacteristicOfLicenseName"
            });

            var storyDelete = new AoSoftwareDeleteSoftwareStory(_fac.storyMapper, _fac.repositoryRui, _fac.sessionFactory);

            var resultDelete = storyDelete.Execute(new AoSoftwareDeleteSoftwareStoryContext() { Id = resultCreate.Id.Value });

            Assert.True(resultDelete);
        }

        [Fact]
        public void Story_TechnicalSupportGetAllTechSupport()
        {
            var story = new AoTechnicalSupportGetAllTechSupportStory(_fac.storyMapper, _fac.repositoryRui);

            var result = story.Execute(new AoTechnicalSupportGetAllTechSupportStoryContext() { AoId = 46 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_TechnicalSupportAddOrUpdateTechSupportStory()
        {
            var story = new AoTechnicalSupportAddOrUpdateTechSupportStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var charactList = new List<CharacteristicOfTechnicalSupport>();

            var result = story.Execute(new AoTechnicalSupportAddOrUpdateTechSupportStoryContext()
            {
                AoId = 46,
                CategoryKindOfSupportId = 1,
                Name = "Name",
                Summa = 1000,
                ManufacturerCustomName = "ManufacturerCastomName",
                Characteristics = charactList
            });

            Assert.NotNull(result.Id);

            var charact = new CharacteristicOfTechnicalSupport()
            {
                Id = 36,
            };

            charactList.Add(charact);

            var resultUpdate = story.Execute(new AoTechnicalSupportAddOrUpdateTechSupportStoryContext()
            {
                Id = result.Id,
                AoId = 46,
                CategoryKindOfSupportId = 1,
                Name = "NameNew",
                Summa = 2000,
                ManufacturerCustomName = "ManufacturerCastomNameNew",
                Characteristics = charactList
            });

            Assert.NotNull(resultUpdate.Id);
            Assert.NotEmpty(resultUpdate.Characteristics);

        }

        [Fact]
        public void Story_TechnicalSupportDeleteTechSupportStory()
        {
            var storyCreate = new AoTechnicalSupportAddOrUpdateTechSupportStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var charactList = new List<CharacteristicOfTechnicalSupport>();

            var resultCreate = storyCreate.Execute(new AoTechnicalSupportAddOrUpdateTechSupportStoryContext()
            {
                AoId = 46,
                CategoryKindOfSupportId = 1,
                Name = "Name",
                Summa = 1000,
                ManufacturerCustomName = "ManufacturerCastomName",
                Characteristics = charactList
            });

            var storyDelete = new AoTechnicalSupportDeleteTechSupportStory(_fac.sessionFactory);

            var resultDelete = storyDelete.Execute(new AoTechnicalSupportDeleteTechSupportStoryContext() { Id = resultCreate.Id.GetValueOrDefault() });

            Assert.True(resultDelete);
        }

        [Fact]
        public void Story_TechnicalSupportAddOrUpdateTechSupportCharacteristicStory()
        {
            var story = new AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStoryContext() { TechnicalSupportId = 19, Name = "Name", UnitId = 1, Value = 1 });

            Assert.NotNull(result.Id);
        }

        [Fact]
        public void Story_TechnicalSupportDeleteTechSupportCharacteristicStory()
        {
            var storyCreate = new AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var resultCreate = storyCreate.Execute(new AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStoryContext() { TechnicalSupportId = 19, Name = "Name", UnitId = 1 });

            var storyDelete = new AoTechnicalSupportDeleteTechSupportCharacteristicStory(_fac.sessionFactory);

            var resultDelete = storyDelete.Execute(new AoTechnicalSupportDeleteTechSupportCharacteristicStoryContext() { Id = resultCreate.Id.GetValueOrDefault() });

            Assert.True(resultDelete);
        }

        [Fact]
        public void Story_WorkAndServiceGetAllWork()
        {
            var story = new AoWorkAndServiceGetAllWorkStory(_fac.storyMapper, _fac.repositoryRui);

            var result = story.Execute(new AoWorkAndServiceGetAllWorkStoryContext() { AoId = 46 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_WorkAndServiceAddOrUpdateWork()
        {
            var story = new AoWorkAndServiceAddOrUpdateWorkStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new AoWorkAndServiceAddOrUpdateWorkStoryContext() { AoId = 46, Name = "Имя", CategoryKindOfSupportId = 1, Summa = 1000 });

            Assert.NotNull(result.Id);
        }

        [Fact]
        public void Story_WorkAndServiceDeleteWork()
        {
            var storyCreate = new AoWorkAndServiceAddOrUpdateWorkStory(_fac.sessionFactory, _fac.repositoryRui, _fac.storyMapper);

            var resultCreate = storyCreate.Execute(new AoWorkAndServiceAddOrUpdateWorkStoryContext() { AoId = 46, Name = "Имя", CategoryKindOfSupportId = 1, Summa = 1000 });

            var storyDelete = new AoWorkAndServiceDeleteWorkStory(_fac.sessionFactory);

            var resultDelete = storyDelete.Execute(new AoWorkAndServiceDeleteWorkStoryContext() { Id = resultCreate.Id.GetValueOrDefault() });

            Assert.True(resultDelete);
        }

        [Fact]
        public void Story_FinancingGetInfoStory()
        {
            var story = new AoFinancingGetInfoStory(_fac.repositoryRui, _fac.storyMapper);

            var result = story.Execute(new AoFinancingGetInfoStoryContext() { AoId = 46 });

            Assert.NotNull(result.AoId);
        }

        [Fact]
        public void Story_FinancingUpdateInfo()
        {
            var story = new AoFinancingUpdateInfoStory(_fac.repositoryRui, _fac.storyMapper, _fac.sessionFactory);

            var result = story.Execute(new AoFinancingUpdateInfoStoryContext()
            {
                AoId = 46,
                quarter1 = 1234,
                quarter2 = 2341,
                quarter3 = 3412,
                quarter4 = 4123
            });

            Assert.NotNull(result.AoId);
            Assert.NotNull(result.quarter1);
            Assert.NotNull(result.quarter2);
            Assert.NotNull(result.quarter3);
            Assert.NotNull(result.quarter4);

        }


        [Fact]
        public void Story_ContractAddOrUpdate()
        {
            var story = new AoContractAddOrUpdateStory(_fac.storyMapper, _fac.repositoryRui, _fac.sessionFactory);

            var result = story.Execute(new AoContractAddOrUpdateStoryContext() { AoId = 46, Name = "Имя", Number = "123", BeginDate = DateTime.Now, EndDate = DateTime.Now });

            Assert.NotNull(result.Id);

            var resultUpdate = story.Execute(new AoContractAddOrUpdateStoryContext() { Id = result.Id, AoId = 46, Name = "Имя123", Number = "12345", BeginDate = DateTime.Now, EndDate = DateTime.Now });

            Assert.NotNull(resultUpdate.Id);
        }


        [Fact]
        public void Story_ContractsGetAllContracts()
        {
            var getLinkedActivityStory = new AoGetLinkedActivitiesStory(_fac.repositoryRui, _fac.storyMapper);
            var story = new AoContractsGetAllContractsStory(
                _fac.repositoryRui,
                _fac.storyMapper,
                getLinkedActivityStory);

            var result = story.Execute(new AoContractsGetAllContractsStoryContext() { AoId = 46 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_ContractsDeleteContract()
        {
            var storyCreate = new AoContractAddOrUpdateStory(_fac.storyMapper, _fac.repositoryRui, _fac.sessionFactory);

            var resultCreate = storyCreate.Execute(new AoContractAddOrUpdateStoryContext() { AoId = 46, Name = "Имя", Number = "123", BeginDate = DateTime.Now, EndDate = DateTime.Now });

            var storyDelete = new AoContractsDeleteContractStory(_fac.sessionFactory);

            var resultDelete = storyDelete.Execute(new AoContractsDeleteContractStoryContext() { Id = resultCreate.Id });

            Assert.True(resultDelete);
        }

        [Theory]
        [InlineData("Units")]
        [InlineData("ClassificationСategory")]
        [InlineData("Okved")]
        [InlineData("Manufacturer")]
        [InlineData("CategoryKind")]
        [InlineData("aoArticle")]
        [InlineData("aoCategory")]
        [InlineData("aoCharacteristicType")]
        [InlineData("ActivityType")]
        [InlineData("ExpenditureItem")]
        [InlineData("OKPD")]
        [InlineData("ExpenseDirection")]
        [InlineData("ExpenseType")]
        [InlineData("ProductGroup")]
        public void Story_GetDictionaryStory(string dictName)
        {
            var story = new GetDictionaryStory(_fac.storyMapper, _fac.repositoryRui);

            var result = story.Execute(new GetDictionaryStoryContext() { Name = dictName });

            Assert.NotNull(result);
            Assert.True(result.Any());
        }

        [Fact]
        public void Story_GetDictionaryStory_Search()
        {
            var story = new GetDictionaryStory(_fac.storyMapper, _fac.repositoryRui);

            var result = story.Execute(new GetDictionaryStoryContext() { Name = "Units", Search = "шт" });

            Assert.NotNull(result);
            Assert.True(result.Any());
            Assert.Single(result);
        }

        [Fact]
        public void Story_GetDictionaryStory_ArticleBudget()
        {
            var story = new GetDictionaryStory(_fac.storyMapper, _fac.repositoryRui);

            var result = story.Execute(new GetDictionaryStoryContext() { Name = "ArticleBudget" });

            Assert.NotNull(result);
            Assert.True(result.Any());
        }

        [Fact]
        public void Story_GetDictionaryStory_Departments()
        {
            var story = new GetDictionaryStory(_fac.storyMapper, _fac.repositoryRui);

            var result = story.Execute(new GetDictionaryStoryContext() { Name = "Departments" });

            Assert.NotNull(result);
            Assert.True(result.Any());

            foreach (var item in result)
            {
                Assert.NotNull(item.Name);
            }
        }

        [Fact]
        public void Story_GetDictionaryStory_Search_ClassificationСategory()
        {
            var story = new GetDictionaryStory(_fac.storyMapper, _fac.repositoryRui);

            var result = story.Execute(
                new GetDictionaryStoryContext() { Name = "ClassificationСategory", Search = "child" });

            Assert.NotNull(result);
            Assert.True(result.Any());
        }

        [Fact]
        public void Story_GetDepartmentsAllChildrensByIds()
        {
            var story = new GetDepartmentsAllChildrensByIdsStory(_fac.logger, _fac.storyMapper, _fac.repositoryRui);

            var result = story.Execute(
                new GetDepartmentsAllChildrensByIdsStoryContext() { Id = 2 });

            Assert.NotNull(result);
            Assert.True(result.Any());
        }

        [Fact]
        public void Story_ActsAddOrUpdateAct()
        {
            var story = new AoActsAddOrUpdateActStory(_fac.repositoryRui, _fac.sessionFactory, _fac.storyMapper, _fac.fileProviderRui);

            var result = story.Execute(new AoActsAddOrUpdateActStoryContext()
            {
                AoId = 46,
                Name = "Имя",
                Number = "123",
                DateAdoption = DateTime.Now,
                Cost = 123,
                File = new ActsFileDto()
                {
                    Id = 630,
                },
            });

            Assert.NotNull(result.Id);

            var resultUpdate = story.Execute(new AoActsAddOrUpdateActStoryContext()
            {
                Id = result.Id,
                AoId = 46,
                Name = "Имя123",
                Number = "12345",
                DateAdoption = DateTime.Now,
                Cost = 123,
                File = new ActsFileDto()
                {
                    Id = result.File.Id,
                },
            });

            Assert.NotNull(resultUpdate.Id);
        }

        [Fact]
        public void Story_ActsGetAllActs()
        {
            var story = new AoActsGetAllActsStory(_fac.storyMapper, _fac.repositoryRui);

            var result = story.Execute(new AoActsGetAllActsStoryContext() { AoId = 46 });

            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_ActsDeleteAct()
        {
            var story = new AoActsAddOrUpdateActStory(_fac.repositoryRui, _fac.sessionFactory, _fac.storyMapper, _fac.fileProviderRui);

            var result = story.Execute(new AoActsAddOrUpdateActStoryContext()
            {
                AoId = 46,
                Name = "Имя",
                Number = "123",
                DateAdoption = DateTime.Now,
                Cost = 123,
                File = new ActsFileDto()
                {
                    Id = 630,
                },
            });


            var storyDel = new AoActsDeleteActStory(_fac.storyMapper, _fac.repositoryRui, _fac.sessionFactory, _fac.fileProviderRui);

            var resultDel = storyDel.Execute(new AoActsDeleteActStoryContext()
            {
                Id = result.Id,
            });

            Assert.True(resultDel);
        }

        [Fact]
        public void Story_FinancingReport()
        {
            var story = new FinancingReportStory(_fac.repositoryRui);

            //var data = new List<TemplateBlockDto>();

            //var positions = new List<PositionsTemplateDto>();

            //var pos1 = new PositionsTemplateDto() { Id = 1, Article = "Art1", Name = "Name1", AccountingObject = "Ao1", Quart1 = 1, Quart2 = 2, Quart3 = 3, Quart4 = 4, SubDivision = "SubDivision1" };

            //var pos2 = new PositionsTemplateDto() { Id = 2, Article = "Art2", Name = "Name2", AccountingObject = "Ao2", Quart1 = 12, Quart2 = 22, Quart3 = 32, Quart4 = 42, SubDivision = "SubDivision2" };

            //positions.Add(pos1);

            //positions.Add(pos2);

            //var block1 = new TemplateBlockDto() { Title = "block1", Positions = positions };

            //var block2 = new TemplateBlockDto() { Title = "block2", Positions = positions };

            //data.Add(block1);
            //data.Add(block2);

            var result = story.Execute(new FinancingReportStoryContext()
            {
               // PositionsBlocks = data
            });

            Assert.NotNull(result);
        }

        [Fact(Skip = "")]
        public void Story_ConvertHtmlToPdf()
        {
            var story = new FinancingReportPDFStory(_fac.converterPdf, _fac.repositoryRui);

            var result = story.Execute(new FinancingReportPDFStoryContext());

            Assert.True(result);
        }


        [Fact(Skip = "")]
        public void Story_ConvertHtmlToXls()
        {
            var story = new FinancingReportXlsStory(_fac.repositoryRui, _fac.converterXLS);

            var result = story.Execute(new FinancingReportXlsStoryContext());

            Assert.True(result);
        }

        [Fact]
        public void Story_AoGetLinkedActivitiesStory()
        {
            var story = new AoGetLinkedActivitiesStory(_fac.repositoryRui, _fac.storyMapper);
            var context = new AoGetLinkedActivitiesStoryContext
            {
                AoId = 46
            };

            var result = story.Execute(context);

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_AccountingObjectGetAllStory()
        {
            var story = new AccountingObjectGetAllStory(
               _fac.storyMapper,
               _fac.managerAccess,
               _fac.repositoryRui);
            var context = new AccountingObjectGetAllStoryContext();

            var result = story.Execute(context);

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void Story_AoContractsGetAllContractsStory()
        {
            var getLinkedActivityStory = new AoGetLinkedActivitiesStory(_fac.repositoryRui, _fac.storyMapper);
            var story = new AoContractsGetAllContractsStory(_fac.repositoryRui, _fac.storyMapper, getLinkedActivityStory);
            var context = new AoContractsGetAllContractsStoryContext
            {
                AoId = 46
            };

            var result = story.Execute(context);

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }
    }
}