﻿using System.ComponentModel;

namespace bgTeam.IsRui.Zakupki360.Dto
{
    public enum PurchaseTypeEnum
    {
         
        [Description("Открытый конкурс")]
        Open = 1,

        [Description("Электронный аукцион")]
        Auction = 2
    }
}
