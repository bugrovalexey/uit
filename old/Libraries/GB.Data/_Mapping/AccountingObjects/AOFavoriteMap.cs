﻿using GB.Data.AccountingObjects;

namespace GB._Mapping.AccountingObjects
{
    internal class AOFavoriteMap : ClassMapHiLo<AOFavorite>
    {
        public AOFavoriteMap()
            : base("OU_Favorites") { }

        protected override void InitMap()
        {
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
            References(x => x.User, "Users_ID");
        }
    }
}
