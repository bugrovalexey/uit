﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using bgTeam.IsRui.WebApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class StepBaseViewModel
    {
        public readonly string StepNexpText = "Следующий шаг";
        public readonly string StepPrevText = "Предыдущий шаг";
        public readonly string ReadyText = "Готово";

        public int Id { get; private set; }
        public bool ReadOnly { get; set; }
        public MasterItem Master { get; set; }
        public MasterItemEnum MasterCurrentStep { get; set; }
        public IList<string> MasterItems { get; set; }

        public string BaseName { get; set; }
        public string BaseStage { get; set; }
        public string BaseStatus { get; set; }

        protected readonly AccountingObject Current;
        

        public StepBaseViewModel(int id, MasterItemEnum item, params Expression<Func<AccountingObject, object>>[] fetches)
        {
            Id = id;

            Current = new RepositoryDc<AccountingObject>().Get(q =>
            {
                q = q.Where(x => x.Id == id);

                foreach (var f in fetches)
                {
                    q.Fetch(f).Eager.Clone();
                }
            });

            if (Current == null)
                throw new EntityNotFoundException(Id);

            //ReadOnly = Current.CanEdit() == false;

            BaseName = Current.ShortName;
            BaseStage = Current.Stage.GetDescription();
            BaseStatus = Current.Status.Name;

            MasterCurrentStep = item;

            var handler = Singleton<MasterEditManager>.Instance.GetHandler(Current);
            Master = handler.GetItem(item);
            MasterItems = new List<string>();

            bool nextlink = true;
            var items = handler.GetItems();
            foreach (var page in items)
	        {
                if (page.Item == item)
                {
                    MasterItems.Add(string.Concat("<li  class='current'>", page.TitleStep, "</li>"));
                    //nextlink = false;
                }
                else if (nextlink)
                    MasterItems.Add(string.Concat("<li>", LinkBuilder.ActionLink(page.TitleStep, page.Action, "AccountingObjects", RouteHelper.Merge(new { id = Id }, Master.Params)), "</li>"));
                else
                    MasterItems.Add(string.Concat("<li>", page.TitleStep, "</li>"));
	        }
        }
    }
}
