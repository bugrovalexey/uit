﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Validator
{
    class PurchasePlan
    {
        /// <summary>Параметры для валидации</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create)
            {
                Entities.Plans.PurchasePlan po = Entity as Entities.Plans.PurchasePlan;

                if (po != null)
                    return "0;" + po.Plan.Id.ToString() + ";";
                else
                    return "0";
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
