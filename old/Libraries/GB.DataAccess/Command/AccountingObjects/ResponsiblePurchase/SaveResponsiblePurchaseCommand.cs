﻿using GB.Data.AccountingObjects;

namespace GB.DataAccess.Command.AccountingObjects.ResponsiblePurchase
{
    public class SaveResponsiblePurchaseCommand : ICommand<SaveResponsiblePurchaseContext>
    {
        ISessionFactoryGb factory;

        public SaveResponsiblePurchaseCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(SaveResponsiblePurchaseContext context)
        {
            var ao = new Repository.Repository<AccountingObject>().Get(context.Id);

            ao.ResponsiblePurchase = new Repository.Repository<GB.Data.Common.Responsible>().Get(context.ResponsiblePurchase);

            using (var session = factory.Create())
            {
                session.Update(ao);
                session.Commit();
            }
        }
    }
}
