﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
    public class SoftwareMap : ClassMapBase<Software>
    {
        public SoftwareMap()
            : base("PRS_8_7")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_7_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"	 @PRS_8_7_1 = ? 
//							,@PRS_8_7_3 = ? 
//							,@PRS_8_7_4 = ? 
//							,@PRS_8_7_5 = ? 
//							,@PRS_8_7_6 = ? 
//							,@Plans_ID = ? 
//							,@PRS_8_7_Soft_Type_ID = ? 
//							,@PRS_8_7_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_7_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_7_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_7_Delete @PRS_8_7_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_8_7_1");
			Map(x => x.Name, "PRS_8_7_3");
			Map(x => x.Developer, "PRS_8_7_4");
			Map(x => x.InstallCount_CA, "PRS_8_7_5");
			Map(x => x.InstallCount_TP, "PRS_8_7_6");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
			References<SoftwareType>(x => x.SoftwareType, "PRS_8_7_Soft_Type_ID");
        }
    }
}