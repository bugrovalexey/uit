﻿using System.Collections.Generic;
using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
	/// <summary>Соответствие критериям</summary>
	public class ExpCriterionEquivalent : EntityBase // ExpCriterionEquivalentBase
	{
        //private IList<ExpValuationReason> valuationReasons;

        protected ExpCriterionEquivalent() { }

        /// <summary>Акт экспертного заключения</summary>
        public virtual ExpConclAct ExpConclAct { get; set; }

        /// <summary>Критерий</summary>
        public virtual ExpCriterion ExpCriterion { get; set; }

        /// <summary>Соответствие</summary>
        public virtual ExpConformity ExpConformity { get; set; }

        /// <summary>Комментарий</summary>
        public virtual string Comm { get; set; }//null

        /// <summary>
        /// Список оснований для оценивания
        /// </summary>
        public virtual IList<ExpValuationReason> ValuationReasons
        {
            get; // { return this.valuationReasons; }
            protected set; // { this.valuationReasons = value; }
        }
        ///// <summary>Акт экспертного заключения</summary>
        //public virtual new ExpConclAct ExpConclAct
        //{
        //    get { return (ExpConclAct)base.ExpConclAct; }
        //    protected set { base.ExpConclAct = value; }
        //}
	}
}