﻿namespace bgTeam.IsRui.Domain.Entities.Directories
{
    using System;

    /// <summary>
    /// ОКПД
    /// </summary>
    public class OKPD : EntityDirectories, IPeriodDirectories, IEntityCodeName //Было ОКДП - теперь это ОКПД
    {
        /// <summary>Код</summary>
        public virtual string Code { get; set; }//null

        /// <summary> Дата начала действия справочника </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary> Дата окончания действия справочника </summary>
        public virtual DateTime? DateEnd { get; set; }

        public virtual string CodeName
        {
            get
            {
                return EntityHelper.GetCodeName(this);
            }
        }
    }
}