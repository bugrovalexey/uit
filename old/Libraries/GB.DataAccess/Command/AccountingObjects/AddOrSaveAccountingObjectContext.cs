﻿
namespace GB.DataAccess.Command.AccountingObjects
{
    public class AddOrSaveAccountingObjectContext : ICommandContext
    {
        public int Id { get; set; }

        /// <summary>
        /// Полное наименование
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Id Вида
        /// </summary>
        public int KindId { get; set; }

        public int TypeId { get; set; }
    }
}
