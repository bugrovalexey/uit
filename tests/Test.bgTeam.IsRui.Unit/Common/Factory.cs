﻿using bgTeam;
using bgTeam.Core;
using bgTeam.Core.Impl;
using bgTeam.DataAccess;
using bgTeam.DataAccess.Impl;
using bgTeam.Impl;
using bgTeam.Infrastructure;
using bgTeam.IsRui.Common.Services;
using bgTeam.IsRui.Common.Services.Impl;
using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.DataAccess.Impl;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.Access;
using bgTeam.IsRui.MKRF.Logic.Status;
using bgTeam.IsRui.Story.StoryMapping;
using bgTeam.IsRui.WebApp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using ManagerAccess = bgTeam.IsRui.MKRF.Logic.Access.Impl.ManagerAccess;

namespace Test.bgTeam.IsRui.Unit.Common
{
    class Factory
    {
        //private class TestCommandFactory : ICommandFactory
        //{
        //    private readonly Factory _factory;

        //    public TestCommandFactory(Factory factory)
        //    {
        //        _factory = factory;
        //    }

        //    public ICommand<TCommandContext> Create<TCommandContext>()
        //    {
        //        if (typeof(TCommandContext) == typeof(UpdateCommentStatusContext))
        //            return new UpdateCommentStatusCommand(_factory.repositoryRui, _factory.sessionFactory) as ICommand<TCommandContext>;
        //        throw new System.NotSupportedException();
        //    }

        //    public ICommand<TCommandContext, TResult> Create<TCommandContext, TResult>()
        //    {
        //        throw new System.NotSupportedException();
        //    }
        //}

        public IAppLogger logger { get; set; }
        public IAppConfiguration appConfiguration { get; set; }
        public IConnectionFactory connectionFactory { get; set; }
        public IRepository repository { get; set; }
        public IManagerAccess managerAccess { get; set; }
        public IUserRepository userRepository { get; set; }
        public IStatusRepository statusRepository { get; set; }
        public ISessionNHibernateFactory sessionFactory { get; set; }
        public IRepositoryDc<AoActivity> activityRepository { get; set; }
        public IRepositoryDc<AccountingObject> accountingRepository { get; set; }
        public IRepositoryRui repositoryRui { get; set; }
        public IMapperBase storyMapper { get; set; }
        public IWorkflowHandler<AccountingObject> aoWorkflowHandler { get; set; }
        public IFileProviderRui fileProviderRui { get; set; }
        public IHostingEnvironment hostingEnviroment { get; set; }
        public IFileProviderRuiSetting fileProviderRuiSetting { get; set; }
        public IBuildXLS builderXLS { get; set; }
        public IConvertHtmlToXls converterXLS { get; set; }
        public IConvertHtmlToPdf converterPdf { get; set; }




        public Factory()
        {
            logger = new AppLoggerDefault();
            managerAccess = new ManagerAccess();
            appConfiguration = new AppConfigurationDefault("appsettings", "Debug");
            sessionFactory = new SessionNHibernateFactory(
                new SessionNHibernateConfigurationMsSql(),
                new AppSettings(appConfiguration));
            userRepository = new UserRepository(sessionFactory, new HttpContextAccessor());
            repositoryRui = new Repository(sessionFactory);
            statusRepository = new StatusRepository(repositoryRui, userRepository, sessionFactory);
            activityRepository = new RepositoryDc<AoActivity>(sessionFactory);
            //accountingRepository = new AccountingObjectRepository(sessionFactory);
            storyMapper = new AutoMapperMap();
            hostingEnviroment = new HostingEnvironment()
            {
                WebRootPath = "WebRootPath"
            };
            fileProviderRuiSetting = new AppSettings(appConfiguration);
            fileProviderRui = new FileProviderRui(logger, hostingEnviroment, fileProviderRuiSetting);
            builderXLS = new BuldXLS();
            converterXLS = new ConvertHtmlToXls();
            converterPdf = new ConvertHtmlToPdf();
            //_connectionFactory = new ConnectionFactoryMsSql(_logger, _appConfiguration.GetConnectionString("RISDB"));
            //_repository = new RepositoryDapper(ConnectionFactory);
            //_repositoryEntity = new RepositoryEntityDapper(ConnectionFactory);
        }
    }
}
