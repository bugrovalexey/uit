﻿using bgTeam.IsRui.Domain.Entities.Admin;

namespace bgTeam.IsRui.Domain._Mapping.Admin
{
    internal class MetaDirectoryColumnMap : ClassMapBase<MetaDirectoryColumn>
    {
        public MetaDirectoryColumnMap()
            : base("Meta_Dir_Column")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Name");
            Map(x => x.ColumnName, "ColumnName");
            Map(x => x.OrderIndex, "Sort");
            Map(x => x.Visible, "Visible");
            Map(x => x.Type, "Type");

            References(x => x.Owner, "Meta_Dir_ID");
            References(x => x.RefColumn, "Meta_Dir_Column_Par_ID");
        }
    }
}

//Meta_Dir_Column_ID
//
//Meta_Dir_Column_Par_ID
//Name
//
//Sort
//Visible
//Type