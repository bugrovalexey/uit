﻿using System;
using System.Collections.Generic;
using GB.Data.Plans;

namespace GB.DataAccess.Services.Args
{
    public class ActOfAcceptanceArgs : BaseArgsNew
    {
        public ActOfAcceptanceArgs()
        {
            //WorksAndGoodsIds = new List<int>();
            ActToActivityIds = new List<int>();
        }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string DocName { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string DocNumber { get; set; }

        /// <summary>
        /// Дата принятия документа
        /// </summary>
        public DateTime? DocDateAdoption { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public string Files { get; set; }

        /// <summary>
        /// Стоимость 
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Госконтракт
        /// </summary>
        public int GovContractId { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public ActivityTypeEnum? ActivityType { get; set; }

        /// <summary>
        /// Причины отклонений от ожидаемых результатов
        /// </summary>
        public string ReasonsWaitingResultsDeviation { get; set; }

        public List<int> ActToActivityIds { get; set; } 

    }
}
