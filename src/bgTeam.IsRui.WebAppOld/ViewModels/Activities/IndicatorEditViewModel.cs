﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.WebApp.Helpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public class TableIndicator : NotificationBaseViewModel
    {
        public TableIndicator(PlansActivityIndicator entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            ExplanationRelationshipWithProjectMark = entity.ExplanationRelationshipWithProjectMark;
            UnitName = entity.Unit.Return(unit => unit.Name, default(string));
            BaseValue = entity.BaseValue;
            TargetedValue = entity.TargetedValue;
            DateToAchieveGoalValueStr = entity.DateToAchieveGoalValue.Return(date => date.ToString("dd.MM.yyyy"), default(string));
        }

        public int Id { get; set; }

        [DisplayName(IndicatorEditViewModel.ExplanationRelationshipWithProjectMarkCaption)]
        public string ExplanationRelationshipWithProjectMark { get; set; }

        [DisplayName(IndicatorEditViewModel.NameCaption)]
        public string Name { get; set; }

        [DisplayName(IndicatorEditViewModel.AlgorithmOfCalculatingCaption)]
        public string AlgorithmOfCalculating { get; set; }

        [DisplayName(IndicatorEditViewModel.BaseValueCaption)]
        public int? BaseValue { get; set; }

        [DisplayName(IndicatorEditViewModel.TargetedValueCaption)]
        public int? TargetedValue { get; set; }

        [DisplayName(IndicatorEditViewModel.UnitCaption)]
        public string UnitName { get; set; }

        [DisplayName(IndicatorEditViewModel.DateToAchieveGoalValueCaption)]
        public string DateToAchieveGoalValueStr { get; set; }

        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("DeleteIndicator", "Activities", new { id = Id }, "GridIndicators"); }
        }

        }

    [MetadataType(typeof(IndicatorEditViewModel_Valid))]
    public class IndicatorEditViewModel : ActivitiesCardBase
    {
        #region Consts

        public const string ExplanationRelationshipWithProjectMarkCaption = "Пояснение связи с показателем проекта";
        public const string NameCaption = "Наименование";
        public const string AlgorithmOfCalculatingCaption = "Алгоритм расчета";
        public const string BaseValueCaption = "Базовое значение";
        public const string TargetedValueCaption = "Целевое значение";
        public const string UnitCaption = "Единица измерения";
        public const string DateToAchieveGoalValueCaption = "Дата достижения целевого значения";

        #endregion Consts

        #region Ctors

        public IndicatorEditViewModel(IStatusRepository statusRepository, int activityId, int? id)
            : base(statusRepository, activityId)
        {
            UnitsList = new SelectList(new RepositoryDc<Units>().GetAll(), "Id", "Name");

            var entity = id.HasValue ? new BaseIndicatorRepository<PlansActivityIndicator>().Get(id) : null;
            entity.Do(Fill);
        }

        #endregion Ctors

        #region Methods

        protected void Fill(PlansActivityIndicator entity)
        {
            IndicatorId = entity.Id;
            Name = entity.Name;
            ExplanationRelationshipWithProjectMark = entity.ExplanationRelationshipWithProjectMark;
            UnitId = entity.Unit.Return(unit => unit.Id, EntityBase.Default);
            BaseValue = entity.BaseValue;
            TargetedValue = entity.TargetedValue;
            DateToAchieveGoalValue = entity.DateToAchieveGoalValue.ToStringOrDefault();
            AlgorithmOfCalculating = entity.AlgorithmOfCalculating;
        }

        #endregion Methods

        #region Props

        public int IndicatorId { get; set; }

        [DisplayName(ExplanationRelationshipWithProjectMarkCaption)]
        public string ExplanationRelationshipWithProjectMark { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(AlgorithmOfCalculatingCaption)]
        public string AlgorithmOfCalculating { get; set; }

        [DisplayName(BaseValueCaption)]
        public int? BaseValue { get; set; }

        [DisplayName(TargetedValueCaption)]
        public int? TargetedValue { get; set; }

        [DisplayName(UnitCaption)]
        public int UnitId { get; set; }

        [DisplayName(DateToAchieveGoalValueCaption)]
        public string DateToAchieveGoalValue { get; set; }

        public SelectList UnitsList { get; set; }

        #endregion Props
    }

    public class IndicatorEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int? BaseValue { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int? TargetedValue { get; set; }
    }
}