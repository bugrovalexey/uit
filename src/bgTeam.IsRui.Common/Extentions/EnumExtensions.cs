﻿namespace bgTeam.IsRui.Common.Extentions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using bgTeam.Extensions;

    public static class EnumExtensions
    {
        public static IEnumerable<KeyValuePair<int, string>> ToKeyValuePairs<TEnum>()
            where TEnum : struct, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum must be an enumerated type");
            }

            return (from x in Enum.GetValues(typeof(TEnum)).Cast<Enum>() select new KeyValuePair<int, string>(int.Parse(x.ToString("D")), x.GetDescription())).ToList();
        }
    }
}
