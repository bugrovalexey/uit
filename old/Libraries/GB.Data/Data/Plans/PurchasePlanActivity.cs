﻿namespace Uviri.MKRF.Entities.Plans
{
    public class PurchasePlanActivity : EntityBase
    {
        public virtual PurchasePlan PurchasePlan { get; set; }

        public virtual PlansActivity PlansActivity { get; set; }

        public virtual string ActivityName
        {
            get
            {
                return string.Format("{0} - {1}", PlansActivity.ActivityNum, PlansActivity.Name);
            }
        }

        public virtual PlansWork PlansWork { get; set; }


        public virtual string WorkName
        {
            get
            {
                return PlansWork != null ? string.Format("{0} - {1}", PlansWork.ActivityWorkNum, PlansWork.Name) : string.Empty;
            }
        }
    }
}
