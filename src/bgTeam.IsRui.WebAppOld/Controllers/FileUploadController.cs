﻿using bgTeam.IsRui.FileSystem;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public class FileUploadController : Controller
    {
        [HttpPost]
        public ActionResult Upload(object qqfile)
        {
            var files = qqfile as HttpPostedFileBase[];
            HttpPostedFileBase postedFile = files != null ? files.FirstOrDefault() : null;

            string fileName = postedFile == null ? ((string[])qqfile)[0] : postedFile.FileName;
            string generatedFileName = FileProvider.GenerateFileName();

            if (postedFile != null)
            {
                string fullPath = FileProvider.GetPhysicalPath(new Guid(generatedFileName));
                postedFile.SaveAs(fullPath);
            }
            else
            {
                FileProvider.SaveInTemp(Request.InputStream, generatedFileName);
            }

            //System.Threading.Thread.Sleep(1000);

            return Json(new { success = true, filename = fileName, guid = generatedFileName }, "text/html");
        }
    }
}