﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace GB.ESIA.Web.Helpers
{
	/// <summary>
	/// Класс доступа к сертификатам приложения.
	/// </summary>
	internal static class CertificateAccessor
	{
		private static X509Certificate2 serviceProviderCert;
		/// <summary>
		/// Сертифкат ServiceProvider'а.
		/// </summary>
		public static X509Certificate2 ServiceProviderCert
		{
			get
			{
				if (serviceProviderCert == null)
				{
					string relativePath = ConfigAccessor.ServiceProviderCertFile.TrimStart(new char[] {'\\'});
					string fileName = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
					serviceProviderCert = LoadCertificate(fileName, ConfigAccessor.ServiceProviderCertPassword);
				}
				return serviceProviderCert;
			}
		}

		private static X509Certificate2 identityProviderCert;
		/// <summary>
		/// Сертифкат IdentityProvider'а.
		/// </summary>
		public static X509Certificate2 IdentityProviderCert
		{
			get
			{
				if (identityProviderCert == null)
				{
					string relativePath = ConfigAccessor.IdentityProviderCertFile.TrimStart(new char[] { '\\' });
					string fileName = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
					identityProviderCert = LoadCertificate(fileName, null);
				}
				return identityProviderCert;
			}
		}

		/// <summary>
		/// Loads the certificate file.
		/// </summary>
		/// <param name="fileName">The certificate file name.</param>
		/// <param name="password">The password for this certificate file.</param>
		private static X509Certificate2 LoadCertificate(string fileName, string password)
		{
			if (!File.Exists(fileName))
				throw new ArgumentException(string.Format("Файл с сертификатом не найден: {0}.", fileName));
			try
			{
				return new X509Certificate2(fileName, password, X509KeyStorageFlags.MachineKeySet);
			}
			catch (Exception ex)
			{
				throw new ArgumentException(string.Format("Ошибка при загрузке сертификата из файла {0}: {1}", fileName, ex));
			}
		}

		/// <summary>
		/// Verifies the remote Secure Sockets Layer (SSL) certificate used for authentication.
		/// </summary>
		/// <param name="sender">Объект, который содержит информацию для проверки.</param>
		/// <param name="certificate">Сертифкат, который используется для аутентификации удалённого узла.</param>
		/// <param name="chain">Доп. информация связанная с сертификатом удалённого узла.</param>
		/// <param name="sslPolicyErrors">Одна или несколько ошибок, связанные с сертификатом удалённого узла.</param>
		/// <returns>true - сертификату можено доверять, false - в противном случае.</returns>
		private static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			// Всегда true - означает доверие всем сертификатам без исключения.
			// Это подходит для тестирования, в рабочей среде сертификат следует проверять.
			return true;
		}

		/// <summary>
		/// Конструктор.
		/// </summary>
		static CertificateAccessor()
		{
			// Назначение метода для проверки сертификатов.
			ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertificate;
		}
	}
}