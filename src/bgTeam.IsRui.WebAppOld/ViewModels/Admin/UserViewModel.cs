﻿using bgTeam.IsRui.Domain.Entities.Common;
using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    public class UserViewModel : ViewModelBase
    {
        public IList<TableUser> Items { get; private set; }

        public UserViewModel()
        {
            Items = new RepositoryDc<User>().GetAllEx(q => q
                    .Fetch(x => x.Department).Eager
                    .Where(x => x.IsActive))
                .Select(x => new TableUser(x))
                .ToList();
        }
    }
}