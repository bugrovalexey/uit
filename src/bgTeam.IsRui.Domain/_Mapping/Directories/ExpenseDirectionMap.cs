﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    internal class ExpenseDirectionMap : ClassMapHiLo<ExpenseDirection>
    {
        public ExpenseDirectionMap()
            : base("Expense_Direction")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "Expense_Direction_Code").Nullable();
            Map(x => x.Name, "Expense_Direction_Name").Nullable();
            Map(x => x.Code3, "Expense_Direction_Code3").Nullable();
            Map(x => x.Group, "Expense_Direction_Group").Nullable();
            Map(x => x.GroupExport, "Expense_Direction_Export_Group").Nullable();
            Map(x => x.DateStart, "Expense_Direction_Start_Date").Nullable();
            Map(x => x.DateEnd, "Expense_Direction_End_Date").Nullable();
            Map(x => x.Type, "Expense_Direction_Type").CustomType(typeof(ExpenseDirectionTypeEnum)).Nullable();
        }
    }
}