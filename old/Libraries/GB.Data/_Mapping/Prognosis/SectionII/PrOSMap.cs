﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Prognosis.SectionII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionII
{
    public class PrOSMap : ClassMapBase<PrOS>
    {
        public PrOSMap()
            : base("MF2_OS")
        {
//            Table("w_");

//            Id(x => x.Id, "MF2_OS_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF2_OS_Insert
//                          @MF2_OS_CA = ?
//                         ,@MF2_OS_TP = ?
//                         ,@MF2_OS_Comm = ?
//                         ,@OS_Type_ID = ?
//                         ,@MF2_ID = ?
//                         ,@MF2_OS_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF2_OS_Update
//                          @MF2_OS_CA = ?
//                         ,@MF2_OS_TP = ?
//                         ,@MF2_OS_Comm = ?
//                         ,@OS_Type_ID = ?
//                         ,@MF2_ID = ?
//                         ,@MF2_OS_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF2_OS_Delete
//						  @MF2_OS_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.CountCA, "MF2_OS_CA");
            Map(x => x.CountTP, "MF2_OS_TP");
            Map(x => x.Comment, "MF2_OS_Comm");

            References<OSType>(x => x.OS, "OS_Type_ID").Not.Nullable();
            References<TechInfo>(x => x.TechInfo, "MF2_ID").Not.Nullable();
        }
    }
}
