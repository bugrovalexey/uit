﻿namespace GB.MKRF.Entities.Documents
{
    /// <summary>
    /// Тип бумаги для плана
    /// </summary>
    public class PaperType : EntityBase
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }
    }
}