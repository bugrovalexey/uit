﻿using System.ComponentModel;

namespace bgTeam.IsRui.Domain.Entities.Directories
{
    /// <summary>
    /// Типы обеспечения
    /// </summary>
    public enum CategoryTypeOfSupportEnum
    {
        [Description("Не задано")]
        None = 0,

        [Description("Программное обеспечение")]
        Software = 1,

        [Description("Техническое обеспечение")]
        TechnicalSupport = 2,

        [Description("Работы/Услуги")]
        WorksAndServices = 3
    }
}