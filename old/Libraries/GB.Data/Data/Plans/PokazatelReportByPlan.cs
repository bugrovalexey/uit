﻿namespace Uviri.MKRF.Entities.Plans
{
    public class PokazatelReportByPlan: EntityBase
    {
        public virtual OfferByPlan OfferByPlan { get; set; }
        public virtual string Caption { get; set; }
        public virtual string Algoritm {get;set;}
        public virtual string Unit { get; set; }
        public virtual string PlanValue { get; set; }
        public virtual string BaseValue { get; set; }
        /// <summary>
        /// 1 - индикатор, 0 - показатель
        /// </summary>
        public virtual int FeatureType { get; set; }
    }
}
