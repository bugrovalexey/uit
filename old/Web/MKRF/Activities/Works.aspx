﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Activities/EditCard.master" AutoEventWireup="true" CodeBehind="Works.aspx.cs" Inherits="GB.MKRF.Web.Activities.Works" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function GridSave() {
            hf.Set('Name', NameTextBox.GetText());

            hf.Set('OKPD', OKPDComboBox.GetValue());
            hf.Set('ExpenseDirection', ExpenseDirectionComboBox.GetValue());
            hf.Set('ExpenseType', ExpenseTypeComboBox.GetValue());
            hf.Set('KOSGU', ExpenditureComboBox.GetValue());
            hf.Set('OKVED', OKVEDComboBox.GetValue());
            hf.Set('Year', YearComboBox.GetValue());

            hf.Set('SpecCount', SpecCountTextBox.GetText());
            hf.Set('Salary', SalaryTextBox.GetText());
            hf.Set('Duration', DurationTextBox.GetText());
        }

        function validateComboBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && e.value != 0;
        }

        function validateTextBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && e.value != '0' && e.value != '0.00' && e.value != '0,00';
        }

        function WorkCallbackComplete(s, e) {
            SalaryTextBox.SetText(e.result);
        }
    </script>
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
    <dxe:ASPxButton ID="addButton" runat="server" ClientInstanceName="addButton" AutoPostBack="false"
        Text="Добавить" CssClass="topButton fleft">
        <ClientSideEvents Click="function(s,e) { Grid.AddNewRow(); }" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="Grid" KeyFieldName="Id" Width="100%" AutoGenerateColumns="False"
        ClientInstanceName="Grid" OnBeforePerformDataSelect="Grid_BeforePerformDataSelect"
        OnStartRowEditing="Grid_StartRowEditing" OnRowInserting="Grid_RowInserting"
        OnRowUpdating="Grid_RowUpdating" OnRowDeleting="Grid_RowDeleting">
        <Settings ShowFilterRow="true" />
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="true" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Наименование" />
            <dxwgv:GridViewDataTextColumn FieldName="OKPD" Caption="Код ОКПД" />
            <dxwgv:GridViewDataTextColumn FieldName="ExpenseDirection" Caption="Вид затрат" />
            <dxwgv:GridViewDataTextColumn FieldName="ExpenseType" Caption="Тип затрат" />
            <dxwgv:GridViewDataTextColumn FieldName="ExpenditureItem" Caption="КОСГУ" />
            <dxwgv:GridViewDataTextColumn FieldName="OKVED" Caption="ОКВЭД" />
            <dxwgv:GridViewDataComboBoxColumn FieldName="Year" Caption="Год" Name="yearColumn">
                <PropertiesComboBox TextField="Value" ValueField="Key" ValueType="System.Int32" />
            </dxwgv:GridViewDataComboBoxColumn>
            <dxwgv:GridViewDataTextColumn FieldName="SpecialistsCount" Caption="Число специалистов" />
            <dxwgv:GridViewDataTextColumn FieldName="Salary" Caption="Среднемесячная зарплата" />
            <dxwgv:GridViewDataTextColumn FieldName="Duration" Caption="Длительность работ, мес." />
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc required">Наименование
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="NameTextBox" runat="server" ClientInstanceName="NameTextBox" OnDataBinding="NameTextBox_DataBinding"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Код ОКПД
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="OKPDComboBox" runat="server" ClientInstanceName="OKPDComboBox"
                                TextField="CodeName" ValueField="Id" ValueType="System.Int32" OnDataBinding="OKPDComboBox_DataBinding">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Вид затрат
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ExpenseDirectionComboBox" runat="server" ClientInstanceName="ExpenseDirectionComboBox" OnDataBinding="ExpenseDirectionComboBox_DataBinding"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                TextField="Name" ValueField="Id" ValueType="System.Int32">
                                <ClientSideEvents Validation="function(s,e) { validateComboBox(s, e); }" SelectedIndexChanged="function(s, e) { ExpenseTypeComboBox.PerformCallback(s.GetValue()); }" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Тип затрат
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ExpenseTypeComboBox" runat="server" ClientInstanceName="ExpenseTypeComboBox"
                                OnDataBinding="ExpenseTypeComboBox_DataBinding" OnCallback="ExpenseTypeComboBox_Callback"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                TextField="Name" ValueField="Id" ValueType="System.Int32">
                                <ClientSideEvents Validation="function(s,e) { validateComboBox(s, e); }" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">КОСГУ
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ExpenditureComboBox" runat="server" ClientInstanceName="ExpenditureComboBox"
                                OnDataBinding="ExpenditureComboBox_DataBinding"
                                TextField="CodeName" ValueField="Id" ValueType="System.Int32">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">ОКВЭД
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="OKVEDComboBox" runat="server" ClientInstanceName="OKVEDComboBox"
                                OnDataBinding="OKVEDComboBox_DataBinding"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                TextField="CodeName" ValueField="Id" ValueType="System.Int32">
                                <ClientSideEvents Validation="function(s,e) { validateComboBox(s, e); }"
                                    SelectedIndexChanged="function(s, e) { callBack.PerformCallback(s.GetValue()); }" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Год
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="YearComboBox" runat="server" ClientInstanceName="YearComboBox"
                                OnDataBinding="YearComboBox_DataBinding"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                TextField="Value" ValueField="Key" ValueType="System.Int32">
                                <ClientSideEvents Validation="function(s,e) { validateComboBox(s, e); }" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Число специалистов
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="SpecCountTextBox" runat="server" ClientInstanceName="SpecCountTextBox"
                                MaskSettings-Mask="<0..999999999>" OnDataBinding="SpecCountTextBox_DataBinding"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ClientSideEvents Validation="function(s,e) { validateTextBox(s, e); }" />
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Среднемесячная зарплата, руб.
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="SalaryTextBox" runat="server" ClientInstanceName="SalaryTextBox"
                                MaskSettings-Mask="<0..999999999g>.<00..99>"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                OnDataBinding="SalaryTextBox_DataBinding">
                                <ClientSideEvents Validation="function(s,e) { validateTextBox(s, e); }" />
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Длительность работ, мес.
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="DurationTextBox" runat="server" ClientInstanceName="DurationTextBox"
                                MaskSettings-Mask="<0..999999999g>" OnDataBinding="DurationTextBox_DataBinding"
                                ValidationSettings-ErrorDisplayMode="Text" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ClientSideEvents Validation="function(s,e) { validateTextBox(s, e); }" />
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok"><a onclick="Grid.UpdateEdit();">Сохранить</a></span>
                            <span class="cancel"><a onclick="Grid.CancelEdit();">Отмена</a></span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e) { if (e.command=='UPDATEEDIT') GridSave(); }" />
    </dxwgv:ASPxGridView>

    <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" OnCallback="callBack_Callback">
        <ClientSideEvents CallbackComplete="function(s,e) { WorkCallbackComplete(s,e); }" />
    </dxc:ASPxCallback>
</asp:Content>