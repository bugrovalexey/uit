﻿using GB.Data.Frgu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB._Mapping.Frgu
{
    class GovServicePassportMap : ClassMapHiLo<GovServicePassport>
    {
        public GovServicePassportMap()
            : base("Gov_Passport") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "Gov_Passport_Name");
            Map(x => x.SName, "Gov_Passport_SName");
            Map(x => x.Number, "Gov_Passport_Number");
            Map(x => x.OGV, "Gov_Passport_OGV");

            HasMany<GovService>(x => x.Services).KeyColumns.Add("Gov_Passport_ID").Inverse().Cascade.AllDeleteOrphan();
        }
    }
}
