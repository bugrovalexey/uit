﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/AdminMaster.master" AutoEventWireup="true" CodeBehind="Logs.aspx.cs" Inherits="GB.MKRF.Web.Administrator.Logs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
     <script type="text/javascript">
         function getRootWebSitePath() {
             var _location = document.location.toString();
             var applicationNameIndex = _location.indexOf('/', _location.indexOf('://') + 3);
             var applicationName = _location.substring(0, applicationNameIndex) + '/';
             var webFolderIndex = _location.indexOf('/', _location.indexOf(applicationName) + applicationName.length);
             var webFolderFullPath = _location.substring(0, webFolderIndex);

             return webFolderFullPath;
         }

         function rowClick(s, e) {
             var file = s.GetRowKey(e.visibleIndex);
             window.open(getRootWebSitePath() + "/Administrator/Log.aspx?file=" + file);
         }
     </script>
    <dxe:ASPxButton runat="server" ID="btnDelete" Text="Удалить все" AutoPostBack="false" CausesValidation="true" >
        <ClientSideEvents Click="function(s,e){ if(confirm('Подтверждаете удаление? ')) grid.PerformCallback('delete');  }" />
    </dxe:ASPxButton>

    <dxwgv:ASPxGridView runat="server" ID="grid" ClientInstanceName="grid" AutoGenerateColumns="False" Width="100%"
        KeyFieldName="File" OnBeforePerformDataSelect="grid_BeforePerformDataSelect" OnRowDeleting="grid_RowDeleting" OnCustomCallback="grid_CustomCallback"  >
        <Columns>
             <dxwgv:GridViewBandColumn Caption=" " ShowInCustomizationForm="false">
                <Columns>
                    <dxwgv:GridViewCommandColumn Caption=" "  Name="delColumn" ShowInCustomizationForm="False" AllowDragDrop="False"  ButtonType="Image">
                        <DeleteButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/delete_16.png" Image-ToolTip="Удалить" />
                    </dxwgv:GridViewCommandColumn>
                </Columns>
                <HeaderStyle Border-BorderWidth="0px"></HeaderStyle>
            </dxwgv:GridViewBandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Файл" FieldName="File" />
        </Columns>
        <Settings ShowFilterRow="True" />
        <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
        <ClientSideEvents RowClick="rowClick" />
    </dxwgv:ASPxGridView>

</asp:Content>
