﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807100054)]
    public class Migration_201807100054 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("ClassificationСategory")
                .WithColumn("ClassificationСategory_ID").AsInt32().NotNullable().PrimaryKey()
                .WithColumn("CC_Name").AsString().NotNullable()
                .WithColumn("CC_Parent_ID").AsInt32().Nullable();
        }
    }
}
