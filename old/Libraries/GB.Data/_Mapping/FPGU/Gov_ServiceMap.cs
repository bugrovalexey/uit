﻿
using GB;
using GB.Entity.Directories.FPGU;

namespace GB.MKRF.Mapping.FPGU
{
    public class Gov_ServiceMap : ClassMapBase<Gov_Service>
    {
        public Gov_ServiceMap()
            : base("Gov_Service")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Gov_Service_Name");
            Map(x => x.Title, "Gov_Service_Title");
            Map(x => x.Number, "Gov_Service_Number");
            Map(x => x.Result, "Gov_Service_Result");
            
            References<Gov_Passport>(x => x.Passport, "Gov_Passport_ID");
            HasMany<NPA_Document>(x => x.Documents).KeyColumns.Add("Gov_Service_ID").Inverse().Cascade.AllDeleteOrphan();
        }
    }
}
