﻿using GB.Data.Plans;
using GB.DataAccess.Repository;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class PlansActivityMarkService : ServiceBase<PlansActivityMark>, IService<PlansActivityMark, PlansActivityMarkArgs>
    {
        public PlansActivityMark Create(PlansActivityMarkArgs args)
        {
            return SaveOrUpdate(new PlansActivityMark(), args);
        }

        public PlansActivityMark Update(PlansActivityMarkArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private PlansActivityMark SaveOrUpdate(PlansActivityMark entity, PlansActivityMarkArgs args)
        {
            entity = ConvertDtoToEntity.FillPlansActivityMarkFromDto(entity, args);
            base.SaveOrUpdate(entity);

            return entity;
        }

        public void Delete(PlansActivityMarkArgs args)
        {
            base.Delete((int)args.Id);
        }

        private BaseIndicatorRepository<PlansActivityMark> _repository = new BaseIndicatorRepository<PlansActivityMark>();

        public new BaseIndicatorRepository<PlansActivityMark> Repository
        {
            get { return _repository; }
        }
    }
}