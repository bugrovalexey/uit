﻿namespace bgTeam.IsRui.DataAccess.Repository
{
    using System.Linq;
    using System.Threading;
    using bgTeam.IsRui.Domain.Entities.Common;
    using Microsoft.AspNetCore.Http;

    public interface IUserRepository : IRepositoryDc<User>
    {
        User GetCurrent();

        void Refresh();
    }

    public class UserRepository : RepositoryDc<User>, IUserRepository
    {
        private User _user;
        private readonly IHttpContextAccessor _contextAccessor;

        public UserRepository(
            ISessionNHibernateFactory factory,
            IHttpContextAccessor contextAccessor)
                : base(factory)
        {
            _contextAccessor = contextAccessor;
        }

        public User GetCurrent()
        {
            if (_user != null && _contextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                return _user;
            }

            var login = _contextAccessor.HttpContext.User.Identity.Name;

            if (string.IsNullOrEmpty(login))
            {
                return null;
            }

            var user = this.Get(q => q
                .Fetch(x => x.Department).Eager
                .Fetch(x => x.Roles).Eager //в Get так можно, чтобы одним запросов всё считать
                .Where(x => x.Login == login));

            _user = user;

            return user;
        }

        public void Refresh()
        {
            var user = GetCurrent();

            this.Refresh(user);
        }

        public int CurrentUserId
        {
            get
            {
                var user = GetCurrent();
                return user != null ? user.Id : -1;
            }
        }

        //public bool IsAuthorized
        //{
        //    get { return GetCurrent() != null; }
        //}

        //private string CurrentUserLogin
        //{
        //    get
        //    {
        //        return _contextAccessor.HttpContext.User.Identity.IsAuthenticated.Name;

        //        //return (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null)
        //        //    ? Thread.CurrentPrincipal.Identity.Name : null;
        //    }
        //}
    }
}