﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimCreateOrUpdateScopeOfWorkStory : IStory<ClaimCreateOrUpdateScopeOfWorkStoryContext, ClaimScopeOfWorkDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapperBase;
        private readonly ISessionNHibernateFactory _factory;

        public ClaimCreateOrUpdateScopeOfWorkStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase,
            ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _repository = repositoryRui;
            _mapperBase = mapperBase;
            _factory = sessionNHibernateFactory;
        }

        public ClaimScopeOfWorkDto Execute(ClaimCreateOrUpdateScopeOfWorkStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ClaimScopeOfWorkDto> ExecuteAsync(ClaimCreateOrUpdateScopeOfWorkStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка: {context.ClaimId}");
            }

            ClaimScopeOfWork scope;

            if (context.Id.HasValue && context.Id.Value > 0)
            {
                scope = _repository.Get<ClaimScopeOfWork>(context.Id.Value);
                scope = _mapperBase.Map(context, scope);
            }
            else
            {
                scope = _mapperBase.Map(context, new ClaimScopeOfWork());
            }

            using (var session = _factory.OpenSession())
            {
                await session.SaveOrUpdateAsync(scope);
                await session.CommitAsync();
            }

            return _mapperBase.Map(scope, new ClaimScopeOfWorkDto());
        }
    }
}
