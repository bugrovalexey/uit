﻿using System;

namespace GB.Extentions
{
    public static class ExceptionExtensions
    {
        public static Exception GetInnerException(this Exception exp)
        {
            if (exp.InnerException != null)
                return GetInnerException(exp.InnerException);

            return exp;
        }
    }
}