﻿using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.Documents;
using GB.DataAccess.Command.Documents;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class AddOrSaveAOAdoptionBudgetCommand : ICommand<AddOrSaveAOAdoptionBudgetContext>
    {
        ISessionFactoryGb factory;

        public AddOrSaveAOAdoptionBudgetCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(AddOrSaveAOAdoptionBudgetContext context)
        {
            var entity = new Repository<AccountingObject>().GetExisting(context.AccountingObjectId);
            AutoMapper.Mapper.Map(context, entity);            

            using (var session = factory.Create())
            {
                session.Update(entity);
                session.Commit();
            }

            context.xId = entity.DocumentGroundsForCreation == null ? 0 : entity.DocumentGroundsForCreation.Id;
            var budget = new BaseAddOrSaveCommand<AddOrSaveAOAdoptionBudgetContext, AODocumentGroundsForCreation>().Execute(context);

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = context.Files,
                listDocs = budget.Documents,
                owner = budget,
                type = budget.Type
            });
        }
    }
}
