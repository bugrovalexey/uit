﻿namespace bgTeam.AdminDB.Web.Domain
{
    using bgTeam.DataAccess.Impl.Dapper;

    /// <summary>
    /// Предоставляет метаданные описывающие справочник
    /// </summary>
    [TableName("BG_Meta_Dictionaries")]
    public class MetaDictionary
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string TableName { get; set; }

        public int ModifyType { get; set; }

        public int Visible { get; set; }

        public int OrderBy { get; set; }

        [Ignore]
        public bool ReadOnly { get { return ModifyType == 0; } }

        [Ignore]
        public int AccessType { get; set; }

        [Ignore]
        public bool AddEnable { get { return (AccessType & (int)AccessTypeEnum.Add) == (int)AccessTypeEnum.Add; } }

        [Ignore]
        public bool EditEnable { get { return (AccessType & (int)AccessTypeEnum.Edit) == (int)AccessTypeEnum.Edit; } }

        [Ignore]
        public bool DeleteEnable { get { return (AccessType & (int)AccessTypeEnum.Delete) == (int)AccessTypeEnum.Delete; } }
    }
}