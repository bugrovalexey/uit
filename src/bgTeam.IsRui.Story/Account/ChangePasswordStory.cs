﻿namespace bgTeam.IsRui.Story.Account
{
    using bgTeam.IsRui.Common.Helpers;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ChangePasswordStory : IStory<ChangePasswordStoryContext, ChangePasswordDto>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IUserRepository _userRepository;

        public ChangePasswordStory(ISessionNHibernateFactory factory, IUserRepository userRepository)
        {
            _factory = factory;
            _userRepository = userRepository;
        }

        public ChangePasswordDto Execute(ChangePasswordStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ChangePasswordDto> ExecuteAsync(ChangePasswordStoryContext context)
        {
            var user = _userRepository.GetCurrent();

            var password = CryptoHelper.GetMD5Hash(context.CurrentPassword);

            if (password == user.Password)
            {
                user.Password = CryptoHelper.GetMD5Hash(context.NewPassword);

                using (var session = _factory.OpenSession())
                {
                    await session.UpdateAsync(user);
                }

                return new ChangePasswordDto() { Result = true, CurrentPassword = "Пароль изменён" };
            }
            else
            {

                return new ChangePasswordDto() { Result = false, CurrentPassword = "Неверный пароль" };
            }
        }
    }
}
