﻿using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class GovernmentContractViewModel : StepBaseViewModel 
    {
        public IEnumerable<TableGovernmentContract> Contracts { get; set; }

        public GovernmentContractViewModel(int id)
            : base(id, MasterItemEnum.GovernmentContract)
        {
            Contracts = new RepositoryDc<GovContract>().GetAllEx(query => query
                .Where(x => x.AccountingObject == Current))
                //.Where(x => x.Stage == type))
                .Select(x => new TableGovernmentContract(x));
        }
    }
}