﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateResponsibleServiceAccountingObjectContext : ICommandContext
    {
        public int DepartmentId { get; set; }
        public int ResponsibleCoord { get; set; }
        public int ResponsibleInformation { get; set; }
        public int xId { get; set; }
    }
}
