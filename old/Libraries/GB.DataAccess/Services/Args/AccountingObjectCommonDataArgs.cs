﻿using System;
using GB.Data.AccountingObjects;

namespace GB.DataAccess.Services.Args
{
    /// <summary>
    /// Общая информация
    /// </summary>
    public class AccountingObjectCommonDataArgs : BaseArgsNew
    {
        /// <summary>
        /// Стадия
        /// </summary>
        public AccountingObjectsStageEnum Stage { get; set; }

        /// <summary>
        /// Id Типа
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Id Вида
        /// </summary>
        public int KindId { get; set; }

        /// <summary>
        /// Id Статуса
        /// </summary>
        public int StatusId { get; set; }

        /// <summary>
        /// Полное наименование
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Идентификационный номер
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Цели создания ОУ
        /// </summary>
        public string Targets { get; set; }

        /// <summary>
        /// Назначение и область применения объекта учета
        /// </summary>
        public string PurposeAndScope { get; set; }

        #region Раздел "Сведения о принятие к бюджетному учету"

        /// <summary>
        /// Дата принятия к бюджетному учету
        /// </summary>
        public DateTime? AdoptionBudgetDate { get; set; }

        /// <summary>
        /// Код по БК КОСГУ
        ///  </summary>
        public int? ExpenditureItemId { get; set; }

        /// <summary>
        /// Балансовая стоимость
        /// </summary>
        public decimal BalanceCost { get; set; }

        /// <summary>
        /// Основания принятия к бюджетному учету
        /// </summary>
        public string AdoptionBudgetBasis { get; set; }

        #endregion Раздел "Сведения о принятие к бюджетному учету"

        #region Раздел "Сведения о вводе в эксплуатацию"

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string CommissioningDocName { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string CommissioningDocNumber { get; set; }

        /// <summary>
        /// Дата принятия документа
        /// </summary>
        public DateTime? CommissioningDocDateAdoption { get; set; }

        /// <summary>
        /// Пункт, статья документа
        /// </summary>
        public string CommissioningDocParagraph { get; set; }

        /// <summary>
        /// Введен в эксплуатацию
        /// </summary>
        public DateTime? CommissioningDate { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public string CommissioningFiles { get; set; }

        #endregion Раздел "Сведения о вводе в эксплуатацию"

        #region Раздел "Сведения о выводе из эксплуатации"

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string DecommissioningDocName { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string DecommissioningDocNumber { get; set; }

        /// <summary>
        /// Дата принятия документа
        /// </summary>
        public DateTime? DecommissioningDocDateAdoption { get; set; }

        /// <summary>
        /// Выведен из эксплуатации
        /// </summary>
        public DateTime? DecommissioningDate { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public string DecommissioningFiles { get; set; }

        #endregion Раздел "Сведения о выводе из эксплуатации"
    }
}