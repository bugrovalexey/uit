﻿using GB.Data.AccountingObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB._Mapping.AccountingObjects
{
    class GovPurchaseMap : ClassMapHiLo<GovPurchase>
    {
        public GovPurchaseMap()
            : base("OU_Zakupki") { }

        protected override void InitMap()
        { 
            Map(x => x.Name, "OU_Zakupki_Name");
            Map(x => x.Number, "OU_Zakupki_Number");
            Map(x => x.Type, "OU_Zakupki_Type").CustomType<GovPurchaseTypeEnum>();
            Map(x => x.Date, "OU_Zakupki_Date");
            Map(x => x.Price, "OU_Zakupki_Cost");
            Map(x => x.IdZakupki360, "OU_Zakupki_IdZK");
            Map(x => x.Summ, "OU_Zakupki_Summ");

            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
        }
    }
}
