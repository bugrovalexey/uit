﻿using Uviri.Entity.Directories.Zakupki;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ReportContractMap : ClassMapBase<ReportContract>
    {
        public ReportContractMap() :
            base("Report_Contract")
        {

        }

        protected override void InitMap()
        {
            Map(x => x.IsChange, "Report_Contract_Is_Change");

            References<Contract>(x => x.Contract, "Contract_ID");
            References<PlansActivity>(x => x.PlansActivity, "Plans_Activity_ID");
            References<PlansStateContract>(x => x.PlansStateContract, "Plans_State_Contract_ID");
        }
    }
}
