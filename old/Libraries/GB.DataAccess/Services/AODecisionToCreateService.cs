﻿using System;
using GB.Data.AccountingObjects;
using GB.Data.Documents;
using GB.DataAccess.Services.Args;
using GB.DataAccess.Command.Documents;

namespace GB.DataAccess.Services
{
    public class AODecisionToCreateService : ServiceBase<AODecisionToCreate>, IService<AODecisionToCreate, AODecisionToCreateArgs>
    {
        ISessionFactoryGb factory;

        public AODecisionToCreateService()
        {
            factory = new SessionFactory();
        }

        public AODecisionToCreate Create(AODecisionToCreateArgs args)
        {
            return SaveOrUpdate(new AODecisionToCreate(), args);
        }

        public AODecisionToCreate Update(AODecisionToCreateArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private AODecisionToCreate SaveOrUpdate(AODecisionToCreate entity, AODecisionToCreateArgs args)
        {
            entity = ConvertDtoToEntity.FillDocumentAccountingObjectFromDto(entity, args);

            base.SaveOrUpdate(entity);
            base.Refresh(entity);            

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = args.Files,
                listDocs = entity.Documents,
                owner = entity,
                type = DocTypeEnum.DecisionToCreateAccountingObject
            });

            base.SaveOrUpdate(entity);
            base.Refresh(entity);

            return entity;
        }

        public void Delete(AODecisionToCreateArgs args)
        {
            base.Delete((int)args.Id);
        }
    }
}