﻿using GB.MKRF.Entities.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Entities.Plans
{
    public class ActivityConception : EntityBase
    {
        protected ActivityConception() { }

        public ActivityConception(PlansActivity Activity)
        {
            this.Activity = Activity;
        }

        /// <summary>Приказ №</summary>
        public virtual int Order { get; set; }

        /// <summary>Дата приказа</summary>
        public virtual DateTime? OrderDate { get; set; }

        /// <summary>Концепция</summary>
        public virtual IList<Document> Documents { get; protected set; }

        /// <summary>Предпосылки реализации мероприятия</summary>
        public virtual int Name { get; set; }

        /// <summary>Цели мероприятия</summary>
        public virtual int Goal { get; set; }

        /// <summary>Задачи мероприятия</summary>
        public virtual int Task { get; set; }

        /// <summary>Мероприятие</summary>
        public virtual PlansActivity Activity { get; set; }
    }
}

