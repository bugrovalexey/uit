﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Uviri.MKRF.Web.Plans.v2012rel.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script>
        ActiveMenuItem = 2;
    </script>
    <style>
        .dxgvGroupRow_MetropolisBlue
        {
            height: 50px;
        }

        .dxgvCommandColumnItem_MetropolisBlue
        {
            display: inline;
            margin: 0px 5px;
        }
    </style>

    <div class="page">
        <dxhf:ASPxHiddenField runat="server" ID="hfCache" ClientInstanceName="hfCache"></dxhf:ASPxHiddenField>
        <dxwgv:ASPxGridView runat="server" ID="plansGrid" KeyFieldName="Id" Width="100%"
            ClientInstanceName="plansGrid" ViewStateMode="Disabled" AutoGenerateColumns="False"
            OnBeforePerformDataSelect="plansGrid_BeforePerformDataSelect" OnInit="plansGrid_Init" >
            <Columns>
                <dxwgv:GridViewDataColumn Caption=" " ></dxwgv:GridViewDataColumn>
                <dxwgv:GridViewDataTextColumn Caption="Год" FieldName="Year" Width="35px">
                    <Settings AllowHeaderFilter="True" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Наименование департамента" FieldName="DepartmentName">
                    <DataItemTemplate>
                        <%#GetNameLink(Eval("PlanId"), Eval("DepartmentName"), Eval("IsShow")) %>
                    </DataItemTemplate>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Статус" FieldName="Status" Width="200px">
                    <Settings AllowHeaderFilter="True" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Департаменты" ShowInCustomizationForm="False" FieldName="DepartmentName" Visible="false" Name="FieldForGrouping" />
                <dxwgv:GridViewDataTextColumn Caption="Ответственный" FieldName="UserInfo" Visible="false" />
                <dxwgv:GridViewDataTextColumn Caption="Composite" FieldName="Composite" Visible="False" ShowInCustomizationForm="False" />
            </Columns>
            <TotalSummary>
                <dxwgv:ASPxSummaryItem ShowInColumn="DepartmentName" DisplayFormat="Количество планов: {0}" SummaryType="Count" />
            </TotalSummary>
            <Settings ShowFilterRow="True" ShowHeaderFilterBlankItems="False" GridLines="Horizontal"
                ShowFooter="True" GroupFormat="{1} {2}" ShowGroupFooter="Hidden" ShowFilterRowMenu="true" />
            <SettingsBehavior ConfirmDelete="True" />
            <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
            <SettingsPager PageSize="50" />
            <SettingsEditing Mode="EditForm" />
            <SettingsCookies Enabled="true" StoreFiltering="true" />
            <Styles>
                <GroupRow Cursor="pointer" />
            </Styles>
            <ClientSideEvents
                BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') {hfSave(e);} }"
                EndCallback="function(s,e) {
                                    if(s.cpCache){
                                        hfCache.Set('t', s.cpCache);
                                    };
                                }"
                RowClick="function(s, e) {
                            if (plansGrid.IsGroupRowExpanded(e.visibleIndex))
                                plansGrid.CollapseRow(e.visibleIndex);
                            else
                                plansGrid.ExpandRow(e.visibleIndex);
                            }" />
        </dxwgv:ASPxGridView>
    </div>
</asp:Content>
