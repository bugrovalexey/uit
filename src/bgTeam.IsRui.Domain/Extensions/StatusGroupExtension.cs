﻿using bgTeam.IsRui.Domain.Entities.Workflow;

namespace bgTeam.IsRui.Domain.Extensions
{
    public static class StatusGroupExtension
    {
        public static bool IsEq(this StatusGroupEnum e, StatusGroup o)
        {
            return o.Id == (int)e;
        }
    }
}