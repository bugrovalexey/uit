﻿using System.IO;
using System.Security.Cryptography.Xml;
using System.Xml;

namespace GB.SmevBehaviorExtension
{
    class Canonicalizer
    {
        public static Stream Transform(XmlNode node)
        {
            MemoryStream nodeStream = new MemoryStream();
            StreamWriter stw = new StreamWriter(nodeStream);
            stw.Write(node.OuterXml);
            stw.Flush();

            nodeStream.Position = 0;
            XmlDsigExcC14NTransform transform = new XmlDsigExcC14NTransform();
            transform.LoadInput(nodeStream);
            MemoryStream canonicalStream = (MemoryStream)transform.GetOutput(typeof(MemoryStream));
            byte[] canonData = canonicalStream.ToArray();

            return canonicalStream;
        }
    }
}
