﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ServicesCreateOrUpdateServiceStoryContext
    {
        public int? Id { get; set; }

        /// <summary>Мероприятие</summary>
        public int? ActivityId { get; set; }

        /// <summary>Наименование</summary>
        public string Name { get; set; }

        /// <summary>Год</summary>
        public YearEnum Year { get; set; }

        /// <summary>Количество</summary>
        public int? Count { get; set; }

        /// <summary>Стоимость</summary>
        public double Cost { get; set; }

        /// <summary>БК КОСГУ </summary>
        //public virtual ExpenditureItem KOSGU { get; set; }
        public int? KOSGUId { get; set; }
    }
}
