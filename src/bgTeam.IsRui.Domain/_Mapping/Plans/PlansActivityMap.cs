﻿using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Documents;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Domain.Entities.Workflow;
using System;

namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    internal class PlansActivityMap : ClassMapHiLo<PlansActivity>
    {
        public PlansActivityMap()
            : base("Plans_Activity")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Name");
            Map(x => x.Num, "Plans_Activity_Num");

            Map(x => x.Code, "Plans_Activity_Code");

            Map(x => x.Subentry, "Plans_Activity_P_Subentry");

            Map(x => x.IsPriority, "Plans_Activity_Is_Priority");

            /*Map(x => x.PlansActivityVolY0, "Plans_Activity_P_Vol_Y0");
            Map(x => x.PlansActivityVolY1, "Plans_Activity_P_Vol_Y1");
            Map(x => x.PlansActivityVolY2, "Plans_Activity_P_Vol_Y2");
            Map(x => x.PlansActivityAddVolY0, "Plans_Activity_P_Add_Vol_Y0");
            Map(x => x.PlansActivityAddVolY1, "Plans_Activity_P_Add_Vol_Y1");
            Map(x => x.PlansActivityAddVolY2, "Plans_Activity_P_Add_Vol_Y2");
            Map(x => x.PlansActivityFactVolY0, "Plans_Activity_P_Fact_Vol_Y0");
            Map(x => x.PlansActivityFactVolY1, "Plans_Activity_P_Fact_Vol_Y1");
            Map(x => x.PlansActivityFactVolY2, "Plans_Activity_P_Fact_Vol_Y2");
            Map(x => x.PlansActivityFactDate, "Plans_Activity_P_Fact_Date");*/

            Map(x => x.PlanQuarter1, "Plans_Activity_PlanQuarter1").Nullable();
            Map(x => x.PlanQuarter2, "Plans_Activity_PlanQuarter2").Nullable();
            Map(x => x.PlanQuarter3, "Plans_Activity_PlanQuarter3").Nullable();
            Map(x => x.PlanQuarter4, "Plans_Activity_PlanQuarter4").Nullable();
            Map(x => x.ForecastQuarter1, "Plans_Activity_ForecastQuarter1").Nullable();
            Map(x => x.ForecastQuarter2, "Plans_Activity_ForecastQuarter2").Nullable();
            Map(x => x.ForecastQuarter3, "Plans_Activity_ForecastQuarter3").Nullable();
            Map(x => x.ForecastQuarter4, "Plans_Activity_ForecastQuarter4").Nullable();

            Map(x => x.DatePlacement, "Plans_Activity_Date_of_Placement");

            Map(x => x.AnnotationData, "Plans_Activity_Annotation_Data").CustomSqlType("nvarchar(max)").Length(Int32.MaxValue);

            //Map(x => x.ResponsibleStr, "Plans_Activity_ResponsibleStr");
            //Map(x => x.ResponsibleJob, "Plans_Activity_Responsible_Job");
            //Map(x => x.ResponsibleEmail, "Plans_Activity_Responsible_Email");
            //Map(x => x.ResponsiblePhone, "Plans_Activity_Responsible_Phone");

            Map(x => x.FgisInfoText, "Plans_Activity_Fgis_Info_Text");
            Map(x => x.IsAgree, "Plans_Activity_Is_Agree");
            Map(x => x.PlansActivityCoordId, "Plans_Activity_Coord_ID");
            Map(x => x.PlansActivityCoordCode, "Plans_Activity_Coord_Code");
            Map(x => x.BasedOn, "Plans_Activity_Old_ID");

            //Map(x => x.NameSmall, "Plans_Activity_SName");
            Map(x => x.Description, "Plans_Activity_Description");
            Map(x => x.ResponsibleDepartment, "Plans_Activity_Responsible_Organ");

            Map(x => x.Order, "Plans_Activity_Order");
            Map(x => x.OrderDate, "Plans_Activity_Order_Date");
            Map(x => x.Prerequisites, "Plans_Activity_Prereq");
            Map(x => x.Goal, "Plans_Activity_Goal");
            Map(x => x.Task, "Plans_Activity_Task");
            Map(x => x.Year, "Plans_Year");

            //TODO : перемапить на дату создания Create_DT, при переходе на новую БД
            Map(x => x.CreateDate, "Plans_Activity_Timestamp");

            References<Plan>(x => x.Plan, "Plans_ID");
            References<ActivityType>(x => x.ActivityType2, "Plans_Activity_Type2_ID");
            References<IKTComponent>(x => x.IKTComponent, "IS_or_IKT_ID");
            References<GRBS>(x => x.GRBS, "GRBS_ID");
            References<SectionKBK>(x => x.SectionKBK, "Section_KBK_ID");
            References<ExpenseItem>(x => x.ExpenseItem, "Expense_Item_ID");
            References<WorkForm>(x => x.WorkForm, "Work_Form_ID");
            References<Responsible>(x => x.Responsible, "Responsible_ID");
            References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
            References<Status>(x => x.FgisStatus, "FGIS_Status_ID");
            //References<SourceFunding>(x => x.SourceFunding, "Source_Funding_ID");
            //References<IKTObject>(x => x.IKTObject, "IKT_Object_ID");
            References<ExpenditureItem>(x => x.ExpenditureItem, "Expenditure_Items_ID");
            References<PlansActivity>(x => x.JoinedTo, "Plans_Activity_New_ID");

            References<Status>(x => x.Status, "Status_ID");
            References<User>(x => x.ResponsibleInformation, "Users_Razm_ID");
            References<User>(x => x.ResponsibleTechnical, "Users_Tech_ID");
            References<User>(x => x.ResponsibleFinancial, "Users_Fin_ID");
            References<User>(x => x.Signing, "Users_Podp_ID");
            //References<AccountingObject>(x => x.AccountingObject, "OU_ID");
            References<StateProgram>(x => x.StateProgram, "State_Programme_ID");
            References<StateProgram>(x => x.StateSubProgram, "State_Sub_Programme_ID");
            References<StateProgram>(x => x.StateMainEvent, "State_Main_Event_ID");

            References<ActivityExpertiseConclusion>(x => x.ExpertiseConclusion, "Activity_Expertise_Conclusion_ID");

            References<Entities.Common.Workflow>(x => x.Workflow, "WorkflowId");

            References<ArticleBudget>(x => x.BudgetItem, "Plans_Activity_AB_ID").Nullable();

            HasMany(x => x.Specifications).KeyColumns.Add("Plans_Activity_ID").Cascade.Delete().Inverse();
            HasMany(x => x.Works).KeyColumns.Add("Plans_Activity_ID").Cascade.Delete().Inverse();
            HasMany(x => x.ActivityWorks).KeyColumns.Add("Plans_Activity_ID").Cascade.Delete().Inverse();
            //HasMany(x => x.PlansActGTs).KeyColumns.Add("Plans_Activity_ID").Inverse();
            HasMany(x => x.PlansGoalIndicators).KeyColumns.Add("Plans_Activity_ID").Inverse();
            //HasMany(x => x.ActivityReasons).KeyColumns.Add("Plans_Activity_ID").Inverse();
            HasMany(x => x.ExpenseDirections).KeyColumns.Add("Plans_Activity_ID").Inverse();
            //HasMany(x => x.Functions).KeyColumns.Add("Plans_Activity_ID").Inverse();
            //HasMany(x => x.Zods).KeyColumns.Add("Plans_Activity_ID").Inverse();
            //HasMany(x => x.ActivityApfInfos).KeyColumns.Add("Plans_Activity_ID").Inverse();
            //HasMany(x => x.ExpectedResults).KeyColumns.Add("Plans_Activity_ID").Inverse();
            //HasMany(x => x.PlansStateContracts).KeyColumns.Add("Plans_Activity_ID").Inverse();
            HasMany(x => x.PlansActivityGoals).KeyColumns.Add("Plans_Activity_ID").Inverse();
            //HasMany(x => x.PlansInitialActivity).KeyColumns.Add("Plans_Activity_ID").Inverse();
            //HasMany(x => x.Lots).KeyColumns.Add("Plans_Activity_ID").Inverse();
            //HasMany(x => x.PlansZakupki).KeyColumns.Add("Plans_Activity_ID").Inverse();
            HasMany(x => x.Marks).KeyColumns.Add("Plans_Activity_ID").Cascade.Delete().Inverse();
            HasMany(x => x.Indicators).KeyColumns.Add("Plans_Activity_ID").Cascade.Delete().Inverse();
            HasMany(x => x.OtherReasons).KeyColumns.Add("Project_ID").Inverse();

            HasMany<Files>(x => x.Documents)
                   .KeyColumns.Add("Document_Owner_ID")
                   .Where("Portal_Table_ID = " + (int)EntityType.PlansActivity)
                   .Where("Doc_Type_ID = " + (int)DocTypeEnum.ActivityFEO)
                   .Inverse();

            HasMany<Files>(x => x.Logo)
                .KeyColumns.Add("Document_Owner_ID")
                   .Where("Portal_Table_ID = " + (int)EntityType.PlansActivity)
                   .Where("Doc_Type_ID = " + (int)DocTypeEnum.Logo)
                   .Inverse();

            HasMany<Files>(x => x.Conception)
                .KeyColumns.Add("Document_Owner_ID")
                   .Where("Portal_Table_ID = " + (int)EntityType.PlansActivity)
                   .Where("Doc_Type_ID = " + (int)DocTypeEnum.Conception)
                   .Inverse();

            HasOne(x => x.AoActivity).PropertyRef(x => x.Activity).Cascade.Delete();

            HasMany<ActivityDocuments>(x => x.ActivityDocuments)
                .KeyColumns.Add("Plans_Activity_ID").Cascade.Delete().Inverse();

            //ApplyFilter<SecurityFilter>("Plans_Activity_ID in (select * from Get_Object_List(:UserId, " + (int)TableTypeEnum.Activity + ", 'r'))");
        }
    }
}