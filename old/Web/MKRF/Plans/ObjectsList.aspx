﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="ObjectsList.aspx.cs" Inherits="GB.MKRF.Web.Plans.ObjectsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">

    <%--<script type="text/javascript">
        function hfDetailedSave(e) {
            hf.Set('Object', ObjComboBox.GetValue());
        }
    </script>--%>

    <dxhf:ASPxHiddenField runat="server" ID="ASPxHiddenField1" ClientInstanceName="hf" />

    <div class="page" style="position: relative">

        <div class="planinfo" style="position: absolute; top: -38px;">
            Карточка департамента
        </div>

        <table style="width:100%; padding-bottom: 20px; font-size: 1em;">
            <tr>
                <td style="padding-right: 20px; font-size: 1.5em;"><b><%=GetTitle(CurrentDepartment)%></b></td>
            </tr>
        </table>
        
    <%--    <p class="header_grid">
            Исполнители
        </p>
        <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
        <dxwgv:ASPxGridView runat="server" ID="executorsGrid" KeyFieldName="Id" 
            AutoGenerateColumns="False" Width="100%"
            OnBeforePerformDataSelect="executorsGrid_BeforePerformDataSelect">
            <Columns>
                <dxwgv:GridViewDataTextColumn Caption="ФИО" FieldName="UserName">
                    <Settings FilterMode="DisplayText" AutoFilterCondition="Contains" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Должность" FieldName="Job">
                    <Settings FilterMode="DisplayText" AutoFilterCondition="Contains" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Телефон" FieldName="Phone">
                    <Settings FilterMode="DisplayText" AutoFilterCondition="Contains" />
                </dxwgv:GridViewDataTextColumn>
            </Columns>
            <Settings ShowFilterRow="True" ShowHeaderFilterBlankItems="False" />
            <SettingsBehavior ConfirmDelete="True" AllowGroup="true" />
            <SettingsPager PageSize="100" />
            <SettingsEditing Mode="EditForm" />
        </dxwgv:ASPxGridView>--%>

       <%-- <p class="header_grid">
            Объекты планирования
        </p>
        <dxwgv:ASPxGridView runat="server" ID="ObjectsGrid" KeyFieldName="Id" Width="100%"
            AutoGenerateColumns="False" ClientInstanceName="ObjectsGrid" OnBeforePerformDataSelect="ObjectsGrid_BeforePerformDataSelect">
            <Settings ShowFilterRow="True" ShowHeaderFilterBlankItems="False" />
            <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
            <Columns>
                <dxwgv:GridViewDataTextColumn Caption="Вид объекта" VisibleIndex="0" ReadOnly="true"
                    ShowInCustomizationForm="True">
                    <Settings AllowDragDrop="False" />
                    <DataItemTemplate>
                        <%#getType((int)Eval("ObjType"))%>
                    </DataItemTemplate>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="PrtId" ShowInCustomizationForm="False" VisibleIndex="1" Visible="false" />
                <dxwgv:GridViewDataTextColumn FieldName="Year" ShowInCustomizationForm="True" VisibleIndex="2"
                    Caption="Год(-ы)">
                    <Settings AllowAutoFilter="True" AutoFilterCondition="Contains" FilterMode="DisplayText" />
                    <CellStyle HorizontalAlign="Center" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Name" ShowInCustomizationForm="True" VisibleIndex="4"
                    Caption="Наименование">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="True" AutoFilterCondition="Contains"
                        FilterMode="DisplayText" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption=" " VisibleIndex="5" ReadOnly="true" ShowInCustomizationForm="True"
                    Width="100px">
                    <Settings AllowDragDrop="False" />
                    <DataItemTemplate>
                        <%#getActionLink((int)Eval("Id"), (int)Eval("ObjType"))%>
                    </DataItemTemplate>
                </dxwgv:GridViewDataTextColumn>
            </Columns>
            <SettingsPager PageSize="20" />
        </dxwgv:ASPxGridView>--%>
    </div>
</asp:Content>
