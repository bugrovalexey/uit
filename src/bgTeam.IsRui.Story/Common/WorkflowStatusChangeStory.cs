﻿namespace bgTeam.IsRui.Story.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Domain.Entities.Workflow;

    public class WorkflowStatusChangeStory : IStory<WorkflowStatusChangeStoryContext, WorkflowDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;
        private readonly ISessionNHibernateFactory _factory;

        public WorkflowStatusChangeStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IUserRepository userRepository,
            ISessionNHibernateFactory factory)
        {
            _mapper = mapper;
            _repository = repository;
            _userRepository = userRepository;
            _factory = factory;
        }

        public WorkflowDto Execute(WorkflowStatusChangeStoryContext context)
        {
            return ExecuteAsync(context).GetAwaiter().GetResult();
        }

        public async Task<WorkflowDto> ExecuteAsync(WorkflowStatusChangeStoryContext context)
        {
            var list = await new WorkflowStatusGetNextListStory(_mapper, _repository, _userRepository)
                .ExecuteAsync(new WorkflowStatusGetNextListStoryContext()
                {
                    WorkflowId = context.WorkflowId
                });

            context.StatusId.CheckNull(nameof(context.StatusId));

            if (list.Any(x => x.Id == context.StatusId))
            {
                var wf = _repository.GetExisting<Workflow>(context.WorkflowId);

                wf.Status = new Status() { Id = context.StatusId.Value };

                using (var session = _factory.OpenSession())
                {
                    await session.UpdateAsync(wf);
                }

                if (wf.EntityType == 1 || wf.EntityType == 14)
                {
                    var concl = _repository.Get<WorkflowUserConclusion>(q => q
                        .Where(x => x.WorkflowId == wf.Id));

                    var history = new WorkflowUserConclusionHistory();

                    if (concl != null)
                    {
                         _mapper.Map(concl, history);

                        history.User = _repository.Get<User>(concl.UserId);

                        history.Status = _repository.Get<Status>(concl.StatusId);
                    }
                    else
                    {
                        history.Status = _repository.Get<Status>(wf.Status.Id);

                        history.User = _userRepository.GetCurrent();

                        history.Text = "Комментарий отсутствует";

                        history.TimeStamp = DateTime.Now;

                        history.WorkflowId = wf.Id;
                    }

                    using (var session = _factory.OpenSession())
                    {
                        await session.InsertAsync(history);
                    }

                    if (history.Documents != null)
                    {
                        foreach (Files files in history.Documents)
                        {
                            var file = _repository.Get<Files>(files.Id);

                            file.EntityOwner = history;

                            file.Type = DocTypeEnum.WorkflowUserConclusionHistory;

                            using (var session = _factory.OpenSession())
                            {
                                await session.UpdateAsync(file);
                            }
                        }
                    }

                    if (history.Id_Ref.HasValue)
                    {
                        using (var session = _factory.OpenSession())
                        {
                            await session.DeleteAsync<WorkflowUserConclusion>(history.Id_Ref.Value);
                        }
                    }
                }

                return _mapper.Map(wf, new WorkflowDto());
            }

            throw new ArgumentException("Вы не можите перевести бизнесс процесс в данный статус");
        }
    }
}
