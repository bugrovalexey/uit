﻿namespace bgTeam.IsRui.Story.Admin
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Users;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UserDeleteUserStory : IStory<UserDeleteUserStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;

        public UserDeleteUserStory(
            ISessionNHibernateFactory factory,
            IRepositoryRui repository)
        {
            _factory = factory;
            _repository = repository;
        }

        public bool Execute(UserDeleteUserStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(UserDeleteUserStoryContext context)
        {

            using (var session = _factory.OpenSession())
            {
                await session.DeleteAsync<UsersEntity>(context.Id.Value);
            }

            return true;
        }
    }
}
