﻿namespace GB.Entity.Directories.Zakupki
{
    /// <summary>Контракт</summary>
    public class Suppler : EntityBase
    {
        /// <summary>
        /// Тип участника:
        ///  P - Физическое лицо РФ,
        ///  PF - Физическое лицо иностранного государства,
        ///  U - Юридическое лицо РФ,
        ///  UF - Юридическое лицо иностранного государства
        /// </summary>
        public virtual string ParticipantType { get; set; }

        public virtual string INN { get; set; }

        public virtual string KPP { get; set; }

        /// <summary>
        /// Тип организационно -правовой формы.
        /// i97 - Автономная некоммерческая организация
        /// i73 - Автономное учреждение
        /// i60 - Акционерное общество
        /// i77 - Ассоциация крестьянских (фермерских) хозяйств
        /// i72 - Бюджетное учреждение
        /// i82 - Государственная корпорация
        /// ZAO - Закрытое акционерное общество
        /// i91 - Индивидуальный предприниматель
        /// i98 - Иное юридическое лицо
        /// i95 - Крестьянское (фермерское) хозяйство
        /// i96 - Некоммерческое партнерство
        /// i66 - Общество с дополнительной ответственностью
        /// OOO - Общество с ограниченной ответственностью
        /// i84 - Общественное движение
        /// i83 - Общественная и религиозная организация (объединение)
        /// i93 - Объединение юридических лиц (ассоциации и союзы)
        /// i99 - Организация без права юридического лица
        /// i78 - Орган общественной самодеятельности
        /// OAO - Открытое акционерное общество
        /// i92 - Паевой инвестиционный фонд
        /// i51 - Полное товарищество
        /// i85 - Потребительский кооператив
        /// i90 - Представительство и филиал
        /// i52 - Производственный кооператив
        /// i87 - Простое товарищество
        /// i89 - Прочие некоммерческие организации
        /// i76 - Садоводческое, огородническое или дачное некоммерческое товарищество
        /// i80 - Территориальное общественное самоуправление
        /// i64 - Товарищество на вере
        /// i94 - Товарищество собственников жилья
        /// i40 - Унитарное предприятие
        /// i41 - Унитарное предприятие, основанное на праве оперативного управления
        /// i42 - Унитарное предприятие, основанное на праве хозяйственного ведения
        /// i81 - Учреждение
        /// i88 - Фонд
        /// i48 - Хозяйственное товарищество и общество
        /// i71 - Частное учреждение
        /// i39 - Юридическое лицо, являющиеся коммерческой организацией
        /// i70 - Юридическое лицо, являющиеся некоммерческойорганизацией
        /// </summary>
        public virtual string OrganizationForm { get; set; }

        /// <summary>Идентификационный номер для ParticipantType.UF </summary>
        public virtual string IdNumber { get; set; }

        /// <summary>Дополнительный идентификационный номер для ParticipantType.UF </summary>
        public virtual string IdNumberExtension { get; set; }

        /// <summary> Имя организации </summary>
        public virtual string OrganizationName { get; set; } // varchar(4000)

        /// <summary> Цифровой код страны в ОКСМ </summary>
        public virtual string CountryCode { get; set; }

        /// <summary> Полное наименование страны </summary>
        public virtual string CountryFullName { get; set; }

        /// <summary> Адрес </summary>
        public virtual string FactualAddress { get; set; } // varchar(1024)

        /// <summary> Почтовый адрес </summary>
        public virtual string PostAddress { get; set; } // varchar(1024)

        /// <summary> Фамилия </summary>
        public virtual string ContactInfoLastName { get; set; } // varchar(50)

        /// <summary> Имя </summary>
        public virtual string ContactInfoFirstName { get; set; } // varchar(50)

        /// <summary> Отчество </summary>
        public virtual string ContactInfoMiddleName { get; set; } // varchar(50)

        /// <summary> Телефон контактного лица </summary>
        public virtual string ContactPhone { get; set; } // varchar(30)

        /// <summary> Дополнительная информация </summary>
        public virtual string AdditionalInfo { get; set; } // varchar(4000)

        /// <summary>
        /// Статусы поставщика (исполнителя, подрядчика)
        /// – 01, субъект малого предпринимательства
        /// – 02, учреждения уголовно-исправительной системы
        /// – 03, общероссийские общественные организации инвалидов
        /// </summary>
        public virtual string Status { get; set; } // varchar(4000)

        public virtual Contract Contract { get; set; }
    }
}