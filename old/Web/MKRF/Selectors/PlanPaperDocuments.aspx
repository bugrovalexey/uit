﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master"
    AutoEventWireup="true" CodeBehind="PlanPaperDocuments.aspx.cs" Inherits="GB.MKRF.Web.Selectors.PlanPaperDocuments" %>

<%@ Register Src="../Common/DocControl.ascx" TagName="DocControl" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript">
        function RowSelect() {
            <%=dc.GridClientName%>.GetSelectedFieldValues('Id;Name', OnGetSelectedFieldValues);
        }

        function OnGetSelectedFieldValues(selectedValues) {
            var idList = [];
            var textList = [];

            for (var i = 0; i < selectedValues.length; i++) {
                idList.push(selectedValues[i][0]);
                textList.push(selectedValues[i][1]);
            }
            window.parent.PopupClose(idList.join(';'), textList.join('<br />'));
        }

        var gridLoaded = false;
        function SelectRows(s) {

            if (gridLoaded)
                return;

            var ids = window.parent.GetIds();
            if (ids) {
                s.SelectRowsByKey(ids.split(';'));
            }

            gridLoaded = true;
        }


    </script>
    <uc1:DocControl ID="dc" runat="server" SelectionMode="true" />
    <a href="javascript:RowSelect()" class="command" style="padding-top: 20px; display: inline-block;">
        Выбрать
    </a>
</asp:Content>
