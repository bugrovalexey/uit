﻿using System;

namespace GB.MKRF.Entities.Plans
{
    /// <summary>Различие версий мероприятия плана</summary>
    public class PlansActivityCompare : EntityBase
    {
        protected PlansActivityCompare()
        {
        }

        public PlansActivityCompare(PlansActivity Activity, DateTime Date1, DateTime Date2)
        {
            this.Activity = Activity;
            this.Date1 = Date1;
            this.Date2 = Date2;
        }

        /// <summary>Мероприятие</summary>
        public virtual PlansActivity Activity { get; protected set; }

        /// <summary>Дата 1</summary>
        public virtual DateTime Date1 { get; protected set; }

        /// <summary>Дата 2</summary>
        public virtual DateTime Date2 { get; protected set; }

        /// <summary>Различия</summary>
        public virtual string Changes { get; set; }
    }
}