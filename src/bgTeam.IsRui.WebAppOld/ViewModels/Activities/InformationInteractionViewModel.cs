﻿using bgTeam.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using AOViewModels = bgTeam.IsRui.WebApp.ViewModels.AccountingObjects;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public class InformationInteractionViewModel : ActivitiesCardBase
    {
        public InformationInteractionViewModel(IStatusRepository statusRepository, int id)
            : base(statusRepository, id)
        {
            InteractionViewModel =
                new AOViewModels.InformationInteractionViewModel(
                    CurrentActivity.AccountingObject.Return(ao => (int?) ao.Id, default(int?)));
        }

        public AOViewModels.InformationInteractionViewModel InteractionViewModel { get; set; }
    }
}