﻿namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;

    internal class AccountingObjectActsMap : ClassMapHiLo<AccountingObjectActs>
    {
        public AccountingObjectActsMap()
       : base("OU_Acts") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Acts_Name");
            Map(x => x.Number, "OU_Acts_Number");
            Map(x => x.DateAdoption, "OU_Acts_Date_Adoption");
            Map(x => x.Cost, "OU_Acts_Cost");
            Map(x => x.FileId, "OU_Acts_FileId");
            Map(x => x.FileName, "OU_Acts_FileName");
            Map(x => x.ChangeDate, "TimeStamp");

            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
        }
    }
}
