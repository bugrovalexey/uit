﻿using GB.Data;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command
{
    public class BaseAddOrSaveCommand<TContext, TEntity> : ICommand<TContext, TEntity>
        where TEntity : EntityBase, new()
        where TContext : BaseAddOrSaveContext
    {
        ISessionFactoryGb factory;

        public BaseAddOrSaveCommand()
        {
            factory = new SessionFactory();
        }              

        public TEntity Execute(TContext context)
        {
            TEntity entity;

            if (context.xId.HasValue && context.xId != 0)
                entity = new Repository<TEntity>().Get(context.xId.Value);
            else
            {
                entity = new TEntity();
            }

            AutoMapper.Mapper.Map(context, entity);

            using (var session = factory.Create())
            {
                session.SaveOrUpdate(entity);
                session.Commit();
            }

            return entity;
        }
    }    
}
