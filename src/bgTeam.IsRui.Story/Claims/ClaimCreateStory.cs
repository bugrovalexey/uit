﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimCreateStory : IStory<ClaimCreateStoryContext, ClaimDto>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IMapperBase _mapper;
        private readonly IUserRepository _userRepository;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IStatusRepository _statusRepository;

        public ClaimCreateStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase,
            IUserRepository userRepository,
            ISessionNHibernateFactory sessionNHibernateFactory,
            IStatusRepository statusRepository)
        {
            _repositoryRui = repositoryRui;
            _mapper = mapperBase;
            _userRepository = userRepository;
            _factory = sessionNHibernateFactory;
            _statusRepository = statusRepository;
        }

        public ClaimDto Execute(ClaimCreateStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ClaimDto> ExecuteAsync(ClaimCreateStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var user = _userRepository.GetCurrent();
            var activity = _repositoryRui
                .Get<PlansActivity>(q =>
                    q.Where(x => x.Id == context.ActivityId)
                    .JoinQueryOver(x => x.Department)
                    .Where(x => x.Id == user.Department.Id)
                    .Fetch(x => x.AoActivity));

            if (activity == null)
            {
                throw new IsRuiException("Не найдено мероприятие");
            }

            var claims = _repositoryRui
                .GetAllEx<Claim>(q =>
                    q.Where(x => x.PlansActivity.Id == context.ActivityId));
            int number = 1;

            if (claims.Any())
            {
                number = claims.Max(x => int.Parse(x.Number)) + 1;
            }

            var claim = new Claim
            {
                Number = number.ToString(),
                PlansActivity = activity,
                AccountingObject = activity.AccountingObject,
                CreateDate = DateTime.Now,
                TimeStamp = DateTime.Now,
                Cost = 0M,
                InitiatorUser = user
            };

            using (var session = _factory.OpenSession())
            {
                await session.InsertAsync(claim);

                await new WorkflowCreateStory(_statusRepository)
                    .ExecuteAsync(new WorkflowCreateStoryContext()
                    {
                        Entity = claim,
                        Session = session,
                    });
                await session.CommitAsync();
            }

            return _mapper.Map(claim, new ClaimDto());
        }
    }
}
