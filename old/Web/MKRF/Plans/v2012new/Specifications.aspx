﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/v2012new/ActivityCard.master" AutoEventWireup="true" CodeBehind="Specifications.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.Specifications" %>
<%@ Import Namespace="GB.MKRF.Entities.Directories" %>
<%@ Register Src="~/Common/GridColumnManager.ascx" TagName="GridColumnManager" TagPrefix="gcm" %>
<%@ Register src="../../Common/DocumentControl.ascx" tagname="DocumentControl" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">

    <script type="text/javascript">
        function hfSaveSpec(e) {
            //hfSpec.Clear();
            hfSpec.Set('ExpenseDirection', ExpenseDirectionComboBox.GetValue());
            hfSpec.Set('ExpenditureItemsId', ExpenditureItemsComboBox.GetValue());
            hfSpec.Set('STType', STType_Combo.GetValue());
            hfSpec.Set('Qualifier', QualifierComboBox.GetValue());
            hfSpec.Set('Name', NameMemo.GetText());
            hfSpec.Set('OKDP', OKDPTextBox.GetText());
            hfSpec.Set('Analog', AnalogMemo.GetText());
            hfSpec.Set('Quantity', Quantity_SpinEdit.GetValue());
            hfSpec.Set('Cost', CostSpinEdit.GetValue());
            hfSpec.Set('AnalogPrice', AnalogPriceSpinEdit.GetValue());

            var ids = $('.STID');
            var nameList = $('.STName');
            var unionList = $('.STUnion');
            var valueList = $('.STValue');
            $.each(nameList, function (i, el) {                
                hfSpec.Set('STName_' + i, $(el).val());
                hfSpec.Set('STUnion_' + i, $(unionList[i]).val());
                hfSpec.Set('STValue_' + i, $(valueList[i]).val());
                if (ids.length >= i)
                    hfSpec.Set('STID_' + i, $(ids[i]).val());
            });
        }

        function hfTypesSpecSave(e) {
            hfSpec.Set('STName_TextBox', STName_TextBox.GetText());
            hfSpec.Set('STUnion_TextBox', STUnion_TextBox.GetText());
            hfSpec.Set('STValue_TextBox', STValue_TextBox.GetText());
        }

        function bcallback(e) {
            if (e.toUpperCase() == 'UPDATEEDIT') {
                hfSaveSpec(e);
            }
        }

        $(function () {
            //$("a.popup").click(function (e) {
            //    e.preventDefault();
            //    var url = $(this).attr("href");
            //    window.open(url, "_blank", "height=300,width=900,directories=0,location=0,menubar=0,status=0,toolbar=0,scrollbars=1,resizable=1");
            //});

            var GetTypeSpecTemplate = function () {
                return "<tr><td colspan='2' class='sep'></td></tr>" +
                    "<tr>" +
                    "<td class='form_edit_desc'>Наименование характеристики</td>" +
                    "<td class='form_edit_input'>" +
                    "<input type='text' id='STName' class='STName' style='width: 396px;' />" +
                    "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td class='form_edit_desc'>Ед.изм.характеристики</td>" +
                    "<td class='form_edit_input'>" +
                    "<input type='text' id='STUnion' class='STUnion' style='width: 396px;' />" +
                    "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td class='form_edit_desc'>Значение характеристики</td>" +
                    "<td class='form_edit_input'>" +
                    "<input type='text' id='STValue' class='STValue' style='width: 396px;' />" +
                    "</td>" +
                    "</tr>";
            };

            $(".addTypeSpec").live('click', function () {                
                var linkTr = $(this).closest('tr');
                linkTr.before(GetTypeSpecTemplate());
            });

        });

        function validateComboBox(s, e) {
            var cbValue = s.GetValue();
            e.isValid = cbValue && cbValue != 0;
        }

        //function UpdateButtonClear(name) {
        //    var val = hfSpec.Get(name + 'Id');
        //
        //    if (val && val != 0) {
        //        var btName = '#' + name + 'Memo';
        //        var bt = $(btName).parents('td.form_edit_input').find('div.ButtonClear');
        //
        //        bt.css('display', '');
        //    }
        //}
        //
        //SelectValue = function (e) {
        //    PopupControl.SetContentHtml("");
        //    var title;
        //    switch (e) {
        //        case 'ExpenditureItems':
        //            title = 'Выбирите БК КОСГУ';
        //            break;
        //        default:
        //            alert('error');
        //    }
        //
        //    PopupControl.SetContentUrl("../../Selectors/CatalogItems.aspx?year=<%= CurrentActivity.Plan.Year%>&type=" + e);
        //    PopupControl.RefreshContentUrl();
        //    PopupControl.SetHeaderText(title);
        //    PopupControl.Show();
        //};
        //
        //CatalogPopupClose = function (type, result) {
        //    PopupControl.Hide();
        //    var mass = result.split(';');
        //    if (mass[0]) hfSpec.Set(type + 'Id', mass[0]);
        //    if (mass[1]) hfSpec.Set(type + 'Code', mass[1]);
        //    if (mass[2]) hfSpec.Set(type + 'Name', mass[2]);
        //    var Memo = '#' + type + 'Memo';
        //    $(Memo).find('input').val(hfSpec.Get(type + 'Code') + ' ' + hfSpec.Get(type + 'Name'));
        //    UpdateButtonClear(type);
        //
        //    PopupControl.SetContentUrl("../../Selectors/CatalogItems.aspx?year=<%= CurrentActivity.Plan.Year%>&type=-999");
        //    PopupControl.RefreshContentUrl();
        //};
        //
        //
        //function ClearValue(s, name) {
        //    if (hfSpec.Contains(name + 'Id')) {
        //        //ShowConfirm(true);
        //    }
        //
        //    hfSpec.Set(name + 'Id', '0');
        //    hfSpec.Set(name + 'Name', '');
        //    hfSpec.Set(name + 'Code', '');
        //
        //    var Memo = '#' + name + 'Memo';
        //    $(Memo).find('input').val('- Не задано');
        //
        //    $(s).css('display', 'none');
        //};

    </script>
    <dxhf:ASPxHiddenField runat="server" ID="hfSpec" ClientInstanceName="hfSpec" />
    
    <p class="header_grid">
        Товары, приобретенные в рамках мероприятия
        <span class="subtitle">
            Заполняется на третьем этапе
        </span>
    </p>

    <dxe:ASPxButton ID="addSpecTypeButton" runat="server" ClientInstanceName="addSpecTypeButton" Visible="false"
        AutoPostBack="false" Text="Добавить" Width="80px" Style="margin-top: 10px; margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {treeList.StartEditNewNode();}" />
    </dxe:ASPxButton>   

    <div style="float: right;">
        <gcm:GridColumnManager ID="TreeColumnManager" runat="server" GridName="treeList" UserStyle="width:45px; top: 14px; position: relative;" />
    </div>

    <dxtl:ASPxTreeList ID="treeList" ClientInstanceName="treeList" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="Id" ParentFieldName="ParentId"
        OnNodeUpdating="treeList_NodeUpdating" OnNodeInserting="treeList_NodeInserting" OnStartNodeEditing="treeList_StartNodeEditing" 
        OnNodeDeleting="treeList_NodeDeleting" OnNodeDeleted="treeList_NodeDeleted" OnDataBinding="treeList_DataBinding" OnCustomCallback="treeList_CustomCallback"
        OnCommandColumnButtonInitialize="treeList_CommandColumnButtonInitialize" OnCustomSummaryCalculate="treeList_CustomSummaryCalculate" >
        <Settings GridLines="Horizontal" ShowColumnHeaders="True" ShowFooter="True"/>
        <SettingsBehavior ExpandCollapseAction="NodeDblClick" />
        <SettingsEditing Mode="PopupEditForm" ConfirmDelete="true" AllowRecursiveDelete="True" />
        <SettingsPopupEditForm Width="900px" Height="600px" Caption="Товары" VerticalAlign="WindowCenter" HorizontalAlign="Center" Modal="true"/>    
        <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />    
        <Styles>
            <Header Wrap="True" Font-Bold="True" HorizontalAlign="Center"></Header>
            <Cell Wrap="True"></Cell>
            <AlternatingNode Enabled="False" />
        </Styles>
        <Templates>
            <EditForm>
                <select id="typeSelectTemplate" class="typeSelect" style="width: 402px; display: none;">
                    <option value='0' selected='selected'>Не задано</option>
                </select>
                <table class="form_edit" >
                    <tr <%=this.GetTdStyle()%> >
                        <td class="form_edit_desc">Наименование оборудования, программного обеспечения:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo runat="server" ID="NameMemo" Rows="5" ClientInstanceName="NameMemo"
                                OnDataBinding="memo_DataBinding" />
                        </td>
                    </tr>
                    <tr <%=this.GetTdStyle()%> >
                        <td class="form_edit_desc">Тип оборудования (программного обеспечения):
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="QualifierComboBox" runat="server" ClientInstanceName="QualifierComboBox"
                                ValueType="System.Int32" ValueField="Key" TextField="Value" EncodeHtml="false" 
                                OnDataBinding="QualifierComboBox_DataBinding" />
                        </td>
                    </tr>
                    <tr <%=this.GetTdStyle()%> >
                        <td class="form_edit_desc">Код ОКДП:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="OKDPTextBox" runat="server" ClientInstanceName="OKDPTextBox"
                                OnDataBinding="OKDPTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr <%=this.GetTdStyle()%> >
                        <td class="form_edit_desc">Вид затрат:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox runat="server" ID="ExpenseDirectionComboBox" ClientInstanceName="ExpenseDirectionComboBox" 
                                ValueType="System.Int32" TextField="Value" ValueField="Key" EncodeHtml="false" 
                            
                                OnDataBinding="ExpenseDirectionComboBox_DataBinding" >
                                <ItemStyle Wrap="True" />
                                <ClientSideEvents SelectedIndexChanged="function (s, e){
                                        STType_Combo.PerformCallback(s.GetValue());
                                    }" Validation="function(s,e){validateComboBox(s,e);}"/>
                                
                            </dxe:ASPxComboBox>
                            <dxp:ASPxPanel runat="server" ID="LegendPanel" ClientInstanceName="LegendPanel" >
                                <PanelCollection>
                                    <dxp:PanelContent>
                                        <span style="color: red;">Необходимо добавить допустимые виды затрат на вкладке расходы</span>
                                        <br />
                                    </dxp:PanelContent>
                                </PanelCollection>
                                <ClientSideEvents Init="function(s,e) { var items = ExpenseDirectionComboBox.GetItemCount(); if(items <= 1) {LegendPanel.SetVisible(true);} else {LegendPanel.SetVisible(false);} } " />
                            </dxp:ASPxPanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Код по БК КОСГУ
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ExpenditureItemsComboBox" runat="server" ClientInstanceName="ExpenditureItemsComboBox" 
                                OnDataBinding="ExpenditureItemsComboBox_DataBinding" TextField="CodeName" ValueField="Id" ValueType="System.Int32" EncodeHtml="false" DropDownHeight="300px">
                            </dxe:ASPxComboBox>

                            <%--<div style="position: relative">
                                <dxe:ASPxTextBox ID="ExpenditureItemsMemo" runat="server" ClientInstanceName="ExpenditureItemsMemo" ClientIDMode="Static" Rows="2" ReadOnly="true" OnDataBinding="ExpenditureItemsMemo_DataBinding">
                                    <ClientSideEvents Validation="function(s,e){ validateMemo(s,e); }" />
                                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Expenses">
                                        <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                        <RegularExpression ErrorText="Обязательное поле" ValidationExpression="^[- Не задано]" />
                                    </ValidationSettings>
                                </dxe:ASPxTextBox>
                                <div class="ButtonSelected catalogButton" onclick="SelectValue('ExpenditureItems'); return false;"></div>
                                <div class="ButtonClear catalogButton" style="display: none;" onclick="ClearValue(this, 'ExpenditureItems'); return false;"></div>
                            </div>--%>
                        </td>
                    </tr>
                    <tr <%=this.GetTdStyle()%> >
                        <td class="form_edit_desc">Тип затрат:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="STType_Combo" runat="server" ClientInstanceName="STType_Combo"
                                TextField="Value" ValueField="Key" ValueType="System.Int32" EncodeHtml="false"
                                OnDataBinding="STType_Combo_DataBinding" 
                                OnCallback="STType_Combo_Callback">
                                 <ClientSideEvents Validation="function(s,e){validateComboBox(s,e);}" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr <%=this.GetTdStyle()%> >
                        <td class="form_edit_desc">Аналог для расчета стоимости оборудования (лицензии программного обеспечения):
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo runat="server" ID="AnalogMemo" Rows="5" ClientInstanceName="AnalogMemo"
                                OnDataBinding="memo_DataBinding" />
                        </td>
                    </tr>
                    <tr <%=this.GetTdStyle()%> >
                        <td class="form_edit_desc ">Количество оборудования (лицензий программного обеспечения), шт.:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxSpinEdit runat="server" ID="Quantity_SpinEdit" ClientInstanceName="Quantity_SpinEdit"
                                DecimalPlaces="0" AllowNull="False" MinValue="0" MaxValue="100000000" 
                                DisplayFormatString="#,##0" Increment="1" HorizontalAlign="Right" Number="0"
                                OnDataBinding="Quantity_SpinEdit_DataBinding" >
                                <ClientSideEvents Validation="function(s,e){ validateSpin(s,e); }" />
                            </dxe:ASPxSpinEdit>
                        </td>
                    </tr>
                    <tr <%=this.GetTdStyle()%> >
                        <td class="form_edit_desc ">Цена аналога, тыс. руб.
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxSpinEdit runat="server" ID="AnalogPriceSpinEdit" ClientInstanceName="AnalogPriceSpinEdit"
                                DecimalPlaces="4" AllowNull="False" MinValue="0" MaxValue="100000000" 
                                DisplayFormatString="#,##0.0000" Increment="100" HorizontalAlign="Right" Number="0"
                                OnDataBinding="spinEdit_DataBinding">
                                <ClientSideEvents Validation="function(s,e){ validateSpin(s,e); }" />
                            </dxe:ASPxSpinEdit>
                        </td>
                    </tr>
                    <tr <%=this.GetTdStyle()%> >
                        <td class="form_edit_desc ">Стоимость оборудования (лицензии программного обеспечения), тыс. руб.:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxSpinEdit runat="server" ID="CostSpinEdit" ClientInstanceName="CostSpinEdit"
                                DecimalPlaces="4" AllowNull="False" MinValue="0" MaxValue="100000000" 
                                DisplayFormatString="#,##0.0000" Increment="100" HorizontalAlign="Right" Number="0"
                                OnDataBinding="spinEdit_DataBinding">
                                <ClientSideEvents Validation="function(s,e){ validateSpin(s,e); }" />
                            </dxe:ASPxSpinEdit>
                        </td>
                    </tr>
                    <% if (controller.CurrentEdit != null) foreach (SpecType specType in controller.CurrentEdit.SpecType)
                    {%>
                    <tr>
                        <td colspan='2' class='sep'>
                            <input type='hidden' id='STID' class='STID' value="<%=specType.Id %>" />
                        </td>
                    </tr>
                    <tr>
                        <td class='form_edit_desc'>Наименование характеристики</td>
                        <td class='form_edit_input'>
                        <input type='text' id='STName' class='STName' style='width: 396px;' value="<%=specType.CharName %>" />
                        </td>
                    </tr>
                    <tr>
                        <td class='form_edit_desc'>Ед.изм.характеристики</td>
                        <td class='form_edit_input'>
                        <input type='text' id='STUnion' class='STUnion' style='width: 396px;' value="<%=specType.CharUnit %>" />
                        </td>
                    </tr>
                    <tr>
                        <td class='form_edit_desc'>Значение характеристики</td>
                        <td class='form_edit_input'>
                        <input type='text' id='STValue' class='STValue' style='width: 396px;' value="<%=specType.CharValue %>" />
                        </td>
                    </tr>

                    <% } %>
                    <tr>
                        <td colspan="2"><a href="javascript://" class="addTypeSpec">Добавить характеристики</a></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok"><dxtl:ASPxTreeListTemplateReplacement ID="ReasonGridTemplateReplacement1" runat="server" ReplacementType="UpdateButton" /></span>
                            <span class="cancel"><dxtl:ASPxTreeListTemplateReplacement ID="ReasonGridTemplateReplacement2" runat="server" ReplacementType="CancelButton" /></span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <Columns>
            <dxtl:TreeListCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True"></DeleteButton>
            </dxtl:TreeListCommandColumn>

            <dxtl:TreeListCommandColumn Caption=" " Name="updownColumn" ShowInCustomizationForm="False" Width="100">
                <CustomButtons>
                    <dxtl:TreeListCommandColumnCustomButton ID="TreeListUpButton" Text="вверх" />
                    <dxtl:TreeListCommandColumnCustomButton ID="TreeListDownButton" Text="вниз" />
                </CustomButtons>
            </dxtl:TreeListCommandColumn>

            <dxtl:TreeListTextColumn Caption="Код строки" FieldName="SpecNumber" ReadOnly="True" CellStyle-HorizontalAlign="Center" Visible="False" ShowInCustomizationForm="True" />
            <dxtl:TreeListTextColumn Caption="Номер" FieldName="ActivitySpecCode" ReadOnly="True" CellStyle-HorizontalAlign="Center" />
            <dxtl:TreeListTextColumn Caption="Наименование оборудования, программного обеспечения" FieldName="Name" />
            <dxtl:TreeListTextColumn Caption="Тип оборудования (программного обеспечения)" FieldName="QT"  />

            <dxtl:TreeListTextColumn Caption="Код по БК КОСГУ" FieldName="ExpenditureItemsCodeName" VisibleIndex="4" />
            <dxtl:TreeListTextColumn Caption="Код ОКДП" FieldName="OKDP_Text" Visible="False" ShowInCustomizationForm="True" />

            <dxtl:TreeListTextColumn Caption="Вид затрат" FieldName="ExpenseDirection_CodeName" />
            <dxtl:TreeListTextColumn Caption="Тип затрат" FieldName="ExpName" />


            <dxtl:TreeListTextColumn Caption="Аналог для расчета стоимости" FieldName="Analog" />
            <dxtl:TreeListTextColumn Caption="Количество, ед." FieldName="Quantity" />
            <dxtl:TreeListTextColumn Caption="Цена аналога, тыс. руб." FieldName="AnalogPrice" />
            <dxtl:TreeListTextColumn FieldName="Cost" Caption="Стоимость, тыс. руб." >
                <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
            </dxtl:TreeListTextColumn>
            <dxtl:TreeListCommandColumn VisibleIndex="99" Caption=" " ShowInCustomizationForm="False" Width="100" Name="copyColumn">
                <CustomButtons>
                    <dxtl:TreeListCommandColumnCustomButton ID="TreeListCloneButton" Text="Копировать" />
                </CustomButtons>
            </dxtl:TreeListCommandColumn>            
        </Columns>
        <Summary>
            <dxtl:TreeListSummaryItem SummaryType="Custom" FieldName="Cost" ShowInColumn="Cost" DisplayFormat="{0:#,##0.0000}"></dxtl:TreeListSummaryItem>
        </Summary>
        <ClientSideEvents BeginCallback="function(s, e) { bcallback(e.command); }" CustomButtonClick="function(s, e) { s.PerformCallback(e.nodeKey + ';' + e.buttonID) }"></ClientSideEvents>
    </dxtl:ASPxTreeList>
    <br />

    <p class="header_grid">
        Документы
        <span class="subtitle">
            Заполняется на третьем этапе
        </span>
    </p>
    <uc1:DocumentControl ID="dc" runat="server" DocType="Spec" />

     <dxpc:ASPxPopupControl ID="PopupControl" runat="server" AllowDragging="True" AllowResize="True"
        CloseAction="CloseButton" Modal="True" EnableViewState="False" 
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="600px" 
        HeaderText="" ClientInstanceName="PopupControl" EnableHierarchyRecreation="True">
    </dxpc:ASPxPopupControl>

</asp:Content>