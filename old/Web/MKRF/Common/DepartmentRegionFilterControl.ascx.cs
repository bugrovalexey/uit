﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DevExpress.Web.ASPxEditors;
using Uviri.Helpers;
using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Repository;
using Uviri.Repository;

namespace Uviri.MKRF.Web.Common
{
	public partial class DepartmentRegionFilterControl : System.Web.UI.UserControl
	{
	    public void ShowDepartmentFilter(bool showFilter)
	    {
            DepartmentChooseLabel.Visible = showFilter;
            ParentChooseComboBox.Visible = showFilter;
	    }

        public void SetDepartmentLabel(string labelText)
        {
            DepartmentChooseLabel.Text = labelText;
            DepartmentChooseLabel.Visible = true;
        }

        public const int MSK_ID = 71;


		IList<DepartmentType> _departmentTypes = null;
		IList<Department> _rootDepartments = null;
		DataTable rootDepartmentsTable = null;

		public IList<DepartmentType> types
		{
			get
			{
				if (_departmentTypes == null)
					_departmentTypes = RepositoryBase<DepartmentType>.GetAll();
				return _departmentTypes;
			}
		}

        public int YearMin = 1900;


		//TODO: мерзкий костыль
		public IList<Department> rootParents
		{
			get
			{
				//if (_rootDepartments == null)
				//    _rootDepartments = new List<Department>();
				_rootDepartments = null;
				_rootDepartments = new List<Department>();

				//if (RoleEnum.MKS.IsEq(UserRepository.GetCurrent().Role) || RoleEnum.MinFin.IsEq(UserRepository.GetCurrent().Role))
				if (UserRepository.GetCurrent().Department.Parent == null && UserRepository.GetCurrent().Department.Childs.Count > 0 && RoleEnum.MKS.IsEq(UserRepository.GetCurrent().Role))
				{
					//_rootDepartments = RepositoryBase<Department>.GetAll(x => (x.Parent == null && x.Id > 0) //мкс
					//                                                              || (x.Id == 55)); //мзд

                    Department d = RepositoryBase<Department>.Get(MSK_ID);

                    d.Type = new DepartmentType((int)DepartmentTypeEnum.All);

                    _rootDepartments.Add(d); //мкс
					_rootDepartments.Add(RepositoryBase<Department>.Get(55)); //мзд
                }
                else if (RoleEnum.MinFin.IsEq(UserRepository.GetCurrent().Role) ||
                    RoleEnum.RKN.IsEq(UserRepository.GetCurrent().Role))
                {
                    //_rootDepartments = RepositoryBase<Department>.GetAll(x => x.Id == 71); //мкс
                    _rootDepartments.Add(RepositoryBase<Department>.Get(MSK_ID));//мкс
                }
                else if ((RoleEnum.Ministry_of_Economic_Development.IsEq(UserRepository.GetCurrent().Role)) || (RoleEnum.Government_Office.IsEq(UserRepository.GetCurrent().Role)))
                {
                    _rootDepartments.Add(RepositoryBase<Department>.Get(MSK_ID));//мкс
                }
                else
                {
                    _rootDepartments = null;
                }

				return _rootDepartments;
			}
		}

		private Dictionary<string, int> _values = new Dictionary<string, int>();
		public Dictionary<string, int> Values
		{
			get
			{
				_values.Clear();
				if (ParentChooseComboBox.SelectedItem != null)
				{
					_values.Add("depId", Convert.ToInt32(ParentChooseComboBox.SelectedItem.Value.ToString().Split('$')[0]));
					_values.Add("typeId", Convert.ToInt32(ParentChooseComboBox.SelectedItem.Value.ToString().Split('$')[1]));
					_values.Add("depIndex", ParentChooseComboBox.SelectedIndex);
				}

				if (YearsComboBox.SelectedItem != null)
				{
					_values.Add("year", Convert.ToInt32(YearsComboBox.SelectedItem.Value));
					_values.Add("yearIndex", YearsComboBox.SelectedIndex);
				}

                _values.Add("stageId", (int)(StageFilterComboBox.Value ?? 1));

                // TODO: переделать фильтр убрать костыли и разобраться для кого нужен фильтр

                // отключаем начальную точку у роли "Ведомство", чтобы можно было смотреть планы от вышестоящих ведомств
				//if (_values.ContainsKey("depId") &&  RoleEnum.Department.IsEq(UserRepository.GetCurrent().Role))
				//    _values["depId"] = -1;


				return _values;
			}
		}

		public bool YearsVisible
		{
			get { return YearsComboBox.Visible; }
			set { YearsComboBox.Visible = value; }
		}

        public bool DepartmentVisible
        {
            get { return ParentChooseComboBox.Visible; }
            set { ParentChooseComboBox.Visible = value; }
        }

        public bool StageVisible
        {
            get { return StageFilterComboBox.Visible; }
            set { StageFilterComboBox.Visible = value; }
        }
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				PrepareRootDepartmentData();
				ParentChooseComboBox.DataBind();
				YearsComboBox.DataBind();
                StageFilterComboBox.DataBind();

				HttpCookie cookie = Request.Cookies["DepRegFilterValues"];
				if (cookie != null)
				{
					if (RoleEnum.MKS.IsEq(UserRepository.GetCurrent().Role))
					{
						ParentChooseComboBox.SelectedIndex = Convert.ToInt32(cookie["depIndex"]);
					}
                    else if (RoleEnum.MinFin.IsEq(UserRepository.GetCurrent().Role) || 
                        RoleEnum.RKN.IsEq(UserRepository.GetCurrent().Role))
					{
						ParentChooseComboBox.SelectedIndex = 0;
					}
					else
					{
						ParentChooseComboBox.SelectedIndex = Convert.ToInt32(cookie["depIndex"]);
						//ParentChooseComboBox.SelectedIndex = 0;
					}
					YearsComboBox.SelectedIndex = Convert.ToInt32(cookie["yearIndex"]);
                    if (StageFilterComboBox.Value == null)
                        StageFilterComboBox.Value = 1;
				}
				else
				{
					ParentChooseComboBox.SelectedIndex = 0;
                    YearsComboBox.Value = DateTime.Now.Year;

                    //YearsComboBox.SelectedIndex = 1;//2011
				}
			}
		}

        private void PrepareRootDepartmentData()
        {
            rootDepartmentsTable = new DataTable();

            rootDepartmentsTable.Columns.Add("Value", typeof(string));
            rootDepartmentsTable.Columns.Add("Name", typeof(string));

            if (rootParents != null)
            {
                foreach (Department d in rootParents.OrderBy(p => p.Parent))
                {
                    int complect = 0;
                    if (d.Parent == null)
                    {
                        var curRole = UserRepository.GetCurrent().Role;
                        if (!RoleEnum.MinFin.IsEq(curRole) &
                            !RoleEnum.RKN.IsEq(curRole) & 
                            !RoleEnum.Government_Office.IsEq(curRole) & 
                            !RoleEnum.Ministry_of_Economic_Development.IsEq(curRole))
                        {
                            //сюда попадает только мкс? если да, то - (int)DepartmentTypeEnum.Establishment
                            //rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", d.Id, d.Type.Id),
                            //                              string.Format("{0} - [{1}]", d.SName, Customized.GetDepartmentName()));
                            rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", d.Id, (int)DepartmentTypeEnum.Establishment),
                                                          string.Format("{0} - [{1}]", d.SName, "ОГВ"));
                            rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", d.Id, (int)DepartmentTypeEnum.Department),
                                                          string.Format("{0} - {1}", d.SName, "[Департаменты]"));
                        }
                        else
                        {
                            rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", d.Id, d.Type.Id),
                                                          string.Format("{0} - {1}", "", ""));
                        }
                    }
                    else
                    {
#if !REGION
                        if (DepartmentRepository.GetChildrenCountByDepartmentType(d, DepartmentTypeEnum.Establishment) > 0)
                        {
                            rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", d.Id, (int)DepartmentTypeEnum.Establishment),
                                                          string.Format("{0} - [{1}]", d.SName, "ОГВ"));
                            complect++;
                        }
                        if ((DepartmentRepository.GetChildrenCountByDepartmentType(d, DepartmentTypeEnum.Region) > 0) && 
                            !RoleEnum.MinFin.IsEq(UserRepository.GetCurrent().Role) && 
                            !RoleEnum.RKN.IsEq(UserRepository.GetCurrent().Role))
                        {
                            rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", d.Id, (int)DepartmentTypeEnum.Region),
                                                          string.Format("{0} - {1}", d.SName, "[Регионы]"));
                            complect++;
                        } if (DepartmentRepository.GetChildrenCountByDepartmentType(d, DepartmentTypeEnum.NotSpecified) > 0)
                        {
                            rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", d.Id, (int)DepartmentTypeEnum.NotSpecified),
                                                          string.Format("{0} - {1}", d.SName, "[Не задано]"));
                            complect++;
                        }
                        if (complect > 1)
                            rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", d.Id, (int)DepartmentTypeEnum.All),
                                                          string.Format("{0} - {1}", d.SName, "[Все]"));
#endif
                    }
                }
            }
            else if (RoleEnum.Expert.IsEq(UserRepository.GetCurrent().Role))
            {
                rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", -1, (int)DepartmentTypeEnum.All),
                                                      string.Format("{0} - {1}", "", ""));
            }
            else
            {
                if (UserRepository.GetCurrent().Department.Type.Id == (int)DepartmentTypeEnum.Department &&
                    UserRepository.GetCurrent().Department.Parent != null &&
                    UserRepository.GetCurrent().Department.Parent.Id == MSK_ID)
                {
                    rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", UserRepository.GetCurrent().Department.Id, (int)DepartmentTypeEnum.Self),
                            string.Format("{0} - {1}", UserRepository.GetCurrent().Department.SName, "[Свои]"));
                    return;
                }

                if (UserRepository.GetCurrent().Department.Type.Id == (int)DepartmentTypeEnum.Department)
                {
                    rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", UserRepository.GetCurrent().Department.Id, (int)DepartmentTypeEnum.Invited),
                      string.Format("{0} - [{1}]", UserRepository.GetCurrent().Department.Parent.SName, UserRepository.GetCurrent().Department.SName));
                    return;
                }

                rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", UserRepository.GetCurrent().Department.Id, (int)DepartmentTypeEnum.Self),
                        string.Format("{0} - {1}", UserRepository.GetCurrent().Department.SName, "[Свои]"));

                rootDepartmentsTable.Rows.Add(string.Format("{0}${1}", UserRepository.GetCurrent().Department.Id, (int)DepartmentTypeEnum.All),
                                        string.Format("{0} - {1}", UserRepository.GetCurrent().Department.SName, "[С подведомственными]"));
            }
        }

		protected void ParentChooseComboBox_DataBinding(object sender, EventArgs e)
		{
			ParentChooseComboBox.DataSource = rootDepartmentsTable;
		}

		protected void YearsComboBox_DataBinding(object sender, EventArgs e)
		{
            IList<Years> data = RepositoryBase<Years>.GetAll(x => x.Year >= YearMin);

			ASPxComboBox box = (ASPxComboBox)sender;
			box.DataSource = data;

			box.Value = DateTime.Now.Year;
		}

        protected void StageFilterComboBox_DataBinding(object sender, EventArgs e)
        {
            if (StageVisible)
            {
                IList<PlansStage> data = RepositoryBase<PlansStage>.GetAll();

                ASPxComboBox box = (ASPxComboBox)sender;
                box.DataSource = data;
            }
        }

		private void WriteCookie()
		{
			Response.Cookies.Add(CookieHelper.MakeCookie("DepRegFilterValues",
														 new Dictionary<string, string>() {
																							{"depId",Values["depId"].ToString()},
																							{"typeId",Values["typeId"].ToString()},
																							{"depIndex",Values["depIndex"].ToString()},
																							{"year",Values["year"].ToString()},
																							{"yearIndex",Values["yearIndex"].ToString()}
														 },
														 DateTime.Now.AddMonths(3)));
		}

		protected void filterButton_Click(object sender, EventArgs e)
		{
			WriteCookie();
		}
	}
}