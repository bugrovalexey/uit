﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Threading.Tasks;
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
    using bgTeam.IsRui.Domain.Entities.Directories;

    public class AoSoftwareAddOrUpdateSoftwareStory : IStory<AoSoftwareAddOrUpdateSoftwareStoryContext, AddOrUpdateSoftwareDto>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public AoSoftwareAddOrUpdateSoftwareStory(ISessionNHibernateFactory factory, IRepositoryRui repository, IMapperBase mapper)
        {
            _factory = factory;
            _repository = repository;
            _mapper = mapper;
        }

        public AddOrUpdateSoftwareDto Execute(AoSoftwareAddOrUpdateSoftwareStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<AddOrUpdateSoftwareDto> ExecuteAsync(AoSoftwareAddOrUpdateSoftwareStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));
            context.CategoryKindOfSupportId.CheckNull(nameof(context.CategoryKindOfSupportId));

            using (var session = _factory.OpenSession())
            {
                Software software;

                if (context.Id.HasValue)
                {
                    software = _repository.GetExisting<Software>(context.Id);
                    _mapper.Map(context, software);
                }
                else
                {
                    software = new Software();
                    _mapper.Map(context, software);

                    software.AccountingObject = new AccountingObject() { Id = context.AoId.Value };
                    software.CategoryKindOfSupport = new CategoryKindOfSupport() { Id = context.CategoryKindOfSupportId.Value };

                    if (context.Right != 0)
                    {
                        software.Right = context.Right;
                    }
                    else
                    {
                        software.Right = RightsOnSoftwareEnum.Exclusive;
                    }
                }

                software.Manufacturer = await GetManufactura(context);

                await session.SaveOrUpdateAsync(software);

                software.CharacteristicsOfLicense = await SaveCharacteristic(software, context, session);

                return _mapper.Map(software, new AddOrUpdateSoftwareDto());
            }
        }

        private async Task<CharacteristicOfLicense> SaveCharacteristic(Software software, AoSoftwareAddOrUpdateSoftwareStoryContext context, ISessionNHibernate session)
        {
            var characteristic = _repository.Get<CharacteristicOfLicense>(x => x.Where(y => y.Software.Id == context.Id));

            if (characteristic == null)
            {
                characteristic = new CharacteristicOfLicense();
            }
            else
            {
                context.CharacteristicOfLicenseId = characteristic.Id;
            }

            characteristic = _mapper.Map(context, characteristic);

            characteristic.Software = software;

            await session.SaveOrUpdateAsync(characteristic);

            return characteristic;
        }

        private async Task<Manufacturer> GetManufactura(AoSoftwareAddOrUpdateSoftwareStoryContext context)
        {
            if (context.ManufacturerId.HasValue)
            {
                return _repository.GetExisting<Manufacturer>(context.ManufacturerId);
            }

            return null;
        }
    }
}
