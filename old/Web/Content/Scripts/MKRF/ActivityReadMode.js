﻿$(function () {
    $('.item_title').live('click', function (e) {
        if (e.target.tagName == "IMG" || e.target.tagName == "A") {
            e.stopPropagation();
            return;
        }
        var self = $(this);
        if (self.hasClass('expanded')) {
            self.removeClass('expanded');
            self.addClass('collapsed');
            self.next('.item').slideUp();
        } else if (self.hasClass('collapsed')) {
            self.removeClass('collapsed');
            self.addClass('expanded');
            self.next('.item').slideDown();
        }
    });
    
    $('.showAll').live('click', function (e) {
        $('.item_title').removeClass('collapsed');
        $('.item_title').addClass('expanded');
        $('.item').slideDown();
    });
    
    $('.collapseAll').live('click', function (e) {
        $('.item_title').removeClass('expanded');
        $('.item_title').addClass('collapsed');
        $('.item').slideUp();
    });
});

function ToogleDetails(el) {
    var detailsContainer = el.parentNode.nextElementSibling;
    if (detailsContainer.style['display'] == 'none') {
        detailsContainer.style['display'] = 'block';
        addClass(el.parentNode, 'active');
    }
    else {
        detailsContainer.style['display'] = 'none';
        removeClass(el.parentNode, 'active');
    }
}

function addClass(o, c) {
    var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g");
    if (re.test(o.className)) return;
    o.className = (o.className + " " + c).replace(/\s+/g, " ").replace(/(^ | $)/g, "");
}

function removeClass(o, c) {
    var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g");
    o.className = o.className.replace(re, "$1").replace(/\s+/g, " ").replace(/(^ | $)/g, "");
}
