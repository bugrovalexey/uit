﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Documentation/Documentation.Master"
    AutoEventWireup="true" CodeBehind="Support.aspx.cs" Inherits="GB.MKRF.Web.Documentation.Support" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">

    <p class="header_grid">Сообщение администратору</p>
    
    <dxe:ASPxMemo runat="server" ID="MessageMemo" ClientInstanceName="MessageMemo" Width="100%" Rows="6" />
    
    <div style="text-align: center; margin: 17px 0;">
        <button class="green" 
            onclick="
                var text = $.trim(MessageMemo.GetText());

                if(text)
                    callBack.PerformCallback(MessageMemo.GetText()); 
                else
                    alert('Введите сообщение');

                MessageMemo.SetText(null);
                MessageMemo.SetFocus();
                return false;
            ">
        Отправить</button>
    </div>

    <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" oncallback="callBack_Callback">
        <ClientSideEvents 
            BeginCallback="function(s,e){ MainLoadingPanel.Show(); }" 
            EndCallback="function(s,e){ MainLoadingPanel.Hide(); }" 
            CallbackComplete="function(s,e){ alert('Сообщение отправлено.'); }"  />
        </dxc:ASPxCallback>

</asp:Content>
