﻿using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.Documents;
using GB.Data.Common;
using GB.Data.Documents;
using GB.Data.Plans;

namespace GB._Mapping.Documents
{
    internal class FilesMap : ClassMapHiLo<Files>
    {
        public FilesMap()
            : base("Document")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "Document_Num");
            Map(x => x.Date, "Document_DT");
            Map(x => x.Name, "Document_Name");
            Map(x => x.FileName, "Document_File_Name");
            Map(x => x.ContentType, "Document_Content_Type");
            Map(x => x.Comm, "Document_Comm");
            Map(x => x.IsSigned).Formula("case when DATALENGTH(Document_Sign_File) > 0 then 1 else 0 end").ReadOnly();
            //Map(x => x.SignedPackage, "Document_Sign_File").CustomType("BinaryBlob").Length(int.MaxValue).LazyLoad();
            Map(x => x.SignType, "Document_Sign_Type");
            Map(x => x.SignInfo, "Document_ECP");

            Map(x => x.DocTypeText, "Document_Doc_Type");
            Map(x => x.DocType, "Doc_Type_ID").CustomType(typeof(DocTypeEnum)).Not.Nullable();
            Map(x => x.DateCreate, "Document_Create_DT").ReadOnly();
            Map(x => x.DecisionGovComission, "Document_Is_Decision");

            Map(x => x.EntityType, "Portal_Table_ID").ReadOnly();

            ReferencesAny(x => x.Owner)
                .AddMetaValue<PlansActivity>(((int)EntityType.PlansActivity).ToString())
                .AddMetaValue<AccountingObject>(((int)EntityType.AccountingObject).ToString())
                .AddMetaValue<AODecisionToCreate>(((int)EntityType.DocumentAccountingObject).ToString())
                .AddMetaValue<Reason>(((int)EntityType.OtherReason).ToString())
                .AddMetaValue<SpecialCheck>(((int)EntityType.SpecialCheck).ToString())
                .AddMetaValue<AODocumentCommissioning>(((int)EntityType.AODocumentCommissioning).ToString())
                .AddMetaValue<AODocumentDecommissioning>(((int)EntityType.AODocumentDecommissioning).ToString())
                .AddMetaValue<AODocumentGroundsForCreation>(((int)EntityType.AODocumentGroundsForCreation).ToString())
                .AddMetaValue<ActOfAcceptance>(((int)EntityType.ActOfAcceptance).ToString())
                .AddMetaValue<AccountingObjectUserComment>(((int)EntityType.AccountingObjectCommentDocument).ToString())
                .EntityTypeColumn("Portal_Table_ID")
                .EntityIdentifierColumn("Document_Owner_ID").IdentityType<int>();

            References(x => x.Author, "Document_Creator").NotFound.Ignore().Not.Nullable().ReadOnly();
        }
    }
}