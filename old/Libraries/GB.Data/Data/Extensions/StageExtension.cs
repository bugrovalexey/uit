﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.AccountingObjects;

namespace GB.Data.Extensions
{
    public static class StageExtension
    {
        public static bool IsEq(this AccountingObjectsStageEnum s1, AccountingObjectsStageEnum s2)
        {
            return s1 == s2;
        }

    }
}
