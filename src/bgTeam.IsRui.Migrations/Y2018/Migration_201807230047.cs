﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807230047)]
    public class Migration_201807230047 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Plans_Activity_Goods")
                .AddColumn("Plans_Activity_Goods_Quarter").AsString(32).Nullable()
                .AddColumn("Plans_Activity_Goods_AnalogCost").AsDecimal().Nullable()
                .AddColumn("Plans_Activity_Goods_AnalogName").AsString(512).Nullable();
        }
    }
}
