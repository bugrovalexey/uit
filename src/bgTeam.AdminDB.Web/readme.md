# bgTeam.AdminDB

������������� ������� ����������������� ������������ ��� ��

## ���������

��������� ��������������� � ������� ���� ������, ������� �������� ���������� � 
- �������� (BG_Meta_Dictionaries) 
- �������� (BG_Meta_DictionariesColumns)

## ��������� ��� FluentMigrator
### �������� (BG_Meta_Dictionaries) 
```
Create.Table("BG_Meta_Dictionaries")
    .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
    .WithColumn("Name").AsString().NotNullable()
    .WithColumn("TableName").AsString().NotNullable()
    .WithColumn("ModifyType").AsInt32().NotNullable()
    .WithColumn("Visible").AsBoolean().NotNullable()
    .WithColumn("OrderBy").AsInt32().Nullable();
```
### �������� (BG_Meta_Dictionaries) 
```
Create.Table("BG_Meta_DictionariesColumns")
    .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
    .WithColumn("DictionaryId").AsInt32().NotNullable()
    .WithColumn("Name").AsString().NotNullable()
    .WithColumn("ColumnName").AsString().NotNullable()
    .WithColumn("ColumnType").AsString().NotNullable()
    .WithColumn("RefId").AsInt32().Nullable()
    .WithColumn("RefColumn").AsInt32().Nullable()
    .WithColumn("Visible").AsBoolean().NotNullable()
    .WithColumn("OrderBy").AsInt32().Nullable();

Create.ForeignKey("FK_Meta_Dictionaries")
    .FromTable("BG_Meta_DictionariesColumns").ForeignColumn("DictionaryId")
    .ToTable("BG_Meta_Dictionaries").PrimaryColumn("Id");
```

### Sql scripts

```
CREATE TABLE uirdb.dbo.BG_Meta_Dictionaries (
  Id INT NOT NULL
 ,Name NVARCHAR(255) NOT NULL
 ,TableName NVARCHAR(255) NOT NULL
 ,ModifyType INT NOT NULL
 ,Visible BIT NOT NULL
 ,OrderBy INT NULL
 ,CONSTRAINT PK_BG_Meta_Dictionaries PRIMARY KEY CLUSTERED (Id)
)

CREATE TABLE uirdb.dbo.BG_Meta_DictionariesColumns (
  Id INT NOT NULL
 ,DictionaryId INT NOT NULL
 ,Name NVARCHAR(255) NOT NULL
 ,ColumnName NVARCHAR(255) NOT NULL
 ,ColumnType NVARCHAR(255) NOT NULL
 ,RefId INT NULL
 ,RefColumn INT NULL
 ,Visible BIT NOT NULL
 ,OrderBy INT NULL
 ,CONSTRAINT PK_BG_Meta_DictionariesColumns PRIMARY KEY CLUSTERED (Id)
)

INSERT INTO BG_Meta_Dictionaries ([Id], [Name], [TableName], [ModifyType], [Visible], [OrderBy])
VALUES (1, 'SettingsDictionary', 'BG_Meta_Dictionaries', 1, 1 ,0);
	
	
INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (1, 1, 'Id', 'Id', 'Number', null, null, 1, 0) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (2, 1, 'Name', 'Name', 'String', null, null, 1, 1) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (3, 1, 'TableName', 'TableName', 'String', null, null, 1, 2) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (4, 1, 'ModifyType', 'ModifyType', 'Number', null, null, 1, 3) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (5, 1, 'Visible', 'Visible', 'Boolean', null, null, 1, 4) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (6, 1, 'OrderBy', 'OrderBy', 'Number', null, null, 1, 5) 


INSERT INTO BG_Meta_Dictionaries ([Id], [Name], [TableName], [ModifyType], [Visible], [OrderBy])
VALUES (2, 'SettingsColumns', 'BG_Meta_DictionariesColumns', 1, 1 ,1);


INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (7, 2, 'Id', 'Id', 'Number', null, null, 1, 0) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (8, 2, 'DictionaryId', 'DictionaryId', 'Number', null, null, 1, 1) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (9, 2, 'Name', 'Name', 'String', null, null, 1, 2) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (10, 2, 'ColumnName', 'ColumnName', 'String', null, null, 1, 3) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (11, 2, 'ColumnType', 'ColumnType', 'String', null, null, 1, 4) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (12, 2, 'RefId', 'RefId', 'Number', null, null, 1, 4) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (13, 2, 'RefColumn', 'RefColumn', 'Number', null, null, 1, 6) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (14, 2, 'Visible', 'Visible', 'Boolean', null, null, 1, 7) 

INSERT INTO BG_Meta_DictionariesColumns ([Id], [DictionaryId], [Name],  [ColumnName], [ColumnType], [RefId], [RefColumn], [Visible], [OrderBy])
VALUES (15, 2, 'OrderBy', 'OrderBy', 'Number', null, null, 1, 8) 
```