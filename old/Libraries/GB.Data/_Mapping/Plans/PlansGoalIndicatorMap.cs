﻿using GB.Data.Plans;

namespace GB._Mapping.Plans
{
    internal class PlansGoalIndicatorMap : ClassMapHiLo<PlansGoalIndicator>
    {
        public PlansGoalIndicatorMap()
            : base("Plans_Goal_Indicator")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Plans_Goal_Indicator_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            string args = @"
            //						@Plans_Goal_Indicator_Name = ?
            //					   ,@Plans_Goal_Indicator_UOM = ?
            //					   ,@Plans_Goal_Indicator_Val = ?
            //                       ,@Plans_Goal_Indicator_End_Val = ?
            //					   ,@Plans_Goal_Indicator_PRS_Fact_Val = ?
            ////                       ,@Plans_Goal_Indicator_End_Srok = ?
            //                       ,@Plans_Goal_Indicator_Year = ?
            //                        ,@Plans_Goal_Indicator_Add_Funding = ?
            //                        ,@Plans_Goal_Indicator_Par_ID = ?
            //					   ,@Plans_Activity_ID = ?
            //					   ,@Plans_Goal_Indicator_ID = ?";

            //            SqlInsert(@"exec ZZZ_Prc_Plans_Goal_Indicator_Insert " + args).Check.None();
            //            SqlUpdate(@"exec ZZZ_Prc_Plans_Goal_Indicator_Update " + args).Check.None();
            //            SqlDelete(@"exec ZZZ_Prc_Plans_Goal_Indicator_Delete @Plans_Goal_Indicator_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Goal_Indicator_Name").Not.Nullable();
            Map(x => x.UOM, "Plans_Goal_Indicator_UOM");
            Map(x => x.ValStart, "Plans_Goal_Indicator_Val");
            Map(x => x.ValEnd, "Plans_Goal_Indicator_End_Val");
            Map(x => x.PRSFactVal, "Plans_Goal_Indicator_PRS_Fact_Val");
            Map(x => x.ForwardValueEnd, "Plans_Goal_Indicator_End_Srok");
            Map(x => x.Year, "Plans_Goal_Indicator_Year");
            Map(x => x.AddFounding, "Plans_Goal_Indicator_Add_Funding");
            Map(x => x.ReachedValue, "Plans_Goal_Indicator_Reached_Val");
            Map(x => x.FeatureType, "Plans_Goal_Indicator_Feature_Type");
            Map(x => x.Algorythm, "Plans_Goal_Indicator_Algorithm");

            References<PlansGoalIndicator>(x => x.Parent, "Plans_Goal_Indicator_Par_ID").Nullable();
            References<PlansActivity>(x => x.PlanActivity, "Plans_Activity_ID").Not.Nullable();
            //References<PlansActivityFunc>(x => x.ActivityFunc, "Plans_Activity_Func_ID").Nullable();
            References<PlansActivityGoal>(x => x.Goal, "Plans_Activity_Goal_ID").Nullable();
        }
    }
}