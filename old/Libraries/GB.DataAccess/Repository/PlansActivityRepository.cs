﻿using System.Collections.Generic;
using System.Linq;
using GB.Data.Plans;

namespace GB.DataAccess.Repository
{
    /// <summary>Репозиторий мероприятия плана</summary>
    public class PlansActivityRepository : Repository<PlansActivity>
    {
        //        public static bool IsHidden(object ActivityId)
        //        {
        //            var sql = @"
        //SELECT  Plans_Activity_ID
        //FROM    dbo.Plans_Activity
        //WHERE   Is_Deleted = 1 AND Plans_Activity_ID = @ActivityId";

        //            var id = RepositoryBase.GetValueViaSQL(sql,
        //                                                  new Dictionary<string, object>() { { "@ActivityId", ActivityId } });

        //            return id != null && id != DBNull.Value;
        //        }

        //        private static DataTable getHiddenTable(string table_name, object id, DataSet ds, string idColumn = null)
        //        {
        //            string sql = string.Format("SELECT * FROM {0} WHERE {1} = @id", table_name, idColumn ?? table_name + "_ID");
        //            DataTable table = GetTableViaSQL(sql, new Dictionary<string, object> { { "@id", id } }, table_name);
        //            ds.Tables.Add(table);
        //            return table;
        //        }

        //        public static DataSet GetHidden(object ActivityId, bool NativePpropertiesOnly = false)
        //        {
        //            DataSet ds = new DataSet("HiddenActivity");

        //            var activityRow = getHiddenTable("Plans_Activity", ActivityId, ds).Rows[0];

        //            if (NativePpropertiesOnly)
        //                return ds;

        //            getHiddenTable("Plans", activityRow["Plans_ID"], ds);
        //            getHiddenTable("Plans_Activity_Type2", activityRow["Plans_Activity_Type2_ID"], ds);
        //            getHiddenTable("IKT_Object", activityRow["IKT_Object_ID"], ds);
        //            getHiddenTable("IS_or_IKT", activityRow["IS_or_IKT_ID"], ds);  // IKTComponent

        //            string sql = @"
        //SELECT  r.Responsible_ID ,
        //        r.Responsible_FIO ,
        //        r.Responsible_Phone ,
        //        r.Responsible_Email ,
        //        j.Responsible_Job_Name
        //FROM    Responsible r
        //        LEFT JOIN dbo.Responsible_Job j ON r.Responsible_Job_ID = j.Responsible_Job_ID
        //WHERE   r.Responsible_ID = @Responsible";

        //            var responsbleTable = GetTableViaSQL(sql, new Dictionary<string, object> { { "@Responsible", activityRow["Responsible_ID"] } }, "Responsible");
        //            ds.Tables.Add(responsbleTable);

        //            getHiddenTable("Source_Funding", activityRow["Source_Funding_ID"], ds);
        //            getHiddenTable("GRBS", activityRow["GRBS_ID"], ds);
        //            getHiddenTable("Section_KBK", activityRow["Section_KBK_ID"], ds);
        //            getHiddenTable("Expense_Item", activityRow["Expense_Item_ID"], ds);
        //            getHiddenTable("Work_Form", activityRow["Work_Form_ID"], ds);
        //            //getHiddenTable("Expenditure_Items", activityRow["Expenditure_Items_ID"], ds);

        //            var Plans_Activity_Reason = getHiddenTable("Plans_Activity_Reason", activityRow["Plans_Activity_ID"], ds, "Plans_Activity_ID");
        //            AddTable(ds, GetDirectoryForTable(Plans_Activity_Reason, "NPA_Type"));

        //            getHiddenTable("Plans_Activity_Goal", activityRow["Plans_Activity_ID"], ds, "Plans_Activity_ID");

        //            AddTable(ds, ActivityFuncRepository.GetList(ActivityId, true), "Functions");

        //            getHiddenTable("Plans_Goal_Indicator", activityRow["Plans_Activity_ID"], ds, "Plans_Activity_ID");

        //            var Plans_Activity_ED = getHiddenTable("Plans_Activity_ED", activityRow["Plans_Activity_ID"], ds, "Plans_Activity_ID"); // ActivityExpenseDirections
        //            AddTable(ds, GetDirectoryForTable(Plans_Activity_ED, "Expense_Direction"));

        //            var Plans_State_Contract = getHiddenTable("Plans_State_Contract", activityRow["Plans_Activity_ID"], ds, "Plans_Activity_ID");
        //            var Contract = AddTable(ds, GetDirectoryForTable(Plans_State_Contract, "Contract"));
        //            AddTable (ds, GetReference("Contract_Product", Contract));

        //            AddTable(ds, WorksRepository.GetHiddenWorksList(ActivityId), "Works");

        //            AddTable(ds, SpecRepository.GetHiddenSpecList(ActivityId), "Specs");

        //            sql  = @"
        //SELECT  z.Plans_Zakupki_ID ,
        //        z.Plans_Zakupki_Number ,
        //        z.Plans_Zakupki_Publish_Date ,
        //        z.Plans_Zakupki_Contract_Product_Name ,
        //        z.Plans_Zakupki_Izv_Numb ,
        //        z.Plans_Zakupki_Izv_Date ,
        //        z.Plans_Zakupki_Open_Date ,
        //        z.Plans_Zakupki_Cons_Date ,
        //        z.Plans_Zakupki_Itog_Date ,
        //        z.Plans_Zakupki_Site ,
        //        s.Plans_Zakupki_Status_Name ,
        //        w.Order_Way_Name
        //FROM    Plans_Zakupki z
        //        LEFT JOIN dbo.Plans_Zakupki_Status s ON z.Plans_Zakupki_Status_ID = s.Plans_Zakupki_Status_ID
        //        LEFT JOIN dbo.Order_Way w ON z.Order_Way_ID = w.Order_Way_ID
        //WHERE   z.Plans_Activity_ID = @ActivityId";

        //            var Plans_Zakupki = GetTableViaSQL(sql, new Dictionary<string, object> { { "@ActivityId", activityRow["Plans_Activity_ID"] } }, "Plans_Zakupki");
        //            AddTable(ds, Plans_Zakupki, "Plans_Zakupki");

        //            getHiddenTable("Plans_Lots", activityRow["Plans_Activity_ID"], ds, "Plans_Activity_ID");

        //            return ds;
        //        }

        //        static DataTable AddTable(DataSet ds, DataTable table, string TableName = null)
        //        {
        //            if (table != null)
        //            {
        //                if (!string.IsNullOrEmpty(TableName))
        //                {
        //                    table.TableName = TableName;
        //                }
        //                ds.Tables.Add(table);
        //            }

        //            return table;
        //        }

        //        static DataTable GetDirectoryForTable(DataTable SourceTable, string DirectoryTableName)
        //        {
        //            IList<int> idList = null;
        //            var column = DirectoryTableName + "_ID";

        //            if (SourceTable == null || !SourceTable.Columns.Contains(column))
        //                return null;

        //            idList = SourceTable.Rows.Cast<DataRow>()
        //                                .Where(row=>row[column] != DBNull.Value)
        //                                .Select(row=>(int)row[column])
        //                                .ToList();

        //            if (!idList.Any())
        //                return null;

        //            string sql = string.Format(@"SELECT * FROM {0} WHERE {1} in ({2})", DirectoryTableName, column, string.Join<int>(",", idList));

        //            return GetTableViaSQL(sql, null, DirectoryTableName);
        //        }

        //        static DataTable GetReference(string TableName, DataTable table)
        //        {
        //            if (table == null)
        //                return null;

        //            IList<int> idList = null;

        //            var keyColumn = table.TableName + "_ID";

        //            idList = table.Rows.Cast<DataRow>()
        //                          .Where(row => row[keyColumn] != DBNull.Value)
        //                          .Select(row => (int)row[keyColumn])
        //                          .ToList();

        //            if (!idList.Any())
        //                return null;

        //            string sql = string.Format(@"SELECT * FROM {0} WHERE {0}_ID in ({1})", TableName, string.Join<int>(",", idList));

        //            return GetTableViaSQL(sql, null, TableName);
        //        }

        //        public static DataTable GetRelatives(object ActivityId)
        //        {
        //            var sql = @"
        //-- дети
        //SELECT  1 as LT, Plans_Activity_ID, Plans_Activity_Code, Plans_Activity_Name, Is_Deleted -- 'скопировано в..'
        //FROM    dbo.Plans_Activity
        //WHERE   Plans_Activity_Old_ID = @ActivityId
        //
        //union all
        //
        //-- родители
        //SELECT  2 as LT, Plans_Activity_ID, Plans_Activity_Code, Plans_Activity_Name, Is_Deleted -- 'объединено из..'
        //FROM    dbo.Plans_Activity
        //WHERE   Plans_Activity_New_ID = @ActivityId
        //
        //union all
        //
        //SELECT  3 as LT, Plans_Activity_ID, Plans_Activity_Code, Plans_Activity_Name, Is_Deleted -- 'скопировано из..'
        //FROM    dbo.Plans_Activity
        //WHERE   Plans_Activity_ID = (select p.Plans_Activity_Old_ID from Plans_Activity p where p.Plans_Activity_ID = @ActivityId)
        //
        //union all
        //
        //SELECT  4 as LT, Plans_Activity_ID, Plans_Activity_Code, Plans_Activity_Name, Is_Deleted -- 'объединено в..'
        //FROM    dbo.Plans_Activity
        //WHERE   Plans_Activity_ID = (select Plans_Activity_New_ID from Plans_Activity where Plans_Activity_ID = @ActivityId)
        //";

        //            return RepositoryBase.GetTableViaSQL(sql, new Dictionary<string, object>() { { "@ActivityId", ActivityId } });
        //        }

        //        // из исходного списка выбираются те планы, которые были получены в результате объединения
        //        public static IList<int> GetJoinedActivities(IList<int> PlansActivityIdList)
        //        {
        //            var result = new List<int>();

        //            if (PlansActivityIdList != null)
        //            {
        //                string sql = @"
        //SELECT DISTINCT
        //        Plans_Activity_ID
        //FROM    dbo.Plans_Activity
        //WHERE   Plans_Activity_ID IN ({0})
        //        AND Plans_Activity_ID IN (SELECT Plans_Activity_New_ID FROM dbo.Plans_Activity)";

        //                sql = string.Format(sql, PlansActivityIdList.Count > 0 ? string.Join(",", PlansActivityIdList) : "-1");

        //                var allItems = GetTableViaSQL(sql);

        //                if (allItems != null)
        //                {
        //                    result = allItems.Rows.Cast<DataRow>().Select(x => (int)x["Plans_Activity_ID"]).ToList();
        //                }
        //            }
        //            return result;
        //        }

        //        public static bool CheckSyncronization(int id)
        //        {
        //            return Convert.ToBoolean(GetValueViaSQL(string.Format("SELECT dbo.Get_Syncronization_Coord({0})", id)));
        //        }

        //        public static void CleanPlansActivity(int id)
        //        {
        //            var pars = new Dictionary<string, object> { { "@Plans_Activity_ID", id }, { "@ZZZ_USERS_ID", UserRepository.CurrentUserId } };

        //            ExecuteProc("Clean_Plans_Activity", pars);
        //        }

        public List<PlansActivity> GetActivitiesByAccountingObject(int accountingObjectId)
        {
            return GetAllEx(q => q
                    .Fetch(x => x.ActivityType2).Eager
                    .Fetch(x => x.Department).Eager
                    .Fetch(x => x.Status).Eager
                    .Where(x => x.AccountingObject.Id == accountingObjectId))
                .ToList();
        }
    }
}