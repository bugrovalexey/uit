﻿using System;
using System.ServiceModel.Dispatcher;
using System.Xml;
using System.IO;
using System.ServiceModel.Channels;

namespace GB.SmevBehaviorExtension
{
    class ExtendedDispatchMessageInspector : IDispatchMessageInspector
    {
        private string certificateName = null;

        public ExtendedDispatchMessageInspector(string CertificateName)
        {
            this.certificateName = CertificateName;
        }

        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
            return null;
        }

        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            try
            {
                XmlDocument doc = Signer.SignXml(reply.ToString(), this.certificateName, SignMode.Replay);

                MemoryStream ms = new MemoryStream();

                XmlWriter writer = XmlWriter.Create(ms);
                doc.WriteTo(writer);
                writer.Flush();
                ms.Position = 0;

                XmlReader reader = XmlReader.Create(ms);

                Message newRequest = Message.CreateMessage(reader, int.MaxValue, reply.Version);
                newRequest.Properties.Clear();
                newRequest.Properties.CopyProperties(reply.Properties);
                reply = newRequest;
            }
            catch (Exception exp)
            {
                XmlDocument doc = new XmlDocument();
                doc.PreserveWhitespace = false;
                string rpl = reply.ToString();

                if(rpl.StartsWith("<html>", StringComparison.CurrentCultureIgnoreCase))
                    return;

                doc.LoadXml(reply.ToString());


                XmlNamespaceManager ns = new XmlNamespaceManager(doc.NameTable);
                ns.AddNamespace("s", "http://schemas.xmlsoap.org/soap/envelope/");

                XmlElement headerNode = doc.DocumentElement.SelectSingleNode(@"//s:Header ", ns) as XmlElement;
                if (headerNode == null)
                    return;

                XmlElement error = doc.CreateElement("s", "MessageError", headerNode.NamespaceURI);
                error.InnerText = exp.Message.TrimEnd('\n', '\r');
                headerNode.AppendChild(error);

                MemoryStream ms = new MemoryStream();

                XmlWriter writer = XmlWriter.Create(ms);
                doc.WriteTo(writer);
                writer.Flush();
                ms.Position = 0;

                XmlReader reader = XmlReader.Create(ms);

                Message newRequest = Message.CreateMessage(reader, int.MaxValue, reply.Version);
                newRequest.Properties.Clear();
                newRequest.Properties.CopyProperties(reply.Properties);
                reply = newRequest;
            }
        }
    }
}
