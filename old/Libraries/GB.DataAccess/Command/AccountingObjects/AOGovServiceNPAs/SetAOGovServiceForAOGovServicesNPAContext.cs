﻿using GB.Data.AccountingObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServiceNPAs
{
    public class SetAOGovServiceForAOGovServicesNPAContext : ICommandContext
    {
        public List<int> AOGovServicesNpaIds { get; set; }
        public AOGovService AOGovService { get; set; }
    }
}
