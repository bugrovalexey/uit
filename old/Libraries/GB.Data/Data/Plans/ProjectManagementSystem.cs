﻿using System;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Система проектного управления мероприятиями по информатизации
    /// </summary>
    public class ProjectManagementSystem : NumerableEntity
    {
        protected ProjectManagementSystem()
        {
        }

        public ProjectManagementSystem(Plan Plan)
        {
            this.Plan = Plan;
        }

        /// <summary> Номер ведомственного акта</summary>
        public virtual string ActNum { get; set; }

        /// <summary> Наименование ведомственного акта</summary>
        public virtual string ActName { get; set; }

        /// <summary> Дата акта/ГОСТ</summary>
        public virtual DateTime? ActDate { get; set; }

        /// <summary>
        /// Организационная структура проектного управления мероприятиями по информатизации, ФИО
        ///  <summary>
        public virtual string FIO { get; set; }

        /// <summary>
        /// Организационная структура проектного управления мероприятиями по информатизации, Должность
        ///  <summary>
        public virtual string Post { get; set; }

        /// <summary> Наименование проектной роли</summary>
        public virtual string Role { get; set; }

        /// <summary> Выполняемы функции</summary>
        public virtual string Func { get; set; }

        /// <summary> Квалификация и проектный опыт</summary>
        public virtual string Exp { get; set; }

        /// <summary> Мероприятие</summary>
        public virtual PlansActivity PlansActivity { get; set; }

        /// <summary> План</summary>
        public virtual Plan Plan { get; set; }

        /// <summary>Наименование ИС</summary>
        public virtual string ISName { get; set; }

        /// <summary>Перечень процедур этапов управления проектами и их результатов, для автоматизации которых используется ИС</summary>
        public virtual string Procedures { get; set; }


    }
}
