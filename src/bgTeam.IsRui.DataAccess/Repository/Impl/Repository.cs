﻿namespace bgTeam.IsRui.DataAccess.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using bgTeam.IsRui.DataAccess.Impl;
    using bgTeam.IsRui.Domain.Entities;
    using NHibernate;
    using NHibernate.Transform;

    /// <summary>
    /// Общий репозиторий
    /// </summary>
    /// <typeparam name="T">Тип объекта</typeparam>
    public class Repository : RepositorySt, IRepositoryRui
        //where T : EntityBase
    {
        //public Repository()
        //    : base(new SessionNHibernate(NHSession.CurrentSession))
        //{
        //}

        public Repository(ISessionNHibernateFactory factory)
            : base(factory.OpenSession())
        {
        }

        /// <summary>
        /// Возвращает объект по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Объект</returns>
        public T Get<T>(object id)
        {
            if (id == null)
            {
                return default(T);
            }
            else if (id is double)
            {
                id = Convert.ToInt32(id);
            }
            else if (id is Enum)
            {
                id = (int)id;
            }
            else if (id is string)
            {
                int value = 0;
                if (!int.TryParse(id.ToString(), out value))
                {
                    return default(T);
                }

                id = value;
            }

            return Session.Get<T>(id);
        }

        public T Get<T>(Action<IQueryOver<T, T>> query)
            where T : EntityBase
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.SingleOrDefault();
        }

        /// <summary>
        /// Возвращает существующую запись
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Запись, либо ArgumentException, если ее неудалось найти</returns>
        public T GetExisting<T>(object id)
        {
            var entity = Get<T>(id);

            if (entity == null)
            {
                throw new ArgumentException(string.Format("Объект <{0}> не найден в системе", id));
            }

            return entity;
        }

        public T GetDefault<T>()
        {
            return Session.Get<T>(EntityBase.Default);
        }

        public T GetOrDefault<T>(object id)
        {
            T ob = Get<T>(id);

            if (ob == null)
            {
                return GetDefault<T>();
            }

            return ob;
        }

        #region GetAll
        /// <summary>
        /// Возвращает все объекты
        /// </summary>
        /// <returns>Список объектов</returns>
        public IList<T> GetAll<T>()
        {
            return Session.CreateCriteria(typeof(T)).List<T>();
        }

        /// <summary>
        /// Возвращает все объекты
        /// </summary>
        /// <returns>Список объектов</returns>
        public IList<T> GetAllEx<T>(Action<IQueryOver<T, T>> query)
            where T : EntityBase
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.List<T>();
        }

        /// <summary>
        /// Можно вытаскивать типы отличные от Root типа
        /// (конкретное поле, несколько полей, другой объект, используя TransformUsing)
        /// </summary>
        /// <typeparam name="TReturn">Возвращаемый тип</typeparam>
        /// <param name="query">Запрос</param>
        /// <returns>Список объектов возращаемых запросом</returns>
        public IList<TReturn> Select<T, TReturn>(Action<IQueryOver<T, T>> query)
            where T : EntityBase
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.List<TReturn>();
        }

        /// <summary>
        /// Можно вытаскивать типы отличные от Root типа
        /// (конкретное поле, другой объект, используя TransformUsing)
        /// </summary>
        /// <typeparam name="TReturn">Возвращаемый тип</typeparam>
        /// <param name="query">Запрос</param>
        /// <returns>Объект возращаемый запросом</returns>
        public TReturn SelectSingle<T, TReturn>(Action<IQueryOver<T, T>> query)
            where T : EntityBase
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.SingleOrDefault<TReturn>();
        }

        /// <summary>
        /// Выполнить хранимую процедуру через NHibernate
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> SelectExecuteAsync<T>(string str, ParameterProcedure[] parameters)
        {
            IQuery query = Session.CreateSQLQuery(str);

            foreach (var item in parameters)
            {
                query.SetParameter(item.Name, item.Value, item.Type);
            }

            return await query
                .SetResultTransformer(Transformers.AliasToBean<T>())
                .ListAsync<T>();
        }
        #endregion GetAll

        /// <summary>
        /// Возвращает количество строк по запросу
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public int RowCount<T>(Action<IQueryOver<T, T>> query)
            where T : EntityBase
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.RowCount();
        }
    }
}