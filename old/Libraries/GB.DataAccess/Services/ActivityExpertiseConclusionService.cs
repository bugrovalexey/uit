﻿using System;
using GB.Data.Plans;
using GB.DataAccess.Repository;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class ActivityExpertiseConclusionService : ServiceBase<ActivityExpertiseConclusion>, IService<ActivityExpertiseConclusion, ActivityExpertiseConclusionArgs>
    {
        public ActivityExpertiseConclusion Create(ActivityExpertiseConclusionArgs args)
        {
            var e = new ActivityExpertiseConclusion();

            e.Status = new StatusRepository().GetDefault(e);

            return base.SaveOrUpdate(e);
        }

        public ActivityExpertiseConclusion Update(ActivityExpertiseConclusionArgs args)
        {
            throw new NotImplementedException();
        }

        public void Delete(ActivityExpertiseConclusionArgs args)
        {
            throw new NotImplementedException();
        }
    }

    public class ActivityExpertiseConclusionArgs : IServiceArgs
    {
    }
}