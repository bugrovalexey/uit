﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReplyMessage.aspx.cs" Inherits="GB.MKRF.Web.Administrator.ReplyMessage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ответное сообщение</title>
</head>
<body>
    <form id="form1" runat="server">
    <dxlp:ASPxLoadingPanel runat="server" ID="LoadingPanel" ClientInstanceName="LoadingPanel">
    </dxlp:ASPxLoadingPanel>
    <div style="padding: 10px">
        <div style="margin-bottom: 15px; font-weight: bold">
            Отправка ответного сообщения пользователю</div>
        <table>
            <tr>
                <td>
                    Кому:
                </td>
                <td>
                    <dxe:ASPxTextBox runat="server" ID="toEmail" Width="400px" ClientInstanceName="toEmail">
                        <ValidationSettings ValidationGroup="mv">
                            <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                            <%--<RegularExpression ErrorText="Неверно указан адрес получателя" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />--%>
                        </ValidationSettings>
                    </dxe:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Копия:
                </td>
                <td>
                    <dxe:ASPxTextBox runat="server" ID="copyToEmail" Width="400px" ClientInstanceName="copyToEmail">
                    </dxe:ASPxTextBox>
                </td>
            </tr>
        </table>
        <hr />
        Уважаемый пользователь
        <%=currentMessage.User.FullName%>!
        <br />
        <br />
        В ответ на Ваше сообщение:<br />
        <br />
        &quot;<%=currentMessage.Text %>&quot;
        <br />
        <br />
        от:
        <%=currentMessage.Date.ToString() %>
        <br />
        <br />
        сообщаю:<br />
        <dxe:ASPxMemo runat="server" ID="answerMemo" Rows="10" Columns="100" ClientInstanceName="answerMemo">
            <ValidationSettings ValidationGroup="mv">
                <RequiredField ErrorText="Необходимо заполнить" IsRequired="true" />
            </ValidationSettings>
        </dxe:ASPxMemo>
        <br />
        С уважением,<br />
        Администратор системы ИТ «Аудит»<br />
        <br />
        <dxe:ASPxButton runat="server" ID="btnSend" Text="Отправить" AutoPostBack="false" ClientInstanceName="btnSend"
            CausesValidation="true" ValidationGroup="mv">
            <ClientSideEvents Click="function(s,e){ if (ASPxClientEdit.ValidateGroup('mv')) callBack.PerformCallback('');  }" />
        </dxe:ASPxButton>
        <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" 
            oncallback="callBack_Callback">
        <ClientSideEvents 
            BeginCallback="function(s,e){LoadingPanel.Show();}" 
            CallbackError="function(s,e){ e.handled = true; alert('Ошибка. Ответ не отправлен.'); }" 
            CallbackComplete="function(s,e){ 
                    btnSend.SetEnabled(false); 
                    answerMemo.SetEnabled(false); 
                    toEmail.SetEnabled(false);
                    copyToEmail.SetEnabled(false);
                    if(opener && !opener.closed && opener.RefreshContent)
                        opener.RefreshContent();
                    alert('Сообщение отправлено.');}" 
            EndCallback="function(s,e){LoadingPanel.Hide();}" />
        </dxc:ASPxCallback>
    </div>
    </form>
</body>
</html>
