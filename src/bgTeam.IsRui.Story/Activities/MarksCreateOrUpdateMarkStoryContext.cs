﻿namespace bgTeam.IsRui.Story.Activities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class MarksCreateOrUpdateMarkStoryContext
    {
        public int? Id { get; set; }

        public int? ActivityId { get; set; }

        /// <summary>
        /// Пояснение связи с показателем проекта
        /// </summary>
        public string ExplanationRelationshipWithProjectMark { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Алгортим
        /// </summary>
        public string AlgorithmOfCalculating { get; set; }

        /// <summary>
        /// Базовое значение
        /// </summary>
        public int? BaseValue { get; set; }

        /// <summary>
        /// Целевое значение
        /// </summary>
        public int? TargetedValue { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public int? UnitId { get; set; }

        /// <summary>
        /// Дата достижения целевого значения
        /// </summary>
        public DateTime? DateToAchieveGoalValue { get; set; }
    }
}
