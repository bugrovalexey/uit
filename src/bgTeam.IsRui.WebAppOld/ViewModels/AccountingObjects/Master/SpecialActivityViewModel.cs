﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class SpecialActivityViewModel : StepBaseViewModel
    {
        #region Consts

        const string InformAboutActivityGovOrganCaption = "Информационная система, направлена на информирование о деятельности государственного органа";

        #endregion Consts

        public SpecialActivityViewModel(int id)
            : base(id, MasterItemEnum.SpecialActivity)
        {
            GovServices =
                new RepositoryDc<AOGovService>().GetAllEx(q => q.Where(x => x.AccountingObject.Id == id))
                    .Select(x => new TableAOGovServices(x));

        }

        [DisplayName(InformAboutActivityGovOrganCaption)]
        public bool InformAboutActivityGovOrgan
        {
            get { return Current.InformAboutActivityGovOrgan; }
        }

        public IEnumerable<TableAOGovServices> GovServices { get; set; }
        
    }
  
}