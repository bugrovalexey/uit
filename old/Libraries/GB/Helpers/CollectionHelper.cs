﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.Helpers
{
    public static class CollectionHelper
    {
        public static IEnumerable<object> GetEmptyForSelectList()
        {
            return new[] {new {Id = (int?) null, Name = "Не задано"}};
        }
    }

}
