﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainEmpty.Master" AutoEventWireup="true" CodeBehind="ReportActivityTable.aspx.cs" Inherits="GB.MKRF.Web.Monitoring.v2012new.ReportActivityTable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <form id="form1" runat="server">
        <div style="padding:20px">
        <dxe:ASPxButton ID="ExportButton" runat="server" Text="Выгрузить список в Excel"
            Width="180px" OnClick="ExportButton_Click" />
        </div>
        <dxwgve:ASPxGridViewExporter ID="GridViewExporter" runat="server" GridViewID="Grid" />
        <dxwgv:ASPxGridView runat="server" ID="Grid" Width="100%" ClientInstanceName="Grid" KeyFieldName="Id" OnBeforePerformDataSelect="Grid_BeforePerformDataSelect">
            <Columns>
                <%--<dxwgv:GridViewDataTextColumn FieldName="Priority" Caption=" " GroupIndex="1" />--%>

                <dxwgv:GridViewDataCheckColumn Caption="Приоритетность" FieldName="IsNotPriority" GroupIndex="0" SortIndex="0" SortOrder="Ascending"
                    Name="IsNotPriority" ShowInCustomizationForm="True" Visible="False">
                    <PropertiesCheckEdit DisplayTextChecked="Мероприятия по информатизации, не относящиеся к приоритетным"
                        DisplayTextUnchecked="Приоритетные мероприятия">
                    </PropertiesCheckEdit>
                </dxwgv:GridViewDataCheckColumn>

                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="Code" Caption="Код мероприятия" />
                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="Name" Caption="Наименование мероприятия" />
                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="Type" Caption="Тип мероприятия" />
                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="IsIkt" Caption="Классификационный признак ИС или компонента ИТ-инфраструктуры" />
                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="NameIsIkt" Caption="Наименование ИС или объекта ИКТ-инфраструктуры" />
                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="Annotation" Caption="Аннотация" />
                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="Source" Caption="Источник финансирования" />
                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="Responsible" Caption="ФИО ответственного сотрудника" />
                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="Goals" Caption="Цель мероприятия" />

                <%--<dxwgv:GridViewDataTextColumn FieldName="GRBS" Caption="Код по ГРБС" />--%>
                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="SectionKBK" Caption="Код по БК ПЗ/РЗ" />
                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="ExpenseItem" Caption="Код по БК ЦСР" />
                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="WorkForm" Caption="Код по БК ВР" />
                <%--<dxwgv:GridViewDataTextColumn Visible="false" FieldName="ExpenditureItem" Caption="Код по БК КОСГУ" />--%>

                <dxwgv:GridViewDataTextColumn Visible="false" FieldName="Contract" Caption="Ранее заключенные гос. контракты  (номер контракта, дата контракта)" />

                <dxwgv:GridViewBandColumn Visible="false" Caption="Объем планируемых бюджетных ассигнований, тыс. руб">
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="VolY0">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000"/>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                            <HeaderStyle Wrap="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="VolY1">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000"/>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                            <HeaderStyle Wrap="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="VolY2">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000"/>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                            <HeaderStyle Wrap="False" />
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                </dxwgv:GridViewBandColumn>

                <dxwgv:GridViewBandColumn Visible="false" Caption="Дополнительная потребность, тыс. руб" >
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="AddVolY0">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000"/>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                            <HeaderStyle Wrap="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="AddVolY1">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000"/>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                            <HeaderStyle Wrap="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="AddVolY2">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000"/>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                            <HeaderStyle Wrap="False" />
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                </dxwgv:GridViewBandColumn>

                <dxwgv:GridViewBandColumn Visible="false" Caption="Объем фактически израсходованных бюджетных ассигнований, тыс. руб" >
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="год, предшествующий отчетному финансовому году (текущий финансовый год-2" FieldName="FactVolY2" >
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000"/>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                            <HeaderStyle Wrap="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="отчетный финансовый год (текущий финансовый год-1)" FieldName="FactVolY1" >
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000"/>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                            <HeaderStyle Wrap="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="текущий финансовый год" FieldName="FactVolY0">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000"/>
                            <Settings AllowAutoFilter="False" AllowSort="True" SortMode="Value" />
                            <HeaderStyle Wrap="False" />
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                </dxwgv:GridViewBandColumn>

                <%--<dxwgv:GridViewDataTextColumn FieldName="Price" Caption="Стоимость">
                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                </dxwgv:GridViewDataTextColumn>--%>
            </Columns>
            <Settings GroupFormat="{1}" />
            <SettingsBehavior AutoExpandAllGroups="true" />
        </dxwgv:ASPxGridView>
    </form>
</asp:Content>
