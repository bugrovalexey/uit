﻿using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class SoftwareViewModel : StepBaseViewModel
    {
        public IEnumerable<TableSoftware> Softwares { get; set; }

        public SoftwareViewModel(int id)
            : base(id, MasterItemEnum.Software, x => x.Softwares)
        {
            Softwares = Current.Softwares.Select(x => new TableSoftware(x));
        }
    }
}