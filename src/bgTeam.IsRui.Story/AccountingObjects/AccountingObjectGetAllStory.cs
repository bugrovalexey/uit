﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.MKRF.Logic.Access;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AccountingObjectGetAllStory : IStory<AccountingObjectGetAllStoryContext, IEnumerable<AccountingObjectDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IManagerAccess _managerAccess;
        private readonly IRepositoryRui _repositoryRui;

        public AccountingObjectGetAllStory(
            IMapperBase mapper,
            IManagerAccess managerAccess,
            IRepositoryRui repositoryRui)
        {
            _mapper = mapper;
            _managerAccess = managerAccess;
            _repositoryRui = repositoryRui;
        }

        public IEnumerable<AccountingObjectDto> Execute(AccountingObjectGetAllStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<AccountingObjectDto>> ExecuteAsync(AccountingObjectGetAllStoryContext context)
        {
            return _repositoryRui.GetAllEx<AccountingObject>(q =>
                q.Fetch(x => x.Kind).Eager
                    .Fetch(x => x.Type).Eager
                    .Fetch(x => x.Favorites).Eager
                    .Fetch(x => x.Status).Eager
                    .OrderBy(x => x.CreateDate))
                .Select(ConvertObject);
        }

        private AccountingObjectDto ConvertObject(AccountingObject obj)
        {
            var result = _mapper.Map(obj, new AccountingObjectDto());

            result.CanDelete = _managerAccess.CanDelete(obj);

            var finance = 0M;

            if (obj.FinancingQuarter1 != null)
            {
                finance = finance + obj.FinancingQuarter1.Value;
            }

            if (obj.FinancingQuarter2 != null)
            {
                finance = finance + obj.FinancingQuarter2.Value;
            }

            if (obj.FinancingQuarter3 != null)
            {
                finance = finance + obj.FinancingQuarter3.Value;
            }

            if (obj.FinancingQuarter4 != null)
            {
                finance = finance + obj.FinancingQuarter4.Value;
            }

            result.CostValue = finance;
            result.Cost = result.CostValue.ToString("N2");

            /*var childs = _repositoryRui.GetAllEx<AccountingObject>(q => q.Where(a => a.Parent == obj));

            if (obj.Department == _userRepository.GetCurrent().Department)
            {
                if (childs.Count == 1)
                {
                    result.Description = childs[0].Department.Name;
                }

                if (childs.Count > 1)
                {
                    result.Description = "Консолидированное";
                }
            }
            else
            {
                result.Description = obj.Department.Name;

                if (childs.Count == 1)
                {
                    result.Description = childs[0].Department.Name;
                }
            }*/

            return result;
        }
    }
}
