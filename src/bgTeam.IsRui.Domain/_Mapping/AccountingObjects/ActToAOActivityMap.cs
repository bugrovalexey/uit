﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    internal class ActToAOActivityMap : ClassMapHiLo<ActToAOActivity>
    {
        public ActToAOActivityMap()
            : base("Act_To_OU_Activity")
        {
        }

        protected override void InitMap()
        {
            References(x => x.ActOfAcceptance, "Act_ID");
            References(x => x.AoActivity, "OU_Plans_Activity_ID").Not.Nullable();
        }
    }
}
