﻿namespace bgTeam.IsRui.Domain._Mapping.Common
{
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;

    internal class WorkflowUserConclusionMap : ClassMapHiLoNew<WorkflowUserConclusion>
    {
        public WorkflowUserConclusionMap()
            : base("WorkflowUserConclusion")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Text, "Text");
            Map(x => x.UserId, "UserId");
            Map(x => x.StatusId, "StatusId");
            Map(x => x.TimeStamp, "TimeStamp");

            Map(x => x.WorkflowId, "WorkflowId").Not.Nullable();

            HasMany(x => x.Documents).KeyColumns.Add("EntityOwnerId")
                .Where("EntityType = " + (int)EntityType.WorkflowUserConclusion)
                .Where("Type = " + (int)DocTypeEnum.WorkflowUserConclusion);
                //.Cascade.Delete().Inverse();
        }
    }
}
