﻿using System;
using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// ЦОД
    /// </summary>
    public class Zod : EntityBase
    {
        protected Zod()
        {
        }

        public Zod(PlansActivity PlansActivity)
        {
            this.PlansActivity = PlansActivity;
        }


        /// <summary>Мероприятия плана</summary>
        public virtual PlansActivity PlansActivity { get; protected set; }

        /// <summary>Номер реестра ФГИС</summary>
        public virtual FgisInfo FgisInfo { get; set; }

        /// <summary>Наименование государственного органа, совместно использующего ИС или компоненты ИКТ</summary>
        public virtual Department OGV { get; set; }

        /// <summary>Наименование уполномоченного государственного органа</summary>
        public virtual Department OGVAuthorized { get; set; }

        /// <summary> Номер контракта</summary>
        public virtual String ContractNumber { get; set; }

        /// <summary>Занимаемая площадь</summary>
        public virtual String RequiredSpace { get; set; }

        /// <summary>Энергопотребление </summary>
        public virtual String EnergyConsumption { get; set; }

        /// <summary>Наименование ИС/ИКТ </summary>
        public virtual String IKTComponent { get; set; }
    }
}
