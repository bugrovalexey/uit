﻿
function ErrorPanelShow(text) {
    SetDivError("", text);
}

function ErrorPanelHide() {
    SetDivError("none", null);
}


//Найти Error панель и задать ей текст 
function SetDivError(cl, text) {
    var diverr = document.getElementById("div_error_panel");

    if (diverr) {
        diverr.className = cl;
        diverr.innerHTML = text;
    }
}

function validateSpin(s, e) {
    e.isValid = s.GetText();
}

function CreatePopup(url) {
    var leftVal = (screen.width - 1000) / 2;
    var topVal = (screen.height - 600) / 2;

    if (leftVal < 0) leftVal = 0;
    if (topVal < 0) topVal = 0;

    //var url = $(e).attr("href");
    window.open(url, "_blank", "height=600,width=1000,left=" + leftVal + ",top=" + topVal + "directories=0,location=0,menubar=0,status=0,toolbar=0,scrollbars=1,resizable=1");
}

//Найти Error панель и задать ей текст 
function SetDivProperties(component, cl, text) {
    var diverr = document.getElementById(component);

    if (diverr) {
        diverr.className = cl;
        diverr.innerHTML = text;
    }
}

function SetStyle(component_id, style) {
    var component = document.getElementById(component_id);

    if (component) {
        component.className = style;
    }
}

function ValidateRow(component_id, value) {
    if (!value)
        SetStyle(component_id, "not_valid");
    else
        SetStyle(component_id, null);
}

//Возвращает количество выбранных записей на странице таблицы
function GetSelectedRowCountOnPage(grid) {
    var selInput = grid.GetSelectionInput();

    if (!selInput) return 0;

    var selCount = 0;
    var checkList = selInput.value;

    for (var i = 0; i < checkList.length; i++) {
        if (checkList.charAt(i) == "T") selCount++;
    }

    return selCount;
}

//Удалить выбранные строки в таблице
function GridDeleteSelectedRow(grid) {
    if (confirm('Вы действительно хотите удалить запись?')) {
        grid.PerformCallback('Delete');
    }
}

//Выбрать/снять выделение всех строк в таблице
function GridSelectAllRow(grid) {
    if (GetSelectedRowCountOnPage(grid) != grid.pageRowCount)
        grid.SelectAllRowsOnPage(true);
    else
        grid.SelectAllRowsOnPage(false);
}

//Инициализация таблицы
function InitializeTable(table, urlPage, addBackUrl, container) {
    var isLink = false;

    $("td .cell_not_redirect", table).bind("click", function (e) {
        isLink = true;
    });

    $("tr.row_redirect td a", table).bind("click", function (e) {
        isLink = true;
    });

    $("tr.row_redirect", table).bind("click", function (e) {
        if (!isLink) {

            var url = GetGridRowEditUrl(urlPage, addBackUrl, $(this).attr("edit"));

            if (container)
                ShowWindow(container, url);
            else {
                if (!e.ctrlKey)
                    window.location.assign(url);
                else
                    window.open(url);
            }
        }

        isLink = false;
    });
}

function ShowWindowNew(table, urlPage, addBackUrl, container) {
    var url = GetGridRowEditUrl(urlPage, addBackUrl, $(this).attr("edit"));

    if (container)
        ShowWindow(container, url);
    else
        window.location.assign(url);
}

function GetGridRowEditUrl(url, addBackUrl, id) {
    var mass = url.split(';');

    url = mass[0] + "?";

    if (id) {
        url += "id=" + id;
    }

    if (mass.length > 1) {
        for (var i = 1; i < mass.length; i++) {
            url += "&" + mass[i];
        }
    }

    if (addBackUrl) {
        url += "&backurl=" + encodeURIComponent(window.location.pathname + window.location.search);
    }

    return url;
}

function ShowWindow(form, url) {
    if (url) {
        url += "&win=true";
        form.SetContentUrl(url);
    }

    form.SetWidth(1000);
    form.SetHeight(document.documentElement.clientHeight - 100);
    form.Show();
}

function HideWindow(form) {
    form.SetContentHtml('');
    form.Hide();
}



// Валидация данных 
function ValidateForm() {
    var validationGroup = '';
    return RaisePageValidation(validationGroup);
}

function RaisePageValidation(validationGroup) {
    var validationProcs = [RaiseDxValidation, RaiseStandardValidation];
    var result = true;
    for (var index = 0; index < validationProcs.length; index++)
        result = validationProcs[index](validationGroup) && result;
    return result;
}

/* Different validation procs */
function RaiseDxValidation(validationGroup) {
    if (typeof (ASPxClientEdit) != "undefined" && typeof (ASPxClientEdit.ValidateGroup) == "function")
        return ASPxClientEdit.ValidateGroup(validationGroup);
    else
        return true;
}
function RaiseStandardValidation(validationGroup) {
    if (typeof (Page_IsValid) != "undefined" && Page_IsValid != null && typeof (Page_ClientValidate) == "function") {
        Page_ClientValidate(validationGroup);

        for (i = 0; i < Page_Validators.length; i++) {
            if (Page_Validators[i].isvalid) {
                $(Page_Validators[i]).closest('table').removeClass("ErorField");
            }
            else {
                $(Page_Validators[i]).closest('table').addClass("ErorField");
            }
        }

        return Page_IsValid;
    }
    return true;
}

//выставить у меню текущию станицу
//и проставить в ссылках параметры если нет
function UpdateMenu(menu_container, params, useSavingParams) {
    var url = document.location.href;
    var selected = $(menu_container).find("a");

    if (params == undefined)
        params = '';

    if (!params) {
        var urlParams = url.split('?');
        if (urlParams.length > 1) {
            url = urlParams[0];
            params = urlParams[1];
        }
    }

    $.each($(selected), function () {
        var current = false;

        var parent = $(this).closest("ul.submenu");

        //if (this.href && this.href.toLowerCase().indexOf(url.toLowerCase()) == 0) {
        if (this.href && url.toLowerCase() == this.href.split('?')[0].toLowerCase()) {

            $(this).addClass('current');
            $(this).removeAttr('href');
//            $(this).closest("li").addClass('current');

            current = true;

            if (parent.length > 0) {
                var parentlink = parent.siblings("a");

                parentlink.addClass('current');
                parentlink.removeAttr('href');
            }
        }

        if (useSavingParams) {
            urlParams = this.href.split('?');
            if (urlParams.length == 1 && params && !current) {
                this.href = this.href + '?' + params;
            }
        }
    });
    
    //выстовить размер дочерним меню
    //$(menu_container).find("ul.submenu").each(function () {
    //    var pwidth = $(this).prev().outerWidth(true);

    //    $(this).find("a").each(function () {

    //        $(this).css("min-width", pwidth);
    //    });
    //});

    $(menu_container).find("li").each(function () {
        var submenu = $(this).find("ul.submenu");
        if (submenu.length > 0 && !submenu.hasClass('alwaysdisplay')) {

            //show subnav on hover
            $(this).mouseenter(function () {
                submenu.stop(true, true).slideDown("fast");
            });

            //hide submenus on exit
            $(this).mouseleave(function () {
                submenu.stop(true, true).hide();
            });
        }
    });
}

//выставить у меню текущию станицу
//если имеется полное соответствие
function UpdateMenuLite(menu) {
    var url = document.location.href;
    
    menu.find("a").each(function () {
        if (this.href == url) {
            $(this).addClass('current');
        }
    });
}


//выставить у меню текущию станицу
//и проставить в ссылках параметры если нет
function UpdateMenuCoord(menu_container, params, useSavingParams) {
    var url = document.location.href.substr(0, document.location.href.lastIndexOf('/')+1);
    var selected = $(menu_container).find("a");
    if (params == undefined)
        params = '';

    if (!params) {
        var urlParams = url.split('?');
        if (urlParams.length > 1) {
            url = urlParams[0];
            params = urlParams[1];
        }
    }

    $.each($(selected), function () {
        var current = false;

        var parent = $(this).closest("ul.submenu");
        //if (this.href && this.href.toLowerCase().indexOf(url.toLowerCase()) == 0) {
        if (this.href && this.href.split('?')[0].toLowerCase().indexOf(url.toLowerCase()) == 0) {

            $(this).addClass('current');
            //$(this).removeAttr('href');
            //            $(this).closest("li").addClass('current');

            current = true;

            if (parent.length > 0) {
                var parentlink = parent.siblings("a");

                parentlink.addClass('current');
                parentlink.removeAttr('href');
            }
        }

        if (useSavingParams) {
            urlParams = this.href.split('?');
            if (urlParams.length == 1 && params && !current) {
                this.href = this.href + '?' + params;
            }
        }
    });

    //выстовить размер дочерним меню
    //$(menu_container).find("ul.submenu").each(function () {
    //    var pwidth = $(this).prev().outerWidth(true);

    //    $(this).find("a").each(function () {

    //        $(this).css("min-width", pwidth);
    //    });
    //});

    $(menu_container).find("li").each(function () {
        var submenu = $(this).find("ul.submenu");
        if (submenu.length > 0 && !submenu.hasClass('alwaysdisplay')) {

            //show subnav on hover
            $(this).mouseenter(function () {
                submenu.stop(true, true).slideDown("fast");
            });

            //hide submenus on exit
            $(this).mouseleave(function () {
                submenu.stop(true, true).slideUp("fast");
            });
        }
    });
}

function setCookie(name, value, key, expires, path, domain, secure) {
    document.cookie =
        ((key) ? name + "=" + key + "=" + escape(value) : name + "=" + escape(value))  +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

function getCookie(name) {
    var cookie = " " + document.cookie;
    var search = " " + name + "=";
    var setStr = null;
    var offset = 0;
    var end = 0;
    if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = cookie.indexOf(";", offset);
            if (end == -1) {
                end = cookie.length;
            }
            setStr = unescape(cookie.substring(offset, end));
        }
    }
    return (setStr);
}

function parseDate(strDate) {
    var dateParts = strDate.split(".");

    var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);

    return date.toString();
}

function CheckValue(i) {
    if (i < 10) {
        i = "0" + i;
    }

    return i;
}

//Получить текущую дату
function GetDate() {
    var d = new Date();

    var day = CheckValue(d.getDate());
    var month = CheckValue(d.getMonth() + 1);
    var year = d.getFullYear();
    var hour = CheckValue(d.getHours());
    var min = CheckValue(d.getMinutes());

    return (day + "." + month + "." + year + " г.  " + hour + "." + min); //document.write(day + "." + month + "." + year + " г.  " + hour + "." + min);
}


var cooker = {
    set: function (cookie_name, cookie_value, cookie_expires, cookie_path, cookie_domain, cookie_secure) {
        if (cookie_name !== undefined) {
            cookie_expires = cookie_expires || 0;
            var expire_date = new Date;
            expire_date.setTime(expire_date.getTime() + (cookie_expires * 1000));
            document.cookie = cookie_name + "=" + escape(cookie_value) + '; ' +
((cookie_expires === undefined) ? '' : 'expires=' + expire_date.toGMTString() + '; ') +
((cookie_path === undefined) ? 'path=/;' : 'path=' + cookie_path + '; ') +
((cookie_domain === undefined) ? '' : 'domain=' + cookie_domain + '; ') +
((cookie_secure === true) ? 'secure; ' : '');
        }
    },
    get: function (cookie_name) {
        var cookie = document.cookie, length = cookie.length;
        if (length) {
            var cookie_start = cookie.indexOf(cookie_name + '=');
            if (cookie_start != -1) {
                var cookie_end = cookie.indexOf(';', cookie_start);
                if (cookie_end == -1) {
                    cookie_end = length;
                }
                cookie_start += cookie_name.length + 1;
                return unescape(cookie.substring(cookie_start, cookie_end));
            }
        }
    },
    erase: function (cookie_name) {
        cooker.set(cookie_name, '', -1);
    },
    test: function () {
        cooker.set('test_cookie', 'test', 10);
        var work = (cooker.get('test_cookie') === 'test') ? true : false;
        cooker.erase('test_cookie');
        return work;
    }
};