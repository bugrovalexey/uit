﻿using GB.Data.Documents;

namespace GB.Data.AccountingObjects.Documents
{
    /// <summary>
    /// Основания для создания ОУ 
    /// </summary>
    public class AODocumentGroundsForCreation : AODocumentBase
    {
        internal static readonly DocTypeEnum DocType = DocTypeEnum.GroundsForCreationAccountingObject;    

        protected override DocTypeEnum GetTypeDocument()
        {
            return DocType;
        }
    }
}
