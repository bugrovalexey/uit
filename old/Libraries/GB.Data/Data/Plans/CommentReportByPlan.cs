﻿namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Отчет по плану
    /// </summary>
    public class CommentReportByPlan: EntityBase
    {
        public virtual PlansActivity PlansActivity { get; set; }

        public virtual PlansActivityMU PlansActivityMU { get; set; }
        /// <summary>
        /// комментарий для раздела 1.1
        /// </summary>
        public virtual string Comment11 { get; set; }
        /// <summary>
        /// комментарий для раздела 1.2
        /// </summary>
        public virtual string Comment12 { get; set; }
        /// <summary>
        /// комментарий для раздела 2
        /// </summary>
        public virtual string Comment2 { get; set; }
        /// <summary>
        /// комментарий для раздела 3.1
        /// </summary>
        public virtual string Comment31 { get; set; }
        /// <summary>
        /// комментарий для раздела 3.2
        /// </summary>
        public virtual string Comment32 { get; set; }
        /// <summary>
        /// Результат, раздел 1.2
        /// </summary>
        public virtual string Result { get; set; }

        /// <summary>
        /// Фактический объем бюджетных ассигнований, раздел 1.2
        /// </summary>
        public virtual decimal BudgetVolume12 { get; set; }

        /// <summary>
        /// Фактический объем софинансирования, раздел 1.2
        /// </summary>
        public virtual decimal Cofinance12 { get; set; }

        /// <summary>
        /// Фактический объем бюджетных ассигнований, раздел 2
        /// </summary>
        public virtual decimal BudgetVolume2 { get; set; }
    }
}
