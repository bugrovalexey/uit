﻿using GB.Data.Directories;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Внешние пользовательские интерфейсы ИС
    /// </summary>
    public class ExternalUserInterfaceOfIs: EntityBase
    {
        /// <summary>
        /// Собственник
        /// </summary>
        public virtual AccountingObject Owner { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        public virtual string Purpose { get; set; }

        /// <summary>   
        /// Тип
        /// </summary>
        public virtual TypeOfInterface TypeOfInterface { get; set; }

        /// <summary>
        /// Начальный URL
        /// </summary>
        public virtual string Url { get; set; }
    }
}
