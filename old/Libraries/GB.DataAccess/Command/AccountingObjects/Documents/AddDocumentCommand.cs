﻿using GB.Data.AccountingObjects;
using GB.Data.Documents;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;
using GB.Helpers;
using System;
using System.Collections.Generic;

namespace GB.DataAccess.Command.AccountingObjects.Documents
{
    public class AddDocumentCommand : ICommand<AddDocumentContext, Files>
    {
        ISessionFactoryGb factory;

        public AddDocumentCommand()
        {
            factory = new SessionFactory();
        }

        public Files Execute(AddDocumentContext context)
        {
            Files file = null;

            var entity = new Repository<AccountingObjectUserComment>().Get(context.xId);
            if (entity != null)
            {
                using (var session = factory.Create())
                {
                    string[] pair = context.CommentFiles.Split(':');
                    if (pair.Length < 2)
                        return null;

                    string id = pair[0].Trim();
                    string path = pair[1].Trim();

                    file = new Files(DocTypeEnum.AccountingObjectCommentDocument, entity);
                    file.FileName = file.Name = path;

                    session.Insert(file);

                    string guidString = id;
                    try
                    {
                        int slashIndex = guidString.IndexOf('/');
                        if (slashIndex >= 0)
                        {
                            guidString = guidString.Substring(0, slashIndex);
                        }
                        Guid guid = new Guid(guidString);
                        FileProvider.MoveToGeneric(guid, file.Id);
                    }
                    catch (Exception e)
                    {
                        session.Delete(file);
                        Logger.Fatal(string.Format("Не удалось переместить файл из temp в хранилище. key={0} guidString={1} Document.Id={2}",
                            id, guidString, file.Id), e);
                    }

                    session.Commit();
                }
            }            
            return file;
        }
    }
}
