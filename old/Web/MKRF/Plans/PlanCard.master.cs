﻿using System;
using System.Collections.Generic;
using GB;
using GB.Entity;
using GB.Helpers;
using DevExpress.Web.ASPxTabControl;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Helpers;
using GB.MKRF.Report;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Plans
{
    public partial class PlanCard : System.Web.UI.MasterPage
    {
        PlanControllerOld controller = new PlanControllerOld();
        public const string plansIdKey = "Plan";
        Plan currentPlan = null;
        bool planLoaded = false;

        public Plan CurrentPlan
        {
            get{
                if (!planLoaded)
                {
                    planLoaded = true;
                    try
                    {
                        ExpDemand demand = controller.SetCurrentDemand(new PlanControllerArgs()
                            {
                                PlanId = Request.QueryString[plansIdKey]
                            });

                        currentPlan = demand.ExpObject;
                    }
                    catch(AccessViolationException accessException)
                    {
                        Logger.Error(accessException);
                        ResponseHelper.DenyAccess(Response);
                    }
                    catch(Exception exc)
                    {
                        Logger.Error(exc);
                    }

                    //int id = 0;
                    //if (int.TryParse(Request.QueryString[plansIdKey], out id))
                    //{
                    //    planLoaded = true;
                    //    currentPlan = PlanRepository.GetPlan(id);
                    //    if (currentPlan == null || currentPlan.Demand == null || !Validator.Validator.Validate(currentPlan.Demand, ObjActionEnum.view))
                    //    {
                    //        ResponseHelper.DenyAccess(Response);
                    //    }
                    //}
                }
                return currentPlan;
            }
        }

        public ASPxTabControl TabControl
        {
            get { return tc; }
        }

        public static string GetUrl(int Id, ExpObjectTypeEnum PlanType)
        {
            string url;
            switch (PlanType)
            {
                case ExpObjectTypeEnum.Plan:
                    url = "~/Plans/v2011/ActivityList.aspx";
                    break;

                case ExpObjectTypeEnum.PreliminaryPlan:
                    url = "~/Plans/Preliminary/PreActivityList.aspx";
                    break;

                case ExpObjectTypeEnum.Plan2012:
                    url = "~/Plans/v2012/ActivityList.aspx";
                    break;
                case ExpObjectTypeEnum.Plan2012new:
                    url = "~/Plans/v2012new/ActivityList.aspx";
                    break;

                //case ExpObjectTypeEnum.Plan2012rel:
                //    url = "~/Plans/v2012new/ActivityList.aspx";
                //    break;

                default:
                    return null;
            }
            return UrlHelper.Resolve(url) + "?" + PlanCard.plansIdKey + "=" + Id;
        }


        //protected string getPlansListLink()
        //{
        //    if (CurrentPlan != null)
        //        return PlanList.GetUrl(CurrentPlan.PlanType, false);
        //    else
        //        return string.Empty;
        //}

        public static string getViewLinks(Plan plan)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (plan != null)
            {
                int Id = plan.Id;

                switch (plan.PlanType)
                {
                    case ExpObjectTypeEnum.Plan2012new:
                    case ExpObjectTypeEnum.SummaryPlanMKS2012new:

                        sb.Append(ReportPage.GetLink("P 1.1", ReportType.Plan2012NewSectionOne_1, Id)); sb.Append(" ");
                        sb.Append(ReportPage.GetLink("P 1.2", ReportType.Plan2012NewSectionOne_2, Id)); sb.Append(" ");
                        sb.Append(ReportPage.GetLink("P 1.3", ReportType.Plan2012NewSectionOne_3, Id)); sb.Append(" ");
                        sb.Append(ReportPage.GetLink("P 1.4", ReportType.Plan2012NewSectionOne_4, Id)); sb.Append(" ");
                        //sb.Append(ReportPage.GetLink("P 1.4.2 и P 1.4.3", ReportType.Plan2012NewSectionOne_5, Id)); sb.Append(" ");

                        sb.Append(ReportPage.GetLink("P 2.1", ReportType.Plan2012NewSectionTwo_1, Id)); sb.Append(" ");
                        sb.Append(ReportPage.GetLink("P 2.2", ReportType.Plan2012NewSectionTwo_2, Id)); sb.Append(" ");
                        sb.Append(ReportPage.GetLink("P 2.3", ReportType.Plan2012NewSectionTwo_3, Id)); sb.Append(" ");
                        sb.Append(ReportPage.GetLink("P 2.4", ReportType.Plan2012NewSectionTwo_4, Id)); sb.Append(" ");

                        sb.Append(ReportPage.GetLink("P 3.1", ReportType.Plan2012NewSectionThree_1, Id)); sb.Append(" ");
                        sb.Append(ReportPage.GetLink("P 3.2", ReportType.Plan2012NewSectionThree_2, Id)); sb.Append(" ");
                        sb.Append(ReportPage.GetLink("P 3.3", ReportType.Plan2012NewSectionThree_3, Id)); sb.Append(" ");

                        sb.Append(ReportPage.GetLink("P 4.1 P 4.2", ReportType.Plan2012NewSectionFour, Id)); sb.Append(" ");
                        sb.Append(ReportPage.GetLink("P 4.3", ReportType.Plan2012NewSectionFour_3, Id)); sb.Append(" ");
                        sb.Append(ReportPage.GetLink("P 4.4", ReportType.Plan2012NewSectionFour_4, Id)); sb.Append(" ");

                        //sb.Append("<div class='activity_div' style='margin-right: 0px;'>");
                        //sb.Append(Section1ReportByPlan.GetLink("Отчет по плану", Id));
                        //sb.Append("</div>");
                        //sb.Append(" ");

                        //sb.AppendFormat("<a href='{0}'>P 1.1</a> ", ReportPage.GetUrl(Report.ReportType.Plan2012NewSectionOne_1, Id));
                        //sb.AppendFormat("<a href='{0}'>P 1.2</a> ", ReportPage.GetUrl(Report.ReportType.Plan2012NewSectionOne_2, Id));
                        //sb.AppendFormat("<a href='{0}'>P 1.3</a> ", ReportPage.GetUrl(Report.ReportType.Plan2012NewSectionOne_3, Id));
                        //sb.AppendFormat("<a href='{0}'>P 1.4.1</a> ", ReportPage.GetUrl(Report.ReportType.Plan2012NewSectionOne_4, Id));
                        //sb.AppendFormat("<a href='{0}'>P 1.4.2 и P 1.4.3</a> ", ReportPage.GetUrl(Report.ReportType.Plan2012NewSectionOne_5, Id));
                        //sb.AppendFormat("<a href='{0}'>P 2.1</a> ", ReportPage.GetUrl(Report.ReportType.Plan2012NewSectionTwo_1, Id));
                        //sb.AppendFormat("<a href='{0}'>P 2.2</a> ", ReportPage.GetUrl(Report.ReportType.Plan2012NewSectionTwo_2, Id));
                        //sb.AppendFormat("<a href='{0}'>P 2.3</a> ", ReportPage.GetUrl(Report.ReportType.Plan2012NewSectionTwo_3, Id));
                        break;


                    //sb.Append(ReportPage.GetLink("P 1.1", ReportType.Plan2012NewSectionOne_1, Id)); sb.Append(" ");
                    //sb.Append(ReportPage.GetLink("P 1.2", ReportType.Plan2012NewSectionOne_2, Id)); sb.Append(" ");
                    //sb.Append(ReportPage.GetLink("P 1.3", ReportType.Plan2012NewSectionOne_3, Id)); sb.Append(" ");
                    //sb.Append(ReportPage.GetLink("P 1.4.1", ReportType.Plan2012NewSectionOne_4, Id)); sb.Append(" ");
                    //sb.Append(ReportPage.GetLink("P 1.4.2 и P 1.4.3", ReportType.Plan2012NewSectionOne_5, Id)); sb.Append(" ");

                    //sb.Append(ReportPage.GetLink("P 2.1", ReportType.Plan2012NewSectionTwo_1, Id)); sb.Append(" ");
                    //sb.Append(ReportPage.GetLink("P 2.2", ReportType.Plan2012NewSectionTwo_2, Id)); sb.Append(" ");
                    //sb.Append(ReportPage.GetLink("P 2.3", ReportType.Plan2012NewSectionTwo_3, Id)); sb.Append(" ");

                    //sb.Append(Section1_2012.GetLink("ОП", Id)); sb.Append(" ");

                    //break;
                }
            }
            return sb.ToString();
        }



        protected string GetPlanInfo(Plan plan)
        {
            if (plan == null)
                return null;

            return string.Concat("ПЛАН ", plan.Year.Value, " г. ");//, plan.PlanType == ExpObjectTypeEnum.Plan2012rel ? "(Реализация)" : "(Планирование)");
        }
        
        public static string GetPlanTitle(Plan plan)
        {
            if (plan == null)
                return null;

            string depLink = ObjectsList.GetUrl(plan.Department.Id);

            return string.Concat("<a style='color: #333' href='", depLink, "'>", plan.Department.Name, "</a>");
        }

        public static string GetStatusInfo(Plan plan)
        {
            if (plan == null)
                return null;

            if (plan.Demand == null)
                return "Заявка отсутствует";

            RoleBase role = UserRepository.GetCurrent().Role;

            if (role.IsEq(RoleEnum.CA) || role.IsEq(RoleEnum.Department))
                return UrlHelper.Link(plan.Demand.Status.Name, "~/Plans/Workflow.aspx", PlanCard.plansIdKey + "=" + plan.Id, "color:#C53939");
            else
                return plan.Demand.Status.Name;

        }

        //protected string getTypeName()
        //{
        //    if (CurrentPlan != null)
        //        return CurrentPlan.GetTypeName();
                
        //    return string.Empty;
        //}

        protected override void OnPreRender(EventArgs e)
        {
            if (CurrentPlan != null)
            {
                //Exportreportcontrol1.Visible = CurrentPlan.PlanType == ExpObjectTypeEnum.Plan || CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012;

                bool showTabs = false;

                if (CurrentPlan.Demand == null)
                {
                    showTabs = false;

                    // TODO: demand не используется
                    ExpDemand demand = new ExpDemand();
                    demand.ExpObject = CurrentPlan;
                    demand.Status = StatusRepositoryOld.GetFirstStatus(demand);
                    demand.Owner = UserHelper.GetUserInfo(UserRepository.GetCurrent());
                    
                }
                else
                {
                    showTabs = true;
                    tc.Visible = true;
                }

                IList<string> urlParameters = new List<string>();

                // Настройка ссылок
                foreach (Tab t in tc.Tabs)
                {
                //    t.Visible = DbSiteMapProvider.СheckAccessUser(t.NavigateUrl);

                //    if (!t.Visible)
                //        continue;

                    urlParameters.Clear();

                //    if (t.Name != "Activity")
                //    {
                //        t.Visible = showTabs;

                //        if (!t.Visible)
                //            continue;
                //    }


                    switch (t.Name)
                    {
                        //case "MonitoringActivities":
                        //    t.Visible = CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012rel;
                        //    break;


                        //        case "PurchasePlan":
                        //            t.Visible = CurrentPlan.PlanType == ExpObjectTypeEnum.Plan;
                        //            break;

                        //        case "PreActivity":
                        //        case "PreHistory":
                        //            t.Visible = CurrentPlan.PlanType == ExpObjectTypeEnum.PreliminaryPlan;
                        //            break;

                        //        case "Activity2012":
                        //        case "History2012":
                        //            t.Visible = CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012;
                        //            break;

                        //        case "Activity2012new":
                        //        //case "ActivityMU2012new":
                        case "History2012new":
                            t.Visible = CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012new || CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new;
                            break;

                        case "Workflow":
                            t.Visible = CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012new || CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new;
                            break;

                        //        case "Documents":
                        //            t.NavigateUrl = (CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012new || CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new)
                        //                                            ? v2012new.Documents.GetUrl(0) :
                        //                                              Documents.GetUrl(0);
                        //            break;

                        //        case "Inventory":
                        //            t.Visible = (CurrentPlan.PlanType != ExpObjectTypeEnum.Plan2012new && CurrentPlan.PlanType != ExpObjectTypeEnum.SummaryPlanMKS2012new); 
                        //            break; 

                        //        case "CCActivities":
                        //            t.Visible = (CurrentPlan.PlanType == ExpObjectTypeEnum.PreliminaryPlan || CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012);
                        //            break;

                        //        /*
                        //    case "CommonConcTab":
                        //        // TODO: Вынести в валидатор или создавать общее ЭЗ вместе с планом
                        //        // TODO: Статусы ФС и УП совпадают.
                        //        t.Visible = (CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012
                        //            && !StatusEnum.PreCreated.IsEq(CurrentPlan.Demand.Status));
                        //        break;

                        //    case "ConcTab":
                        //        if ((CurrentPlan.PlanType != ExpObjectTypeEnum.Plan2012new && CurrentPlan.PlanType != ExpObjectTypeEnum.SummaryPlanMKS2012new) && UserRepository.GetCurrent().Role.IsEq(RoleEnum.MKS))
                        //            t.Visible = false;
                        //        // если не новый план, покажем старую форму экспертного заключения
                        //        if (CurrentPlan.PlanType != ExpObjectTypeEnum.Plan2012new && CurrentPlan.PlanType != ExpObjectTypeEnum.SummaryPlanMKS2012new)
                        //        {
                        //            t.NavigateUrl = "~/Plans/ExpertConclusionOld.aspx";
                        //        }
                        //        break;

                        //    case "SummaryConclusion":
                        //        t.Visible = CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS;
                        //        break;
                        //         * 
                        //        */
                        //        case "ExpertFormHelp":
                        //            t.Visible = CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012;
                        //            break;

                        //        case "Mails":
                        //            t.Visible = (CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012new || CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new);
                        //            break;

                        //        case "RekInbox":
                        //            t.Visible = (CurrentPlan.PlanType != ExpObjectTypeEnum.Plan2012new && CurrentPlan.PlanType != ExpObjectTypeEnum.SummaryPlanMKS2012new);
                        //            t.Text = "Реквизиты исх. от ОГВ";
                        //            break;
                        //        case "MksEntry":
                        //            t.Visible = (CurrentPlan.PlanType != ExpObjectTypeEnum.Plan2012new && CurrentPlan.PlanType != ExpObjectTypeEnum.SummaryPlanMKS2012new);
                        //            t.Text = "Реквизиты исх. от ОГВ";
                        //            break;

                        default: break;
                    }

                    if (!t.Visible)
                        continue;

                    urlParameters.Add(plansIdKey + "=" + currentPlan.Id);
                    t.NavigateUrl += ((t.NavigateUrl.Contains("?") ? "&" : "?") + StringHelper.Join(urlParameters, "&"));
                }

                Response.Cookies.Add(CookieHelper.MakeCookie("ObjectsListDepartment", new Dictionary<string, string>() {{"depId",CurrentPlan.Department.Id.ToString()}},DateTime.Now.AddMonths(3)));

                //stageRow.Visible = (CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012 || CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012new);
             
            }
            base.OnPreRender(e);
        }

        //#region Export
        //protected void ExportReportControl_BeforeDataBind(Common.ExportReportControl sender, Common.ExportDataEventArgs e)
        //{
        //    e.Data.Add(new object[] { CurrentPlan.Id });
        //}

        //protected void ExportReportControl_AddExportReportType(Common.ExportReportControl sender, DevExpress.Web.ASPxEditors.ListEditItemCollection item)
        //{
            
        //    if (CurrentPlan.PlanType == ExpObjectTypeEnum.Plan)
        //    {
        //        item.Add("В обычном виде", ReportTypeExport.PlanSectionFull);
        //        item.Add("В расширенном виде", ReportTypeExport.PlanSectionFullEx);
        //    }

        //    if (CurrentPlan.PlanType == ExpObjectTypeEnum.PreliminaryPlan)
        //    {
        //        item.Add("Неизвестно", ReportTypeExport.NPlanBudgetAllocation);
        //    }

        //    if (CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012)
        //    {
        //        item.Add("В обычном виде", ReportTypeExport.Plan2012SectionFull);
        //    }
        //}
        //#endregion

        //protected string GetHtmlVersionUrl()
        //{
        //    return UrlHelper.Resolve(string.Format("~/Plans/PlanHtmlVersion.aspx?{0}={1}", PlanHtmlVersion.PlanIdKey, CurrentPlan.Id));
        //}
    }
}