﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Web.ASPxEditors;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.MKRF.Web.Helpers;

namespace GB.MKRF.Web.Plans.v2012new
{
    public class IndicatorsFormControl : System.Web.UI.UserControl
    {
        public static string GetActivityFuncName(PlansActivityFunc func)
        {
            return PlansActivityFuncController.GetName(func);
        }

        public static int GetBaseIndicatorYear(PlansActivity activity)
        {
            return activity.Plan.Year.Value - 1;
        }
    }
}