﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class CategoryKindOfSupportMap : ClassMapHiLo<CategoryKindOfSupport>
    {
        public CategoryKindOfSupportMap()
            : base("VO_Category")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "VO_Category_Name");
            Map(x => x.Type, "VO_Category_Value").CustomType(typeof(CategoryTypeOfSupportEnum)).Nullable();
        }
    }
}