﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class WorksCreateOrUpdateWorkStoryContext
    {
        public int? Id { get; set; }

        public int? ActivityId { get; set; }

        public string Name { get; set; }

        //public int? OKPDId { get; set; }

        //public int? expenseDirectionId { get; set; }

        //public int? expenseTypeId { get; set; }

        //public int? KOSGUId { get; set; }

        //public int? OKVEDId { get; set; }

        //public YearEnum Year { get; set; }

        //public int? specialistsCount { get; set; }

        //public decimal? salary { get; set; }

        public int? Duration { get; set; }

        public decimal? Summ { get; set; }

        public QuarterEnum? Quarter { get; set; }
    }
}
