﻿using Uviri.MKRF.Entities.Prognosis.SelectionV;

namespace Uviri.MKRF.Mapping.Prognosis.SelectionV
{
    public class FinanceResourceMap : ClassMapBase<FinanceResource>
    {
        public FinanceResourceMap()
            : base("MF5_31")
        {
//            Table("w_");

//            Id(x => x.Id, "MF5_31_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF5_31_Insert
//                                  @MF5_31_Num = ?
//                                 ,@MF5_31_Sub_Item = ?
//                                 ,@MF5_31_Code = ?
//                                 ,@MF5_31_Prev = ?
//                                 ,@MF5_31_Cur = ?
//                                 ,@MF5_ID = ?
//                                 ,@MF5_31_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF5_31_Update
//                                  @MF5_31_Num = ?
//                                 ,@MF5_31_Sub_Item = ?
//                                 ,@MF5_31_Code = ?
//                                 ,@MF5_31_Prev = ?
//                                 ,@MF5_31_Cur = ?
//                                 ,@MF5_ID = ?
//                                 ,@MF5_31_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF5_31_Delete
//						  @MF5_31_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "MF5_31_Num");
            Map(x => x.Group, "MF5_31_Sub_Item");
            Map(x => x.Code, "MF5_31_Code");
            Map(x => x.Previous, "MF5_31_Prev");
            Map(x => x.Current, "MF5_31_Cur");

            References<FinanceInfo>(x => x.FinanceInfo, "MF5_ID").Not.Nullable();
        }
    }
}
