﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using GB;
using GB.Handlers;
using GB.Helpers;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Entities.Statuses;
using GB.MKRF.Repository;
using GB.MKRF.Web.Plans;
using GB.MKRF.Web.Plans.v2012new;
using GB.Report;
using GB.Repository;
using DevExpress.Web.ASPxClasses;
//using Uviri.MKRF.Entities.OtherExpObjects;
using GB.MKRF.Report;
using CommonData = GB.MKRF.Web.Plans.v2012new.CommonData;
using Documents = GB.MKRF.Web.Plans.v2012new.Documents;

namespace GB.MKRF.Web.Common
{
    public partial class StatusChangeControl : System.Web.UI.UserControl
    {
        class signedDocument
        {
            public signedDocument(bool preview)
            {
                if (!preview)
                {
                    zip = new ZipFileCollection();
                }
            }
            public string Name { get; set; }
            public string FileName { get; set; }
            public string Preview { get; set; }

            // файл с описью  
            //private string textFile;

            public byte[] GetData()
            {
                return zip.CreateZip();
            }

            public void AddFilesToArchive(object files)
            {
                Document doc = files as Document;

                if (doc != null)
                {
                    AddToArchive(doc.File, doc.FileName);
                }


                var docs = files as ICollection<Document>;

                if (docs != null)
                {
                    foreach (Document d in docs)
                    {
                        AddToArchive(d.File, d.FileName);
                    }
                }
            }

            public void AddTextToArchive(string text, string fileName = "Опись.txt", Encoding enc = null)
            {
                if (enc == null)
                {
                    enc = Encoding.GetEncoding(1251);
                }
                byte[] data = enc.GetBytes(text);
                AddToArchive(data, fileName);
            }

            public void AddToArchive(byte[] data, string fileName)
            {
                MemoryStream memStream = new MemoryStream(data ?? new byte[0]);
                zip.AddFile(memStream, fileName ?? "Без имени");
            }

            public ZipFileCollection zip { get; protected set; }
        }

        public class OwnerRequestEventArgs
        {
            public EntityBase Owner { get; set; }
        }

        public delegate void OwnerRequestEventHandler(object sender, OwnerRequestEventArgs e);

        public event OwnerRequestEventHandler OwnerRequest;


        private EntityBase owner = null;
        /// <summary>
        /// Объект со статусом
        /// </summary>
        public EntityBase Owner
        {
            get
            {
                if (owner == null && OwnerRequest != null)
                {
                    OwnerRequestEventArgs args = new OwnerRequestEventArgs();
                    OwnerRequest(this, args);

                    owner = args.Owner;

                }

                if (owner == null)
                {
                    throw new ArgumentException("owner = null");
                }

                return owner;
            }
            set
            {
                owner = value;
            }
        }



        /// <summary>План</summary>
        private Plan plan_internal = null;
        public Plan plan
        {
            get
            {
                if (plan_internal == null)
                {
                    if (Request["Plan"] == null)
                    {
                        return null;
                    }
                    plan_internal = PlanRepository.Get(int.Parse(Request["Plan"]));
                    if (plan_internal == null)
                        throw new Exception("Не найден план с Id = " + Request["Plan"]);
                }
                return plan_internal;
            }
        }

        public bool RefreshOpenerAfterChangeStatus
        {
            get;
            set;
        }

        protected Status CurrentStatus
        {
            get
            {
                //if (owner is ExpConclusion)
                //    return ((ExpConclusion)owner).Status;
                if (Owner is ExpDemandBase)
                    return ((ExpDemandBase)owner).Status;
                //else if (Owner is ExpObjectConclusion)
                //    return ((ExpObjectConclusion)Owner).Status;
                else
                    throw new Exception(owner.GetType().ToString());
            }
        }

        protected bool allowChange = false;
        protected string warningMessage = null;
        protected string errorCodes = null;

        //protected int IsExceed
        //{
        //    get
        //    {

        //        ExpDemand demand = Owner as ExpDemand;
        //        User currentUser = UserRepository.GetCurrent();
        //        if (demand != null
        //            && demand.ExpObject.PlanType == ExpObjectTypeEnum.Plan2012new
        //            && (currentUser.Role.IsEq(RoleEnum.Department) || currentUser.Role.IsEq(RoleEnum.Department)))
        //        {
        //            return Convert.ToInt32(PlanRepository.IsExceed(demand.ExpObject));
        //        }
        //        else
        //            return 0;
        //    }
        //}

        protected string cryptoProUrl
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["CryptoProCSPDownloadUrl"];
            }
        }

        protected string pluginUrl
        {
            get
            {
                return ResolveClientUrl(System.Configuration.ConfigurationManager.AppSettings["DigitalSignaturePluginDownloadUrl"]);
            }
        }


        private string signPreview_Internal = null;
        /// <summary>Ссылки или содержимое файлов, которые будут подписаны</summary>
        protected string signPreview
        {
            get
            {
                if (signPreview_Internal == null)
                {
                    signedDocument reportPreview = getReportData(true);

                    if (reportPreview == null)
                        signPreview_Internal = String.Empty;
                    else
                        signPreview_Internal = "<div class=\"sopanel\"><span>Подписываемые данные:</span>" + reportPreview.Preview + "</div>";
                }

                return signPreview_Internal;
            }
        }

        /// <summary>Получение активной (последней) бумажной формы</summary>
        private PaperObject getLastPaper()
        {
            // TODO: Оптимизировать и перенести в репозитории.

            IList<PaperObject> papers = RepositoryBase<PaperObject>.GetAll(
                new GetAllArgs<PaperObject>()
                {
                    Expression = (x => x.Owner == Owner),
                    OrderByDesc = (x => x.DateOutgoing)
                });

            return (papers != null && papers.Count > 0) ? papers[0] : null;
        }

        // Возвращает подписанный ОГВ архив, который был сформирован 
        // при последнем переводе статуса в "Отправлено в МКС" или null
        private Document getOgvFile(ExpDemand demand)
        {
            IList<Document> docs = RepositoryBase<Document>.GetAll(
                new GetAllArgs<Document>()
                {
                    Expression = (x => x.Owner == demand),
                });


            return docs.Where(x => (!string.IsNullOrEmpty(x.Name) && x.Name.Contains("ОГВ_Отправлено в МКС"))).OrderByDescending(x => x.DateCreate).FirstOrDefault();
        }

        private IList<Document> getPaperFileList(PaperObject paper, DocToPaperType docPaperType)
        {
            IList<DocToPaper> dtp = RepositoryBase<DocToPaper>.GetAll(new GetAllArgs<DocToPaper>()
            {
                Expression = (x => x.PaperObject == paper),
                Fetch = new Expression<Func<DocToPaper, object>>[] { x => x.Document }
            });

            // отсеиваем запрещнные документы
            AccessRepository.FillAccess<DocToPaper>(dtp, AccessAction.Read);

            return dtp.Where(z => z.Type == docPaperType && (z.Access & AccessAction.Read) == AccessAction.Read)
                        .Select(z => z.Document)
                        .ToArray();

        }

        private string getFileInfo(IList<Document> docList, bool preview)
        {
            return string.Concat(docList.Select(doc => (
                (preview ? "<li>" : "\r\n    - ") +
                            (doc.Name ?? "Безымянный") + " (" + (preview ? FileHandler.GetLink(doc.Id.ToString(), doc.FileName) : (doc.FileName ?? "<имя файла не указано>")) + ")" +
                " дата: " + (doc.DateCreate != null ? doc.DateCreate.Value.ToShortDateString() : "<дата не указана>") +
                " автор: " + (doc.Author.FullName)
                )).ToArray());
        }

        /// <summary>Проверка возможности подписать</summary>
        private bool canSigned(EntityBase Owner)
        {
            if (Owner is ExpDemand)
            {
                var prev = signPreview;
                // Планы подпсываем, если в таблице Status_Change_List указан шаблон
                return !string.IsNullOrEmpty(prev);
            }
            //else if (Owner is ExpObjectDemand)
            //{
            //    // Иные документы можно подписать в любом статусе
            //    return true;
            //}
            //if (owner is ExpConclusion)
            //{
            //    // Экспертные заключения по плану можно подписать в любом статусе
            //    return true;
            //}
            //if (owner is ExpObjectConclusion)
            //{
            //    switch (((ExpObjectConclusion)Owner).Status.Id)
            //    {
            //        case (int)StatusEnum.OtherExpToExpert:
            //            return true;
            //    }
            //}

            return false;

        }

        private signedDocument getPlan2012Report(ExpDemand Owner, Status nextStatus, bool preview)
        {
            //Для всех документов, формируемых в результате подписания или же смены статуса (исключение для экспертных заключений) 
            // формировать название файла по следующей схеме:   Роль_Статус_Дата_Фамилия_ЭЦП
            //где:
            //    роль - роль пользователя, который инициировал смену статуса, а соответственно и формирование архива;
            //    статус - статус, в который была переведена заявка (например, если переводим из создано в отправлено в мкс, то значение = "Отпр_в_МКС"
            //    Дата - дата формирования архива;
            //    Фамилия - фамилия пользователя, сертификат которого использовался (в случае с ЭЗ - фамилия эксперта);
            //    ЭЦП - условный признак, использовался ли ЭЦП при смене статуса (Если использовался, то в конце добавляем"_ЭЦП", если же не использовался, то ничего не добавляем);

            signedDocument result = new signedDocument(preview);

            PaperObject paper = getLastPaper();
            if (paper == null)
                return null;

            DocToPaperType dtpType;
            User currentUser = UserRepository.GetCurrent();

            switch (currentUser.Role.Id)
            {
                case (int)RoleEnum.CA:
                    dtpType = DocToPaperType.Mks;
                    break;
                case (int)RoleEnum.Department:
                //case (int)RoleEnum.DepartmentMKS:
                    dtpType = DocToPaperType.Ogv;
                    break;
                default:
                    throw new NotSupportedException();
            }

            int templateNum = StatusRepositoryOld.GetTemplateNum(Owner.Status.Id, nextStatus.Id);

            switch (templateNum)
            {
                case 1:
                    {
                        if (preview)
                        {
                            result.Preview = string.Format(@"
1. Печатные формы:<br />
<ul>
<li> {0}
</ul>
",
        PlanCard.getViewLinks(Owner.ExpObject));
                        }
                        else
                        {
                            // Добавляем файлы с отчётами
                            //ReportData reportData = new ReportData(ReportTypeExport.PlanSectionFull, ExportFormat.Pdf, this.plan.Id);
                            //ReportCreator.Export(result.zip, reportData);
                        }
                    }
                    break;

                case 2:
                    {
                        var files = getPaperFileList(paper, dtpType);

                        if (preview)
                        {
                            result.Preview = string.Format(@"
1. Реквизиты исходящего от ОГВ:
<br />
<ul>
    <li> Исходящий номер: {0}
    <li> Дата исходящего: {1}
</ul>
2. Файлы, прикрепленные к реквизитам письма:<br />
<ul>
{2}
</ul>
3. Печатные формы:<br />
<ul>
<li> {3}
</ul>
4. Сведения плана:<br />
<ul>
<li> {4}
</ul>",
        paper.NumberOutgoing,
        paper.DateOutgoing != null ? paper.DateOutgoing.Value.ToShortDateString() : "не указана",
        getFileInfo(files, preview),
        PlanCard.getViewLinks(Owner.ExpObject),
        getXml(true));

                        }
                        else
                        {
                            // Добавляем файл с отчётом
                            //ReportData reportData = new ReportData(ReportTypeExport.Plan2012NewSectionFull, ExportFormat.Pdf, this.plan.Id);
                            //ReportCreator.Export(result.zip, reportData);

                            // Добавляем файлы прикрепленные к реквизитам письма
                            result.AddFilesToArchive(files);
                            result.AddTextToArchive(getXml(false), "Сведения плана.xml", Encoding.Unicode);

                            result.AddTextToArchive(string.Format(@"
1. Реквизиты исходящего от ОГВ:

    - Исходящий номер: {0}
    - Дата исходящего: {1}


2. Файлы, прикрепленные к реквизитам письма:
    {2}


3. Печатные формы (файлы в архиве)

4. Сведения плана (файл в архиве)",
            paper.NumberOutgoing,
            paper.DateOutgoing != null ? paper.DateOutgoing.Value.ToShortDateString() : "не указана",
            getFileInfo(files, preview)));
                        }
                    }
                    break;

                case 3:
                    {
                        var files = getPaperFileList(paper, dtpType);
                        if (preview) // превью
                        {
                            result.Preview = string.Format(@"
1. Реквизиты исходящего от МКС:
<br/>
<ul>
    <li> Исходящий номер Минкультуры: {0}
    <li> Дата исходящего: {1}
    <li> Комментарий: {2}
</ul>
2. Файлы, прикрепленные к реквизитам письма:
<br/>
<ul>
{3}
</ul>",
                             paper.NumberOutgoingMKS,
                             paper.DateOutgoingMKS != null ? paper.DateOutgoingMKS.Value.ToShortDateString() : null,
                             paper.Comment,
                             getFileInfo(files, preview));
                        }
                        else // файл
                        {
                            // Добавляем файлы прикрепленные к реквизитам письма
                            result.AddFilesToArchive(files);
                            result.AddTextToArchive(string.Format(@"
1. Реквизиты исходящего от МКС
    
    - Исходящий номер Минкультуры: {0}
    - Дата исходящего: {1}
    - Комментарий: {2}


2. Файлы, прикрепленные к реквизитам письма
    {3}
",
                             paper.NumberOutgoingMKS,
                             paper.DateOutgoingMKS != null ? paper.DateOutgoingMKS.Value.ToShortDateString() : null,
                             paper.Comment,
                             getFileInfo(files, preview)));
                        }
                    }
                    break;

                case 4:
                    {
                        var files = getPaperFileList(paper, DocToPaperType.Ogv);

                        Document ogvSignedFile = getOgvFile(Owner);
                        if (preview) // превью
                        {
                            result.Preview = string.Format(@"
1.Реквизиты исходящего от ОГВ с номером входящего в МКС:
<br/>
<ul>
    <li> Исходящий номер: {0}
    <li> Дата исходящего: {1}
    <li> Входящий номер: {2}
    <li> Дата входящего: {3}    
    <li> Статус: {4}
</ul>
2. Файлы, прикрепленные к реквизитам письма:
<br/>
<ul>
    {5}
</ul>
3. Подписанный пакет документов, полученный от ОГВ:
<br/>
<ul>
    {6}
</ul>",

                            paper.NumberOutgoing,
                            paper.DateOutgoing.HasValue ? paper.DateOutgoing.Value.ToShortDateString() : null,
                            paper.NumberInboxMKS,
                            paper.DateInboxMKS.HasValue ? paper.DateInboxMKS.Value.ToShortDateString() : null,
                            paper.Status != null ? paper.Status.Name : "<не указан>",
                            getFileInfo(files, preview),
                            ogvSignedFile != null ? getFileInfo(new List<Document>() { ogvSignedFile }, true) : null);

                        }
                        else // файл
                        {
                            // Добавляем файлы прикрепленные к реквизитам письма
                            result.AddFilesToArchive(files);
                            result.AddFilesToArchive(ogvSignedFile);

                            result.AddTextToArchive(string.Format(@"
1.Реквизиты исходящего от ОГВ с номером входящего в МКС:

    - Исходящий номер: {0}
    - Дата исходящего: {1}
    - Входящий номер: {2}
    - Дата входящего: {3}    
    - Статус: {4}


2. Файлы, прикрепленные к реквизитам письма:
    {5}
1. Реквизиты исходящего от МКС<br/>
<ul>
    <li> Исходящий номер Минкультуры: {0}
    <li> Дата исходящего: {1}
    <li> Комментарий: {2}
</ul>
2. Файлы, прикрепленные к реквизитам письма
<ul>
{3}
</ul>

3. Подписанный пакет документов, полученный от ОГВ:
    {6}
",
paper.NumberOutgoing,
paper.DateOutgoing.HasValue ? paper.DateOutgoing.Value.ToShortDateString() : null,
paper.NumberInboxMKS,
paper.DateInboxMKS.HasValue ? paper.DateInboxMKS.Value.ToShortDateString() : null,
paper.Status != null ? paper.Status.Name : "<не указан>",
getFileInfo(files, preview),
ogvSignedFile != null ? getFileInfo(new List<Document>() { ogvSignedFile }, false) : null
));
                        }
                    }
                    break;


                case 5:
                    {
                        var files = getPaperFileList(paper, dtpType);

                        if (preview) // превью
                        {
                            result.Preview = string.Format(@"
1. Реквизиты исходящего от МКС<br/>
<ul>
    <li> Исходящий номер Минкультуры: {0}
    <li> Дата исходящего: {1}
    <li> Комментарий: {2}
</ul>
2. Файлы, прикрепленные к реквизитам письма
<ul>
{3}
</ul>",
    paper.NumberOutgoingMKS,
    paper.DateOutgoingMKS != null ? paper.DateOutgoingMKS.Value.ToShortDateString() : null,
    paper.Comment,
    getFileInfo(files, true));
                        }
                        else // файл
                        {
                            result.AddFilesToArchive(files);
                            result.AddTextToArchive(string.Format(@"
1. Реквизиты исходящего от МКС
    - Исходящий номер Минкультуры: {0}
    - Дата исходящего: {1}
    - Комментарий: {2}

2. Файлы, прикрепленные к реквизитам письма
    {3}
",
     paper.NumberOutgoingMKS,
     paper.DateOutgoingMKS != null ? paper.DateOutgoingMKS.Value.ToShortDateString() : null,
     paper.Comment,
     getFileInfo(files, false)));
                        }
                    }
                    break;

                default:
                    return null;
            }
            return result;
        }

//        private signedDocument getOtherExpReport(ExpObjectDemand Owner, Status nextStatus, bool preview)
//        {
//            PaperObject paper = getLastPaper();
//            if (paper == null)
//                return null;

//            User currentUser = UserRepository.GetCurrent();
//            signedDocument result = new signedDocument(preview);
//            IList<Document> files = null;

//            switch (Owner.Status.Id)
//            {
//                /////////////////////////////////////////////////////////
//                // создано ---> отправлено в мкс
//                // запрошены дм ---> отправлено в мкс
//                // Исходящий номер (лев):
//                // Дата исходящего (лев):
//                // Файлы
//                /////////////////////////////////////////////////////////
//                case (int)StatusEnum.OtherCreated:
//                case (int)StatusEnum.OtherMaterialsRequested:

//                    files = getPaperFileList(paper, DocToPaperType.Ogv);
//                    if (preview)
//                    {
//                        result.Preview = string.Format(@"
//1. Реквизиты исходящего от ОГВ:
//<br />
//<ul>
//    <li> Исходящий номер: {0}
//    <li> Дата исходящего: {1}
//</ul>
//2. Файлы, прикрепленные к реквизитам письма:<br />
//<ul>
//{2}
//</ul>
//3. Сведения:<br />
//<ul>
//    <li> {3}
//</ul>
//",
//                        paper.NumberOutgoing,
//                        paper.DateOutgoing != null ? paper.DateOutgoing.Value.ToShortDateString() : "не указана",
//                        getFileInfo(files, preview),
//                        getXml(true));
//                    }
//                    else
//                    {
//                        // Добавляем файлы прикрепленные к реквизитам письма
//                        result.AddFilesToArchive(files);
//                        result.AddTextToArchive(getXml(false), "Сведения.xml", Encoding.Unicode);

//                        result.AddTextToArchive(string.Format(@"
//1. Реквизиты исходящего от ОГВ:
//
//    - Исходящий номер: {0}
//    - Дата исходящего: {1}
//
//
//2. Файлы, прикрепленные к реквизитам письма:
//    {2}
//
//3. Сведения (файл в архиве)",
//        paper.NumberOutgoing,
//        paper.DateOutgoing != null ? paper.DateOutgoing.Value.ToShortDateString() : "не указана",
//        getFileInfo(files, preview)

//        ));
//                    }
//                    break;

//                /////////////////////////////////////////////////////////
//                // отправлено в мкс ---> зарегистрировано
//                // зарегистрировано ---> на экспертизе
//                // Все поля (лев)
//                // Файлы
//                /////////////////////////////////////////////////////////
//                case (int)StatusEnum.OtherPostedInTheMKS:
//                case (int)StatusEnum.OtherRegistered:
//                    files = getPaperFileList(paper, DocToPaperType.Ogv);
//                    if (preview)
//                    {
//                        result.Preview = string.Format(@"
//1.Реквизиты исходящего от ОГВ с номером входящего в МКС:
//<br/>
//<ul>
//    <li> Исходящий номер: {0}
//    <li> Дата исходящего: {1}
//    <li> Входящий номер: {2}
//    <li> Дата входящего: {3}    
//    <li> Статус: {4}
//</ul>
//2. Файлы, прикрепленные к реквизитам письма:
//<br/>
//<ul>
//    {5}
//</ul>
//",
//                       paper.NumberOutgoing,
//                       paper.DateOutgoing.HasValue ? paper.DateOutgoing.Value.ToShortDateString() : null,
//                       paper.NumberInboxMKS,
//                       paper.DateInboxMKS.HasValue ? paper.DateInboxMKS.Value.ToShortDateString() : null,
//                       paper.Status != null ? paper.Status.Name : "<не указан>",
//                       getFileInfo(files, preview));
//                    }
//                    else
//                    {
//                        result.AddFilesToArchive(files);
//                        result.AddTextToArchive(string.Format(@"
//1.Реквизиты исходящего от ОГВ с номером входящего в МКС:
//
//    - Исходящий номер: {0}
//    - Дата исходящего: {1}
//    - Входящий номер: {2}
//    - Дата входящего: {3}    
//    - Статус: {4}
//
//
//2. Файлы, прикрепленные к реквизитам письма:
//    {5}
//",
//                        paper.NumberOutgoing,
//                        paper.DateOutgoing.HasValue ? paper.DateOutgoing.Value.ToShortDateString() : null,
//                        paper.NumberInboxMKS,
//                        paper.DateInboxMKS.HasValue ? paper.DateInboxMKS.Value.ToShortDateString() : null,
//                        paper.Status != null ? paper.Status.Name : "<не указан>",
//                        getFileInfo(files, preview)));
//                    }
//                    break;

//                case (int)StatusEnum.OtherOnExpertise:
//                    {
//                        switch ((int)nextStatus.Id)
//                        {
//                            /////////////////////////////////////////////////////////
//                            // на экспертизе ---> запрошены дм
//                            // Все поля(прав)
//                            // Файлы
//                            /////////////////////////////////////////////////////////
//                            case (int)StatusEnum.OtherMaterialsRequested:
//                                files = getPaperFileList(paper, DocToPaperType.Mks);
//                                if (preview)
//                                {
//                                    result.Preview = string.Format(@"
//1. Реквизиты исходящего от МКС:
//<br/>
//<ul>
//    <li> Исходящий номер Минкультуры: {0}
//    <li> Дата исходящего: {1}
//    <li> Комментарий: {2}
//</ul>
//2. Файлы, прикрепленные к реквизитам письма:
//<br/>
//<ul>
//{3}
//</ul>",
//                                    paper.NumberOutgoingMKS,
//                                    paper.DateOutgoingMKS != null ? paper.DateOutgoingMKS.Value.ToShortDateString() : null,
//                                    paper.Comment,
//                                    getFileInfo(files, preview));
//                                }
//                                else
//                                {
//                                    result.AddFilesToArchive(files);
//                                    result.AddTextToArchive(string.Format(@"
//1. Реквизиты исходящего от МКС:
//    
//    - Исходящий номер Минкультуры: {0}
//    - Дата исходящего: {1}
//    - Комментарий: {2}
//
//
//2. Файлы, прикрепленные к реквизитам письма:
//    {3}
//",
//                                     paper.NumberOutgoingMKS,
//                                     paper.DateOutgoingMKS != null ? paper.DateOutgoingMKS.Value.ToShortDateString() : null,
//                                     paper.Comment,
//                                     getFileInfo(files, preview)));
//                                }
//                                break;

//                            /////////////////////////////////////////////////////////
//                            // на экспертизе ---> оформление ответа
//                            // сводное ЭЗ
//                            /////////////////////////////////////////////////////////
////                            case (int)StatusEnum.OtherReply:

////                                if (Owner.SummaryConclusion != null && preview)
////                                {
////                                    result.Preview = string.Format(@"
////<a href='{0}'>Сводное экспертное заключение</a>",
////                                        ReportPage.GetUrl(ReportType.ExpertObjectAdvice, Owner.SummaryConclusion.Id));
////                                }
////                                else
////                                {
////                                }
////                                break;
//                        }
//                    }
//                    break;

//                /////////////////////////////////////////////////////////
//                // оформление ответа ---> согласовано с замечаниями
//                // оформление ответа ---> согласовано
//                // Все поля (прав)
//                // Файлы
//                /////////////////////////////////////////////////////////
//                // TODO: Исключить дублирование. Идентично переводу из статуса на экспертизе ---> запрошены дм
//                case (int)StatusEnum.OtherReply:
//                    files = getPaperFileList(paper, DocToPaperType.Mks);
//                    if (preview)
//                    {
//                        result.Preview = string.Format(@"
//1. Реквизиты исходящего от МКС:
//<br/>
//<ul>
//    <li> Исходящий номер Минкультуры: {0}
//    <li> Дата исходящего: {1}
//    <li> Комментарий: {2}
//</ul>
//2. Файлы, прикрепленные к реквизитам письма:
//<br/>
//<ul>
//{3}
//</ul>",
//                        paper.NumberOutgoingMKS,
//                        paper.DateOutgoingMKS != null ? paper.DateOutgoingMKS.Value.ToShortDateString() : null,
//                        paper.Comment,
//                        getFileInfo(files, preview));
//                    }
//                    else
//                    {
//                        result.AddFilesToArchive(files);
//                        result.AddTextToArchive(string.Format(@"
//1. Реквизиты исходящего от МКС:
//    
//    - Исходящий номер Минкультуры: {0}
//    - Дата исходящего: {1}
//    - Комментарий: {2}
//
//
//2. Файлы, прикрепленные к реквизитам письма:
//    {3}
//",
//                        paper.NumberOutgoingMKS,
//                        paper.DateOutgoingMKS != null ? paper.DateOutgoingMKS.Value.ToShortDateString() : null,
//                        paper.Comment,
//                        getFileInfo(files, preview)));
//                    }
//                    break;

//                default:
//                    throw new NotSupportedException();
//            }
//            return result;
//        }

        private signedDocument getReportData(bool preview = false)
        {
            if (nextStatusId <= 0)
            {
                if (StatusComboBox.Value != null)
                    nextStatusId = (int)StatusComboBox.Value;
                else
                    return null;
            }

            Status nextStatus = RepositoryBase<Status>.Get(nextStatusId);
            if (nextStatus == null)
                return null;


            signedDocument result = new signedDocument(preview);

            if (Owner is ExpDemandBase)
            {
                if (Owner is ExpDemand) // подписываем только планы 2012
                {
                    if (plan.PlanType != ExpObjectTypeEnum.Plan2012
                        && plan.PlanType != ExpObjectTypeEnum.Plan2012new
                        && plan.PlanType != ExpObjectTypeEnum.Plan)
                    {
                        throw new NotSupportedException("Подписание данного типа плана не поддерживается");
                    }

                    result = getPlan2012Report((ExpDemand)Owner, nextStatus, preview);
                }
                //else if (Owner is ExpObjectDemand) // иные документы
                //{
                //    result = getOtherExpReport((ExpObjectDemand)Owner, nextStatus, preview);
                //}
            }
            //else if (Owner is ExpConclusion) // заключения по планам
            //{
            //    if (preview)
            //    {
            //        // ссылка на отчёт
            //        result.Preview = string.Format("<a href='{0}'>Экспертное заключение</a> ", ReportPage.GetUrl(ReportType.ExpertAdvice, (Owner as ExpConclusion).Id));
            //    }
            //    else
            //    {
            //        ReportData reportData = new ReportData(ReportTypeExport.ExpertAdvice, ExportFormat.Pdf, (Owner as ExpConclusion).Id.ToString());
            //        ReportCreator.Export(result.zip, reportData);
            //    }
            //}
            //else if (owner is ExpObjectConclusion) // заключения по иным документам
            //{
            //    if (preview)
            //    {
            //        // ссылка на отчёт
            //        result.Preview = string.Format("<a href='{0}'>Экспертное заключение</a> ", ReportPage.GetUrl(ReportType.ExpertObjectAdvice, (Owner as ExpObjectConclusion).Id));
            //    }
            //    else
            //    {
            //        ReportData reportData = new ReportData(ReportTypeExport.ExpertObjectAdvice, ExportFormat.Pdf, (Owner as ExpObjectConclusion).Id.ToString());
            //        ReportCreator.Export(result.zip, reportData);
            //    }
            //}
            else
            {
                throw new NotSupportedException(Owner == null ? "Отсутствует объект со статусом" : Owner.GetType().ToString());
            }

            if (!preview && result != null)
            {
                User currentUser = UserRepository.GetCurrent();

                result.Name = string.Join("_", new string[] {
                    currentUser.Role.Name,
                    nextStatus.Name,
                    DateTime.Now.ToShortDateString(),
                    currentUser.Surname,
                    "ЭЦП"
                });
                result.FileName = result.Name + ".zip";
            }

            return result;
        }

        /// <summary>Ссылка на XML плана</summary>        
        private string getXml(bool preview)
        {
            if (preview)
                return "<a href='javascript:ShowXml()'>XML файл</a>";
            else
            {
                XmlDocument xmlDoc = null;

                if (this.Owner is ExpDemand)
                    xmlDoc = StatusRepositoryOld.GetPlanXml((this.Owner as ExpDemand).ExpObject.Id);
                //else if (this.Owner is ExpObjectDemand)
                //    xmlDoc = StatusRepository.GetExpObjectXml(Owner.Id);
                else
                    throw new NotSupportedException();

                using (var stringWriter = new StringWriter())
                using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                {
                    xmlDoc.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                    return stringWriter.GetStringBuilder().ToString();
                }
            }
        }

        /// <summary>Создание файла(архива), по содержимиму которого вычисляется хэш</summary>
        protected void dsc_GetSource(object sender, Controls.GetSourceEventArgs e)
        {
            signedDocument signDoc = getReportData();
            e.Data = signDoc != null ? signDoc.GetData() : null;

            // После того как клиент подпишет хэш, необходимо будет установить связать ЭП с файлом.
            // В переменной Info сохраняем информацию для получения исходного файла 
            // (Символ ; в Info жлательно не использовать)
            string fullPath = FileHelper.SaveInTemp(e.Data);

            // имя файла (GUID) в temp : имя файла
            e.Info = fullPath.Substring(fullPath.LastIndexOf("\\") + 1) + ":" + signDoc.FileName + ":" + signDoc.Name;
        }


        protected void dsc_ReturnSignedData(object sender, Controls.ReturnSignedDataEventArgs e)
        {
            User currentUser = UserRepository.GetCurrent();

            if (!string.IsNullOrEmpty(currentUser.TokenInfo))
            {
                // проверяем сертификат
                if (e.Certificate == null)
                    throw new Exception("Отсутствует сертификат");

                X509Certificate cert = new X509Certificate(System.Convert.FromBase64String(e.Certificate));
                if (!currentUser.TokenInfo.Contains("=") ||
                    !cert.Subject.Contains(currentUser.TokenInfo))
                {
                    throw new Exception("Сертификат не связан с текущим пользователем");
                }
            }

            // Data - исходный хэш, который был подписан
            // Sign - подпись (ЭП)
            // Info - <имя файла в папке Temp>:<имя файла>

            // Создаём документ и сохраняем рядом с ним Sign
            string[] trio = e.Info.Split(':');
            string tempFileName = null;
            string fileName = null;
            string docName = null;

            if (trio.Length > 0)
            {
                tempFileName = trio[0];
                if (trio.Length > 1)
                {
                    fileName = trio[1];
                    if (trio.Length > 2)
                        docName = trio[2];
                    else
                        docName = fileName;
                }
            }
            try
            {

                Document doc = null;

                DocTypeEnum docType;

                if (Owner is ExpDemand && ((ExpDemand)Owner).ExpObject.PlanType == ExpObjectTypeEnum.Plan)
                {
                    // для планов 2011
                    docType = DocTypeEnum.demand_document;
                }
                else
                {
                    switch (currentUser.Role.Id)
                    {
                        //case (int)RoleEnum.Expert:
                        //    docType = DocTypeEnum.PackageExpert;
                        //    break;

                        case (int)RoleEnum.Department:
                        //case (int)RoleEnum.DepartmentMKS:
                            docType = DocTypeEnum.PackageOGV;
                            break;

                        case (int)RoleEnum.CA:
                            docType = DocTypeEnum.PackageMKS;
                            break;

                        default:
                            throw new NotSupportedException();
                    }
                }

                if (Owner is ExpDemandBase)
                {
                    if (Owner is ExpDemand)
                    {
                        doc = new Document(docType, plan.Demand);
                        doc.ContentType = "zip";
                    }
                    //else if (Owner is ExpObjectDemand)
                    //{
                    //    doc = new Document(docType, Owner as ExpObjectDemand);
                    //    doc.ContentType = "zip";
                    //}
                }
                //else if (Owner is ExpConclusion)
                //{
                //    doc = new Document(docType, Owner as ExpConclusion);
                //    doc.ContentType = "zip";
                //}
                //else if (Owner is ExpObjectConclusion)
                //{
                //    doc = new Document(docType, Owner as ExpObjectConclusion);
                //    doc.ContentType = "zip";
                //}
                else
                {
                    throw new NotSupportedException(Owner == null ? "Отсутствует объект со статусом" : Owner.GetType().ToString());
                }



                doc.FileName = FileHelper.TranslateFileName(fileName.ToString());
                doc.Name = docName;

                doc.DateCreate = DateTime.Now;
                doc.Author = currentUser;
                doc.Comm = "";

                doc.SignType = e.SignType;
                doc.SignedPackage = e.CMS;
                doc.File = System.IO.File.ReadAllBytes(FileHelper.GetPhysicalPath(new Guid(tempFileName)));

                RepositoryBase<Document>.SaveOrUpdate(doc);
            }
            catch (Exception exp)
            {
                Logger.Fatal("Не удалось сохранить подписанный пакет", exp);
                throw exp;
            }

            changeState();

            if (!RefreshOpenerAfterChangeStatus)
            {
                ASPxWebControl.RedirectOnCallback(Request.RawUrl);
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            //User currentUser = UserRepository.GetCurrent();

            var statuses = StatusRepositoryOld.GetAvailableNew(Owner, out warningMessage);

            if (!string.IsNullOrEmpty(warningMessage))
            {
                warningMessage = string.Concat("<div style=\"color: #C53939; font-weight: bold; font-size: 1.2em;\">Внимание!</div>", warningMessage);
                RefreshStatusGrid();
                //return;
            }

            StringBuilder builder = new StringBuilder();
            int selectedIndex = -1;
            if (statuses.Count > 0)
                foreach (var status in statuses)
                {
                    builder.Append("arr[" + status.Key + "]='");
                    if (status.Value.StatusError.Count > 0)
                        builder.Append(getErrorList(status));
                    else
                    {
                        builder.Append("ok");
                        if (selectedIndex < 0) selectedIndex = status.Key;
                    }
                    builder.Append("';");
                }

            StatusComboBox.DataSource = statuses;
            StatusComboBox.DataBind();

            this.errorCodes = builder.ToString();

            if (selectedIndex >= 0)
            {
                StatusComboBox.SelectedIndex = selectedIndex;
                allowChange = (statuses != null && statuses.Count > 0);
                ChangeStatusButton.ClientEnabled = true;
                CommentMemo.ClientEnabled = true;
            }
            else
            {
                StatusComboBox.Value = null;
                ChangeStatusButton.ClientEnabled = false;
                CommentMemo.ClientEnabled = false;
            }

            allowChange = (statuses != null && statuses.Count > 0);

            CommentMemo.Text = null;
            RefreshStatusGrid();

            //bool showSignPanel = canSigned(owner);
            //SignaturePanel.ClientVisible = false;
            //ApplySignatureCheckBox.Enabled = showSignPanel;


            //// TODO: проверить права на подписание
            //if (owner is ExpDemandBase && showSignPanel)
            //{
            //    if (currentUser.Department.SignatureIsRequired)
            //    {
            //        ApplySignatureCheckBox.Visible = false;
            //        SignaturePanel.ClientVisible = true;
            //    }
            //    else
            //    {
            //        ApplySignatureCheckBox.Visible = true;
            //        SignaturePanel.ClientVisible = false;
            //    }
            //}
            
            //if (showSignPanel)
            //{
            //    if (StatusComboBox.Value != null && StatusComboBox.Value is int)
            //    {
            //        nextStatusId = (int)StatusComboBox.Value;
            //    }
            //    cpPreview.DataBind();
            //}

            if (this.RefreshOpenerAfterChangeStatus)
            {
                callBack.ClientSideEvents.CallbackComplete = "function(s,e) { RefreshOpener(); }";
                //dsc.CustomSignCallbackCompleteJS = "RefreshOpener();";
            }

            base.OnPreRender(e);
        }

        void RefreshStatusGrid()
        {
            statusGrid.DataBind();

            bool showAuthors = !(RoleEnum.Department.IsEq(UserRepository.GetCurrent().Role));
            statusGrid.Columns["author"].Visible = showAuthors;
            statusGrid.Columns["author"].ShowInCustomizationForm = showAuthors;
        }

        private string getErrorList(KeyValuePair<int, StatusRepositoryOld.StatusInfo> status)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<div style=\"color: #C53939; font-weight: bold; font-size: 1.2em;\">Внимание!</div>");

            foreach (var error in status.Value.StatusError)
            {
                sb.AppendFormat("<span class=\"statusError\">{0}</span> ", error.Text);

                if (error.TabId == 0)
                {
                    sb.Append("<br />");
                }
                else if (error.TabId < 10)
                {
                    if (!error.Activities.IsNullOrWhiteSpace())
                    {
                        sb.Append("<div class=\"statusErrorActyvity\">");
                        string[] acts = error.Activities.Split(';');
                        foreach (var act in acts)
                        {
                            var a = act.Split(',');
                            int actId = Int32.Parse(a[0]);
                            sb.Append(generateLink(error.TabId, a[1], actId));
                        }
                        sb.Append("</div>");
                    }
                }
                else
                {
                    sb.Append(generateLink(error.TabId, "Исправить"));
                    sb.Append("<br />");
                }
            }
            return sb.ToString();
        }

        string generateLink(int tabId, string activityCaption, int activityId = 0)
        {
            string linkAddress = string.Empty;

            if (tabId > -10 && tabId <= 15 && plan == null)
                return activityCaption;

            #region case
            switch (tabId)
            {
                case 1: // общие сведения
                    linkAddress = CommonData.GetUrl(activityId);
                    break;
                //case 2: // функции
                //    linkAddress = Functions.GetUrl(activityId);
                //    break;
                //case 3: // индикаторы
                //    linkAddress = IndicatorsOld.GetUrl(activityId);
                //    break;
                //case 4: // специальные сведения
                //    linkAddress = SpecialInformation.GetUrl(activityId);
                //    break;
                case 5: // расходы
                    linkAddress = Expenses.GetUrl(activityId);
                    break;
                case 6: // результаты
                    linkAddress = GoalsResults.GetUrl(activityId);
                    break;
                case 7: // работы
                    linkAddress = Works.GetUrl(activityId);
                    break;
                //case 8: // специальные сведения
                //    linkAddress = SpecialInformation.GetUrl(activityId);
                //    break;
                //case 10: // план-график
                //    linkAddress = SchedulePlan.GetUrl(plan.Id);
                //    break;
                //case 11: // управление
                //    linkAddress = ProjectManagement.GetUrl(plan.Id);
                //    break;
                case 12: // версии
                    linkAddress = History.GetUrl(plan.Id);
                    break;
                case 13: //документы
                    linkAddress = Plans.Documents.GetUrl(plan.Id);
                    break;
                case 14: // письма
                    linkAddress = Mails.GetUrl(plan.Id);
                    break;
                case 15: // список мероприятий
                    linkAddress = ActivityList.GetUrl(plan.Id);
                    break;
                
            }
            #endregion

            return String.Format("<a class=\"statusError\" href=\"{0}\">{1}</a> ", linkAddress, activityCaption);
        }



        protected void statusGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            IList<StatusHistory> history = StatusRepositoryOld.GetHistory(Owner);
            statusGrid.DataSource = EntityHelper.ConvertToAnonymous<StatusHistory>(history,

                                            delegate(StatusHistory s)
                                            {
                                                return new
                                                {
                                                    Id = s.Id,
                                                    ChangeDateTime = s.Date,
                                                    UserFullName = s.User.FullName,
                                                    BeginStatusName = s.From.Name,
                                                    EndStatusName = s.In.Name,
                                                    Comm = s.Comment
                                                };
                                            });
        }

        //сменить статус
        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            changeState();

            if (!RefreshOpenerAfterChangeStatus)
            {
                ASPxWebControl.RedirectOnCallback(Request.RawUrl);
            }
        }

        private void changeState()
        {
            int newStatusId = (int)StatusComboBox.Value;

            if (StatusRepositoryOld.Change(Owner, newStatusId, CommentMemo.Text))
            {
                ProcessingStatusChange((StatusEnum)newStatusId);

                // Надо, чтоб ЭЗ помещалось во вкладку Документы независимо от подписания ЭЦП, 
                // после того, как Эксперт изменил статус на "Получен проект ЭЗ".(DEPREGINF-1864)
            }
        }

        /// <summary>
        /// Выполнить дополнительные действия после изменения статуса.
        /// </summary>
        /// <param name="status">Новый статус</param>
        /// <param name="documentIsSaved">Был сохранён подписанный ЭЦП документ</param>
        private void ProcessingStatusChange(StatusEnum status)
        {
            ExpDemand demand = null;

            switch (status)
            {
                //case StatusEnum.MKRFApproved:

                //    demand = Owner as ExpDemand;

                //    Uviri.MKRF.Helpers.CopyHelper.CopyPlan(demand.ExpObject, demand.Owner, ExpObjectTypeEnum.Plan2012rel, status: StatusEnum.MKRFRelInWork);

                //    break;

                //case StatusEnum.Plan2012NewDemProjectReceivedEZ:
                    // Надо, чтоб ЭЗ помещалось во вкладку Документы независимо от подписания ЭЦП, 
                    // после того, как Эксперт изменил статус на "Получен проект ЭЗ". (DEPREGINF-1864)
                    //if (!documentIsSaved && Owner is ExpConclusion)
                    //{
                    //    AddExpConcVersion();

                    //    signedDocument signedDoc = getReportData();
                    //    Document doc = new Document(DocTypeEnum.ExpertConclusion, Owner as ExpConclusion);
                    //    User currentUser = UserRepository.GetCurrent();
                    //    doc.ContentType = "zip";
                    //    doc.Name = signedDoc.Name; // "Экспертное заключение не подписано ЭЦП " + currentUser.FullName + " " + DateTime.Now.ToShortDateString();
                    //    doc.FileName = signedDoc.FileName;
                    //    doc.DateCreate = DateTime.Now;
                    //    doc.Author = currentUser;
                    //    doc.File = signedDoc.GetData();
                    //    RepositoryBase<Document>.SaveOrUpdate(doc);
                    //}
                    //break;
                //case StatusEnum.PaperNotReceived:
                //    break;
                //case StatusEnum.PaperReceivedWithoutSign:
                //    break;
                //case StatusEnum.PaperReceivedWithSign:
                //    break;
                //case StatusEnum.Pl11Created:
                //    break;
                //case StatusEnum.Pl11PostedInTheMKS:
                //    break;
                //case StatusEnum.Pl11OnExpertise:
                //    demand = Owner as ExpDemand;
                //    // При смене статуса заявки в "На экспертизе", создаем(по необходимости) мероприятия по заключению
                //    // и направляем экспертам, которым еще не направляли
                //    StatusRepository.StartExpertise(Owner as ExpDemandBase);
                //    break;
                //case StatusEnum.Pl11PostedAtMeeting:
                //    break;
                //case StatusEnum.Pl11MaterialsRequested:
                //    break;
                //case StatusEnum.Pl11PositiveConclusion:
                //    break;
                //case StatusEnum.Pl11Rejected:
                //    break;
                //case StatusEnum.Pl11Correction:
                //    break;
                //case StatusEnum.Pl11Registered:
                //    break;
                //case StatusEnum.Pl11CompletenessChecking:
                //    break;
                //case StatusEnum.Pl11CompletenessChecked:
                //    break;
                //case StatusEnum.DemExpertAssigned:
                //    break;
                //case StatusEnum.DemSendedToExpert:
                //    break;
                //case StatusEnum.DemDocsRequested:
                //    break;
                //case StatusEnum.DemRequestAccepted:
                //    break;
                //case StatusEnum.DemFinished:
                //    break;
                //case StatusEnum.PreCreated:
                //    break;
                //case StatusEnum.PrePostedInTheMKS:
                //    break;
                //case StatusEnum.PreRegistered:
                //    break;
                //case StatusEnum.PreOnExpertise:
                //    break;
                //case StatusEnum.PreSentRevision:
                //    break;
                //case StatusEnum.PreAgreed:
                //    break;
                //case StatusEnum.PreDirectGovCommission:
                //    break;
                //case StatusEnum.PreNotApproved:
                //    break;
                //case StatusEnum.PreApproved:
                //    break;
                //case StatusEnum.PreCompletenessChecking:
                //    break;
                //case StatusEnum.PreRejected:
                //    break;
                //case StatusEnum.PreCompletenessChecked:
                //    break;
                //case StatusEnum.OtherCreated:
                //    break;
                //case StatusEnum.OtherPostedInTheMKS:
                //    break;
                //case StatusEnum.OtherRegistered:
                //    break;
                //case StatusEnum.OtherOnExpertise:
                //    break;
                //case StatusEnum.OtherReply:
                //    break;
                //case StatusEnum.OtherAgreed:
                //    break;
                //case StatusEnum.OtherAgreedWithWomments:
                //    break;
                //case StatusEnum.FgisRegistered:
                //    break;
                //case StatusEnum.FgisCreatedNotRegistered:
                //    break;
                //case StatusEnum.FgisNotCreated:
                //    break;
                //case StatusEnum.WorkBegined:
                //    break;
                //case StatusEnum.WorkRealised:
                //    break;
                //case StatusEnum.PlanReportNeed:
                //    break;
                //case StatusEnum.PlanReportDiscuss:
                //    break;
                //case StatusEnum.PlanReportChalenge:
                //    break;
                //case StatusEnum.PlanReportOther:
                //    break;
                //case StatusEnum.DepPlanCreated:
                //    break;

                case StatusEnum.DepPlanPostDICT:
                case StatusEnum.Plan2012NewDepPostedInDICT:
                case StatusEnum.Plan2012NewDepPostedInDICT_Stage2:
                case StatusEnum.Plan2012NewDepPostedInDICT_Stage3:

                    IList<PaperObject> list = RepositoryBase<PaperObject>.GetAll(x => x.Owner == Owner);

                    //заполняем входящие в мкс (номер, дату)
                    foreach (PaperObject item in list)
                    {
                        bool change = false;

                        if (string.IsNullOrEmpty(item.NumberInboxMKS))
                        {
                            item.NumberInboxMKS = item.NumberOutgoing;
                            change = true;
                        }

                        if (!item.DateInboxMKS.HasValue)
                        {
                            item.DateInboxMKS = item.DateOutgoing;
                            change = true;
                        }

                        if (change)
                            RepositoryBase<PaperObject>.SaveOrUpdate(item);
                    }

                    break;
                //case StatusEnum.DepPlanCompletenessChecking:
                //    break;
                //case StatusEnum.DepPlanRejected:
                //    break;
                //case StatusEnum.DepPlanCompletenessChecked:
                //    break;
                //case StatusEnum.DepPlanOnExpertise:
                //    break;
                //case StatusEnum.DepPlanMaterialsRequested:
                //    break;
                //case StatusEnum.DepPlanSentRevision:
                //    break;
                //case StatusEnum.DepPlanPositiveConclusion:
                //    break;
                //case StatusEnum.DepPlanIncludedPlanMinistry:

                //    User user = UserRepository.GetCurrent();

                //    if (user.Department != null && user.Department.Id == DepartmentRegionFilterControl.MSK_ID)
                //    {
                //        IList<Plan> listP = RepositoryBase<Plan>.GetAll(x => x.Department == user.Department);
                //    }

                //    break;
                case StatusEnum.DepPlanCorrection:

                    demand = Owner as ExpDemand;

                    if (demand != null)
                    {
                        //demand.ExpObject.Department.Parent

                        IList<Plan> plans = PlanRepository.GetAll(x => x.Department == demand.ExpObject.Department.Parent && x.PlanType == ExpObjectTypeEnum.SummaryPlanMKS
                            && x.Year == demand.ExpObject.Year);

                        if (plans.Count == 1)
                        {
                            StatusRepositoryOld.Change(plans[0].Demand, StatusEnum.SPlanProject, "Статус переведён автоматически");
                        }

                    }

                    break;

                //case StatusEnum.SPlanProject:
                //    break;
                case StatusEnum.SPlanApproved:
                    DataTable table = PlanRepository.GetDepExpDemandList(Owner.Id);

                    foreach (DataRow item in table.Rows)
                    {
                        demand = RepositoryBase<ExpDemand>.Get(item["Id"]);

                        if (demand != null && demand.Status.Id != (int)StatusEnum.DepPlanIncludedPlanMinistry)
                        {
                            StatusRepositoryOld.Change(demand, StatusEnum.DepPlanIncludedPlanMinistry, "Статус переведён автоматически");
                        }
                    }

                    break;
                //case StatusEnum.AddDocsRequested:

                //    demand = Owner as ExpDemand;
                //    DataTable table1 = ConclusionRepository.GetList(demand);

                //    foreach (DataRow item in table1.Rows)
                //    {
                //        ExpConclusion expConclusion = RepositoryBase<ExpConclusion>.Get(item["Id"]);

                //        if (expConclusion != null && expConclusion.Status.Id == (int)StatusEnum.NeedConfirmed)
                //        {
                //            expConclusion.Status = RepositoryBase<Status>.Get((int)StatusEnum.SendToExpert);
                //            RepositoryBase<ExpConclusion>.SaveOrUpdate(expConclusion);
                //        }
                //    }

                //    break;

                //case StatusEnum.Plan2012NewPostedInTheMKS:
                //case StatusEnum.Plan2012NewPostedInTheMKS_2:
                //case StatusEnum.Plan2012NewPostedInTheMKS_3:
                //    ChangeSendedExpert(Owner);
                //    break;

                case StatusEnum.Plan2012NewSentResponse:
                case StatusEnum.Plan2012NewSentResponse_2:
                case StatusEnum.Plan2012NewSentResponse_3:
                case StatusEnum.Plan2012NewDiclaimResponse:
                case StatusEnum.Plan2012NewDiclaimResponse_2:
                case StatusEnum.Plan2012NewDiclaimResponse_3:
                    //AddExpConcVersion();
                    break;

                //default:
                //    break;
                case StatusEnum.Plan2012NewDepPlanCorrection:

                    demand = Owner as ExpDemand;

                    if (demand != null)
                    {
                        //demand.ExpObject.Department.Parent

                        IList<Plan> plans = PlanRepository.GetAll(x => x.Department == demand.ExpObject.Department.Parent && x.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new
                            && x.Year == demand.ExpObject.Year);

                        if (plans.Count == 1)
                        {
                            StatusRepositoryOld.Change(plans[0].Demand, StatusEnum.Plan2012SPlanProject, "Статус переведён автоматически");
                        }

                    }

                    break;
            }
        }

        /// <summary>
        /// Смена статуса экспертизы на "Эксперт назначен"
        /// </summary>
        /// <param name="Owner"></param>
        //private void ChangeSendedExpert(EntityBase Owner)
        //{
        //    var demand = Owner as ExpDemand;

        //    var statusSendedToExpert = RepositoryBase<Status>.Get((int)StatusEnum.Plan2012NewDemToExpert);
        //    ExpConclusion conc = demand.SummaryConclusion;
        //    if (conc != null)
        //    {
        //        conc.Status = statusSendedToExpert;
        //        RepositoryBase<ExpConclusion>.SaveOrUpdate(conc);
        //    }

        //    DataTable table1 = ConclusionRepository.GetConclusionListForResetSimple(demand);

        //    foreach (DataRow item in table1.Rows)
        //    {
        //        ExpConclusion expConclusion = RepositoryBase<ExpConclusion>.Get(item["Id"]);

        //        if (expConclusion != null)
        //        {
        //            expConclusion.Status = statusSendedToExpert;
        //            RepositoryBase<ExpConclusion>.SaveOrUpdate(expConclusion);
        //        }
        //    }
        //}

        int nextStatusId = 0;
        //protected void cpPreview_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        //{
        //    int temp = 0;
        //    if (int.TryParse(e.Parameter, out temp))
        //    {
        //        nextStatusId = temp;
        //    }
        //    cpPreview.DataBind();
        //}

        //void AddExpConcVersion()
        //{
        //    if (Owner is ExpConclusion)
        //    {
        //        HistoryExpert.AddVersion(Owner as ExpConclusion, UserRepository.CurrentUserId); // сохраняем версию заключения
        //        return;
        //    }
        //    else if (Owner is ExpDemand)
        //    {
        //        var demand = Owner as ExpDemand;
        //        DataTable table1 = ConclusionRepository.GetConclusionListForReset(demand);

        //        foreach (DataRow item in table1.Rows)
        //        {
        //            ExpConclusion expConclusion = RepositoryBase<ExpConclusion>.Get(item["Id"]);

        //            if (expConclusion != null)
        //            {
        //                HistoryExpert.AddVersion(expConclusion, UserRepository.CurrentUserId); // сохраняем версию заключения
        //            }
        //        }
        //    }
        //    else throw new Exception("Owner class not ExpConclusion or ExpDemand");
        //}

    }
}