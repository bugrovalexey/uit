﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.OtherExpObjects;

namespace Uviri.MKRF.Mapping.Expertise
{
	public class ExpObjectValuationReasonMap : ClassMapBase<ExpObjectValuationReason>
	{
        public ExpObjectValuationReasonMap()
            : base("Exp_Valuation_Reason_Other")
		{
		}

        protected override void InitMap()
        {
            Map(x => x.Comm, "Exp_Valuation_Reason_Other_Comm");

            References<ExpObjectCriterionEquivalent>(x => x.ExpCriterionEquivalent, "Exp_Criterion_Equivalent_Other_ID");
            References<ExpReason>(x => x.ExpReason, "Exp_Reason_ID");
            References<ExpConformity>(x => x.ExpConformity, "Exp_Conformity_ID");
        }
    }
}