﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="Roles.aspx.cs" Inherits="GB.MKRF.Web.Administrator.Roles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <dxwgv:ASPxGridView runat="server" ID="grid" ClientInstanceName="grid" AutoGenerateColumns="false"
        Width="100%" KeyFieldName="Id" OnBeforePerformDataSelect="grid_BeforePerformDataSelect">
        <Settings ShowFilterRow="True" />
        <Columns>
            <dxwgv:GridViewDataTextColumn Visible="false" FieldName="ID">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="10" Caption="Наименование" FieldName="Name">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="20" Caption="Web страница" FieldName="WebPage">
            </dxwgv:GridViewDataTextColumn>
        </Columns>
    </dxwgv:ASPxGridView>
</asp:Content>
