﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CommonInfoGetInfoStory : IStory<CommonInfoGetInfoStoryContext, ActivityCommonInfoDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public CommonInfoGetInfoStory(IRepositoryRui repository, IMapperBase mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public ActivityCommonInfoDto Execute(CommonInfoGetInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityCommonInfoDto> ExecuteAsync(CommonInfoGetInfoStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var activity = _repository.Get<PlansActivity>(context.ActivityId);

            return _mapper.Map(activity, new ActivityCommonInfoDto());
        }
    }
}
