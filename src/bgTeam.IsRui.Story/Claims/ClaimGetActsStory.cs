﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimGetActsStory : IStory<ClaimGetActsStoryContext, IEnumerable<ClaimActDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapperBase;

        public ClaimGetActsStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase)
        {
            _repository = repositoryRui;
            _mapperBase = mapperBase;
        }

        public IEnumerable<ClaimActDto> Execute(ClaimGetActsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ClaimActDto>> ExecuteAsync(ClaimGetActsStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка: {context.ClaimId}");
            }

            var indicators = _repository
                .GetAllEx<ClaimAct>(q => q.Where(x => x.Claim.Id == context.ClaimId));

            foreach (ClaimAct d in indicators)
            {
                var doc = _repository.Get<Files>(q => q.Where(x => x.Type == DocTypeEnum.ClaimAct && x.EntityOwner == d));

                d.File = doc;
            }

            return indicators.Select(x => _mapperBase.Map(x, new ClaimActDto()));
        }
    }
}
