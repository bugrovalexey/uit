﻿namespace bgTeam.IsRui.Common.Services.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Xsl;
    using GemBox.Spreadsheet;
    using Microsoft.Office.Interop.Excel;


    public class ConvertHtmlToXls : IConvertHtmlToXls
    {
        public void ConvertHtmlToXlsFile(string htmlText)
        {

            var htmlFilePathAndName = Path.Combine(Environment.CurrentDirectory, @"wwwroot\Temp\htmlReport.html");

            using (FileStream fs = File.Create(htmlFilePathAndName))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(htmlText);
                fs.Write(info, 0, info.Length);
            }

            var newXlsxFilePathAndName = Path.Combine(Environment.CurrentDirectory, @"wwwroot\Temp\ReportExcel.xlsx");

            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

            ExcelFile ef = ExcelFile.Load(htmlFilePathAndName, LoadOptions.HtmlDefault);

            ef.Save(newXlsxFilePathAndName, SaveOptions.XlsxDefault);

            File.Delete(htmlFilePathAndName);
        }
    }
}