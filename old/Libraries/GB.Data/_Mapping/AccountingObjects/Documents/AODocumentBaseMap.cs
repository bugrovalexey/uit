﻿using GB.Data.AccountingObjects.Documents;
using GB.Data.Documents;

namespace GB._Mapping.AccountingObjects.Documents
{
    class AODocumentBaseMap<T> : ClassMapHiLo<T> where T : AODocumentBase
    {
        protected AODocumentBaseMap()
            : base("OU_Document")
        {
        }

    
        protected override void InitMap()
        {
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();

            Map(x => x.Name, "OU_Document_Name");
            Map(x => x.Number, "OU_Document_Number");
            Map(x => x.DateAdoption, "OU_Document_Date");
            Map(x => x.Paragraph, "OU_Document_Paragraph");
            Map(x => x.Type, "OU_Document_Type").CustomType<DocTypeEnum>().Not.Nullable();
        
            AdditionalMap();
        }

        protected virtual void AdditionalMap()
        {
        
        }

    }
}
