﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ExpObjectTypeMap : ClassMapBase<ExpObjectType>
    {
		public ExpObjectTypeMap()
            : base("Exp_Obj_Type")
        {
//            Table("w_");

//            Id(x => x.Id, "Exp_Obj_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_Exp_Obj_Type_Insert
//                         @Exp_Obj_Type_Name = ?
//                        ,@Exp_Obj_Type_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Exp_Obj_Type_Update
//                         @Exp_Obj_Type_Name = ?
//                        ,@Exp_Obj_Type_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Obj_Type_Delete
//						  @Exp_Obj_Type_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Exp_Obj_Type_Name").Not.Nullable();
        }
    }
}