﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoWorkAndServiceGetAllWorkStory : IStory<AoWorkAndServiceGetAllWorkStoryContext, IEnumerable<WorkAndServiceGetAllWorkDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;

        public AoWorkAndServiceGetAllWorkStory(IMapperBase mapper, IRepositoryRui repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public IEnumerable<WorkAndServiceGetAllWorkDto> Execute(AoWorkAndServiceGetAllWorkStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<WorkAndServiceGetAllWorkDto>> ExecuteAsync(AoWorkAndServiceGetAllWorkStoryContext context)
        {

            var workAndService = _repository.GetAllEx<WorkAndService>(
                q => q.Where(x => x.AccountingObject.Id == context.AoId))
                .Select(x => _mapper.Map(x, new WorkAndServiceGetAllWorkDto()));

            return workAndService;
        }
    }
}
