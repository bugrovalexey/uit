﻿using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Admin
{
    public class YearRoleArcMap : ClassMapBase<YearRoleArc>
	{
        public YearRoleArcMap()
            : base("Year_Role_Arc")
		{
//            Table("w_");

//            Id(x => x.Id, "Year_Role_Arc_ID").GeneratedBy.Assigned();

            


//            SqlInsert(@"exec ZZZ_Prc_Year_Role_Arc_Insert
//                              @Year = ?
//                             ,@Role_ID = ?
//                             ,@Portal_Table_ID = ?
//                             ,@Year_Role_Arc_ID = ?").Check.None();

//            SqlUpdate(@"raiserror ('Unsupported',10,1)").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Year_Role_Arc_Delete
//						  @Year_Role_Arc_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Year, "Year");

            References<Role>(x => x.Role, "Role_ID");
            References<PortalTable>(x => x.PortalTable, "Portal_Table_ID");
        }
    }
}