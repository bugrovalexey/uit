﻿using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Entities.Directories
{
    /// <summary>Ответственный от мкс по ведомству</summary>
    public class MksResponsible : EntityBase
    {
        /// <summary>Ответственный</summary>
        public virtual User User { get; set; }

        /// <summary>Ведомство</summary>
        public virtual Department Department { get; set; }
    }
}