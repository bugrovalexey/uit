﻿using System.ComponentModel;
namespace GB.Data.Plans
{
    public enum ActivityTypeEnum
    {
        [Description("Не задано")]
        None = 0,

        [Description("Создание")]
        Create = 1,

        [Description("Развитие")]
        Develop = 2,

        [Description("Эксплуатация")]
        Exploitation = 4
    }

}