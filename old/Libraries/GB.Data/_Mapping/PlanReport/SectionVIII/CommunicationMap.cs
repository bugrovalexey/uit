﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
    public class CommunicationMap : ClassMapBase<Communication>
    {
        public CommunicationMap()
            : base("PRS_8_4")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_4_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_8_4_CA_1 = ? 
//                          ,@PRS_8_4_TP_1 = ? 
//                          ,@PRS_8_4_CA_2 = ? 
//                          ,@PRS_8_4_TP_2 = ? 
//                          ,@PRS_8_4_CA_3 = ? 
//                          ,@PRS_8_4_TP_3 = ? 
//                          ,@PRS_8_4_CA_4 = ? 
//                          ,@PRS_8_4_TP_4 = ? 
//                          ,@PRS_8_4_CA_5 = ? 
//                          ,@PRS_8_4_TP_5 = ? 
//                          ,@PRS_8_4_CA_6 = ? 
//                          ,@PRS_8_4_TP_6 = ? 
//                          ,@PRS_8_4_CA_7 = ? 
//                          ,@PRS_8_4_TP_7 = ? 
//                          ,@PRS_8_4_CA_8 = ? 
//                          ,@PRS_8_4_TP_8 = ? 
//                          ,@PRS_8_4_CA_9 = ? 
//                          ,@PRS_8_4_TP_9 = ? 
//                          ,@Plans_ID = ? 
//						  ,@PRS_8_4_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_4_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_4_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_4_Delete @PRS_8_4_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.AnalogPhoneCount_CA, "PRS_8_4_CA_1");
			Map(x => x.AnalogPhoneCount_TP, "PRS_8_4_TP_1");
			Map(x => x.IPGadgetCount_CA, "PRS_8_4_CA_2");
			Map(x => x.IPGadgetCount_TP, "PRS_8_4_TP_2");
			Map(x => x.MobileCount_CA, "PRS_8_4_CA_3");
			Map(x => x.MobileCount_TP, "PRS_8_4_TP_3");
			Map(x => x.FaxCount_CA, "PRS_8_4_CA_4");
			Map(x => x.FaxCount_TP, "PRS_8_4_TP_4");
			Map(x => x.IsPhoneIntegrated_CA, "PRS_8_4_CA_5");
			Map(x => x.IsPhoneIntegrated_TP, "PRS_8_4_TP_5");
			Map(x => x.InternalNumVol_CA, "PRS_8_4_CA_6");
			Map(x => x.InternalNumVol_TP, "PRS_8_4_TP_6");
			Map(x => x.SharedNumVol_CA, "PRS_8_4_CA_7");
			Map(x => x.SharedNumVol_TP, "PRS_8_4_TP_7");
			Map(x => x.EmptyInternalNumVol_CA, "PRS_8_4_CA_8");
			Map(x => x.EmptyInternalNumVol_TP, "PRS_8_4_TP_8");
			Map(x => x.EmptySharedNumVol_CA, "PRS_8_4_CA_9");
			Map(x => x.EmptySharedNumVol_TP, "PRS_8_4_TP_9");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
        }
    }
}