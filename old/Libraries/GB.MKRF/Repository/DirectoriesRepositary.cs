﻿using GB.Data;
using System;
using System.Collections.Generic;

namespace GB.MKRF.Repository
{
    public class DirectoriesRepositary
    {
        public static IList<T> GetDirectories<T>(int year) where T : EntityBase, IPeriodDirectories
        {
            var y = DateTime.Now.Year;
            // для планов этого года берём текущюю дату
            DateTime date = DateTime.Now;

            //для планов в прошлом за дату берём 31 декабря года плана
            if (y > year)
                date = new DateTime(year, 12, 31);

            // для планов в бущуем берём 1 января
            if (y < year)
                date = new DateTime(year, 1, 1);

            return new Repository<T>().GetAll(x => x.DateStart <= date && date <= x.DateEnd);
        }
    }
}