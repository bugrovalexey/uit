﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityDeleteActStory : IStory<ActivityDeleteActStoryContext, bool>
    {
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;

        public ActivityDeleteActStory(
            IRepositoryRui repositoryRui,
            ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _repository = repositoryRui;
            _factory = sessionNHibernateFactory;
        }

        public bool Execute(ActivityDeleteActStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(ActivityDeleteActStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            var act = _repository.Get<PlansActivityAct>(context.Id);

            if (act == null)
            {
                throw new IsRuiException("Не найден акт");
            }

            bool result;

            using (var session = _factory.OpenSession())
            {
                if (act.FileId.HasValue)
                {
                    await session.DeleteAsync<Files>(act.FileId.Value);
                }

                await session.DeleteAsync<ActivityDocuments>(context.Id.Value);

                result = true;
            }

            return result;
        }
    }
}
