﻿using GB.Data.Common;
using GB.Data.Workflow;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    public class AccessToEntityService : ServiceBase<AccessToEntity>, IService<AccessToEntity, AccessToEntityArgs>
    {
        public AccessToEntity Create(AccessToEntityArgs args)
        {
            var entity = new AccessToEntity { EntityType = args.EntityType };
            return SaveOrUpdate(entity, args);
        }

        public AccessToEntity Update(AccessToEntityArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private AccessToEntity SaveOrUpdate(AccessToEntity entity, AccessToEntityArgs args)
        {
            entity.AccessMask = args.AccessMask;
            entity.Note = args.Note;
            entity.Role = GetEntity<Role>(args.RoleId);
            entity.Status = GetEntity<Status>(args.StatusId);

            base.SaveOrUpdate(entity);
            return entity;
        }

        public void Delete(AccessToEntityArgs args)
        {
            base.Delete(args.Id);
        }
    }
}