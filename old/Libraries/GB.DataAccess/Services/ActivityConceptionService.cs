﻿using GB.MKRF.Controllers;
using GB.MKRF.Entities.Plans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Services
{
    public class ActivityConceptionService : ControllerBase<ActivityConception>, IService<ActivityConception, IActivityConceptionArgs>
    {
        //public ActivityConception Create(PlansActivityServiceArgs args)
        //{
        //    var pas = new PlansActivityService(args.Activity);

        //    return SaveOrUpdate(pas, args);
        //}

        //public ActivityConception Update(PlansActivityServiceArgs args)
        //{
        //    return SaveOrUpdate(Repository.Get(args.Id), args);
        //}

        //public void Delete(PlansActivityServiceArgs args)
        //{
        //    Repository.Delete((int)args.Id);
        //}

        //private ActivityConception SaveOrUpdate(PlansActivityService pas, PlansActivityServiceArgs args)
        //{
        //    pas.Name = args.Name as string;
        //    pas.Year = (YearEnum)Convert.ToInt32(args.Year);
        //    pas.Count = Convert.ToInt32(args.Count);
        //    pas.Cost = ToDoubleNull(args.Cost);
        //    pas.KOSGU = GetEntity<ExpenditureItem>(args.KOSGU);

        //    Repository.SaveOrUpdate(pas);

        //    return pas;
        //}
        public ActivityConception Create(IActivityConceptionArgs args)
        {
            throw new NotImplementedException();
        }

        public ActivityConception Update(IActivityConceptionArgs args)
        {
            throw new NotImplementedException();
        }

        public void Delete(IActivityConceptionArgs args)
        {
            throw new NotImplementedException();
        }
    }

    public interface IActivityConceptionArgs : IServiceArgs
    {
        int Order { get; set; }
        DateTime? OrderDate { get; set; }
        string Documents { get; set; }
        int Name { get; set; }
        int Goal { get; set; }
        int Task { get; set; }

        PlansActivity Activity { get; set; }
    }
}
