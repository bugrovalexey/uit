﻿using bgTeam.IsRui.Story.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.bgTeam.IsRui.Unit.Common;
using Xunit;

namespace Test.bgTeam.IsRui.Unit.Tests
{
    public class TestAdminStory
    {
        private Factory _fac;

        public TestAdminStory()
        {
            _fac = new Factory();
        }

        [Fact]
        public void Story_UserGetAllUsers()
        {
            var story = new UserGetAllUsersStory( _fac.repositoryRui, _fac.storyMapper);

            var res = story.Execute(new UserGetAllUsersStoryContext());

            Assert.NotEmpty(res);
        }

        [Fact]
        public void Story_UserGetUserInfo()
        {
            var story = new UserGetUserInfoStory(_fac.repositoryRui, _fac.storyMapper);

            var res = story.Execute(new UserGetUserInfoStoryContext() { Id = 4});

            Assert.NotNull(res.Id);
        }

        [Fact]
        public void Story_UserAddOrUpdateUser()
        {
            var story = new UserAddOrUpdateUserStory( _fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var res = story.Execute(new UserAddOrUpdateUserStoryContext()
            {
                Login = "UnitTest",
                Password = "1234",
                IsActive = true,
                Middlename = "MidName",
                GovtOrganId = 1
            });

            Assert.NotNull(res.Id);

            var resUpdate = story.Execute(new UserAddOrUpdateUserStoryContext()
            {
                Id = res.Id,
                Login = "UnitTest1",
                Password = "1234",
                IsActive = true,
                Middlename = "MidName",
                GovtOrganId = 1
            });

            Assert.NotNull(res.Id);
        }


        [Fact]
        public void Story_UserDeleteUserStory()
        {
            var story = new UserAddOrUpdateUserStory(_fac.storyMapper, _fac.sessionFactory, _fac.repositoryRui);

            var res = story.Execute(new UserAddOrUpdateUserStoryContext()
            {
                Login = "UnitTest",
                Password = "1234",
                IsActive = true,
                Middlename = "MidName",
                GovtOrganId = 1
            });

            Assert.NotNull(res.Id);

            var storyDel = new UserDeleteUserStory( _fac.sessionFactory, _fac.repositoryRui);

            var resDel = storyDel.Execute( new UserDeleteUserStoryContext() { Id = res.Id });

            Assert.True(resDel);

        }

        [Fact]
        public void Story_UserChangePassword()
        {
            var story = new UserChangePasswordStory(_fac.sessionFactory ,_fac.repositoryRui);

            var res = story.Execute(new UserChangePasswordStoryContext() { Id = 26, NewPassword = "1234" });

            Assert.True(res);
        }
    }
}
