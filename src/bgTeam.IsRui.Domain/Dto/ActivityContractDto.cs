﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityContractDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public DateTime? BeginDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? ChangeDate { get; set; }

        public ActivityDto Activity { get; set; }
    }
}
