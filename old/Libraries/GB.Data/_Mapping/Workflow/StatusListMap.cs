﻿using GB.Data.Common;
using GB.Data.Workflow;

namespace GB._Mapping.Workflow
{
    internal class StatusListMap : ClassMapHiLo<StatusList>
    {
        public StatusListMap()
            : base("Status_Change_List") { }

        protected override void InitMap()
        {
            References<Role>(x => x.Role, "Role_ID").Not.Nullable();
            References<Status>(x => x.From, "Status_From_ID").Not.Nullable();
            References<Status>(x => x.To, "Status_To_ID").Not.Nullable();
        }
    }
}