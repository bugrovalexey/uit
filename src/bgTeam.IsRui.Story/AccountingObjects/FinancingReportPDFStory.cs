﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess.Repository;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class FinancingReportPDFStory : IStory<FinancingReportPDFStoryContext, bool>
    {
        private readonly IConvertHtmlToPdf _converter;
        private readonly IRepositoryRui _repository; 

        public FinancingReportPDFStory(IConvertHtmlToPdf converter, IRepositoryRui repository)
        {
            _converter = converter;
            _repository = repository;
        }

        public bool Execute(FinancingReportPDFStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(FinancingReportPDFStoryContext context)
        {
            var storyReportHtml = new FinancingReportStory(_repository);

            var reportHtml = await storyReportHtml.ExecuteAsync(new FinancingReportStoryContext());

            _converter.ConvertHtmlToPdfFile(reportHtml);

            return true;
        }
    }
}
