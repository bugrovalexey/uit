﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class WorkflowUserConclusionDto
    {
        public int Id { get; set; }

        public int WorkflowId { get; set; }

        public int UserId { get; set; }

        public int StatusId { get; set; }

        public string Text { get; set; }

        public IList<FilesDto> Documents { get; set; }
    }
}
