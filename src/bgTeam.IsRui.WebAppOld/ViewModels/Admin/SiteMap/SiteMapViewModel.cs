﻿using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.DataAccess.Repository;
using SiteMapNode = bgTeam.IsRui.Domain.Entities.Admin.SiteMapNode;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin.SiteMap
{
    public class SiteMapViewModel : ViewModelBase
    {
        public SiteMapViewModel()
        {
            Items = new RepositoryDc<SiteMapNode>().GetAllEx(q => q
                .OrderBy(x => x.Controller).Asc
                .ThenBy(x => x.Action).Asc.Clone())
                .Select(x => new TableSiteMap(x));

            SiteMapToRole = new SiteMapToRoleViewModel();
        }

        public IEnumerable<TableSiteMap> Items { get; set; }

        public SiteMapToRoleViewModel SiteMapToRole { get; set; }
    }
}