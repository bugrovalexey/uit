﻿using GB.Data;
using GB.Data.Common;
using GB.Data.Workflow;

namespace GB._Mapping.Workflow
{
    internal class StatusHistoryMap : ClassMapHiLo<StatusHistory>
    {
        public StatusHistoryMap()
            : base("Status_Change_History") { }

        protected override void InitMap()
        {
            Map(x => x.Date, "Status_Change_History_Date");
            Map(x => x.Comment, "Status_Change_History_Comment");

            Map(x => x.EntityId, "Status_Change_History_Object_ID").Not.Nullable();
            Map(x => x.EntityType, "Status_Change_History_Object").CustomType(typeof(EntityType)).Not.Nullable();

            References<User>(x => x.User, "Users_ID").Not.Nullable();
            References<Status>(x => x.From, "Status_Change_History_From_ID").Not.Nullable();
            References<Status>(x => x.In, "Status_Change_History_To_ID").Not.Nullable();
        }
    }
}