﻿using GB.MKRF.Entities.Directories;
using System;

namespace GB.MKRF.Entities.Plans
{
    /// <summary>НПА по мероприятию</summary>
    public class NPA : EntityBase
    {
        /// <summary>Тип НПА (Вид документа)</summary>
        public virtual NPAType NPAType { get; set; }

        /// <summary>Тип Документа</summary>
        public virtual NPAKind NPAKind { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Номер НПА</summary>
        public virtual string Num { get; set; }

        /// <summary>Дата</summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>Пункт, статья документа</summary>
        public virtual string Item { get; set; }

        /// <summary>Информация о НПА</summary>
        //public virtual string Info { get; set; }
    }
}