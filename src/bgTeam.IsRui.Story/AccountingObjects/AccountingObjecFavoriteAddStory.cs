﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System.Threading.Tasks;

    public class AccountingObjecFavoriteAddStory : IStory<AccountingObjecFavoriteAddStoryContext, bool>
    {
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;
        private readonly ISessionNHibernateFactory _factory;

        public AccountingObjecFavoriteAddStory(
            IRepositoryRui repository,
            IUserRepository userRepository,
            ISessionNHibernateFactory factory)
        {
            _repository = repository;
            _userRepository = userRepository;
            _factory = factory;
        }

        public bool Execute(AccountingObjecFavoriteAddStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<bool> ExecuteAsync(AccountingObjecFavoriteAddStoryContext context)
        {
            var currentUser = _userRepository.GetCurrent();

            var entity = _repository.Get<AOFavorite>(q =>
            {
                q.Where(x => x.AccountingObject.Id == context.Id)
                .Where(x => x.User.Id == currentUser.Id);
            });

            using (var session = _factory.OpenSession())
            {
                if (entity == null)
                {
                    var accountingObject = _repository.GetExisting<AccountingObject>(context.Id);
                    entity = new AOFavorite
                    {
                        AccountingObject = accountingObject,
                        User = currentUser
                    };
                    await session.InsertAsync(entity);
                    return true;
                }
                else
                {
                    await session.DeleteAsync<AOFavorite>(entity.Id);
                    return false;
                }
            }
        }
    }
}
