﻿namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class GoodsCharacteristicMap : ClassMapHiLo<GoodsCharacteristic>
    {
        public GoodsCharacteristicMap()
               : base("Plans_Activity_GoodsCharacteristic")
        {
        }


        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_GoodsCharacteristic_Name");
            Map(x => x.Value, "Plans_Activity_GoodsCharacteristic_Value");

            References(x => x.Unit, "Plans_Activity_GoodsCharacteristic_UnitId").Not.Nullable();
            References(x => x.Goods, "Plans_Activity_Goods_ID");
        }
    }
}
