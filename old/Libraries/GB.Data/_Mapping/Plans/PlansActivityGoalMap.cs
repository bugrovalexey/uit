﻿using GB.Data.Plans;

namespace GB._Mapping.Plans
{
    internal class PlansActivityGoalMap : ClassMapHiLo<PlansActivityGoal>
    {
        public PlansActivityGoalMap()
            : base("Plans_Activity_Goal")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Goal_Name");
            References<PlansActivity>(x => x.PlansActivity, "Plans_Activity_ID");
        }
    }
}