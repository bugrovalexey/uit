﻿namespace bgTeam.IsRui.Common.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IConvertHtmlToPdf
    {
        void ConvertHtmlToPdfFile(string path);
    }
}
