﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command
{
    public class BaseAddOrSaveContext : ICommandContext
    {
        public int? xId { get; set; }
    }
}
