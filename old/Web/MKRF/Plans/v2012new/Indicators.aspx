﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Plans/v2012new/ActivityCard.master" CodeBehind="Indicators.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.Indicators" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <style>
    </style>

    <script type="text/javascript">
        function SavePokazatel(e) {
            hfGR.Set('Name', ddeName_0_0.GetText());
            hfGR.Set('UOM', DescTextBox_0_0.GetText());
            hfGR.Set('BaseValue', BaseValueTextBox.GetText());
            hfGR.Set('GIActivityFunc', ActivityFuncComboBox_0_0.GetValue());
            hfGR.Set('GIGoal', GoalComboBox_0_0.GetValue());
            hfGR.Set('FirstPlanValue', FirstPlanValueTextBox.GetText());
            hfGR.Set('FirstPlanAddValue', FirstPlanAddValueTextBox.GetText());
            hfGR.Set('SecondPlanValue', SecondPlanValueTextBox.GetText());
            hfGR.Set('SecondPlanAddValue', SecondPlanAddValueTextBox.GetText());
            hfGR.Set('ThirdPlanValue', ThirdPlanValueTextBox.GetText());
            hfGR.Set('ThirdPlanAddValue', ThirdPlanAddValueTextBox.GetText());
        }

        function SaveIndicator(e) {
            hfGR.Set('Name', ddeName_1_0.GetText());
            hfGR.Set('Algorythm', AlgorythmTextBox.GetText());
            hfGR.Set('GIActivityFunc', ActivityFuncComboBox_1_0.GetValue());
            hfGR.Set('FirstPlanValue', FirstPlanValueTextBox_1_0.GetText());
            hfGR.Set('FirstPlanAddValue', FirstPlanAddValueTextBox_1_0.GetText());
            hfGR.Set('SecondPlanValue', SecondPlanValueTextBox_1_0.GetText());
            hfGR.Set('SecondPlanAddValue', SecondPlanAddValueTextBox_1_0.GetText());
            hfGR.Set('ThirdPlanValue', ThirdPlanValueTextBox_1_0.GetText());
            hfGR.Set('ThirdPlanAddValue', ThirdPlanAddValueTextBox_1_0.GetText());
        }

        function validateTextBox(s, e) {
            if (s.GetVisible()) {
                e.isValid = s.GetText() != "";
            }
            else {
                e.isValid = true;
            }
        }

        function validateComboBox(s, e) {
            if (s.GetVisible()) {
                var validValue = e.validValue === undefined ? null : e.validValue;
                e.isValid = s.GetValue() != validValue;
            }
            else
                e.isValid = true;
        }

        //функции, управляющие доступом к полям формы
        //при условии выбранного значения в соответствующем комбобоксе
        function EnabledForm_0_0(actFuncCombo) {
            EnabledForm(ActivityFuncComboBox_0_0, [ddeName_0_0, DescTextBox_0_0, ValueTextBox_0_0, GoalComboBox_0_0]);
        }

        function EnabledForm_1_0(actFuncCombo) {
            EnabledForm(ActivityFuncComboBox_1_0, [ddeName_1_0, AlgorythmMemo_1_0]);
        }

        function EnabledForm(targetCombo, controls) {
            $.each(controls, function (i, e) {
                e.SetEnabled(targetCombo.GetValue() != 0);
            });
        }
    </script>
    <dxhf:ASPxHiddenField runat="server" ID="hfGR" ClientInstanceName="hfGR" />
    <p class="header_grid">
        Показатели
        <span class="subtitle">
            Заполняется на первом этапе
        </span>
    </p>
    <dxe:ASPxButton ID="IndexGridAddButton" runat="server" AutoPostBack="false" Text="Добавить"
        Style="margin: 10px;">
        <ClientSideEvents Click="function(s,e) { 
            IndicatorGrid.CancelEdit();
            IndexGrid.AddNewRow();
            }" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="IndexGrid" ClientInstanceName="IndexGrid" Width="100%" KeyFieldName="Id"
        OnBeforePerformDataSelect="IndexGrid_BeforePerformDataSelect"
        OnStartRowEditing="IndexGrid_StartRowEditing"
        OnRowInserting="IndexGrid_RowInserting"
        OnRowUpdating="IndexGrid_RowUpdating"
        OnRowDeleting="IndexGrid_RowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ButtonType="Link" AllowDragDrop="False" FooterCellStyle-BackColor="#DBEBFF">
                <EditButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/edit_16.png" Image-ToolTip="Изменить" />
                <ClearFilterButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/clear_16.png" Image-ToolTip="Очистить" />
                <DeleteButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/delete_16.png" Image-ToolTip="Удалить" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" />
            <dxwgv:GridViewDataTextColumn Caption="Госуслуга/госфункция" FieldName="Func" />
            <dxwgv:GridViewDataTextColumn Caption="Цель мероприятия" FieldName="Goal" />
            <dxwgv:GridViewDataTextColumn Caption="Базовое значение" FieldName="BaseVal" CellStyle-HorizontalAlign="Center" />
            <dxwgv:GridViewBandColumn Caption="" Name="FirstPlanYear">
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="значение" FieldName="FirstPlanVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="с учетом доп. фин-ия" FieldName="FirstPlanAddVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
            <dxwgv:GridViewBandColumn Caption="" Name="SecondPlanYear">
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="значение" FieldName="SecondPlanVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="с учетом доп. фин-ия" FieldName="SecondPlanAddVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
            <dxwgv:GridViewBandColumn Caption="" Name="ThirdPlanYear">
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="значение" FieldName="ThirdPlanVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="с учетом доп. фин-ия" FieldName="ThirdPlanAddVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
        </Columns>
        <SettingsEditing Mode="EditForm" />
        <Styles>
            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
            </Header>
        </Styles>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Госуслуга/госфункция:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ActivityFuncComboBox_0_0" runat="server" ClientInstanceName="ActivityFuncComboBox_0_0"
                                OnDataBinding="ActivityFuncComboBox_DataBinding" OnDataBound="ActivityFuncComboBox_DataBound"
                                ValueField="Id" ValueType="System.Int32" TextField="Name" EncodeHtml="False"
                                ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>">
                                <ItemStyle Wrap="True" />
                                <ListBoxStyle CssClass="heigth180" />
                                <ValidationSettings ErrorText="Выберите госуслугу/госфункцию"></ValidationSettings>
                                <ClientSideEvents Validation="function(s,e){  }"
                                    SelectedIndexChanged="function(s,e){ 
                                                        var param = ' |' + s.GetValue();
                                                        lbName_0_0.PerformCallback(param);
                                                    }"></ClientSideEvents>
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Наименование:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDropDownEdit ID="ddeName_0_0" ClientInstanceName="ddeName_0_0" runat="server" NullText=""
                                ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" OnDataBinding="ddeName_DataBinding">
                                <DropDownWindowTemplate>
                                    <dxe:ASPxListBox ID="lbName_0_0" runat="server" ClientInstanceName="lbName_0_0"
                                        SelectionMode="Single" Width="100%"
                                        OnDataBinding="lbName_DataBinding" OnCallback="lbName_Callback">
                                        <ItemStyle Wrap="True" />
                                        <ClientSideEvents SelectedIndexChanged="function(s, e){
                                                            var selItem = s.GetSelectedItem();
                                                            ddeName_0_0.SetText(selItem.text);
                                                            ddeName_0_0.HideDropDown();
                                                        }"
                                            EndCallback="function(s,e){
                                                            
                                                        }"></ClientSideEvents>
                                    </dxe:ASPxListBox>
                                </DropDownWindowTemplate>
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                                <ClientSideEvents KeyUp="function(s,e){
                                                    if (e.htmlEvent.keyCode == 38 || e.htmlEvent.keyCode == 40)
                                                        lbName_0_0.Focus();
                                                    if (e.htmlEvent.keyCode == 13)
                                                        s.HideDropDown();
                                                    var param = s.GetText() + '|' + ActivityFuncComboBox_0_0.GetValue();
                                                    lbName_0_0.PerformCallback(param);
                                                    s.ShowDropDown();
                                                }"
                                    Validation="validateTextBox"></ClientSideEvents>
                            </dxe:ASPxDropDownEdit>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Цель мероприятия:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="GoalComboBox_0_0" runat="server" ClientInstanceName="GoalComboBox_0_0"
                                OnDataBinding="GoalComboBox_DataBinding" OnDataBound="GoalComboBox_DataBound"
                                ValueField="Id" ValueType="System.Int32" TextField="Name" EncodeHtml="False"
                                DropDownHeight="350px">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Единица измерения:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="Desc_0_0" runat="server" ClientInstanceName="DescTextBox_0_0" OnDataBinding="DescTextBox_DataBinding"
                                ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="validateTextBox" />
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Базовое значение:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="BaseValueTextBox" runat="server" ClientInstanceName="BaseValueTextBox" OnDataBinding="BaseValueTextBox_DataBinding"
                                ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RegularExpression ValidationExpression="([0-9]*)[.,/]?([0-9]+)" ErrorText="введите число" />
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="validateTextBox" />
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Плановое значение:
                        </td>
                        <td class="form_edit_input">
                            <table style="width: 398px">
                                <tr>
                                    <td>на <%= BaseIndicatorYear + 1 %> год</td>
                                    <td>на <%= BaseIndicatorYear + 2 %> год</td>
                                    <td>на <%= BaseIndicatorYear + 3 %> год</td>
                                </tr>
                                <tr>
                                    <td>
                                        <dxe:ASPxTextBox ID="FirstPlanValueTextBox" runat="server" ClientInstanceName="FirstPlanValueTextBox" OnDataBinding="FirstPlanValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" Width="100px">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                <RegularExpression ValidationExpression="([0-9]*)[.,/]?([0-9]+)" ErrorText="введите число" />
                                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="validateTextBox" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="SecondPlanValueTextBox" runat="server" ClientInstanceName="SecondPlanValueTextBox" OnDataBinding="SecondPlanValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" Width="100px">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                <RegularExpression ValidationExpression="([0-9]*)[.,/]?([0-9]+)" ErrorText="введите число" />
                                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="validateTextBox" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="ThirdPlanValueTextBox" runat="server" ClientInstanceName="ThirdPlanValueTextBox" OnDataBinding="ThirdPlanValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" Width="100px">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                <RegularExpression ValidationExpression="([0-9]*)[.,/]?([0-9]+)" ErrorText="введите число" />
                                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="validateTextBox" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">С учетом доп. финансирования:
                        </td>
                        <td class="form_edit_input">
                            <table style="width: 398px">
                                <tr>
                                    <td>
                                        <dxe:ASPxTextBox ID="FirstPlanAddValueTextBox" runat="server" ClientInstanceName="FirstPlanAddValueTextBox" OnDataBinding="FirstPlanAddValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" MaxLength="100" Width="100px">
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="SecondPlanAddValueTextBox" runat="server" ClientInstanceName="SecondPlanAddValueTextBox" OnDataBinding="SecondPlanAddValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" MaxLength="100" Width="100px">
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="ThirdPlanAddValueTextBox" runat="server" ClientInstanceName="ThirdPlanAddValueTextBox" OnDataBinding="ThirdPlanAddValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" MaxLength="100" Width="100px">
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                                <span class="ok"><dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" /></span>
                                <span class="cancel"><dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" /></span>
                            </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){if (e.command=='UPDATEEDIT') SavePokazatel(e); }" />
    </dxwgv:ASPxGridView>

    <p class="header_grid">
        Индикаторы
        <span class="subtitle">
            Заполняется на первом этапе
        </span>
    </p>
    <dxe:ASPxButton ID="IndicatorGridAddButton" runat="server" AutoPostBack="false" Text="Добавить"
        Style="margin: 10px;">
        <ClientSideEvents Click="function(s,e) { 
            IndexGrid.CancelEdit();
            IndicatorGrid.AddNewRow();
            }" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="IndicatorGrid" ClientInstanceName="IndicatorGrid" Width="100%" KeyFieldName="Id"
        OnBeforePerformDataSelect="IndicatorGrid_BeforePerformDataSelect"
        OnStartRowEditing="IndicatorGrid_StartRowEditing"
        OnRowInserting="IndicatorGrid_RowInserting"
        OnRowUpdating="IndicatorGrid_RowUpdating"
        OnRowDeleting="IndicatorGrid_RowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ButtonType="Link" AllowDragDrop="False" FooterCellStyle-BackColor="#DBEBFF">
                <EditButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/edit_16.png" Image-ToolTip="Изменить" />
                <ClearFilterButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/clear_16.png" Image-ToolTip="Очистить" />
                <DeleteButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/delete_16.png" Image-ToolTip="Удалить" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" />
            <dxwgv:GridViewDataTextColumn Caption="Госуслуга/госфункция" FieldName="Func" />
            <dxwgv:GridViewDataTextColumn Caption="Алгоритм расчета" FieldName="Algorythm" />
            <dxwgv:GridViewBandColumn Caption="" Name="FirstPlanYear">
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="значение" FieldName="FirstPlanVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="с учетом доп. фин-ия" FieldName="FirstPlanAddVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
            <dxwgv:GridViewBandColumn Caption="" Name="SecondPlanYear">
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="значение" FieldName="SecondPlanVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="с учетом доп. фин-ия" FieldName="SecondPlanAddVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
            <dxwgv:GridViewBandColumn Caption="" Name="ThirdPlanYear">
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="значение" FieldName="ThirdPlanVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="с учетом доп. фин-ия" FieldName="ThirdPlanAddVal"
                        Width="50px" FooterCellStyle-BackColor="#DBEBFF" CellStyle-HorizontalAlign="Center">
                        <Settings AutoFilterCondition="Equals" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
        </Columns>
        <SettingsEditing Mode="EditForm" />
        <Styles>
            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
            </Header>
        </Styles>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Госуслуга/госфункция:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ActivityFuncComboBox_1_0" runat="server" ClientInstanceName="ActivityFuncComboBox_1_0"
                                OnDataBinding="ActivityFuncForIndicatorsComboBox_DataBinding" OnDataBound="ActivityFuncComboBox_DataBound"
                                ValueField="Id" ValueType="System.Int32" TextField="Name" EncodeHtml="False"
                                ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>">
                                <ItemStyle Wrap="True" />
                                <ListBoxStyle CssClass="heigth180" />
                                <ValidationSettings ErrorText="Выберите госуслугу/госфункцию"></ValidationSettings>
                                <ClientSideEvents Validation="function(s,e){  }"
                                    SelectedIndexChanged="function(s,e){ 
                                                        var param = ' |' + s.GetValue();
                                                        lbName_1_0.PerformCallback(param);
                                                    }"></ClientSideEvents>
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Наименование:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDropDownEdit ID="ddeName_1_0" ClientInstanceName="ddeName_1_0" runat="server" NullText=""
                                ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" OnDataBinding="ddeName_DataBinding">
                                <DropDownWindowTemplate>
                                    <dxe:ASPxListBox ID="lbName_1_0" runat="server" ClientInstanceName="lbName_1_0"
                                        SelectionMode="Single" Width="100%"
                                        OnDataBinding="lbNameForIndicators_DataBinding" OnCallback="lbNameForIndicators_Callback">
                                        <ItemStyle Wrap="True" />
                                        <ClientSideEvents SelectedIndexChanged="function(s, e){
                                                            var selItem = s.GetSelectedItem();
                                                            ddeName_1_0.SetText(selItem.text);
                                                            ddeName_1_0.HideDropDown();
                                                        }"
                                            EndCallback="function(s,e){
                                                            
                                                        }"></ClientSideEvents>
                                    </dxe:ASPxListBox>
                                </DropDownWindowTemplate>
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                                <ClientSideEvents KeyUp="function(s,e){
                                                    if (e.htmlEvent.keyCode == 38 || e.htmlEvent.keyCode == 40)
                                                        lbName_1_0.Focus();
                                                    if (e.htmlEvent.keyCode == 13)
                                                        s.HideDropDown();
                                                    var param = s.GetText() + '|' + ActivityFuncComboBox_1_0.GetValue();
                                                    lbName_1_0.PerformCallback(param);
                                                    s.ShowDropDown();
                                                }"
                                    Validation="validateTextBox"></ClientSideEvents>
                            </dxe:ASPxDropDownEdit>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Плановое значение:
                        </td>
                        <td class="form_edit_input">
                            <table style="width: 398px">
                                <tr>
                                    <td>на <%= BaseIndicatorYear + 1 %> год</td>
                                    <td>на <%= BaseIndicatorYear + 2 %> год</td>
                                    <td>на <%= BaseIndicatorYear + 3 %> год</td>
                                </tr>
                                <tr>
                                    <td>
                                        <dxe:ASPxTextBox ID="FirstPlanValueTextBox_1_0" runat="server" ClientInstanceName="FirstPlanValueTextBox_1_0" OnDataBinding="FirstPlanValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" Width="100px">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                <RegularExpression ValidationExpression="([0-9]*)[.,/]?([0-9]+)" ErrorText="введите число" />
                                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="validateTextBox" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="SecondPlanValueTextBox_1_0" runat="server" ClientInstanceName="SecondPlanValueTextBox_1_0" OnDataBinding="SecondPlanValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" Width="100px">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                <RegularExpression ValidationExpression="([0-9]*)[.,/]?([0-9]+)" ErrorText="введите число" />
                                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="validateTextBox" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="ThirdPlanValueTextBox_1_0" runat="server" ClientInstanceName="ThirdPlanValueTextBox_1_0" OnDataBinding="ThirdPlanValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" Width="100px">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                <RegularExpression ValidationExpression="([0-9]*)[.,/]?([0-9]+)" ErrorText="введите число" />
                                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="validateTextBox" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">С учетом доп. финансирования:
                        </td>
                        <td class="form_edit_input">
                            <table style="width: 398px">
                                <tr>
                                    <td>
                                        <dxe:ASPxTextBox ID="FirstPlanAddValueTextBox_1_0" runat="server" ClientInstanceName="FirstPlanAddValueTextBox_1_0" OnDataBinding="FirstPlanAddValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" MaxLength="100" Width="100px">
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="SecondPlanAddValueTextBox_1_0" runat="server" ClientInstanceName="SecondPlanAddValueTextBox_1_0" OnDataBinding="SecondPlanAddValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" MaxLength="100" Width="100px">
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="ThirdPlanAddValueTextBox_1_0" runat="server" ClientInstanceName="ThirdPlanAddValueTextBox_1_0" OnDataBinding="ThirdPlanAddValueTextBox_DataBinding"
                                            ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>" MaxLength="100" Width="100px">
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Алгоритм расчета:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="AlgorythmTextBox" runat="server" ClientInstanceName="AlgorythmTextBox" OnDataBinding="AlgorythmTextBox_DataBinding"
                                ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                                <span class="ok"><dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" /></span>
                                <span class="cancel"><dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" /></span>
                            </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){if (e.command=='UPDATEEDIT') SaveIndicator(e); }" />
    </dxwgv:ASPxGridView>
</asp:Content>
