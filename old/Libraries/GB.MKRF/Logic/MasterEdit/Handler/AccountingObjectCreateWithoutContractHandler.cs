﻿using GB.Data.AccountingObjects;

namespace GB.MKRF.Logic.MasterEdit.Handler
{
    class AccountingObjectCreateWithoutContractHandler : MasterEditBaseHandler
    {
        protected override MasterInItem[] GetListPage()
        {
            return new MasterInItem[] 
            {
                new MasterInItem(MasterItemEnum.None),
                new MasterInItem(MasterItemEnum.ResponsiblePurchase),
                new MasterInItem(MasterItemEnum.GovernmentContract)
                {
                    Title = "Укажите сведения о заключенных государственных контрактах",
                },
                new MasterInItem(MasterItemEnum.Software)
                {
                    Title = "Заполните перечень программного обеспечения" +
                            " необходимого для функционирования объекта учета",
                },
                new MasterInItem(MasterItemEnum.TechnicalSupport)
                {
                    Title = "Заполните перечень технического " +
                            "оборудования необходимого для функционирования объекта учета",
                },
                new MasterInItem(MasterItemEnum.WorkAndService)
                {
                    Title = "Заполните перечень работ",
                },
                new MasterInItem(MasterItemEnum.FactualLocation),
                new MasterInItem(MasterItemEnum.CardForRead)
            };
        }
    }
}
