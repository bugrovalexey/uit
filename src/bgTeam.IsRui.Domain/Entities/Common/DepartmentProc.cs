﻿namespace bgTeam.IsRui.Domain.Entities.Common
{
    public class DepartmentProc : EntityBase
    {
        public virtual int Govt_Organ_ID { get; set; }

        /// <summary>Код</summary>
        public virtual string Govt_Organ_Code { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Govt_Organ_Name { get; set; }

        /// <summary>Адрес</summary>
        public virtual string Govt_Organ_Addr { get; set; }

        /// <summary>Краткое наименование</summary>
        public virtual string Govt_Organ_SName { get; set; }

        /// <summary>Телефон</summary>
        public virtual string Govt_Organ_Phone { get; set; }

        /// <summary>Веб-сайт</summary>
        public virtual string Govt_Organ_Web { get; set; }

        /// <summary>Признак активности</summary>
        public virtual bool Govt_Organ_Is_Active { get; set; }

        /// <summary>Сводный перечень заказчиков</summary>
        public virtual string Govt_Organ_SPZ { get; set; }

        /// <summary>Родительский орган</summary>
        public virtual int? Govt_Organ_Par_ID { get; set; }
    }
}