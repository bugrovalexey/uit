﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using GB.Entity;
using GB.Handlers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Entities.Statuses;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Web.Helpers;

namespace GB.MKRF.Web.Common
{
    /// <summary>
    /// Компонент для работы с бумажными формами (перепиской мкс и огв)
    /// </summary>
    public partial class MailsControl : System.Web.UI.UserControl
    {
        PaperController controller = new PaperController();

        //public ExpDemandBase CurrentDemand
        //{
        //    get;
        //    set;
        //}

        public Plan CurrentPlan
        {
            get;
            set;
        }

        private PaperObject currentPaperObject_Internal = null;
        protected PaperObject CurrentPaperObject
        {
            get
            {
                if (currentPaperObject_Internal == null)
                {
                    try
                    {
                        currentPaperObject_Internal = RepositoryBase<PaperObject>.Get(Grid.GetRowValues(Grid.EditingRowVisibleIndex, "Id"));
                    }
                    catch
                    {
                        return currentPaperObject_Internal;
                    }
                }
                return currentPaperObject_Internal;
            }
            set
            {
                if (value != null)
                    currentPaperObject_Internal = value;
            }
        }

        // список файлов (используется на форме редактирования)
        IList<DocToPaper> filesList_Internal = null;
        bool filesList_Loaded = false;
        IList<DocToPaper> FilesList
        {
            get
            {
                if (!filesList_Loaded)
                {
                    filesList_Internal = null;
                    if (CurrentPaperObject != null && (userSide == userSideEnum.ogv || userSide == userSideEnum.mks))
                    {
                        DocToPaperType docType = (editMode == editModeEnum.ogv ? DocToPaperType.Ogv : DocToPaperType.Mks);

                        filesList_Internal = RepositoryBase<DocToPaper>.GetAll(new GetAllArgs<DocToPaper>()
                        {
                            Expression = (x => x.PaperObject == CurrentPaperObject && x.Type == docType),
                            Fetch = new Expression<Func<DocToPaper, object>>[] { x => x.Document }
                        });

                        // отсеиваем запрещнные документы
                        AccessRepository.FillAccess<DocToPaper>(filesList_Internal, AccessAction.Read, AccessAction.Edit, AccessAction.Delete);

                        filesList_Internal = filesList_Internal.Where(x => (x.Access & AccessAction.Read) == AccessAction.Read).ToList();
                        filesList_Loaded = true;
                    }
                }
                return filesList_Internal;
            }
        }

        /// Режим редактирования(правый / левый столбец)
        private editModeEnum? editMode_Internal = null;
        protected editModeEnum editMode
        {
            get
            {
                if (!editMode_Internal.HasValue)
                {
                    editMode_Internal = editModeEnum.none;
                    if (hf.Contains("editMode"))
                    {
                        string mode = hf["editMode"].ToString();
                        if (mode == "ogv")
                        {
                            // слева могут править мкс и огв
                            if (userSide == userSideEnum.ogv || userSide == userSideEnum.mks)
                                editMode_Internal = editModeEnum.ogv;
                        }
                        else if (mode == "mks" && userSide == userSideEnum.mks)
                        {
                            // справа может править только мкс
                            editMode_Internal = editModeEnum.mks;
                        }
                    }
                }
                return editMode_Internal.Value;
            }
        }

        /// Сторона пользователя (при редактировании)
        private userSideEnum? userSide_Internal = null;
        protected userSideEnum userSide
        {
            get
            {
                if (!userSide_Internal.HasValue)
                {
                    RoleBase currentRole = (UserRepositoryBase.GetCurrentEx() as User).Role;

                    if (currentRole.IsEq(RoleEnum.Department))
                        userSide_Internal = userSideEnum.ogv;
                    else if (currentRole.IsEq(RoleEnum.CA)) // TODO: уточнить кто ещё обладает правами мкс
                        userSide_Internal = userSideEnum.mks;
                    else
                        userSide_Internal = userSideEnum.none;
                }
                return userSide_Internal.Value;
            }
        }

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    var r = 10;
        //}

        protected override void OnPreRender(EventArgs e)
        {
            AddButton.Visible = Validation.Validator.Validate(new PaperObject(CurrentPlan.Demand, new PaperType()), ObjActionEnum.create);
            
            Grid.DataBind();

            base.OnPreRender(e);
        }


        protected void grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            // извлекаем все бумажные формы плана
            IList<PaperObject> papers = RepositoryBase<PaperObject>.GetAll(
                new GetAllArgs<PaperObject>()
                {
                    Expression = (x => x.Owner == CurrentPlan.Demand),
                    Fetch = new Expression<Func<PaperObject, object>>[]
                    {
                        x=>x.Status,
                        x=>x.PaperType
                    },
                    OrderByDesc = (x => x.DateOutgoing)
                });

            // заполняем права доступа
            AccessRepository.FillAccess<PaperObject>(papers, AccessAction.Edit, AccessAction.Delete);

            // извлекаем документы бумажных форм
            IList<DocToPaper> docToPaperList = RepositoryBase<DocToPaper>.GetAll(
               new GetAllArgs<DocToPaper>()
               {
                   IdsSelector = (x => x.PaperObject),
                   Ids = papers.Select(x => x.Id).ToList(),
                   Fetch = new Expression<Func<DocToPaper, object>>[]
                   {
                       x => x.Document
                   }
               });

            // отсеиваем запрещнные документы
            AccessRepository.FillAccess<DocToPaper>(docToPaperList, AccessAction.Read);

            var datasource = papers.Select(x => new
            {
                Id = x.Id,
                NumberOutgoing = x.NumberOutgoing,
                DateOutgoing = x.DateOutgoing.HasValue ? x.DateOutgoing.Value.ToShortDateString() : null,
                NumberInboxMKS = x.NumberInboxMKS,
                DateInboxMKS = x.DateInboxMKS.HasValue ? x.DateInboxMKS.Value.ToShortDateString() : null,
                State = x.Status.Name,
                NumberOutgoingMKS = x.NumberOutgoingMKS,
                DateOutgoingMKS = x.DateOutgoingMKS.HasValue ? x.DateOutgoingMKS.Value.ToShortDateString() : null,
                Comment = x.Comment,


              

                FilesOgv = MassToUl(docToPaperList
                                                    .Where(z => z.PaperObject == x && z.Type == DocToPaperType.Ogv && (z.Access & AccessAction.Read) == AccessAction.Read)
                                                    .Select(d => FileHandler.GetLink(d.Document.Id.ToString(), d.Document.Name)
                                                                 + (d.IsExpertConclusion ? " (ЭЗ)" : null)
                                                        )
                                                    .ToArray()),
                FilesMks = MassToUl(docToPaperList
                                                    .Where(z => z.PaperObject == x && z.Type == DocToPaperType.Mks && (z.Access & AccessAction.Read) == AccessAction.Read)
                                                    .Select(d => FileHandler.GetLink(d.Document.Id.ToString(), d.Document.Name)
                                                                 + (d.IsExpertConclusion ? " (ЭЗ)" : null)
                                                                )
                                                    .ToArray()),

                CanEdit = ((x.Access & AccessAction.Edit) == AccessAction.Edit),
                CanDelete = ((x.Access & AccessAction.Delete) == AccessAction.Delete),
                Direction = string.IsNullOrWhiteSpace(x.NumberOutgoingMKS) ? "→" : "⇆"

            }).ToList();

            Grid.DataSource = datasource;
        }

        private string MassToUl(string[] mass)
        {
            string str = string.Empty;

            for (int i = 0; i < mass.Length; i++)
            {
                str = string.Concat(str, "<li>", mass[i], "</li>");
            }

            return string.Concat("<ul>", str, "</ul>");
        }

        protected void grid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            CurrentPaperObject = RepositoryBase<PaperObject>.Get(e.EditingKeyValue);
            Grid.SettingsText.PopupEditFormCaption = editMode == editModeEnum.ogv ? "Исходящее письмо из ОГВ" : "Письмо";
            //grid.Images.PopupEditFormWindowClose.Url = "/Content/GB/Styles/Imgs/popup_close.png";
        }

        protected void grid_CustomJSProperties(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewClientJSPropertiesEventArgs e)
        {
            string ids = string.Empty;
            if (this.FilesList != null)
            {
                ids = string.Join(";", FilesList.Select(x => x.Document.Id.ToString()).ToArray());
            }
            e.Properties["cpFiles"] = ids;
        }

        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            PaperControllerArgs args = new PaperControllerArgs();
            args.Demand = CurrentPlan.Demand;
            setArgs(args);
            controller.Create(args);

            //PaperObject p = new PaperObject(CurrentPlan.Demand, RepositoryBase<PaperType>.Get((int)Undefined.value));
            //p.Status = StatusRepository.GetFirstStatus(p);
            //saveRow(p);

            // Чтобы спрятать кнопку "Создать" делаем редирект
            DevExpress.Web.ASPxGridView.ASPxGridView.RedirectOnCallback(Request.RawUrl);
            //(sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            PaperControllerArgs args = new PaperControllerArgs();
            args.Id = e.Keys[0];
            setArgs(args);
            controller.SaveOrUpdate(args);

            //PaperObject paperObject = RepositoryBase<PaperObject>.Get((int)e.Keys[0]);
            //if (paperObject != null)
            //{
            //    saveRow(paperObject);
            //}

            (sender as ASPxGridView).GridCancelEdit(e, true);
        }

        protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            controller.Delete(new PlanControllerArgs() { Id = e.Keys[0] });
            
            //PaperObject paperObject = RepositoryBase<PaperObject>.Get((int)e.Keys[0]);
            //RepositoryBase<PaperObject>.Delete(paperObject.Id);

            // Чтобы отобразить кнопку "Создать" делаем редирект
            DevExpress.Web.ASPxGridView.ASPxGridView.RedirectOnCallback(Request.RawUrl);
            //(sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void setArgs(PaperControllerArgs args)
        {
            args.editMode = this.editMode;
            args.userSide = this.userSide;

            args.NumberOutgoing =  GetHFValue("NumberOutgoing");
            args.DateOutgoing = GetHFValue("DateOutgoing");
            args.NumberInboxMKS = GetHFValue("NumberInboxMKS");
            args.DateInboxMKS = GetHFValue("DateInboxMKS");
            args.Status = GetHFValue("State");
            args.NumberOutgoingMKS = GetHFValue("NumberOutgoingMKS");
            args.DateOutgoingMKS = GetHFValue("DateOutgoingMKS");
            args.Comment = GetHFValue("Comment");
            args.Files = GetHFValue("Files");
            args.SelectedFiles = GetHFValue("SelectedFiles");
        }

        object GetHFValue(string valueName)
        {
            return hf.Contains(valueName) ? hf[valueName] : null;
        }

        protected void Box_DataBinding(object sender, EventArgs e)
        {
            switch (((System.Web.UI.Control)sender).ID)
            {
                case "NumberOutgoingTextBox":
                    (sender as ASPxTextBox).Text = (CurrentPaperObject != null ? CurrentPaperObject.NumberOutgoing : null);
                    (sender as ASPxTextBox).Enabled = (userSide == userSideEnum.ogv);
                    break;

                case "DateOutgoingDateEdit":
                    (sender as ASPxDateEdit).Value = (CurrentPaperObject != null && CurrentPaperObject.DateOutgoing !=null) ? CurrentPaperObject.DateOutgoing : DateTime.Now;
                    (sender as ASPxDateEdit).Enabled = (userSide == userSideEnum.ogv);
                    break;

                case "NumberInboxMKSTextBox":
                    (sender as ASPxTextBox).Text = (CurrentPaperObject != null ? CurrentPaperObject.NumberInboxMKS : null);
                    (sender as ASPxTextBox).Enabled = (userSide == userSideEnum.mks);
                    break;

                case "DateInboxMKSDateEdit":
                    (sender as ASPxDateEdit).Value = (CurrentPaperObject != null && CurrentPaperObject.DateInboxMKS != null) ? 
                        CurrentPaperObject.DateInboxMKS : 
                        (CurrentPaperObject != null && CurrentPaperObject.DateOutgoing != null ? (DateTime?)DateTime.Now : null);
                    (sender as ASPxDateEdit).Enabled = (userSide == userSideEnum.mks);
                    break;

                case "StateComboBox":
                    {
                        ASPxComboBox box = (sender as ASPxComboBox);
                        box.DataSource = RepositoryBase<Status>.GetAll(x => x.StatusGroup.Id == (int)StatusGroupEnum.Paper); 

                        if (CurrentPaperObject == null)
                        {
                            PaperObject p = new PaperObject(CurrentPlan.Demand, RepositoryBase<PaperType>.Get((int)Undefined.value));
                            box.Value = StatusRepositoryOld.GetFirstStatus(p).Id;
                        }
                        else
                        {
                            box.Value = CurrentPaperObject.Status.Id;
                        }
                        box.Enabled = (userSide == userSideEnum.mks);
                    }
                    break;


                // далее компоненты, которые правит только мкс - они всегда открыты для редактирования
                case "NumberOutgoingTextBox2": (sender as ASPxTextBox).Text = (CurrentPaperObject != null ? CurrentPaperObject.NumberOutgoing : null); break;
                case "NumberOutgoingMKSTextBox": (sender as ASPxTextBox).Text = (CurrentPaperObject != null ? CurrentPaperObject.NumberOutgoingMKS : null); break;
                case "DateOutgoingMKSDateEdit": (sender as ASPxDateEdit).Value = (CurrentPaperObject != null && CurrentPaperObject.DateOutgoingMKS != null ? CurrentPaperObject.DateOutgoingMKS : DateTime.Now); break;
                case "CommentMemo": (sender as ASPxMemo).Text = (CurrentPaperObject != null ? CurrentPaperObject.Comment : null); break;

                default:
                    throw new ArgumentException(((System.Web.UI.Control)sender).ID);
            }
        }

        //protected string getFiles()
        //{
        //    return (FilesList != null) ? string.Join("<br />", FilesList.Select(x => x.Document.Name).ToArray()) : null;
        //}

        protected class filesGridItem
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public bool Checked {get; set;}
        }

        IList<filesGridItem> filesGridDatasourceInternal = null;
        /// <summary>Создаёт filesGridDatasourceInternal или возвращает его, если он уже есть</summary>
        IList<filesGridItem> CreateFilesGridDataSource(object source = null, bool recreate = false)
        {
            if (recreate)
                filesGridDatasourceInternal = null;
            else if (filesGridDatasourceInternal != null)
                return filesGridDatasourceInternal;

            if (source != null && source is IList<DocToPaper>)
            {
                filesGridDatasourceInternal = ((IList<DocToPaper>)FilesList).Select(x => new
                                                filesGridItem()
                                                {
                                                    Id = x.Document.Id,
                                                    Name = x.Document.Name,
                                                    Checked = x.IsExpertConclusion
                                                }).ToList();
            }
            else if (source != null && source is string)
            {
                // слева - ид файлов, справа - ид файлов, являющихся ЭЗ
                string[] pair = source.ToString().Split('#');
                if (pair.Length == 2)
                {
                    IList<Document> docs = RepositoryBase<Document>.GetAll(new GetAllArgs<Document>()
                                            {
                                                Ids = pair[0].Split(';').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)).ToList()
                                            });

                    HashSet<int> selected = new HashSet<int>(pair[1].Split(';').Where(x=> !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)).ToList());

                    filesGridDatasourceInternal = docs.Select(x => 
                        new filesGridItem()
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Checked = selected.Contains(x.Id)
                        }).ToList();
                }
            }

            if (filesGridDatasourceInternal == null)
            {
                filesGridDatasourceInternal = new List<filesGridItem>();
            }

            return filesGridDatasourceInternal;
        }

        protected void filesGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            ASPxGridView grid = ((ASPxGridView)sender);
            //grid.Columns["Checked"].Visible = (this.editMode == editModeEnum.mks);                
            grid.DataSource = CreateFilesGridDataSource(grid.IsCallback ? null : FilesList);
        }

        protected void filesGrid_CustomJSProperties(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewClientJSPropertiesEventArgs e)
        {
            IList<filesGridItem> ds= CreateFilesGridDataSource(FilesList);
            e.Properties["cpSelected"] = (ds != null) ? string.Join(";", ds.Where(x => x.Checked).Select(x => x.Id.ToString()).ToArray()) : null;
        }

        protected void filesGrid_Callback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            CreateFilesGridDataSource(e.Parameters ?? string.Empty, true);
            ((ASPxGridView)sender).DataBind();
        }

        protected void chk_Init(object sender, EventArgs e)
        {
            ASPxCheckBox chk = sender as ASPxCheckBox;
            GridViewDataItemTemplateContainer container = chk.NamingContainer as GridViewDataItemTemplateContainer;
            chk.ClientSideEvents.CheckedChanged = "function (s, e) { updateSelectedFiles('" + container.KeyValue + "', s.GetChecked()); }";
        }





        protected string GetMailText(string num, string date, string state = null)
        {
            if (!string.IsNullOrWhiteSpace(num) && !string.IsNullOrWhiteSpace(date))
            {
                string str = !string.IsNullOrWhiteSpace(state) ? string.Concat(" (", state, ")") : null;

                return string.Concat("№ ", num, " от ", date, str);
            }
            else
            {
                return " — ";
            }
        }
    }
}