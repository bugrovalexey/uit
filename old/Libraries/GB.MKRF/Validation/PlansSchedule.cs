﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Validator
{
    class PlansSchedule
    {
        public static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create)
            {
                Entities.Plans.PlansSchedule entity = Entity as Entities.Plans.PlansSchedule;

                if (entity.Plan != null)
                    return "0;" + entity.Plan.Id.ToString();
                else
                    return "0";
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
