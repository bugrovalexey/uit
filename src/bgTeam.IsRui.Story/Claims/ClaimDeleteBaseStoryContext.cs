﻿namespace bgTeam.IsRui.Story.Claims
{
    /// <summary>
    /// Контекст удаления заявки
    /// </summary>
    public class ClaimDeleteBaseStoryContext
    {
        /// <summary>
        /// Идентификатор заявки
        /// </summary>
        public int? Id { get; set; }
    }
}
