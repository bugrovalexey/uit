﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Система проектного управления мероприятиями по информатизации
    /// </summary>
    public class ManagementStructure : EntityBase
    {
        protected ManagementStructure()
        {
        }

        public ManagementStructure(Plan Plan)
        {
            this.Plan = Plan;
        }

        /// <summary>Коллегиальный элемент органа управления</summary>
        public virtual string Element { get; set; }

        /// <summary>Уровни управления</summary>
        //public virtual ManagementLevel Level { get; set; }

        /// <summary>Периодичность совещаний коллегиального элемента управления</summary>
        public virtual string Period { get; set; }

        /// <summary> План</summary>
        public virtual Plan Plan { get; protected set; }

        /// <summary> Комментарии </summary>
        public virtual string Comment { get; set; }

        
        /// <summary> фамилия руководителя </summary>
        public virtual string ManagerLastName { get; set; }

        /// <summary> имя руководителя </summary>
        public virtual string ManagerFirstName { get; set; }
        
        /// <summary> отчество руководителя </summary>
        public virtual string ManagerSurName { get; set; }

        /// <summary> должность руководителя </summary>
        public virtual string ManagerPost { get; set; }
    }
}
