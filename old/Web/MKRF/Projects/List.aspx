﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="GB.MKRF.Web.Projects.List" %>

<%@ Import Namespace="GB" %>
<%@ Import Namespace="GB.MKRF.Entities.Directories" %>
<%@ Import Namespace="GB.MKRF.Web.Projects" %>

<%@ MasterType VirtualPath="~/Main.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        ActiveMenuItem = 4;

        function GridSave(e) {
            hf.Set('ShortName', ShortNameTextBox.GetValue());
        }
    </script>
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
    <div class="page">
        <div class="detailTop">
            <dxe:ASPxButton ID="addButton" runat="server" ClientInstanceName="addButton" AutoPostBack="false"
                Text="Добавить" CssClass="topButton fleft">
                <ClientSideEvents Click="function(s,e) { Grid.AddNewRow(); }" />
            </dxe:ASPxButton>
        </div>
        <dxwgv:ASPxGridView runat="server" ID="Grid" KeyFieldName="Id" Width="100%"
            ClientInstanceName="Grid" AutoGenerateColumns="False"
            OnBeforePerformDataSelect="Grid_BeforePerformDataSelect" OnStartRowEditing="Grid_StartRowEditing"
            OnRowInserting="Grid_RowInserting" OnRowUpdating="Grid_RowUpdating" OnRowDeleting="Grid_RowDeleting">
            <Settings ShowFooter="True" ShowFilterRow="true" />
            <Columns>
                <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False"
                    AllowDragDrop="False" ButtonType="Image" CellStyle-VerticalAlign="Middle">
                    <ClearFilterButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/clear_16.png" Image-ToolTip="Очистить" />
                </dxwgv:GridViewCommandColumn>

                <dxwgv:GridViewDataTextColumn Caption="Краткое наименование" FieldName="ShortName">
                    <DataItemTemplate>
                        <%# GetLinkToCard(Eval("Id"), Eval("ShortName")) %>
                    </DataItemTemplate>
                    <Settings AutoFilterCondition="Contains" AllowSort="True" SortMode="Value" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Учреждение ответственное за проект" FieldName="Department.Name">
                    <Settings AutoFilterCondition="Contains" AllowSort="True" SortMode="Value" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Сотрудник ответственный за проект" FieldName="ResponsibleUser.FullName">
                    <Settings AutoFilterCondition="Contains" AllowSort="True" SortMode="Value" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Сумма планируемых расходов" FieldName="SumOfExpenses" Width="150px">
                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                    <Settings AutoFilterCondition="Equals" AllowSort="True" SortMode="Value" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Статус" FieldName="Status.Name" Width="200px">
                    <Settings AutoFilterCondition="Contains" AllowSort="True" SortMode="Value" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewCommandColumn Caption=" " Name="delColumn" ShowInCustomizationForm="False" AllowDragDrop="False" ButtonType="Image">
                    <DeleteButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/delete_16.png" Image-ToolTip="Удалить" />
                </dxwgv:GridViewCommandColumn>
            </Columns>
            <TotalSummary>
                <dxwgv:ASPxSummaryItem ShowInColumn="ShortName" DisplayFormat="Количество проектов: {0}" SummaryType="Count" />
                <dxwgv:ASPxSummaryItem ShowInColumn="SumOfExpenses" DisplayFormat="{0:#,##0.0000}" FieldName="SumOfExpenses" SummaryType="Sum" />
            </TotalSummary>
            <Templates>
                <EditForm>
                    <table class="form_edit">
                        <tr>
                            <td class="form_edit_desc required">Краткое наименование:
                            </td>
                            <td class="form_edit_input">
                                <dxe:ASPxTextBox runat="server" ID="ShortNameTextBox" ClientInstanceName="ShortNameTextBox"
                                    OnDataBinding="ShortNameTextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                    <ValidationSettings ErrorDisplayMode="Text">
                                        <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                    </ValidationSettings>
                                </dxe:ASPxTextBox>
                            </td>
                        </tr
                        <tr>
                            <td colspan="2" class="form_edit_buttons">
                                <span class="ok">
                                    <a onclick="Grid.UpdateEdit();">Сохранить</a>
                                </span>
                                <span class="cancel">
                                    <a onclick="Grid.CancelEdit();">Отмена</a>
                                </span>
                            </td>
                        </tr>
                    </table>
                </EditForm>
            </Templates>
            <ClientSideEvents BeginCallback="function(s,e) { if (e.command=='UPDATEEDIT') { GridSave(e);} }" />
        </dxwgv:ASPxGridView>
    </div>
</asp:Content>
