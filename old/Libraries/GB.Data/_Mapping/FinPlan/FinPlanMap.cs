﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.FinPlan
{
    public class FinPlanMap : ClassMapBase<Entities.FinPlan.FinPlan>
	{
        public FinPlanMap()
            : base("Fin_Plan")
		{
//            Table("w_");

//            Id(x => x.Id, "Fin_Plan_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_Fin_Plan_Insert 
//						  @Year = ?
//						 ,@Fin_Plan_Date = ?
//						 ,@Fin_Plan_Analysis_Date = ?
//						 ,@Govt_Organ_ID = ?
//                         ,@Fin_Plan_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Fin_Plan_Delete 
//						  @Fin_Plan_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Year, "Year");
			Map(x => x.Date, "Fin_Plan_Date");
			Map(x => x.AnalysisDate, "Fin_Plan_Analysis_Date");

			References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
        }
    }
}
