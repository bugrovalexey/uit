﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>Мероприятие Плана</summary>
    public class PlansActivityMUReportRegion : EntityBase
    {
        /// <summary> Мероприятие для муниципальных объектов </summary>
        public virtual PlansActivityMU PlansActivityMU { get; set; }
        /// <summary> Регион  </summary>
        public virtual Department Region { get; set; }
    }
}