﻿using System;
using System.Collections.Generic;
using System.Linq;
using GB;
using GB.Controls;
using GB.Handlers;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using GB.MKRF.Report;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class CommonData : System.Web.UI.Page
    {
        const string FGIS_NAME = "Мероприятие не направлено на создание и эксплуатацию ФГИС";

        /// <summary>
        /// Текущее мероприятие
        /// </summary>
        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as ActivityCard).CurrentActivity;
            }
        }

        public static string GetUrl(object activityId)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/Plans/v2012new/CommonData.aspx"), ActivityCard.ActivityIdKey, activityId);
        }

        protected string GetIKTSelectorUrl()
        {
            return String.Format("../../Selectors/IKT.aspx?year={0}&id={1}", CurrentActivity.Plan.Year,
                CurrentActivity.IKTComponent == null ? 0 : CurrentActivity.IKTComponent.Id);
        }

        protected string GetIKTObjectSelectorUrl()
        {
            return String.Format("../../Selectors/IKTObject.aspx?id={0}",
                CurrentActivity.IKTObject == null ? 0 : CurrentActivity.IKTObject.Id);
        }

        protected string GetRespSelectorUrl()
        {
            return String.Format("../../Selectors/Responsible.aspx?id={0}", CurrentActivity.Responsible == null ? 0 : CurrentActivity.Responsible.Id);            
        }

        /// <summary>
        /// Разрешено редактировать мероприятие ?
        /// </summary>
        //public bool EnableEdit
        //{
        //    get
        //    {
        //        return (Master as ActivityCard).EnableEdit;
        //    }
        //}

        protected override void OnPreRender(EventArgs e)
        {
            ShowObjectsRepository.SaveVisitedObjects(CurrentActivity.Id, ObjectsType.Activity, UserRepository.GetCurrent().Id);            

            AnnotationMemo.Text = CurrentActivity.AnnotationData;
            PlansActivityDatePlacement.Value = CurrentActivity.DatePlacement;
            TypeComboBox.DataBind();
            //ResponsibleComboBox.DataBind();
            FundingComboBox.DataBind();
//            IKTObjectComboBox.DataBind();
            IKTObjectTextBox.DataBind();

            SetAccess((Master as ActivityCard).EnableEdit);

            ReasonsGrid.DataBind();
            
            
            PlansActivityGoalGrid.DataBind();
            

            IKTSelectionTextBox.DataBind();
            //IKTPopupControl.ContentUrl = GetIKTSelectorUrl();

            ResponsibleTextBox.DataBind();
            //ResponsiblePopupControl.ContentUrl = GetRespSelectorUrl();


            base.OnPreRender(e);
        }

        private void SetAccess(bool acs)
        {
            saveButton.ClientEnabled = acs;
            TypeComboBox.ClientEnabled = acs;
            IKTObjectTextBox.ClientEnabled = acs;
            IKTSelectionTextBox.ClientEnabled = acs;
            AnnotationMemo.ClientEnabled = acs;
            ResponsibleTextBox.ClientEnabled = acs;
            FundingComboBox.ClientEnabled = acs;
            PlansActivityDatePlacement.ClientEnabled = acs;

            AddReasonsButton.Visible = acs;
            ReasonsGrid.Columns["cmdColumn"].Visible = acs;

            AddPlansActivityGoalButton.Visible = acs;
            PlansActivityGoalGrid.Columns["cmdColumn"].Visible = acs;
        }

        void refreshActivity()
        {
            (Master as ActivityCard).RefreshActivity();
        }

        protected string getDownloadUrl(string FileInfo)
        {
            if (string.IsNullOrEmpty(FileInfo))
                return null;

            List<string> links = new List<string>();

            string[] files = FileInfo.Split(';');
            foreach (string f in files)
            {
                string[] pair = f.Split(':');
                links.Add(string.Format("<a href='{0}'>{1}</a>", FileHandler.GetUrl(pair[0]),
                                                                 pair.Length > 1 ? pair[1] : "Скачать"));
            }
            return string.Join("<br />", links);
        }


        protected void TypeComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = RepositoryBase<ActivityType2>.GetAll();

            box.Value = CurrentActivity.ActivityType2.Id;
        }
        
        protected void FundingComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = RepositoryBase<SourceFunding>.GetAll();
            
            box.Value = CurrentActivity.SourceFunding != null ? CurrentActivity.SourceFunding.Id : 0;
        }

        protected void IKTObjectComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = RepositoryBase<IKTObject>.GetAll(null, x=> x.Name);

            if (CurrentActivity.IKTObject != null)
            {
                box.Value = CurrentActivity.IKTObject.Name;
            }
        }

        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            var args = new PlansActivityArgs()
            {
                Id = CurrentActivity.Id,
                AnnotationData = hf["Annotation"],
                IKTSelectionId = hf["IKTSelectionId"],
                ActivityType2 = hf["Type"],
                ResponsibleId = hf["ResponsibleId"],
                FundingId = hf["Funding"],
                IKTObjectId = hf["IKTObjectId"],
                DatePlacement = hf["DatePlacement"]
            };
            
            var controller = new PlansActivityController();
            controller.Update(args);
            refreshActivity();
        }

        protected void TextBox_DataBinding(object sender, EventArgs e)
        {
            var textbox = sender as ASPxTextBox;
            switch (textbox.ID)
            {
                case "IKTSelectionTextBox":
                    SetHF(textbox, "IKTSelection", CurrentActivity.IKTComponent);
                    if (CurrentActivity.IKTComponent != null)
                    {
                        textbox.Text = CurrentActivity.IKTComponent.Code.ToString() + " " + CurrentActivity.IKTComponent.Name;
                        hf.Set("IKTSelectionName", textbox.Text);
                    }
                    break;
                case "ResponsibleTextBox":
                    SetHF(textbox, "Responsible", CurrentActivity.Responsible);
                    break;
                case "IKTObjectTextBox":
                    SetHF(textbox, "IKTObject", CurrentActivity.IKTObject);
                    break;
               
                default:
                    throw new ApplicationException("Отсутствие элемента");
            }
        }
        private void SetHF(ASPxTextBox textbox, string name, IEntityDirectories entity)
        {
            if ((CurrentActivity == null) || (entity == null))
            {
                textbox.Text = "Не задано";
                hf.Set(name + "Id", 0);
            }
            else
            {
                textbox.Text = entity.Name;
                hf.Set(name + "Id", entity.Id);
                hf.Set(name + "Name", entity.Name);
            }
        }    
        
        #region ReasonGrid
        PlansActivityReasonController reasonController = new PlansActivityReasonController();
        protected void ReasonsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            if (CurrentActivity != null)
                ReasonsGrid.DataSource = 
                    EntityHelper.ConvertToAnonymous<PlansActivityReason>(CurrentActivity.ActivityReasons,
                        delegate(PlansActivityReason ar)
                        {
                            return new
                            {
                                Id = ar.Id,
                                Name = string.Format("{0} \"{1}\"", ar.NPAType.Name, ar.Name),
                                Rekvisit = string.Format("№{0} {1}", ar.Num, ar.Date.HasValue ? ("от " + Convert.ToDateTime(ar.Date).ToShortDateString()) : null),
                                Item = ar.Item,
                                FileInfo = string.Join(";", ar.Documents.Select(x => (x.Id.ToString() + ":" + x.FileName)).ToArray())
                            };
                        });
        }

        protected void ReasonsGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            reasonController.Create(new PlansActivityReasonArgs()
            {
                ActivityId = CurrentActivity.Id,
                Name = hfReasons["NPAName"],
                Type = hfReasons["NPATypeId"],
                Num = hfReasons["NPANum"],
                Date = hfReasons["NPADate"],
                Item = hfReasons["NPAItem"],
                Files = hfReasons["NPAFiles"]
                });

            ReasonsGrid.GridCancelEdit(e);
        }

        protected void ReasonsGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            reasonController.Update(new PlansActivityReasonArgs(){
                Id = e.Keys[0],
                Name = hfReasons["NPAName"],
                Type = hfReasons["NPATypeId"],
                Num = hfReasons["NPANum"],
                Date = hfReasons["NPADate"],
                Item = hfReasons["NPAItem"],
                Files = hfReasons["NPAFiles"]
            });

            ReasonsGrid.GridCancelEdit(e);
        }

        protected void ReasonsGrid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            reasonController.Delete(new PlansActivityReasonArgs(){
                Id = e.Keys[0]
            });

            ReasonsGrid.GridCancelEdit(e);
        }

        protected void ReasonsGrid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            reasonController.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void ReasonNPANameTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxMemo box = (ASPxMemo)sender;
            box.Text = reasonController.CurrentEdit == null ? string.Empty : reasonController.CurrentEdit.Name;
        }

        protected void ReasonNPATypeComboBox_DataBinding(object sender, EventArgs e)
        {
            Dictionary<int, string> listStr = new Dictionary<int, string>();
            IList<NPAType> list = RepositoryBase<NPAType>.GetAll();

            foreach (NPAType item in list)
            {
                listStr.Add(item.Id, item.Name);
            }

            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = listStr;

            box.Value = reasonController.CurrentEdit == null ? 0 : reasonController.CurrentEdit.NPAType.Id;
        }

        protected void ReasonNPANumTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = reasonController.CurrentEdit == null ? string.Empty : reasonController.CurrentEdit.Num;
        }

        protected void ReasonNPADateDateEdit_DataBinding(object sender, EventArgs e)
        {
            ASPxDateEdit box = (ASPxDateEdit)sender;

            box.Value = reasonController.CurrentEdit == null ? null : reasonController.CurrentEdit.Date;
        }

        protected void ReasonItemTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = reasonController.CurrentEdit == null ? string.Empty : reasonController.CurrentEdit.Item;
        }

        protected void uploader2_DataBinding(object sender, EventArgs e)
        {
            FileUploadControl2 uploader = (FileUploadControl2)sender;
            uploader.FileItemTemplate = "<a href=\"" + FileHandler.GetUrl("") + "{id}\">{name}</a>";

            if (reasonController.CurrentEdit != null)
            {
                foreach (Document doc in reasonController.CurrentEdit.Documents)
                {
                    uploader.SetValue(doc.FileName, doc.Id.ToString());
                }
            }
        }

        #endregion

        #region PlansActivityGoalsGrid

        private PlansActivityGoalController goalController = new PlansActivityGoalController();
        protected void PlansActivityGoalGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            PlansActivityGoalGrid.DataSource = CurrentActivity.PlansActivityGoals;
        }

        protected void PlansActivityGoalGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            goalController.Create(new PlansActivityGoalArgs()
            {
                ActivityId = CurrentActivity.Id,
                Name = hfPlansActivityGoal["GoalName"]
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void PlansActivityGoalGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            goalController.Update(new PlansActivityGoalArgs()
            {
                Id = e.Keys[0],
                Name = hfPlansActivityGoal["GoalName"]
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void PlansActivityGoalGrid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            goalController.Delete(new PlansActivityGoalArgs()
            {
                Id = e.Keys[0],
            });
            
            (sender as ASPxGridView).GridCancelEdit(e);
        }
        
        protected void PlansActivityGoalGrid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            goalController.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void PlansActivityGoalTextBox_Load(object sender, EventArgs e)
        {
            if (goalController.CurrentEdit != null)
                (sender as ASPxMemo).Text = goalController.CurrentEdit.Name;
        }
        #endregion              
    }
}