﻿using GB.Entity.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class OGVMap : ClassMapBase<OGV>
    {
        public OGVMap()
            : base("OGV") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "OGV_Name");
            Map(x => x.ShortName, "OGV_SName");
            Map(x => x.Mnemo, "OGV_Mnemo");
            Map(x => x.OgvType, "OGV_Type");
            Map(x => x.Inn, "OGV_Inn");
            Map(x => x.Ogrn, "OGV_Ogrn");
            Map(x => x.ParentOgvId, "OGV_Par_ID");
        }
    }
}