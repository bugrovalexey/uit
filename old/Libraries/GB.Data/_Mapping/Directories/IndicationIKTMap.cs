﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class IndicationIKTMap : ClassMapBase<IndicationIKT>
    {
        public IndicationIKTMap() :
            base("Indication_IS")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Indication_IS_Name");

            References<MeasureUnit>(x => x.Measure, "Measure_Unit_ID");
            References<ClassComponentIKT>(x => x.ClassComponentIKT, "Class_Component_IKT_ID");

            HasManyToMany(x => x.ActivityTypes)
                .Table("Indication_Type")
                .ParentKeyColumn("Indication_IS_ID")
                .ChildKeyColumn("Plans_Activity_Type2_ID")
                .ReadOnly();

            HasManyToMany(x => x.IKTComponents)
                .Table("Indication_IKT_Component")
                .ParentKeyColumn("Indication_IS_ID")
                .ChildKeyColumn("IKT_Component_ID")
                .ReadOnly();
        }
    }
}