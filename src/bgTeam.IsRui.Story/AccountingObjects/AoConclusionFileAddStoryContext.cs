﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;

    public class AoConclusionFileAddStoryContext
    {
        public int? Id { get; set; }

        public int? AoId { get; set; }

        public Guid? TempGuid { get; set; }

        public string FileName { get; set; }

        public DocKindEnum Kind { get; set; }

        public string Extension { get; set; }

        public int? Size { get; set; }

        public string Url { get; set; }

        public string MimeType { get; set; }
    }
}