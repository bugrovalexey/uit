﻿using bgTeam.IsRui.Domain.Entities.Documents;
using bgTeam.IsRui.Domain.Entities.Plans;

namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    // !!! Не используется !!!
    class PlansActivityReasonMap : ClassMapBase<PlansActivityReason>
    {
        public PlansActivityReasonMap()
            : base("Plans_Activity_Reason")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Plans_Activity_Reason_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_Plans_Activity_Reason_Insert
            //                          @Plans_Activity_ID = ?
            //                         ,@NPA_ID = ?
            //                         ,@Plans_Activity_Reason_Document_ID = ?
            //                         ,@Plans_Activity_Reason_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Plans_Activity_Reason_Update
            //                          @Plans_Activity_ID = ?
            //                         ,@NPA_ID = ?
            //                         ,@Plans_Activity_Reason_Document_ID = ?
            //                         ,@Plans_Activity_Reason_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Plans_Activity_Reason_Delete
            //						  @Plans_Activity_Reason_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Reason_Name");
            Map(x => x.Num, "Plans_Activity_Reason_Num");
            Map(x => x.Date, "Plans_Activity_Reason_DT").Nullable();
            Map(x => x.Item, "Plans_Activity_Reason_Item");

            //References<NPAType>(x => x.NPAType, "NPA_Type_ID");
            //References<NPAKind>(x => x.NPAKind, "NPA_Kind_ID");
            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID");

            //References<NPA>(x => x.NPA, "NPA_ID");
            //References<Document>(x => x.Document, "Plans_Activity_Reason_Document_ID").Nullable();

            HasMany<Files>(x => x.Documents)
                   .KeyColumns.Add("Document_Owner_ID")
                   //.Where("Portal_Table_ID = " + (int)TableTypeEnum.ActivityReason)
                   .Inverse();
        }
    }
}