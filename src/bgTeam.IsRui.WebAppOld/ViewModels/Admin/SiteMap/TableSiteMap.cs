﻿using bgTeam.IsRui.Domain;
using bgTeam.IsRui.WebApp.Helpers;
using System.ComponentModel;
using SiteMapNode = bgTeam.IsRui.Domain.Entities.Admin.SiteMapNode;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin.SiteMap
{
    public class TableSiteMap : NotificationBaseViewModel, ITree
    {
        private const string TitleCaption = "Наименование";
        private const string ControllerCaption = "Контроллер";
        private const string ActionCaption = "Действие";

        public TableSiteMap(SiteMapNode siteMapNode)
        {
            Id = siteMapNode.Id;
            Title = siteMapNode.Title;
            Controller = siteMapNode.Controller;
            Action = siteMapNode.Action;
            Url = siteMapNode.Url;
            ParentId = siteMapNode.ParentId;
        }

        public int Id { get; set; }

        [DisplayName(TitleCaption)]
        public string Title { get; set; }

        [DisplayName(ControllerCaption)]
        public string Controller { get; set; }

        [DisplayName(ActionCaption)]
        public string Action { get; set; }

        public string Url { get; set; }

        public int? ParentId { get; set; }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteSiteMap", "Admin", new { id = Id }, "Grid");
            }
        }

        public string EditLink
        {
            get
            {
                return LinkBuilder.PopupLink("Изменить", "SiteMapEdit", "Admin", new { id = Id });
            }
        }
    }
}