﻿using GB.Data.Plans;
using GB.Data.Workflow;
using System.Linq;
using System.Text;

namespace GB.MKRF.Logic.Status.Handler
{
    public class ExpertiseWorkflowHandler : WorkflowHandlerBase<ActivityExpertiseConclusion>
    {
        protected override bool CheckNewStatus(ActivityExpertiseConclusion e, Data.Workflow.Status status, out string errorMessage, out string confirmMessage)
        {
            confirmMessage = null;
            var sb = new StringBuilder();
            var se = (StatusEnum)status.Id;
            switch (se)
            {
                case StatusEnum.ExpAppointment:
                    break;

                case StatusEnum.ExpTechnician:
                    if (!e.Experts.Any(x => x.Role == ActivityExpertEnum.Technician))
                        sb.AppendLine("Необходимо назначить технического специалиста для экпертизы.");
                    if (!e.Experts.Any(x => x.Role == ActivityExpertEnum.Economist))
                        sb.AppendLine("Необходимо назначить экономиста для экпертизы.");
                    break;

                case StatusEnum.ExpEconomist:
                    break;

                case StatusEnum.ExpAgreed:
                    break;

                case StatusEnum.ActOnCompletion:
                    //TODO возможно надо проверить наличие сообщения от эксперта
                    break;

                case StatusEnum.ActAgreed:
                    break;

                default:
                    errorMessage = "Экспертиза не может иметь статус: " + se;
                    return false;
            }
            errorMessage = sb.ToString().TrimEnd('\r', '\n');
            return string.IsNullOrEmpty(errorMessage);
        }

        public override void ChangeAfter(ActivityExpertiseConclusion obj)
        {
        }
    }
}