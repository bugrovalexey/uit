﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServiceNPAs
{
    public class SetAOGovServiceForAOGovServicesNPACommand : ICommand<SetAOGovServiceForAOGovServicesNPAContext>
    {
        ISessionFactoryGb factory;

        public SetAOGovServiceForAOGovServicesNPACommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(SetAOGovServiceForAOGovServicesNPAContext context)
        {
            var aoGovServicesNpa = new Repository<AOGovServiceNPA>().GetAllEx(q => q.WhereRestrictionOn(x => x.Id).IsIn(context.AOGovServicesNpaIds));

            using (var session = factory.Create())
            {
                foreach (var item in aoGovServicesNpa)
                {
                    item.GovService = context.AOGovService;
                    session.SaveOrUpdate(item);
                }               

                session.Commit();
            }
        }
    }
}
