﻿namespace bgTeam.IsRui.WebApp.Controllers
{
    using System;
    using System.Web;
    using bgTeam.IsRui.WebApp.Helpers;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.DataAccess.Repository;
    using System.Web.Mvc;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.FileSystem;

    public class FilesController : Controller
    {
        //[HttpPost]
        public void Download(int? id)
        {
            if (!id.HasValue)
            {
                throw new ArgumentNullException(nameof(id));
            }

            //FileInfo info = FileInfo.FromFullUrl(context.Request.RawUrl);

            SendFile(this.HttpContext, id.Value);

            //return Content("ntcn");
        }

        private void SendFile(HttpContextBase context, int id)
        {
            string physicalPath = FileProvider.GetPhysicalPath(id);

            Logger.Debug("Попытка скачать файл - " + physicalPath);

            if (System.IO.File.Exists(physicalPath) == false)
            {
                //var errorTxt = string.Format("Файл не найден id={0} filename={1} path={2}",
                //    info.With(i => i.Id).Return(xid => xid.ToString(), "null"),
                //    info.Return(i => i.FileName, "null"),
                //    physicalPath);
                var errorTxt = physicalPath;
                Logger.Debug(errorTxt);
                ResponseHelper.FileNotFound(context.Response, errorTxt);
                return;
            }

            string fileName = GetFileName(id);// иначе получаем имя файла из БД                    

            //// Если имя не задано == файл не доступен
            //if (string.IsNullOrWhiteSpace(fileName))
            //{
            //    ResponseHelper.AccessDenied(context.Response,
            //        string.Format("Отказ в получении файла id={0}", info.Id.Return(id => info.Id.ToString(), "null")));
            //    return;
            //}

            Logger.Debug("Отправляем файл");

            FillResponse(context, physicalPath, fileName);
        }

        /// <summary>
        /// Возвращает имя файла из БД
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns>Если документ не доступен возвращает null</returns>
        private string GetFileName(object fileId)
        {
            return new RepositoryDc<Files>().SelectSingle<string>(q => q.Select(x => x.Name).Where(x => x.Id == (int)fileId));
        }

        /// <summary>
        /// Отсылает файл клиенту
        /// </summary>
        private void FillResponse(HttpContextBase context, string physicalFilePath, string fileName)
        {
            context.Response.Clear();
            context.Response.Buffer = false;
            context.Response.ContentType = "application/octet-stream";

            context.Response.AppendHeader("Content-Length", (new System.IO.FileInfo(physicalFilePath)).Length.ToString());
            context.Response.AppendHeader("Content-Transfer-Encoding", "binary");
            context.Response.AppendHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0, max-age=0");

            context.Response.AppendHeader(
                "Content-Disposition",
                string.Format("attachment; filename=\"{0}\";", fileName));

            context.Response.TransmitFile(physicalFilePath);
            context.Response.End();
        }
    }
}