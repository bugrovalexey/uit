﻿namespace bgTeam.IsRui.Common.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using bgTeam.Extensions;

    /// <summary>
    /// Утилиты для работы со строками
    /// </summary>
    public static class StringHelper
    {
        /// <summary>Строку в положительное число(0, если ошибка)</summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static int ToPositiveInt(this string s)
        {
            int i;
            return int.TryParse(s, out i) ? i : 0;
        }

        /// <summary>
        /// Объединение строк
        /// </summary>
        public static string Join(IList<string> items, string separator)
        {
            StringBuilder builder = new StringBuilder();
            bool notEmpty = false;

            if (items != null)
            {
                foreach (string s in items)
                {
                    if (string.IsNullOrEmpty(s))
                    {
                        continue;
                    }

                    if (notEmpty)
                    {
                        builder.Append(separator);
                    }
                    else
                    {
                        notEmpty = true;
                    }

                    builder.Append(s);
                }
            }

            return builder.ToString();
        }

        /// <summary>Производит отсечение длинных строк. </summary>
        /// <param name="sourceString">Исходная строка</param>
        /// <param name="maxChars">Точка отсечения</param>
        /// <returns></returns>
        public static string Trim(object sourceString, int maxChars)
        {
            if (sourceString == null)
            {
                return null;
            }

            string s = sourceString.ToString();

            if (s == null)
            {
                return s;
            }

            if (s.Length <= maxChars)
            {
                return s;
            }

            return s.Substring(0, maxChars) + "...";
        }

        /// <summary>
        /// Формирует список идентификторов из строки
        /// </summary>
        /// <param name="idList">Строка с идентификаторами</param>
        /// <returns>Список идентификаторов</returns>
        public static List<int> GetIds(object idList)
        {
            List<int> ids = new List<int>();

            var idListR = idList as string;

            if (idListR != null)
            {
                string value = ((string)idListR).Trim();
                if (string.IsNullOrEmpty(value))
                {
                    return ids;
                }

                int delimeterIndex = value.IndexOfAny(new char[] { ',', ';', ' ' });

                string[] splitted = (delimeterIndex > 0) ? value.Split(value[delimeterIndex]) : new string[] { value };
                int id;
                foreach (string idString in splitted)
                {
                    if (int.TryParse(idString, out id))
                    {
                        ids.Add(id);
                    }
                }
            }
            else
            {
                throw new NotSupportedException(idList.GetType().ToString());
            }

            return ids;
        }

        /// <summary>
        /// Преобразует строку значений, полученную от FileUploadControl, в Dictionary
        /// </summary>
        /// <param name="fileList">строка значений полученная от FileUploadControl</param>
        /// <returns></returns>
        public static Dictionary<string, string> FileListToDictionary(string fileList)
        {
            Dictionary<string, string> fileDict = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(fileList))
            {
                string[] fileItems = fileList.Split(';');
                foreach (string item in fileItems)
                {
                    string[] pair = item.Split(':');
                    if (pair.Length < 2)
                    {
                        continue;
                    }

                    string id = pair[0].Trim();
                    if (fileDict.ContainsKey(id))
                    {
                        continue;
                    }

                    fileDict.Add(id, pair[1].Trim());
                }
            }

            return fileDict;
        }

        /// <summary>
        /// Преобразование цифрового значения денег в строку
        /// </summary>
        /// <param name="value">Значение, которое нужно преобразовать</param>
        /// <param name="format"></param>
        /// <returns>Строковое представление значения денег, если value = null возвращается null</returns>
        public static string ToMoneyString(this object value, string format = "N2")
        {
            // Возможно добавление какого-нибудь постфикса к строке ( руб. и т.п.)
            return value.Return(
                v =>
            {
                if (value is decimal)
                {
                    return ((decimal)value).ToString(format);
                }

                if (value is double)
                {
                    return ((double)value).ToString(format);
                }

                throw new ArgumentException("ToMoneyString. Type of value not support.");
            }, default);
        }
    }
}