﻿using GB.Data.Workflow;

namespace GB.Data.Common
{
    public class AccessToEntity : EntityBase
    {
        public virtual EntityType EntityType { get; set; }

        public virtual AccessActionEnum AccessMask { get; set; }

        public virtual Role Role { get; set; }

        public virtual Status Status { get; set; }

        public virtual string Note { get; set; }
    }
}