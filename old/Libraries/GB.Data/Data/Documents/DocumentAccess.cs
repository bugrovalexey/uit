﻿using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Entities.Documents
{
    /// <summary>
    /// Доступ к документам
    /// </summary>
    public class DocumentAccess : EntityBase
    {
        /// <summary>Роль</summary>
        public virtual Role Role { get; set; }

        /// <summary>Документ</summary>
        public virtual Document Document { get; set; }
    }
}