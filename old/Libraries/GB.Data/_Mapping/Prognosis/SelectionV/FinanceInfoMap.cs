﻿using Uviri.MKRF.Entities.Prognosis;
using Uviri.MKRF.Entities.Prognosis.SelectionV;

namespace Uviri.MKRF.Mapping.Prognosis.SelectionV
{
    public class FinanceInfoMap : ClassMapBase<FinanceInfo>
    {
        public FinanceInfoMap()
            : base("MF5")
        {
//            Table("w_");

//            Id(x => x.Id, "MF5_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string field =
//                        @"@MF5_Prev_30_1 = ?
//                         ,@MF5_Cur_30_1 = ?
//                         ,@MF5_Prev_30_2 = ?
//                         ,@MF5_Cur_30_2 = ?
//                         ,@MF5_Prev_30_3 = ?
//                         ,@MF5_Cur_30_3 = ?
//                         ,@MF5_Prev_30_4 = ?
//                         ,@MF5_Cur_30_4 = ?
//                         ,@MF5_Prev_30_5 = ?
//                         ,@MF5_Cur_30_5 = ?
//                         ,@MF5_Prev_30_6 = ?
//                         ,@MF5_Cur_30_6 = ?
//                         ,@MF5_Prev_30_7 = ?
//                         ,@MF5_Cur_30_7 = ?
//                         ,@MF5_Prev_30_8 = ?
//                         ,@MF5_Cur_30_8 = ?
//                         ,@MF5_Prev_30_9 = ?
//                         ,@MF5_Cur_30_9 = ?
//                         ,@MF5_Prev_30_10 = ?
//                         ,@MF5_Cur_30_10 = ?
//                         ,@MF5_Prev_30_11 = ?
//                         ,@MF5_Cur_30_11 = ?
//                         ,@MF5_Prev_30_12 = ?
//                         ,@MF5_Cur_30_12 = ?
//                         ,@Mon_Form_ID = ?
//                         ,@MF5_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_MF5_Insert " + field).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF5_Update " + field).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF5_Delete
//						  @MF5_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.TotalAmount_Prv, "MF5_Prev_30_1");
            Map(x => x.TotalAmount_Cur, "MF5_Cur_30_1");
            Map(x => x.PurchaseHardware_Prv, "MF5_Prev_30_2");
            Map(x => x.PurchaseHardware_Cur, "MF5_Cur_30_2");
            Map(x => x.PurchaseSoftware_Prv, "MF5_Prev_30_3");
            Map(x => x.PurchaseSoftware_Cur, "MF5_Cur_30_3");
            Map(x => x.DevelopmentInformationSystems_Prv, "MF5_Prev_30_4");
            Map(x => x.DevelopmentInformationSystems_Cur, "MF5_Cur_30_4");
            Map(x => x.DevelopmentCommunicationSystems_Prv, "MF5_Prev_30_5");
            Map(x => x.DevelopmentCommunicationSystems_Cur, "MF5_Cur_30_5");
            Map(x => x.CertifiedTraining_Prv, "MF5_Prev_30_6");
            Map(x => x.CertifiedTraining_Cur, "MF5_Cur_30_6");
            Map(x => x.AccessInfoResources_Prv, "MF5_Prev_30_7");
            Map(x => x.AccessInfoResources_Cur, "MF5_Cur_30_7");
            Map(x => x.AmountInternet_Prv, "MF5_Prev_30_8");
            Map(x => x.AmountInternet_Cur, "MF5_Cur_30_8");
            Map(x => x.AmountFixedLine_Prv, "MF5_Prev_30_9");
            Map(x => x.AmountFixedLine_Cur, "MF5_Cur_30_9");
            Map(x => x.AmountMobilePhone_Prv, "MF5_Prev_30_10");
            Map(x => x.AmountMobilePhone_Cur, "MF5_Cur_30_10");
            Map(x => x.TechnicalSupport_Prv, "MF5_Prev_30_11");
            Map(x => x.TechnicalSupport_Cur, "MF5_Cur_30_11");
            Map(x => x.PurchaseConsumables_Prv, "MF5_Prev_30_12");
            Map(x => x.PurchaseConsumables_Cur, "MF5_Cur_30_12");

            References<Prognos>(x => x.Prognos, "Mon_Form_ID").Not.Nullable();
        }
    }
}
