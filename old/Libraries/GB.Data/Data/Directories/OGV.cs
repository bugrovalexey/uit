﻿namespace GB.Entity.Directories
{
    /// <summary>Орган государственной власти (ведомство)</summary>
    public class OGV : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null

        /// <summary>Краткое наименование</summary>
        public virtual string ShortName { get; set; }//null

        public virtual string Mnemo { get; set; }

        public virtual string OgvType { get; set; }

        public virtual string Inn { get; set; }

        public virtual string Ogrn { get; set; }

        public virtual string ParentOgvId { get; set; }

        //public virtual string OgvID { get; set; }

        public virtual string CodeName
        {
            get
            {
                return string.Format("{0} - {1}", ShortName, Name);
            }
        }
    }
}