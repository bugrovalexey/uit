﻿using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Entities.Statuses;
using GB.MKRF.Helpers;

namespace GB.MKRF.Mapping.Plans
{
    class ExpDemandMap : ClassMapBase<ExpDemand>
    {
        public ExpDemandMap()
            : base("Exp_Demand")
        {
            //            Table("v_");

            //            Id(x => x.Id, "Exp_Demand_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_Exp_Demand_Insert
            //						  @Exp_Demand_Num = ?
            //						 ,@Exp_Demand_Comm = ?
            //						 ,@Exp_Demand_Owner = ?
            //                         ,@Plans_ID = ?
            //						 ,@Status_ID = ?
            //                         ,@Exp_Demand_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Exp_Demand_Update
            //						  @Exp_Demand_Num = ?
            //						 ,@Exp_Demand_Comm = ?
            //						 ,@Exp_Demand_Owner = ?
            //                         ,@Plans_ID = ?
            //						 ,@Status_ID = ?
            //                         ,@Exp_Demand_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Exp_Demand_Delete
            //						  @Exp_Demand_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            //            Map(x => x.Num, "Exp_Demand_Num");
            //            Map(x => x.Comment, "Exp_Demand_Comm");
            Map(x => x.Owner, "Exp_Demand_Owner");
            //Map(x => x.SendMKS, "Exp_Demand_Send_MKC");
            //Map(x => x.ConclusionStatus, "Exp_Conclusion_Status").ReadOnly();
            //Map(x => x.SummaryConclusionId, "Exp_Summary_Conclusion_ID");

            References<Plan>(x => x.ExpObject, "Plans_ID").Not.Nullable();
            References<Status>(x => x.Status, "Status_ID").Not.Nullable();
            References<Status>(x => x.ReportByPlanStatus, "Status_Report_ID").Nullable();

            //References<ExpConclusion>(x => x.SummaryConclusion, "Exp_Summary_Conclusion_ID");
            //References<PlansStage>(x => x.Stage, "Plans_Stage_ID").Not.Nullable();

            HasMany<Files>(x => x.DocumentList).KeyColumns.Add("Document_Owner_ID")
                    .Where("Doc_Type_ID = " + (int)DocTypeEnum.demand_document)
                    .Where("Portal_Table_ID = " + (int)TableTypeEnum.Exp_Demand)
                    .Inverse();

            HasMany<Files>(x => x.DocumentInventoryList)
                    .KeyColumns.Add("Document_Owner_ID")
                    .Where("Doc_Type_ID = " + (int)DocTypeEnum.demand_inventory)
                    .Where("Portal_Table_ID = " + (int)TableTypeEnum.Exp_Demand)
                    .Inverse();

            //HasMany<ExpConclusion>(x => x.ConclusionList)
            //        .KeyColumns.Add("Exp_Demand_ID")
            //        .Inverse()
            //        .ApplyFilter<SecurityFilter>("Exp_Conclusion_ID in (select * from Get_Object_List(:UserId, " + (int)TableTypeEnum.Exp_Conclusion + ", 'r'))");

            //ApplyFilter<UserFilter>("Exp_Demand_ID in (select * from Get_Object_List(:UserId, " + (int)TableTypeEnum.Exp_Demand + ", 'r'))");
        }
    }
}