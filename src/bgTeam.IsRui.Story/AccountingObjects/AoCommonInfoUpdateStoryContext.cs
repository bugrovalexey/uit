﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    public class AoCommonInfoUpdateStoryContext
    {
        public int? Id { get; set; }

        public int? KindId { get; set; }

        public int? ArticleId { get; set; }

        public int? KindOfActivityId { get; set; }

        public string Name { get; set; }
    }
}
