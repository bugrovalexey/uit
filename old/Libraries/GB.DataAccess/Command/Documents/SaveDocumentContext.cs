﻿using GB.Data;
using GB.Data.Common;
using GB.Data.Documents;
using System.Collections.Generic;

namespace GB.DataAccess.Command.Documents
{
    public class SaveDocumentContext : ICommandContext
    {
        public string fileList { get; set; } 
        public IList<Files> listDocs { get; set; }
        public EntityBase owner { get; set; }
        public DocTypeEnum type { get; set; }
        internal SessionHibernate Session { get; set; }
    }
}
