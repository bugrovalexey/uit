﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="SystemSettings.aspx.cs" Inherits="GB.MKRF.Web.Administrator.SystemSettings" %>
<%@ Import Namespace="GB.MKRF.Entities.Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function saveValue() {
            var v;

            if (grid.cpType == '<%=OptionsTypeEnum.String%>')
                v = ValueTextBox.GetText();
            else if (grid.cpType == '<%=OptionsTypeEnum.Int%>')
                v = ValueSpin.GetText();
            else if (grid.cpType == '<%=OptionsTypeEnum.Bool%>')
                v = ValueCheckBox.GetChecked() ? 'true' : 'false';
            else if (grid.cpType == '<%=OptionsTypeEnum.Date%>')
                v = ValueDateEdit.GetDate();

            hf.Set('typ', grid.cpType);
            hf.Set('val', v);
        }
    </script>
    
    
    <h3>Системные настройки</h3>
    
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
    <dxwgv:ASPxGridView runat="server" ID="grid" ClientInstanceName="grid" KeyFieldName="Id"
        AutoGenerateColumns="False" OnBeforePerformDataSelect="grid_BeforePerformDataSelect"
        Width="100%" OnHtmlRowCreated="grid_HtmlRowCreated" 
        OnRowUpdating="grid_RowUpdating" oncustomjsproperties="grid_CustomJSProperties">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " VisibleIndex="0">
                <EditButton Visible="True">
                </EditButton>
                <ClearFilterButton Visible="True">
                </ClearFilterButton>
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Группа" VisibleIndex="1" FieldName="GroupName"
                ReadOnly="true" GroupIndex="1">
                <Settings AllowHeaderFilter="True" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Название" VisibleIndex="2" FieldName="Name"
                ReadOnly="true">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Значение" VisibleIndex="3" FieldName="Value">
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Settings ShowFilterRow="True" />
        <Templates>
            <EditForm>
                <table style="margin: 10px">
                    <tr>
                        <td class="userCapt" style="width: 200px">
                            Группа:
                        </td>
                        <td>
                            <%#Eval("GroupName")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="userCapt" style="padding-top: 20px; padding-bottom: 10px;">
                            <%#Eval("Name")%>:
                        </td>
                        <td style="padding-top: 20px; padding-bottom: 10px;">
                            <dxe:ASPxTextBox runat="server" ID="ValueTextBox" Visible="false" Width="400px" ClientInstanceName="ValueTextBox">
                            </dxe:ASPxTextBox>
                            <dxe:ASPxSpinEdit runat="server" ID="ValueSpin" Visible="false" Width="400px" ClientInstanceName="ValueSpin">
                            </dxe:ASPxSpinEdit>
                            <dxe:ASPxCheckBox runat="server" ID="ValueCheckBox" Visible="false" Width="400px" ClientInstanceName="ValueCheckBox">
                            </dxe:ASPxCheckBox>
                            <dxe:ASPxDateEdit runat="server" ID="ValueDateEdit" Visible="false" Width="400px" ClientInstanceName="ValueDateEdit">
                            </dxe:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td class="form_edit_buttons" >
                                        <span class="ok">
                                            <dxwgv:ASPxGridViewTemplateReplacement ID="ASPxGridViewTemplateReplacement1" runat="server"
                                                ReplacementType="EditFormUpdateButton">
                                            </dxwgv:ASPxGridViewTemplateReplacement>
                                        </span>
                                    </td>
                                    <td class="form_edit_buttons" >
                                        <span class="cancel">
                                            <dxwgv:ASPxGridViewTemplateReplacement ID="ASPxGridViewTemplateReplacement2" runat="server"
                                                ReplacementType="EditFormCancelButton">
                                            </dxwgv:ASPxGridViewTemplateReplacement>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') saveValue(); }" />
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsEditing Mode="EditForm" />
    </dxwgv:ASPxGridView>
   <%-- <br/>
    <h3>Настройки статусов</h3>
    <dxwgv:ASPxGridView runat="server" ID="statusGrid" KeyFieldName="Id"
        AutoGenerateColumns="False" OnBeforePerformDataSelect="statusGrid_BeforePerformDataSelect"
         OnRowUpdating="statusGrid_RowUpdating">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " VisibleIndex="0">
                <EditButton Visible="True">
                </EditButton>
                <ClearFilterButton Visible="True">
                </ClearFilterButton>
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Название" FieldName="Name"
                ReadOnly="true">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Время действия статуса, дней" FieldName="DaysCount" Width="200">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Группа" FieldName="StatusGroup.Name" GroupIndex="0">
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="20"></SettingsPager>
    </dxwgv:ASPxGridView>--%>
    
    <%--<h3>Этапы планирования</h3>
    <dxwgv:ASPxGridView runat="server" ID="TransitionPlansStageGrid" KeyFieldName="Id" ClientInstanceName="TransitionPlansStageGrid"
        AutoGenerateColumns="False" OnBeforePerformDataSelect="TransitionPlansStageGrid_BeforePerformDataSelect"
        OnRowUpdating="TransitionPlansStageGrid_RowUpdating" OnStartRowEditing="TransitionPlansStageGrid_StartRowEditing" >
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " VisibleIndex="0">
                <EditButton Visible="True">
                </EditButton>
                <ClearFilterButton Visible="True">
                </ClearFilterButton>
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Год плана информатизации" FieldName="Year">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataDateColumn Caption="Дата начала первого этапа" FieldName="FirstStageDate">
            </dxwgv:GridViewDataDateColumn>
            <dxwgv:GridViewDataDateColumn Caption="Дата начала второго этапа" FieldName="ChangeSecondStage">
            </dxwgv:GridViewDataDateColumn>
            <dxwgv:GridViewDataDateColumn Caption="Дата начала третьего этапа" FieldName="ChangeThirdStage" >
            </dxwgv:GridViewDataDateColumn>
        </Columns>
        <Templates>
            <EditForm>
                <table style="margin: 10px">
                    <tr>
                        <td class="userCapt" style="width: 200px">
                            Год плана информатизации:
                        </td>
                        <td>
                            <%#Eval("Year")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="userCapt" style="width: 200px">
                            Дата начала первого этапа:
                        </td>
                        <td>
                            <dxe:ASPxDateEdit runat="server" ID="ASPxDateEdit1" Date='<%#Bind("FirstStageDate") %>' />
                        </td>
                    </tr>

                    <tr>
                        <td class="userCapt" style="width: 200px">
                            Дата начала второго этапа:
                        </td>
                        <td>
                            <dxe:ASPxDateEdit runat="server" ID="ChangeSecondStageDateEdit" Date='<%#Bind("ChangeSecondStage") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="userCapt" style="width: 200px">
                            Дата начала третьего этапа:
                        </td>
                        <td>
                            <dxe:ASPxDateEdit runat="server" ID="ChangeThirdStageDateEdit" Date='<%#Bind("ChangeThirdStage") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <dxwgv:ASPxGridViewTemplateReplacement ID="ASPxGridViewTemplateReplacement1" runat="server"
                                            ReplacementType="EditFormUpdateButton">
                                        </dxwgv:ASPxGridViewTemplateReplacement>
                                    </td>
                                    <td>
                                        <dxwgv:ASPxGridViewTemplateReplacement ID="ASPxGridViewTemplateReplacement2" runat="server"
                                            ReplacementType="EditFormCancelButton">
                                        </dxwgv:ASPxGridViewTemplateReplacement>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="20"></SettingsPager>
    </dxwgv:ASPxGridView>
    
    <dxp:ASPxPanel ID="TestPanel" runat="server" Visible="false">
        <PanelCollection>
            <dxp:PanelContent>
                <h3>Тестирование автоматического перевода заявок</h3>
                <dxe:ASPxButton ID="TransitionTestButton" runat="server" AutoPostBack="false" Text="Запустить автоматический перевод заявок" Width="160px" OnClick="TransitionTestButton_Click" />
            </dxp:PanelContent>
        </PanelCollection>
    </dxp:ASPxPanel>--%>
    
</asp:Content>

