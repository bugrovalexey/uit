﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Мероприятия ОУ
    /// </summary>
    public class AoActivity : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Год</summary>
        public virtual short Year { get; set; }

        /// <summary>Стадия</summary>
        public virtual AccountingObjectsStageEnum Stage { get; set; }

        /// <summary>Тип</summary>
        public virtual Plans.ActivityTypeEnum ActivityType { get; set; }

        /// <summary>Планируемые расходы - Очередной финансовый год</summary>
        public virtual decimal CostY0 { get; set; }

        /// <summary>Планируемые расходы - Первый год планового периода</summary>
        public virtual decimal CostY1 { get; set; }

        /// <summary>Планируемые расходы - Второй год планового периода</summary>
        public virtual decimal CostY2 { get; set; }


        /// <summary>Мероприятие</summary>
        public virtual Plans.PlansActivity Activity { get; set; }

        /// <summary>ОУ</summary>
        public virtual AccountingObject AccountingObject { get; set; }

        public virtual IList<ActToAOActivity> ActToAOActivitys { get; set; }
    }
}
