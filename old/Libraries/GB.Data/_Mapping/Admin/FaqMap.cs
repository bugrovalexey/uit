﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Mapping.Admin
{
    public class FaqMap : ClassMapBase<Faq>
	{
        public FaqMap()
            : base("FAQ")
		{
//            Table("w_");

//            Id(x => x.Id, "FAQ_ID").GeneratedBy.Assigned();

			

//            SqlInsert(@"exec ZZZ_Prc_FAQ_Insert
//						  @FAQ_Question = ?
//						 ,@FAQ_Answer = ?
//						 ,@FAQ_Order = ?
//						 ,@FAQ_ID = ?").Check.None();
			
//            SqlUpdate(@"exec ZZZ_Prc_FAQ_Update
//						  @FAQ_Question = ?
//						 ,@FAQ_Answer = ?
//						 ,@FAQ_Order = ?
//						 ,@FAQ_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_FAQ_Delete
//						  @FAQ_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Question, "FAQ_Question");
			Map(x => x.Answer, "FAQ_Answer");
			Map(x => x.Order, "FAQ_Order");
        }
    }
}