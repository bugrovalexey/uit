﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class IKTObjectProvisionMap : ClassMapBase<IKTObjectProvision>
    {
        public IKTObjectProvisionMap()
            : base("IKT_Object_Provision")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "IKT_Object_Provision_Name");
            References(x => x.Type, "IKT_Object_Provision_Type_ID");
        }
    }
}