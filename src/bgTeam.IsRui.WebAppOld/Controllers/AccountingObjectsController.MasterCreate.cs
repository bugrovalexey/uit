﻿using AutoMapper;
using bgTeam.IsRui.WebApp.Controllers.From;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.GovContracts;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.ResponsiblePurchase;
using System.Web.Mvc;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.ActsToAOActivities;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.WorkAndGood;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.ActsOfAcceptance;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public partial class AccountingObjectsController
    {

        #region ResponsiblePurchase
        public ActionResult ResponsiblePurchase(int id)
        {
            return View(new ResponsiblePurchaseViewModel(id));
        }

        [HttpPost]
        public ActionResult ResponsiblePurchase(ResponsiblePurchaseFrom from)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                _Command.Build(Mapper.Map<SaveResponsiblePurchaseContext>(from)).Execute();
            });
        } 
        #endregion

        #region GovernmentContract

        public ActionResult GovernmentContract(int id)
        {
            return View(new GovernmentContractViewModel(id));
        }

        [HttpPost]
        public ActionResult GovernmentContractAdd(GovernmentContractAddFrom from)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var contract = _Command.Build(Mapper.Map<AddGovContractContext>(from)).Return<GovContract>();

                return new TableGovernmentContract(contract);
            });
        }

        public ActionResult GovernmentContractEdit(int id)
        {
            return View(new GovernmentContractEditViewModel(id));
        }

        [HttpPost]
        public ActionResult GovernmentContractEdit(SaveGovContractContext from)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var contract = _Command.Build(from).Return<GovContract>();
                
                return new TableGovernmentContract(contract);
            });
        }

        [HttpPost]
        public ActionResult GovernmentContractDelete(int id)
        {
            return NotificationHelper.NotificationDelete(id, () =>
            {
                _Command.Build(new DeleteGovContractContext(id)).Execute();
            });
        } 

        #endregion

        #region AdoptionBudget
        public ActionResult AdoptionBudget(int id)
        {
            return View(new AdoptionBudgetViewModel(id));
        }

        [HttpPost]
        public ActionResult AdoptionBudget(AddOrSaveAOAdoptionBudgetContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() => _Command.Build(context).Execute());
        } 
        #endregion

        public ActionResult FactualLocation(int id)
        {
            return View(new FactualLocationViewModel(id));
        }

        [HttpPost]
        public ActionResult FactualLocation(UpdateAOFactualLocationAccountingObjectContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() => { _Command.Build(context).Execute(); });
        }

        public ActionResult ActsOfAcceptance(int id)
        {
            return View(new ActsOfAcceptanceViewModel(id));
        }

        public ActionResult ActsOfAcceptanceEdit(int accountingObjectId, int? id)
        {
            return View(new ActsOfAcceptanceEditViewModel(accountingObjectId, id));
        }

        [HttpPost]
        public ActionResult ActsOfAcceptanceEdit(AddOrSaveActOfAcceptanceContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var entity = _Command.Build(context).Return<ActOfAcceptance>();
                var m = new TableActOfAcceptance(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteActOfAcceptance(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => _Command.Build(new DeleteActOfAcceptanceContext() { xId = id }).Execute());
        }

        [HttpPost]
        public ActionResult FormingBalanceCost(UpdateBalanceCostContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() => _Command.Build(context).Execute());
        }

        public ActionResult AoActivities(int actId, int aoId, ActivityTypeEnum type, string selectListCallback)
        {
            return View(new AoActivitiesViewModel(actId, aoId, type, selectListCallback));
        }

        [HttpPost]
        public ActionResult AddActToAOActivity(AddActToAOActivityContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var res = _Command.Build(context).Return<ActToAOActivity>();
                return new TableActToAoActivity(res);
            });
        }

        [HttpPost]
        public ActionResult DeleteActToAOActivity(DeleteActivityFromActContext context)
        {
            return NotificationHelper.NotificationDelete(context.Id, () => _Command.Build(context).Execute());
        }

        public ActionResult WorkAndGoodsEdit(int actsOfAcceptanceId, int accountingObjectId, int? id)
        {
            return View(new WorkAndGoodsEditViewModel(actsOfAcceptanceId, accountingObjectId, id));
        }

        [HttpPost]
        public ActionResult WorkAndGoodsEdit(AddOrSaveActToWorkAndGoodsContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var entity = _Command.Build(context).Return<WorkAndGoods>();
                var m = new TableWorkAndGoods(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteWorkAndGoods(int id)
        {            
            return NotificationHelper.NotificationDelete(id, () => _Command.Build(new DeleteActFromWorkAndGoodsContext() { xId = id }).Execute());
        }

    }
}