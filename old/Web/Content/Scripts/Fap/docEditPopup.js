﻿function OpenDocEditPopup(baseUrl, nRow, ownerId, type, deleteUrl, name) {
    var url = baseUrl + (baseUrl.indexOf('?') != -1 ? '&' : '?') + 'ownerId='+ownerId+'&type='+type+'&deleteUrl='+deleteUrl+'&refreshGrid='+name;
    if (nRow) {
        var tr = $(nRow);
        var id = tr.data('id');
        url += '&itemId=' + id;
    }
    popup.popover({
        url: url
    });
}