﻿using System.Collections.Generic;

namespace Uviri.MKRF.Entities.Admin
{
    /// <summary>
    ///  Данные описывающие вопросы анкеты
    /// </summary>
    public class QuestionnaireQuestion : EntityBase
    {
        public QuestionnaireQuestion() {}

        public QuestionnaireQuestion(Questionnaire parent)
        {
            this.Questionnaire = parent;
            this.Answers = new List<QuestionAnswer>();
        }

        /// <summary>
        /// Номер
        /// </summary>
        public virtual int Num { get; set; }

        /// <summary>
        /// Текст
        /// </summary>
        public virtual string Text { get; set; }
        
        /// <summary>
        /// Анкета
        /// </summary>
        public virtual Questionnaire Questionnaire { get; set; }

        /// <summary>
        /// Варианты ответов
        /// </summary>
        public virtual IList<QuestionAnswer> Answers { get; protected set; }
    }
}
