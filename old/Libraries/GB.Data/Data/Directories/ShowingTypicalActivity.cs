﻿using System.Collections.Generic;

namespace GB.MKRF.Entities.Directories
{
    /// <summary>
    /// Показатели типовой деятельности
    /// </summary>
    public class ShowingTypicalActivity : EntityBase
    {
        /// <summary>
        /// Наименование показателя
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Сфера деятельности
        /// </summary>
        public virtual TypicalActivities TypicalActivities { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public virtual MeasureUnit Measure { get; set; }

        public virtual IList<ActivityType2> ActivityTypes { get; set; }

        public virtual IList<IKTComponent> IKTComponents { get; set; }
    }
}