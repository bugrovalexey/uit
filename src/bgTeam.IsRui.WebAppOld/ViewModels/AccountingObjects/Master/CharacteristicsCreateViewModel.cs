﻿using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class CharacteristicsCreateViewModel : StepBaseViewModel
    {
        public CharacteristicsCreateViewModel(int id)
            : base(id, MasterItemEnum.CharacteristicsCreate)
        {
            Characteristics = new AccountingObjectCharacteristicsRepository()
                .GetAllByAccountingObject(id).Select(x => new
                TableCharacteristic(x));
        }

        public IEnumerable<TableCharacteristic> Characteristics { get; set; }
    }
}