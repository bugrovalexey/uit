﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using GB.Controls;

namespace GB.MKRF.Web.Administrator
{
    public partial class Logs : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            grid.DataBind();
            base.OnPreRender(e);
        }


        protected void grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var appSetting = Server.MapPath("~/Logs");

            List<string> files = new List<string>();
            if (!string.IsNullOrEmpty(appSetting))
            {
                foreach (var path in appSetting.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    files.AddRange(Directory.GetFiles(path));

            }

            grid.DataSource = files.Select(x => new
            {
                File = x.Substring(x.LastIndexOf('\\') + 1)
            }); ;
        }

        protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            var file = (string) e.Keys[0];
            if (string.IsNullOrEmpty(file))
                return;

            var appSetting = Server.MapPath("~/Logs");
            var fileName = Path.Combine(appSetting, file);
            if (File.Exists(fileName))
                File.Delete(fileName);

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (e.Parameters.StartsWith("delete"))
            {
                var appSetting = Server.MapPath("~/Logs");

                foreach (var file in Directory.GetFiles(appSetting))
                {
                    System.IO.File.Delete(file);
                }
            }
            (sender as ASPxGridView).GridCancelEdit( new CancelEventArgs());
        }
    }
}