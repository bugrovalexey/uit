﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    class TypeOfInterfaceMap : ClassMapHiLo<TypeOfInterface>
    {
        public TypeOfInterfaceMap()
            : base("Interface_Type")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Interface_Type_Name").Not.Nullable();
        }
    }
}
