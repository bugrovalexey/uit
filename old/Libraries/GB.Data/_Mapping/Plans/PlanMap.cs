﻿using GB.Data.Common;
using GB.Data.Directories;
using GB.Data.Plans;

namespace GB._Mapping.Plans
{
    internal class PlanMap : ClassMapHiLo<Plan>
    {
        public PlanMap()
            : base("Plans")
        {
        }

        protected override void InitMap()
        {
            //Map(x => x.PlanType, "Exp_Obj_Type_ID").CustomType(typeof(ExpObjectTypeEnum)).Not.Nullable();
            // Map(x => x.IsPublic, "Plans_Is_Public").Not.Nullable(); Map(x => x.PlanECP, "Plans_ECP");
            //
            // Map(x => x.VolLim0, "Plans_Vol_Lim_Y0"); Map(x => x.VolLim1, "Plans_Vol_Lim_Y1");
            // Map(x => x.VolLim2, "Plans_Vol_Lim_Y2"); Map(x => x.VolAddFB0,
            // "Plans_Add_Vol_FB_Y0"); Map(x => x.VolAddFB1, "Plans_Add_Vol_FB_Y1"); Map(x =>
            // x.VolAddFB2, "Plans_Add_Vol_FB_Y2");

            References<Years>(x => x.Year, "Year").Not.Nullable();
            References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
            References<User>(x => x.User, "Users_ID").Not.Nullable();
            References<Plan>(x => x.Parent, "Plans_Par_ID");

            //HasMany<ExpDemand>(Reveal.Member<Plan>("DemandList"))
            //    .KeyColumns.Add("Plans_ID")
            //    .Access.Field()
            //    .Inverse();
            //Зачем нужна данная строка
            //Она ведёт к некоректной работе, у плана всегда дожна быть звявка
            //.ApplyFilter<UserFilter>("Exp_Demand_ID in (select * from Get_Object_List(:UserId, " + (int)TableTypeEnum.Exp_Demand + ", 'r'))");

            HasMany(x => x.Activities)
                .KeyColumns.Add("Plans_ID")
                .Inverse();
            //.ApplyFilter<UserFilter>("Plans_Activity_ID in (select * from Get_Object_List(:UserId, " + (int)TableTypeEnum.Activity + ", 'r'))");

            //HasMany(x => x.PurchasePlans).KeyColumns.Add("Plans_ID").Inverse();

            //HasMany(x => x.PlansSchedules).KeyColumns.Add("Plans_ID").Inverse();
            //HasMany(x => x.ProjectMngSystems).KeyColumns.Add("Plans_ID").Inverse();
        }
    }
}