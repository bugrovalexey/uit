﻿using System.Collections.Generic;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxTreeList;

namespace GB.Controls
{
    public static class DevExpressExtension
    {
        public static void GridCancelEdit(this ASPxGridView grid, System.ComponentModel.CancelEventArgs e)
        {
            grid.CancelEdit();
            e.Cancel = true;

            grid.DataBind();
        }

        public static void TreeCancelEdit(this ASPxTreeList tree, System.ComponentModel.CancelEventArgs e)
        {
            tree.CancelEdit();
            e.Cancel = true;

            tree.DataBind();
        }

        public static IEnumerable<Tab> GetVisibleTabs(this ASPxTabControl tabControl)
        {
            foreach (Tab t in tabControl.Tabs)
            {
                if (t.Visible)
                    yield return t;
            }
        }

        public const string VALIDATION_NOT_NULL = "Данное поле не может быть пустым";

        public static void SetValidation(this ASPxEdit control)
        {
            SetValidation(control, VALIDATION_NOT_NULL);
        }

        public static void SetValidation(this ASPxEdit control, string errorText)
        {
            control.ValidationSettings.Display = Display.Dynamic;
            control.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.Text;
            control.ValidationSettings.ErrorFrameStyle.CssClass = "ErorField";

            control.ValidationSettings.RequiredField.IsRequired = true;
            control.ValidationSettings.RequiredField.ErrorText = errorText;
        }

        public static void SetValidationExpression(this ASPxEdit control, string regex, string errorText)
        {
            control.ValidationSettings.RegularExpression.ValidationExpression = regex;
            control.ValidationSettings.RegularExpression.ErrorText = errorText;
        }
    }
}
