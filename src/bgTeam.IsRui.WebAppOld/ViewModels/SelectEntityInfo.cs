﻿using bgTeam.IsRui.Domain;

namespace bgTeam.IsRui.WebApp.ViewModels
{
    public class SelectEntityInfo : IEntityName
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}