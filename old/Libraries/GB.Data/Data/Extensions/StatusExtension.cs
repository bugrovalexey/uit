﻿using GB.Data.AccountingObjects;
using GB.Data.Workflow;

namespace GB.Data.Extensions
{
    public static class StatusExtension
    {
        public static bool IsEq(this StatusEnum e, Status o)
        {
            return o.Id == (int)e;
        }

        public static bool IsEq(this StatusEnum e, IWorkflowObject o)
        {
            return o.Status.Id == (int)e;
        }

        public static bool IsEq(this Status s, StatusEnum e)
        {
            if (s == null)
                return false;

            return s.Id == (int)e;
        }

        public static bool IsEq(this Status s, params StatusEnum[] mass)
        {
            if (s == null)
                return false;

            foreach (var e in mass)
            {
                if (s.Id == (int)e)
                    return true;
            }

            return false;
        }

        public static bool IsEq(this Status s1, Status s2)
        {
            if (s1 != null && s2 != null)
                return s1.Id == s2.Id;
            return s1 == s2;
        }

        public static StatusEnum ToEnum(this Status s)
        {
            if (s == null)
                return StatusEnum.None;

            return (StatusEnum)s.Id;
        }
    }
}