﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Закупки
    /// </summary>
    public class GovPurchaseDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Номер закупки
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public GovPurchaseTypeEnum Type { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Дата публикации
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public decimal Price { get; set; }

        public decimal Summ { get; set; }

        public int IdZakupki360 { get; set; }
    }
}
