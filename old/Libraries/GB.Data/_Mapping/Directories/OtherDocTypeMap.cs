﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class OtherDocTypeMap : ClassMapBase<OtherDocType>
    {
        public OtherDocTypeMap()
            : base("Other_Doc_Type")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Other_Doc_Type_Name");
        }
    }
}
