﻿<%@ Page Language="C#" MasterPageFile="~/Projects/EditCard.Master" AutoEventWireup="true" CodeBehind="CommonData.aspx.cs" Inherits="GB.MKRF.Web.Projects.CommonData" %>

<%--Общие сведения--%>
<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function validateTextBox(s, e) {
            var cbValue = s.GetValue();
            e.isValid = (cbValue != undefined || cbValue) && cbValue != 'Не задано';
        }

        function UserSave() {
            hf.Set('InvolvedId', InvolvedComboBox.GetValue());
        }

        function FormSave() {
            hf.Set('Logo', FileUploader.GetFiles());
            hf.Set('FullName', NameTextBox.GetValue());
            hf.Set('ShortName', ShortNameTextBox.GetValue());
            hf.Set('DepartmentId', DepartmentComboBox.GetValue());
            hf.Set('Description', DescriptionTextBox.GetValue());
            hf.Set('Subdivision', SubdivisionTextBox.GetValue());
        }

        function SelectValue(e) {
            PopupControl.SetContentHtml('');
            switch (e) {
                case 'RU':
                    PopupControl.SetHeaderText('Выберите ответственного');
                    PopupControl.SetContentUrl('<%=GetResponsibleUrl() %>');
                    break;
                case 'SU':
                    PopupControl.SetHeaderText('Выберите подписывающего проект');
                    PopupControl.SetContentUrl('<%=GetSignerUrl() %>');
                    break;
            }

            PopupControl.RefreshContentUrl();
            PopupControl.Show();
        };

        function PopupClose(type, result) {
            PopupControl.Hide();
            var mass = result.split(';');
            if (mass[0]) hf.Set(type + 'Id', mass[0]);
            if (mass[1]) hf.Set(type + 'Name', mass[1]);
            var Memo = '#' + type + 'TextBox';
            $(Memo).find('input').val(hf.Get(type + 'Name'));
            UpdateButtonClear(type);
            ShowConfirm(true);

            PopupControl.SetContentHtml("");
        };

        function UpdateButtonClear(name) {
            var val = hf.Get(name + 'Id');

            if (val && val != -1) {
                var btName = '#' + name + 'TextBox';
                var bt = $(btName).parents('td.form_edit_input').find('div.ButtonClear');

                bt.css('display', '');
            }
        }

        function ClearValue(s, name) {
            if (hf.Contains(name + 'Id')) {
                ShowConfirm(true);
            }

            hf.Set(name + 'Id', '-1');
            hf.Set(name + 'Name', '');

            var Memo = '#' + name + 'TextBox';
            $(Memo).find('input').val('Не задано');

            $(s).css('display', 'none');
        };


        function ShowConfirm(isChange) {
            if (isChange) {
                window.onbeforeunload = function (e) {
                    return "Внесенные на странице данные не сохранены.";
                };
            } else {
                window.onbeforeunload = null;
            }
        }

        $(document).ready(function () {
            //ResponsibleUserTextBox.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });

            UpdateButtonClear('ResponsibleUser');
            UpdateButtonClear('Signer');
        });
    </script>

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" SyncWithServer="true" />
    <dxpc:ASPxPopupControl ID="PopupControl" runat="server" AllowDragging="True" AllowResize="True"
        CloseAction="CloseButton" Modal="True" EnableViewState="False"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="600px"
        ClientInstanceName="PopupControl" EnableHierarchyRecreation="True">
    </dxpc:ASPxPopupControl>

    <div class="panel_button">
        <dxe:ASPxButton ID="saveButton" runat="server" AutoPostBack="false" Text="Сохранить" ValidationGroup="CommonData" CssClass="fleft topButton save_button">
            <ClientSideEvents Click="function(s,e) { FormSave(); callBack.PerformCallback(''); MainLoadingPanel.Show(); ShowConfirm(false); }" />
        </dxe:ASPxButton>
    </div>
    <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" OnCallback="callBack_Callback">
        <ClientSideEvents
            CallbackComplete="function(s, e) { MainLoadingPanel.Hide(); }"
            CallbackError="function(s,e) { MainLoadingPanel.Hide(); }" />
    </dxc:ASPxCallback>

    <table class="form_edit">
        <tr>
            <td class="form_edit_desc">Логотип</td>
            <td class="form_edit_input">
                <cc:FileUploadControl2 runat="server" ID="FileUploader" ClientInstanceName="FileUploader"
                    AutoUpload="true" Mode="OneFile" AllowedFileExtensions=".jpg,.jpeg,.gif,.png"
                    OnDataBinding="FileUploader_DataBinding" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">Наименование</td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox ID="NameTextBox" runat="server" ClientInstanceName="NameTextBox" ClientIDMode="Static"
                    OnDataBinding="NameTextBox_DataBinding">
                </dxe:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc required">Краткое наименование</td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox ID="ShortNameTextBox" runat="server" ClientIDMode="Static" ClientInstanceName="ShortNameTextBox"
                    OnDataBinding="ShortNameTextBox_DataBinding">
                    <ValidationSettings ErrorDisplayMode="Text">
                        <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                    </ValidationSettings>
                </dxe:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">Описание</td>
            <td class="form_edit_input">
                <dxe:ASPxMemo ID="DescriptionTextBox" runat="server" ClientIDMode="Static" ClientInstanceName="DescriptionTextBox"
                    OnDataBinding="DescriptionTextBox_DataBinding">
                </dxe:ASPxMemo>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc required">Учреждение ответственное за проект</td>
            <td class="form_edit_input">
                <dxe:ASPxComboBox ID="DepartmentComboBox" runat="server" ClientInstanceName="DepartmentComboBox"
                    TextField="Name" ValueField="Id" ValueType="System.Int32"
                    OnDataBinding="DepartmentComboBox_DataBinding">
                    <ValidationSettings ErrorDisplayMode="Text">
                        <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                    </ValidationSettings>
                </dxe:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">Структурное подразделение, ответственное за проект</td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox ID="SubdivisionTextBox" runat="server" ClientIDMode="Static" ClientInstanceName="SubdivisionTextBox"
                    OnDataBinding="SubdivisionTextBox_DataBinding">
                </dxe:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc required">ФИО сотрудника подразделения, подписывающего проект</td>
            <td class="form_edit_input">
                <div style="position: relative">
                    <dxe:ASPxTextBox ID="SignerTextBox" runat="server" ClientInstanceName="SignerTextBox" ClientIDMode="Static"
                        OnDataBinding="SignerTextBox_DataBinding" ReadOnly="true">
                        <ClientSideEvents Validation="function(s,e){ validateTextBox(s,e); }" />
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('SU'); return false;"></div>
                    <div class="ButtonClear catalogButton" style="display: none;" onclick="ClearValue(this, 'Signer'); return false;"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc required">Сотрудник, ответственный за проект</td>
            <td class="form_edit_input">
                <div style="position: relative">
                    <dxe:ASPxTextBox ID="ResponsibleUserTextBox" runat="server" ClientInstanceName="ResponsibleUserTextBox" ClientIDMode="Static"
                        OnDataBinding="ResponsibleUserTextBox_DataBinding" ReadOnly="true">
                        <ClientSideEvents Validation="function(s,e){ validateTextBox(s,e); }" />
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('RU'); return false;"></div>
                    <div class="ButtonClear catalogButton" style="display: none;" onclick="ClearValue(this, 'ResponsibleUser'); return false;"></div>
                </div>
            </td>
        </tr>
    </table>

    <p class="header_grid">Сотрудники подразделения, участвующие в проекте</p>

    <dxe:ASPxButton ID="AddUserButton" runat="server"
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {UsersGrid.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="UsersGrid" ClientInstanceName="UsersGrid" Width="100%" KeyFieldName="Id"
        OnBeforePerformDataSelect="UsersGrid_BeforePerformDataSelect" OnRowInserting="UsersGrid_RowInserting" OnRowDeleting="UsersGrid_RowDeleting">
        <Settings ShowFilterRow="true" />
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False" AllowDragDrop="False" Width="60px">
                <DeleteButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="ФИО" FieldName="Name">
                <Settings AutoFilterCondition="Contains" AllowSort="True" SortMode="Value" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Id" ShowInCustomizationForm="False" Visible="False" />
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Участник проекта:</td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox runat="server" ID="InvolvedComboBox" ClientInstanceName="InvolvedComboBox"
                                TextField="FullName" ValueField="Id" ValueType="System.Int32" 
                                OnDataBinding="InvolvedComboBox_DataBinding">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') UserSave(); }" />
    </dxwgv:ASPxGridView>

</asp:Content>
