﻿using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Domain.Entities.Purchase;

namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    internal class PlansSpecMap : ClassMapHiLo<PlansSpec>
    {
        public PlansSpecMap()
            : base("Plans_Spec")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.EquipmentType, "Plans_Spec_Equip_Type");
            Map(x => x.Name, "Plans_Spec_Name");
            Map(x => x.Analog, "Plans_Spec_Analog");
            Map(x => x.Quantity, "Plans_Spec_Quantity");
            Map(x => x.Cost, "Plans_Spec_Cost").Not.Nullable(); ;
            Map(x => x.ExpenseVolumeFB, "Plans_Spec_Expense_Vol_FB").Not.Nullable();
            Map(x => x.ExpenseVolumeRB, "Plans_Spec_Expense_Vol_RB").Not.Nullable();
            Map(x => x.ExpenseVolumeVB, "Plans_Spec_Expense_Vol_VB").Not.Nullable();
            Map(x => x.SubEntry, "Plans_Spec_Subentry");
            Map(x => x.Comm, "Plans_Spec_Comm");
            Map(x => x.ExpenseVolumeFB_Fact, "Plans_Spec_Expense_Vol_FB_Fact").Not.Nullable();
            Map(x => x.ExpenseVolumeRB_Fact, "Plans_Spec_Expense_Vol_RB_Fact").Not.Nullable();
            Map(x => x.ExpenseVolumeVB_Fact, "Plans_Spec_Expense_Vol_VB_Fact").Not.Nullable();
            Map(x => x.Num, "Plans_Spec_Num");
            Map(x => x.OrigNum, "Plans_Spec_Orig_Num");

            Map(x => x.ExpenseVolFBY0, "Plans_Spec_P_Vol_FB_Y0");
            Map(x => x.ExpenseVolVBY0, "Plans_Spec_P_Vol_VB_Y0");
            Map(x => x.ExpenseAddVolFBY0, "Plans_Spec_P_Add_Vol_FB_Y0");
            Map(x => x.ExpenseAddVolVBY0, "Plans_Spec_P_Add_Vol_VB_Y0");
            Map(x => x.ExpenseVolFBY1, "Plans_Spec_P_Vol_FB_Y1");
            Map(x => x.ExpenseVolVBY1, "Plans_Spec_P_Vol_VB_Y1");
            Map(x => x.ExpenseAddVolFBY1, "Plans_Spec_P_Add_Vol_FB_Y1");
            Map(x => x.ExpenseAddVolVBY1, "Plans_Spec_P_Add_Vol_VB_Y1");
            Map(x => x.ExpenseVolFBY2, "Plans_Spec_P_Vol_FB_Y2");
            Map(x => x.ExpenseVolVBY2, "Plans_Spec_P_Vol_VB_Y2");
            Map(x => x.ExpenseAddVolFBY2, "Plans_Spec_P_Add_Vol_FB_Y2");
            Map(x => x.ExpenseAddVolVBY2, "Plans_Spec_P_Add_Vol_VB_Y2");

            Map(x => x.SupportPriceY0, "Plans_Spec_S_Vol_TP_Y0");
            Map(x => x.SupportPriceY1, "Plans_Spec_S_Vol_TP_Y1");
            Map(x => x.SupportPriceY2, "Plans_Spec_S_Vol_TP_Y2");
            Map(x => x.SupportPriceVBY0, "Plans_Spec_S_Vol_TP_VB_Y0");
            Map(x => x.SupportPriceVBY1, "Plans_Spec_S_Vol_TP_VB_Y1");
            Map(x => x.SupportPriceVBY2, "Plans_Spec_S_Vol_TP_VB_Y2");
            Map(x => x.QuantityY0, "Plans_Spec_S_Quantity_Y0");
            Map(x => x.QuantityY1, "Plans_Spec_S_Quantity_Y1");
            Map(x => x.QuantityY2, "Plans_Spec_S_Quantity_Y2");
            Map(x => x.QuantityVBY0, "Plans_Spec_S_Quantity_VB_Y0");
            Map(x => x.QuantityVBY1, "Plans_Spec_S_Quantity_VB_Y1");
            Map(x => x.QuantityVBY2, "Plans_Spec_S_Quantity_VB_Y2");

            Map(x => x.AgreedExpenseVolY0, "Plans_Spec_S_Vol_Y0");
            Map(x => x.AgreedExpenseVolY1, "Plans_Spec_S_Vol_Y1");
            Map(x => x.AgreedExpenseVolY2, "Plans_Spec_S_Vol_Y2");
            Map(x => x.AgreedExpenseVolVBY0, "Plans_Spec_S_Vol_VB_Y0");
            Map(x => x.AgreedExpenseVolVBY1, "Plans_Spec_S_Vol_VB_Y1");
            Map(x => x.AgreedExpenseVolVBY2, "Plans_Spec_S_Vol_VB_Y2");

            Map(x => x.ReestrNumber, "Plans_Spec_Reestr_Num");
            Map(x => x.DetailsActs, "Plans_Spec_Rekv_Act");
            Map(x => x.FactName, "Plans_Spec_Name_Fact");
            Map(x => x.FactCost, "Plans_Spec_Cost_Fact");
            Map(x => x.PRSContract, "Plans_Spec_PRS_Contract");
            Map(x => x.PRSContractor, "Plans_Spec_PRS_Contractor");

            Map(x => x.ExpenseVolumeCurrentYear, "Plans_Spec_Assgn_Vol_Y0");
            Map(x => x.ExpenseVolumeFirstYear, "Plans_Spec_Assgn_Vol_Y1");
            Map(x => x.ExpenseVolumeSecondYear, "Plans_Spec_Assgn_Vol_Y2");
            Map(x => x.AnalogPrice, "Plans_Spec_Analog_Price");
            Map(x => x.Comment32, "Plans_Spec_Comment");

            Map(x => x.ReportQuantity, "Plans_Spec_Report_Quantity");
            Map(x => x.ReportCost, "Plans_Spec_Report_Cost");

            Map(x => x.Year, "Year").CustomType(typeof(YearEnum)).Not.Nullable();
            Map(x => x.Duration, "Plans_Spec_Duration");

            References<PlansActivity>(x => x.PlanActivity, "Plans_Activity_ID").Not.Nullable();
            References<ExpenseDirection>(x => x.ExpenseDirection, "Expense_Direction_ID").Not.Nullable();
            References<GRBS>(x => x.GRBS, "GRBS_ID").Not.Nullable();
            References<OKPD>(x => x.OKPD, "OKDP_ID").Not.Nullable();
            References<SectionKBK>(x => x.SectionKBK, "Section_KBK_ID").Not.Nullable();
            References<ExpenseItem>(x => x.ExpenseItem, "Expense_Item_ID").Not.Nullable();
            References<WorkForm>(x => x.WorkForm, "Work_Form_ID").Not.Nullable();
            References<Contract>(x => x.Contract, "Contract_ID");

            //References<Qualifier>(x => x.Qualifier, "Qualifier_ID");

            References<ExpenseDirection>(x => x.ReportExpenseDirection, "Report_Expense_Direction_ID").Nullable();
            References<ExpenseType>(x => x.ExpenseType, "Expense_Type_ID").Not.Nullable();
            //его уже нет References<ExpenseType>(x => x.ReportExpenseType, "Report_Expense_Type_ID").Nullable();

            References<ExpenditureItem>(x => x.ExpenditureItem, "Expenditure_Items_ID");
            References<ProductGroup>(x => x.ProductGroup, "Product_Group_ID");

            HasMany<SpecType>(x => x.SpecType).KeyColumns.Add("Plans_Spec_ID").Inverse();

            HasMany<PlansSpecCharacteristic>(x => x.Characteristics).KeyColumns.Add("Plans_Spec_ID").Inverse().Cascade.AllDeleteOrphan();
        }
    }
}