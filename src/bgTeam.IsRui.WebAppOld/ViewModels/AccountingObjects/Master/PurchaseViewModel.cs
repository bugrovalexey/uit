﻿using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using System.Collections.Generic;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class PurchaseViewModel : StepBaseViewModel
    {
        public IEnumerable<TablePurchase> Purchases { get; set; }

        public PurchaseViewModel(int id)
            : base(id, MasterItemEnum.Purchase)
        {
            Purchases = new RepositoryDc<GovPurchase>().GetAllEx(query => query
                .Where(x => x.AccountingObject == Current))
                //.Where(x => x.Stage == type))
                .Select(x => new TablePurchase(x));
        }
    }
}