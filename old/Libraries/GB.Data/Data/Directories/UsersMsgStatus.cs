﻿namespace GB.MKRF.Entities.Directories
{
    /// <summary>Статус пользовательского сообщения</summary>
    public class UsersMsgStatus : EntityBase
    {
        /// <summary>Наименование статуса</summary>
        public virtual string Name { get; set; }//null
    }
}