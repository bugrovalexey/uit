﻿using System;
using System.Web;
using GB.Helpers;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Plans;
using GB.Repository;
//using Uviri.MKRF.Entities.OtherExpObjects;

namespace GB.MKRF.Web.Selectors
{
    public partial class PlanPaperDocuments : System.Web.UI.Page
    {
        public static string GetUrl(ExpDemandBase Demand)
        {
            return VirtualPathUtility.ToAbsolute("~/Selectors/PlanPaperDocuments.aspx") + 
                        "?" + 
                        ((Demand is ExpDemand) ? "Demand" : "id") + 
                        "=" + 
                        Demand.Id;
        }

        ExpDemandBase currentDemand = null;
        public ExpDemandBase CurrentDemand
        {
            get
            {
                int id = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["Demand"]) && int.TryParse(Request.QueryString["Demand"], out id))
                {
                    // Заявка на план
                    currentDemand = RepositoryBase<ExpDemand>.Get(id);
                }
                //else if (!string.IsNullOrEmpty(Request.QueryString["ObjDemand"]) && int.TryParse(Request.QueryString["ObjDemand"], out id))
                //{
                //    // Заявка на прочий ОЭ

                //    currentDemand = RepositoryBase<ExpObjectDemand>.Get(id);
                //}

                if (currentDemand == null || !Validation.Validator.Validate(currentDemand, ObjActionEnum.view))
                {
                    currentDemand = null;
                    ResponseHelper.DenyAccess(Response);
                }

                return currentDemand;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            dc.Demand = CurrentDemand;
            dc.DisplayedDocTypes = new DocTypeEnum[] { 
                DocTypeEnum.demand_document, 
                DocTypeEnum.demand_inventory,
                DocTypeEnum.PackageExpert,
                DocTypeEnum.PackageOGV,
                DocTypeEnum.PackageMKS
            };
            dc.NewDocumentDocType = DocTypeEnum.demand_document;
            base.OnInit(e);
        }
    }
}