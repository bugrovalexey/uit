﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
	public class ActivityLevelMap : ClassMapBase<ActivityLevel>
	{
        public ActivityLevelMap()
            : base("Activity_Level")
		{
//            Table("w_");

//            Id(x => x.Id, "Activity_Level_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            SqlInsert(@"exec ZZZ_Prc_Activity_Level_Insert
//						  @Activity_Level_Code = ?
//						 ,@Activity_Level_Name = ?
//						 ,@Activity_Level_ID = ?").Check.None();
			
//            SqlUpdate(@"exec ZZZ_Prc_Activity_Level_Update
//						  @Activity_Level_Code = ?
//						 ,@Activity_Level_Name = ?
//						 ,@Activity_Level_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Activity_Level_Delete
//						  @Activity_Level_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Code, "Activity_Level_Code").Nullable();
			Map(x => x.Name, "Activity_Level_Name").Nullable();
        }
    }
}