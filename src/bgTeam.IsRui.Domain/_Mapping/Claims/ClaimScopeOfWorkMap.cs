﻿namespace bgTeam.IsRui.Domain._Mapping.Claims
{
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class ClaimScopeOfWorkMap : ClassMapHiLo<ClaimScopeOfWork>
    {
        public ClaimScopeOfWorkMap()
            : base("ClaimScopeOfWork")
        {
        }

        protected override void InitMap()
        {
            References<Claim>(x => x.Claim, "ClaimScopeOfWork_Claim_ID").Not.Nullable();
            References<SpecialistCategoryType>(x => x.Specialist, "ClaimScopeOfWork_SpecialistId").Nullable();

            Map(x => x.WorkType, "ClaimScopeOfWork_WorkType").Nullable();
            Map(x => x.WorkHours, "ClaimScopeOfWork_WorkHours").Nullable();
            Map(x => x.Description, "ClaimScopeOfWork_Description").Nullable();
            Map(x => x.DateBegin, "ClaimScopeOfWork_DateBegin").Nullable();
            Map(x => x.DatePlanEnd, "ClaimScopeOfWork_DatePlanEnd").Nullable();
            Map(x => x.Cost, "ClaimScopeOfWork_Cost").Nullable();
            Map(x => x.LaborCostsHours, "ClaimScopeOfWork_LaborCostsHours").Nullable();
        }
    }
}
