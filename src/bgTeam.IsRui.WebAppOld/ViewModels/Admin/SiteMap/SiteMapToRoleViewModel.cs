﻿using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin.SiteMap
{
    public class SiteMapToRoleViewModel : ViewModelBase
    {
        public SiteMapToRoleViewModel()
        {
            SiteMapToRoles = new List<TableSiteMapToRole>();
        }

        public SiteMapToRoleViewModel(int siteMapId)
        {
            SiteMapId = siteMapId;
            var siteMap = new SiteMapService().Repository.GetExisting(siteMapId);
            SiteMapToRoles = siteMap.Roles.Select(ntr => new TableSiteMapToRole(ntr));
        }

        public int? SiteMapId { get; set; }

        public IEnumerable<TableSiteMapToRole> SiteMapToRoles { get; set; }
    }
}