﻿using Uviri.MKRF.Entities.Expertise;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpFormHelpMap : ClassMapBase<ExpFormHelp>
    {
        public ExpFormHelpMap()
            : base("Exp_Concl_Sprv")
        {
//            Table("w_");

//            Id(x => x.Id, "Exp_Concl_Sprv_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@Exp_Concl_Sprv_2_Comp = ? 
//						  ,@Exp_Concl_Sprv_2_Work = ?
//                          ,@Exp_Conclusion_ID = ?
//						  ,@Exp_Concl_Sprv_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Exp_Concl_Sprv_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Concl_Sprv_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Exp_Concl_Sprv_Delete @Exp_Concl_Sprv_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Component, "Exp_Concl_Sprv_2_Comp");
            Map(x => x.Work, "Exp_Concl_Sprv_2_Work");

            References<ExpConclusion>(x => x.Conclusion, "Exp_Conclusion_ID").Not.Nullable();
        }
    }
}
