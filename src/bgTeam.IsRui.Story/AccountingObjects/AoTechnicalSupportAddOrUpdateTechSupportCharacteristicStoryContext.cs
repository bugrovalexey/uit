﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStoryContext
    {
        public int? Id { get; set; }

        /// <summary>
        /// Техническое обеспечение ОУ
        /// </summary>
        public int? TechnicalSupportId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public int UnitId { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        public double? Value { get; set; }
    }
}
