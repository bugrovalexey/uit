﻿using GB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command
{
    public class StatusChangeContext : ICommandContext
    {
        public IWorkflowObject Object { get; set; }

        public Action<IWorkflowObject> funChangeAfter { get; set; }
    }
}
