﻿using System.Runtime.Serialization;

namespace Core.Import.ESKI
{
    /// <summary>
    /// Пользователь
    /// </summary>
    /// 
    [DataContract]
    public class User : IEntity
    {
        public string EntityName { get { return "User"; } }

        /// <summary>Идентификатор</summary>
        [DataMember(Order = 0)]
        public int Id { get; set; }

        /// <summary>Внешний идентификатор (ИД в ЕСКИ)</summary>
        [DataMember(Order = 1)]
        public string ExtId { get; set; }

        /// <summary>Логин</summary>
        [DataMember(Order = 2, IsRequired = true)]
        public string Login { get; set; }

        /// <summary>Признак активности</summary>
        [DataMember(Order = 3)]
        public bool IsActive { get; set; }

        /// <summary>Пароль (ХЭШ пароля)</summary>
        [DataMember(Order = 4)]
        public string Password { get; set; }

        /// <summary>Фамилия</summary>
        [DataMember(Order = 5)]
        public string Surname { get; set; }

        /// <summary>Имя</summary>
        [DataMember(Order = 6)]
        public string Name { get; set; }

        /// <summary>Отчество</summary>
        [DataMember(Order = 7)]
        public string Middlename { get; set; }

        /// <summary>Снилс</summary>
        [DataMember(Order = 8)]
        public string Snils { get; set; }

        /// <summary>Ид роли в АИС "Координация"</summary>
        [DataMember(Order = 9)]
        public int RoleID { get; set; }

        /// <summary>Ид ведомства</summary>
        [DataMember(Order = 10)]
        public int DepartmentID { get; set; }
    }
}