﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GoodsCreateOrUpdateGoodsStoryContext
    {
        public int? Id { get; set; }

        public int? ActivityId { get; set; }

        public string Name { get; set; }

        //public virtual int? OKPDId { get; set; }

        //public virtual int? ProductGroupId { get; set; }

        //public virtual int? ExpenseDirectionId { get; set; }

        //public virtual int? ExpenseTypeId { get; set; }

        //public virtual int? KOSGUId { get; set; }

        //public virtual YearEnum Year { get; set; }

        public int? Quantity { get; set; }

        //public virtual int? Duration { get; set; }

        public int? Cost { get; set; }

        public decimal? Summ { get; set; }

        public IList<GoodsCharacteristic> Characteristic { get; set; }

        public DateTime? ChangeDate { get; set; }

        public QuarterEnum Quarter { get; set; }

        public string AnalogName { get; set; }

        public decimal AnalogCost { get; set; }
    }
}
