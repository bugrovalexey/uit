﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CharacteristicsAddOrUpdateDto
    {
        public int? Id { get; set; }

        public int AoId { get; set; }

        public string Name { get; set; }

        public int? TypeId { get; set; }

        public int? UnitId { get; set; }

        public int? Norm { get; set; }

        public int? Max { get; set; }

        public int? Fact { get; set; }

        public DateTime? DateBeginning { get; set; }

        public DateTime? DateEnd { get; set; }
    }
}
