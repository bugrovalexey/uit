﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class IKTObjectStatusMap : ClassMapBase<IKTObjectStatus>
    {
        public IKTObjectStatusMap()
            : base("IKT_Object_Status")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "IKT_Object_Status_Name");
        }
    }
}