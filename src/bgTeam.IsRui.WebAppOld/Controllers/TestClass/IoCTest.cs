﻿using bgTeam.DataAccess;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.GovContracts;

namespace bgTeam.IsRui.WebApp.Controllers.TestClass
{

    public interface IoCTest
    {
        void Execute();
    }

    public class Test : IoCTest
    {
        ICommandBuilder _command;

        public Test(ICommandBuilder command)
        {
            _command = command;
        }

        public void Execute()
        {
            _command.Build(new DeleteGovContractContext(10)).Execute();
        }
    }

    public class Test2 : IoCTest
    {
        public ICommandBuilder _command { get; set; }

        public void Execute()
        {
            _command.Build(new DeleteGovContractContext(10)).Execute();
        }
    }
}