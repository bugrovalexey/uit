﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System.IO;

    public class DownloadFileDto
    {
        public int Id { get; set; }

        public string FileName { get; set; }

        public Stream FileData { get; set; }

        public string MimeType { get; set; }

        public string Url { get; set; }
    }
}
