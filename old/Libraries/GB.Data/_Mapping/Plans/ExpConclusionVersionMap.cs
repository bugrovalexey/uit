﻿using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;
using Uviri.MKRF.Entities.Statuses;
using Uviri.MKRF.Helpers;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ExpConclusionVersionMap : ClassMapBase<ExpConclusionVersion>
    {
        public ExpConclusionVersionMap()
            : base("Exp_Conclusion_Ver")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.ExpConclusionNum1, "Exp_Conclusion_Ver_Num1");
            Map(x => x.ExpConclusionNum2, "Exp_Conclusion_Ver_Num2");
            Map(x => x.ExpConclusionNum3, "Exp_Conclusion_Ver_Num3");
            Map(x => x.ExpConclusionNum4, "Exp_Conclusion_Ver_Num4");
            Map(x => x.ExpConclusionNum5, "Exp_Conclusion_Ver_Num5");

            Map(x => x.CustInfo, "Exp_Conclusion_Ver_Сust_Info");
            Map(x => x.GetDate, "Exp_Conclusion_Ver_Get_Date");
            Map(x => x.Conflict, "Exp_Conclusion_Ver_Conflict").Not.Nullable();
            Map(x => x.IsPublic, "Exp_Conclusion_Ver_Is_Public");

            Map(x => x.Completion, "Exp_Conclusion_Ver_Completion");

            Map(x => x.Recom, "Exp_Conclusion_Ver_Recom");
            Map(x => x.Decision, "Exp_Conclusion_Ver_Decision");

            Map(x => x.Agreement, "Exp_Conclusion_Ver_Agreement");
            Map(x => x.Additional, "Exp_Conclusion_Ver_Additional");
            Map(x => x.Reason, "Exp_Conclusion_Ver_Reason");
  
            References<Valuation>(x => x.Valuation, "Valuation_ID").Not.Nullable();
            References<ExpDemand>(x => x.Demand, "Exp_Demand_ID").Not.Nullable();
            References<User>(x => x.User, "Users_ID");
            References<Status>(x => x.Status, "Status_ID").Not.Nullable();
            References<ExpDocSufficiency>(x => x.ExpDocSufficiency, "Exp_Doc_Sufficiency_ID").Not.Nullable();
            References<ExpDocEquivalent>(x => x.ExpDocEquivalent, "Exp_Doc_Equivalent_ID").Not.Nullable();

            HasMany<ExpConclAct>(x => x.Acts).KeyColumns.Add("Exp_Conclusion_ID").Inverse();

            ApplyFilter<SecurityFilter>("Exp_Conclusion_ID in (select * from Get_Object_List(:UserId, " + (int)TableTypeEnum.Exp_Conclusion + ", 'r'))");
        }
    }
}