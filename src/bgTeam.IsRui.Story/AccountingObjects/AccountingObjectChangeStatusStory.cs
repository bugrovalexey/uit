﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Workflow;
    using bgTeam.IsRui.MKRF.Logic.Status;
    using System;
    using System.Threading.Tasks;

    public class AccountingObjectChangeStatusStory : IStory<AccountingObjectChangeStatusStoryContext, AccountingObjectChangeStatusDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IWorkflowHandler<AccountingObject> _handler;

        public AccountingObjectChangeStatusStory(
            IRepositoryRui repository,
            ISessionNHibernateFactory factory,
            IWorkflowHandler<AccountingObject> handler)
        {
            _repository = repository;
            _factory = factory;
            _handler = handler;
        }

        public AccountingObjectChangeStatusDto Execute(AccountingObjectChangeStatusStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<AccountingObjectChangeStatusDto> ExecuteAsync(AccountingObjectChangeStatusStoryContext context)
        {
            var ns = _repository.GetExisting<Status>(int.Parse(context.Status));
            var ao = _repository.GetExisting<AccountingObject>(context.Id);

            using (var session = _factory.OpenSession())
            {
                var woResult = await _handler.BeforeChange(ao, ns);
                if (woResult.Successful)
                {
                    ao.Status = ns;
                    await _handler.AfterChange(ao);
                    await session.UpdateAsync(ao);
                    return new AccountingObjectChangeStatusDto
                    {
                        Id = context.Id,
                        Type = context.Type,
                        Status = context.Status
                    };
                }
                else
                {
                    throw new InvalidOperationException(woResult.ErrorMessage);
                }
            }
        }
    }
}
