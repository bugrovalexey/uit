﻿namespace bgTeam.IsRui.Domain.Entities.Claims
{
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimAct : EntityBase
    {
        /// <summary>
        /// Заявка
        /// </summary>
        public virtual Claim Claim { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public virtual string DocName { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public virtual string DocNumber { get; set; }

        /// <summary>
        /// Дата добавления
        /// </summary>
        public virtual DateTime? DateAdoption { get; set; }

        /// <summary>
        /// Затраты
        /// </summary>
        public virtual decimal Cost { get; set; }

        /// <summary>
        /// Файл акта
        /// </summary>
        public virtual Files File { get; set; }
    }
}
