﻿namespace GB.Data
{
    public interface IAccess
    {
        AccessActionEnum? Access { get; set; }
    }
}