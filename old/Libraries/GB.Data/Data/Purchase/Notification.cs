﻿namespace GB.Entity.Directories.Zakupki
{
    /// <summary>Контракт</summary>
    public class Notification : EntityBase
    {
        /// <summary>номер уведомления</summary>
        public virtual string NotificationNumber { get; set; }

        /// <summary>максимальная сумма контракта</summary>
        public virtual decimal MaxSum { get; set; }
    }
}