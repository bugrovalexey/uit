﻿using GB.Extentions;
using DevExpress.Web.ASPxGridView;
using System;
using System.Linq;
using GB.MKRF.Controllers;
using GB.MKRF.Repository;
using GB.Controls;
using GB.Controls.Extensions;

namespace GB.MKRF.Web.Plans
{
    public partial class List : System.Web.UI.Page
    {
        protected string GetLinkToPlanCard(object Id, object Name)
        {
            return string.Format("<a class='gridLink' href='{0}'>{1}</a>", CommonData.GetUrl(Id), Name);
        }

        protected override void OnPreRender(EventArgs e)
        {
            Grid.DataBind();

            base.OnPreRender(e);
        }

        #region Grid

        private PlanController controller = new PlanController();

        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var data = controller.Repository.GetAllEx(q => q
                .Where(x => x.Department == UserRepository.GetCurrent().Department)
                .OrderBy(x => x.Department).Asc
                .OrderBy(x => x.Year).Asc
                .Fetch(f => f.Year).Eager
                .Fetch(f => f.Department).Eager.Clone())
            .Select(x => new
            {
                Id = x.Id,
                Department = x.Department.Name,
                Year = x.Year.Value
            });

            (sender as ASPxGridView).DataSource = data;
        }

        protected void Grid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void Grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var args = new PlanArgs()
            {
                Department = UserRepository.GetCurrent().Department,
                Year = hf["Year"]
            };
            controller.Create(args);

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void Grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var args = new PlanArgs()
            {
                Id = e.Keys[0],
                Year = hf["Year"]
            };
            controller.Update(args);

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void Grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            controller.Delete(new PlanArgs()
            {
                Id = e.Keys[0],
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void DepartmentTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(controller.CurrentEdit
                .Return(x => x.Department, UserRepository.GetCurrent().Department)
                .With(x => x.Name));
        }

        protected void YearComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(new YearRepository().GetYearsForPlan(controller.CurrentEdit))
                .SetIndexComboBox(controller.CurrentEdit.With(x => x.Year), 0);
        }

        #endregion Grid
    }
}