﻿using GB.Data.AccountingObjects;

namespace GB.DataAccess.Services.Args
{
    public class SoftwareArgs : BaseArgsNew, IWorkAndGoodsArgs
    {
        public int? AccountingObjectId { get; set; }

        public string Name { get; set; }

        public int? CategoryKindOfSupportId { get; set; }

        public int? ManufacturerId { get; set; }

        public string ManufacturerName { get; set; }

        public int Amount { get; set; }

        public decimal Summa { get; set; }

        public RightsOnSoftwareEnum Right { get; set; }

        #region Характеристики лицензии

        public int IdCharacteristicOfLicense { get; set; }
        
        public string NameCharacteristicOfLicense { get; set; }

        public string Model { get; set; }

        public TypeOfConnectionToServerEnum? TypeOfConnectionToServer { get; set; }

        public int? NumberOnlineSameTime { get; set; }

        public string ProductNumber { get; set; }

        public int? NumberAllocatedSameTime { get; set; }

        public string Licensor { get; set; }

        #endregion Характеристики лицензии
    }
}