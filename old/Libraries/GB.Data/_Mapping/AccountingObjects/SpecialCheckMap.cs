﻿using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.Documents;

namespace GB._Mapping.AccountingObjects
{
     class SpecialCheckMap : ClassMapHiLo<SpecialCheck>
    {
        public SpecialCheckMap()
            : base("OU_Special_Check")
        {
        }

        protected override void InitMap()
        {
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();

            Map(x => x.Name, "OU_Special_Check_Name").Not.Nullable();

            HasMany(x => x.Documents).KeyColumns.Add("Document_Owner_ID")
                .Where("Doc_Type_ID = " + (int)DocTypeEnum.SpecialCheck)
                .Where("Portal_Table_ID = " + (int)EntityType.SpecialCheck)
                .Inverse();
        }
    }
}
