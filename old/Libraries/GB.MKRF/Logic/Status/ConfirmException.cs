﻿using System;

namespace GB.MKRF.Logic.Status
{
    public class OperationConfirmException : Exception
    {
        public OperationConfirmException(string message)
            : base(message)
        {
        }
    }
}