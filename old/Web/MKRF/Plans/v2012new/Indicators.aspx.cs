﻿using System;
using System.Collections.Generic;
using System.Linq;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.Repository;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using GB.MKRF.Web.Helpers;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class Indicators : System.Web.UI.Page
    {
        PlansGoalIndicatorController controller = new PlansGoalIndicatorController();

        /// <summary>
        /// Разрешено редактировать мероприятие ?
        /// </summary>
        public bool EnableEdit
        {
            get
            {
                return (Master as ActivityCard).EnableEdit;
            }
        }
        
        /// <summary>
        /// Текущее мероприятие
        /// </summary>
        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as ActivityCard).CurrentActivity;
            }
        }

        public int BaseIndicatorYear
        {
            get
            {
                return CurrentActivity.Plan.Year.Value - 1;
            }
        }
        
        private bool islbNameCallback = false;

        public static int GetBaseIndicatorYear(PlansActivity activity)
        {
            return activity.Plan.Year.Value - 1;
        }
        

        protected override void OnPreRender(EventArgs e)
        {
            PlansActivity activity = CurrentActivity;

            IndexGrid.Columns["FirstPlanYear"].Caption = string.Format("Плановое значение на {0} год", BaseIndicatorYear + 1);
            IndexGrid.Columns["SecondPlanYear"].Caption = string.Format("Плановое значение на {0} год", BaseIndicatorYear + 2);
            IndexGrid.Columns["ThirdPlanYear"].Caption = string.Format("Плановое значение на {0} год", BaseIndicatorYear + 3);
            IndexGrid.DataBind();

            IndexGridAddButton.Visible = EnableEdit;
            IndexGrid.Columns["cmdColumn"].Visible = EnableEdit;


            IndicatorGrid.Columns["FirstPlanYear"].Caption = string.Format("Плановое значение на {0} год", BaseIndicatorYear + 1);
            IndicatorGrid.Columns["SecondPlanYear"].Caption = string.Format("Плановое значение на {0} год", BaseIndicatorYear + 2);
            IndicatorGrid.Columns["ThirdPlanYear"].Caption = string.Format("Плановое значение на {0} год", BaseIndicatorYear + 3);
            IndicatorGrid.DataBind();

            IndicatorGridAddButton.Visible = EnableEdit;
            IndicatorGrid.Columns["cmdColumn"].Visible = EnableEdit;

            base.OnPreRender(e);
        }
        
        protected void SetActivityFuncComboBoxValue(ASPxComboBox comboBox)
        {
            if (controller.CurrentEdit == null)
                return;

            var activityFunc = controller.CurrentEdit.Parent != null ? controller.CurrentEdit.Parent.ActivityFunc : controller.CurrentEdit.ActivityFunc;

            if (activityFunc == null)
                return;

            comboBox.Value = activityFunc.Id;
        }

        

        //private bool IsActivityIKT()
        //{
        //    return CurrentActivity.IKTComponent.Code.StartsWith("4") || CurrentActivity.IKTComponent.Code.StartsWith("3");
        //}

        protected List<string> GetIndicatorNameDictionary(string filter, int indicatorFeatureType, int funcId)
        {
            var result = new List<string>();

            var func = RepositoryBase<PlansActivityFunc>.Get(funcId);

            if (func == null)
            {
                result.AddRange(GetGovFunctionIndicatorsDictionary(indicatorFeatureType));
                result.AddRange(GetTypicalActivityDictionary(indicatorFeatureType));
            }
            else if (func.Type.Id == (int)FuncTypeEnum.AddFunc)
            {
                result.AddRange(GetTypicalActivityDictionary(indicatorFeatureType));
            }
            else if (func.Type.Id == (int)FuncTypeEnum.MainFunc)
            {
                result.AddRange(GetGovFunctionIndicatorsDictionary(indicatorFeatureType));
            }

            if (CurrentActivity.ActivityType2.Id == (int)ActivityType2Enum.Modern
                || CurrentActivity.ActivityType2.Id == (int)ActivityType2Enum.Exploitation
                || CurrentActivity.ActivityType2.Id == (int)ActivityType2Enum.Develop)
            {
                result.AddRange(GetIKTIndicatorsDictionary(indicatorFeatureType));
            }
            
            result = result.Where(x => x.ToLower().Contains(filter.ToLower().Trim())).OrderBy(x => x).ToList();

            return result;
        }

        private List<string> GetTypicalActivityDictionary(int indicatorFeatureType)
        {
            return indicatorFeatureType == 0
                       ? RepositoryBase<ShowingTypicalActivity>.GetAll().Where(x => (x.ActivityTypes.Count == 0 || x.ActivityTypes.Contains(CurrentActivity.ActivityType2))
                                                                                 && (x.IKTComponents.Count == 0 || x.IKTComponents.Contains(CurrentActivity.IKTComponent)))
                                                                        .Select(x => x.Name).ToList()
                       : RepositoryBase<IndicatorTypicalActivity>.GetAll().Where(x => (x.ActivityTypes.Count == 0 || x.ActivityTypes.Contains(CurrentActivity.ActivityType2))
                                                                                 && (x.IKTComponents.Count == 0 || x.IKTComponents.Contains(CurrentActivity.IKTComponent)))
                                                                          .Select(x => x.Name).ToList();
        }

        private List<string> GetGovFunctionIndicatorsDictionary(int indicatorFeatureType)
        {
            return indicatorFeatureType == 0
                       ? RepositoryBase<TypingShowingGovFunction>.GetAll().Where(x => (x.ActivityTypes.Count == 0 || x.ActivityTypes.Contains(CurrentActivity.ActivityType2))
                                                                                 && (x.IKTComponents.Count == 0 || x.IKTComponents.Contains(CurrentActivity.IKTComponent)))
                                                                          .Select(x => x.Name).ToList()
                       : RepositoryBase<TypingIndicatorGovFunction>.GetAll().Where(x => (x.ActivityTypes.Count == 0 || x.ActivityTypes.Contains(CurrentActivity.ActivityType2))
                                                                                 && (x.IKTComponents.Count == 0 || x.IKTComponents.Contains(CurrentActivity.IKTComponent)))
                                                                          .Select(x => x.Name).ToList();
        }

        private List<string> GetIKTIndicatorsDictionary(int indicatorFeatureType)
        {
            return indicatorFeatureType == 0
                       ? RepositoryBase<ShowingIKT>.GetAll().Where(x => (x.ActivityTypes.Count == 0 || x.ActivityTypes.Contains(CurrentActivity.ActivityType2))
                                                                                 && (x.IKTComponents.Count == 0 || x.IKTComponents.Contains(CurrentActivity.IKTComponent)))
                                                                          .Select(x => x.Name).ToList()
                       : RepositoryBase<IndicationIKT>.GetAll().Where(x => (x.ActivityTypes.Count == 0 || x.ActivityTypes.Contains(CurrentActivity.ActivityType2))
                                                                                 && (x.IKTComponents.Count == 0 || x.IKTComponents.Contains(CurrentActivity.IKTComponent)))
                                                                          .Select(x => x.Name).ToList();
        }
        
        protected void IndexGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var indexGrid = (sender as ASPxGridView);
            indexGrid.DataSource = controller.GetIndicatorDataSource(CurrentActivity, 0);
        }

        protected void IndexGrid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void IndexGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            var baseIndicator = controller.Create(new PlansGoalIndicatorArgs()
            {
                ActivityId = CurrentActivity.Id,
                Year = BaseIndicatorYear,
                FeatureType = 0,
                Name = hfGR["Name"],
                Uom = hfGR["UOM"],
                FactVal = hfGR["BaseValue"],
                ActivityFuncId = hfGR["GIActivityFunc"],
                GoalId = hfGR["GIGoal"]
            });

            //firstGoalIndicator
            controller.Create(new PlansGoalIndicatorArgs()
            {
                ActivityId = CurrentActivity.Id,
                ParentId = baseIndicator.Id,
                Year = BaseIndicatorYear+1,
                FeatureType = 0,
                FactVal = hfGR["FirstPlanValue"],
                AddFounding = hfGR["FirstPlanAddValue"]
            });

            //secondGoalIndicator
            controller.Create(new PlansGoalIndicatorArgs()
            {
                ActivityId = CurrentActivity.Id,
                ParentId = baseIndicator.Id,
                Year = BaseIndicatorYear + 2,
                FeatureType = 0,
                FactVal = hfGR["SecondPlanValue"],
                AddFounding = hfGR["SecondPlanAddValue"]
            });

            //thirdGoalIndicator
            controller.Create(new PlansGoalIndicatorArgs()
            {
                ActivityId = CurrentActivity.Id,
                ParentId = baseIndicator.Id,
                Year = BaseIndicatorYear + 3,
                FeatureType = 0,
                FactVal = hfGR["ThirdPlanValue"],
                AddFounding = hfGR["ThirdPlanAddValue"]
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void IndexGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            var baseIndicator = controller.Update(new PlansGoalIndicatorArgs()
            {
                Id = e.Keys[0],
                Name = hfGR["Name"],
                Uom = hfGR["UOM"],
                FactVal = hfGR["BaseValue"],
                ActivityFuncId = hfGR["GIActivityFunc"],
                GoalId = hfGR["GIGoal"]
            });

            //firstGoalIndicator
            controller.Update(new PlansGoalIndicatorArgs()
            {
                Id = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, baseIndicator, 1).Id,
                FactVal = hfGR["FirstPlanValue"],
                AddFounding = hfGR["FirstPlanAddValue"]
            });

            //secondGoalIndicator
            controller.Update(new PlansGoalIndicatorArgs()
            {
                Id = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, baseIndicator, 2).Id,
                FactVal = hfGR["SecondPlanValue"],
                AddFounding = hfGR["SecondPlanAddValue"]
            });

            //thirdGoalIndicator
            controller.Update(new PlansGoalIndicatorArgs()
            {
                Id = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, baseIndicator, 3).Id,
                FactVal = hfGR["ThirdPlanValue"],
                AddFounding = hfGR["ThirdPlanAddValue"]
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void IndexGrid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            controller.Delete(new PlansGoalIndicatorArgs()
            {
                Id = e.Keys[0],
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }


        protected void IndicatorGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var indexGrid = (sender as ASPxGridView);
            indexGrid.DataSource = controller.GetIndicatorDataSource(CurrentActivity, 1);
        }

        protected void IndicatorGrid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void IndicatorGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            var baseIndicator = controller.Create(new PlansGoalIndicatorArgs()
            {
                ActivityId = CurrentActivity.Id,
                Year = BaseIndicatorYear,
                FeatureType = 1,
                Name = hfGR["Name"],
                Uom = "%",
                Algorythm = hfGR["Algorythm"],
                ActivityFuncId = hfGR["GIActivityFunc"]
            });

            //firstGoalIndicator
            controller.Create(new PlansGoalIndicatorArgs()
            {
                ActivityId = CurrentActivity.Id,
                ParentId = baseIndicator.Id,
                Year = BaseIndicatorYear + 1,
                FeatureType = 1,
                Uom = "%",
                FactVal = hfGR["FirstPlanValue"],
                AddFounding = hfGR["FirstPlanAddValue"]
            });


            //secondGoalIndicator
            controller.Create(new PlansGoalIndicatorArgs()
            {
                ActivityId = CurrentActivity.Id,
                ParentId = baseIndicator.Id,
                Year = BaseIndicatorYear + 2,
                FeatureType = 1,
                Uom = "%",
                FactVal = hfGR["SecondPlanValue"],
                AddFounding = hfGR["SecondPlanAddValue"]
            });

            //thirdGoalIndicator
            controller.Create(new PlansGoalIndicatorArgs()
            {
                ActivityId = CurrentActivity.Id,
                ParentId = baseIndicator.Id,
                Year = BaseIndicatorYear + 3,
                FeatureType = 1,
                Uom = "%",
                FactVal = hfGR["ThirdPlanValue"],
                AddFounding = hfGR["ThirdPlanAddValue"]
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void IndicatorGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            var baseIndicator = controller.Update(new PlansGoalIndicatorArgs()
            {
                Id = e.Keys[0],
                Name = hfGR["Name"],
                Uom = "%",
                Algorythm = hfGR["Algorythm"],
                ActivityFuncId = hfGR["GIActivityFunc"],
            });


            //firstGoalIndicator
            controller.Update(new PlansGoalIndicatorArgs()
            {
                Id = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, baseIndicator, 1).Id,
                Uom = "%",
                FactVal = hfGR["FirstPlanValue"],
                AddFounding = hfGR["FirstPlanAddValue"]
            });

            //secondGoalIndicator
            controller.Update(new PlansGoalIndicatorArgs()
            {
                Id = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, baseIndicator, 2).Id,
                Uom = "%",
                FactVal = hfGR["SecondPlanValue"],
                AddFounding = hfGR["SecondPlanAddValue"]
            });

            //thirdGoalIndicator
            controller.Update(new PlansGoalIndicatorArgs()
            {
                Id = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, baseIndicator, 3).Id,
                Uom = "%",
                FactVal = hfGR["ThirdPlanValue"],
                AddFounding = hfGR["ThirdPlanAddValue"]
            });


            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void IndicatorGrid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            controller.Delete(new PlansGoalIndicatorArgs()
            {
                Id = e.Keys[0],
            });
            (sender as ASPxGridView).GridCancelEdit(e);
        }
        
        protected void ActivityFuncComboBox_DataBinding(object sender, EventArgs e)
        {
            var comboBox = (sender as ASPxComboBox);
            comboBox.DataSource = controller.GetAllIndicators(CurrentActivity, false, false);
        }

        protected void ActivityFuncForIndicatorsComboBox_DataBinding(object sender, EventArgs e)
        {
            var comboBox = (sender as ASPxComboBox);
            comboBox.DataSource = controller.GetAllIndicators(CurrentActivity, true, false); 
        }

        protected void ActivityFuncComboBox_DataBound(object sender, EventArgs e)
        {
            SetActivityFuncComboBoxValue(sender as ASPxComboBox);
        }

        protected void ddeName_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            (sender as ASPxDropDownEdit).Text = controller.CurrentEdit.Name;
        }

        protected void lbName_DataBinding(object sender, EventArgs e)
        {
            if (!islbNameCallback)
            {
                (sender as ASPxListBox).DataSource = GetIndicatorNameDictionary("", 0, 0);
            }
        }

        protected void lbName_Callback(object sender, CallbackEventArgsBase e)
        {
            islbNameCallback = true;
            var param = e.Parameter.Split('|');
            var filterText = param[0];
            var funcId = param[1].Equals("null") ? 0 : Convert.ToInt32(param[1]);

            var box = (sender as ASPxListBox);
            box.DataSource = GetIndicatorNameDictionary(filterText, 0, funcId);
            box.DataBind();
        }

        protected void lbNameForIndicators_DataBinding(object sender, EventArgs e)
        {
            if (!islbNameCallback)
            {
                (sender as ASPxListBox).DataSource = GetIndicatorNameDictionary("", 1, 0);
            }
        }

        protected void lbNameForIndicators_Callback(object sender, CallbackEventArgsBase e)
        {
            islbNameCallback = true;
            var param = e.Parameter.Split('|');
            var filterText = param[0];
            var funcId = param[1].Equals("null") ? 0 : Convert.ToInt32(param[1]);

            var box = (sender as ASPxListBox);
            box.DataSource = GetIndicatorNameDictionary(filterText, 1, funcId);
            box.DataBind();
        }

        protected void GoalComboBox_DataBinding(object sender, EventArgs e)
        {
            var comboBox = (sender as ASPxComboBox);
            var list = new List<object> { new { Id = 0, Name = "Не задано" } };
            if (CurrentActivity != null)
            {
                var goals = RepositoryBase<PlansActivityGoal>.GetAll(x => x.PlansActivity != null && x.PlansActivity.Id == CurrentActivity.Id)
                    .Select(
                        item => new
                        {
                            Id = item.Id,
                            Name = item.Name.Trim()
                        });

                list.AddRange(goals);
            }
            comboBox.DataSource = list;
        }

        protected void GoalComboBox_DataBound(object sender, EventArgs e)
        {
            var comboBox = (sender as ASPxComboBox);
            comboBox.Value = 0;
            if (controller.CurrentEdit == null || controller.CurrentEdit.Goal == null)
                return;

            comboBox.Value = controller.CurrentEdit.Goal.Id;
        }

        protected void DescTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            (sender as ASPxTextBox).Text = controller.CurrentEdit.UOM;
        }

        protected void BaseValueTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            (sender as ASPxTextBox).Text = controller.CurrentEdit.PRSFactVal;
        }

        protected void FirstPlanValueTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            var planGI = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, controller.CurrentEdit, 1);

            (sender as ASPxTextBox).Text = planGI != null ? planGI.PRSFactVal : string.Empty;
        }

        protected void FirstPlanAddValueTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            var planGI = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, controller.CurrentEdit, 1);

            (sender as ASPxTextBox).Text = planGI != null ? planGI.AddFounding : string.Empty;
        }

        protected void SecondPlanValueTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            var planGI = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, controller.CurrentEdit, 2);

            (sender as ASPxTextBox).Text = planGI != null ? planGI.PRSFactVal : string.Empty;
        }

        protected void SecondPlanAddValueTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            var planGI = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, controller.CurrentEdit, 2);

            (sender as ASPxTextBox).Text = planGI != null ? planGI.AddFounding : string.Empty;
        }

        protected void ThirdPlanValueTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            var planGI = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, controller.CurrentEdit, 3);

            (sender as ASPxTextBox).Text = planGI != null ? planGI.PRSFactVal : string.Empty;
        }

        protected void ThirdPlanAddValueTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            var planGI = PlansGoalIndicatorController.GetPlanGoalIndicator(CurrentActivity, controller.CurrentEdit, 3);

            (sender as ASPxTextBox).Text = planGI != null ? planGI.AddFounding : string.Empty;
        }

        protected void AlgorythmTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            (sender as ASPxTextBox).Text = controller.CurrentEdit.Algorythm;
        }

        public static string GetUrl(int activityId)
        {
            return UrlHelper.Resolve("~/Plans/v2012new/Indicators.aspx?ActivityId=" + activityId.ToString());
        }

        public static PlansGoalIndicator GetPlanGoalIndicator(PlansGoalIndicator parent, int planYear)
        {
            return PlansGoalIndicatorController.GetPlanGoalIndicator(parent.PlanActivity, parent, planYear);
        }
    }
}