﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="CatalogItems.aspx.cs" Inherits="GB.MKRF.Web.Selectors.CatalogItems" %>
<asp:Content ID="Head" ContentPlaceHolderID="Head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
     <script type="text/javascript">
         function rowClick(s, e) {
             
             var id = s.GetRowKey(e.visibleIndex);
             s.GetRowValues(e.visibleIndex, 'Id;Code;Name', function (values) {
                 var result = id + ';' + values[1] + ';' + values[2];
                 
                 window.parent.CatalogPopupClose('<% =Request.QueryString["type"] %>', result);
             });
         }
    </script>
    <dxwgv:ASPxGridView ID="CatalogItemsGrid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" KeyFieldName="Id"
        OnBeforePerformDataSelect="CatalogItemsGrid_BeforePerformDataSelect" Width="98%" >
        <Columns>
            <dxwgv:GridViewDataTextColumn Caption="Код" FieldName="Code" Width="100px">
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>            
        </Columns>        
        <Styles>
            <AlternatingRow Enabled="true" />
            <Header Wrap="True" />
        </Styles> 
        <ClientSideEvents RowClick="rowClick" />
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="6" />
    </dxwgv:ASPxGridView>
</asp:Content>
