﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class PurchasePlanMap : ClassMapBase<PurchasePlan>
    {
        public PurchasePlanMap()
            : base("Plan_Purchase")
        {
//            Table("w_");

//            Id(x => x.Id, "Plan_Purchase_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_Plan_Purchase_Insert
//                          @Plan_Purchase_Num = ?
//                         ,@Plan_Purchase_Work = ?
//                         ,@Plan_Purchase_Status = ?
//                         ,@Plan_Purchase_Vol = ?
//                         ,@Plans_ID = ?
//                         ,@Plan_Purchase_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Plan_Purchase_Update
//                          @Plan_Purchase_Num = ?
//                         ,@Plan_Purchase_Work = ?
//                         ,@Plan_Purchase_Status = ?
//                         ,@Plan_Purchase_Vol = ?
//                         ,@Plans_ID = ?
//                         ,@Plan_Purchase_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Plan_Purchase_Delete
//						  @Plan_Purchase_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "Plan_Purchase_Num");
            Map(x => x.Name, "Plan_Purchase_Work");
            Map(x => x.State, "Plan_Purchase_Status");
            Map(x => x.AmountFunding, "Plan_Purchase_Vol");

            References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();

            HasMany(x => x.PurchasePlanActivity).KeyColumns.Add("Plan_Purchase_ID").Inverse();
        }
    }
}
