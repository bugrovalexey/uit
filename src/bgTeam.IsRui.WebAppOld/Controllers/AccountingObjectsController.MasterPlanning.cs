﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Frgu;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.Purchase;
using bgTeam.IsRui.DataAccess.Command;
using bgTeam.IsRui.DataAccess.Repository;
using NHibernate.Transform;
using System.Linq;
using System.Web.Mvc;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.AOGovServices;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.AOGovServiceNPAs;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.InternalIsAndComponentItkis;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.ExternalIsAndComponentItkis;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.ExternalUserInterfaceOfIss;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public partial class AccountingObjectsController
    {
        public ActionResult CommonDataCreate(int? id)
        {
            return View(new CommonDataCreateViewModel(id));
        }

        [HttpPost]
        public ActionResult CommonDataCreate(AddOrSaveAccountingObjectContext args)
        {
            return
                NotificationHelper.NotificationCreateEdit(() => 
                {
                    var res = _Command.Build(args).Return<AccountingObject>();

                    return new AccountingObjectNotificationViewModel(res);
                });
        }

        public ActionResult DocumentBase(int id)
        {
            return View(new DocumentBaseViewModel(id));
        }

        public ActionResult ResponsibleService(int id)
        {
            return View(new ResponsibleServiceViewModel(_userRepository, id));
        }

        [HttpPost]
        public ActionResult ResponsibleServiceEdit(UpdateResponsibleServiceAccountingObjectContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() => _Command.Build(context).Execute());
        }

        #region SpecialActivity

        public ActionResult SpecialActivity(int id)
        {
            return View(new SpecialActivityViewModel(id));
        }

        [HttpPost]
        public ActionResult SpecialActivity(UpdateSpecialActivityAccountingObjectContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() => _Command.Build(context).Execute());
        }

        public ActionResult AOGovServiceEdit(int accountingObjectId, int? id)
        {
            return View(new AOGovServiceEditViewModel(accountingObjectId, id));
        }

        [HttpPost]
        public ActionResult AOGovServiceEdit(AddOrSaveAOGovServiceContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var entity = _Command.Build(context).Return<AOGovService>();
                var m = new TableAOGovServices(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteAOGovServices(int id)
        {            
            return NotificationHelper.NotificationDelete(id, () => _Command.Build(new DeleteAOGovServiceContext() { xId = id }).Execute());
        }

        public ActionResult AutocomplateAOGovServiceName(string term)
        {
        var result = new AutocompleteInfo();

           var objs = new RepositoryDc<GovService>().Select<AutocompleteInfo>(
                q => q.WhereRestrictionOn(x => x.Name).IsLike(term + "%").SelectList(list => list
                    .Select(p => p.Id).WithAlias(() => result.id)
                    .Select(p => p.Name).WithAlias(() => result.value)
                    .Select(p => p.Name).WithAlias(() => result.label))
                    .TransformUsing(Transformers.AliasToBean<AutocompleteInfo>()));

            return Json(objs,
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetInfoAboutGovService(int id)
        {
            var govService =
                new RepositoryDc<GovServiceNPA>().GetAllEx(
                    q => q.Where(x => x.GovService.Id == id));

            return Json(govService.Select(x => new TableAOGovServicesNPA(x)), JsonRequestBehavior.AllowGet);
        }


        public ActionResult AOGovServiceNPAEdit(int aoGovServiceId, int? id)
        {
            return View(new AOGovServiceNPAEditViewModel(aoGovServiceId, id));
        }

        [HttpPost]
        public ActionResult AOGovServiceNPAEdit(/*AOGovServiceNPAArgs args*/AddOrSaveAOGovServiceNPAContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                //var isNew = args.Id == 0;

                //var service = new AOGovServiceNPAService();

                var entity = _Command.Build(context).Return<AOGovServiceNPA>();
                    //isNew
                    //? service.Create(args)
                    //: service.Update(args);

                var m = new TableAOGovServicesNPA(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteAOGovServicesNPA(int id)
        {
            //var service = new AOGovServiceNPAService();
            return NotificationHelper.NotificationDelete(id, () => _Command.Build(new DeleteAOGovServiceNPAContext() { xId = id }).Execute() /*service.Delete(new AOGovServiceNPAArgs() { Id = id })*/);
        }


        #endregion SpecialActivity

        

        public ActionResult GoalsAndPurpose(int id)
        {
            return View(new GoalsAndPurposeViewModel(id));
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult GoalsAndPurpose(UpdateGoalAndPurposeAccountingObjectContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() => _Command.Build(context).Execute());
        }

        public ActionResult TechnicalSupport(int id)
        {
            return View(new TechnicalSupportViewModel(id));            
        }

        public ActionResult Software(int id)
        {
            return View(new SoftwareViewModel(id));
        }

        public ActionResult WorkAndService(int id)
        {
            return View(new WorkAndServiceViewModel(id));                
        }

        public ActionResult CharacteristicsCreate(int id)
        {
            return View(new CharacteristicsCreateViewModel(id));                
        }

        public ActionResult InformationAboutSecurity(int id)
        {
            return View(new InformationAboutSecurityViewModel(id));                
        }

        #region InformationInteractionCreate

        public ActionResult InformationInteractionCreate(int id)
        {
            return View(new InformationInteractionCreateViewModel(id));
        }

        public ActionResult InternalIsAndComponentItkiEdit(int accountingObjectId, int? id)
        {
            var model = new InternalIsAndComponentItkiEditViewModel(accountingObjectId, id);

            return View(model);
        }

        [HttpPost]
        public ActionResult InternalIsAndComponentItkiEdit(AddOrUpdateInternalIsAndComponentItkiContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var entity = _Command.Build(context).Return<InternalIsAndComponentsItki>();                    
                var m = new TableInternalIsAndComponentItki(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteInternalIsAndComponentItki(int id)
        {
            return NotificationHelper.NotificationDelete(id, () =>
                new BaseDeleteCommand<BaseAddOrSaveContext, InternalIsAndComponentsItki>().Execute(new BaseAddOrSaveContext() { xId = id })
                
                //_Command.Execute<
                );
        }

        public ActionResult ExternalIsAndComponentItkiEdit(int accountingObjectId, int? id)
        {
            var model = new ExternalIsAndComponentItkiEditViewModel(accountingObjectId, id);
            return View(model);
        }

        [HttpPost]
        public ActionResult ExternalIsAndComponentItkiEdit(AddOrSaveExternalIsAndComponentItkiContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var entity = _Command.Build(context).Return<ExternalIsAndComponentsItki>();                                        
                var m = new TableExternalIsAndComponentItki(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteExternalIsAndComponentItki(int id)
        {
            return NotificationHelper.NotificationDelete(id, () =>                
                new BaseDeleteCommand<BaseAddOrSaveContext, ExternalIsAndComponentsItki>().Execute(new BaseAddOrSaveContext() { xId = id })
                );
        }

        public ActionResult ExternalUserInterfaceOfIsEdit(int accountingObjectId, int? id)
        {
            var model = new ExternalUserInterfaceOfIsEditViewModel(accountingObjectId, id);

            return View(model);
        }

        [HttpPost]
        public ActionResult ExternalUserInterfaceOfIsEdit(AddOrSaveExternalUserInterfaceOfIsContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var entity = _Command.Build(context).Return<ExternalUserInterfaceOfIs>();                    
                var m = new TableExternalUserInterfaceOfIs(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteExternalUserInterfaceOfIs(int id)
        {
            return NotificationHelper.NotificationDelete(id, () =>                
                new BaseDeleteCommand<BaseAddOrSaveContext, ExternalUserInterfaceOfIs>().Execute(new BaseAddOrSaveContext() { xId = id }));
        }


        #endregion InformationInteractionCreate


        public ActionResult KindMasterEdit(int id)
        {
            return View(new KindMasterEditViewModel(id));                
        }

        [HttpPost]
        public ActionResult KindMasterEdit(int id, int kindId)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                _Command.Build(new UpdateKindAccountingObjectContext() { xId = id, KindId = kindId }).Execute();
                //new AccountingObjectNotificationViewModel(Service.Update(args)));
            });
        }

        public ActionResult Purchase(int id)
        {
            return View(new PurchaseViewModel(id));                
        }

        [HttpPost]
        public ActionResult PurchaseAdd(AddPurchaseAccountingObjectContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var res = _Command.Build(context).Return<GovPurchase>();

                return new TablePurchase(res);
            });
        }

        [HttpPost]
        public ActionResult PurchaseDelete(int id)
        {
            return NotificationHelper.NotificationDelete(id, () =>
            {
                _Command.Build(new DeletePurchaseAccountingObjectContext() { Id = id }).Execute();
            });
        } 
    }
}