﻿namespace bgTeam.IsRui.WebApp.ViewModels
{
    /// <summary>
    /// Базовый класс, для объектов, при изменении
    /// которых необходимо выводить всплывающее сообщение на экран
    /// </summary>
    public class NotificationBaseViewModel : ViewModelBase
    {
        public bool success { get; set; }

        public string message { get; set; }
    }
}