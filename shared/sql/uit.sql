INSERT INTO dbo.Role (Role_ID, Role_Name, Role_Web_Page) VALUES ( 0, N'Администратор', N'~\Admin\');
INSERT INTO dbo.Role (Role_ID, Role_Name, Role_Web_Page) VALUES ( 1, N'Ведомство', N'~\AccountingObjects\');
INSERT INTO dbo.Role (Role_ID, Role_Name, Role_Web_Page) VALUES ( 60, N'Учереждение', N'~\AccountingObjects\');
GO

INSERT INTO dbo.Govt_Organ ( Govt_Organ_ID ,Govt_Organ_Code ,Govt_Organ_Name ,Govt_Organ_SName ,Govt_Organ_Addr ,Govt_Organ_Phone ,Govt_Organ_Web ,Govt_Organ_Is_Active ,Govt_Organ_SPZ ,Govt_Organ_Par_ID)
VALUES ( 1, N'1', N'Тестовая организация', N'', N'', N'', N'', 0, N'', null);
INSERT INTO dbo.Govt_Organ ( Govt_Organ_ID ,Govt_Organ_Code ,Govt_Organ_Name ,Govt_Organ_SName ,Govt_Organ_Addr ,Govt_Organ_Phone ,Govt_Organ_Web ,Govt_Organ_Is_Active ,Govt_Organ_SPZ ,Govt_Organ_Par_ID)
VALUES ( 2, N'1', N'Организация для разработки', N'', N'', N'', N'', 0, N'', null);

INSERT INTO dbo.Users ( Users_ID ,Users_Is_Active ,Users_Login ,Users_Password ,Users_Surname ,Users_Name ,Users_Middlename ,Users_Job ,Users_Work_Phone ,Users_Mob_Phone ,Users_Fax ,Users_Email ,Users_Work ,Users_Education ,Users_Receive_Mail ,Users_Is_Responsible ,Users_Snils ,Users_Is_ESIA ,Users_Token ,Govt_Organ_ID)
VALUES ( 1, 1, N'Test', N'81dc9bdb52d04dc20036dbd8313ed055', N'Test', N'Test', N'Test', N'', N'', N'', N'', N'', N'', N'', 0, 0, N'', 0, N'', 1);
INSERT INTO dbo.Users ( Users_ID ,Users_Is_Active ,Users_Login ,Users_Password ,Users_Surname ,Users_Name ,Users_Middlename ,Users_Job ,Users_Work_Phone ,Users_Mob_Phone ,Users_Fax ,Users_Email ,Users_Work ,Users_Education ,Users_Receive_Mail ,Users_Is_Responsible ,Users_Snils ,Users_Is_ESIA ,Users_Token ,Govt_Organ_ID)
VALUES ( 2, 1, N'Test2', N'81dc9bdb52d04dc20036dbd8313ed055', N'Test2', N'Test2', N'Test2', N'', N'', N'', N'', N'', N'', N'', 0, 1, N'', 0, N'', 1);
INSERT INTO dbo.Users ( Users_ID ,Users_Is_Active ,Users_Login ,Users_Password ,Users_Surname ,Users_Name ,Users_Middlename ,Users_Job ,Users_Work_Phone ,Users_Mob_Phone ,Users_Fax ,Users_Email ,Users_Work ,Users_Education ,Users_Receive_Mail ,Users_Is_Responsible ,Users_Snils ,Users_Is_ESIA ,Users_Token ,Govt_Organ_ID)
VALUES ( 3, 1, N'Test3', N'81dc9bdb52d04dc20036dbd8313ed055', N'Test3', N'Test3', N'Test3', N'', N'', N'', N'', N'', N'', N'', 0, 1, N'', 0, N'', 1);

INSERT INTO dbo.Users ( Users_ID ,Users_Is_Active ,Users_Login ,Users_Password ,Users_Surname ,Users_Name ,Users_Middlename ,Users_Job ,Users_Work_Phone ,Users_Mob_Phone ,Users_Fax ,Users_Email ,Users_Work ,Users_Education ,Users_Receive_Mail ,Users_Is_Responsible ,Users_Snils ,Users_Is_ESIA ,Users_Token ,Govt_Organ_ID)
VALUES ( 4, 1, N'Work1', N'81dc9bdb52d04dc20036dbd8313ed055', N'Work1', N'Work1', N'Work1', N'', N'', N'', N'', N'', N'', N'', 0, 1, N'', 0, N'', 2);
INSERT INTO dbo.Users ( Users_ID ,Users_Is_Active ,Users_Login ,Users_Password ,Users_Surname ,Users_Name ,Users_Middlename ,Users_Job ,Users_Work_Phone ,Users_Mob_Phone ,Users_Fax ,Users_Email ,Users_Work ,Users_Education ,Users_Receive_Mail ,Users_Is_Responsible ,Users_Snils ,Users_Is_ESIA ,Users_Token ,Govt_Organ_ID)
VALUES ( 5, 1, N'Work2', N'81dc9bdb52d04dc20036dbd8313ed055', N'Work2', N'Work2', N'Work2', N'', N'', N'', N'', N'', N'', N'', 0, 1, N'', 0, N'', 2);


INSERT INTO dbo.Users_To_Role ( Users_To_Role_ID ,Role_ID ,Users_ID) VALUES ( 1, 60, 1);
INSERT INTO dbo.Users_To_Role ( Users_To_Role_ID ,Role_ID ,Users_ID) VALUES ( 2, 60, 2);
INSERT INTO dbo.Users_To_Role ( Users_To_Role_ID ,Role_ID ,Users_ID) VALUES ( 3, 60, 3);

INSERT INTO dbo.Users_To_Role ( Users_To_Role_ID ,Role_ID ,Users_ID) VALUES ( 4, 60, 4);
INSERT INTO dbo.Users_To_Role ( Users_To_Role_ID ,Role_ID ,Users_ID) VALUES ( 5, 60, 5);
GO

INSERT INTO dbo.Responsible_Job (Responsible_Job_ID, Responsible_Job_Name) VALUES (1, N'Директор');
INSERT INTO dbo.Responsible_Job (Responsible_Job_ID, Responsible_Job_Name) VALUES (2, N'Руководитель отдела');
INSERT INTO dbo.Responsible (Responsible_ID, Responsible_FIO, Responsible_Phone, Responsible_Email, Responsible_Is_Active, Govt_Organ_ID, Responsible_Job_ID)
VALUES (1, N'Ответственный1',N'8 999 999 99 99',N'test@test.ru',1,1,2);
GO

ALTER TABLE NH_HiLo ADD TableKey NVARCHAR(255) NULL;

INSERT INTO NH_HiLo ( NextHi ,TableKey) VALUES ( 1, 'OU');
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Charact')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Charact_Type')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Charact_Value')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Document')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Exploitation_Department')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Externel_IS')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Externel_User_Interface')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Favorites')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Gos_Contract')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Internel_IS')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_NPA_Document')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Plans_Activity')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Purchase_Decision')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Software')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Special_Check')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Stage')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Technical')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_User_Comment')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Uslugi')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Work_And_Goods')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Work_And_Service')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'OU_Zakupki')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Document')

INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Activity')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Activity_ED')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Activity_Goal')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Activity_Indicators')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Activity_Mark')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Activity_Service')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Activity_Synchronization_Coord')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Activity_Type2')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Goal_Indicator')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Spec')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Spec_Charact')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Plans_Work')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Activity_Expert')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Activity_Expertise_Conclusion')

INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'License_Charact')
INSERT INTO [dbo].[NH_HiLo]([NextHi],[TableKey])VALUES(1,'Charact_TO')


GO

INSERT INTO [dbo].[IS_or_IKT] ([IS_or_IKT_ID] ,[IS_or_IKT_Name] ,[IS_or_IKT_Code] ,[IS_or_IKT_Start_Date] ,[IS_or_IKT_End_Date] ,[IS_or_IKT_Parent])
VALUES (0 ,'Не задано' ,null ,'1.1.2016' ,'1.1.2018' ,NULL)
INSERT INTO [dbo].[IS_or_IKT] ([IS_or_IKT_ID] ,[IS_or_IKT_Name] ,[IS_or_IKT_Code] ,[IS_or_IKT_Start_Date] ,[IS_or_IKT_End_Date] ,[IS_or_IKT_Parent])
VALUES (1 ,'Базовый икт' ,'1' ,'1.1.2016' ,'1.1.2018' ,NULL)
INSERT INTO [dbo].[IS_or_IKT] ([IS_or_IKT_ID] ,[IS_or_IKT_Name] ,[IS_or_IKT_Code] ,[IS_or_IKT_Start_Date] ,[IS_or_IKT_End_Date] ,[IS_or_IKT_Parent])
VALUES (2 ,'Дочерний икт' ,'1.1' ,'1.1.2016' ,'1.1.2018' ,1)
GO


INSERT INTO [dbo].[Status_Group] ([Status_Group_ID] ,[Status_Group_Name])VALUES (0 , 'None')
INSERT INTO [dbo].[Status_Group] ([Status_Group_ID] ,[Status_Group_Name])VALUES (1 , 'Статусы мероприятий')
INSERT INTO [dbo].[Status_Group] ([Status_Group_ID] ,[Status_Group_Name])VALUES (2 , 'Статусы экспертизы по мероприятию')
INSERT INTO [dbo].[Status_Group] ([Status_Group_ID] ,[Status_Group_Name])VALUES (3 , 'Статусы объекта учета')

INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (0 ,'Не задано' ,'Статус не задан' ,NULL ,0) 

INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (1 ,'Создано' ,'Создано' ,NULL ,1) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (2 ,'На экспертизе в ЦА' ,'На экспертизе в ЦА' ,NULL ,1) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (3 ,'На доработке' ,'На доработке' ,NULL ,1) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (4 ,'Согласовано ЦА' ,'Согласовано ЦА' ,NULL ,1) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (5 ,'Отправлено в МКС' ,'Отправлено в МКС' ,NULL ,1) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (6 ,'Отклонено в МКС' ,'Отклонено в МКС' ,NULL ,1) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (7 ,'Включено в план' ,'Включено в план' ,NULL ,1) 

INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (50 ,'Назначаются эксперты' ,'Назначаются эксперты' ,NULL ,2) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (51 ,'На экспертизе у технического специалиста' ,'На экспертизе у технического специалиста' ,NULL ,2) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (52 ,'На экспертизе у экономиста' ,'На экспертизе у экономиста' ,NULL ,2) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (53 ,'Согласовано экспертами' ,'Согласовано экспертами' ,NULL ,2) 

INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (100 ,'Подготовка сведений' ,'Подготовка сведений' ,NULL ,3) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (101 ,'На согласовании' ,'На согласование' ,NULL ,3) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (102 ,'На доработке' ,'На доработку' ,NULL ,3) 
INSERT INTO [dbo].[Status] ([Status_ID] ,[Status_Name] ,[Status_Action_Name] ,[Status_Css] ,[Status_Group_ID])
VALUES (103 ,'Cогласовано вышестоящим' ,'Согласовано' ,NULL ,3) 
GO

INSERT INTO [dbo].[Status_Change_List] (Status_Change_List_ID, Role_ID, Status_From_ID, Status_To_ID) VALUES (1, 60, 100, 101)
INSERT INTO [dbo].[Status_Change_List] (Status_Change_List_ID, Role_ID, Status_From_ID, Status_To_ID) VALUES (2, 60, 101, 102)
INSERT INTO [dbo].[Status_Change_List] (Status_Change_List_ID, Role_ID, Status_From_ID, Status_To_ID) VALUES (3, 60, 101, 103)
INSERT INTO [dbo].[Status_Change_List] (Status_Change_List_ID, Role_ID, Status_From_ID, Status_To_ID) VALUES (4, 60, 102, 101)
GO

ALTER TABLE Document ADD Document_Sign_File binary
ALTER TABLE Document ALTER COLUMN Document_Creator INTEGER NULL
GO

INSERT INTO [dbo].[OU_Charact_Type] ([OU_Charact_Type_ID],[OU_Charact_Type_Name])VALUES(0,'Не задано')
INSERT INTO [dbo].[OU_Charact_Type] ([OU_Charact_Type_ID],[OU_Charact_Type_Name])VALUES(1,'Тип 1')
INSERT INTO [dbo].[OU_Charact_Type] ([OU_Charact_Type_ID],[OU_Charact_Type_Name])VALUES(2,'Тип 2')
GO

INSERT INTO [dbo].[MUnit]([MUnit_ID],[MUnit_Name],[MUnit_SName])VALUES(0,'Не задано',' - ')
INSERT INTO [dbo].[MUnit]([MUnit_ID],[MUnit_Name],[MUnit_SName])VALUES(1,'шт.','штуки')
INSERT INTO [dbo].[MUnit]([MUnit_ID],[MUnit_Name],[MUnit_SName])VALUES(2,'кг.','килограм')
GO

INSERT INTO [dbo].[w_Year]([Year_ID],[Year])VALUES(2016,2016)
INSERT INTO [dbo].[w_Year]([Year_ID],[Year])VALUES(2017,2017)
INSERT INTO [dbo].[w_Year]([Year_ID],[Year])VALUES(2018,2018)
GO

INSERT INTO [dbo].[VO_Category]([VO_Category_ID],[VO_Category_Name],[VO_Category_Value])VALUES(0,'Не задано',0)
INSERT INTO [dbo].[VO_Category]([VO_Category_ID],[VO_Category_Name],[VO_Category_Value])VALUES(1,'Категория 1',1)
INSERT INTO [dbo].[VO_Category]([VO_Category_ID],[VO_Category_Name],[VO_Category_Value])VALUES(2,'Категория 2',2)
INSERT INTO [dbo].[VO_Category]([VO_Category_ID],[VO_Category_Name],[VO_Category_Value])VALUES(3,'Категория 3',3)
GO

INSERT INTO [dbo].[Producer]([Producer_ID],[Producer_Name])VALUES(0,'Не задано')
INSERT INTO [dbo].[Producer]([Producer_ID],[Producer_Name])VALUES(1,'Dell')
INSERT INTO [dbo].[Producer]([Producer_ID],[Producer_Name])VALUES(2,'Microsoft')
GO

INSERT INTO [dbo].[OKVED]([OKVED_ID],[OKVED_Code],[OKVED_Name],[OKVED_Salary])VALUES(0,'','Не задано',0)
INSERT INTO [dbo].[OKVED]([OKVED_ID],[OKVED_Code],[OKVED_Name],[OKVED_Salary])VALUES(1,'','ОКВЭД 1',1)
INSERT INTO [dbo].[OKVED]([OKVED_ID],[OKVED_Code],[OKVED_Name],[OKVED_Salary])VALUES(2,'','ОКВЭД 1',2)
GO

INSERT INTO [dbo].[Interface_Type]([Interface_Type_ID],[Interface_Type_Name])VALUES(0,'Не задано')
INSERT INTO [dbo].[Interface_Type]([Interface_Type_ID],[Interface_Type_Name])VALUES(1,'Интерфейс 1')
INSERT INTO [dbo].[Interface_Type]([Interface_Type_ID],[Interface_Type_Name])VALUES(2,'Интерфейс 2')
GO

INSERT INTO [dbo].[Plans_Activity_Type2]([Plans_Activity_Type2_ID],[Plans_Activity_Type2_Name])VALUES(0,'Не задано')
INSERT INTO [dbo].[Plans_Activity_Type2]([Plans_Activity_Type2_ID],[Plans_Activity_Type2_Name])VALUES(1,'Тип 1')
INSERT INTO [dbo].[Plans_Activity_Type2]([Plans_Activity_Type2_ID],[Plans_Activity_Type2_Name])VALUES(2,'Тип 2')
GO