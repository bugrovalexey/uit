﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.Directories;
using GB.Data.Documents;
using GB.Data.Plans;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;
using GB.DataAccess.Services.Args;
using GB.Extentions;
using GB.Helpers;

namespace GB.DataAccess.Command.Activities
{
    public class CreateAccountingObjectCommand : ICommand<CreateAccountingObjectContext>
    {
        ISessionFactoryGb factory;

        public CreateAccountingObjectCommand()
        {
            factory = new SessionFactory();
        }
        
        public void Execute(CreateAccountingObjectContext context)
        {
            AccountingObject accountingObject = null;

            //Если есть возможность редактировать ОУ,
            //нужно его либо создать, либо обновить существующему поля
            if (context.IsEditAO)
            {
                var args = context as IAccountingObjectControllerArgs;

                accountingObject = new Repository<AccountingObject>().Get(args.Id);

                if (accountingObject == null)
                {
                    accountingObject = new AccountingObject();

                    //Проставляем значения по умолчанию для Not Nullable полей
                    accountingObject.Status = new StatusRepository().GetDefault(accountingObject);
                    accountingObject.Stage = AccountingObjectsStageEnum.Planned; // задаем стадию при создании
                    accountingObject.Department = UserRepository.GetCurrent().Department;
                    accountingObject.Type = new Repository<IKTComponent>().Get(EntityBase.Default);
                }

                accountingObject.Kind = new Repository<IKTComponent>().Get(args.Kind);
                accountingObject.ShortName = args.ShortName;
                accountingObject.FullName = args.FullName;
            }

            using (var session = factory.Create())
            {
                //Если есть возможность редактировать ОУ,
                //нужно его сохранить
                if (context.IsEditAO)
                {
                    session.SaveOrUpdate(accountingObject);
                    context.AccountingObject = accountingObject.Id;
                }

                var args = context as IPlansActivityArgs;

                var activity = new Repository<PlansActivity>().GetExisting(args.Id);
                //if (!activity.CanEdit())
                //{
                //    throw new InvalidOperationException("Нет прав на изменение объекта.");
                //}

                Mapper.Map(args, activity);

                session.Update(activity);

                //Сохранение файлов
                Save(args.Logo, activity.Logo, activity, DocTypeEnum.Logo, session);
                Save(args.Files, activity.Documents, activity, DocTypeEnum.ActivityFEO, session);
                Save(args.Conception, activity.Conception, activity, DocTypeEnum.Conception, session);


                //Связь с ОУ у мероприятия через AoActivity
                var aoActivity = activity.AoActivity;

                //Если ОУ нет, удаляем связь
                if (context.AccountingObject.HasValue == false)
                {
                   aoActivity.Do(session.Delete);
                }
                else
                {
                    //Создаем связь, если нужно
                    //И обновляем поля
                    if (aoActivity == null)
                    {
                        aoActivity = new AoActivity();
                        aoActivity.Activity = activity;
                        aoActivity.ActivityType = activity.ActivityType2.Return(a => (ActivityTypeEnum)a.Id);
                    }

                    aoActivity.Name = context.Name;
                    aoActivity.AccountingObject = accountingObject ?? new Repository<AccountingObject>().Get(context.AccountingObject);
                    aoActivity.Stage = aoActivity.AccountingObject.Stage;
                    session.SaveOrUpdate(aoActivity);
                }

                session.Commit();
            }
        }


        private void Save(string fileList, IList<Files> listDocs, EntityBase owner, DocTypeEnum type, SessionHibernate session)
        {
            Dictionary<string, string> newFiles = StringHelper.FileListToDictionary(fileList);

            foreach (Files file in listDocs)
            {
                string id = file.Id.ToString();
                if (newFiles.ContainsKey(id))
                {
                    newFiles.Remove(id);
                }
                else
                {
                    session.Delete(file);
                }
            }

            foreach (string key in newFiles.Keys)
            {
                Files file = new Files(type, owner);
                file.FileName = file.Name = newFiles[key];
                session.SaveOrUpdate(file);

                string guidString = key;
                try
                {
                    int slashIndex = guidString.IndexOf('/');
                    if (slashIndex >= 0)
                    {
                        guidString = guidString.Substring(0, slashIndex);
                    }
                    Guid guid = new Guid(guidString);
                    FileProvider.MoveToGeneric(guid, file.Id);
                }
                catch (Exception e)
                {
                    session.Delete(file);
                    Logger.Fatal(string.Format("Не удалось переместить файл из temp в хранилище. key={0} guidString={1} Document.Id={2}",
                        key, guidString, file.Id), e);
                }
            }
        }
    }
}
