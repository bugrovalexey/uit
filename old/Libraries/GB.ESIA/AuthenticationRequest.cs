﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Web.UI;
using System.Xml;
using Atp.Saml2;

namespace GB.ESIA
{
	/// <summary>
	/// Запрос аутентификации.
	/// </summary>
	public class AuthenticationRequest
	{
		private readonly string relayState = Guid.NewGuid().ToString();
		/// <summary>
		/// GUID-идентификатор запроса.
		/// </summary>
		public string RelayState
		{
			get { return relayState; }
		}

		/// <summary>
		/// Идентификатор запроса.
		/// </summary>
		public string Id
		{
			get { return authnRequest.Id; }
		}

		/// <summary>
		/// Представление запроса в виде XML.
		/// </summary>
		/// <remarks>Пример использования: [EasyAuthenticationRequest].Xml.OuterXml</remarks>
		public XmlNode Xml
		{
			get { return authnRequest.GetXml().ParentNode; }
		}

		/// <summary>
		/// Страница, с которой инициирован запрос.
		/// </summary>
		private readonly Page pageFrom;

		/// <summary>
		/// Сертификат, которым должен быть подписан отправляемый запрос.
		/// </summary>
		private readonly X509Certificate2 spX509Certificate;

		/// <summary>
		/// Запрос на аутентификацию.
		/// </summary>
		private readonly AuthnRequest authnRequest;

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="page">Страница, с которой инициирован запрос.</param>
		/// <param name="spCertificate">Сертификат, которым должен быть подписан отправляемый запрос.</param>
		/// <param name="singleSignOnServiceUrl">URL, на который должен быть отправлен запрос аутентификации.</param>
		/// <param name="assertionConsumerServiceUrl">URL, на который IdentityProvider должен посылать ответ после идентификации.</param>
		public AuthenticationRequest(Page page, X509Certificate2 spCertificate, string singleSignOnServiceUrl, string assertionConsumerServiceUrl)
            : this(page, Util.GetAbsoluteUrl(page, "~/"), spCertificate, singleSignOnServiceUrl, assertionConsumerServiceUrl) {}


	    /// <summary>
	    /// Конструктор для MVC
	    /// </summary>
	    /// <param name="page">Страница, с которой инициирован запрос.</param>
	    /// <param name="absoluteUrl">URL</param>
	    /// <param name="spCertificate">Сертификат, которым должен быть подписан отправляемый запрос.</param>
	    /// <param name="singleSignOnServiceUrl">URL, на который должен быть отправлен запрос аутентификации.</param>
	    /// <param name="assertionConsumerServiceUrl">URL, на который IdentityProvider должен посылать ответ после идентификации.</param>
	    public AuthenticationRequest(Page page, string absoluteUrl,  X509Certificate2 spCertificate, string singleSignOnServiceUrl, string assertionConsumerServiceUrl)
        {
            authnRequest = new AuthnRequest();
            authnRequest.Destination = singleSignOnServiceUrl;

            //var absoluteUrl = Util.GetAbsoluteUrl(page, "~/");
            /*if (page.Request.IsSecureConnection)
            {
                absoluteUrl = absoluteUrl.Replace("https://", "http://");
            }
            absoluteUrl = absoluteUrl.Replace("www.", string.Empty);
            */
            authnRequest.Issuer = new Issuer(absoluteUrl);

            authnRequest.ForceAuthn = false;
            authnRequest.NameIdPolicy = null;
            authnRequest.AssertionConsumerServiceUrl = assertionConsumerServiceUrl;
            authnRequest.ProtocolBinding = "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST";
            spX509Certificate = spCertificate;
            pageFrom = page;
        }

		/// <summary>
		/// Отправляет запрос аутентификации.
		/// </summary>
		public void Send()
		{
			authnRequest.Redirect(pageFrom.Response, authnRequest.Destination, relayState, spX509Certificate.PrivateKey);	
		}
	}
}
