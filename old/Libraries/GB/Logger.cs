﻿using log4net;
using System;

namespace GB
{
    /// <summary>
    /// Данный класс нужен для именования логгера, который описан в web.config
    /// <remarks>Если класс изменил namespace, но надо исправить полное имя логера в web.config </remarks>
    /// </summary>
    public class LoggerBase
    {
    }

    public class Logger : Logger<LoggerBase>
    {
    }

    public class Logger<T> where T : class
    {
        private static string _Level = "\t\t\t";
        private static ILog _Logger = Init();

        private static ILog Init()
        {
            log4net.Config.XmlConfigurator.Configure();

            return log4net.LogManager.GetLogger(typeof(T));
        }

        public static void Debug(string message)
        {
            _Logger.Debug(string.Concat(_Level, message));
        }

        public static void Info(string message, params object[] args)
        {
            var mess = string.Format(message, args);

            _Logger.Info(string.Concat(_Level, mess));
        }

        public static void Error(string message)
        {
            _Logger.Error(message);
        }

        public static void Error(Exception exp)
        {
            _Logger.Error(exp.Message, exp);
        }

        public static void Error(string message, Exception exp)
        {
            _Logger.Error(message, exp);
        }

        public static void Fatal(string message)
        {
            _Logger.Fatal(message);
        }

        public static void Fatal(Exception exp)
        {
            _Logger.Fatal(exp.Message, exp);
        }

        public static void Fatal(string message, Exception exp)
        {
            _Logger.Fatal(message, exp);
        }

        //private static string LogEx(Exception exception)
        //{
        //    return string.Format("{0}Error: {1}; {0} StackTrace: {2}; {0} InnerException: {3} {0}",
        //                               Environment.NewLine,
        //                               exception.Message,
        //                               exception.StackTrace,
        //                               exception.InnerException == null ? "none" : LogEx(exception.InnerException));
        //}
    }
}