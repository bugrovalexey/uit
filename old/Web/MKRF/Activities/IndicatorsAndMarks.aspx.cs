﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB;
using GB.Extentions;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Dtos;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Extensions;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.Data;
using GB.Controls;
using GB.Controls.Extensions;
using GB.MKRF.Entities.AccountingObjects;

namespace GB.MKRF.Web.Activities
{
    public partial class IndicatorsAndMarks : BasePagePlansActivity
    {
        #region Fields

        private PlansActivityMarkController _markController;

        private PlansActivityIndicatorController _indicatorController;

        #endregion Fields


        #region Prop

        public PlansActivityMarkController MarkController
        {
            get
            {
                if (_markController == null)
                {
                    _markController = new PlansActivityMarkController();
                }

                return _markController;
            }
        }

        public PlansActivityIndicatorController IndicatorController
        {
            get
            {
                if (_indicatorController == null)
                {
                    _indicatorController = new PlansActivityIndicatorController();
                }

                return _indicatorController;
            }
        }


        #endregion Prop


        #region Handlers

        #region PageHandlers
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            gridMarks.DataBind();
            gridIndicators.DataBind();
        }

        #endregion PageHandlers

        #region Control Handlers

        #region Mark

        #region EditFom

        protected void TxtNameMark_OnDataBinding(object sender, EventArgs e)
        {
            MarkController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Name));
        }

        protected void TxtJustification_OnDataBinding(object sender, EventArgs e)
        {
            MarkController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Justification));
        }

        protected void TxtExplanationMark_OnDataBinding(object sender, EventArgs e)
        {
            MarkController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.ExplanationRelationshipWithProjectMark));
        }

        protected void TxtBaseValueMark_OnDataBinding(object sender, EventArgs e)
        {
            MarkController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.BaseValue));
        }

        protected void TxtTargetedValueMark_OnDataBinding(object sender, EventArgs e)
        {
            MarkController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.TargetedValue));
        }

        protected void cbxUnitMark_OnDataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(RepositoryBase<Units>.GetAll())
                   .SetValueComboBox(MarkController.With(x => x.CurrentEdit).With(x => x.Unit).Return(x => x.Id, EntityBase.Default));
        }

        protected void deDateMark_OnDataBinding(object sender, EventArgs e)
        {
            MarkController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.DateToAchieveGoalValue));
        }

        #endregion EditForm


        protected void gridMarks_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            gridMarks.DataSource = MarkController.Repository.GetAllByPlansActivity(ActivityId.GetValueOrDefault());
        }

        protected void gridMarks_OnRowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            MarkController.Create(GetMarkArgs());
            gridMarks.GridCancelEdit(e);
        }

        protected void gridMarks_OnRowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            MarkController.Update(GetMarkArgs((int)e.Keys[0]));
            gridMarks.GridCancelEdit(e);
        }

        protected void gridMarks_OnStartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            MarkController.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void gridMarks_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            MarkController.Delete(new PlansActivityMarkArgs()
            {
                Id = (int)e.Keys[0]
            });

            gridMarks.GridCancelEdit(e);
        }

        #endregion Mark

        #region Indicator


        #region EditFom

        protected void TxtAlgorithmOfCalculating_OnDataBinding(object sender, EventArgs e)
        {
            IndicatorController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.AlgorithmOfCalculating));
        }

        protected void TxtExplanationIndicator_OnDataBinding(object sender, EventArgs e)
        {
            IndicatorController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.ExplanationRelationshipWithProjectMark));
        }

        protected void TxtNameIndicator_OnDataBinding(object sender, EventArgs e)
        {
            IndicatorController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Name));
        }

        protected void TxtBaseValueIndicator_OnDataBinding(object sender, EventArgs e)
        {
            IndicatorController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.BaseValue));
        }

        protected void TxtTargetedValueIndicator_OnDataBinding(object sender, EventArgs e)
        {
            IndicatorController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.TargetedValue));
        }

        protected void cbxUnitIndicator_OnDataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(RepositoryBase<Units>.GetAll())
                   .SetValueComboBox(IndicatorController.With(x => x.CurrentEdit).With(x => x.Unit).Return(x => x.Id, EntityBase.Default));
        }

        protected void deDateIndicator_OnDataBinding(object sender, EventArgs e)
        {
            IndicatorController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.DateToAchieveGoalValue));
        }

        #endregion EditForm


        protected void gridIndicators_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            gridIndicators.DataSource = IndicatorController.Repository.GetAllByPlansActivity(ActivityId.GetValueOrDefault());
        }

        protected void gridIndicators_OnRowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            IndicatorController.Create(GetIndicatorArgs());
            gridIndicators.GridCancelEdit(e);
        }

        protected void gridIndicators_OnRowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            IndicatorController.Update(GetIndicatorArgs((int)e.Keys[0]));
            gridIndicators.GridCancelEdit(e);
        }

        protected void gridIndicators_OnStartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            IndicatorController.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void gridIndicators_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            IndicatorController.Delete(new PlansActivityIndicatorArgs()
            {
                Id = (int)e.Keys[0]
            });

            gridIndicators.GridCancelEdit(e);
        }


        #endregion Indicator

        #endregion Control Handlers

        #endregion Handlers


        #region Helpers

        public static string GetUrl(object activityId)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/AccountingObjects/IndicatorsAndMarks.aspx"), EditCard.ActivityIdKey, activityId);
        }

        private PlansActivityMarkArgs GetMarkArgs(int id = 0)
        {
            var result = new PlansActivityMarkArgs()
            {
                Id = id,
                PlansActivity = CurrentActivity,
                Name = hfMark["Name"].ToStringOrDefault(),
                ExplanationRelationshipWithProjectMark = hfMark["Explanation"].ToStringOrDefault(),
                Justification = hfMark["Justification"].ToStringOrDefault(),
                BaseValue = hfMark["BaseValue"].ToIntNull(),
                TargetedValue = hfMark["TargetedValue"].ToIntNull(),
                UnitId = hfMark["UnitId"].ToIntNull(),
                DateToAchieveGoalValue = hfMark["Date"].ToDateTimeNull(),
            };

            return result;
        }


        private PlansActivityIndicatorArgs GetIndicatorArgs(int id = 0)
        {
            var result = new PlansActivityIndicatorArgs()
            {
                Id = id,
                PlansActivity = CurrentActivity,
                Name = hfIndicators["Name"].ToStringOrDefault(),
                ExplanationRelationshipWithProjectMark = hfIndicators["Explanation"].ToStringOrDefault(),
                AlgorithmOfCalculating = hfIndicators["AlgorithmOfCalculating"].ToStringOrDefault(),
                BaseValue = hfIndicators["BaseValue"].ToIntNull(),
                TargetedValue = hfIndicators["TargetedValue"].ToIntNull(),
                UnitId = hfIndicators["UnitId"].ToIntNull(),
                DateToAchieveGoalValue = hfIndicators["Date"].ToDateTimeNull(),
            };

            return result;
        }

        #endregion Helpers

    }
}