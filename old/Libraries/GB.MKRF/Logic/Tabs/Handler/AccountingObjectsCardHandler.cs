﻿namespace GB.MKRF.Logic.Tabs.Handler
{
    internal class AccountingObjectsCardHandler : TabsBaseHandler
    {
        public AccountingObjectsCardHandler()
            : base("AccountingObjects")
        {
            Add("Общие сведения", "CommonData");
            Add("Информационное взаимодействие", "InformationInteraction");
            Add("Характеристики", "Characteristics");
            Add("Мероприятия по информатизации", "Activities");
            Add("Ответственные", "Responsibles");
            Add("Виды обеспечения", "KindsOfSupport");
        }
    }
}