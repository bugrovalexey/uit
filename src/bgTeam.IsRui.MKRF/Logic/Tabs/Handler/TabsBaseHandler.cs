﻿using bgTeam.IsRui.Common;
using bgTeam.IsRui.MKRF.Providers;
using System.Collections.Generic;

namespace bgTeam.IsRui.MKRF.Logic.Tabs.Handler
{
    internal class TabsBaseHandler : ITabsHandler
    {
        private readonly IAccessControllerProvider m_accessControllerProvider;
        private string _Controller;

        private IList<ActivityPage> Tabs { get; set; }

        public TabsBaseHandler(IAccessControllerProvider accessControllerProvider)
        {
            Tabs = new List<ActivityPage>();
            m_accessControllerProvider = accessControllerProvider;
        }

        public TabsBaseHandler(string controller, IAccessControllerProvider accessControllerProvider)
            : this(accessControllerProvider)
        {
            _Controller = controller;
        }

        protected void Add(string name, string action)
        {
            Add(name, action, _Controller);
        }

        protected void Add(string name, string action, string controller)
        {
            Tabs.Add(new ActivityPage()
            {
                Name = name,
                Action = action,
                Controller = controller,
            });
        }

        public string GetAction()
        {
            foreach (var item in Tabs)
            {
                if (m_accessControllerProvider.СheckAccessCurrentUser(item.Controller, item.Action))
                    return item.Action;
            }

            throw new ObjectAccessException();
        }

        public IList<string> GetTabsList(ReturnTabsText fun)
        {
            List<string> res = new List<string>();

            foreach (var item in Tabs)
            {
                if (m_accessControllerProvider.СheckAccessCurrentUser(item.Controller, item.Action))
                    res.Add(fun(item.Name, item.Action, item.Controller));
            }

            return res;
        }
    }

    internal class ActivityPage
    {
        public string Name { get; set; }

        public string Action { get; set; }

        public string Controller { get; set; }
    }
}