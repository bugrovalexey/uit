﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201803051226)]
    public class Migration_201803051225 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("WorkflowUserConclusionHistory").AddColumn("Id_Ref").AsInt32().Nullable();
        }
    }
}
