﻿using System;

namespace bgTeam.IsRui.Domain.Entities.Plans
{
    public class PlansActivitySynchronization : EntityBase
    {
        /// <summary>Мероприятие Плана</summary>
        public virtual PlansActivity PlanActivity { get; set; }

        public virtual DateTime SynchronizationDate { get; set; }

        public PlansActivitySynchronization(PlansActivity pa)
        {
            PlanActivity = pa;
        }

        public PlansActivitySynchronization()
        {
        }
    }
}