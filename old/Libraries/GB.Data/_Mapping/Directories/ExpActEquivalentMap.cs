﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ExpActEquivalentMap : ClassMapBase<ExpActEquivalent>
	{
        public ExpActEquivalentMap()
            : base("Exp_Act_Equivalent")
		{
//            Table("w_");

//            Id(x => x.Id, "Exp_Act_Equivalent_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            SqlInsert(@"exec ZZZ_Prc_Exp_Act_Equivalent_Insert
//						  @Exp_Act_Equivalent_ID = ?
//						 ,@Exp_Act_Equivalent_Name = ?").Check.None();


//            SqlUpdate(@"exec ZZZ_Prc_Exp_Act_Equivalent_Update
//						  @Exp_Act_Equivalent_ID = ?
//						 ,@Exp_Act_Equivalent_Name = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Act_Equivalent_Delete
//						  @Exp_Act_Equivalent_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Exp_Act_Equivalent_Name").Nullable();
        }
    }
}
