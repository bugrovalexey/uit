﻿using System.Collections.Generic;
namespace GB.DataAccess.Command.AccountingObjects.ActsToAOActivities
{
    public class UpdateActToAOActivityContext : ICommandContext
    {
        public UpdateActToAOActivityContext()
        {            
            ActToActivityIds = new List<int>();
        }

        public List<int> ActToActivityIds { get; set; }

        public int ActId { get; set; }
    }
}
