﻿using GB.Data.Frgu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB._Mapping.Frgu
{
    class GovServiceMap : ClassMapHiLo<GovService>
    {
        public GovServiceMap()
            : base("Gov_Service") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "Gov_Service_Name");
            Map(x => x.Title, "Gov_Service_Title");
            Map(x => x.Number, "Gov_Service_Number");
            Map(x => x.Result, "Gov_Service_Result");

            //References<Gov_Passport>(x => x.Passport, "Gov_Passport_ID");
            //References<Gov_Service>(x => x.Parent, "Gov_Service_Parent").Nullable();

            HasMany<GovServiceNPA>(x => x.Documents).KeyColumns.Add("Gov_Service_ID").Inverse().Cascade.AllDeleteOrphan();
        }
    }
}
