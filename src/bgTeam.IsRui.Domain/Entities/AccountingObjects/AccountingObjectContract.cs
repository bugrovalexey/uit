﻿namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AccountingObjectContract : EntityBase
    {
       // public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string Number { get; set; }

        public virtual DateTime? BeginDate { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual DateTime? ChangeDate { get; set; }

        public virtual AccountingObject AccountingObject { get; set; }
    }
}
