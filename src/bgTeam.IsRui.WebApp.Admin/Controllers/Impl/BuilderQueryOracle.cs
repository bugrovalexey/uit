﻿namespace bgTeam.IsRui.WebApp.Admin.Controllers.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam;
    using bgTeam.IsRui.WebApp.Admin.Domain;
    using bgTeam.DataAccess;

    public class BuilderQueryOracle : IBuilderQuery
    {
        public const string REF_PREFIX = "RF";
        public const string PRIMARY_KEY = "SYSTEM_ID";

        private IAppLogger _logger;
        private IRepository _repositiry;

        public BuilderQueryOracle(
            IAppLogger logger,
            IRepository repositiry)
        {
            _logger = logger;
            _repositiry = repositiry;
        }

        public async Task<string> BuildAsync(MetaDictionary dictionary, IEnumerable<MetaColumn> columns)
        {
            string Columns = string.Concat("th.", PRIMARY_KEY, ",");
            string TablesRef = string.Empty;

            foreach (var item in columns)
            {
                switch (item.Type)
                {
                    case MetaColumnTypeEnum.TString:
                    case MetaColumnTypeEnum.TInt:
                    case MetaColumnTypeEnum.TDecimal:
                    case MetaColumnTypeEnum.TBigString:
                    case MetaColumnTypeEnum.TBool:
                    case MetaColumnTypeEnum.TDateTime:
                        Columns = string.Format("{0}th.{1}, ", Columns, item.ColumnName);
                        break;

                    case MetaColumnTypeEnum.TRef:

                        var rdictionary = await _repositiry.GetAsync<MetaDictionary>(x => x.Id == item.RefId);
                        var rcolumns = await _repositiry.GetAsync<MetaColumn>(x => x.Id == item.RefColumn);

                        Columns = string.Format("{0}th.{2},", Columns, dictionary.TableName, item.ColumnName);
                        Columns = string.Format("{0}{1}.{2} as {4}{3},", Columns, rdictionary.TableName, rcolumns.ColumnName,
                                                item.ColumnName, REF_PREFIX);

                        TablesRef = string.Format("{0} left join {1} on {1}.{2} = th.{4}",
                                                  TablesRef,
                                                  rdictionary.TableName,
                                                  PRIMARY_KEY,
                                                  dictionary.TableName,
                                                  item.ColumnName);
                        break;

                    //case MetaColumnTypeEnum.TChild:
                    //    MetaDictionary cdict = GetDataDictionary(item.RefId, false);
                    //    Columns = string.Format("{0}{1}.{2},", Columns, cdict.ViewName, item.ColumnName);
                    //    break;

                    default:
                        throw new NotSupportedException("Тип не поддерживается: " + item.Type);
                }
            }

            //if (dict.IsUse_InUse)
            //{
            //    Columns = string.Format("{0}{1},", Columns, MetaDictionary.INUSE);
            //}

            Columns = Columns.TrimEnd(',', ' ');

            return LoggerSql(string.Format("select {0} from {1} th {2}", Columns, dictionary.TableName, TablesRef));
        }

        public async Task<string> BuildWithDelete(MetaDictionary dictionary, IEnumerable<MetaColumn> columns)
        {
            var str = await BuildAsync(dictionary, columns);

            return LoggerSql($"{str} where th.DELETED < 1 or th.DELETED is null");
        }

        public async Task<string> BuildItemAsync(MetaDictionary dictionary, IEnumerable<MetaColumn> columns, int itemId)
        {
            var sql = await BuildAsync(dictionary, columns);

            if (dictionary.ReadOnly)
            {
                return LoggerSql(sql);
            }

            return LoggerSql($"{sql} where th.{PRIMARY_KEY} = {itemId}");
        }

        public string BuildGetPrimaryKey(MetaDictionary dict)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("SELECT NVL(MAX({0}), 1) + 1 FROM {1}", PRIMARY_KEY, dict.TableName);

            return sb.ToString();
        }

        public string BuildInsert(MetaDictionary dict, IEnumerable<MetaColumn> columns)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("INSERT INTO {0} ({1}, ", dict.TableName, PRIMARY_KEY);

            foreach (var item in columns)
            {
                sb.AppendFormat("{0}, ", item.ColumnName);
            }

            sb.AppendFormat(") VALUES (:{0}, ", PRIMARY_KEY);

            foreach (var item in columns)
            {
                sb.AppendFormat(":{0}, ", item.ColumnName);
            }

            sb.Append(")");

            return LoggerSql(sb.ToString().Replace(", )", ")"));
        }

        public string BuildUpdate(MetaDictionary dict, IEnumerable<MetaColumn> columns)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("UPDATE {0} SET ", dict.TableName);

            foreach (var item in columns)
            {
                sb.AppendFormat("{0} = :{0}, ", item.ColumnName);
            }

            sb.AppendFormat("WHERE {0} = :{0}", PRIMARY_KEY);

            return LoggerSql(sb.ToString().Replace(", WHERE", " WHERE"));
        }

        public string BuildDelete(MetaDictionary dict, int id)
        {
            return LoggerSql($"DELETE FROM {dict.TableName} WHERE {PRIMARY_KEY} = {id}");
        }

        public string BuildDeleteWithDelete(MetaDictionary dict, int id)
        {
            return LoggerSql($"UPDATE {dict.TableName} SET DELETED = 1 WHERE {PRIMARY_KEY} = {id}");
        }

        public string BuildSelectList(MetaDictionary dict, string fieldValue, string fieldName)
        {
            return LoggerSql($"SELECT {fieldValue}, {fieldName} FROM {dict.TableName} ORDER BY {fieldName}");
        }

        private string LoggerSql(string sql)
        {
            _logger.Debug(sql);

            return sql;
        }

        public string BuildGetPrimaryKey(MetaDictionary dict, MetaColumn column)
        {
            throw new NotImplementedException();
        }

        public string BuildDelete(MetaDictionary dict, MetaColumn column)
        {
            throw new NotImplementedException();
        }

        public string BuildDeleteWithDelete(MetaDictionary dict, MetaColumn column)
        {
            throw new NotImplementedException();
        }
    }
}