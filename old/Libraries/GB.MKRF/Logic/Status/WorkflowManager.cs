﻿using GB.Data;
using GB.DataAccess;
using GB.DataAccess.Command;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;
using System;
//TODO : изменить namespace Logic.Status
using StatusEx = GB.Data.Workflow.Status;

namespace GB.MKRF.Logic.Status
{
    public class WorkflowManager : ManagerBase<IWorkflowHandler, IWorkflowObject>
    {
        protected WorkflowManager() { }
    }
}