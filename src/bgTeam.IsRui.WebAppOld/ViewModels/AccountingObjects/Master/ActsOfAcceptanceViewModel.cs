﻿using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class ActsOfAcceptanceViewModel : StepBaseViewModel
    {
        public ActsOfAcceptanceViewModel(int id)
            : base(id, MasterItemEnum.ActsOfAcceptance)
        {
            GovContract contr = null;

            Acts =
                //new ActOfAcceptanceService().GetAllByAccountingObject(id, Current.Stage)
                new RepositoryDc<ActOfAcceptance>().GetAllEx(q => q.JoinAlias(x => x.GovContract, () => contr)
                    .Where(x => contr.AccountingObject.Id == id && contr.Stage == Current.Stage))
                    .Select(x => new TableActOfAcceptance(x));

            ReadOnly = ExistGovContracts() == false;
        }

        public IEnumerable<TableActOfAcceptance> Acts { get; set; }

        private bool ExistGovContracts()
        {
            AccountingObject ao = null;

            return new RepositoryDc<GovContract>().RowCount(
                q =>
                    q.JoinAlias(x => x.AccountingObject, () => ao).Where(x => x.AccountingObject.Id == Id)
                        .Where(x => x.Stage == ao.Stage)) > 0;
        }
    }
}