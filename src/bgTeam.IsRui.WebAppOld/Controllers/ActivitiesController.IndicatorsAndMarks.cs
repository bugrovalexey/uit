﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Activities;
using System.Web.Mvc;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.IsRui.DataAccess.Services.Args;

namespace bgTeam.IsRui.WebApp.Controllers
{
    partial class ActivitiesController
    {
        public ActionResult IndicatorsAndMarksSingle(int Id)
        {
            return View(new IndicatorsAndMarksViewModel(_statusRepository, Id));
        }

        public ActionResult IndicatorEdit(int activityId, int? id)
        {
            var model = new IndicatorEditViewModel(_statusRepository, activityId, id);
            return View(model);
        }

        [HttpPost]
        public ActionResult IndicatorEdit(PlansActivityIndicatorArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var isNew = args.Id == 0;
                var doc = isNew
                    ? new PlansActivityIndicatorService().Create(args)
                    : new PlansActivityIndicatorService().Update(args);

                var m = new TableIndicator(doc);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteIndicator(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => new PlansActivityIndicatorService().Delete(id));
        }

        public ActionResult MarkEdit(int activityId, int? id)
        {
            var model = new MarkEditViewModel(_statusRepository, activityId, id);
            return View(model);
        }

        [HttpPost]
        public ActionResult MarkEdit(PlansActivityMarkArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var isNew = args.Id == 0;
                var doc = isNew
                    ? new PlansActivityMarkService().Create(args)
                    : new PlansActivityMarkService().Update(args);

                var m = new TableMark(doc);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteMark(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => new PlansActivityMarkService().Delete(id));
        }
    }
}