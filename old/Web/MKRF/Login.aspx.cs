﻿using System;
using System.Web.Security;
using GB.Controls;
using GB.Helpers;
using GB.MKRF.Entities.Admin;
using GB.Repository;

namespace GB.MKRF.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected string PageTitle
        {
            get { return RepositoryBase<Option>.Get(OptionsEnum.PageTitle).Value; }
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            if (!IsPostBack)
            {
                CookieHelper.ClearCookie(FormsAuthentication.FormsCookieName);
            }

            //Ok_Button.Focus();

            base.OnPreRender(e);
        }

        protected void loginForm_Authorize(object sender, LoginControlEventArgs e)
        {
            string username = e.Login;
            string pass = e.Password;
            if (Membership.Provider != null && Membership.Provider.ValidateUser(username, pass))
            {
                FormsAuthentication.RedirectFromLoginPage(username, true);
            }
            else
            {
                e.Cancel = true;
                div_error_panel.InnerText = "Неправильное имя пользователя или пароль!";
                div_error_panel.Attributes.Add("class", "curved");
            }
        }

        //public void RaisePostBackEvent(string eventArgument)
        //{
        //    if (eventArgument.Equals("Password"))
        //    {
        //        if (Membership.Provider != null && Membership.Provider.ValidateUser(Login_TextBox.Text, Password_TextBox.Text))
        //        {
        //            div_error_panel.Attributes["class"] = "none";
        //            div_error_panel.InnerHtml = null;

        //            FormsAuthentication.RedirectFromLoginPage(Login_TextBox.Text, true);
        //        }
        //        else
        //        {
        //            div_error_panel.Attributes["class"] = string.Empty;
        //            div_error_panel.InnerHtml = "Указанный вами пароль неверен.";
        //        }
        //    }
        //}
    }
}