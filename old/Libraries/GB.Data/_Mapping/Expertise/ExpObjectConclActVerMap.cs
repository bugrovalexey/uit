﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.OtherExpObjects;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpObjectConclActVerMap : ClassMapBase<ExpObjectConclActVer>
	{
        public ExpObjectConclActVerMap() : base("Exp_Concl_Act_Other_Ver"){}
   
        protected override void InitMap()
        {
            Map(x => x.Disagreement, "Exp_Concl_Act_Other_Ver_Disagreement");
            Map(x => x.AppropriateVolumeFinanceY0, "Exp_Concl_Act_Other_Ver_VF_Y0");
            Map(x => x.AppropriateVolumeFinanceY1, "Exp_Concl_Act_Other_Ver_VF_Y1");
            Map(x => x.AppropriateVolumeFinanceY2, "Exp_Concl_Act_Other_Ver_VF_Y2");
            Map(x => x.IsNeedFunding, "Exp_Concl_Act_Other_Ver_Is_Need_Funding");
            Map(x => x.Completion, "Exp_Concl_Act_Other_Ver_Completion");

            References<ExpObjectConclusionVer>(x => x.ExpConclusionVer, "Exp_Obj_Conclusion_Ver_ID").Not.Nullable();
			References<ExpActEquivalent>(x => x.ExpActEquivalent, "Exp_Act_Equivalent_ID").Not.Nullable();

            References<Appropriate>(x => x.GoalEffective, "Goal_Effective_ID");
            References<Appropriate>(x => x.OrderFinance, "Order_Finance_ID");

   

            HasMany<ExpObjectCriterionEquivalent>(x => x.ExpCriterionEquivalent)
                    .KeyColumns.Add("Exp_Concl_Act_Other_ID")
                    .Inverse()
                    .Cascade.AllDeleteOrphan();
        }
    }
}