﻿namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Отчётные формы по проектной деятельности
    /// </summary>
    public class ReportForm : EntityBase
    {
        protected ReportForm()
        {
        }

        public ReportForm(Plan Plan)
        {
            this.Plan = Plan;
        }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Описание</summary>
        public virtual string Description { get; set; }

        /// <summary>Периодичность формирования</summary>
        public virtual string Period { get; set; }

        /// <summary>Ответственный за подготовку</summary>
        public virtual ManagementRole Preparation { get; set; }

        /// <summary>Ответственный за согласование</summary>
        public virtual ManagementRole Coordination { get; set; }

        /// <summary>Ответственный за утверждение</summary>
        public virtual ManagementRole Statement { get; set; }

        /// <summary> План</summary>
        public virtual Plan Plan { get; protected set; }
    }
}
