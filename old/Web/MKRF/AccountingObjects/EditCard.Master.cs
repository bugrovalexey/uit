﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB;
using GB.Helpers;
using GB.MKRF.Entities.AccountingObjects;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Web.AccountingObjects
{
    public partial class EditCard : System.Web.UI.MasterPage
    {
        #region consts

        public static readonly string AccountingObjectIdKey = "AccountingObjectId";

        #endregion consts
        
        #region Fields

        private AccountingObject _accountingObject;

        #endregion Fields

        #region Props

        public int? AccountingObjectId
        {
            get
            {
                int accountingObjectId;

                if (int.TryParse(Request.QueryString[AccountingObjectIdKey], out accountingObjectId))
                {
                    return accountingObjectId;
                }

                return null;
            }
        }

        /// <summary>
        /// Текущий объект учета
        /// </summary>
        public AccountingObject CurrentAccountingObject
        {
            get
            {
                if (_accountingObject == null)
                {
                    try
                    {
                        _accountingObject = RepositoryBase<AccountingObject>.Get(AccountingObjectId);
                    }
                    catch (Exception exp)
                    {
                        Logger.Fatal("Ошибка получения AccountingObject", exp);
                        ResponseHelper.DenyAccess(this.Response);
                    }
                }

                return _accountingObject;
            }
        }

        #endregion Props

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            IList<string> urlParameters = new List<string>();

            // Настройка ссылок
            foreach (Tab t in tc.Tabs)
            {
                urlParameters.Clear();

                if (!t.Visible)
                    continue;

                urlParameters.Add(AccountingObjectIdKey + "=" + CurrentAccountingObject.Id);
                t.NavigateUrl += (t.NavigateUrl.Contains("?") ? "&" : "?") + string.Join("&", urlParameters);
            }

            AccountingObjectLabel.DataBind();
        }

        protected void AccountingObjectLabel_OnDataBinding(object sender, EventArgs e)
        {
            (sender as ASPxLabel).Text = CurrentAccountingObject.ShortName;
        }
    }
}