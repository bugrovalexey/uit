﻿using GB.Data.AccountingObjects;
using GB.Data.Plans;
using GB.MKRF.Logic.Status.Handler;

namespace GB.MKRF.Logic.Status
{
    /// <summary>
    /// Регистрация менеджера, логика по переводу статусов объекта
    /// </summary>
    public class WorkflowRegistration : ManagerRegistrationBase<WorkflowManager>
    {
        protected override void RegisterHandlers(WorkflowManager manager)
        {
            manager.RegisterHandler<PlansActivity>(new ActivityWorkflowHandler());
            manager.RegisterHandler<ActivityExpertiseConclusion>(new ExpertiseWorkflowHandler());

            manager.RegisterHandler<AccountingObject>(new AccountingObjectWorkflowHandler());
        }
    }
}