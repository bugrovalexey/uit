﻿namespace bgTeam.IsRui.Domain.Entities.Directories
{
    using System;

    /// <summary>
    /// Тип затрат
    /// </summary>
    public class ExpenseType : EntityDirectories, IEntityName
    {
        /// <summary> Дата начала действия справочника </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary> Дата окончания действия справочника </summary>
        public virtual DateTime? DateEnd { get; set; }

        public virtual ExpenseDirection ExpenseDirection { get; set; }
    }
}