﻿using bgTeam.IsRui.Common;
using bgTeam.IsRui.Zakupki360.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace bgTeam.IsRui.Zakupki360.Impl
{
    public class ClientSystems : IClientSystems
    {
        private readonly string Host;
        private readonly string Key;

        internal ClientSystems(string host, string key)
        {
            Host = host;
            Key = key;
        }

        public void Test()
        {
            var result = GetRequestResult(new TestGetOrders
            {
                Filter = new OrdersSearchForm()
                {
                    CustomerFilters = new List<CustomerFilter>() { new CustomerFilter() { Inn = "7705596339", Kpp = "770501001" } },
                    PublishDateFrom = new DateTime(2015, 1, 1),
                    PublishDateTo = DateTime.Now
                },
                ApiKey = "3f42807K7G892JDf2wl223nav"
            }, "GetOrders");
        }

        class TestGetOrders
        {
            public OrdersSearchForm Filter { get; set; }

            public string ApiKey { get; set; }
        }

        public class OrdersSearchForm
        {
            public IEnumerable<CustomerFilter> CustomerFilters { get; set; }

            public IEnumerable<string> ProductsCodes { get; set; }

            public string KbkCodeTemplate { get; set; }

            public string SphinxSearchTerm { get; set; }

            public DateTime? PublishDateFrom { get; set; }

            public DateTime? PublishDateTo { get; set; }
        }

        public class CustomerFilter
        {
            public string Inn { get; set; }

            public string Kpp { get; set; }
        }

        public IEnumerable<OrderInfo> GetOrders(SearchParametersOrders filter, out string error)
        {
            var result = GetRequestResult(new RequestGetOrders { Filter = filter, ApiKey = Key }, "GetOrdersEx");

            try
            {
                var res = JsonConvert.DeserializeObject(result, typeof(ResponseGetOrders),
                    new IsoDateTimeConverter { DateTimeFormat = @"dd.MM.yyyy" }) as ResponseGetOrders;

                error = res.Error;
                return res.Orders ?? Enumerable.Empty<OrderInfo>();
            }
            catch (Exception exp)
            {
                error = exp.Message;
                //Logger.Error(exp);
            }

            return Enumerable.Empty<OrderInfo>();
        }

        public IEnumerable<ContractInfo> GetConracts(SearchParametersContracts filter, out string error)
        {
            var result = GetRequestResult(new RequestGetConracts { Filter = filter, ApiKey = Key }, "GetConracts");

            try
            {
                var res = JsonConvert.DeserializeObject(result, typeof(ResponseGetConracts),
                        new IsoDateTimeConverter { DateTimeFormat = @"dd.MM.yyyy" }) as ResponseGetConracts;

                error = res.Error;
                return res.Contracts ?? Enumerable.Empty<ContractInfo>();
            }
            catch (Exception exp)
            {
                error = exp.Message;
                //Logger.Error(exp);
            }

            return Enumerable.Empty<ContractInfo>();
        }


        private string GetRequestResult(object requestObject, string action)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Concat(Host, action));

            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            var postData = JsonConvert.SerializeObject(requestObject);

            using (Stream requestStream = request.GetRequestStream())
            using (StreamWriter writer = new StreamWriter(requestStream))
            {
                writer.Write(postData);
            }

            using (var response = (HttpWebResponse)request.GetResponse())
            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
