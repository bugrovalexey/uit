﻿using bgTeam.DataAccess;
using bgTeam.DataAccess.Impl;
using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.MKRF.Providers;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace bgTeam.IsRui.WebApp.Infrastructure.Installers
{
    public class CommandInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var command1 = Types.FromAssemblyContaining<IDataAccessLibrary>()
                .BasedOn(typeof(ICommand<>))
                .WithService.AllInterfaces()
                .Configure(x => x.LifestyleTransient());

            var command2 = Types.FromAssemblyContaining<IDataAccessLibrary>()
                .BasedOn(typeof(ICommand<,>))
                .WithService.AllInterfaces()
                .Configure(x => x.LifestyleTransient());

            container.Register(command1, command2,
                Component.For<IAccessControllerProvider>().ImplementedBy<AccessControllerProvider>().LifestyleSingleton(),
                Component.For<IConnectionSetting>().ImplementedBy<AppSettings>(),
                Component.For<ICommandBuilder>().ImplementedBy<CommandBuilder>(),
                Component.For<ICommandFactory>().AsFactory().LifestyleTransient());
        }
    }
}