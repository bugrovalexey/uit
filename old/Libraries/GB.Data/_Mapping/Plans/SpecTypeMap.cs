﻿using GB.Data.Directories;
using GB.Data.Plans;

namespace GB._Mapping.Plans
{
    internal class SpecTypeMap : ClassMapBase<SpecType>
    {
        public SpecTypeMap() :
            base("Spec_Exp_Type")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.CharName, "Spec_Exp_Type_Name");
            Map(x => x.CharUnit, "Spec_Exp_Type_Unit");
            Map(x => x.CharValue, "Spec_Exp_Type_Value");
            Map(x => x.Analog, "Spec_Exp_Type_Analog");
            Map(x => x.Quantity, "Spec_Exp_Type_Quantity");
            Map(x => x.AnalogPrice, "Spec_Exp_Type_Analog_Price");

            Map(x => x.ExpenseVolumeYear0, "Spec_Exp_Type_Expense_Vol_Y0");
            Map(x => x.ExpenseVolumeYear1, "Spec_Exp_Type_Expense_Vol_Y1");
            Map(x => x.ExpenseVolumeYear2, "Spec_Exp_Type_Expense_Vol_Y2");

            Map(x => x.Comment, "Spec_Exp_Type_Comment");

            References<PlansSpec>(x => x.PlanSpec, "Plans_Spec_ID");
            References<ExpenseDirection>(x => x.ReportExpenseDirection, "Report_Expense_Direction_ID").Nullable();
        }
    }
}