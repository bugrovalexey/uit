﻿using Uviri.MKRF.Entities.Prognosis.SectionII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionII
{
    public class OtherSoftMap : ClassMapBase<OtherSoft>
	{
        public OtherSoftMap()
            : base("MF2_Other_Soft")
        {
//            Table("w_");

//            Id(x => x.Id, "MF2_Other_Soft_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

            

//            SqlInsert(@"exec ZZZ_Prc_MF2_Other_Soft_Insert
//                          @MF2_Other_Soft_Name = ?
//                         ,@MF2_Other_Soft_CA = ?
//                         ,@MF2_Other_Soft_TP = ?
//                         ,@MF2_ID = ?
//                         ,@MF2_Other_Soft_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF2_Other_Soft_Update
//                          @MF2_Other_Soft_Name = ?
//                         ,@MF2_Other_Soft_CA = ?
//                         ,@MF2_Other_Soft_TP = ?
//                         ,@MF2_ID = ?
//                         ,@MF2_Other_Soft_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF2_Other_Soft_Delete
//						  @MF2_Other_Soft_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "MF2_Other_Soft_Name");
            Map(x => x.CountCA, "MF2_Other_Soft_CA");
            Map(x => x.CountTP, "MF2_Other_Soft_TP");
            
            References<TechInfo>(x => x.TechInfo, "MF2_ID").Not.Nullable();
        }
    }
}