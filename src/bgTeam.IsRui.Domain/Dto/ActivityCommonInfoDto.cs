﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityCommonInfoDto
    {
        public int? ActivityId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int ResponsibleDepartmentId { get; set; }

        public string ResponsibleDepartmentName { get; set; }

        //public int? ResponsibleInformationId { get; set; }

        public int? SigningId { get; set; }
    }
}
