﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Command.AccountingObjects.WorkAndGood;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.ActsOfAcceptance
{
    public class DeleteActOfAcceptanceCommand : ICommand<DeleteActOfAcceptanceContext>
    {
        ISessionFactoryGb factory;

        public DeleteActOfAcceptanceCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteActOfAcceptanceContext context)
        {            
            if (context.xId.HasValue && context.xId != 0)
            {
                var entity = new Repository<ActOfAcceptance>().Get(context.xId.Value);

                if (entity != null)
                {
                    new DeleteWorkAndGoodsByActCommand().Execute(new DeleteWorkAndGoodsByActContext() { xId = context.xId.Value });

                    using (var session = factory.Create())
                    {                        
                        session.Delete(entity);
                        session.Commit();
                    }
                }
            }
        }
    }
}
