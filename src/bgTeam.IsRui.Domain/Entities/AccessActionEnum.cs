﻿namespace bgTeam.IsRui.Domain.Entities
{
    using System;
    using System.ComponentModel;

    [Flags]
    public enum AccessActionEnum
    {
        [Description("Запрещен")]
        None = 0,

        [Description("Чтение")]
        Read = 1,

        [Description("Редактирование")]
        Edit = 2,

        [Description("Удаление")]
        Delete = 4,

        [Description("Создание")]
        Create = 8,

        //!!!Внимание!!!
        //При добавлении новых значений, добавить их в All
        [Description("Полный доступ")]
        All = Read | Edit | Delete | Create,
    }
}