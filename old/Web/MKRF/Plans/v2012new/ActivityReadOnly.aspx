﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Plans/v2012new/ActivityCardCommon.master" CodeBehind="ActivityReadOnly.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.ActivityReadOnly" %>

<%@ Register Src="~/Common/ActivityReadMode.ascx" TagPrefix="arm" TagName="ActivityReadMode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">

    <script type="text/javascript" src="/Content/GB/Scripts/jquery.nav.js"></script>
    <script type="text/javascript">
        $(function () {
            var menu = $('#activityMenu');
            var detailPanelHeight = $('.detailTopFull').height() + 2;
            menu.onePageNav({ scrollOffset: detailPanelHeight, scrollThreshold: 0.1 });

            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });

            $('#back-top a').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
        });
    </script>
    <div class="mblock">
<%--        <div class="lblock">--%>
<%--            <dxtc:ASPxTabControl runat="server" ID="tc" ActiveTabIndex="0" EnableViewState="false" TabPosition="Left">--%>
<%--                <Tabs>--%>
<%--                    <dxtc:Tab Text="Общие сведения" NavigateUrl="#CommonData" />--%>
<%--                    $1$<dxtc:Tab Text="Функции" NavigateUrl="#Functions" />#1#--%>
<%--                    <dxtc:Tab Text="Целевые показатели" NavigateUrl="#Indicators" />--%>
<%--                    $1$<dxtc:Tab Text="Специальные сведения" NavigateUrl="#SpecialInformation" />#1#--%>
<%--                    <dxtc:Tab Text="Расходы" NavigateUrl="#Expenses" />--%>
<%--                    <dxtc:Tab Text="Результаты" NavigateUrl="#Results" />--%>
<%--                    <dxtc:Tab Text="Работы" NavigateUrl="#Works" />--%>
<%--                    <dxtc:Tab Text="Товары" NavigateUrl="#Specification" />--%>
<%--                </Tabs>--%>
<%--                <TabStyle>--%>
<%--                    <Paddings PaddingBottom="10" PaddingTop="10" />--%>
<%--                </TabStyle>--%>
<%--                <Paddings Padding="0px" />--%>
<%--                <ClientSideEvents TabClick="function(s, e){--%>
<%--                        if (e.htmlElement.link0.hash) {--%>
<%--                            $(e.htmlElement.link0.hash).removeClass('collapsed').addClass('expanded');--%>
<%--                            $(e.htmlElement.link0.hash).next('.item').slideDown();--%>
<%--                        }--%>
<%--                    }"></ClientSideEvents>--%>
<%--            </dxtc:ASPxTabControl>--%>
<%--        </div>--%>
<%--        <div class="clear"></div>--%>
        <div class="lblock">
            <ul id="activityMenu" class="lblock-width">
                <li><a class="current" href="#CommonData">Общие сведения</a></li>
                <li><a href="#Functions">Функции</a></li>
                <li><a href="#Indicators">Показатели/Индикаторы</a></li>
                <li><a href="#Expenses">Расходы</a></li>
                <li><a href="#Results">Результаты</a></li>
                <li><a href="#Works">Работы</a></li>
                <li><a href="#Specification">Товары</a></li>
                <li><a href="#InitialActivity">Исходные меропритятия</a></li>
                <% if(CurrentActivity.Plan.PlanType == GB.MKRF.Entities.Directories.ExpObjectTypeEnum.Plan2012rel) { %>
                    <li><a href="#Purchases">Закупки</a></li>
                <% } %>
            </ul>
        </div>
        <div class="rblock">
            <arm:ActivityReadMode runat="server" ID="ActivityReadMode" DefaultShowEditLinks="True" DefaultShowCollapseLink="True"/>
        </div>
    </div>
    <p id="back-top" style="display: none;">
        <a href="#top">
            <span></span>
            в начало
        </a>
    </p>
    <div class="clear"></div>
</asp:Content>

