﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GetGovtOrganHierarchyStory : IStory<GetGovtOrganHierarchyStoryContext, IEnumerable<ActivityDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IUserRepository _userRepository;
        private readonly IRepositoryRui _repository;

        public GetGovtOrganHierarchyStory(IMapperBase mapper, IUserRepository userRepository, IRepositoryRui repository)
        {
            _mapper = mapper;
            _userRepository = userRepository;
            _repository = repository;
        }

        public IEnumerable<ActivityDto> Execute(GetGovtOrganHierarchyStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivityDto>> ExecuteAsync(GetGovtOrganHierarchyStoryContext context)
        {
            //using (var session = _factory.OpenSession())
            //{
            if (!context.Id.HasValue)
            {
                var user = _userRepository.GetCurrent();

                context.Id = user.Department.Id;
            }

            var query = await _repository.SelectExecuteAsync<DepartmentProc>(
                "EXEC GetGovtOrganHierarchy :Id",
                new[] { new ParameterProcedure("Id", context.Id.Value, new NHibernate.Type.Int32Type()) });

            var depart = _mapper.Map(query, new List<DepartmentDtoProc>());

            var result = new List<PlansActivity>();

            foreach (DepartmentDtoProc dep in depart)
            {
                var activities = _repository.GetAllEx<PlansActivity>(q =>
                q.Where(x => x.Department.Id == dep.Id));

                result.AddRange(activities);
            }

            return _mapper.Map(result, new List<ActivityDto>());
            //}
        }
    }
}
