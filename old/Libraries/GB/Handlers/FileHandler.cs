﻿using GB.Helpers;
using System;
using System.Text;
using System.Web;

namespace GB.Handlers
{
    /// <summary>
    /// Хэндлер загруженных файлов
    /// </summary>
    public abstract class FileHandler : IHttpHandler
    {
        public class File_Info
        {
            private object id_internal = null;

            public object Id
            {
                get
                {
                    return id_internal;
                }
                set
                {
                    if (value == null || value is Int32 || value is Guid)
                    {
                        id_internal = value;
                    }
                    else
                    {
                        throw new ArgumentException(value.ToString());
                    }
                }
            }

            public string FileName { get; set; }

            public static File_Info Parse(string shortUrl)
            {
                if (string.IsNullOrWhiteSpace(shortUrl))
                    return null;

                string[] urlParts = shortUrl.Split('/');

                if (urlParts.Length <= 0)
                    return null;

                int id = 0;
                Guid guid;
                File_Info info = new File_Info();

                if (int.TryParse(urlParts[0], out id))
                {
                    info.Id = id;
                }
                else if (Guid.TryParse(urlParts[0], out guid))
                {
                    info.Id = guid;
                }
                else
                {
                    // если не удаётся определить ID, то весь путь к файлу считается как FileName,
                    // а в Id оставляем null. Необходимо для совместимости с HtmlEditor на портале
                    info.FileName = shortUrl;
                }

                if (info.Id != null && urlParts.Length > 1)
                {
                    info.FileName = urlParts[1];
                }

                return info;
            }
        }

        public enum StorageTypeEnum
        {
            FileServer = 0,
            Database = 1
        }

        private const string urlPrefix = @"/files/";
        private const string urlRoot = @"~/files/";

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest Request = context.Request;

            File_Info info = ParseUrl(Request.RawUrl);

            if (info == null)
            {
                return; // вернуть ошибку
            }

            StorageTypeEnum storageType = GetStorageType(info);

            if (storageType == StorageTypeEnum.Database)
            {
                SentFileFromDatabase(context, info);
            }
            else
            {
                SentFileFromFileServer(context, info);
            }
        }

        protected virtual void SentFileFromFileServer(HttpContext context, File_Info info)
        {
            // извлекаем Document по ид, проверяем доступ к нему и отсылаем файл с именем, взятым из Document.FileName
            string physicalPath = FileHelper.GetPhysicalPath((info.Id != null) ? info.Id : info.FileName);
            string fileName = null;

            Logger.Debug("Попытка скачать файл - " + physicalPath);

            if (!System.IO.File.Exists(physicalPath))
            {
                var error_txt = string.Format("Файл не найден id={0} filename={1} path={2}", (info != null && info.Id != null) ? info.Id.ToString() : "null",
                                                                                                         (info != null) ? info.FileName : "null",
                                                                                                         physicalPath);

                Logger.Debug(error_txt);
                ResponseHelper.FileNotFound(context.Response, error_txt);

                return;
            }

            if (FileHelper.IsPublic(info.Id))
            {
                fileName = string.IsNullOrEmpty(info.FileName) ? GetFileName(info) : info.FileName;
            }
            else
            {
                if (info.Id != null)
                    // получаем имя файла / проверяем доступ
                    fileName = GetFileName(info);
                else
                    // если файл не надится в БД,
                    // скачиваем по прямой ссылке
                    fileName = info.FileName;

                // Если имя не задано == файл не доступен
                if (string.IsNullOrEmpty(fileName))
                {
                    ResponseHelper.AccessDenied(context.Response,
                                                string.Format("Отказ в получении файла id={0}", info.Id != null ? info.Id.ToString() : "null"));
                    return;
                }
            }

            Logger.Debug("Отправляем файл");
            // файл прошёл проверку или общедоступный - отдаём клиенту
            FileHelper.SendFile(context, physicalPath, fileName);
        }

        protected virtual void SentFileFromDatabase(HttpContext context, File_Info info)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Возвращает имя файла по Id доукмента
        /// Если документ не доступен возвращает null
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <returns></returns>
        protected abstract string GetFileName(File_Info info);

        protected abstract StorageTypeEnum GetStorageType(File_Info info);

        /// <summary>
        /// Разбирает ссылку на файл и извлекает из неё ИД документа и
        /// имя файла (если есть). Возвращает null, если отсутствует ИД
        ///
        /// Файл в ссылке всегда должен начинаться с идентификатора файла - числа или GUID,
        /// за которым через '/' может следовать имя файла.
        /// Пример: "files/65_мероприятие1.jpg",  "files/7323f90c-4809-4688-93e5-31c23bf7c070/document_1.pdf"
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        public static File_Info ParseUrl(string Url)
        {
            if (string.IsNullOrEmpty(Url))
                return null;

            int slashIndex = Url.LastIndexOf(urlPrefix, StringComparison.InvariantCultureIgnoreCase);

            if (slashIndex < 0)
                slashIndex = 0;
            else
                slashIndex += urlPrefix.Length;

            string shortUrl = Url.Substring(slashIndex);

            File_Info info = File_Info.Parse(shortUrl);

            return info;
        }

        /// <summary>
        /// Формирует ссылку на файл
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetUrl(object id, string name = null, string urlRoot = urlRoot)
        {
            if (id == null)
            {
                return UrlHelper.Resolve(urlRoot);
                //throw new ArgumentException("Необходимо указать Id файла");
            }

            //            if (!FileHelper.IsExist(id))
            //            {
            //                return UrlHelper.Resolve("~/") + "errorfile";
            //            }

            StringBuilder builder = new StringBuilder();

            builder.Append(UrlHelper.Resolve(urlRoot));
            builder.Append(id);

            if (!string.IsNullOrEmpty(name))
            {
                builder.Append("/");
                builder.Append(name);
            }

            return builder.ToString();
        }

        public static string GetLink(string id, string name = null)
        {
            return "<a href='" + FileHandler.GetUrl(id) + "'>" + (name ?? "Скачать") + "</a>";
        }
    }
}