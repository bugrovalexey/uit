﻿namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    using System;
    using System.Collections.Generic;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Workflow;

    /// <summary>
    /// Комментарий пользователя к ОУ
    /// </summary>
    public class AccountingObjectUserComment : EntityBase
    {
        public AccountingObjectUserComment()
        {
            Documents = new List<Files>();
        }

        /// <summary>Комментарий</summary>
        public virtual string Comment { get; set; }

        /// <summary>ОУ</summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>Пользователь</summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public virtual DateTime CreateDate { get; set; }

        /// <summary>
        /// Документ
        /// </summary>
        public virtual IList<Files> Documents { get; set; }

        //public virtual int? StatusId { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual Status Status { get; set; }
    }
}
