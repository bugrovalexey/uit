﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Claims;

    public class ClaimDeleteScopeOfWorkStory : ClaimDeleteBaseStory<ClaimScopeOfWork, ClaimDeleteScopeOfWorkStoryContext>
    {
        public ClaimDeleteScopeOfWorkStory(
            IRepositoryRui repositoryRui,
            ISessionNHibernateFactory sessionNHibernateFactory)
            : base(repositoryRui, sessionNHibernateFactory)
        {
        }
    }
}
