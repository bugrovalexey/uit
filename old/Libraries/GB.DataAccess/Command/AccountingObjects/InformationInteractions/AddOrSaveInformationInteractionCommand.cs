﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.InformationInteractions
{
    public class AddOrSaveInformationInteractionCommand : ICommand<AddOrSaveInformationInteractionContext, InformationInteraction>
    {
        ISessionFactoryGb factory;

        public AddOrSaveInformationInteractionCommand()
        {
            factory = new SessionFactory();
        }

        public InformationInteraction Execute(AddOrSaveInformationInteractionContext context)
        {
            InformationInteraction infInt;

            if (context.xId.HasValue && context.xId != 0)
                infInt = new Repository<InformationInteraction>().Get(context.xId.Value);
            else
            {
                infInt = new InformationInteraction();                
            }

            AutoMapper.Mapper.Map(context, infInt);
            
            using (var session = factory.Create())
            {
                session.SaveOrUpdate(infInt);
                session.Commit();
            }            

            return infInt;
        }
    }
}
