﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System.Collections.Generic;
    using bgTeam.IsRui.Domain.Entities.Plans;

    public class AoActivityDto
    {
        /// <summary>ИД</summary>
        public int Id { get; set; }

        /// <summary>ОУ</summary>
        public int AoId { get; set; }

        /// <summary>Наименование</summary>
        public string Name { get; set; }

        /// <summary>Год</summary>
        public short Year { get; set; }

        /// <summary>Стадия</summary>
        public AccountingObjectsStageEnum Stage { get; set; }

        /// <summary>Тип</summary>
        public ActivityTypeEnum ActivityType { get; set; }

        /// <summary>Планируемые расходы - Очередной финансовый год</summary>
        public decimal CostY0 { get; set; }

        /// <summary>Планируемые расходы - Первый год планового периода</summary>
        public decimal CostY1 { get; set; }

        /// <summary>Планируемые расходы - Второй год планового периода</summary>
        public decimal CostY2 { get; set; }


        /// <summary>Мероприятие</summary>
        public int PlansActivity { get; set; }

    }
}
