﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Services.Args
{
    public class AOAdoptionBudgetArgs : BaseArgsNew
    {
        /// <summary>
        /// Дата принятия к бюджетному учету
        /// </summary>
        public DateTime? AdoptionBudgetDate { get; set; }

        /// <summary>Код по БК ГРБС</summary>
        public int? BK_GRBS { get; set; }

        /// <summary>Код по БК ПЗ/РЗ</summary>
        public int? BK_PZRZ { get; set; }

        /// <summary>Код по БК ЦСР</summary>
        public int? BK_CSR { get; set; }

        /// <summary>Код по БК ВР</summary>
        public int? BK_WR { get; set; }

        /// <summary>
        /// Балансовая стоимость
        /// </summary>
        public decimal BalanceCost { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string DocName { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string DocNumber { get; set; }

        /// <summary>
        /// Дата принятия документа
        /// </summary>
        public DateTime? DocDateAdoption { get; set; }

        /// <summary>
        /// Пункт, статья документа
        /// </summary>
        public string DocParagraph { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public string Files { get; set; }

    }
}
