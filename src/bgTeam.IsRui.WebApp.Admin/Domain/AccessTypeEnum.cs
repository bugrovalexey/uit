﻿namespace bgTeam.IsRui.WebApp.Admin.Domain
{
    using System;

    [Flags]
    public enum AccessTypeEnum
    {
        None = 0,
        Add = 1,
        Edit = 2,
        Delete = 4
    }
}