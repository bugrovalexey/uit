﻿using System;
using System.Collections.Generic;
using System.Web.Security;
using GB.Entity;
using GB.Repository;

namespace GB.Providers
{
    public class DbRoleProvider : RoleProvider
    {
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            //Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("admin"), null);
            base.Initialize(name, config);
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            var roles = RepositoryBase<RoleBase>.GetAll();

            return getRolesNames(roles);
        }

        public override string[] GetRolesForUser(string username)
        {
            var roles = new List<RoleBase>();
            
            var user = UserRepositoryBase.GetCurrentEx();

            if (user != null && user.Role != null)
                roles.Add(user.Role);

            return getRolesNames(roles);
        }
        
        private string[] getRolesNames(IList<RoleBase> roles)
        {
            if (roles == null || roles.Count <= 0)
                return new string[0];

            string[] names = new string[roles.Count];

            for (int idx = 0; idx < roles.Count; idx++)
            {
                names[idx] = roles[idx].Name;
            }

            return names;            
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}