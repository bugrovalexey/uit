﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ValuationMap : ClassMapBase<Valuation>
    {
        public ValuationMap():
            base("Valuation")
        {

        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Valuation_Name").Not.Nullable();
        }
    }
}
