﻿using System;
using GB.Data.Common;
using GB.Data.Plans;
using GB.DataAccess.Repository;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class ActivityExpertService : ServiceBase<ActivityExpert>, IService<ActivityExpert, ActivityExpertArgs>
    {
        public ActivityExpert Create(ActivityExpertArgs args)
        {
            var e = new ActivityExpert(new Repository<ActivityExpertiseConclusion>().Get(args.Conclusion));

            e.Expert = GetEntity<User>(args.User);
            e.Role = (ActivityExpertEnum)args.Role;

            return base.SaveOrUpdate(e);
        }

        public ActivityExpert Update(ActivityExpertArgs args)
        {
            throw new NotImplementedException();
        }

        public void Delete(ActivityExpertArgs args)
        {
            if (args.Id.HasValue)
                base.Delete(args.Id.Value);
        }
    }

    public class ActivityExpertArgs :  IServiceArgs
    {
        public int? Id { get; set; }

        public int Conclusion { get; set; }

        public int Role { get; set; }

        public int User { get; set; }
    }
}