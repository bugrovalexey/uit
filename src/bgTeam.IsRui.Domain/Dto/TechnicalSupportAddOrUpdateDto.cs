﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class TechnicalSupportAddOrUpdateDto
    {
        public int? Id { get; set; }

        public int? AoId { get; set; }

        public virtual int? Amount { get; set; }

        public int CategoryKindOfSupportId { get; set; }

        public IList<CharacteristicOfTechnicalSupportGetDto> Characteristics { get; set; }

        public int? ManufacturerId { get; set; }

        public string ManufacturerCustomName { get; set; }

        public string Name { get; set; }

        public decimal? Summa { get; set; }
    }
}
