﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Mapping.Admin
{
    public class TooltipMap : ClassMapBase<Tooltip>
	{
        public TooltipMap()
            : base("Tooltip")
		{
//            Table("");

//            Id(x => x.Id, "Tooltip_ID").GeneratedBy.Assigned();

			

//            SqlInsert(@"exec ZZZ_Prc_Tooltip_Insert
//						  @Tooltip_Name = ?
//						 ,@Tooltip_Content = ?
//						 ,@Tooltip_Width = ?
//						 ,@Tooltip_Height = ?
//						 ,@Tooltip_Corner = ?
//						 ,@Tooltip_Target_Corner = ?
//						 ,@Tooltip_ID = ?").Check.None();
			
//            SqlUpdate(@"exec ZZZ_Prc_Tooltip_Update
//						  @Tooltip_Name = ?
//						 ,@Tooltip_Content = ?
//						 ,@Tooltip_Width = ?
//						 ,@Tooltip_Height = ?
//						 ,@Tooltip_Corner = ?
//						 ,@Tooltip_Target_Corner = ?
//						 ,@Tooltip_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Tooltip_Delete
//						  @Tooltip_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Tooltip_Name").Not.Nullable();
			Map(x => x.Content, "Tooltip_Content").Nullable();
			Map(x => x.Width, "Tooltip_Width").Not.Nullable();
			Map(x => x.Height, "Tooltip_Height").Nullable();
			Map(x => x.TooltipCorner, "Tooltip_Corner").Not.Nullable();
			Map(x => x.TargetCorner, "Tooltip_Target_Corner").Nullable();
        }
    }
}