﻿using System.Linq;
using AutoMapper;
using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.WorksAndGoods;
using GB.Data.Directories;
using GB.DataAccess.Command.AccountingObjects;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class SoftwareService : ServiceBase<Software>, IService<Software, SoftwareArgs>
    {
        public Software Create(SoftwareArgs args)
        {
            var entity = new Software();
            var result = SaveOrUpdate(entity, args);

            args.Id = result.Id;
            new WorksAndGoodsService().Create(args);

            return result;
        }

        public Software Update(SoftwareArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            var result = SaveOrUpdate(entity, args);

            new WorksAndGoodsService().Update(args);

            return result;
        }

        private Software SaveOrUpdate(Software entity, SoftwareArgs args)
        {
            Manufacturer man;

            if (args.ManufacturerId.HasValue == false && string.IsNullOrWhiteSpace(args.ManufacturerName) == false)
            {
                var мanufacturerName = args.ManufacturerName.Trim();
                man =
                    new Repository.Repository<Manufacturer>().GetAllEx(
                        q => q.Where(x => x.Name == мanufacturerName)).FirstOrDefault();

                if (man == null)
                {
                    man = new CrudService<Manufacturer, EntityDirectoriesArgs>().Create(new EntityDirectoriesArgs()
                    {
                        Name = мanufacturerName
                    });
                }

                ConvertDtoToEntity.FillSoftwareFromArgs(entity, args);
                entity.Manufacturer = man;
            }
            else
            {
                ConvertDtoToEntity.FillSoftwareFromArgs(entity, args);
            }


            SaveOrUpdate(entity);

            var characteristicOfLicenseService =
                new CrudService<CharacteristicOfLicense, CharacteristicOfLicenseArgs>();
            var isNew = args.IdCharacteristicOfLicense == 0;

            if (entity.Right == RightsOnSoftwareEnum.NotExclusive)
            {
                var characteristicOfLicenseArgs = Mapper.Map<CharacteristicOfLicenseArgs>(args);
                characteristicOfLicenseArgs.Software = entity;

                var characteristic = isNew
                    ? characteristicOfLicenseService.Create(characteristicOfLicenseArgs)
                    : characteristicOfLicenseService.Update(characteristicOfLicenseArgs);

                entity.CharacteristicsOfLicense = characteristic;
            }
            else if (isNew == false)
            {
                characteristicOfLicenseService.Delete(args.IdCharacteristicOfLicense);
            }

            return entity;
        }

        public void Delete(SoftwareArgs args)
        {
            Delete(args.Id);
            new WorksAndGoodsService().Delete(args);
        }
    }
}
