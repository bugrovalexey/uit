﻿$(function () {
    $(".chosen-select").chosen({ width: "75%" });

    if (!String.prototype.deleteSpaces) {
        String.prototype.deleteSpaces = function () {
            var result = this.replace(/&nbsp;/g, "");
            return result.replace(/ /g, "");
        }
    }

    if (!String.prototype.toDecimal) {
        String.prototype.toDecimal = function () {
            return this.replace(",", ".");
        }
    }

    if (!String.prototype.toRuLocalization) {
        String.prototype.toRuLocalization = function () {
            return this.replace(".", ",");
        }
    }

});


InitAutocomplete = function (selector, source) {

    var tags = source;

    $(selector).autocomplete({
        source: function (request, response) {
            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(tags, function (item) {
                return matcher.test(item);
            }));
        }
    });
};

$.fn.moneyRur = function () {
    if ($.fn.autoNumeric) {

        $(this).autoNumeric({
            aSep: ' ',
            aDec: ',',
            vMin: '0.00',
            vMax: '999999999999.99',
            //aSign: ' руб.',
            pSign: 's'
        });
    }
}

$.fn.floatValue = function () {
    if ($.fn.autoNumeric) {

        $(this).autoNumeric({
            aSep: ' ',
            aDec: ',',
            vMin: '0',
            vMax: '999999999999.99999999',
            pSign: 's'
        });
    }
}

$.fn.percent = function () {
    if ($.fn.autoNumeric) {

        $(this).autoNumeric({
            vMin: '0',
            vMax: '100',
            aSign: ' %',
            pSign: 's'
        });
    }
}


$.fn.autoNumericSet = function (value) {
    return $(this).autoNumeric('set', value);
};

$.fn.autoNumericGet = function () {
    var res = $(this).autoNumeric('get');
    return res ? res : 0;
};

$.fn.autoNumericGetLocal = function () {
    var res = $(this).autoNumeric('get');
    return res ? res.toRuLocalization() : 0;
};
