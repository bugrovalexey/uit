﻿namespace bgTeam.IsRui.Domain.Entities
{
    public class EntityNumerable : EntityBase
    {
        /// <summary>Номер</summary>
        public virtual int Num { get; set; }
    }
}