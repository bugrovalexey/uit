﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.IsRui.Domain.Dto;
    using System;

    public class ActivityCreateOrUpdateActStoryContext
    {
        public int? Id { get; set; }

        public int? ActivityId { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public int? Cost { get; set; }

        public ActsFileDto File { get; set; }
    }
}
