﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ConclusionAddFileStory : IStory<ConclusionAddFileStoryContext, FilesDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IFileProviderRui _fileProvider;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IStatusRepository _statusRepository;
        private readonly IUserRepository _userRepository;

        public ConclusionAddFileStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IFileProviderRui fileProvider,
            ISessionNHibernateFactory factory,
            IStatusRepository statusRepository,
            IUserRepository userRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _fileProvider = fileProvider;
            _factory = factory;
            _statusRepository = statusRepository;
            _userRepository = userRepository;
        }

        public FilesDto Execute(ConclusionAddFileStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<FilesDto> ExecuteAsync(ConclusionAddFileStoryContext context)
        {
            EntityBase entity;

            if (context.ConclusionId.HasValue)
            {
                entity = _repository.GetExisting<WorkflowUserConclusion>(context.ConclusionId);
            }
            else
            {
                var storyConclusion = new WorkflowUserConclusionSaveOrUpdateStory(_mapper, _repository, _userRepository, _factory, _statusRepository);
                var resConclusion = storyConclusion.Execute(new WorkflowUserConclusionSaveOrUpdateStoryContext
                {
                    ActivityId = context.ActivityId,
                });
                entity = _repository.GetExisting<WorkflowUserConclusion>(resConclusion.Id);
            }

            var addfilecontext = _mapper.Map(context, new FileAddStoryContext());

            addfilecontext.Type = DocTypeEnum.WorkflowUserConclusion;
            addfilecontext.Entity = entity;

            var result = await new FileAddStory(_mapper, _repository, _fileProvider, _factory)
                .ExecuteAsync(addfilecontext);

            //var resultDto = new ConclusionFileDto()
            //{
            //    ConclusionId = entity.Id,
            //    FileId = result.Id,
            //    FileName = result.Name,
            //    ActivityId = context.ActivityId
            //};

            return _mapper.Map(result, new FilesDto());
        }
    }
}
