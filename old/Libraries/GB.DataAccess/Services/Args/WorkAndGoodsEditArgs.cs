﻿
namespace GB.DataAccess.Services.Args
{
    public class WorkAndGoodsEditArgs: BaseArgsNew
    {
        public int? ActsOfAcceptanceId { get; set; }

        public int? NewId { get; set; }
    }
}
