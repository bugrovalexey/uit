﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServiceNPAs
{
    public class CopyGovServiceNPAInGovServiceCommand : ICommand<CopyGovServiceNPAInGovServiceContext>
    {
        ISessionFactoryGb factory;

        public CopyGovServiceNPAInGovServiceCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(CopyGovServiceNPAInGovServiceContext context)
        {
            var npasIds = context.Npas.Select(n => n.Id).ToList();
            var existAONpa = new Repository<AOGovServiceNPA>().GetAllEx(q => q.Where(x => x.GovService.Id == context.AOGovServiceId));
            var existNpaIds = existAONpa.Where(x => x.GovServiceNpa != null).Select(x => x.GovServiceNpa.Id).ToList();

            var deleteNPA = existAONpa.Where(x => x.GovServiceNpa == null || npasIds.Contains(x.GovServiceNpa.Id) == false).ToList();

            var addNPA = context.Npas.Where(x => existNpaIds.Contains(x.Id) == false).ToList();

            AOGovService currentAOGovService = new Repository<AOGovService>().Get(context.AOGovServiceId);

            using (var session = factory.Create())
            {
                foreach (var delItem in deleteNPA)
                {
                    session.Delete(delItem);
                }

                foreach (var addItem in addNPA)
                {
                    var entity = new AOGovServiceNPA();
                    entity.Name = addItem.Name;
                    entity.GovServiceNpa = addItem;
                    entity.GovService = currentAOGovService;
                    session.SaveOrUpdate(entity);
                }

                session.Commit();
            }            
        }
    }
}
