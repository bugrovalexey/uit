﻿namespace bgTeam.IsRui.WebApp.Admin.Models
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    public class EditorCardEditRefFieldModelView
    {
        public string Column { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }

        public IEnumerable<TableRowDataView> Data { get; set; }

        public EditorCardEditRefFieldModelView(string column, string fieldValue, string fieldName, IEnumerable<dynamic> data)
        {
            Column = column;
            FieldValue = fieldValue;
            FieldName = fieldName;

            var list = data.Select(x => new TableRowDataView(x));

            //if (list.Any(x => x.))
            //var dr = data.NewRow();

            //dr[fieldValue] = int.MinValue;
            //dr[fieldName] = "Не задано";

            //data.Rows.InsertAt(dr, 0);

            Data = list;
        }
    }
}