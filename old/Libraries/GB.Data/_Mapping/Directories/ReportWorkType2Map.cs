﻿using Uviri.Entity.Directories;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ReportWorkType2Map : ClassMapBase<ReportWorkType2>
    {
        public ReportWorkType2Map()
            : base("Report_Work_Type2")
        {

        }

protected override void InitMap()
        {
            Map(x => x.IsChange, "Report_Work_Type2_Expense_Vol_Y0_Is_Change");
            
            Map(x => x.ExpenseVolumeYear0, "Report_Work_Type2_Expense_Vol_Y0");
            Map(x => x.ExpenseVolumeYear1, "Report_Work_Type2_Expense_Vol_Y1");
            Map(x => x.ExpenseVolumeYear2, "Report_Work_Type2_Expense_Vol_Y2");

            References<PlansWork>(x => x.PlanWork, "Plans_Work_ID");
            References<ExpenseType>(x => x.ExpenseType, "Expense_Type_ID");
            References<WorkType>(x => x.WorkType, "Work_Exp_Type_ID");
        }
    }
}
