﻿using System;
using System.Collections.Generic;

namespace GB.Helpers
{
    public class ActivityCodeSortComparer<T> : IComparer<string>, IDisposable
    {
        private bool isAscending;

        public ActivityCodeSortComparer(bool inAscendingOrder = true)
        {
            this.isAscending = inAscendingOrder;
        }

        #region IComparer<string> Members

        public int Compare(string x, string y)
        {
            throw new NotImplementedException();
        }

        #endregion IComparer<string> Members

        #region IComparer<string> Members

        int IComparer<string>.Compare(string x, string y)
        {
            int returnVal = 0;
            // 0 если равны, положительное — если значение объекта х больше, чем у объекта у;
            //отрицательное — если значение объекта х меньше, чем у объекта у

            if (x == y)
                return 0;

            int leftX, leftY, rightX, rightY;
            leftX = leftY = rightX = rightY = 0;

            char separator = '.';

            var xs = x.Split(separator);
            leftX = Convert.ToInt32(xs[0]);
            rightX = Convert.ToInt32(xs[1]);

            var ys = y.Split(separator);
            leftY = Convert.ToInt32(ys[0]);
            rightY = Convert.ToInt32(ys[1]);

            if (leftX > leftY)
            {
                returnVal = 1;
            }
            else if (leftX < leftY)
            {
                returnVal = -1;
            }
            else if (rightX > rightY)
            {
                returnVal = 1;
            }
            else if (rightX < rightY)
            {
                returnVal = -1;
            }
            else return 0;

            return isAscending ? returnVal : -returnVal;
        }

        //private static int PartCompare(string left, string right)
        //{
        //    int x, y;
        //    if (!int.TryParse(left, out x))
        //        return left.CompareTo(right);

        //    if (!int.TryParse(right, out y))
        //        return left.CompareTo(right);

        //    return x.CompareTo(y);
        //}

        #endregion IComparer<string> Members

        //private Dictionary<string, string[]> table = new Dictionary<string, string[]>();

        public void Dispose()
        {
            //table.Clear();
            //table = null;
        }
    }
}