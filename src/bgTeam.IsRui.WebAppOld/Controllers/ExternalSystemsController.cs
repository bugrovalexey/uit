﻿using bgTeam.IsRui.Zakupki360;
using bgTeam.IsRui.Zakupki360.Dto;
using bgTeam.IsRui.Zakupki360.Impl;
using bgTeam.IsRui.WebApp.ViewModels.ExternalSystems;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Controllers
{
    /// <summary>
    /// Преднозначен для взаимодействия с внешними системами
    /// </summary>
    public class ExternalSystemsController : Controller
    {
        [HttpPost]
        public ActionResult FindGovernmentContract(int? status, string number, string order, string performer, DateTime? fdate, DateTime? tdate)
        {
            string error = null;
            IClientSystems cs = new ClientSystemsFactory().Create();

            var res = cs.GetConracts(
                new SearchParametersContracts()
                {
                    Status = status,
                    Number = number,
                    OrderNumber = order,
                    Performer = performer,
                    FromDate = fdate,
                    ToDate = tdate,
                    ResCount = 5
                },
                out error);

            var list = new List<ContractInfoViewModel>();

            foreach (var item in res)
            {
                list.Add(new ContractInfoViewModel(item));
            }

            return Json(list);
        }

        [HttpPost]
        public ActionResult FindPurchase(string number, int type, string name, DateTime? fdate, DateTime? tdate)
        {
            var list = GetTestPurchase();

            return Json(list);
        }

        /// <summary>
        /// Удалить после реализации сервиса на закупках
        /// </summary>
        /// <returns></returns>
        private List<PurchaseInfoViewModel> GetTestPurchase()
        {
            var list = new List<PurchaseInfoViewModel>();

            list.Add(new PurchaseInfoViewModel()
            {
                Id = 1,
                Number = "0161200001715000565",
                Type = 2,
                TypeStr = "Электронный аукцион",
                Name = "Поставка лекарственного препарата Инсулин",
                Date = "30.07.2015",
                Price = (3272160).ToString("N2"),
            });

            list.Add(new PurchaseInfoViewModel()
            {
                Id = 2,
                Number = "0320300091115000088",
                Type = 2,
                TypeStr = "Электронный аукцион",
                Name = "Поставка реактивов для рентген кабинета",
                Date = "24.07.2015",
                Price = (280000).ToString("N2"),
            });

            list.Add(new PurchaseInfoViewModel()
            {
                Id = 3,
                Number = "0320300091115000087",
                Type = 2,
                TypeStr = "Электронный аукцион",
                Name = "Поставка товаров для хозяйственных нужд.",
                Date = "24.07.2014",
                Price = (1323033.54d).ToString("N2"),
            });

            list.Add(new PurchaseInfoViewModel()
            {
                Id = 4,
                Number = "0138300003815000101",
                Type = 2,
                TypeStr = "Электронный аукцион",
                Name = "Капитальный ремонт - Установка двух допонительных котлов на жидком топливе KS-400 в котельной №6.",
                Date = "16.07.2015",
                Price = (3898495).ToString("N2"),
            });

            list.Add(new PurchaseInfoViewModel()
            {
                Id = 5,
                Number = "0180300003115000001",
                Type = 2,
                TypeStr = "Электронный аукцион",
                Name = "Ремонт дороги по улице Линейная  пст. Тигей Усть-Абаканского района согласно техническому заданию, локальному сметному расчету",
                Date = "24.07.2015",
                Price = (2213871).ToString("N2"),
            });

            return list;
        }
    }
}