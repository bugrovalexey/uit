﻿using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Mapping.Admin
{
    internal class NotificationMap : ClassMapBase<Notification>
    {
        public NotificationMap()
            : base("Notification")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Notification_Name");
            Map(x => x.Message, "Notification_Msg");
            Map(x => x.IsActive, "Notification_Is_Active");
            Map(x => x.Subject, "Notification_Subj");
        }
    }
}