﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.WebApp.Helpers;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class TableAODecisionToCreate : NotificationBaseViewModel
    {
        #region consts

        public const string NameCaption = "Наименование документа";
        public const string NumberCaption = "Номер документа";
        public const string DateAdoptionCaption = "Дата принятия документа";
        public const string UrlCaption = "URL на документ";
        public const string FilesDataCaption = "Документ";
        public const string ParticleOfDocumentCaption = "Пункт, статья документа";

        #endregion consts

        #region ctor

        public TableAODecisionToCreate(AODecisionToCreate entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            Number = entity.Number;
            DateAdoption = entity.DateAdoption.ToDisplay();
            ParticleOfDocument = entity.ParticleOfDocument;
            Url = entity.Url;
            FilesData =
                entity.Documents.Return(docs => docs.Select(x => new SelectEntityInfo {Id = x.Id, Name = x.Name}), default(IEnumerable<SelectEntityInfo>));
            FilenameExtension = entity.Documents.FirstOrDefault().Return(d => d.FileName.GetExtension().ToLower(), default(string));
        }

        #endregion ctor

        #region Props

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(NumberCaption)]
        public string Number { get; set; }

        [DisplayName(DateAdoptionCaption)]
        public string DateAdoption { get; set; }

        public string Url { get; set; }

        [DisplayName(ParticleOfDocumentCaption)]
        public string ParticleOfDocument { get; set; }

        [DisplayName(UrlCaption)]
        public string UrlLink
        {
            get { return Url.Return(url => LinkBuilder.Link(url, url), default(string)); }
        }

        /// <summary>
        /// Расширение имени файла
        /// </summary>
        public string FilenameExtension { get; set; }

        private IEnumerable<IEntityName> FilesData { get; set; }

        [DisplayName(FilesDataCaption)]
        public string FileLink
        {
            get
            {
                var links = FilesData.Select(fd => LinkBuilder.Link(fd));
                return string.Join("<br />", links);
            }
        }

        public string NameByFileLink
        {
            get
            {
                var links = FilesData.Select(fd => LinkBuilder.Link(Name, fd.Id));
                return string.Join("<br />", links);
            }
        }


        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteDocAccountingObject", "AccountingObjects", new { id = Id }, "GridDocuments");
            }
        }

        #endregion Props
    }
}