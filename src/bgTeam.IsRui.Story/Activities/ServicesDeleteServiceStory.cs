﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ServicesDeleteServiceStory : IStory<ServicesDeleteServiceStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;

        public ServicesDeleteServiceStory(ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _factory = sessionNHibernateFactory;
        }

        public bool Execute(ServicesDeleteServiceStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(ServicesDeleteServiceStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            bool result;

            using (var session = _factory.OpenSession())
            {
                await session.DeleteAsync<PlansActivityService>(context.Id.Value);

                result = true;
            }

            return result;
        }
    }
}
