﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Core.Entities.Directories;

namespace Core.Mapping.Directories
{
	public class PlanTypeMap : ClassMap<PlanType>
    {
		public PlanTypeMap()
        {
            Table("w_Plans_Type");

            Id(x => x.Id, "Plans_Type_ID").GeneratedBy.Custom(typeof(Core.Infrastructure.IdGenerator));

            Map(x => x.Name, "Plans_Type_Name");
        }
    }
}