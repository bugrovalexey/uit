﻿using GB.Data.Directories;

namespace GB.Data.Plans
{
    /// <summary>
    /// Характеристика товара
    /// </summary>
    public class PlansSpecCharacteristic : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Единица измерения</summary>
        public virtual Units Unit { get; set; }

        /// <summary>Значение</summary>
        public virtual int? Value { get; set; }

        /// <summary>Товар</summary>
        public virtual PlansSpec Goods { get; set; }
    }
}