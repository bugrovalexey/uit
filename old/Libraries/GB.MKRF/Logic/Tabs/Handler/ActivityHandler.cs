﻿namespace GB.MKRF.Logic.Tabs.Handler
{
    internal class ActivityHandler : TabsBaseHandler
    {
        public ActivityHandler()
            : base("Activities")
        {
            Add("Мои мероприятия", "List");
            Add("На согласовании", "ListApprove");
            Add("На экспертизе", "ListExpertise");
        }
    }
}