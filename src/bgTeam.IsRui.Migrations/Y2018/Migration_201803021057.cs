﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201803021059)]
    public class Migration_201803021057 : ForwardOnlyMigration
    {
        public override void Up()
        {

            Create.Table("Plans_Activity_Works")
                .WithColumn("Plans_Activity_Works_ID").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Plans_Activity_ID").AsInt32().NotNullable()
                .WithColumn("Plans_Activity_Works_Name").AsString().Nullable()
                .WithColumn("Plans_Activity_Works_OKPDId").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Works_ExpenseDirectionId").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Works_ExpenseTypeId").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Works_KOSGUId").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Works_OKVEDId").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Works_Year").AsString().Nullable()
                .WithColumn("Plans_Activity_Works_SpecialistsCount").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Works_Salary").AsDecimal().Nullable()
                .WithColumn("Plans_Activity_Works_Duration").AsInt32().Nullable()
                .WithColumn("Plans_Activity_Works_Summ").AsDecimal().Nullable()
                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Create.ForeignKey("FK_Plans_Activity_Works")
               .FromTable("Plans_Activity_Works").ForeignColumn("Plans_Activity_ID")
               .ToTable("Plans_Activity").PrimaryColumn("Plans_Activity_ID");

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "Plans_Activity_Works" });

        }
    }
}
