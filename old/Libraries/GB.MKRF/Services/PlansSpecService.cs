﻿using GB.Data.Directories;
using GB.Data.Plans;
using GB.MKRF.Helpers;
using GB.MKRF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    public class PlansSpecService : ServiceBase<PlansSpec>, IService<PlansSpec, PlansSpecArgs>
    {
        //[Obsolete]
        //public PlansSpec Create(PlansSpecControllerArgs args)
        //{
        //    var plansSpec = new PlansSpec();
        //    plansSpec.PlanActivity = args.ParentActivity;

        //    plansSpec.Num = plansSpec.PlanActivity.Specifications.Count > 0 ? NumHelper<PlansSpec>.GetMinAvailableNum(plansSpec.PlanActivity.Specifications) : 1;
        //    plansSpec.OrigNum = GetNextNum(args.ParentActivity);

        //    plansSpec.OKPD = RepositoryBase<OKPD>.Get(EntityBase.Default);

        //    return SaveOrUpdate(plansSpec, args.Values);
        //}

        //[Obsolete]
        //public PlansSpec Update(PlansSpecControllerArgs args)
        //{
        //    return SaveOrUpdate(Repository.Get(args.PlansSpecId), args.Values);
        //}

        //private PlansSpec SaveOrUpdate(PlansSpec plansSpec, Dictionary<string, object> values)
        //{
        //    var edid = Convert.ToInt32(values["ExpenseDirection"]);
        //    if (edid == 0)
        //        throw new ArgumentException("Укажите Вид затрат");

        //    var etid = Convert.ToInt32(values["STType"]);
        //    if (etid == 0)
        //        throw new ArgumentException("Укажите Тип затрат");

        //    plansSpec.ExpenseDirection = RepositoryBase<ExpenseDirection>.Get(edid);
        //    plansSpec.Qualifier = RepositoryBase<Qualifier>.Get(Convert.ToInt32(values["Qualifier"]));

        //    plansSpec.Name = values["Name"].ToString();
        //    plansSpec.Analog = values["Analog"].ToString();
        //    plansSpec.OKDPText = values["OKDP"].ToString();
        //    plansSpec.OKPD = RepositoryBase<OKPD>.GetDefault();
        //    plansSpec.Quantity = Convert.ToInt32(values["Quantity"]);
        //    plansSpec.Cost = Convert.ToDecimal(values["Cost"]);
        //    plansSpec.GRBS = RepositoryBase<GRBS>.GetDefault();
        //    var workForm = RepositoryBase<WorkForm>.GetDefault();//.Get(Convert.ToInt32(hfSpec["WorkFormComboBox"]));
        //    plansSpec.WorkForm = workForm;

        //    plansSpec.SectionKBK = RepositoryBase<SectionKBK>.GetDefault();
        //    plansSpec.ExpenseItem = RepositoryBase<ExpenseItem>.GetDefault();
        //    plansSpec.AnalogPrice = Convert.ToDouble(values["AnalogPrice"]);

        //    plansSpec.ExpenditureItem = values.ContainsKey("ExpenditureItemsId") ? GetEntity<ExpenditureItem>(values["ExpenditureItemsId"]) : RepositoryBase<ExpenditureItem>.GetDefault();

        //    PlansSpecExpenseType currentExpenseType = null;
        //    if (plansSpec.SpecExpenseType.Count <= 0)
        //    {
        //        currentExpenseType = new PlansSpecExpenseType() { PlansSpec = plansSpec };
        //        plansSpec.SpecExpenseType.Add(currentExpenseType);
        //    }
        //    else
        //    {
        //        currentExpenseType = plansSpec.SpecExpenseType[0];
        //    }
        //    currentExpenseType.ExpenseType = RepositoryBase<ExpenseType>.Get(etid);

        //    base.SaveOrUpdate(plansSpec);

        //    foreach (KeyValuePair<string, object> spec in values)
        //    {
        //        if (spec.Key.StartsWith("STName_"))
        //        {
        //            var i = spec.Key.Split('_')[1];

        //            var st = new SpecType(plansSpec);
        //            if (values.Keys.Contains("STID_" + i))
        //            {
        //                var id = Convert.ToInt32(values["STID_" + i]);
        //                st = plansSpec.SpecType.First(x => x.Id == id);
        //            }
        //            st.CharName = values["STName_" + i].ToString();
        //            st.CharUnit = values["STUnion_" + i].ToString();
        //            st.CharValue = values["STValue_" + i].ToString();
        //            st.PlanSpec = plansSpec;
        //            plansSpec.SpecType.Add(st);
        //            RepositoryBase<SpecType>.SaveOrUpdate(st);
        //        }
        //    }

        //    return plansSpec;
        //}

        //public void Delete(int id)
        //{
        //    base.Delete(id);
        //}

        //public void DeleteSpecType(int typeId)
        //{
        //    RepositoryBase<SpecType>.Delete(typeId);
        //}

        private static int GetNextNum(PlansActivity activity)
        {
            if (activity.Specifications.Count == 0)
                return 1;

            return activity.Specifications.OrderByDescending(i => i.OrigNum).First().OrigNum + 1;
        }

        //public DataTable GetSpecList(PlansActivity activity)
        //{
        //    return SpecRepository.GetSpecList(activity, true);
        //}

        //public QualifierList GetQualifierList()
        //{
        //    return new QualifierList
        //        {
        //            List = RepositoryBase<Qualifier>.GetAll().OrderBy(x => x.Name, new NumComparer()).ToDictionary(x => x.Id, x => x.Name),
        //            Selected = CurrentEdit != null && CurrentEdit.Qualifier != null ? CurrentEdit.Qualifier.Id : EntityBase.Default
        //        };
        //}

        //public ExpenseDirections GetExpenseDirections(PlansActivity activity)
        //{
        //    Dictionary<int, string> listStr = new Dictionary<int, string>(){
        //            {0, "Не задано"}
        //        };

        //    var validED = new[] { "12", "2" };

        //    foreach (var item in activity.ExpenseDirections.OrderBy(x => x.ExpenseDirection.Code))
        //    {
        //        if (!validED.Contains(item.ExpenseDirection.Code))
        //            continue;

        //        listStr.Add(item.ExpenseDirection.Id, item.ExpenseDirection.CodeName);
        //    }

        //    if (CurrentEdit != null)
        //        if (!listStr.ContainsKey(CurrentEdit.ExpenseDirection.Id))
        //            listStr.Add(CurrentEdit.ExpenseDirection.Id, CurrentEdit.ExpenseDirection.CodeName);

        //    return new ExpenseDirections
        //    {
        //        List = listStr,
        //        Selected = CurrentEdit != null ? CurrentEdit.ExpenseDirection.Id : EntityBase.Default
        //    };
        //}

        //public OKDPList GetOKDPList()
        //{
        //    Dictionary<int, string> listStr = new Dictionary<int, string>();
        //    IList<OKPD> list = RepositoryBase<OKPD>.GetAll();

        //    foreach (OKPD item in list)
        //    {
        //        listStr.Add(item.Id, item.Name);
        //    }

        //    return new OKDPList
        //        {
        //            List = listStr,
        //            Selected = CurrentEdit != null && CurrentEdit.OKPD != null ? CurrentEdit.OKPD.Id : EntityBase.Default
        //        };
        //}

        ////public void CopySpec(object id)
        ////{
        ////    CopyHelper.CopySpec(null, Repository.Get(id));
        ////}

        //public Dictionary<int, string> GetExpenseTypeItems(object expenseDirectionId = null)
        //{
        //    ExpenseDirection expenseDirection = RepositoryBase<ExpenseDirection>.Get(expenseDirectionId) ?? (CurrentEdit != null ? CurrentEdit.ExpenseDirection : null);

        //    if (expenseDirection == null)
        //        return new Dictionary<int, string>() { { 0, "Не задано" } };

        //    IList<ExpenseType> list = new List<ExpenseType>();
        //    list = RepositoryBase<ExpenseType>.GetAll(x => x.ExpenseDirection == expenseDirection || x.ExpenseDirection.Id == 0);

        //    return list.ToDictionary(item => item.Id, item => item.Name);
        //}

        //public void ChangeOrder(PlansSpecControllerArgs args, bool up)
        //{
        //    NumHelper<PlansSpec>.OrderNums(args.ParentActivity.Specifications, true);
        //    NumHelper<PlansSpec>.Move(args.ParentActivity.Specifications, Repository.Get(args.PlansSpecId), up);
        //}

        #region ReturnPackages

        public class QualifierList
        {
            public Dictionary<int, string> List;
            public int Selected;
        }

        public class ExpenseDirections
        {
            public Dictionary<int, string> List;
            public int Selected;
        }

        public class OKDPList
        {
            public Dictionary<int, string> List;
            public int Selected;
        }

        #endregion ReturnPackages

        public PlansSpec Create(PlansSpecArgs args)
        {
            var plansSpec = new PlansSpec();
            plansSpec.PlanActivity = args.Activity ?? GetEntity<PlansActivity>(args.ActivityId);

            plansSpec.Num = plansSpec.PlanActivity.Specifications.Count > 0 ? NumHelper<PlansSpec>.GetMinAvailableNum(plansSpec.PlanActivity.Specifications) : 1;
            plansSpec.OrigNum = GetNextNum(plansSpec.PlanActivity);

            return SaveOrUpdate(plansSpec, args);
        }

        public PlansSpec Update(PlansSpecArgs args)
        {
            return SaveOrUpdate(Repository.GetExisting(args.Id), args);
        }

        private PlansSpec SaveOrUpdate(PlansSpec plansSpec, PlansSpecArgs args)
        {
            var edid = Convert.ToInt32(args.ExpenseDirection);
            if (edid == 0)
                throw new ArgumentException("Укажите Вид затрат");

            //var etid = Convert.ToInt32(args.ExpenseType);
            //if (etid == 0)
            //    throw new ArgumentException("Укажите Тип затрат");

            plansSpec.Name = args.Name;
            //plansSpec.Qualifier = RepositoryBase<Qualifier>.Get(Convert.ToInt32(args.Qualifier"]));
            //plansSpec.Analog = args.Analog;
            //plansSpec.OKDPText = args.OKDP;
            plansSpec.GRBS = new Repository<GRBS>().GetDefault();
            plansSpec.WorkForm = new Repository<WorkForm>().GetDefault();
            plansSpec.SectionKBK = new Repository<SectionKBK>().GetDefault();
            plansSpec.ExpenseItem = new Repository<ExpenseItem>().GetDefault();

            //plansSpec.AnalogPrice = Convert.ToDouble(args.AnalogPrice"]);

            plansSpec.ExpenseDirection = GetEntity<ExpenseDirection>(edid);
            plansSpec.ExpenseType = GetEntity<ExpenseType>(args.ExpenseType);
            plansSpec.OKPD = GetEntity<OKPD>(args.OKPD);
            plansSpec.ExpenditureItem = GetEntity<ExpenditureItem>(args.KOSGU);
            plansSpec.ProductGroup = GetEntity<ProductGroup>(args.Category);

            plansSpec.Year = (YearEnum)Convert.ToInt32(args.Year);
            plansSpec.Quantity = args.Quantity;
            plansSpec.Duration = args.Duration;
            plansSpec.Cost = args.Cost;

            base.SaveOrUpdate(plansSpec);

            return plansSpec;
        }

        public void Delete(PlansSpecArgs args)
        {
            base.Delete((int)args.Id);
        }
    }

    public class PlansSpecArgs : BaseArgsNew
    {
        public string Name { get; set; }

        public int OKPD { get; set; }

        public int Category { get; set; }

        public int ExpenseDirection { get; set; }

        public int ExpenseType { get; set; }

        public int KOSGU { get; set; }

        public YearEnum Year { get; set; }

        public int Quantity { get; set; }

        public int Duration { get; set; }

        public decimal Cost { get; set; }

        public PlansActivity Activity { get; set; }

        public int ActivityId { get; set; }
    }

    [Obsolete]
    public class PlansSpecControllerArgs
    {
        public object PlansSpecId { get; set; }

        public Dictionary<string, object> Values { get; set; }

        public PlansActivity ParentActivity { get; set; }
    }

    /*
    public class NumComparer : IComparer<string>
    {
        private char[] separatorList = new char[] { ')', '.', };

        public NumComparer()
        {
        }

        public NumComparer(char Separator)
        {
            this.separatorList = new char[] { Separator };
        }

        int IComparer<string>.Compare(string x, string y)
        {
            if (x != null && y != null)
            {
                int idxA = x.IndexOfAny(separatorList);
                int idxB = y.IndexOfAny(separatorList);

                if (idxA > 0 && idxB > 0)
                {
                    int valA = 0;
                    int valB = 0;

                    if (int.TryParse(x.Substring(0, idxA).Trim(), out valA) &&
                        int.TryParse(y.Substring(0, idxB).Trim(), out valB))
                    {
                        return valA.CompareTo(valB);
                    }
                }
            }

            return string.Compare(x, y);
        }
    }
    */
}