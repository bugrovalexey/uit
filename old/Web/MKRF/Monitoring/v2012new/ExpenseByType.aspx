﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ExpenseByType.aspx.cs" Inherits="GB.MKRF.Web.Monitoring.v2012new.ExpenseByType" %>
<%@ Import Namespace="GB.MKRF.Entities.Directories" %>
<%@ Import Namespace="GB.MKRF.Web.Plans" %>

<asp:Content ID="Content2" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function reloadPage(got) {
            pageUrl = 'ExpenseByType.aspx';
            var args = [];
            args.push('Year=' + YearsComboBox.GetValue());
            args.push('type=' + got);
            window.open(pageUrl + "?" + args.join('&'), '_self');
        }

    </script>
    <table style="width: 100%">
        <tr>
            <td align="center">
                <table style="width: 800px">
                    <tr>
                        <td>Год:</td>
                        <td style="width: 100px">
                            <dxe:ASPxComboBox runat="server" ID="YearsComboBox" Width="80px" ValueType="System.Int32"
                                ValueField="Year" TextField="Year" ClientInstanceName="YearsComboBox" OnDataBinding="YearsComboBox_DataBinding">
                                <ClientSideEvents SelectedIndexChanged="function(s,e) { reloadPage(0);  }" />
                            </dxe:ASPxComboBox>
                        </td>
                        <td>Тип затрат:</td>
                        <td>
                            <dxe:ASPxComboBox runat="server" ID="ExpenseTypeComboBox" Width="480px" ValueType="System.Int32"
                                ValueField="ID" TextField="Name" ClientInstanceName="ExpenseTypeComboBox" OnDataBinding="ExpenseTypeComboBox_DataBinding">
                                <ClientSideEvents SelectedIndexChanged="function(s,e) { reloadPage(ExpenseTypeComboBox.GetSelectedItem().value);  }" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br />

                <dxchartsui:WebChartControl ID="Chart" Width="1000px"
                    runat="server" SideBySideEqualBarWidth="True" Height="670px" AppearanceName="In A Fog"
                    PaletteName="Origin" OnCustomDrawSeriesPoint="Chart_CustomDrawSeriesPoint">
<seriestemplate>
<ViewSerializable>
<dxcharts:SideBySideBarSeriesView></dxcharts:SideBySideBarSeriesView>
</ViewSerializable>
<LabelSerializable>
<dxcharts:SideBySideBarSeriesLabel LineVisible="True">
<FillStyle><OptionsSerializable>
<dxcharts:SolidFillOptions></dxcharts:SolidFillOptions>
</OptionsSerializable>
</FillStyle>
</dxcharts:SideBySideBarSeriesLabel>
</LabelSerializable>
<PointOptionsSerializable>
<dxcharts:PointOptions></dxcharts:PointOptions>
</PointOptionsSerializable>
<LegendPointOptionsSerializable>
<dxcharts:PointOptions></dxcharts:PointOptions>
</LegendPointOptionsSerializable>
</seriestemplate>


                    <palettewrappers>
<dxchartsui:PaletteWrapper Name="Palette 1" ScaleMode="Repeat"><Palette>
<dxcharts:PaletteEntry Color="79, 129, 189" Color2="51, 90, 136"></dxcharts:PaletteEntry>
<dxcharts:PaletteEntry Color="192, 80, 77" Color2="139, 52, 49"></dxcharts:PaletteEntry>
<dxcharts:PaletteEntry Color="155, 187, 89" Color2="111, 137, 56"></dxcharts:PaletteEntry>
<dxcharts:PaletteEntry Color="128, 100, 162" Color2="89, 69, 115"></dxcharts:PaletteEntry>
<dxcharts:PaletteEntry Color="75, 172, 198" Color2="46, 124, 145"></dxcharts:PaletteEntry>
<dxcharts:PaletteEntry Color="247, 150, 70" Color2="213, 101, 9"></dxcharts:PaletteEntry>
</Palette>
</dxchartsui:PaletteWrapper>
</palettewrappers>


                    <fillstyle><OptionsSerializable>
                    <dxcharts:SolidFillOptions></dxcharts:SolidFillOptions>
                    </OptionsSerializable>
                    </fillstyle>

                    <padding bottom="0" left="0" right="0" top="0"></padding>
                    <legend alignmentvertical="Center" equallyspaceditems="False" font="Tahoma, 10pt"></legend>
                    <titles>
                    <dxcharts:ChartTitle WordWrap="True" MaximumLinesCount="4" Text="Структура планируемых расходов по направлениям затрат на ИКТ" Font="Tahoma, 14pt"></dxcharts:ChartTitle>
                    </titles>
                    <borderoptions visible="False" />
                    <backimage imageurl="/Content/Style/Coord/backgrounds/diagram_back.jpg"></backimage>
                    <borderoptions visible="False"></borderoptions>
                    <diagramserializable>
<dxcharts:XYDiagram>

<AxisX Title-Text="Тип департамента" VisibleInPanesSerializable="-1">
<Tickmarks Visible="False" MinorVisible="False"></Tickmarks>
<Label Staggered="False" Antialiasing="True"></Label>
<Range SideMarginsEnabled="True"></Range>
</AxisX>

<AxisY Title-Text="Объем расходов тыс. руб." Title-Visible="True"  VisibleInPanesSerializable="-1">
<Tickmarks MinorVisible="False" CrossAxis="True"></Tickmarks>
<Label Antialiasing="True"></Label>
<Range SideMarginsEnabled="True"></Range>
</AxisY>

<DefaultPane BackColor="White" EnableAxisXScrolling="False" EnableAxisYScrolling="False" EnableAxisXZooming="False" EnableAxisYZooming="False"></DefaultPane>
</dxcharts:XYDiagram>
</diagramserializable>
                </dxchartsui:WebChartControl>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br />
                <dxwgv:ASPxGridView runat="server" ID="grid" Width="1000" OnDataBinding="grid_DataBinding" KeyFieldName="ID">
                     <Settings ShowFilterRow="True" ShowFooter="true" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Тип департамента" FieldName="Name" GroupIndex="1">
                            <Settings AutoFilterCondition="Contains" />
                        </dxwgv:GridViewDataTextColumn>

                        <dxwgv:GridViewDataTextColumn Caption="Наименование департамента" FieldName="OGV" >
                            <DataItemTemplate>
                                <%#string.Format("<a class='gridLink' href='{0}'>{1}</a>", PlanCard.GetUrl((int)Eval("PlanId"), ExpObjectTypeEnum.Plan2012new), Eval("OGV").ToString()) %>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Расходы" FieldName="ExpenseVolumeY0">
                            <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                            <Settings AutoFilterCondition="Contains" />
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <Settings ShowFilterRow="True" ShowFooter="True" GroupFormat="{1}" />
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="Итого:" SummaryType="Sum" FieldName="OGV" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="ExpenseVolumeY0" SummaryType="Sum" />
                    </TotalSummary>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
    </table>
</asp:Content>