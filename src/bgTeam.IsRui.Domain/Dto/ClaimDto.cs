﻿namespace bgTeam.IsRui.Domain.Dto
{
    public class ClaimDto
    {
        public int Id { get; set; }

        public string Number { get; set; }

        public string AccountingObjectName { get; set; }

        public string ActivityName { get; set; }

        public string SubdivisionName { get; set; }

        public int Cost { get; set; }

        public string StatusName { get; set; }
    }
}
