﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpValuationReasonMap : ClassMapBase<ExpValuationReason>
    {
        public ExpValuationReasonMap()
            : base("Exp_Valuation_Reason")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Comm, "Exp_Valuation_Reason_Comm");            

            References<ExpCriterionEquivalent>(x => x.ExpCriterionEquivalent, "Exp_Criterion_Equivalent_ID");
            References<ExpReason>(x => x.ExpReason, "Exp_Reason_ID");
            References<ExpConformity>(x => x.ExpConformity, "Exp_Conformity_ID");
        }
    }
}