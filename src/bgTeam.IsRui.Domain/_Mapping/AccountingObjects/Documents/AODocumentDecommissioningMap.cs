﻿namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects.Documents
{
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.Documents;
    using bgTeam.IsRui.Domain.Entities.Documents;

    internal class AODocumentDecommissioningMap : AODocumentBaseMap<AODocumentDecommissioning>
    {
        protected DocTypeEnum Type
        {
            get { return AODocumentDecommissioning.DocType; }
        }

        protected override void AdditionalMap()
        {
            HasMany(x => x.Documents).KeyColumns.Add("EntityOwnerId")
                .Where("EntityType = " + (int)EntityType.AODocumentDecommissioning)
                .Where("Type = " + (int)Type)
                .Cascade.Delete().Inverse();
        }
    }
}
