﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class DeleteActivityStory : IStory<DeleteActivityStoryContext, bool>
    {
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;

        public DeleteActivityStory(IRepositoryRui repositoryRui, ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _repository = repositoryRui;
            _factory = sessionNHibernateFactory;
        }

        public bool Execute(DeleteActivityStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(DeleteActivityStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            var activity = _repository.GetOrDefault<PlansActivity>(context.Id);

            if (activity != null)
            {
                using (var session = _factory.OpenSession())
                {
                    await DeleteReference(activity);

                    await session.DeleteAsync<PlansActivity>(context.Id.Value);
                }

                return true;
            }
            else
            {
                throw new IsRuiException($"Не найдено мероприятие id: {context.Id}");
            }
        }

        public async Task<bool> DeleteReference(PlansActivity activity)
        {
            var serviceList = _repository.GetAllEx<PlansActivityService>(q => q.Where(x => x.Activity.Id == activity.Id));

            var storyDel = new ServicesDeleteServiceStory(_factory);

            foreach (PlansActivityService service in serviceList)
            {
                await storyDel.ExecuteAsync(new ServicesDeleteServiceStoryContext() { Id = service.Id });
            }

            var goodsList = _repository.GetAllEx<ActivityGoods>(q => q.Where(x => x.PlansActivityId == activity.Id));

            var storyDelete = new GoodsDeleteGoodsStory(_factory);

            foreach (ActivityGoods goods in goodsList)
            {
                await storyDelete.ExecuteAsync(new GoodsDeleteGoodsStoryContext() { Id = goods.Id });
            }

            return true;
        }
    }
}
