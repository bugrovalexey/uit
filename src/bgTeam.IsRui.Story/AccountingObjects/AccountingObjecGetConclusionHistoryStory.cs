﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class AccountingObjecGetConclusionHistoryStory : IStory<AccountingObjecGetConclusionHistoryStoryContext, IEnumerable<ConclusionHistoryDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;

        public AccountingObjecGetConclusionHistoryStory(
            IMapperBase mapper,
            IRepositoryRui repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public IEnumerable<ConclusionHistoryDto> Execute(AccountingObjecGetConclusionHistoryStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<IEnumerable<ConclusionHistoryDto>> ExecuteAsync(AccountingObjecGetConclusionHistoryStoryContext context)
        {
            var result = _repository.GetAllEx<AccountingObjectUserComment>(q =>
            {
                q.Where(x => x.AccountingObject.Id == context.Id && x.Status != null);
            });
            return result.Select(x => _mapper.Map(x, new ConclusionHistoryDto()));
        }
    }
}
