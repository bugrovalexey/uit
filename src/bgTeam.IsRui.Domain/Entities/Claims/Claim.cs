﻿namespace bgTeam.IsRui.Domain.Entities.Claims
{
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Заявка
    /// </summary>
    public class Claim : EntityBase, IEntityWorkflow
    {
        /// <summary>
        /// Номер заявки по порядку
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Инициатор заявки
        /// </summary>
        public virtual User InitiatorUser { get; set; }

        /// <summary>
        /// Объект учёта
        /// </summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Мероприятие плпнп
        /// </summary>
        public virtual PlansActivity PlansActivity { get; set; }

        /// <summary>
        /// Сокращенное наименование подразделения
        /// </summary>
        public virtual string SubDivisionName { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public virtual decimal Cost { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        //public virtual Status Status { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public virtual DateTime CreateDate { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual Workflow Workflow { get; set; }

        /// <summary>
        /// Обоснование
        /// </summary>
        public virtual string Justification { get; set; }

        /// <summary>
        /// Информация по текущему заключению
        /// </summary>
        public virtual ClaimConclusion Conclusion { get; set; }

        /// <summary>
        /// Стоимость реализации
        /// </summary>
        public virtual decimal? ImplementationCost { get; set; }

        /// <summary>
        /// Содержание
        /// </summary>
        public virtual string Content { get; set; }

        /// <summary>
        /// Документы заявки
        /// </summary>
        public virtual IEnumerable<ClaimDocument> Documents { get; set; }

        /// <summary>
        /// Индикаторы
        /// </summary>
        public virtual IEnumerable<ClaimIndicator> Indicators { get; set; }

        /// <summary>
        /// Показатели
        /// </summary>
        public virtual IEnumerable<ClaimMark> Marks { get; set; }

        /// <summary>
        /// Акты
        /// </summary>
        public virtual IEnumerable<ClaimAct> Acts { get; set; }

        /// <summary>
        /// Объём работ
        /// </summary>
        public virtual IEnumerable<ClaimScopeOfWork> ScopeOfWork { get; set; }
    }
}
