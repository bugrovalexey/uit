﻿using GB.Data.AccountingObjects;
using System.Collections.Generic;

namespace GB.MKRF.Repository
{
    public class DocumentAccountingObjectRepository : Repository<DocumentAccountingObject>
    {
        public IList<DocumentAccountingObject> GetAllByAccountingObject(int? accountingObjectId)
        {
            return GetAll(x => x.AccountingObject.Id == accountingObjectId);
        }
    }
}