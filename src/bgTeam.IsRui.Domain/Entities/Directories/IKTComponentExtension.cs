﻿namespace bgTeam.IsRui.Domain.Entities.Directories
{
    public static class IKTComponentExtension
    {
        public static bool IsEq(this IKTComponentEnum e, IKTComponent o)
        {
            if (o == null)
                return false;

            return o.Id == (int)e;
        }

        public static bool IsEq(this IKTComponent o, IKTComponentEnum e)
        {
            if (o == null)
                return false;

            return o.Id == (int)e;
        }
    }
}
