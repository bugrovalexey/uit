﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GetListActivitiesStory : IStory<GetListActivitiesStoryContext, IEnumerable<ActivityDto>>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IMapperBase _mapper;
        private readonly IUserRepository _userRepository;

        public GetListActivitiesStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase,
            IUserRepository userRepository)
        {
            _repositoryRui = repositoryRui;
            _mapper = mapperBase;
            _userRepository = userRepository;
        }

        public IEnumerable<ActivityDto> Execute(GetListActivitiesStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivityDto>> ExecuteAsync(GetListActivitiesStoryContext context)
        {
            var curUser = _userRepository.GetCurrent();

            var activities = _repositoryRui
                .GetAllEx<PlansActivity>(query => query.Where(a => a.Department.Id == curUser.Department.Id)).ToList();
            return activities
                .Select(x => _mapper.Map(x, new ActivityDto()));
        }
    }
}
