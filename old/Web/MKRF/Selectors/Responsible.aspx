﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="Responsible.aspx.cs" Inherits="GB.MKRF.Web.Selectors.ResponsibleSelector" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript">
        function hfResponsibleSave(e) {
            hfResponsible.Set('Name', NameTextBox.GetText());
            hfResponsible.Set('Job', JobComboBox.GetValue());
            hfResponsible.Set('Phone', PhoneTextBox.GetValue());
            hfResponsible.Set('Email', EmailTextBox.GetText());
        }

        function rowClick(s, e) {
            var id = s.GetRowKey(e.visibleIndex);
            s.GetRowValues(e.visibleIndex, 'Id;Name', function (values) {
                var result = id + ';' + values[1];

                window.parent.PopupClose('Responsible', result);
            });
        }

        function validateComboBox(s, e) {
            e.isValid = s.GetValue() != 0;
        }
    </script>

    <dxe:ASPxButton ID="AddRespButton" runat="server" ClientInstanceName="AddRespButton"
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {RespGrid.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView ID="RespGrid" ClientInstanceName="RespGrid" runat="server" AutoGenerateColumns="False" KeyFieldName="Id" Width="100%"
        OnBeforePerformDataSelect="RespGrid_BeforePerformDataSelect"
        OnRowInserting="RespGrid_RowInserting"
        OnRowUpdating="RespGrid_RowUpdating"
        OnStartRowEditing="RespGrid_StartRowEditing"
        OnHtmlRowCreated="RespGrid_HtmlRowCreated">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False"
                AllowDragDrop="False">
                <EditButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>

            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="ФИО">
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Job.Name" Caption="Должность">
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Department.Name" Caption="Департамент">
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Phone" Caption="Телефон">
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True" />
                <PropertiesTextEdit DisplayFormatString="(###) ###-####" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Email" Caption="Email">
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True" />
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc"><span class="required"></span>ФИО:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="NameTextBox" ClientInstanceName="NameTextBox"
                                OnDataBinding="TextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc"><span class="required"></span>Должность:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="JobComboBox" runat="server" EnableCallbackMode="true" ClientInstanceName="JobComboBox"
                                 ValueType="System.Int32" ValueField="Id" TextField="Name" DropDownStyle="DropDown"
                                OnDataBinding="JobComboBox_DataBinding" Style="margin-left: -3px;" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ClientSideEvents Validation="function(s,e){validateComboBox(s,e);}" />
                                <ValidationSettings ErrorText="Обязательное поле" ErrorDisplayMode="None" ValidationGroup="EditForm">
                                    <RequiredField IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Департамент:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxLabel ID="DepaptmentLabel" runat="server" OnDataBinding="DepaptmentLabel_DataBinding"></dxe:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Телефон:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="PhoneTextBox" ClientInstanceName="PhoneTextBox" OnDataBinding="TextBox_DataBinding">
                                <%--<MaskSettings Mask="+9 (999) 999-99-99 'доб.' 9999" IncludeLiterals="None" ErrorText="Некорректный формат телефона" />--%>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Email:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="EmailTextBox" ClientInstanceName="EmailTextBox" OnDataBinding="TextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') hfResponsibleSave(e); }" />
        <Styles>
            <AlternatingRow Enabled="true" />
            <Header Wrap="True" />
        </Styles>
        <ClientSideEvents RowClick="rowClick" />
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="15" />
    </dxwgv:ASPxGridView>

    <dxhf:ASPxHiddenField runat="server" ID="hfResponsible" ClientInstanceName="hfResponsible" />
</asp:Content>

