﻿namespace bgTeam.IsRui.MKRF.Logic.Access.Handlers
{
    public interface IAccessHandler
    {
        bool CanRead();

        bool CanEdit();

        bool CanCreate();

        bool CanDelete();
    }
}
