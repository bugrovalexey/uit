﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.Smev
{
    public class StringWriterWithEncoding : System.IO.StringWriter
    {

        private readonly System.Text.Encoding _Encoding;


        public StringWriterWithEncoding(System.Text.StringBuilder sb, System.Text.Encoding encoding)
            : base(sb)
        {
            _Encoding = encoding;
        }


        public override System.Text.Encoding Encoding
        {
            get
            {
                return _Encoding ?? base.Encoding;
            }
        }
    }
}
