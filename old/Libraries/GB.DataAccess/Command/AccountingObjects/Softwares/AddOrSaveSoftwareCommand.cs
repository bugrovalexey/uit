﻿using AutoMapper;
using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.WorksAndGoods;
using GB.Data.Directories;
using GB.DataAccess.Command.AccountingObjects.CharacteristicOfLicenses;
using GB.DataAccess.Command.AccountingObjects.WorkAndGood;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;
using GB.DataAccess.Services.Args;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.Softwares
{
    public class AddOrSaveSoftwareCommand : ICommand<AddOrSaveSoftwareContext, Software>
    {
        ISessionFactoryGb factory;

        public AddOrSaveSoftwareCommand()
        {
            factory = new SessionFactory();
        }

        public Software Execute(AddOrSaveSoftwareContext context)
        {
            Software soft;

            if (context.xId.HasValue && context.xId != 0)
                soft = new Repository<Software>().Get(context.xId.Value);
            else
            {
                soft = new Software();                
            }

            AutoMapper.Mapper.Map(context, soft);
            
            Manufacturer man;

            if (context.ManufacturerId.HasValue == false && string.IsNullOrWhiteSpace(context.ManufacturerName) == false)
            {
                var мanufacturerName = context.ManufacturerName.Trim();
                man = new Repository<Manufacturer>().GetAllEx(q => q.Where(x => x.Name == мanufacturerName)).FirstOrDefault();

                if (man == null)
                {
                    man = new BaseAddOrSaveCommand<AddOrSaveDirectoryContext, Manufacturer>().Execute(new AddOrSaveDirectoryContext()
                        //new CrudService<Manufacturer, EntityDirectoriesArgs>().Create(new EntityDirectoriesArgs()
                    {
                        Name = мanufacturerName,
                        xId = 0
                    });
                }

                soft.Manufacturer = man;
            }

            using (var session = factory.Create())
            {
                session.SaveOrUpdate(soft);
                session.Commit();
            }            

            var isNew = context.IdCharacteristicOfLicense == 0;

            if (soft.Right == RightsOnSoftwareEnum.NotExclusive)
            {
                var args = new AddOrSaveCharacteristicOfLicenseContext()
                {
                    Licensor = context.Licensor,
                    Model = context.Model,
                    Name = context.NameCharacteristicOfLicense,
                    NumberAllocatedSameTime = context.NumberAllocatedSameTime,
                    NumberOnlineSameTime = context.NumberOnlineSameTime,
                    ProductNumber = context.ProductNumber,
                    SoftwareId = soft.Id,
                    TypeOfConnectionToServer = context.TypeOfConnectionToServer,
                    xId = context.IdCharacteristicOfLicense
                };

                var characteristic = new BaseAddOrSaveCommand<AddOrSaveCharacteristicOfLicenseContext, CharacteristicOfLicense>().Execute(args);

                soft.CharacteristicsOfLicense = characteristic;
            }
            else if (isNew == false)
            {
                new BaseDeleteCommand<BaseAddOrSaveContext, CharacteristicOfLicense>().Execute(new BaseAddOrSaveContext() { xId = context.IdCharacteristicOfLicense });
            }            

            new AddOrSaveWorkAndGoodsCommand().Execute(new AddOrSaveWorkAndGoodsContext()
            {
                Type = CategoryTypeOfSupportEnum.Software,
                AccountingObjectId = context.AccountingObjectId,
                CategoryKindOfSupportId = context.CategoryKindOfSupportId,
                Name = context.Name,
                ObjectId = soft.Id,
                Summa = context.Summa,
                IsNew = !context.xId.HasValue || context.xId == 0
            });

            return soft;
        }
    }
}
