﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    using System;

    public class PlansActivityAct : EntityBase
    {
        public virtual string Name { get; set; }

        public virtual string Number { get; set; }

        public virtual DateTime? DateAdoption { get; set; }

        public virtual int Cost { get; set; }

        public virtual int? FileId { get; set; }

        public virtual string FileName { get; set; }

        public virtual PlansActivity Activity { get; set; }

        public virtual DateTime? ChangeDate { get; set; }
    }
}
