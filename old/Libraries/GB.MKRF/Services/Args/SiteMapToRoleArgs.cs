﻿namespace GB.MKRF.Services.Args
{
    public class SiteMapToRoleArgs : BaseArgsNew, IServiceArgs
    {
        public int SiteMapId { get; set; }

        public int RoleId { get; set; }
    }
}