﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    public class AccountingObjectChangeStatusStoryContext
    {
        /// <summary>
        /// Идентификатор ОУ
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Тип объекта
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Идентификатор Нового статуса ОУ
        /// </summary>
        public string Status { get; set; }
    }
}
