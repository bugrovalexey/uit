﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201801231120)]
    public class Migration_201801231120 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.UniqueConstraint("UK_OU_User_Comment").OnTable("OU_User_Comment").Columns("OU_ID", "Users_ID", "Status_ID");
        }
    }
}
