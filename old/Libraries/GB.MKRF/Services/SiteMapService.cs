﻿using GB.Data.Admin;
using GB.MKRF.Helpers;
using GB.MKRF.Services.Args;
using NHibernate;
using System.Linq;

namespace GB.MKRF.Services
{
    public class SiteMapService : ServiceBase<SiteMapNode>, IService<SiteMapNode, SiteMapArgs>
    {
        public SiteMapNode Create(SiteMapArgs args)
        {
            return SaveOrUpdate(new SiteMapNode(), args);
        }

        public SiteMapNode Update(SiteMapArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private SiteMapNode SaveOrUpdate(SiteMapNode entity, SiteMapArgs args)
        {
            entity.FillSiteMap(args);

            base.SaveOrUpdate(entity);

            return entity;
        }

        public void Delete(SiteMapArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            if (entity.Children.Count > 0)
            {
                throw new ObjectDeletedException("Узел имеет подчиненные узлы", args.Id, "Карта сайта");
            }

            base.Delete(args.Id);
        }

        public void CopyRolesToChildrenSiteMap(int siteMapId)
        {
            var parent = Repository.GetExisting(siteMapId);

            var siteMapToRoleService = new SiteMapToRoleService();

            foreach (var child in parent.Children)
            {
                var deleteSiteMapToRoles = child.Roles.Except(parent.Roles);

                foreach (var deleteSiteMapToRole in deleteSiteMapToRoles)
                {
                    siteMapToRoleService.Delete(new SiteMapToRoleArgs() { Id = deleteSiteMapToRole.Id });
                }

                var addSiteMapToRoles = parent.Roles.Except(child.Roles);

                foreach (var addSiteMapToRole in addSiteMapToRoles)
                {
                    var newSiteMapToRole = new SiteMapToRole();

                    newSiteMapToRole.SiteMap = child;
                    newSiteMapToRole.Role = addSiteMapToRole.Role;

                    //new Repository<SiteMapToRole>().SaveOrUpdate(newSiteMapToRole);
                    InternalStaticMethods.Insert(newSiteMapToRole);
                }
            }
        }
    }
}