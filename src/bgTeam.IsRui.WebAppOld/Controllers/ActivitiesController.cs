﻿using bgTeam.DataAccess;
using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess.Command.Activities;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Domain.Entities.Workflow;
using bgTeam.IsRui.Domain.Extensions;
using bgTeam.IsRui.MKRF.Logic.Tabs;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Activities;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Controllers
{
    [FilterException, FilterAction]
    public partial class ActivitiesController : ControllerBase
    {
        public readonly ICommandBuilder _command;
        private readonly IUserRepository _userRepository;
        private readonly IStatusRepository _statusRepository;

        public ActivitiesController(
            ICommandBuilder command,
            IUserRepository userRepository,
            IStatusRepository statusRepository)
        {
            _command = command;
            _userRepository = userRepository;
            _statusRepository = statusRepository;
        }

        /// <summary>
        /// Точка входа в раздел
        /// </summary>
        public ActionResult Index()
        {
            var handler = Singleton<TabsManager>.Instance.GetHandler<ActivityTabs>();

            return RedirectToAction(handler.GetAction());
        }

        /// <summary>
        /// Точка входа в карточку
        /// </summary>
        public ActionResult IndexCard(int id)
        {
            var handler = Singleton<TabsManager>.Instance.GetHandler<ActivityCardTabs>();

            return RedirectToAction(handler.GetAction(), new { id });
        }

        public ActionResult Create()
        {
            //if (!Singleton<AccessRightsProvider>.Instance.CanCreate(EntityType.PlansActivity))
            //    throw new ObjectAccessException();

            return View(new CreateVeiwModel());
        }


        [HttpPost]
        public ActionResult Create(CreateVeiwModel m)
        {
            return NotificationHelper.NotificationCreateEdit(delegate()
            {
                new ActivityService(_userRepository, _statusRepository).Create(m);
            });
        }

        public ActionResult List()
        {
            return View(new ListModel(_userRepository));
        }

        public ActionResult ListApprove()
        {
            return View(new ListApproveModel(_userRepository));
        }

        public ActionResult ListExpertise()
        {
            return View(new ListExpertiseModel());
        }

        [HttpPost]
        public ActionResult CommentList(int activityId)
        {
            var list = new RepositoryDc<ActivityComment>().GetAllEx(q => q
                .Where(x => x.Activity.Id == activityId)
                .JoinQueryOver(x => x.User)
                .JoinQueryOver(x => x.Department));

            return Json(new
            {
                Comments = list.Select(x => new
                {
                    User = x.User.FullName,
                    UserRole = string.Join("; ", x.User.Roles.Select(c => c.Name)),
                    UserOrganization = x.User.Department.Name,
                    Date = x.Date.ToShortDateString(),
                    Text = x.Text,
                })
            });
        }

        [HttpPost]
        public ActionResult AddComment(int activityId, string text)
        {
            return NotificationHelper.NotificationCreateEdit(delegate()
            {
                new ActivityCommentService(_userRepository).Create(new ActivityCommentArgs()
                {
                    Activity = activityId,
                    Text = text,
                    Page = this.Request.UrlReferrer.AbsolutePath,
                });
            });
        }


        [HttpPost]
        public ActionResult Delete(int id)
        {
            return NotificationHelper.NotificationDelete(id, delegate()
            {
                new ActivityService(_userRepository, _statusRepository).Delete(new PlansActivityArgs() { Id = id });
            });
        }

        public ActionResult CommonDataSingle(int id)
        {
            return View(new CommonDataModel(_statusRepository, id));
        }

        [HttpPost]
        public ActionResult CommonDataSingleEdit(CreateAccountingObjectContext data)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                _command.Build(data).Execute();
            });
        }

        public ActionResult ExpensesSingle(int Id)
        {
            return View(new ExpensesViewModel(_statusRepository, Id));
        }

        [HttpPost]
        public ActionResult ExpensesSingleEdit(ExpensesArgs data)
        {
            return NotificationHelper.NotificationCreateEdit(delegate()
            {
                //TODO : Сделать рефакторинг
                data.VolY0 = data.tVolY0;
                data.VolY1 = data.tVolY1;
                data.VolY2 = data.tVolY2;

                new ActivityService(_userRepository, _statusRepository).Update4(data);
            });
        }

        /// <summary>
        /// Перевести статус
        /// </summary>
        [HttpPost]
        public ActionResult StatusChange(int id, int status)
        {
            return NotificationHelper.NotificationExecute(delegate()
                {
                    var activity = new RepositoryDc<PlansActivity>().GetExisting(id);
                    var conclusion = StatusEnum.ActOnExpertise.IsEq(activity) ? activity.ExpertiseConclusion : null;
                    //var rep = new StatusRepository();
                    var statusService = new StatusService();

                    //var manager = Singleton<WorkflowManager>.Instance;

                    ////Перед переводом еще раз убеждаемся, что у пользователя есть право на перевод статуса
                    ////Чтобы не было дублирования в случае, если пользователь делал перевод, на странице произошла ошибка, но перевод удался
                    //if (rep.GetNext(activity, conclusion).Any(x => x.To.Id == status))
                    //{
                    //    if (conclusion == null) // если текущий статус не "На экспертизе в ЦА"
                    //    {
                    //        manager.Change(activity, status);
                    //    }
                    //    else
                    //    {
                        //    if (status == (int)StatusEnum.ActOnCompletion)
                        //    {
                        //        у экспертизы надо сбросить статус на ExpAppointment - Назначаются эксперты

                        //        manager.Change(activity, status);
                        //        manager.Change(conclusion, (int)StatusEnum.ExpAppointment);
                        //    }
                        //    else if (status == (int)StatusEnum.ActAgreed)
                        //    {
                        //        manager.Change(activity, status);
                        //    }
                        //    else
                        //    {
                        //        manager.Change(conclusion, status);
                        //    }
                        //}
                    //}
                    //else
                    //{
                    //    throw new Exception("Статус не доступен для перевода");
                    //}
                },
                "Статус успешно переведён",
                "При переводе статуса возникла ошибка");
        }

        #region InformationInteraction

        public ActionResult InformationInteraction(int id)
        {
            return View(new InformationInteractionViewModel(_statusRepository, id));
        }

        #endregion InformationInteraction
    }
}