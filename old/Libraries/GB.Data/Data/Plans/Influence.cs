﻿namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>Влияние мероприятия по информатизации на индикаторы и (или) показатели ГП, ФЦП, ВЦП 
    /// (концепций, стратегий и иных долгосрочных актов определяющих приоритеты информатизации)</summary>
    public class Influence : EntityBase
    {
        protected Influence()
        {
        }

        public Influence(Reference Reference)
        {
            this.Reference = Reference;
        }

		/// <summary>Проект (справка)</summary>
		public virtual Reference Reference { get; protected set; }

        /// <summary>Индикатор / показатель</summary>
        public virtual string Indicator { get; set; }

        /// <summary>Документ-источник</summary>
        public virtual string Document { get; set; }
    }
}
