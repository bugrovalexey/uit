﻿using System.Web.Mvc;
using bgTeam.IsRui.MKRF.Logic.Tabs;
using bgTeam.IsRui.WebApp.ViewModels.Analytics;
using bgTeam.IsRui.Common;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public class AnalyticsController : Controller
    {
        public ActionResult Index()
        {
            var handler = Singleton<TabsManager>.Instance.GetHandler<AnalyticsTabs>();

            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            return View(new AnalyticsViewModel());
        }

        [HttpPost]
        public ActionResult DataForChart(int? id)
        {
            var vm = new AnalyticsViewModel();
            var res = new JsonResult() { Data = vm.GetChartData(id) };
            return res;
        }
    }
}
