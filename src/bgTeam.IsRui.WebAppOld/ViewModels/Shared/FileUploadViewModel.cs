﻿using bgTeam.IsRui.Domain;
using System.Collections.Generic;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.Shared
{
    public class FileUploadViewModel
    {
        public string Name { get; set; }

        public IEnumerable<IEntityName> Files { get; set; }

        public bool Multi { get; set; }

        public bool ReadOnly { get; set; }

        public string DisplayFormat { get; set; }

        public string Extentions { get; set; }

        public string Exts
        {
            get
            {
                return string.Concat("'", Extentions, "'");
            }
        }

        public FileUploadViewModel()
        {
        }

        public FileUploadViewModel(bool readOnly, string ext, bool multi = false)
        {
            ReadOnly = readOnly;
            Extentions = ext;
            Multi = multi;

            DisplayFormat = "<a href=\"{0}\">{1}</a>";
        }
    }

    public class FileUploadMultiViewModel
    {
        public string Name { get; set; }

        public IEnumerable<string> Extentions { get; set; }

        public string Exts
        {
            get
            {
                return Extentions.Count() == 0 ? string.Empty : string.Join(", ", Extentions.Select(x => string.Format("'{0}'", x.Trim())));
            }
        }

        public string OnCompleteJSFunction { get; set; }
    }
}