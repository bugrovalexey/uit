﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    /// <summary>
    /// Показатели ИКТ
    /// </summary>
    class ShowingIKTMap : ClassMapBase<ShowingIKT>
    {
        public ShowingIKTMap() :
            base("Showing_IKT")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Showing_IKT_Name");

            References<MeasureUnit>(x => x.Measure, "Measure_Unit_ID");
            References<ClassComponentIKT>(x => x.ClassComponentIKT, "Class_Component_IKT_ID");

            HasManyToMany(x => x.ActivityTypes)
                .Table("Showing_Type")
                .ParentKeyColumn("Showing_IKT_ID")
                .ChildKeyColumn("Plans_Activity_Type2_ID")
                .ReadOnly();

            HasManyToMany(x => x.IKTComponents)
                .Table("Showing_IKT_Component")
                .ParentKeyColumn("Showing_IKT_ID")
                .ChildKeyColumn("IKT_Component_ID")
                .ReadOnly();
        }
    }
}