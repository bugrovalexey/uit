﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;

    public class ClaimCreateOrUpdateMarkStoryContext
    {
        public int? Id { get; set; }

        public int? ClaimId { get; set; }

        /// <summary>
        /// Пояснение связи с показателем проекта
        /// </summary>
        public string ExplanationRelationshipWithProjectMark { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Алгортим
        /// </summary>
        public string AlgorithmOfCalculating { get; set; }

        /// <summary>
        /// Базовое значение
        /// </summary>
        public int? BaseValue { get; set; }

        /// <summary>
        /// Целевое значение
        /// </summary>
        public int? TargetedValue { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public int? UnitId { get; set; }

        /// <summary>
        /// Дата достижения целевого значения
        /// </summary>
        public DateTime? DateToAchieveGoalValue { get; set; }

        /// <summary>
        /// Обоснование
        /// </summary>
        public string Justification { get; set; }

        /// <summary>
        /// True - когда источник формирования показателя - Госпрограмма
        /// False - Другой источник
        /// </summary>
        public bool SourceIsStateProgram { get; set; }

        /// <summary>
        /// Наименование источника формирования
        /// </summary>
        public string SourceName { get; set; }

        /// <summary>
        /// Пояснение связи с показателем госпрограммы
        /// </summary>
        public string ExplanationRelationshipWithStateProgramMark { get; set; }

        /// <summary>
        /// Показатель гос. программы
        /// </summary>
        public int? StateProgramMarkId { get; set; }
    }
}
