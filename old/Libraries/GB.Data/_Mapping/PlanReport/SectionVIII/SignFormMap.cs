﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
    public class SignFormMap : ClassMapBase<SignForm>
    {
        public SignFormMap()
            : base("PRS_8_6_Sign_Form")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_6_Sign_Form_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_8_6_Sign_Form_Name = ? 
//						  ,@PRS_8_6_Sign_Form_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_6_Sign_Form_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_6_Sign_Form_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_6_Sign_Form_Delete @PRS_8_6_Sign_Form_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "PRS_8_6_Sign_Form_Name");
        }
    }
}