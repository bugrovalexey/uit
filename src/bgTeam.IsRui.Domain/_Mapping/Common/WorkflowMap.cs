﻿namespace bgTeam.IsRui.Domain._Mapping.Common
{
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;

    internal class WorkflowMap : ClassMapHiLoNew<Workflow>
    {
        public WorkflowMap()
            : base("Workflow")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.TimeStamp, "TimeStamp");

            References(x => x.Status, "StatusId");

            Map(x => x.EntityType, "EntityType").ReadOnly();
            ReferencesAny(x => x.EntityOwner)
                .AddMetaValue<PlansActivity>(((int)EntityType.PlansActivity).ToString())
                .AddMetaValue<AccountingObject>(((int)EntityType.AccountingObject).ToString())
                .AddMetaValue<Claim>(((int)EntityType.ClaimWorkflow).ToString())
                .EntityTypeColumn("EntityType")
                .EntityIdentifierColumn("EntityOwnerId").IdentityType<int>();
        }
    }
}
