﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807220029)]
    public class Migration_201807220029 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Plans_Activity_Works")
                .AddColumn("Plans_Activity_Works_Quarter").AsString(32).Nullable();
        }
    }
}
