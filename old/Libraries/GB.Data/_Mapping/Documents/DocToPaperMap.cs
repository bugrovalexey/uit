﻿using GB.MKRF.Entities.Documents;

namespace GB.MKRF.Mapping.Documents
{
    class DocToPaperMap : ClassMapBase<DocToPaper>
    {
        public DocToPaperMap()
            : base("Doc_Papper")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Type, "Doc_Papper_Type").CustomType<DocToPaperType>().Not.Nullable();
            Map(x => x.IsExpertConclusion, "Doc_Papper_Is_Concl");

            References<Document>(x => x.Document, "Document_ID").Not.Nullable();
            References<PaperObject>(x => x.PaperObject, "Plans_Paper_ID").Not.Nullable();
        }
    }
}