﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class DecommissioningViewModel : StepBaseViewModel
    {
        #region consts

        private const string DecommissioningDateCaption = "Дата вывода из эксплуатации";
        private const string DocNameCaption = "Наименование документа";
        private const string DocNumberCaption = "Номер документа";
        private const string DocDateAdoptionCaption = "Дата принятия документа";
        private const string FilesDataCaption = "Документ";

        #endregion consts

        public DecommissioningViewModel(int id)
            : base(id, MasterItemEnum.Decommissioning)
        {
            DecommissioningFilesData =
                Current.DocumentDecommissioning.With(dc => dc.Documents).Return(
                    docs => docs.Select(x => new SelectEntityInfo() {Id = x.Id, Name = x.Name}),
                    new List<SelectEntityInfo>());
        }

        [DisplayName(DocNameCaption)]
        public string DecommissioningDocName
        {
            get { return Current.DocumentDecommissioning.Return(dc => dc.Name, default(string)); }
        }

        [DisplayName(DocNumberCaption)]
        public string DecommissioningDocNumber
        {
            get { return Current.DocumentDecommissioning.Return(dc => dc.Number, default(string)); }
        }

        [DisplayName(DocDateAdoptionCaption)]
        public string DecommissioningDocDateAdoption
        {
            get { return Current.DocumentDecommissioning.Return(dc => dc.DateAdoption.ToStringOrDefault(), default(string)); }
        }

        [DisplayName(DecommissioningDateCaption)]
        public string DecommissioningDate
        {
            get { return Current.DecommissioningDate.ToStringOrDefault(); }
        }

        [DisplayName(FilesDataCaption)]
        public IEnumerable<IEntityName> DecommissioningFilesData { get; set; }

    }
}