﻿namespace bgTeam.IsRui.Story.Claims
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimCreateOrUpdateScopeOfWorkStoryContext
    {
        public int? Id { get; set; }

        public int? ClaimId { get; set; }

        /// <summary>
        /// Тип работы
        /// </summary>
        public string WorkType { get; set; }

        /// <summary>
        /// Количество запланированных часов
        /// </summary>
        public int WorkHours { get; set; }

        /// <summary>
        /// Описание работ
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Дата начала работ
        /// </summary>
        public DateTime? DateBegin { get; set; }

        /// <summary>
        /// Плановая дата окончания
        /// </summary>
        public DateTime? DatePlanEnd { get; set; }

        /// <summary>
        /// Трудозатраты, чел/часов
        /// </summary>
        public int LaborCostsHours { get; set; }

        /// <summary>
        /// id категории специалиста
        /// </summary>
        public int SpecialistId { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public decimal Cost { get; set; }
    }
}
