﻿namespace bgTeam.IsRui.DataAccess.Impl
{
    using bgTeam.DataAccess;
    using NHibernate;

    public sealed class SessionNHibernateFactory : ISessionNHibernateFactory
    {
        private static readonly object _lock = new object();

        private readonly ISessionFactory _factory;

        private ISessionNHibernate _session;

        public SessionNHibernateFactory(
            ISessionNHibernateConfiguration configuration,
            IConnectionSetting settings) 
            : this(configuration, settings.ConnectionString)
        {
        }

        public SessionNHibernateFactory(
            ISessionNHibernateConfiguration configuration,
            string connectionString)
        {
            var conf = configuration.GetConfiguration(connectionString);

            _factory = conf.BuildSessionFactory();

            //var session = _factory.OpenSession();

            //session.FlushMode = FlushMode.Manual;
        }

        public ISessionNHibernate OpenSession()
        {
            lock (_lock)
            {
                if (_session == null)
                {
                    _session = CreateSession();
                }
            }

            return _session;
        }

        private ISessionNHibernate CreateSession()
        {
            var session = _factory.OpenSession();

            session.FlushMode = FlushMode.Manual;

            return new SessionNHibernate(session);
        } 

        //public ISession CurrentSession
        //{
        //    get
        //    {
        //        if (CurrentSessionContext.HasBind(_sessionFactory))
        //            return _sessionFactory.GetCurrentSession();

        //        throw new InvalidOperationException(
        //            "Database access logic cannot be used, if session not opened. Implicitly session usage not allowed now. Please open session explicitly through UnitOfWorkFactory.StartLongConversation method");
        //    }
        //}
    }
}