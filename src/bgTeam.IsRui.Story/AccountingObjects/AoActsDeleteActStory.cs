﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoActsDeleteActStory : IStory<AoActsDeleteActStoryContext, bool>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IFileProviderRui _fileProvider;

        public AoActsDeleteActStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            ISessionNHibernateFactory factory,
            IFileProviderRui fileProvider)
        {
            _mapper = mapper;
            _repository = repository;
            _factory = factory;
            _fileProvider = fileProvider;
        }

        public bool Execute(AoActsDeleteActStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(AoActsDeleteActStoryContext context)
        {
            bool result = false;

            using (var sesion = _factory.OpenSession())
            {
                var act = _repository.Get<AccountingObjectActs>(context.Id.Value);

                await sesion.DeleteAsync<AccountingObjectActs>(context.Id.Value);

                if (act.FileId != null)
                {
                    await new FileDeleteStory(_repository, _fileProvider, _factory)
                            .ExecuteAsync(new FileDeleteStoryContext
                            {
                                Id = act.FileId,
                            });
                }

                result = true;
            }

                return result;
        }
    }
}
