﻿<%@ Page Title="Копирование мероприятия" Language="C#" MasterPageFile="~/MainEmpty.Master" AutoEventWireup="true" CodeBehind="CopyActivity.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.CopyActivity" %>
<asp:Content ID="ContentHdr" ContentPlaceHolderID="head" runat="server">
    <style>
        .itemText {
            white-space: normal;
        }

        .activityText {
            font-size: 1.2em;
        }

        .iktText {
            font-size: 1.1em;
        }

        .subCommand {
            font-size: 0.9em;
            /*margin-left: 0.4em;*/
            margin-right: 1.4em;
        }

        .subText {
            font-size: 0.8em;
            color: gray;
        }

        tr.disabledRow {
            background-color: gray;
        }

        .lastCell {
            border-right: 1px solid #cfcfcf !important;
        }

        /*.firstCell {
            border-left: 1px solid #cfcfcf !important;
        }*/

        
        .dxtlIndentWithButton_MetropolisBlue {
            border-top: 1px solid #cfcfcf !important;
        }

        /*.dxtlIndent_MetropolisBlue*/
        .dxtl__IM {
            border-left: 1px solid #cfcfcf !important;
        }

        a.subCommand:hover {
            color: #f12938;
        }

        .dxtlDataTable {
            /*border-bottom: 1px solid #cfcfcf !important;*/
            border-collapse: collapse !important;
        }


        .dxtlNode_MetropolisBlue:hover {
            background-color: #fffcda;
        }

        .headerSum {
            white-space: nowrap;
            display: block;
        }

        .headerNote {
            color: red;
            display: inline-block;
        }

        .sumRed {
            color: red;
        }

        .sumGreen {
            color: green;
        }
    </style>

</asp:Content>
  
<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <%@ Import Namespace="GB.MKRF.Controllers" %>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function StartEditNewNode(key, parentKey) {
                hf.Set('newNodeKey', key);
                hf.Set('parentNodeKey', parentKey);
                treeView.StartEditNewNode(parentKey);
            }

            function treeUpdateEdit() {

                if (!treeView.cpCurrentForm)
                    return;

                if (ASPxClientEdit.ValidateGroup(treeView.cpCurrentForm))
                    treeView.UpdateEdit();
            }

            function validateComboBox(s, e) {
                var cbValue = s.GetValue();
                e.isValid = (cbValue != undefined || cbValue) && cbValue != 0;
            }

            var copyTask = [];
            var copyArgs = null;

            function doWork() {

                copyTask = [];

                if (!treeView.cpActivityList)
                    return;

                var activityList = treeView.cpActivityList.split(',');

                for (var i = activityList.length-1; i >= 0; i--) {
                    copyTask.push(activityList[i]);
                    //copyCallback.PerformCallback('doCopy' + activityList[i]);
                }

                copyArgs = {
                    total: activityList.length,
                };

                LoadingPanel.SetText("");
                LoadingPanel.Show();

                doCopy();
            }

            function doCopy() {
                var id = copyTask.pop();

                if (id) {
                    LoadingPanel.SetText('Создание копии мероприятия № ' + (copyArgs.total - copyTask.length) + ' из ' + copyArgs.total);
                    copyCallback.PerformCallback('doCopy' + id);
                }
                else {
                    LoadingPanel.SetText('Копирование завершено. Обновление страницы.');
                    parent.location.reload(true);
                }
            }

            function copyCallbackError(s, e) {
                // ошибка
                e.handled = true;
            }
        
            function copyCallbackComplete(s, e) {
                if (e.parameter) {
                    if (e.parameter.indexOf('doCopy') == 0) {
                        doCopy();
                    }
                    else if (e.parameter.indexOf('clone') == 0) {
                        pnl.PerformCallback();
                    }
                }
            }          

            function CloneActivity(s, e) {
                copyCallback.PerformCallback('clone');
            }

            function getSelectionText() {
                var text = "";
                if (window.getSelection) {
                    text = window.getSelection().toString();
                } else if (document.selection && document.selection.type != "Control") {
                    text = document.selection.createRange().text;
                }
                return text;
            }

            var priorKey = '<%=Controller.GetProirityKey(true)%>'.toUpperCase();
            var nonPriorKey = '<%=Controller.GetProirityKey(false)%>'.toUpperCase();

            function OnNodeClick(s, e) {
                if (e.nodeKey.toUpperCase() === priorKey || e.nodeKey.toUpperCase() === nonPriorKey)
                    return;

                if (!getSelectionText()) {
                    var delay = 100;
                    setTimeout(function () {
                        s.StartEdit(e.nodeKey);
                    }, delay);
                }
            }

        </script>
        <dxlp:ASPxLoadingPanel runat="server" ID="LoadingPanel" ClientInstanceName="LoadingPanel" Modal="True">
        </dxlp:ASPxLoadingPanel>
    
        <dxc:ASPxCallback ID="copyCallback" runat="server" ClientInstanceName="copyCallback"
            OnCallback="copyCallback_Callback">
            <ClientSideEvents CallbackError="function(s,e){}" CallbackComplete="copyCallbackComplete" />
        </dxc:ASPxCallback>
        <%--<span style='color:#C53939; font-size:1.3em'><%=Controller.Source.Code%></span> — <%=Controller.Source.Name%>--%>
        <%--<br />--%>
        <table>
            <tr>
                <td>
                    <dxe:ASPxButton ID="addActivityButton" runat="server" ClientInstanceName="addActivityButton"
                        AutoPostBack="false" Text="Добавить мероприятие" Style="margin-top: 10px; margin-bottom: 10px;">
                        <ClientSideEvents Click="function(s,e) { StartEditNewNode('activity@0'); }" />
                    </dxe:ASPxButton>

                </td>
                <td>
                    <dxe:ASPxButton ID="cloneActivityButton" runat="server" ClientInstanceName="cloneActivityButton"
                        AutoPostBack="false" Text="Добавить копию основного мероприятия" Style="margin-top: 10px; margin-bottom: 10px;">
                        <ClientSideEvents Click="function(s,e) { CloneActivity(); }" />
                    </dxe:ASPxButton>
                </td>
                <td>
                    <dxe:ASPxCheckBox runat="server" ID="deleteActivityCheckBox" ClientInstanceName="deleteActivityCheckBox" Text="Удалить исходное мероприятие после разъединения" Checked="false"></dxe:ASPxCheckBox>
                </td>
            </tr>
        </table>

   
       <%--PerformCallback--%>
        <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf"></dxhf:ASPxHiddenField>
        <dxcp:ASPxCallbackPanel runat="server" ID="pnl" ClientInstanceName="pnl" OnCallback="pnl_Callback">
            <PanelCollection>
                <dxp:PanelContent>
                    <dxtl:ASPxTreeList runat="server" ID="treeView" ClientInstanceName="treeView" EnableCallbacks="true" KeyFieldName="Key" ParentFieldName="ParentKey" Width="100%" 
            OnVirtualModeCreateChildren="treeView_VirtualModeCreateChildren"
            OnVirtualModeNodeCreating="treeView_VirtualModeNodeCreating"
            OnNodeInserting="treeView_NodeInserting"
            OnNodeUpdating="treeView_NodeUpdating"
            OnNodeDeleting="treeView_NodeDeleting"
            OnCustomJSProperties="treeView_CustomJSProperties"
            OnHtmlDataCellPrepared="treeView_HtmlDataCellPrepared"
            OnStartNodeEditing="treeView_StartNodeEditing"
            OnInitNewNode="treeView_InitNewNode"
            OnCommandColumnButtonInitialize="treeView_CommandColumnButtonInitialize">
            <BorderBottom BorderColor="#cfcfcf" BorderStyle="Solid" BorderWidth="1px" />
            <Settings ScrollableHeight="600" GridLines="Horizontal" />
            <SettingsBehavior AllowDragDrop="false" />
            <SettingsEditing Mode="PopupEditForm" AllowRecursiveDelete="true" AllowNodeDragDrop="false" />
            <SettingsPopupEditForm Width="750px" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Modal="true" ShowHeader="true" />
            <%--<ClientSideEvents NodeClick="OnNodeClick" />--%>
            <Templates>
                <EditForm>
                    <table class="form_edit">
                        <%--==============================================================================================================================================--%>
                        <asp:PlaceHolder runat="server" ID="phActivity" Visible='<%#CopyActivityController.IsActivity(currentNodeKey)%>'>
                            <tr>
                                <td class="form_edit_desc" colspan="2">
                                    <dxe:ASPxCheckBox runat="server" ID="importExpenses" Text="Импортировать расходы, товары и работы из основного мероприятия" TextAlign="Right">
                                    </dxe:ASPxCheckBox>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">
                                    <span class="required"></span>
                                    Наименование
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxMemo ID="ActivityNameMemo" runat="server" ClientInstanceName="ActivityNameMemo" Rows="5" Text='<%# Eval("Name") %>'>
                                        <ValidationSettings ErrorDisplayMode="None" RequiredField-IsRequired="true" ValidationGroup="activity" />
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">Приоритетное
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxCheckBox runat="server" ID="PriorityCheckBox" ClientInstanceName="PriorityCheckBox" Checked='<%# getValue<bool>(Eval("JSON"), "IsPriority") %>'>
                                    </dxe:ASPxCheckBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <p class="header_grid">
                                        Объем планируемых бюджетных ассигнований, тыс. руб
                                            <span class="subtitle">Заполняется на первом этапе
                                            </span>
                                    </p>
                                    <table>
                                        <tr>
                                            <td></td>
                                            <td class="form_edit_input">Основной объем</td>
                                            <td class="form_edit_input">Дополнительная потребность</td>
                                        </tr>
                                        <tr>
                                            <td class="form_edit_desc">на <%=Controller.Source.Year0 %> г.
                                            </td>
                                            <td class="form_edit_input">
                                                <dxe:ASPxSpinEdit ID="PlansActivityVolY0SpinEdit" runat="server" AllowNull="False" Width="194"
                                                    ClientInstanceName="PlansActivityVolY0SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                                                    HorizontalAlign="Right" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("Y0")) %>'>
                                                </dxe:ASPxSpinEdit>
                                            </td>
                                            <td class="form_edit_input">
                                                <dxe:ASPxSpinEdit ID="PlansActivityAddVolY0SpinEdit" runat="server" AllowNull="False"
                                                    ClientInstanceName="PlansActivityAddVolY0SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                                                    HorizontalAlign="Right" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("AddY0")) %>' Width="194">
                                                </dxe:ASPxSpinEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form_edit_desc">на <%=Controller.Source.Year1 %> г.
                                            </td>
                                            <td class="form_edit_input">
                                                <dxe:ASPxSpinEdit ID="PlansActivityVolY1SpinEdit" runat="server" AllowNull="False" Width="194"
                                                    ClientInstanceName="PlansActivityVolY1SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                                                    HorizontalAlign="Right" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("Y1")) %>'>
                                                </dxe:ASPxSpinEdit>
                                            </td>
                                            <td class="form_edit_input">
                                                <dxe:ASPxSpinEdit ID="PlansActivityAddVolY1SpinEdit" runat="server" AllowNull="False"
                                                    ClientInstanceName="PlansActivityAddVolY1SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                                                    HorizontalAlign="Right" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("AddY1")) %>' Width="194">
                                                </dxe:ASPxSpinEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form_edit_desc">на <%=Controller.Source.Year2 %> г.
                                            </td>
                                            <td class="form_edit_input">
                                                <dxe:ASPxSpinEdit ID="PlansActivityVolY2SpinEdit" runat="server" AllowNull="False" Width="194"
                                                    ClientInstanceName="PlansActivityVolY2SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                                                    HorizontalAlign="Right" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("Y2")) %>'>
                                                </dxe:ASPxSpinEdit>
                                            </td>
                                            <td class="form_edit_input">
                                                <dxe:ASPxSpinEdit ID="PlansActivityAddVolY2SpinEdit" runat="server" AllowNull="False"
                                                    ClientInstanceName="PlansActivityAddVolY2SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                                                    HorizontalAlign="Right" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("AddY2")) %>' Width="194">
                                                </dxe:ASPxSpinEdit>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <%--==============================================================================================================================================--%>
                        <asp:PlaceHolder runat="server" ID="phIkt" Visible='<%#CopyActivityController.IsIkt(currentNodeKey)%>'>
                            <tr>
                                <td class="form_edit_desc">Вид затрат
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxComboBox ID="ExpenseDirectionComboBox" runat="server" Value='<%# getValue<int?>(Eval("JSON"), "ExpId") %>'
                                        TextField="Value" ValueField="Key" ValueType="System.Int32" EncodeHtml="false" Width="450px"
                                         OnDataBinding="ExpenseDirectionComboBox_DataBinding">
                                        <ValidationSettings ErrorText="Обязательное поле" ErrorDisplayMode="None" ValidationGroup="ikt">
                                            <RequiredField IsRequired="true" />
                                        </ValidationSettings>
                                        <ClientSideEvents Validation="function(s,e){validateComboBox(s,e);}" />
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>

                            <tr>
                                <td><b>Основной объем, тыс. руб</b></td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">очередной финансовый год</td>
                                <td class="form_edit_input">
                                    <dxe:ASPxSpinEdit ID="IktY0" runat="server" AllowNull="False" Width="194" HorizontalAlign="Right" 
                                        DecimalPlaces="4" DisplayFormatString="#,##0.0000" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("Y0")) %>'>
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">первый год планового периода</td>
                                <td class="form_edit_input">
                                    <dxe:ASPxSpinEdit ID="IktY1" runat="server" AllowNull="False" Width="194" HorizontalAlign="Right"
                                        DecimalPlaces="4" DisplayFormatString="#,##0.0000" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("Y1")) %>'>
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">второй год планового периода</td>
                                <td class="form_edit_input">
                                    <dxe:ASPxSpinEdit ID="IktY2" runat="server" AllowNull="False" Width="194" HorizontalAlign="Right"
                                        DecimalPlaces="4" DisplayFormatString="#,##0.0000" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("Y2")) %>'>
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>

                            <tr>
                                <td><b>Доп. потребность, тыс. руб</b></td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">очередной финансовый год</td>
                                <td class="form_edit_input">
                                   <dxe:ASPxSpinEdit ID="IktAddY0" runat="server" AllowNull="False" Width="194" HorizontalAlign="Right"
                                        DecimalPlaces="4" DisplayFormatString="#,##0.0000" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("Y2")) %>'>
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">первый год планового периода</td>
                                <td class="form_edit_input">
                                      <dxe:ASPxSpinEdit ID="IktAddY1" runat="server" AllowNull="False" Width="194" HorizontalAlign="Right"
                                        DecimalPlaces="4" DisplayFormatString="#,##0.0000" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("Y2")) %>'>
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">второй год планового периода</td>
                                <td class="form_edit_input">
                                      <dxe:ASPxSpinEdit ID="IktAddY2" runat="server" AllowNull="False" Width="194" HorizontalAlign="Right"
                                        DecimalPlaces="4" DisplayFormatString="#,##0.0000" Increment="100" MinValue="0" Number='<%# Convert.ToDecimal(Eval("Y2")) %>'>
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <%--==============================================================================================================================================--%>
                        <asp:PlaceHolder runat="server" ID="phWork" Visible='<%#CopyActivityController.IsWork(currentNodeKey)%>'>
                            <tr>
                                <td class="form_edit_desc">
                                    <span class="required"></span>
                                    Наименование
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxMemo ID="WorkNameMemo" runat="server" Rows="5" Text='<%# Eval("Name") %>'>
                                        <ValidationSettings ErrorDisplayMode="None" RequiredField-IsRequired="true" ValidationGroup="work" />
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">Код ОКДП
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxTextBox Text='<%# getValue<string>(Eval("JSON"), "OKDP")%>' ID="WorkOKDPTextBox" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">Код по БК КОСГУ
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxComboBox ID="ExpenditureItemsComboBox" runat="server" Value='<%# getValue<int>(Eval("JSON"), "ExpenditureId") %>' DropDownHeight="200px"
                                        TextField="Value" ValueField="Key" ValueType="System.Int32" EncodeHtml="false" Width="450px"
                                         OnDataBinding="ExpenditureItemsComboBox_DataBinding">
                                        <ValidationSettings ErrorText="Обязательное поле" ErrorDisplayMode="None" ValidationGroup="work">
                                            <RequiredField IsRequired="true" />
                                        </ValidationSettings>
                                        <ClientSideEvents Validation="function(s,e){validateComboBox(s,e);}" />
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <%--==============================================================================================================================================--%>
                        <asp:PlaceHolder runat="server" ID="phWorkType" Visible='<%#CopyActivityController.IsWorkType(currentNodeKey)%>'>
                            <tr>
                                <td class="form_edit_desc">Тип затрат
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxComboBox ID="WorkExpTypeComboBox" runat="server" Value='<%# getValue<int>(Eval("JSON"), "ExpTypeId") %>'
                                        TextField="Value" ValueField="Key" ValueType="System.Int32" EncodeHtml="false" Width="450px"
                                         OnDataBinding="WorkExpTypeComboBox_DataBinding">
                                        <ValidationSettings ErrorText="Обязательное поле" ErrorDisplayMode="None" ValidationGroup="worktype">
                                            <RequiredField IsRequired="true" />
                                        </ValidationSettings>
                                        <ClientSideEvents Validation="function(s,e){validateComboBox(s,e);}" />
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class='form_edit_desc'>Объем бюджетных ассигнований</td>
                                <td class='form_edit_input'>
                                    <dxe:ASPxTextBox ID="WorkTypeY0" runat="server" MaskSettings-Mask="<0..999999999g>.<0000..9999>" Text='<%# Eval("Y0")%>' />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <%--==============================================================================================================================================--%>
                        <asp:PlaceHolder runat="server" ID="phSpec" Visible='<%#CopyActivityController.IsSpec(currentNodeKey)%>'>
                            <tr>
                                <td class="form_edit_desc">Наименование оборудования, программного обеспечения
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxMemo runat="server" ID="SpecNameMemo" Rows="5" Text='<%# Eval("Name") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">Тип оборудования (программного обеспечения)
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxComboBox ID="QualifierComboBox" runat="server" ValueType="System.Int32" ValueField="Key" TextField="Value"
                                        Value='<%# getValue<int ?>(Eval("JSON"), "QualifierId") %>' EncodeHtml="false" OnDataBinding="QualifierComboBox_DataBinding" />
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">Код ОКДП
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxTextBox ID="SpecOKDPTextBox" runat="server" Text='<%# getValue<string>(Eval("JSON"), "OKDP")%>' />
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc"><span class="required"></span>Тип затрат
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxComboBox ID="SpecExpTypeComboBox" runat="server" TextField="Value" ValueField="Key" ValueType="System.Int32"
                                        EncodeHtml="false" OnDataBinding="SpecExpTypeComboBox_DataBinding" Value='<%# getValue<int>(Eval("JSON"), "ExpTypeId") %>'>
                                        <ValidationSettings ValidationGroup="spec" ErrorDisplayMode="None"></ValidationSettings>
                                        <ClientSideEvents Validation="function(s,e){validateComboBox(s,e);}" />
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc">Аналог для расчета стоимости оборудования (лицензии программного обеспечения)
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxMemo runat="server" ID="AnalogMemo" Rows="5" Text='<%# getValue<string>(Eval("JSON"), "Analog") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc ">Количество оборудования (лицензий программного обеспечения), шт.
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxSpinEdit runat="server" ID="Quantity_SpinEdit" DecimalPlaces="0" AllowNull="False" MinValue="0" MaxValue="100000000"
                                        DisplayFormatString="#,##0" Increment="1" HorizontalAlign="Right" Number='<%# getValue<decimal>(Eval("JSON"), "Quantity") %>'>
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc ">Цена аналога, тыс. руб.
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxSpinEdit runat="server" ID="AnalogPriceSpinEdit" DecimalPlaces="4" AllowNull="False" MinValue="0" MaxValue="100000000"
                                        DisplayFormatString="#,##0.0000" Increment="100" HorizontalAlign="Right" Number='<%# getValue<decimal>(Eval("JSON"), "AnalogPrice") %>'>
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                            <tr>
                                <td class="form_edit_desc ">Стоимость оборудования (лицензии программного обеспечения), тыс. руб.
                                </td>
                                <td class="form_edit_input">
                                    <dxe:ASPxSpinEdit runat="server" ID="CostSpinEdit" DecimalPlaces="4" AllowNull="False" MinValue="0" MaxValue="100000000"
                                        DisplayFormatString="#,##0.0000" Increment="100" HorizontalAlign="Right" Number='<%#  getValue<decimal>(Eval("JSON"), "Cost") %>'>
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <%--==============================================================================================================================================--%>
                        <asp:PlaceHolder runat="server" ID="phSpecType" Visible='<%#CopyActivityController.IsSpecType(currentNodeKey)%>'>
                            <tr>
                                <td class='form_edit_desc'>Наименование характеристики</td>
                                <td class='form_edit_input'>
                                    <dxe:ASPxTextBox ID="STCharNameTextBox" runat="server" Text='<%# getValue<string>(Eval("JSON"), "CharName")%>' />
                                </td>
                            </tr>
                            <tr>
                                <td class='form_edit_desc'>Ед.изм.характеристики</td>
                                <td class='form_edit_input'>
                                    <dxe:ASPxTextBox ID="STCharUnitTextBox" runat="server" Text='<%# getValue<string>(Eval("JSON"), "CharUnit")%>' />
                                </td>
                            </tr>
                            <tr>
                                <td class='form_edit_desc'>Значение характеристики</td>
                                <td class='form_edit_input'>
                                    <dxe:ASPxTextBox ID="STCharValueTextBox" runat="server" Text='<%# getValue<string>(Eval("JSON"), "CharValue")%>' />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <%--==============================================================================================================================================--%>
                        <tr>
                            <td colspan="2" class="form_edit_buttons">
                                <span class="ok">
                                     <a onclick="treeUpdateEdit()">Сохранить</a>
                                </span>
                                <span class="cancel">
                                    <a onclick="treeView.CancelEdit();">Отмена</a>
                                </span>
                            </td>
                        </tr>
                    </table>
                </EditForm>
            </Templates>
            <Columns>
                <dxtl:TreeListDataColumn Visible="false" FieldName="JSON"></dxtl:TreeListDataColumn>
                
                <dxtl:TreeListTextColumn Caption="Наименование" FieldName="Name" Width="70%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle" >
                    <DataCellTemplate>
                        <%#GetNameText(Container.DataItem) %>
                        <br />
                        <%#getButtons(Container.NodeKey)%>
                    </DataCellTemplate>
                    <CellStyle CssClass="firstCell"></CellStyle>
                </dxtl:TreeListTextColumn>
                <dxtl:TreeListSpinEditColumn Caption="Год + 0" FieldName="Y0" Name="Y0" Width="90px">
                    <PropertiesSpinEdit MinValue="0" MaxValue="999999999999" AllowNull="false" NullDisplayText="0" NullText="0" DisplayFormatString="#,##0.0000" DisplayFormatInEditMode="true">
                    </PropertiesSpinEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <HeaderCaptionTemplate><%=getHeaderText("Основной объём на {0} год", Controller.Source.Year0, Controller.Source.sumY0, Controller.GetRemain("Y0")) %></HeaderCaptionTemplate>
                </dxtl:TreeListSpinEditColumn>
                <dxtl:TreeListSpinEditColumn Caption="Год + 1" FieldName="Y1" Name="Y1" Width="90px">
                    <PropertiesSpinEdit MinValue="0" MaxValue="999999999999" AllowNull="false" NullDisplayText="0" NullText="0" DisplayFormatString="#,##0.0000" DisplayFormatInEditMode="true">
                    </PropertiesSpinEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <HeaderCaptionTemplate><%=getHeaderText("Основной объём на {0} год", Controller.Source.Year1, Controller.Source.sumY1, Controller.GetRemain("Y1")) %></HeaderCaptionTemplate>
                </dxtl:TreeListSpinEditColumn>
                <dxtl:TreeListSpinEditColumn Caption="Год + 2" FieldName="Y2" Name="Y2" Width="90px">
                    <PropertiesSpinEdit MinValue="0" MaxValue="999999999999" AllowNull="false" NullDisplayText="0" NullText="0" DisplayFormatString="#,##0.0000" DisplayFormatInEditMode="true">
                    </PropertiesSpinEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <HeaderCaptionTemplate><%=getHeaderText("Основной объём на {0} год", Controller.Source.Year2, Controller.Source.sumY2, Controller.GetRemain("Y2")) %></HeaderCaptionTemplate>
                </dxtl:TreeListSpinEditColumn>

                <dxtl:TreeListSpinEditColumn Caption="Год + 0" FieldName="AddY0" Name="AddY0" Width="90px">
                    <PropertiesSpinEdit MinValue="0" MaxValue="999999999999" AllowNull="false" NullDisplayText="0" NullText="0" DisplayFormatString="#,##0.0000" DisplayFormatInEditMode="true">
                    </PropertiesSpinEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <HeaderCaptionTemplate><%=getHeaderText("Дополнительная потребность на {0} год", Controller.Source.Year0, Controller.Source.sumAddY0, Controller.GetRemain("AddY0")) %></HeaderCaptionTemplate>
                </dxtl:TreeListSpinEditColumn>
                <dxtl:TreeListSpinEditColumn Caption="Год + 1" FieldName="AddY1" Name="AddY1" Width="90px">
                    <PropertiesSpinEdit MinValue="0" MaxValue="999999999999" AllowNull="false" NullDisplayText="0" NullText="0" DisplayFormatString="#,##0.0000" DisplayFormatInEditMode="true">
                    </PropertiesSpinEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <HeaderCaptionTemplate><%=getHeaderText("Дополнительная потребность на {0} год", Controller.Source.Year1, Controller.Source.sumAddY1, Controller.GetRemain("AddY1")) %></HeaderCaptionTemplate>
                </dxtl:TreeListSpinEditColumn>
                <dxtl:TreeListSpinEditColumn Caption="Год + 2" FieldName="AddY2" Name="AddY2" Width="90px">
                    <PropertiesSpinEdit MinValue="0" MaxValue="999999999999" AllowNull="false" NullDisplayText="0" NullText="0" DisplayFormatString="#,##0.0000" DisplayFormatInEditMode="true">
                    </PropertiesSpinEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <HeaderCaptionTemplate><%=getHeaderText("Дополнительная потребность на {0} год", Controller.Source.Year2, Controller.Source.sumAddY2, Controller.GetRemain("AddY2")) %></HeaderCaptionTemplate>
                </dxtl:TreeListSpinEditColumn>
                <dxtl:TreeListCommandColumn Caption=" " Width="50px" CellStyle-HorizontalAlign="Right" ButtonType="Image">
                    <EditButton Visible="true">
                        <Image Url="/Content/GB/Styles/Imgs/edit_16.png"></Image>
                    </EditButton>
                    <DeleteButton Visible="true">
                        <Image Url="/Content/GB/Styles/Imgs/delete_16.png"></Image>
                    </DeleteButton>
                    <CellStyle CssClass="lastCell"></CellStyle>
                </dxtl:TreeListCommandColumn>
            </Columns>
        </dxtl:ASPxTreeList>
                </dxp:PanelContent>
            </PanelCollection>
        </dxcp:ASPxCallbackPanel>
        <span class="subText"><span style="color:red">*</span> - остаток расходов разделяемого мероприятия.</span>
        <%--<table style="width:100%">
            <tr>
                <td class="form_edit_buttons" style="text-align:center">
                     <span class="ok"><a onclick="doWork()">Выполнить разъединение</a></span>
                </td>
            </tr>
        </table>--%>
    </form>
</asp:Content>
