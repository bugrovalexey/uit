﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.Common;

    public class ResponsibleDto
    {
        public int Id { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        public ResponsibleJob Job { get; set; }

        /// <summary>
        /// Учреждение
        /// </summary>
        public DepartmentDto Department { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public virtual string Email { get; set; }

        public virtual bool IsActive { get; set; }
    }
}
