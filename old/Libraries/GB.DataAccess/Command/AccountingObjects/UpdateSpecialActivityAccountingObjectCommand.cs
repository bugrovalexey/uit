﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateSpecialActivityAccountingObjectCommand : ICommand<UpdateSpecialActivityAccountingObjectContext>
    {
        ISessionFactoryGb factory;

        public UpdateSpecialActivityAccountingObjectCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(UpdateSpecialActivityAccountingObjectContext context)
        {
            var entity = new Repository<AccountingObject>().GetExisting(context.xId);

            entity.InformAboutActivityGovOrgan = context.InformAboutActivityGovOrgan;

            using (var session = factory.Create())
            {
                session.Update(entity);
                session.Commit();
            }
        }
    }
}
