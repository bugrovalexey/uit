﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Государственная услуга (функция) ГУ/ГФ
    /// </summary>
    public class GovServiceDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование услуги, данное поле заполняется из serviceTargets/serviceTarget
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Наименование услуги из описания услуги
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Уникальный реестровый номер услуги
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Описание результата предоставления услуги
        /// </summary>
        public string Result { get; set; }
    }
}
