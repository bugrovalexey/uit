﻿using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Mapping.Plans
{
    class NPAMap : ClassMapBase<NPA>
    {
        public NPAMap()
            : base("NPA")
        {
            //            Table("v_");

            //            Id(x => x.Id, "NPA_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            string args = @" @NPA_Name = ?
            //							,@NPA_Num = ?
            //							,@NPA_DT = ?
            //							,@NPA_Item = ?
            //							,@NPA_Type_ID = ?
            //                            ,@NPA_Kind_ID = ?
            //                            ,@NPA_ID = ?";

            //            SqlInsert(@"exec ZZZ_Prc_NPA_Insert " + args).Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_NPA_Update " + args).Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_NPA_Delete
            //						  @NPA_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            //Map(x => x.Info, "NPA_Info").LazyLoad().ReadOnly();
            Map(x => x.Name, "NPA_Name");
            Map(x => x.Num, "NPA_Num");
            Map(x => x.Date, "NPA_DT").Nullable();
            Map(x => x.Item, "NPA_Item");

            References<NPAType>(x => x.NPAType, "NPA_Type_ID");

            References<NPAKind>(x => x.NPAKind, "NPA_Kind_ID");
        }
    }
}