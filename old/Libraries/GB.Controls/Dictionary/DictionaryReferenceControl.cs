﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

namespace GB.Controls.Dictionary
{
    public class DictionaryReferenceControl : CompositeControl
    {
        #region ChildsControl
        protected HiddenField _HiddenField_VisibleIndex;
        protected HiddenField _HiddenField_ColumnName;
        protected HtmlGenericControl container;
        protected ASPxGridView Grid;
        protected ASPxButton _Button_Cancel;
        #endregion

        #region Properties
        public string ColumnName
        {
            get
            {
                return _HiddenField_ColumnName.Value;
            }
            set
            {
                _HiddenField_ColumnName.Value = value;
            }
        }

        public int DictionaryID
        {
            get
            {
                if (ViewState["DictionaryID"] != null)
                {
                    return Convert.ToInt32(ViewState["DictionaryID"]);
                }

                return -1;
            }
            set
            {
                ViewState["DictionaryID"] = value;
            }
        }

        public int DictionaryFieldID
        {
            get
            {
                if (ViewState["DictionaryFieldID"] != null)
                {
                    return Convert.ToInt32(ViewState["DictionaryFieldID"]);
                }

                return -1;
            }
            set
            {
                ViewState["DictionaryFieldID"] = value;
            }
        }

        public string DictionaryField
        {
            get
            {
                if (ViewState["DictionaryField"] != null)
                {
                    return ViewState["DictionaryField"].ToString();
                }

                return string.Empty;
            }
            set
            {
                ViewState["DictionaryField"] = value;
            }
        }


        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                base.Visible = value;

                if (base.Visible && DictionaryID != -1)
                {
                    Grid.DataBind();
                }
            }
        }

        MetaDictionary SelectDictionary
        {
            get
            {
                return MetaDictionaries.ListDictionaryItem[DictionaryID];
            }
        } 
        #endregion

        public DictionaryReferenceControl()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            _HiddenField_VisibleIndex = new HiddenField { ID = "_HiddenField_VisibleIndex" };
            _HiddenField_ColumnName = new HiddenField { ID = "_HiddenField_ColumnName" };

            container = new HtmlGenericControl("div");

            Grid = new ASPxGridView { ID = "Grid", ClientInstanceName = "Grid", Width = new Unit("100%") };
            Grid.BeforePerformDataSelect += Grid_BeforePerformDataSelect;
            var cmdColumn = new GridViewCommandColumn { Caption = " ", Name = "cmdColumn", VisibleIndex = 100, Width = new Unit("100px") };
            cmdColumn.CustomButtons.Add(new GridViewCommandColumnCustomButton { ID = "ButtonSelect", Text = "Выбрать", Visibility = GridViewCustomButtonVisibility.AllDataRows });
#if DX14
            cmdColumn.ShowClearFilterButton = true;
#else
            cmdColumn.ClearFilterButton.Visible = true;
#endif
            Grid.Columns.Add(cmdColumn);
            Grid.SettingsPager.PageSize = 25;
            Grid.Settings.ShowFilterRow = true;
            Grid.ClientSideEvents.CustomButtonClick = string.Format(@"function(s, e) {{
                                                            if(e.buttonID == 'ButtonSelect')
                                                            {{
                                                                SelectRow(e.visibleIndex);
                                                            }}
                                                        }}");
            Grid.CustomCallback += Grid_CustomCallback;
            Grid.ClientSideEvents.EndCallback = string.Format(@"function(s, e) {{
                                                                if (s.cpHFId){{
                                                                    $('#'+s.cpHFId).val(s.cpHFValue);
                                                                    $('#'+s.cpTBId).find('input').val(s.cpTBValue);
                                                                    s.cpHFId = '';
                                                                    $('.DivDictionaryReference').hide();
                                                                    s.ClearFilter();
                                                                }}
                                                            }}");

            _Button_Cancel = new ASPxButton { ID = "_Button_Cancel", Text = "Отмена", Width = new Unit("80px") };
            _Button_Cancel.Click += _Button_Cancel_Click;

            Controls.Add(_HiddenField_VisibleIndex);
            Controls.Add(_HiddenField_ColumnName);

            container.Attributes.Add("class", "DivDictionaryReference");
            container.Controls.Add(Grid);

            var table = new HtmlTable();
            var row = new HtmlTableRow();
            var cell = new HtmlTableCell { Width = "100%" };
            row.Cells.Add(cell);
            cell = new HtmlTableCell();
            cell.Controls.Add(_Button_Cancel);
            row.Cells.Add(cell);
            table.Rows.Add(row);

            container.Controls.Add(table);
            Controls.Add(container);
        }

        protected override void CreateChildControls()
        {
        }

        public override void RenderBeginTag(System.Web.UI.HtmlTextWriter writer)
        {
            base.RenderBeginTag(writer);

            writer.Write("<style type=\"text/css\">");
            writer.Write(@".dxgvDataRow_MetropolisBlue
                            {
                                height: 25px;
                            }");
            writer.Write(@".DivDictionaryReference
                            {
                                position: absolute;
	                            width: 80%;
	                            height: auto;
	                            top: 10%;
	                            left: 10%;
	                            background-color: #FFFFE1;
	                            margin: 10 10 10 10;
	                            border-style: solid;
	                            border-width: thin;
                            }");
            writer.Write("</style>");

            writer.Write("<script type=\"text/javascript\">");
            writer.Write(string.Format(@"function SelectRow(visibleIndex)
                                        {{
                                            var hfId = document.getElementById('{0}');
                                            if(hfId)
                                            {{
                                                hfId.value = visibleIndex;
                                                Grid.PerformCallback('Select');
                                            }}
                                        }}", _HiddenField_VisibleIndex.ClientID));
            writer.Write("</script>");
        }

        public void UpdateTableColums()
        {
            DictionaryField = SelectDictionary.ListColumns[DictionaryFieldID].ColumnName;

            DictionaryHelper.CreareGridColumns(SelectDictionary, Grid, true);
        }

        /// <summary>
        /// Прежде чем выполнить выборку данных (для перелистываня странц)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            if (base.Visible && DictionaryID != -1)
            {
                (sender as ASPxGridView).DataSource = DictionaryHelper.GetDataDictionary(SelectDictionary);
            }
        }

        protected void _Button_Cancel_Click(object sender, EventArgs e)
        {
            Visible = false;
        }

        private void Grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if(e.Parameters.Equals("Select", StringComparison.CurrentCultureIgnoreCase))
            {
                this.Visible = false;
                int VisibleIndex = int.Parse(_HiddenField_VisibleIndex.Value);

                object value = Grid.GetRowValues(VisibleIndex, Grid.KeyFieldName, DictionaryField);

                object[] m_value = value as object[];

                if (m_value != null && m_value.Length == 2)
                {
                    Control hfcont = DictionaryHelper.FindControl((Parent as DictionaryEditControl), "HiddenField_" + _HiddenField_ColumnName.Value);
                    Control tbcont = DictionaryHelper.FindControl((Parent as DictionaryEditControl), _HiddenField_ColumnName.Value);

                    Grid.JSProperties["cpHFId"] = hfcont.ClientID;
                    Grid.JSProperties["cpHFValue"] = Convert.ToInt32(m_value[0]);
                    Grid.JSProperties["cpTBId"] = tbcont.ClientID;
                    Grid.JSProperties["cpTBValue"] = m_value[1].ToString();
                }
            }
        }

        public void ClearGrid()
        {
            GridViewColumn ecolumn = Grid.Columns["cmdColumn"];

            Grid.ClearSort();
            Grid.Columns.Clear();

            Grid.Columns.Add(ecolumn);
        }
    }
}
