﻿using System;
using System.Collections.Generic;
using System.Linq;
using GB;
using GB.Extentions;
using GB.Helpers;
using DevExpress.Web.ASPxEditors;
using GB.MKRF.Entities.Projects;
using GB.MKRF.Repository;
using GB.Controls;

namespace GB.MKRF.Web.Projects
{
    public partial class EditCard : System.Web.UI.MasterPage
    {
        public const string ProjectIdKey = "ProjectId";

        private Project _project;
        public Project CurrentProject
        {
            get
            {
                if (_project == null)
                {
                    if (string.IsNullOrWhiteSpace(Request.QueryString[ProjectIdKey]))
                        Response.Redirect("~/Projects/List.aspx");

                    try
                    {
                        var id = Request.QueryString[ProjectIdKey];
                        _project = ProjectRepository.GetOrThrow(id);
                    }
                    catch (Exception exp)
                    {
                        Logger.Fatal("Ошибка получения CurrentProject", exp);
                        ResponseHelper.DenyAccess(this.Response);
                    }
                }
                return _project;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ProjectLabel.DataBind();

            string parameters = ProjectIdKey + "=" + CurrentProject.Id;

            // Настройка ссылок
            foreach (DevExpress.Web.ASPxTabControl.Tab t in tc.GetVisibleTabs())
            {
                t.NavigateUrl += (t.NavigateUrl.Contains("?") ? "&" : "?") + parameters;
            }

            base.OnPreRender(e);
        }

        protected void ProjectLabel_DataBinding(object sender, EventArgs e)
        {
            (sender as ASPxLabel).Text = CurrentProject.ShortName;
        }
    }
}