﻿using System.Collections.Generic;

namespace GB.DataAccess.Services.Args
{
    public class TechnicalSupportArgs : BaseArgsNew, IWorkAndGoodsArgs
    {
        public TechnicalSupportArgs()
        {
            CharacteristicsOfTechnicalSupportIds = new List<int>();
        }

        public int? AccountingObjectId { get; set; }

        public string Name { get; set; }

        public int? CategoryKindOfSupportId { get; set; }

        public int? ManufacturerId { get; set; }

        public string ManufacturerName { get; set; }

        public int Amount { get; set; }

        public decimal Summa { get; set; }

        public List<int> CharacteristicsOfTechnicalSupportIds { get; set; } 
    }
}