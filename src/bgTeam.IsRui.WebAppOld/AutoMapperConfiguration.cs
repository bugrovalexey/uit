﻿using AutoMapper;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.GovContracts;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.ResponsiblePurchase;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.WebApp.Controllers.From;
using bgTeam.IsRui.WebApp.ViewModels.Activities;
using bgTeam.IsRui.WebApp.ViewModels.Activities.Goods;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace bgTeam.IsRui.WebApp
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                IncludeSourceExtensionMethods(cfg, typeof(YearRepository).Assembly);

                cfg.AddGlobalIgnore("success");
                cfg.AddGlobalIgnore("message");
                cfg.AddGlobalIgnore("ReadOnly");
                cfg.AddGlobalIgnore("CurrentActivity");
                cfg.AddGlobalIgnore("Tabs");

                cfg.RecognizeAlias("ExpenditureItem", "KOSGU");

                #region Товары

                cfg.CreateMap<PlansSpec, TableGoods>(MemberList.Destination)
                    .IgnoreAllPropertiesWithAnInaccessibleSetter();
                cfg.CreateMap<PlansSpec, GoodsEditViewModel>(MemberList.Destination)
                    .IgnoreAllPropertiesWithAnInaccessibleSetter()
                    .ForMember(d => d.GoodsId, opt => opt.MapFrom(s => s.Id));

                #endregion

                #region Работы

                cfg.CreateMap<PlansWork, TableWork>(MemberList.Destination)
                    .IgnoreAllPropertiesWithAnInaccessibleSetter();
                cfg.CreateMap<PlansWork, WorkEditViewModel>(MemberList.Destination)
                    .IgnoreAllPropertiesWithAnInaccessibleSetter()
                    .ForMember(d => d.WorkId, opt => opt.MapFrom(s => s.Id));

                #endregion

                #region Гос контракты ОУ

                cfg.CreateMap<GovernmentContractAddFrom, AddGovContractContext>();
                cfg.CreateMap<AddGovContractContext, GovContract>()
                    .ForMember(x => x.IdZakupki360, s => s.MapFrom(y => y.IdZK))
                    .ForMember(x => x.ReasonsCostDeviation, s => s.MapFrom(y => y.RcDeviation));

                #endregion

                cfg.CreateMap<ResponsiblePurchaseFrom, SaveResponsiblePurchaseContext>();

                bgTeam.IsRui.DataAccess.AutoMapperConfiguration.Configure(cfg);
            });
            Mapper.Configuration.CompileMappings();
        }

        private static void IncludeSourceExtensionMethods(IMapperConfigurationExpression cfg, Assembly assembly)
        {
            var extensions = assembly.ExportedTypes
                .Where(type => type.IsSealed && !type.IsGenericType && !type.IsNested)
                .Where(type => type.GetMethods().Any(mi => mi.IsStatic && mi.IsDefined(typeof(ExtensionAttribute), false) && mi.GetParameters().Length == 1));
            foreach (var item in extensions)
                cfg.IncludeSourceExtensionMethods(item);
        }
    }
}