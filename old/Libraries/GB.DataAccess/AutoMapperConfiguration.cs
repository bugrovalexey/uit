﻿using AutoMapper;
using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.Documents;
using GB.Data.AccountingObjects.WorksAndGoods;
using GB.Data.Common;
using GB.Data.Directories;
using GB.Data.Extensions;
using GB.Data.Plans;
using GB.DataAccess.Command.AccountingObjects;
using GB.DataAccess.Command.AccountingObjects.Activities;
using GB.DataAccess.Command.AccountingObjects.UserComments;
using GB.DataAccess.Command.AccountingObjects.Decommissioning;
using GB.DataAccess.Command.AccountingObjects.Purchase;
using GB.DataAccess.Command.AccountingObjects.ResponsibleExploitation;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;
using GB.DataAccess.Services.Args;
using GB.DataAccess.Services.Args.Documents;
using GB.DataAccess.Command.AccountingObjects.CharacteristicOfTechnicalSupports;
using GB.DataAccess.Command.AccountingObjects.TechnicalSupports;
using GB.DataAccess.Command.AccountingObjects.Softwares;
using GB.DataAccess.Command.AccountingObjects.WorkAndServices;
using GB.DataAccess.Command.AccountingObjects.WorkAndGood;
using GB.DataAccess.Command.AccountingObjects.InformationInteractions;
using GB.DataAccess.Command.AccountingObjects.ActsOfAcceptance;
using GB.DataAccess.Command.AccountingObjects.AODecisionsToCreate;
using GB.DataAccess.Command.AccountingObjects.SpecialChecks;
using GB.DataAccess.Command.AccountingObjects.AOGovServices;
using GB.Data.Frgu;
using GB.DataAccess.Command.AccountingObjects.AOGovServiceNPAs;
using GB.DataAccess.Command.AccountingObjects.AccountingObjectsCharacteristics;
using GB.DataAccess.Command.AccountingObjects.CharacteristicOfLicenses;
using GB.DataAccess.Command.AccountingObjects.InternalIsAndComponentItkis;
using GB.DataAccess.Command.AccountingObjects.ExternalIsAndComponentItkis;
using GB.DataAccess.Command.AccountingObjects.ExternalUserInterfaceOfIss;


namespace GB.DataAccess
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            #region Товары

            Mapper.CreateMap<PlansSpecArgs, PlansSpec>(MemberList.Source)
                .ForSourceMember(s => s.ActivityId, opt => opt.Ignore())
                .ForMember(d => d.OKPD, opt => opt.ResolveUsing(a => GetEntity<OKPD>(a.OKPD)))
                .ForMember(d => d.ExpenseDirection, opt => opt.ResolveUsing(a => GetEntity<ExpenseDirection>(a.ExpenseDirection)))
                .ForMember(d => d.ExpenseType, opt => opt.ResolveUsing(a => GetEntity<ExpenseType>(a.ExpenseType)))
                .ForMember(d => d.ExpenditureItem, opt => opt.ResolveUsing(a => GetEntity<ExpenditureItem>(a.ExpenditureItem)))
                .ForMember(d => d.ProductGroup, opt => opt.ResolveUsing(a => GetEntity<ProductGroup>(a.ProductGroup)));

            #endregion

            #region Работы

            Mapper.CreateMap<PlansWorkArgs, PlansWork>(MemberList.Source)
                .ForSourceMember(s => s.Activity, opt => opt.Ignore())
                .ForSourceMember(s => s.ActivityId, opt => opt.Ignore())
                .ForMember(d => d.OKPD, opt => opt.ResolveUsing(a => GetEntity<OKPD>(a.OKPD)))
                .ForMember(d => d.ExpenseDirection, opt => opt.ResolveUsing(a => GetEntity<ExpenseDirection>(a.ExpenseDirection)))
                .ForMember(d => d.ExpenseType, opt => opt.ResolveUsing(a => GetEntity<ExpenseType>(a.ExpenseType)))
                .ForMember(d => d.ExpenditureItem, opt => opt.ResolveUsing(a => GetEntity<ExpenditureItem>(a.ExpenditureItem)))
                .ForMember(d => d.OKVED, opt => opt.ResolveUsing(a => GetEntity<OKVED>(a.OKVED)));

            #endregion

            #region  Спецдеятельность

            Mapper.CreateMap<AOGovServiceArgs, AOGovService>(MemberList.Source)
                .ForMember(d => d.AccountingObject,
                    opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)));


            Mapper.CreateMap<AOGovServiceNPAArgs, AOGovServiceNPA>(MemberList.Source)
                .ForMember(d => d.GovService, opt => opt.ResolveUsing(a => GetEntity<AOGovService>(a.AOGovServiceId)));


            #endregion  Спецдеятельность

            #region Виды обеспечения

            Mapper.CreateMap<SoftwareArgs, CharacteristicOfLicenseArgs>(MemberList.Source)
               .ForMember(x => x.Id, opt => opt.MapFrom(a => a.IdCharacteristicOfLicense));

            Mapper.CreateMap<CharacteristicOfLicenseArgs, CharacteristicOfLicense>(MemberList.Source)
                .ForMember(x => x.Name, opt => opt.MapFrom(a => a.NameCharacteristicOfLicense));

            Mapper.CreateMap<SoftwareArgs, WorkAndGoodsArgs>()
                .ForMember(d => d.ObjectId, opt => opt.MapFrom(a => a.Id))
                .ForMember(d => d.Type, opt => opt.MapFrom(a => Software.Type))
                .ForMember(d => d.Id, opt => opt.Ignore());

            Mapper.CreateMap<TechnicalSupportArgs, WorkAndGoodsArgs>()
                .ForMember(d => d.ObjectId, opt => opt.MapFrom(a => a.Id))
                .ForMember(d => d.Type, opt => opt.MapFrom(a => TechnicalSupport.Type))
                .ForMember(d => d.Id, opt => opt.Ignore());

            Mapper.CreateMap<WorkAndServiceArgs, WorkAndGoodsArgs>()
                .ForMember(d => d.ObjectId, opt => opt.MapFrom(a => a.Id))
                .ForMember(d => d.Type, opt => opt.MapFrom(a => WorkAndService.Type))
                .ForMember(d => d.Id, opt => opt.Ignore());

            Mapper.CreateMap<WorkAndGoodsArgs, WorkAndGoods>()
                .ForMember(d => d.Id, opt => opt.Ignore())
                .ForMember(d => d.AccountingObject,
                    opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                    .ForMember(d => d.CategoryKindOfSupport,
                    opt =>
                        opt.ResolveUsing(
                            a => GetEntity<CategoryKindOfSupport>(a.CategoryKindOfSupportId)));

            #endregion Виды обеспечения

            #region Работы и услуги ОУ

            Mapper.CreateMap<WorkAndServiceArgs, WorkAndService>(MemberList.Source)
                .BeforeMap((src, d) =>
                {
                    // 17 - Работы (услуги) по аренде ТО и ПО, включая аренду ресурсов на основе «облачных вычислений».
                    if (src.CategoryKindOfSupportId != 17)
                    {
                        src.InformationLeasedInfrastructure = null;
                    }
                })
                .ForMember(d => d.AccountingObject,
                    opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.CategoryKindOfSupport,
                    opt =>
                        opt.ResolveUsing(
                            a => GetEntity<CategoryKindOfSupport>(a.CategoryKindOfSupportId)))
                .ForMember(d => d.Okved, opt => opt.ResolveUsing(a => GetEntity<OKVED>(a.OkvedId)));

            #endregion Работы и услуги ОУ

            #region  Информационное взаимодействие

            Mapper.CreateMap<InternalIsAndComponentItkiArgs, InternalIsAndComponentsItki>(MemberList.Source)
                .ForMember(d => d.Owner, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.Object,
                    opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.ObjectId)));

            Mapper.CreateMap<ExternalIsAndComponentItkiArgs, ExternalIsAndComponentsItki>(MemberList.Source)
                .ForMember(d => d.Owner, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)));

            Mapper.CreateMap<ExternalUserInterfaceOfIsArgs, ExternalUserInterfaceOfIs>(MemberList.Source)
                .ForMember(d => d.Owner, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.TypeOfInterface,
                    opt => opt.ResolveUsing(a => GetEntity<TypeOfInterface>(a.TypeId)));

            #endregion  Информационное взаимодействие

            #region  Сведения о принятии объекта учета к бюджетному учету

            Mapper.CreateMap<AOAdoptionBudgetArgs, AccountingObject>()
                .ForMember(d => d.Type, opt => opt.Ignore())
                .ForMember(d => d.BK_GRBS,
                    opt => opt.ResolveUsing(a => GetEntity<GRBS>(a.BK_GRBS)))
                .ForMember(d => d.BK_PZRZ,
                    opt => opt.ResolveUsing(a => GetEntity<SectionKBK>(a.BK_PZRZ)))
                .ForMember(d => d.BK_CSR,
                    opt => opt.ResolveUsing(a => GetEntity<ExpenseItem>(a.BK_CSR)))
                .ForMember(d => d.BK_WR,
                    opt => opt.ResolveUsing(a => GetEntity<WorkForm>(a.BK_WR)));

            Mapper.CreateMap<AddOrSaveAOAdoptionBudgetContext, AccountingObject>()
                .ForMember(d => d.Type, opt => opt.Ignore())
                .ForMember(d => d.BK_GRBS,
                    opt => opt.ResolveUsing(a => GetEntity<GRBS>(a.BK_GRBS)))
                .ForMember(d => d.BK_PZRZ,
                    opt => opt.ResolveUsing(a => GetEntity<SectionKBK>(a.BK_PZRZ)))
                .ForMember(d => d.BK_CSR,
                    opt => opt.ResolveUsing(a => GetEntity<ExpenseItem>(a.BK_CSR)))
                .ForMember(d => d.BK_WR,
                    opt => opt.ResolveUsing(a => GetEntity<WorkForm>(a.BK_WR)));

            #endregion  Сведения о принятии объекта учета к бюджетному учету

            #region Документы ОУ

            Mapper.CreateMap<AccountingObjectCommonDataArgs, AODocumentCommissioningArgs>()
                .ForMember(d => d.AccountingObjectId, opt => opt.MapFrom(a => a.Id))
                .ForMember(d => d.Name, opt => opt.MapFrom(a => a.CommissioningDocName))
                .ForMember(d => d.Number, opt => opt.MapFrom(a => a.CommissioningDocNumber))
                .ForMember(d => d.DateAdoption, opt => opt.MapFrom(a => a.CommissioningDocDateAdoption))
                .ForMember(d => d.Paragraph, opt => opt.MapFrom(a => a.CommissioningDocParagraph))
                .ForMember(d => d.Id, opt => opt.Ignore());

            Mapper.CreateMap<AODocumentCommissioningArgs, AODocumentCommissioning>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.Type, opt => opt.Ignore())
                .ForMember(d => d.Id, opt => opt.Ignore());

            Mapper.CreateMap<AccountingObjectCommonDataArgs, AODocumentDecommissioningArgs>()
                .ForMember(d => d.AccountingObjectId, opt => opt.MapFrom(a => a.Id))
                .ForMember(d => d.Name, opt => opt.MapFrom(a => a.DecommissioningDocName))
                .ForMember(d => d.Number, opt => opt.MapFrom(a => a.DecommissioningDocNumber))
                .ForMember(d => d.DateAdoption, opt => opt.MapFrom(a => a.DecommissioningDocDateAdoption))
                .ForMember(d => d.Id, opt => opt.Ignore());

            Mapper.CreateMap<AODocumentDecommissioningArgs, AODocumentDecommissioning>()
                .ForMember(d => d.AccountingObject,
                    opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.Type, opt => opt.Ignore())
                .ForMember(d => d.Id, opt => opt.Ignore());

            Mapper.CreateMap<SaveDecommissioningContext, AODocumentDecommissioning>()
                .ForMember(d => d.Type, opt => opt.Ignore());

            Mapper.CreateMap<AOAdoptionBudgetArgs, AODocumentGroundsForCreationArgs>()
                .ForMember(d => d.AccountingObjectId, opt => opt.MapFrom(a => a.Id))
                .ForMember(d => d.Name, opt => opt.MapFrom(a => a.DocName))
                .ForMember(d => d.Number, opt => opt.MapFrom(a => a.DocNumber))
                .ForMember(d => d.DateAdoption, opt => opt.MapFrom(a => a.DocDateAdoption))
                .ForMember(d => d.Paragraph, opt => opt.MapFrom(a => a.DocParagraph))
                .ForMember(d => d.Id, opt => opt.Ignore());

            Mapper.CreateMap<AODocumentGroundsForCreationArgs, AODocumentGroundsForCreation>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.Type, opt => opt.Ignore())
                .ForMember(d => d.Id, opt => opt.Ignore());

            #endregion Документы ОУ

            #region Фактическое месторасположение объекта учета

            Mapper.CreateMap<AOFactualLocationArgs, AccountingObject>()
                .ForMember(d => d.Type, opt => opt.Ignore());

            #endregion Фактическое месторасположение объекта учета

            #region Акт сдачи-приемки

            Mapper.CreateMap<ActOfAcceptanceArgs, ActOfAcceptance>()
                .ForMember(d => d.GovContract,
                    opt => opt.ResolveUsing(a => GetEntity<GovContract>(a.GovContractId)));


            #endregion Акт сдачи-приемки

            #region Эксплуатация объекта учета.

            Mapper.CreateMap<SaveResponsibleExploitationContext, AOExploitationDepartment>()
                .ForMember(d => d.Department,
                    opt => opt.ResolveUsing(a => GetEntity<Department>(a.DepartmentId)));


            #endregion Эксплуатация объекта учета.

            #region ОУ

            Mapper.CreateMap<AddOrSaveAccountingObjectContext, AccountingObject>()
                .ForMember(d => d.Id, opt => opt.Ignore())
                .ForMember(d => d.Type, opt => opt.ResolveUsing(a => GetEntity<IKTComponent>(a.TypeId)))
                .ForMember(d => d.Kind, opt => opt.ResolveUsing(a => GetEntity<IKTComponent>(a.KindId)));

            #endregion ОУ

            Mapper.CreateMap<AddActivityContext, AoActivity>();
                //.ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)));
                //.ForMember(d => d.ActivityType, opt => opt.ResolveUsing(a => a.StageEnum.ToActivityTypeEnum()));

            Mapper.CreateMap<AddPurchaseAccountingObjectContext, GovPurchase>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.IdAO)))
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.IdAO)));
                //.ForMember(d => d.ActivityType, opt => opt.ResolveUsing(a => a.StageEnum.ToActivityTypeEnum()));

            Mapper.CreateMap<AddUserCommentContext, AccountingObjectUserComment>()
                .ForMember(d => d.AccountingObject,
                    opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)));

            Mapper.CreateMap<AddOrSaveCharacteristicOfTechnicalSupportContext, CharacteristicOfTechnicalSupport>()
                .ForMember(d => d.TechnicalSupport, opt => opt.ResolveUsing(a => GetEntity<TechnicalSupport>(a.TechnicalSupportId)))
                .ForMember(d => d.Unit, opt => opt.ResolveUsing(a => GetEntity<Units>(a.UnitId)));

            Mapper.CreateMap<AddOrSaveTechnicalSupportContext, TechnicalSupport>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.CategoryKindOfSupport, opt => opt.ResolveUsing(a => GetEntity<CategoryKindOfSupport>(a.CategoryKindOfSupportId)))
                .ForMember(d => d.Manufacturer, opt => opt.ResolveUsing(a => GetEntity<Manufacturer>(a.ManufacturerId)));

            Mapper.CreateMap<AddOrSaveSoftwareContext, Software>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.CategoryKindOfSupport, opt => opt.ResolveUsing(a => GetEntity<CategoryKindOfSupport>(a.CategoryKindOfSupportId)))
                .ForMember(d => d.Manufacturer, opt => opt.ResolveUsing(a => GetEntity<Manufacturer>(a.ManufacturerId)))
                .ForMember(d => d.CharacteristicsOfLicense, opt => opt.ResolveUsing(a => GetEntity<CharacteristicOfLicense>(a.IdCharacteristicOfLicense)));

            Mapper.CreateMap<AddOrSaveWorkAndServiceContext, WorkAndService>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.CategoryKindOfSupport, opt => opt.ResolveUsing(a => GetEntity<CategoryKindOfSupport>(a.CategoryKindOfSupportId)))
                .ForMember(d => d.Okved, opt => opt.ResolveUsing(a => GetEntity<OKVED>(a.OkvedId)));

            Mapper.CreateMap<AddOrSaveWorkAndGoodsContext, WorkAndGoods>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.CategoryKindOfSupport, opt => opt.ResolveUsing(a => GetEntity<CategoryKindOfSupport>(a.CategoryKindOfSupportId)));

            Mapper.CreateMap<AddOrSaveInformationInteractionContext, InformationInteraction>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.Type, opt => opt.ResolveUsing(a => GetEntity<InformationInteractionType>(a.TypeId)));

            Mapper.CreateMap<AddOrSaveActOfAcceptanceContext, ActOfAcceptance>()
                .ForMember(d => d.GovContract, opt => opt.ResolveUsing(a => GetEntity<GovContract>(a.GovContractId)));

            Mapper.CreateMap<AddOrSaveAODecisionToCreateContext, AODecisionToCreate>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)));

            Mapper.CreateMap<AddOrSaveSpecialCheckContext, SpecialCheck>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)));

            Mapper.CreateMap<AddOrSaveAOGovServiceContext, AOGovService>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.GovService, opt => opt.ResolveUsing(a => GetEntity<GovService>(a.GovServiceId)));

            Mapper.CreateMap<AddOrSaveAOGovServiceNPAContext, AOGovServiceNPA>()
                .ForMember(d => d.GovService, opt => opt.ResolveUsing(a => GetEntity<AOGovService>(a.AOGovServiceId)));

            Mapper.CreateMap<AddOrSaveAccountingObjectCharacteristicsContext, AccountingObjectCharacteristics>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.Type, opt => opt.ResolveUsing(a => GetEntity<AccountingObjectCharacteristicsType>(a.TypeId)))
                .ForMember(d => d.Unit, opt => opt.ResolveUsing(a => GetEntity<Units>(a.UnitId)));

            Mapper.CreateMap<AddOrSaveCharacteristicOfLicenseContext, CharacteristicOfLicense>()
                .ForMember(d => d.Software, opt => opt.ResolveUsing(a => GetEntity<Software>(a.SoftwareId)));

            Mapper.CreateMap<AddOrUpdateInternalIsAndComponentItkiContext, InternalIsAndComponentsItki>()
                .ForMember(d => d.Owner, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.Object, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.ObjectId)));

            Mapper.CreateMap<AddOrSaveExternalIsAndComponentItkiContext, ExternalIsAndComponentsItki>()
                .ForMember(d => d.Owner, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)));

            Mapper.CreateMap<AddOrSaveExternalUserInterfaceOfIsContext, ExternalUserInterfaceOfIs>()
                .ForMember(d => d.Owner, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.TypeOfInterface, opt => opt.ResolveUsing(a => GetEntity<TypeOfInterface>(a.TypeId)));

            Mapper.CreateMap<AddOrSaveAODocumentBaseContext, AODocumentCommissioning>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.Type, opt => opt.Ignore());

            Mapper.CreateMap<AddOrSaveAOAdoptionBudgetContext, AODocumentGroundsForCreation>()
                .ForMember(d => d.AccountingObject, opt => opt.ResolveUsing(a => GetEntity<AccountingObject>(a.AccountingObjectId)))
                .ForMember(d => d.Type, opt => opt.Ignore());


            //, 

            Mapper.CreateMap<EntityDirectoriesArgs, Manufacturer>();

            #region Мероприятия

            Mapper.CreateMap<IPlansActivityArgs, PlansActivity>()
                .ForMember(d => d.Id, opt => opt.Ignore())
                .ForMember(d => d.AccountingObject, opt => opt.Ignore())
                .ForMember(d => d.Logo, opt => opt.Ignore())
                .ForMember(d => d.Documents, opt => opt.Ignore())
                .ForMember(d => d.Conception, opt => opt.Ignore())
                .ForMember(d => d.ResponsibleInformation,
                    opt => opt.ResolveUsing(a => GetEntity<User>(a.ResponsibleInformation)))
                .ForMember(d => d.ResponsibleTechnical,
                    opt => opt.ResolveUsing(a => GetEntity<User>(a.ResponsibleTechnical)))
                .ForMember(d => d.ResponsibleFinancial,
                    opt => opt.ResolveUsing(a => GetEntity<User>(a.ResponsibleFinancial)))
                .ForMember(d => d.Signing,
                    opt => opt.ResolveUsing(a => GetEntity<User>(a.Signing)));




            #endregion Мероприятия

        }

        static T GetEntity<T>(int? id) where T : EntityBase
        {
            return new Repository<T>().Get(id);
        }
    }
}
