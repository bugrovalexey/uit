﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.Workflow
{
    public class CreateMultiCommand : ICommand<CreateMultiContext>
    {
        SessionFactory factory;

        public CreateMultiCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(CreateMultiContext context)
        {
            var ao_multi = new AccountingObject(AccountingObjectEnum.Multi);

            ao_multi.CreateDate = DateTime.Now;
            ao_multi.Status = new StatusRepository().GetDefault(ao_multi);
            ao_multi.Department = UserRepository.GetCurrent().Department;

            ao_multi.ShortName = context.AO.ShortName;
            ao_multi.FullName = context.AO.FullName;
            ao_multi.Stage = context.AO.Stage;
            ao_multi.Type = context.AO.Type;
            ao_multi.Kind = context.AO.Type;

            var session = factory.CreateInternal();

            session.Insert(ao_multi);
            
            context.AO.Parent = ao_multi;
            session.Update(ao_multi);
        }
    }
}
