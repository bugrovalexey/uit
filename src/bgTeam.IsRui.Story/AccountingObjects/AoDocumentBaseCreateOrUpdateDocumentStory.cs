﻿

namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain._Mapping;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Story.Common;
    using NHibernate.Util;
    using System;
    using System.Linq;
    using System.Threading.Tasks;


    public class AoDocumentBaseCreateOrUpdateDocumentStory : IStory<AoDocumentBaseCreateOrUpdateDocumentStoryContext, CreateFileAnswerDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IFileProviderRui _fileProvider;

        public AoDocumentBaseCreateOrUpdateDocumentStory(IRepositoryRui repository, ISessionNHibernateFactory factory, IMapperBase mapperBase, IFileProviderRui fileProvider)
        {
            _repository = repository;
            _factory = factory;
            _mapper = mapperBase;
            _fileProvider = fileProvider;
        }

        public CreateFileAnswerDto Execute(AoDocumentBaseCreateOrUpdateDocumentStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<CreateFileAnswerDto> ExecuteAsync(AoDocumentBaseCreateOrUpdateDocumentStoryContext context)
        {
            AODecisionToCreate aoDecision;

            Files result;

            using (var session = _factory.OpenSession())
            {
                //var fileAdd = new FileAddStory(_mapper, _repository, _fileProvider, _factory);

                aoDecision = new AODecisionToCreate();

                _mapper.Map(context, aoDecision);

                if (context.Id.HasValue)
                {
                    await session.UpdateAsync(aoDecision);

                    if (context.file.TempGuid != null && context.file.Id == null)
                    {
                        var entitydocForDel = _repository.Get<AODecisionToCreate>(context.Id);

                        var docForDel = _repository.Get<Files>(q => q.Where(x => x.EntityType == 4 && x.EntityOwner == entitydocForDel));

                        if (docForDel != null)
                        {
                            new FileDeleteStory(_repository, _fileProvider, _factory)
                            .Execute(new FileDeleteStoryContext
                            {
                                Id = docForDel.Id,
                            });
                        }

                        var addfilecontext = _mapper.Map(context.file, new FileAddStoryContext());

                        addfilecontext.Type = DocTypeEnum.AccountingObjectCommentDocument;
                        addfilecontext.Entity = aoDecision;

                        result = await new FileAddStory(_mapper, _repository, _fileProvider, _factory)
                            .ExecuteAsync(addfilecontext);

                        //result = await fileAdd.ExecuteAsync(new FileAddStoryContext()
                        //{
                        //    Type = DocTypeEnum.AccountingObjectCommentDocument,
                        //    Entity = aoDecision,
                        //    Name = context.file.Name,
                        //    TempGuid = context.file.TempGuid,
                        //});

                        context.file.Id = result.Id;
                        context.file.FileName = result.Name;
                    }
                }
                else
                {
                    if ((context.file.TempGuid != null && context.file.Id == null) || context.file.Url != null)
                    {
                        await session.InsertAsync(aoDecision);

                        //result = await fileAdd.ExecuteAsync(new FileAddStoryContext()
                        //{
                        //    Type = DocTypeEnum.AccountingObjectCommentDocument,
                        //    Entity = aoDecision,
                        //    Name = context.file.Name,
                        //    TempGuid = context.file.TempGuid,
                        //});

                        var addfilecontext = _mapper.Map(context.file, new FileAddStoryContext());

                        addfilecontext.Type = DocTypeEnum.AccountingObjectCommentDocument;
                        addfilecontext.Entity = aoDecision;

                        result = await new FileAddStory(_mapper, _repository, _fileProvider, _factory)
                            .ExecuteAsync(addfilecontext);

                        context.file.Id = result.Id;
                        context.file.FileName = result.Name;
                    }
                }

                context.Id = aoDecision.Id;
            }

            return _mapper.Map(context, new CreateFileAnswerDto());

        }
    }
}
