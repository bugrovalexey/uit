﻿namespace GB.MKRF.Logic.Tabs.Handler
{
    internal class AccountingObjectsHandler : TabsBaseHandler
    {
        public AccountingObjectsHandler()
            : base("AccountingObjects")
        {
            Add("Мои объекты учета", "List");
            Add("На согласовании", "ListApprove");
            //Add("Согласованные", "ListMyApprove");
        }
    }
}