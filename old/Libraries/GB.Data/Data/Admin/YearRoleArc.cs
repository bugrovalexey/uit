﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Admin
{
	/// <summary>Архивный год для роли</summary>
	public class YearRoleArc : EntityBase
	{
		/// <summary>Роль</summary>
		public virtual Role Role { get; set; }

		/// <summary>Таблица системы</summary>
		public virtual PortalTable PortalTable { get; set; }

		/// <summary>Год</summary>
		public virtual int Year { get; set; }
	}
}