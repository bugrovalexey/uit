﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ExpDocEquivalentMap : ClassMapBase<ExpDocEquivalent>
	{
		public ExpDocEquivalentMap()
            : base("Exp_Doc_Equivalent")
		{
//            Table("w_");

//            Id(x => x.Id, "Exp_Doc_Equivalent_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            SqlInsert(@"exec ZZZ_Prc_Exp_Doc_Equivalent_Insert
//						  @Exp_Doc_Equivalent_ID = ?
//						 ,@Exp_Doc_Equivalent_Name = ?").Check.None();


//            SqlUpdate(@"exec ZZZ_Prc_Exp_Doc_Equivalent_Update
//						  @Exp_Doc_Equivalent_ID = ?
//						 ,@Exp_Doc_Equivalent_Name = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Doc_Equivalent_Delete
//						  @Exp_Doc_Equivalent_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Exp_Doc_Equivalent_Name").Nullable();
        }
    }
}
