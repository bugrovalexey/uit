﻿using bgTeam.IsRui.Domain.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.WebApp.Admin.Models
{
    public class UserViewModel
    {
        private string _password;

        public UserViewModel()
        {

        }

        public UserViewModel(UsersDto userDto)
        {
            Id = userDto.Id.Value;
            Education = userDto.Education;
            Email = userDto.Email;
            Fax = userDto.Fax;
            GovtOrganId = userDto.GovtOrganId;
            IsActive = userDto.IsActive;
            IsESIA = userDto.IsESIA;
            IsResponsible = userDto.IsResponsible;
            Job = userDto.Job;
            Login = userDto.Login;
            Middlename = userDto.Middlename;
            MobPhone = userDto.MobPhone;
            Name = userDto.Name;
            ReceiveMail = userDto.ReceiveMail;
            Snils = userDto.Snils;
            Surname = userDto.Surname;
            Token = userDto.Token;
            Work = userDto.Work;
            WorkPhone = userDto.WorkPhone;
        }

        public int? Id { get; set; }

        public string Login { get; set; }

        public string Password
        {
            get
            {
                if (Id > 0)
                {
                    return null;
                }
                else
                {
                    return _password;
                }
            }
            set
            {
                _password = value;
            }
        }

        public bool IsActive { get; set; }

        public string Surname { get; set; }

        public string Name { get; set; }

        public string Middlename { get; set; }

        public string Job { get; set; }

        public string WorkPhone { get; set; }

        public string MobPhone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string Work { get; set; }

        public string Education { get; set; }

        public bool ReceiveMail { get; set; }

        public bool IsResponsible { get; set; }

        public string Snils { get; set; }

        public bool IsESIA { get; set; }

        public string Token { get; set; }

        public int? GovtOrganId { get; set; }
    }
}
