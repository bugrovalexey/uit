﻿using AutoMapper;
using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.Directories;
using GB.DataAccess.Repository;
using System;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class AddOrSaveAccountingObjectCommand : ICommand<AddOrSaveAccountingObjectContext, AccountingObject>
    {
        ISessionFactoryGb factory;

        public AddOrSaveAccountingObjectCommand()
        {
            factory = new SessionFactory();
        }

        public AccountingObject Execute(AddOrSaveAccountingObjectContext context)
        {
            var IsNew = context.Id == 0;

            AccountingObject entity;

            if(IsNew)
            {
                entity = new AccountingObject();

                //Проставляем значения по умолчанию для Not Nullable полей
                entity.Type = new Repository<IKTComponent>().Get(context.TypeId);
                entity.Kind = new Repository<IKTComponent>().Get(context.KindId);
                entity.Status = new StatusRepository().GetDefault(entity);
                entity.Stage = AccountingObjectsStageEnum.Planned; // задаем стадию при создании
                entity.Department = UserRepository.GetCurrent().Department;
            }
            else
            {
                entity = new Repository<AccountingObject>().GetExisting(context.Id);
            }

            Mapper.Map(context, entity);

            entity.CreateDate = DateTime.Now;

            using (var session = factory.Create())
            {
                session.SaveOrUpdate(entity);
                session.Commit();
            }

            return entity;
        }
    }
}
