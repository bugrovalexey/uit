﻿namespace bgTeam.IsRui.Common.Extentions
{
    public static class BoolExtensions
    {
        public static string ToRussian(this bool val)
        {
            if (val)
            {
                return "Да";
            }

            return "Нет";
        }
    }
}
