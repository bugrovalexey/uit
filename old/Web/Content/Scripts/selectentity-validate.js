﻿function IsSelectEntityValid(name) {
    var hfName = 'hf_' + name;
    var span = $('span[data-valmsg-for=' + hfName + ']', $('#se_' + name).parent());
    if ($('#' + hfName).val() == '') {
        span.removeClass('field-validation-valid');
        span.addClass('field-validation-error');
        span.css('margin-top', '0px');
        span.html('Укажите<br/>значение');

        if (!$('#' + hfName).data("events")) {
            $('#' + hfName).change(function () {
                IsSelectEntityValid(name);
            });
        }

        return false;
    } else {
        span.html('');
        span.removeClass('field-validation-error');
        span.addClass('field-validation-valid');
        return true;
    }
}