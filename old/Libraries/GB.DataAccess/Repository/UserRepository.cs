﻿using System.Runtime.Remoting.Messaging;
using System.Threading;
using GB.Data.Common;

namespace GB.DataAccess.Repository
{
    public class UserRepository : Repository<User>
    {
        private const string KEY_CURRENT_USER = "CurrentUser";

        private static string CurrentUserLogin
        {
            get
            {
                return (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null)
                    ? Thread.CurrentPrincipal.Identity.Name : null;
            }
        }

        private static User GetByLoginEx(string login)
        {
            var user = new Repository<User>().Get(q => q
                .Fetch(x => x.Department).Eager
                .Fetch(x => x.Roles).Eager //в Get так можно, чтобы одним запросов всё считать
                .Where(x => x.Login == login));
            
            return user;
        }
        
        /// <summary>
        /// Возвращает текущего пользователя
        /// </summary>
        public static User GetCurrent()
        {
            var user = (User)CallContext.GetData(KEY_CURRENT_USER);

            if (user == null)
            {
                user = GetByLoginEx(CurrentUserLogin);
                CallContext.SetData(KEY_CURRENT_USER, user);
            }

            return user;
        }

        
        //public static User GetCurrent()
        //{
        //    return GetCurrentEx();
        //}

        public static int CurrentUserId
        {
            get
            {
                var user = GetCurrent();
                return user != null ? user.Id : -1;
            }
        }

        public static bool IsAuthorized
        {
            get { return GetCurrent() != null; }
        }
    }
}