﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.Extensions;
using bgTeam.IsRui.Domain;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class TableSpecCheck : NotificationBaseViewModel
    {
         #region Consts

        public const string NameCaption = "Наименование";
        public const string FilesDataCaption = "Документ";

        #endregion Consts

        #region Ctors

        public TableSpecCheck(SpecialCheck entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            FilesData = entity.Documents.Return(docs => docs.Select(x => new SelectEntityInfo() { Id = x.Id, Name = x.Name }), default(IEnumerable<SelectEntityInfo>));

        }

        #endregion Ctors


        #region Props

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        private IEnumerable<IEntityName> FilesData { get; set; }

        [DisplayName(FilesDataCaption)]
        public string FileLink
        {
            get
            {
                var links = FilesData.Select(LinkBuilder.Link);
                return string.Join("<br />", links);
            }
        }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteSpecCheck", "AccountingObjects", new { id = Id }, "GridSpecChecks");
            }
        }

        #endregion Props

    }
}