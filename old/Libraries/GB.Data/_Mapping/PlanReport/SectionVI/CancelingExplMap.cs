﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.PlanReport.SectionVI;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVI
{
    public class CancelingExplMap : ClassMapBase<CancelingExpl>
	{
        public CancelingExplMap()
            : base("PRS_6g")
		{
//            Table("w_");

//            Id(x => x.Id, "PRS_6g_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string args = @" @PRS_6g_Num = ? 
//							,@PRS_6g_IS = ? 
//							,@PRS_6g_Reason = ? 
//							,@Plans_ID = ? 
//							,@IKT_Component_ID = ? 
//							,@PRS_6g_ID = ? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_6g_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_6g_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_6g_Delete
//						  @PRS_6g_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_6g_Num");
			Map(x => x.IS, "PRS_6g_IS");
			Map(x => x.Reasons, "PRS_6g_Reason");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
			References<IKTComponent>(x => x.IKTComponent, "IKT_Component_ID");
        }
    }
}