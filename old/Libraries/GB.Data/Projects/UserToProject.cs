﻿using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Entities.Projects
{
    /// <summary>Связь пользователя с проектом</summary>
    public class UserToProject : EntityBase
    {
        /// <summary>Пользователь</summary>
        public virtual User User { get; set; }

        /// <summary>Проект</summary>
        public virtual Project Project { get; set; }
    }
}