﻿using Uviri.MKRF.Entities.Expertise;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpDemandCheckMap : ClassMapBase<ExpDemandCheck>
    {
        public ExpDemandCheckMap()
            : base("Exp_Demand_Check")
        {
//            Table("w_");

//            Id(x => x.Id, "Exp_Demand_Check_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@Exp_Demand_Check_Name = ? 
//						  ,@Exp_Demand_ID = ? 
//						  ,@Exp_Demand_Check_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Exp_Demand_Check_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Demand_Check_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Exp_Demand_Check_Delete @Exp_Demand_Check_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Exp_Demand_Check_Name");

            References<ExpDemand>(x => x.Demand, "Exp_Demand_ID").Not.Nullable();
        }
    }
}
