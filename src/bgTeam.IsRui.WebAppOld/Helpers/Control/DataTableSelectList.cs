﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Helpers.Control
{
    public class DataTableSelectList
    {
        private DataTable _dataTable;
        private string _valueField;
        private string _textField;
        private object _selectedValue;

        public DataTableSelectList(DataTable dt, string valueField, string textField, object selectedValue)
        {
            _dataTable = dt;
            _valueField = valueField;
            _textField = textField;
            _selectedValue = selectedValue;
        }

        public SelectList ToSelectList()
        {
            if (_dataTable == null || string.IsNullOrEmpty(_valueField) || string.IsNullOrEmpty(_textField))
                return null;

            var list = new List<Object>();

            for (int i = 0; i < _dataTable.Rows.Count; i++)
            {
                list.Add(new
                    {
                        value = _dataTable.Rows[i][_valueField].ToString(),
                        text = _dataTable.Rows[i][_textField].ToString()
                    });
            }
            return new SelectList(list.AsEnumerable(), "value", "text", _selectedValue);
        }
    }
}