﻿namespace GB.Data.Admin
{
    /// <summary></summary>
    public class MetaDirectoryColumn : EntityBase
    {
        /// <summary>Родитель</summary>
        public virtual MetaDirectory Owner { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Наименование столбца</summary>
        public virtual string ColumnName { get; set; }

        /// <summary>Тип столбца</summary>
        public virtual MetaDirectoryColumnType Type { get; set; }

        /// <summary>Порядок сортировки</summary>
        public virtual int OrderIndex { get; set; }

        /// <summary>Отображать</summary>
        public virtual bool Visible { get; set; }

        /// <summary>Для ссылочных столбцов указывает на столбец с текстом (ид вычисляется из имени таблицы)</summary>
        public virtual MetaDirectoryColumn RefColumn { get; set; }
    }

    public enum MetaDirectoryColumnType
    {
        TString,
        TBigString,
        TInt,
        TDecimal,
        TRef,
        TChild,
        TBool,
        TRefMany
    }
}