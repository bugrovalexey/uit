﻿namespace bgTeam.IsRui.Domain.Entities.Directories
{
    using System;
    using System.Collections.Generic;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;

    /// <summary>ИКТ компонент ведомственной информатизации</summary>
    public class IKTComponent : EntityDirectories, IEntityName, IPeriodDirectories
    {
        /// <summary>Код для сортировки</summary>
        public virtual string Code { get; set; }

        /// <summary> Дата начала действия справочника </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary> Дата окончания действия справочника </summary>
        public virtual DateTime? DateEnd { get; set; }

        /// <summary>Родитель</summary>
        public virtual IKTComponent Parent { get; set; }

        /// <summary>
        /// Дети
        /// </summary>
        public virtual IList<IKTComponent> Child { get; set; }

        public virtual IList<AccountingObject> AccountingObjects { get; set; }
    
        public virtual string CodeName
        {
            get
            {
                return string.Format("{0} - {1}", Code, Name);
            }
        }
    }
}