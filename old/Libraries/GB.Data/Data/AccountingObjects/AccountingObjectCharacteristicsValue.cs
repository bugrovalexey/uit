﻿using GB.Data.Plans;
using System;

namespace GB.Data.AccountingObjects
{
    [Serializable]
    public class AccountingObjectCharacteristicsValue : EntityBase
    {
        protected AccountingObjectCharacteristicsValue()
        {
        }

        public AccountingObjectCharacteristicsValue(PlansActivity pa, AccountingObjectCharacteristics characteristics)
        {
            this.Activity = pa;
            this.Characteristics = characteristics;
        }

        /// <summary>Номинальное значение</summary>
        public virtual int? Norm { get; set; }

        /// <summary>Максимальное значение</summary>
        public virtual int? Max { get; set; }

        /// <summary>Характеристика ОУ</summary>
        public virtual AccountingObjectCharacteristics Characteristics { get; set; }

        /// <summary>Мероприятие</summary>
        public virtual PlansActivity Activity { get; set; }
    }
}