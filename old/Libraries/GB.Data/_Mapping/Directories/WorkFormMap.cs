﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class WorkFormMap : ClassMapHiLo<WorkForm>
    {
        public WorkFormMap()
            : base("Work_Form")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Work_Form_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_Work_Form_Insert
            //						  @Work_Form_Code = ?
            //						 ,@Work_Form_Name = ?
            //						 ,@Work_Form_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Work_Form_Update
            //						  @Work_Form_Code = ?
            //						 ,@Work_Form_Name = ?
            //						 ,@Work_Form_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Work_Form_Delete
            //						  @Work_Form_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "Work_Form_Code").Nullable();
            Map(x => x.Name, "Work_Form_Name").Nullable();
            Map(x => x.DateStart, "Work_Form_Start_Date");
            Map(x => x.DateEnd, "Work_Form_End_Date");
        }
    }
}