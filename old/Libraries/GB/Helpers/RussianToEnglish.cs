﻿using System.Collections.Generic;

namespace GB.Helpers
{
    public class RussianToEnglish
    {
        private static Dictionary<char, string> _KeshTranslit = Init();

        private static Dictionary<char, string> Init()
        {
            var abc = new Dictionary<char, string>();

            abc.Add('а', "A");
            abc.Add('б', "B");
            abc.Add('в', "V");
            abc.Add('г', "G");
            abc.Add('д', "D");
            abc.Add('е', "E");
            abc.Add('ё', "YO");
            abc.Add('ж', "JE");
            abc.Add('з', "Z");
            abc.Add('и', "I");
            abc.Add('й', "I");
            abc.Add('к', "K");
            abc.Add('л', "L");
            abc.Add('м', "M");
            abc.Add('н', "N");
            abc.Add('о', "O");
            abc.Add('п', "P");
            abc.Add('р', "R");
            abc.Add('с', "S");
            abc.Add('т', "T");
            abc.Add('у', "U");
            abc.Add('ф', "F");
            abc.Add('х', "H");
            abc.Add('ц', "C");
            abc.Add('ч', "CH");
            abc.Add('ш', "SH");
            abc.Add('щ', "SHA");
            abc.Add('ъ', "");
            abc.Add('ы', "Y");
            abc.Add('ь', "");
            abc.Add('э', "IE");
            abc.Add('ю', "YU");
            abc.Add('я', "YA");
            abc.Add('А', "A");
            abc.Add('Б', "B");
            abc.Add('В', "V");
            abc.Add('Г', "G");
            abc.Add('Д', "D");
            abc.Add('Е', "E");
            abc.Add('Ё', "YO");
            abc.Add('Ж', "JE");
            abc.Add('З', "Z");
            abc.Add('И', "I");
            abc.Add('Й', "I");
            abc.Add('К', "K");
            abc.Add('Л', "L");
            abc.Add('М', "M");
            abc.Add('Н', "N");
            abc.Add('О', "O");
            abc.Add('П', "P");
            abc.Add('Р', "R");
            abc.Add('С', "S");
            abc.Add('Т', "T");
            abc.Add('У', "U");
            abc.Add('Ф', "F");
            abc.Add('Х', "H");
            abc.Add('Ц', "C");
            abc.Add('Ч', "CH");
            abc.Add('Ш', "SH");
            abc.Add('Щ', "SHA");
            abc.Add('Ъ', "J");
            abc.Add('Ы', "Y");
            abc.Add('Ь', "J");
            abc.Add('Э', "IE");
            abc.Add('Ю', "YU");
            abc.Add('Я', "YA");

            return abc;
        }

        public static string Translit(string word)
        {
            string res = string.Empty;

            foreach (char ch in word)
            {
                if (_KeshTranslit.ContainsKey(char.ToUpper(ch)))
                {
                    if (char.IsUpper(ch))
                    {
                        res = res + _KeshTranslit[ch];
                    }
                    else
                    {
                        res = res + _KeshTranslit[char.ToUpper(ch)].ToLower();
                    }
                }
                else
                {
                    res = res + ch;
                }
            }
            return res;
        }
    }
}