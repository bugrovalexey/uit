﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.Access;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport
{
    [MetadataType(typeof(WorkAndServiceEditViewModel_Valid))]
    public class WorkAndServiceEditViewModel : ViewModelBase
    {
        public WorkAndServiceEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;
            ReadOnly = ManagerAccess.CanEdit(new RepositoryDc<AccountingObject>().GetExisting(accountingObjectId)) ==
                       false;

            Okveds = new SelectList(
                new RepositoryDc<OKVED>().GetAll(),
                "Id", "CodeName");

            CategoriesKindOfSupport = new SelectList(
                new RepositoryDc<CategoryKindOfSupport>().GetAllEx(
                    q => q.Where(cat => cat.Type == CategoryTypeOfSupportEnum.WorksAndServices)),
                "Id", "Name");

            var entity = id.HasValue ? new RepositoryDc<WorkAndService>().GetExisting(id) : null;
            entity.Do(Fill);
        }

        protected void Fill(WorkAndService entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            CategoryKindOfSupportId = entity.CategoryKindOfSupport.Return(cat => cat.Id, default(int));
            InformationLeasedInfrastructure = entity.InformationLeasedInfrastructure;
            OkvedId = entity.Okved.Return(okved => okved.Id, default(int));
            Summa = entity.Summa;
        }

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        [DisplayName(TableWorkAndService.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableWorkAndService.CategoryKindOfSupportCaption)]
        public int? CategoryKindOfSupportId { get; set; }

        [DisplayName(TableWorkAndService.InformationLeasedInfrastructuretCaption)]
        public string InformationLeasedInfrastructure { get; set; }

        [DisplayName(TableWorkAndService.OkvedCaption)]
        public int? OkvedId { get; set; }

        [DisplayName(TableWorkAndService.SummaCaption)]
        public decimal? Summa { get; set; }

        public SelectList Okveds { get; set; }

        public SelectList CategoriesKindOfSupport { get; set; }
    }

    internal class WorkAndServiceEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public int? CategoryKindOfSupportId { get; set; }

        [Required(ErrorMessage = RequiredText)]
        [Range(MoneyMin, MoneyMax, ErrorMessage = RangeMoneyText)]
        public decimal? Summa { get; set; }
    }
}