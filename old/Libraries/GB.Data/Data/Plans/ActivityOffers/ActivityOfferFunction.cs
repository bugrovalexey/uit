﻿using Uviri.Entity.Directories.FPGU;
using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans.ActivityOffers
{
    public class ActivityOfferFunction : EntityBase
    {
        /// <summary>Мероприятие плана</summary>
        public virtual ActivityOffer Offer { get; set; }

        /// <summary>Название</summary>
        public virtual string Name { get; set; }

        /// <summary> Пункт, статья документа </summary>
        public virtual string Comment { get; set; }

        /// <summary>Вид</summary>
        public virtual FuncType Type { get; set; }

        /// <summary>НПА</summary>
        public virtual NPA NPA { get; set; }

        /// <summary> Ссылка на госуслугу </summary>
        public virtual Gov_Service Service { get; set; }

        /// <summary> Ссылка на НПА документ </summary>
        public virtual NPA_Document Document { get; set; }

        /// <summary>
        /// Мероприятие направлено на эксплуатацию информационной системы
        /// </summary>
        public virtual bool DirectedToExpl { get; set; }

    }
}
