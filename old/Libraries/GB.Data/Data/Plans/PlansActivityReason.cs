﻿using System;
using System.Collections.Generic;
using GB.Data.Documents;

namespace GB.Data.Plans
{
    // !!! Не используется !!!
    /// <summary>Наименование и реквизиты основания реализации мероприятия</summary>
    public class PlansActivityReason : EntityBase
    {
        protected PlansActivityReason()
        {
            Documents = new List<Files>();
        }

        public PlansActivityReason(PlansActivity Activity)
        {
            this.Activity = Activity;
        }

        /// <summary>Мероприятие</summary>
        public virtual PlansActivity Activity { get; protected set; }

        /// <summary>НПА</summary>
        //public virtual NPA NPA { get; set; }

        /// <summary>Тип НПА (Вид документа)</summary>
        //public virtual NPAType NPAType { get; set; }

        /// <summary>Тип Документа</summary>
        //public virtual NPAKind NPAKind { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Номер НПА</summary>
        public virtual string Num { get; set; }

        /// <summary>Дата</summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>Пункт, статья документа</summary>
        public virtual string Item { get; set; }

        ///// <summary>Документ-основание </summary>
        //[System.Obsolete]
        //public virtual Document Document { get; set; }

        /// <summary>Документы - основания </summary>
        public virtual IList<Files> Documents { get; protected set; }
    }
}