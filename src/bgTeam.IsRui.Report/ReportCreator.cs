﻿using bgTeam.IsRui.FileSystem;
using Stimulsoft.Report;
using System.IO;


namespace bgTeam.IsRui.Report
{
    /// <summary>
    /// Представляет объект для создания отчетов
    /// </summary>
    public static class ReportCreator
    {
        private static readonly string ReportFolder = "~/Reports/";

        static ReportCreator()
        {
            string temp = System.Configuration.ConfigurationManager.AppSettings["ReportFolder"];
            if (!string.IsNullOrEmpty(temp))
                ReportFolder = temp;

            if (!string.IsNullOrEmpty(temp) && temp.StartsWith("../"))
            {
                if(Directory.Exists(ReportFolder))
                {
                    ReportFolder = string.Format("{0}\\{1}",
                                            Directory.GetParent(temp).FullName,
                                            temp.LastIndexOf("/") > 0
                                            && temp.LastIndexOf("/") < temp.Length ?
                                            temp.Substring(temp.LastIndexOf("/") + 1) + "\\" : string.Empty);
                }
            }

            ReportFolder = FileHelper.GetPath(ReportFolder);

            StiOptions.Dictionary.BusinessObjects.MaxLevel = 2;

            StiOptions.Export.Rtf.RemoveEmptySpaceAtBottom = false;
            StiOptions.Export.Rtf.LineHeightExactly = true;
        }


        public static StiReport Create(IReportData data)
        {
            StiReport report = GetRepot(data.Type);
            BaseReportRepository repository = data.GetRepository();

            if (repository != null)
            {
                repository.FillData(report, data.Param);
            }

            return report;
        }

        

        /// <summary>
        /// Загрузить представление отчёта.
        /// </summary>
        private static StiReport GetRepot(string report)
        {
            string path = string.Concat(ReportFolder, report, ".mrt");

            if (File.Exists(path))
            {
                return RepotLoad(path);
            }

            return new ReportMessage("Представление отчёта с наменованием " + report + " не найдено");
        }

        /// <summary>
        /// Загрузить представление отчёта.
        /// </summary>
        private static StiReport RepotLoad(string path)
        {
            StiReport sreport = new StiReport();

            sreport.Load(path);

            System.Web.HttpRuntime.Cache.Insert(path, sreport);

            return sreport;
        }


        public static ZipFileCollection Export(IReportData data)
        {
            ZipFileCollection zip = new ZipFileCollection();

            Export(zip, data);

            return zip;
        }

        public static void Export(ZipFileCollection zip, IReportData data)
        {
            BaseReportExport exporter = data.GetExporter();

            if (exporter != null)
            {
                exporter.FillData(zip, data.Format, data.Param);
            }
        }
    }
}
