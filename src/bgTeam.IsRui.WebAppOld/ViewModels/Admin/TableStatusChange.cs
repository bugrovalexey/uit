﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.Workflow;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    public class TableStatusChange : NotificationBaseViewModel
    {
        public TableStatusChange(StatusList entity)
        {
            Id = entity.Id;
            From = entity.From.Name;
            To = entity.To.Name;
            Role = entity.Role.Name;
        }

        public int Id { get; set; }

        [DisplayName(StatusChangeViewModel.RoleCaption)]
        public string Role { get; set; }

        [DisplayName(StatusChangeViewModel.StatusFromCaption)]
        public string From { get; set; }

        [DisplayName(StatusChangeViewModel.StatusToCaption)]
        public string To { get; set; }

        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("StatusChangeDelete", "Admin", new { id = Id }, "Grid"); }
        }
    }
}