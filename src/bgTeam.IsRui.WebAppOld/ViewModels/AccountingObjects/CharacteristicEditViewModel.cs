﻿using System.Web.Mvc;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using bgTeam.IsRui.MKRF.Logic.Access;
using bgTeam.IsRui.Common.Extentions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    [MetadataType(typeof(CharacteristicEditViewModel_Valid))]
    public class CharacteristicEditViewModel : ViewModelBase
    {
        #region consts

        public const string DateBeginningCaption = "Дата начала";
        public const string DateEndCaption = "Дата конца";

        #endregion consts 

        public CharacteristicEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;
            ReadOnly = ManagerAccess.CanEdit(new RepositoryDc<AccountingObject>().GetExisting(accountingObjectId)) ==
                       false;

            TypesList =
                new SelectList(new RepositoryDc<AccountingObjectCharacteristicsType>().GetAll(),
                    "Id",
                    "Name");

            UnitsList =
                new SelectList(new RepositoryDc<Units>().GetAll(),
                    "Id",
                    "Name");


            var entity = id.HasValue
                ? new AccountingObjectCharacteristicsRepository().Get(id)
                : null;
            entity.Do(Fill);
        }

        #region Methods

        protected void Fill(AccountingObjectCharacteristics entity)
        {
            Id = entity.Id;
            AccountingObjectId = entity.AccountingObject.Id;
            Name = entity.Name;
            TypeId = entity.Type.Return(type => type.Id, EntityBase.Default);
            UnitId = entity.Unit.Return(unit => unit.Id, EntityBase.Default);
            Norm = entity.Norm;
            Max = entity.Max;
            Fact = entity.Fact.GetValueOrDefault();
            DateBeginning = entity.DateBeginning.ToStringOrDefault();
            DateEnd = entity.DateEnd.ToStringOrDefault();
        }

        #endregion Methods

        #region Props

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        [DisplayName(TableCharacteristic.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableCharacteristic.TypeCaption)]
        public int TypeId { get; set; }

        [DisplayName(TableCharacteristic.UnitCaption)]
        public int UnitId { get; set; }

        [DisplayName(TableCharacteristic.FactCaption)]
        public int Fact { get; set; }

        [DisplayName(TableCharacteristic.NormCaption)]
        public int Norm { get; set; }

        [DisplayName(TableCharacteristic.MaxCaption)]
        public int? Max { get; set; }

        [DisplayName(DateBeginningCaption)]
        public string DateBeginning { get; set; }

        [DisplayName(DateEndCaption)]
        public string DateEnd { get; set; }

        public SelectList TypesList { get; set; }

        public SelectList UnitsList { get; set; }

        #endregion Props
    }

    public class CharacteristicEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [RangeAttribute(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        [Required(ErrorMessage = RequiredText)]
        public int? TypeId { get; set; }

        [RangeAttribute(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        [Required(ErrorMessage = RequiredText)]
        public int? UnitId { get; set; }

        [RangeAttribute(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int? Norm { get; set; }
    }
}