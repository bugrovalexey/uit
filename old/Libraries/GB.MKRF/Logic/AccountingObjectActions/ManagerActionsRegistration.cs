﻿using GB.Data.AccountingObjects;
using GB.MKRF.Logic.AccountingObjectActions.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Logic.AccountingObjectActions
{
    public class ManagerActionsRegistration : ManagerRegistrationBase<ManagerActions>
    {
        protected override void RegisterHandlers(ManagerActions manager)
        {
            manager.RegisterHandler(AccountingObjectsStageEnum.Planned, new ActionPlannedHandler());
            manager.RegisterHandler(AccountingObjectsStageEnum.Creation, new ActionCreationHandler());
            manager.RegisterHandler(AccountingObjectsStageEnum.Exploitation, new ActionExploitationHandler());
            manager.RegisterHandler(AccountingObjectsStageEnum.OutExploitation, new ActionOutExploitationHandler());
        }
    }
}
