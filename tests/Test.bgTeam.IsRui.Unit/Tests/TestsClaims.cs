﻿using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Documents;
using bgTeam.IsRui.Story.Claims;
using bgTeam.IsRui.Story.Common;
using Microsoft.AspNetCore.Http.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.bgTeam.IsRui.Unit.Common;
using Xunit;

namespace Test.bgTeam.IsRui.Unit.Tests
{
    public class TestsClaims
    {
        private static Factory _factory = new Factory();

        [Fact]
        public void Story_ClaimGetMyListStory()
        {
            var story = new ClaimGetMyListStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.userRepository);
            var context = new ClaimGetMyListStoryContext();

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.True(res.Any());
        }

        [Fact]
        public void Story_ClaimCreateStory()
        {
            var story = new ClaimCreateStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.userRepository,
                _factory.sessionFactory,
                _factory.statusRepository);
            var context = new ClaimCreateStoryContext
            {
                ActivityId = 212
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimInfoStory()
        {
            var workflowStory = new WorkflowStatusGetNextListStory(
                _factory.storyMapper,
                _factory.repositoryRui,
                _factory.userRepository);
            var conclusionStory = new ClaimGetUserCommentStory(
                _factory.storyMapper,
                _factory.repositoryRui,
                _factory.userRepository);
            var actStory = new ClaimGetActsStory(_factory.repositoryRui, _factory.storyMapper);
            var docStory = new ClaimGetDocumentsStory(_factory.repositoryRui, _factory.storyMapper);
            var story = new ClaimInfoStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.userRepository,
                workflowStory,
                conclusionStory,
                docStory,
                actStory);
            var context = new ClaimInfoStoryContext
            {
                ClaimId = 22
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.NotEmpty(res.Indicators);
            Assert.NotEmpty(res.Acts);
            Assert.NotEmpty(res.Documents);
        }

        [Fact]
        public void Story_ClaimInfoStory_Conclusion()
        {
            var workflowStory = new WorkflowStatusGetNextListStory(
                _factory.storyMapper,
                _factory.repositoryRui,
                _factory.userRepository);
            var conclusionStory = new ClaimGetUserCommentStory(
                _factory.storyMapper,
                _factory.repositoryRui,
                _factory.userRepository);
            var actStory = new ClaimGetActsStory(_factory.repositoryRui, _factory.storyMapper);
            var docStory = new ClaimGetDocumentsStory(_factory.repositoryRui, _factory.storyMapper);
            var story = new ClaimInfoStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.userRepository,
                workflowStory,
                conclusionStory,
                docStory,
                actStory);
            var context = new ClaimInfoStoryContext
            {
                ClaimId = 24
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.NotNull(res.Conclusion);
        }

        [Theory]
        [InlineData(18)]
        public void Story_ClaimInfoStory_theory(int id)
        {
            var workflowStory = new WorkflowStatusGetNextListStory(
                _factory.storyMapper,
                _factory.repositoryRui,
                _factory.userRepository);
            var conclusionStory = new ClaimGetUserCommentStory(
                _factory.storyMapper,
                _factory.repositoryRui,
                _factory.userRepository);
            var actStory = new ClaimGetActsStory(_factory.repositoryRui, _factory.storyMapper);
            var docStory = new ClaimGetDocumentsStory(_factory.repositoryRui, _factory.storyMapper);
            var story = new ClaimInfoStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.userRepository,
                workflowStory,
                conclusionStory,
                docStory,
                actStory);
            var context = new ClaimInfoStoryContext
            {
                ClaimId = id
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimGetContentStory()
        {
            var story = new ClaimGetContentStory(_factory.repositoryRui, _factory.storyMapper);
            var context = new ClaimGetContentStoryContext
            {
                ClaimId = 24
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.NotNull(res.Content);
        }

        [Fact]
        public void Story_ClaimSetContentStory()
        {
            var story = new ClaimSetContentStory(_factory.repositoryRui, _factory.sessionFactory);
            var context = new ClaimSetContentStoryContext
            {
                ClaimId = 24,
                Content = "<html></html>"
            };

            var res = story.Execute(context);

            Assert.True(res);
        }

        [Fact]
        public void Story_ClaimSetJustificationStory()
        {
            var story = new ClaimSetJustificationStory(_factory.repositoryRui, _factory.sessionFactory);
            var context = new ClaimSetJustificationStoryContext
            {
                ClaimId = 24,
                Justification = "Justification"
            };

            var res = story.Execute(context);

            Assert.True(res);
        }

        [Fact]
        public void Story_ClaimGetJustificationStory()
        {
            var story = new ClaimGetJustificationStory(_factory.repositoryRui, _factory.storyMapper);
            var context = new ClaimGetJustificationStoryContext
            {
                ClaimId = 24
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.NotNull(res.Justification);
        }

        [Fact]
        public void Story_ClaimGetIndicatorsStory()
        {
            var story = new ClaimGetIndicatorsStory(_factory.repositoryRui, _factory.storyMapper);
            var context = new ClaimGetIndicatorsStoryContext
            {
                ClaimId = 24
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.NotEmpty(res);
        }

        [Fact]
        public void Story_ClaimCreateOrUpdateIndicatorStory()
        {
            var story = new ClaimCreateOrUpdateIndicatorStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.sessionFactory);
            var context = new ClaimCreateOrUpdateIndicatorStoryContext
            {
                ClaimId = 24,
                AlgorithmOfCalculating = string.Empty,
                BaseValue = 1,
                DateToAchieveGoalValue = new DateTime(2018, 6, 13),
                ExplanationRelationshipWithProjectMark = string.Empty,
                Id = 1,
                Justification = string.Empty,
                Name = string.Empty,
                TargetedValue = 2,
                UnitId = null
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Theory]
        [InlineData(24)]
        [InlineData(30)]
        [InlineData(34)]
        public void Story_ClaimGetDocumentsStory(int id)
        {
            var story = new ClaimGetDocumentsStory(_factory.repositoryRui, _factory.storyMapper);
            var context = new ClaimGetDocumentsStoryContext
            {
                ClaimId = id
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.NotEmpty(res);
        }

        [Fact]
        public void Story_ClaimCreateOrUpdateDocumentStory_Update()
        {
            var story = new ClaimCreateOrUpdateDocumentStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.sessionFactory,
                _factory.fileProviderRui);
            var context = new ClaimCreateOrUpdateDocumentStoryContext
            {
                ClaimId = 24,
                File = null,
                Id = 38,
                DateAdoption = DateTime.Now,
                Name = "Claim document",
                Number = "#1",
                ParticleOfDocument = "The big text 1023 symbols"
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimCreateOrUpdateDocumentStory_Create()
        {
            var story = new ClaimCreateOrUpdateDocumentStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.sessionFactory,
                new FileProviderRuiMock());
            var context = new ClaimCreateOrUpdateDocumentStoryContext
            {
                ClaimId = 24,
                File = new global::bgTeam.IsRui.Domain.Dto.CreateDocumentFileData
                {
                    FileName = "Files add step 1.png",
                    TempGuid = new Guid("40fd14ac-c7e2-418e-8a85-163897d62aa6"),
                    Extension = "png",
                    Kind = global::bgTeam.IsRui.Domain.Entities.Documents.DocKindEnum.File,
                    Id = null,
                    MimeType = "",
                    Size = null
                },
                Id = null,
                DateAdoption = DateTime.Now,
                Name = "Claim document",
                Number = "#" + new Random().Next(0x0, 0xFFFF),
                ParticleOfDocument = "The big text 1023 symbols"
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimGetBaseInfoStory()
        {
            var story = new ClaimGetBaseInfoStory(_factory.sessionFactory);
            var context = new ClaimGetBaseInfoStoryContext
            {
                ClaimId = 24
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.Equal("4", res.Number);
        }

        [Fact]
        public void Story_ClaimGetActsStory()
        {
            var story = new ClaimGetActsStory(_factory.repositoryRui, _factory.storyMapper);
            var context = new ClaimGetActsStoryContext
            {
                ClaimId = 24
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.NotEmpty(res);
        }

        [Fact]
        public void Story_ClaimCreateOrUpdateActStory_Update()
        {
            var story = new ClaimCreateOrUpdateActStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.sessionFactory,
                new FileProviderRuiMock());
            var context = new ClaimCreateOrUpdateActStoryContext
            {
                ClaimId = 24,
                Id = 1,
                Name = "ClaimAct",
                Number = "#1",
                File = new global::bgTeam.IsRui.Domain.Dto.CreateDocumentFileData
                {
                    FileName = "ActFile.png",
                    TempGuid = new Guid("40fd14ac-c7e2-418e-8a85-163897d62aa6"),
                    Extension = "png",
                    Kind = global::bgTeam.IsRui.Domain.Entities.Documents.DocKindEnum.File,
                    Id = null,
                    MimeType = "",
                    Size = null
                },
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimCreateOrUpdateActStory_Create()
        {
            var story = new ClaimCreateOrUpdateActStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.sessionFactory,
                new FileProviderRuiMock());
            var context = new ClaimCreateOrUpdateActStoryContext
            {
                ClaimId = 24,
                Id = null,
                Name = "ClaimAct",
                Number = "#" + new Random().Next(1, 0xFFFF),
                DateAdoption = DateTime.Now,
                Cost = 1010M,
                File = new global::bgTeam.IsRui.Domain.Dto.CreateDocumentFileData
                {
                    FileName = "ActFile.png",
                    TempGuid = new Guid("40fd14ac-c7e2-418e-8a85-163897d62aa6"),
                    Extension = "png",
                    Kind = global::bgTeam.IsRui.Domain.Entities.Documents.DocKindEnum.File,
                    Id = null,
                    MimeType = "",
                    Size = null
                },
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimCreateOrUpdateActStory_Create2()
        {
            var story = new ClaimCreateOrUpdateActStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.sessionFactory,
                new FileProviderRuiMock());
            var context = new ClaimCreateOrUpdateActStoryContext
            {
                ClaimId = 24,
                Id = null,
                Name = "ClaimAct",
                Number = "#" + new Random().Next(1, 0xFFFF),
                File = new global::bgTeam.IsRui.Domain.Dto.CreateDocumentFileData
                {
                    FileName = "ActFile.png",
                    TempGuid = new Guid("40fd14ac-c7e2-418e-8a85-163897d62aa6")
                },
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimCreateOrUpdateActStory_KITVLG_237()
        {
            var byteStr = Encoding.UTF8.GetBytes("Test file.");
            var ms = new MemoryStream(byteStr);

            var file = new FormFile(ms, 0, byteStr.LongLength, "Test", "TestFile");
            var guidStr = _factory.fileProviderRui.SaveAsync(file).Result;

            var story = new ClaimCreateOrUpdateActStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.sessionFactory,
                _factory.fileProviderRui);
            var context = new ClaimCreateOrUpdateActStoryContext
            {
                ClaimId = 22,
                Id = null,
                Name = "Акт о вводе в эксплуатацию",
                Number = "25",
                DateAdoption = DateTime.Now,
                Cost = 1000M,
                File = new global::bgTeam.IsRui.Domain.Dto.CreateDocumentFileData
                {
                    FileName = "Test file.txt",
                    TempGuid = new Guid(guidStr)
                },
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimGetScopeOfWorkStory()
        {
            var story = new ClaimGetScopeOfWorkStory(_factory.repositoryRui, _factory.storyMapper);
            var context = new ClaimGetScopeOfWorkStoryContext
            {
                ClaimId = 22
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
            Assert.NotEmpty(res);
        }

        [Fact]
        public void Story_ClaimCreateOrUpdateScopeOfWorkStory_Update()
        {
            var story = new ClaimCreateOrUpdateScopeOfWorkStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.sessionFactory);
            var context = new ClaimCreateOrUpdateScopeOfWorkStoryContext
            {
                ClaimId = 24,
                Id = 1,
                Cost = 100M,
                DateBegin = DateTime.Now,
                DatePlanEnd = DateTime.Now.AddDays(20),
                Description = "Market develop",
                LaborCostsHours = 1400,
                SpecialistId = 1,
                WorkHours = 1200,
                WorkType = "Programming"
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimCreateOrUpdateScopeOfWorkStory_Create()
        {
            var story = new ClaimCreateOrUpdateScopeOfWorkStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.sessionFactory);
            var context = new ClaimCreateOrUpdateScopeOfWorkStoryContext
            {
                ClaimId = 24,
                Id = null,
                Cost = 100M,
                DateBegin = DateTime.Now,
                DatePlanEnd = DateTime.Now.AddDays(20),
                Description = "Market develop",
                LaborCostsHours = 1400,
                SpecialistId = 1,
                WorkHours = 1200,
                WorkType = "Programming"
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimCreateOrUpdateScopeOfWorkStory_Create2()
        {
            var story = new ClaimCreateOrUpdateScopeOfWorkStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.sessionFactory);
            var context = new ClaimCreateOrUpdateScopeOfWorkStoryContext
            {
                ClaimId = 24,
                Id = null,
                Cost = 100M,
                DateBegin = null,
                DatePlanEnd = null,
                Description = "Market develop",
                LaborCostsHours = 1400,
                SpecialistId = 1,
                WorkHours = 1200,
                WorkType = "Programming"
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimCreateOrUpdateUserCommentStory_Create()
        {
            var story = new ClaimCreateOrUpdateUserCommentStory(
                _factory.repositoryRui,
                _factory.storyMapper,
                _factory.sessionFactory,
                _factory.userRepository,
                _factory.statusRepository);
            var context = new ClaimCreateOrUpdateUserCommentStoryContext
            {
                Id = null,
                ClaimId = 24,
                Text = "User comment"
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimGetUserCommentStory()
        {
            var story = new ClaimGetUserCommentStory(
                _factory.storyMapper,
                _factory.repositoryRui,
                _factory.userRepository);
            var context = new ClaimGetUserCommentStoryContext
            {
                ClaimId = 24
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimAddFileUserCommentStory()
        {
            var byteStr = Encoding.UTF8.GetBytes("Test file.");
            var ms = new MemoryStream(byteStr);

            var file = new FormFile(ms, 0, byteStr.LongLength, "Test", "TestFile");
            var guidStr = _factory.fileProviderRui.SaveAsync(file).Result;

            var story = new ClaimAddFileUserCommentStory(
                _factory.storyMapper,
                _factory.repositoryRui,
                _factory.userRepository,
                _factory.sessionFactory,
                _factory.fileProviderRui,
                _factory.statusRepository);
            var context = new ClaimAddFileUserCommentStoryContext
            {
                ClaimId = 24,
                Extension = "png",
                FileName = "TestFile",
                Kind = DocKindEnum.File,
                MimeType = "image/png",
                Size = byteStr.Length,
                TempGuid = new Guid(guidStr)
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }

        [Fact]
        public void Story_ClaimGetConclusionHistoryStory()
        {
            var story = new ClaimGetConclusionHistoryStory(
                _factory.repositoryRui,
                _factory.storyMapper);
            var context = new ClaimGetConclusionHistoryStoryContext
            {
                ClaimId = 24
            };

            var res = story.Execute(context);

            Assert.NotNull(res);
        }
    }
}
