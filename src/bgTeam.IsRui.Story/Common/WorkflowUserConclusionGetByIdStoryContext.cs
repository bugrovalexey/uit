﻿namespace bgTeam.IsRui.Story.Common
{
    public class WorkflowUserConclusionGetByIdStoryContext
    {
        public int? Id { get; set; }
    }
}