﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivitysMarksDto
    {
        public int? Id { get; set; }

        public int? ActivityId { get; set; }

        /// <summary>
        /// Пояснение связи с показателем проекта
        /// </summary>
        public string ExplanationRelationshipWithProjectMark { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Алгортим
        /// </summary>
        public string AlgorithmOfCalculating { get; set; }

        /// <summary>
        /// Базовое значение
        /// </summary>
        public int? BaseValue { get; set; }

        /// <summary>
        /// Целевое значение
        /// </summary>
        public int? TargetedValue { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public int? UnitId { get; set; }

        public string UnitName { get; set; }

        /// <summary>
        /// Дата достижения целевого значения
        /// </summary>
        public DateTime? DateToAchieveGoalValue { get; set; }
    }
}
