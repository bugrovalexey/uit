﻿using bgTeam.IsRui.Common;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.MKRF.Providers;
using System;
using System.Web.Mvc;
using System.Reflection;
using Castle.Windsor;

namespace bgTeam.IsRui.WebApp
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    internal sealed class FilterExceptionAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext expContext)
        {
            Logger.Fatal(expContext.Exception);

            //нет доступа к объекту
            if (expContext.Exception is ObjectAccessException)
                ResponseHelper.DenyAccessToObject(expContext.HttpContext.Response);

            //объект не существует
            else if (expContext.Exception is EntityNotFoundException)
                expContext.HttpContext.Response.Redirect("~/Error/NotFound");

            //filterContext.HttpContext.Response.RedirectToRoute("AccessError");
        }
    }

    [Serializable]
    public class EntityNotFoundException : Exception
    {
        //private int id;

        public EntityNotFoundException(int id)
        {
            //this.id = id;
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    internal sealed class FilterActionAttribute : ActionFilterAttribute
    {
        private static IAccessControllerProvider s_accessControllerProvider;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Обрабатываем только GET
            if (filterContext.HttpContext.Request.HttpMethod.Equals("POST")) return;

            //Dirty Hack ¯\_(ツ)_/¯
            if (s_accessControllerProvider == null)
            {
                if ((filterContext.HttpContext.ApplicationInstance
                    ?.GetType()
                    ?.BaseType
                    ?.GetField("container", BindingFlags.NonPublic | BindingFlags.Static)
                    ?.GetValue(filterContext.HttpContext.ApplicationInstance) is IWindsorContainer container) &&
                    container.Resolve<IAccessControllerProvider>() is IAccessControllerProvider provider)
                {
                    s_accessControllerProvider = provider;
                }
                else s_accessControllerProvider = Singleton<AccessControllerProvider>.Instance;
            }

            // Проверяем доступ к странице
            if (!s_accessControllerProvider.СheckAccessCurrentUser(
                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.ActionDescriptor.ActionName))
                ResponseHelper.DenyAccessToPage(filterContext.HttpContext.Response);
        }
    }
}