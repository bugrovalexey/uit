﻿using bgTeam.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Extensions;
using bgTeam.IsRui.WebApp.Helpers.Control;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    [MetadataType(typeof(AccessToEntityViewModel_Valid))]
    public class AccessToEntityViewModel : ViewModelBase
    {
        private const string RoleCaption = "Роль";
        private const string StatusCaption = "Статус";
        private const string NoteCaption = "Примечание";

        public AccessToEntityViewModel(IStatusRepository statusRepository)
        {
            _statusRepository = statusRepository;
        }

        public AccessToEntityViewModel(EntityType entityType, int? id)
        {
            //this.EntityType = entityType;
            var access = id.HasValue ? new RepositoryDc<AccessToEntity>().GetExisting(id) : null;
            access.Do(Fill);

            StatusList = new IdNameSelectListWithNull(
                _statusRepository.GetStatuses(entityType),
                Status);
            RoleList = new SelectList(
                new RepositoryDc<Role>().GetAll(),
                "Id", "Name", Role);
        }

        public void Fill(AccessToEntity value)
        {
            //this.EntityType = value.EntityType;
            Id = value.Id;
            AccessMask = value.AccessMask;
            Role = value.Role.Return(x => x.Id, default(int));
            Status = value.Status.Return(x => x.Id, default(int));
            Note = value.Note;
        }

        //EntityType EntityType;
        private AccessActionEnum AccessMask;
        private readonly IStatusRepository _statusRepository;

        public int Id { get; set; }

        [DisplayName("Чтение")]
        public bool AccessRead
        {
            get { return AccessMask.CanDo(AccessActionEnum.Read); }
        }

        [DisplayName("Редактирование")]
        public bool AccessEdit
        {
            get { return AccessMask.CanDo(AccessActionEnum.Edit); }
        }

        [DisplayName("Удаление")]
        public bool AccessDelete
        {
            get { return AccessMask.CanDo(AccessActionEnum.Delete); }
        }

        [DisplayName("Создание")]
        public bool AccessCreate
        {
            get { return AccessMask.CanDo(AccessActionEnum.Create); }
        }

        //[DisplayName("Полный доступ")]
        //public bool AccessAll
        //{
        //    get { return AccessMask.CanDo(AccessAction.All); }
        //}

        [DisplayName(RoleCaption)]
        public int Role { get; set; }

        [DisplayName(StatusCaption)]
        public int? Status { get; set; }

        [DisplayName(NoteCaption)]
        public string Note { get; set; }

        public SelectList RoleList { get; set; }

        public SelectList StatusList { get; set; }
    }

    public class AccessToEntityViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public int Role { get; set; }
    }
}