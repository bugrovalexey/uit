﻿using System.Security.Permissions;
using System.Web;
using System.Web.UI;

namespace GB.Controls
{
    [AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal),
    AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    public class RoundPanel : Control
    {
        public override string ID 
        { 
            get; 
            set; 
        }

        public string Header
        {
            get;
            set;
        }

        public string HeaderImg
        {
            get;
            set;
        }

        public string Width
        {
            get;
            set;
        }

        //protected override void CreateChildControls()
        //{
        //    base.CreateChildControls();

        //    HtmlTable table = new System.Web.UI.HtmlControls.HtmlTable();
        //    table.Attributes.Add("class", "panel");

        //    if (!string.IsNullOrEmpty(this.Width))
        //        table.Width = this.Width;

        //    HtmlTableRow tr = new HtmlTableRow();

        //    HtmlTableCell td = new HtmlTableCell();
        //    td.Attributes.Add("class", "paneltopl");
        //    tr.Cells.Add(td);

        //    td = new HtmlTableCell();
        //    td.Attributes.Add("class", "paneltopm");

        //    HtmlTable headerTable = new HtmlTable();
        //    headerTable.Attributes.Add("class", "panelhead");

        //    HtmlTableRow headerRow = new HtmlTableRow();
        //    HtmlTableCell headerCell = null;
        //    if (!string.IsNullOrEmpty(HeaderImg))
        //    {
        //        headerCell = new HtmlTableCell();
        //        HtmlImage image = new HtmlImage();
        //        image.Src = Page.ResolveUrl(HeaderImg);
        //        image.Alt = Header;
        //        image.Border = 0;
        //        headerCell.Controls.Add(image);
        //        headerRow.Cells.Add(headerCell);

        //        headerCell = new HtmlTableCell();
        //        headerCell.Width = "5px";
        //        headerRow.Cells.Add(headerCell);
        //    }

        //    headerCell = new HtmlTableCell();
        //    headerCell.InnerText = Header;
        //    headerRow.Cells.Add(headerCell);
        //    headerTable.Rows.Add(headerRow);

        //    td.Controls.Add(headerTable);
        //    tr.Cells.Add(td);

        //    td = new HtmlTableCell();
        //    td.Attributes.Add("class", "paneltopr");
        //    tr.Cells.Add(td);

        //    table.Rows.Add(tr);


        //    tr = new HtmlTableRow();

        //    td = new HtmlTableCell();
        //    td.Attributes.Add("class", "panell");
        //    tr.Cells.Add(td);

        //    td = new HtmlTableCell();
        //    td.Attributes.Add("class", "panelm");
        //    for (int i = this.Controls.Count - 1; i >= 0; i--)
        //    {
        //        Control c = this.Controls[i];
        //        this.Controls.RemoveAt(i);
        //        td.Controls.AddAt(0, c);
        //    }
        //    tr.Cells.Add(td);

        //    td = new HtmlTableCell();
        //    td.Attributes.Add("class", "panelr");
        //    tr.Cells.Add(td);

        //    table.Rows.Add(tr);


        //    tr = new HtmlTableRow();

        //    td = new HtmlTableCell();
        //    td.Attributes.Add("class", "panelfotl");
        //    tr.Cells.Add(td);

        //    td = new HtmlTableCell();
        //    td.Attributes.Add("class", "panelfotm");
        //    tr.Cells.Add(td);

        //    td = new HtmlTableCell();
        //    td.Attributes.Add("class", "panelfotr");
        //    tr.Cells.Add(td);

        //    table.Rows.Add(tr);


        //    tr = new HtmlTableRow();

        //    td = new HtmlTableCell();
        //    td.Attributes.Add("class", "panelsep");
        //    td.ColSpan = 3;
        //    tr.Cells.Add(td);

        //    table.Rows.Add(tr);



        //    this.Controls.Clear();
        //    this.Controls.Add(table);
        //}

        protected override void Render(HtmlTextWriter writer)
        {
            string html = @"
<table class=""panel"" " + (string.IsNullOrEmpty(Width) ? "" : ("width='" + Width + "'")) + (string.IsNullOrEmpty(ID) ? "" : (" id='" + ID + "'")) + @" >
    <tr>
        <td class=""paneltopl""></td>
        <td class=""paneltopm"">
            <table class=""panelhead"">
                <tr>" + (string.IsNullOrEmpty(HeaderImg) ? "" : (@"
                    <td>
                        <img src='" + HeaderImg + @"' alt='" + Header + @"' border=""0"" />
                    </td>
                    <td width=""5px""></td>")) + @"                    
                    <td>
                        " + Header + @"
                    </td>
                </tr>
            </table>
        </td>
        <td class=""paneltopr""></td>
    </tr>
    <tr>
        <td class=""panell""></td>
        <td class=""panelm"">";


            writer.Write(html);
            base.Render(writer);

            html = @"</td>
        <td class=""panelr""></td>
    </tr>
    <tr>
        <td class=""panelfotl""></td>
        <td class=""panelfotm""></td>
        <td class=""panelfotr""></td>
    </tr>
    <tr>
        <td class=""panelsep"" colspan=""3""></td>
    </tr>
</table>";

            writer.Write(html);

        }

    }
}