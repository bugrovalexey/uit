﻿using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Documents;
using bgTeam.IsRui.Domain.Entities.Workflow;
using bgTeam.IsRui.Domain.Extensions;
using bgTeam.IsRui.MKRF.Logic.Access;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using bgTeam.IsRui.MKRF.Logic.Status;
using bgTeam.IsRui.MKRF.Logic.Tabs;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class AccountingObjectBase : ViewModelBase
    {
        protected IMasterEditHandler MasterHandler { get; set; }

        public string UrlEdit { get; private set; }

        private AccountingObjectUserComment _comment;
        private readonly IStatusRepository _statusRepository;
        private readonly IUserRepository _userRepository;

        public AccountingObjectBase(
            IStatusRepository statusRepository,
            IUserRepository userRepository)
        {
            _statusRepository = statusRepository;
            _userRepository = userRepository;
        }

        public AccountingObjectBase(int? id, params Expression<Func<AccountingObject, object>>[] fetches)
        {
            if (id.HasValue == false)
            {
                ReadOnly = true;
                return;
            }

            Id = id.Value;

            this.Current = new RepositoryDc<AccountingObject>().Get(q =>
            {
                q = q.Where(x => x.Id == id);

                foreach (var f in fetches)
                {
                    q.Fetch(f).Eager.Clone();
                }
            });

            if (this.Current == null)
                throw new EntityNotFoundException(id.Value);


            if (ManagerAccess.CanRead(Current) == false)
                throw new ObjectAccessException();

            ReadOnly = ManagerAccess.CanEdit(Current) == false;

            var handler = Singleton<TabsManager>.Instance.GetHandler<AccountingObjectsCardTabs>();

            Tabs = handler.GetTabsList(InitTabs);

            MasterHandler = Singleton<MasterEditManager>.Instance.GetHandler(Current);

            //формируем ссылку для редактирования
            var first = MasterHandler.First();

            UrlEdit = LinkBuilder.Url(first.Action, "AccountingObjects", RouteHelper.Merge(new { id = Id }, first.Params));

            FilesData = new List<IEntityName>();                
        }

        private string InitTabs(string text, string actionName, string controllerName)
        {
            return LinkBuilder.ActionLink(text, actionName, controllerName, new { Id });
        }

        public IEnumerable<AjaxLinkViewModel> GetNextStatusLinks()
        {
            var handlerActivity = Singleton<WorkflowManager>.Instance.GetHandler<AccountingObject>();
            foreach (StatusList item in _statusRepository.GetNext(Current))
            {
                yield return CreateAjaxLink(Current, handlerActivity, item);
            }
        }

        private AjaxLinkViewModel CreateAjaxLink(IWorkflowObject wo, IWorkflowHandler handler, StatusList item)
        {
            var link = new AjaxLinkViewModel
            {
                Text = item.To.ActionName ?? item.To.Name,
                Title = string.Format("Переход в статус '{0}'", item.To.Name),
                Callback = string.Format("ChangeStatus({0}, '{1}', {2})", Id, wo.GetTableType(), item.To.Id),
                Css = item.To.Css
            };

            if (handler != null)
            {
                string confirm, error;
                if (!handler.ReadyForNewStatus(wo, item.To, out error, out confirm))
                {
                    link.Disabled = true;
                    link.Title = error;
                    link.Callback = string.Format("alert('{0}')", error.Replace("'", "\"").Replace(Environment.NewLine, " "));
                }
                else
                {
                    link.ConfirmMessage = confirm;
                }
            }
            return link;
        }

        public int Id { get; private set; }

        public AccountingObject Current { get; set; }

        #region Layout

        public string Title
        {
            get
            {
                return Current.ShortName;
            }
        }

        public int? CommentId
        {
            get
            {
                return Comment != null ? Comment.Id : (int?)null;
            }
        }

        

        public AccountingObjectUserComment Comment
        {
            get
            {
                if (_comment == null)
                {
                    var curUser = _userRepository.GetCurrent();                   

                    if (curUser != null)
                    {
                        _comment = new RepositoryDc<AccountingObjectUserComment>().Get(q => q
                                            .Fetch(x => x.Documents).Eager                                            
                                            .Where(x => x.AccountingObject.Id == Current.Id && x.User.Id == curUser.Id && x.Status == null));
                    }
                }

                return _comment;
            }
        }

        public IList<Files> CommentDocs
        {
            get
            {
                return Comment != null ? Comment.Documents : new List<Files>();
            }

        }

        public string CommentText
        {
            get
            {
                return Comment != null ? Comment.Comment : string.Empty;                
        }
        }

        public IList<string> Tabs { get; private set; }

        [DisplayName("")]
        public IEnumerable<IEntityName> FilesData { get; set; }

        #endregion Layout
    }
}