﻿using System;
using System.Web;
using System.Web.Configuration;

namespace GB.ESIA.Web.Helpers
{
	/// <summary>
	/// Класс доступа к параметрам приложения.
	/// </summary>
	internal class ConfigAccessor
	{
		/// <summary>
		/// URL, на который ServiceProvider должен отправлять запрос аутентификации.
		/// </summary>
		public static string SingleSignOnServiceUrl
		{
			get
			{
				return WebConfigurationManager.AppSettings["SingleSignOnServiceUrl"];
			}
		}

		/// <summary>
		/// URL, на который ServiceProvider должен посылать запрос выхода.
		/// </summary>
		public static string SingleLogoutServiceUrl
		{
			get
			{
				return WebConfigurationManager.AppSettings["SingleLogoutServiceUrl"];
			}
		}
		
		/// <summary>
		/// URL, на который ServiceProvider должен посылать подтверждение успешного выхода.
		/// </summary>
		public static string SingleLogoutServiceResponseUrl
		{
			get
			{
				return WebConfigurationManager.AppSettings["SingleLogoutServiceResponseUrl"];
			}
		}

        ///// <summary>
        ///// URL, на который IdentityProvider должен посылать ответ после идентификации.
        ///// </summary>
        //public static string AssertionServiceUrl
        //{
        //    get
        //    {
        //        return WebConfigurationManager.AppSettings["AssertionServiceUrl"];
        //    }
        //}
        
        /// <summary>
		/// URL, на который IdentityProvider должен посылать ответ после идентификации.
		/// </summary>
        public static string AssertionServiceUrl(HttpRequest request)
        {
            var serviceUrl = WebConfigurationManager.AppSettings["AssertionServiceUrl"];
            if (request.Url.Host.Contains("www."))
            {
                serviceUrl = serviceUrl.Replace("://", "://www.");
            }
            if (request.IsSecureConnection)
            {
                serviceUrl = serviceUrl.Replace("http://", "https://");
            }
            return serviceUrl;
        }
        //public static string AssertionServiceUrl
        //{
        //    get
        //    {
        //        return WebConfigurationManager.AppSettings["AssertionServiceUrl"];
        //    }
        //}

		/// <summary>
		/// Путь к сертификату IdentityProvider'а. Указывается относительно каталога, в котором располагается web-сайт.
		/// </summary>
		public static string IdentityProviderCertFile
		{
			get
			{
				return WebConfigurationManager.AppSettings["IdentityProviderCertFile"];
			}
		}

		/// <summary>
		/// Путь к сертификату ServiceProvider'а. Указывается относительно каталога, в котором располагается web-сайт.
		/// </summary>
		public static string ServiceProviderCertFile
		{
			get
			{
				return WebConfigurationManager.AppSettings["ServiceProviderCertFile"];
			}
		}

		/// <summary>
		/// Пароль к сертификату ServiceProvider'а.
		/// </summary>
		public static string ServiceProviderCertPassword
		{
			get
			{
				return WebConfigurationManager.AppSettings["ServiceProviderCertPassword"];
			}
		}

		/// <summary>
		/// Режим отладки: true - включен, false - не включен.
		/// При включенном режиме отладки на диск записывается отладочная информация.
		/// </summary>
		public static bool DebugMode
		{
			get { return Convert.ToBoolean(WebConfigurationManager.AppSettings["DebugMode"] ?? "false"); }
		}

        /// <summary>
        /// Показывать контролы для аутентификации через ЕСИА
        /// </summary>
        public static bool VisibleControl 
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings["ShowESIAControls"] ?? "false"); }
        }
	}
}