﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    /// <summary>Целевой показатель Плана</summary>
    public class PlansGoalIndicator : EntityBase
    {
        protected PlansGoalIndicator()
        {
        }

        public PlansGoalIndicator(PlansActivity PlansActivity)
        {
            this.PlanActivity = PlansActivity;
        }

        /// <summary>Год</summary>
        public virtual int Year { get; set; }

        /// <summary>Мероприятие Плана</summary>
        public virtual PlansActivity PlanActivity { get; protected set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Единица измерения</summary>
        public virtual string UOM { get; set; }//null

        /// <summary>Начальное значение</summary>
        public virtual string ValStart { get; set; }//null

        /// <summary>Конечное значение</summary>
        public virtual string ValEnd { get; set; }//null

        /// <summary>Фактическое значение</summary>
        public virtual string PRSFactVal { get; set; }

        /// <summary>Срок достижения конечного значения показателя</summary>
        public virtual string ForwardValueEnd { get; set; }

        /// <summary> Дополнительное финансирование </summary>
        public virtual string AddFounding { get; set; }

        /// <summary> Базовый индикатор </summary>
        public virtual PlansGoalIndicator Parent { get; set; }

        /// <summary>
        /// Тип, 1 - индикатор или 0 - показатель
        /// </summary>
        public virtual int FeatureType { get; set; }

        /// <summary>
        /// Достигнутое значение
        /// </summary>
        public virtual string ReachedValue { get; set; }

        //public virtual PlansActivityFunc ActivityFunc { get; set; }

        /// <summary> Цель мероприятия </summary>
        public virtual PlansActivityGoal Goal { get; set; }

        public virtual string Algorythm { get; set; }
    }
}