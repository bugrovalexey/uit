﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Repository;
using GB.MKRF.ViewModels;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class HiddenActivity : System.Web.UI.Page
    {
        public const string FromIdKey = "from";
        public static string GetUrl(object ActivityId, object fromActivity = null)
        {
            var s = string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/Plans/v2012new/HiddenActivity.aspx"), ActivityCard.ActivityIdKey, ActivityId);
            if (fromActivity != null)
            {
                s += string.Format("&{0}={1}", FromIdKey, fromActivity);
            }
            return s;
        }

        protected ActivityViewModel model = null;
        protected PlansActivityController controller = new PlansActivityController();


        protected void Page_Load(object sender, EventArgs e)
        {
            var id = Page.Request.QueryString[ActivityCard.ActivityIdKey];
            if (id != null) // && PlansActivityRepository.IsHidden(id)
            {
                DataSet data = PlansActivityRepository.GetHidden(id);
                model = controller.GetActivityViewModel(data);
                ActivityReadMode.SetViewModel(model);
            }

            ActivityReadMode.DefaultShowEditLinks = false;

            //ShowObjectsRepository.SaveVisitedObjects(d, ObjectsType.Activity, UserRepository.GetCurrent().Id);
        }

        protected string GetActivityTitle()
        {
            if (model == null)
                return null;

            return string.Format("<span style='color:#C53939'>{0}</span> — {1}", model.Code, model.Name.Trim());
        }

        protected string GetOwner()
        {
            string fromId = Request.QueryString[FromIdKey];
            if (string.IsNullOrWhiteSpace(fromId))
            {
                if (model != null && model.JoinedTo.HasValue)
                {
                    fromId = model.JoinedTo.Value.ToString();
                }
            }

            if (!string.IsNullOrWhiteSpace(fromId))
            {
                var owner = PlansActivityRepository.GetHidden(fromId, true);

                if (owner != null && owner.Tables.Contains("Plans_Activity") && owner.Tables["Plans_Activity"].Rows.Count > 0)
                {
                    var row = owner.Tables["Plans_Activity"].Rows[0];

                    string url = Convert.ToBoolean(row["Is_Deleted"]) ? GetUrl(fromId, model.Id) : CommonData.GetUrl(fromId);

                    return string.Format("<a href='{0}'>{1} — {2}</a>", url, row["Plans_Activity_Code"], row["Plans_Activity_Name"]);
                }

            }
            return null;
        }

        protected IList<ActivityLinkViewModel> Links
        {
            get
            {
                return controller.GetLinks(model.Id);
            }
        }


        protected string GetLinks(int LinkType)
        {
            var items = Links.Where(x => x.LinkType == LinkType).ToList();

            if (items.Any())
            {
                return "<ul style='text-align:left'>" + 
                    
                    string.Concat(
                        items.Select(x => string.Format("<li><a href='{0}'><span style='color:#C53939'>{1}</span> — {2}</a>{3}</li>", 
                                                        x.IsDeleted ? GetUrl(x.Id, model.Id) : ActivityReadOnly.GetUrl(x.Id),
                                                        x.Code,
                                                        x.Name,
                                                        x.IsDeleted ? " [удалено]" : ""))
                             .ToArray()) + 
                    
                    "</ul>";
            }
            else
            {
                return "";
            }
        }

    }
}