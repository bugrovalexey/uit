﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Web.UI;
using System.Xml;
using Atp.Saml2;

namespace GB.ESIA
{
	/// <summary>
	/// Запрос отсоединения от SSO.
	/// </summary>
	public class LogoutRequest
	{
		private readonly string relayState = Guid.NewGuid().ToString();
		/// <summary>
		/// GUID-идентификатор запроса.
		/// </summary>
		public string RelayState
		{
			get { return relayState; }
		}

		/// <summary>
		/// Идентификатор запроса.
		/// </summary>
		public string Id
		{
			get { return logoutRequest.Id; }
		}

		/// <summary>
		/// Представление запроса в виде XML.
		/// </summary>
		/// <remarks>Пример использования: [EasyLogoutRequest].Xml.OuterXml</remarks>
		public XmlNode Xml
		{
			get { return logoutRequest.GetXml().ParentNode; }
		}

		/// <summary>
		/// Страница, с которой инициирован запрос.
		/// </summary>
		private readonly Page pageFrom;

		/// <summary>
		/// Сертификат, которым должен быть подписан отправляемый запрос.
		/// </summary>
		private readonly X509Certificate2 spX509Certificate;

		/// <summary>
		/// Запрос на выход.
		/// </summary>
		private readonly Atp.Saml2.LogoutRequest logoutRequest;

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="page">Страница, с которой инициирован запрос.</param>
		/// <param name="spCertificate">Сертификат, которым должен быть подписан отправляемый запрос.</param>
		/// <param name="singleLogoutServiceUrl">URL, на который должен быть отправлен запрос выхода.</param>
		/// <param name="nameIdentifier">Идентификатор, желательно тот же, что был получен в результате успешной аутентификации.</param>
		public LogoutRequest(Page page, X509Certificate2 spCertificate, string singleLogoutServiceUrl, string nameIdentifier)
            : this(page, Util.GetAbsoluteUrl(page, "~/"), spCertificate, singleLogoutServiceUrl, nameIdentifier) { }

	    /// <summary>
        /// Конструктор для MVC
	    /// </summary>
	    /// <param name="page">Страница, с которой инициирован запрос.</param>
	    /// <param name="absoluteUrl">URL</param>
	    /// <param name="spCertificate">Сертификат, которым должен быть подписан отправляемый запрос.</param>
	    /// <param name="singleLogoutServiceUrl">URL, на который должен быть отправлен запрос выхода.</param>
	    /// <param name="nameIdentifier">Идентификатор, желательно тот же, что был получен в результате успешной аутентификации.</param>
	    public LogoutRequest(Page page, string absoluteUrl, X509Certificate2 spCertificate, string singleLogoutServiceUrl, string nameIdentifier)
        {
            logoutRequest = new Atp.Saml2.LogoutRequest();
            logoutRequest.Destination = singleLogoutServiceUrl;
            logoutRequest.Issuer = new Issuer(absoluteUrl);
            // Сейчас в logoutRequest.NameId можно передавать что угодно.
            logoutRequest.NameId = new NameId(nameIdentifier);
            logoutRequest.NameId.Format = "urn:oasis:names:tc:SAML:2.0:nameid-format:transient";
            logoutRequest.Reason = "urn:oasis:names:tc:SAML:2.0:logout:user";
            spX509Certificate = spCertificate;
            pageFrom = page;
        }

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <remarks>Используется для обработки запроса на подтверждение отключения.</remarks>
		/// <param name="page">Страница, на которую поступил запрос на подтверждение отключения.</param>
		/// <param name="idpCertificate">Сертификат, которым поступивший запрос должен быть подписан.</param>
		public LogoutRequest(Page page, X509Certificate2 idpCertificate)
		{
			logoutRequest = Atp.Saml2.LogoutRequest.Create(page.Request, idpCertificate.PublicKey.Key);
		}

		/// <summary>
		/// Отправляет запрос на выход.
		/// </summary>
		public void Send()
		{
			logoutRequest.Redirect(pageFrom.Response, logoutRequest.Destination, relayState, spX509Certificate.PrivateKey);	
		}
	}
}
