﻿using bgTeam.IsRui.Domain.Entities.Documents;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects.Documents
{
    /// <summary>
    /// Документ ввода в эксплуатацию ОУ
    /// </summary>
    public class AODocumentCommissioning : AODocumentBase
    {
        internal static readonly DocTypeEnum DocType = DocTypeEnum.CommissioningAccountingObject;    

        protected override DocTypeEnum GetTypeDocument()
        {
            return DocType;
        }

    }
}
