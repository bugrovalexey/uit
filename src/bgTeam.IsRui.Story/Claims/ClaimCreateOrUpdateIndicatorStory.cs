﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimCreateOrUpdateIndicatorStory : IStory<ClaimCreateOrUpdateIndicatorStoryContext, ClaimIndicatorDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapperBase;
        private readonly ISessionNHibernateFactory _factory;

        public ClaimCreateOrUpdateIndicatorStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase,
            ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _repository = repositoryRui;
            _mapperBase = mapperBase;
            _factory = sessionNHibernateFactory;
        }

        public ClaimIndicatorDto Execute(ClaimCreateOrUpdateIndicatorStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ClaimIndicatorDto> ExecuteAsync(ClaimCreateOrUpdateIndicatorStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка: {context.ClaimId}");
            }

            ClaimIndicator indicator;

            if (context.Id.HasValue && context.Id.Value > 0)
            {
                indicator = _repository.Get<ClaimIndicator>(context.Id.Value);
                indicator = _mapperBase.Map(context, indicator);
            }
            else
            {
                indicator = _mapperBase.Map(context, new ClaimIndicator());
            }

            using (var session = _factory.OpenSession())
            {
                await session.SaveOrUpdateAsync(indicator);
                await session.CommitAsync();
            }

            return _mapperBase.Map(indicator, new ClaimIndicatorDto());
        }
    }
}
