﻿namespace GB.DataAccess.Services.Args
{
    public class SiteMapArgs : BaseArgsNew, IServiceArgs
    {
        public int? ParentId { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }
    }
}