﻿using bgTeam.DataAccess;
using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess.Command;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.AccountingObjectsCharacteristics;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.AODecisionsToCreate;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.CharacteristicOfTechnicalSupports;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.Documents;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.InformationInteractions;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.List;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.Softwares;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.SpecialChecks;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.TechnicalSupports;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.UserComments;
using bgTeam.IsRui.DataAccess.Command.AccountingObjects.WorkAndServices;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.IsRui.DataAccess.Services.Args;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.Domain.Entities.Documents;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport;
using bgTeam.IsRui.MKRF.Logic.Tabs;
using System;
using System.Web.Mvc;
using bgTeam.IsRui.DataAccess.Repository;

namespace bgTeam.IsRui.WebApp.Controllers
{
    [FilterException, FilterAction]
    public partial class AccountingObjectsController : ControllerBase
    {
        private AccountingObjectService _service;
        private readonly IUserRepository _userRepository;
        private readonly IStatusRepository _statusRepository;

        public AccountingObjectsController(
            IUserRepository userRepository,
            IStatusRepository statusRepository)
        {
            _userRepository = userRepository;
            _statusRepository = statusRepository;
        }


        public ICommandBuilder _Command { get; set; }


        

        public AccountingObjectService Service
        {
            get
            {
                if (_service == null)
                {
                    _service = new AccountingObjectService(_userRepository, _statusRepository);
                }

                return _service;
            }
            set { _service = value; }
        }

        /// <summary>
        /// Точка входа в раздел
        /// </summary>
        public ActionResult Index()
        {
            var handler = Singleton<TabsManager>.Instance.GetHandler<AccountingObjectsTabs>();

            return RedirectToAction(handler.GetAction());
        }

        /// <summary>
        /// Точка входа в карточку
        /// </summary>
        public ActionResult IndexCard(int id)
        {
            var handler = Singleton<TabsManager>.Instance.GetHandler<AccountingObjectsCardTabs>();

            //return RedirectToAction(handler.GetAction(), new { id });

            return RedirectToAction("CardForRead", new { id });
        }

        #region List

        public ActionResult List()
        {
            return View(new ListViewModel(_userRepository, _statusRepository, "all", null, null));
        }

        [HttpPost]
        public ActionResult ListSort(string mode, int? status, int? asc)
        {
            return Json(new ListViewModel(_userRepository, _statusRepository, mode, status, asc).Items);
        }

        public ActionResult ListApprove()
        {
            return View(new ListApproveViewModel(_userRepository, _statusRepository));
        }

        public ActionResult ListMyApprove()
        {
            return View(new ListMyApproveViewModel(_userRepository, _statusRepository));
        }

        public ActionResult Create()
        {
            return RedirectToAction("CommonDataCreate");
        }

        [HttpPost]
        public ActionResult Create(AccountingObjectArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                Service.Create(args);
            });
        }

        [HttpPost]
        public ActionResult DeleteAccountingObject(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => 
            {
                _Command.Build(new DeleteAccountingObjectObjectContext() { Id = id }).Execute();
            });
        }

        public ActionResult AddInFavorites(AddInFavoritesContext context)
        {
            try
            {
                var res = _Command.Build(context).Return<bool>();

                return new JsonResult()
                {
                    Data = new
                    {
                        success = true,
                        favorite = res,
                        message = res ? "ОУ добавлен в избранное" : "ОУ выведен из избранного"
                    }
                };
            }
            catch(Exception exp)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        success = false,
                        message = string.Format("Произошла ошибка<br/><br/>{0}", exp.Message)
                    }
                };
            }


            return NotificationHelper.NotificationCreateEdit(() => 
            {
                _Command.Build(context).Execute();
            });
        }

        [HttpPost]
        public ActionResult AddOrUpdateComment(AddUserCommentContext context)
        {
            int ID = _Command.Build(context).Return<int>();

            return new JsonResult { Data = new { id = ID } };            
        }

        [HttpPost]
        public ActionResult AddCommentFile(AddDocumentContext context)
        {
            if (!context.xId.HasValue)
            {
                int commentID = new AddUserCommentCommand(_userRepository)
                    .Execute(new AddUserCommentContext { 
                          AccountingObjectId = context.AccountingObjectId, 
                          Comment = string.Empty, 
                          xId = null 
                    });

                context.xId = commentID;
            }

            var file = _Command.Build(context).Return<Files>();

            if (file != null)
            {
                return new JsonResult { Data = new { fileid = file.Id, filename = file.Name, commentid = context.xId } };
            }

            return new JsonResult { Data = new { fileid = "", filename = "", commentid = context.xId } };
        }

        [HttpPost]
        public ActionResult DeleteCommentFile(DeleteDocumentContext context)
        {
            int id = context.xId ?? -1;
            return NotificationHelper.NotificationDelete(id, () => _Command.Build(context).Execute());
        }

        #endregion List

        #region CardForRead


        public ActionResult CardForRead(int id)
        {
            return View(new CardForReadViewModel(id));
        }


        #endregion CardForRead

        #region CommonData

        public ActionResult CommonData(int id)
        {
            return View(new CommonDataViewModel(_statusRepository, id));
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CommonDataEdit(AccountingObjectCommonDataArgs data)
        {
            return NotificationHelper.NotificationCreateEdit(() => { Service.Update(data); });
        }

        public ActionResult AccountingDocEdit(int accountingObjectId, int? id)
        {
            var model = new AODecisionToCreateEditViewModel(accountingObjectId, id);
            return View(model);
        }

        [HttpPost]
        public ActionResult AccountingDocEdit(AddOrSaveAODecisionToCreateContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {                
                var doc = _Command.Build(context).Return<AODecisionToCreate>();
                var m = new TableAODecisionToCreate(doc);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteDocAccountingObject(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => _Command.Build(new DeleteAODecisionToCreateContext() { xId = id }).Execute());
        }

        #endregion CommonData

        #region InformtionInteraction

        public ActionResult InformationInteraction(int? id)
        {
            return View(new InformationInteractionViewModel(id));
        }

        public ActionResult InformationInteractionEdit(int accountingObjectId, int? id)
        {
            var model = new InformationInteractionEditViewModel(accountingObjectId, id);

            return View(model);
        }

        [HttpPost]
        public ActionResult InformationInteractionEdit(/*InformationInteractionArgs args*/AddOrSaveInformationInteractionContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                //var isNew = args.Id == 0;
                var entity = _Command.Build(context).Return<InformationInteraction>();
                    //isNew
                    //? new InformationInteractionService().Create(args)
                    //: new InformationInteractionService().Update(args);

                var m = new TableInformationInteraction(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteInformationInteraction(int id)
        {
            return NotificationHelper.NotificationDelete(id, 
                //() => _Command.Execute(new DeleteInformationInteractionContext() { xId = id })/*new InformationInteractionService().Delete(id)*/
                () => new BaseDeleteCommand<BaseAddOrSaveContext, InformationInteraction>().Execute(new BaseAddOrSaveContext() { xId = id})
                );
        }

        #endregion InformtionInteraction

        #region Characteristics

        public ActionResult Characteristics(int id)
        {
            return View(new CharacteristicsViewModel(id));
        }

        [HttpPost]
        public ActionResult Characteristics(AccountingObjectDataProtectionArgs args)
        {
            return
                NotificationHelper.NotificationCreateEdit(() =>  { Service.Update(args); });
        }

        public ActionResult CharacteristicEdit(int accountingObjectId, int? id)
        {
            var model = new CharacteristicEditViewModel(accountingObjectId, id);

            return View(model);
        }

        [HttpPost]
        public ActionResult CharacteristicEdit(AddOrSaveAccountingObjectCharacteristicsContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var entity = _Command.Build(context).Return<AccountingObjectCharacteristics>();
                var m = new TableCharacteristic(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteCharacteristic(int id)
        {
            return NotificationHelper.NotificationDelete(id, () =>                
                new BaseDeleteCommand<BaseAddOrSaveContext, AccountingObjectCharacteristics>().Execute(new BaseAddOrSaveContext() { xId = id }));
        }

        public ActionResult SpecCheckEdit(int accountingObjectId, int? id)
        {
            var model = new SpecCheckEditViewModel(accountingObjectId, id);
            return View(model);
        }

        [HttpPost]
        public ActionResult SpecCheckEdit(/*SpecialCheckArgs args*/AddOrSaveSpecialCheckContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                //var isNew = args.Id == 0;
                var entity = _Command.Build(context).Return<SpecialCheck>();
                    //isNew
                    //? new SpecCheckService().Create(args)
                    //: new SpecCheckService().Update(args);

                var m = new TableSpecCheck(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteSpecCheck(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => /*new SpecCheckService().Delete(id)*/ _Command.Build(new DeleteSpecialCheckContext() { xId = id }).Execute());
        }

        #endregion Characteristics

        #region Responsibles

        public ActionResult Responsibles(int id)
        {
            return View(new ResponsiblesViewModel(id));
        }

        [HttpPost]
        public ActionResult ResponsiblesEdit(AccountingObjectResponsiblesArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() => { Service.Update(args); });
        }

        #endregion Responsibles

        #region Виды обеспечения

        public ActionResult KindsOfSupport(int id)
        {
            return View(new KindsOfSupportViewModel(id));
        }

        #region TechnicalSupport

        public ActionResult TechnicalSupportEdit(int accountingObjectId, int? id)
        {
            return View(new TechnicalSupportEditViewModel(accountingObjectId, id));
        }

        [HttpPost]
        public ActionResult TechnicalSupportEdit(AddOrSaveTechnicalSupportContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var entity = _Command.Build(context).Return<TechnicalSupport>();

                var m = new TableTechnicalSupport(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteTechnicalSupport(int id)
        {            
            return NotificationHelper.NotificationDelete(id, () => _Command.Build(new DeleteTechnicalSupportContext() { xId = id }).Execute());
        }

        public ActionResult CharacteristicOfTechnicalSupportEdit(int technicalSupportId, int accountingObjectId, int? id)
        {
            return View(new CharacteristicOfTechnicalEditViewModel(technicalSupportId, accountingObjectId, id));
        }

        [HttpPost]
        public ActionResult CharacteristicOfTechnicalSupportEdit(AddOrSaveCharacteristicOfTechnicalSupportContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var m = new TableCharacteristicOfTechnicalSupport(_Command.Build(context).Return< CharacteristicOfTechnicalSupport>());
                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteCharacteristicOfTechnicalSupport(int id)
        {            
            return NotificationHelper.NotificationDelete(id, () => _Command.Build(new DeleteCharacteristicOfTechnicalSupportContext() { xId = id }).Execute());
        }

        #endregion TechnicalSupport

        #region Software

        public ActionResult SoftwareEdit(int accountingObjectId, int? id)
        {
            return View(new SoftwareEditViewModel(accountingObjectId, id));
        }

        [HttpPost]
        public ActionResult SoftwareEdit(AddOrSaveSoftwareContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var entity = _Command.Build(context).Return<Software>();
                var m = new TableSoftware(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteSoftware(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => _Command.Build(new DeleteSoftwareContext() { xId = id }).Execute());
        }

        #endregion Software

        #region WorkAndService

        public ActionResult WorkAndServiceEdit(int accountingObjectId, int? id)
        {
            return View(new WorkAndServiceEditViewModel(accountingObjectId, id));
        }

        [HttpPost]
        public ActionResult WorkAndServiceEdit(AddOrSaveWorkAndServiceContext context)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var entity = _Command.Build(context).Return<WorkAndService>();
                var m = new TableWorkAndService(entity);

                return m;
            });
        }

        [HttpPost]
        public ActionResult DeleteWorkAndService(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => _Command.Build(new DeleteWorkAndServiceContext { xId = id }).Execute());
        }

        #endregion WorkAndService

        #endregion Виды обеспечения

        public ActionResult Activities(int id)
        {
            return View(new ActivitiesViewModel(id));
        }

        public ActionResult CommentList(int id)
        {
            return View(new CommentListViewModel(id));
        }


        public ActionResult StageEdit(int id)
        {
            return View(new StageEditViewModel(id));
        }

        [HttpPost]
        public ActionResult StageChange(int id, int stage)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                _Command.Build(new UpdateStageAccountingObjectContext() { Id = id, StageId = stage }).Execute();
            });
        }
    }
}