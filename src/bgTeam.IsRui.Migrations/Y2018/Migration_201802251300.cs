﻿using FluentMigrator;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201802251301)]
    public class Migration_201802251300 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("BG_Meta_Dictionaries")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("TableName").AsString().NotNullable()
                .WithColumn("ModifyType").AsInt32().NotNullable()
                .WithColumn("Visible").AsBoolean().NotNullable()
                .WithColumn("OrderBy").AsInt32().Nullable();

            Create.Table("BG_Meta_DictionariesColumns")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("DictionaryId").AsInt32().NotNullable()
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("ColumnName").AsString().NotNullable()
                .WithColumn("ColumnType").AsString().NotNullable()
                .WithColumn("RefId").AsInt32().Nullable()
                .WithColumn("RefColumn").AsInt32().Nullable()
                .WithColumn("Visible").AsBoolean().NotNullable()
                .WithColumn("OrderBy").AsInt32().Nullable();

            Create.ForeignKey("FK_Meta_Dictionaries")
               .FromTable("BG_Meta_DictionariesColumns").ForeignColumn("DictionaryId")
               .ToTable("BG_Meta_Dictionaries").PrimaryColumn("Id");
        }
    }
}
