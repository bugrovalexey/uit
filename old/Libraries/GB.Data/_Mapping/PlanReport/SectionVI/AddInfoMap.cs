﻿using Uviri.MKRF.Entities.PlanReport.SectionVI;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVI
{
    public class AddInfoMap : ClassMapBase<AddInfo>
	{
        public AddInfoMap()
            : base("PRS_6v")
		{
//            Table("w_");

//            Id(x => x.Id, "PRS_6v_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string args = @" @PRS_6v_1 = ? 
//							,@PRS_6v_2 = ? 
//							,@PRS_6v_3 = ? 
//							,@PRS_6v_4 = ? 
//							,@PRS_6v_5 = ? 
//							,@PRS_6v_6 = ? 
//							,@PRS_6v_7 = ? 
//							,@Plans_ID = ? 
//							,@PRS_6v_ID = ? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_6v_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_6v_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_6v_Delete
//						  @PRS_6v_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.BU_Integration, "PRS_6v_1");
			Map(x => x.BU_UnitCount, "PRS_6v_2");
			Map(x => x.CTO_UsersCount, "PRS_6v_3");
			Map(x => x.CTO_QualityCtrl, "PRS_6v_4");
			Map(x => x.ED_LawRel, "PRS_6v_5");
			Map(x => x.ED_IsMEDO, "PRS_6v_6");
			Map(x => x.ED_UnitCount, "PRS_6v_7");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
        }
    }
}