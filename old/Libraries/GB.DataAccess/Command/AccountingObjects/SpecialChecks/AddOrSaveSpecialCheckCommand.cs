﻿using GB.Data.AccountingObjects;
using GB.Data.Documents;
using GB.DataAccess.Command.Documents;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.SpecialChecks
{
    public class AddOrSaveSpecialCheckCommand : ICommand<AddOrSaveSpecialCheckContext, SpecialCheck>
    {
        ISessionFactoryGb factory;

        public AddOrSaveSpecialCheckCommand()
        {
            factory = new SessionFactory();
        }

        public SpecialCheck Execute(AddOrSaveSpecialCheckContext context)
        {
            SpecialCheck spec;

            if (context.xId.HasValue && context.xId != 0)
                spec = new Repository<SpecialCheck>().Get(context.xId.Value);
            else
            {
                spec = new SpecialCheck();                
            }

            AutoMapper.Mapper.Map(context, spec);
            
            using (var session = factory.Create())
            {
                session.SaveOrUpdate(spec);
                session.Commit();
            }

            using (var session = factory.Create())
            {
                session.Refresh(spec);
                session.Commit();
            }

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = context.Files,
                listDocs = spec.Documents,
                owner = spec,
                type = DocTypeEnum.SpecialCheck
            });            

            using (var session = factory.Create())
            {
                session.Refresh(spec);
                session.Commit();
            }

            return spec;
        }
    }
}
