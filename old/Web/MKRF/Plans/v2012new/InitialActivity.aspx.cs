﻿using GB.Entity.Directories;
using GB.Helpers;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Controls;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class InitialActivity : System.Web.UI.Page
    {
        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as ActivityCard).CurrentActivity;
            }
        }

        public static string GetUrl(object activityId)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/Plans/v2012new/InitialActivity.aspx"), ActivityCard.ActivityIdKey, activityId);
        }


        protected override void OnPreRender(EventArgs e)
        {
            ActivityGrid.DataBind();

            base.OnPreRender(e);
        }

        #region Grid
        private InitialActivityController controller = new InitialActivityController();
        protected void ActivityGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            (sender as ASPxGridView).DataSource = CurrentActivity.PlansInitialActivity;
        }

        protected void ActivityGrid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }
        
        protected void ActivityGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            controller.Create(new InitialActivityArgs()
            {
                Activity = CurrentActivity,
                Value = hf.ToDictionary(x => x.Key, y => y.Value)
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void ActivityGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            controller.Update(new InitialActivityArgs()
            {
                Id = e.Keys[0],
                Value = hf.ToDictionary(x => x.Key, y => y.Value)
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void ActivityGrid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            controller.Delete(new InitialActivityArgs()
            {
                Id = e.Keys[0],
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

                     
        protected void NumTextBox_DataBinding(object sender, EventArgs e)
        {
            if(controller.CurrentEdit != null)
                (sender as ASPxTextBox).Value = controller.CurrentEdit.RegNum;
        }

        protected void NameTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit != null)
                (sender as ASPxMemo).Value = controller.CurrentEdit.Name;
        }
        
        protected void WorkFormComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = DirectoriesRepositary.GetDirectories<WorkForm>(DateTime.Now.Year).OrderBy(x => x.Code);

            if (controller.CurrentEdit == null || controller.CurrentEdit.WorkForm == null)
            {
                //box.Value = Core.EntityBase.Default;
                box.SelectedIndex = 0;
            }
            else
            {
                box.Value = controller.CurrentEdit.WorkForm.Id;
            }
        }

        protected void ExpenditureItemsComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = DirectoriesRepositary.GetDirectories<ExpenditureItem>(DateTime.Now.Year).OrderBy(x => x.Code);

            if (controller.CurrentEdit == null || controller.CurrentEdit.ExpenditureItem == null)
            {
                box.Value = GB.EntityBase.Default;
            }
            else
            {
                box.Value = controller.CurrentEdit.ExpenditureItem.Id;
            }
        }

        protected void CommentTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit != null)
                (sender as ASPxTextBox).Value = controller.CurrentEdit.Comment;
        }

        protected void SummTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit != null)
                (sender as ASPxTextBox).Value = controller.CurrentEdit.Summ;
        }
        
        protected void ActivityGrid_SummaryDisplayText(object sender, ASPxGridViewSummaryDisplayTextEventArgs e)
        {
            if (e.IsTotalSummary)
            {
                (sender as ASPxGridView).Columns[e.Item.FieldName].FooterCellStyle.ForeColor = System.Drawing.Color.Black;

                switch (e.Item.FieldName)
                {
                    case "Summ":
                        if ((CurrentActivity.PlansActivityVolY0 + CurrentActivity.PlansActivityAddVolY0) < Convert.ToDouble(e.Value))
                            (sender as ASPxGridView).Columns[e.Item.FieldName].FooterCellStyle.ForeColor = System.Drawing.Color.Red;
                        break;
                }
            }
        }
        #endregion

        
    }
}