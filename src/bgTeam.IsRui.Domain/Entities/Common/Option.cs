﻿namespace bgTeam.IsRui.Domain.Entities.Common
{
    /// <summary>Опция системы</summary>
    public class Option : EntityBase
    {
        //// <summary>Наименование</summary>
        public virtual string Name { get; protected set; }

        // <summary>Группа</summary>
        public virtual string GroupName { get; protected set; }

        /// <summary>Тип</summary>
        public virtual OptionsTypeEnum Type { get; protected set; }

        /// <summary>Сериализованное значение</summary>
        public virtual string Value { get; set; }
    }
}