﻿using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Mapping.Admin
{
    internal class EmailQueueMap : ClassMapBase<EmailQueue>
    {
        public EmailQueueMap()
            : base("Email_Queue")
        {
            //Table("w_");

            //Id(x => x.Id, "Email_Queue_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            string sql = @"  @Email_Queue_To = ?
            //                            ,@Email_Queue_Cc = ?
            //                            ,@Email_Queue_Hcc = ?
            //                            ,@Email_Queue_Sender = ?
            //                            ,@Email_Queue_Subj = ?
            //                            ,@Email_Queue_Text = ?
            //                            ,@Email_Queue_Sheduled_DT = ?
            //                            ,@Email_Queue_Send_DT = ?
            //                            ,@Email_Queue_Priority = ?
            //                            ,@Email_Queue_Is_HTML = ?
            //                            ,@Email_Queue_Error_Text = ?
            //                            ,@Email_Queue_Data = ?
            //                            ,@Notification_ID = ?
            //                            ,@Email_Queue_ID = ? ";

            //            SqlInsert(@"exec ZZZ_Prc_Email_Queue_Insert " + sql).Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Email_Queue_Update " + sql).Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Email_Queue_Delete
            //                             @Email_Queue_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.To, "Email_Queue_To").Not.Nullable();
            Map(x => x.CopyTo, "Email_Queue_Cc");
            Map(x => x.HiddenCopyTo, "Email_Queue_Hcc");
            Map(x => x.From, "Email_Queue_Sender");
            Map(x => x.Subject, "Email_Queue_Subj");
            Map(x => x.Text, "Email_Queue_Text").Not.Nullable();
            Map(x => x.SheduledDate, "Email_Queue_Sheduled_DT").Not.Nullable();
            Map(x => x.SendDate, "Email_Queue_Send_DT");
            Map(x => x.Priority, "Email_Queue_Priority").CustomType(typeof(EMailPriority)).Not.Nullable();
            Map(x => x.IsHtml, "Email_Queue_Is_HTML");
            Map(x => x.ErrorCode, "Email_Queue_Error_Text");
            Map(x => x.SysData, "Email_Queue_Data");
            Map(x => x.Attachments, "Email_Queue_Attachments");

            References(x => x.Notification, "Notification_ID");
        }
    }
}