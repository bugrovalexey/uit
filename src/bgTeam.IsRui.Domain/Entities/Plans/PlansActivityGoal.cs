﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    public class PlansActivityGoal : EntityBase
    {
        public PlansActivityGoal()
        {
        }

        public PlansActivityGoal(PlansActivity activity)
        {
            PlansActivity = activity;
        }

        public virtual string Name { get; set; }

        public virtual PlansActivity PlansActivity { get; set; }
    }
}