﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    internal class AccountingObjectCharacteristicsTypeMap : ClassMapHiLo<AccountingObjectCharacteristicsType>
    {
        public AccountingObjectCharacteristicsTypeMap()
            : base("OU_Charact_Type")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Charact_Type_Name");
        }
    }
}