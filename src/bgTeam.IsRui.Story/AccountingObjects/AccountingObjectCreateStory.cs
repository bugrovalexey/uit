﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Threading.Tasks;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using bgTeam.IsRui.Domain.Entities.Workflow;

    public class AccountingObjectCreateStory : IStory<AccountingObjectCreateStoryContext, EntityDefault>
    {
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;
        private readonly IStatusRepository _statusRepository;
        private readonly ISessionNHibernateFactory _factory;

        public AccountingObjectCreateStory(
            IRepositoryRui repository,
            IUserRepository userRepository,
            IStatusRepository statusRepository,
            ISessionNHibernateFactory factory)
        {
            _repository = repository;
            _userRepository = userRepository;
            _statusRepository = statusRepository;
            _factory = factory;
        }

        public EntityDefault Execute(AccountingObjectCreateStoryContext context)
        {
            return this.ExecuteAsync(context).Result;
        }

        public async Task<EntityDefault> ExecuteAsync(AccountingObjectCreateStoryContext context)
        {
            var entity = new AccountingObject()
            {
                ShortName = context.Name
            };

            if (context.kindOfActivityId.HasValue)
            {
                entity.ClassificationCategory = _repository.Get<ClassificationCategory>(context.kindOfActivityId.Value);
            }

            if (context.ArticleId.HasValue)
            {
                entity.Article = _repository.Get<AoArticle>(context.ArticleId.Value);
            }

            //Проставляем значения по умолчанию для Not Nullable полей
            entity.Type = _repository.Get<IKTComponent>(0);
            entity.Kind = _repository.Get<IKTComponent>(context.KindId);
            entity.CreateDate = DateTime.Now;
            entity.Department = _repository.Get<Department>(context.DepartmentId);
            entity.Status = _repository.Get<Status>((int)StatusEnum.AoDraft);
            entity.Stage = AccountingObjectsStageEnum.Planned;

            //entity.ClassificationСategoryfe

            using (var session = _factory.OpenSession())
            {
                await session.SaveOrUpdateAsync(entity);
            }

            return new EntityDefault() { Id = entity.Id };
        }
    }
}
