﻿using bgTeam.IsRui.Domain.Entities;
using System.Threading.Tasks;

namespace bgTeam.IsRui.MKRF.Logic.Status
{
    using OStatus = bgTeam.IsRui.Domain.Entities.Workflow.Status;

    public class WorkflowHandlerResult
    {
        public string ErrorMessage { get; set; }

        public string ConfirmMessage { get; set; }

        public bool Successful { get; set; }
    }

    public interface IWorkflowHandler<TObject>
        where TObject : IWorkflowObject
    {
        Task AfterChange(TObject obj);

        Task<WorkflowHandlerResult> BeforeChange(TObject obj, OStatus status);
    }

    public interface IWorkflowHandler
    {
        bool ReadyForNewStatus(IWorkflowObject @object, bgTeam.IsRui.Domain.Entities.Workflow.Status status, out string errorMessage, out string confirmMessage);

        void ChangeAfter(IWorkflowObject obj);
    }
}