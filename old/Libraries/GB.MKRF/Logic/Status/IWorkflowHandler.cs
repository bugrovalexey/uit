﻿using GB.Data;
using System;

namespace GB.MKRF.Logic.Status
{
    public interface IWorkflowHandler
    {
        bool ReadyForNewStatus(IWorkflowObject @object, Data.Workflow.Status status, out string errorMessage, out string confirmMessage);

        void ChangeAfter(IWorkflowObject obj);
    }
}