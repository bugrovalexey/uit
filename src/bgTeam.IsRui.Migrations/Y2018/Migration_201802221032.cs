﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201802221034)]
    public class Migration_201802221032 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Rename.Table("w_Meta_Dir").To("Meta_Directories");
            Rename.Table("w_Meta_Dir_Column").To("Meta_Directories_Column");

            Delete.Table("Meta_Directories_Column");
            Delete.Table("Meta_Directories");

            Create.Table("Meta_Directories")
                 .WithColumn("ID").AsInt32().PrimaryKey()
                 .WithColumn("Name").AsString()
                 .WithColumn("ViewName").AsString()
                 .WithColumn("FieldUse_InUse").AsBoolean()
                 .WithColumn("Visible").AsBoolean()
                 .WithColumn("Access").AsInt32()
                 .WithColumn("Sort").AsInt32()

                 .WithColumn("ProcedureInsert").AsString(12).Nullable()
                 .WithColumn("ProcedureUpdate").AsString(12).Nullable()
                 .WithColumn("ProcedureDelete").AsString(12).Nullable();

            Create.Table("Meta_Directories_Column")
                 .WithColumn("ID").AsInt32().PrimaryKey()
                 .WithColumn("Dictionary_ID").AsInt32()
                 .WithColumn("Name").AsString()
                 .WithColumn("ColumnName").AsString()
                 .WithColumn("Visible").AsBoolean()
                 .WithColumn("Type").AsString(20)
                 .WithColumn("Sort").AsInt32()

                 .WithColumn("RefDictionary").AsInt32().Nullable()
                 .WithColumn("RefColumn").AsInt32().Nullable();

            Create.ForeignKey("FK_Meta_Directories_Column_Directories").FromTable("Meta_Directories_Column").ForeignColumn("Dictionary_ID").ToTable("Meta_Directories").PrimaryColumn("ID");
        }
    }
}
