﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="Regions.aspx.cs" Inherits="GB.MKRF.Web.Selectors.Regions" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
     <script type="text/javascript">
         function RowSelect() {
             RegionsGrid.GetSelectedFieldValues('Id;Name', OnGetSelectedFieldValues);
         }

         function OnGetSelectedFieldValues(selectedValues) {
             var idList = null;
             var textList = '';

             for (var i = 0; i < selectedValues.length; i++) {

                 idList = idList + selectedValues[i][0] + ';';
                 textList = textList + selectedValues[i][1] + '<br />';
             }

             window.parent.PopupClose(idList, textList);
         }

         var gridLoaded = false;
         function SelectRows(s) {
             if (gridLoaded)
                 return;

             var ids = window.parent.GetIds();
             if (ids) {
                 s.SelectRowsByKey(ids.split(';'));
             }

             gridLoaded = true;
         }


    </script>
    <dxwgv:ASPxGridView ID="RegionsGrid" runat="server" ClientInstanceName="RegionsGrid"
        Width="100%" AutoGenerateColumns="False" KeyFieldName="Id" 
        OnBeforePerformDataSelect="RegionsGrid_BeforePerformDataSelect">
        <ClientSideEvents Init="function(s,e){ SelectRows(s); }" />
        <Columns>
            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Caption=" " />
            <dxwgv:GridViewDataMemoColumn Caption="Регион" FieldName="Name" SortOrder="Ascending" />
        </Columns>
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="17" />
    </dxwgv:ASPxGridView>

    <a href="javascript:RowSelect()" class="command" style="padding-top: 20px; display: inline-block;">Выбрать</a>
</asp:Content>
