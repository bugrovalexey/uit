﻿using GB.Data.Common;
using GB.Data.Plans;

namespace GB._Mapping.Plans
{
    internal class ActivityExpertMap : ClassMapHiLo<ActivityExpert>
    {
        public ActivityExpertMap()
            : base("Activity_Expert") { }

        protected override void InitMap()
        {
            Map(x => x.Role, "Activity_Expert_Role").CustomType(typeof(ActivityExpertEnum));

            References<User>(x => x.Expert, "Users_ID").Not.Nullable();
            References<ActivityExpertiseConclusion>(x => x.Conclusion, "Activity_Expertise_Conclusion_ID").Not.Nullable();
        }
    }
}