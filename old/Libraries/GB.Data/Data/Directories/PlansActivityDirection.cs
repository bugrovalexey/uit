﻿namespace GB.MKRF.Entities.Directories
{
    /// <summary>Направление мероприятия Плана</summary>
    public class PlansActivityDirection : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null
    }
}