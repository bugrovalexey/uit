﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityWorksDto
    {
        public int? Id { get; set; }

        public int? ActivityId { get; set; }

        public string Name { get; set; }

        /*public int? OKPDId { get; set; }

        public int? ExpenseDirectionId { get; set; }

        public int? ExpenseTypeId { get; set; }

        public int? KOSGUId { get; set; }

        public int? OKVEDId { get; set; }

        public YearEnum Year { get; set; }

        public int? SpecialistsCount { get; set; }

        public decimal? Salary { get; set; }*/

        public int? Duration { get; set; }

        public decimal? Summ { get; set; }

        public QuarterEnum? Quarter { get; set; }
    }
}
