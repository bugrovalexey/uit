﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.AccountingObjects;

namespace GB._Mapping.AccountingObjects
{
    class AOGovServiceNPAMap : ClassMapHiLo<AOGovServiceNPA>
    {
        public AOGovServiceNPAMap()
            : base("OU_NPA_Document")
        {
        }

        protected override void InitMap()
        {
            References(x => x.GovService, "OU_Uslugi_ID");
            References(x => x.GovServiceNpa, "NPA_Document_ID");
            //References(x => x.Kind, "NPA_Kind_ID");

            Map(x => x.Name, "OU_NPA_Document_Name");
            Map(x => x.DocumentNumber, "OU_NPA_Document_Number");
            Map(x => x.Date, "OU_NPA_Document_Date");
            Map(x => x.ParticleOfDocument, "OU_NPA_Document_Paragraph");
        }

    }
}
