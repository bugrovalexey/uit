﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimGetMyListStory : IStory<ClaimGetMyListStoryContext, IEnumerable<ClaimDto>>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IMapperBase _mapper;
        private readonly IUserRepository _userRepository;

        public ClaimGetMyListStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase,
            IUserRepository userRepository)
        {
            _repositoryRui = repositoryRui;
            _mapper = mapperBase;
            _userRepository = userRepository;
        }

        public IEnumerable<ClaimDto> Execute(ClaimGetMyListStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ClaimDto>> ExecuteAsync(ClaimGetMyListStoryContext context)
        {
            var curUser = _userRepository.GetCurrent();

            var activities = _repositoryRui
                .GetAllEx<Claim>(
                query => query
                    .JoinQueryOver(x => x.PlansActivity)
                    .JoinQueryOver(q => q.Department)
                    .Where(a => a.Id == curUser.Department.Id))
                .ToList();
            return activities
                .Select(x => _mapper.Map(x, new ClaimDto()));
        }
    }
}
