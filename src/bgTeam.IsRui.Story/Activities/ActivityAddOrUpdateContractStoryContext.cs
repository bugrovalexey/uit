﻿namespace bgTeam.IsRui.Story.Activities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityAddOrUpdateContractStoryContext
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public DateTime? BeginDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? ChangeDate { get; set; }

        public int? ActivityId { get; set; }
    }
}
