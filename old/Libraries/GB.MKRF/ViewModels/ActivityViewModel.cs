﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.ViewModels
{
    public class ActivityLinkViewModel
    {
        public int Id;
        public string Code;
        public string Name;
        public bool IsDeleted;
        public int LinkType; // 1=скопировано в.. 2=объединено из.. 3=получено из.. 4=объединено в..
    }
    public class ActivityViewModel
    {
        public ActivityViewModel()
        {
            ActivityReasons = new List<ReasonViewModel>();
            Goals = new List<GoalViewModel>();
            Functions = new List<FuncViewModel>();
            Indexes = new List<IndexViewModel>();
            Indicators = new List<IndicatorViewModel>();
            ExpenseDirections = new List<ExpenseDirectionsViewModel>();
            Contracts = new List<ContractViewModel>();
            Works = new List<WorkViewModel>();
            WorkDocuments = new List<DocumentViewModel>();
            Specs = new List<SpecViewModel>();
            SpecDocuments = new List<DocumentViewModel>();
            PlansZakupki = new List<ZakupkiViewModel>();
            
        }
        public int Id;
        public int? JoinedTo;
        public int Plan_Year;
        public int Plan_Type;
        public string Code;
        public string Name;
        public string ActivityType2_Name;
        public string IKTObject;
        public string IKTComponentCodeName;
        public string AnnotationData;
        public string ResponsibleInfo;
        public string SourceFunding;

        public string GRBS_Code;
        public string GRBS_Code_Name;
        public string SectionKBK_Code;
        public string SectionKBK_Code_Name;
        public string ExpenseItem_Code;
        public string ExpenseItem_Code_Name;
        public string WorkForm_Code;
        public string WorkForm_Code_Name;
        public string ExpenditureItem_Code;
        public string Subentry;

        public double PlansActivityVolY0;
        public double PlansActivityVolY1;
        public double PlansActivityVolY2;

        public double PlansActivityAddVolY0;
        public double PlansActivityAddVolY1;
        public double PlansActivityAddVolY2;

        public DateTime? PlansActivityFactDate;
        public double PlansActivityFactVolY0;
        public double PlansActivityFactVolY1;
        public double PlansActivityFactVolY2;

        public List<ReasonViewModel> ActivityReasons;
        public List<GoalViewModel> Goals;
        public List<FuncViewModel> Functions;

        public List<IndexViewModel> Indexes;
        public List<IndicatorViewModel> Indicators;

        public List<ExpenseDirectionsViewModel> ExpenseDirections;
        public List<ContractViewModel> Contracts;
        public List<WorkViewModel> Works;
        public List<DocumentViewModel> WorkDocuments;
        public List<SpecViewModel> Specs;
        public List<DocumentViewModel> SpecDocuments;
        public List<ZakupkiViewModel> PlansZakupki;
        public List<ActivityLinkViewModel> Links;
        public List<InitialActivityModel> InitialActivity;
    }

    public class ReasonViewModel
    {
        public ReasonViewModel()
        {
            Documents = new List<DocumentViewModel>();
        }
        public string Name;
        public string NPAType_Name; // reason.NPAType != null ? reason.NPAType.Name : string.Empty
        public string Num;
        public DateTime? Date;
        public string Item;

        public List<DocumentViewModel> Documents;
    }

    public class DocumentViewModel
    {
        public int Id;
        public string FileName;
        public string Name;
        public string Num;
        public DateTime? Date;
    }

    public class GoalViewModel
    {
        public string Name;
    }

    public class FuncViewModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public List<string> Acts { get; set; }
    }

    public class IndexViewModel
    {
        public string Name;
        public string ActivityFunc;
        public string Goal;
        public string PRSFactVal;
        public string UOM;

        public string FirstPRSFactVal;
        public string FirstAddFounding;
        public string FirstUOM;

        public string SecondPRSFactVal;
        public string SecondAddFounding;
        public string SecondUOM;

        public string ThridPRSFactVal;
        public string ThridAddFounding;
        public string ThridUOM;

    }

    public class IndicatorViewModel
    {
        public string Name;
        public string ActivityFunc;
        public string Algorythm;

        public string FirstPRSFactVal;
        public string FirstAddFounding;
        public string FirstUOM;

        public string SecondPRSFactVal;
        public string SecondAddFounding;
        public string SecondUOM;

        public string ThridPRSFactVal;
        public string ThridAddFounding;
        public string ThridUOM;
    }
      
    public class ExpenseDirectionsViewModel
    {
        public string Code;
        public string CodeName;
        public decimal ExpenseVolY0;
        public decimal ExpenseVolY1;
        public decimal ExpenseVolY2;

        public decimal ExpenseAddVolY0;
        public decimal ExpenseAddVolY1;
        public decimal ExpenseAddVolY2;
    }

    public class ContractViewModel
    {
        public ContractViewModel()
        {
            Products = new List<string>();
        }
        public string Number;
        public List<string> Products;
        public DateTime? Sign_Date;
        public string DescriptionPreviousResult;
    }

    public class WorkViewModel
    {
        public WorkViewModel()
        {
            WorkTypes = new List<WorkTypeViewModel>();
        }
        public string Name;
        public string ActivityWorkCode;
        public string OKDP_CodeName;
        public string ExpenseDirection_CodeName;
        public string ExpeditureItem;
        public decimal ExpenseVolumeYear0;
        public List<WorkTypeViewModel> WorkTypes;
    }

    public class WorkTypeViewModel
    {
        public string ExpenseType_Name;
        public decimal ExpenseVolumeYear0;
    }

    public class SpecViewModel
    {
        public SpecViewModel()
        {
            SpecTypes = new List<SpecTypeViewModel>();
        }
        public string Name;
        public string ActivitySpecCode;
        public string OKDP_Text;
        public string QT;
        public string Quantity;
        public string Cost;
        public string ExpenseDirection_CodeName;
        public string ExpName;
        public string Analog;
        public string AnalogPrice;
        public string ExpeditureItem;
        public List<SpecTypeViewModel> SpecTypes;
    }

    public class SpecTypeViewModel
    {
        public string CharName;
        public string CharUnit;
        public string CharValue;
    }

    public class ZakupkiViewModel
    {
        public ZakupkiViewModel()
        {
            Lots = new List<LotViewModel>();
        }
        public string NumberName;
        public int Number;
        public string Name;
        public DateTime? PublishDate;
        public string OrderWay_Name;
        public string Status_Name;
        public string IzvNumb;
        public DateTime? IzvDate;
        public DateTime? OpenDate;
        public DateTime? ConsDate;
        public DateTime? ItogDate;
        public List<LotViewModel> Lots;
    }

    public class LotViewModel
    {
        public int? Number;
        public string Code; 
        public string Name; 
        public string Unit; 
        public int? Count;
    }

    public class InitialActivityModel
    {
        public string RegNum { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public string Summ { get; set; }

        public string WorkForm { get; set; }
        public string ExpenditureItem { get; set; }
    }
}
