﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807092248)]
    public class Migration_201807092248 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Information_Exchange")
                .AddColumn("PlansActivity_ID").AsInt32().Nullable();

            Alter.Table("Information_Exchange").AlterColumn("OU_ID").AsInt32().Nullable();
        }
    }
}
