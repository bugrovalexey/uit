﻿namespace bgTeam.IsRui.Story.Common
{
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.Domain;
    using bgTeam.IsRui.Domain.Entities;

    public class WorkflowCreateStoryContext
    {
        public IEntityWorkflow Entity { get; set; }

        public ISessionNHibernate Session { get; set; }
    }
}