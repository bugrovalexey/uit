﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using GB.Controls;
using GB.Handlers;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Entities.Statuses;
using GB.MKRF.Helpers;
using GB.MKRF.Report;
using GB.MKRF.Repository;
using GB.MKRF.Validation;
using GB.MKRF.Web.Common;
using GB.MKRF.Web.Services.Coord;
using GB.Repository;
using DevExpress.Utils;
using DevExpress.Web.ASPxCallback;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Web.Services.Registry;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class ActivityList : System.Web.UI.Page
    {
        PlansActivityController controller = new PlansActivityController();
        public static string ActivityFromKey = "to";
        private static string key = "Plan";

        public static string GetUrl(int planId)
        {
            return UrlHelper.Resolve("~/Plans/v2012new/ActivityList.aspx") + "?" + key + "=" + planId;
        }

        protected Plan CurrentPlan
        {
            get 
            {
                return (Master as PlanCard).CurrentPlan;
            }
        }

        protected bool ExportEnabled
        {
            get
            {
                return true;
                //                return Convert.ToBoolean(RepositoryBase<Option>.Get((int)OptionsEnum.ExportImportXmlEnabled).Value);
            }
        }

        protected bool ImportEnabled
        {
            get
            {
                return Validation.Validator.Validate(new PlansActivity(CurrentPlan), ObjActionEnum.create);
                //                return Convert.ToBoolean(RepositoryBase<Option>.Get((int)OptionsEnum.ExportImportXmlEnabled).Value) &&
                //                       Validator.Validator.Validate(new PlansActivity(CurrentPlan), ObjActionEnum.create);
            }
        }

        protected bool UpdateFromUviriEnabled
        {
            get
            {
                var user = UserRepository.GetCurrent();
                return Convert.ToBoolean(RepositoryBase<Option>.Get((int)OptionsEnum.UviriIntegrationEnabled).Value) &&
                    user != null && user.Role.IsEq(RoleEnum.CA) && CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new;
            }
        }

        protected bool UpdateUviriEnabled
        {
            get
            {
                var user = UserRepository.GetCurrent();
                return Convert.ToBoolean(RepositoryBase<Option>.Get((int)OptionsEnum.UviriIntegrationEnabled).Value) && 
                    user != null && user.Role.IsEq(RoleEnum.CA) && CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new;
            }
        }

        private bool IsUnallocated
        {
            get
            {
                return hfUnallocated.Contains("unallocated") &&
                       (bool) hfUnallocated["unallocated"];
            }
        }

        protected bool AllowJoining
        {
            get
            {
                return false;
                //var user = UserRepository.GetCurrent();
                //return (user != null && user.Role != null && !user.Role.IsEq(RoleEnum.MKRF));
            }
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            var user = UserRepository.GetCurrent();
            ShowObjectsRepository.SaveVisitedObjects(CurrentPlan.Id, ObjectsType.Plan, user.Id);
            
            listGrid.DataBind();
            summaryGrid.DataBind();
            
            // может создавать мероприятия
            bool enableAdd = Validation.Validator.Validate(new PlansActivity(CurrentPlan), ObjActionEnum.create);
            
            addButton.Visible = enableAdd;
            
            var departmentColumn = listGrid.Columns["DepartmentNameColumn"];
            departmentColumn.ShowInCustomizationForm =
            departmentColumn.Visible = (CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new);

            //var zakCountColumn = listGrid.Columns["ZakCount"];
            //if (CurrentPlan.PlanType != ExpObjectTypeEnum.Plan2012rel)
            //{
            //    zakCountColumn.Visible = false;
            //    zakCountColumn.ShowInCustomizationForm = false;
            //}
            //zakCountColumn.ShowInCustomizationForm = CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012rel;

            listGrid_NavigateToPage(Request["to"]);

            //styleBtnSplit.ClientVisible = ((DataTable)listGrid.DataSource).Rows.Count > 0 ? true : false;
            UnallocatedBtn.Visible = CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new;
            UnallocatedBtn.Checked = IsUnallocated;
            if (UnallocatedBtn.Visible)
            {
                var unallocatedCount =
                    new PlanControllerOld().GetUnallocatedActivityList(CurrentPlan.Year.Value, user.Id).Rows.Count;
                UnallocatedBtn.Text = unallocatedCount > 0 ? unallocatedCount.ToString() : string.Empty;
            }

            listGrid.Columns["PlansActivityVolY0"].Caption = string.Concat(CurrentPlan.Year.Value, " г.");
            listGrid.Columns["PlansActivityVolY1"].Caption = string.Concat(CurrentPlan.Year.Value + 1, " г.");
            listGrid.Columns["PlansActivityVolY2"].Caption = string.Concat(CurrentPlan.Year.Value + 2, " г.");

            listGrid.Columns["PlansActivityAddVolY0"].Caption = string.Concat(CurrentPlan.Year.Value, " г.");
            listGrid.Columns["PlansActivityAddVolY1"].Caption = string.Concat(CurrentPlan.Year.Value + 1, " г.");
            listGrid.Columns["PlansActivityAddVolY2"].Caption = string.Concat(CurrentPlan.Year.Value + 2, " г.");

            listGrid.Columns["PlansActivityFactVolY0"].Caption = string.Concat(CurrentPlan.Year.Value, " г.");
            listGrid.Columns["PlansActivityFactVolY1"].Caption = string.Concat(CurrentPlan.Year.Value - 1, " г.");
            listGrid.Columns["PlansActivityFactVolY2"].Caption = string.Concat(CurrentPlan.Year.Value - 2, " г.");

            listGrid.Columns["VolY0"].Caption = string.Concat(CurrentPlan.Year.Value, " г.");
            listGrid.Columns["VolY1"].Caption = string.Concat(CurrentPlan.Year.Value + 1, " г.");
            listGrid.Columns["VolY2"].Caption = string.Concat(CurrentPlan.Year.Value + 2, " г.");

            summaryGrid.Columns["Y0"].Caption = string.Concat("на ", CurrentPlan.Year.Value, " г.");
            summaryGrid.Columns["Y1"].Caption = string.Concat("на ", CurrentPlan.Year.Value + 1, " г.");
            summaryGrid.Columns["Y2"].Caption = string.Concat("на ", CurrentPlan.Year.Value + 2, " г.");

            importUploader.FileItemTemplate = "<a href=\"" + FileHandler.GetUrl("") + "{id}\">{name}</a>";

            Lim242Panel.DataBind();

            ItemsPerPage.Value = UserRepository.GetOption<int?>(UserRepository.opt_ActivitiesPerPage, CurrentPlan.Id) ?? (int?)10;

            base.OnPreRender(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _GridColumnManager.Grid = listGrid;
        }

        private void listGrid_NavigateToPage(string to)
        {
            if (string.IsNullOrEmpty(to))
                return;
            listGrid.PageIndex = Convert.ToInt32(to);

            var master = (Master as PlanCard);
            if (master != null) 
                master.TabControl.ActiveTab = master.TabControl.Tabs.FindByName("Activity2012new");
        }

        protected string GetFillingStatus(object status)
        {
            string className = string.Empty;
            switch (status.ToString())
            {
                case "Мероприятие блокирует перевод статуса":
                    className = "lock";
                    break;
                default:
                    return string.Empty;
            }

            return String.Format("<span class='{0} battery' title='{1}'>&nbsp;</span>", className, status);
        }

        protected string GetFavStatus(bool isFavor, int activityID)
        {
            return String.Format("<span class='checkFavor {0}' title='{1}' data-id={2}>&nbsp;</span>", isFavor ? "favorite" : "unfavorite", isFavor ? "Отмечено как избранное" : string.Empty, activityID);
        }


        //private static DataTable DataTableCustomFilter(DataTable dt, string filterExpression)
        //{
        //    var rows = dt.Select(filterExpression);
        //    if (rows.Any())
        //    {
        //        return rows.CopyToDataTable();
        //    }
        //    else
        //    {
        //        dt.Rows.Clear();
        //        return dt;
        //    }
        //}

        protected void listGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            bool? isPriorityFilter = null;
            
            //if (hfFilterMode.Contains("filterMode"))
            //{
            //    switch(hfFilterMode["filterMode"].ToString())
            //    {
            //        case "priority":    isPriorityFilter = true;    break;
            //        case "notpriority": isPriorityFilter = false;   break;
            //    }
            //}
           
            //listGrid.DataSource = filterMode == "priority"
            //                                ? DataTableCustomFilter(GetListGridData(), "IsNotPriority = 0")
            //                                : filterMode == "notpriority"
            //                                      ? DataTableCustomFilter(GetListGridData(), "IsNotPriority = 1")
            //                                      : GetListGridData();

            //if (ChBFavourites.Checked)
            //{
            //    listGrid.DataSource = DataTableCustomFilter((DataTable)listGrid.DataSource, "isFavor = 1");
            //}

            listGrid.DataSource = controller.GetActivityList(CurrentPlan,
                                                             IsUnallocated,
                                                             isPriorityFilter,
                                                             ChBFavourites.Checked);



            //if(listGrid.GroupCount == 0)
            //    (listGrid.Columns["IsNotPriority"] as GridViewDataCheckColumn).GroupIndex = 0;

            //(listGrid.Columns["IsNotPriority"] as GridViewDataCheckColumn).GroupIndex = 0;
            //listGrid.ExpandAll();
        }

        protected void listGrid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void listGrid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var args = new PlansActivityArgs()
            {
                СurrentPlan = CurrentPlan,
                Name = hf["Name"],
                IsPriority = hf["IsPriority"],
                GRBS = 24635, //054 Министерство культуры Российской Федерации
                SectionKBK = 21772, //0801 Культура
                WorkForm = 44451, //242 Закупка товаров, работ, услуг в сфере информационно-коммуникационных технологий
                CSR = 1066348, //11 5 9999 Реализация федеральной целевой программы "Культура России (2012 - 2018 годы)" государственной программы Российской Федерации "Развитие культуры и туризма"
            };
            controller.Create(args);
            //preActivityListInternal = null;
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void listGrid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var args = new PlansActivityArgs()
            {
                Id = (int)e.Keys[0],
                Name = hf["Name"],
                IsPriority = hf["IsPriority"],
            };
            controller.Update(args);
            //preActivityListInternal = null;
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void listGrid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            var args = new PlansActivityArgs()
            {
                Id = (int)e.Keys[0],
            };
            controller.Delete(args);
            //preActivityListInternal = null;
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void listGrid_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            switch (e.ButtonID)
            {
                case "upButton":
                case "downButton":
                    var argsMove = new PlansActivityArgs()
                    {
                        Id = (int)listGrid.GetRowValues(e.VisibleIndex, "Id"),
                        IsMoveUp = e.ButtonID == "upButton"
                    };
                    controller.Move(argsMove);
                    listGrid.DataBind();
                    break;

                case "copyButton":
                    var argsCopy = new PlansActivityArgs()
                    {
                        Id = (int)listGrid.GetRowValues(e.VisibleIndex, "Id"),
                    };
                    controller.Copy(argsCopy);
                    listGrid.DataBind();
                    break;
            }
        }

        protected void chk_Init(object sender, EventArgs e)
        {
            ASPxCheckBox chk = sender as ASPxCheckBox;

            if (chk == null)
                return;

            bool allowedState = StatusExtension.IsEq(StatusEnum.SendToMKRF, CurrentPlan.Demand.Status) ||
                                StatusExtension.IsEq(StatusEnum.MKRFPreAgreed, CurrentPlan.Demand.Status);
                        
            
            var currentRole = UserRepository.GetRole();
            chk.Enabled = allowedState && currentRole.IsEq(RoleEnum.CA);
            if (!chk.Enabled)
            {
                chk.CheckedImage.Url = "/Content/GB/Styles/Imgs/ok_16.png";
                chk.CheckedImage.ToolTip ="Согласовано";
                chk.CheckedImage.Width = 16;
                chk.CheckedImage.Height = 16;
                chk.UncheckedImage.Url = "/Content/GB/Styles/Imgs/delete2_16.png";
                chk.UncheckedImage.ToolTip = "Не согласовано";
                chk.UncheckedImage.Width = 16;
                chk.UncheckedImage.Height = 16;
                return;
            }

            GridViewDataItemTemplateContainer container = chk.NamingContainer as GridViewDataItemTemplateContainer;


            chk.ClientSideEvents.CheckedChanged = String.Format("function (s, e) {{ ActivityDetailsCallbackPanel.PerformCallback('agree;'+{0}); }}", container.KeyValue);
        }
        
        protected void NameMemo_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            ASPxMemo box = (ASPxMemo)sender;

            box.Text = controller.CurrentEdit.Name;
        }

        protected void IsPriorityCheckBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            ASPxCheckBox box = (ASPxCheckBox)sender;

            box.Value = controller.CurrentEdit.IsPriority;
        }
        
        protected void listGrid_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Edit || e.ButtonType == ColumnCommandButtonType.Delete)
                e.Visible = Validation.Validator.Validate(new PlansActivity(CurrentPlan), ObjActionEnum.create);
        }
        
        protected void listGrid_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {
            if (e.CellType == GridViewTableCommandCellType.Filter)
                return;

            var priorCount = 0;
            var priorActivities = CurrentPlan.Activities.Where(i => i.IsPriority);
            if (priorActivities.Any())
                priorCount = priorActivities.Count();

            int rowIndex = Int32.Parse(listGrid.GetRowValues(e.VisibleIndex, "Num").ToString());
            switch (e.ButtonID)
            {
                case "upButton":
                    if (rowIndex == 1 || rowIndex == (1 + priorCount))
                        e.Visible = DefaultBoolean.False;
                    else
                        e.Visible = DefaultBoolean.True;
                    break;

                case "downButton":
                    if (rowIndex == CurrentPlan.Activities.Count || rowIndex == priorCount)
                        e.Visible = DefaultBoolean.False;
                    else
                        e.Visible = DefaultBoolean.True;
                    break;
            }

            ASPxGridView grid = (ASPxGridView)sender;
            int? composite = null;

            composite = (int?)grid.GetRowValues(e.VisibleIndex, "Composite");

            if (e.CellType == GridViewTableCommandCellType.Data)
            {
                e.Visible = (DevExpress.Utils.DefaultBoolean)Convert.ToInt16(!(composite.HasValue && (composite.Value & (int)CompositeEnum.Edit) != 0));
            }
        }

        protected void listGrid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "VolY0")
                CheckVolAndED(sender, e, "Y0");
            
            if (e.DataColumn.FieldName == "VolY1")
                CheckVolAndED(sender, e, "Y1");

            if (e.DataColumn.FieldName == "VolY2")
                CheckVolAndED(sender, e, "Y2");
        }

        private void CheckVolAndED(object sender, ASPxGridViewTableDataCellEventArgs e, string key)
        {
            double vol = ToDouble((sender as ASPxGridView).GetRowValues(e.VisibleIndex, "Vol" + key));
            double ed = ToDouble((sender as ASPxGridView).GetRowValues(e.VisibleIndex, "ED_" + key));

            if (vol < ed)
                e.Cell.ForeColor = System.Drawing.Color.Red;
        }

        double ToDouble(object value)
        {
            if(value == null || Convert.IsDBNull(value))
                return 0;

            return Convert.ToDouble(value);
        }

        protected string getLinkToActivityCard(object Id, string Name, string IsShow, int? Composite)
        {
            if (Composite.HasValue && (Composite.Value & (int)CompositeEnum.Edit) != 0)
            {
                return string.Format("<a class='gridLink' id='pan_{3}' {0} href='{1}'>{2}</a>", IsShow.Equals("False") ? " style='font-weight: bold' " : string.Empty, CommonData.GetUrl(Id), Name, Id);
            }
            else
            {
                return string.Format("<a class='gridLink' id='pan_{3}' {3} href='ActivityReadOnly.aspx?ActivityId={0}&from={1}'>{2}</a>", Id, this.listGrid.PageIndex, Name, IsShow.Equals("False") ? " style='font-weight: bold' " : string.Empty);
            }
        }


        protected void summaryGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            summaryGrid.DataSource = controller.GetTableSummary(CurrentPlan);
        }

        protected string GetHtmlVersionUrl()
        {
            return UrlHelper.Resolve(string.Format("~/Plans/PlanHtmlVersion.aspx?{0}={1}", PlanHtmlVersion.PlanIdKey, CurrentPlan.Id));
        }

        public static string getViewLinks(Plan plan)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (plan != null)
            {
                int Id = plan.Id;
                sb.Append("<br />");

                sb.Append(ReportPage.GetLink("P 1.1", ReportType.Plan2012NewSectionOne_1, Id)); sb.Append(" ");
                sb.Append(ReportPage.GetLink("P 1.2", ReportType.Plan2012NewSectionOne_2, Id)); sb.Append(" ");
                sb.Append(ReportPage.GetLink("P 1.3", ReportType.Plan2012NewSectionOne_3, Id)); sb.Append(" ");
                sb.Append(ReportPage.GetLink("P 1.4", ReportType.Plan2012NewSectionOne_4, Id)); sb.Append(" ");

                sb.Append("<br />");

                sb.Append(ReportPage.GetLink("P 2.1", ReportType.Plan2012NewSectionTwo_1, Id)); sb.Append(" ");
                sb.Append(ReportPage.GetLink("P 2.2", ReportType.Plan2012NewSectionTwo_2, Id)); sb.Append(" ");
                sb.Append(ReportPage.GetLink("P 2.3", ReportType.Plan2012NewSectionTwo_3, Id)); sb.Append(" ");
                sb.Append(ReportPage.GetLink("P 2.4", ReportType.Plan2012NewSectionTwo_4, Id)); sb.Append(" ");

                sb.Append("<br />");

                sb.Append(ReportPage.GetLink("P 3.1", ReportType.Plan2012NewSectionThree_1, Id)); sb.Append(" ");
                sb.Append(ReportPage.GetLink("P 3.2", ReportType.Plan2012NewSectionThree_2, Id)); sb.Append(" ");
                sb.Append(ReportPage.GetLink("P 3.3", ReportType.Plan2012NewSectionThree_3, Id)); sb.Append(" ");

                sb.Append("<br />");

                sb.Append(ReportPage.GetLink("P 4.1 P 4.2", ReportType.Plan2012NewSectionFour, Id)); sb.Append(" ");
                sb.Append(ReportPage.GetLink("P 4.3", ReportType.Plan2012NewSectionFour_3, Id)); sb.Append(" ");
                sb.Append(ReportPage.GetLink("P 4.4", ReportType.Plan2012NewSectionFour_4, Id)); sb.Append(" ");

                //if (plan.PlanType == ExpObjectTypeEnum.Plan2012rel)
                //{
                //    sb.Append("<br />");

                //    sb.Append(ReportPage.GetLink("Закупки", ReportType.Plan2012NewPurchases, Id)); sb.Append(" ");
                //}
            }

            return sb.ToString();
        }

        protected void ActivityDetailsCallbackPanel_OnCallback(object sender, CallbackEventArgsBase e)
        {
            PlansActivity act = null;

            if (e.Parameter.StartsWith("agree"))
            {
                var args = e.Parameter.Split(';');
                act = RepositoryBase<PlansActivity>.Get(Int32.Parse(args[1]));
                act.IsAgree = !act.IsAgree;
                RepositoryBase<PlansActivity>.SaveOrUpdate(act);
                return;
            }

            if (e.Parameter.StartsWith("favor"))
            {
                var args = e.Parameter.Split(';');
                act = RepositoryBase<PlansActivity>.Get(Int32.Parse(args[1]));
                User currentUser = UserRepository.GetCurrent();
                NewFavoriteValue(currentUser, act);
                return;
            }

            act = RepositoryBase<PlansActivity>.Get(Convert.ToInt32(e.Parameter));
            var control = (ActivityReadMode)Page.LoadControl("~/Common/ActivityReadMode.ascx");
            control.CurrentId = e.Parameter;
            control.EnableEdit = false;
            control.DefaultCollapsed = true;
            control.DefaultShowEditLinks = true;
            control.DefaultShowCollapseLink = true;
            control.SetViewModel(controller.GetActivityViewModel(act));
            var sb = new StringBuilder();
            var tw = new StringWriter(sb);
            var hw = new HtmlTextWriter(tw);
            

            var spanBlock = new HtmlGenericControl("span");
            spanBlock.Attributes.Add("class", "spanPrevNextButtons");
            spanBlock.InnerHtml = string.Format(@"<span class='spanPrevButton' title='Следующее мероприятие'></span>");
            spanBlock.InnerHtml += string.Format(@"<span class='spanNextButton' title='Предыдущее мероприятие'></span>");
            

            var title = new HtmlGenericControl("div");
            title.InnerHtml = string.Format(@"<span style='font-weight: bold; font-family: cuprum; font-size: 1.5em;'>{0}</span>",
                                            act.Name);
            title.Style.Add("margin", "15px 0px");

            var panel = sender as ASPxCallbackPanel;
            panel.Controls.Add(spanBlock);
            panel.Controls.Add(title);
            panel.Controls.Add(control);
            panel.RenderControl(hw);
        }
       
        public bool NewFavoriteValue(User user, PlansActivity act)
        {
            var favoriteActivity = RepositoryBase<FavoriteActivity>.GetAll(new GetAllArgs<FavoriteActivity>()
            {
                Expression = (x => (x.User == UserRepository.GetCurrent() && x.PlansActivity == act))
            }).SingleOrDefault();

            if (favoriteActivity == null)
            {
                FavoriteActivity f = new FavoriteActivity();
                f.PlansActivity = act;
                f.User = user;
                f.IsFavorite = true;
                RepositoryBase<FavoriteActivity>.SaveOrUpdate(f);
                favoriteActivity = f;
                return true;
            }
            else
            {
                favoriteActivity.IsFavorite = !favoriteActivity.IsFavorite;
                RepositoryBase<FavoriteActivity>.SaveOrUpdate(favoriteActivity);
                return favoriteActivity.IsFavorite;
            }
        }
        
        protected string ExportPlan()
        {
            return UrlHelper.Resolve(string.Format("~/Plans/ExportPlan.aspx?{0}={1}", PlanHtmlVersion.PlanIdKey, CurrentPlan.Id));
        }

        protected object ImportPlan()
        {
            return UrlHelper.Resolve(string.Format("~/Plans/ImportPlan.aspx?{0}={1}", PlanHtmlVersion.PlanIdKey, CurrentPlan.Id));
        }

        protected void importUploader_SaveFile(object sender, FileUploadEventArgs e)
        {
            FileHelper.SaveInTemp(e.FileContent, e.Id.ToString());
        }

        protected void importUploader_FileUploadComplete(object sender, FileUploadEventArgs e)
        {
            XmlHelper.ImportPlanFromXml(CurrentPlan, e.FilePath);
        }

        protected void listGrid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;

            var parameters = e.Parameters.Split(':');

            int index = 0;
            if (parameters.Length < 2 || !int.TryParse(parameters[1], out index))
                return;

            switch (parameters[0])
            {
                case "GroupFields":
                    {
                        grid.BeginUpdate();
                        try
                        {
                            grid.ClearSort();
                            switch (index)
                            {
                                case 0:
                                    grid.GroupBy(grid.Columns["IsNotPriority"]);
                                    break;
                                case 1:
                                    grid.GroupBy(grid.Columns["Funding"]);
                                    break;
                            }
                        }
                        finally
                        {
                            grid.EndUpdate();
                        }
                        grid.ExpandAll();
                    }
                    break;

                case "ItemsPerPage":
                    {
                        configurePager(grid, index);
                        grid.DataBind();
                        UserRepository.SetOption(UserRepository.opt_ActivitiesPerPage, index, CurrentPlan.Id);
                        UserRepository.SaveOrUpdate(UserRepository.GetCurrent());                        
                    }
                    break;

            }
        }

        private int GetGroupCountOnPage(ASPxGridView grid, int pageSize, int howManyRowsBefore = 0)
        {
            var groupRowCount = grid.GetChildRowCount(howManyRowsBefore);
            if (groupRowCount < pageSize - howManyRowsBefore - 1)
            {
                return 1 + GetGroupCountOnPage(grid, pageSize, groupRowCount + howManyRowsBefore + 1);
            }
            else
            {
                return 1;
            }
        }

        protected void listGrid_Init(object sender, EventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            var rowsPerPage = UserRepository.GetOption<int?>(UserRepository.opt_ActivitiesPerPage, CurrentPlan.Id);

            configurePager(grid, rowsPerPage);
        }

        private void configurePager(ASPxGridView grid, int? rowsPerPage)
        {
            if (!rowsPerPage.HasValue)
            {
                rowsPerPage = 10;
            }
            
            if (rowsPerPage == 0)
            {
                grid.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
            }
            else
            {
                grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
                grid.SettingsPager.PageSize = rowsPerPage.Value + GetGroupCountOnPage(grid, rowsPerPage.Value);
            }
        }

        protected void Lim242Panel_Callback(object sender, CallbackEventArgsBase e)
        {
            Lim242Panel.DataBind();
        }

        protected string getExpense242Labels()
        {
            StringBuilder htmlBuilder = new StringBuilder();
            IList<LimitMoney> type242Expense = RepositoryBase<LimitMoney>.GetAll(x => x.Department.Id == CurrentPlan.Department.Id);

            if (type242Expense != null &&
                ((CurrentPlan.PlanType == ExpObjectTypeEnum.Plan2012new) ||
                (CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new))
                && CurrentPlan.Year.Value > 2012)
            {
                //expense242TitleLabel.Visible = true;
                htmlBuilder.Append("<p><b>Лимит финансирования по 242 виду расходов для департамента согласно бюджету</b></p>");

                double sumY0 = 0;
                double sumY1 = 0;
                double sumY2 = 0;

                if (CurrentPlan.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new)
                {
                    bool? isPriorityFilter = null;
                    var table = controller.GetActivityList(CurrentPlan,
                                                             IsUnallocated,
                                                             isPriorityFilter,
                                                             ChBFavourites.Checked);//GetActivityList(true);

                    foreach (DataRow row in table.Rows)
                    {
                        sumY0 += Convert.ToDouble(row["PlansActivityVolY0"]) + Convert.ToDouble(row["PlansActivityAddVolY0"]);
                        sumY1 += Convert.ToDouble(row["PlansActivityVolY1"]) + Convert.ToDouble(row["PlansActivityAddVolY1"]);
                        sumY2 += Convert.ToDouble(row["PlansActivityVolY2"]) + Convert.ToDouble(row["PlansActivityAddVolY2"]);
                    }
                }
                else
                {
                    foreach (PlansActivity act in CurrentPlan.Activities)
                    {
                        sumY0 += getSummYear(act.PlansActivityVolY0, act.PlansActivityAddVolY0);
                        sumY1 += getSummYear(act.PlansActivityVolY1, act.PlansActivityAddVolY1);
                        sumY2 += getSummYear(act.PlansActivityVolY2, act.PlansActivityAddVolY2);
                    }
                }

                var limY0 = type242Expense.Where(x => x.Year.Id == CurrentPlan.Year.Id).Select(x => x.Money).FirstOrDefault();
                var limY1 = type242Expense.Where(x => x.Year.Id == CurrentPlan.Year.Id + 1).Select(x => x.Money).FirstOrDefault();
                var limY2 = type242Expense.Where(x => x.Year.Id == CurrentPlan.Year.Id + 2).Select(x => x.Money).FirstOrDefault();
                int currentYear = DateTime.Now.Year;

                if (limY0 > 0)
                {
                    htmlBuilder.Append(GetLimitInfo(type242Expense.Where(x => x.Year.Id == CurrentPlan.Year.Id).Select(x => x.Money).FirstOrDefault(),
                                                    sumY0,
                                                    CurrentPlan.Year.Id));
                }

                if (limY1 > 0)
                {
                    htmlBuilder.Append(GetLimitInfo(type242Expense.Where(x => x.Year.Id == CurrentPlan.Year.Id + 1).Select(x => x.Money).FirstOrDefault(),
                                                    sumY1,
                                                    CurrentPlan.Year.Id + 1));
                }

                if (limY2 > 0)
                {
                    htmlBuilder.Append(GetLimitInfo(type242Expense.Where(x => x.Year.Id == CurrentPlan.Year.Id + 2).Select(x => x.Money).FirstOrDefault(),
                                                    sumY2,
                                                    CurrentPlan.Year.Id + 2));
                }

                htmlBuilder.Append("</p>");

                //////if (CurrentPlan.Year.Year == 2013)
                //////{
                //////    //////expense242TitleLabel.InnerHtml += GetLimitInfo(type242Expense.Y2013, sumY0, 2013);
                //////    //////expense242TitleLabel.InnerHtml += GetLimitInfo(type242Expense.Y2014, sumY1, 2014);
                //////    //////expense242TitleLabel.InnerHtml += GetLimitInfo(type242Expense.Y2015, sumY2, 2015);
                //////}
                //////if (CurrentPlan.Year.Year == 2014)
                //////{
                //////    //////expense242TitleLabel.InnerHtml += GetLimitInfo(type242Expense.Y2014, sumY0, 2014);
                //////    //////expense242TitleLabel.InnerHtml += GetLimitInfo(type242Expense.Y2015, sumY1, 2015);
                //////    //////expense242TitleLabel.InnerHtml += GetLimitInfo(type242Expense.Y2016, sumY2, 2016);
                //////}
                //////if (CurrentPlan.Year.Year == 2015)
                //////{
                //////    //////expense242TitleLabel.InnerHtml += GetLimitInfo(type242Expense.Y2015, sumY0, 2015);
                //////    //////expense242TitleLabel.InnerHtml += GetLimitInfo(type242Expense.Y2016, sumY1, 2016);
                //////}
                //////if (CurrentPlan.Year.Year == 2016)
                //////{
                //////    //////expense242TitleLabel.InnerHtml += GetLimitInfo(type242Expense.Y2016, sumY0, 2016);
                //////}
            }
            //else
            //{
            //    expense242TitleLabel.Visible = false;
            //}
            return htmlBuilder.ToString();
        }

        private double getSummYear(double? y, double? add)
        {
            var v = y + add;

            return v.HasValue ? v.Value : 0;
        }

        private string GetLimitInfo(double limit, double sum, int year)
        {
            if (Math.Round(sum, 4) > Math.Round(limit, 4))
            {
                return String.Format("<b>на {3} г.:</b> <label style='{1}'>{0:### ### ### ### ##0.0000} (превышен на {2:### ### ### ### ##0.0000})</label> ", limit, "color:red", sum - limit, year
                    );
            }
            else
            {
                return String.Format("<b>на {3} г:</b> <label style='{1}'>{0:### ### ### ### ##0.0000} (доступно {2:### ### ### ### ##0.0000})</label> ", limit, "color:green", limit - sum, year
                    );
            }
        }

        protected void unionGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var data = controller.GetActivityList(CurrentPlan, IsUnallocated, null, ChBFavourites.Checked);
            (sender as ASPxGridView).DataSource = data;
            (sender as ASPxGridView).ExpandAll();
        }

        protected void unionGrid_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
        {
            var isEmpty = (sender as ASPxGridView).VisibleRowCount <= 0 ? 1 : 0;

            e.Properties["cpIsEmpty"] = isEmpty;
            
            string cpJoinToItems = "";

            if (isEmpty == 0)
            {

                var data = controller.GetActivityList(CurrentPlan, IsUnallocated, null, ChBFavourites.Checked);
                var joined = controller.ExtractJoinedActivities(data);
                

                if (joined != null)
                {
                    var joinedActivities = joined.Rows.Cast<DataRow>().Select(x => new
                                {
                                    Id = (int)x["Id"],
                                    Name = (x["ActCode"] + " " + x["Name"]),
                                }).ToList();

                    joinedActivities.Insert(0, new
                    {
                        Id = 0,
                        Name = "[ новое мероприятие ]",
                    });

                    cpJoinToItems = Newtonsoft.Json.JsonConvert.SerializeObject(joinedActivities);
                }
            }
            e.Properties["cpJoinToItems"] = cpJoinToItems;
        }

        protected void callBackUnion_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(e.Parameter))
            {
                var args = Newtonsoft.Json.JsonConvert.DeserializeObject<UnionArgs>(e.Parameter);
                var newActivity = controller.Union(args);
                e.Result = args.target.ToString();
            }            
        }

        protected void copyCallback_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            e.Result = null;

            if (e.Parameter == "getActivities")
            {
                var data = controller.GetActivityList(CurrentPlan, IsUnallocated, null, ChBFavourites.Checked);
                if(data!=null)
                {
                    var activityList = data.Rows.Cast<DataRow>()
                        .OrderBy(row => row["IsNotPriority"]).ThenBy(row => row["Num"])  
                        .Select(row=> new
                    {
                        Id = (int)row["Id"] ,
                        Name = String.Format("{0} {1}", row["ActCode"], row["Name"]),                        
                    }).ToList();

                    e.Result = Newtonsoft.Json.JsonConvert.SerializeObject(activityList);
                }
            }
            else if (e.Parameter != null && e.Parameter.StartsWith("doCopy"))
            {
                var argsCopy = new PlansActivityArgs()
                {
                    Id = int.Parse(e.Parameter.Substring(6))
                };
                controller.Copy(argsCopy);
            }
        }

        protected void UpdateUviriCallback_Callback(object source, CallbackEventArgs e)
        {
            var uuc = source as ASPxCallback;
            try
            {
                var client = new CoordPlansActivityUpdatingServiceClient();
                if (e.Parameter.StartsWith("fromUviri"))
                {
                    if (e.Parameter.Contains("CheckSynch"))
                    {
                        var synchCheckResultItems =
                            (from DataRow row in
                                 PlanRepository.GetActivityList2012new(CurrentPlan, UserRepositoryBase.CurrentUserId).Rows
                             where !PlansActivityRepository.CheckSyncronization(Convert.ToInt32(row["ID"]))
                             select string.Format("{0} - {1}", row["ActCode"], row["Name"])).ToList();

                        if (synchCheckResultItems.Count > 0)
                        {
                            e.Result = "MkrfNotSynchronized";
                            if (uuc != null) uuc.JSProperties["cpNotSynchronizedActivities"] = synchCheckResultItems;
                            return;
                        }
                    }
                    
                    var result = client.GetExportPlanForMKRF(CurrentPlan.Year.Value);
                    if (!result.Success)
                    {
                        e.Result = result.ErrorMsg;
                    }
                    else
                    {
                        var importResult = XmlHelper.ImportPlanFromXml(CurrentPlan, result.Xml, true);

                        foreach (var valuePair in importResult)
                        {
                            var pa = RepositoryBase<PlansActivity>.Get(valuePair.Value);
                            var paSynchronization = RepositoryBase<PlansActivitySynchronization>.Get(x => x.PlanActivity == pa) ?? new PlansActivitySynchronization(pa);
                            paSynchronization.SynchronizationDate = DateTime.Now;
                            RepositoryBase<PlansActivitySynchronization>.SaveOrUpdate(paSynchronization);
                        }
                    }
                }
                else if (e.Parameter.StartsWith("toUviri"))
                {
                    var sourceFundingListBoxSelectedValues = SourceFundingListBox.SelectedValues.Cast<string>();
                    var activityTypeListBoxSelectedValues = ActivityTypeListBox.SelectedValues.Cast<string>();

                    if (e.Parameter.Contains("CheckSynch"))
                    {
                        int[] plansActivitiesIds =
                            PlanRepository.GetActivityList2012new(CurrentPlan, UserRepositoryBase.CurrentUserId)
                            .Rows
                            .Cast<DataRow>()
                            .Where(x => x["CoordID"] != DBNull.Value
                                && (PriorityRadioButtonList.Value == null || Convert.ToBoolean(x["IsPriority"]) == Convert.ToBoolean(Convert.ToInt32(PriorityRadioButtonList.Value)))
                                && (SourceFundingListBox.SelectedValues.Count == 0 || sourceFundingListBoxSelectedValues.Contains(x["Source_Funding_ID"].ToString()))
                                && (ActivityTypeListBox.SelectedValues.Count == 0 || activityTypeListBoxSelectedValues.Contains(x["ActTypeID"].ToString()))
                                )
                            .Select(x => Convert.ToInt32(x["CoordID"]))
                            .ToArray();
                        var synchCheckResult = client.CheckSynchronizationMKRF(plansActivitiesIds);
                        if (!synchCheckResult.Synchronized)
                        {
                            e.Result = "UviriNotSynchronized";
                            if (uuc != null) uuc.JSProperties["cpNotSynchronizedActivities"] = synchCheckResult.Items;
                            return;
                        }
                    }
                    
                    var uviriUser = RepositoryBase<Option>.Get(OptionsEnum.UVIRIUser).Value;

                    if (string.IsNullOrWhiteSpace(uviriUser))
                    {
                        e.Result =
                            "Пользователь, ответственный за размещение сведений в АИС УВИРИ, не задан. Обратитесь к Администратору.";
                        return;
                    }

                    var xml = XmlHelper.ExportPlanToXmlByProcedure(CurrentPlan,
                        PriorityRadioButtonList.Value != null ? (int?)Convert.ToInt32(PriorityRadioButtonList.Value) : null,
                        SourceFundingListBox.SelectedValues.Count > 0 ? string.Join(",", sourceFundingListBoxSelectedValues) : null,
                        ActivityTypeListBox.SelectedValues.Count > 0 ? string.Join(",", activityTypeListBoxSelectedValues) : null);
                    var result = client.UpdatePlanFromMKRF(CurrentPlan.Year.Value, Encoding.UTF8.GetBytes(xml.OuterXml), uviriUser);
                    if (!result.Success)
                    {
                        e.Result = result.ErrorMsg;
                    }
                    else
                    {
                        foreach (UpdatePlanFromMKRFResultItem resultItem in result.Items)
                        {
                            var pa = PlansActivityRepository.Get(resultItem.PlansActivityMKRFId);
                            if (pa != null)
                            {
                                pa.PlansActivityCoordId = resultItem.PlansActivityCoordId;
                                pa.PlansActivityCoordCode = resultItem.PlansActivityCoordCode;
                                PlansActivityRepository.SaveOrUpdate(pa);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                e.Result = ex.Message;
            }
        }

        protected void UnallocatedBtn_Click(object sender, EventArgs e)
        {
            var btn = sender as ASPxButton;
            if (btn != null)
            {
                hfUnallocated["unallocated"] = btn.Checked = !btn.Checked;
            }

            controller.ClearCache();
            listGrid.DataBind();
        }

        protected void ListBox_PreRender(object sender, EventArgs e)
        {
            (sender as ASPxListBox).DataBind();
        }

        protected void ActivityTypeListBox_DataBinding(object sender, EventArgs e)
        {
            var listBox = sender as ASPxListBox;
            if (listBox != null)
            {
                listBox.DataSource = RepositoryBase<ActivityType2>.GetAll();
            }
        }

        protected void SourceFundingListBox_DataBinding(object sender, EventArgs e)
        {
            var listBox = sender as ASPxListBox;
            if (listBox != null)
            {
                listBox.DataSource = RepositoryBase<SourceFunding>.GetAll();
            }
        }
    }
}