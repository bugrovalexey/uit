﻿namespace bgTeam.IsRui.Domain.Entities.Directories
{
    /// <summary>
    /// Общероссийский классификатор единиц измерения
    /// </summary>
    public class Okei : EntityDirectories
    {
        public virtual string FullName { get; set; }

        public virtual int Code { get; set; }

        public virtual string InternationalName { get; set; }

        public virtual string NationalCode { get; set; }

        public virtual string InternationalCode { get; set; }
    }
}
