﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using AutoMapper;

namespace TestLibrary
{
    class Program
    {
        static void Main(string[] args)
        {
            //new NHSession("Coord");

            try
            {
                //var res = RepositoryBase<Plan>.GetAll();
                GB.Coord.AutoMapperConfiguration.Configure();
                Mapper.AssertConfigurationIsValid();
            }
            catch (ReflectionTypeLoadException exp)
            {
                Console.WriteLine("1");
                GetErrorFull(exp);
            }
            catch (Exception exp)
            {
                Console.WriteLine("2");
                GetError(exp);
            }

            Console.WriteLine("OK");
            Console.ReadKey();
        }



        

        private static void GetError(Exception exp)
        {
            if (exp.InnerException != null)
                GetError(exp.InnerException);

            if (exp is ReflectionTypeLoadException)
            {
                GetErrorFull(exp as ReflectionTypeLoadException);
                return;
            }

            Console.WriteLine(exp.Message);
            Console.WriteLine(exp.StackTrace);
            Console.WriteLine();
        }
        
        private static void GetErrorFull(ReflectionTypeLoadException exp)
        {
            var i = 1;
            StringBuilder sb = new StringBuilder();
            foreach (Exception exSub in exp.LoaderExceptions)
            {
                sb.AppendLine(i.ToString() + ". " + exSub.Message);
                FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
                if (exFileNotFound != null)
                {
                    if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                    {
                        sb.AppendLine("Fusion Log:");
                        sb.AppendLine(exFileNotFound.FusionLog);
                    }
                }
                sb.AppendLine();
                i++;
            }

            Console.WriteLine(sb.ToString());
            Console.WriteLine();
            Console.WriteLine(exp.Message);
            Console.WriteLine(exp.StackTrace);
            Console.WriteLine();
        }
    }

}
