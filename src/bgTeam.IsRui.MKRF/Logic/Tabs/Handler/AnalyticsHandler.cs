﻿using bgTeam.IsRui.MKRF.Providers;

namespace bgTeam.IsRui.MKRF.Logic.Tabs.Handler
{
    class AnalyticsHandler : TabsBaseHandler
    {
        public AnalyticsHandler(IAccessControllerProvider accessControllerProvider) : base("Analytics", accessControllerProvider)
        {
            Add("Мои отчеты", "List");            
        }
    }
}
