﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.Extensions;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport
{
    public class TableCharacteristicOfTechnicalSupport : NotificationBaseViewModel
    {
        #region consts

        public const string NameCaption = "Наименование";
        public const string UnitCaption = "Единица измерения";
        public const string ValueCaption = "Значение ";

        #endregion consts

        public TableCharacteristicOfTechnicalSupport(CharacteristicOfTechnicalSupport entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            UnitName = entity.Unit.Return(unit => unit.Name, default(string));
            Value = entity.Value.ToString();
        }

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(UnitCaption)]
        public string UnitName { get; set; }

        [DisplayName(ValueCaption)]
        public string Value { get; set; }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteCharacteristicOfTechnicalSupport", "AccountingObjects", new { id = Id }, "GridCharacteristicOfTechnicalSupport");
            }
        }
    }
}