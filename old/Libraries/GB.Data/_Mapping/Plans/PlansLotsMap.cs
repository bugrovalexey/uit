﻿using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Mapping.Plans
{
    class PlansLotsMap : ClassMapBase<PlansLots>
    {
        public PlansLotsMap()
            : base("Plans_Lots")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Number, "Plans_Lots_Number");
            Map(x => x.Code, "Plans_Lots_OKDP_Code");
            Map(x => x.Name, "Plans_Lots_Name");
            Map(x => x.Unit, "Plans_Lots_Unit");
            Map(x => x.Count, "Plans_Lots_Kol ");
            Map(x => x.Price, "Plans_Lots_Price ");

            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID");
            References<PlansZakupki>(x => x.Zakupki, "Plans_Zakupki_ID");
        }
    }
}