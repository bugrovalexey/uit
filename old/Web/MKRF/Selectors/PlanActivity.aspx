﻿<%@ Page Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="PlanActivity.aspx.cs" Inherits="GB.MKRF.Web.Selectors.PlanActivity" %>

<asp:Content ID="Head" ContentPlaceHolderID="Head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
     <script type="text/javascript">
     // <![CDATA[
         function rowClick(s, e) {
             var id = s.GetRowKey(e.visibleIndex);
             window.parent.PopupClose(id);
         }
      // ]]> 
    </script>
    <dxwgv:ASPxGridView ID="SelectorsPlanActivityGrid" ClientInstanceName="SelectorsPlanActivityGrid" runat="server" AutoGenerateColumns="False" 
        KeyFieldName="Id" Width="100%" SettingsPager-Mode="ShowAllRecords"
        OnBeforePerformDataSelect="SelectorsPlanActivityGrid_BeforePerformDataSelect" 
        OnHtmlRowCreated="SelectorsPlanActivityGrid_HtmlRowCreated">
        <Columns>
            <dxwgv:GridViewDataTextColumn Caption="Код мероприятия" FieldName="Code" SortOrder="Ascending" SortIndex="1" />
            <dxwgv:GridViewDataTextColumn Caption="Название" FieldName="Name" />
        </Columns>
        <Styles>
            <AlternatingRow Enabled="true" />
            <Header Wrap="True" />
        </Styles> 
        <ClientSideEvents RowClick="rowClick" />
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="15" />
    </dxwgv:ASPxGridView>
</asp:Content>
