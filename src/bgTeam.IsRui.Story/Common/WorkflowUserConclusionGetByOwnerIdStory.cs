﻿namespace bgTeam.IsRui.Story.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.Common;

    public class WorkflowUserConclusionGetByOwnerIdStory : IStory<WorkflowUserConclusionGetByOwnerIdStoryContext, IEnumerable<WorkflowUserConclusionDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;

        public WorkflowUserConclusionGetByOwnerIdStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IUserRepository userRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _userRepository = userRepository;
        }

        public IEnumerable<WorkflowUserConclusionDto> Execute(WorkflowUserConclusionGetByOwnerIdStoryContext context)
        {
            return ExecuteAsync(context).GetAwaiter().GetResult();
        }

        public async Task<IEnumerable<WorkflowUserConclusionDto>> ExecuteAsync(WorkflowUserConclusionGetByOwnerIdStoryContext context)
        {
            context.EntityType.CheckNull(nameof(context.EntityType));
            context.EntityOwnerId.CheckNull(nameof(context.EntityOwnerId));
            context.StatusId.CheckNull(nameof(context.StatusId));

            var curUser = _userRepository.GetCurrent();
            var owner = Domain.Extensions.EntityExtension.GetObj(context.EntityType, context.EntityOwnerId);

            var res = _repository.GetAllEx<WorkflowUserConclusion>( q => q
                //.Fetch(x => x.Documents).Eager
                .Where(x => 
                //    x.EntityOwner == owner &&
                //    x.StatusId == context.StatusId &&
                    x.UserId == curUser.Id));

            return _mapper.Map(res, new List<WorkflowUserConclusionDto>());
        }
    }
}
