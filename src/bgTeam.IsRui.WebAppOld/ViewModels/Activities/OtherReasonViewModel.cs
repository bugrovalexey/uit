﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using bgTeam.IsRui.Domain;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public class TableOtherReason : NotificationBaseViewModel
    {
        public int Id { get; set; }

        [DisplayName(OtherReasonEditViewModel.NameCaption)]
        public string Name { get; set; }

        [DisplayName(OtherReasonEditViewModel.NumCaption)]
        public string Num { get; set; }

        [DisplayName(OtherReasonEditViewModel.SignerNameCaption)]
        public string SignerName { get; set; }

        [DisplayName(OtherReasonEditViewModel.SignerPositionCaption)]
        public string SignerPosition { get; set; }

        [DisplayName(OtherReasonEditViewModel.URLCaption)]
        public string UrlLink { get; set; }

        [DisplayName(OtherReasonEditViewModel.DocumentsCaption)]
        public string DocumentsLink { get; set; }

        //Служебные поля
        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("OtherReasonDelete", "Activities", new { id = Id }, "Grid"); }
        }

        public TableOtherReason(Reason value)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            Id = value.Id;
            Name = value.Name;
            Num = value.Num;
            SignerName = value.SignerName;
            SignerPosition = value.SignerPosition;

            UrlLink = value.URL.Return(url => LinkBuilder.Link(url, url), default(string));
            DocumentsLink = string.Join("<br />", value.Documents.Select(LinkBuilder.Link));
        }
    }

    [MetadataType(typeof(OtherReasonEditViewModel_Validation))]
    public class OtherReasonEditViewModel : ActivitiesCardBase
    {
        public const string NameCaption = "Наименование документа";
        public const string NumCaption = "Номер документа";
        public const string SignerNameCaption = "ФИО подписавшего";
        public const string SignerPositionCaption = "Должность подписавшего";
        public const string URLCaption = "URL документа";
        public const string DocumentsCaption = "Документ-основание";

        public const string EditActionLink = "OtherReasonEdit";

        public int ReasonId { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(NumCaption)]
        public string Num { get; set; }

        [DisplayName(URLCaption)]
        public string URL { get; set; }

        [DisplayName(SignerNameCaption)]
        public string SignerName { get; set; }

        [DisplayName(SignerPositionCaption)]
        public string SignerPosition { get; set; }

        [DisplayName(DocumentsCaption)]
        public IEnumerable<IEntityName> Documents { get; set; }

        public OtherReasonEditViewModel(IStatusRepository statusRepository, int ownerId, int? id)
            : base(statusRepository, ownerId)
        {
            Documents = new List<SelectEntityInfo>();
            if (id.HasValue)
            {
                FillData(new RepositoryDc<Reason>().GetExisting(id));
            }
        }

        private void FillData(Reason value)
        {
            ReasonId = value.Id;
            Name = value.Name;
            Num = value.Num;
            URL = value.URL;
            SignerName = value.SignerName;
            SignerPosition = value.SignerPosition;
            Documents = value.Documents.Select(x => new SelectEntityInfo() { Id = x.Id, Name = x.Name });
        }
    }

    public class OtherReasonEditViewModel_Validation : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string Num { get; set; }

        [RegularExpression(ConstatntHelper.UrlRegEx, ErrorMessage = NotUrl)]
        public string URL { get; set; }
    }
}