﻿using System;

namespace GB.DataAccess.Services.Args.Documents
{
    public abstract class AODocumentBaseArgs: BaseArgsNew
    {
        /// <summary>
        /// Id объекта учета
        /// </summary>
        public int AccountingObjectId { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Дата принятия документа
        /// </summary>
        public DateTime? DateAdoption { get; set; }

        public string Paragraph { get; set; }
    }
}
