﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;
using Lucene.Net.Highlight;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Uviri.MKRF.Repository;

namespace Uviri.MKRF.Web.Search
{
    public class SearchEngine
    {
        SearchArgs args;

        public int PageSize { get; protected set; }
        public int CurrentPage { get; protected set; }
        public int Total { get; protected set; }

        string fnId = SearchRepository.DocField.Id.ToString();
        string fnType = SearchRepository.DocField.Type.ToString();
        string fnName = SearchRepository.DocField.Name.ToString();
        string fnText = SearchRepository.DocField.Text.ToString();
        string fnModDate = SearchRepository.DocField.ModDate.ToString();
        string fnOwnerId = SearchRepository.DocField.OwnerId.ToString();
        string fnOwnerPti = SearchRepository.DocField.OwnerPTI.ToString();
        string fnOwnerInfo = SearchRepository.DocField.OwnerInfo.ToString();

        public SearchEngine(SearchArgs Args)
        {
            this.args = Args;
        }

        public DataTable DoSearch(string IndexDirectory)
        {
            FSDirectory directory = FSDirectory.Open(new System.IO.DirectoryInfo(IndexDirectory));

            IndexReader reader = IndexReader.Open(directory, true);
            Searcher searcher = new IndexSearcher(reader);
            Analyzer analyzer = new Snowball.Net.Lucene.Net.Analysis.Snowball.RuAnalyzer();

            BooleanQuery filterQuery = new BooleanQuery();

            SortField sortField = null;

            // Текст
            Query textQuery = null;
            if (string.IsNullOrEmpty(args.Query))
            {
                textQuery = new Lucene.Net.Search.MatchAllDocsQuery();
                sortField = new SortField(SearchRepository.DocField.ModDate.ToString(), SortField.STRING, true);
            }
            else
            {
                MultiFieldQueryParser parser = new MultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_29, new string[] { 
                    SearchRepository.DocField.Text.ToString(), 
                    SearchRepository.DocField.Name.ToString() }, analyzer);

                parser.SetDefaultOperator(QueryParser.Operator.OR);
                textQuery = parser.Parse(args.Query);
            }

            // Тип
            addSet(filterQuery, analyzer, SearchRepository.DocField.Type, args.Type);

            // ОГВ
            addSet(filterQuery, analyzer, SearchRepository.DocField.Department, args.Department);

            // Год
            if (args.Year1.HasValue || args.Year2.HasValue)
            {
                int yearStart = args.Year1.HasValue ? args.Year1.Value : int.MinValue;
                int yearEnd = args.Year2.HasValue ? args.Year2.Value : int.MaxValue;

                MultiFieldQueryParser dateParser = new MultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_29, new string[] { SearchRepository.DocField.Year1.ToString(), SearchRepository.DocField.Year2.ToString() }, analyzer);
                filterQuery.Add(dateParser.Parse(string.Format("({0}:[{1} TO {2}]) OR ({3}:[{1} TO {2}])", SearchRepository.DocField.Year1, yearStart, yearEnd, SearchRepository.DocField.Year2)), BooleanClause.Occur.MUST);
            }


            // Дата изменения
            if (args.Date1.HasValue || args.Date2.HasValue)
            {
                string dateStart = DateTools.DateToString(args.Date1.HasValue ? args.Date1.Value : DateTime.MinValue, DateTools.Resolution.MINUTE);
                string dateEnd = DateTools.DateToString(args.Date2.HasValue ? args.Date2.Value : DateTime.MaxValue, DateTools.Resolution.MINUTE);

                QueryParser dateParser = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, SearchRepository.DocField.ModDate.ToString(), analyzer);

                filterQuery.Add(dateParser.Parse("[" + dateStart + " TO " + dateEnd + "]"), BooleanClause.Occur.MUST);
            }


            // Автор изменения
            if (!string.IsNullOrEmpty(args.Author))
            {
                QueryParser parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, SearchRepository.DocField.Author.ToString(), analyzer);
                filterQuery.Add(parser.Parse(args.Author), BooleanClause.Occur.MUST);
            }

            // Статус
            addSet(filterQuery, analyzer, SearchRepository.DocField.Status, args.Status);

            // Куратор
            if (!string.IsNullOrEmpty(args.Curator))
            {
                QueryParser parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, SearchRepository.DocField.Curator.ToString(), analyzer);
                filterQuery.Add(parser.Parse(args.Curator), BooleanClause.Occur.MUST);
            }

            // Икт компонент
            addSet(filterQuery, analyzer, SearchRepository.DocField.IKT, args.IKT);

            // суммы планов информатизации v2011
            addMoney(filterQuery, SearchRepository.DocField.sF, args.sF_1, args.sF_2);
            addMoney(filterQuery, SearchRepository.DocField.sR, args.sR_1, args.sR_2);
            addMoney(filterQuery, SearchRepository.DocField.sV, args.sV_1, args.sV_2);
            addMoney(filterQuery, SearchRepository.DocField.sSum, args.sSum_1, args.sSum_2);

            // суммы планов информатизации v2012 и форм сведений
            addMoney(filterQuery, SearchRepository.DocField.sY0, args.sY0_1, args.sY0_2);
            addMoney(filterQuery, SearchRepository.DocField.sY1, args.sY1_1, args.sY1_2);
            addMoney(filterQuery, SearchRepository.DocField.sY2, args.sY2_1, args.sY2_2);

            addMoney(filterQuery, SearchRepository.DocField.sAddY0, args.sAddY0_1, args.sAddY0_2);
            addMoney(filterQuery, SearchRepository.DocField.sAddY1, args.sAddY1_1, args.sAddY1_2);
            addMoney(filterQuery, SearchRepository.DocField.sAddY2, args.sAddY2_1, args.sAddY2_2);

            addMoney(filterQuery, SearchRepository.DocField.sSumY0, args.sSumY0_1, args.sSumY0_2);
            addMoney(filterQuery, SearchRepository.DocField.sSumY1, args.sSumY1_1, args.sSumY1_2);
            addMoney(filterQuery, SearchRepository.DocField.sSumY2, args.sSumY2_1, args.sSumY2_2);


            CachingWrapperFilter cwf = null;
            if (filterQuery.GetClauses().Length <= 0)
                cwf = new CachingWrapperFilter(new QueryWrapperFilter(new Lucene.Net.Search.MatchAllDocsQuery()));
            else
                cwf = new CachingWrapperFilter(new QueryWrapperFilter(filterQuery));

            TopScoreDocCollector collector = TopScoreDocCollector.create(10000, true);

            ScoreDoc[] hits = null;

            if (sortField != null)
            {
                hits = searcher.Search(textQuery, cwf, 1000, new Sort(sortField)).scoreDocs;
            }
            else
            {
                searcher.Search(textQuery, cwf, collector);
                hits = collector.TopDocs().scoreDocs;
            }

            
            DataTable table = new DataTable();
            table.Columns.Add(fnId);
            table.Columns.Add(fnType);
            table.Columns.Add(fnName);
            table.Columns.Add(fnText);
            table.Columns.Add(fnModDate);
            table.Columns.Add(fnOwnerId);
            table.Columns.Add(fnOwnerInfo);

            if (hits.Length <= 0)
                return table;
                

            Dictionary<int, string> docToOwnerDict = new Dictionary<int, string>();
            Dictionary<string, HashSet<string>> ptiAndIdDict = new Dictionary<string, HashSet<string>>();

            // валидация 

            
            
            foreach (ScoreDoc scoreDoc in hits)
            {
                Lucene.Net.Documents.Document doc = searcher.Doc(scoreDoc.doc);
                //IList fields = doc.GetFields();

                int docId = scoreDoc.doc;

                string ownerId = doc.Get(fnOwnerId);
                string ownerPti = doc.Get(fnOwnerPti);

                string ownerCompositeKey = string.Concat(ownerPti, "_", ownerId);

                docToOwnerDict.Add(docId, ownerCompositeKey);

                if (!ptiAndIdDict.ContainsKey(ownerPti))
                    ptiAndIdDict.Add(ownerPti, new HashSet<string>());

                if (!ptiAndIdDict[ownerPti].Contains(ownerId))
                    ptiAndIdDict[ownerPti].Add(ownerId);
            }
            HashSet<string> whiteList = SearchRepository.Validate(ptiAndIdDict);
         
            this.PageSize = args.PageSize;
            this.CurrentPage = args.Page;


            int start = CurrentPage * PageSize;
            int end = start + PageSize;
            
            
            this.Total = 0;


            foreach (ScoreDoc scoreDoc in hits)
            {
                if (!whiteList.Contains(docToOwnerDict[scoreDoc.doc]))
                    continue;
      
                if (start <= this.Total && this.Total < end)
                {
                    Lucene.Net.Documents.Document doc = searcher.Doc(scoreDoc.doc);

                    string title = generatePreviewText(textQuery, doc.Get(fnName), fnName, 110);
                    string text = generatePreviewText(textQuery, doc.Get(fnText), fnText, 250);

                    table.Rows.Add(new object[] { 
                        doc.Get(fnId), 
                        doc.Get(fnType), 
                        title, 
                        text,
                        convertDate(doc.Get(fnModDate)),
                        doc.Get(fnOwnerId),
                        doc.Get(fnOwnerInfo)
                    });
                }

                this.Total++;
            }

            reader.Close();
            searcher.Close();
            analyzer.Close();
             
            return table;
        }

        string generatePreviewText(Query q, string text, string fieldName, int fragmentLength)
        {
            if (string.IsNullOrEmpty(text))
                return null;

            string highlightStartTag = "<em>";
            string highlightEndTag = "</em>";

            QueryScorer scorer = new QueryScorer(q);
            Formatter formatter = new SimpleHTMLFormatter(highlightStartTag, highlightEndTag);
            Highlighter highlighter = new Highlighter(formatter, scorer);
            highlighter.SetTextFragmenter(new SimpleFragmenter(fragmentLength));
            TokenStream stream = new Snowball.Net.Lucene.Net.Analysis.Snowball.RuAnalyzer().TokenStream(fieldName, new StringReader(text));

            string bestFragment = highlighter.GetBestFragment(stream, text);

            if (!string.IsNullOrEmpty(bestFragment))
            {
                // TODO: костыльная подстановка троеточия в разрыв строки
                string temp = bestFragment.Replace(highlightEndTag, "").Replace(highlightStartTag, "");
                if (!text.StartsWith(temp))
                {
                    bestFragment = "..." + bestFragment;
                }
                if (!text.EndsWith(temp))
                {
                    bestFragment = bestFragment + "...";
                }
            }

            return string.IsNullOrEmpty(bestFragment) ? trimText(text, fragmentLength) : bestFragment;
        }

        string trimText(string text, int fragmentLength)
        {
            if (text.Length <= fragmentLength)
                return text;

            string temp = text.Substring(0, fragmentLength);

            if (char.IsLetterOrDigit(text[fragmentLength]))
            {
                int i = temp.Length - 3;
                for (; i >= 0; i--)
                {
                    if (!char.IsLetterOrDigit(temp[i]))
                        break;
                }
                if (i > 0)
                {
                    temp = temp.Substring(0, i) + "...";
                }
            }

            return temp;
        }

        string convertDate(string date)
        {

            if (string.IsNullOrEmpty(date))
                return "";

            date = date.Substring(0, 8);

            return date.Substring(0, 4) + "." + date.Substring(4, 2) + "." + date.Substring(6, 2);
        }


        void addSet(BooleanQuery filterQuery, Analyzer analyzer, SearchRepository.DocField field, HashSet<int> set)
        {
            if (set != null && set.Count > 0)
            {
                QueryParser parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, field.ToString(), analyzer);
                StringBuilder builder = new StringBuilder();
                
                foreach (int value in set)
                {
                    if (builder.Length > 0)
                        builder.Append(" ");
                    
                    builder.Append(value);
                }

                filterQuery.Add(parser.Parse(builder.ToString()), BooleanClause.Occur.MUST);
            }

        }

        void addMoney(BooleanQuery filterQuery, SearchRepository.DocField field, double? d1, double? d2)
        {
            if (d1.HasValue || d2.HasValue)
            {
                double moneyFrom = d1.HasValue ? d1.Value : double.MinValue;
                double moneyTo = d2.HasValue ? d2.Value : double.MaxValue;

                filterQuery.Add(NumericRangeQuery.NewDoubleRange(field.ToString(), moneyFrom, moneyTo, true, true), BooleanClause.Occur.MUST);
            }
        }
    }
}