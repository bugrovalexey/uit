﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    [MetadataType(typeof(AOGovServiceNPAEditViewModel_Valid))]
    public class AOGovServiceNPAEditViewModel : ViewModelBase
    {
        public AOGovServiceNPAEditViewModel(int aoGovServiceId, int? id)
        {
            AOGovServiceId = aoGovServiceId;

            var entity = id.HasValue
                ? new RepositoryDc<AOGovServiceNPA>().GetExisting(id)
                : null;

            entity.Do(Fill);
        }

        protected void Fill(AOGovServiceNPA entity)
        {
            Id = entity.Id;
            AOGovServiceId = entity.GovService.Id;
            Name = entity.Name;
            DocumentNumber = entity.DocumentNumber;
            Date = entity.Date.ToStringOrDefault();
            ParticleOfDocument = entity.ParticleOfDocument;
        }

        public int Id { get; set; }

        public int AOGovServiceId { get; set; }

        [DisplayName(TableAOGovServicesNPA.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableAOGovServicesNPA.DocumentNumberCaption)]
        public string DocumentNumber { get; set; }

        [DisplayName(TableAOGovServicesNPA.DateCaption)]
        public string Date { get; set; }

        [DisplayName(TableAOGovServicesNPA.ParticleOfDocumentCaption)]
        public string ParticleOfDocument { get; set; }

        public SelectList NPAKindsList { get; set; }
    }

    internal class AOGovServiceNPAEditViewModel_Valid : ValidateViewModel
    {
    }
}