﻿using bgTeam.IsRui.Domain.Entities.Plans;

namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    internal class ActivityType2Map : ClassMapHiLo<ActivityType>
    {
        public ActivityType2Map()
            : base("Plans_Activity_Type2")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Type2_Name").Nullable();
        }
    }
}