﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Helpers.Control
{
    /// <summary>
    /// Класс для описания свойств типизированной таблицы DataTables
    /// </summary>
    public class DataTableSettings<TItem> : DataTableSettingsBase
    {
        private readonly ViewDataDictionary<TItem> viewDataDictionary = new ViewDataDictionary<TItem>();

        public DataTableSettings()
            : base()
        {
        }

        public DataTableSettings(string keyId)
            : base(keyId)
        {
        }

        public override IList<DataTablesColumn> GetColumnsList()
        {
            return ColumnsList;
        }

        public DataTableSettings<TItem> AddColumn(DataTablesColumn column)
        {
            ColumnsList.Add(column);
            return this;
        }

        public DataTableSettings<TItem> AddColumn<TValue>(Expression<Func<TItem, TValue>> expression,
            string caption = null, string format = null,
            bool searchable = false, bool sortable = false, bool selectable = false, bool visible = true)
        {
            var info = GetExpressionInfo<TValue>(expression);
            DataTablesColumn column = new DataTablesColumn(expression, info.Key, caption ?? info.Value);
            column
                .SetFormat(format)
                .Search(searchable)
                .Sort(sortable)
                .Select(selectable)
                .SetVisiblie(visible);
            ColumnsList.Add(column);
            return this;
        }

        public DataTableSettings<TItem> AddDeleteColumn(bool visible)
        {
            var column = new DataTablesDeleteColumn(visible);
            ColumnsList.Add(column);
            return this;
        }

        public DataTableSettings<TItem> AddFooter(string template = DataTableSettingsBase.DefaultFooter)
        {
            FooterTemplateText = template;
            return this;
        }

        public DataTableSettings<TItem> EditPage(string url)
        {
            ItemEditPage = url;
            return this;
        }

        public DataTableSettings<TItem> IsTreeTable()
        {
            IsTreeTableFlag = true;
            return this;
        }

        private KeyValuePair<string, string> GetExpressionInfo<TValue>(Expression<Func<TItem, TValue>> expression)
        {
            if (expression.Body.NodeType == ExpressionType.Call)
            {
                MethodCallExpression methodExpression = expression.Body as MethodCallExpression;
                bool isSingleArgumentIndexer = (((methodExpression != null) && (methodExpression.Arguments.Count == 1))
                    && methodExpression.Method.DeclaringType.GetDefaultMembers()
                        .OfType<PropertyInfo>()
                        .Any<PropertyInfo>(p => (p.GetGetMethod() == methodExpression.Method)));
                if (!isSingleArgumentIndexer)
                {
                    var value = expression.Body.ToString();
                    return new KeyValuePair<string, string>(
                        "''", // для сложных выражений возвращаем пустую строку
                        value);
                }
            }

            //Шаблоны могут использоваться только с выражениями обращения к полю, обращения к свойству,
            //индекса одномерного массива и настраиваемого индексатора с одним параметром.
            var modelMeta = ModelMetadata.FromLambdaExpression(expression, viewDataDictionary);
            var expr = ExpressionHelper.GetExpressionText(expression);
            var displayName = modelMeta.DisplayName ?? expr;
            return new KeyValuePair<string, string>(
                expr, // для простых выражений возвращаем само выражение, т.е. для "x.KOSGU.Name" вернем "KOSGU.Name"
                displayName);
        }
    }
}