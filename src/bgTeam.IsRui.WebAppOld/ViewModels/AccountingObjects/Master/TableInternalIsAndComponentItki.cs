﻿using System.ComponentModel;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class TableInternalIsAndComponentItki : NotificationBaseViewModel
    {
        #region Consts

        private const string ShortNameCaption = "Краткое наименование ОУ";
        private const string KindNameCaption = "Классификационная категория ОУ";
        private const string DepartmentNameCaption = "Учреждение";
        public const string PurposeCaption = "Назначение";

        #endregion Consts

        #region Ctors

        public TableInternalIsAndComponentItki(InternalIsAndComponentsItki entity)
        {
            Id = entity.Id;
            entity.Object.Do(obj =>
            {
                ShortName = obj.ShortName;
                KindName = obj.Kind.Return(k => k.Name, default(string));
                DepartmentName = obj.ResponsibleDepartment.Return(d => d.Name, default(string));
            });

            Purpose = entity.Purpose;
        }

        #endregion Ctors

        #region Props

        public int Id { get; set; }

        [DisplayName(ShortNameCaption)]
        public string ShortName { get; set; }

        [DisplayName(KindNameCaption)]
        public string KindName { get; set; }

        [DisplayName(DepartmentNameCaption)]
        public string DepartmentName { get; set; }

        [DisplayName(PurposeCaption)]
        public string Purpose { get; set; }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteInternalIsAndComponentItki", "AccountingObjects", new { id = Id }, "GridInternalIsAndComponentsItki");
            }
        }

        #endregion Props
    }
}