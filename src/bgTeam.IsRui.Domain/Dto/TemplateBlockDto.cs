﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class TemplateBlockDto
    {
        public string Title { get; set; }

        public List<PositionsTemplateDto> Positions { get; set; }
    }
}
