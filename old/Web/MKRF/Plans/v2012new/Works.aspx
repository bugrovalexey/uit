﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/v2012new/ActivityCard.master" AutoEventWireup="true" CodeBehind="Works.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.Works" %>
<%@ Import Namespace="GB.MKRF.Entities.Directories" %>
<%@ Register Src="~/Common/GridColumnManager.ascx" TagName="GridColumnManager" TagPrefix="gcm" %>
<%@ Register Src="../../Common/DocumentControl.ascx" TagName="DocumentControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <%--<script src="/content/Scripts/jquery.json-2.3.js"></script>--%>
    <script type="text/javascript">
        function hfWorksSave(e) {
            //            eiId = hfWorks.Get("ExpenditureItemsId");
            hfWorks.Clear();
            hfWorks.Set('Name', NameMemo.GetText());
            hfWorks.Set('OKDP', OKDPTextBox.GetText());
            hfWorks.Set('ExpenseDirectionId', ExpenseDirectionComboBox.GetValue());
            hfWorks.Set('ExpenditureItemsId', ExpenditureItemsComboBox.GetValue());

//            hfWorks.Set('ExpenditureItemsId', eiId);
            var typeList = $('.typeSelect:not(#typeSelectTemplate)');
            $.each(typeList, function (i, el) {
                hfWorks.Set('ExpenseType_' + i, $(el).val());
                hfWorks.Set('ExpenseTypeVolume_' + i, $(el).closest('tr').next('tr').find('.volume').val());
                var typeid = $(el).data('typeid');
                if (typeid) {
                    hfWorks.Set('ExpenseTypeId_' + i, typeid);
                }
            });
        }

        function hfTypesWorkSave(e) {
            hfWorks.Clear();
            hfWorks.Set('WTType', WTType_Combo.GetValue());
            hfWorks.Set('WTY0', WTExpY0_TextBox.GetValue());
        }

        function bcallback(e) {
            if (e.toUpperCase() == 'UPDATEEDIT') {
                hfWorksSave(e);
            }
        }

        function UpdateTypeListSource(jsonExpenseTypeList) {
            //десериализуем список
            var typeList = eval(jsonExpenseTypeList);

            if (typeList) {
                //заходим в каждый селект
                $.each($('.typeSelect'), function (i, e) {

                    //запоминаем текущий выбранный элемент
                    var selectedValue = $(e).val();

                    //чистим селект
                    $('option', e).remove();

                    //пробегаем каждый элемент списка типов
                    $.each(typeList, function (ix, ex) {
                        var str = ex.Value;
                        var len = str.length;
                        var loop = len / 50;
                        var value = str.length >= 70 ? str.substring(0, 67) + '...' : str;
//                        for (i = 0; i <= loop; i++) {
//                            value += str.slice(i * 50, (i * 50) + 50);
//                            //                            value += '<br>';
//                        }
                        //вставляем в селект очередной тип из списка
                        $(e).append($('<option></option>').attr('value', ex.Key).html(value));
                        //делаем выбранным, который был выбран элемент, если такой есть
                        $('option[value="' + selectedValue + '"]', e).attr('selected', 'selected');
                    });
                });
            }
        }

        $(function () {
            //$("a.popup").click(function (e) {
            //    e.preventDefault();
            //    var url = $(this).attr("href");
            //    window.open(url, "_blank", "height=300,width=700,directories=0,location=0,menubar=0,status=0,toolbar=0,scrollbars=1,resizable=1");
            //});
        
            var GetTypeTemplate = function (typeSelect) {
                return "<tr><td colspan='2' class='sep'></td></tr>" +
                    "<tr>" +
                    "<td class='form_edit_desc'>Тип затрат</td>" +
                    "<td class='form_edit_input'>" +
                    typeSelect +
                    "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td class='form_edit_desc'>Объем бюджетных ассигнований</td>" +
                    "<td class='form_edit_input'>" +
                    "<input type='text' class='volume' style='width: 396px;' />" +
                    "</td>" +
                    "</tr>";
            };
        
            $(".addType").live('click', function () {
                var linkTr = $(this).closest('tr');
                linkTr.before(GetTypeTemplate($('#typeSelectTemplate').clone().attr('id', '').show().wrap('<p>').parent().html()));
            });
            
        
            //UpdateButtonClear('ExpenditureItems');
        });

        function validateComboBox(s, e) {
            var cbValue = s.GetValue();
            e.isValid = cbValue && cbValue != 0;
        }
        
        function validateMemo(s, e) {
            var cbValue = s.GetValue();
            e.isValid = (cbValue != undefined || cbValue) && cbValue != '- Не задано';
        }
        
        //function UpdateButtonClear(name) {
        //    var val = hfWorks.Get(name + 'Id');
        //
        //    if (val && val != 0) {
        //        var btName = '#' + name + 'Memo';
        //        var bt = $(btName).parents('td.form_edit_input').find('div.ButtonClear');
        //
        //        bt.css('display', '');
        //    }
        //}


        //SelectValue = function (e) {
        //    PopupControl.SetContentHtml("");
        //    var title;
        //    switch (e) {
        //        case 'ExpenditureItems':
        //            title = 'Выбирите БК КОСГУ';
        //            break;
        //        default:
        //            alert('error');
        //    }
        //
        //    PopupControl.SetContentUrl("../../Selectors/CatalogItems.aspx?year=<%= CurrentActivity.Plan.Year%>&type=" + e);
        //    PopupControl.RefreshContentUrl();
        //    PopupControl.SetHeaderText(title);
        //    PopupControl.Show();
        //};

        //CatalogPopupClose = function (type, result) {
        //    PopupControl.Hide();
        //    var mass = result.split(';');
        //    if (mass[0]) hfWorks.Set(type + 'Id', mass[0]);
        //    if (mass[1]) hfWorks.Set(type + 'Code', mass[1]);
        //    if (mass[2]) hfWorks.Set(type + 'Name', mass[2]);
        //    var Memo = '#' + type + 'Memo';
        //    $(Memo).find('input').val(hfWorks.Get(type + 'Code') + ' ' + hfWorks.Get(type + 'Name'));
        //    UpdateButtonClear(type);
        //
        //    PopupControl.SetContentUrl("../../Selectors/CatalogItems.aspx?year=<%= CurrentActivity.Plan.Year%>&type=-999");
        //    PopupControl.RefreshContentUrl();
        //};


        //function ClearValue(s, name) {
        //    if (hfWorks.Contains(name + 'Id')) {
        //        //ShowConfirm(true);
        //    }

        //    hfWorks.Set(name + 'Id', '0');
        //    hfWorks.Set(name + 'Name', '');
        //    hfWorks.Set(name + 'Code', '');

        //    var Memo = '#' + name + 'Memo';
        //    $(Memo).find('input').val('- Не задано');

        //    $(s).css('display', 'none');
        //};
    </script>
    <dxhf:ASPxHiddenField runat="server" ID="hfWorks" ClientInstanceName="hfWorks" />

    <p class="header_grid">
        Работы, проводимые в рамках мероприятия
        <span class="subtitle">Заполняется на третьем этапе
        </span>
    </p>

    <dxe:ASPxButton ID="addWorksButton" runat="server" ClientInstanceName="addWorksButton"
        AutoPostBack="false" Text="Добавить" Width="80px" Style="margin-top: 10px; margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {treeList.StartEditNewNode();}" />
    </dxe:ASPxButton>
    
    <div style="float: right;">
                <gcm:GridColumnManager ID="TreeColumnManager" runat="server" GridName="treeList" UserStyle="width:45px; top: 14px; position: relative;" />
    </div>

    <dxtl:ASPxTreeList ID="treeList" ClientInstanceName="treeList" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="Id" ParentFieldName="ParentId"
        OnNodeUpdating="treeList_NodeUpdating" OnNodeInserting="treeList_NodeInserting" OnStartNodeEditing="treeList_StartNodeEditing"
        OnNodeDeleting="treeList_NodeDeleting" OnNodeDeleted="treeList_NodeDeleted" OnDataBinding="treeList_DataBinding" OnCustomCallback="treeList_CustomCallback"
        OnCommandColumnButtonInitialize="treeList_CommandColumnButtonInitialize" OnHtmlRowPrepared="treeList_HtmlRowPrepared" OnCustomSummaryCalculate="treelest_CustomSummaryCalculate">
        <Settings GridLines="Horizontal" ShowColumnHeaders="True" ShowFooter="True"/>
        <SettingsBehavior ExpandCollapseAction="NodeDblClick" />
        <SettingsEditing Mode="PopupEditForm" ConfirmDelete="true" AllowRecursiveDelete="True" />
        <SettingsPopupEditForm Modal="true" Width="900px"  Caption="Работа" VerticalAlign="WindowCenter" HorizontalAlign="Center" />
        <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
        <%--<SettingsCookies Enabled="true" />--%>
        <Styles>
            <Header Wrap="True" Font-Bold="True" HorizontalAlign="Center"></Header>
            <Cell Wrap="True"></Cell>
            <AlternatingNode Enabled="False" />
        </Styles>
        <Templates>
            <EditForm>
                <select id="typeSelectTemplate" class="typeSelect" style="width: 402px; display: none;">
                    <option value='0' selected='selected'>Не задано</option>
                </select>
                <dxc:ASPxCallback runat="server" ID="ExpenseDirectionCallback" ClientInstanceName="ExpenseDirectionCallback" OnCallback="ExpenseDirection_Callback">
                    <ClientSideEvents
                        Init="function(s,e){
                                            ExpenseDirectionCallback.PerformCallback(ExpenseDirectionComboBox.GetSelectedItem().value);
                                        }"
                        EndCallback="function(s,e){
                                            UpdateTypeListSource(s.cp_expenseTypeList);
                                        }"></ClientSideEvents>
                </dxc:ASPxCallback>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc required">
                            Наименование:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo ID="NameMemo" runat="server" ClientInstanceName="NameMemo" Rows="5"
                                OnDataBinding="NameMemo_DataBinding" >
                            </dxe:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Вид затрат:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ExpenseDirectionComboBox" runat="server" ClientInstanceName="ExpenseDirectionComboBox"
                                OnDataBinding="ExpenseDirectionComboBox_DataBinding" TextField="Value" ValueField="Key" ValueType="System.Int32" EncodeHtml="false">
                                <ClientSideEvents SelectedIndexChanged="function(s,e){
                                            ExpenseDirectionCallback.PerformCallback(s.GetSelectedItem().value);
                                        }"></ClientSideEvents>
                            </dxe:ASPxComboBox>
                            <dxp:ASPxPanel runat="server" ID="LegendPanel" ClientInstanceName="LegendPanel">
                                <PanelCollection>
                                    <dxp:PanelContent>
                                        <span style="color: red;">Необходимо добавить допустимые виды затрат на вкладке расходы</span>
                                        <br />
                                    </dxp:PanelContent>
                                </PanelCollection>
                                <ClientSideEvents Init="function(s,e) { var items = ExpenseDirectionComboBox.GetItemCount(); if(items <= 1) {LegendPanel.SetVisible(true);} else {LegendPanel.SetVisible(false);} } " />
                            </dxp:ASPxPanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Код ОКДП:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="OKDPTextBox" runat="server" ClientInstanceName="OKDPTextBox" OnDataBinding="OKDPTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Код по БК КОСГУ
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ExpenditureItemsComboBox" runat="server" ClientInstanceName="ExpenditureItemsComboBox" 
                                OnDataBinding="ExpenditureItemsComboBox_DataBinding" TextField="CodeName" ValueField="Id" ValueType="System.Int32" EncodeHtml="false" DropDownHeight="300px">
                            </dxe:ASPxComboBox>


                            <%--<div style="position:relative">
                                    <dxe:ASPxTextBox ID="ExpenditureItemsMemo" runat="server" ClientInstanceName="ExpenditureItemsMemo" ClientIDMode="Static" Rows="2" ReadOnly="true" OnDataBinding="Memo_DataBinding">
                                        <ClientSideEvents Validation="function(s,e){ validateMemo(s,e); }" />
                                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Expenses">
                                            <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                            <RegularExpression ErrorText="Обязательное поле" ValidationExpression="^[- Не задано]" />
                                        </ValidationSettings>
                                    </dxe:ASPxTextBox>
                                    <div class="ButtonSelected catalogButton" onclick="SelectValue('ExpenditureItems'); return false;" ></div>
                                    <div class="ButtonClear catalogButton" style="display:none;" onclick="ClearValue(this, 'ExpenditureItems'); return false;" ></div>   
                                </div> --%>
                        </td>
                    </tr>
                    <% if (controller.CurrentEdit != null) foreach (WorkType workType in controller.CurrentEdit.WorkType)
                           {%>
                    <tr>
                        <td colspan='2' class='sep'></td>
                    </tr>
                    <tr>
                        <td class='form_edit_desc'>Тип затрат</td>
                        <td class='form_edit_input'>
                            <select class="typeSelect" data-typeid="<%=workType.Id %>" style="width: 402px; height: 20px;">
                                <option value='<%=workType.ExpenseType.Id %>' selected='selected'><%=workType.ExpenseType.Name %></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class='form_edit_desc'>Объем бюджетных ассигнований</td>
                        <td class='form_edit_input'>
                            <input type='text' class='volume' style='width: 396px;' value="<%=workType.ExpenseVolumeYear0 %>" />
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td colspan="2"><a href="javascript://" class="addType">Добавить тип затрат</a></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxtl:ASPxTreeListTemplateReplacement ID="ReasonGridTemplateReplacement1" runat="server" ReplacementType="UpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxtl:ASPxTreeListTemplateReplacement ID="ReasonGridTemplateReplacement2" runat="server" ReplacementType="CancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <Columns>
            <dxtl:TreeListCommandColumn VisibleIndex="0" Caption=" " ShowInCustomizationForm="False" Name="cmdColumn">
                <EditButton Visible="True" />
                <DeleteButton Visible="True"></DeleteButton>
            </dxtl:TreeListCommandColumn>
            <dxtl:TreeListTextColumn Caption="Номер" FieldName="ActivityWorkCode" ReadOnly="True" CellStyle-HorizontalAlign="Center" VisibleIndex="1">
            </dxtl:TreeListTextColumn>
            <dxtl:TreeListTextColumn FieldName="Name" Caption="Наименование" VisibleIndex="2">
            </dxtl:TreeListTextColumn>
            <dxtl:TreeListTextColumn Caption="Вид затрат" FieldName="ExpenseDirection_CodeName" VisibleIndex="3" />
            <dxtl:TreeListTextColumn Caption="Код по БК КОСГУ" FieldName="ExpenditureItemsCodeName" VisibleIndex="4" />
            <dxtl:TreeListTextColumn Caption="Код ОКДП" FieldName="OKDP_CodeName" Visible="False" ShowInCustomizationForm="True"/>
            <dxtl:TreeListTextColumn FieldName="ExpenseType_Name" Caption="Тип затрат" VisibleIndex="5">
            </dxtl:TreeListTextColumn>
            <dxtl:TreeListTextColumn FieldName="ExpenseVolumeYear0" Caption="Объем бюджетных ассигнований на очередной финансовый год, тыс. руб." MinWidth="200" VisibleIndex="6">
                <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
            </dxtl:TreeListTextColumn>
            <dxtl:TreeListCommandColumn VisibleIndex="99" Caption=" " ShowInCustomizationForm="False" Width="100" Name="copyColumn">
                <CustomButtons>
                    <dxtl:TreeListCommandColumnCustomButton ID="TreeListCommandColumnCustomButton" Text="Копировать" />
                </CustomButtons>
            </dxtl:TreeListCommandColumn>
        </Columns>
        <Summary>
            <dxtl:TreeListSummaryItem SummaryType="Custom" FieldName="ExpenseVolumeYear0" ShowInColumn="ExpenseVolumeYear0" DisplayFormat="{0:#,##0.0000}"></dxtl:TreeListSummaryItem>
        </Summary>
        <ClientSideEvents BeginCallback="function(s, e) { bcallback(e.command); }" CustomButtonClick="function(s, e) { s.PerformCallback(e.nodeKey) }"></ClientSideEvents>
    </dxtl:ASPxTreeList>

    <div style="padding: 10px 20px; font: 0.9em Arial;">
        * В случае, если объем бюджетных ассигнований по какому-либо виду расходов превышает планируемый объем расходования средств  по данному направлению, значение выделено <span style="color: red">красным цветом</span>
    </div>

    <p class="header_grid">
        Документы
        <span class="subtitle">Заполняется на третьем этапе
        </span>
    </p>
    <uc1:DocumentControl ID="dc" runat="server" DocType="Work" />

    <dxpc:ASPxPopupControl ID="PopupControl" runat="server" AllowDragging="True" AllowResize="True"
        CloseAction="CloseButton" Modal="True" EnableViewState="False" 
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="600px" 
        HeaderText="" ClientInstanceName="PopupControl" EnableHierarchyRecreation="True">
    </dxpc:ASPxPopupControl>
</asp:Content>
