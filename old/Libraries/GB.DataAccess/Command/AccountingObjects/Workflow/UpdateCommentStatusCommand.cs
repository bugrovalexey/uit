﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.Workflow
{
    public class UpdateCommentStatusCommand : ICommand<UpdateCommentStatusContext>
    {
        SessionFactory factory;

        public UpdateCommentStatusCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(UpdateCommentStatusContext context)
        {
            var comments = new Repository<AccountingObjectUserComment>().GetAllEx(q => q
                                    .Fetch(x => x.Status).Eager
                                    .Where(c => c.AccountingObject.Id == context.AO.Id && c.Status == null));
            
            var session = factory.CreateInternal();

            foreach (var comment in comments)
            {
                comment.Status = context.AO.Status;
                session.Update(comment);
            }            
        }
    }
}
