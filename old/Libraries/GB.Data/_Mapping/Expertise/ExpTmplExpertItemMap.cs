﻿using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Expertise;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpTmplExpertItemMap : ClassMapBase<ExpTmplExpertItem>
	{
        public ExpTmplExpertItemMap()
            : base("Exp_Tmpl_Expert_Item")
		{
//            Table("w_");

//            Id(x => x.Id, "Exp_Tmpl_Expert_Item_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_Exp_Tmpl_Expert_Item_Insert
//						  @Exp_Tmpl_Expert_ID = ?
//						 ,@Users_ID = ?
//						 ,@Exp_Tmpl_Expert_Item_ID = ?").Check.None();
			
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Tmpl_Expert_Item_Update
//						  @Exp_Tmpl_Expert_ID = ?
//						 ,@Users_ID = ?
//						 ,@Exp_Tmpl_Expert_Item_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Tmpl_Expert_Item_Delete
//						  @Exp_Tmpl_Expert_Item_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            References<ExpTmplExpert>(x => x.ExpTmplExpert, "Exp_Tmpl_Expert_ID").Not.Nullable();
			References<User>(x => x.User, "Users_ID").Not.Nullable();
        }
    }
}