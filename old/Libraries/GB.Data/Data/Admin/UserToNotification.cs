﻿namespace GB.MKRF.Entities.Admin
{
    /// <summary>Связь пользователя с рассылкой</summary>
    public class UserToNotification : EntityBase
    {
        /// <summary>Пользователь</summary>
        public virtual User User { get; set; }

        /// <summary>Рассылка</summary>
        public virtual Notification Notification { get; set; }
    }
}