﻿namespace bgTeam.IsRui.Domain._Mapping.Common
{
    using bgTeam.IsRui.Domain.Entities.Common;
    using FluentNHibernate.Mapping;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class ArticleGroupMap : ClassMap<ArticleGroup>
    {
        public ArticleGroupMap()
        {
            Table("ArticleGroup");

            Id(x => x.Id, "AG_ID");

            InitMap();
        }

        protected void InitMap()
        {
            Map(x => x.Name, "AG_Name");
            Map(x => x.TimeStamp, "AG_TimeStamp");
        }
    }
}
