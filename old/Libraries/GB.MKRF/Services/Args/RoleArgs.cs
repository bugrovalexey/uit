﻿namespace GB.MKRF.Services.Args
{
    public class RoleArgs : BaseArgsNew, IServiceArgs
    {
        /// <summary>Наименование</summary>
        public string Name { get; set; }

        // <summary>Стартовая страница для роли</summary>
        public string WebPage { get; set; }
    }
}