﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.Extentions
{
    public static class BoolExtensions
    {
        public static string ToRussian (this bool val)
        {
            if(val)
            {
                return "Да";
            }

            return "Нет";
        }
    }
}
