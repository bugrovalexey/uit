﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Admin;
using bgTeam.IsRui.WebApp.ViewModels.Admin.SiteMap;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.DataAccess.Services;
using bgTeam.IsRui.DataAccess.Services.Args;
using bgTeam.IsRui.MKRF.Providers;
using System.Web.Mvc;
using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.Common;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public partial class AdminController : ControllerBase
    {
        private readonly IAccessControllerProvider m_accessControllerProvider;

        public AdminController(IAccessControllerProvider accessControllerProvider)
        {
            m_accessControllerProvider = accessControllerProvider;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Users");
        }

        #region Roles

        public ActionResult Roles()
        {
            return View(new RoleViewModel());
        }

        public ActionResult RoleEdit(int? id)
        {
            return View(new RoleEditViewModel(id));
        }

        [HttpPost]
        public ActionResult RoleEdit(RoleArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var isNew = args.Id == 0;
                var entity = isNew
                    ? new RoleService().Create(args)
                    : new RoleService().Update(args);

                return new TableRoles(entity);
            });
        }

        [HttpPost]
        public ActionResult DeleteRole(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => new RoleService().Delete(new RoleArgs() { Id = id }));
        }

        #endregion Roles

        #region Users

        public ActionResult Users()
        {
            return View(new UserViewModel());
        }

        public ActionResult UserEdit(int? id)
        {
            return View(new UserEditViewModel(id));
        }

        [HttpPost]
        public ActionResult UserEdit(UserArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var isNew = args.Id == 0;
                var entity = isNew
                    ? new UserService().Create(args)
                    : new UserService().Update(args);

                return new TableUser(entity);
            });
        }

        [HttpPost]
        public ActionResult DeleteUser(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => new UserService().Delete(id));
        }

        #endregion Users

        #region AccessRights

        public ActionResult AccessRights()
        {
            return View(new AccessRightsViewModel());
        }

        [HttpPost]
        public ActionResult GetAccessRights(EntityType entity)
        {
            return PartialView("AccessRightsTable", new AccessRightsViewModel(entity).AccessTable);
        }

        public ActionResult AccessRightEdit(EntityType entityType, int? id)
        {
            var vm = new AccessToEntityViewModel(entityType, id);
            var view = View(vm);
            view.ViewBag.Title = string.Format("Права доступа к «{0}»", EnumHelper.GetDescription(entityType));
            return view;
        }

        [HttpPost]
        public ActionResult AccessRightEdit(AccessToEntityArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var isNew = args.Id == 0;
                var entity = isNew
                    ? new AccessToEntityService().Create(args)
                    : new AccessToEntityService().Update(args);

                return new TableAccessToEntity(entity);
            });
        }

        [HttpPost]
        public ActionResult AccessRightDelete(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => new AccessToEntityService().Delete(new AccessToEntityArgs { Id = id }));
        }

        [HttpPost]
        public ActionResult RefreshAccessRights()
        {
            return NotificationHelper.NotificationExecute(() => Singleton<AccessEntityProvider>.Instance.Reset(),
                "Права доступа обновлены", "Не удалось обновить права доступа");
        }

        #endregion AccessRights

        #region SiteMap

        public ActionResult SiteMap()
        {
            return View(new SiteMapViewModel());
        }

        public ActionResult SiteMapEdit(int? id)
        {
            return View(new SiteMapEditViewModel(id));
        }

        [HttpPost]
        public ActionResult SiteMapEdit(SiteMapArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var isNew = args.Id == 0;
                var entity = isNew
                    ? new SiteMapService().Create(args)
                    : new SiteMapService().Update(args);

                return new TableSiteMap(entity);
            });
        }

        [HttpPost]
        public ActionResult DeleteSiteMap(int id)
        {
            return NotificationHelper.NotificationDeleteCustomizable(id, () => new SiteMapService().Delete(new SiteMapArgs() { Id = id }));
        }

        [HttpPost]
        public ActionResult GetSiteMapToRoles(int siteMapId)
        {
            return PartialView("SiteMapToRoleTablePartial", new SiteMapToRoleViewModel(siteMapId));
        }

        public ActionResult SiteMapToRoleEdit(int siteMapId)
        {
            return View(new SiteMapToRoleEditViewModel(siteMapId));
        }

        [HttpPost]
        public ActionResult SiteMapToRoleEdit(SiteMapToRoleArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var entity = new SiteMapToRoleService().Create(args);
                return new TableSiteMapToRole(entity);
            });
        }

        [HttpPost]
        public ActionResult DeleteSiteMapToRole(int id)
        {
            return NotificationHelper.NotificationDeleteCustomizable(id, () => new SiteMapToRoleService().Delete(new SiteMapToRoleArgs() { Id = id }));
        }

        [HttpPost]
        public ActionResult CopyRolesToChildrenSiteMap(int siteMapId)
        {
            return NotificationHelper.NotificationExecute(() => new SiteMapService().CopyRolesToChildrenSiteMap(siteMapId),
                "Роли скопированы", "Не удалось скопировать роли");
        }

        [HttpPost]
        public ActionResult RefreshSiteMap()
        {
            return NotificationHelper.NotificationExecute(() => m_accessControllerProvider.Reset(),
                "Меню обновлено", "Не удалось обновить меню");
        }

        #endregion SiteMap

        #region StatusChange

        public ActionResult StatusChange()
        {
            return View(new StatusChangeViewModel());
        }

        public ActionResult StatusChangeEdit(int? id)
        {
            return View(new StatusChangeEditViewModel(null, id));
        }

        [HttpPost]
        public ActionResult StatusChangeEdit(StatusListArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                var isNew = args.Id == 0;
                var entity = isNew
                    ? new StatusListService().Create(args)
                    : new StatusListService().Update(args);

                return new TableStatusChange(entity);
            });
        }

        [HttpPost]
        public ActionResult StatusChangeDelete(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => new StatusListService().Delete(new StatusListArgs() { Id = id }));
        }

        #endregion StatusChange
    }
}