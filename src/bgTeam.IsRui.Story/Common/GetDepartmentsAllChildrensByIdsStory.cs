﻿namespace bgTeam.IsRui.Story.Common
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GetDepartmentsAllChildrensByIdsStory : IStory<GetDepartmentsAllChildrensByIdsStoryContext, IEnumerable<DepartmentDtoProc>>
    {
        private readonly IAppLogger _logger;
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;

        public GetDepartmentsAllChildrensByIdsStory(
            IAppLogger logger,
            IMapperBase mapper,
            IRepositoryRui repository)
        {
            _logger = logger;
            _mapper = mapper;
            _repository = repository;
        }

        public IEnumerable<DepartmentDtoProc> Execute(GetDepartmentsAllChildrensByIdsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<DepartmentDtoProc>> ExecuteAsync(GetDepartmentsAllChildrensByIdsStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            var query = await _repository.SelectExecuteAsync<DepartmentProc>(
                "EXEC GetGovtOrganHierarchy :Id",
                new[] { new ParameterProcedure("Id", context.Id.Value, new NHibernate.Type.Int32Type()) });

            return _mapper.Map(query, new List<DepartmentDtoProc>());
        }
    }
}
