﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ExpReasonMap : ClassMapBase<ExpReason>
    {
        public ExpReasonMap()
            : base("Exp_Reason")
        { }

        protected override void InitMap()
        {
            Map(x => x.Num, "Exp_Reason_Num").Nullable();
            Map(x => x.Name, "Exp_Reason_Name").Nullable();
            Map(x => x.IsActive, "Exp_Reason_Is_Active");

            References<ExpCriterion>(x => x.ExpCriterion, "Exp_Criterion_ID").Not.Nullable();
            References<ExpValuationInfo>(x => x.Info, "Exp_Valuation_Info_ID");

        }
    }
}
