﻿namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    using bgTeam.IsRui.Domain.Entities.Directories;

    internal class CategoryKindOfSupportMap : ClassMapHiLo<CategoryKindOfSupport>
    {
        public CategoryKindOfSupportMap()
            : base("VO_Category")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "VO_Category_Name");
            Map(x => x.Type, "VO_Category_Value").CustomType(typeof(CategoryTypeOfSupportEnum)).Nullable();
        }
    }
}