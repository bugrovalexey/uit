﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class SignStatusMap : ClassMapBase<SignStatus>
	{
        public SignStatusMap()
            : base("Sign_Status")
		{
//            Table("w_");

//            Id(x => x.Id, "Sign_Status_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            SqlInsert(@"exec ZZZ_Prc_Sign_Status_Insert
//						  @Sign_Status_Name = ?
//						 ,@Sign_Status_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Sign_Status_Update
//						  @Sign_Status_Name = ?
//						 ,@Sign_Status_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Sign_Status_Delete
//						  @Sign_Status_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Sign_Status_Name").Nullable();
        }
    }
}
