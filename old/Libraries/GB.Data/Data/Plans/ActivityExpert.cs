﻿using GB.Data.Common;

namespace GB.Data.Plans
{
    /// <summary>
    /// Пользователи проводящие зкспертизу мероприятия
    /// </summary>
    public class ActivityExpert : EntityBase
    {
        public ActivityExpert()
        {
        }

        public ActivityExpert(ActivityExpertiseConclusion conclusion)
        {
            this.Conclusion = conclusion;
        }

        /// <summary>Пользователь осуществляющий экспертизу</summary>
        public virtual User Expert { get; set; }

        /// <summary>Роль эксперта</summary>
        public virtual ActivityExpertEnum Role { get; set; }

        public virtual ActivityExpertiseConclusion Conclusion { get; set; }
    }
}