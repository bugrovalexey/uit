﻿namespace bgTeam.IsRui.Common
{
    public class IsRuiEditException : IsRuiException
    {
        public IsRuiEditException()
            : base("Вы не можете редактировать эту запись")
        {
        }
    }
}
