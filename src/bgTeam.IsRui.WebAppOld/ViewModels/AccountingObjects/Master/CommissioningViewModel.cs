﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class CommissioningViewModel : StepBaseViewModel
    {
        #region consts

        private const string DocNameCaption = "Наименование";
        private const string DocNumberCaption = "Номер документа";
        private const string DocDateAdoptionCaption = "Дата подписания";
        private const string DocParagraphCaption = "Пункт, статья документа";
        private const string CommissioningDateCaption = "Дата ввода в эксплуатацию";
        private const string FilesDataCaption = "Документ";

        #endregion consts

        public CommissioningViewModel(int id)
            : base(id, MasterItemEnum.Commissioning)
        {
            CommissioningFilesData =
                Current.DocumentCommissioning.With(dc => dc.Documents).Return(
                    docs => docs.Select(x => new SelectEntityInfo() {Id = x.Id, Name = x.Name}),
                    new List<SelectEntityInfo>());
        }

        [DisplayName(DocNameCaption)]
        public string CommissioningDocName
        {
            get { return Current.DocumentCommissioning.Return(dc => dc.Name, default(string)); }
        }

        [DisplayName(DocNumberCaption)]
        public string CommissioningDocNumber
        {
            get { return Current.DocumentCommissioning.Return(dc => dc.Number, default(string)); }
        }

        [DisplayName(DocParagraphCaption)]
        public string CommissioningDocParagraph
        {
            get { return Current.DocumentCommissioning.Return(dc => dc.Paragraph, default(string)); }
        }

        [DisplayName(DocDateAdoptionCaption)]
        public string CommissioningDocDateAdoption
        {
            get { return Current.DocumentCommissioning.Return(dc => dc.DateAdoption.ToStringOrDefault(), default(string)); }
        }

        [DisplayName(CommissioningDateCaption)]
        public string CommissioningDate
        {
            get { return Current.CommissioningDate.ToStringOrDefault(); }
        }

        [DisplayName(FilesDataCaption)]
        public IEnumerable<IEntityName> CommissioningFilesData { get; set; }

    }
}