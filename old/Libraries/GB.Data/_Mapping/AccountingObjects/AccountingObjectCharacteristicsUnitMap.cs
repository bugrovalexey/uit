﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Uviri.MKRF.Entities.AccountingObjects;

namespace Uviri.MKRF.Mapping.AccountingObjects
{
    public class AccountingObjectCharacteristicsUnitMap : ClassMapBase<AccountingObjectCharacteristicsUnit>
    {
        public AccountingObjectCharacteristicsUnitMap()
            : base("OU_Charact_MU")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Charact_MU_Name");
        }
    }
}
