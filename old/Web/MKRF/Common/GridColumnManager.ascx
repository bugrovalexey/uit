﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GridColumnManager.ascx.cs" Inherits="GB.MKRF.Web.Common.GridColumnManager" %>

<script language="javascript" type="text/javascript">
    function ShowCustWindow_Button_Click(grid) {
        if (grid.IsCustomizationWindowVisible())
            grid.HideCustomizationWindow();
        else
            grid.ShowCustomizationWindow();
    }

    function ClearCookies_Button_Click(grid) {
        grid.PerformCallback('ClearCookies');

        <%=OnPreClickClearButtom%>
    }
</script>
    
<div class="columnsSettingControl" style="<%=this.UserStyle%>" >
<dxhf:ASPxHiddenField runat="server" ID="_hfGridName" ClientInstanceName="hfGridName" />
<a href="javascript:ShowCustWindow_Button_Click(<%=this.GridName %>);" 
    title="вы можете настроить отображение нужных вам полей, перетаскивая их из списка в шапку таблицы и наоборот"
    style="font-size: x-small; white-space:nowrap;"><img src="/Content/GB/Styles/Imgs/settings.png" title="Настроить таблицу" alt="Настроить таблицу" /></a>
<% if (Grid != null) {%>
<a href="javascript:ClearCookies_Button_Click(<%=this.GridName %>);" 
    title="Сбросить настойки отображения таблицы по умолчанию"
    style="font-size: x-small; white-space:nowrap;"><img src="/Content/GB/Styles/Imgs/clear_16.png" title="Сбросить настройки" alt="Сбросить настройки" /></a>
<%} %>
</div>

