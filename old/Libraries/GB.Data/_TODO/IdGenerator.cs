﻿using GB.Data;
using NHibernate;
using NHibernate.Id;
using NHibernate.Impl;
using System.Collections.Generic;
using System.Data;

namespace GB._TODO
{
    /// <summary>
    /// Генератор идентификатора для новой сущности
    /// </summary>
    internal class IdGenerator : IIdentifierGenerator, IConfigurable
    {
        //private static object Lock = new object();

        private string TableName;

        public void Configure(NHibernate.Type.IType type, IDictionary<string, string> parms, NHibernate.Dialect.Dialect dialect)
        {
            parms.TryGetValue(PersistentIdGeneratorParmsNames.PK, out this.TableName);
            if (this.TableName == null || !TableName.EndsWith("_ID"))
            {
                throw new MappingException("no table name");
            }

            TableName = TableName.Substring(0, TableName.Length - 3);
        }

        public object Generate(NHibernate.Engine.ISessionImplementor session, object obj)
        {
            EntityBase entity = obj as EntityBase;

            if (entity != null && entity.Id > 0)
                return entity.Id;

            SessionImpl si = session as SessionImpl;

            //Logger.Debug("Получить id");

            int id;

            using (IDbCommand command = session.Connection.CreateCommand())
            {
                si.Transaction.Enlist(command);

                id = GetNewId(command, TableName);
            }

            //Logger.Debug("id - получен");

            return id;
        }

        public static int GetNewId(IDbCommand command, string tableName)
        {
            command.CommandText = @"dbo.Get_Last_ID";
            command.CommandType = CommandType.StoredProcedure;

            IDbDataParameter seqVal = command.CreateParameter();
            seqVal.ParameterName = "SeqVal_Name";
            seqVal.Value = tableName;
            command.Parameters.Add(seqVal);

            IDbDataParameter parId = command.CreateParameter();
            parId.Direction = ParameterDirection.InputOutput;
            parId.ParameterName = "ResVal";
            parId.DbType = DbType.Int32;
            parId.Value = 0;
            command.Parameters.Add(parId);

            //lock (Lock)
            //{
            //    Logger.Debug(System.Threading.Thread.CurrentThread.Name + " получить id");

            command.ExecuteNonQuery();

            //    Logger.Debug(System.Threading.Thread.CurrentThread.Name + " готово");
            //}

            return (int)parId.Value;
        }
    }

    //DONE : Избавиться от этого класса
    //public class IdGeneratorPublic
    //{
    //    public static int GetNewId(IDbCommand command, string tableName)
    //    {
    //        return IdGenerator.GetNewId(command, tableName);
    //    }
    //}
}