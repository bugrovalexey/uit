﻿using bgTeam.IsRui.Common;
using System;
using System.Web;

namespace bgTeam.IsRui.WebApp.Helpers
{
    public static class ResponseHelper
    {
        /// <summary>
        /// Запрет доступа( прекращается построение страницы, клиенту шлется 403 ошибка)
        /// </summary>
        /// <param name="response"></param>
        public static void DenyAccessToPage(HttpResponseBase response)
        {
            response.Redirect("~/Error/AccessToPageError");
        }

        public static void DenyAccessToPage(HttpResponse response)
        {
            response.Redirect("~/Error/AccessToPageError");
        }

        /// <summary>
        /// Запрет доступа( прекращается построение страницы, клиенту шлется 403 ошибка)
        /// </summary>
        /// <param name="response"></param>
        public static void DenyAccessToObject(HttpResponseBase response)
        {
            response.Redirect("~/Error/AccessToObjectError");
        }

        public static void Error(HttpResponse response)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Отсутствие пользователя
        /// </summary>
        /// <param name="response"></param>
        public static void IncorrectUser(HttpResponse response)
        {
            throw new NotImplementedException();
        }

        public static void FileNotFound(HttpResponseBase response, string logMessage = null)
        {
            response.Redirect("~/Error/FileError");
        }

        public static void FileNotFound(HttpResponse response, string logMessage = null)
        {
            response.Redirect("~/Error/FileError");
        }

        public static void AccessDenied(HttpResponse response, string logMessage = null)
        {
            EndResponse(response, 403, logMessage: logMessage);
        }

        static void EndResponse(HttpResponse response, int statusCode, string status = null, string logMessage = null)
        {
            if (!string.IsNullOrWhiteSpace(logMessage))
            {
                Logger.Error(logMessage);
            }
            response.StatusCode = statusCode;
            if (status != null)
                response.Status = status;
            //response.TrySkipIisCustomErrors = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            //response.End();
        }
    }
}