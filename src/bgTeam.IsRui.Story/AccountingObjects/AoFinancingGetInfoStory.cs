﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoFinancingGetInfoStory : IStory<AoFinancingGetInfoStoryContext, FinancingDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;

        public AoFinancingGetInfoStory(IRepositoryRui repository,IMapperBase mapper)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public FinancingDto Execute(AoFinancingGetInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<FinancingDto> ExecuteAsync(AoFinancingGetInfoStoryContext context)
        {
            var ao = _repository.Get<AccountingObject>(context.AoId);

            return _mapper.Map(ao, new FinancingDto());
        }
    }
}
