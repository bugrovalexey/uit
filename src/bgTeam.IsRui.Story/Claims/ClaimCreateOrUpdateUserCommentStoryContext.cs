﻿namespace bgTeam.IsRui.Story.Claims
{
    using System;

    public class ClaimCreateOrUpdateUserCommentStoryContext
    {
        public int? Id { get; set; }

        /// <summary>ОУ</summary>
        public int? ClaimId { get; set; }

        /// <summary>Комментарий</summary>
        public string Text { get; set; }
    }
}
