﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Activities;
using System.Web.Mvc;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.Controllers
{
    partial class ActivitiesController
    {
        public ActionResult Expertise(int id)
        {
            return View(new ExpertiseViewModel(_userRepository, _statusRepository, id));
        }

        public ActionResult ExpertiseEdit(int conclusionId, int? id)
        {
            return View(new ExpertiseEditViewModel(conclusionId, id));
        }
    }
}