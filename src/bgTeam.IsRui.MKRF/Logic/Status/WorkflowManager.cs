﻿using bgTeam.IsRui.Domain.Entities;

namespace bgTeam.IsRui.MKRF.Logic.Status
{
    public class WorkflowManager : ManagerBase<IWorkflowHandler, IWorkflowObject>
    {
        protected WorkflowManager() { }
    }
}