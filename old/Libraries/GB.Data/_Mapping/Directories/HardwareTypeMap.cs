﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class HardwareTypeMap : ClassMapBase<HardwareType>
    {
        public HardwareTypeMap()
            : base("Hardware_Type")
        {
            //Table("w_");

            //Id(x => x.Id, "Hardware_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Hardware_Type_Name");
            Map(x => x.NameSmall, "Hardware_Type_Sname");
            Map(x => x.Comment, "Hardware_Type_Comm");
        }
    }
}
