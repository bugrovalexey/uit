﻿using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Purchase;
using bgTeam.IsRui.Domain.Entities.Workflow;
using System.Collections.Generic;

namespace bgTeam.IsRui.Domain.Entities.Plans
{
    /// <summary>Работа Плана</summary>
    public class PlansWork : EntityNumerable
    {
        /// <summary>Мероприятия Плана</summary>
        public virtual PlansActivity PlanActivity { get; set; }

        /// <summary>Номер, склеенный с номером мероприятия</summary>
        public virtual string ActivityWorkNum
        {
            get
            {
                string activityNum = PlanActivity.ActivityNum.Remove(2);
                string workNum = "";
                if (OrigNum < 10)
                    workNum = "0" + OrigNum;
                if (OrigNum >= 10)
                    workNum = OrigNum.ToString();
                return string.Format("{0}{1}", activityNum, workNum);
            }
        }

        /// <summary>Наименование + Номер</summary>
        public virtual string NameNum
        {
            get
            {
                return string.Format("{0} - {1}", ActivityWorkNum, Name);
            }
        }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null

        /// <summary>Результаты предыдущих периодов, с указанием года реализации, номера(ов) ГК</summary>
        public virtual string Res { get; set; }//null

        /// <summary>Ведомство</summary>
        public virtual Department GovtOrgan { get; set; }//null

        /// <summary>КБК-секция</summary>
        public virtual SectionKBK SectionKBK { get; set; }

        /// <summary>Элемент расходов</summary>
        public virtual ExpenseItem ExpenseItem { get; set; }

        /// <summary> Код по БК КОСГУ </summary>
        public virtual ExpenditureItem ExpenditureItem { get; set; }

        /// <summary>Объем расходов из Федерального Бюджета</summary>
        public virtual double ExpenseVolumeFB { get; set; }

        /// <summary>Объем расходов из Регионального Бюджета</summary>
        public virtual double ExpenseVolumeRB { get; set; }

        /// <summary>Объем расходов из Внебюджетных источников</summary>
        public virtual double ExpenseVolumeVB { get; set; }

        /// <summary></summary>
        public virtual int FormID { get; set; }

        /// <summary>Вид расходов</summary>
        public virtual ExpenseDirection ExpenseDirection { get; set; }

        /// <summary>
        /// Вид расходов, отчет по плану
        /// </summary>
        public virtual ExpenseDirection ReportExpenseDirection { get; set; }

        /// <summary>Тип расходов</summary>
        public virtual ExpenseType ExpenseType { get; set; }

        /// <summary>Тип расходов в отчете</summary>
        public virtual ExpenseType ReportExpenseType { get; set; }

        /// <summary>Тип работы</summary>
        // public virtual WorkType WorkType { get; set; }

        public virtual IList<WorkType> WorkType { get; set; }

        /// <summary>Подпрограмма</summary>
        public virtual string SubEntry { get; set; }//null

        /// <summary>Комментарий эксперта</summary>
        public virtual string Comm { get; set; }//null

        /// <summary>Фактический объем расходов из Федерального Бюджета</summary>
        public virtual double ExpenseVolumeFB_Fact { get; set; }

        /// <summary>Фактический объем расходов из Регионального Бюджета</summary>
        public virtual double ExpenseVolumeRB_Fact { get; set; }

        /// <summary>Фактический объем расходов из Внебюджетных источников</summary>
        public virtual double ExpenseVolumeVB_Fact { get; set; }

        /// <summary>Номер в реестре ФГИС</summary>
        //public virtual FgisInfo FgisInfo { get; set; }

        /// <summary>Номер программного решения в ФАП</summary>
        //public virtual ApfInfo ApfInfo { get; set; }

        /// <summary>ГРБС</summary>
        public virtual GRBS GRBS { get; set; }//null

        /// <summary>Форма работы</summary>
        public virtual WorkForm WorkForm { get; set; }//null

        /// <summary>Основной объем/Федеральный бюджет/Очередной год</summary>
        public virtual double ExpenseVolFBY0 { get; set; }

        /// <summary>Основной объем/Федеральный бюджет/Первый год</summary>
        public virtual double ExpenseVolFBY1 { get; set; }

        /// <summary>Основной объем/Федеральный бюджет/Второй год</summary>
        public virtual double ExpenseVolFBY2 { get; set; }

        /// <summary>Дополнительная потребность/Федеральный бюджет/Очередной год</summary>
        public virtual double ExpenseAddVolFBY0 { get; set; }

        /// <summary>Дополнительная потребность/Федеральный бюджет/Первый год</summary>
        public virtual double ExpenseAddVolFBY1 { get; set; }

        /// <summary>Дополнительная потребность/Федеральный бюджет/Второй год</summary>
        public virtual double ExpenseAddVolFBY2 { get; set; }

        /// <summary>Основной объем/Внебюджетный фонд/Очередной год</summary>
        public virtual double ExpenseVolVBY0 { get; set; }

        /// <summary>Основной объем/Внебюджетный фонд/Первый год</summary>
        public virtual double ExpenseVolVBY1 { get; set; }

        /// <summary>Основной объем/Внебюджетный фонд/Второй год</summary>
        public virtual double ExpenseVolVBY2 { get; set; }

        /// <summary>Дополнительная потребность/Внебюджетный фонд/Очередной год</summary>
        public virtual double ExpenseAddVolVBY0 { get; set; }

        /// <summary>Дополнительная потребность/Внебюджетный фонд/Первый год</summary>
        public virtual double ExpenseAddVolVBY1 { get; set; }

        /// <summary>Дополнительная потребность/Внебюджетный фонд/Второй год</summary>
        public virtual double ExpenseAddVolVBY2 { get; set; }

        /// <summary>ОКПД</summary>
        public virtual OKPD OKPD { get; set; }//null

        /// <summary>ОКВЭД</summary>
        public virtual OKVED OKVED { get; set; } //null

        /// <summary>Год</summary>
        public virtual YearEnum? Year { get; set; }

        /// <summary>Число специалистов</summary>
        public virtual int? SpecialistsCount { get; set; } //null

        /// <summary>Среднемесячная зарплата</summary>
        public virtual decimal Salary { get; set; } //NOT NULL

        /// <summary>Длительность работ, мес.</summary>
        public virtual int? Duration { get; set; } //null

        /// <summary>Статус работ</summary>
        public virtual Status Status { get; set; }//null

        /// <summary>Планируемые результаты/Очередной год</summary>
        public virtual string ResultY0 { get; set; }

        /// <summary>Планируемые результаты/Первый год</summary>
        public virtual string ResultY1 { get; set; }

        /// <summary>Планируемые результаты/Второй год</summary>
        public virtual string ResultY2 { get; set; }

        /// <summary>Согласованные Основной объем/ФБ/Очередной год</summary>
        public virtual double AgreedExpenseVolY0 { get; set; }

        /// <summary>Согласованные Основной объем/ФБ/Первый год</summary>
        public virtual double AgreedExpenseVolY1 { get; set; }

        /// <summary>Согласованные Основной объем/ФБ/Второй год</summary>
        public virtual double AgreedExpenseVolY2 { get; set; }

        /// <summary>Согласованные Дополнительная потребность/ФБ/Очередной год</summary>
        public virtual double AgreedExpenseAddVolY0 { get; set; }

        /// <summary>Согласованные Дополнительная потребность/ФБ/Первый год</summary>
        public virtual double AgreedExpenseAddVolY1 { get; set; }

        /// <summary>Согласованные Дополнительная потребность/ФБ/Второй год</summary>
        public virtual double AgreedExpenseAddVolY2 { get; set; }

        /// <summary>Согласованные Основной объем/ВБ/Очередной год</summary>
        public virtual double AgreedExpenseVolVBY0 { get; set; }

        /// <summary>Согласованные Основной объем/ВБ/Первый год</summary>
        public virtual double AgreedExpenseVolVBY1 { get; set; }

        /// <summary>Согласованные Основной объем/ВБ/Второй год</summary>
        public virtual double AgreedExpenseVolVBY2 { get; set; }

        /// <summary>Согласованные Дополнительная потребность/ВБ/Очередной год</summary>
        public virtual double AgreedExpenseAddVolVBY0 { get; set; }

        /// <summary>Согласованные Дополнительная потребность/ВБ/Первый год</summary>
        public virtual double AgreedExpenseAddVolVBY1 { get; set; }

        /// <summary>Согласованные Дополнительная потребность/ВБ/Второй год</summary>
        public virtual double AgreedExpenseAddVolVBY2 { get; set; }

        //=======================PlanReport========================

        /// <summary>Фактические результаты работы</summary>
        public virtual string PRSFactRes { get; set; }

        /// <summary>Должны быть продолжены в следующем периоде</summary>
        public virtual bool PRSNextPeriod { get; set; }

        /// <summary>Номер реестровой записи</summary>
        public virtual string ReestrNumber { get; set; }

        /// <summary>Реквизиты актов приемки</summary>
        public virtual string DetailsActs { get; set; }

        /// <summary>Объем бюджетных ассигнований, Факт</summary>
        public virtual double FactBuget { get; set; }

        /// <summary>Реквизиты госконтракта</summary>
        public virtual string PRSContract { get; set; }

        /// <summary>Наименование подрядчика</summary>
        public virtual string PRSContractor { get; set; }

        //===================================================Plan2012new=================================================================

        /// <summary>Объем бюджетных ассигнований очередной финансовый год</summary>
        public virtual double ExpenseVolumeCurrentYear { get; set; }

        /// <summary>Объем бюджетных ассигнований на первый год планового периода</summary>
        public virtual double ExpenseVolumeFirstYear { get; set; }

        /// <summary>Объем бюджетных ассигнований на второй год планового периода</summary>
        public virtual double ExpenseVolumeSecondYear { get; set; }

        public virtual Contract Contract { get; set; }

        /// <summary>
        /// комментарий для отчета по плану
        /// </summary>
        public virtual string Comment { get; set; }

        /// <summary>
        /// Фактический объем бюджетных ассигнований, раздел 3.1, отчет по плану
        /// </summary>
        public virtual decimal BudgetVolume { get; set; }

        /// <summary>
        /// Результат, раздел 3.1, отчет по плану
        /// </summary>
        public virtual string Result { get; set; }

        /// <summary>
        /// Номер для кода
        /// </summary>
        public virtual int OrigNum { get; set; }

        /// <summary>черновик</summary>
        public virtual bool IsDraft { get; set; }
    }
}