﻿namespace GB.DataAccess.Services.Args
{
    /// <summary>
    /// Ответственные
    /// </summary>
    public class AccountingObjectResponsiblesArgs : BaseArgsNew
    {
        /// <summary>
        /// Учреждение, уполномоченное на создание,
        /// развитие ОУ
        /// </summary>
        public int? DepartmentId { get; set; }

        #region Раздел "Сведения об ответственных должностных лицах"

        /// <summary>
        /// Ответственный за координацию мероприятий
        /// по информатизации
        /// </summary>
        public int? ResponsibleCoordId { get; set; }

        /// <summary>
        /// Ответственный за обеспечение
        /// эксплуатации
        /// </summary>
        public int? ResponsibleExploitationId { get; set; }

        /// <summary>
        /// Ответственный за организацию
        /// закупок
        /// </summary>
        public int? ResponsiblePurchaseId { get; set; }

        /// <summary>
        /// Ответственный за размещение сведений
        /// об объекте учета
        /// </summary>
        public int? ResponsibleInformationId { get; set; }

        #endregion Раздел "Сведения об ответственных должностных лицах"

        #region Раздел "Сведения о фактическом месторасположении объекта учета"

        /// <summary>
        /// Наименование организации
        /// </summary>
        public string LocationOrgName { get; set; }

        /// <summary>
        /// ФИО ответственного
        /// </summary>
        public string LocationFioResponsible { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string LocationOrgAddress { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string LocationOrgPhone { get; set; }

        #endregion Раздел "Сведения о фактическом месторасположении объекта учета"
    }
}