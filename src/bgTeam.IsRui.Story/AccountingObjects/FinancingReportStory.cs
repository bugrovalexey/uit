﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.Common.Helpers;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;

    public class FinancingReportStory : IStory<FinancingReportStoryContext, string>//FinanceTemplateDto>
    {
        private readonly IRepositoryRui _repository;

        public FinancingReportStory(IRepositoryRui repository)
        {
            _repository = repository;
        }

        public string Execute(FinancingReportStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<string> ExecuteAsync(FinancingReportStoryContext context)
        {
            string strings;
            using (StreamReader sr = new StreamReader(@"Templates\шаблон08.txt"))
            {
                strings = sr.ReadToEnd();
            }

            if (context.PositionsBlocks == null)
            {
                context = GetData();
            }

            var blocksResult = new StringBuilder();

            foreach (TemplateBlockDto blockPos in context.PositionsBlocks)
            {
               var block = BuildPositions(blockPos);

                blocksResult.Append(block);
            }

            var replaceBlocks = strings.Replace("$BlockN$", blocksResult.Replace(",", ".").ToString());

            var blocksSummResult = new StringBuilder();

            foreach (TemplateBlockDto blockPos in context.PositionsBlocks)
            {
                var block = BuildPositionsSumm(blockPos);

                blocksSummResult.Append(block);
            }

            var replaceBlocksSumm = replaceBlocks.Replace("$BlockNResalt$", blocksSummResult.Replace(",", ".").ToString());

            decimal qurt1SummAll = 0;
            decimal qurt2SummAll = 0;
            decimal qurt3SummAll = 0;
            decimal qurt4SummAll = 0;

            foreach (TemplateBlockDto blockPos in context.PositionsBlocks)
            {
                foreach (PositionsTemplateDto str in blockPos.Positions)
                {
                    qurt1SummAll += str.Quart1;
                    qurt2SummAll += str.Quart2;
                    qurt3SummAll += str.Quart3;
                    qurt4SummAll += str.Quart4;

                }
            }

            var resultStringQ1 = replaceBlocksSumm.Replace("$1quartSumm$", qurt1SummAll.ToString().Replace(",", "."));
            var resultStringQ2 = resultStringQ1.Replace("$2quartSumm$", qurt2SummAll.ToString().Replace(",", "."));
            var resultStringQ3 = resultStringQ2.Replace("$3quartSumm$", qurt3SummAll.ToString().Replace(",", "."));
            var resultStringQ4 = resultStringQ3.Replace("$4quartSumm$", qurt4SummAll.ToString().Replace(",", "."));
            var resultStringQ5 = resultStringQ4.Replace("$resultSumm$", (qurt1SummAll + qurt2SummAll + qurt3SummAll + qurt4SummAll).ToString().Replace(",", "."));

            context.Title = "\"ИНФОРМАТИЗАЦИЯ\"";
            var resultStringTitle = resultStringQ5.Replace("$Title$", context.Title);
            context.Year = DateTime.Today.Year.ToString();
            var resultStringYear = resultStringTitle.Replace("$Year$", context.Year);
            context.TitleYaer = DateTime.Today.ToLongDateString();
            var resultStringTitleYear = resultStringYear.Replace("$titleDate$ ", context.TitleYaer);

            return resultStringTitleYear;
        }

        public StringBuilder BuildPositions(TemplateBlockDto block)
        {
            var result = new StringBuilder();



            result.Append("<tr><td class=\"column0 style3 s style3\" colspan=\"10\">" + block.Title + "</td></tr>");

            foreach (PositionsTemplateDto str in block.Positions)
            {
                var valueId = "<td>" + str.Id + "</td>";
                var valueSubDivision = "<td>" + str.SubDivision + "</td>";
                var valueArticle = "<td>" + str.Article + "</td>";
                var valueAccountingObject = "<td>" + str.AccountingObject + "</td>";
                var valueName = "<td>" + str.Name + "</td>";
                var valueQuart1 = "<td>" + str.Quart1 + "</td>";
                var valueQuart2 = "<td>" + str.Quart2 + "</td>";
                var valueQuart3 = "<td>" + str.Quart3 + "</td>";
                var valueQuart4 = "<td>" + str.Quart4 + "</td>";
                decimal summValue = str.Quart1 + str.Quart2 + str.Quart3 + str.Quart4;
                var summ = "<td>" + summValue + "</td>";

                var row = "<tr>" + valueId + valueSubDivision + valueArticle + valueAccountingObject + valueName + valueQuart1 + valueQuart2 + valueQuart3 + valueQuart4 + summ + "</tr>";

                result.Append(row);

                row = string.Empty;
            }

            return result;
        }

        public StringBuilder BuildPositionsSumm(TemplateBlockDto block)
        {
            decimal qurt1Summ = 0;
            decimal qurt2Summ = 0;
            decimal qurt3Summ = 0;
            decimal qurt4Summ = 0;

            foreach (PositionsTemplateDto str in block.Positions)
            {
                qurt1Summ += str.Quart1;
                qurt2Summ += str.Quart2;
                qurt3Summ += str.Quart3;
                qurt4Summ += str.Quart4;
            }

            var result = new StringBuilder();

            result.Append("<tr>");
            result.Append("<td colspan=\"5\">" + block.Title + "</td>");
            result.Append("<td>" + qurt1Summ + "</td>");
            result.Append("<td>" + qurt2Summ + "</td>");
            result.Append("<td>" + qurt3Summ + "</td>");
            result.Append("<td>" + qurt4Summ + "</td>");
            var summRes = qurt1Summ + qurt2Summ + qurt3Summ + qurt4Summ;
            result.Append("<td>" + summRes + "</td>");
            result.Append("</tr>");

            return result;
        }

        public FinancingReportStoryContext GetData()
        {

            var data = new FinancingReportStoryContext() { PositionsBlocks = new List<TemplateBlockDto>() };

            var aoList = _repository.GetAll<AccountingObject>();

            var articleList = _repository.GetAll<AoArticle>();

            int countId = 0;

            foreach (AoArticle article in articleList)
            {
                var aoBlock = aoList.Where(q => q.Article == article);

                var block = new TemplateBlockDto() { Title = article.Name, Positions = new List<PositionsTemplateDto>() };

                foreach (AccountingObject ao in aoBlock)
                {
                    var position = new PositionsTemplateDto();

                    countId++;

                    position.Id = countId;

                    position.SubDivision = ao.Department.Name;

                    position.Article = ao.Article.Name;

                    position.AccountingObject = ao.Kind.Name;

                    position.Name = ao.FullName;

                    if (ao.FinancingQuarter1 != null)
                    {
                        position.Quart1 = ao.FinancingQuarter1.Value;
                    }

                    if (ao.FinancingQuarter2 != null)
                    {
                        position.Quart2 = ao.FinancingQuarter2.Value;
                    }

                    if (ao.FinancingQuarter3 != null)
                    {
                        position.Quart3 = ao.FinancingQuarter3.Value;
                    }

                    if (ao.FinancingQuarter4 != null)
                    {
                        position.Quart4 = ao.FinancingQuarter4.Value;
                    }

                    block.Positions.Add(position);
                }

                if (block.Positions.Count > 0)
                {
                    data.PositionsBlocks.Add(block);
                }
            }

            return data;
        }
    }
}
