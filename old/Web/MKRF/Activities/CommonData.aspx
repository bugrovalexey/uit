﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Activities/EditCard.master" AutoEventWireup="true" CodeBehind="CommonData.aspx.cs" Inherits="GB.MKRF.Web.Activities.CommonData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <style>
        table.a_cd .form_edit_desc {
            width: 320px;
        }

        table.a_cd_ou .form_edit_desc {
            width: 220px;
        }
    </style>

    <script type="text/javascript">
        function validateSelectBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && e.value != 'Не задано';
        }

        function validateComboBox(s, e) {
            e.isValid = (e.value != undefined || e.value) && e.value != 0;
        }

        function FormSave() {
            hf.Set('ActivityFiles', FileUploader.GetFiles());
        }

        function SaveButtonClick(s, e) {
            if (ASPxClientEdit.ValidateGroup('CommonData')) {
                FormSave();
                MainLoadingPanel.Show();
                callBack.PerformCallback('');
                ShowConfirm(false);
            }
        }

        function ReasonsSave() {
            hf.Set('ReasonName', ReasonNPANameTextBox.GetText());
            hf.Set('ReasonTypeId', ReasonNPATypeComboBox.GetValue());
            hf.Set('ReasonNum', ReasonNPANumTextBox.GetText());
            hf.Set('ReasonDate', ReasonNPADateDateEdit.GetValue());
            hf.Set('ReasonItem', ReasonItemTextBox.GetText());
            hf.Set('ReasonFiles', uploader2.GetFiles());
        }

        function SelectValue(e) {
            PopupControl.SetContentHtml('');

            PopupControl.SetHeaderText('Выберите - Ответственного');

            switch (e) {
                case 'RI':
                    PopupControl.SetContentUrl('<%=GetResponsibleUrl("ResponsibleInformation") %>');
                    break;
                case 'RT':
                    PopupControl.SetContentUrl('<%=GetResponsibleUrl("ResponsibleTechnical") %>');
                    break;
                case 'RF':
                    PopupControl.SetContentUrl('<%=GetResponsibleUrl("ResponsibleFinancial") %>');
                    break;
            }

            PopupControl.RefreshContentUrl();
            PopupControl.Show();
        };

        function UOSelectValue(e) {

            //if()
            //{
            //    OUPopupControl.Show();
            //}
            //else
            //{
            PopupControl.SetHeaderText('Выберите - Объект учета');
            PopupControl.SetContentUrl('<%=GetOUUrl() %>');
                PopupControl.RefreshContentUrl();
                PopupControl.Show();
            //}
            };

            function PopupClose(type, result) {
                PopupControl.Hide();
                var mass = result.split(';');
                if (mass[0]) hf.Set(type + 'Id', mass[0]);
                if (mass[1]) hf.Set(type + 'Name', mass[1]);
                var Memo = '#' + type + 'TextBox';
                $(Memo).find('input').val(hf.Get(type + 'Name'));
                UpdateButtonClear(type);
                ShowConfirm(true);

                PopupControl.SetContentHtml('');
            };

            function UpdateButtonClear(name) {
                var val = hf.Get(name + 'Id');

                if (val && val != -1) {
                    var btName = '#' + name + 'TextBox';
                    var bt = $(btName).parents('td.form_edit_input').find('div.ButtonClear');

                    bt.css('display', '');
                }
            }

            function ClearValue(s, name) {
                if (hf.Contains(name + 'Id')) {
                    ShowConfirm(true);
                }

                hf.Set(name + 'Id', '-1');
                hf.Set(name + 'Name', '');

                var Memo = '#' + name + 'TextBox';
                $(Memo).find('input').val('Не задано');

                $(s).css('display', 'none');
            };

            function ShowConfirm(isChange) {
                if (isChange) {
                    window.onbeforeunload = function (e) {
                        return "Внесенные на странице данные не сохранены.";
                    };
                } else {
                    window.onbeforeunload = null;
                }
            }

            //function OUPopupControl_Save()
            //{
            //    hf.Set('UOName', UONameTextBox.GetText());
            //    hf.Set('UOSName', UONameSmallTextBox.GetText());
            //    hf.Set('OUType', OUTypeComboBox.GetValue());
            //    hf.Set('OUKind', OUKindComboBox.GetValue());

            //    ou_callBack.PerformCallback('');
            //}

            //function OUPopupControl_Close()
            //{
            //    OUPopupControl.Hide();
            //}

            $(document).ready(function () {
                UpdateButtonClear('ResponsibleInformation');
                UpdateButtonClear('ResponsibleTechnical');
                UpdateButtonClear('ResponsibleFinancial');
            });
    </script>

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" SyncWithServer="true" />
    <dxpc:ASPxPopupControl ID="PopupControl" runat="server" AllowDragging="True" AllowResize="True"
        CloseAction="CloseButton" Modal="True" EnableViewState="False"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="600px"
        ClientInstanceName="PopupControl" EnableHierarchyRecreation="True">
    </dxpc:ASPxPopupControl>

    <div class="panel_button">
        <dxe:ASPxButton ID="saveButton" runat="server" AutoPostBack="false" Text="Сохранить" ValidationGroup="CommonData" CssClass="fleft topButton save_button">
            <ClientSideEvents Click="SaveButtonClick" />
        </dxe:ASPxButton>
    </div>
    <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" OnCallback="callBack_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { MainLoadingPanel.Hide(); }"
            CallbackError="function(s,e) { MainLoadingPanel.Hide(); }" />
    </dxc:ASPxCallback>

    <table class="form_edit a_cd">
        <tr>
            <td class="form_edit_desc">Учреждение ответственное за мероприятие
            </td>
            <td class="form_edit_input">
                <dxe:ASPxLabel runat="server" ID="DepCheckBox" OnDataBinding="DepCheckBox_DataBinding" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc required">Ответственный за размещение сведений
            </td>
            <td class="form_edit_input">
                <div style="position: relative">
                    <dxe:ASPxTextBox ID="ResponsibleInformationTextBox" runat="server" ClientInstanceName="ResponsibleInformationTextBox" ClientIDMode="Static"
                        OnDataBinding="ResponsibleInformationTextBox_DataBinding" ReadOnly="true">
                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData" />
                        <ClientSideEvents Validation="function(s,e){ validateSelectBox(s, e); }" />
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('RI'); return false;"></div>
                    <div class="ButtonClear catalogButton" style="display: none;" onclick="ClearValue(this, 'ResponsibleInformation'); return false;"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">Ответственный сотрудник технической службы
            </td>
            <td class="form_edit_input">
                <div style="position: relative">
                    <dxe:ASPxTextBox ID="ResponsibleTechnicalTextBox" runat="server" ClientInstanceName="ResponsibleTechnicalTextBox" ClientIDMode="Static"
                        OnDataBinding="ResponsibleTechnicalTextBox_DataBinding" ReadOnly="true">
                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData" />
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('RT'); return false;"></div>
                    <div class="ButtonClear catalogButton" style="display: none;" onclick="ClearValue(this, 'ResponsibleTechnical'); return false;"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">Ответственный сотрудник финансовой службы
            </td>
            <td class="form_edit_input">
                <div style="position: relative">
                    <dxe:ASPxTextBox ID="ResponsibleFinancialTextBox" runat="server" ClientInstanceName="ResponsibleFinancialTextBox" ClientIDMode="Static"
                        OnDataBinding="ResponsibleFinancialTextBox_DataBinding" ReadOnly="true">
                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData" />
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('RF'); return false;"></div>
                    <div class="ButtonClear catalogButton" style="display: none;" onclick="ClearValue(this, 'ResponsibleFinancial'); return false;"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">Дополнительные материалы в рамках ФЭО
                <br />
                (Анализ рынка)
            </td>
            <td class="form_edit_input">
                <cc:FileUploadControl2 runat="server" ID="FileUploader" ClientInstanceName="FileUploader" AutoUpload="true" Mode="MultiFile"
                    AllowedFileExtensions=".pdf,.tiff,.tif,.jpg,.rar,.zip,.doc,.xls,.docx,.xlsx" OnDataBinding="FileUploader_DataBinding" />
            </td>
        </tr>
    </table>

    <p class="header_grid">
        Объект учета
    </p>
    <% if (IsOUEdit())
       {%>
    <table class="form_edit a_cd">
        <tr>
            <td class="form_edit_desc">Полное наименование:
            </td>
            <td class="form_edit_input">
                <dxe:ASPxMemo ID="UONameTextBox" runat="server" ClientInstanceName="UONameTextBox" OnDataBinding="UONameTextBox_DataBinding" />
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc required">Краткое наименование:
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox ID="UONameSmallTextBox" runat="server" ClientInstanceName="UONameSmallTextBox" OnDataBinding="UONameSmallTextBox_DataBinding">
                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData">
                        <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                    </ValidationSettings>
                </dxe:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc required">Тип ОУ:
            </td>
            <td class="form_edit_input">
                <dxe:ASPxComboBox ID="OUTypeComboBox" runat="server" ClientInstanceName="OUTypeComboBox"
                    TextField="Name" ValueField="Id" ValueType="System.Int32"
                    OnDataBinding="OUTypeComboBox_DataBinding">
                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData" />
                    <ClientSideEvents Validation="function(s,e){ validateComboBox(s, e);  }"
                        SelectedIndexChanged="function(s, e) { OUKindComboBox.PerformCallback(s.GetValue()); }" />
                </dxe:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc required">Вид ОУ:
            </td>
            <td class="form_edit_input">
                <dxe:ASPxComboBox ID="OUKindComboBox" runat="server" ClientInstanceName="OUKindComboBox"
                    TextField="Name" ValueField="Id" ValueType="System.Int32"
                    OnDataBinding="OUKindComboBox_DataBinding" OnCallback="OUKindComboBox_Callback">
                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData" />
                    <ClientSideEvents Validation="function(s,e){ validateComboBox(s, e); }" />
                </dxe:ASPxComboBox>
            </td>
        </tr>
    </table>
    <% }
       else
       { %>
    <table class="form_edit a_cd">
        <tr>
            <td class="form_edit_desc">Выбирите существующий:
            </td>
            <td class="form_edit_input">
                <div style="position: relative">
                    <dxe:ASPxTextBox ID="OUTextBox" runat="server" ClientInstanceName="OUTextBox" ClientIDMode="Static"
                        OnDataBinding="OUTextBox_DataBinding" ReadOnly="true">
                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="CommonData" />
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="UOSelectValue('OU'); return false;"></div>
                    <div class="ButtonClear catalogButton" style="display: none;" onclick="ClearValue(this, 'OU'); return false;"></div>
                </div>
            </td>
        </tr>
    </table>
    <% } %>

    <p class="header_grid">
        Документы основания реализации мероприятия
    </p>
    <dxe:ASPxButton ID="AddReasonsButton" runat="server"
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {ReasonsGrid.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="ReasonsGrid" Width="100%" ClientInstanceName="ReasonsGrid"
        KeyFieldName="Id"
        OnBeforePerformDataSelect="ReasonsGrid_BeforePerformDataSelect"
        OnRowInserting="ReasonsGrid_RowInserting"
        OnRowUpdating="ReasonsGrid_RowUpdating"
        OnStartRowEditing="ReasonsGrid_StartRowEditing"
        OnRowDeleting="ReasonsGrid_RowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False"
                AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" />
            <dxwgv:GridViewDataTextColumn FieldName="Id" ShowInCustomizationForm="False" Visible="False" />
            <dxwgv:GridViewDataTextColumn FieldName="Rekvisit" Caption="Реквизиты документа" />
            <dxwgv:GridViewDataTextColumn FieldName="Item" Caption="Пункт, статья документа" />
            <dxwgv:GridViewDataTextColumn ShowInCustomizationForm="False">
                <Settings AllowDragDrop="False" />
                <DataItemTemplate>
                    <%#getDownloadUrl((string)Eval("FileInfo"))%>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Вид документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox runat="server" ID="ReasonNPATypeComboBox" ClientInstanceName="ReasonNPATypeComboBox"
                                OnDataBinding="ReasonNPATypeComboBox_DataBinding" TextField="Value" ValueField="Key"
                                ValueType="System.Int32" EncodeHtml="false">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Наименование документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo runat="server" ID="ReasonNPANameTextBox" ClientInstanceName="ReasonNPANameTextBox" Rows="5"
                                OnDataBinding="ReasonNPANameTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Номер документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="ReasonNPANumTextBox" ClientInstanceName="ReasonNPANumTextBox"
                                OnDataBinding="ReasonNPANumTextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Дата документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="ReasonNPADateDateEdit" ClientInstanceName="ReasonNPADateDateEdit"
                                OnDataBinding="ReasonNPADateDateEdit_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Пункт, статья документа:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="ReasonItemTextBox" ClientInstanceName="ReasonItemTextBox"
                                OnDataBinding="ReasonItemTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td class="form_edit_desc">
                            <div id="docLabel">
                                Файлы:
                            </div>
                        </td>
                        <td class="form_edit_input">
                            <cc:FileUploadControl2 runat="server" ID="uploader2" Mode="MultiFile" ClientInstanceName="uploader2" AutoUpload="true"
                                AllowedFileExtensions=".pdf,.tiff,.tif,.jpg,.rar,.zip,.doc,.xls,.docx,.xlsx" OnDataBinding="uploader2_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') ReasonsSave(); }" />
    </dxwgv:ASPxGridView>
</asp:Content>