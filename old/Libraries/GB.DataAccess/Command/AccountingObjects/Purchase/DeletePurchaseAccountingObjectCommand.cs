﻿using GB.Data.AccountingObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.Purchase
{
    public class DeletePurchaseAccountingObjectCommand : ICommand<DeletePurchaseAccountingObjectContext>
    {
        ISessionFactoryGb factory;

        public DeletePurchaseAccountingObjectCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeletePurchaseAccountingObjectContext context)
        {
            using (var session = factory.Create())
            {
                session.Delete<GovPurchase>(context.Id);
                session.Commit();
            }
        }
    }
}
