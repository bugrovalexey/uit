﻿namespace bgTeam.IsRui.Domain.Entities.Directories
{
    using System.Collections.Generic;

    /// <summary>
    /// Классификационная категория
    /// </summary>
    public class ClassificationCategory : EntityTreeDirectories<ClassificationCategory>
    {
    }
}
