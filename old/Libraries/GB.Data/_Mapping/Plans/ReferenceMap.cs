﻿using System;
using Uviri.MKRF.Entities.Documents;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ReferenceMap : ClassMapBase<Reference>
    {
        public ReferenceMap()
            : base("Reference")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "Reference_Code");
            Map(x => x.Name, "Reference_Name");
            Map(x => x.Purpose, "Reference_Purpose").CustomSqlType("nvarchar(max)").Length(Int32.MaxValue);
            Map(x => x.Docs, "Reference_Docs").CustomSqlType("nvarchar(max)").Length(Int32.MaxValue);
            //Map(x => x.Document, "Reference_Document");
            //Map(x => x.Influence, "Reference_Influence");
            //Map(x => x.Activity, "Reference_Activity");

            References<ExpDemand>(x => x.Demand, "Exp_Demand_ID").Not.Nullable();

            HasMany<Document>(x => x.Documents)
                .KeyColumns.Add("Document_Owner_ID")
                .Where("Portal_Table_ID = " + (int)TableTypeEnum.Reference)
                .Inverse();

            HasMany<Influence>(x => x.Influences)
                .KeyColumns.Add("Reference_ID")
                .Inverse();

            HasMany<PlansActivity>(x => x.ActivityList)
               .KeyColumns.Add("Reference_ID")
               .Inverse()
               .ReadOnly();

        }
    }
}
