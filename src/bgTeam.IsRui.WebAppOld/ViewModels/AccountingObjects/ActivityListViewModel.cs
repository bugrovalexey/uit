﻿using bgTeam.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using bgTeam.IsRui.WebApp.Helpers;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class ActivityListViewModel : StepBaseViewModel
    {
        public IEnumerable<TableActivity> Activities { get; set; }

        public ActivityListViewModel(int id)
            : base(id, MasterItemEnum.ActivityList)
        {
            Activities =
                new RepositoryDc<AoActivity>().GetAllEx(query => query
                .Where(x => x.AccountingObject == Current)
                .Where(x => x.Stage == Current.Stage))
                    .Select(a => new TableActivity(a));
        }
    }

    public class TableActivity : NotificationBaseViewModel
    {
        public int Id { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Год реализации")]
        public string Year { get; set; }

        [DisplayName("Тип")]
        public string Type { get; set; }

        [DisplayName("Очередной финансовый год, руб.")]
        public string CostY0 { get; set; }

        [DisplayName("Первый год планового периода, руб.")]
        public string CostY1 { get; set; }

        [DisplayName("Второй год планового периода, руб.")]
        public string CostY2 { get; set; }

        public string DeleteLink { get; set; }

        public TableActivity(AoActivity a)
        {
            Id = a.Id;
            Name = a.Name;
            Year = a.Year.ToString();
            Type = a.ActivityType.GetDescription();
            CostY0 = a.CostY0.ToString("N2");
            CostY1 = a.CostY1.ToString("N2");
            CostY2 = a.CostY2.ToString("N2");

            DeleteLink = LinkBuilder.AjaxDeleteLink("ActivityDelete", "AccountingObjects", new { id = Id }, "GridActivityList");
        }
    }
}