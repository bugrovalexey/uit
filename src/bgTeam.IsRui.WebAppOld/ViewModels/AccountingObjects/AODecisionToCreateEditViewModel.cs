﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.Access;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    [MetadataType(typeof(AODecisionToCreateEditViewModel_Valid))]
    public class AODecisionToCreateEditViewModel : ViewModelBase
    {
        #region ctor

        public AODecisionToCreateEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;
            ReadOnly = ManagerAccess.CanEdit(new RepositoryDc<AccountingObject>().GetExisting(accountingObjectId)) ==
                       false;

            FilesData = new List<SelectEntityInfo>();

            var entity = id.HasValue
                ? new AODecisionToCreateRepository().Get(
                    q => q.Fetch(x => x.Documents).Eager
                        .Where(x => x.Id == id))
                : null;

            entity.Do(Fill);
        }

        #endregion ctor

        #region methods

        protected void Fill(AODecisionToCreate entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            Number = entity.Number;
            DateAdoption = entity.DateAdoption.ToStringOrDefault();
            ParticleOfDocument = entity.ParticleOfDocument;
            Url = entity.Url;
            FilesData = entity.Documents.Return(docs => docs.Select(x => new SelectEntityInfo() { Id = x.Id, Name = x.Name }), default(IEnumerable<SelectEntityInfo>));
        }

        #endregion methods

        #region Props

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        [DisplayName(TableAODecisionToCreate.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableAODecisionToCreate.NumberCaption)]
        public string Number { get; set; }

        [DisplayName(TableAODecisionToCreate.DateAdoptionCaption)]
        public string DateAdoption { get; set; }

        [DisplayName(TableAODecisionToCreate.ParticleOfDocumentCaption)]
        public string ParticleOfDocument { get; set; }

        [DisplayName(TableAODecisionToCreate.UrlCaption)]
        public string Url { get; set; }

        [DisplayName(TableAODecisionToCreate.FilesDataCaption)]
        public IEnumerable<IEntityName> FilesData { get; set; }

        #endregion Props
    }

    public class AODecisionToCreateEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }
    }
}