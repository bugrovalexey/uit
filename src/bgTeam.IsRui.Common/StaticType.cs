﻿namespace bgTeam.IsRui.Common
{
    using System;

    public static class StaticType<T>
    {
        public static Type Type { get; } = typeof(T);
    }
}
