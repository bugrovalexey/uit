﻿namespace bgTeam.IsRui.Domain.Entities.Users
{
    using bgTeam.IsRui.Domain.Entities.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UsersEntity : EntityBase
    {
        public virtual string Login { get; set; }

        public virtual string Password { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual string Surname { get; set; }

        public virtual string Name { get; set; }

        public virtual string Middlename { get; set; }

        public virtual string Job { get; set; }

        public virtual string WorkPhone { get; set; }

        public virtual string MobPhone { get; set; }

        public virtual string Fax { get; set; }

        public virtual string Email { get; set; }

        public virtual string Work { get; set; }

        public virtual string Education { get; set; }

        public virtual bool ReceiveMail { get; set; }

        public virtual bool IsResponsible { get; set; }

        public virtual string Snils { get; set; }

        public virtual bool IsESIA { get; set; }

        public virtual string Token { get; set; }

        public virtual Department GovtOrgan { get; set; }

    }
}
