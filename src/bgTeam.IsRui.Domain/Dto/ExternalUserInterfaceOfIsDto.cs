﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Внешние пользовательские интерфейсы ИС
    /// </summary>
    public class ExternalUserInterfaceOfIsDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// Тип. Id
        /// </summary>
        public int TypeOfInterfaceId { get; set; }

        /// <summary>
        /// Тип. Name
        /// </summary>
        public string TypeOfInterfaceName { get; set; }

        /// <summary>
        /// Начальный URL
        /// </summary>
        public string Url { get; set; }
    }
}
