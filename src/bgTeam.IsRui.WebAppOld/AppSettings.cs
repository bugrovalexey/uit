﻿using bgTeam.DataAccess;

namespace bgTeam.IsRui.WebApp
{
    public class AppSettings : IConnectionSetting
    {
        public string ConnectionString
        {
            get;
            set;
        }

        public AppSettings()
        {
            ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["CoordConnectionString"].ConnectionString;
        }
    }
}
