﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using GB;
using GB.Handlers;
using GB.Helpers;
using GB.MKRF.ViewModels;
using GB.MKRF.Web.Plans.v2012new;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Common
{
    public partial class ActivityReadMode : System.Web.UI.UserControl
    {
        const string NOT_VALUE = "—";

        public string CurrentId { get; set; }

        public bool DefaultCollapsed { get; set; }

        public bool DefaultShowEditLinks { get; set; }

        public bool DefaultShowCollapseLink { get; set; }

        public const string ActivityIdKey = "ActivityId";

        protected string ItemTitleStyle
        {
            get
            {

                return DefaultShowCollapseLink ? DefaultCollapsed ? "collapsed" : "expanded" : string.Empty;
            }
        }

        protected string ItemStyle
        {
            get { return DefaultCollapsed ? "display_none" : ""; }
        }

        public ActivityViewModel CurrentActivity = new ActivityViewModel();

        public void SetViewModel(ActivityViewModel ActivityViewModel)
        {
            CurrentActivity = ActivityViewModel;
        }

        public bool EnableEdit { get; set; }

        protected string ShowValue(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? "<span class='emptyvalue'>—</span>" : value;
        }

        protected string ShowValue(IEntityCodeDirectories value)
        {
            return value == null ? "<span class='emptyvalue'>—</span>" : value.CodeName;
        }

        protected string ShowValue(IEntityDirectories value)
        {
            return value == null ? "<span class='emptyvalue'>—</span>" : value.Name;
        }

        
        //protected string GetExpenseItemCode(PlansActivity activity)
        //{
        //    if (activity.ExpenseItem == null || string.IsNullOrWhiteSpace(activity.ExpenseItem.Code) || activity.ExpenseItem.Id == EntityBase.Default)
        //        return NOT_VALUE;

        //    if (string.IsNullOrWhiteSpace(activity.Subentry) )
        //    {
        //        return activity.ExpenseItem.Code.Replace(" ", "");
        //    }
        //    var code = activity.ExpenseItem.Code.Replace(" ", "");
        //    var lastFour = code.Substring(code.Length - 4);
        //    return code.Replace(lastFour, activity.Subentry.Replace(" ", ""));
        //}

        protected string editModeLinkButton(string relativeUrl)
        {
            if (EnableEdit)
                return string.Format("<a href='{0}?{1}={2}'><img height='14' title='Редактировать' src='/Content/GB/Styles/Imgs/edit_16.png'/></a>",
                                  UrlHelper.Resolve(relativeUrl), ActivityCard.ActivityIdKey, CurrentActivity.Id);
            else
                return null;
        }
        
        protected string getReasonDownloadUrl(string FileInfo)
        {
            if (string.IsNullOrEmpty(FileInfo))
                return null;

            List<string> links = new List<string>();

            string[] files = FileInfo.Split(';');
            foreach (string f in files)
            {
                string[] pair = f.Split(':');
                links.Add(string.Format("<a href='{0}'>{1}</a>", FileHandler.GetUrl(pair[0]),
                                                                 pair.Length > 1 ? pair[1] : "Скачать"));
            }
            return string.Join("<br />", links);
        }

        //protected IList<ActivityFuncReadModeView> GetActivityFuncs()
        //{
        //    return g;
        //}

        //protected PlansGoalIndicator GetPlanGoalIndicator(PlansGoalIndicator parent, int planYear)
        //{
        //    var i = PlansGoalIndicatorController.GetPlanGoalIndicator(parent.PlanActivity, parent, planYear);
            
        //    return i ?? new PlansGoalIndicator(parent.PlanActivity);
        //}

        protected string GetEditUrl(string relativeUrl)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve(relativeUrl), ActivityCard.ActivityIdKey, CurrentActivity.Id);
        }

        protected string GetDocDownloadUrl(int docId)
        {
            StringBuilder sb = new StringBuilder();

            if (docId > 0)
                sb.Append("<a href='" + FileHandler.GetUrl(docId) + "'>Скачать</a>");
            //if (RepositoryBase<Document>.Get(docId) != null && RepositoryBase<Document>.Get(docId).SignedPackage != null)
            //    sb.Append("<br/><a href='javascript:checkECP()'>Проверить ЭП</a>");

            return sb.ToString();
        }

        public string GetDate(DateTime? date)
        {
            if (date.HasValue)
                return date.Value.ToShortDateString();

            return NOT_VALUE;
        }
    }

    
}