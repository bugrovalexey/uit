﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Domain.Extensions;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    [MetadataType(typeof(ActsOfAcceptanceEditViewModel_Valid))]
    public class ActsOfAcceptanceEditViewModel : ViewModelBase
    {
        #region consts

        private const string DocNameCaption = "Наименование";
        private const string DocNumberCaption = "Номер документа";
        private const string DocDateAdoptionCaption = "Дата подписания";
        private const string FilesDataCaption = "Документ";
        private const string GovContractCaption = "Госконтракт";
        private const string ReasonsWaitingResultsDeviationCaption = "Причины отклонений от ожидаемых результатов";
        private const string ActivityTypeCaption = "Тим мероприятия";

        #endregion consts

        public ActsOfAcceptanceEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;
            var ao = new RepositoryDc<AccountingObject>().GetExisting(accountingObjectId);


            FilesData = new List<SelectEntityInfo>();
            //WorkAndGoods = new List<TableWorkAndGoods>();
            Activities = new List<TableActToAoActivity>();

            var activityTypes = ao.Stage.ToActivityTypeEnum();
            ActivityTypesList = new SelectList(activityTypes.Select(x => new {Key = x.ToString("D"), Value = x.GetDescription()}),
                    "Key", "Value");
            ActivityType = (int)activityTypes.First();

            //TODO сделать как SubQuery
            var stage = new RepositoryDc<AccountingObject>().SelectSingle<AccountingObjectsStageEnum>(q => q.Where(x => x.Id == accountingObjectId).Select(x => x.Stage));

            GovContracts =
                new SelectList(
                    new RepositoryDc<GovContract>().GetAllEx(
                        q => q.Where(x => x.AccountingObject.Id == accountingObjectId && x.Stage == stage)), "Id", "Number");

            var entity = id.HasValue
                ? new RepositoryDc<ActOfAcceptance>().Get(id)
                : null;

            entity.Do(Fill);
        }

        #region Methods

        protected void Fill(ActOfAcceptance entity)
        {
            Id = entity.Id;

            DocName = entity.DocName;
            DocNumber = entity.DocNumber;
            DocDateAdoption = entity.DocDateAdoption.ToStringOrDefault();

            FilesData = entity.Documents.Return(
                    docs => docs.Select(x => new SelectEntityInfo { Id = x.Id, Name = x.Name }),
                    new List<SelectEntityInfo>());

            Cost = entity.Cost;
            GovContractId = entity.GovContract.Id;
            ReasonsWaitingResultsDeviation = entity.ReasonsWaitingResultsDeviation;

            Activities =
                new RepositoryDc<ActToAOActivity>().GetAllEx(query => query
                .Where(x => x.ActOfAcceptance == entity))
                    .Select(a => new TableActToAoActivity(a));

            ActivityType = entity.ActivityType.Return<ActivityTypeEnum, int>(x => (int) x, ActivityType);
            //WorkAndGoods = new Repository<WorkAndGoods>().GetAllEx(
            //    q =>
            //        q.Fetch(x => x.CategoryKindOfSupport)
            //            .Eager.Where(x => x.ActOfAcceptance.Id == entity.Id)).Select(x => new TableWorkAndGoods(x));
        }

        #endregion Methods

        #region Props

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        [DisplayName(DocNameCaption)]
        public string DocName { get; set; }

        [DisplayName(DocNumberCaption)]
        public string DocNumber { get; set; }

        [DisplayName(DocDateAdoptionCaption)]
        public string DocDateAdoption { get; set; }

        [DisplayName(FilesDataCaption)]
        public IEnumerable<IEntityName> FilesData { get; set; }

        [DisplayName(TableActOfAcceptance.CostCaption)]
        public object Cost { get; set; }

        [DisplayName(GovContractCaption)]
        public int GovContractId { get; set; }

        [DisplayName(ReasonsWaitingResultsDeviationCaption)]
        public string ReasonsWaitingResultsDeviation { get; set; }

        public IList<IEntityName> WorksAndGoods { get; set; }

        public SelectList GovContracts { get; set; }

        //public IEnumerable<TableWorkAndGoods> WorkAndGoods { get; set; }
        public IEnumerable<TableActToAoActivity> Activities { get; set; }

        public SelectList ActivityTypesList { get; set; }

        [DisplayName(ActivityTypeCaption)]
        public int ActivityType { get; set; }

        #endregion Props
    }


    public class ActsOfAcceptanceEditViewModel_Valid : ValidateViewModel
    {
        [Range(1, int.MaxValue, ErrorMessage = RangeIntText)]
        [Required(ErrorMessage = RequiredText)]
        public string GovContractId { get; set; }
    }

}