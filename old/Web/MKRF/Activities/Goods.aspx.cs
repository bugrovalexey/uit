﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using GB.Entity.Directories;
using GB.Extentions;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using GB.Controls;
using GB.Controls.Extensions;

namespace GB.MKRF.Web.Activities
{
    public partial class Goods : System.Web.UI.Page
    {
        public PlansActivity CurrentActivity
        {
            get { return (Master as EditCard).CurrentActivity; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ((GridViewDataComboBoxColumn)Grid.Columns["yearColumn"]).PropertiesComboBox.DataSource = PlanYearsRepository.YearList;
            Grid.DataBind();

            base.OnPreRender(e);
        }

        #region Grid

        readonly PlansSpecController controller = new PlansSpecController();

        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var data = controller.Repository.GetAll(x => x.PlanActivity == CurrentActivity);
            ((ASPxGridView)sender).DataSource = data.Select(w =>
                 new
                 {
                     w.Id,
                     w.Name,
                     OKPD = w.OKPD.Return(x => x.CodeName),
                     Category = w.ProductGroup.Return(x => x.Name),
                     ExpenseDirection = w.ExpenseDirection.Return(x => x.Name),
                     ExpenseType = w.ExpenseType.Return(x => x.Name),
                     ExpenditureItem = w.ExpenditureItem.Return(x => x.CodeName),
                     w.Year,
                     w.Quantity,
                     w.Duration,
                     w.Cost
                 }).ToArray();
        }

        protected void Grid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void Grid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            var args = new PlansSpecArgs { Activity = CurrentActivity };
            FillPlansSpecArgs(args, hf);
            controller.Create(args);

            ((ASPxGridView)sender).GridCancelEdit(e);
        }

        protected void Grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            var args = new PlansSpecArgs { Id = Convert.ToInt32(e.Keys[0]) };
            FillPlansSpecArgs(args, hf);
            controller.Update(args);

            ((ASPxGridView)sender).GridCancelEdit(e);
        }

        protected void Grid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            controller.Delete(new PlansSpecArgs { Id = Convert.ToInt32(e.Keys[0]) });

            ((ASPxGridView)sender).GridCancelEdit(e);
        }

        protected void NameTextBox_DataBinding(object sender, EventArgs e)
        {
            controller.CurrentEdit.With(x => x.Name).Do(name => sender.SetValueTextBox(name));
        }

        protected void DurationTextBox_DataBinding(object sender, EventArgs e)
        {
            controller.CurrentEdit.With(x => x.Duration).Do(sender.SetValueTextBoxVal<int>);
        }

        protected void QuantityTextBox_DataBinding(object sender, EventArgs e)
        {
            controller.CurrentEdit.With(x => x.Quantity).Do(sender.SetValueTextBoxVal<int>);
        }

        protected void CostTextBox_DataBinding(object sender, EventArgs e)
        {
            controller.CurrentEdit.With(x => x.Cost).DoVal(sender.SetValueTextBoxVal<decimal>);
        }

        protected void OKPDComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(DirectoriesRepositary.GetDirectories<OKPD>(DateTime.Now.Year).OrderBy(x => x.Code))
                  .SetValueComboBox<int>(controller.CurrentEdit.With(x => x.OKPD).Return(x => x.Id));
        }

        protected void ExpenseDirectionComboBox_DataBinding(object sender, EventArgs e)
        {
            // Необходимо чтобы в поле "Вид затрат" отображались только актуальные значения на текущую дату.
            DateTime date = DateTime.Now;
            sender.SetDataComboBox(new RepositoryBaseNew<ExpenseDirection>().GetAll(x => (x.Type == ExpenseTypeEnum.Goods && x.DateStart <= date && date <= x.DateEnd) || x.Id == GB.EntityBase.Default))
                  .SetValueComboBox<int>(controller.CurrentEdit.With(x => x.ExpenseDirection).Return(x => x.Id));
        }

        protected void ExpenseTypeComboBox_DataBinding(object sender, EventArgs e)
        {
            int directionId = controller.CurrentEdit.With(x => x.ExpenseDirection).Return(x => x.Id);
            //Перечень доступных значений зависит от выбранного значения в поле виды затрат
            FillExpenseTypes(sender, directionId, controller.CurrentEdit.With(x => x.ExpenseType).Return(x => x.Id));
        }

        protected void ExpenditureComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(DirectoriesRepositary.GetDirectories<ExpenditureItem>(DateTime.Now.Year).OrderBy(x => x.Code))
                  .SetValueComboBox<int>(controller.CurrentEdit.Return(x => x.ExpenditureItem.Id));
        }

        protected void YearComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(PlanYearsRepository.YearList)
                  .SetValueComboBox<int>(controller.CurrentEdit.Return(x => (int)x.Year, 1));
        }

        protected void ExpenseTypeComboBox_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            FillExpenseTypes(sender, Convert.ToInt32(e.Parameter), GB.EntityBase.Default).DataBind();
        }

        protected void CategoryComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(new RepositoryBaseNew<ProductGroup>().GetAll())
                  .SetValueComboBox<int>(controller.CurrentEdit.With(x => x.ProductGroup).Return(x => x.Id));
        }

        #endregion

        #region CharacteristicsGrid

        readonly PlansSpecCharacteristicsController characteristicsController = new PlansSpecCharacteristicsController();

        protected void CharacteristicsGrid_Init(object sender, EventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            //Не нужно, т.к. только 1 дочерняя сторока
            //grid.ClientInstanceName = string.Concat("CharacteristicsGrid_", grid.GetMasterRowKeyValue());
        }

        protected void CharacteristicsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            var masterkey = grid.GetMasterRowKeyValue();
            var spec = controller.Repository.GetExisting(masterkey);
            grid.DataSource = characteristicsController.Repository.GetAll(x => x.Owner == spec);
        }

        protected void CharacteristicsGrid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            characteristicsController.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void CharacteristicsGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            var masterkey = grid.GetMasterRowKeyValue();
            var spec = controller.Repository.GetExisting(masterkey);
            var args = new PlansSpecCharacteristicArgs { Owner = spec };
            FillPlansSpecCharacteristicArgs(args, hfCharacteristic);
            characteristicsController.Create(args);

            grid.GridCancelEdit(e);
        }

        protected void CharacteristicsGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            var args = new PlansSpecCharacteristicArgs { Id = Convert.ToInt32(e.Keys[0]) };
            FillPlansSpecCharacteristicArgs(args, hfCharacteristic);
            characteristicsController.Update(args);

            ((ASPxGridView)sender).GridCancelEdit(e);
        }

        protected void CharacteristicsGrid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            var args = new PlansSpecCharacteristicArgs { Id = Convert.ToInt32(e.Keys[0]) };
            characteristicsController.Delete(args);

            ((ASPxGridView)sender).GridCancelEdit(e);
        }

        protected void CharNameTextBox_DataBinding(object sender, EventArgs e)
        {
            characteristicsController.CurrentEdit.With(x => x.Name).Do(name => sender.SetValueTextBox(name));
        }

        protected void UnitComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(new RepositoryBaseNew<Units>().GetAll())
                  .SetValueComboBox<int>(characteristicsController.CurrentEdit.With(x => x.Unit).Return(x => x.Id));
        }

        protected void CharValueTextBox_DataBinding(object sender, EventArgs e)
        {
            characteristicsController.CurrentEdit.With(x => x.Value).Do(sender.SetValueTextBoxVal<int>);
        }

        #endregion

        private static ASPxComboBox FillExpenseTypes(object sender, int directionId, int typeId)
        {
            var data = new RepositoryBaseNew<ExpenseType>().GetAll(x => (x.ExpenseDirection != null && x.ExpenseDirection.Id == directionId) || x.Id == GB.EntityBase.Default);
            return sender.SetDataComboBox(data)
                         .SetValueComboBox<int>(typeId);
        }

        private static void FillPlansSpecArgs(PlansSpecArgs args, IDictionary<string, object> values)
        {
            args.Name = (string)values["Name"];
            args.OKPD = Convert.ToInt32(values["OKPD"]);
            args.Category = Convert.ToInt32(values["Category"]);
            args.ExpenseDirection = Convert.ToInt32(values["ExpenseDirection"]);
            args.ExpenseType = Convert.ToInt32(values["ExpenseType"]);
            args.KOSGU = Convert.ToInt32(values["KOSGU"]);
            args.Year = (YearEnum)Convert.ToInt32(values["Year"]);
            args.Quantity = Convert.ToInt32(values["Quantity"]);
            args.Duration = Convert.ToInt32(values["Duration"]);
            args.Cost = Convert.ToDecimal(values["Cost"]);
        }

        private static void FillPlansSpecCharacteristicArgs(PlansSpecCharacteristicArgs args, IDictionary<string, object> values)
        {
            args.Name = (string)values["Name"];
            args.Unit = Convert.ToInt32(values["Unit"]);
            args.Value = Convert.ToInt32(values["Value"]);
        }

    }
}