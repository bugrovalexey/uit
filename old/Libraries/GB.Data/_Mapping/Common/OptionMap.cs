﻿using GB.Data.Common;

namespace GB._Mapping.Common
{
    internal class OptionMap : ClassMapHiLo<Option>
    {
        public OptionMap()
            : base("Options")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Options_ID").GeneratedBy.Assigned();

            //            SqlInsert(@"raiserror ('Unsupported',10,1)").Check.None();
            //            SqlUpdate(@"exec ZZZ_Prc_Options_Update
            //                         @Options_Val = ?
            //                        ,@Options_ID = ?").Check.None();
            //            SqlDelete(@"raiserror ('Unsupported',10,1)").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.GroupName, "Options_Group").ReadOnly();
            Map(x => x.Name, "Options_Name").ReadOnly();
            Map(x => x.Value, "Options_Val");
            Map(x => x.Type, "Options_Type").CustomType(typeof(OptionsTypeEnum)).Not.Nullable().ReadOnly();
        }
    }
}