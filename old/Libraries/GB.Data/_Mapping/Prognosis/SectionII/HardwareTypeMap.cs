﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Prognosis.SectionII
{
    public class HardwareTypeMap : ClassMapBase<HardwareType>
	{
        public HardwareTypeMap()
            : base("Hardware_Type")
        {
//            Table("w_");

//            Id(x => x.Id, "Hardware_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
//            //Map(x => x.OrigId, "Hardware_Type_Orig_ID");

//            SqlInsert(@"exec ZZZ_Prc_Hardware_Type_Insert
//                          @Hardware_Type_Name = ?
//                         ,@Hardware_Type_Sname = ?
//                         ,@Hardware_Type_Comm = ?
//                         ,@Hardware_Type_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Hardware_Type_Update
//                          @Hardware_Type_Name = ?
//                         ,@Hardware_Type_Sname = ?
//                         ,@Hardware_Type_Comm = ?
//                         ,@Hardware_Type_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Hardware_Type_Delete
//						  @Hardware_Type_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Hardware_Type_Name");
			Map(x => x.NameSmall, "Hardware_Type_Sname");
			Map(x => x.Comment, "Hardware_Type_Comm");
        }
    }
}