﻿using AutoMapper;
using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.Documents;
using GB.DataAccess.Command.Documents;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;

namespace GB.DataAccess.Command.AccountingObjects.Decommissioning
{
    public class SaveDecommissioningCommand : ICommand<SaveDecommissioningContext>
    {
        ISessionFactoryGb factory;

        public SaveDecommissioningCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(SaveDecommissioningContext context)
        {
            var entity = new Repository<AccountingObject>().GetExisting(context.AccountingObjectId);

            entity.DecommissioningDate = context.DecommissioningDate;

            var isNew = entity.DocumentDecommissioning == null;
            AODocumentDecommissioning documentDecommissioning;

            using (var session = factory.Create())
            {
                session.Update(entity);

                if (isNew)
                {
                    documentDecommissioning = new AODocumentDecommissioning {AccountingObject = entity};
                }
                else
                {
                    documentDecommissioning = entity.DocumentDecommissioning;
                }

                Mapper.Map(context, documentDecommissioning);
                session.SaveOrUpdate(documentDecommissioning);

                session.Commit();
            }

            //new DocumentService().Save(context.DecommissioningFiles, documentDecommissioning.Documents,
            //    documentDecommissioning, documentDecommissioning.Type);

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext { fileList = context.DecommissioningFiles,
                                                                        listDocs = documentDecommissioning.Documents,
                                                                        owner = documentDecommissioning,
                                                                        type = documentDecommissioning.Type
                                                                      });
        }

    }
}
