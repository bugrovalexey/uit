﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807240058)]
    public class Migration_201807240058 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Plans_Activity")
                .AddColumn("Plans_Activity_AB_ID").AsInt32().Nullable()
                .AddColumn("Plans_Activity_PlanQuarter1").AsDecimal().Nullable()
                .AddColumn("Plans_Activity_PlanQuarter2").AsDecimal().Nullable()
                .AddColumn("Plans_Activity_PlanQuarter3").AsDecimal().Nullable()
                .AddColumn("Plans_Activity_PlanQuarter4").AsDecimal().Nullable()
                .AddColumn("Plans_Activity_ForecastQuarter1").AsDecimal().Nullable()
                .AddColumn("Plans_Activity_ForecastQuarter2").AsDecimal().Nullable()
                .AddColumn("Plans_Activity_ForecastQuarter3").AsDecimal().Nullable()
                .AddColumn("Plans_Activity_ForecastQuarter4").AsDecimal().Nullable();
        }
    }
}
