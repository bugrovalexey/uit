﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityGoodsGetDto
    {
        public int? Id { get; set; }

        public int? ActivityId { get; set; }

        public virtual string Name { get; set; }

        //public int? OKPDId { get; set; }

        //public string OKPDName { get; set; }

        //public int? ProductGroupId { get; set; }

        //public string ProductGroupName { get; set; }

        //public int? ExpenseDirectionId { get; set; }

        //public string ExpenseDirectionName { get; set; }

        //public int? ExpenseTypeId { get; set; }

        //public string ExpenseTypeName { get; set; }

        //public int? KOSGUId { get; set; }

        //public string KOSGUName { get; set; }

        //public YearEnum Year { get; set; }

        public int? Quantity { get; set; }

        //public int? Duration { get; set; }

        public int? Cost { get; set; }

        public decimal? Summ { get; set; }

        public QuarterEnum Quarter { get; set; }

        public string AnalogName { get; set; }

        public decimal AnalogCost { get; set; }

        public IList<GoodsCharacteristicDto> Characteristic { get; set; }
    }
}
