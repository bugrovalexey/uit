﻿namespace GB.DataAccess.Services.Args
{
    public class UserArgs : BaseArgsNew, IServiceArgs
    {
        public bool IsActive { get; set; }

        public string Login { get; set; }

        public string NewPassword { get; set; }

        public int[] RoleIds { get; set; }

        public string Surname { get; set; }

        public string Name { get; set; }

        public string Middlename { get; set; }

        public int? DepartmentId { get; set; }

        public string Email { get; set; }

        public string WorkPhone { get; set; }

        public string MobPhone { get; set; }
    }
}