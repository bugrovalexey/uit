﻿using System;
using GB.Helpers;

namespace GB.MKRF.Web.Plans.v2012new
{
    /// <summary>
    /// Бумажные формы
    /// </summary>
    public partial class Mails : System.Web.UI.Page
    {
        public static string GetUrl(int PlanId)
        {
            string url = UrlHelper.Resolve("~/Plans/v2012new/Mails.aspx");
            return (PlanId > 0) ? url + "?" + PlanCard.plansIdKey + "=" + PlanId : url;
        }

        protected override void OnInit(EventArgs e)
        {
            this.mc.CurrentPlan = (Master as PlanCard).CurrentPlan;

            base.OnInit(e);
        }
    }
}