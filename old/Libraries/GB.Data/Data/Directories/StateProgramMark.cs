﻿namespace GB.Data.Directories
{
    /// <summary>
    /// Показатель Гос. программы
    /// </summary>
    public class StateProgramMark : EntityDirectories
    {
        /// <summary>
        /// Гос. программа
        /// </summary>
        public virtual StateProgram StateProgram { get; set; }
    }
}