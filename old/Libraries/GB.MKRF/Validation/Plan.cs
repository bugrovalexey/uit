﻿using GB;
using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Validation
{
    internal class Plan
    {
        /// <summary>Параметры для валидации плана</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action== ObjActionEnum.CanPublish || Action == ObjActionEnum.CanRenumberActivityList || Action == ObjActionEnum.CanEditAgreedExpenses)
            {
                return Action.ToString() + ";" + Entity.Id.ToString();
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
