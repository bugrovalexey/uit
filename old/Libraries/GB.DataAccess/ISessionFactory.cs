﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GB.DataAccess
{
    public interface ISessionFactoryGb
    {
        SessionHibernate Create(IsolationLevel isolationLevel = IsolationLevel.ReadUncommitted);
    }
}
