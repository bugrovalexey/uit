﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    class TypeOfInterfaceMap : ClassMapHiLo<TypeOfInterface>
    {
        public TypeOfInterfaceMap()
            : base("Interface_Type")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Interface_Type_Name").Not.Nullable();
        }
    }
}
