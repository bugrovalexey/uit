﻿using System;

namespace GB.MKRF.Entities.Plans
{
    /// <summary>
    /// Экономические обоснования, раздел 5, отчет по плану
    /// </summary>
    public class ReportByPlanDocument : EntityBase
    {
        public virtual string Comment { get; set; }

        public virtual OfferByPlan OfferByPlan { get; set; }

        public virtual byte[] File { get; set; }

        public virtual string FileName { get; set; }

        public virtual string ContentType { get; set; }

        public virtual DateTime? DateCreate { get; set; }
    }
}