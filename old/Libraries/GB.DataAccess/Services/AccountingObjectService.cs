﻿using AutoMapper;
using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.Documents;
using GB.Data.Common;
using GB.Data.Directories;
using GB.DataAccess.Command;
using GB.DataAccess.Command.AccountingObjects;
using GB.DataAccess.Command.Documents;
using GB.DataAccess.Repository;
using GB.DataAccess.Services.Args;
using GB.DataAccess.Services.Args.Documents;
using GB.Extentions;
using NHibernate.Criterion;

namespace GB.DataAccess.Services
{
    /// <summary>
    /// Контроллер объектов учета
    /// </summary>
    public class AccountingObjectService : ServiceBase<AccountingObject>, IService<AccountingObject, AccountingObjectArgs>
    {
        ISessionFactoryGb factory;

        public AccountingObjectService()
        {
            factory = new SessionFactory();
        }

        #region AccountingObjectArgs

        public AccountingObject Create(AccountingObjectArgs args)
        {
            var entity = ConvertDtoToEntity.FillAccountingObjectFromDto(new AccountingObject(), args);

            //Проставляем значения по умолчанию для Not Nullable полей
            entity.Type = GetEntity<IKTComponent>(EntityBase.Default);
            entity.Kind = GetEntity<IKTComponent>(EntityBase.Default);
            entity.Status = new StatusRepository().GetDefault(entity);

            base.SaveOrUpdate(entity);

            return entity;
        }

        public AccountingObject Update(AccountingObjectArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            entity = ConvertDtoToEntity.FillAccountingObjectFromDto(entity, args);
            base.SaveOrUpdate(entity);

            return entity;
        }

        public void Delete(AccountingObjectArgs args)
        {
            Delete(args.Id);
        }

        #endregion AccountingObjectArgs

        public AccountingObject Update(AccountingObjectCommonDataArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            entity = ConvertDtoToEntity.FillAccountingObjectFromDto(entity, args);
            SaveOrUpdate(entity);

            SaveAODocument(Mapper.Map<AODocumentCommissioningArgs>(args), args.CommissioningFiles,
                entity.DocumentCommissioning);
            SaveAODocument(Mapper.Map<AODocumentDecommissioningArgs>(args), args.DecommissioningFiles,
                entity.DocumentDecommissioning);

            return entity;
        }

        //public bool IsReadOnly(int id)
        //{
        //    return Repository.GetExisting(id).CanEdit() == false;
        //}

        public AccountingObject Update(AccountingObjectResponsiblesArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            entity = ConvertDtoToEntity.FillAccountingObjectFromDto(entity, args);

            base.SaveOrUpdate(entity);
            return entity;
        }

        #region AccountingObjectControllerArgs

        public AccountingObject Create(IAccountingObjectControllerArgs args)
        {
            var entity = new AccountingObject();
            entity.Status = new StatusRepository().GetDefault(entity);
            return SaveOrUpdate(entity, args);
        }

        public AccountingObject Update(AccountingObjectControllerArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            return SaveOrUpdate(entity, args);
        }

        public AccountingObject Update(IAccountingObjectControllerArgs args)
        {
            var entity = Repository.Get(args.Id);
            AccountingObject result;

            if (entity == null)
            {
                var command = new AddOrSaveAccountingObjectCommand();
                result = command.Execute(new AddOrSaveAccountingObjectContext()
                {
                    FullName = args.FullName,
                    ShortName = args.ShortName,
                    KindId = args.Kind,
                    TypeId = args.Type,
                });
            }
            else
            {
                result = SaveOrUpdate(entity, args);                
            }

            return result;
        }

        private AccountingObject SaveOrUpdate(AccountingObject entity, IAccountingObjectControllerArgs args)
        {
            entity = ConvertDtoToEntity.FillAccountingObjectFromDto(entity, args);
            base.SaveOrUpdate(entity);

            return entity;
        }

        #endregion AccountingObjectControllerArgs

        #region PlannedMaster

        public AccountingObject Update(AccountingObjectResponsibleServiceArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            entity.ResponsibleDepartment = GetEntity<Department>(args.DepartmentId);
            entity.ResponsibleCoord = GetEntity<Responsible>(args.ResponsibleCoord);
            entity.ResponsibleInformation = GetEntity<Responsible>(args.ResponsibleInformation);

            return SaveOrUpdate(entity);
        }

        public AccountingObject Update(AccountingObjectGoalsAndPurposeArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            entity = ConvertDtoToEntity.FillAccountingObjectFromDto(entity, args);

            SaveOrUpdate(entity);
            return entity;
        }

        public AccountingObject Update(AccountingObjectDataProtectionArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            entity = ConvertDtoToEntity.FillAccountingObjectFromDto(entity, args);

            SaveOrUpdate(entity);
            return entity;
        }

        public AccountingObject Update(SpecialActivityArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            entity.InformAboutActivityGovOrgan = args.InformAboutActivityGovOrgan;
            SaveOrUpdate(entity);
            return entity;
        }

        #endregion PlannedMaster

        #region CreateMaster

        public AccountingObject Update(AddOrSaveAOAdoptionBudgetContext context)
        {
            var entity = Repository.GetExisting(context.AccountingObjectId);
            AutoMapper.Mapper.Map(context, entity);
            
            SaveOrUpdate(entity);

            context.xId = entity.DocumentGroundsForCreation == null ? 0 : entity.DocumentGroundsForCreation.Id;
            var budget = new BaseAddOrSaveCommand<AddOrSaveAOAdoptionBudgetContext, AODocumentGroundsForCreation>().Execute(context);

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = context.Files,
                listDocs = budget.Documents,
                owner = budget,
                type = budget.Type
            });            

            return entity;
        }

        public AccountingObject Update(AOFactualLocationArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            Mapper.Map(args, entity);

            SaveOrUpdate(entity);
            return entity;
        }

        public void UpdateBalanceCost(int accountingObjectId)
        {
            var entity = Repository.GetExisting(accountingObjectId);

            GovContract contr = null;

            var balanceCost =
                /*new ActOfAcceptanceService().Repository*/new Repository<ActOfAcceptance>()                
                .SelectSingle<decimal>(
                    q => q.JoinAlias(x => x.GovContract, () => contr)
                        .Where(x => contr.AccountingObject.Id == accountingObjectId)
                        .Select(Projections.Sum<ActOfAcceptance>(x => x.Cost)));

            entity.BalanceCost = balanceCost;
            SaveOrUpdate(entity);
        }

        #endregion CreateMaster

        #region ExploitationMaster

        public AccountingObject UpdateCommissioningData(AddOrSaveAODocumentBaseContext context)
        {
            var entity = Repository.GetExisting(context.AccountingObjectId);

            entity.CommissioningDate = context.Date;
            SaveOrUpdate(entity);

            context.xId = entity.DocumentCommissioning == null ? 0 : entity.DocumentCommissioning.Id;
            var commis = new BaseAddOrSaveCommand<AddOrSaveAODocumentBaseContext, AODocumentCommissioning>().Execute(context);

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = context.Files,
                listDocs = commis.Documents,
                owner = commis,
                type = commis.Type
            });

            return entity;
        }

        #endregion ExploitationMaster

        private void SaveAODocument<T, TArgs>(TArgs args, string files, T entity)
            where T : AODocumentBase, new()
            where TArgs : AODocumentBaseArgs
        {
            var docService = new CrudService<T, TArgs>();

            if (entity == null)
            {
                entity = docService.Create(args);
            }
            else
            {
                args.Id = entity.Id;
                entity = docService.Update(args);
            }

            //new DocumentService().Save(files, entity.Documents, entity, entity.Type);

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = files,
                listDocs = entity.Documents,
                owner = entity,
                type = entity.Type
            });
        }


    }
}