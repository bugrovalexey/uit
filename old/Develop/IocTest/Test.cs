﻿using GB.DataAccess;
using GB.DataAccess.Command.AccountingObjects.GovContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IocTest
{
    interface ITest
    {
        void Execute();
    }

    class Test : ITest
    {
        ICommandBuilder _command;

        public Test(ICommandBuilder command)
        {
            _command = command;
        }

        public void Execute()
        {
            _command.Execute(new DeleteGovContractContext(10));
        }
    }

    class Test2 : ITest
    {
        public ICommandBuilder _command { get; set; }
        public ICommandFactory _factory { get; set; }

        public void Execute()
        {
            _command.Execute(new DeleteGovContractContext(10));
        }
    }
}
