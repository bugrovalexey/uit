﻿namespace GB.DataAccess.Command.AccountingObjects.ResponsiblePurchase
{
    public class SaveResponsiblePurchaseContext : ICommandContext
    {
        public int Id { get; set; }
        public int ResponsiblePurchase { get; set; }
    }
}
