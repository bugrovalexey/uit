﻿using GB.Data;
using GB.Data.Extensions;
using GB.DataAccess;

namespace GB.MKRF.Logic.Access.Handlers
{
    public abstract class AccessHandlerBase<T> : IAccessHandler where T : EntityBase, IWorkflowObject 
    {
        public abstract T Entity { get; set; }

        public AccessHandlerBase(T entity)
        {
            Entity = entity;
        }

        public virtual bool CanRead()
        {
            if (!Entity.Access.HasValue)
                Singleton<AccessEntityProvider>.Instance.SetAccess(Entity);

            return Entity.Access.Value.CanDo(AccessActionEnum.Read);
        }

        public virtual bool CanEdit()
        {
            if (!Entity.Access.HasValue)
                Singleton<AccessEntityProvider>.Instance.SetAccess(Entity);

            return Entity.Access.Value.CanDo(AccessActionEnum.Edit);
        }

        public virtual bool CanCreate()
        {
            if (!Entity.Access.HasValue)
                Singleton<AccessEntityProvider>.Instance.SetAccess(Entity);

            return Entity.Access.Value.CanDo(AccessActionEnum.Create);
        }

        public virtual bool CanDelete()
        {
            if (!Entity.Access.HasValue)
                Singleton<AccessEntityProvider>.Instance.SetAccess(Entity);

            return Entity.Access.Value.CanDo(AccessActionEnum.Delete);
        }

    }
}
