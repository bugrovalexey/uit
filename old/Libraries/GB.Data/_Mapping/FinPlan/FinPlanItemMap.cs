﻿using Uviri.Entity.Directories;
using Uviri.MKRF.Entities.FinPlan;

namespace Uviri.MKRF.Mapping.FinPlan
{
    public class FinPlanItemMap : ClassMapBase<FinPlanItem>
	{
        public FinPlanItemMap()
            : base("Fin_Plan_Item")
		{
//            Table("w_");

//            Id(x => x.Id, "Fin_Plan_Item_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_Fin_Plan_Item_Insert 
//						  @Fin_Plan_Item_Current_Year_Vol = ?
//						 ,@Fin_Plan_Item_First_Year_Vol = ?
//						 ,@Fin_Plan_Item_Second_Year_Vol = ?
//						 ,@Fin_Plan_Item_Current_Year_Add_Vol = ?
//						 ,@Fin_Plan_Item_First_Year_Add_Vol = ?
//						 ,@Fin_Plan_Item_Second_Year_Add_Vol = ?
//						 ,@Fin_Plan_ID = ?
//						 ,@Expense_Direction_ID = ?
//						 ,@Section_KBK_ID = ?
//						 ,@Expense_Item_ID = ?
//						 ,@Work_Form_ID = ?
//                         ,@Fin_Plan_Item_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Fin_Plan_Item_Update 
//						  @Fin_Plan_Item_Current_Year_Vol = ?
//						 ,@Fin_Plan_Item_First_Year_Vol = ?
//						 ,@Fin_Plan_Item_Second_Year_Vol = ?
//						 ,@Fin_Plan_Item_Current_Year_Add_Vol = ?
//						 ,@Fin_Plan_Item_First_Year_Add_Vol = ?
//						 ,@Fin_Plan_Item_Second_Year_Add_Vol = ?
//						 ,@Fin_Plan_ID = ?
//						 ,@Expense_Direction_ID = ?
//						 ,@Section_KBK_ID = ?
//						 ,@Expense_Item_ID = ?
//						 ,@Work_Form_ID = ?
//                         ,@Fin_Plan_Item_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Fin_Plan_Item_Delete 
//						  @Fin_Plan_Item_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.CurrentYearVolume, "Fin_Plan_Item_Current_Year_Vol");
			Map(x => x.FirstYearVolume, "Fin_Plan_Item_First_Year_Vol");
			Map(x => x.SecondYearVolume, "Fin_Plan_Item_Second_Year_Vol");
			Map(x => x.CurrentYearAddVolume, "Fin_Plan_Item_Current_Year_Add_Vol");
			Map(x => x.FirstYearAddVolume, "Fin_Plan_Item_First_Year_Add_Vol");
			Map(x => x.SecondYearAddVolume, "Fin_Plan_Item_Second_Year_Add_Vol");

			References<Entities.FinPlan.FinPlan>(x => x.FinPlan, "Fin_Plan_ID").Not.Nullable();
			References<ExpenseDirection>(x => x.ExpenseDirection, "Expense_Direction_ID").Not.Nullable();
			References<SectionKBK>(x => x.SectionKBK, "Section_KBK_ID").Not.Nullable();
			References<ExpenseItem>(x => x.ExpenseItem, "Expense_Item_ID").Not.Nullable();
			References<WorkForm>(x => x.WorkForm, "Work_Form_ID").Not.Nullable();
        }
    }
}