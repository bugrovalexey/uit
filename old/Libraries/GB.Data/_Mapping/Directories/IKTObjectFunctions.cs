﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class IKTObjectFunctionsMap : ClassMapBase<IKTObjectFunctions>
    {
        public IKTObjectFunctionsMap()
            : base("IKT_Object_Functions")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "IKT_Object_Functions_Name");
        }
    }
}