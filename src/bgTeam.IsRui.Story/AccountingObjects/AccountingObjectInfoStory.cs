﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AccountingObjectInfoStory : IStory<AccountingObjectInfoStoryContext, AccountingObjectInfoDto>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IMapperBase _mapper;
        private readonly IStory<AoContractsGetAllContractsStoryContext, IEnumerable<ActivityContractDto>> _getAllContractsStory;
        private readonly IStory<AoGetLinkedActivitiesStoryContext, IEnumerable<ActivityDto>> _getLinkedActivites;

        public AccountingObjectInfoStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase,
            IStory<AoContractsGetAllContractsStoryContext, IEnumerable<ActivityContractDto>> getAllContractsStory,
            IStory<AoGetLinkedActivitiesStoryContext, IEnumerable<ActivityDto>> getLinkedActivites)
        {
            _repositoryRui = repositoryRui;
            _mapper = mapperBase;
            _getAllContractsStory = getAllContractsStory;
            _getLinkedActivites = getLinkedActivites;
        }

        public AccountingObjectInfoDto Execute(AccountingObjectInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<AccountingObjectInfoDto> ExecuteAsync(AccountingObjectInfoStoryContext context)
        {
            var accObject = _repositoryRui.Get<AccountingObject>(context.Id);

            var dto = _mapper.Map(accObject, new AccountingObjectInfoDto());

            dto.CreateCost = accObject.AoActivities.Where(x => x.ActivityType == ActivityTypeEnum.Create).Select(x => x.CostY0).Sum();
            dto.DevelopCost = accObject.AoActivities.Where(x => x.ActivityType == ActivityTypeEnum.Develop).Select(x => x.CostY0).Sum();
            dto.ExploitationCost = accObject.AoActivities.Where(x => x.ActivityType == ActivityTypeEnum.Exploitation).Select(x => x.CostY0).Sum();

            var story = new AoDocumentBaseGetAllDocumentsStory(_repositoryRui, _mapper);
            dto.Documents = await story.ExecuteAsync(new AoDocumentBaseGetAllDocumentsStoryContext() { AoId = accObject.Id });

            dto.AOGovService = _repositoryRui
                .GetAllEx<AOGovService>(q => q.Where(x => x.AccountingObject.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new AOGovServiceDto()));

            dto.Characteristics = _repositoryRui.GetAllEx<AccountingObjectCharacteristics>(q => q
                .Fetch(x => x.Type).Eager
                .Fetch(x => x.Unit).Eager
                //.Where(x => x.IsRegister)
                .Where(x => x.AccountingObject.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new AccountingObjectCharacteristicsDto()));

            dto.Software = _repositoryRui.GetAllEx<Software>(
                q => q.Where(x => x.AccountingObject.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new SoftwareDto()));

            dto.WorksAndServices = _repositoryRui.GetAllEx<WorkAndService>(
                q => q.Where(x => x.AccountingObject.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new WorkAndServiceDto()));

            dto.InternalIsAndComponentsItki = _repositoryRui.GetAllEx<InternalIsAndComponentsItki>(
                q => q.Fetch(x => x.Object).Eager
                    .Fetch(x => x.Object.ResponsibleDepartment).Eager
                    .Fetch(x => x.Object.Kind).Eager
                    .Where(x => x.Owner.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new InternalIsAndComponentsItkiDto()));

            dto.ExternalIsAndComponentsItki = _repositoryRui.GetAllEx<ExternalIsAndComponentsItki>(
                q => q.Where(x => x.Owner.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new ExternalIsAndComponentsItkiDto()));

            dto.ExternalUserInterfacesOfIs = _repositoryRui.GetAllEx<ExternalUserInterfaceOfIs>(
                q => q.Fetch(x => x.TypeOfInterface).Eager.Where(x => x.Owner.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new ExternalUserInterfaceOfIsDto()));

            dto.GovPurchases = _repositoryRui.GetAllEx<GovPurchase>(
                q => q
                .Where(x => x.AccountingObject.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new GovPurchaseDto()));

            dto.GovContracts = _repositoryRui.GetAllEx<GovContract>(
                q => q.Where(x => x.AccountingObject.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new GovContractDto()));

            GovContract contr = null;
            dto.Acts = _repositoryRui.GetAllEx<ActOfAcceptance>(
                q => q.JoinAlias(x => x.GovContract, () => contr)
                .Where(x => contr.AccountingObject.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new ActOfAcceptanceDto()));

            dto.CreateCostFact = dto.Acts.Where(x => x.ActivityType == ActivityTypeEnum.Create).Select(x => x.Cost).Sum();
            dto.DevelopCostFact = dto.Acts.Where(x => x.ActivityType == ActivityTypeEnum.Develop).Select(x => x.Cost).Sum();
            dto.ExploitationCostFact = dto.Acts.Where(x => x.ActivityType == ActivityTypeEnum.Exploitation).Select(x => x.Cost).Sum();

            dto.TechnicalSupports = _repositoryRui.GetAllEx<TechnicalSupport>(
                q => q.Where(x => x.AccountingObject.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new TechnicalSupportDto()));

            dto.AccountingObjectActs = _repositoryRui.GetAllEx<AccountingObjectActs>(
                 q => q.Where(x => x.AccountingObject.Id == accObject.Id))
                .Select(x => _mapper.Map(x, new ActsDto()));

            dto.Contracts = await _getAllContractsStory.ExecuteAsync(new AoContractsGetAllContractsStoryContext { AoId = accObject.Id });
            dto.Activities = await _getLinkedActivites.ExecuteAsync(new AoGetLinkedActivitiesStoryContext { AoId = accObject.Id });

            dto.FinancingQuarter1 = accObject.FinancingQuarter1;
            dto.FinancingQuarter2 = accObject.FinancingQuarter2;
            dto.FinancingQuarter3 = accObject.FinancingQuarter3;
            dto.FinancingQuarter4 = accObject.FinancingQuarter4;

            return dto;
        }
    }
}
