﻿namespace bgTeam.AdminDB.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using bgTeam.AdminDB.Web.Domain;

    public class TableRowDataView
    {
        public object PrimaryKeyValue { get; internal set; }

        public IDictionary<string, object> Values { get; internal set; }

        public TableRowDataView(dynamic item, string pkField = null)
        {
            var obj = (IDictionary<string, object>)item;

            Values = new Dictionary<string, object>(obj, StringComparer.InvariantCultureIgnoreCase);

            if (pkField != null)
            {
                PrimaryKeyValue = Values[pkField];
            }
        }
    }
}