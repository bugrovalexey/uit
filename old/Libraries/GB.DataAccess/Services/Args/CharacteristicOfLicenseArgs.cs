﻿using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.WorksAndGoods;

namespace GB.DataAccess.Services.Args
{
    public class CharacteristicOfLicenseArgs : BaseArgsNew, IServiceArgs
    {
        public Software Software { get; set; }

        public string NameCharacteristicOfLicense { get; set; }

        public string Model { get; set; }

        public TypeOfConnectionToServerEnum? TypeOfConnectionToServer { get; set; }

        public int? NumberOnlineSameTime { get; set; }

        public string ProductNumber { get; set; }

        public int? NumberAllocatedSameTime { get; set; }

        public string Licensor { get; set; }

    }
}
