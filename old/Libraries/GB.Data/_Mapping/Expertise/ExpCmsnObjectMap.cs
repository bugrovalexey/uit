﻿using Uviri.MKRF.Entities.Expertise;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpCmsnObjectMap : ClassMapBase<ExpCmsnObject>
	{
        public ExpCmsnObjectMap()
            : base("Exp_Cmsn_Object")
		{
//            Table("w_");

//            Id(x => x.Id, "Exp_Cmsn_Object_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_Exp_Cmsn_Object_Insert
//						  @Exp_Cmsn_Object_Comm = ?
//						 ,@Exp_Cmsn_Meeting_ID = ?
//						 ,@Exp_Demand_ID = ?
//						 ,@Exp_Cmsn_Object_ID = ?").Check.None();
			
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Cmsn_Object_Update
//						  @Exp_Cmsn_Object_Comm = ?
//						 ,@Exp_Cmsn_Meeting_ID = ?
//						 ,@Exp_Demand_ID = ?
//						 ,@Exp_Cmsn_Object_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Cmsn_Object_Delete
//						  @Exp_Cmsn_Object_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Comm, "Exp_Cmsn_Object_Comm");

			References<ExpCmsnMeeting>(x => x.ExpCmsnMeeting, "Exp_Cmsn_Meeting_ID").Not.Nullable();
			References<ExpDemand>(x => x.ExpDemand, "Exp_Demand_ID").Not.Nullable();
        }
    }
}