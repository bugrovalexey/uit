﻿using Uviri.Entity.Directories.FPGU;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;
using Uviri.MKRF.Entities.Plans.ActivityOffers;

namespace Uviri.MKRF.Mapping.Plans.ActivityOffers
{
    public class ActivityOfferFunctionMap : ClassMapBase<ActivityOfferFunction>
    {
        public ActivityOfferFunctionMap()
            : base("Activity_Offer_Func")
	    {
	    }

        protected override void InitMap()
        {
            Map(x => x.Name, "Activity_Offer_Func_Functionality");
            Map(x => x.DirectedToExpl, "Activity_Offer_Func_Directed");
            Map(x => x.Comment, "Activity_Offer_Func_Comment");

            References<ActivityOffer>(x => x.Offer, "Activity_Offer_Inform_ID");
            References<FuncType>(x => x.Type, "Func_Type_ID");
			References<NPA>(x => x.NPA, "NPA_ID");

            References<Gov_Service>(x => x.Service, "Gov_Service_ID");
            References<NPA_Document>(x => x.Document, "NPA_Document_ID");
        }
    }
}