﻿namespace GB.MKRF.Entities.Plans
{
    public class PlansLots : EntityBase
    {
        public PlansLots()
        {
        }

        public PlansLots(PlansActivity activity)
        {
            Activity = activity;
        }

        public virtual PlansActivity Activity { get; protected set; }

        public virtual PlansZakupki Zakupki { get; set; }

        public virtual int? Number { get; set; }

        public virtual string Code { get; set; }

        public virtual string Name { get; set; }

        public virtual string Unit { get; set; }

        public virtual int? Count { get; set; }

        public virtual decimal Price { get; set; }
    }
}