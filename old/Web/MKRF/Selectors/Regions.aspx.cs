﻿using System;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.Repository;

namespace GB.MKRF.Web.Selectors
{
    public partial class Regions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            RegionsGrid.DataBind();

            base.OnPreRender(e);
        }

        protected void RegionsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            RegionsGrid.DataSource = RepositoryBase<Department>.GetAll(x => x.Type.Id == (int)DepartmentTypeEnum.Region);
        }
    }
}