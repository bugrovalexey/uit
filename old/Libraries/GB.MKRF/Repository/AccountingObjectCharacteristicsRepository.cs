﻿using GB.Data.AccountingObjects;
using System.Collections.Generic;

namespace GB.MKRF.Repository
{
    public class AccountingObjectCharacteristicsRepository : Repository<AccountingObjectCharacteristics>
    {
        public IList<AccountingObjectCharacteristics> GetAllByAccountingObject(int? accountingObjectId)
        {
            return GetAllEx(q => q
                .Fetch(x => x.Type).Eager
                .Fetch(x => x.Unit).Eager
                .Where(x => x.AccountingObject.Id == accountingObjectId && x.IsRegister));
        }
    }
}