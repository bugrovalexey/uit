﻿namespace GB.Data.Common
{
    public enum RoleEnum
    {
        /// <summary>Администратор</summary>
        Admin = 0,

        /// <summary>Ведомство (куратор)</summary>
        CA = 1,

        /// <summary>Тех. специалист</summary>
        Technician = 3,

        /// <summary>Экономист</summary>
        Economist = 4,

        /// <summary>Учереждение</summary>
        Institution = 60,
    }
}