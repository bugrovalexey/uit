﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.InformationInteractions
{
    public class AddOrSaveInformationInteractionContext : ICommandContext
    {        
        public int AccountingObjectId { get; set; }
        
        public string Name { get; set; }
        
        public string Purpose { get; set; }
        
        public int? TypeId { get; set; }
        
        public string SMEV { get; set; }
        
        public string Title { get; set; }
        
        public string Url { get; set; }

        public int? xId { get; set; }
    }
}
