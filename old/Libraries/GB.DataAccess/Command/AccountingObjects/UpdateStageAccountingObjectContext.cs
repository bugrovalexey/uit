﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateStageAccountingObjectContext : ICommandContext
    {
        public int Id { get; set; }

        public int StageId { get; set; }
    }
}
