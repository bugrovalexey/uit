﻿namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;

    internal class AccountingObjectContractMap : ClassMapHiLo<AccountingObjectContract>
    {
        public AccountingObjectContractMap()
       : base("OU_Contract") { }

        protected override void InitMap()
        {
            //Map(x => x.Id, "OU_Contract_ID");
            Map(x => x.Name, "OU_Contract_Name");
            Map(x => x.Number, "OU_Contract_Number");
            Map(x => x.BeginDate, "OU_Contract_BeginDate");
            Map(x => x.EndDate, "OU_Contract_EndDate");
            Map(x => x.ChangeDate, "TimeStamp");


            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
        }
    }
}
