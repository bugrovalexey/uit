﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.OtherExpObjects;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpObjectConclActMap : ClassMapBase<ExpObjectConclAct>
	{
        public ExpObjectConclActMap()
            : base("Exp_Concl_Act_Other")
		{
		}

        protected override void InitMap()
        {
            Map(x => x.Disagreement, "Exp_Concl_Act_Other_Disagreement");
            Map(x => x.AppropriateVolumeFinanceY0, "Exp_Concl_Act_Other_VF_Y0");
            Map(x => x.AppropriateVolumeFinanceY1, "Exp_Concl_Act_Other_VF_Y1");
            Map(x => x.AppropriateVolumeFinanceY2, "Exp_Concl_Act_Other_VF_Y2");
            Map(x => x.IsNeedFunding, "Exp_Concl_Act_Other_Is_Need_Funding");
            Map(x => x.Completion, "Exp_Concl_Act_Other_Completion");

            References<ExpObjectConclusion>(x => x.ExpConclusion, "Exp_Obj_Conclusion_ID").Not.Nullable();
			References<ExpActEquivalent>(x => x.ExpActEquivalent, "Exp_Act_Equivalent_ID").Not.Nullable();

            References<Appropriate>(x => x.GoalEffective, "Goal_Effective_ID");
            References<Appropriate>(x => x.OrderFinance, "Order_Finance_ID");

   

            HasMany<ExpObjectCriterionEquivalent>(x => x.ExpCriterionEquivalent)
                    .KeyColumns.Add("Exp_Concl_Act_Other_ID")
                    .Inverse()
                    .Cascade.AllDeleteOrphan();
        }
    }
}