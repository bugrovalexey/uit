﻿namespace bgTeam.IsRui.Common.Extentions
{
    using System;

    public static class ExceptionExtensions
    {
        public static Exception GetInnerException(this Exception exp)
        {
            if (exp.InnerException != null)
            {
                return GetInnerException(exp.InnerException);
            }

            return exp;
        }
    }
}