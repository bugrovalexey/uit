﻿using bgTeam.IsRui.Domain.Entities.Frgu;

namespace bgTeam.IsRui.Domain._Mapping.Frgu
{
    class GovServiceNPAMap : ClassMapHiLo<GovServiceNPA>
    {
        public GovServiceNPAMap()
            : base("NPA_Document") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "NPA_Document_Name");

            References(x => x.GovService, "Gov_Service_ID");
        }
    }
}
