﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans.ActivityOffers;

namespace Uviri.MKRF.Mapping.Plans.ActivityOffers
{
    public class ActivityOfferApfMap : ClassMapBase<ActivityOfferApf>
    {
        public ActivityOfferApfMap()
            : base("Activity_Offer_FAP")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Usage, "Activity_Offer_FAP_Usage");
            Map(x => x.FGIS_Number, "Activity_Offer_FAP_FGIS_Name");

            References<ApfInfo>(x => x.APF, "APF_Info_ID");
            References<ActivityOffer>(x => x.Offer, "Activity_Offer_Inform_ID");
        }
    }
}