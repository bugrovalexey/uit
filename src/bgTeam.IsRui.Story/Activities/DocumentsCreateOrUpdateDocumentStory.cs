﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Story.AccountingObjects;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class DocumentsCreateOrUpdateDocumentStory : IStory<DocumentsCreateOrUpdateDocumentStoryContext, ActivityDocumentDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IFileProviderRui _fileProvider;

        public DocumentsCreateOrUpdateDocumentStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            ISessionNHibernateFactory factory,
            IFileProviderRui fileProvider)
        {
            _mapper = mapper;
            _repository = repository;
            _factory = factory;
            _fileProvider = fileProvider;
        }

        public ActivityDocumentDto Execute(DocumentsCreateOrUpdateDocumentStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityDocumentDto> ExecuteAsync(DocumentsCreateOrUpdateDocumentStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            Files file;

            ActivityDocuments result;

            using (var session = _factory.OpenSession())
            {
                if (context.Id.HasValue)
                {
                     file = _repository.Get<Files>(context.file.Id);

                    result = _repository.Get<ActivityDocuments>(context.Id);

                    if (file != null && context.file.Id != file.Id && context.file.Id.HasValue)
                    {
                        await new FileDeleteStory(_repository, _fileProvider, _factory)
                            .ExecuteAsync(new FileDeleteStoryContext()
                            {
                                Id = file.Id
                            });
                    }

                    file = await SaveFile(context);
                }
                else
                {
                    result = new ActivityDocuments();

                    file = await SaveFile(context);
                }

                _mapper.Map(context, result);

                if (file != null)
                {
                    result.FileId = file.Id;
                    result.FileName = file.Name;
                }

                result.Activity = new PlansActivity() { Id = context.ActivityId.Value };
                    //_repository.Get<PlansActivity>(context.ActivityId);

                result.ChangeDate = DateTime.Now;

                await session.SaveOrUpdateAsync(result);

                //return _mapper.Map(result, new ActivityDocumentDto());

                var dto = _mapper.Map(result, new ActivityDocumentDto());

                dto.file.Kind = file.Kind;

                dto.file.Size = file.Size;

                dto.file.Url = file.Url;

                dto.file.MimeType = file.MimeType;

                dto.file.Extension = file.Extension;

                return dto;
            }
        }

        private async Task<Files> SaveFile(DocumentsCreateOrUpdateDocumentStoryContext context)
        {
            var addFile = new FileAddStory(_mapper, _repository, _fileProvider, _factory);

            if (context.file.Url == null)
            {
                var result = await addFile.ExecuteAsync(new FileAddStoryContext()
                {
                    Type = DocTypeEnum.ActivityFEO,
                    Entity = _repository.Get<PlansActivity>(context.ActivityId),
                    Name = context.file.FileName,
                    TempGuid = context.file.TempGuid,
                    MimeType = context.file.MimeType,
                    Kind = context.file.Kind,
                    Size = context.file.Size,
                    Extension = context.file.Extension,
                });
                return result;
            }
            else
            {
                var result = await addFile.ExecuteAsync(new FileAddStoryContext()
                {
                    Type = DocTypeEnum.ActivityFEO,
                    Entity = _repository.Get<PlansActivity>(context.ActivityId),
                    Name = context.file.FileName,
                    Url = context.file.Url,
                    MimeType = context.file.MimeType,
                    Kind = context.file.Kind,
                    Size = context.file.Size,
                    Extension = context.file.Extension,
            });
                return result;
            }
        }
    }
}
