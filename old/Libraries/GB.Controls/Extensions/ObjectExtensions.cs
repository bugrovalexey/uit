﻿using DevExpress.Web.ASPxEditors;
using System;
using System.Collections;

namespace GB.Controls.Extensions
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Устанавливает редактируемое значение
        /// </summary>
        /// <param name="sender">Контрол</param>
        /// <param name="value">Значение</param>
        public static ASPxEditBase SetValueTextBox(this object sender, object value)
        {
            if (sender is ASPxEditBase)
            {
                var box = (sender as ASPxEditBase);

                box.Value = value;

                return box;
            }
            else
            {
                throw new ArgumentException("Невозможно проставить значение, так как контрол имеет неизвестный тип.", sender.GetType().ToString());
            }
        }

        public static void SetValueTextBoxVal<T>(this object sender, T value) where T : struct
        {
            sender.SetValueTextBox(value);
        }

        public static ASPxComboBox SetDataComboBox(this object sender, IEnumerable data)
        {
            if (sender is ASPxComboBox)
            {
                var box = sender as ASPxComboBox;

                box.DataSource = data;

                return box;
            }

            throw new ArgumentException("Невозможно проставить значение, так как контрол имеет неизвестный тип.", sender.GetType().ToString());
        }

        public static ASPxListBox SetDataListBox(this object sender, IEnumerable data)
        {
            if (sender is ASPxListBox)
            {
                var box = sender as ASPxListBox;

                box.DataSource = data;

                return box;
            }

            throw new ArgumentException("Невозможно проставить значение, так как контрол имеет неизвестный тип.", sender.GetType().ToString());
        }


        public static ASPxComboBox SetValueComboBox<T>(this ASPxComboBox box, T value) where T : struct
        {
            box.Value = value;

            return box;
        }

        public static ASPxComboBox SetValueComboBoxEnt(this ASPxComboBox box, GB.IEntity value)
        {
            if (value != null)
                box.Value = value.Id;

            return box;
        }

        public static ASPxComboBox SetIndexComboBox(this ASPxComboBox box, GB.IEntity value, int index)
        {
            if (value != null)
                box.Value = value.Id;
            else
                box.SelectedIndex = index;

            return box;
        }

        /// <summary>
        /// Задать значение или установить индекс, если значение null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="box"></param>
        /// <param name="value"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static ASPxComboBox SetIndexComboBox<T>(this ASPxComboBox box, T? value, int index) where T : struct
        {
            if (value.HasValue)
                box.Value = value;
            else
                box.SelectedIndex = index;

            return box;
        }

        //public static void SetIndexCombobox<T>(this ASPxComboBox box, T value) where T : struct
        //{
        //    box.Value = value;
        //}

        //public static void SetValueSelectBox<T>(this ASPxComboBox box, T value) where T : struct
        //{
        //    string res;

        //    if (entity == null)
        //    {
        //        res = "Не задано";
        //        hf.Set(field + "Id", -1);
        //        hf.Set(field + "Name", "Не задано");
        //    }
        //    else
        //    {
        //        res = entity.FullName;
        //        hf.Set(field + "Id", entity.Id);
        //        hf.Set(field + "Name", entity.Name);
        //    }

        //    return res;
        //}
    }
}