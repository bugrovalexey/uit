﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
	/// <summary>Цель и задача мероприятия Плана</summary>
	public class PlansActGT : EntityBase
	{
        protected PlansActGT()
        {
        }

        public PlansActGT(PlansActivity PlanActivity)
        {
            this.PlanActivity = PlanActivity;
        }
        
		/// <summary>Мероприятие Плана</summary>
		public virtual PlansActivity PlanActivity { get; protected set; }

		/// <summary>Цель</summary>
		public virtual string Goal { get; set; }//null

		/// <summary>Задача</summary>
		public virtual string Task { get; set; }//null

		/// <summary>Приоритет</summary>
		public virtual string Priority { get; set; }//null

		/// <summary></summary>
		public virtual string PropsNormAct { get; set; }//null

		/// <summary>Функциональность</summary>
		public virtual string Functionality { get; set; }//null

		/// <summary>Тип функции</summary>
		public virtual FuncType FuncType { get; set; }

		/// <summary></summary>
		public virtual string GovtPropsNormAct { get; set; }//null

		/// <summary>Направление мероприятия Плана</summary>
		public virtual PlansActivityDirection PlanActivityDirection { get; set; }
	}
}