﻿namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class ActivityGoodsMap : ClassMapHiLo<ActivityGoods>
    {
        public ActivityGoodsMap()
            : base("Plans_Activity_Goods")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Goods_Name");
            //Map(x => x.Year, "Plans_Activity_Goods_Year");
            Map(x => x.Quantity, "Plans_Activity_Goods_Quantity");
            //Map(x => x.Duration, "Plans_Activity_Goods_Duration");
            Map(x => x.Cost, "Plans_Activity_Goods_Cost");
            Map(x => x.Summ, "Plans_Activity_Goods_Summ");
            Map(x => x.ChangeDate, "TimeStamp");
            Map(x => x.PlansActivityId, "Plans_Activity_ID");
            Map(x => x.Quarter, "Plans_Activity_Goods_Quarter");
            Map(x => x.AnalogCost, "Plans_Activity_Goods_AnalogCost");
            Map(x => x.AnalogName, "Plans_Activity_Goods_AnalogName");

            //References(x => x.OKPD, "Plans_Activity_Goods_OKPDId");
            //References(x => x.ProductGroup, "Plans_Activity_Goods_ProductGroupId");
            //References(x => x.ExpenseDirection, "Plans_Activity_Goods_ExpenseDirectionId");
            //References(x => x.ExpenseType, "Plans_Activity_Goods_ExpenseTypeId");
            //References(x => x.KOSGU, "Plans_Activity_Goods_KOSGUId");

            HasMany(x => x.Characteristic).KeyColumn("Plans_Activity_Goods_ID").Inverse().Cascade.Delete();
        }
    }
}
