﻿using GB.Controls;
using GB.Handlers;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Dtos;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Repository;
using GB.Extentions;
using GB.Controls.Extensions;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB;

namespace GB.MKRF.Web.Activities
{
    public partial class CommonData : System.Web.UI.Page
    {
        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as EditCard).CurrentActivity;
            }
        }


        public static string GetUrl(object activityId, Dictionary<string, string> list = null)
        {
            var param = string.Empty;

            if (list != null)
            {
                foreach (var item in list)
                {
                    param = string.Format("{2}&{0}={1}", item.Key, item.Value, param);
                }
            }

            return string.Format("{0}?{1}={2}{3}", UrlHelper.Resolve("~/Activities/CommonData.aspx"), EditCard.ActivityIdKey, activityId, param);
        }

        protected string GetResponsibleUrl(string type)
        {
            return String.Format("../Selectors/UsersOne.aspx?id={0}&type={1}",
                CurrentActivity.ResponsibleInformation == null ? 0 : CurrentActivity.ResponsibleInformation.Id, type);
        }

        protected string GetOUUrl()
        {
            return String.Format("../Selectors/OU.aspx?id={0}&type=OU",
                CurrentActivity.ResponsibleInformation == null ? 0 : CurrentActivity.ResponsibleInformation.Id);
        }

        protected bool IsOUEdit()
        {
            return CurrentActivity.ActivityType2.IsEq(ActivityType2Enum.Create);
        }


        protected override void OnPreRender(EventArgs e)
        {
            DepCheckBox.DataBind();
            ResponsibleInformationTextBox.DataBind();
            ResponsibleTechnicalTextBox.DataBind();
            ResponsibleFinancialTextBox.DataBind();
            FileUploader.DataBind();

            UONameTextBox.DataBind();
            UONameSmallTextBox.DataBind();
            OUTypeComboBox.DataBind();
            OUKindComboBox.DataBind();

            OUTextBox.DataBind();
            //OUPopupControl.DataBind();

            ReasonsGrid.DataBind();

            ShowAlertMassage();

            base.OnPreRender(e);
        }

        private void ShowAlertMassage()
        {
            var message = Request.QueryString["alr"];

            if (!string.IsNullOrWhiteSpace(message))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MessageBlock", string.Concat("alert('", message, "');"), true);
            }
        }

        protected void DepCheckBox_DataBinding(object sender, EventArgs e)
        {
            (sender as ASPxLabel).Text = CurrentActivity.Department.Name;
        }


        protected void ResponsibleInformationTextBox_DataBinding(object sender, EventArgs e)
        {
            var box = sender as ASPxTextBox;

            box.Text = SetHF(CurrentActivity.ResponsibleInformation, "ResponsibleInformation");
        }
        
        protected void ResponsibleTechnicalTextBox_DataBinding(object sender, EventArgs e)
        {
            var box = sender as ASPxTextBox;

            box.Text = SetHF(CurrentActivity.ResponsibleTechnical, "ResponsibleTechnical");
        }

        protected void ResponsibleFinancialTextBox_DataBinding(object sender, EventArgs e)
        {
            var box = sender as ASPxTextBox;

            box.Text = SetHF(CurrentActivity.ResponsibleFinancial, "ResponsibleFinancial");
        }
        
        
        private string SetHF(User entity, string field)
        {
            string res;

            if (entity == null)
            {
                res = "Не задано";
                hf.Set(field + "Id", -1);
                hf.Set(field + "Name", "Не задано");
            }
            else
            {
                res = entity.FullName;
                hf.Set(field + "Id", entity.Id);
                hf.Set(field + "Name", entity.Name);
            }

            return res;
        }
        
        protected void FileUploader_DataBinding(object sender, EventArgs e)
        {
            FileUploaderDataBinding(sender as FileUploadControl2, CurrentActivity.Documents);
        }
   

        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            var controller = new PlansActivityController();
            
            controller.Update3(new PlansActivityArgs()
            {
                Id = CurrentActivity.Id,
                ResponsibleInformation = hf["ResponsibleInformationId"],
                ResponsibleTechnical = hf["ResponsibleTechnicalId"],
                ResponsibleFinancial = hf["ResponsibleFinancialId"],
                AccountingObject = hf["OUId"],
                Files = hf["ActivityFiles"]
            });

            controller.Repository.Refresh(CurrentActivity);

            if(IsOUEdit())
                new AccountingObjectService()
                    .Update(new AccountingObjectControllerArgs()
                    {
                        Id = CurrentActivity.AccountingObject.Id,
                        FullName = UONameTextBox.Value as string,
                        ShortName = UONameSmallTextBox.Value as string,
                        Type = Convert.ToInt32(OUTypeComboBox.Value),
                        Kind = Convert.ToInt32(OUKindComboBox.Value),
                    });
        }


        protected void OUTextBox_DataBinding(object sender, EventArgs e)
        {
            if (CurrentActivity.AccountingObject != null)
            {
                var box = sender as ASPxTextBox;
                var entity = CurrentActivity.AccountingObject;

                box.Text = entity.ShortName;

                hf.Set("OUId", entity.Id);
                hf.Set("OUName", entity.Name);
            }
            else
            {
                hf.Set("OUId", -1);
            }
        }

        #region OU Popup
        protected void UONameTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(CurrentActivity.AccountingObject.With(x => x.Name));
        }

        protected void UONameSmallTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(CurrentActivity.AccountingObject.With(x => x.ShortName));
        }

        protected void OUTypeComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(new IKTComponentRepository().GetAllParents())
                .SetValueComboBox<int>(CurrentActivity.AccountingObject.Return(x => x.Type.Id, EntityBase.Default));
        }

        protected void OUKindComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(new IKTComponentRepository().GetAllChildren(Convert.ToInt32(OUTypeComboBox.Value)))
                .SetValueComboBox<int>(CurrentActivity.AccountingObject.Return(x => x.Kind.Id, EntityBase.Default));
        }

        protected void OUKindComboBox_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            sender.SetDataComboBox(new IKTComponentRepository().GetAllChildren(Convert.ToInt32(e.Parameter)))
                .SetValueComboBox<int>(EntityBase.Default).DataBind();
        }
        #endregion


        #region ReasonGrid
        PlansActivityReasonController reasonController = new PlansActivityReasonController();
        protected void ReasonsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            if (CurrentActivity != null)
                ReasonsGrid.DataSource =
                    EntityHelper.ConvertToAnonymous<PlansActivityReason>(CurrentActivity.ActivityReasons,
                        delegate(PlansActivityReason ar)
                        {
                            return new
                            {
                                Id = ar.Id,
                                Name = string.Format("{0} \"{1}\"", ar.NPAType.Name, ar.Name),
                                Rekvisit = string.Format("№{0} {1}", ar.Num, ar.Date.HasValue ? ("от " + Convert.ToDateTime(ar.Date).ToShortDateString()) : null),
                                Item = ar.Item,
                                FileInfo = string.Join(";", ar.Documents.Select(x => (x.Id.ToString() + ":" + x.FileName)).ToArray())
                            };
                        });
        }
        
        protected void ReasonsGrid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            reasonController.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void ReasonsGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            reasonController.Create(new PlansActivityReasonArgs()
            {
                ActivityId = CurrentActivity.Id,
                Name = hf["ReasonName"],
                Type = hf["ReasonTypeId"],
                Num = hf["ReasonNum"],
                Date = hf["ReasonDate"],
                Item = hf["ReasonItem"],
                Files = hf["ReasonFiles"]
            });

            ReasonsGrid.GridCancelEdit(e);
        }

        protected void ReasonsGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            reasonController.Update(new PlansActivityReasonArgs()
            {
                Id = e.Keys[0],
                Name = hf["ReasonName"],
                Type = hf["ReasonTypeId"],
                Num = hf["ReasonNum"],
                Date = hf["ReasonDate"],
                Item = hf["ReasonItem"],
                Files = hf["ReasonFiles"]
            });

            ReasonsGrid.GridCancelEdit(e);
        }

        protected void ReasonsGrid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            reasonController.Delete(new PlansActivityReasonArgs()
            {
                Id = e.Keys[0]
            });

            ReasonsGrid.GridCancelEdit(e);
        }


        protected void ReasonNPANameTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxMemo box = (ASPxMemo)sender;
            box.Text = reasonController.CurrentEdit == null ? string.Empty : reasonController.CurrentEdit.Name;
        }

        protected void ReasonNPATypeComboBox_DataBinding(object sender, EventArgs e)
        {
            Dictionary<int, string> listStr = new Dictionary<int, string>();
            IList<NPAType> list = RepositoryBase<NPAType>.GetAll();

            foreach (NPAType item in list)
            {
                listStr.Add(item.Id, item.Name);
            }

            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = listStr;

            box.Value = reasonController.CurrentEdit == null ? 0 : reasonController.CurrentEdit.NPAType.Id;
        }

        protected void ReasonNPANumTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = reasonController.CurrentEdit == null ? string.Empty : reasonController.CurrentEdit.Num;
        }

        protected void ReasonNPADateDateEdit_DataBinding(object sender, EventArgs e)
        {
            ASPxDateEdit box = (ASPxDateEdit)sender;

            box.Value = reasonController.CurrentEdit == null ? null : reasonController.CurrentEdit.Date;
        }

        protected void ReasonItemTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = reasonController.CurrentEdit == null ? string.Empty : reasonController.CurrentEdit.Item;
        }

        protected void uploader2_DataBinding(object sender, EventArgs e)
        {
            if (reasonController.CurrentEdit != null)
            {
                FileUploaderDataBinding(sender as FileUploadControl2, reasonController.CurrentEdit.Documents);
            }


            //FileUploadControl2 uploader = sender as FileUploadControl2;
            //uploader.FileItemTemplate = "<a href=\"" + FileHandler.GetUrl(string.Empty) + "{id}\">{name}</a>";

            //if (reasonController.CurrentEdit != null)
            //{
            //    foreach (Document doc in reasonController.CurrentEdit.Documents)
            //    {
            //        uploader.SetValue(doc.FileName, doc.Id.ToString());
            //    }
            //}
        }


        protected string getDownloadUrl(string FileInfo)
        {
            if (string.IsNullOrEmpty(FileInfo))
                return null;

            List<string> links = new List<string>();

            string[] files = FileInfo.Split(';');
            foreach (string f in files)
            {
                string[] pair = f.Split(':');
                links.Add(string.Format("<a href='{0}'>{1}</a>", FileHandler.GetUrl(pair[0]),
                                                                 pair.Length > 1 ? pair[1] : "Скачать"));
            }
            return string.Join("<br />", links);
        }
        #endregion


        private void FileUploaderDataBinding(FileUploadControl2 uploader, IList<Document> list)
        {
            uploader.FileItemTemplate = "<a href=\"" + FileHandler.GetUrl(string.Empty) + "{id}\">{name}</a>";

            foreach (Document doc in list)
            {
                uploader.SetValue(doc.FileName, doc.Id.ToString());
            }
        }

        

        

        
    }
}