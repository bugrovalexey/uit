﻿using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Plans;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    [MetadataType(typeof(CreateVeiwModel_Valid))]
    public class CreateVeiwModel : IPlansActivityCreateArgs
    {
        public int Id { get; set; }

        public string NameSmall { get; set; }

        public int Type { get; set; }

        public System.Web.Mvc.SelectList TypeList { get; set; }

        public CreateVeiwModel()
        {
            TypeList = new System.Web.Mvc.SelectList(new RepositoryDc<ActivityType>().GetAll(), "Id", "Name", EntityBase.Default);
        }
    }

    public class CreateVeiwModel_Valid : ValidateViewModel
    {
        [DisplayName("Наименование")]
        [Required(ErrorMessage = RequiredText)]
        public string NameSmall { get; set; }

        [DisplayName("Тип")]
        [RangeAttribute(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        [Required(ErrorMessage = RequiredText)]
        public int Type { get; set; }
    }
}