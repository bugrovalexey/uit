﻿using Uviri.MKRF.Entities.Prognosis;
using Uviri.MKRF.Entities.Prognosis.SectionIII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionIII
{
    public class FuncSupportMap : ClassMapBase<FuncSupport>
	{
        public FuncSupportMap()
            : base("MF3_13")
        {
//            Table("w_");

//            Id(x => x.Id, "MF3_13_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF3_13_Insert
//                          @MF3_13_Num = ?
//                         ,@MF3_13_1 = ?
//                         ,@MF3_13_2 = ?
//                         ,@MF3_13_3 = ?
//                         ,@MF3_13_4 = ?
//                         ,@MF3_13_5 = ?
//                         ,@MF3_13_6 = ?
//                         ,@MF3_13_7 = ?
//                         ,@MF3_13_8 = ?
//                         ,@MF3_13_9 = ?
//                         ,@MF3_13_10 = ?
//                         ,@Mon_Form_ID = ?
//                         ,@MF3_13_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF3_13_Update
//                          @MF3_13_Num = ?
//                         ,@MF3_13_1 = ?
//                         ,@MF3_13_2 = ?
//                         ,@MF3_13_3 = ?
//                         ,@MF3_13_4 = ?
//                         ,@MF3_13_5 = ?
//                         ,@MF3_13_6 = ?
//                         ,@MF3_13_7 = ?
//                         ,@MF3_13_8 = ?
//                         ,@MF3_13_9 = ?
//                         ,@MF3_13_10 = ?
//                         ,@Mon_Form_ID = ?
//                         ,@MF3_13_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF3_13_Delete
//						  @MF3_13_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "MF3_13_Num");
            Map(x => x.Function, "MF3_13_1");
            Map(x => x.Name, "MF3_13_2");
            Map(x => x.MainTask, "MF3_13_3");
            Map(x => x.BaseCreation, "MF3_13_4");
            Map(x => x.YearIntroduction, "MF3_13_5");
            Map(x => x.UsedSoftware, "MF3_13_6");
            Map(x => x.CreatorProject, "MF3_13_7");
            Map(x => x.CountUsers, "MF3_13_8");
            Map(x => x.StructuralUnitMainUser, "MF3_13_9");
            Map(x => x.Resource, "MF3_13_10");

            References<Prognos>(x => x.Prognos, "Mon_Form_ID").Not.Nullable();
        }
    }
}
