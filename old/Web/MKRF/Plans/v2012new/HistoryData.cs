﻿using System;
using System.Collections.Generic;
using System.Data;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Repository;

namespace GB.MKRF.Web.Plans.v2012new
{
    public class HistoryData
    {
        public const string version1 = "1";
        public const string version2 = "2";
        public const string idColumnName = "Id";
        public const string keyColumnName = "Key";
        public const string valueColumnName = "Value";
        public const string orderColumnName = "Order";

        // Формат столбца RowId   [номер]__[версия]@[изменения]
        public const string versionColumnName = "Version";
        public const string changesColumnName = "Changes";
        public const string captionColumnName = "Caption";
        public const string rowIdColumnName = "RowId";
        public const string emptyCellCode = " —";

        public const string redHeaderTemplate = "<div class='r3_del gridHeader compareRed'>Удалённые записи</div>";
        public const string yellowHeaderTemplate = "<div class='r0_cmp1 gridHeader compareYellow'>Сравнение № {0}</div>";
        public const string greenHeaderTemplate = "<div class='r2_new gridHeader compareGreen'>Новые записи</div>";
        public const string whiteHeaderTemplate = "<div class='r1_cmp2 gridHeader compareWhite'>Сравнение № {0}</div>";
        public const string LegendTemplate = "<b>Версия от {0} (жирный шрифт)</b><br/>Версия от {1} (обычный шрифт)";

        DateTime date1;
        DateTime? date2;
        int CurrentActivityId;
 
        public HistoryData(int CurrentActivityId,  DateTime date1, DateTime? date2)
        {
            this.CurrentActivityId = CurrentActivityId;
            this.date1 = date1;
            this.date2 = date2;
        }

        // источники данных / кэш
        Dictionary<string, DataTable> dataSources = new Dictionary<string, DataTable>();

        public DataTable getDataSource(string dataSourceName, object ownerKey = null)
        {
            DataTable table = null;

            if (dataSources.ContainsKey(dataSourceName))
            {
                table = dataSources[dataSourceName];
            }
            else
            {
                table = extractTable(dataSourceName, date1);

                if (table != null && this.date2.HasValue)
                {
                    DataTable table2 = extractTable(dataSourceName, date2);
                    table = mergeTables(table, table2);
                }

                dataSources.Add(dataSourceName, table);
            }

            if (ownerKey != null && table != null)
            {
                table = filterRows(table, dataSourceName, ownerKey);
            }
            return table;
        }

        protected DataTable filterRows(DataTable table, string dataSourceName, object ownerKey)
        {
            if ((dataSourceName == "gridTypesWork" || dataSourceName == "gridTypesSpec")
                && ownerKey != null)
            {
                string ownerId = ownerKey.ToString();
                DataTable filteredTable = table.Clone();
                foreach (DataRow row in table.Rows)
                {
                    if (row["OwnerId"].ToString() == ownerId)
                    {
                        filteredTable.Rows.Add(row.ItemArray);
                    }
                }
                return filteredTable;
            }

            return table;
        }

        // идентификатор в таблицах должен быть в столбце Id (idColumnName)
        public static DataTable mergeTables(DataTable table1, DataTable table2)
        {
            if (table1 == null || table2 == null)
                throw new ArgumentException();

            DataTable table3 = table1.Clone();
            table3.Columns.Add(versionColumnName);
            table3.Columns.Add(rowIdColumnName);
            table3.Columns.Add(captionColumnName);
            table3.Columns.Add(changesColumnName);


            HashSet<string> columns = new HashSet<string>();
            Dictionary<object, DataRow> IdToRow = new Dictionary<object, DataRow>();
            int counter = 0;


            // составляем справочник столбцов
            foreach (DataColumn column in table3.Columns)
            {
                if ((column.ColumnName != versionColumnName) &&
                    (column.ColumnName != rowIdColumnName) &&
                    (column.ColumnName != captionColumnName) &&
                    (column.ColumnName != changesColumnName))
                {

                    columns.Add(column.ColumnName);
                }
            }

            // сливаем версию 1
            table3.Merge(table1);
            foreach (DataRow row in table3.Rows)
            {
                row[versionColumnName] = "1";
                row[rowIdColumnName] = (++counter).ToString() + "__1";
                row[captionColumnName] = counter.ToString("000");
                IdToRow.Add(row[idColumnName], row);
            }

            int rowIndex = table3.Rows.Count;
            // сливаем версию2
            table3.Merge(table2);
            for (; rowIndex < table3.Rows.Count; rowIndex++)
            {
                DataRow row2 = table3.Rows[rowIndex];
                row2[versionColumnName] = "2";
                row2[rowIdColumnName] = (++counter).ToString() + "__2";

                if (IdToRow.ContainsKey(row2[idColumnName]))
                {
                    DataRow row1 = IdToRow[row2[idColumnName]];



                    List<string> rowChanges = new List<string>();

                    foreach (string columnName in columns)
                    {
                        if (columnName == idColumnName ||
                            columnName == keyColumnName ||
                            columnName == orderColumnName)
                            continue;

                        if (row2[columnName].ToString() != row1[columnName].ToString())
                        {
                            rowChanges.Add(columnName);
                        }
                    }



                    row1[changesColumnName] = string.Join(";", rowChanges);

                    row1[rowIdColumnName] = (row1[rowIdColumnName] + "@" + row1[changesColumnName]);

                    row2[captionColumnName] = row1[captionColumnName] = rowChanges.Count > 0 ?
                                                                            string.Format(yellowHeaderTemplate, row1[captionColumnName]) :
                                                                            string.Format(whiteHeaderTemplate, row1[captionColumnName]);
                }
                else
                {
                    row2[captionColumnName] = redHeaderTemplate;
                    row2[changesColumnName] = "Delete";
                }
            }

            foreach (DataRow r in table3.Rows)
            {
                if (r[captionColumnName].ToString().Length <= 3)
                {
                    r[captionColumnName] = greenHeaderTemplate;
                    r[changesColumnName] = "New";
                }
            }

            return table3;
        }

        protected DataTable extractTable(string dataSourceName, DateTime? date)
        {
            DataTable table = null;

            switch (dataSourceName)
            {
                case "gridResponsible": table = getResponsible(date.Value); break;
                case "gridResponsible2": table = getResponsible2(date.Value); break;


                case "gridFunc": table = getFuncs(date.Value); break; // отсутствуют wh_.... уточнить
                case "gridReason": table = getReasons(date.Value); break; // отсутствуют wh_.... уточнить
                //case "gridAPFInfo": table = getAPFInfo(date.Value); break; // отсутствует  wh_Plans_Activity_APF_Info
                case "gridZOD":  break;

                case "gridExpense1": table = getExpenses1(date.Value); break;
                case "gridExpense2": table = getExpenses2(date.Value); break;
                case "gridExpense3": table = getExpenses3(date.Value); break;
                case "gridExpenseDirection": table = getExpenseDirections(date.Value); break;


                case "gridPlansActivityGoal": table = getPlansActivityGoal(date.Value); break;
                case "gridStateContract": table = getGetStateContract(date.Value); break;
                case "gridGoalIndicator": table = getGoalIndicators(date.Value); break;

                case "gridSpecification": table = getSpecifications(date.Value); break;
                case "gridTypesSpec": table = getTypesSpec(date.Value); break;
                case "gridWork": table = getWorks(date.Value); break;
                case "gridTypesWork": table = getTypesWork(date.Value); break;

                default: return null;
                //throw new NotImplementedException();
            }
            return table;
        }



        protected DataTable createEmptyInvertedTable()
        {
            DataTable table = new DataTable();
            table.Columns.Add(idColumnName);
            table.Columns.Add(keyColumnName);
            table.Columns.Add(valueColumnName);
            table.Columns.Add(orderColumnName);
            return table;

        }


        protected DataTable getResponsible(DateTime date)
        {
            DataTable table = extractActivity(date);
            DataTable responsible = null;

            responsible = createEmptyInvertedTable();

            DataRow row = table.Rows.Count > 0 ? table.Rows[0] : table.NewRow();

            string key = null;
            int order = 0;

            foreach (DataColumn column in table.Columns)
            {
                switch (column.ColumnName)
                {
                    case "Plans_Activity_Type2_Name": order = 50; key = "Тип мероприятия:"; break;
                    case "Year": order = 60; key = "Плановый год окончания мероприятия:"; break;
                    case "FGIS_Info_Name": order = 70; key = "Регистрационный номер в реестре ФГИС:"; break;
                    case "Plans_Activity_Fgis_Name": order = 80; key = "Наименование ФГИС:"; break;
                    case "Plans_Activity_Fgis_Info_Text": order = 90; key = "Наименование ИС или объекта ИКТ-инфраструктуры:"; break;
                    case "IKT_Component_Name": order = 100; key = "Классификационный признак ИС или компонента ИТ-инфраструктуры:"; break;
                    case "Plans_Activity_P_Subentry": order = 110; key = "Наименование подпрограммы ГП \"Информационное общество\":"; break;
                    // case "Plans_Activity_Other_Data": order = 120; key = "Иные сведения:"; break;
                    case "Plans_Activity_Annotation_Data": order = 130; key = "Аннотация:"; break;
                    case "Plans_Activity_Responsible_Department_Name": order = 140; key = "Субъект Российской Федерации (орган местного управления), осуществляющий мероприятие по информатизации:"; break;
                    case "Source_Funding_Name": order = 150; key = "Источник финансирования"; break;

                    default:
                        continue;
                }

                if (date2.HasValue && date == date2.Value)
                {
                    key = "";
                    order = order + 1;
                }

                string value = row[column].ToString();

                responsible.Rows.Add(new object[] { column.ColumnName, key, string.IsNullOrEmpty(value) ? emptyCellCode : value, order.ToString("0000") });
            }
            return responsible;
        }

        protected DataTable getResponsible2(DateTime date)
        {
            DataTable table = extractActivity(date);
            DataTable responsible = null;

            responsible = createEmptyInvertedTable();

            DataRow row = table.Rows.Count > 0 ? table.Rows[0] : table.NewRow();

            string key = null;
            int order = 0;

            foreach (DataColumn column in table.Columns)
            {
                // извлекаем данные об ответственном
                switch (column.ColumnName)
                {
                    case "Responsible_FIO": order = 10; key = "ФИО:"; break;
                    case "Responsible_Job": order = 20; key = "Должность:"; break;
                    case "Responsible_Phone": order = 30; key = "Телефон:"; break;
                    case "Responsible_Email": order = 40; key = "Email:"; break;

                    default:
                        continue;
                }

                if (date2.HasValue && date == date2.Value)
                {
                    key = "";
                    order = order + 1;
                }

                string value = row[column].ToString();

                responsible.Rows.Add(new object[] { column.ColumnName, key, string.IsNullOrEmpty(value) ? emptyCellCode : value, order.ToString("0000") });
            }
            return responsible;
        }

        public DataTable extractActivity(DateTime date)
        {
            string dsKey = "Activity" + date.Ticks.ToString();
            if (!dataSources.ContainsKey(dsKey))
            {
                dataSources[dsKey] = HistoryRepository.GetActivity(CurrentActivityId, date);
            }
            return dataSources[dsKey];
        }


        protected DataTable getExpenses1(DateTime date)
        {
            DataTable table = extractActivity(date);
            DataTable expenses = createEmptyInvertedTable();

            DataRow row = table.Rows.Count > 0 ? table.Rows[0] : table.NewRow();
            string key;
            int order;
            foreach (DataColumn column in table.Columns)
            {
                switch (column.ColumnName)
                {
                    case "GRBS_Code_Name": order = 10; key = "Код по БК ГРБС:"; break;
                    case "Section_KBK_Code_Name": order = 20; key = "Код по БК ПЗ/РЗ:"; break;
                    case "Expense_Item_Code_Name": order = 30; key = "Код по БК ЦСР:"; break;
                    case "Work_Form_Code_Name": order = 40; key = "Код по БК ВР:"; break;

                    default:
                        continue;
                }

                if (date2.HasValue && date == date2.Value)
                {
                    key = "";
                    order = order + 1;
                }

                string value = row[column].ToString();

                expenses.Rows.Add(new object[] { column.ColumnName, key, string.IsNullOrEmpty(value) ? emptyCellCode : value, order.ToString("0000") });
            }

            return expenses;
        }

        protected DataTable getExpenses2(DateTime date)
        {
            DataTable table = extractActivity(date);
            DataTable expenses = createEmptyInvertedTable();
            expenses.Columns.Add("Value2");

            Dictionary<int, DataRow> rowCache = new Dictionary<int, DataRow>();
            int order = 0;
            int id = 0;
            DataRow newRow = null;

            DataRow row = table.Rows.Count > 0 ? table.Rows[0] : table.NewRow();

            foreach (DataColumn column in table.Columns)
            {
                string key = null;
                string key2 = null;

                switch (column.ColumnName)
                {
                    case "PlansActivityVolY0": order = 10; key = "Очередной финансовый год:"; break;
                    case "PlansActivityAddVolY0": order = 10; key2 = "Очередной финансовый год:"; break;

                    case "PlansActivityVolY1": order = 20; key = "Первый год планового периода:"; break;
                    case "PlansActivityAddVolY1": order = 20; key2 = "Первый год планового периода:"; break;

                    case "PlansActivityVolY2": order = 30; key = "Второй год планового периода:"; break;
                    case "PlansActivityAddVolY2": order = 30; key2 = "Второй год планового периода:"; break;

                    default:
                        continue;
                }

                id = order;


                if (rowCache.ContainsKey(id))
                {
                    newRow = rowCache[id];
                }
                else
                {
                    newRow = expenses.NewRow();
                    rowCache.Add(id, newRow);

                    newRow[0] = id.ToString("0000");
                    if (date2.HasValue && date == date2.Value)
                    {
                        newRow[1] = "";
                        newRow[3] = (order + 1).ToString("0000");
                    }
                    else
                    {
                        newRow[1] = (key ?? key2);
                        newRow[3] = order.ToString("0000");
                    }
                    expenses.Rows.Add(newRow);
                }

                if (key != null)
                {
                    newRow[2] = (row[column] != DBNull.Value ? row[column] : emptyCellCode);
                }
                else if (key2 != null)
                {
                    newRow[4] = (row[column] != DBNull.Value ? row[column] : emptyCellCode);
                }
            }

            return expenses;
        }

        protected DataTable getExpenses3(DateTime date)
        {
            DataTable table = extractActivity(date);
            DataTable expenses = createEmptyInvertedTable();

            string key;
            int order;

            DataRow row = table.Rows.Count > 0 ? table.Rows[0] : table.NewRow();

            foreach (DataColumn column in table.Columns)
            {
                switch (column.ColumnName)
                {
                    // TODO вместо -1 и -2 подставлять год
                    case "PlansActivityFactVolY0": order = 10; key = "Фактически израсходовано в текущем финансовом году:"; break;
                    case "PlansActivityFactVolY1": order = 20; key = "В отчетном финансовом году (текущий год -1):"; break;
                    case "PlansActivityFactVolY2": order = 30; key = "в финансовом году, предшествующему отчетному (текущий год -2):"; break;

                    default:
                        continue;
                }

                if (date2.HasValue && date == date2.Value)
                {
                    key = "";
                    order = order + 1;
                }
                string value = row[column].ToString();

                expenses.Rows.Add(new object[] { column.ColumnName, key, string.IsNullOrEmpty(value) ? emptyCellCode : value, order.ToString("0000") });
            }

            return expenses;
        }


        protected DataTable getPlansActivityGoal(DateTime date)
        {
            return HistoryRepository.GetPlansActivityGoals(this.CurrentActivityId, date);
        }

        protected DataTable getGetStateContract(DateTime date)
        {
            return HistoryRepository.GetStateContract(this.CurrentActivityId, date);
        }

        protected DataTable getGoalIndicators(DateTime date)
        {
            return HistoryRepository.GetGoalIndicators(this.CurrentActivityId, date);
        }



        protected DataTable getFuncs(DateTime date)
        {
            return HistoryRepository.GetFunc(this.CurrentActivityId, date);
        }

        protected DataTable getReasons(DateTime date)
        {
            return HistoryRepository.GetReasons(this.CurrentActivityId, date);
        }

        //protected DataTable getAPFInfo(DateTime date)
        //{
        //    return HistoryRepository.GetAPFInfo(this.CurrentActivityId, date);
        //}




        protected DataTable getExpenseDirections(DateTime date)
        {
            return HistoryRepository.GetExpenseDirections(this.CurrentActivityId, date);
        }


        protected DataTable getWorks(DateTime date)
        {
            string dsKey = "Works" + date.Ticks.ToString();
            if (!dataSources.ContainsKey(dsKey))
            {
                dataSources[dsKey] = HistoryRepository.GetWorks(this.CurrentActivityId, date);
            }
            return dataSources[dsKey];
        }

        protected DataTable getTypesWork(DateTime date)
        {
            string dsKey = "TypesWork" + date.Ticks.ToString();
            if (!dataSources.ContainsKey(dsKey))
            {
                // извлекаем ID работ
                DataTable works = getWorks(date);
                HashSet<string> workIds = new HashSet<string>();
                if (works != null)
                {
                    foreach (DataRow row in works.Rows)
                    {
                        workIds.Add(row["Id"].ToString());
                    }
                }

                dataSources[dsKey] = HistoryRepository.GetTypesWork(workIds, date);
            }

            return dataSources[dsKey];
        }

        protected DataTable getSpecifications(DateTime date)
        {
            string dsKey = "Specifications" + date.Ticks.ToString();
            if (!dataSources.ContainsKey(dsKey))
            {
                dataSources[dsKey] = HistoryRepository.GetSpecifications(this.CurrentActivityId, date);
            }
            return dataSources[dsKey];
        }

        protected DataTable getTypesSpec(DateTime date)
        {
            string dsKey = "TypesSpec" + date.Ticks.ToString();
            if (!dataSources.ContainsKey(dsKey))
            {
                // извлекаем ID спецификаций
                DataTable works = getSpecifications(date);
                HashSet<string> specIds = new HashSet<string>();
                if (works != null)
                {
                    foreach (DataRow row in works.Rows)
                    {
                        specIds.Add(row["Id"].ToString());
                    }
                }

                dataSources[dsKey] = HistoryRepository.GetTypesSpec(specIds, date);
            }

            return dataSources[dsKey];
        }


        public string Compare()
        {
            if (!date2.HasValue)
                return null;


            var plansActivity = RepositoryBase<PlansActivity>.Get(this.CurrentActivityId);
            if (plansActivity != null)
            {
                PlansActivityCompare cmp = RepositoryBase<PlansActivityCompare>.Get(x => (x.Activity == plansActivity &&
                                                                                          x.Date1 == this.date1 &&
                                                                                          x.Date2 == this.date2.Value));

                if (cmp != null)
                    return cmp.Changes;
            }


            string[] tables = new string[] {
            "gridResponsible",                                       
            "gridResponsible2",   
            "gridFunc",                                              
            "gridReason",                                            
            //"gridAPFInfo",                                                                                          
            "gridExpense1",                                          
            "gridExpense2",                                          
            "gridExpense3",                                          
            "gridExpenseDirection",                                  
            "gridPlansActivityGoal",                                 
            "gridStateContract",                                     
            "gridGoalIndicator",                                     
            "gridSpecification",                                     
            "gridTypesSpec",                                         
            "gridWork",                                              
            "gridTypesWork"};

            IList<string> changes = new List<string>();
            foreach (string t in tables)
            {
                DataTable table = getDataSource(t);
                foreach (DataRow row in table.Rows)
                {
                    if (!string.IsNullOrEmpty(row[HistoryData.changesColumnName].ToString()))
                    {
                        changes.Add(t);
                        break;
                    }
                }
            }

            if (plansActivity != null)
            {
                var cmp = new PlansActivityCompare(plansActivity, date1, date2.Value)
                    {
                        Changes = string.Join(",", changes)
                    };

                RepositoryBase<PlansActivityCompare>.SaveOrUpdate(cmp);
            }
            return string.Join(",", changes);
        }
    }
}