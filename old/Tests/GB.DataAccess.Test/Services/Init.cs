﻿using System.Security.Principal;
using System.Threading;

namespace GB.DataAccess.Test.Services
{
    static class Init
    {
        internal static void InitTest()
        {
            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("test_org"), null);

            new NHSession("Coord");
            Coord.AutoMapperConfiguration.Configure();
            AutoMapperConfiguration.Configure();
        }
    }
}
