﻿using bgTeam.IsRui.WebApp.ViewModels.Plans;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public class PlansController : Controller
    {
        //
        // GET: /Plans/

        public ActionResult Index()
        {
            //var handler = Singleton<TabsManager>.Instance.GetHandler<AccountingObjectsTabs>();

            return RedirectToAction("List");//handler.GetAction());
        }

        public ActionResult List()
        {
            return View();// new ListViewModel(null));
        }
    }
}
