﻿(function (window) {
    function showSuccess(message, timeout) {
        showMessage(message, { background: "#65b565" }, timeout || 2000);
    }

    function showWarning(message, timeout) {
        showMessage(message, { background: 'rgba(255, 32, 32, 0.6)' }, timeout || 5000);
    }

    function showResult(res) {
        res.success ? notify.showSuccess(res.message) : notify.showWarning(res.message);
    }

    function showMessage(message, css, timeout) {

        var _container = $("<div class='notify'></div>");
        _container.css(css).html(message);
        var _lastNotify = $(".notify").last();
        //_container.appendTo(document.body).show();
        _container.appendTo(document.body).show(), window.setTimeout(function () {
            _container.fadeOut(function () { _container.remove(); });
        }, timeout);
    }

    window.notify = { showSuccess: showSuccess, showWarning: showWarning, showResult: showResult };
})(window);

(function (window) {

    var notValue = '-2147483648';

    function FillListBox(list, selector) {
        $(selector).empty();
        $(selector).closest('.dropdown').find('a').remove();

        $.each(list, function (index, elem) {
            $(selector).append($('<option>', { value: elem.Value })
            .text(elem.Text));
        });

        $(selector).dropdown();

        //if (list[0]) {
        //    var value = '' + list[0].Text;
        //    $(selector).dropdown('set text', value);
        //}
    }

    function FillDropDown(list, selector) {
        var box = $(selector);

        box.empty();
        box.closest('.dropdown').find('div.text').empty();

        if (list.length > 0) {
            $.each(list, function (index, elem) {
                box.append($('<option>', { value: elem.SysId }).text(elem.Name));
            });

            setTimeout(function () { box.dropdown('set selected', list[0].SysId); }, 1);

        } else {
            box.append($('<option>', { value: notValue }).text('Не задано'));

            $.each(list, function (index, elem) {
                box.append($('<option>', { value: elem.SysId }).text(elem.Name));
            });

            setTimeout(function () { box.dropdown('set selected', notValue); }, 1);
        }
    }

    window.helper = { fillListBox: FillListBox, fillDropDown: FillDropDown, };
})(window);

function errorBaseHandler(err) {
    var text = $('.errorText', err.responseText).text();

    if (!text) {
        var regex = /<h2 class="stackerror">(.+)<\/h2>/gim;
        var match = regex.exec(err.responseText);
        if (match[1])
            text = match[1];
    }

    notify.showWarning('При операции возникла ошибка.<br><br>' + text);
}

// PRELOADER
$(function () {
    this.Preloader = function () {

        // Create global element references
        // this.closeButton = null;
        // this.modal = null;
        // this.overlay = null;

        // Define option defaults
        var defaults = {
            container: '.preloader',
            during: 500,
            easing: 'easeInOutCubic',
        }

        // Create options by extending defaults with the passed in arugments
        if (arguments[0] && typeof arguments[0] === "object") {
            this.options = extendDefaults(defaults, arguments[0]);
        }

        buildPreloader();
    }

    // Public Methods
    Preloader.prototype.show = function () {
        $(this.options.container).addClass('active');
        //$(this.options.container).animate({ opacity: 1 }, this.options.during, this.options.easing);
    }

    Preloader.prototype.hide = function () {
        var t = this;
        $(t.options.container).addClass('hidePop');
        setTimeout(function () {
            $(t.options.container).removeClass('hidePop active');
        }, 600);

        //var self = this;
        //$(this.options.container).animate({ opacity: 0 }, this.options.during, this.options.easing, function () {
        //    $(self.options.container).css('display', 'none');
        //});
    }

    // Private Methods
    function buildPreloader() {
        var _body = $("body");
        var htmlTemplate = '<div class="preloader"><div class="preloaderBlock">Подождите. Идёт выполнение операции ...</div></div>';
        _body.append(htmlTemplate);
        void 0;
    }

    // extend defaults with user options
    function extendDefaults(source, properties) {
        var property;
        for (property in properties) {
            if (properties.hasOwnProperty(property)) {
                source[property] = properties[property];
            }
        }
        return source;
    }
}());

window.preloader = new Preloader({});

var defaultTableConfig = {
    order: [],
    columnDefs: [
        { targets: '_all', orderable: false },
    ],
    lengthChange: !1,
    info: false,
    paging: true,
    pagingType: "simple_numbers",
    pageLength: 25,
    dom: '<"toolbar">rtip',
    //searching: true,
    language: {
        aria: {
            sortAscending: ": сортировка по возрастанию",
            sortDescending: ": сортировка по убыванию"
        },
        paginate: {
            first: "Первая",
            last: "Последняя",
            next: "Следующая",
            previous: "Предыдущая"
        },
        emptyTable: "Нет данных для отображения",
        info: "Показано с _START_ по _END_ запись из _TOTAL_",
        infoEmpty: "Показано 0 записей",
        infoFiltered: "(выбрано из _MAX_ записей)",
        infoPostFix: "",
        thousands: ",",
        lengthMenu: "Показывать по _MENU_ записей",
        loadingRecords: "Загрузка...",
        processing: "Обработка...",
        search: "Фильтр:",
        url: "",
        zeroRecords: "Не найдено данных"
    }
};