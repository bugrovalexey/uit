﻿using FluentNHibernate.Mapping;

namespace GB._TODO
{
    internal class testMap : ClassMap<test>
    {
        public testMap()
        {
            Table("test");

            Id(x => x.Id, "test_ID");//.GeneratedBy.Increment();// .UnsavedValue(EntityBase.UnsavedId);

            Map(x => x.Name, "test_name");

            //SqlInsert(@"exec ZZZ_Prc_test_Insert @test_name=?, @test_ID=?").Check.None();
            //SqlUpdate(@"exec ZZZ_Prc_test_Update @test_name=?, @test_ID=?").Check.None();
            //SqlDelete(@"exec ZZZ_Prc_test_Delete @test_ID = ?").Check.None();
        }
    }
}