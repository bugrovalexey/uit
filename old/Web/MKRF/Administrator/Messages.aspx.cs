﻿using System;
using System.Collections.Generic;
using GB.Helpers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using System.Linq;

namespace GB.MKRF.Web.Administrator
{
    public partial class Messages : System.Web.UI.Page
    {

        UsersMsg currentMessage = null;

        protected override void OnPreRender(EventArgs e)
        {
            grid.DataBind();
            base.OnPreRender(e);
        }

        protected void grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            IList<UsersMsg> messages = RepositoryBase<UsersMsg>.GetAll(new GetAllArgs<UsersMsg>()
            {
                Fetch = new System.Linq.Expressions.Expression<Func<UsersMsg, object>>[] { 
                    x=>x.User, 
                    x=>x.User.Department, 
                    x=>x.User.Role 
                },
                OrderByAsc = ( x => x.Date )
            });

            grid.DataSource = EntityHelper.ConvertToAnonymous<UsersMsg>(messages,
                                            delegate(UsersMsg m)
                                                {
                                                    var userData = string.Concat(m.User.FullName, " (", m.User.Role.Name, ", ", m.User.Login, ") ", m.User.Department.Name);

                                                    return new
                                                    {
                                                        Id = m.Id,
                                                        Date = m.Date,
                                                        Status = m.UsersMsgStatus.Name,
                                                        Url = m.Url,
                                                        Text = m.Text,
                                                        Response = ((m.AdminDate.HasValue ? m.AdminDate.Value.ToString() : null) + " " + m.AdminText).Trim(),
                                                        Department = m.User.Department != null ? m.User.Department.Name : null,
                                                        UserName = m.User.FullName,
                                                        Job = m.User.Job,
                                                        Phone = m.User.WorkPhone,
                                                        Email = m.User.Email,
                                                        Login = m.User.Login,
                                                        RoleName = m.User.Role != null ? m.User.Role.Name : null,
                                                        User = userData
                                                    };
                                            });
        }

        protected void StatusComboBox_DataBinding(object sender, EventArgs e)
        {
            if (currentMessage == null)
                return;

            IList<UsersMsgStatus> items = RepositoryBase<UsersMsgStatus>.GetAll();

            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = items;

            box.Value = currentMessage.UsersMsgStatus.Id;
        }

        protected void grid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            currentMessage = RepositoryBase<UsersMsg>.Get(e.EditingKeyValue);          
        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            currentMessage = RepositoryBase<UsersMsg>.Get((int)e.Keys[0]);
            currentMessage.UsersMsgStatus = RepositoryBase<UsersMsgStatus>.Get(Convert.ToInt32(hf["status"]));
            RepositoryBase<UsersMsg>.SaveOrUpdate(currentMessage);

            grid.CancelEdit();
            e.Cancel = true;
            grid.DataBind();
        }

        protected void grid_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs e)
        {
        }

        protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
        {
            if (e.Column.FieldName == "Status")
            {
                ComboBoxProperties combo = new ComboBoxProperties();
                IList<UsersMsgStatus> items = RepositoryBase<UsersMsgStatus>.GetAll();
                combo.Items.Add("");
                foreach (UsersMsgStatus s in items)
                    combo.Items.Add(s.Name);
                e.EditorProperties = combo;
            }
        }        
    }
}