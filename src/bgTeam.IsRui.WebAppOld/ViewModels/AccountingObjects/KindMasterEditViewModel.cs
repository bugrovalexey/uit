﻿using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using System.ComponentModel;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class KindMasterEditViewModel : StepBaseViewModel
    {
        private const string KindCaption = "Классификационная категория ОУ";

        [DisplayName(KindCaption)]
        public int? KindId { get; set; }

        public SelectList KindList { get; set; }

        public KindMasterEditViewModel(int id)
            : base(id, MasterItemEnum.KindMasterEdit)
        {
            KindId = Current.Kind.Id;
            KindList = new SelectList(new IKTComponentRepository().GetAll(), "Id", "Name");
        }
    }
}