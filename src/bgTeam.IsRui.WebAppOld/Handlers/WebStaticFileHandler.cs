﻿using bgTeam.IsRui.FileSystem;
using System;
using System.Linq;
using System.Web;

namespace bgTeam.IsRui.Handlers
{
    /// <summary>
    /// Обработчик загруженных изображений и видео
    /// </summary>
    public class WebStaticFileHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string url = context.Request.RawUrl;
            var splitUrl = url.Split('/');
            int id = Convert.ToInt32(splitUrl.Last());

            string type = splitUrl[splitUrl.Length - 2];
            switch (type)
            {
                case "image":
                    context.Response.ContentType = "image/jpeg";
                    break;

                case "video":
                    context.Response.ContentType = "video/mpeg";
                    break;
            }

            var path = FileProvider.GetPhysicalPath(id);
            context.Response.WriteFile(path);
        }

        public bool IsReusable
        {
            get { return false; }
        }

        /*
        public static string GetImageUrl(object id, string applicationPath = null)
        {
            if (id == null)
            {
                return null;
                //throw new ArgumentException("Необходимо указать Id файла");
            }

            //            if (!FileHelper.IsExist(id))
            //            {
            //                return applicationPath != null ? applicationPath + "/errorfile" : UrlHelper.Resolve("~/errorfile");
            //            }

            StringBuilder builder = new StringBuilder();
            builder.Append(applicationPath != null ? applicationPath + "/image/" : UrlHelper.Resolve("~/image/"));
            //            builder.Append(UrlHelper.Resolve("~/image/"));
            builder.Append(id);

            return builder.ToString();
        }

        public static string GetVideoUrl(object id, string applicationPath = null)
        {
            if (id == null)
            {
                return null;
                //throw new ArgumentException("Необходимо указать Id файла");
            }

            //            if (!FileHelper.IsExist(id))
            //            {
            //                return applicationPath != null ? applicationPath + "/errorfile" : UrlHelper.Resolve("~/errorfile");
            //            }

            StringBuilder builder = new StringBuilder();
            builder.Append(applicationPath != null ? applicationPath + "/video/" : UrlHelper.Resolve("~/video/"));
            builder.Append(id);

            return builder.ToString();
        }
        */
    }
}