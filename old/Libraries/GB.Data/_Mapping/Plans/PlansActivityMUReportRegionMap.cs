﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class PlansActivityMUReportRegionMap : ClassMapBase<PlansActivityMUReportRegion>
	{
        public PlansActivityMUReportRegionMap()
            : base("Activity_MU_Report_Region"){}

        protected override void InitMap()
        {
            References<PlansActivityMU>(x => x.PlansActivityMU, "Plans_Activity_MU_ID").Cascade.None();
            References<Department>(x => x.Region, "Govt_Organ_ID").Not.Nullable().Cascade.None();
        }
    }
}