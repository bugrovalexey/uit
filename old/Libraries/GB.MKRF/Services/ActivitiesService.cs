﻿using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.Common;
using GB.Data.Directories;
using GB.Data.Documents;
using GB.Data.Extensions;
using GB.Data.Plans;
using GB.MKRF.Extensions;
using GB.MKRF.Repository;
using System;
using System.Data;
using System.Linq;

namespace GB.MKRF.Services
{
    #region args

    public interface IPlansActivityArgs
    {
        int Id { get; set; }

        string Name { get; set; }

        string Description { get; set; }

        string ResponsibleDepartment { get; set; }

        int? ResponsibleInformation { get; set; }

        int? ResponsibleTechnical { get; set; }

        int? ResponsibleFinancial { get; set; }

        int? AccountingObject { get; set; }

        int? Signing { get; set; }

        string Logo { get; set; }

        string Files { get; set; }

        int? Order { get; set; }

        DateTime? OrderDate { get; set; }

        string Prerequisites { get; set; }

        string Goal { get; set; }

        string Task { get; set; }

        string Conception { get; set; }
    }

    public interface IPlansActivityExpensesArgs
    {
        int Id { get; set; }

        int? GRBS { get; set; }

        int? SectionKBK { get; set; }

        int? WorkForm { get; set; }

        int? CSR { get; set; }

        int? KOSGU { get; set; }

        object VolY0 { get; set; }

        object VolY1 { get; set; }

        object VolY2 { get; set; }
    }

    public interface IPlansActivityReasonsArgs
    {
        int Id { get; set; }

        int StateProgram { get; set; }

        int StateSubProgram { get; set; }

        int StateMainEvent { get; set; }
    }

    public interface IPlansActivityCreateArgs
    {
        int Id { get; set; }

        string NameSmall { get; set; }

        int Type { get; set; }
    }

    //public class PlansActivityExpertiseArgs : IPlansActivityExpensesArgs
    //{
    //    public Plan
    //    public object ActivityExpertiseConclusion { get; set; }
    //}

    public class PlansActivityArgs : IPlansActivityExpensesArgs
    {
        public int Id { get; set; }

        public object СurrentPlan { get; set; }

        public object Name { get; set; }

        public object IsPriority { get; set; }

        //common data
        public object AnnotationData { get; set; }

        public object ActivityType2 { get; set; }

        public object IsMoveUp { get; set; }

        public object IKTSelectionId { get; set; }

        public object ResponsibleId { get; set; }

        public object FundingId { get; set; }

        public object IKTObjectId { get; set; }

        // expenses
        public int? GRBS { get; set; }

        public int? SectionKBK { get; set; }

        public int? WorkForm { get; set; }

        public int? CSR { get; set; }

        public int? KOSGU { get; set; }

        public object Subentry { get; set; }

        public object VolY0 { get; set; }

        public object VolY1 { get; set; }

        public object VolY2 { get; set; }

        public object PlansActivityAddVolY0 { get; set; }

        public object PlansActivityAddVolY1 { get; set; }

        public object PlansActivityAddVolY2 { get; set; }

        public object PlansActivityFactVolY0 { get; set; }

        public object PlansActivityFactVolY1 { get; set; }

        public object PlansActivityFactVolY2 { get; set; }

        public object PlansActivityFactDate { get; set; }

        public object NewDepartamentId { get; set; }

        public object DatePlacement { get; set; }

        public object ResponsibleInformation { get; set; }

        public object ResponsibleTechnical { get; set; }

        public object ResponsibleFinancial { get; set; }

        public object Files { get; set; }

        public object AccountingObject { get; set; }
    }

    public class UnionArgs
    {
        public int source { get; set; }

        public int target { get; set; }

        public string name { get; set; }

        public bool priority { get; set; }
    }

    #endregion args

    public class ActivitiesService : ServiceBase<PlansActivity>
    {
        //public override void SetCurrentEdit(object entityId)
        //{
        //    CurrentEdit = null;

        //    if (IsEmpty(entityId))
        //    {
        //        throw new ArgumentException("Не указан ID");
        //    }

        //    //base.CurrentEdit = Repository.Get(entityId);
        //    CurrentEdit = GetExistingWithAccess(entityId);
        //    //TODO по идее, сюда можно добавить проверку прав на редактирование

        //    if (CurrentEdit == null && !PlansActivityRepository.IsHidden(entityId))
        //    {
        //        throw new Exception(String.Format("Объект <{0}> не найден в бд", entityId));
        //    }
        //}

        //private DataTable activityListCache = null;

        public PlansActivity Create(IPlansActivityCreateArgs args)
        {
            var activity = new PlansActivity();

            activity.NameSmall = args.NameSmall;
            activity.ActivityType2 = GetEntity<ActivityType>(args.Type);
            activity.Department = UserRepository.GetCurrent().Department;
            activity.Status = new StatusRepository().GetDefault(activity);

            //activity.GRBS = GetEntity<GRBS>(EntityBase.Default); //не задано
            //activity.SectionKBK = GetEntity<SectionKBK>(EntityBase.Default); //не задано
            //activity.ExpenseItem = GetEntity<ExpenseItem>(EntityBase.Default); //не задано
            //activity.WorkForm = GetEntity<WorkForm>(EntityBase.Default); //не задано
            activity.IKTComponent = GetEntity<IKTComponent>(EntityBase.Default); //не задано

            activity.PlansActivityVolY0 = 0;
            activity.PlansActivityVolY1 = 0;
            activity.PlansActivityVolY2 = 0;

            activity.PlansActivityAddVolY0 = 0;
            activity.PlansActivityAddVolY1 = 0;
            activity.PlansActivityAddVolY2 = 0;

            activity.ExpertiseConclusion = new GB.MKRF.Services.ActivityExpertiseConclusionService().Create(null);

            base.SaveOrUpdate(activity);

            return activity;
        }

        //public PlansActivity Create(PlansActivityArgs args)
        //{
        //    activityListCache = null;
        //    Logger.Debug("Создание мероприятия " + args.Id);
        //    var activity = new PlansActivity();

        //    //if (!Validator.Validator.Validate(activity, ObjActionEnum.create))
        //    //    throw new AccessViolationException("Данный пользователь не может создавать мероприятия");

        //    activity.Department = UserRepository.GetCurrent().Department;
        //    activity.FgisStatus = StatusRepositoryOld.GetFirstStatus(activity);

        //    activity.Num =
        //        NumHelper<PlansActivity>.GetMinAvailableNum(
        //            PlanRepository.GetActivityListWithoutSecurity(args.СurrentPlan as Plan));

        //    activity.Name = Convert.ToString(args.Name);

        //    activity.Plan = args.СurrentPlan as Plan;

        //    activity.GRBS = GetEntity<GRBS>(EntityBase.Default); //не задано
        //    activity.SectionKBK = GetEntity<SectionKBK>(EntityBase.Default); //не задано
        //    activity.ExpenseItem = GetEntity<ExpenseItem>(EntityBase.Default); //не задано
        //    activity.WorkForm = GetEntity<WorkForm>(EntityBase.Default); //не задано
        //    activity.IKTComponent = GetEntity<IKTComponent>(EntityBase.Default); //не задано

        //    activity.ActivityType2 = GetEntity<ActivityType2>(args.ActivityType2);

        //    activity.PlansActivityVolY0 = 0;
        //    activity.PlansActivityVolY1 = 0;
        //    activity.PlansActivityVolY2 = 0;

        //    activity.PlansActivityAddVolY0 = 0;
        //    activity.PlansActivityAddVolY1 = 0;
        //    activity.PlansActivityAddVolY2 = 0;

        //    if (activity.ActivityType2.IsEq(ActivityType2Enum.Create))
        //    {
        //        var ac = new AccountingObjectService();

        //        activity.AccountingObject = ac.Create(new AccountingObjectArgs() { ShortName = "-" });
        //    }

        //    activity.Status = new StatusRepository().GetDefault(activity);

        //    //activity.DatePlacement = (DateTime?)(args.DatePlacement);

        //    //activity.IsPriority = Convert.ToBoolean(args.IsPriority);
        //    //CodeHelper.FillActivityCode(activity, Convert.ToBoolean(args.IsPriority));

        //    base.SaveOrUpdate(activity);
        //    //Repository.Refresh(activity);

        //    //OrderNumCode(PlanRepository.GetActivityListWithoutSecurity(activity.Plan));

        //    return activity;
        //}

        //public PlansActivity Update(PlansActivityArgs args)
        //{
        //    SetCurrentEdit(args.Id);
        //    activityListCache = null;
        //    Logger.Debug("Обновление мероприятия " + args.Id);

        //    var name = Convert.ToString(args.Name);
        //    if (string.IsNullOrEmpty(name)) // commondata
        //    {
        //        CurrentEdit.AnnotationData = Convert.ToString(args.AnnotationData);
        //        CurrentEdit.DatePlacement = (DateTime?)(args.DatePlacement);

        //        CurrentEdit.IKTComponent = GetEntity<IKTComponent>(args.IKTSelectionId) ?? GetEntity<IKTComponent>(EntityBase.Default);
        //        CurrentEdit.ActivityType2 = GetEntity<ActivityType2>(args.ActivityType2) ?? GetEntity<ActivityType2>(EntityBase.Default);
        //        CurrentEdit.Responsible = GetEntity<Responsible>(args.ResponsibleId);
        //        CurrentEdit.SourceFunding = GetEntity<SourceFunding>(args.FundingId);
        //        CurrentEdit.IKTObject = GetEntity<IKTObject>(args.IKTObjectId);

        //    }
        //    else //activityList
        //    {
        //        CurrentEdit.Name = Convert.ToString(args.Name);
        //        // если обновляем приоритет и меняем приоритет, то пересчитываем кол-во элементов с новым приоритетом
        //        var isPriority = Convert.ToBoolean(args.IsPriority);
        //        if (CurrentEdit.IsPriority != isPriority)
        //        {
        //            CodeHelper.FillActivityCode(CurrentEdit, isPriority);

        //            if (isPriority)
        //            {
        //                var priorCount = 0;
        //                var priorActivities = CurrentEdit.Plan.Activities.Where(i => i.IsPriority);
        //                if (priorActivities.Any())
        //                    priorCount = priorActivities.Count();

        //                CurrentEdit.Num = priorCount + 1;
        //            }
        //            else
        //            {
        //                CurrentEdit.Num = CurrentEdit.Plan.Activities.Count + 1;
        //            }
        //        }

        //        CurrentEdit.IsPriority = isPriority;
        //    }
        //    base.SaveOrUpdate(CurrentEdit);
        //    Repository.Refresh(CurrentEdit);

        //    if (!string.IsNullOrEmpty(name))
        //        OrderNumCode(PlanRepository.GetActivityListWithoutSecurity(CurrentEdit.Plan));
        //    return CurrentEdit;
        //}

        public PlansActivity Update2(PlansActivityArgs args)
        {
            var activity = Repository.Get(args.Id);

            activity.Name = Convert.ToString(args.Name);

            base.SaveOrUpdate(activity);

            return activity;
        }

        public PlansActivity Update3(IPlansActivityArgs args)
        {
            var activity = Repository.GetExistingWithAccess(args.Id);
            if (!activity.CanEdit())
                throw new InvalidOperationException("Нет прав на изменение объекта.");

            activity.Name = args.Name;
            activity.Description = args.Description;
            activity.ResponsibleDepartment = args.ResponsibleDepartment;

            activity.ResponsibleInformation = GetEntity<User>(args.ResponsibleInformation);
            activity.ResponsibleTechnical = GetEntity<User>(args.ResponsibleTechnical);
            activity.ResponsibleFinancial = GetEntity<User>(args.ResponsibleFinancial);

            activity.Signing = GetEntity<User>(args.Signing);

            activity.Order = args.Order;
            activity.OrderDate = args.OrderDate;
            activity.Prerequisites = args.Prerequisites;
            activity.Goal = args.Goal;
            activity.Task = args.Task;

            //При смене ОУ удаляем все значения характиристик
            var ao = GetEntity<AccountingObject>(args.AccountingObject);
            if (activity.AccountingObject != null)
            {
                if (ao != null && !activity.AccountingObject.Is(ao))
                {
                    new AccountingObjectCharacteristicsValueService().DeleteAll(activity);
                    activity.AccountingObject = ao;
                }
            }
            else
                activity.AccountingObject = ao;

            base.SaveOrUpdate(activity);

            new DocumentService()
                .Save(args.Logo as string, activity.Logo, activity, DocTypeEnum.Logo);

            new DocumentService()
                .Save(args.Files as string, activity.Documents, activity, DocTypeEnum.ActivityFEO);

            new DocumentService()
                .Save(args.Conception as string, activity.Conception, activity, DocTypeEnum.Conception);

            //activity.Conception = args.Conception;

            return activity;
        }

        [Obsolete]
        public PlansActivity Update3(PlansActivityArgs args)
        {
            var activity = Repository.Get(args.Id);

            activity.ResponsibleInformation = GetEntity<User>(args.ResponsibleInformation);
            activity.ResponsibleTechnical = GetEntity<User>(args.ResponsibleTechnical);
            activity.ResponsibleFinancial = GetEntity<User>(args.ResponsibleFinancial);

            //При смене ОУ удаляем все значения характиристик
            var ao = GetEntity<AccountingObject>(args.AccountingObject);
            if (activity.AccountingObject != null)
            {
                if (ao != null && !activity.AccountingObject.Is(ao))
                {
                    new AccountingObjectCharacteristicsValueService().DeleteAll(activity);
                    activity.AccountingObject = ao;
                }
            }
            else
                activity.AccountingObject = ao;

            base.SaveOrUpdate(activity);

            new DocumentService()
                .Save(args.Files.ToString(), activity.Documents, activity, DocTypeEnum.ActivityFEO);

            return activity;
        }

        public PlansActivity Update4(IPlansActivityExpensesArgs args)
        {
            var activity = Repository.GetExistingWithAccess(args.Id);
            if (!activity.CanEdit())
                throw new InvalidOperationException("Нет прав на изменение объекта.");

            activity.GRBS = GetEntity<GRBS>(args.GRBS);
            activity.SectionKBK = GetEntity<SectionKBK>(args.SectionKBK);
            activity.WorkForm = GetEntity<WorkForm>(args.WorkForm);
            activity.ExpenseItem = GetEntity<ExpenseItem>(args.CSR);
            activity.ExpenditureItem = GetEntity<ExpenditureItem>(args.KOSGU);

            activity.PlansActivityVolY0 = args.VolY0.ToDoubleNull();
            activity.PlansActivityVolY1 = args.VolY1.ToDoubleNull();
            activity.PlansActivityVolY2 = args.VolY2.ToDoubleNull();

            base.SaveOrUpdate(activity);

            return activity;
        }

        public PlansActivity Update5(IPlansActivityReasonsArgs args)
        {
            var activity = Repository.GetExistingWithAccess(args.Id);
            if (!activity.CanEdit())
                throw new InvalidOperationException("Нет прав на изменение объекта.");

            activity.StateProgram = GetEntity<StateProgram>(args.StateProgram);
            activity.StateSubProgram = GetEntity<StateProgram>(args.StateSubProgram);
            activity.StateMainEvent = GetEntity<StateProgram>(args.StateMainEvent);

            base.SaveOrUpdate(activity);
            return activity;
        }

        //public PlansActivity Update(PlansActivityExpertiseArgs args)
        //{
        //}

        public void Delete(PlansActivityArgs args)
        {
            //activityListCache = null;
            Logger.Debug("Удаление мероприятия " + args.Id);
            //SetCurrentEdit(args.Id);
            var CurrentEdit = Repository.GetExistingWithAccess(args.Id);
            if (!CurrentEdit.CanDelete())
                throw new InvalidOperationException("Нет прав на удаление объекта.");
            var plan = CurrentEdit.Plan;
            base.Delete((int)args.Id);
            //NumHelper<PlansActivity>.OrderNums(PlanRepository.GetActivityListWithoutSecurity(plan));
        }

        //public int ChangeDepartament(PlansActivityArgs args)
        //{
        //    Logger.Debug("Изменение департамента мероприятия " + args.Id);
        //    SetCurrentEdit(args.Id);

        //    int newDepartamentId = 0;
        //    if (args.NewDepartamentId == null || !Int32.TryParse(args.NewDepartamentId.ToString(), out newDepartamentId))
        //        throw new ArgumentException("Не передан департамент для перевода");

        //    //int newDepartamentUserId = 0;
        //    //if (args.NewDepartamentUserId == null || !Int32.TryParse(args.NewDepartamentUserId.ToString(), out newDepartamentUserId))
        //    //    throw new ArgumentException("Не передан отвественный для перевода в новый департамент");

        //    var departament = RepositoryBase<Department>.Get(newDepartamentId);

        //    // для текущего департамента возвращаем текущий план
        //    if (departament == CurrentEdit.Department)
        //        return CurrentEdit.Plan.Id;

        //    CurrentEdit.Department = departament;

        //    var year = CurrentEdit.Plan.Year;

        //    var newPlan = PlanRepository.Get(x => x.Year == year && x.Department == departament && x.PlanType == ExpObjectTypeEnum.Plan2012new);

        //    CurrentEdit.Plan = newPlan;

        //    CodeHelper.FillActivityCode(CurrentEdit, CurrentEdit.IsPriority);
        //    base.SaveOrUpdate(CurrentEdit);
        //    Repository.Refresh(CurrentEdit);

        //    //OrderNumCode(PlanRepository.GetActivityListWithoutSecurity(CurrentEdit.Plan));

        //    return CurrentEdit.Plan.Id;
        //}

        //public void Move(PlansActivityArgs args)
        //{
        //    Logger.Debug("Изменение номера мероприятия " + args.Id);
        //    SetCurrentEdit(args.Id);

        //    var plan = CurrentEdit.Plan;
        //    IList<PlansActivity> Activities = PlanRepository.GetActivityListWithoutSecurity(plan);

        //    NumHelper<PlansActivity>.RefreshNums(Activities.OrderBy(x => x.IsPriority).ThenBy(x => x.Num).ToList());
        //    NumHelper<PlansActivity>.Move(Activities, CurrentEdit, (bool)args.IsMoveUp);
        //}

        //public void Copy(PlansActivityArgs args)
        //{
        //    Logger.Debug("Копирование мероприятия " + args.Id);
        //    SetCurrentEdit(args.Id);
        //    var sourceActivity = Repository.Get(CurrentEdit.Id);
        //    var copyID = CopyHelper.CopyActivity(sourceActivity);
        //    var pa = Repository.Get(copyID);
        //    pa.BasedOn = sourceActivity.Id;
        //    base.SaveOrUpdate(pa);
        //    RepositoryBase<Plan>.Refresh(CurrentEdit.Plan);
        //    NumHelper<PlansActivity>.OrderNums(PlanRepository.GetActivityListWithoutSecurity(CurrentEdit.Plan));
        //}

        //private static void OrderNumCode(IList<PlansActivity> ownerList)
        //{
        //    SortedDictionary<string, PlansActivity> dict = new SortedDictionary<string, PlansActivity>();
        //    foreach (PlansActivity item in ownerList)
        //    {
        //        dict.Add((item.IsPriority ? "0" : "1") + item.Num.ToString("0000000000") + ";" + item.Code + ";" + item.Id.ToString("0000000000"), item);
        //    }

        //    int num = 1;
        //    foreach (string key in dict.Keys)
        //    {
        //        PlansActivity item = dict[key];
        //        if (item.Num != num)
        //        {
        //            item.Num = num;
        //            RepositoryBase<PlansActivity>.SaveOrUpdate(item);
        //        }
        //        num++;
        //    }
        //}

        //public void ClearCache()
        //{
        //    activityListCache = null;
        //}

        //public DataTable GetActivityList(Plan currentPlan, bool isUnallocated, bool? IsPriority = null, bool? IsFavourites = null)
        //{
        //    if (activityListCache == null)
        //    {
        //        var userId = UserRepository.GetCurrent().Id;
        //        activityListCache = !isUnallocated
        //                                ? PlanRepository.GetActivityList2012new(currentPlan, userId)
        //                                : new PlanControllerOld().GetUnallocatedActivityList(currentPlan.Year.Value, userId);
        //    }

        //    List<string> filters = new List<string>();

        //    if (IsPriority == true)
        //    {
        //        filters.Add("IsNotPriority = 0");
        //    }
        //    else if (IsPriority == false)
        //    {
        //        filters.Add("IsNotPriority = 1");
        //    }

        //    if (IsFavourites == true)
        //    {
        //        filters.Add("isFavor = 1");
        //    }

        //    if (filters.Count > 0)
        //    {
        //        return DataTableCustomFilter(activityListCache, string.Join(" AND ", filters));
        //    }
        //    else
        //    {
        //        return activityListCache;
        //    }
        //}

        //public DataTable ExtractJoinedActivities(DataTable ActivityList)
        //{
        //    if (ActivityList == null)
        //        return null;

        //    var result = ActivityList.Clone();

        //    var idList = ActivityList.Rows.Cast<DataRow>().Select(x => (int)x["Id"]).ToList();
        //    var joinedIdList = PlansActivityRepository.GetJoinedActivities(idList);

        //    for (int i = ActivityList.Rows.Count - 1; i >= 0; i--)
        //    {
        //        var row = ActivityList.Rows[i];
        //        if (joinedIdList.Contains((int)row["Id"]))
        //        {
        //            result.Rows.Add(row.ItemArray);
        //            //ActivityList.Rows.RemoveAt(i);
        //        }
        //    }

        //    //ActivityList.AcceptChanges();
        //    return result;
        //}

        private DataTable DataTableCustomFilter(DataTable dt, string filterExpression)
        {
            var rows = dt.Select(filterExpression);
            if (rows.Any())
            {
                return rows.CopyToDataTable();
            }
            else
            {
                dt.Rows.Clear();
                return dt;
            }
        }

        //public DataTable GetTableSummary(Plan plan)
        //{
        //    return PlanRepository.GetSummaryActivityList(plan, UserRepository.GetCurrent().Id);
        //}

        public void SaveExpenses(PlansActivityArgs args)
        {
            //SetCurrentEdit(args.Id);
            var CurrentEdit = Repository.GetExistingWithAccess(args.Id);

            Logger.Debug("Обновление мероприятия " + args.Id);

            CurrentEdit.GRBS = GetEntity<GRBS>(args.GRBS);
            CurrentEdit.SectionKBK = GetEntity<SectionKBK>(args.SectionKBK);
            CurrentEdit.WorkForm = GetEntity<WorkForm>(args.WorkForm);
            CurrentEdit.ExpenseItem = GetEntity<ExpenseItem>(args.CSR);
            //            CurrentEdit.ExpenditureItem = GetEntity<ExpenditureItem>(args.ExpenditureItemsId);

            if (args.Subentry != null && !string.IsNullOrEmpty(args.Subentry.ToString()))
                CurrentEdit.Subentry = args.Subentry.ToString();

            double parseValue = 0;
            if (args.VolY0 != null && double.TryParse(args.VolY0.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityVolY0 = parseValue;
            }

            if (args.VolY1 != null && double.TryParse(args.VolY1.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityVolY1 = parseValue;
            }

            if (args.VolY2 != null && double.TryParse(args.VolY2.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityVolY2 = parseValue;
            }

            if (args.PlansActivityAddVolY0 != null && double.TryParse(args.PlansActivityAddVolY0.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityAddVolY0 = parseValue;
            }

            if (args.PlansActivityAddVolY1 != null && double.TryParse(args.PlansActivityAddVolY1.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityAddVolY1 = parseValue;
            }

            if (args.PlansActivityAddVolY2 != null && double.TryParse(args.PlansActivityAddVolY2.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityAddVolY2 = parseValue;
            }

            if (args.PlansActivityFactVolY0 != null && double.TryParse(args.PlansActivityFactVolY0.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityFactVolY0 = parseValue;
            }

            if (args.PlansActivityFactVolY1 != null && double.TryParse(args.PlansActivityFactVolY1.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityFactVolY1 = parseValue;
            }

            if (args.PlansActivityFactVolY2 != null && double.TryParse(args.PlansActivityFactVolY2.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityFactVolY2 = parseValue;
            }

            DateTime fact;
            if (args.PlansActivityFactDate != null
                && DateTime.TryParse(args.PlansActivityFactDate.ToString(), out fact)
                && fact > new DateTime(1, 1, 1))
                CurrentEdit.PlansActivityFactDate = fact;
            else
                CurrentEdit.PlansActivityFactDate = null;

            base.SaveOrUpdate(CurrentEdit);
        }

        //private struct moneyAndIndex
        //{
        //    public decimal money;
        //    public int index;
        //}

        /// <summary>
        /// Склеивает мероприятия
        /// </summary>
        /// <param name="ActivityIdList"></param>
        /// <returns>Новое мероприятие</returns>
        //public PlansActivity Union(UnionArgs args)
        //{
        //    //IList<PlansActivity> activityList = Repository.GetAll(new GetAllArgs<PlansActivity>() {
        //    //    Ids = ActivityIdList
        //    //});

        //    var activity = Repository.Get(args.source);
        //    if (activity == null)
        //    {
        //        return null;
        //    }

        //    PlansActivity newActivity = null;

        //    if (args.target == 0)
        //    {
        //        newActivity = new PlansActivity(activity.Plan);

        //        newActivity.Name = args.name;
        //        newActivity.IsPriority = args.priority;

        //        CopyHelper.CalculateActivityNumberAndCode(newActivity);

        //        newActivity.Department = activity.Department;
        //        //newActivity.Responsible = m.Responsible;
        //        newActivity.FgisStatus = activity.FgisStatus;
        //        // На вкладке расходы поля "Код по БК ГРБС", "Код по БК ПЗ/РЗ", "Код по БК ЦСР", "Код по БК ВР" заполняются по умолчанию. Поле Код по БК КОСГУ остается не заполненным.
        //        newActivity.SourceFunding = RepositoryBase<SourceFunding>.GetDefault();
        //        newActivity.IKTObject = RepositoryBase<IKTObject>.GetDefault();
        //        newActivity.IKTComponent = RepositoryBase<IKTComponent>.GetDefault();

        //        // TODO: Сделать так, чтобы мероприятие создавалось только в ActivityController

        //        newActivity.GRBS = RepositoryBase<GRBS>.Get(24635); //054 Министерство культуры Российской Федерации
        //        newActivity.SectionKBK = RepositoryBase<SectionKBK>.Get(21772); //0801 Культура
        //        newActivity.ExpenseItem = RepositoryBase<ExpenseItem>.Get(1066348); //11 5 9999 Реализация федеральной целевой программы "Культура России (2012 - 2018 годы)" государственной программы Российской Федерации "Развитие культуры и туризма"
        //        newActivity.WorkForm = RepositoryBase<WorkForm>.Get(44451); //242 Закупка товаров, работ, услуг в сфере информационно-коммуникационных технологий

        //        base.SaveOrUpdate(newActivity);
        //        args.target = newActivity.Id;
        //    }
        //    else
        //    {
        //        newActivity = Repository.Get(args.target);
        //        if (newActivity == null)
        //        {
        //            return null;
        //        }
        //    }

        //    // Таблица "Наименование и реквизиты основания реализации мероприятия и (или) решения о создании системы с указанием пунктов, статей" на вкладке "общие сведения" содержит все .
        //    CopyHelper.CopyReasons(activity, newActivity);

        //    // Таблица "Цели мероприятия" на вкладке "общие сведения" содержит все строки из склеиваемых мероприятий.
        //    var goals = CopyHelper.CopyGoals(activity, newActivity);

        //    // Таблица "Показатели" и "Индикаторы" содержат все строки из склеиваемых мероприятий. При этом поле Госуслуга/госфункция осается не заполненным.
        //    CopyHelper.CopyPlansGoalIndicators(activity, newActivity, goals, true);

        //    // Значения заполненные в разделе "Объем планируемых бюджетных ассигнований, тыс. руб" и основного объема и дополнительного - суммируются.
        //    newActivity.PlansActivityVolY0 += activity.PlansActivityVolY0;
        //    newActivity.PlansActivityVolY1 += activity.PlansActivityVolY1;
        //    newActivity.PlansActivityVolY2 += activity.PlansActivityVolY2;
        //    newActivity.PlansActivityAddVolY0 += activity.PlansActivityAddVolY0;
        //    newActivity.PlansActivityAddVolY1 += activity.PlansActivityAddVolY1;
        //    newActivity.PlansActivityAddVolY2 += activity.PlansActivityAddVolY2;

        //    // В разделе "Объем фактически израсходованных бюджетных ассигнований, тыс. руб" поле "по состоянию на дату" остается не заполненным. Значение фактических расходов суммируются.
        //    newActivity.PlansActivityFactVolY0 += activity.PlansActivityFactVolY0;
        //    newActivity.PlansActivityFactVolY1 += activity.PlansActivityFactVolY1;
        //    newActivity.PlansActivityFactVolY2 += activity.PlansActivityFactVolY2;

        //    // В таблице "Направления расходования средств на ИКТ" должна содержать строки со всеми видами затрат из присутствующими в склеиваемых мероприятиях. Значения расходов на каждый вид затрат должны суммироваться.
        //    CopyHelper.CopyExpenseDirections(activity, newActivity, true);

        //    // Таблица на вкладке "Результаты" должна содержать госконтракты из всех мероприятий.
        //    CopyHelper.CopyContracts(activity, newActivity);

        //    // Таблицы на вкладке "Работы" и "Товары" должны содержать значения из всех склеиваемых мероприятий.
        //    foreach (PlansWork w in activity.Works)
        //    {
        //        CopyHelper.CopyWork(newActivity, w);
        //    }

        //    foreach (PlansSpec s in activity.Specifications)
        //    {
        //        CopyHelper.CopySpec(newActivity, s);
        //    }

        //    // Таблицы на вкладке "Закупки" должны содержать упорядоченные значения из всех склеиваемых мероприятий.
        //    CopyHelper.CopyZakupki(activity, newActivity);

        //    activity.JoinedTo = newActivity;
        //    base.SaveOrUpdate(activity);

        //    base.Delete(activity);

        //    base.SaveOrUpdate(newActivity);

        //    Repository.Refresh(newActivity);

        //    return newActivity;
        //}

        //public ActivityViewModel GetActivityViewModel(object ActivityObject)
        //{
        //    ActivityViewModel model = new ActivityViewModel();

        //    if (ActivityObject is PlansActivity)
        //    {
        //        var activity = ActivityObject as PlansActivity;
        //        model.Id = activity.Id;
        //        model.JoinedTo = (activity.JoinedTo != null) ? (int?)activity.JoinedTo.Id : null;

        //        /////////////////////////////////////  общие сведения + расходы
        //        model.Plan_Year = activity.Plan.Year.Value;
        //        model.Plan_Type = (int)activity.Plan.PlanType;

        //        model.Code = activity.Code;
        //        model.Name = activity.Name;
        //        model.ActivityType2_Name = activity.ActivityType2 != null ? activity.ActivityType2.Name : "";
        //        model.IKTObject = activity.IKTObject != null ? activity.IKTObject.Name : "";
        //        model.IKTComponentCodeName = activity.IKTComponent == null
        //                                      ? string.Empty
        //                                      : string.Format("{0}{1}", string.IsNullOrEmpty(activity.IKTComponent.Code) ? string.Empty : activity.IKTComponent.Code + " ", activity.IKTComponent.Name);
        //        model.AnnotationData = activity.AnnotationData;
        //        model.ResponsibleInfo = activity.Responsible == null
        //                                    ? string.Empty
        //                                    : string.Join(", ", new[] { activity.Responsible.Name,
        //                                                                activity.Responsible.Job != null ? activity.Responsible.Job.Name : string.Empty,
        //                                                                activity.Responsible.Phone,
        //                                                                activity.Responsible.Email }.Where(x => !string.IsNullOrWhiteSpace(x)));
        //        model.SourceFunding = activity.SourceFunding != null ? activity.SourceFunding.CodeName : string.Empty;

        //        GRBS grbs = activity.GRBS;
        //        if (grbs == null)
        //        {
        //            grbs = DirectoriesRepositary.GetDirectories<GRBS>(activity.Plan.Year.Value).FirstOrDefault();
        //        }

        //        if (grbs != null)
        //        {
        //            model.GRBS_Code = grbs.Code;
        //            model.GRBS_Code_Name = grbs.CodeName;
        //        }

        //        if (activity.SectionKBK != null)
        //        {
        //            model.SectionKBK_Code = activity.SectionKBK.Code;
        //            model.SectionKBK_Code_Name = activity.SectionKBK.CodeName;
        //        }

        //        if (activity.ExpenseItem != null)
        //        {
        //            model.ExpenseItem_Code_Name = activity.ExpenseItem.CodeName;

        //            if (!string.IsNullOrWhiteSpace(activity.ExpenseItem.Code) && activity.ExpenseItem.Id != EntityBase.Default)
        //            {
        //                var code = activity.ExpenseItem.Code.Replace(" ", "");

        //                if (string.IsNullOrWhiteSpace(activity.Subentry))
        //                {
        //                    model.ExpenseItem_Code = code;
        //                }
        //                else
        //                {
        //                    var lastFour = code.Substring(code.Length - 4);
        //                    model.ExpenseItem_Code = code.Replace(lastFour, activity.Subentry.Replace(" ", ""));
        //                }
        //            }
        //        }

        //        if (activity.WorkForm != null)
        //        {
        //            model.WorkForm_Code = activity.WorkForm.Code;
        //            model.WorkForm_Code_Name = activity.WorkForm.CodeName;
        //        }

        //        //                if (activity.ExpenditureItem != null)
        //        //                {
        //        //                    model.ExpenditureItem_Code = activity.ExpenditureItem.CodeName;
        //        //                    model.ExpenditureItem_Code_Name = activity.ExpenditureItem.CodeName;
        //        //                }

        //        if (activity.ActivityReasons != null)
        //        {
        //            model.ActivityReasons = activity.ActivityReasons.Select(x => new ReasonViewModel()
        //            {
        //                Name = x.Name,
        //                NPAType_Name = x.NPAType != null ? x.NPAType.Name : string.Empty,
        //                Num = x.Num,
        //                Date = x.Date,
        //                Item = x.Item,
        //                Documents = x.Documents.Select(d => new DocumentViewModel()
        //                {
        //                    Id = d.Id,
        //                    FileName = d.FileName
        //                }).ToList()
        //            }).ToList();
        //        }

        //        if (activity.PlansActivityGoals != null)
        //        {
        //            model.Goals = activity.PlansActivityGoals.Select(x => new GoalViewModel()
        //            {
        //                Name = x.Name
        //            }).ToList();
        //        }

        //        model.Subentry = activity.Subentry;

        //        model.PlansActivityVolY0 = activity.PlansActivityVolY0.HasValue ? activity.PlansActivityVolY0.Value : 0;
        //        model.PlansActivityVolY1 = activity.PlansActivityVolY1.HasValue ? activity.PlansActivityVolY1.Value : 0;
        //        model.PlansActivityVolY2 = activity.PlansActivityVolY2.HasValue ? activity.PlansActivityVolY2.Value : 0;

        //        model.PlansActivityAddVolY0 = activity.PlansActivityAddVolY0.HasValue ? activity.PlansActivityAddVolY0.Value : 0;
        //        model.PlansActivityAddVolY1 = activity.PlansActivityAddVolY1.HasValue ? activity.PlansActivityAddVolY1.Value : 0;
        //        model.PlansActivityAddVolY2 = activity.PlansActivityAddVolY2.HasValue ? activity.PlansActivityAddVolY2.Value : 0;

        //        model.PlansActivityFactDate = activity.PlansActivityFactDate;
        //        model.PlansActivityFactVolY0 = activity.PlansActivityFactVolY0;
        //        model.PlansActivityFactVolY1 = activity.PlansActivityFactVolY1;
        //        model.PlansActivityFactVolY2 = activity.PlansActivityFactVolY2;

        //        /////////////////////////////////////  функции

        //        model.Functions = ActivityFuncRepository.GetList(activity.Id)
        //                                         .Rows.Cast<DataRow>()
        //                                         .GroupBy(x => new
        //                                                     {
        //                                                         Name = x["Name"],
        //                                                         Type = x["Type"]
        //                                                     },
        //                                                  x => new { Doc = x["NormativeAct"] })
        //                                         .Select(x => new FuncViewModel
        //                                         {
        //                                             Name = x.Key.Name.ToString(),
        //                                             Type = x.Key.Type.ToString(),
        //                                             Acts = x.Select(d => d.Doc.ToString()).ToList()
        //                                         }).ToList();

        //        /////////////////////////////////////  показатели / индикаторы

        //        var indexes_and_indicators = RepositoryBase<PlansGoalIndicator>.GetAll(x => x.PlanActivity == activity);

        //        model.Indexes = indexes_and_indicators.Where(x => (x.FeatureType == 0 && x.Parent == null))
        //                                              .Select(x => createIndexViewModel(x, indexes_and_indicators))
        //                                              .ToList();

        //        model.Indicators = indexes_and_indicators.Where(x => (x.FeatureType == 1 && x.Parent == null))
        //                                                 .Select(x => createIndicatorViewModel(x, indexes_and_indicators))
        //                                                 .ToList();

        //        /////////////////////////////////////  расходы

        //        model.ExpenseDirections = activity.ExpenseDirections.Select(x => new ExpenseDirectionsViewModel()
        //        {
        //            Code = x.ExpenseDirection.Code,
        //            CodeName = x.ExpenseDirection.CodeName,
        //            ExpenseVolY0 = x.ExpenseVolY0,
        //            ExpenseVolY1 = x.ExpenseVolY1,
        //            ExpenseVolY2 = x.ExpenseVolY2,
        //            ExpenseAddVolY0 = x.ExpenseAddVolY0,
        //            ExpenseAddVolY1 = x.ExpenseAddVolY1,
        //            ExpenseAddVolY2 = x.ExpenseAddVolY2,
        //        }).ToList();

        //        /////////////////////////////////////  Результаты

        //        var plansContracts = RepositoryBase<PlansStateContract>.GetAll(x => x.PlanActivity.Id == activity.Id);
        //        model.Contracts = plansContracts.Select(x => new ContractViewModel()
        //            {
        //                Number = (x.Contract != null) ? x.Contract.Number : string.Empty,
        //                Products = (x.Contract != null && x.Contract.Products != null)
        //                            ? x.Contract.Products.Select(i => i.Name).ToList()
        //                            : new List<string>(),
        //                Sign_Date = (x.Contract != null) ? (DateTime?)x.Contract.Sign_Date : null,
        //                DescriptionPreviousResult = x.DescriptionPreviousResult
        //            }).ToList();

        //        /////////////////////////////////////  Работы

        //        model.Works = WorksRepository.GetWorksList(activity)
        //                            .Rows.Cast<DataRow>()
        //                            .Select(row => new WorkViewModel()
        //                            {
        //                                Name = row["Name"].ToString(),
        //                                ActivityWorkCode = row["ActivityWorkCode"].ToString(),
        //                                ExpeditureItem = row["ExpenditureItemsCodeName"].ToString(),
        //                                OKDP_CodeName = row["OKDP_CodeName"].ToString(),
        //                                ExpenseDirection_CodeName = row["ExpenseDirection_CodeName"].ToString(),
        //                                ExpenseVolumeYear0 = (decimal)row["ExpenseVolumeYear0"],
        //                                WorkTypes = RepositoryBase<PlansWork>.Get(row["Id"]).WorkType.Select(wt => new WorkTypeViewModel()
        //                                {
        //                                    ExpenseType_Name = wt.ExpenseType.Name,
        //                                    ExpenseVolumeYear0 = Convert.ToDecimal(wt.ExpenseVolumeYear0)
        //                                }).ToList()
        //                            }).ToList();

        //        model.WorkDocuments = DocumentRepositoryOld.GetList(DocTypeEnum.Work, activity)
        //                                    .Rows.Cast<DataRow>()
        //                                    .Select(doc => new DocumentViewModel()
        //                                    {
        //                                        Id = (int)doc["Id"],
        //                                        Name = doc["Name"].ToString(),
        //                                        Num = doc["Num"].ToString(),
        //                                        Date = (DateTime?)doc["Date"]
        //                                    }).ToList();

        //        /////////////////////////////////////  Товары

        //        model.Specs = SpecRepository.GetSpecList(activity)
        //                        .Rows.Cast<DataRow>()
        //                        .Select(row => new SpecViewModel()
        //                        {
        //                            Name = row["Name"].ToString(),
        //                            ActivitySpecCode = row["ActivitySpecCode"].ToString(),
        //                            ExpeditureItem = row["ExpenditureItemsCodeName"].ToString(),
        //                            OKDP_Text = row["OKDP_Text"].ToString(),
        //                            QT = row["QT"].ToString(),
        //                            Quantity = row["Quantity"].ToString(),
        //                            Cost = row["Cost"].ToString(),
        //                            ExpenseDirection_CodeName = row["ExpenseDirection_CodeName"].ToString(),
        //                            ExpName = row["ExpName"].ToString(),
        //                            Analog = row["Analog"].ToString(),
        //                            AnalogPrice = row["AnalogPrice"].ToString(),

        //                            SpecTypes = RepositoryBase<PlansSpec>.Get(row["Id"]).SpecType.Select(st => new SpecTypeViewModel()
        //                            {
        //                                CharName = st.CharName,
        //                                CharUnit = st.CharUnit,
        //                                CharValue = st.CharValue
        //                            }).ToList()

        //                        }).ToList();

        //        model.SpecDocuments = DocumentRepositoryOld.GetList(DocTypeEnum.Spec, activity)
        //                                    .Rows.Cast<DataRow>()
        //                                    .Select(doc => new DocumentViewModel()
        //                                    {
        //                                        Id = (int)doc["Id"],
        //                                        Name = doc["Name"].ToString(),
        //                                        Num = doc["Num"].ToString(),
        //                                        Date = (DateTime?)doc["Date"]
        //                                    }).ToList();

        //        /////////////////////////////////////  Закупки

        //        model.PlansZakupki = activity.PlansZakupki.Select(x => new ZakupkiViewModel()
        //        {
        //            NumberName = x.NumberName,
        //            Number = x.Number,
        //            Name = x.Name,
        //            PublishDate = x.PublishDate,
        //            OrderWay_Name = x.OrderWay.Name,
        //            Status_Name = x.Status.Name,
        //            IzvNumb = x.IzvNumb,
        //            IzvDate = x.IzvDate,
        //            ConsDate = x.ConsDate,
        //            ItogDate = x.ItogDate,

        //            Lots = activity.Lots.Where(p => p.Zakupki.Id == x.Id).Select(lot => new LotViewModel()
        //            {
        //                Number = lot.Number,
        //                Code = lot.Code,
        //                Name = lot.Name,
        //                Unit = lot.Unit,
        //                Count = lot.Count
        //            }).ToList()

        //        }).ToList();

        //        model.InitialActivity = activity.PlansInitialActivity.Select(x => new InitialActivityModel()
        //        {
        //            RegNum = x.RegNum,
        //            Name = x.Name,
        //            Comment = x.Comment,
        //            Summ = x.Summ.ToString(),
        //            WorkForm = x.WorkForm.Code,
        //            ExpenditureItem = x.ExpenditureItem.Code,
        //        }).ToList();
        //    }
        //    else if (ActivityObject is DataSet)
        //    {
        //        var ds = ActivityObject as DataSet;

        //        var activityTable = ds.Tables["Plans_Activity"];

        //        model.Id = getValue<int>(activityTable, 0, "Plans_Activity_ID");
        //        model.JoinedTo = getValue<int?>(activityTable, 0, "Plans_Activity_New_ID");
        //        model.Plan_Year = getValue<short>(ds.Tables["Plans"], 0, "Year");
        //        model.Plan_Type = getValue<int>(ds.Tables["Plans"], 0, "Exp_Obj_Type_ID");
        //        model.Code = getValue<string>(activityTable, 0, "Plans_Activity_Code");
        //        model.Name = getValue<string>(activityTable, 0, "Plans_Activity_Name");
        //        model.ActivityType2_Name = getValue<string>(ds.Tables["Plans_Activity_Type2"], 0, "Plans_Activity_Type2_Name");
        //        model.IKTObject = getValue<string>(ds.Tables["IKT_Object"], 0, "IKT_Object_Name");
        //        model.IKTComponentCodeName = string.Join(" ", new[] {
        //                                                        getValue<string>(ds.Tables["IS_or_IKT_ID"], 0, "IS_or_IKT_ID_Code"),
        //                                                        getValue<string>(ds.Tables["IS_or_IKT_ID"], 0, "IS_or_IKT_ID_Name")}
        //                                                        .Where(x => !string.IsNullOrWhiteSpace(x)));

        //        model.AnnotationData = getValue<string>(activityTable, 0, "Plans_Activity_Annotation_Data");
        //        model.Subentry = getValue<string>(activityTable, 0, "Plans_Activity_P_Subentry");
        //        model.PlansActivityVolY0 = Convert.ToDouble(getValue<decimal>(activityTable, 0, "Plans_Activity_P_Vol_Y0"));
        //        model.PlansActivityVolY1 = Convert.ToDouble(getValue<decimal>(activityTable, 0, "Plans_Activity_P_Vol_Y1"));
        //        model.PlansActivityVolY2 = Convert.ToDouble(getValue<decimal>(activityTable, 0, "Plans_Activity_P_Vol_Y2"));
        //        model.PlansActivityAddVolY0 = Convert.ToDouble(getValue<decimal>(activityTable, 0, "Plans_Activity_P_Add_Vol_Y0"));
        //        model.PlansActivityAddVolY1 = Convert.ToDouble(getValue<decimal>(activityTable, 0, "Plans_Activity_P_Add_Vol_Y1"));
        //        model.PlansActivityAddVolY2 = Convert.ToDouble(getValue<decimal>(activityTable, 0, "Plans_Activity_P_Add_Vol_Y2"));
        //        model.PlansActivityFactDate = getValue<DateTime?>(activityTable, 0, "Plans_Activity_P_Fact_Date");
        //        model.PlansActivityFactVolY0 = Convert.ToDouble(getValue<decimal>(activityTable, 0, "Plans_Activity_P_Fact_Vol_Y0"));
        //        model.PlansActivityFactVolY1 = Convert.ToDouble(getValue<decimal>(activityTable, 0, "Plans_Activity_P_Fact_Vol_Y1"));
        //        model.PlansActivityFactVolY2 = Convert.ToDouble(getValue<decimal>(activityTable, 0, "Plans_Activity_P_Fact_Vol_Y2"));

        //        model.ResponsibleInfo = string.Join(", ", new[] {
        //                                                        getValue<string>(ds.Tables["Responsible"], 0, "Responsible_FIO"),
        //                                                        getValue<string>(ds.Tables["Responsible"], 0, "Responsible_Job_Name"),
        //                                                        getValue<string>(ds.Tables["Responsible"], 0, "Responsible_Phone"),
        //                                                        getValue<string>(ds.Tables["Responsible"], 0, "Responsible_Email")}
        //                                                        .Where(x => !string.IsNullOrWhiteSpace(x)));

        //        model.SourceFunding = string.Format("{0} - {1}", getValue<string>(ds.Tables["Source_Funding"], 0, "Source_Funding_Code"),
        //                                                         getValue<string>(ds.Tables["Source_Funding"], 0, "Source_Funding_Name"));

        //        var grbs = ds.Tables["GRBS"];
        //        if (grbs == null || grbs.Rows.Count == 0)
        //        {
        //            var grbsObject = DirectoriesRepositary.GetDirectories<GRBS>(model.Plan_Year).FirstOrDefault();
        //            model.GRBS_Code = grbsObject.Code;
        //            model.GRBS_Code_Name = grbsObject.CodeName;
        //        }
        //        else
        //        {
        //            model.GRBS_Code = getValue<string>(grbs, 0, "GRBS_Code");
        //            model.GRBS_Code_Name = string.Format("{0} - {1}", model.GRBS_Code,
        //                                                              getValue<string>(grbs, 0, "GRBS_Name"));
        //        }

        //        var Section_KBK = ds.Tables["Section_KBK"];
        //        if (Section_KBK != null && Section_KBK.Rows.Count > 0)
        //        {
        //            model.SectionKBK_Code = getValue<string>(Section_KBK, 0, "Section_KBK_Code");
        //            model.SectionKBK_Code_Name = string.Format("{0} - {1}", model.SectionKBK_Code,
        //                                                                    getValue<string>(Section_KBK, 0, "Section_KBK_Name"));
        //        }

        //        var Expense_Item = ds.Tables["Expense_Item"];
        //        if (Expense_Item != null && Expense_Item.Rows.Count > 0)
        //        {
        //            model.ExpenseItem_Code = getValue<string>(Expense_Item, 0, "Expense_Item_Code");
        //            model.ExpenseItem_Code_Name = string.Format("{0} - {1}", model.ExpenseItem_Code,
        //                                                                     getValue<string>(Expense_Item, 0, "Expense_Item_Name"));

        //            if (!string.IsNullOrWhiteSpace(model.ExpenseItem_Code)
        //                && getValue<int>(Expense_Item, 0, "Expense_Item_ID") != EntityBase.Default)
        //            {
        //                var code = model.ExpenseItem_Code.Replace(" ", "");

        //                if (string.IsNullOrWhiteSpace(model.Subentry))
        //                {
        //                    model.ExpenseItem_Code = code;
        //                }
        //                else
        //                {
        //                    var lastfour = code.Substring(code.Length - 4);
        //                    model.ExpenseItem_Code = code.Replace(lastfour, model.Subentry.Replace(" ", ""));
        //                }
        //            }
        //        }

        //        var Work_Form = ds.Tables["Work_Form"];
        //        if (Work_Form != null && Work_Form.Rows.Count > 0)
        //        {
        //            model.WorkForm_Code = getValue<string>(Work_Form, 0, "Work_Form_Code");
        //            model.WorkForm_Code_Name = string.Format("{0} - {1}", model.WorkForm_Code,
        //                                                                  getValue<string>(Work_Form, 0, "Work_Form_Name"));
        //        }

        //        var Plans_Activity_Reason = ds.Tables["Plans_Activity_Reason"];
        //        if (Plans_Activity_Reason != null && Plans_Activity_Reason.Rows.Count > 0)
        //        {
        //            var NPA_Type = ds.Tables.Contains("NPA_Type") ? ds.Tables["NPA_Type"] : null;
        //            model.ActivityReasons = Plans_Activity_Reason.Rows.Cast<DataRow>()
        //                                        .Select(x => new ReasonViewModel()
        //                                        {
        //                                            Name = x["Plans_Activity_Reason_Name"].ToString(),
        //                                            NPAType_Name = Convert.ToString(FindValue(NPA_Type, x["NPA_Type_ID"], "NPA_Type_Name")),
        //                                            Num = x["Plans_Activity_Reason_Num"].ToString(),
        //                                            Date = x["Plans_Activity_Reason_DT"] as Nullable<DateTime>,
        //                                            Item = x["Plans_Activity_Reason_Item"].ToString(),
        //                                            //Documents = x.Documents.Select(d => new DocumentViewModel()
        //                                            //{
        //                                            //    Id = d.Id,
        //                                            //    FileName = d.FileName
        //                                            //}).ToList()
        //                                        }).ToList();
        //        }

        //        var Plans_Activity_Goal = ds.Tables["Plans_Activity_Goal"];
        //        if (Plans_Activity_Goal != null && Plans_Activity_Goal.Rows.Count > 0)
        //        {
        //            model.Goals = Plans_Activity_Goal.Rows.Cast<DataRow>()
        //                           .Select(row => new GoalViewModel()
        //                            {
        //                                Name = row["Plans_Activity_Goal_Name"].ToString()
        //                            }).ToList();
        //        }

        //        /////////////////////////////////////  функции
        //        var functions = ds.Tables["Functions"];
        //        model.Functions = functions.Rows.Cast<DataRow>()
        //                                    .GroupBy(x => new
        //                                    {
        //                                        Name = x["Name"],
        //                                        Type = x["Type"]
        //                                    }, x => new { Doc = x["NormativeAct"] })
        //                                    .Select(x => new FuncViewModel
        //                                    {
        //                                        Name = x.Key.Name.ToString(),
        //                                        Type = x.Key.Type.ToString(),
        //                                        Acts = x.Select(d => d.Doc.ToString()).ToList()
        //                                    }).ToList();

        //        /////////////////////////////////////  показатели / индикаторы
        //        var indexes_and_indicators = ds.Tables["Plans_Goal_Indicator"].Rows.Cast<DataRow>();

        //        model.Indexes = indexes_and_indicators.Where(row => ((int)row["Plans_Goal_Indicator_Feature_Type"] == 0 &&
        //                                                             (row["Plans_Goal_Indicator_Par_ID"] as System.Nullable<int>) == null))

        //                                              .Select(x => createIndexViewModelDT(x, indexes_and_indicators, functions, Plans_Activity_Goal))
        //                                              .ToList();

        //        model.Indicators = indexes_and_indicators.Where(row => ((int)row["Plans_Goal_Indicator_Feature_Type"] == 1 &&
        //                                                             (row["Plans_Goal_Indicator_Par_ID"] as System.Nullable<int>) == null))
        //                                              .Select(x => createIndicatorViewModelDT(x, indexes_and_indicators, functions))
        //                                              .ToList();

        //        /////////////////////////////////////  расходы
        //        var Plans_Activity_ED = ds.Tables["Plans_Activity_ED"];
        //        var Expense_Direction = ds.Tables["Expense_Direction"];

        //        if (Work_Form != null && Work_Form.Rows.Count > 0)
        //        {
        //            model.ExpenseDirections = Plans_Activity_ED.Rows.Cast<DataRow>()
        //                                       .Select(row => new ExpenseDirectionsViewModel()
        //                                       {
        //                                           Code = Convert.ToString(FindValue(Expense_Direction, row["Expense_Direction_ID"], "Expense_Direction_Code")),
        //                                           CodeName = string.Format("{0} - {1}", FindValue(Expense_Direction, row["Expense_Direction_ID"], "Expense_Direction_Code"),
        //                                                                                 FindValue(Expense_Direction, row["Expense_Direction_ID"], "Expense_Direction_Name")),
        //                                           ExpenseVolY0 = getValue<decimal>(row, "Plans_Activity_ED_VB_Y0"),
        //                                           ExpenseVolY1 = getValue<decimal>(row, "Plans_Activity_ED_VB_Y1"),
        //                                           ExpenseVolY2 = getValue<decimal>(row, "Plans_Activity_ED_VB_Y2"),
        //                                           ExpenseAddVolY0 = getValue<decimal>(row, "Plans_Activity_ED_Add_Y0"),
        //                                           ExpenseAddVolY1 = getValue<decimal>(row, "Plans_Activity_ED_Add_Y1"),
        //                                           ExpenseAddVolY2 = getValue<decimal>(row, "Plans_Activity_ED_Add_Y2"),
        //                                       }).ToList();
        //        }

        //        /////////////////////////////////////  Результаты
        //        var Plans_State_Contract = ds.Tables["Plans_State_Contract"];

        //        if (Plans_State_Contract != null)
        //        {
        //            var Contract = ds.Tables.Contains("Contract") ? ds.Tables["Contract"] : null;
        //            var Contract_Product = ds.Tables.Contains("Contract_Product") ? ds.Tables["Contract_Product"] : null;

        //            model.Contracts = new List<ContractViewModel>();
        //            foreach (DataRow row in Plans_State_Contract.Rows)
        //            {
        //                ContractViewModel contractModel = new ContractViewModel();
        //                contractModel.DescriptionPreviousResult = getValue<string>(row, "Plans_State_Contract_Results_Discr");
        //                int contractId = getValue<int>(row, "Contract_ID");
        //                DataRow contractRow = (DataRow)FindValue(Contract, contractId);
        //                if (contractRow != null)
        //                {
        //                    contractModel.Number = getValue<string>(contractRow, "Contract_Number");
        //                    contractModel.Sign_Date = getValue<DateTime?>(contractRow, "Contract_Sign_Date");

        //                    if (Contract_Product != null)
        //                    {
        //                        contractModel.Products = Contract_Product.Rows.Cast<DataRow>()
        //                                                                 .Where(cp => cp["Contract_ID"].Equals(contractId))
        //                                                                 .Select(cp => cp["Contract_Product_Name"].ToString())
        //                                                                 .ToList();
        //                    }
        //                }
        //            }
        //        }

        //        ///////////////////////////////////////  Работы
        //        var Works = ds.Tables["Works"];

        //        var workList = Works.Rows.Cast<DataRow>();
        //        model.Works = workList
        //                        .Where(row => (int)row["ParentId"] == 0)
        //                        .Select(row => new WorkViewModel()
        //                        {
        //                            Name = row["Name"].ToString(),
        //                            ActivityWorkCode = row["ActivityWorkCode"].ToString(),
        //                            ExpeditureItem = row["ExpenditureItemsCodeName"].ToString(),
        //                            OKDP_CodeName = row["OKDP_CodeName"].ToString(),
        //                            ExpenseDirection_CodeName = row["ExpenseDirection_CodeName"].ToString(),
        //                            ExpenseVolumeYear0 = (decimal)row["ExpenseVolumeYear0"],
        //                            WorkTypes = workList
        //                                            .Where(wt => wt["ParentId"].Equals(row["Id"]))
        //                                            .Select(wt => new WorkTypeViewModel()
        //                                            {
        //                                                ExpenseType_Name = getValue<string>(wt, "ExpenseType_Name"),
        //                                                ExpenseVolumeYear0 = getValue<decimal>(wt, "ExpenseVolumeYear0")
        //                                            }).ToList()
        //                        }).ToList();

        //        ///////////////////////////////////////  Товары

        //        var Specs = ds.Tables["Specs"];
        //        var specList = Specs.Rows.Cast<DataRow>();

        //        model.Specs = specList
        //                        .Where(row => (int)row["ParentId"] == 0)
        //                        .Select(row => new SpecViewModel()
        //                        {
        //                            Name = row["Name"].ToString(),
        //                            ActivitySpecCode = row["ActivitySpecCode"].ToString(),
        //                            ExpeditureItem = row["ExpenditureItemsCodeName"].ToString(),
        //                            OKDP_Text = row["OKDP_Text"].ToString(),
        //                            QT = row["QT"].ToString(),
        //                            Quantity = row["Quantity"].ToString(),
        //                            Cost = row["Cost"].ToString(),
        //                            ExpenseDirection_CodeName = row["ExpenseDirection_CodeName"].ToString(),
        //                            ExpName = row["ExpName"].ToString(),
        //                            Analog = row["Analog"].ToString(),
        //                            AnalogPrice = row["AnalogPrice"].ToString(),

        //                            SpecTypes = specList
        //                                        .Where(st => st["ParentId"].Equals(row["Id"]))
        //                                        .Select(st => new SpecTypeViewModel()
        //                                        {
        //                                            CharName = st["ExpName"].ToString(),
        //                                            CharUnit = st["Analog"].ToString(),
        //                                            CharValue = st["Quantity"].ToString()
        //                                        }).ToList()
        //                        }).ToList();

        //        /////////////////////////////////////  Закупки

        //        var Plans_Zakupki = ds.Tables["Plans_Zakupki"];
        //        if (Plans_Zakupki != null)
        //        {
        //            var lots = ds.Tables["Plans_Lots"].Rows.Cast<DataRow>();

        //            model.PlansZakupki = Plans_Zakupki.Rows.Cast<DataRow>()
        //                .Select(row => new ZakupkiViewModel()
        //                {
        //                    NumberName = (row["Plans_Zakupki_Number"].ToString() + " - " + row["Plans_Zakupki_Contract_Product_Name"].ToString()),
        //                    Number = getValue<int>(row, "Plans_Zakupki_Number"),
        //                    Name = row["Plans_Zakupki_Contract_Product_Name"].ToString(),
        //                    PublishDate = getValue<DateTime?>(row, "Plans_Zakupki_Publish_Date"),
        //                    OrderWay_Name = row["Order_Way_Name"].ToString(),
        //                    Status_Name = row["Plans_Zakupki_Status_Name"].ToString(),
        //                    IzvNumb = row["Plans_Zakupki_Izv_Numb"].ToString(),
        //                    IzvDate = getValue<DateTime?>(row, "Plans_Zakupki_Izv_Date"),
        //                    ConsDate = getValue<DateTime?>(row, "Plans_Zakupki_Cons_Date"),
        //                    ItogDate = getValue<DateTime?>(row, "Plans_Zakupki_Itog_Date"),

        //                    Lots = lots.Where(lot => row["Plans_Zakupki_ID"].Equals(lot["Plans_Zakupki_ID"]))
        //                               .Select(lot => new LotViewModel()
        //                                {
        //                                    Number = getValue<int?>(lot, "Plans_Lots_Number"),
        //                                    Code = getValue<string>(lot, "Plans_Lots_OKDP_Code"),
        //                                    Name = getValue<string>(lot, "Plans_Lots_Name"),
        //                                    Unit = getValue<string>(lot, "Plans_Lots_Unit"),
        //                                    Count = getValue<int?>(lot, "Plans_Lots_Kol"),
        //                                })
        //                                .ToList()
        //                })
        //                .ToList();
        //        }

        //        if (ds.Tables.Contains("Document"))
        //        {
        //            //model.WorkDocuments = DocumentRepository.GetList(DocTypeEnum.Work, activity)
        //            //                        .Rows.Cast<DataRow>()
        //            //                        .Select(doc => new DocumentViewModel()
        //            //                        {
        //            //                            Id = (int)doc["Id"],
        //            //                            Name = doc["Name"].ToString(),
        //            //                            Num = doc["Num"].ToString(),
        //            //                            Date = (DateTime?)doc["Date"]
        //            //                        }).ToList();

        //            //model.SpecDocuments = DocumentRepository.GetList(DocTypeEnum.Spec, activity)
        //            //                            .Rows.Cast<DataRow>()
        //            //                            .Select(doc => new DocumentViewModel()
        //            //                            {
        //            //                                Id = (int)doc["Id"],
        //            //                                Name = doc["Name"].ToString(),
        //            //                                Num = doc["Num"].ToString(),
        //            //                                Date = (DateTime?)doc["Date"]
        //            //                            }).ToList();
        //        }

        //    }

        //    return model;
        //}

        private object FindValue(DataTable table, object key, string column = null)
        {
            if (table == null || key == null)
                return null;

            string keyColumnName = table.TableName + "_ID";

            if (string.IsNullOrEmpty(column))
            {
                return table.Rows.Cast<DataRow>().Where(row => row[keyColumnName].Equals(key))
                                                 .FirstOrDefault();
            }
            else
            {
                return table.Rows.Cast<DataRow>().Where(row => row[keyColumnName].Equals(key))
                                                 .Select(row => row[column])
                                                 .FirstOrDefault();
            }
        }

        private T getValue<T>(DataRow row, string column)
        {
            if (row[column] == DBNull.Value)
            {
                return default(T);
            }
            else
            {
                return (T)row[column];
            }
        }

        private T getValue<T>(DataTable table, int rowIndex, string column)
        {
            if (table == null || table.Rows.Count <= rowIndex)
            {
                return default(T);
            }
            else
            {
                return getValue<T>(table.Rows[rowIndex], column);
            }
        }

        //private IndexViewModel createIndexViewModel(PlansGoalIndicator currentIndex, IList<PlansGoalIndicator> allIndexes)
        //{
        //    var indexModel = new IndexViewModel()
        //        {
        //            Name = currentIndex.Name,
        //            ActivityFunc = PlansActivityFuncController.GetName(currentIndex.ActivityFunc),
        //            Goal = currentIndex.Goal != null ? currentIndex.Goal.Name : null,
        //            PRSFactVal = currentIndex.PRSFactVal,
        //            UOM = currentIndex.UOM,
        //        };

        //    var firstYear = allIndexes.Where(x => (x.Parent == currentIndex && (x.Year - currentIndex.Year) == 1)).FirstOrDefault();
        //    if (firstYear != null)
        //    {
        //        indexModel.FirstPRSFactVal = firstYear.PRSFactVal;
        //        indexModel.FirstAddFounding = firstYear.AddFounding;
        //        indexModel.FirstUOM = firstYear.UOM;
        //    }

        //    var secondYear = allIndexes.Where(x => (x.Parent == currentIndex && (x.Year - currentIndex.Year) == 2)).FirstOrDefault();
        //    if (secondYear != null)
        //    {
        //        indexModel.SecondPRSFactVal = secondYear.PRSFactVal;
        //        indexModel.SecondAddFounding = secondYear.AddFounding;
        //        indexModel.SecondUOM = secondYear.UOM;
        //    }

        //    var thridYear = allIndexes.Where(x => (x.Parent == currentIndex && (x.Year - currentIndex.Year) == 3)).FirstOrDefault();
        //    if (thridYear != null)
        //    {
        //        indexModel.ThridPRSFactVal = thridYear.PRSFactVal;
        //        indexModel.ThridAddFounding = thridYear.AddFounding;
        //        indexModel.ThridUOM = thridYear.UOM;
        //    }

        //    return indexModel;
        //}

        //private IndexViewModel createIndexViewModelDT(DataRow currentIndex, IEnumerable<DataRow> allIndexes, DataTable functions, DataTable goals)
        //{
        //    string goalName = null;
        //    if (goals != null)
        //    {
        //        var goalRow = goals.Rows.Cast<DataRow>().Where(r => r["Plans_Activity_Goal_ID"].Equals(currentIndex["Plans_Activity_Goal_ID"])).FirstOrDefault();
        //        if (goalRow != null)
        //        {
        //            goalName = goalRow["Plans_Activity_Goal_Name"].ToString();
        //        }
        //    }

        //    var indexModel = new IndexViewModel()
        //    {
        //        Name = currentIndex["Plans_Goal_Indicator_Name"].ToString(),
        //        ActivityFunc = GetActivityFuncNameDT(currentIndex["Plans_Activity_Func_ID"] as Nullable<int>, functions),
        //        Goal = goalName,
        //        PRSFactVal = currentIndex["Plans_Goal_Indicator_PRS_Fact_Val"].ToString(),
        //        UOM = currentIndex["Plans_Goal_Indicator_UOM"].ToString(),
        //    };

        //    var currentIndexId = (int)currentIndex["Plans_Goal_Indicator_Id"];
        //    var currentIndexYear = (short)currentIndex["Plans_Goal_Indicator_Year"];

        //    var firstYear = allIndexes.Where(x => (x["Plans_Goal_Indicator_Par_ID"] as Nullable<int> == currentIndexId
        //                                       && ((short)x["Plans_Goal_Indicator_Year"] - currentIndexYear) == 1)).FirstOrDefault();
        //    if (firstYear != null)
        //    {
        //        indexModel.FirstPRSFactVal = firstYear["Plans_Goal_Indicator_PRS_Fact_Val"].ToString();
        //        indexModel.FirstAddFounding = firstYear["Plans_Goal_Indicator_Add_Funding"].ToString();
        //        indexModel.FirstUOM = firstYear["Plans_Goal_Indicator_UOM"].ToString();
        //    }

        //    var secondYear = allIndexes.Where(x => (x["Plans_Goal_Indicator_Par_ID"] as Nullable<int> == currentIndexId
        //                                       && ((short)x["Plans_Goal_Indicator_Year"] - currentIndexYear) == 2)).FirstOrDefault();
        //    if (secondYear != null)
        //    {
        //        indexModel.SecondPRSFactVal = secondYear["Plans_Goal_Indicator_PRS_Fact_Val"].ToString();
        //        indexModel.SecondAddFounding = secondYear["Plans_Goal_Indicator_Add_Funding"].ToString();
        //        indexModel.SecondUOM = secondYear["Plans_Goal_Indicator_UOM"].ToString();
        //    }

        //    var thridYear = allIndexes.Where(x => (x["Plans_Goal_Indicator_Par_ID"] as Nullable<int> == currentIndexId
        //                                       && ((short)x["Plans_Goal_Indicator_Year"] - currentIndexYear) == 3)).FirstOrDefault();
        //    if (thridYear != null)
        //    {
        //        indexModel.ThridPRSFactVal = thridYear["Plans_Goal_Indicator_PRS_Fact_Val"].ToString();
        //        indexModel.ThridAddFounding = thridYear["Plans_Goal_Indicator_Add_Funding"].ToString();
        //        indexModel.ThridUOM = thridYear["Plans_Goal_Indicator_UOM"].ToString();
        //    }

        //    return indexModel;
        //}

        //protected static string GetActivityFuncNameDT(int? Plans_Activity_Func_ID, DataTable functions)
        //{
        //    if (Plans_Activity_Func_ID == null || functions != null || functions.Rows.Count > 0)
        //    {
        //        return "Не задано";
        //    }

        //    var funcRow = functions.Rows.Cast<DataRow>().Where(x => (int?)x["Id"] == Plans_Activity_Func_ID).FirstOrDefault();

        //    return (funcRow == null) ? "Не задано" :
        //                               funcRow["Name"].ToString();
        //}

        //private IndicatorViewModel createIndicatorViewModel(PlansGoalIndicator currentIndicator, IList<PlansGoalIndicator> allIndexes)
        //{
        //    var indicatorModel = new IndicatorViewModel()
        //    {
        //        Name = currentIndicator.Name,
        //        ActivityFunc = PlansActivityFuncController.GetName(currentIndicator.ActivityFunc),
        //        Algorythm = currentIndicator.Algorythm,
        //    };

        //    var firstYear = allIndexes.Where(x => (x.Parent == currentIndicator && (x.Year - currentIndicator.Year) == 1)).FirstOrDefault();
        //    if (firstYear != null)
        //    {
        //        indicatorModel.FirstPRSFactVal = firstYear.PRSFactVal;
        //        indicatorModel.FirstAddFounding = firstYear.AddFounding;
        //        indicatorModel.FirstUOM = firstYear.UOM;
        //    }

        //    var secondYear = allIndexes.Where(x => (x.Parent == currentIndicator && (x.Year - currentIndicator.Year) == 2)).FirstOrDefault();
        //    if (secondYear != null)
        //    {
        //        indicatorModel.SecondPRSFactVal = secondYear.PRSFactVal;
        //        indicatorModel.SecondAddFounding = secondYear.AddFounding;
        //        indicatorModel.SecondUOM = secondYear.UOM;
        //    }

        //    var thridYear = allIndexes.Where(x => (x.Parent == currentIndicator && (x.Year - currentIndicator.Year) == 3)).FirstOrDefault();
        //    if (thridYear != null)
        //    {
        //        indicatorModel.ThridPRSFactVal = thridYear.PRSFactVal;
        //        indicatorModel.ThridAddFounding = thridYear.AddFounding;
        //        indicatorModel.ThridUOM = thridYear.UOM;
        //    }

        //    return indicatorModel;
        //}

        //private IndicatorViewModel createIndicatorViewModelDT(DataRow currentIndicator, IEnumerable<DataRow> allIndexes, DataTable functions)
        //{
        //    var indicatorModel = new IndicatorViewModel()
        //    {
        //        Name = currentIndicator["Plans_Goal_Indicator_Name"].ToString(),
        //        ActivityFunc = GetActivityFuncNameDT(currentIndicator["Plans_Activity_Func_ID"] as Nullable<int>, functions),
        //        Algorythm = currentIndicator["Plans_Goal_Indicator_Algorithm"].ToString(),
        //    };

        //    var currentIndexId = (int)currentIndicator["Plans_Goal_Indicator_Id"];
        //    var currentIndexYear = (short)currentIndicator["Plans_Goal_Indicator_Year"];

        //    var firstYear = allIndexes.Where(x => (x["Plans_Goal_Indicator_Par_ID"] as Nullable<int> == currentIndexId
        //                                       && ((short)x["Plans_Goal_Indicator_Year"] - currentIndexYear) == 1)).FirstOrDefault();
        //    if (firstYear != null)
        //    {
        //        indicatorModel.FirstPRSFactVal = firstYear["Plans_Goal_Indicator_PRS_Fact_Val"].ToString();
        //        indicatorModel.FirstAddFounding = firstYear["Plans_Goal_Indicator_Add_Funding"].ToString();
        //        indicatorModel.FirstUOM = firstYear["Plans_Goal_Indicator_UOM"].ToString();
        //    }

        //    var secondYear = allIndexes.Where(x => (x["Plans_Goal_Indicator_Par_ID"] as Nullable<int> == currentIndexId
        //                                       && ((short)x["Plans_Goal_Indicator_Year"] - currentIndexYear) == 2)).FirstOrDefault();
        //    if (secondYear != null)
        //    {
        //        indicatorModel.SecondPRSFactVal = secondYear["Plans_Goal_Indicator_PRS_Fact_Val"].ToString();
        //        indicatorModel.SecondAddFounding = secondYear["Plans_Goal_Indicator_Add_Funding"].ToString();
        //        indicatorModel.SecondUOM = secondYear["Plans_Goal_Indicator_UOM"].ToString();
        //    }

        //    var thridYear = allIndexes.Where(x => (x["Plans_Goal_Indicator_Par_ID"] as Nullable<int> == currentIndexId
        //                                       && ((short)x["Plans_Goal_Indicator_Year"] - currentIndexYear) == 3)).FirstOrDefault();
        //    if (thridYear != null)
        //    {
        //        indicatorModel.ThridPRSFactVal = thridYear["Plans_Goal_Indicator_PRS_Fact_Val"].ToString();
        //        indicatorModel.ThridAddFounding = thridYear["Plans_Goal_Indicator_Add_Funding"].ToString();
        //        indicatorModel.ThridUOM = thridYear["Plans_Goal_Indicator_UOM"].ToString();
        //    }

        //    return indicatorModel;
        //}

        //private IList<ActivityViewModel> hiddenChilds_internal;
        //public IList<ActivityViewModel> HiddenChilds
        //{
        //    get
        //    {
        //        if (hiddenChilds_internal == null)
        //        {
        //            hiddenChilds_internal = new List<ActivityViewModel>();

        //            var hidden = PlansActivityRepository.GetRelatives(CurrentEdit.Id);
        //            if (hidden != null && hidden.Rows.Count > 0)
        //            {
        //                foreach (DataRow child in hidden.Rows)
        //                {
        //                    if ((int)child["LT"] == 0 &&  )
        //                    {
        //                        var childDS = PlansActivityRepository.GetHidden(child["Plans_Activity_ID"]);
        //                        hiddenChilds_internal.Add(this.GetActivityViewModel(childDS));
        //                    }
        //                }
        //            }
        //        }

        //        return hiddenChilds_internal;
        //    }
        //}

        //private List<ActivityLinkViewModel> links_internal = null;

        //public IList<ActivityLinkViewModel> GetLinks(object ActivityId = null, int? LinkType = null)
        //{
        //    if (links_internal == null)
        //    {
        //        object id = ActivityId ?? this.CurrentEdit.Id;

        //        var dt = PlansActivityRepository.GetRelatives(id);
        //        if (dt != null)
        //        {
        //            links_internal = dt.Rows.Cast<DataRow>()
        //                                    .Select(x => new ActivityLinkViewModel()
        //                                    {
        //                                        Id = (int)x["Plans_Activity_ID"],
        //                                        Code = (string)x["Plans_Activity_Code"],
        //                                        Name = (string)x["Plans_Activity_Name"],
        //                                        IsDeleted = Convert.ToBoolean(x["Is_Deleted"]),
        //                                        LinkType = (int)x["LT"]
        //                                    })
        //                                    .ToList();
        //        }
        //        else
        //        {
        //            links_internal = new List<ActivityLinkViewModel>();
        //        }
        //    }

        //    if (LinkType.HasValue)
        //    {
        //        return links_internal.Where(x => x.LinkType == LinkType.Value).ToList();
        //    }
        //    else
        //    {
        //        return links_internal;
        //    }

        //}

        ////public IList<ActivityLinkViewModel> relativeLinks_internal;
        //public IList<ActivityLinkViewModel> GetRelatives(object ActivityId)
        //{
        //    List<ActivityLinkViewModel> links = new List<ActivityLinkViewModel>();

        //    var hiddenChilds = PlansActivityRepository.GetChilds(ActivityId);
        //    if (hiddenChilds != null)
        //    {
        //        foreach(DataRow row in hiddenChilds.Rows)
        //        {
        //            var ds = PlansActivityRepository.GetHidden(row[0], true);
        //            if (ds != null && ds.Tables.Contains("Plans_Activity") && ds.Tables["Plans_Activity"].Rows.Count > 0)
        //            {
        //                var activ = ds.Tables["Plans_Activity"].Rows[0];
        //                links.Add(new ActivityLinkViewModel()
        //                {
        //                    Id = (int)activ["Plans_Activity_ID"],
        //                    Name = activ["Plans_Activity_Name"].ToString(),
        //                    Code = activ["Plans_Activity_Code"].ToString(),
        //                    IsChild = true,
        //                    IsDeleted = Convert.ToBoolean(activ["Is_Deleted"]),
        //                });
        //            }
        //        }
        //    }

        //    var hiddenOwners = PlansActivityRepository.GetJoinedActivities(new List<int>() { Convert.ToInt32(ActivityId) });
        //    if (hiddenOwners!= null)
        //    {
        //        foreach(var id in hiddenOwners)
        //        {
        //            var ds = PlansActivityRepository.GetHidden(id, true);
        //            if (ds != null && ds.Tables.Contains("Plans_Activity") && ds.Tables["Plans_Activity"].Rows.Count > 0)
        //            {
        //                var activ = ds.Tables["Plans_Activity"].Rows[0];
        //                links.Add(new ActivityLinkViewModel()
        //                {
        //                    Id = (int)activ["Plans_Activity_ID"],
        //                    Name = activ["Plans_Activity_Name"].ToString(),
        //                    Code = activ["Plans_Activity_Code"].ToString(),
        //                    IsOwner = true,
        //                    IsDeleted = Convert.ToBoolean(activ["Is_Deleted"]),
        //                });
        //            }
        //        }
        //    }

        //    return links;
        //}

        //public object GetList()
        //{
        //    Repository
        //}
    }
}