﻿using System;
using System.Web;
using GB.Handlers;
using GB.Helpers;
using GB.MKRF.Entities.Plans;
using GB.Repository;
using GB.MKRF.Web.Helpers;

namespace GB.MKRF.Web.Handlers
{
    public class OfferDocsFileHandler: FileHandler
    {
        const string urlRoot = @"~/rdocs/";

        protected override string GetFileName(File_Info info)
        {
            ReportByPlanDocument document = RepositoryBase<ReportByPlanDocument>.Get(info.Id);

            return document.FileName;
        }

        protected override StorageTypeEnum GetStorageType(FileHandler.File_Info info)
        {
                return StorageTypeEnum.Database;
        }

        protected override void SentFileFromDatabase(HttpContext context, File_Info info)
        {
            int fileId = 0;
            if (info.FileName.Contains("repdocs"))
            {
                string[] urlParts = info.FileName.Split('/');
                fileId = Convert.ToInt32(urlParts[urlParts.Length - 1]);
            }
            else return;

            if (fileId == 0) return;
            ReportByPlanDocument document = RepositoryBase<ReportByPlanDocument>.Get(fileId);
            //string fileName = string.IsNullOrEmpty(info.FileName) ? document.FileName : info.FileName;
            string fileName = document.FileName;

            if (string.IsNullOrEmpty(fileName))
            {
                fileName = "noname";
            }

            FileHelper.SendFile(context, fileName, null, document.File);
        }

    }
}