﻿using System.Collections.Generic;
using bgTeam.IsRui.Domain.Entities;

namespace bgTeam.IsRui.DataAccess
{
    public class NumHelper<T> where T : EntityNumerable
    {
        /// <summary>Переместить по списку</summary>
        //public static void Move(IList<T> ownerList, T currentEntity, bool Up)
        //{
        //    int nextNum = currentEntity.Num + (Up ? -1 : 1);
        //    T nextEntity = null;

        //    // ищем соседа
        //    foreach (T item in ownerList)
        //    {
        //        if ((nextEntity == null && (Up && item.Num <= nextNum || !Up && item.Num >= nextNum)) ||
        //            (Up && item.Num <= nextNum && item.Num > nextEntity.Num ||
        //                !Up && item.Num >= nextNum && item.Num < nextEntity.Num ))
        //        {
        //            nextEntity = item;

        //            if (nextNum == nextEntity.Num)
        //                break;
        //        }
        //    }

        //    if (nextEntity != null)
        //    {
        //        // меняем номера местами
        //        int num = currentEntity.Num;
        //        currentEntity.Num = nextEntity.Num;
        //        nextEntity.Num = num;

        //        RepositoryBase<T>.SaveOrUpdate(currentEntity);
        //        RepositoryBase<T>.SaveOrUpdate(nextEntity);
        //    }
        //}

        /// <summary>Получить минимально возможный номер</summary>
        public static int GetMinAvailableNum(IList<T> ownerList)
        {
            int minNum = 0;

            if (ownerList != null || ownerList.Count > 0)
            {
                foreach (T item in ownerList)
                {
                    if (minNum < item.Num)
                    {
                        minNum = item.Num;
                    }
                }
            }

            return minNum + 1;
        }

        ///// <summary>Упорядочить номера</summary>
        //public static void OrderNums(IList<T> ownerList)
        //{
        //    OrderNums(ownerList, true);

        //}

        ///// <summary>Упорядочить номера</summary>
        //public static void OrderNums(IList<T> ownerList, bool saveItems)
        //{
        //    SortedDictionary<string, T> dict = new SortedDictionary<string, T>();
        //    foreach (T item in ownerList)
        //    {
        //        dict.Add(item.Num.ToString("0000000000") + ";" + item.Id.ToString("0000000000"), item);
        //    }

        //    int num = 1;
        //    foreach (string key in dict.Keys)
        //    {
        //        T item = dict[key];
        //        if (item.Num != num)
        //        {
        //            item.Num = num;
        //            if (saveItems)
        //            {
        //                RepositoryBase<T>.SaveOrUpdate(item);
        //            }
        //        }
        //        num++;
        //    }
        //}

        //public static void RefreshNums(IList<T> list)
        //{
        //    int num = 1;

        //    foreach (T item in list)
        //    {
        //        if (item.Num != num)
        //        {
        //            item.Num = num;

        //            RepositoryBase<T>.SaveOrUpdate(item);
        //        }
        //        num++;
        //    }
        //}
    }
}