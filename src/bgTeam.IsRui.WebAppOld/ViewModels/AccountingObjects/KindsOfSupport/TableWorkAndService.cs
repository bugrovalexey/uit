﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.WebApp.Helpers;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport
{
    public class TableWorkAndService : NotificationBaseViewModel
    {
        #region consts

        public const string NameCaption = "Наименование";
        public const string CategoryKindOfSupportCaption = "Вид работы";
        public const string InformationLeasedInfrastructuretCaption = "Сведения по арендуемой инфраструктуре и условиям ее использования";
        public const string OkvedCaption = "ОКВЭД";
        public const string SummaCaption = "Стоимость";

        #endregion consts

        public TableWorkAndService(WorkAndService entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            CategoryKindOfSupportName = entity.CategoryKindOfSupport.Return(cat => cat.Name, default(string));
            InformationLeasedInfrastructure = entity.InformationLeasedInfrastructure;
            OkvedName = entity.Okved.Return(okved => okved.Name, default(string));
            Summa = entity.Summa.ToMoneyString();
        }

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(CategoryKindOfSupportCaption)]
        public string CategoryKindOfSupportName { get; set; }

        [DisplayName(InformationLeasedInfrastructuretCaption)]
        public string InformationLeasedInfrastructure { get; set; }

        [DisplayName(OkvedCaption)]
        public string OkvedName { get; set; }

        [DisplayName(SummaCaption)]
        public string Summa { get; set; }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteWorkAndService", "AccountingObjects", new { id = Id }, "GridWorksAndServices");
            }
        }
    }
}