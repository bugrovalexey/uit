﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System.Threading.Tasks;
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Documents;

    public class FileDeleteStory : IStory<FileDeleteStoryContext, bool>
    {
        private readonly IRepositoryRui _repository;
        private readonly IFileProviderRui _fileProvider;
        private readonly ISessionNHibernateFactory _factory;

        public FileDeleteStory(
            IRepositoryRui repository,
            IFileProviderRui fileProvider,
            ISessionNHibernateFactory factory)
        {
            _repository = repository;
            _fileProvider = fileProvider;
            _factory = factory;
        }

        public bool Execute(FileDeleteStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(FileDeleteStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            var entity = _repository.Get<Files>(context.Id.Value);

            if (entity != null)
            {
                using (var session = _factory.OpenSession())
                {
                    _fileProvider.DeleteFile(context.Id.Value);
                    await session.DeleteAsync<Files>(context.Id.Value);
                    return true;
                }
            }

            return false;
        }
    }
}
