﻿namespace GB.Data.Directories
{
    /// <summary>
    /// ОКВЭД
    /// </summary>
    public class OKVED : EntityBase, IEntityName, IEntityCodeName
    {
        /// <summary>Код</summary>
        public virtual string Code { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Средняя зарплата</summary>
        public virtual decimal Salary { get; set; }

        /// <summary>Код и наименование</summary>
        public virtual string CodeName { get { return EntityHelper.GetCodeName(this); } }
    }
}