﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="GB.MKRF.Web.Selectors.Users" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript">
        function RowSelect() {
            Grid.GetSelectedFieldValues('Id;FullName', OnGetSelectedFieldValues);
        }

        function OnGetSelectedFieldValues(selectedValues) {
            var idList = null;
            var textList = '';

            for (var i = 0; i < selectedValues.length; i++) {

                idList = idList + selectedValues[i][0] + ';';
                textList = textList + selectedValues[i][1] + '<br />';
            }

            window.parent.PopupClose(idList, textList);
        }

        var gridLoaded = false;
        function SelectRows(s) {
            if (gridLoaded)
                return;

            var ids = window.parent.GetIds();
            if (ids) {
                s.SelectRowsByKey(ids.split(';'));
            }

            gridLoaded = true;
        }


    </script>
    <dxwgv:ASPxGridView ID="SelectorsUsersGrid" runat="server" ClientInstanceName="Grid"
        Width="100%" AutoGenerateColumns="False" KeyFieldName="Id" 
        OnBeforePerformDataSelect="SelectorsUsersGrid_BeforePerformDataSelect">
        <ClientSideEvents Init="function(s,e){ SelectRows(s); }" />
        <Columns>
            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Caption=" " />
            <dxwgv:GridViewDataMemoColumn Caption="ФИО" FieldName="FullName" SortOrder="Ascending" />
            <dxwgv:GridViewDataMemoColumn Caption="ОГВ" FieldName="Department.SName" />
            <dxwgv:GridViewDataMemoColumn Caption="Роль" FieldName="Role.Name" />
        </Columns>
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="17" />
    </dxwgv:ASPxGridView>

    <a href="javascript:RowSelect()" class="command" style="padding-top: 20px; display: inline-block;">Выбрать</a>
</asp:Content>
