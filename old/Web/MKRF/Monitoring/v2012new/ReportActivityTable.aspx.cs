﻿using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Web.Monitoring.v2012new
{
    public partial class ReportActivityTable : System.Web.UI.Page
    {
        public const string PLAN_ID = "planId";

        Plan currentPlan;

        Plan CurrentPlan
        {
            get
            {
                if (currentPlan == null)
                {
                    currentPlan = Repository.PlanRepository.Get(Request.QueryString[PLAN_ID]);
                }

                return currentPlan;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            var colums = Request.QueryString["col"].Split(';');

            foreach (var c in colums)
            {
                int i = int.Parse(c);

                Grid.Columns[i].Visible = true;
            }

            Grid.Columns["VolY0"].Caption = string.Format("на {0} г.", CurrentPlan.Year.Value);
            Grid.Columns["VolY1"].Caption = string.Format("на {0} г.", CurrentPlan.Year.Value + 1);
            Grid.Columns["VolY2"].Caption = string.Format("на {0} г.", CurrentPlan.Year.Value + 2);

            Grid.Columns["AddVolY0"].Caption = string.Format("на {0} г.", CurrentPlan.Year.Value);
            Grid.Columns["AddVolY1"].Caption = string.Format("на {0} г.", CurrentPlan.Year.Value + 1);
            Grid.Columns["AddVolY2"].Caption = string.Format("на {0} г.", CurrentPlan.Year.Value + 2);

            Grid.Columns["FactVolY0"].Caption = string.Format("на {0} г.", CurrentPlan.Year.Value);
            Grid.Columns["FactVolY1"].Caption = string.Format("на {0} г.", CurrentPlan.Year.Value - 1);
            Grid.Columns["FactVolY2"].Caption = string.Format("на {0} г.", CurrentPlan.Year.Value - 2);

            Grid.DataBind();

            base.OnPreRender(e);
        }

        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var grid = sender as ASPxGridView;

            grid.DataSource = CurrentPlan.Activities.Select(x => new
            {
                IsNotPriority = (x.IsPriority ? 0 : 1),
                Code = x.Code,
                Name = x.Name,
                Type = x.ActivityType2.Name,
                IsIkt = x.IKTComponent == null ? null : x.IKTComponent.CodeName,
                NameIsIkt = (x.IKTObject == null) ? null : x.IKTObject.Name,
                Annotation = x.AnnotationData,
                Source = x.SourceFunding == null ? null : x.SourceFunding.Name,
                Responsible = x.Responsible == null ? null : x.Responsible.Name,
                Goals = GetGoals(x.PlansActivityGoals),

                GRBS = (x.GRBS != null) ? x.GRBS.CodeName : String.Empty,
                SectionKBK = (x.SectionKBK != null) ? x.SectionKBK.CodeName : String.Empty, // "Код по БК ПЗ/РЗ"
                ExpenseItem = (x.ExpenseItem != null) ? x.ExpenseItem.CodeName : String.Empty, // "Код по БК ЦС"
                WorkForm = (x.WorkForm != null) ? x.WorkForm.CodeName : String.Empty, // "Код по БК ВР"
//                ExpenditureItem = (x.ExpenditureItem != null) ? x.ExpenditureItem.CodeName : String.Empty, // "Код по БК КОСГУ"

                Contract = GetContracts(x.PlansStateContracts),

                VolY0 = x.PlansActivityVolY0,
                VolY1 = x.PlansActivityVolY1,
                VolY2 = x.PlansActivityVolY2,

                AddVolY0 = x.PlansActivityAddVolY0,
                AddVolY1 = x.PlansActivityAddVolY1,
                AddVolY2 = x.PlansActivityAddVolY2,

                FactDate = x.PlansActivityFactDate.HasValue ? x.PlansActivityFactDate.ToString() : string.Empty,
                FactVolY0 = x.PlansActivityFactVolY0,
                FactVolY1 = x.PlansActivityFactVolY1,
                FactVolY2 = x.PlansActivityFactVolY2,
            }).ToList();
        }

        private string GetGoals(IList<PlansActivityGoal> list)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var g in list)
            {
                sb.Append(g.Name);
                sb.Append(';');
                sb.AppendLine();
            }

            return sb.ToString();
        }

        private object GetContracts(IList<PlansStateContract> list)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var c in list)
            {
                if (c.Contract != null)
                {
                    sb.AppendFormat("{0} от {1} ({2})", c.Contract.Number, c.Contract.Sign_Date.ToShortDateString(), c.Contract.Products != null ? string.Join("; ", c.Contract.Products.Select(i => i.Name.Trim()).ToArray()) : string.Empty);
                    sb.AppendLine();
                }
            }

            return sb.ToString();
        }

        protected void ExportButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions()
            {
                SheetName = "Отчет по плану",
                TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value,
            }; 
            
            GridViewExporter.WriteXlsToResponse("Отчет по плану", options);
        }
    }
}