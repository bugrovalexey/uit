﻿using GB.Data.Directories;

namespace GB.Data.AccountingObjects.WorksAndGoods
{
    /// <summary>
    /// Программное обеспечение ОУ
    /// </summary>
    public class Software : EntityBase
    {
        /// <summary>
        /// Вид обеспечения
        /// </summary>
        public static readonly CategoryTypeOfSupportEnum Type = CategoryTypeOfSupportEnum.Software;    

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Классификационная категория
        /// </summary>
        public virtual CategoryKindOfSupport CategoryKindOfSupport { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public virtual decimal Summa { get; set; }

        /// <summary>
        /// Объект учета
        /// </summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Производитель
        /// </summary>
        public virtual Manufacturer Manufacturer { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public virtual int Amount { get; set; }

        /// <summary>
        /// Права на ПО
        /// </summary>
        public virtual RightsOnSoftwareEnum Right { get; set; }

        /// <summary>
        /// Характеристики лицензи
        /// </summary>
        public virtual CharacteristicOfLicense CharacteristicsOfLicense { get; set; }

    }
}