﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    internal class ExpenseTypeMap : ClassMapHiLo<ExpenseType>
    {
        public ExpenseTypeMap()
            : base("Expense_Type")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Expense_Type_Name");

            References(x => x.ExpenseDirection, "Expense_Direction_ID");
        }
    }
}