﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;

namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects.WorksAndGoods
{
    internal class WorkAndServiceMap : ClassMapHiLo<WorkAndService>
    {
        public WorkAndServiceMap()
            : base("OU_Work_And_Service")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Work_And_Service_Name").Not.Nullable();
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
            References(x => x.CategoryKindOfSupport, "VO_Category_ID").Not.Nullable();
            Map(x => x.InformationLeasedInfrastructure, "OU_Work_And_Service_Data");
            References(x => x.Okved, "OKVED_ID");
            Map(x => x.Summa, "OU_Work_And_Service_Sum").Not.Nullable();
        }
    }
}