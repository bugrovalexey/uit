﻿namespace bgTeam.IsRui.Common.Services.Impl
{
    using System;

    public class CacheValue<T>
    {
        public CacheValue()
        {
        }

        public CacheValue(T value, TimeSpan timeToExpired)
        {
            Value = value;
            ExpiredDateUtc = DateTime.UtcNow.Add(timeToExpired);
        }

        public T Value { get; set; }

        public DateTime ExpiredDateUtc { get; set; }

        public bool IsExpire => ExpiredDateUtc <= DateTime.UtcNow;

        public void Refresh(TimeSpan newExpiredTime)
        {
            ExpiredDateUtc = DateTime.UtcNow.Add(newExpiredTime);
        }
    }
}
