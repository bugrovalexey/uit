﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System.Collections.Generic;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Directories;

    public class TechnicalSupportDto
    {
        public int? Id { get; set; }
        /// <summary>
        /// Вид обеспечения
        /// </summary>
        public static readonly CategoryTypeOfSupportEnum Type = CategoryTypeOfSupportEnum.TechnicalSupport;

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Классификационная категория
        /// </summary>
        public string CategoryKindOfSupport { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public decimal Summa { get; set; }

        /// <summary>
        /// Объект учета
        /// </summary>
        public int AccountingObject { get; set; }

        /// <summary>
        /// Производитель
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// Характеристики
        /// </summary>
        public IList<CharacteristicOfTechnicalSupportGetDto> Characteristics { get; set; }

        public string ManufacturerCustomName { get; set; }
    }
}
