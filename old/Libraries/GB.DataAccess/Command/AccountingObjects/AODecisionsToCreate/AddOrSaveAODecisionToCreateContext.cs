﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AODecisionsToCreate
{
    public class AddOrSaveAODecisionToCreateContext : ICommandContext
    {        
        public int AccountingObjectId { get; set; }
        
        public string Name { get; set; }
        
        public string Number { get; set; }
        
        public DateTime? DateAdoption { get; set; }
        
        public string ParticleOfDocument { get; set; }
        
        public string Url { get; set; }
        
        public string Files { get; set; }

        public int? xId { get; set; }
    }
}
