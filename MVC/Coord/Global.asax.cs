﻿using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.FileSystem;
using bgTeam.IsRui.MKRF.Logic.AccountingObjectActions;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using bgTeam.IsRui.MKRF.Logic.Status;
using bgTeam.IsRui.MKRF.Logic.Tabs;
using bgTeam.IsRui.MKRF.Providers;
using bgTeam.IsRui.WebApp;
using bgTeam.IsRui.WebApp.Infrastructure;
using bgTeam.IsRui.WebApp.Infrastructure.Installers;
using Castle.Windsor;
using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace GB.Coord
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode,
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {

        private static IWindsorContainer container;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        protected void Application_Start()
        {
            //RouteTable.Routes.MapConnection<bgTeam.IsRui.WebApp.SignalRAccount.UserAccountHub>("chatservice", "realtime/chat");

            //RouteTable.Routes.MapConnection<bgTeam.IsRui.WebApp.SignalRAccount.UserAccountHub>(
            //                                                                            name: "chatservice",
            //                                                                            url: "realtime/chat",
            //                                                                            configuration: new Microsoft.AspNet.SignalR.ConnectionConfiguration
            //                                                                            {
            //                                                                                EnableCrossDomain = true
            //                                                                            }
            //                                                                       );            

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            BootstrapContainer();
            
            Logger.Info("Начало работы приложения");

            new NHSession("Coord");

            FileProvider.Init();

            var act = container.Resolve<IAccessControllerProvider>();

            new WorkflowRegistration().Register();
            new TabsRegistration(act).Register();
            new MasterEditRegistration().Register();
            new ManagerActionsRegistration().Register();

            bgTeam.IsRui.WebApp.AutoMapperConfiguration.Configure();           
        }        

        private static void BootstrapContainer()
        {
            container = new WindsorContainer().Install(
                new WindsorInstaller(),
                new ManagersInstaller(),
                new RepositoryInstaller(),
                new ControllersInstaller(), 
                new CommandInstaller());

            var controllerFactory = new WindsorControllerFactory(container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        protected void Application_End()
        {
            container.Dispose();
            Logger.Info("Окончание работы приложения");
        }

        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    NHSession.BeginRequest();
        //}

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            NHSession.EndRequest();
        }

        //protected void Application_Error(object sender, EventArgs e)
        //{
        //    //System.Web.HttpContext context = HttpContext.Current;
        //    System.Exception exp = Context.Server.GetLastError();

        //    if (exp != null)
        //    {
        //        Logger.Fatal(exp);

        //        var cookieValues = new Dictionary<string, string>()
        //        {
        //            {"em", exp.Message},
        //            {"im", exp.InnerException != null ? exp.InnerException.Message : "InnerException is null"}
        //        };

        //        CookieHelper.SaveCookie("ApfError", cookieValues, DateTime.Now.AddMinutes(20));
        //    }
        //}
    }
}