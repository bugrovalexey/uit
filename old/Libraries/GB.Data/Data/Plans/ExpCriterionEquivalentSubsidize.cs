﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
	/// <summary>Соответствие критериям</summary>
	public class ExpCriterionEquivalentSubsidize : EntityBase // ExpCriterionEquivalentBase
	{
        protected ExpCriterionEquivalentSubsidize() { }
        

        /// <summary>Акт экспертного заключения</summary>
        public virtual ExpConclSubsidizeAct ExpConclSubsidizeAct { get; set; }

        /// <summary>Критерий</summary>
        public virtual ExpCriterion ExpCriterion { get; set; }

        /// <summary>Соответствие</summary>
        public virtual ExpConformity ExpConformity { get; set; }

        /// <summary>Комментарий</summary>
        public virtual string Comm { get; set; }//null

        ///// <summary>Акт экспертного заключения</summary>
        //public virtual new ExpConclAct ExpConclAct
        //{
        //    get { return (ExpConclAct)base.ExpConclAct; }
        //    protected set { base.ExpConclAct = value; }
        //}
	}
}