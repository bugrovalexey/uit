﻿using System.Collections.Generic;

namespace bgTeam.IsRui.Domain.Entities.Admin
{
    /// <summary></summary>
    public class MetaDirectory : EntityBase
    {
        public MetaDirectory()
        {
            Columns = new List<MetaDirectoryColumn>();
        }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Наименование view справочника</summary>
        public virtual string ViewName { get; set; }

        /// <summary>Порядок сортировки</summary>
        public virtual int OrderIndex { get; set; }

        /// <summary>Наименование процедуры вставки</summary>
        public virtual string ProcInsert { get; set; }

        /// <summary>Наименование процедуры обновления</summary>
        public virtual string ProcUpdate { get; set; }

        /// <summary>Наименование процедуры удаления</summary>
        public virtual string ProcDelete { get; set; }

        /// <summary>Определяет разрешение на удаление записи</summary>
        public virtual bool InUse { get; set; }

        /// <summary>Отображать справочник</summary>
        public virtual bool Visible { get; set; }

        /// <summary>Только для чтения</summary>
        public virtual bool Readonly { get; set; }

        /// <summary>Столбцы справочника</summary>
        public virtual IList<MetaDirectoryColumn> Columns { get; protected set; }
    }
}