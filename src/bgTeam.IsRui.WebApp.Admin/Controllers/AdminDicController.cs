﻿namespace bgTeam.IsRui.WebApp.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using bgTeam.IsRui.WebApp.Admin.Domain;
    using bgTeam.IsRui.WebApp.Admin.Models;
    using bgTeam.DataAccess;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Authorization;

    [Route("admin/[controller]/[action]")]
    //[Authorize(Roles = "administrator")]
    public class AdminDicController : Controller
    {
        private readonly IAppLogger _logger;
        private readonly IRepository _repository;
        private readonly ICrudService _crudService;
        private readonly IStoryBuilder _storyBuilder;
        private readonly IBuilderQuery _builderQuery;

        public AdminDicController(
            IAppLogger logger,
            IRepository repository,
            ICrudService crudService,
            IStoryBuilder storyBuilder,
            IBuilderQuery builderQuery)
        {
            _logger = logger;
            _repository = repository;
            _crudService = crudService;
            _storyBuilder = storyBuilder;
            _builderQuery = builderQuery;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return RedirectToAction("Editor");
        }

        [HttpGet]
        public async Task<IActionResult> Editor(int? dictId = null)
        {
            var dict = await _repository.GetAllAsync<MetaDictionary>(x => x.Visible);

            return View(new EditorViewModel(dict.OrderBy(x => x.OrderBy), dictId));
        }

        [HttpGet]
        public async Task<IActionResult> EditorTable(int dictId)
        {
            var dict = await _repository.GetAsync<MetaDictionary>(x => x.Id == dictId);
            var columns = await _repository.GetAllAsync<MetaColumn>(x => x.DictionaryId == dictId);

            var data = await _repository.GetAllAsync<dynamic>(await _builderQuery.BuildAsync(dict, columns));

            return View(new EditorTableViewModel(dict, columns.OrderBy(x => x.OrderBy), data));
        }

        [HttpGet]
        public async Task<IActionResult> EditorCardEdit(int dictId, int? itemId = null)
        {
            var dict = await _repository.GetAsync<MetaDictionary>(x => x.Id == dictId);
            var columns = await _repository.GetAllAsync<MetaColumn>(x => x.DictionaryId == dictId);

            if (itemId.HasValue)
            {
                var data = await _repository.GetAsync<dynamic>(await _builderQuery.BuildItemAsync(dict, columns, itemId.Value));
                return View(new EditorCardEditViewModel(dict, columns.OrderBy(x => x.OrderBy), data));
            }
            else
            {
                return View(new EditorCardEditViewModel(dict, columns.OrderBy(x => x.OrderBy)));
            }
        }

        [HttpPost]
        public async Task<IActionResult> EditorCardEdit(IFormCollection form, int dictId, int? itemId = null)
        {
            var dict = await _repository.GetAsync<MetaDictionary>(x => x.Id == dictId);
            var columns = await _repository.GetAllAsync<MetaColumn>(x => x.DictionaryId == dictId);

            if (dict.ReadOnly)
            {
                throw new InvalidOperationException("Данный справочник нельзя редактировать");
            }

            var values = new Dapper.DynamicParameters();

            //обработка значений
            foreach (var item in columns)
            {
                if (item.Name == AppSettings.PRIMARY_KEY && !itemId.HasValue)
                {
                    var colPK = columns.GetAndCheckColumnPK(dict);
                    var id = await _repository.GetAsync<int>(_builderQuery.BuildGetPrimaryKey(dict, colPK));
                    values.Add(item.ColumnName, id);
                    continue;
                }

                switch (item.Type)
                {
                    //case MetaColumnTypeEnum.TString:
                    //    break;
                    //case MetaColumnTypeEnum.TBigString:
                    //    break;
                    case MetaColumnTypeEnum.TInt:
                    case MetaColumnTypeEnum.TDecimal:
                        var str = form[item.ColumnName].FirstOrDefault();
                        values.Add(item.ColumnName, str == string.Empty ? null : str);
                        break;
                    //case MetaColumnTypeEnum.TDateTime:
                    //    break;
                    //case MetaColumnTypeEnum.TChild:
                    //    break
                    //case MetaColumnTypeEnum.TRef:
                    //    if (form[item.ColumnName].Equals(int.MinValue))
                    //    {
                    //        values.Add(item.ColumnName, null);
                    //    }
                    //    break;
                    case MetaColumnTypeEnum.TBool:
                        if (form[item.ColumnName].Equals("on"))
                        {
                            values.Add(item.ColumnName, "1");
                        }
                        else
                        {
                            values.Add(item.ColumnName, "0");
                        }
                        break;

                    default:
                        values.Add(item.ColumnName, form[item.ColumnName]);
                        break;
                }
            }

            await _crudService.ExecuteAsync(itemId.HasValue ? _builderQuery.BuildUpdate(dict, columns) : _builderQuery.BuildInsert(dict, columns), values);

            return RedirectToAction("Editor", new { dictId });
        }

        [HttpGet]
        public async Task<IActionResult> EditorCardEditRefField(int dictId, int fieldNameId, string column)
        {
            var dict = await _repository.GetAsync<MetaDictionary>(x => x.Id == dictId);
            var columns = await _repository.GetAllAsync<MetaColumn>(x => x.DictionaryId == dictId);

            var colPK = columns.GetAndCheckColumnPK(dict);
            var colRef = columns.First(x => x.Id == fieldNameId); //await _repository.GetAsync<MetaColumn>(x => x.Id == fieldNameId);

            var fieldValue = colPK.ColumnName;
            var fieldName = colRef.ColumnName;

            var data = await _repository.GetAllAsync<dynamic>(_builderQuery.BuildSelectList(dict, fieldValue, fieldName));

            return View(new EditorCardEditRefFieldModelView(column, fieldValue, fieldName, data));
        }

        [HttpPost]
        public async Task<JsonResult> EditorItemDelete(int dictId, int itemId)
        {
            var dict = await _repository.GetAsync<MetaDictionary>(x => x.Id == dictId);
            var columns = await _repository.GetAllAsync<MetaColumn>(x => x.DictionaryId == dictId);

            var colPK = columns.GetAndCheckColumnPK(dict);

            if (dict.ReadOnly)
            {
                throw new InvalidOperationException("Данный справочник нельзя редактировать");
            }

            var sql = _builderQuery.BuildDelete(dict, colPK);

            var res = await _crudService.ExecuteAsync(sql, new { Id = itemId });

            //var res = CommandBuilder
            //    .Build(new ExecuteSqlContext()
            //    {
            //        SQL = _builderQuery.BuildDeleteWithDelete(dict, itemId)
            //    })
            //    .Return<int>();

            if (res == 1)
            {
                return Json(new
                {
                    result = true,
                    id = itemId,
                    message = "Запись успешно удалена"
                });
            }

            _logger.Error($"EditorItemDelete: delete row - {res}");

            return Json(new
            {
                result = false,
                message = "При удалении возникла ошибка"
            });
            //}
        }
    }
}
