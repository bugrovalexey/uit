﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class OKDPToYearMap : ClassMapBase<OKDPToYear>
    {
		public OKDPToYearMap()
            : base("OKDP_Years", "v_", true)
        {
            //Table("v_");

            //Id(x => x.Id, "OKDP_ID").GeneratedBy.Custom(typeof(IdGenerator));

			
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "OKDP_Code");
			Map(x => x.CodeName, "OKDP_CodeName");
            Map(x => x.Year, "Year");
        }
    }
}
