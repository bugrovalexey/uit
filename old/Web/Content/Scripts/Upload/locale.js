﻿/*
 * jQuery File Upload Plugin Localization Example 6.5
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2012, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

window.locale = {
    "fileupload": {
        "errors": {
            "maxFileSize": "Файл слишком большой",
            "minFileSize": "Файл слишком маленький",
            "acceptFileTypes": "Тип файлов не поддерживается",
            "maxNumberOfFiles": "Достигнуто максимальное число файлов",
            "uploadedBytes": "Загруженные файлы превышают максимальный размер",
            "emptyResult": "Ошибка загрузки"
        },
        "error": "Ошибка",
        "start": "Загрузить",
        "cancel": "Отменить",
        "destroy": "Удалить"
    }
};
