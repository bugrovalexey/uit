﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimGetConclusionHistoryStory : IStory<ClaimGetConclusionHistoryStoryContext, IEnumerable<ConclusionHistoryDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public ClaimGetConclusionHistoryStory(IRepositoryRui repository, IMapperBase mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<ConclusionHistoryDto> Execute(ClaimGetConclusionHistoryStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ConclusionHistoryDto>> ExecuteAsync(ClaimGetConclusionHistoryStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var entity = _repository.Get<Claim>(context.ClaimId);

            if (entity == null)
            {
                throw new IsRuiException($"Не найдена заявка: {context.ClaimId}");
            }

            var workflow = _repository.Get<Workflow>(q => q.Where(x => x.EntityOwner == entity));

            var conclusion = _repository.GetAllEx<WorkflowUserConclusionHistory>(
                q => q.Where(x => x.WorkflowId == workflow.Id))
                .Select(x => _mapper.Map(x, new ConclusionHistoryDto()));

            return conclusion;
        }
    }
}
