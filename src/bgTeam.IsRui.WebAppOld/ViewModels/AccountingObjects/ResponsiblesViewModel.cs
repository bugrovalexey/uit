﻿using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class ResponsiblesViewModel : AccountingObjectBase
    {
        #region consts

        private const string DepartmentCaption = "Учреждение, уполномоченное на создание, развитие ОУ";

        public const string OrgNameCaption = "Наименование организации";
        public const string FioResponsibleCaption = "ФИО ответственного";
        public const string OrgAddressCaption = "Адрес";
        public const string OrgPhoneCaption = "Телефон";

        private const string ResponsibleCoordCaption = "Ответственный за координацию мероприятий по информатизации";
        public const string ResponsibleExploitationCaption = "Ответственный за обеспечение эксплуатации объекта учета";
        private const string ResponsiblePurchaseCaption = "Ответственный за организацию закупок";
        private const string ResponsibleInformationCaption = "Ответственный за размещение сведений об объекте учета";

        #endregion consts

        public ResponsiblesViewModel(int id)
            : base(id, x => x.ResponsibleCoord, x => x.ResponsibleExploitation, x => x.ResponsiblePurchase, x => x.ResponsibleInformation)
        {
            DepartmentList = new SelectList(new RepositoryDc<Department>().GetAllEx(q => q.OrderBy(x => x.Code)), "Id", "Name");

            ResponsibleCoord =
                Current.ResponsibleCoord.Return(
                    x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.Name } }, default(List<IEntityName>));
            ResponsibleExploitation =
                Current.ResponsibleExploitation.Return(
                    x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.Name } }, default(List<IEntityName>));
            ResponsiblePurchase =
                Current.ResponsiblePurchase.Return(
                    x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.Name } }, default(List<IEntityName>));
            ResponsibleInformation =
                Current.ResponsibleInformation.Return(
                    x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.Name } }, default(List<IEntityName>));
        }

        public SelectList DepartmentList { get; set; }

        [DisplayName(DepartmentCaption)]
        public int? DepartmentId { get { return Current.ResponsibleDepartment.Return(dep => dep.Id, default(int)); } }

        #region Раздел "Учреждение или иное юридическое лицо, уполномоченное на эксплуатацию"

        [DisplayName(OrgNameCaption)]
        public string ExploitationOrgName { get { return Current.ExploitationDepartment.Return(dep => dep.Name, default(string)); } }

        [DisplayName(FioResponsibleCaption)]
        public string ExploitationFioResponsible { get { return Current.ExploitationDepartment.Return(dep => dep.FioResponsible, default(string)); } }

        [DisplayName(OrgAddressCaption)]
        public string ExploitationOrgAddress { get { return Current.ExploitationDepartment.Return(dep => dep.Address, default(string)); } }

        [DisplayName(OrgPhoneCaption)]
        public string ExploitationOrgPhone { get { return Current.ExploitationDepartment.Return(dep => dep.Phone, default(string)); } }

        #endregion Раздел "Учреждение или иное юридическое лицо, уполномоченное на эксплуатацию"

        #region Ответственные

        [DisplayName(ResponsibleCoordCaption)]
        public IList<IEntityName> ResponsibleCoord { get; private set; }

        [DisplayName(ResponsibleExploitationCaption)]
        public IList<IEntityName> ResponsibleExploitation { get; private set; }

        [DisplayName(ResponsiblePurchaseCaption)]
        public IList<IEntityName> ResponsiblePurchase { get; private set; }

        [DisplayName(ResponsibleInformationCaption)]
        public IList<IEntityName> ResponsibleInformation { get; private set; }

        #endregion Ответственные

        #region Раздел "Сведения о фактическом месторасположении объекта учета"

        [DisplayName(OrgNameCaption)]
        public string LocationOrgName { get { return Current.LocationOrgName; } }

        [DisplayName(FioResponsibleCaption)]
        public string LocationFioResponsible { get { return Current.LocationFioResponsible; } }

        [DisplayName(OrgAddressCaption)]
        public string LocationOrgAddress { get { return Current.LocationOrgAddress; } }

        [DisplayName(OrgPhoneCaption)]
        public string LocationOrgPhone { get { return Current.LocationOrgPhone; } }

        #endregion Раздел "Сведения о фактическом месторасположении объекта учета"
    }
}