﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities.Goods
{
    public class GoodsViewModel : ActivitiesCardBase
    {
        public const string EditActionLink = "GoodsEdit";

        public const string NameCaption = "Наименование";
        public const string OKPDCaption = "Код ОКПД";
        public const string CategoryCaption = "Группа товаров";
        public const string ExpenseDirectionCaption = "Вид затрат";
        public const string ExpenseTypeCaption = "Тип затрат";
        public const string KOSGUCaption = "КОСГУ";
        public const string YearCaption = "Год";
        public const string QuantityCaption = "Количество";
        public const string CostCaption = "Цена за единицу, руб.";
        public const string DurationCaption = "Срок поставки, дней";
        public const string SummCaption = "Сумма расходов, руб";

        public IList<TableGoods> Items { get; private set; }

        public GoodsViewModel(IStatusRepository statusRepository, int ownerId)
            : base(statusRepository, ownerId)
        {
            var controller = new PlansSpecService();
            Items = controller.Repository.GetAllEx(q => q
                    .Fetch(x => x.OKPD).Eager
                    .Fetch(x => x.ProductGroup).Eager
                    .Fetch(x => x.ExpenseDirection).Eager
                    .Fetch(x => x.ExpenseType).Eager
                    .Fetch(x => x.ExpenditureItem).Eager
                    .Where(x => x.PlanActivity == CurrentActivity))
                .Select(w => new TableGoods(w))
                .ToList();
        }
    }

    public class TableGoods : NotificationBaseViewModel
    {
        public int Id { get; set; }

        [DisplayName(GoodsViewModel.NameCaption)]
        public string Name { get; set; }

        [DisplayName(GoodsViewModel.QuantityCaption)]
        public int Quantity { get; set; }

        [DisplayName(GoodsViewModel.DurationCaption)]
        public int? Duration { get; set; }

        [DisplayName(GoodsViewModel.CostCaption)]
        public decimal Cost { get; set; }

        [DisplayName(GoodsViewModel.YearCaption)]
        public string YearDescription { get; set; } // Description = YearEnumExtension.GetDescription

        [DisplayName(GoodsViewModel.OKPDCaption)]
        public string OKPDName { get; set; }

        [DisplayName(GoodsViewModel.CategoryCaption)]
        public string ProductGroupName { get; set; }

        [DisplayName(GoodsViewModel.ExpenseDirectionCaption)]
        public string ExpenseDirectionName { get; set; }

        [DisplayName(GoodsViewModel.ExpenseTypeCaption)]
        public string ExpenseTypeName { get; set; }

        [DisplayName(GoodsViewModel.KOSGUCaption)]
        public string KOSGUCodeName { get; set; }

        [DisplayName(GoodsViewModel.SummCaption)]
        public decimal Summ
        {
            get { return Cost * Quantity; }
        }

        //Служебные поля
        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("GoodsDelete", "Activities", new { id = Id }, "GridGoods"); }
        }

        public TableGoods(PlansSpec goods)
        {
            Mapper.Map(goods, this);
        }
    }

    [MetadataType(typeof(ProductEditViewModel_Validation))]
    public class GoodsEditViewModel : ActivitiesCardBase
    {
        public int GoodsId { get; set; }

        [DisplayName(GoodsViewModel.NameCaption)]
        public string Name { get; set; }

        #region OKPD

        public SelectList OKPDList { get; private set; }

        [DisplayName(GoodsViewModel.OKPDCaption)]
        public int OKPDId { get; set; }

        #endregion OKPD

        #region ProductGroup

        public SelectList CategoryList { get; private set; }

        [DisplayName(GoodsViewModel.CategoryCaption)]
        public int ProductGroupId { get; set; }

        #endregion ProductGroup

        #region ExpenseDirection

        public SelectList ExpenseDirectionList { get; private set; }

        [DisplayName(GoodsViewModel.ExpenseDirectionCaption)]
        public int ExpenseDirectionId { get; set; }

        #endregion ExpenseDirection

        #region ExpenseType

        public SelectList ExpenseTypeList { get; private set; }

        [DisplayName(GoodsViewModel.ExpenseTypeCaption)]
        public int ExpenseTypeId { get; set; }

        #endregion ExpenseType

        #region KOSGU

        public SelectList KOSGUList { get; private set; }

        [DisplayName(GoodsViewModel.KOSGUCaption)]
        public int KOSGUId { get; set; }

        #endregion KOSGU

        [DisplayName(GoodsViewModel.YearCaption)]
        public YearEnum Year { get; set; }

        public SelectList YearList { get; private set; }

        [DisplayName(GoodsViewModel.QuantityCaption)]
        public int Quantity { get; set; }

        [DisplayName(GoodsViewModel.DurationCaption)]
        public int? Duration { get; set; }

        [DisplayName(GoodsViewModel.CostCaption)]
        public decimal Cost { get; set; }

        [DisplayName(GoodsViewModel.SummCaption)]
        public decimal Summ
        {
            get { return Cost * Quantity; }
        }

        public IList<TableCharacteristic> Characteristics { get; private set; }

        public GoodsEditViewModel(IStatusRepository statusRepository, int activityId, int? id)
            : base(statusRepository, activityId)
        {
            if (id.HasValue)
            {
                var product = new RepositoryDc<PlansSpec>().GetExisting(id);
                Mapper.Map(product, this);
                Characteristics = new PlansSpecCharacteristicsService().Repository
                    .GetAllEx(q => q
                        .Fetch(x => x.Unit).Eager
                        .Where(x => x.Goods == product))
                    .Select(x => new TableCharacteristic(x))
                    .ToList();
            }
            else
            {
                Characteristics = new List<TableCharacteristic>();
            }

            YearList = new SelectList(
                YearRepository.YearList,
                "Key", "Value", Year.ToString());

            OKPDList = new SelectList(
                DirectoriesRepositary.GetDirectories<OKPD>(DateTime.Now.Year).OrderBy(x => x.Code),
                "Id", "CodeName", OKPDId);

            CategoryList = new SelectList(
                 new RepositoryDc<ProductGroup>().GetAll(),
                 "Id", "Name", ProductGroupId);

            var date = DateTime.Now;

            ExpenseDirectionList = new SelectList(
                new RepositoryDc<ExpenseDirection>().GetAllEx(
                    q =>
                        q.Where(
                            x =>
                                (x.Type == ExpenseDirectionTypeEnum.Goods && x.DateStart <= date && date <= x.DateEnd) ||
                                x.Id == EntityBase.Default)).OrderBy(x => x.Code),
                "Id", "Name", ExpenseDirectionId);

            ExpenseTypeList = new SelectList(
                new RepositoryDc<ExpenseType>().GetAllEx(q => q.Where(e =>
                    (e.ExpenseDirection != null && e.ExpenseDirection.Id == ExpenseDirectionId) ||
                    e.Id == EntityBase.Default)),
                "Id", "Name", ExpenseTypeId);

            KOSGUList = new SelectList(
                DirectoriesRepositary.GetDirectories<ExpenditureItem>(DateTime.Now.Year).OrderBy(x => x.Code),
                "Id", "CodeName", KOSGUId);
        }

    }

    public class ProductEditViewModel_Validation : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        public int ExpenseDirectionId { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int Quantity { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int? Duration { get; set; }

        [Range(MoneyMin, MoneyMax, ErrorMessage = RangeMoneyText)]
        public double Cost { get; set; }
    }
}