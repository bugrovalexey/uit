﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ActivityApfInfoMap : ClassMapBase<ActivityApfInfo>
    {
        public ActivityApfInfoMap()
            : base("Plans_Activity_APF_Info")
        {
//            Table("w_");

//            Id(x => x.Id, "Plans_Activity_APF_Info_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string args = @"
//						 @Plans_Activity_APF_Comment = ?
//                        ,@Plans_Activity_ID = ?
//                        ,@APF_Info_ID = ?
//                        ,@Plans_Activity_APF_Info_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Plans_Activity_APF_Info_Insert" + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Plans_Activity_APF_Info_Update" + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Plans_Activity_APF_Info_Delete
//						  @Plans_Activity_APF_Info_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Comment, "Plans_Activity_APF_Comment");

            References<PlansActivity>(x => x.PlansActivity, "Plans_Activity_ID");
            References<ApfInfo>(x => x.ApfInfo, "APF_Info_ID");
        }
    }
}
