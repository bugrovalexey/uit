﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Сведения об алгоритмах и (или) программах для ЭВМ , которые были разработаны 
    /// (доработаны) в процессе реализации мероприятий по информатизации и которые включены в ФАП
    /// </summary>
    /// 
    public class APFActivity: EntityBase
    {
        public APFActivity()
        {
        }

        public APFActivity(PlansActivity activity)
        {
            PlansActivity = activity;
        }

        public virtual PlansActivity PlansActivity { get; set; }

        public virtual ApfInfo ApfInfo { get; set; }
    }
}
