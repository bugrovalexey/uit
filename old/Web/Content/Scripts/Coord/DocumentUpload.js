﻿var fileFolder = '<%=Web.Helpers.UrlHelper.Resolve("~/TempFiles")%>/';

function fileUploaded(fileInfo) {

    var info = fileInfo.split('+');

    hf.Set('FilePath', info[0]);
    hf.Set('FileName', info[1]);
    hf.Set('ContentType', info[2]);

    showDocContainer(info[1]);
}

function showDocContainer(fileName) {
    var div = document.getElementById("docDiv");
    div.innerHTML = "<b>Файл " + fileName + " загружен</b>";
    div = document.getElementById("docLabel");
    div.innerHTML = "";
}