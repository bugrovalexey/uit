﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GoodsCharacteristicDto
    {
        public int? Id { get; set; }

        public int? GoodsId { get; set; }

        public string Name { get; set; }

        public int? Value { get; set; }

        public int? UnitId { get; set; }

        public string UnitName { get; set; }
    }
}
