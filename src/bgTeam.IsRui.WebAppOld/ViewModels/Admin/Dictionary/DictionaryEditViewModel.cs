﻿using bgTeam.IsRui.MKRF.DictyonaryEdit;
using System.Collections.Generic;
using System.Data;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin.Dictionary
{
    public class DictionaryEditViewModel : AdminViewModel
    {
        public Dictionary<int, MetaDictionaryColumn> Columns { get; set; }

        public DataRow Data { get; set; }

        public int DictId { get; set; }

        public string Title { get; set; }
    }
}