﻿using GB.Data.AccountingObjects;

namespace GB.MKRF.Services.Args
{
    public class InformationInteractionArgs : BaseArgsNew
    {
        /// <summary>
        /// Объект учета
        /// </summary>
        public AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Id объекта учета
        /// </summary>
        public int AccountingObjectId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// Id Типа
        /// </summary>
        public int? TypeId { get; set; }

        /// <summary>
        /// СМЭВ
        /// </summary>
        public string SMEV { get; set; }

        /// <summary>
        /// Заголовок
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// URL
        /// </summary>
        public string Url { get; set; }
    }
}