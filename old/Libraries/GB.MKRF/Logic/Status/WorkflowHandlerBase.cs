﻿using GB.Data;
using System;

namespace GB.MKRF.Logic.Status
{
    public abstract class WorkflowHandlerBase<T> : IWorkflowHandler where T : IWorkflowObject
    {
        protected abstract bool CheckNewStatus(T @object, Data.Workflow.Status status, out string errorMessage, out string confirmMessage);

        bool IWorkflowHandler.ReadyForNewStatus(IWorkflowObject @object, Data.Workflow.Status status, out string errorMessage, out string confirmMessage)
        {
            return CheckNewStatus((T)@object, status, out errorMessage, out confirmMessage);
        }
        
        public abstract void ChangeAfter(T obj);

        public void ChangeAfter(IWorkflowObject obj)
        {
            ChangeAfter((T)obj);
        }
    }
}