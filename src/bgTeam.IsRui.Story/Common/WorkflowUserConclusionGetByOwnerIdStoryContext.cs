﻿namespace bgTeam.IsRui.Story.Common
{
    public class WorkflowUserConclusionGetByOwnerIdStoryContext
    {
        public int? EntityType { get; set; }

        public int? EntityOwnerId { get; set; }

        public int? StatusId { get; set; }
    }
}