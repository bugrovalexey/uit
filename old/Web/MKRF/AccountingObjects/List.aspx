﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.Master" CodeBehind="List.aspx.cs" Inherits="GB.MKRF.Web.AccountingObjects.List" %>

<%@ MasterType VirtualPath="~/Main.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        ActiveMenuItem = 3;

        function GridSave(e) {
            hf.Set('ShortName', txtShortName.GetValue());
        }

    </script>

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />

    <div class="page">
        <div class="detailTop">
            <dxe:ASPxButton ID="addButton" runat="server" ClientInstanceName="addButton" AutoPostBack="false"
                Text="Добавить" Width="80px" CssClass="fleft topButton">
                <ClientSideEvents Click="function(s,e) { gridData.AddNewRow(); }" />
            </dxe:ASPxButton>
        </div>
        <dxwgv:ASPxGridView runat="server" ID="gridData" KeyFieldName="Id" Width="100%"
            ClientInstanceName="gridData" ViewStateMode="Disabled" AutoGenerateColumns="False"
            OnBeforePerformDataSelect="gridData_BeforePerformDataSelect"
            OnRowInserting="gridData_RowInserting"
            OnRowUpdating="gridData_RowUpdating"
            OnStartRowEditing="gridData_OnStartRowEditing"
            OnRowDeleting="gridData_OnRowDeleting">
            <Settings ShowFooter="True" ShowFilterRow="True" />
            <SettingsEditing Mode="EditForm" />
            <Columns>
                <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" AllowDragDrop="False" ButtonType="Image" Width="20px" >
                    <EditButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/edit_16.png" Image-ToolTip="Изменить" />
                    <ClearFilterButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/clear_16.png" Image-ToolTip="Очистить" />
                </dxwgv:GridViewCommandColumn>
                <dxwgv:GridViewDataTextColumn Caption="Уникальный идентификационный номер ОУ" FieldName="Number">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Краткое наименование объекта учета" FieldName="ShortName">
                    <DataItemTemplate>
                        <%#GetLinkToAccountingObject(Eval("Id"),Eval("ShortName")) %>
                    </DataItemTemplate>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Тип ОУ" FieldName="Type.Name" />
                <dxwgv:GridViewDataTextColumn Caption="Вид ОУ" FieldName="Kind.Name" />
                <dxwgv:GridViewDataTextColumn Caption="Статус" FieldName="Status.Name" />
                <dxwgv:GridViewDataTextColumn FieldName="Access" Visible="False" />
                <dxwgv:GridViewCommandColumn Caption=" " Name="delColumn" VisibleIndex="99" AllowDragDrop="False" ButtonType="Image" Width="20px">
                    <DeleteButton Visible="True" Image-Url="/Content/GB/Styles/Imgs/delete_16.png" Image-ToolTip="Удалить" />
                </dxwgv:GridViewCommandColumn>
            </Columns>
            <TotalSummary>
                <dxwgv:ASPxSummaryItem FieldName="ShortName" DisplayFormat="Количество: {0}" SummaryType="Count" />
            </TotalSummary>
            <Templates>
                <EditForm>
                    <table class="form_edit">
                        <tr>
                            <td class="form_edit_desc required">
                                Краткое наименование объекта учета:
                            </td>
                            <td class="form_edit_input">
                                <dxe:ASPxTextBox ID="txtShortName" ClientInstanceName="txtShortName"
                                    runat="server" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                    OnDataBinding="txtShortName_OnDataBinding">
                                    <ValidationSettings ErrorDisplayMode="Text">
                                        <RequiredField ErrorText="Обязательное поле" IsRequired="true"></RequiredField>
                                    </ValidationSettings>
                                </dxe:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="form_edit_buttons">
                                <span class="ok">
                                    <a onclick="gridData.UpdateEdit();">Сохранить</a>
                                </span>
                                <span class="cancel">
                                    <a onclick="gridData.CancelEdit();">Отмена</a>
                                </span>
                            </td>
                        </tr>
                    </table>
                </EditForm>
            </Templates>
            <ClientSideEvents
                BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') {GridSave(e);} }" />
        </dxwgv:ASPxGridView>
    </div>
</asp:Content>
