﻿using System;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;

namespace GB.Report
{
    public class ZipFileCollection
    {
        List<string> _ListName;
        List<MemoryStream> _ListStream;

        public ZipFileCollection()
        {
            _ListName = new List<string>();
            _ListStream = new List<MemoryStream>();
        }

        public void AddFile(MemoryStream ms, string fileName)
        {
            _ListStream.Add(ms);
            _ListName.Add(CheckFileName(fileName));
        }


        private string CheckFileName(string fileName)
        {
            int i = 0;
            string str = fileName;

            while (!string.IsNullOrEmpty(
                _ListName.Find(
                    delegate(string item)
                    {
                        if (item.Equals(str, StringComparison.CurrentCultureIgnoreCase))
                        {
                            return true;
                        }

                        return false;
                    })))
            {
                i++;
                str = string.Format("{0} ({1}){2}", Path.GetFileNameWithoutExtension(fileName), i, Path.GetExtension(fileName));
            }

            return str;
        }

		/// <summary>
		/// Создать zip-архив
		/// </summary>
		public byte[] CreateZip()
		{
			return CreateZip(_ListStream, _ListName);
		}

        public void SendUser(System.Web.HttpContext context, System.Web.UI.Page page, string fileName)
        {
            byte[] zipData = CreateZip(_ListStream, _ListName);

            if (!fileName.EndsWith(".zip"))
                fileName = string.Format("{0}.zip", fileName);


            if ((page != null) && (page.Response != null))
            {
                //MemoryStream stream = new MemoryStream();
                //getStream(stream, options);
                
                string str = false ? "attachment" : "inline";

                page.Response.Clear();
                page.Response.Buffer = false;
                page.Response.AppendHeader("Content-Type", "application/zip");
                page.Response.AppendHeader("Content-Transfer-Encoding", "binary");
                page.Response.AppendHeader("Content-Disposition", string.Format("{0}; filename=\"{1}\"", str, System.Web.HttpUtility.UrlEncode(fileName).Replace("+", "%20")));

                if (zipData.Length > 0L)
                {
                    page.Response.BinaryWrite(zipData);
                }
                
                page.Response.End();
            }
        }

        public void SendExportPlan(System.Web.HttpContext context, System.Web.UI.Page page, string fileName)
        {
            byte[] zipData = CreateZip(_ListStream, _ListName);

            if (!fileName.EndsWith(".zip"))
                fileName = string.Format("{0}.zip", fileName);


            if ((page != null) && (page.Response != null))
            {
                //MemoryStream stream = new MemoryStream();
                //getStream(stream, options);

                string str = false ? "attachment" : "inline";

                page.Response.Clear();
                page.Response.Buffer = false;
                page.Response.AppendHeader("Content-Type", "application/zip");
                page.Response.AppendHeader("Content-Transfer-Encoding", "binary");
                page.Response.AppendHeader("Content-Disposition", string.Format("{0}; filename=\"{1}\"", str, fileName.Replace("+", "%20")));

                if (zipData.Length > 0L)
                {
                    page.Response.BinaryWrite(zipData);
                }

                page.Response.End();
            }
        }



        /// <summary>
        /// Создать zip-архив
        /// </summary>
        private static byte[] CreateZip(List<MemoryStream> streams, List<string> names)
        {
            ZipConstants.DefaultCodePage = 866;
            using (MemoryStream zipStream = new MemoryStream())
            {
                using (ZipOutputStream s = new ZipOutputStream(zipStream))
                {
                    for (int i = 0; i < streams.Count; i++)
                    {
                        s.SetLevel(9); // 0 - store only to 9 - means best compression

                        ZipEntry entry = new ZipEntry(names[i]);

                        entry.DateTime = DateTime.Now;
                        
                        s.PutNextEntry(entry);
                        s.Write(streams[i].ToArray(), 0, streams[i].ToArray().Length);
                        s.Flush();
                    }
                }

                return zipStream.ToArray();
            }
        }
    }
}
