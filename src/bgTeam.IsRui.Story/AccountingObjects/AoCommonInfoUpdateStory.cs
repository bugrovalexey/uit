﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoCommonInfoUpdateStory : IStory<AoCommonInfoUpdateStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public AoCommonInfoUpdateStory(
            ISessionNHibernateFactory factory,
            IRepositoryRui repository,
            IMapperBase mapper)
        {
            _factory = factory;
            _repository = repository;
            _mapper = mapper;
        }

        public bool Execute(AoCommonInfoUpdateStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(AoCommonInfoUpdateStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));
            context.KindId.CheckNull(nameof(context.KindId));
            context.ArticleId.CheckNull(nameof(context.ArticleId));
            context.Name.CheckNull(nameof(context.Name));

            var ao = _repository.GetExisting<AccountingObject>(context.Id);

            ao = _mapper.Map(context, ao);

            ao.ClassificationCategory = _repository.Get<ClassificationCategory>(context.KindOfActivityId);

            using (var session = _factory.OpenSession())
            {
                await session.UpdateAsync(ao);
            }

            return true;
        }
    }
}
