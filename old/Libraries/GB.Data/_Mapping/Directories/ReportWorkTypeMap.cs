﻿using Uviri.Entity.Directories;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ReportWorkTypeMap : ClassMapBase<ReportWorkType>
    {
        public ReportWorkTypeMap()
            : base("Report_Work_Type")
        {

        }

        protected override void InitMap()
        {
            References<PlansWork>(x => x.PlanWork, "Plans_Work_ID").Nullable();
            References<ExpenseType>(x => x.ExpenseType, "Expense_Type_ID").Nullable();
            References<PlansSpec>(x => x.PlansSpec, "Plans_Spec_ID").Nullable();
            References<SpecType>(x => x.SpecType, "Spec_Exp_Type_ID").Nullable();
        }
    }
}
