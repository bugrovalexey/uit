﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;

    public class GRBSDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Код
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        public virtual string SName { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public virtual string Addr { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// Сайт
        /// </summary>
        public virtual string Web { get; set; }

        /// <summary>
        /// Дата начала действия справочника
        /// </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary>
        /// Дата окончания действия справочника
        /// </summary>
        public virtual DateTime? DateEnd { get; set; }
    }
}
