﻿using bgTeam.IsRui.MKRF.Providers;

namespace bgTeam.IsRui.MKRF.Logic.Tabs.Handler
{
    internal class MainTabsHandler : TabsBaseHandler
    {
        public MainTabsHandler(IAccessControllerProvider accessControllerProvider)
            :base(accessControllerProvider)
        {
            Add("Объекты учета", "Index", "AccountingObjects");
            Add("Мероприятия", "Index", "Activities");
            //Add("Планирование", "Index", "Plans");
            //Add("Аналитика", "Index", "Analytics");
        }
    }
}