﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoFinancingUpdateInfoStoryContext
    {
        public int? AoId { get; set; }

        public decimal? quarter1 { get; set; }

        public decimal? quarter2 { get; set; }

        public decimal? quarter3 { get; set; }

        public decimal? quarter4 { get; set; }
    }
}
