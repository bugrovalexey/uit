﻿namespace bgTeam.IsRui.Common.Services.Impl
{
    using System.Collections.Generic;
    using System.IO;
    using bgTeam.IsRui.Common.Extentions;
    using Firefly.SimpleXls;

    public class BuldXLS : IBuildXLS
    {
        public void ExportToXLS(List<XlsOrderViewModelExtension> orders)
        {
            Exporter.CreateNew()
                .AddSheet(orders, settings => { settings.SheetName = "Отчёт о финансировании"; })
                .Export(@"Templates\FinancingReport.xlsx");

        }
    }
}
