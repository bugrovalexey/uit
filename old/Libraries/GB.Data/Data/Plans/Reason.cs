﻿using GB.Data.Documents;
using System.Collections.Generic;

namespace GB.Data.Plans
{
    /// <summary>Другие основания реализации</summary>
    public class Reason : EntityBase
    {
        public Reason()
        {
            Documents = new List<Files>(0);
        }

        /// <summary>Наименование документа</summary>
        public virtual string Name { get; set; }

        /// <summary>Номер документа</summary>
        public virtual string Num { get; set; }

        /// <summary>URL документа</summary>
        public virtual string URL { get; set; }

        /// <summary>ФИО подписавшего</summary>
        public virtual string SignerName { get; set; }

        /// <summary>Должность подписавшего</summary>
        public virtual string SignerPosition { get; set; }

        /// <summary>Проект</summary>
        public virtual PlansActivity Owner { get; set; }

        /// <summary>Документ-основание</summary>
        public virtual IList<Files> Documents { get; protected set; }
    }
}