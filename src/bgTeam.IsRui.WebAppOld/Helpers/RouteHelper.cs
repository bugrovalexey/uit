﻿using bgTeam.Extensions;
using System.Web.Routing;

namespace bgTeam.IsRui.WebApp.Helpers
{
    public class RouteHelper
    {
        public static RouteValueDictionary Merge(params object[] items)
        {
            var result = new RouteValueDictionary();

            foreach (var itemObj in items)
            {
                itemObj.Do(item =>
                {
                    foreach (System.Reflection.PropertyInfo fi in item.GetType().GetProperties())
                    {
                        result[fi.Name] = fi.GetValue(item, null);
                    }
                });
            }
 
            return result;
        }
    }
}