﻿namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class AoArticleMap : ClassMapHiLo<AoArticle>
    {
        public AoArticleMap()
            : base("OU_Article")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Article_Name");
            Map(x => x.TimeStamp, "TimeStamp");
        }
    }
}
