﻿using bgTeam.IsRui.Common.Services.Impl;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Documents;
using bgTeam.IsRui.Story.Common;
using System;
using System.Linq;
using Test.bgTeam.IsRui.Unit.Common;
using Xunit;

namespace Test.bgTeam.IsRui.Unit.Tests
{
    public class TestsCommon
    {
        private Factory _fac;

        public TestsCommon()
        {
            _fac = new Factory();
        }

        [Fact]
        public void Story_FileAddStory_Google()
        {
            var story = new FileAddStory(_fac.storyMapper, _fac.repositoryRui, _fac.fileProviderRui, _fac.sessionFactory);
            var res = story.Execute(new  FileAddStoryContext()
            {
                Kind = DocKindEnum.GoogleDrive,
                Entity = new WorkflowUserConclusion() { Id = 6 },
                Type = DocTypeEnum.WorkflowUserConclusion,
                
                Name = "Test.png",
                Size = 100,
                Url = "https://drive.google.com/file/d/1w-jVBm0KNBQrQMb-QH_3VOGp5QagneUV/preview?usp=drive_web",
                MimeType = "image/png",
            });

            Assert.NotNull(res);
            //Assert.Equal(6, res.Id);
            //Assert.Equal(0, res.StatusId);
            //Assert.Equal("Test id", res.Text);
        }

        [Fact]
        public void CacheValue_IsExpired()
        {
            var cacheValue = new CacheValue<int>(1, new System.TimeSpan(0, 0, 1));

            Assert.False(cacheValue.IsExpire);

            System.Threading.Thread.Sleep(1010);

            Assert.True(cacheValue.IsExpire);
        }

        [Fact]
        public void CacheManager_CanSetValues()
        {
            var cache = new CacheManager();

            Assert.True(cache.TrySetValue(1, 1));
            Assert.True(cache.TrySetValue(2, 2));
            Assert.True(cache.TrySetValue(3, 3));
            Assert.True(cache.TrySetValue(4, 4));
            Assert.True(cache.TrySetValue(1, "aa"));
            Assert.True(cache.TrySetValue(2, "bb"));
            Assert.True(cache.TrySetValue("1", 2));
            Assert.True(cache.TrySetValue("2", 2));
            Assert.True(cache.TrySetValue("2", DateTime.Now));
            Assert.True(cache.TrySetValue("2", 2D));
            Assert.True(cache.TrySetValue("2", 2L));
        }

        [Fact]
        public void CacheManager_CanSetGetValues()
        {
            var cache = new CacheManager();

            Assert.True(cache.TrySetValue(1, 1));
            Assert.True(cache.TryGetValue(1, out int int1));
            Assert.Equal(1, int1);

            Assert.True(cache.TrySetValue(2, 2));
            Assert.True(cache.TryGetValue(2, out int int2));
            Assert.Equal(2, int2);

            Assert.True(cache.TrySetValue(3, 3));
            Assert.True(cache.TryGetValue(3, out int int3));
            Assert.Equal(3, int3);

            Assert.True(cache.TrySetValue(4, 4));
            Assert.True(cache.TryGetValue(4, out int int4));
            Assert.Equal(4, int4);

            Assert.True(cache.TrySetValue(1, "aa"));
            Assert.True(cache.TryGetValue(1, out string str_aa));
            Assert.Equal("aa", str_aa);

            Assert.True(cache.TrySetValue(2, "bb"));
            Assert.True(cache.TryGetValue(2, out string str_bb));
            Assert.Equal("bb", str_bb);

            Assert.True(cache.TrySetValue("1", 2));
            Assert.True(cache.TryGetValue("1", out int int2_2));
            Assert.Equal(2, int2_2);

            Assert.True(cache.TrySetValue("2", 2));
            Assert.True(cache.TryGetValue("2", out int int2_3));
            Assert.Equal(2, int2_3);

            var dt = DateTime.Now;
            Assert.True(cache.TrySetValue("2", dt));
            Assert.True(cache.TryGetValue("2", out DateTime dateTime_1));
            Assert.Equal(dt, dateTime_1);

            Assert.True(cache.TrySetValue("2", 2D));
            Assert.True(cache.TryGetValue("2", out double double2));
            Assert.Equal(2D, double2);

            Assert.True(cache.TrySetValue("2", 2L));
            Assert.True(cache.TryGetValue("2", out long long2));
            Assert.Equal(2L, long2);
        }

        [Fact]
        public void CacheManager_CanUpdateValues()
        {
            var cache = new CacheManager();

            Assert.True(cache.TrySetValue(1, 1));
            Assert.True(cache.TryGetValue(1, out int int1));
            Assert.Equal(1, int1);

            Assert.True(cache.TrySetValue(1, 2));
            Assert.True(cache.TryGetValue(1, out int int2));
            Assert.Equal(2, int2);

            Assert.True(cache.TrySetValue(1, 3));
            Assert.True(cache.TryGetValue(1, out int int3));
            Assert.Equal(3, int3);

            Assert.True(cache.TrySetValue(1, 4));
            Assert.True(cache.TryGetValue(1, out int int4));
            Assert.Equal(4, int4);
        }
    }
}