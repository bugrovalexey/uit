﻿namespace bgTeam.IsRui.Domain._Mapping.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Domain.Entities.Common;
    using FluentNHibernate.Mapping;

    internal class ArticleBudgetMap : ClassMap<ArticleBudget>
    {
        public ArticleBudgetMap()
        {
            Table("ArticleBudget");

            Id(x => x.Id, "AB_ID");

            InitMap();
        }

        protected void InitMap()
        {
            Map(x => x.Name, "AB_Name");
            Map(x => x.TimeStamp, "AB_TimeStamp");

            References(x => x.Group, "AB_Group_ID");
        }
    }
}
