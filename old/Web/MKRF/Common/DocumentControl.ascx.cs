﻿using System;
using System.Text;
using GB;
using GB.Controls;
using GB.Handlers;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Repository;
using GB.MKRF.Validation;
using GB.MKRF.Web.Helpers;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

namespace GB.MKRF.Web.Common
{
    public partial class DocumentControl : System.Web.UI.UserControl
    {
        private EntityBase owner = null;
        /// <summary>
        /// Владелец документа
        /// </summary>
        public EntityBase Owner
        {
            get
            {
                return owner;
            }
            set
            {
                owner = value;
            }
        }

        private DocTypeEnum? docType = null;
        /// <summary>
        /// Тип документа
        /// </summary>
        public DocTypeEnum DocType
        {
            get
            {
                if (!docType.HasValue)
                    throw new ArgumentException("docType = null");

                return docType.Value;
            }
            set
            {
                docType = value;
            }
        }

        protected Document currentDoc = null;
        protected bool isNewRow = false;

        protected override void OnPreRender(EventArgs e)
        {
            docsGrid.DataBind();

            addButton.Visible = Validation.Validator.Validate(new Document(DocType, Owner), ObjActionEnum.create);

            base.OnPreRender(e);
        }

        protected void docsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            docsGrid.DataSource = DocumentRepositoryOld.GetList(DocType, Owner);
        }

        protected void docsGrid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            currentDoc = RepositoryBase<Document>.Get(e.EditingKeyValue);
        }

        protected void docsGrid_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            isNewRow = true;
        }

        protected void docsGrid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            Document doc = new Document(DocType, Owner);
            saveRow(doc);

            if (Owner != null)
                RepositoryBase<EntityBase>.Refresh(Owner);

            docsGrid.CancelEdit();
            e.Cancel = true;
        }

        protected void docsGrid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            Document doc = RepositoryBase<Document>.Get((int)e.Keys[0]);
            saveRow(doc);

            docsGrid.CancelEdit();
            e.Cancel = true; 
        }

        protected void docsGrid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            DocumentController.Delete((int)e.Keys[0]);

            docsGrid.CancelEdit();
            e.Cancel = true;
        }

        protected void NameTextBox_DataBinding(object sender, EventArgs e)
        {
            if (!isNewRow && currentDoc == null)
                return;

            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = isNewRow ? string.Empty : currentDoc.Name;
        }

        protected void NumTextBox_DataBinding(object sender, EventArgs e)
        {
            if (!isNewRow && currentDoc == null)
                return;

            ASPxTextBox box = (ASPxTextBox)sender;

            box.Text = isNewRow ? string.Empty : currentDoc.Num;
        }

        void saveRow(Document doc)
        {
            DocumentController.SaveOrUpdate(doc, new DocumentArgs()
            {
                Author = UserRepository.GetCurrent(),
                FileInfo = hfDoc["File"],
                Name = hfDoc["Name"],
                Num = hfDoc["Num"],
                Date = DateTime.Now
            });       
        }

        protected void docsGrid_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            int? composite = null;

            composite = (int?)grid.GetRowValues(e.VisibleIndex, "Composite");

            if (e.ButtonType == ColumnCommandButtonType.Edit)
            {
                e.Visible = (composite.HasValue && (composite.Value & (int)CompositeEnum.Edit) != 0);
            }
            else if (e.ButtonType == ColumnCommandButtonType.Delete)
            {
                e.Visible = (composite.HasValue && (composite.Value & (int)CompositeEnum.Delete) != 0);
            }
        }

        protected void docsGrid_CancelRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            docsGrid.DataBind();}

        protected string getDownloadUrl(int docId, bool access)
        {
			StringBuilder sb = new StringBuilder();

			if (docId > 0)
				sb.Append("<a href='" + FileHandler.GetUrl(docId) + "'>Скачать</a>");
			if (RepositoryBase<Document>.Get(docId) != null && RepositoryBase<Document>.Get(docId).SignedPackage != null)
				sb.Append("<br/><a href='javascript:checkECP()'>Проверить ЭП</a>");

            if (this.DocType == DocTypeEnum.demand_document && access)
                sb.Append("<br/><br/><a href='javascript:RoleGrid.PerformCallback(" + docId + ");'>Настройка доступа</a>");

        	return sb.ToString();
        }

		protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
		{
			//e.Result = DigitalSignatureHelper.VerifySignature(Owner).error.message;
			try
			{
				switch (DigitalSignatureHelper.IsSignatureValid(Owner))
				{
					case true:
						e.Result = "Подпись действительна";
						break;
					case false:
						e.Result = "Подпись не действительна";
						break;
				}
			}
			catch
			{
				e.Result = "Проверить подпись не удалось. Сервис проверки недоступен.";
			}
		}

        protected void RoleGrid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            int Id = Convert.ToInt32(e.Parameters);
            ASPxGridView grid = (sender as ASPxGridView);

            grid.DataSource = EntityHelper.ConvertToAnonymous<Role>(RepositoryBase<Role>.GetAll(x => (x.Id != (int)RoleEnum.CA && x.Id != (int)RoleEnum.Admin)),
                delegate(Role r)
                {
                    return new
                    {
                        Id = r.Id,
                        Name = r.Name,
                        DocId = Id
                    };
                });



            grid.DataBind();

            DocumentAccessList list = new DocumentAccessList(Id);

            for (int i = 0; i < grid.VisibleRowCount; i++)
            {
                int roleId = (int)grid.GetRowValues(i, "Id");

                if (list.Find(roleId) == null)
                {
                    grid.Selection.SelectRow(i);
                }
            }
        }

        class DocumentAccessList
        {
            System.Collections.Generic.IList<DocumentAccess> list;

            public DocumentAccessList(int id)
            {
                list = RepositoryBase<DocumentAccess>.GetAll(x => x.Document.Id == id);
            }

            public DocumentAccess Find(int roleId)
            {
                foreach (DocumentAccess item in list)
                {
                    if (item.Role.Id == roleId)
                    {
                        return item;
                    }
                }

                return null;
            }

            public void DeleteAll()
            {
                foreach (DocumentAccess item in list)
                {
                    RepositoryBase<DocumentAccess>.Delete(item.Id);
                }
            }
        }

        protected void RoleCallBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            int Id = (int)RoleGrid.GetRowValues(0, "DocId");

            //удалить все записи
            DocumentAccessList list = new DocumentAccessList(Id);

            list.DeleteAll();

            for (int i = 0; i < RoleGrid.VisibleRowCount; i++)
            {
                if (!RoleGrid.Selection.IsRowSelected(i))
                {
                    DocumentAccess da = new DocumentAccess();

                    da.Document = RepositoryBase<Document>.Get(Id);
                    da.Role = RepositoryBase<Role>.Get((int)RoleGrid.GetRowValues(i, "Id"));

                    RepositoryBase<DocumentAccess>.SaveOrUpdate(da);
                }
            }
        }

        protected void DocGroupComboBox_DataBinding(object sender, EventArgs e)
        {
            if (!isNewRow && currentDoc == null)
                return;

            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = DocumentRepositoryOld.GetDocGroups();

            box.Value = isNewRow ? (int)DocGroupEnum.None : (int)currentDoc.DocGroup;
        }

        protected void uploader2_DataBinding(object sender, EventArgs e)
        {
            FileUploadControl2 uploader = (FileUploadControl2)sender;

            if (currentDoc == null)
                return;

            uploader.SetValue(currentDoc.FileName, currentDoc.Id.ToString());
            uploader.FileItemTemplate = "<a href=\"" + FileHandler.GetUrl("") + "{id}\">{name}</a>";
        }
    }
}