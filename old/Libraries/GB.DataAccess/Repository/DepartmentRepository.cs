﻿using GB.Data.Common;
using System.Collections.Generic;

namespace GB.DataAccess.Repository
{
    public class DepartmentRepository : Repository<Department>
    {
        public static IList<int> GetAllChildrensIds(int parentId)
        {
            return Session.CreateSQLQuery("select Govt_Organ_ID from dbo.v_Govt_Organ_Tree(?)").SetInt32(0, parentId).List<int>();
        }
    }
}