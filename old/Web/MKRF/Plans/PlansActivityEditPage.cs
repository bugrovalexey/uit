﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using GB.Helpers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Repository;

namespace GB.MKRF.Web.Plans
{
    public class PlansActivityEditPage : Page
    {
        public const string ActivityIdKey = "ActivityId";

        private PlansActivity _currentActivity = null;
        public PlansActivity CurrentActivity
        {
            get
            {
                if (_currentActivity == null)
                {
                    int id = 0;
                    if (int.TryParse(Request[ActivityIdKey], out id))
                    {
                        _enableEdit = null;
                        _currentActivity = PlanRepository.GetActivity(id);
                    }

                    if (_currentActivity != null && !Validation.Validator.Validate(_currentActivity.Plan, ObjActionEnum.view))
                        _currentActivity = null;

                    if (_currentActivity == null)
                        ResponseHelper.DenyAccess(Response);

                }
                return _currentActivity;
            }
        }

        bool? _enableEdit = null;
        public virtual bool EnableEdit
        {
            get
            {
                if (_enableEdit == null)
                {
                    _enableEdit = CurrentActivity != null && Validation.Validator.Validate(CurrentActivity, ObjActionEnum.edit);
                }
                return _enableEdit.Value;
            }
        }

        public void RefreshActivity()
        {
            RepositoryBase<PlansActivity>.Refresh(CurrentActivity);
        }
    }
}