﻿using FluentNHibernate.Mapping;
using GB.Data.AccountingObjects;

namespace GB._Mapping.AccountingObjects
{
    class GovContractMap : ClassMapHiLo<GovContract>
    {
        public GovContractMap()
            : base("OU_Gos_Contract") { }

        protected override void InitMap()
        {
            Map(x => x.Number, "OU_Gos_Contract_Reestr_Num");
            Map(x => x.Date, "OU_Gos_Contract_Date");
            Map(x => x.Status, "OU_Gos_Contract_Status").CustomType<GovContractStatusEnum>();
            Map(x => x.Executer, "OU_Gos_Contract_Executer");
            Map(x => x.Price, "OU_Gos_Contract_Cost").Not.Nullable();
            Map(x => x.Currency, "OU_Gos_Contract_Currency");
            Map(x => x.UrlOnZakupki, "OU_Gos_Contract_Link");
            Map(x => x.Order, "OU_Gos_Contract_Zakupka");
            Map(x => x.ReasonsCostDeviation, "OU_Gos_Contract_Reason");

            Map(x => x.IdZakupki360, "OU_Gos_Contract_IdZK");
            Map(x => x.Stage, "OU_Stage_ID").CustomType<AccountingObjectsStageEnum>();
            
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();

            HasMany(x => x.Acts).KeyColumns.Add("OU_Gos_Contract_ID")
                .Inverse().Cascade.AllDeleteOrphan();
        }
    }
}
