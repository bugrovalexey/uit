﻿using Uviri.MKRF.Entities.Prognosis;
using Uviri.MKRF.Entities.Prognosis.SectionII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionII
{
    public class TechInfoMap : ClassMapBase<TechInfo>
    {
        public TechInfoMap()
            : base("MF2")
        {
//            Table("w_");

//            Id(x => x.Id, "MF2_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string Field = 
//                        @"@MF2_CA_8_1 = ?
//                         ,@MF2_TP_8_1 = ?
//                         ,@MF2_CA_8_2 = ?
//                         ,@MF2_TP_8_2 = ?
//                         ,@MF2_CA_8_3 = ?
//                         ,@MF2_TP_8_3 = ?
//                         ,@MF2_CA_8_4 = ?
//                         ,@MF2_TP_8_4 = ?
//                         ,@MF2_CA_8_5 = ?
//                         ,@MF2_TP_8_5 = ?
//                         ,@MF2_CA_8_6_1 = ?
//                         ,@MF2_TP_8_6_1 = ?
//                         ,@MF2_CA_8_6_2 = ?
//                         ,@MF2_TP_8_6_2 = ?
//                         ,@MF2_CA_8_7 = ?
//                         ,@MF2_TP_8_7 = ?
//                         ,@MF2_CA_8_8 = ?
//                         ,@MF2_TP_8_8 = ?
//                         ,@MF2_CA_8_9 = ?
//                         ,@MF2_TP_8_9 = ?
//                         ,@MF2_CA_8_10 = ?
//                         ,@MF2_TP_8_10 = ?
//                         ,@MF2_CA_8_11 = ?
//                         ,@MF2_TP_8_11 = ?
//                         ,@MF2_CA_8_12 = ?
//                         ,@MF2_TP_8_12 = ?
//                         ,@MF2_9_1 = ?
//                         ,@MF2_CA_9_2 = ?
//                         ,@MF2_TP_9_2 = ?
//                         ,@MF2_CA_9_3 = ?
//                         ,@MF2_TP_9_3 = ?
//                         ,@MF2_CA_9_4 = ?
//                         ,@MF2_TP_9_4 = ?
//                         ,@MF2_CA_9_5_DialUp = ?
//                         ,@MF2_TP_9_5_DialUp = ?
//                         ,@MF2_CA_9_5_ISDN = ?
//                         ,@MF2_TP_9_5_ISDN = ?
//                         ,@MF2_CA_9_5_ADSL = ?
//                         ,@MF2_TP_9_5_ADSL = ?
//                         ,@MF2_CA_9_5_WiFi = ?
//                         ,@MF2_TP_9_5_WiFi = ?
//                         ,@MF2_CA_9_5_WiMAX = ?
//                         ,@MF2_TP_9_5_WiMAX = ?
//                         ,@MF2_CA_9_5_Sputnik = ?
//                         ,@MF2_TP_9_5_Sputnik = ?
//                         ,@MF2_CA_9_5_Dedic_Line = ?
//                         ,@MF2_TP_9_5_Dedic_Line = ?
//                         ,@MF2_CA_9_6 = ?
//                         ,@MF2_TP_9_6 = ?
//                         ,@MF2_CA_9_7 = ?
//                         ,@MF2_TP_9_7 = ?
//                         ,@MF2_CA_10_1 = ?
//                         ,@MF2_TP_10_1 = ?
//                         ,@MF2_CA_10_2 = ?
//                         ,@MF2_TP_10_2 = ?
//                         ,@MF2_CA_10_3 = ?
//                         ,@MF2_TP_10_3 = ?
//                         ,@MF2_CA_10_4 = ?
//                         ,@MF2_TP_10_4 = ?
//                         ,@MF2_CA_10_5_Internal = ?
//                         ,@MF2_TP_10_5_Internal = ?
//                         ,@MF2_CA_10_5_External = ?
//                         ,@MF2_TP_10_5_External = ?
//                         ,@MF2_CA_10_6_Internal = ?
//                         ,@MF2_TP_10_6_Internal = ?
//                         ,@MF2_CA_10_6_External = ?
//                         ,@MF2_TP_10_6_External = ?
//                         ,@MF2_CA_10_7 = ?
//                         ,@MF2_TP_10_7 = ?
//                         ,@MF2_CA_10_8 = ?
//                         ,@MF2_TP_10_8 = ?
//                         ,@Mon_Form_ID = ?
//                         ,@MF2_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_MF2_Insert " + Field).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF2_Update " + Field).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF2_Delete
//						  @MF2_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.CountPC_Intel2_CA, "MF2_CA_8_1");
            Map(x => x.CountPC_Intel2_TP, "MF2_TP_8_1");
            Map(x => x.CountPC_Intel3_CA, "MF2_CA_8_2");
            Map(x => x.CountPC_Intel3_TP, "MF2_TP_8_2");
            Map(x => x.CountPC_Apple_CA, "MF2_CA_8_3");
            Map(x => x.CountPC_Apple_TP, "MF2_TP_8_3");
            Map(x => x.CountLapTop_CA, "MF2_CA_8_4");
            Map(x => x.CountLapTop_TP, "MF2_TP_8_4");
            Map(x => x.CountServer86_CA, "MF2_CA_8_5");
            Map(x => x.CountServer86_TP, "MF2_TP_8_5");
            Map(x => x.CountBackupSystems_CA, "MF2_CA_8_6_1");
            Map(x => x.CountBackupSystems_TP, "MF2_TP_8_6_1");
            Map(x => x.AmountDataStored_CA, "MF2_CA_8_6_2");
            Map(x => x.AmountDataStored_TP, "MF2_TP_8_6_2");
            Map(x => x.CountDataCenters_CA, "MF2_CA_8_7");
            Map(x => x.CountDataCenters_TP, "MF2_TP_8_7");
            Map(x => x.CountMultifunction_CA, "MF2_CA_8_8");
            Map(x => x.CountMultifunction_TP, "MF2_TP_8_8");
            Map(x => x.CountXeroxs_CA, "MF2_CA_8_9");
            Map(x => x.CountXeroxs_TP, "MF2_TP_8_9");
            Map(x => x.CountPublicPrinters_CA, "MF2_CA_8_10");
            Map(x => x.CountPublicPrinters_TP, "MF2_TP_8_10");
            Map(x => x.CountPersonPrinters_CA, "MF2_CA_8_11");
            Map(x => x.CountPersonPrinters_TP, "MF2_TP_8_11");
            Map(x => x.CountScanners_CA, "MF2_CA_8_12");
            Map(x => x.CountScanners_TP, "MF2_TP_8_12");
            Map(x => x.CountTerritorialConnectLAN, "MF2_9_1");
            Map(x => x.CountPCConnectLAN_CA, "MF2_CA_9_2");
            Map(x => x.CountPCConnectLAN_TP, "MF2_TP_9_2");
            Map(x => x.CountPCConnectNET128_CA, "MF2_CA_9_3");
            Map(x => x.CountPCConnectNET128_TP, "MF2_TP_9_3");
            Map(x => x.CountPCConnectNET10_CA, "MF2_CA_9_4");
            Map(x => x.CountPCConnectNET10_TP, "MF2_TP_9_4");
            Map(x => x.CountNETtoDialUp_CA, "MF2_CA_9_5_DialUp");
            Map(x => x.CountNETtoDialUp_TP, "MF2_TP_9_5_DialUp");
            Map(x => x.CountNETtoISDN_CA, "MF2_CA_9_5_ISDN");
            Map(x => x.CountNETtoISDN_TP, "MF2_TP_9_5_ISDN");
            Map(x => x.CountNETtoADSL_CA, "MF2_CA_9_5_ADSL");
            Map(x => x.CountNETtoADSL_TP, "MF2_TP_9_5_ADSL");
            Map(x => x.CountNETtoWiFi_CA, "MF2_CA_9_5_WiFi");
            Map(x => x.CountNETtoWiFi_TP, "MF2_TP_9_5_WiFi");
            Map(x => x.CountNETtoWiMAX_CA, "MF2_CA_9_5_WiMAX");
            Map(x => x.CountNETtoWiMAX_TP, "MF2_TP_9_5_WiMAX");
            Map(x => x.CountNETtoSatellite_CA, "MF2_CA_9_5_Sputnik");
            Map(x => x.CountNETtoSatellite_TP, "MF2_TP_9_5_Sputnik");
            Map(x => x.CountNETtoLine_CA, "MF2_CA_9_5_Dedic_Line");
            Map(x => x.CountNETtoLine_TP, "MF2_TP_9_5_Dedic_Line");
            Map(x => x.CountEmployeesIndividualConnectNET_CA, "MF2_CA_9_6");
            Map(x => x.CountEmployeesIndividualConnectNET_TP, "MF2_TP_9_6");
            Map(x => x.CountEmployeesPublicConnectNET_CA, "MF2_CA_9_7");
            Map(x => x.CountEmployeesPublicConnectNET_TP, "MF2_TP_9_7");
            Map(x => x.CountPhoneFederal_CA, "MF2_CA_10_1");
            Map(x => x.CountPhoneFederal_TP, "MF2_TP_10_1");
            Map(x => x.CountPhonePublic_CA, "MF2_CA_10_2");
            Map(x => x.CountPhonePublic_TP, "MF2_TP_10_2");
            Map(x => x.PhonePSTN_CA, "MF2_CA_10_3");
            Map(x => x.PhonePSTN_TP, "MF2_TP_10_3");
            Map(x => x.CountPhoneInternal_CA, "MF2_CA_10_4");
            Map(x => x.CountPhoneInternal_TP, "MF2_TP_10_4");
            Map(x => x.InternalPhone_CA, "MF2_CA_10_5_Internal");
            Map(x => x.InternalPhone_TP, "MF2_TP_10_5_Internal");
            Map(x => x.PublicPhone_CA, "MF2_CA_10_5_External");
            Map(x => x.PublicPhone_TP, "MF2_TP_10_5_External");
            Map(x => x.FreeInternalPhone_CA, "MF2_CA_10_6_Internal");
            Map(x => x.FreeInternalPhone_TP, "MF2_TP_10_6_Internal");
            Map(x => x.FreePublicPhone_CA, "MF2_CA_10_6_External");
            Map(x => x.FreePublicPhone_TP, "MF2_TP_10_6_External");
            Map(x => x.CountFacsimileMachine_CA, "MF2_CA_10_7");
            Map(x => x.CountFacsimileMachine_TP, "MF2_TP_10_7");
            Map(x => x.CountMobilePhone_CA, "MF2_CA_10_8");
            Map(x => x.CountMobilePhone_TP, "MF2_TP_10_8");

            References<Prognos>(x => x.Prognos, "Mon_Form_ID").Not.Nullable();
        }
    }

}
