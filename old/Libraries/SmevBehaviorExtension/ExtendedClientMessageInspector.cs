﻿using System.IO;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Xml;

namespace GB.SmevBehaviorExtension
{
    public class ExtendedClientMessageInspector : IClientMessageInspector
    {
        private string certificateName = null;

        public ExtendedClientMessageInspector(string CertificateName)
        {
            this.certificateName = CertificateName;
        }

        void IClientMessageInspector.AfterReceiveReply(ref Message reply, object correlationState)
        {
        }

        object IClientMessageInspector.BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
        {
            XmlDocument doc = Signer.SignXml(request.ToString(), this.certificateName, SignMode.Request);

            MemoryStream ms = new MemoryStream();
            
            XmlWriter writer = XmlWriter.Create(ms);
            doc.WriteTo(writer);
            writer.Flush();
            ms.Position = 0;


            XmlReader reader = XmlReader.Create(ms);

            Message newRequest = Message.CreateMessage(reader, int.MaxValue,  request.Version);
            newRequest.Properties.Clear();
            newRequest.Properties.CopyProperties(request.Properties);
            request = newRequest;

            return null;
        }
    }
}
