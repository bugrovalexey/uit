﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateAOFactualLocationAccountingObjectCommand : ICommand<UpdateAOFactualLocationAccountingObjectContext>
    {
        ISessionFactoryGb factory;

        public UpdateAOFactualLocationAccountingObjectCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(UpdateAOFactualLocationAccountingObjectContext context)
        {
            var entity = new Repository<AccountingObject>().GetExisting(context.xId);

            entity.LocationFioResponsible = context.LocationFioResponsible;
            entity.LocationOrgAddress = context.LocationOrgAddress;
            entity.LocationOrgName = context.LocationOrgName;
            entity.LocationOrgPhone = context.LocationOrgPhone;           

            using (var session = factory.Create())
            {
                session.Update(entity);
                session.Commit();
            }
        }
    }
}
