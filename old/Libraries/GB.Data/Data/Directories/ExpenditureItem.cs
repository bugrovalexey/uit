﻿using System;

namespace GB.Data.Directories
{
    /// <summary>
    /// Код по БК КОСГУ
    /// </summary>
    public class ExpenditureItem : EntityBase, IEntityCodeName, IPeriodDirectories
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Код</summary>
        public virtual string Code { get; set; }

        /// <summary> Дата начала действия справочника </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary> Дата окончания действия справочника </summary>
        public virtual DateTime? DateEnd { get; set; }

        public virtual string CodeName { get { return EntityHelper.GetCodeName(this); } }
    }
}