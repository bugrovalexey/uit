﻿using Uviri.MKRF.Entities.PlanReport.SectionIV;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionIV
{
    public class PRS4Map : ClassMapBase<PRS4>
    {
        public PRS4Map()
            : base("PRS_4")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_4_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@PRS_4_Equip = ? 
//						  ,@PRS_4_Price = ? 
//						  ,@PRS_4_Count =? 
//						  ,@Plans_Spec_ID = ? 
//						  ,@PRS_4_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_4_Insert " + sql).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_4_Update " + sql).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_4_Delete @PRS_4_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Equip, "PRS_4_Equip");
            Map(x => x.Price, "PRS_4_Price");
            Map(x => x.Count, "PRS_4_Count");

            References<PlansSpec>(x => x.PlansSpec, "Plans_Spec_ID");
        }
    }
}
