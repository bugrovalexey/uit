﻿using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Mapping.Admin
{
    internal class UserToNotificationMap : ClassMapBase<UserToNotification>
    {
        public UserToNotificationMap()
            : base("Users_Notification")
        {
        }

        protected override void InitMap()
        {
            References<User>(x => x.User, "Users_ID").Not.Nullable();
            References<Notification>(x => x.Notification, "Notification_ID").Not.Nullable();
        }
    }
}