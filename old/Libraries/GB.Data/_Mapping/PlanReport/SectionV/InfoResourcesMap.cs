﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.PlanReport.SectionV;

namespace Uviri.MKRF.Mapping.PlanReport.SectionV
{
    public class InfoResourcesMap : ClassMapBase<InfoResources>
	{
        public InfoResourcesMap()
            : base("PRS_5_8")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_5_8_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string args = @" @PRS_5_8_Name =? 
//							,@PRS_5_ID =? 
//							,@Govt_Organ_ID =? 
//							,@PRS_5_8_ID =? 
//							";

//            SqlInsert(@"exec ZZZ_Prc_PRS_5_8_Insert " + args).Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_PRS_5_8_Update " + args).Check.None();

//            SqlDelete(@"exec ZZZ_Prc_PRS_5_8_Delete
//						  @PRS_5_8_ID = ?").Check.None();

        }

        protected override void InitMap()
        {
            Map(x => x.Name, "PRS_5_8_Name");

			References<SupplyISInfo>(x => x.SupplyISInfo, "PRS_5_ID").Not.Nullable();
			References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
        }
    }
}