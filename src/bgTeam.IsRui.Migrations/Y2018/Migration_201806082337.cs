﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201806080005)]
    public class Migration_201806080005 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Claim").AddColumn("Claim_ImplementationCost").AsDecimal().Nullable();
            Alter.Table("ClaimScopeOfWork").AddColumn("ClaimScopeOfWork_WorkHours").AsInt32().Nullable();
        }
    }
}
