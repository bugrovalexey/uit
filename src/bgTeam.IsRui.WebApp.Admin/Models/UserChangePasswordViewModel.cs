﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.WebApp.Admin.Models
{
    public class UserChangePasswordViewModel
    {
        public UserChangePasswordViewModel()
        {

        }

        public UserChangePasswordViewModel(string errorMessage)
        {
            Message = errorMessage;
        }

        public UserChangePasswordViewModel(int userId, string userName)
        {
            Id = userId;
            UserName = userName;
        }

        public int Id { get; set; }

        public string UserName { get; set; }

        public string NewPassword { get; set; }

        public string NewPasswordConfirm { get; set; }

        public string Message { get; set; }
    }
}
