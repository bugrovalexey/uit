﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace GB.Controls.Dictionary
{
    /// <summary>
    /// Предоставляет статический класс хранения справочников
    /// </summary>
    static internal class MetaDictionaries
    {
        //internal const string ConnectionString = "ConnectionString";

        public static string ConnectionString { get; set; }

        static Dictionary<int, MetaDictionary> _ListDictionaryItem;

        internal static Dictionary<int, MetaDictionary> ListDictionaryItem
        {
            get
            {
                if (_ListDictionaryItem == null)
                {
                    _ListDictionaryItem = new Dictionary<int, MetaDictionary>();

                    Update();
                }

                //Update();

                return _ListDictionaryItem;
            }
        }

        /// <summary>
        /// Обновить информацию о справочниках
        /// </summary>
        internal static void Update()
        {
            _ListDictionaryItem.Clear();

            GetDataDictionaries(_ListDictionaryItem);

            foreach (var item in _ListDictionaryItem.Values)
            {
                item.Update();
            }
        }

        internal static void Clear()
        {
            _ListDictionaryItem = null;
        }

        /// <summary>
        /// Получить данные описывающие справочники
        /// </summary>
        /// <param name="list"></param>
        private static void GetDataDictionaries(Dictionary<int, MetaDictionary> list)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))//System.Configuration.ConfigurationManager.ConnectionStrings[ConnectionString].ConnectionString))
            {
                SqlCommand command = new SqlCommand("SELECT ID, Name, ViewName, FieldUse_InUse, ProcedureInsert, ProcedureUpdate, ProcedureDelete, Visible, ReadOnly, isnull(OrderBy, '') OrderBy FROM Meta_Directories ORDER BY Name", connection);

                try
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            MetaDictionary item = new MetaDictionary(reader.GetInt32(0),
                                                                            reader.GetString(1),
                                                                            reader.GetString(2),
                                                                            reader.GetBoolean(3),
                                                                            reader.GetString(4),
                                                                            reader.GetString(5),
                                                                            reader.GetString(6),
                                                                            reader.GetBoolean(7),
                                                                            reader.GetBoolean(8),
                                                                            reader.GetString(9));

                            list.Add(item.ID, item);
                        }
                    }
                }
                catch (Exception exp)
                {
                    throw exp;
                }
            }
        }
    }

    /// <summary>
    /// Предоставляет метаданные описывающие справочник
    /// </summary>
    internal class MetaDictionary
    {
        //системный столбец определяющий разрешение на удаление записи
        internal const string INUSE = "In_Use";
        //столбец определяющий первичный ключ
        internal const string PRIMARY_KEY_NAME = "ID";


        public MetaDictionaryColumn PrimaryKey { get; protected set; }

        public Dictionary<int, MetaDictionaryColumn> ListColumns { get; protected set; }


        public int ID { get; protected set; }
        public string Name { get; protected set; }
        public string ViewName { get; protected set; }

        public bool IsUse_InUse { get; protected set; }

        public string ProcedureInsert { get; protected set; }
        public string ProcedureUpdate { get; protected set; }
        public string ProcedureDelete { get; protected set; }

        public bool Visible { get; protected set; }
        public bool ReadOnly { get; protected set; }

        public string OrderBy { get; protected set; }

        public MetaDictionary(int id, string name, string viewName, bool isUse, string pi, string pu, string pd, bool visible, bool readOnly, string orderBy)
        {
            ID = id;
            Name = name;
            ViewName = viewName;
            IsUse_InUse = isUse;

            ProcedureInsert = pi;
            ProcedureUpdate = pu;
            ProcedureDelete = pd;

            Visible = visible;
            ReadOnly = readOnly;
            OrderBy = orderBy;

            ListColumns = new Dictionary<int, MetaDictionaryColumn>();
        }

        /// <summary>
        /// Обновить информацию о справочнике
        /// </summary>
        public void Update()
        {
            GetDataDictionaryColumns(ListColumns);
        }

        /// <summary>
        /// Получить данные описывающие столбцы справочника
        /// </summary>
        /// <param name="list"></param>
        private void GetDataDictionaryColumns(Dictionary<int, MetaDictionaryColumn> list)
        {
            using (SqlConnection connection = new SqlConnection(MetaDictionaries.ConnectionString))//System.Configuration.ConfigurationManager.ConnectionStrings[MetaDictionaries.ConnectionString].ConnectionString))
            {
                SqlCommand command = new SqlCommand(string.Format("SELECT ID, Dictionary_ID, Name, ColumnName, Visible, Type, RefDictionary, RefColumn FROM Meta_Directories_Column WHERE Dictionary_ID = {0} ORDER BY Sort", ID), connection);

                try
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            MetaDictionaryColumn item = new MetaDictionaryColumn(reader.GetInt32(0),
                                                                                    reader.GetInt32(1),
                                                                                    reader.GetString(2),
                                                                                    reader.GetString(3),
                                                                                    reader.GetString(4),
                                                                                    reader.GetString(5),
                                                                                    GetReaderInt32(reader, 6),
                                                                                    GetReaderInt32(reader, 7));
                            list.Add(item.ID, item);

                            //определяем столбец первичного ключа
                            if (item.Name.Equals(PRIMARY_KEY_NAME, StringComparison.CurrentCultureIgnoreCase))
                            {
                                PrimaryKey = item;
                            }
                        }
                    }
                }
                catch (Exception exp)
                {
                    throw new Exception("Meta_Directories_Column ID = " + ID, exp);
                }
            }
        }

        private int GetReaderInt32(SqlDataReader reader, int index)
        {
            if (reader.IsDBNull(index))
            {
                return -1;
            }
            else
            {
                return reader.GetInt32(index);
            }
        }
    }

    internal enum ColumnType
    {
        TString,
        TBigString,
        TInt,
        TDecimal,
        TRef,
        TChild,
        TBool,
        TDateTime
    }

    /// <summary>
    /// Предоставляет метаданные описывающие столбец справочника
    /// </summary>
    internal class MetaDictionaryColumn
    {
        public int ID { get; set; }

        public int Dictionary_ID { get; protected set; }

        public string Name { get; protected set; }

        public string ColumnName { get; protected set; }

        public bool Visible { get; protected set; }

        public ColumnType Type { get; protected set; }

        public int RefId { get; protected set; }

        public int RefColumn { get; protected set; }


        public MetaDictionaryColumn(int id,
                                    int dictionary_ID,
                                    string name,
                                    string columnName,
                                    string visible,
                                    string type,
                                    int refId,
                                    int refColumn)
        {
            ID = id;
            Dictionary_ID = dictionary_ID;
            Name = name;
            ColumnName = columnName;
            Visible = visible == "1" ? true : false;
            Type = (ColumnType)Enum.Parse(typeof(ColumnType), type);
            RefId = refId;
            RefColumn = refColumn;
        }
    }
}