﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.Access;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport
{
    [MetadataType(typeof(TechnicalSupportEditViewModel_Valid))]
    public class TechnicalSupportEditViewModel : ViewModelBase
    {

        
        public TechnicalSupportEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;
            ReadOnly = ManagerAccess.CanEdit(new RepositoryDc<AccountingObject>().GetExisting(accountingObjectId)) ==
                       false;

            ManufacturerViewModel = new ManufacturerViewModel();
            CategoriesKindOfSupport = new SelectList(
                new RepositoryDc<CategoryKindOfSupport>().GetAllEx(
                    q => q.Where(cat => cat.Type == CategoryTypeOfSupportEnum.TechnicalSupport)),
                "Id", "Name");

            Characteristics = new List<TableCharacteristicOfTechnicalSupport>();

            var entity = id.HasValue
                ? new RepositoryDc<TechnicalSupport>().Get(
                    q => q.Fetch(x => x.Characteristics).Eager.Where(x => x.Id == id))
                : null;
            entity.Do(Fill);
        }

        protected void Fill(TechnicalSupport entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            CategoryKindOfSupportId = entity.CategoryKindOfSupport.Return(cat => cat.Id, default(int));
            ManufacturerViewModel.ManufacturerId = entity.Manufacturer.Return(manufacturer => manufacturer.Id, default(int));
            Amount = entity.Amount;
            Summa = entity.Summa;
            Characteristics = entity.Characteristics.Select(x => new TableCharacteristicOfTechnicalSupport(x));
        }

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        public ManufacturerViewModel ManufacturerViewModel { get; set; }

        [DisplayName(TableTechnicalSupport.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableTechnicalSupport.CategoryKindOfSupportCaption)]
        public int? CategoryKindOfSupportId { get; set; }

        [DisplayName(TableTechnicalSupport.AmountCaption)]
        public int Amount { get; set; }

        [DisplayName(TableTechnicalSupport.SummaCaption)]
        public object Summa { get; set; }

        public SelectList CategoriesKindOfSupport { get; set; }

        public IEnumerable<TableCharacteristicOfTechnicalSupport> Characteristics { get; set; }
    }

    internal class TechnicalSupportEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public int? CategoryKindOfSupportId { get; set; }

        [Required(ErrorMessage = RequiredText)]
        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int Amount { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public object Summa { get; set; }
    }
}