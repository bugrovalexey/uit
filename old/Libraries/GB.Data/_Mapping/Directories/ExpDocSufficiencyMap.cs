﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
	public class ExpDocSufficiencyMap : ClassMapBase<ExpDocSufficiency>
	{
        public ExpDocSufficiencyMap()
            : base("Exp_Doc_Sufficiency")
		{
//            Table("w_");

//            Id(x => x.Id, "Exp_Doc_Sufficiency_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            SqlInsert(@"exec ZZZ_Prc_Exp_Doc_Sufficiency_Insert
//						  @Exp_Doc_Sufficiency_ID = ?
//						 ,@Exp_Doc_Sufficiency_Name = ?").Check.None();


//            SqlUpdate(@"exec ZZZ_Prc_Exp_Doc_Sufficiency_Update
//						  @Exp_Doc_Sufficiency_ID = ?
//						 ,@Exp_Doc_Sufficiency_Name = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Doc_Sufficiency_Delete
//						  @Exp_Doc_Sufficiency_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Exp_Doc_Sufficiency_Name").Nullable();
        }
    }
}
