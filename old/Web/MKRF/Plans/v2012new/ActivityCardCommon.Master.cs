﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core;
using GB;
using GB.Entity;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Entities.Statuses;
using GB.MKRF.Helpers;
using GB.MKRF.Report;
using GB.MKRF.Repository;
using GB.MKRF.ViewModels;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class ActivityCardCommon : System.Web.UI.MasterPage
    {
        bool? enableEdit = null;

        PlansActivityController controller_internal = null;
        protected PlansActivityController Controller
        {
            get
            {
                if (controller_internal == null)
                {
                    controller_internal = new PlansActivityController();
                    try
                    {
                        enableEdit = null;
                        controller_internal.SetCurrentEdit(Request.QueryString[ActivityCard.ActivityIdKey]);
                    }
                    catch (Exception ex)
                    {
                        Logger.Fatal("Ошибка получения CurrentActivity - " + ex.Message);
                        ResponseHelper.DenyAccess(this.Response);
                    }
                }
                return controller_internal;
            }
        }


        /// <summary>
        /// Текущее мероприятие
        /// </summary>
        public PlansActivity CurrentActivity
        {
            get
            {
                return Controller.CurrentEdit;
            }
        }

        protected IList<ActivityLinkViewModel> Links
        {
            get
            {
                return new List<ActivityLinkViewModel>(); // Controller.GetLinks();
            }
        }

        protected string GetLinks(int LinkType)
        {
            var items = Links.Where(x => x.LinkType == LinkType).ToList();

            if (items.Any())
            {
                return "<ul style='text-align:left'>" +

                    string.Concat(
                        items.Select(x => string.Format("<li><a href='{0}'><span style='color:#C53939'>{1}</span> — {2}</a>{3}</li>",
                                                        x.IsDeleted ? HiddenActivity.GetUrl(x.Id, CurrentActivity.Id) : ActivityReadOnly.GetUrl(x.Id),
                                                        x.Code,
                                                        x.Name,
                                                        x.IsDeleted ? " [удалено]" : ""))
                             .ToArray()) +

                    "</ul>";
            }
            else
            {
                return "";
            }
        }

        //protected IList<ActivityViewModel> Owners
        //{
        //    get 
        //    {
        //        return null;
        //    }
        //}

        /*
         <% if (this.Owners.Any()) { %>
            <dxe:ASPxButton ID="hidButton" runat="server" ClientInstanceName="hidButton" AutoPostBack="false" Text="Объединено из ▼" CssClass="fleft topButton"></dxe:ASPxButton>
            <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CloseAction="OuterMouseClick" PopupElementID="hidButton" PopupVerticalAlign="Below" PopupHorizontalAlign="LeftSides"
                ShowFooter="False" ShowHeader="False" Width="760">
                <ContentCollection>
                    <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                        <ul>
                        <% foreach (var owner in this.Owners) { %>
                            <li /> <a href="<%=HiddenActivity.GetUrl(owner.Id)%>"><%= string.Format("<span style='color:#C53939'>{0}</span> — {1}", owner.Code, owner.Name) %></a>
                        <% } %>
                        </ul>
                    </dxpc:PopupControlContentControl>
                </ContentCollection>
            </dxpc:ASPxPopupControl>
            <% } %>         
         */

        /// <summary>
        /// Разрешено редактировать мероприятие ?
        /// </summary>
        public bool EnableEdit
        {
            get
            {
                if (enableEdit == null)
                {
                    if (CurrentActivity == null)
                        enableEdit = false;
                    else
                        enableEdit = Validation.Validator.Validate(CurrentActivity, ObjActionEnum.edit);
                }
                return enableEdit.Value;
            }
        }

        string nestedPage
        {
            get
            {
                return Request.Url.Segments[Request.Url.Segments.Length - 1];
            }
        }

        FavoriteActivity _favoriteActivity;
        FavoriteActivity favoriteActivity
        {
            get
            {
                if (_favoriteActivity != null) return _favoriteActivity;
                _favoriteActivity = RepositoryBase<FavoriteActivity>.GetAll(new GetAllArgs<FavoriteActivity>()
                {
                    Expression = (x => (x.User == UserRepository.GetCurrent() && x.PlansActivity == CurrentActivity))
                }).SingleOrDefault();
                return _favoriteActivity;
            }
        }

        public static string getReportUrl(PlansActivity activity)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (activity != null)
            {
                int Id = activity.Id;
                return ReportPage.GetUrl(ReportType.Plan2012NewActivityCard, Id, "");
            }

            return string.Empty;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (CurrentActivity != null)
            {
                btnFavorite.ImageUrl = isFavoriteActivity(CurrentActivity)
                    ? "/Content/GB/Styles/Imgs/favor16.png" : "/Content/GB/Styles/Imgs/favor16d.png";

                RoleBase role = UserRepository.GetCurrent().Role;

                // кнопка активно только для МКС в статусах "Отправлено в Минкульт" и "Предварительно согласовано"
                if (role.IsEq(RoleEnum.CA))
                {
                    btnAgree.Visible = StatusExtension.IsEq(StatusEnum.SendToMKRF, CurrentActivity.Plan.Demand.Status) ||
                                        StatusExtension.IsEq(StatusEnum.MKRFPreAgreed, CurrentActivity.Plan.Demand.Status);
                    btnAgree.Text = CurrentActivity.IsAgree ? "Не согласовывать" : "Согласовать";

                    btnChangeDep.Visible = true;
                    DepartmentGrid.DataBind();
                }
            }

            //btnCopy.Visible = 
            btnEditTop.Visible = 
            //btnViewTop.Visible = 
                                    EnableEdit;
        }

        protected string GetReadModeUrl(string id)
        {
            var pageName = new System.IO.FileInfo(this.Request.Url.LocalPath).Name;
            string anchor = null;
            switch (pageName.ToLower())
            {
                case "commondata.aspx": anchor = "CommonData"; break;
                case "functions.aspx": anchor = "Functions"; break;
                case "indicators.aspx": anchor = "Indicators"; break;
                case "specialinformation.aspx": anchor = "SpecialInformation"; break;
                case "expenses.aspx": anchor = "Expenses"; break;
                case "goalsresults.aspx": anchor = "Results"; break;
                case "works.aspx": anchor = "Works"; break;
                case "specifications.aspx": anchor = "Specification"; break;
            }
            if (anchor != null)
            {
                anchor = ("#" + anchor);
            }

            return string.Format("{0}?{1}={2}{3}", UrlHelper.Resolve("~/Plans/v2012new/ActivityReadOnly.aspx"), ActivityCard.ActivityIdKey, id, anchor);
        }

        protected string GetEditUrl(string id)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/Plans/v2012new/CommonData.aspx"), ActivityCard.ActivityIdKey, id);
        }

        protected string GetPlanInfo(PlansActivity activity)
        {
            if (activity == null)
                return null;

            string url = string.Empty;

            switch (activity.Plan.PlanType)
            {
                case ExpObjectTypeEnum.Plan2012new:
                case ExpObjectTypeEnum.SummaryPlanMKS2012new:
                //case ExpObjectTypeEnum.Plan2012rel:
                    url = UrlHelper.Resolve("~/Plans/v2012new/ActivityList.aspx");
                    break;
                case ExpObjectTypeEnum.Unallocated:
                    return string.Format("<a href='{0}?{1}={2}{5}'>{3} {4} г.</a>",
                                        UrlHelper.Resolve("~/Plans/v2012new/ActivityList.aspx"),
                                         PlanCard.plansIdKey, 
                                         PlanRepository.Get(x => x.PlanType == ExpObjectTypeEnum.SummaryPlanMKS2012new && x.Year == activity.Plan.Year).Id,
                                         "ПЛАН",
                                         activity.Plan.Year,
                                         string.IsNullOrEmpty(Request["from"])
                                             ? string.Empty
                                             : string.Format("&{0}={1}", ActivityList.ActivityFromKey,
                                                             Request["from"]));
            }

            return string.Format("<a href='{0}?{1}={2}{5}'>{3} {4} г.</a>",
                                url,
                                PlanCard.plansIdKey,
                                activity.Plan.Id,
                                "ПЛАН",
                                activity.Plan.Year,
                                string.IsNullOrEmpty(Request["from"])
                                    ? string.Empty
                                    : string.Format("&{0}={1}", ActivityList.ActivityFromKey,
                                                    Request["from"]));
        }

        protected string GetActivityTitle(PlansActivity activity)
        {
            if (activity == null)
                return null;

            return string.Format("<span style='color:#C53939'>{0}</span> — {1}", activity.Code, activity.Name.ToString().Trim());
        }

        protected string GetActivityTitle(int activityId)
        {
            if (PlansActivityRepository.IsHidden(activityId))
            {
                var owner = PlansActivityRepository.GetHidden(activityId, true);

                if (owner != null && 
                    owner.Tables.Contains("Plans_Activity") && 
                    owner.Tables["Plans_Activity"].Rows.Count > 0)
                {
                    var row = owner.Tables["Plans_Activity"].Rows[0];
                    string url = HiddenActivity.GetUrl(activityId);
                    return string.Format("<a href='{0}'><span style='color:#C53939'>{1}</span> — {2}</a>", url, row["Plans_Activity_Code"], row["Plans_Activity_Name"]);
                }
            }
            else
            {
                var owner = RepositoryBase<PlansActivity>.Get(activityId);
                if (owner != null)
                {
                    return string.Format("<span style='color:#C53939'>{0}</span> — {1}", owner.Code, owner.Name.Trim());
                }
            }
            return null;
        }

        protected string GetStatusInfo(PlansActivity activity)
        {
            if (activity.Plan == null)
                return null;

            if (activity.Plan.Demand == null)
                return "Заявка отсутствует";

            RoleBase role = UserRepository.GetCurrent().Role;

            if (role.IsEq(RoleEnum.CA) || role.IsEq(RoleEnum.Department))
                return UrlHelper.Link(activity.Plan.Demand.Status.Name, "~/Plans/Workflow.aspx", PlanCard.plansIdKey + "=" + activity.Plan.Id, "color:#C53939");
            else
                return activity.Plan.Demand.Status.Name;
        }



        protected void ActivityCard_Callback_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            if (e.Parameter == "agree")
            {
                CurrentActivity.IsAgree = !CurrentActivity.IsAgree;
                RepositoryBase<PlansActivity>.SaveOrUpdate(CurrentActivity);
                e.Result = CurrentActivity.IsAgree ? "Agree" : "DisAgree";
            }
            if (e.Parameter == "favorite")
            {
                e.Result = NewFavoriteValue(UserRepository.GetCurrent(), CurrentActivity).ToString();
            }
            else if (e.Parameter == "copy")
            {
                    var copyID = CopyHelper.CopyActivity(CurrentActivity);
                    var pa = RepositoryBase<PlansActivity>.Get(copyID);
                    RepositoryBase<PlansActivity>.SaveOrUpdate(pa);
                    RepositoryBase<Plan>.Refresh(CurrentActivity.Plan);
                    NumHelper<PlansActivity>.OrderNums(PlanRepository.GetActivityListWithoutSecurity(CurrentActivity.Plan));
                    e.Result = "OK";
            }
            else if (e.Parameter.StartsWith("changeDep"))
            {
                var ids = e.Parameter.Split('|');
                
                var argsMove = new PlansActivityArgs()
                {
                    Id = Convert.ToInt32(ids[1]),
                    NewDepartamentId = Convert.ToInt32(ids[2])
                };
                var controller = new PlansActivityController();
                var newPlanId = controller.ChangeDepartament(argsMove);

                //Response.Redirect(ActivityList.GetUrl(newPlanId));
                e.Result = "changeComplete_" + newPlanId.ToString();
            }
        }
        

        private bool NewFavoriteValue(User user, PlansActivity act)
        {
            if (favoriteActivity == null)
            {
                FavoriteActivity f = new FavoriteActivity();
                f.PlansActivity = act;
                f.User = user;
                f.IsFavorite = true;
                RepositoryBase<FavoriteActivity>.SaveOrUpdate(f);
                _favoriteActivity = f;
                return true;
            }
            else
            {
                favoriteActivity.IsFavorite = !favoriteActivity.IsFavorite;
                RepositoryBase<FavoriteActivity>.SaveOrUpdate(favoriteActivity);
                return favoriteActivity.IsFavorite;
            }
        }

        bool isFavoriteActivity(PlansActivity activity)
        {
            if (favoriteActivity == null || !favoriteActivity.IsFavorite)
            {
                return false;
            }
            else return true;
        }

        protected string CallbackParams()
        {
            return UserRepository.GetCurrent().Id + ";" + CurrentActivity.Id;
        }

        public void SetBtnViewTopChecked()
        {
            btnViewTop.Checked = true;
        }

        protected string linkPrev(string Direction)
        {
            if (nestedPage == "ActivityReadOnly.aspx") return string.Empty;
            var acts = RepositoryBase<PlansActivity>.GetAll(new GetAllArgs<PlansActivity>
            {
                Expression = x=>x.Plan == CurrentActivity.Plan,
                OrderByAsc = x=>x.Code
            }).ToArray();

            string fileName, linkAdress, title;
            int actId = 0;
            if (Direction == "Prev")
            {
                fileName = "prev_blue.png";
                title = "Предыдущее мероприятие";
                for (int i = 0; i < acts.Length; i++ )
                {
                    if (acts[i].Id == CurrentActivity.Id)
                    {
                        if (i != 0)
                        {
                            actId = acts[i - 1].Id;
                        }
                        else actId = -1;
                    }
                }
            }
            else
            {
                fileName = "next_blue.png";
                title = "Следующее мероприятие";
                for (int i = 0; i < acts.Length; i++)
                {
                    if (acts[i].Id == CurrentActivity.Id)
                    {
                        if (i != acts.Length-1)
                        {
                            actId = acts[i + 1].Id;
                        }
                        else actId = -1;
                    }
                }
            }

            switch (nestedPage)
            {
                #region case 
                case "CommonData.aspx":
                    linkAdress = CommonData.GetUrl(actId);
                    break;

                case "Indicators.aspx":
                    linkAdress = Indicators.GetUrl(actId);
                    break;
                
                case "Expenses.aspx":
                    linkAdress = Expenses.GetUrl(actId);
                    break;

                case "Functions.aspx":
                    linkAdress = Functions.GetUrl(actId);
                    break;

                case "GoalsResults.aspx":
                    linkAdress = GoalsResults.GetUrl(actId);
                    break;
                case "Works.aspx":
                    linkAdress = Works.GetUrl(actId);
                    break;
                case "Specifications.aspx":
                    linkAdress = Specifications.GetUrl(actId);
                    break;
                case "Purchases.aspx":
                    linkAdress = Purchases.GetUrl(actId);
                    break;

                case "InitialActivity.aspx":
                    linkAdress = InitialActivity.GetUrl(actId);
                    break;

                default:
                    throw new Exception("Unknown pagename " + nestedPage);
                #endregion
            }

            return actId > -1 ? String.Format("<a title={1} class='linksPrevNext' href='"+linkAdress+@"'>
                <img class='linksPrevNext' src='/Content/Style/Coord/img/{0}' />
                </a>", fileName, title) : string.Empty;
        }

        protected void DepartmentGrid_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.SelectCheckbox && 
                DepartmentGrid.GetRowValues(e.VisibleIndex, "CanTransfer").ToString().ToLower()=="false" )
                e.Enabled = false;
        }

        protected void DepartmentGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var year = CurrentActivity.Plan.Year;

            var existsDep = RepositoryBase<Plan>.GetAll(p => p.Year == year).Select(x => x.Department.Id);

            DepartmentGrid.DataSource = RepositoryBase<Department>.GetAll(u => u.Id != CurrentActivity.Plan.Department.Id 
                                                                            && u.Type.Id == (int)DepartmentTypeEnum.Department)
                .Select(
                x => new
                {
                    Id = x.Id,
                    CanTransfer = existsDep.Contains(x.Id),
                    Name = x.NameSmall
                });
        }
    }
}