function Popup(zIndex) {

    this.popover = popover;
    this.minipopup = minipopup;
    this.closeContainer = closeContainer;

        
    var currentOverlay;
    var popupseed = zIndex || 0;
    var zIndexSeed = 500;

    if (zIndex) {
        zIndexSeed = zIndex;
    }

    //---------------------------------------------------------------------------------------------------
    // public functions
    function minipopup(html_or_elements, el, options) {
        options = $.extend({}, options);

        //options.bottom = true;
        popupseed++;
        var _el = $(el);
        var offset = _el.offset();

        var _container = $("<div id='mdropopup_" + popupseed + "' class='close-handler' style='visibility:hidden'></div>");

        var _parentContainer = _el.closest('.close-handler');
        if (_parentContainer.length) {
            _parentContainer[0].childContainers = _parentContainer.childContainers || [];
            _parentContainer[0].childContainers.push(_container);
        }

        var shadow = !options.bottom ? "2px 2px 5px #888" : "2px 1px 4px #888";
        var _popup = $("<div class='popup_panel' style='padding: 10px;'></div>");

        //var _popup = $("<div></div>");

        _popup.css("z-index", ++zIndexSeed);
        var _popupArrow;
        if (!options.bottom) {
            _popupArrow = $("<img src='/Content/Style/all/img/popup_arrow2.png' alt='' style='position: absolute; top: -11px;'/>");
        } else {
            _popupArrow = $("<img src='/Content/Style/all/img/popup_arrow_bottom2.png' alt='' style='position: absolute; bottom: -11px;'/>");
        }
        if (options.right === undefined && !options.firstLetterPosition) {
            var center = offset.left + _el.outerWidth() / 2;
            options.right = center > $(window).width() / 2;
        }
        //var 
        _popupArrow.css("z-index", ++zIndexSeed);
        _popupArrow.css(options.right ? { right: "23px" } : { left: "13px" });
        var _closeBox = $("<img src='/Content/Style/all/img/popup_close.png' alt='' style='position: absolute; right: 5px; top: 5px; cursor:pointer;'/>");
        _closeBox.css("z-index", zIndexSeed);
        _container.append(_popup);
        _popup.append(_popupArrow);
        if (!options.disableCloseBox) {
            _popup.append(_closeBox);
        }
        _popup.append(html_or_elements);

        //    // positioningFunc allows setting some rules on size/position of the popup
        //    // we'll have some predefiend functions (for static, for element-related and for buildings as custom thing).
        function closePopup(recursive) {
            //            log("Closing popup...");
            if (!confirmUnsavedForm(_container)) return false;
            _container.remove();
            $("body").unbind("mousedown", bodyMouseDownHandler);
            if (options.close) options.close();
            if (recursive && _parentContainer[0]) {
                _parentContainer[0].close();
            }
        }
        _container[0].close = closePopup;
        this.close = closePopup;

        function bodyMouseDownHandler(e) {
            function isEventInsideContainer(c, e) {
                //                log("isEventInsideContainer", c, c.childContainers, e);
                if (c == e.target || $(c).has(e.target).length) return true;
                for (var i = 0; c.childContainers && i < c.childContainers.length; i++) {
                    if (isEventInsideContainer(c.childContainers[i], e)) return true;
                }
                return false;
            }

            if (!isEventInsideContainer(_container[0], e)) {
                closePopup();
            }
        }
        $("body").mousedown(bodyMouseDownHandler);
        _closeBox.one("click", function () { closePopup(); });
        $("#" + _container.attr('id') + " .popup-close").live('click', closePopup);


        $("body").prepend(_container);
        // position & show
        _popup.css("position", "absolute");
        if (!options.bottom) {
            //            log(offset.top, _el.outerHeight());
            _popup.css("top", offset.top + _el.outerHeight() + 5);
        } else {
            _popup.css("top", offset.top - _popup.outerHeight() - 5);
        }
        //log(offset);
        if (!options.firstLetterPosition) {
            _popup.css(options.right ? { right: $(window).width() - offset.left - _el.outerWidth() - 10 } : { left: offset.left });
        } else {
            _popup.css("left", offset.left - 27 + (_el.outerWidth() / 2));
        }
        _popup.fadeIn("fast");
        _container.css("visibility", "visible");
    }

    function popover(options) {
        var url = options.url,
            method = options.method || "GET",
            data = options.data,
            html = options.html,
            width = options.width || 800,
            height = options.height,
            scroll = options.scroll !== undefined ? options.scroll : false,
            //back = options.back === undefined ? !height : options.back,
            onclose = options.onclose,
            onload = options.onload,
            showTitle = options.showTitle !== undefined ? options.showTitle : true;

        if (options.zIndex) {
            zIndexSeed = options.zIndex;
        }

        if (!html == !url) alert("html or url shoud be supplied but not both.");

        var _contentHeight = 0;

        var _shadow = $("<div class='popup_background'></div>");
        $(".popup_background").live('click', function () { closePopup(); });
        _shadow.css("z-index", ++zIndexSeed);

        popupseed++;
        var _container = $("<div style='margin:auto; position: relative;' id='mdropopup_" + popupseed + "' class='close-handler'></div>");
        _container.css("z-index", ++zIndexSeed);

        var _content = $("<div class='popup_panel' style='padding: 6px 0 0 0;'></div>");
        var _titleContent = $("<div class='popup_title'></div>");
        var _innerContent = $("<div class='scroll-async-reset'></div>");

        var _closeBox = $("<div class='popup_close '><i class='material-icons'>close</i></div>");
        _closeBox.css("z-index", ++zIndexSeed);

        _content.appendTo(_container);
        //if (back) {
        //    _backButton.prependTo(_titleContent);
        //}
        if (showTitle) {
            _titleContent.appendTo(_content);
            _closeBox.appendTo(_content);
        }
        _innerContent.appendTo(_content);

        var request;

        // prevent mousewheel
        _shadow.mousewheel(function (e) { e.preventDefault(); });
        _content.mousewheel(function (e, delta) {
            if (delta > 0 && _innerContent.scrollTop() == 0 || delta < 0 &&
                    _innerContent[0].scrollHeight - _innerContent.outerHeight() <= _innerContent.scrollTop() + 2) {
                e.preventDefault();
            }
        });

        var isDragging = false;
        var dragX;
        var dragY;
        var altX = 0;
        var altY = 0;
        _titleContent.mousedown(function (e) {
            if (!isDragging) return;
            //            log("mousedown", e);
            dragX = e.pageX - altX;
            dragY = e.pageY - altY;
            isDragging = true;
        });
        function handleMove(e) {
            if (!isDragging) return;
            //            log("mousemove", e);
            altX = e.pageX - dragX;
            altY = e.pageY - dragY;
            setPosition();
        }
        function handleMouseUp(e) {
            if (!isDragging) return;
            //            log("mouseup", e);
            isDragging = false;
        }
        $(window).mousemove(handleMove).mouseup(handleMouseUp);

        function setPosition() {
            _contentHeight = _content.height();
            var totalWidth = $(window).width();
            var totalHeight = $(window).height();
            var goodHeight = totalHeight * 90 / 100;

            //if (_contentHeight + Math.floor((totalHeight - goodHeight) / 2) >= Math.floor(goodHeight) + 15) {

            //    var localHeight = height;

            //    if (!localHeight) {
            //        localHeight = goodHeight - 60;
            //    }

            //    _innerContent.css("height", localHeight);
            //}
            //else {
            //    _innerContent.css("height", '');
            //}

            //if (scroll) {
            //    _innerContent.css({ "overflow-y": "scroll", "overflow-x": "hidden" });
            //}

            // calculate left, top
            var left = (totalWidth - width) / 2;
            var top = (totalHeight - _contentHeight) / 2;

            if (top > 40) top = 40;

            var goodTop = (totalHeight - goodHeight) / 2;

            if (top < goodTop)
                top = goodTop;

            top = 110;

            

            var height = totalHeight - (110 + 50); // window.height - (header+padd) - bottom margin

            //_content.width(width).css({ left: left + altX, top: top + altY });
            _content.width(width).css({ left: left, top: top, height: height });
        }

        this.setPosition = setPosition;

        $(window).resize(setPosition);

        function closePopup() {
            // display confirmation box is there's something dirty inside popup
            if (!confirmUnsavedForm(_content)) return false;

            $(window).unbind("resize", setPosition).unbind("mousemove", handleMove).unbind("mouseup", handleMouseUp);
            if (request) request.abort();
            _shadow.remove();
            _container.remove();
            if (onclose) onclose();
            currentOverlay = null;
            return false;
        }

        $("#" + _container.attr('id') + " .popup_close").live('click', closePopup);
        _container[0].close = closePopup;

        function showHtml(el) {
            //������� ������ ������� ��������
            var _el = $(el);

            // discover and set title
            var title = _el.filter("h1:first").hide().html();
            if (!title) {
                title = _el.find("h1").first().hide().html();
            }

            _titleContent.append(title);

            _el.appendTo(_innerContent);
            _content.fadeIn("fast");

            _content.width(width).css({ left: 0, top: 0 });
            //            _contentHeight = _content.height();

            setPosition();
        }

        function showRootElements() {
            $("body").prepend(_container);
            $("body").prepend(_shadow);
        }

        //showRootElements();

        if (html) {
            showHtml(html);
        } else {
            // add racing with timer
            request = $.ajax({
                url: url,
                data: data,
                type: method,
                cache: false,
                dataType: "html",
                success: function (result) {
                    //_loading.hide();
                    showRootElements();
                    showHtml(result);
                    if (onload) onload();
                }, error: function (xhr, ajaxOptions, thrownError) {
                    if (window.console && console.log) {
                        console.log(xhr.status);
                        console.log(ajaxOptions);
                        console.log(thrownError);
                        console.log('request url: ' + url);
                    }
                    alert("Error.");
                    closePopup();
                }
            });
        }
        currentOverlay = this;
        this.close = closePopup;
    }

    function closeContainer(innerElement) {
        //log("closeContainer", innerElement);

        $(innerElement).closest('.close-handler')[0].close();
    }

    //---------------------------------------------------------------------------------------------------
    // private functions
    function confirmUnsavedForm(element) {
        var hasDirtyForms = $("form.dirty", element).length;
        return !hasDirtyForms || confirm("The form contains unsaved data.\n\nClose it anyway?");
    }


}


(function (window) {

    // export
    window.popup = new Popup();

})(window);