﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImproveInterfaceButton.ascx.cs"
    Inherits="GB.MKRF.Web.Common.ImproveInterfaceButton" %>
<%@ Import Namespace="GB.Helpers" %>

<script type="text/javascript">

    function ShowImpWindow() {
        impPopupWindow.Show();
        return false;
    };

    function successResponse(s, e) {
        var str = e.result;
        $("#<%=MessageMemo.ClientID%>").find("textarea").val(''); //clear message textarea
        if (str == "ok") {
            impPopupWindow.Hide();
        }
    };
</script>
<dxc:ASPxCallback runat="server" ID="impCallBack" ClientInstanceName="impCallBack" 
    oncallback="impCallBack_Callback">
    <ClientSideEvents CallbackComplete="function(s, e) { successResponse(s, e); }" />
</dxc:ASPxCallback>

<img src="/Content/GB/Styles/Imgs/idea.png" alt="Есть идея?" style="top: -3px; position: relative;" title="Есть предложение по улучшению системы?" onclick="ShowImpWindow();return false;" />

<dxpc:ASPxPopupControl ID="impPopupWindow" runat="server" ClientInstanceName="impPopupWindow"
    CloseAction="CloseButton" HeaderText="Отправка отзыва" Modal="True" PopupHorizontalAlign="WindowCenter"
    PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="True" Width="700px">
    <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
            <div style="padding-bottom: 5px;">
                Опишите пожалуйста, Ваше предложение по улучшению интерфейса:
            </div>
            <dxe:ASPxMemo runat="server" ID="MessageMemo" ClientInstanceName="MessageMemo" 
                    Width="100%" Height="100px">
                </dxe:ASPxMemo>
            <br />
            <br />
            <div style="float: right;">
                <table>
                    <tr>
                        <td>
                        <dxe:ASPxButton ID="sendReq" runat="server" AutoPostBack="False" Text="Отправить"
                                Width="80px">
                                <ClientSideEvents Click="function(s, e) { impCallBack.PerformCallback(MessageMemo.value); }" />
                            </dxe:ASPxButton>
                        </td>
                        <td>
                            <dxe:ASPxButton ID="CloseButton" runat="server" AutoPostBack="False" CommandName="Close"
                                Width="80px" Text="Закрыть">
                                <ClientSideEvents Click="function(s, e) { impPopupWindow.Hide(); }" />
                            </dxe:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" height="11px" />
                    </tr>
                </table>
            </div>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>
