﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityDocumentDto
    {
        public int ActivityId { get; set; }

        public int? Id { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public DateTime? DateAdoption { get; set; }

        public string ParticleOfDocument { get; set; }

        public CreateDocumentFileData file { get; set; }
    }
}
