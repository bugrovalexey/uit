﻿using bgTeam.IsRui.Domain;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Helpers.Control
{
    public class IdNameSelectListWithNull : SelectList
    {
        private const string DefaultNullText = "-";
        private const string IdText = "Id";
        private const string NameText = "Name";

        public IdNameSelectListWithNull(IEnumerable<IEntityName> values, object selectedValue, string nullText = null)
            : base(GetItems(values, nullText), IdText, NameText, selectedValue)
        {
        }

        //public IdNameSelectListWithNull(IEnumerable<IEntityName> values, object selectedValue, string nullText = null)
        //    : base(GetItems(values, nullText), IdText, NameText, selectedValue)
        //{
        //}

        private static IEnumerable GetItems(IEnumerable<IEntityName> values, string nullText)
        {
            yield return new { Id = (int?)null, Name = nullText ?? DefaultNullText };
            foreach (var v in values)
            {
                yield return new { Id = v.Id, Name = v.Name };
            }
        }

        //private static IEnumerable GetItems(IEnumerable<IEntityName> values, string nullText)
        //{
        //    yield return new { Id = (int?)null, Name = nullText ?? DefaultNullText };
        //    foreach (var v in values)
        //    {
        //        yield return new { Id = v.Id, Name = v.Name };
        //    }
        //}
    }
}