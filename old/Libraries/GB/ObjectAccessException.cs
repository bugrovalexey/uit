﻿using System;

namespace GB
{
    /// <summary>
    /// Нет доступа к объекту
    /// </summary>
    [Serializable]
    public class ObjectAccessException : Exception
    {
    }
}