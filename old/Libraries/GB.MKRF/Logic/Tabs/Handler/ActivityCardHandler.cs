﻿namespace GB.MKRF.Logic.Tabs.Handler
{
    internal class ActivityCardHandler : TabsBaseHandler
    {
        public ActivityCardHandler()
            : base("Activities")
        {
            Add("Общая информация", "CommonDataSingle");
            //Add("Характеристики ОУ", "CharacteristicsSingle");
            Add("Информационное взаимодействие", "InformationInteraction");
            Add("Расходы", "ExpensesSingle");
            Add("Услуги", "ServicesSingle");
            Add("Показатели и индикаторы", "IndicatorsAndMarksSingle");
            Add("Работы", "WorksSingle");
            Add("Товары", "GoodsSingle");
            Add("Основания", "ReasonsSingle");
            Add("Экспертиза", "Expertise");
        }
    }
}