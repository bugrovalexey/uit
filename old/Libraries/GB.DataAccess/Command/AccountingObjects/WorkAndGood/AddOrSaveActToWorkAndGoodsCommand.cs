﻿using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.WorksAndGoods;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.WorkAndGood
{
    public class AddOrSaveActToWorkAndGoodsCommand : ICommand<AddOrSaveActToWorkAndGoodsContext, WorkAndGoods>
    {
        ISessionFactoryGb factory;

        public AddOrSaveActToWorkAndGoodsCommand()
        {
            factory = new SessionFactory();
        }

        public WorkAndGoods Execute(AddOrSaveActToWorkAndGoodsContext context)
        {
            WorkAndGoods workAndGoods = new Repository<WorkAndGoods>().Get(context.NewId);

            if (context.Id == 0)
            {
                if (context.ActsOfAcceptanceId.HasValue)
                {
                    workAndGoods.ActOfAcceptance = new Repository<ActOfAcceptance>().Get(context.ActsOfAcceptanceId);
                    
                    using (var session = factory.Create())
                    {
                        session.SaveOrUpdate(workAndGoods);
                        session.Commit();
                    }
                }
            }
            else if (context.Id != context.NewId)
            {
                var lastEntity = new Repository<WorkAndGoods>().Get(context.Id);
                lastEntity.ActOfAcceptance = null;
                workAndGoods.ActOfAcceptance = new Repository<ActOfAcceptance>().Get(context.ActsOfAcceptanceId);                

                using (var session = factory.Create())
                {
                    session.SaveOrUpdate(lastEntity);
                    session.SaveOrUpdate(workAndGoods);
                    session.Commit();
                }
            }

            return workAndGoods;
        }
    }
}
