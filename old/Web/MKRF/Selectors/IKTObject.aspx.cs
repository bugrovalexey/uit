﻿using System;
using System.Linq;
using GB;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Controls;

namespace GB.MKRF.Web.Selectors
{
    public partial class IKTObjectSelector : System.Web.UI.Page
    {
        protected IKTObjectController controller = new IKTObjectController();

        protected void TextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            ASPxTextBox box = (ASPxTextBox)sender;
            switch (box.ID)
            {
                case "NumberTextBox":
                    box.Text = controller.CurrentEdit.Number;
                    break;

                case "NameTextBox":
                    box.Text = controller.CurrentEdit.Name;
                    break;

                case "ShortNameTextBox":
                    box.Text = controller.CurrentEdit.ShortName;
                    break;
            }
        }

        #region grid
        protected override void OnPreRender(EventArgs e)
        {
            IKTObjectGrid.DataBind();

            base.OnPreRender(e);
        }
        protected void IKTObjectGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            IKTObjectGrid.DataSource = DirectoriesRepositary.GetDirectories<IKTObject>(DateTime.Now.Year); //controller.GetIKTObjectList();
        }

        protected void IKTObjectGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "$(this).addClass(\"grid_mouseover\")");
            e.Row.Attributes.Add("onmouseout", "$(this).removeClass(\"grid_mouseover\")");
        }
        protected void IKTObjectGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            controller.Create(new IKTObjectControllerArgs
                {
                    Values = hfIKTObject.ToDictionary(x => x.Key, x => x.Value)
                });
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void IKTObjectGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            controller.Update(new IKTObjectControllerArgs
                {
                    IKTObjectId = e.Keys[0],
                    Values = hfIKTObject.ToDictionary(x => x.Key, x => x.Value)
                });
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void IKTObjectGrid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }
        #endregion

        protected void StatusComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = controller.GetIKTObjectStatuses();

            if (controller.CurrentEdit != null && controller.CurrentEdit.Status != null)
            {
                box.Value = controller.CurrentEdit.Status.Id;
            }
            else
            {
                box.Value = EntityBase.Default;
            }
        }

        protected void CreationDate_DateEdit_DataBinding(object sender, EventArgs e)
        {
            ASPxDateEdit box = sender as ASPxDateEdit;
            if (controller.CurrentEdit != null && controller.CurrentEdit.CreationDate.HasValue)
            {
                box.Value = controller.CurrentEdit.CreationDate.Value;
            }
        }
    }
}