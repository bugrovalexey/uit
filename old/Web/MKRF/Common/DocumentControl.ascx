﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentControl.ascx.cs" Inherits="GB.MKRF.Web.Common.DocumentControl" %>
<%@ Import Namespace="GB" %>

<script type="text/javascript">
    function hfSave(e) {
        hfDoc.Set('Name', NameTextBox.GetText());
        hfDoc.Set('Num', NumTextBox.GetText());
        hfDoc.Set('File', uploader2.GetFiles());
    }

    function checkECP() {
        callBack.PerformCallback('');
        LoadingPanel.Show();
    }

    function ecpResponse(s, e) {
        LoadingPanel.Hide();
        var str = e.result;
        alert(str);
    }
</script>
<div id="panel">
    <dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" 
        oncallback="callBack_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { ecpResponse(s, e); }" />
    </dxc:ASPxCallback>
    <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
        Modal="True" ContainerElementID="panel" 
        Text="Проверка подписи&amp;hellip;">
    </dxlp:ASPxLoadingPanel>
    <dxe:ASPxButton ID="addButton" runat="server" ClientInstanceName="addButton" AutoPostBack="false"
        Text="Добавить" Style="margin-top: 10px; margin-left: 10px; margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {docsGrid.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxhf:ASPxHiddenField runat="server" ID="hfDoc" ClientInstanceName="hfDoc" />
    <dxwgv:ASPxGridView runat="server" ID="docsGrid" Width="100%" KeyFieldName="Id" AutoGenerateColumns="False"
        ClientInstanceName="docsGrid" OnBeforePerformDataSelect="docsGrid_BeforePerformDataSelect"
        OnInitNewRow="docsGrid_InitNewRow" OnRowDeleting="docsGrid_RowDeleting" OnRowInserting="docsGrid_RowInserting"
        OnRowUpdating="docsGrid_RowUpdating" OnStartRowEditing="docsGrid_StartRowEditing"
        OnCancelRowEditing="docsGrid_CancelRowEditing" OnCommandButtonInitialize="docsGrid_CommandButtonInitialize">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" VisibleIndex="0" AllowDragDrop="False"
                ShowInCustomizationForm="False" Width="150px">
                <EditButton Visible="True">
                </EditButton>
                <DeleteButton Visible="True">
                </DeleteButton>
                <ClearFilterButton Visible="True">
                </ClearFilterButton>
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" VisibleIndex="1">
                <Settings FilterMode="DisplayText" AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Номер документа" FieldName="Num" VisibleIndex="2">
                <Settings FilterMode="DisplayText" AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataDateColumn Caption="Дата добавления" FieldName="Date" VisibleIndex="4" />
            <dxwgv:GridViewDataTextColumn VisibleIndex="5" ShowInCustomizationForm="False">
                <Settings AllowDragDrop="False" />
                <DataItemTemplate>
                    <%#getDownloadUrl((int)Eval("Id"), (bool)Eval("Access"))%>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Settings ShowFilterRow="True" ShowHeaderFilterBlankItems="False" />
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsPager PageSize="100" />
        <SettingsEditing Mode="EditForm" />
        <Styles>
            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
        </Styles>
        <Styles>
            <LoadingDiv BackColor="#777777" Opacity="50"></LoadingDiv>
        </Styles>
        <Templates>
            <EditForm>
                <table style="margin: 10px">
                    <tr>
                        <td class="userCapt">
                            Наименование:
                        </td>
                        <td>
                            <dxe:ASPxTextBox ID="NameTextBox" runat="server" ClientInstanceName="NameTextBox"
                                ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>" Width="500px"
                                OnDataBinding="NameTextBox_DataBinding">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="userCapt">
                            Номер:
                        </td>
                        <td>
                            <dxe:ASPxTextBox ID="NumTextBox" runat="server" ClientInstanceName="NumTextBox" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                Width="500px" OnDataBinding="NumTextBox_DataBinding">
                                <%--                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                            </ValidationSettings>--%>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td class="userCapt">
                            <div id="docLabel">
                                Файл:
                            </div>
                        </td>
                        <td>
                            <cc:FileUploadControl2 runat="server" ID="uploader2" Mode="OneFile" ClientInstanceName="uploader2"
                                OnDataBinding="uploader2_DataBinding" AutoUpload="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td style="width: 98%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 1%"  class="form_edit_buttons" >
                                        <span class="ok">
                                            <dxwgv:ASPxGridViewTemplateReplacement ID="ASPxGridViewTemplateReplacement1" runat="server"
                                                ReplacementType="EditFormUpdateButton"></dxwgv:ASPxGridViewTemplateReplacement>
                                        </span>
                                    </td>
                                    <td style="width: 1%"  class="form_edit_buttons" >
                                        <span class="cancel">
                                            <dxwgv:ASPxGridViewTemplateReplacement ID="ASPxGridViewTemplateReplacement2" runat="server"
                                                ReplacementType="EditFormCancelButton"></dxwgv:ASPxGridViewTemplateReplacement>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e)
        { 
            if (e.command=='STARTEDIT') 
                hfDoc.Clear(); 
            if (e.command=='UPDATEEDIT') 
                hfSave(e); 
        }" EndCallback="function(s, e) {
            hfDoc.Set('FilePath', '');
            hfDoc.Set('FileName', '');
    }" />
    </dxwgv:ASPxGridView>
    <dxc:ASPxCallback runat="server" ID="RoleCallBack" 
        ClientInstanceName="RoleCallBack" oncallback="RoleCallBack_Callback" >
        <ClientSideEvents CallbackComplete="function(s, e) { PopupWindow.Hide(); }" />
    </dxc:ASPxCallback>
    <dxpc:ASPxPopupControl ID="PopupWindow" runat="server" ClientInstanceName="PopupWindow"
        CloseAction="CloseButton" HeaderText="Настройка доступа" Modal="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="True" 
        Width="700px" >
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <dxwgv:ASPxGridView runat="server" ID="RoleGrid" ClientInstanceName="RoleGrid"
                    AutoGenerateColumns="False"
                    Width="100%" KeyFieldName="Id" OnCustomCallback="RoleGrid_CustomCallback" >
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Роль" FieldName="Name"
                            VisibleIndex="2">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewCommandColumn Caption=" " ShowSelectCheckbox="True"  VisibleIndex="3"
                            Width="30px">
                        </dxwgv:GridViewCommandColumn>
                        <dxwgv:GridViewDataTextColumn Caption=" " FieldName="DocId" VisibleIndex="4" Visible="false" />
                    </Columns>
                    <SettingsPager PageSize="20">
                    </SettingsPager>
                    <Styles>
                        <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        </Header>
                    </Styles>
                    <ClientSideEvents EndCallback="function(s, e) { PopupWindow.Show(); }" />
                </dxwgv:ASPxGridView>
                <br />
                <div style="float: right;">
                    <table>
                        <tr>
                            <td>
                                <dxe:ASPxButton ID="SaveButton" runat="server" AutoPostBack="False" Text="Сохранить" Width="80px">
                                    <ClientSideEvents Click="function(s, e) { RoleCallBack.PerformCallback(''); }" />
                                </dxe:ASPxButton>
                            </td>
                            <td>
                                <dxe:ASPxButton ID="CloseButton" runat="server" AutoPostBack="False" CommandName="Close" Width="80px"
                                    Text="Закрыть">
                                    <ClientSideEvents Click="function(s, e) { PopupWindow.Hide(); }" />
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="11px" />
                        </tr>
                    </table>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
</div>
