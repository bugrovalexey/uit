﻿namespace GB.Data.Directories
{
    /// <summary>Группа товаров</summary>
    public class ProductGroup : EntityBase, IEntityName
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }
    }
}