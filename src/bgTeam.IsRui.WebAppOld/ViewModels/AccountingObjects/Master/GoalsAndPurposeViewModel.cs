﻿using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class GoalsAndPurposeViewModel: StepBaseViewModel
    {
        #region consts

        private const string TargetsCaption = "Цели создания объекта учета";
        private const string PurposeAndScopeCaption = "Назначение и область применения объекта учета";

        #endregion consts

        public GoalsAndPurposeViewModel(int id) 
            : base(id, MasterItemEnum.GoalsAndPurpose)
        {            
        }

        [DisplayName(TargetsCaption)]
        public string Targets
        {
            get { return Current.Targets; }
        }

        [DisplayName(PurposeAndScopeCaption)]
        public string PurposeAndScope
        {
            get { return Current.PurposeAndScope; }
        }

    }
}