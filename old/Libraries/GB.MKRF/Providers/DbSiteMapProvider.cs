﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Linq;
using GB.Entity;
using GB.Repository;
using GB.Extentions;

namespace GB.Providers
{
    [Obsolete("Используйте GbSiteMapProvider")]
    public class DbSiteMapProvider : StaticSiteMapProvider
    {
        const char _separator = '/';
        
        private readonly object smLock = new object();

        private SiteMapNode _root;
        private Dictionary<string, string> _AccessList;


        public DbSiteMapProvider()
        {
            this.BuildSiteMap();
        }

        /// <summary>
        /// Проверить есть ли у пользователя доступ к странице.
        /// </summary>
        /// <param name="page">Страница.</param>
        /// <param name="user">Пользователь.</param>
        internal bool СheckAccessUser(string page, UserBase user)
        {
            var mass = page.Split(_separator);

            for (int i = mass.Length - 1; i > 0; i--)
            {
                var str = string.Empty;

                for (int j = i; j > 0; j--)
                {
                    str = string.Concat(_separator, mass[j], str);
                }

                var access = checkAccess(string.Concat(mass[0], str), user);

                if (access.HasValue)
                    return access.Value;
            }

            return false;
        }

        private bool? checkAccess(string path, UserBase user)
        {
            if (!_AccessList.ContainsKey(path))
                return null;

            foreach (var item in user.Roles)
            {
                if (_AccessList[path].Contains(item.Name))
                    return true;
            }

            return false;
        }
        
        public string GetTitlePage(string page)
        {
            //TODO : Реализовать метод

            return null;
        }

        /// <summary>
        /// Сбрасывает карту сайта
        /// </summary>
        public void Reset()
        {
            lock (this.smLock)
            {
                this.Clear();
                this._root = null;
                this._AccessList = null;

                BuildSiteMap();
            }
        }
        
        
        public override bool IsAccessibleToUser(HttpContext context, SiteMapNode node)
        {
            return true;
        }

        protected override SiteMapNode GetRootNodeCore()
        {
            return _root;
        }

        /// <summary>
        /// Строит карту сайта
        /// </summary>
        /// <returns></returns>
        public override SiteMapNode BuildSiteMap()
        {
            lock (this.smLock)
            {
                if (_root != null)
                return _root;

                // Создаем корневой узел и добавляем к карте
                _root = new SiteMapNode(this, "root", "~/Default.aspx", null, null, new List<string>() { { "*" } }, null, null, null);

                AddNode(_root, null);

                _AccessList = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

                Dictionary<string, SiteMapNode> parents = new Dictionary<string, SiteMapNode>();

                foreach (var item in GetSiteMapData())
                {
                    if (string.IsNullOrEmpty(item.Roles))
                        continue;

                    string url = item.Url.Trim();
                    if (_AccessList.ContainsKey(url))
                        continue;

                    _AccessList.Add(url, item.Roles);

                    // Строим меню
                    if (item.Visible)
                    {
                        SiteMapNode smn = new SiteMapNode(this, item.Id.ToString(), item.Url, item.Title, item.Description, item.Roles.Split(','), null, null, null);
                        parents.Add(smn.Key, smn);

                        string pid = item.ParId.ToString();

                        if (parents.ContainsKey(pid))
                            AddNode(smn, parents[pid]);
                        else
                            AddNode(smn, _root);
                    }
                }

                return _root;
            }
        }

        /// <summary>
        /// Получить данные об узлах проекта.
        /// </summary>
        /// <returns></returns>
        private IList<SiteMapData> GetSiteMapData()
        {
            var sql = @"
SELECT Site_Map_Node_ID as Id,
       Site_Map_Node_Par_ID as ParId,
       Site_Map_Node_Url as Url,
       Site_Map_Node_Title as Title,
       Site_Map_Node_Descr as Description,
       Site_Map_Node_Roles as Roles,
       Site_Map_Node_Visible as Visible
FROM v_Site_Map_Node z
ORDER BY Site_Map_Node_Level,
         Site_Map_Node_Order,
         Site_Map_Node_Title";

            return RepositoryBase.ExecuteSqlToObject<SiteMapData>(sql);
        }

        private class SiteMapData
        {
            public int Id { get; set; }
            public int? ParId { get; set; }
            public string Url { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string Roles { get; set; }
            public bool Visible { get; set; }
        }
    }
}