﻿using System.Collections.Generic;
using System.ComponentModel;
using bgTeam.IsRui.Domain;
using bgTeam.Extensions;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class ResponsiblePurchaseViewModel : StepBaseViewModel
    {
        private const string ResponsiblePurchaseCaption = "Ответственный за организацию закупок";

        [DisplayName(ResponsiblePurchaseCaption)]
        public IList<IEntityName> ResponsiblePurchase { get; private set; }

        public ResponsiblePurchaseViewModel(int id)
            : base(id, MasterItemEnum.ResponsiblePurchase)
        {
            ResponsiblePurchase =
                Current.ResponsiblePurchase.Return(
                    x => new List<IEntityName>() { new SelectEntityInfo() { Id = x.Id, Name = x.Name } }, default(List<IEntityName>));
        }
    }
}