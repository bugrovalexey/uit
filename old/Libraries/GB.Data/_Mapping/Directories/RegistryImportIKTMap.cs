﻿using GB;
using GB.Entity.Import;

namespace GB.MKRF.Mapping.Directories
{
    public class RegistryImportIKTMap: ClassMapBase<RegistryImportIKT>
    {
        public RegistryImportIKTMap():
            base("Registry_Import_IKT")
        {

        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Registry_Import_IKT_Name");
            Map(x => x.Orig_ID, "Registry_Import_IKT_Orig_ID");
            Map(x => x.Orig_Par_ID, "Registry_Import_IKT_Orig_Par_ID");
            Map(x => x.Code, "Registry_Import_IKT_Code");
            Map(x => x.Status, "Registry_Import_IKT_Status");
        }
    }
}
