﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Validator
{
    internal class Executor
    {
        /// <summary>Параметры для валидации исполнителя</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create)
            {
                Entities.Directories.Executor Item = (Entities.Directories.Executor)Entity;
                if (Item.Child != null)
                    return "0;" + Item.Child.Id.ToString(); // можно ли пригласить  Item.Child ?
                else
                    return "0"; // моржно ли приглашать ?
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
