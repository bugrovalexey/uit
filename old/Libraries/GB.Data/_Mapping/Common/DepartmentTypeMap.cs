﻿using GB.Data.Common;

namespace GB._Mapping.Common
{
    internal class DepartmentTypeMap : ClassMapBase<DepartmentType>
    {
        public DepartmentTypeMap()
            : base("Govt_Organ_Type")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Govt_Organ_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_Govt_Organ_Insert
            //						  @Govt_Organ_Name = ?
            //						 ,@Govt_Organ_Type_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Govt_Organ_Update
            //						  @Govt_Organ_Name = ?
            //						 ,@Govt_Organ_Type_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Govt_Organ_Delete
            //						  @Govt_Organ_Type_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Govt_Organ_Type_Name").Nullable();
        }
    }
}