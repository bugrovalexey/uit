﻿namespace bgTeam.IsRui.Common.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    public static class MimeTypeHelper
    {
        private const string DefaultMime = "text/plain";

        private static readonly Dictionary<string, string> _mimeTypes = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
        {
            { "art", "image/x-jg" },
            { "bm", "image/bmp" },
            { "bmp", "image/bmp" },
            { "dwg", "image/x-dwg" },
            { "dxf", "image/x-dwg" },
            { "doc", "application/msword" },
            { "docx", "application/msword" },
            { "gif", "image/gif" },
            { "gz", "application/x-gzip" },
            { "gzip", "application/x-gzip" },
            { "htm", "text/html" },
            { "html", "text/html" },
            { "htmls", "text/html" },
            { "ico", "image/x-icon" },
            { "jpe", "image/jpeg" },
            { "jpeg", "image/jpeg" },
            { "jpg", "image/jpeg" },
            { "jps", "image/x-jps" },
            { "js", "text/javascript" },
            { "mp3", "audio/mpeg3" },
            { "mp4", "video/mp4" },
            { "mpa", "video/mpeg" },
            { "mpeg", "video/mpeg" },
            { "mpg", "video/mpeg" },
            { "pdf", "application/pdf" },
            { "pic", "image/pict" },
            { "pict", "image/pict" },
            { "png", "image/png" },
            { "pot", "application/mspowerpoint" },
            { "ppa", "application/vnd.ms-powerpoint" },
            { "pps", "application/mspowerpoint" },
            { "ppt", "application/mspowerpoint" },
            { "ppz", "application/mspowerpoint" },
            { "text", "text/plain" },
            { "tif", "image/tiff" },
            { "tiff", "image/tiff" },
            { "txt", "text/plain" },
            { "wav", "audio/wav" },
            { "xla", "application/excel" },
            { "xlb", "application/excel" },
            { "xlc", "application/excel" },
            { "xld", "application/excel" },
            { "xlk", "application/excel" },
            { "xll", "application/excel" },
            { "xlm", "application/excel" },
            { "xls", "application/excel" },
            { "xlsx", "application/excel" },
            { "xlt", "application/excel" },
            { "xlv", "application/excel" },
            { "xlw", "application/excel" },
            { "xml", "text/xml" },
            { "zip", "application/zip" }
        };

        private static readonly Regex _regexp = new Regex("\\.(\\w+)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public static string GetMimeTypeFromFileName(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return null;
            }

            string ext = string.Empty;

            var match = _regexp.Match(fileName);

            if (match.Success)
            {
                ext = match.Groups[1].Value;
            }

            if (_mimeTypes.ContainsKey(ext))
            {
                return _mimeTypes[ext];
            }

            return DefaultMime;
        }
    }
}