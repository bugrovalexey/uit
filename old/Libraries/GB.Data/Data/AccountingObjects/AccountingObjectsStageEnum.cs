﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace GB.Data.AccountingObjects
{
    public enum AccountingObjectsStageEnum
    {
        //[Description("Не задано")]
        //None = 0,

        /// <summary>
        /// Подготовка к созданию
        /// </summary>
        [Description("Подготовка к созданию")]
        Planned = 1,
        
        [Description("Создание")]
        Creation = 2,

        [Description("Эксплуатация")]
        Exploitation = 3,

        //Более не используется
        //[Description("Развитие")]
        //Evolution = 4,
        
        [Description("Выведен из эксплуатации")]
        OutExploitation = 4
    }
}
