﻿using GB.MKRF.Entities.Statuses;
using System;
using System.Collections.Generic;

namespace GB.MKRF.Entities.Documents
{
    /// <summary>Бумажная форма</summary>
    public class PaperObjectVer : EntityBase
    {
        protected PaperObjectVer()
        {
        }

        public PaperObjectVer(EntityBase Owner, PaperType PaperType)
        {
            this.Owner = Owner;
            this.PaperType = PaperType;
            this.Docs = new List<DocToPaperVer>();
        }

        /// <summary>Родительский объект</summary>
        public virtual EntityBase Owner { get; set; }

        /// <summary>Номер исходящего (вкладка огв)</summary>
        public virtual string NumberOutgoing { get; set; }

        /// <summary>Дата исходящего (вкладка огв)</summary>
        public virtual DateTime? DateOutgoing { get; set; }

        /// <summary>Номер входящего МКС (вкладка огв)</summary>
        public virtual string NumberInboxMKS { get; set; }

        /// <summary>Дата входящего МКС (вкладка огв)</summary>
        public virtual DateTime? DateInboxMKS { get; set; }

        /// <summary>Номер исходящего (вкладка мкс)</summary>
        public virtual string NumberOutgoingMKS { get; set; }

        /// <summary>Дата исходящего (вкладка мкс)</summary>
        public virtual DateTime? DateOutgoingMKS { get; set; }

        /// <summary>Дата начала экспертной оценки (факт.)</summary>
        public virtual DateTime? ExpStartDateF { get; set; }

        /// <summary>Дата окончания экспертной оценки (факт.)</summary>
        public virtual DateTime? ExpEndDateF { get; set; }

        /// <summary>Дата отправки плана на доработку</summary>
        public virtual DateTime? ExpReturnDate { get; set; }

        /// <summary>Комментарий </summary>
        public virtual string Comment { get; set; }

        /// <summary>Дата создания письма </summary>
        public virtual DateTime CreateDT { get; set; }

        /// <summary>Статус</summary>
        public virtual Status Status { get; set; }

        /// <summary>Документ привязаный к бумажной форме (вкладка огв)</summary>
        public virtual DocumentVer Document { get; set; }

        /// <summary>Документ привязаный к бумажной форме (вкладка мкс)</summary>
        public virtual DocumentVer DocumentMKS { get; set; }

        /// <summary>Тип бумаги</summary>
        public virtual PaperType PaperType { get; set; }

        /// <summary>Ссылки на документы бумажной формы</summary>
        public virtual IList<DocToPaperVer> Docs { get; protected set; }
    }
}