﻿namespace bgTeam.IsRui.Story.Activities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GoodsAddOrUpdateGoodsCharacteristicStoryContext
    {
        public int? Id { get; set; }

        public int? GoodsId { get; set; }

        public string Name { get; set; }

        public int? Value { get; set; }

        public int? UnitId { get; set; }

    }
}
