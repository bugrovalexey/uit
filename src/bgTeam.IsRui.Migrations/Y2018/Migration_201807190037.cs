﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807190037)]
    public class Migration_201807190037 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("OU")
                .AddColumn("ClassificationСategory_ID").AsInt32().Nullable();
        }
    }
}
