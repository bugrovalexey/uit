﻿using Uviri.MKRF.Entities.Admin;

namespace Uviri.MKRF.Mapping.Admin
{
    public class RightMap : ClassMapBase<Right>
	{
        public RightMap()
            : base("Rights", true)
		{
            //Table("w_");

            //Id(x => x.Id, "Rights_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

            //SqlInsert(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlUpdate(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlDelete(@"raiserror ('Unsupported',10,1)").Check.None();
		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Rights_Name");
        }
    }
}
