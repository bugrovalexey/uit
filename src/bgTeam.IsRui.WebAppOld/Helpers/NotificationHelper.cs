﻿using bgTeam.IsRui.Common;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.WebApp.ViewModels;
using System;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Helpers
{
    public static class NotificationHelper
    {
        public const string AddTextSuccess = "Запись успешно сохранена";

        public static JsonResult NotificationCreateEdit(Action action)
        {
            Exception e = null;

            try
            {
                action();
            }
            catch (Exception exp) { e = exp; }

            return GetEditMessage(e, null);
        }

        public static JsonResult NotificationCreateEdit<TResult>(Func<TResult> func) where TResult : class
        {
            TResult res = null;
            Exception e = null;

            try
            {
                res = func();
            }
            catch (Exception exp) { e = exp; }

            return GetEditMessage(e, res);
        }

        public static JsonResult GetEditMessage(Exception e, object res)
        {
            if (e == null)
            {
                var data = res ?? new NotificationBaseViewModel();

                if (data is NotificationBaseViewModel)
                {
                    var notify = data as NotificationBaseViewModel;
                    notify.success = true;
                    notify.message = AddTextSuccess;
                }

                return new JsonResult { Data = data };
            }
            else
            {
                var exp = e.GetInnerException();

                Logger.Fatal(exp);

                var result = (new NotificationBaseViewModel()
                {
                    success = false,
                    message = string.Format("Произошла ошибка при сохранении<br/><br/>{0}", exp.Message)
                });

                return new JsonResult { Data = result };
            }
        }

        public static JsonResult NotificationDelete(int id, Action action)
        {
            try
            {
                action();

                var result = (new
                {
                    id,
                    success = true,
                    message = "Запись успешно удалена"
                });

                return new JsonResult { Data = result };
            }
            catch (Exception exp)
            {
                Logger.Fatal(exp);

                var result = (new
                {
                    id,
                    success = false,
                    message = string.Format("Произошла ошибка при удалении<br/><br/>{0}", exp.Message)
                });

                return new JsonResult { Data = result };
            }
        }

        public static JsonResult NotificationDeleteCustomizable(int id, Action action, string failMessage = null)
        {
            try
            {
                action();

                return new JsonResult()
                {
                    Data = new
                    {
                        id,
                        success = true,
                        message = "Запись успешно удалена"
                    }
                };
            }
            catch (Exception exp)
            {
                Logger.Fatal(exp);

                failMessage += string.IsNullOrWhiteSpace(failMessage) ? null : "<br/><br/>";

                return new JsonResult()
                {
                    Data = new
                    {
                        id,
                        success = false,
                        message = string.Format("{0}{1}", failMessage, exp.Message)
                    }
                };
            }
        }

        public static JsonResult NotificationExecute(Action action, string success, string fail)
        {
            try
            {
                action();

                return new JsonResult()
                {
                    Data = new
                    {
                        success = true,
                        message = success
                    }
                };
            }
            catch (Exception exp)
            {
                Logger.Fatal(exp);

                return new JsonResult()
                {
                    Data = new
                    {
                        success = false,
                        message = string.Format("{0}<br/><br/>{1}", fail, exp.Message)
                    }
                };
            }
        }
    }
}
