﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using NHibernate.Criterion;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using bgTeam.IsRui.MKRF.Logic.Access;
using bgTeam.IsRui.Common.Helpers;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport
{
    [MetadataType(typeof(SoftwareEditViewModel_Valid))]
    public class SoftwareEditViewModel : ViewModelBase
    {
        #region consts

        const string NameCharacteristicOfLicenseCaption = "Наименование типовой лицензии";
        const string ModelCaption = "Модель лицензирования";
        const string TypeOfConnectionToServerCaption = "Тип соединения с сервером ";
        const string NumberOnlineSameTimeCaption = "Количество одновременных подключений";
        const string ProductNumberCaption = "Продуктовый номер лицензии";
        const string NumberAllocatedSameTimeCaption = "Количество единовременно распределяемых лицензий";
        const string LicensorCaption = "Лицензиар";

        #endregion consts


        public SoftwareEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;
            ReadOnly = ManagerAccess.CanEdit(new RepositoryDc<AccountingObject>().GetExisting(accountingObjectId)) ==
                       false;

            ManufacturerViewModel = new ManufacturerViewModel();

            CategoriesKindOfSupport = new SelectList(
                new RepositoryDc<CategoryKindOfSupport>().GetAllEx(
                    q => q.Where(cat => cat.Type == CategoryTypeOfSupportEnum.Software)),
                "Id", "Name");

            Rights = new SelectList(
                EnumHelper.ToDictionary<RightsOnSoftwareEnum>(),
                "Key", "Value");

            TypesOfConnectionToServerList = new SelectList(
                EnumHelper.ToDictionary<TypeOfConnectionToServerEnum>(),
                "Key", "Value");

            LicensorNamesJson =
                JsonHelper.ToJson(
                    new RepositoryDc<CharacteristicOfLicense>().Select<string>(
                        q => q.Select(
                            Projections.Distinct(
                                Projections.ProjectionList()
                                    .Add(Projections.Property<CharacteristicOfLicense>(x => x.Licensor))))));

            var entity = id.HasValue
                ? new RepositoryDc<Software>().Get(q =>
                    q.Fetch(x => x.CharacteristicsOfLicense).Eager.Where(x => x.Id == id))
                : null;
            entity.Do(Fill);
        }

        protected void Fill(Software entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            CategoryKindOfSupportId = entity.CategoryKindOfSupport.Return(cat => cat.Id, default(int));
            ManufacturerViewModel.ManufacturerId = entity.Manufacturer.Return(manufacturer => manufacturer.Id, default(int));
            Amount = entity.Amount;
            Summa = entity.Summa;
            Right = entity.Right;
            entity.CharacteristicsOfLicense.Do(Fill);
        }

        protected void Fill(CharacteristicOfLicense entity)
        {
            IdCharacteristicOfLicense = entity.Id;
            NameCharacteristicOfLicense = entity.Name;
            Model = entity.Model;
            TypeOfConnectionToServer = entity.TypeOfConnectionToServer;
            NumberOnlineSameTime = entity.NumberOnlineSameTime;
            ProductNumber = entity.ProductNumber;
            NumberAllocatedSameTime = entity.NumberAllocatedSameTime;
            Licensor = entity.Licensor;
        }

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        public ManufacturerViewModel ManufacturerViewModel { get; set; }

        [DisplayName(TableSoftware.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableSoftware.CategoryKindOfSupportCaption)]
        public int? CategoryKindOfSupportId { get; set; }

        [DisplayName(TableSoftware.AmountCaption)]
        public int Amount { get; set; }

        [DisplayName(TableSoftware.SummaCaption)]
        public decimal Summa { get; set; }

        [DisplayName(TableSoftware.RightCaption)]
        public RightsOnSoftwareEnum Right { get; set; }

        public SelectList CategoriesKindOfSupport { get; set; }

        public SelectList Rights { get; set; }

        #region Характеристики лицензии

        public int IdCharacteristicOfLicense { get; set; }

        [DisplayName(NameCharacteristicOfLicenseCaption)]
        public string NameCharacteristicOfLicense { get; set; }

        [DisplayName(ModelCaption)]
        public string Model { get; set; }

        [DisplayName(TypeOfConnectionToServerCaption)]
        public TypeOfConnectionToServerEnum? TypeOfConnectionToServer { get; set; }

        [DisplayName(NumberOnlineSameTimeCaption)]
        public int? NumberOnlineSameTime { get; set; }

        [DisplayName(ProductNumberCaption)]
        public string ProductNumber { get; set; }

        [DisplayName(NumberAllocatedSameTimeCaption)]
        public int? NumberAllocatedSameTime { get; set; }

        [DisplayName(LicensorCaption)]
        public string Licensor { get; set; }

        public string LicensorNamesJson { get; set; }

        public SelectList TypesOfConnectionToServerList { get; set; }

        #endregion Характеристики лицензии

    }

    internal class SoftwareEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Required(ErrorMessage = NotZeroValueText)]
        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int? CategoryKindOfSupportId { get; set; }

        [Required(ErrorMessage = RequiredText)]
        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int? Amount { get; set; }

        [Required(ErrorMessage = RequiredText)]
        [Range(MoneyMin, MoneyMax, ErrorMessage = RangeMoneyText)]
        public decimal? Summa { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public virtual RightsOnSoftwareEnum Right { get; set; }
    }
}