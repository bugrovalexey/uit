﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    [MetadataType(typeof(ExternalUserInterfaceOfIsEditViewModel_Valid))]
    public class ExternalUserInterfaceOfIsEditViewModel : ViewModelBase
    {
        public ExternalUserInterfaceOfIsEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;

            Types = new SelectList(new RepositoryDc<TypeOfInterface>().GetAll(), "Id", "Name");

            var entity = id.HasValue
                ? new RepositoryDc<ExternalUserInterfaceOfIs>().Get(id)
                : null;

            entity.Do(Fill);
        }

        #region Methods

        protected void Fill(ExternalUserInterfaceOfIs entity)
        {
            Id = entity.Id;
            AccountingObjectId = entity.Owner.Id;
            Purpose = entity.Purpose;
            Name = entity.Name;
            TypeId = entity.TypeOfInterface.Return(type => type.Id, default(int));
            Url = entity.Url;
        }

        #endregion Methods

        #region Props

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        [DisplayName(TableExternalUserInterfaceOfIs.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableExternalUserInterfaceOfIs.PurposeCaption)]
        public string Purpose { get; set; }

        [DisplayName(TableExternalUserInterfaceOfIs.TypeCaption)]
        public int? TypeId { get; set; }

        [DisplayName(TableExternalUserInterfaceOfIs.UrlCaption)]
        public string Url { get; set; }

        public SelectList Types { get; set; }    

        #endregion Props
    }

    public class ExternalUserInterfaceOfIsEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string Purpose { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public int? TypeId { get; set; }
    }

}