﻿namespace bgTeam.IsRui.Common.Extentions
{
    using System;
    using System.Reflection;

    public static class CustomAttributeProviderExtensions
    {
        // Methods
        public static T[] GetCustomAttributes<T>(this ICustomAttributeProvider attributeProvider, bool inherit)
            where T : Attribute
        {
            return (T[])attributeProvider.GetCustomAttributes(typeof(T), inherit);
        }
    }
}
