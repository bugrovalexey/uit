﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Prognosis.SectionII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionII
{
    public class SoftEmailMap : ClassMapBase<SoftEmail>
	{
        public SoftEmailMap()
            : base("MF2_Soft_Email")
        {
//            Table("w_");

//            Id(x => x.Id, "MF2_Soft_Email_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF2_Soft_Email_Insert
//                          @MF2_Soft_Email_CA = ?
//                         ,@MF2_Soft_Email_TP = ?
//                         ,@MF2_Soft_Email_Comm = ?
//                         ,@MF2_ID = ?
//                         ,@Soft_Email_Type_ID = ?
//                         ,@MF2_Soft_Email_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF2_Soft_Email_Update
//                          @MF2_Soft_Email_CA = ?
//                         ,@MF2_Soft_Email_TP = ?
//                         ,@MF2_Soft_Email_Comm = ?
//                         ,@MF2_ID = ?
//                         ,@Soft_Email_Type_ID = ?
//                         ,@MF2_Soft_Email_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF2_Soft_Email_Delete
//						  @MF2_Soft_Email_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.CountCA, "MF2_Soft_Email_CA");
            Map(x => x.CountTP, "MF2_Soft_Email_TP");
            Map(x => x.Comment, "MF2_Soft_Email_Comm");

            References<TechInfo>(x => x.TechInfo, "MF2_ID").Not.Nullable();
			References<SoftEmailType>(x => x.SoftEmailType, "Soft_Email_Type_ID").Not.Nullable();
        }
    }
}