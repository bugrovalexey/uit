﻿using System;

namespace Core.Import.ESKI
{
    public interface IEntity
    {
        /// <summary>Идентификатор</summary>
        int Id { get; set; }

        /// <summary>Внешний идентификатор</summary>
        string ExtId { get; set; }

        /// <summary>Название сущности</summary>
        string EntityName { get; }
    }

    public class AnnotationAttribute : Attribute
    {
        public string Annotation { get; set; }

        public AnnotationAttribute(string Annotation)
        {
            this.Annotation = Annotation;
        }
    }
}


