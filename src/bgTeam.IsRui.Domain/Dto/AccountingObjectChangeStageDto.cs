﻿namespace bgTeam.IsRui.Domain.Dto
{
    public class AccountingObjectChangeStageDto
    {
        public int Id { get; set; }

        public string Stage { get; set; }
    }
}
