﻿using System;

namespace GB.Data.Directories
{
    /// <summary>
    /// Тип затрат
    /// </summary>
    public class ExpenseType : EntityBase, IEntityName
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary> Дата начала действия справочника </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary> Дата окончания действия справочника </summary>
        public virtual DateTime? DateEnd { get; set; }

        public virtual ExpenseDirection ExpenseDirection { get; set; }
    }
}