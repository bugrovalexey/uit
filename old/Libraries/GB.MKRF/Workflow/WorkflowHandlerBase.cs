﻿using System;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Statuses;

namespace GB.MKRF.Workflow
{
    public abstract class WorkflowHandlerBase<T> : IWorkflowHandler where T : IWorkflowObject
    {
        protected abstract void CheckNewStatus(T @object, Status newStatus);

        void IWorkflowHandler.ReadyForNewStatus(IWorkflowObject workflowObject, Status newStatus)
        {
            CheckNewStatus((T)workflowObject, newStatus);
        }
    }
}