﻿using System;
using System.Collections.Generic;
using System.Web;
using GB.MKRF.Entities.Admin;
using GB.Repository;
using Newtonsoft.Json;

namespace GB.MKRF.Web.Selectors
{
    public partial class Users : System.Web.UI.Page
    {
        public class JqGridResult<T>
        {
            public int CurrentPage { get; set; }
            public int TotalPages { get; set; }
            public int TotalRecords { get; set; }
            public List<T> Records { get; protected set; }

            public JqGridResult()
            {
                Records = new List<T>();

            }
        }

        public class JqUser
        {
            public int Id { get; set; }
            public string LastName { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string Department { get; set; }
            public string Role { get; set; }
        }

        public override void ProcessRequest(HttpContext context)
        {
            HttpRequest request = HttpContext.Current.Request;
            if (request.PathInfo.Contains("GetUsers"))
            {
                // request.QueryString["sord"] asc /desc
                // request.QueryString["sidx"] имя столбца
                JqGridResult<JqUser> jsUsers = new JqGridResult<JqUser>();
                System.Linq.Expressions.Expression<Func<User, object>> OrderBy = null;
                switch (request.QueryString["sidx"])
                {
                    case "FirstName": OrderBy = (x => x.Name); break;
                    case "LastName": OrderBy = (x => x.Surname); break;
                    case "MiddleName": OrderBy = (x => x.Middlename); break;
                    case "Department": OrderBy = (x => x.Department.NameSmall); break;
                    case "Role": OrderBy = (x => x.Role.Name); break;
                }
                
                bool desc = (request.QueryString["sord"] == "desc");

                IList<User> allUsers = RepositoryBase<User>.GetAll(
                    new GetAllArgs<User>()
                    {
                        Expression = x => x.IsActive == true,
                        OrderByAsc = desc ? null : OrderBy,
                        OrderByDesc = desc ? OrderBy : null,
                        Fetch = new System.Linq.Expressions.Expression<Func<User, object>>[]{
                            x=>x.Department,
                            x=>x.Role}
                    });

                foreach (User user in allUsers)
                {
                    jsUsers.Records.Add(new JqUser() { 
                        LastName = user.Surname,
                        FirstName = user.Name, 
                        MiddleName = user.Middlename,
                        Department = user.Department.NameSmall,
                        Role = user.Role.Name, 
                        Id = user.Id });
                }
                jsUsers.TotalPages = 1;
                jsUsers.TotalRecords = jsUsers.Records.Count;
                jsUsers.CurrentPage = 1;

                string json = JsonConvert.SerializeObject(jsUsers);
                HttpContext.Current.Response.Write(json);
                HttpContext.Current.Response.End();
            }
            else
            {
                base.ProcessRequest(context);
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            SelectorsUsersGrid.DataBind();

            base.OnPreRender(e);
        }

        protected void SelectorsUsersGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            SelectorsUsersGrid.DataSource = RepositoryBase<User>.GetAll(new GetAllArgs<User>()
            {
                Expression = (x => x.IsActive == true),
                Fetch = new System.Linq.Expressions.Expression<Func<User, object>>[] { x=>x.Role, x=>x.Department }
            });
        }
    }
}