﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GB.Helpers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxGridView;

namespace GB.MKRF.Web.Selectors
{
    public partial class PlanActivity : System.Web.UI.Page
    {
        public static string GetUrl(object DemandId)
        {
            return string.Format("{0}?Demand={1}", UrlHelper.Resolve("~/Selectors/PlanActivity.aspx"), DemandId);
        }

        ExpDemand currentDemand_Internal = null;
        public ExpDemand CurrentDemand
        {
            get
            {
                int id = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["Demand"]) && int.TryParse(Request.QueryString["Demand"], out id))
                {
                    // Заявка на план
                    currentDemand_Internal = RepositoryBase<ExpDemand>.Get(id);
                }
               
                if (currentDemand_Internal == null || !Validation.Validator.Validate(currentDemand_Internal, ObjActionEnum.view))
                {
                    currentDemand_Internal = null;
                    ResponseHelper.DenyAccess(Response);
                }
                return currentDemand_Internal;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            SelectorsPlanActivityGrid.DataBind();
            base.OnPreRender(e);
        }

        protected void SelectorsPlanActivityGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            DataTable table = null;
            if (this.CurrentDemand.ExpObject.PlanType != ExpObjectTypeEnum.SummaryPlanMKS2012new)
            {
                table = PlanRepository.GetActivityList2012new(this.CurrentDemand.ExpObject, UserRepository.GetCurrent().Id);
            }
            else
            {
                //если сводный план то выбираем все дочернии у Департаментов мкс
                table = PlanRepository.GetDepActivityList2012new(this.CurrentDemand.ExpObject);
            }

            IList<string>ids = new List<string>();

            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    ids.Add(row["Id"].ToString());
                }
            }

            IList<PlansActivity> activityList = RepositoryBase<PlansActivity>.GetAll( new GetAllArgs<PlansActivity>()
            {
                 Ids = ids.ToArray(),
                 IdsSelector = (x=>x.Id)
            });   
                
            (sender as ASPxGridView).DataSource = activityList.Select(x => new
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name
            });
        }

        protected void SelectorsPlanActivityGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "$(this).addClass(\"grid_mouseover\")");
            e.Row.Attributes.Add("onmouseout", "$(this).removeClass(\"grid_mouseover\")");
        }
    }
}