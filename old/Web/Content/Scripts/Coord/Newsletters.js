﻿$(function () {
    $('div.item_title').live('click', function (e) {
        toogleFunction(e)
    });

    function toogleFunction(e)
    {
        var type = e.target.id;
        if ($(e.target).hasClass('sheader') == true) {
            var obj = e.target.parentElement;
        }
        else var obj = e.target;
        var target = $('#exp' + obj.id);
        if ($(obj).hasClass('collapsed') == true) {
            $(obj).removeClass('collapsed')
                  .addClass('expanded');
            target.slideDown();
        }
        else {
            $(obj).removeClass('expanded')
                  .addClass('collapsed');
            target.slideUp();
        }
    }

    $('.showAll').live('click', function (e) {
        $('.item_title').removeClass('collapsed');
        $('.item_title').addClass('expanded');
        $('.expitem').slideDown();
    });

    $('.collapseAll').live('click', function (e) {
        $('.item_title').removeClass('expanded');
        $('.item_title').addClass('collapsed');
        $('.expitem').slideUp();
    });
});