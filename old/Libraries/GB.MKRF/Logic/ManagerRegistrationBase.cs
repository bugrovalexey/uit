﻿namespace GB.MKRF.Logic
{
    public abstract class ManagerRegistrationBase<T>
        where T : class, IManager
    {
        private readonly object syncRoot = new object();
        private bool isRegistered;

        public void Register()
        {
            lock (syncRoot)
            {
                if (isRegistered) return;

                RegisterHandlers(Singleton<T>.Instance);

                isRegistered = true;
            }
        }

        protected abstract void RegisterHandlers(T manager);
    }
}