﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Helpers;
using GB.Repository;
using FileHelper = GB.Helpers.FileHelper;

namespace GB.MKRF.Web.Plans
{
    public partial class ExportPlan : Page
    {
        public const string PlanIdKey = "Id";
        protected int CurrentId { get; set; }
        private Plan _currentPlan;
        protected Plan CurrentPlan
        {
            get
            {
                return _currentPlan ??
                       (_currentPlan = RepositoryBase<Plan>.Get(CurrentId));
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.Request.QueryString[PlanIdKey] != null)
            {
                CurrentId = Convert.ToInt32(Page.Request.QueryString[PlanIdKey]);
            }
            
            using (MemoryStream stream = new MemoryStream())
            {
                var xmlDocument = XmlHelper.ExportPlanToXmlByProcedure(CurrentPlan);
                xmlDocument.Save(stream);
                
//                var serializer = new DataContractSerializer(CurrentPlan.GetType());
//                serializer.WriteObject(stream, CurrentPlan);

                // Convert the memory stream to an array of bytes.
                byte[] byteArray = stream.ToArray();
                
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                var title = FileHelper.TranslateFileName(CurrentPlan.Name);
                Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=\"{0}.xml\";", title));

                Response.ContentType = "application/xml";
                Response.Buffer = false;

                Response.AppendHeader("Content-Length", byteArray.Length.ToString());
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Cache-Control", "no-cache, post-check=0, pre-check=0, max-age=0");

                Response.BinaryWrite(byteArray);
                Response.Flush();
                Response.End();
            }
        }
    }
}