﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.Extensions;
using System.ComponentModel;
using bgTeam.IsRui.Common.Helpers;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class TableCharacteristic : NotificationBaseViewModel
    {
        #region Consts

        public const string NameCaption = "Наименование";
        public const string TypeCaption = "Тип";
        public const string UnitCaption = "Единица измерения";
        public const string NormCaption = "Номинальное значение";
        public const string MaxCaption = "Максимальное значение";
        public const string FactCaption = "Фактическое значение";
        public const string PeriodCaption = "Период измерения";

        #endregion Consts

        #region Ctors

        public TableCharacteristic(AccountingObjectCharacteristics entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            TypeName = entity.Type.Return(type => type.Name, default(string));
            UnitName = entity.Unit.Return(unit => unit.Name, default(string));
            Norm = entity.Norm;
            Max = entity.Max;
            Fact = entity.Fact;
            Period = DateHelper.GetPeriodString(entity.DateBeginning, entity.DateEnd);


        }

        #endregion Ctors

        #region Props

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(TypeCaption)]
        public string TypeName { get; set; }

        [DisplayName(UnitCaption)]
        public string UnitName { get; set; }

        [DisplayName(NormCaption)]
        public int Norm { get; set; }

        [DisplayName(MaxCaption)]
        public int? Max { get; set; }

        [DisplayName(FactCaption)]
        public int? Fact { get; set; }

        [DisplayName(PeriodCaption)]
        public string Period { get; set; }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteCharacteristic", "AccountingObjects", new { id = Id }, "Grid");
            }
        }

        #endregion Props
    }
}