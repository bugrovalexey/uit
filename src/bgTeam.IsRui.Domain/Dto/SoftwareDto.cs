﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class SoftwareDto
    {
        /// <summary>
        /// Ид
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Классификационная категория
        /// </summary>
        public CategoryKindOfSupportDto CategoryKindOfSupport { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public decimal Summa { get; set; }

        /// <summary>
        /// Id - Производителя
        /// </summary>
        public int ManufacturerId { get; set; }

        /// <summary>
        /// Производитель
        /// </summary>
        public string ManufacturerName { get; set; }

        /// <summary>
        /// Производитель(заданный пользователем)
        /// </summary>
        public string ManufacturerCustomName { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// Права на ПО
        /// </summary>
        public RightsOnSoftwareEnum Right { get; set; }

        /// <summary>
        /// Характеристики лицензи
        /// </summary>
        public CharacteristicOfLicenseDto CharacteristicsOfLicense { get; set; }
    }
}
