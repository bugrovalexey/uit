﻿using AutoMapper;
using GB.Data.AccountingObjects;

namespace GB.DataAccess.Command.AccountingObjects.Purchase
{
    public class AddPurchaseAccountingObjectCommand : ICommand<AddPurchaseAccountingObjectContext, GovPurchase>
    {
        ISessionFactoryGb factory;

        public AddPurchaseAccountingObjectCommand()
        {
            factory = new SessionFactory();
        }

        public GovPurchase Execute(AddPurchaseAccountingObjectContext context)
        {
            var gp = new GovPurchase();

            Mapper.Map(context, gp);

            using (var session = factory.Create())
            {
                session.Insert(gp);
                session.Commit();
            }

            return gp;
        }
    }
}
