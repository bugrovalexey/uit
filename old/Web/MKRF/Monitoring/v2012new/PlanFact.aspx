﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Monitoring/Monitoring.master" AutoEventWireup="true" CodeBehind="PlanFact.aspx.cs" Inherits="GB.MKRF.Web.Monitoring.v2012new.PlanFact" %>

<%@ Register Src="~/Common/GridColumnManager.ascx" TagName="GridColumnManager" TagPrefix="gcm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="buttons" runat="server">

    <script type="text/javascript">
        function reloadPage() {

            var url = window.location.pathname + '?dep=' + DepComboBox.GetValue();

            window.open(url, '_self');
        }

        function DrawCrosshair(s, e) {

            if (e.crosshairGroupHeaderElements[0]) {
                var l = hf.Get(e.crosshairGroupHeaderElements[0].text + 'Limit');
                var d = hf.Get(e.crosshairGroupHeaderElements[0].text + 'Difference');

                e.crosshairElements[e.crosshairElements.length - 1].LabelElement.text +=
                    ('<br /><br />' +
                    'Лимит финансирования : ' + l + '<br />' +
                    'Разница : ' + d );
            }
        }
    </script>

    <table style="width: 100%">
        <tr>
            <td style="width: 1%; white-space: nowrap; text-align: right">Департамент:</td>
            <td style="width: 1%">
                <dxe:ASPxComboBox runat="server" ID="DepComboBox" Width="340px" ValueType="System.Int32"
                    ValueField="Id" TextField="Name" ClientInstanceName="DepComboBox" OnDataBinding="DepComboBox_DataBinding">
                    <ClientSideEvents SelectedIndexChanged="function(s,e) { reloadPage();  }" />
                </dxe:ASPxComboBox>
            </td>
            <td style="width: 98%"></td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="center" runat="server">
    <div style="margin: 0 auto; width: 1000px;">
        <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
        <dxchartsui:WebChartControl ID="Chart" Width="1000px"
            runat="server" SideBySideEqualBarWidth="True" Height="700px" PaletteName="Origin" >
	        <padding bottom="0" left="0" right="0" top="0"/>
	        <legend AlignmentHorizontal="Center" AlignmentVertical="BottomOutside" font="Tahoma, 10pt">
                <Margins Top="20"/>
            </legend>
	        <titles>
	            <dxcharts:ChartTitle WordWrap="True" MaximumLinesCount="4" Text="План-факт" Font="Tahoma, 14pt"/>
	        </titles>
	        <borderoptions visible="False" />
	        <diagramserializable>
	            <dxcharts:XYDiagram>
		            <AxisX Title-Text="Год"  >
                        <DateTimeScaleOptions GridAlignment="Year" MeasureUnit="Year" />
		            </AxisX>

		            <AxisY Title-Text="Объем расходов тыс. руб." Title-Visible="True" >
		                <Label TextPattern="{A:#,##0}"/>
		            </AxisY>
		            <DefaultPane BackColor="White" EnableAxisXScrolling="False" EnableAxisYScrolling="False" EnableAxisXZooming="False" EnableAxisYZooming="False"/>
	            </dxcharts:XYDiagram>
	        </diagramserializable>
            <ClientSideEvents CustomDrawCrosshair="DrawCrosshair"></ClientSideEvents>
        </dxchartsui:WebChartControl>
    </div>
    <div style="padding-top:20px">
        <cc:GridExport runat="server" ID="GridExport" FileName="План_факт" UserStyle="float:left" />
        <div style="clear: both"></div>
        <dxwgv:ASPxGridView runat="server" ID="Grid" KeyFieldName="Id"
            Width="100%" AutoGenerateColumns="False" ClientInstanceName="Grid"
            OnDataBinding="Grid_DataBinding">
            <Columns>
                <dxwgv:GridViewDataTextColumn Caption="Год плана" FieldName="Year" />
                <dxwgv:GridViewDataTextColumn Caption="Лимит финансирования" FieldName="Limit" >
                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Планируемые расходы" FieldName="Plan">
                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Фактические расходы" FieldName="Fact" >
                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Разница" FieldName="Difference" >
                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                </dxwgv:GridViewDataTextColumn>
            </Columns>
            <SettingsPager PageSize="50" />
            <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
        </dxwgv:ASPxGridView>
    </div>

</asp:Content>
