﻿using System;

namespace bgTeam.IsRui.Domain.Entities.Directories
{
    /// <summary>
    /// БК ЦСР
    /// </summary>
    public class ExpenseItem : EntityBase, IEntityCodeName, IPeriodDirectories
    {
        /// <summary>Код</summary>
        public virtual string Code { get; set; }//null

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null

        /// <summary> Дата начала действия справочника </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary> Дата окончания действия справочника </summary>
        public virtual DateTime? DateEnd { get; set; }

        public virtual string CodeName { get { return EntityHelper.GetCodeName(this); } }
    }
}