﻿using Uviri.MKRF.Entities.PlanReport.SectionIX;
using Uviri.MKRF.Entities.Plans;
using Uviri.MKRF.Entities.Statuses;

namespace Uviri.MKRF.Mapping.PlanReport.SectionIX
{
    public class PlanOutFuncMap : ClassMapBase<PlanOutFunc>
    {
		public PlanOutFuncMap()
            : base("PRS_9v")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_9v_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_9v_Num = ?
//                          ,@PRS_9v_Func = ?
//                          ,@PRS_9v_Comm = ?
//                          ,@Plans_ID = ?
//                          ,@Status_ID = ?
//						  ,@PRS_9v_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_9v_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_9v_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_9v_Delete @PRS_9v_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_9v_Num");
			Map(x => x.Functions, "PRS_9v_Func");
			Map(x => x.Comment, "PRS_9v_Comm");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
			References<Status>(x => x.Status, "Status_ID");
        }
    }
}