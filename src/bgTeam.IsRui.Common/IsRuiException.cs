﻿namespace bgTeam.IsRui.Common
{
    using System;

    public class IsRuiException : Exception
    {
        public IsRuiException()
        {
        }

        public IsRuiException(string message)
            : base(message)
        {
        }

        public IsRuiException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
