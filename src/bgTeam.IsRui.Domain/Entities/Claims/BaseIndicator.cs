﻿namespace bgTeam.IsRui.Domain.Entities.Claims
{
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;

    /// <summary>
    /// Базовый класс для показателей и индикаторов заявок
    /// </summary>
    public class BaseIndicator : EntityBase
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Пояснение связи с показателем проекта
        /// </summary>
        public virtual string ExplanationRelationshipWithProjectMark { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public virtual Units Unit { get; set; }

        /// <summary>
        /// Базовое значение
        /// </summary>
        public virtual int? BaseValue { get; set; }

        /// <summary>
        /// Целевое значение
        /// </summary>
        public virtual int? TargetedValue { get; set; }

        /// <summary>
        /// Дата достижения целевого значения
        /// </summary>
        public virtual DateTime? DateToAchieveGoalValue { get; set; }

        /// <summary>
        /// Заявка
        /// </summary>
        public virtual Claim Claim { get; set; }
    }
}
