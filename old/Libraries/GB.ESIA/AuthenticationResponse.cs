﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Web.UI;
using System.Xml;
using Atp.Saml2;

namespace GB.ESIA
{
	/// <summary>
	/// Ответ с результатами аутентификации.
	/// </summary>
	public class AuthenticationResponse
	{
		private readonly string relayState = Guid.NewGuid().ToString();
		/// <summary>
		/// GUID-идентификатор запроса.
		/// </summary>
		public string RelayState
		{
			get { return relayState; }
		}

		/// <summary>
		/// Идентификатор запроса.
		/// </summary>
		public string Id
		{
			get { return authnResponse.Id; }
		}

		/// <summary>
		/// Идентификатор запроса аутентификации.
		/// </summary>
		public string AuthenticationRequestId
		{
			get { return authnResponse.InResponseTo; }
		}

		/// <summary>
		/// Представление запроса в виде XML.
		/// </summary>
		/// <remarks>Пример использования: [EasyAuthenticationResponse].Xml.OuterXml</remarks>
		public XmlNode Xml
		{
			get { return authnResponse.GetXml().ParentNode; }
		}

		/// <summary>
		/// Сертификат, которым должен быть подписан отправляемый запрос.
		/// </summary>
		private readonly X509Certificate2 spX509Certificate;

		/// <summary>
		/// Первое утверждение в ответе.
		/// </summary>
		private readonly Assertion samlAssertion;

		/// <summary>
		/// Ответ с результатами аутентификации.
		/// </summary>
		private readonly Atp.Saml2.Response authnResponse;

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="page">Страница, на которую пришли результаты аутентификации.</param>
		/// <param name="idpCertificate">Сертификат, которым пришедший ответ должен быть подписан.</param>
		/// <param name="spCertificate">Сертификат, которым пришедший ответ должен быть расшифрован.</param>
		public AuthenticationResponse(Page page, X509Certificate2 idpCertificate, X509Certificate2 spCertificate)
            : this(page.Request, idpCertificate, spCertificate)
        {}

        public AuthenticationResponse(System.Web.HttpRequest request, X509Certificate2 idpCertificate, X509Certificate2 spCertificate)
		{
			authnResponse = Atp.Saml2.Response.Create(request);
			relayState = authnResponse.RelayState;
			spX509Certificate = spCertificate;
            try
            {
                if (authnResponse.IsSigned())
                {
                    // Проверка на то, что указанным сертификатом можно расшифровать пришедший ответ.
                    if (!authnResponse.Validate(idpCertificate))
                        throw new ApplicationException("Ошибка при проверки подписи.");
                }
            }
            catch (Exception ex)
            {
                
            }
            
			if (!IsSuccess)
				return;
			// Проверка наличия утверждений в запросе и извлечение первого из них.
			if (authnResponse.GetAssertions().Count > 0)
				samlAssertion = authnResponse.GetAssertions()[0];
			else if (authnResponse.GetEncryptedAssertions().Count > 0)
				samlAssertion = authnResponse.GetEncryptedAssertions()[0].Decrypt(spX509Certificate.PrivateKey, null);
			else
				throw new ApplicationException("Нет утверждений в ответе.");

			AttributeStatement attributeStatement = GetFirstAttributeStatement(samlAssertion.AttributeStatements);
			User = new User(attributeStatement);
		}

		/// <summary>
		/// Возвращает первую секцию AttributeStatement из ответа на запрос аутентификации.
		/// </summary>
		/// <param name="items">Список секций.</param>
		/// <returns>Первая секция AttributeStatement из ответа на запрос аутентификации.</returns>
		private static AttributeStatement GetFirstAttributeStatement(IList<AttributeStatement> items)
		{
			return (items == null) || (items.Count == 0) ? null : items[0];
		}

		/// <summary>
		/// Свойство, в котором может передаваться идентификатор пользователя.
		/// </summary>
		public string NameIdentifier
		{
			get 
			{
				if (samlAssertion.Subject.NameId != null)
					return samlAssertion.Subject.NameId.NameIdentifier;
				if (samlAssertion.Subject.EncryptedId != null)
				{
					NameId nameId = samlAssertion.Subject.EncryptedId.Decrypt(spX509Certificate.PrivateKey, null);
					return nameId.NameIdentifier;
				}
				return string.Empty;
			}
		}

		/// <summary>
		/// Информация о пользователе.
		/// </summary>
		public User User { get; private set; }

		/// <summary>
		/// Общий результат аутентификации: true - успех, false - в противном случае.
		/// </summary>
		public bool IsSuccess
		{
			get { return authnResponse.IsSuccess(); }
		}

		/// <summary>
		/// Текстовое представление статуса аутентификации.
		/// </summary>
		public string StatusMessage
		{
			get { return authnResponse.Status.StatusMessage != null ? authnResponse.Status.StatusMessage.Message : string.Empty; }
		}
	}
}
