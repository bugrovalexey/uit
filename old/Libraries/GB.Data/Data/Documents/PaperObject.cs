﻿using GB.MKRF.Entities.Statuses;
using System;
using System.Collections.Generic;

namespace GB.MKRF.Entities.Documents
{
    /// <summary>Бумажная форма</summary>
    public class PaperObject : EntityBase
    {
        protected PaperObject()
        {
        }

        public PaperObject(EntityBase Owner, PaperType PaperType)
        {
            this.Owner = Owner;
            this.PaperType = PaperType;
            this.Docs = new List<DocToPaper>();
        }

        /// <summary>Родительский объект</summary>
        public virtual EntityBase Owner { get; set; }

        /// <summary>Номер исходящего (вкладка огв)</summary>
        public virtual string NumberOutgoing { get; set; }

        /// <summary>Дата исходящего (вкладка огв)</summary>
        public virtual DateTime? DateOutgoing { get; set; }

        /// <summary>Номер входящего МКС (вкладка огв)</summary>
        public virtual string NumberInboxMKS { get; set; }

        /// <summary>Дата входящего МКС (вкладка огв)</summary>
        public virtual DateTime? DateInboxMKS { get; set; }

        /// <summary>Номер исходящего (вкладка мкс)</summary>
        public virtual string NumberOutgoingMKS { get; set; }

        /// <summary>Дата исходящего (вкладка мкс)</summary>
        public virtual DateTime? DateOutgoingMKS { get; set; }

        /// <summary>Дата начала экспертной оценки (факт.)</summary>
        public virtual DateTime? ExpStartDateF { get; set; }

        /// <summary>Дата окончания экспертной оценки (факт.)</summary>
        public virtual DateTime? ExpEndDateF { get; set; }

        /// <summary>Дата отправки плана на доработку</summary>
        public virtual DateTime? ExpReturnDate { get; set; }

        /// <summary>Комментарий </summary>
        public virtual string Comment { get; set; }

        /// <summary>Статус</summary>
        public virtual Status Status { get; set; }

        /// <summary>Документ привязаный к бумажной форме (вкладка огв)</summary>
        public virtual Document Document { get; set; }

        /// <summary>Документ привязаный к бумажной форме (вкладка мкс)</summary>
        public virtual Document DocumentMKS { get; set; }

        /// <summary>Тип бумаги</summary>
        public virtual PaperType PaperType { get; set; }

        /// <summary>Ссылки на документы бумажной формы</summary>
        public virtual IList<DocToPaper> Docs { get; protected set; }
    }

    /// <summary>
    /// Тип бумажной формы
    /// </summary>
    public enum PaperObjectType
    {
        /// <summary>
        /// Исходящие ОГВ
        /// </summary>
        Department = 0,

        /// <summary>
        /// Исходящие МКС
        /// </summary>
        MKS = 1
    }

    public static class HelperPaperObject
    {
        public static DocTypeEnum GetDocType(this PaperObjectType type)
        {
            switch (type)
            {
                case PaperObjectType.Department:
                    return DocTypeEnum.Outgoing;

                case PaperObjectType.MKS:
                    return DocTypeEnum.Inbox;
            }

            throw new NotImplementedException();
        }
    }
}