﻿using GB.Data;
using GB.DataAccess.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GB.DataAccess.Test.Services
{
    [TestFixture]
    public class ActivityServiceTest
    {
        private ActivityService _service;

        public ActivityServiceTest()
        {
            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("test_org"), null);

            new NHSession("Coord");

            _service = new ActivityService();
        }

        [Test]
        public void Create()
        {
            _service.Create(new CreateVeiwModel()
            {
                NameSmall = "Тестовое мероприятие",
                Type = EntityBase.Default,
            });
        }
    }

    public class CreateVeiwModel : IPlansActivityCreateArgs
    {
        public int Id { get; set; }

        public string NameSmall { get; set; }

        public int Type { get; set; }
    }
}
