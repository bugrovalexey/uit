﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CostsGetCostsStory : IStory<CostsGetCostsStoryContext, ActivityCostDto>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IMapperBase _mapper;

        public CostsGetCostsStory(IRepositoryRui repositoryRui, IMapperBase mapperBase)
        {
            _repositoryRui = repositoryRui;
            _mapper = mapperBase;
        }

        public ActivityCostDto Execute(CostsGetCostsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityCostDto> ExecuteAsync(CostsGetCostsStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var activiti = _repositoryRui.Get<PlansActivity>(context.ActivityId);

            return _mapper.Map(activiti, new ActivityCostDto());
        }
    }
}
