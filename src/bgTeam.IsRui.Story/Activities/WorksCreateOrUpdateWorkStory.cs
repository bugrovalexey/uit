﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class WorksCreateOrUpdateWorkStory : IStory<WorksCreateOrUpdateWorkStoryContext, ActivityWorksDto>
    {
        private readonly IMapperBase _mapper;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;

        public WorksCreateOrUpdateWorkStory(
            IMapperBase mapper,
            ISessionNHibernateFactory factory,
            IRepositoryRui repository)
        {
            _mapper = mapper;
            _factory = factory;
            _repository = repository;
        }

        public ActivityWorksDto Execute(WorksCreateOrUpdateWorkStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityWorksDto> ExecuteAsync(WorksCreateOrUpdateWorkStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            using (var session = _factory.OpenSession())
            {
                ActivityWorks works;

                if (context.Id.HasValue)
                {
                    works = _repository.Get<ActivityWorks>(context.Id);
                }
                else
                {
                    works = new ActivityWorks();

                    works.PlansActivityId = context.ActivityId;
                }

                _mapper.Map(context, works);

                works.ChangeDate = DateTime.Now;

                /*if (context.expenseDirectionId != null)
                {
                    works.ExpenseDirection = _repository.Get<ExpenseDirection>(context.expenseDirectionId);
                }

                if (context.expenseTypeId != null)
                {
                    works.ExpenseType = _repository.Get<ExpenseType>(context.expenseTypeId);
                }

                if (context.KOSGUId != null)
                {
                    works.KOSGU = _repository.Get<ExpenseItem>(context.KOSGUId);
                }

                if (context.OKPDId != null)
                {
                    works.OKPD = _repository.Get<OKPD>(context.OKPDId);
                }

                if (context.OKVEDId != null)
                {
                    works.OKVED = _repository.Get<OKVED>(context.OKVEDId);
                }*/

                await session.SaveOrUpdateAsync(works);

                return _mapper.Map(works, new ActivityWorksDto());
            }
        }
    }
}
