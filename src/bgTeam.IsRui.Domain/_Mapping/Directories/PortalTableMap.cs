﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    class PortalTableMap : ClassMapBase<PortalTable>
    {
        public PortalTableMap()
            : base("Portal_Table")
        {
            //Table("");

            //Id(x => x.Id, "Portal_Table_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //SqlInsert(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlUpdate(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlDelete(@"raiserror ('Unsupported',10,1)").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.RusName, "Portal_Table_Rus_Name");
            Map(x => x.Name, "Portal_Table_Name");
        }
    }
}