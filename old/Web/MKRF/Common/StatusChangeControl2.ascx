﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatusChangeControl2.ascx.cs" Inherits="GB.MKRF.Web.Common.StatusChangeControl2" %>

<script type="text/javascript">
    function ChangeStatus() {
        MainLoadingPanel.Show();

        callBack.PerformCallback('');
    }
</script>

<table class="form_edit">
    <tr>
        <td class="form_edit_desc">Перевести в статус
        </td>
        <td class="form_edit_input">
            <dxe:ASPxComboBox runat="server" ID="StatusComboBox" 
                ValueField="Key" TextField="Value" ClientInstanceName="StatusComboBox"
                OnDataBinding="StatusComboBox_DataBinding">
                <%--<ClientSideEvents SelectedIndexChanged="function(s,e){ activateButton(s); }" />--%>
            </dxe:ASPxComboBox>
        </td>
    </tr>
    <tr runat="server">
        <td class="form_edit_desc">Комментарий:
        </td>
        <td class="form_edit_input">
            <dxe:ASPxMemo runat="server" ID="CommentMemo" Rows="7" ClientInstanceName="CommentMemo" />
        </td>
    </tr>
    <tr>
        <td class="form_edit_desc"></td>
        <td class="form_edit_input">
            <dxe:ASPxButton runat="server" ID="ChangeStatusButton" Text="Перевести" AutoPostBack="false" ClientInstanceName="ChangeStatusButton">
                <ClientSideEvents Click="function(s,e){ ChangeStatus(); }" />
            </dxe:ASPxButton>
        </td>
    </tr>
</table>

<dxc:ASPxCallback runat="server" ID="callBack" ClientInstanceName="callBack" OnCallback="callBack_Callback">
    <ClientSideEvents CallbackComplete="function(s, e) { MainLoadingPanel.Hide(); }"
        CallbackError="function(s,e) { MainLoadingPanel.Hide(); }" />
</dxc:ASPxCallback>
