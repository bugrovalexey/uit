﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpValuationReasonSubsidizeMap : ClassMapBase<ExpValuationReasonSubsidize>
    {
        public ExpValuationReasonSubsidizeMap()
            : base("Exp_Valuation_Reason_Subsidize")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Comm, "Exp_Valuation_Reason_Subsidize_Comm");            
            
            References<ExpCriterionEquivalentSubsidize>(x => x.ExpCriterionEquivalentSubsidize, "Exp_Criterion_Equivalent_Subsidize_ID");
            References<ExpReason>(x => x.ExpReason, "Exp_Reason_ID");
            References<ExpConformity>(x => x.ExpConformity, "Exp_Conformity_ID");


        }
    }
}