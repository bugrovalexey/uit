﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class SoftEmailTypeMap : ClassMapBase<SoftEmailType>
	{
        public SoftEmailTypeMap()
            : base("Soft_Email_Type")
        {
            //Table("w_");

            //Id(x => x.Id, "Soft_Email_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

            
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Soft_Email_Type_Name");
        }
    }
}