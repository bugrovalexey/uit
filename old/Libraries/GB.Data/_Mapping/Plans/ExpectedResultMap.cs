﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ExpectedResultMap : ClassMapBase<ExpectedResult>
    {
        public ExpectedResultMap()
            : base("Plans_Activity_Expected_Result")
        {
//            Table("w_");

//            Id(x => x.Id, "Plans_Activity_Expected_Result_ID").GeneratedBy.Custom(typeof(IdGenerator));

            


//            string sql = @"@Plans_Activity_Expected_Result_Year = ?
//                          ,@Plans_Activity_Expected_Result_Discr = ?
//                          ,@Plans_Activity_Expected_Result_Value = ?
//                          ,@Plans_Activity_ID = ?
//                          ,@Plans_Activity_Expected_Result_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Plans_Activity_Expected_Result_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Plans_Activity_Expected_Result_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Plans_Activity_Expected_Result_Delete @Plans_Activity_Expected_Result_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Year, "Plans_Activity_Expected_Result_Year");
            Map(x => x.Description, "Plans_Activity_Expected_Result_Discr");
            Map(x => x.Value, "Plans_Activity_Expected_Result_Value");
            

            References<PlansActivity>(x => x.PlansActivity, "Plans_Activity_ID").Not.Nullable();
        }
    }
}
