﻿using System;
using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Наименование программного решения в ФАП
    /// </summary>
    public class ActivityApfInfo : EntityBase
    {
        protected ActivityApfInfo()
        {
        }

        public ActivityApfInfo(PlansActivity PlansActivity)
        {
            this.PlansActivity = PlansActivity;
        }

        /// <summary>Мероприятия плана</summary>
        public virtual PlansActivity PlansActivity { get; set; }

        /// <summary>Номер в ФАП</summary>
        public virtual ApfInfo ApfInfo { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public virtual String Comment { get; set; }
    }
}
