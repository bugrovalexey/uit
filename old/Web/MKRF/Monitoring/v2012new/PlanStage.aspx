﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Monitoring/Monitoring.master" AutoEventWireup="true" CodeBehind="PlanStage.aspx.cs" Inherits="GB.MKRF.Web.Monitoring.v2012new.PlanStage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="buttons" runat="server">

    <script type="text/javascript">
        function reloadPage() {
            var args = [];
            args.push('year=' + YearsComboBox.GetValue());
            args.push('dep=' + DepartmentComboBox.GetValue());
            window.open(window.location.pathname + "?" + args.join('&'), '_self');
        }
    </script>

    <table>
        <tr>
            <td>Департамент:</td>
            <td>
                <dxe:ASPxComboBox runat="server" ID="DepartmentComboBox" Width="480px" ValueType="System.Int32"
                    ValueField="ID" TextField="Name" ClientInstanceName="DepartmentComboBox" OnDataBinding="DepartmentComboBox_DataBinding">
                    <ClientSideEvents SelectedIndexChanged="function(s,e) { reloadPage();  }" />
                </dxe:ASPxComboBox>
            </td>
            <td style="width:20px" />
            <td>Год:</td>
            <td style="width: 100px">
                <dxe:ASPxComboBox runat="server" ID="YearsComboBox" Width="80px" ValueType="System.Int32"
                    ValueField="Year" TextField="Year" ClientInstanceName="YearsComboBox" OnDataBinding="YearsComboBox_DataBinding">
                    <ClientSideEvents SelectedIndexChanged="function(s,e) { reloadPage();  }" />
                </dxe:ASPxComboBox>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="center" runat="server">
    <div style="margin: 0 auto; width: 1000px;">
        <dxchartsui:WebChartControl ID="Chart" Width="1000px" 
            runat="server" SideBySideEqualBarWidth="True" Height="700px" PaletteName="Origin">
            <padding bottom="0" left="0" right="0" top="0" />
            <legend alignmenthorizontal="Center" alignmentvertical="BottomOutside" font="Tahoma, 10pt" Visible="false">
                <margins top="20" />
            </legend>
            <titles>
	            <dxcharts:ChartTitle WordWrap="True" MaximumLinesCount="4" Text="Стадии плана информатизации" Font="Tahoma, 14pt"/>
	        </titles>
            <borderoptions visible="False" />
            <diagramserializable>
	            <dxcharts:XYDiagram>
		            <AxisX Title-Text="Этап"  >
		            </AxisX>

		            <AxisY Title-Text="Занолнено на, %" Title-Visible="True" >
		                <Label TextPattern="{A:#,##0}"/>
		            </AxisY>
		            <DefaultPane BackColor="White" EnableAxisXScrolling="False" EnableAxisYScrolling="False" EnableAxisXZooming="False" EnableAxisYZooming="False"/>
	            </dxcharts:XYDiagram>
	        </diagramserializable>
            <clientsideevents customdrawcrosshair="DrawCrosshair"></clientsideevents>
        </dxchartsui:WebChartControl>
    </div>
</asp:Content>
