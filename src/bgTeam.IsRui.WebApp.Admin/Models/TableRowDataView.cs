﻿namespace bgTeam.IsRui.WebApp.Admin.Models
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using bgTeam.IsRui.WebApp.Admin.Domain;

    public class TableRowDataView
    {
        public object PrimaryKeyValue { get; internal set; }

        public IDictionary<string, object> Values { get; internal set; }

        public TableRowDataView(dynamic item, string pkField = null)
        {
            var obj = (IDictionary<string, object>)item;

            Values = obj;

            if (pkField != null)
            {
                PrimaryKeyValue = obj[pkField];
            }
        }
    }
}