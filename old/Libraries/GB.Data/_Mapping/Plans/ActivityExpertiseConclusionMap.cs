﻿using GB.Data.Plans;
using GB.Data.Workflow;

namespace GB._Mapping.Plans
{
    internal class ActivityExpertiseConclusionMap : ClassMapHiLo<ActivityExpertiseConclusion>
    {
        public ActivityExpertiseConclusionMap()
            : base("Activity_Expertise_Conclusion") { }

        protected override void InitMap()
        {
            References<Status>(x => x.Status, "Status_ID").Not.Nullable();

            HasMany(x => x.Experts).KeyColumns.Add("Activity_Expertise_Conclusion_ID").Inverse();
        }
    }
}