﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/PlanCard.master" AutoEventWireup="true"
    CodeBehind="Workflow.aspx.cs" Inherits="GB.MKRF.Web.Plans.Workflow" %>

<%@ Register Src="../Common/StatusChangeControl.ascx" TagName="StatusChangeControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <div class="page">
        <uc1:StatusChangeControl ID="scc" runat="server" />
    </div>
</asp:Content>
