﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;

namespace GB.DataAccess.Command.AccountingObjects.ActsToAOActivities
{
    public class UpdateActToAOActivityCommand : ICommand<UpdateActToAOActivityContext>
    {
        ISessionFactoryGb factory;

        public UpdateActToAOActivityCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(UpdateActToAOActivityContext context)
        {
            var act = new Repository<ActOfAcceptance>().GetExisting(context.ActId);

            using (var session = factory.Create())
            {
                foreach (var actToActivId in context.ActToActivityIds)
                {
                    var actToActivity = new Repository<ActToAOActivity>().GetExisting(actToActivId);
                    actToActivity.ActOfAcceptance = act;

                    session.Update(actToActivity);
                }
                session.Commit();
            }
        }
    }
}
