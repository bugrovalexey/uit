﻿namespace bgTeam.IsRui.WebApp.Controllers
{
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Story.Common;
    using bgTeam.IsRui.WebApp.Model;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Threading.Tasks;

    [Authorize]
    [Route("api/[controller]/[action]")]
    public class FileController : Controller
    {
        private readonly IFileProviderRui _fileProvider;
        private readonly IStoryBuilder _storyBuilder;

        public FileController(IFileProviderRui fileProvider, IStoryBuilder storyBuilder)
        {
            _fileProvider = fileProvider;
            _storyBuilder = storyBuilder;
        }

        /// <summary>
        /// Загрузка файла во временную папку
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<FileModel> Upload([FromForm]IFormFile file)
        {
            return new FileModel
            {
                FileName = file.FileName,
                Guid = await _fileProvider.SaveAsync(file)
            };
        }

        /// <summary>
        /// Скачать файл по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор файла</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Download(int id)
        {
            var file = await _storyBuilder.Build(new FileDownloadStoryContext() { Id = id }).ReturnAsync<DownloadFileDto>();

            if (!string.IsNullOrEmpty(file.Url))
            {
                return Redirect(file.Url);
            }

            return File(file.FileData, file.MimeType, file.FileName);
        }
    }
}
