﻿using System;

namespace bgTeam.IsRui.WebApp.Controllers.From
{
    public class GovernmentContractAddFrom
    {
        public int IdAO { get; set; }
        public int IdZK { get; set; }

        public int Status { get; set; }
        public string Number { get; set; }
        public string Order { get; set; }
        public DateTime? Date { get; set; }
        public string Executer { get; set; }
        public decimal Price { get; set; }
        public string RcDeviation { get; set; }
    }
}