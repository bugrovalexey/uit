﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.AOGovServices
{
    public class DeleteAOGovServiceCommand : ICommand<DeleteAOGovServiceContext>
    {
        ISessionFactoryGb factory;

        public DeleteAOGovServiceCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteAOGovServiceContext context)
        {            
            if (context.xId.HasValue && context.xId != 0)
            {
                var entity = new Repository<AOGovService>().Get(context.xId.Value);

                if (entity != null)
                {
                    using (var session = factory.Create())
                    {                        
                        session.Delete(entity);
                        session.Commit();
                    }
                }
            }
        }
    }
}
