﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command
{
    public class AddOrSaveDirectoryContext : BaseAddOrSaveContext
    {
        public string Name { get; set; }
    }
}
