﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    public class AoConclusionAddOrUpdateStoryContext
    {
        public int? Id { get; set; }

        public string Comment { get; set; }

        public int AoId { get; set; }

        //public int? FileId { get; set; }
    }
}