﻿using System.ComponentModel;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class TableActToAoActivity : NotificationBaseViewModel
    {
        public int Id { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Год реализации")]
        public string Year { get; set; }

        [DisplayName("Тип")]
        public string Type { get; set; }

        [DisplayName("Очередной финансовый год, руб.")]
        public string CostY0 { get; set; }

        [DisplayName("Первый год планового периода, руб.")]
        public string CostY1 { get; set; }

        [DisplayName("Второй год планового периода, руб.")]
        public string CostY2 { get; set; }

        public string DeleteLink { get; set; }


        public TableActToAoActivity(ActToAOActivity actToAOActivity)
        {
            Id = actToAOActivity.Id;

            var a = actToAOActivity.AoActivity;
            Name = a.Name;
            Year = a.Year.ToString();
            Type = a.ActivityType.GetDescription();
            CostY0 = a.CostY0.ToString("N2");
            CostY1 = a.CostY1.ToString("N2");
            CostY2 = a.CostY2.ToString("N2");

            DeleteLink = LinkBuilder.AjaxDeleteLink("DeleteActToAOActivity", "AccountingObjects", new { Id }, "GridActToActivityList");
        }
    }
}