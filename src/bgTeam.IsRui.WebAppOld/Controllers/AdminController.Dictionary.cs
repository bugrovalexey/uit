﻿using bgTeam.IsRui.WebApp.ViewModels.Admin.Dictionary;
using bgTeam.IsRui.MKRF.DictyonaryEdit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Controllers
{
    partial class AdminController
    {
        public ActionResult Dictionaries(int? id = null)
        {
            var dicts = DictionaryRepository.GetDataDictionaries();

            //если передан id, то показывать нужный справочник, иначе первый. Если справочника с таким id нет, то первый.
            var dictId =
                dicts.FirstOrDefault(
                    x => x.Value.Visible && (!id.HasValue || !dicts.ContainsKey(id.Value) || x.Key == id)).Key;

            var model = new DictionaryViewModel
            {
                Dictionaries = new SelectList(dicts.Values.Where(x => x.Visible).ToList(), "Id", "Name", dictId),
                Dictionary = DictionaryRepository.GetDataDictionary(dictId)
            };

            return View("Dictionary/Dictionaries", model);
        }

        [HttpPost]
        public ActionResult GetDictionary(int id)
        {
            try
            {
                var dict = DictionaryRepository.GetDataDictionary(id);
                return PartialView("Dictionary/Dictionary", dict);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult DictionaryEdit(int dictId, int? itemId = null)
        {
            var er = string.Empty;
            try
            {
                var dict = DictionaryRepository.GetDataDictionary(dictId);
                var model = new DictionaryEditViewModel
                {
                    Columns = DictionaryRepository.GetDataDictionaryColumns(dictId),
                    DictId = dictId,
                    Title = "Добавление записи в справочник"
                };

                if (itemId.HasValue)
                {
                    model.Data = DictionaryRepository.GetDictionaryDataItem(dict, itemId.Value, ref er).Rows[0];
                    model.Title = "Редактирование записи справочника";
                }
                return View("Dictionary/Edit", model);
            }
            catch (Exception e)
            {
                throw e;
            }
            //return View("Dictionary/Edit");
        }

        [HttpPost]
        public ActionResult DictionaryEdit(FormCollection form, int dictId, int? itemId = null)
        {
            try
            {
                var dict = DictionaryRepository.GetDataDictionary(dictId);
                var values = dict.Columns.Values.Where(x => x.Visible).ToDictionary(column => column.ColumnName,
                                                                                    column =>
                                                                                    column.Type == ColumnType.TRef
                                                                                        ? form["hf_" + column.ColumnName]
                                                                                        : column.Type == ColumnType.TBool
                                                                                              ? (form[column.ColumnName] == "on"
                                                                                                     ? true.ToString()
                                                                                                     : false.ToString())
                                                                                              : form[column.ColumnName]);

                var er = itemId.HasValue ? DictionaryRepository.UpdateDictionaryItem(dict, itemId.Value, values) : DictionaryRepository.InsertDictionaryItem(dict, values);

                if (!string.IsNullOrWhiteSpace(er))
                {
                    TempData["SuccessMessage"] = er;
                    return RedirectToAction("DictionaryEdit", new { dictId, itemId });
                }

                TempData["SuccessMessage"] = "Изменения успешно сохранены";
                return RedirectToAction("Dictionaries", new { id = dictId });
            }
            catch (Exception ex)
            {
                TempData["SuccessMessage"] = ex.Message;
                return RedirectToAction("DictionaryEdit", new { dictId, itemId });
            }
        }

        [HttpPost]
        public JsonResult DictionaryDelete(int dictId, int itemId)
        {
            try
            {
                var dict = DictionaryRepository.GetDataDictionary(dictId);
                DictionaryRepository.DeleteDictionaryItem(dict, itemId);

                return Json(new
                {
                    id = itemId,
                    message = "Запись успешно удалена"
                });
            }
            catch
            {
            }

            return Json(new { });
        }

        public ActionResult DictionaryChoose(int dictId, int refDict, int refCol, string inputName)
        {
            try
            {
                var dict = DictionaryRepository.GetDataDictionary(refDict);
                var column = dict.Columns.Values.SingleOrDefault(x => x.ID == refCol);
                var childColumns =
                    DictionaryRepository.GetDataDictionary(dictId).Columns.Values.Where(
                        x => x.Type == ColumnType.TChild && x.RefId == refDict).ToList();

                var columns = new List<MetaDictionaryColumn> { column };
                columns.AddRange(childColumns);

                var model = new DictionaryChooseViewModel
                {
                    PrimaryKey = dict.PrimaryKey,
                    InputName = inputName,
                    Columns = columns,
                    Data = dict.Data
                };

                return View("Dictionary/Choose", model);
            }
            catch
            {
            }

            return View("Dictionary/Choose");
        }
    }
}