﻿using GB.Data.Workflow;

namespace GB.Data
{
    public interface IWorkflowObject : IEntity, IAccess
    {
        Status Status { get; set; }
    }
}