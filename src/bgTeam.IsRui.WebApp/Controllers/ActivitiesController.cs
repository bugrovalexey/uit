﻿namespace bgTeam.IsRui.WebApp.Controllers
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Story.AccountingObjects;
    using bgTeam.IsRui.Story.Activities;
    using bgTeam.IsRui.Story.Common;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Управление мероприятиями
    /// </summary>
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class ActivitiesController : Controller
    {
        private readonly IStoryBuilder _builder;

        public ActivitiesController(IStoryBuilder storyBuilder)
        {
            _builder = storyBuilder;
        }

        /// <summary>
        /// Получить список мероприятий
        /// </summary>
        /// <param name="context">Идентификатор ОУ</param>
        [HttpPost]
        public async Task<IEnumerable<ActivityDto>> GetMyList([FromBody] GetListActivitiesStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivityDto>>();
        }

        /// <summary>
        /// Получить информациб о мероприятии
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivityInfoDto> GetObjectInfo([FromBody] GetObjectInfoStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityInfoDto>();
        }

        /// <summary>
        /// Создать мероприятие
        /// </summary>
        /// <param name="context">Данные создоваемого мероприятия</param>
        [HttpPost]
        public async Task<ActivityDto> Create([FromBody] CreateActivityStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityDto>();
        }

        /// <summary>
        /// Удаление мероптриятия
        /// </summary>
        /// <param name="context">Идентификатор уталяемого мероприятия</param>
        [HttpPost]
        public async Task<bool> Delete([FromBody] DeleteActivityStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Обновить данные по расходам
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivityCostDto> CostsUpdateCostsStory([FromBody] CostsUpdateCostsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityCostDto>();
        }

        /// <summary>
        /// Получить данные по расходам
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivityCostDto> CostsGetCosts([FromBody] CostsGetCostsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityCostDto>();
        }

        /// <summary>
        /// Получить документы мероприятия
        /// </summary>
        /// <param name="context">Идентификатор ОУ</param>
        [HttpPost]
        public async Task<IEnumerable<ActivityDocumentDto>> DocumentsGetAllDocuments([FromBody] DocumentsGetAllDocumentsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivityDocumentDto>>();
        }

        /// <summary>
        /// Создать документ мероприятия
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivityDocumentDto> DocumentsCreateOrUpdateDocument([FromBody] DocumentsCreateOrUpdateDocumentStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityDocumentDto>();
        }

        /// <summary>
        /// Удаление документа мероптриятия
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<bool> DocumentsDeleteDocument([FromBody] DocumentsDeleteDocumentStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить услуги
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<IEnumerable<ActivitiesServiceDto>> ServicesGetAllServices([FromBody] ServicesGetAllServicesStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivitiesServiceDto>>();
        }

        /// <summary>
        /// Создать/обновить услугу
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivitiesServiceDto> ServicesCreateOrUpdateService([FromBody] ServicesCreateOrUpdateServiceStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivitiesServiceDto>();
        }

        /// <summary>
        /// Удаление услуги
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<bool> ServicesDeleteService([FromBody] ServicesDeleteServiceStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить индикаторы
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<IEnumerable<ActivitysMarksDto>> MarksGetAllMarks([FromBody] MarksGetAllMarksStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivitysMarksDto>>();
        }

        /// <summary>
        /// Создать/обновить индикатор
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivitysMarksDto> MarksCreateOrUpdateMark([FromBody] MarksCreateOrUpdateMarkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivitysMarksDto>();
        }

        /// <summary>
        /// Удалениеиндикатора
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<bool> MarksDeleteMarks([FromBody] MarksDeleteMarksStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить показатели
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<IEnumerable<ActivityIndicatorDto>> IndicatorsGetAllIndicators([FromBody] IndicatorsGetAllIndicatorsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivityIndicatorDto>>();
        }

        /// <summary>
        /// Создать/обновить показатели
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivityIndicatorDto> IndicatorsCreateOrUpdateIndicatork([FromBody] IndicatorsCreateOrUpdateIndicatorStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityIndicatorDto>();
        }

        /// <summary>
        /// Удаление показателей
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<bool> IndicatorsDeleteIndicator([FromBody] IndicatorsDeleteIndicatorStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить работы
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<IEnumerable<ActivityWorksDto>> WorksGetAllWork([FromBody] WorksGetAllWorkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivityWorksDto>>();
        }

        /// <summary>
        /// Создать/обновить работы
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivityWorksDto> WorksCreateOrUpdateWork([FromBody] WorksCreateOrUpdateWorkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityWorksDto>();
        }

        /// <summary>
        /// Удаление работ
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<bool> WorksDeleteWork([FromBody] WorksDeleteWorkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить основные сведения
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivityCommonInfoDto> CommonInfoGetInfo([FromBody] CommonInfoGetInfoStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityCommonInfoDto>();
        }

        /// <summary>
        /// Обновить основные сведения
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivityCommonInfoDto> CommonInfoUpdateInfo([FromBody] CommonInfoUpdateInfoStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityCommonInfoDto>();
        }

        #region Conclusion
        /// <summary>
        /// Получение заключение для текущего пользователя
        /// </summary>
        /// <param name="context">
        /// - Id индетификато заключения
        /// </param>

        [HttpPost]
        [Produces("application/json")]
        public async Task<WorkflowUserConclusionDto> ConclusionGet([FromBody] WorkflowUserConclusionGetByIdStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<WorkflowUserConclusionDto>();
        }

        /// <summary>
        /// Добавить либо обновить информацию по заключению
        /// </summary>
        /// <param name="context">
        /// </param>

        [HttpPost]
        [Produces("application/json")]
        public async Task<WorkflowUserConclusionDto> ConclusionSaveOrUpdateByActivityId([FromBody] WorkflowUserConclusionSaveOrUpdateStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));
            context.Id = null;

            return await _builder
                .Build(context)
                .ReturnAsync<WorkflowUserConclusionDto>();
        }

        /// <summary>
        /// Добавить либо обновить информацию по заключению
        /// </summary>
        /// <param name="context">
        /// </param>

        [HttpPost]
        [Produces("application/json")]
        public async Task<WorkflowUserConclusionDto> ConclusionSaveOrUpdateById([FromBody] WorkflowUserConclusionSaveOrUpdateStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));
            context.ActivityId = null;

            return await _builder
                .Build(context)
                .ReturnAsync<WorkflowUserConclusionDto>();
        }
        #endregion

        /// <summary>
        /// Получить товары
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<IEnumerable<ActivityGoodsDto>> GoodsGetAllGoods([FromBody] GoodsGetAllGoodsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivityGoodsDto>>();
        }

        /// <summary>
        /// Создать/обновить товары
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivityGoodsDto> GoodsCreateOrUpdateGoods([FromBody] GoodsCreateOrUpdateGoodsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityGoodsDto>();
        }

        /// <summary>
        /// Удаление товара
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<bool> GoodsDeleteGoods([FromBody] GoodsDeleteGoodsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Создать/обновить характеристику товара
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<GoodsCharacteristicDto> GoodsAddOrUpdateGoodsCharacteristic([FromBody] GoodsAddOrUpdateGoodsCharacteristicStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<GoodsCharacteristicDto>();
        }

        /// <summary>
        /// Удаление характеристики товара
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<bool> GoodsDeleteGoodsCharacteristic([FromBody] GoodsDeleteGoodsCharacteristicStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить историю заключений
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<IEnumerable<ConclusionHistoryDto>> GetConclusionHistory([FromBody] GetConclusionHistoryStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ConclusionHistoryDto>>();
        }

        /// <summary>
        /// Добавить файл заключения
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<FilesDto> ConclusionAddFile([FromBody] ConclusionAddFileStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<FilesDto>();
        }

        /// <summary>
        /// Удалить файл заключения
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<bool> ConclusionDeleteFile([FromBody] FileDeleteStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получить иерархию мероприятия для Руководителя
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<IEnumerable<ActivityDto>> GetGovtOrganHierarchy([FromBody] GetGovtOrganHierarchyStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivityDto>>();
        }

        /// <summary>
        /// Получить информационное взаимодействие
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<InformationInteractionDto> GetInformationInteraction([FromBody] GetInformationInteractionStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<InformationInteractionDto>();
        }

        /// <summary>
        /// Обновить информационное взаимодействие
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<InformationInteractionDto> SetInformationInteraction([FromBody] SetInformationInteractionStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<InformationInteractionDto>();
        }

        /// <summary>
        /// Добавить или обновить акт
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<ActivityActDto> CreateOrUpdateAct([FromBody] ActivityCreateOrUpdateActStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityActDto>();
        }

        /// <summary>
        /// Получить акты мероприятия
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<IEnumerable<ActivityActDto>> GetActs([FromBody] ActivityGetActsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivityActDto>>();
        }

        /// <summary>
        /// Удалить акт
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        public async Task<bool> DeleteAct([FromBody] ActivityDeleteActStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Добавить/обновить контракт
        /// </summary>
        [HttpPost]
        public async Task<ActivityContractDto> AddOrUpdateContract([FromBody] ActivityAddOrUpdateContractStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityContractDto>();
        }

        /// <summary>
        /// Получить список контрактов
        /// </summary>
        [HttpPost]
        public async Task<IEnumerable<ActivityContractDto>> GetContracts([FromBody] ActivityGetContractsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivityContractDto>>();
        }

        /// <summary>
        /// Удалить контракт
        /// </summary>
        [HttpPost]
        public async Task<bool> DeleteContract([FromBody] ActivityDeleteContractStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Добавить/обновить закупку
        /// </summary>
        [HttpPost]
        public async Task<ActivityPurchaseDto> AddOrUpdatePurchase([FromBody] ActivityAddOrUpdatePurchaseStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActivityPurchaseDto>();
        }

        /// <summary>
        /// Получить список закупок
        /// </summary>
        [HttpPost]
        public async Task<IEnumerable<ActivityPurchaseDto>> GetPurchases([FromBody] ActivityGetPurchasesStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivityPurchaseDto>>();
        }

        /// <summary>
        /// Удалить закупку
        /// </summary>
        [HttpPost]
        public async Task<bool> DeletePurchase([FromBody] ActivityDeletePurchaseStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }
    }
}
