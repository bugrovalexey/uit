﻿using GB.MKRF.Entities.AccountingObjects;

namespace GB.MKRF.Mapping.AccountingObjects
{
    internal class StatusAccountingObjectMap : ClassMapBase<StatusAccountingObject>
    {
        public StatusAccountingObjectMap()
            : base("OU_Status")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Status_Name");
        }
    }
}