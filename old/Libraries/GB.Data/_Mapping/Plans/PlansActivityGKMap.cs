﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class PlansActivityGKMap : ClassMapBase<PlansActivityGK>
    {
        public PlansActivityGKMap()
            : base("Plans_Activity_GK")
        {
//            Table("w_");

//            Id(x => x.Id, "Plans_Activity_GK_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@Plans_Activity_GK_DT = ?
//                          ,@Plans_Activity_GK_Num = ?
//                          ,@Plans_Activity_GK_Predmet = ?
//                          ,@Plans_Activity_GK_Year = ?
//                          ,@Plans_Activity_ID = ?
//                          ,@Plans_Activity_GK_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Plans_Activity_GK_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Plans_Activity_GK_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Plans_Activity_GK_Delete @Plans_Activity_GK_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Date, "Plans_Activity_GK_DT");
            Map(x => x.Num, "Plans_Activity_GK_Num");
            Map(x => x.Subject, "Plans_Activity_GK_Predmet");
            Map(x => x.Year, "Plans_Activity_GK_Year");

            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID").Not.Nullable();
        }
    }
}
