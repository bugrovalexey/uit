﻿using System.Web.UI;

namespace GB.Helpers
{
    public static class ControlHelper
    {
        public static Control FindControl(this Control root, string id)
        {
            if (root.ID == id)
                return root;

            foreach (Control Ctl in root.Controls)
            {
                Control FoundCtl = FindControl(Ctl, id);
                if (FoundCtl != null)
                    return FoundCtl;
            }
            return null;
        }
    }
}