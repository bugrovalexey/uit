﻿namespace GB.Data.Directories
{
    /// <summary>
    /// Классификационная категория видов обеспечения
    /// </summary>
    public class CategoryKindOfSupport : EntityDirectories
    {
        /// <summary>
        /// Вид обеспечения
        /// </summary>
        public virtual CategoryTypeOfSupportEnum Type { get; set; }
    }
}