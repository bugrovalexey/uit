﻿using System.Collections.Generic;
using GB.Data.AccountingObjects;

namespace GB.DataAccess.Repository
{
    public class AODecisionToCreateRepository : Repository<AODecisionToCreate>
    {
        public IList<AODecisionToCreate> GetAllByAccountingObject(int? accountingObjectId)
        {
            return GetAllEx(q => q.Where(x => x.AccountingObject.Id == accountingObjectId));
        }
    }
}