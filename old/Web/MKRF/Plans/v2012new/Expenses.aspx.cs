﻿using System;
using System.Collections.Generic;
using System.Linq;
using GB;
using GB.Entity.Directories;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Web.Helpers;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class Expenses : System.Web.UI.Page
    {
        /// <summary>
        /// Текущее мероприятие
        /// </summary>
        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as ActivityCard).CurrentActivity;
            }
        }

        /// <summary>
        /// Разрешено редактировать мероприятие ?
        /// </summary>
        public bool EnableEdit
        {
            get
            {
                return (Master as ActivityCard).EnableEdit;
            }
        }

        public static string GetUrl(int activityId)
        {
            return UrlHelper.Resolve("~/Plans/v2012new/Expenses.aspx?ActivityId=" + activityId.ToString());
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            ExpenseDirectionsGrid.Columns["ExpenseVolY0"].Caption = string.Concat(CurrentActivity.Plan.Year.Value, " г.");
            ExpenseDirectionsGrid.Columns["ExpenseVolY1"].Caption = string.Concat(CurrentActivity.Plan.Year.Value + 1, " г.");
            ExpenseDirectionsGrid.Columns["ExpenseVolY2"].Caption = string.Concat(CurrentActivity.Plan.Year.Value + 2, " г.");

            ExpenseDirectionsGrid.Columns["ExpenseAddVolY0"].Caption = string.Concat(CurrentActivity.Plan.Year.Value, " г.");
            ExpenseDirectionsGrid.Columns["ExpenseAddVolY1"].Caption = string.Concat(CurrentActivity.Plan.Year.Value + 1, " г.");
            ExpenseDirectionsGrid.Columns["ExpenseAddVolY2"].Caption = string.Concat(CurrentActivity.Plan.Year.Value + 2, " г.");

            ExpenseDirectionsGrid.Columns["IY0"].Caption = string.Concat(CurrentActivity.Plan.Year.Value, " г.");
            ExpenseDirectionsGrid.Columns["IY1"].Caption = string.Concat(CurrentActivity.Plan.Year.Value + 1, " г.");
            ExpenseDirectionsGrid.Columns["IY2"].Caption = string.Concat(CurrentActivity.Plan.Year.Value + 2, " г.");


            ExpenseItemMemo.DataBind();
            GRBSMemo.DataBind();
            SectionKBKMemo.DataBind();
            WorkFormMemo.DataBind();
//            ExpenditureItemsMemo.DataBind();

            base.OnPreRender(e);
        }

        protected void Memo_DataBinding(object sender, EventArgs e)
        {
            var memo = sender as ASPxTextBox;
            switch (memo.ID)
            {
                case "GRBSMemo":
                    SetHF(memo, "GRBS", CurrentActivity.GRBS);
                    break;
                case "SectionKBKMemo":
                    SetHF(memo, "SectionKBK", CurrentActivity.SectionKBK);
                    break;
                case "ExpenseItemMemo":
                    SetHF(memo, "ExpenseItem", CurrentActivity.ExpenseItem);
                    break;
                case "WorkFormMemo":
                    SetHF(memo, "WorkForm", CurrentActivity.WorkForm);
                    break;
//                case "ExpenditureItemsMemo":
//                    SetHF(memo, "ExpenditureItems", CurrentActivity.ExpenditureItem);
//                    break;
                default:
                    throw new ApplicationException("Отсутствие элемента");
            }            
        }

        private void SetHF(ASPxTextBox memo, string name, IEntityCodeDirectories entity)
        {
            if ((CurrentActivity == null) || (entity == null))
            {
                memo.Text = "- Не задано";
                hf.Set(name + "Id", 0);
                hf.Set(name + "Name", string.Empty);
                hf.Set(name + "Code", string.Empty);
            }
            else
            {
                memo.Text = entity.Code.ToString() + " " + entity.Name;
                hf.Set(name + "Id", entity.Id);
                hf.Set(name + "Name", entity.Name);
                hf.Set(name + "Code", entity.Code);
            }            
        }       

        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                loadData();
            }
            base.OnLoad(e);
        }

        protected override void OnInit(EventArgs e)
        {

            base.OnInit(e);
        }

        void loadData()
        {
            SubentryTextBox.Text = CurrentActivity.Subentry;

            PlansActivityVolY0SpinEdit.Value = CurrentActivity.PlansActivityVolY0;
            PlansActivityVolY1SpinEdit.Value = CurrentActivity.PlansActivityVolY1;
            PlansActivityVolY2SpinEdit.Value = CurrentActivity.PlansActivityVolY2;

            PlansActivityAddVolY0SpinEdit.Value = CurrentActivity.PlansActivityAddVolY0;
            PlansActivityAddVolY1SpinEdit.Value = CurrentActivity.PlansActivityAddVolY1;
            PlansActivityAddVolY2SpinEdit.Value = CurrentActivity.PlansActivityAddVolY2;

            if (CurrentActivity.PlansActivityFactDate.HasValue)
                PlansActivityFactDate.Date = CurrentActivity.PlansActivityFactDate.Value;

            PlansActivityFactVolY0SpinEdit.Value = CurrentActivity.PlansActivityFactVolY0;
            PlansActivityFactVolY1SpinEdit.Value = CurrentActivity.PlansActivityFactVolY1;
            PlansActivityFactVolY2SpinEdit.Value = CurrentActivity.PlansActivityFactVolY2;
            
            AddExpenseDirectionsButton.Visible = EnableEdit;
            ExpenseDirectionsGrid.Columns["cmdColumn"].Visible = EnableEdit;
            ExpenseDirectionsGrid.DataBind();

            controlsEnability(EnableEdit);
        }

        class CodeFormat : IFormatProvider, ICustomFormatter
        {
            public object GetFormat(Type formatType)
            {
                if (formatType == typeof(ICustomFormatter))
                    return this;
                else
                    return null;
            }

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                string res = arg as string;

                if (res.Length < 3)
                {
                    for (int i = res.Length; i < 3; i++)
                    {
                        res = string.Concat("0", res);
                    }
                }

                return res;
            }
        }
        
        void bindCombo(ASPxComboBox box, object dataSource)
        {
            box.DataSource = dataSource;
            box.DataBind();
        }

        void controlsEnability(bool enableEdit)
        {
            //GRBSComboBox.Enabled = enableEdit;
            //SectionKBKComboBox.Enabled = enableEdit;
            //WorkFormComboBox.Enabled = enableEdit;
            PlansActivityVolY0SpinEdit.ClientEnabled = enableEdit;
            PlansActivityVolY1SpinEdit.ClientEnabled = enableEdit;
            PlansActivityVolY2SpinEdit.ClientEnabled = enableEdit;
            PlansActivityFactVolY0SpinEdit.ClientEnabled = enableEdit;
            PlansActivityFactVolY1SpinEdit.ClientEnabled = enableEdit;
            PlansActivityFactVolY2SpinEdit.ClientEnabled = enableEdit;
            PlansActivityAddVolY0SpinEdit.ClientEnabled = enableEdit;
            PlansActivityAddVolY1SpinEdit.ClientEnabled = enableEdit;
            PlansActivityAddVolY2SpinEdit.ClientEnabled = enableEdit;
            SubentryTextBox.ClientEnabled = enableEdit;
            PlansActivityFactDate.ClientEnabled = enableEdit;

            saveButton.ClientEnabled = enableEdit;
        }

        protected void callBackEx_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            var controller = new PlansActivityController();
            controller.SaveExpenses(
                new PlansActivityArgs()
                {
                    Id = CurrentActivity.Id,
                    KOSGU = Convert.ToInt32(SaveValue("ExpenditureItems")),
                    GRBS = Convert.ToInt32(SaveValue("GRBS")),
                    SectionKBK = Convert.ToInt32(SaveValue("SectionKBK")),
                    WorkForm = Convert.ToInt32(SaveValue("WorkForm")),
                    CSR = Convert.ToInt32(SaveValue("ExpenseItem")),
                    Subentry = SubentryTextBox.Text,
                    VolY0 = PlansActivityVolY0SpinEdit.Number,
                    VolY1 = PlansActivityVolY1SpinEdit.Number,
                    VolY2 = PlansActivityVolY2SpinEdit.Number,

                    PlansActivityAddVolY0 = PlansActivityAddVolY0SpinEdit.Number,
                    PlansActivityAddVolY1 = PlansActivityAddVolY1SpinEdit.Number,
                    PlansActivityAddVolY2 = PlansActivityAddVolY2SpinEdit.Number,

                    PlansActivityFactVolY0 = PlansActivityFactVolY0SpinEdit.Number,
                    PlansActivityFactVolY1 = PlansActivityFactVolY1SpinEdit.Number,
                    PlansActivityFactVolY2 = PlansActivityFactVolY2SpinEdit.Number,
                    PlansActivityFactDate = PlansActivityFactDate.Date
                });
        }

        private object SaveValue(string name) 
        {
            if (hf.Contains(name + "Id"))
            {
                return hf[name + "Id"];
            }
            return null;
        }

        #region ExpenseDirectionGrid

        bool isNewExpenseDirectionsRow = false;

        protected void ExpenseDirectionsGrid_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            if (!EnableEdit)
                throw new Exception("Добавление запрещено");
            isNewExpenseDirectionsRow = true;
        }

        protected void ExpenseDirectionsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            if (CurrentActivity != null)
            {
                ExpenseDirectionsGrid.DataSource = CurrentActivity.ExpenseDirections.OrderBy(x => x.ExpenseDirection.Code)
                    .Select(x => new
                    {
                        Id = x.Id,
                        CodeName = x.ExpenseDirection.CodeName,
                        ExpenseVolY0 = x.ExpenseVolY0,
                        ExpenseVolY1 = x.ExpenseVolY1,
                        ExpenseVolY2 = x.ExpenseVolY2,
                        ExpenseAddVolY0 = x.ExpenseAddVolY0,
                        ExpenseAddVolY1 = x.ExpenseAddVolY1,
                        ExpenseAddVolY2 = x.ExpenseAddVolY2,
                        IY0 = x.ExpenseVolY0 + x.ExpenseAddVolY0,
                        IY1 = x.ExpenseVolY1 + x.ExpenseAddVolY1,
                        IY2 = x.ExpenseVolY2 + x.ExpenseAddVolY2,
                    });
            }
        }

        ActivityExpenseDirections currentDirection;

        protected void ExpenseDirectionsGrid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            currentDirection = RepositoryBase<ActivityExpenseDirections>.Get(e.EditingKeyValue);
        }

        protected void ExpenseDirectionsGrid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            ActivityExpenseDirections direction = new ActivityExpenseDirections(CurrentActivity);
            saveExpenseDirectionsRow(direction);
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void ExpenseDirectionsGrid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ActivityExpenseDirections direction = RepositoryBase<ActivityExpenseDirections>.Get((int)e.Keys[0]);
            saveExpenseDirectionsRow(direction);
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void ExpenseDirectionsGrid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            RepositoryBase<ActivityExpenseDirections>.Delete((int)e.Keys[0]);
            RepositoryBase<PlansActivity>.Refresh(CurrentActivity);
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        void saveExpenseDirectionsRow(ActivityExpenseDirections direction)
        {
            direction.ExpenseDirection = RepositoryBase<ExpenseDirection>.Get(Convert.ToInt32(hfExpenseDirections["Id"]));
            direction.ExpenseVolY0 = Convert.ToDecimal(hfExpenseDirections["Y0"]);
            direction.ExpenseVolY1 = Convert.ToDecimal(hfExpenseDirections["Y1"]);
            direction.ExpenseVolY2 = Convert.ToDecimal(hfExpenseDirections["Y2"]);
            direction.ExpenseAddVolY0 = Convert.ToDecimal(hfExpenseDirections["AY0"]);
            direction.ExpenseAddVolY1 = Convert.ToDecimal(hfExpenseDirections["AY1"]);
            direction.ExpenseAddVolY2 = Convert.ToDecimal(hfExpenseDirections["AY2"]);

            RepositoryBase<ActivityExpenseDirections>.SaveOrUpdate(direction);
            RepositoryBase<PlansActivity>.Refresh(CurrentActivity);
        }

        protected void ExpenseDirectionComboBox_DataBinding(object sender, EventArgs e)
        {
            if (!isNewExpenseDirectionsRow && currentDirection == null)
                return;

            IList<ExpenseDirection> items = GetExpenseDirections(CurrentActivity);

            Dictionary<int, string> listStr = new Dictionary<int, string>();

            foreach (ExpenseDirection item in items)
            {
                listStr.Add(item.Id, item.CodeName);
            }

            if (currentDirection != null)
                listStr.Add(currentDirection.ExpenseDirection.Id, currentDirection.ExpenseDirection.CodeName);

            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = listStr.OrderBy(x => x.Key);

            box.Value = isNewExpenseDirectionsRow ? listStr.FirstOrDefault().Key : currentDirection.ExpenseDirection.Id;
        }

        protected void Y0TextBox_DataBinding(object sender, EventArgs e)
        {
            if (currentDirection != null)
                (sender as ASPxTextBox).Value = currentDirection.ExpenseVolY0;
        }

        protected void Y1TextBox_DataBinding(object sender, EventArgs e)
        {
            if (currentDirection != null)
                (sender as ASPxTextBox).Value = currentDirection.ExpenseVolY1;
        }

        protected void Y2TextBox_DataBinding(object sender, EventArgs e)
        {
            if (currentDirection != null)
                (sender as ASPxTextBox).Value = currentDirection.ExpenseVolY2;
        }

        protected void AddY0TextBox_DataBinding(object sender, EventArgs e)
        {
            if (currentDirection != null)
                (sender as ASPxTextBox).Value = currentDirection.ExpenseAddVolY0;
        }

        protected void AddY1TextBox_DataBinding(object sender, EventArgs e)
        {
            if (currentDirection != null)
                (sender as ASPxTextBox).Value = currentDirection.ExpenseAddVolY1;
        }

        protected void AddY2TextBox_DataBinding(object sender, EventArgs e)
        {
            if (currentDirection != null)
                (sender as ASPxTextBox).Value = currentDirection.ExpenseAddVolY2;
        }

        protected void ExpenseDirectionsGrid_SummaryDisplayText(object sender, ASPxGridViewSummaryDisplayTextEventArgs e)
        {
            if (e.IsTotalSummary)
            {
                (sender as ASPxGridView).Columns[e.Item.FieldName].FooterCellStyle.ForeColor = System.Drawing.Color.Black;

                switch (e.Item.FieldName)
                {
                    case "ExpenseVolY0":
                        if (CurrentActivity.PlansActivityVolY0 < Convert.ToDouble(e.Value))
                            SetErrorCell(sender as ASPxGridView, e.Item.FieldName);

                        break;
                    case "ExpenseVolY1":
                        if (CurrentActivity.PlansActivityVolY1 < Convert.ToDouble(e.Value))
                            SetErrorCell(sender as ASPxGridView, e.Item.FieldName);

                        break;
                    case "ExpenseVolY2":
                        if (CurrentActivity.PlansActivityVolY2 < Convert.ToDouble(e.Value))
                            SetErrorCell(sender as ASPxGridView, e.Item.FieldName);

                        break;
                    case "ExpenseAddVolY0":
                        if (CurrentActivity.PlansActivityAddVolY0 < Convert.ToDouble(e.Value))
                            SetErrorCell(sender as ASPxGridView, e.Item.FieldName);

                        break;
                    case "ExpenseAddVolY1":
                        if (CurrentActivity.PlansActivityAddVolY1 < Convert.ToDouble(e.Value))
                            SetErrorCell(sender as ASPxGridView, e.Item.FieldName);

                        break;
                    case "ExpenseAddVolY2":
                        if (CurrentActivity.PlansActivityAddVolY2 < Convert.ToDouble(e.Value))
                            SetErrorCell(sender as ASPxGridView, e.Item.FieldName);

                        break;
                    case "IY0":
                        if (Math.Round(getSummYear(CurrentActivity.PlansActivityVolY0, CurrentActivity.PlansActivityAddVolY0), 4) < Convert.ToDouble(e.Value))
                            SetErrorCell(sender as ASPxGridView, e.Item.FieldName);

                        break;
                    case "IY1":
                        if (Math.Round(getSummYear(CurrentActivity.PlansActivityVolY1, CurrentActivity.PlansActivityAddVolY1), 4) < Convert.ToDouble(e.Value))
                            SetErrorCell(sender as ASPxGridView, e.Item.FieldName);

                        break;
                    case "IY2":
                        if (Math.Round(getSummYear(CurrentActivity.PlansActivityVolY2, CurrentActivity.PlansActivityAddVolY2), 4) < Convert.ToDouble(e.Value))
                            SetErrorCell(sender as ASPxGridView, e.Item.FieldName);

                        break;
                }
            }
        }

        private double getSummYear(double? y, double? add)
        {
            var v = y + add;

            return v.HasValue ? v.Value : 0;
        }




        public static IList<ExpenseDirection> GetExpenseDirections(PlansActivity activity)
        {
            HashSet<int> addedEDIds = new HashSet<int>();
            foreach (ActivityExpenseDirections aed in activity.ExpenseDirections)
                //if (aed != currentDirection)
                addedEDIds.Add(aed.ExpenseDirection.Id);

            IList<ExpenseDirection> items = DirectoriesRepositary.GetDirectories<ExpenseDirection>(activity.Plan.Year.Value);
            //RepositoryBase<ExpenseDirectionToYear>.GetAll(x => x.Year == activity.Plan.Year.Year);
            items = (from i in items
                     where !addedEDIds.Contains(i.Id)
                     orderby i.Code
                     select i).ToList();
            return items;
        }

        private static void SetErrorCell(ASPxGridView grid, string field)
        {
            grid.Columns[field].FooterCellStyle.ForeColor = System.Drawing.Color.Red;
        }
        #endregion      
    }
}