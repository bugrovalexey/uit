﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ReportFormMap : ClassMapBase<ReportForm>
    {
        public ReportFormMap()
            : base("Report_Forms")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Description, "Report_Forms_Descr");
            Map(x => x.Name, "Report_Forms_Name");
            Map(x => x.Period, "Report_Forms_Period");

            References<Plan>(x => x.Plan, "Plans_ID");
            References<ManagementRole>(x => x.Coordination, "Gov_Struct_Role_Coordination_ID");
            References<ManagementRole>(x => x.Preparation, "Gov_Struct_Role_Preparation_ID");
            References<ManagementRole>(x => x.Statement, "Gov_Struct_Role_Statement_ID");
        }
    }
}