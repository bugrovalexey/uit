﻿using GB.Data.AccountingObjects;
using GB.Data.Extensions;
using GB.Data.Workflow;
using GB.DataAccess.Repository;

namespace GB.MKRF.Logic.Access.Handlers
{
    public class AccountingObjectAccessHandler : AccessHandlerBase<AccountingObject>
    {
        public override AccountingObject Entity { get; set; }


        public AccountingObjectAccessHandler(AccountingObject ao)
            : base(ao)
        {
        }

        public override bool CanRead()
        {
            if (base.CanRead() == false)
            {
                return false;
            }

            var user = UserRepository.GetCurrent();

            if (user.Roles.IsEq(Data.Common.RoleEnum.Institution))
            {
                if (Entity.Department.Id == user.Department.Id)
                    return true;
            }

            if (user.Roles.IsEq(Data.Common.RoleEnum.CA))
            {
                //if (Entity.Parent.Department.Id == user.Department.Id)
                return true;
            }

            return false;
        }

        public override bool CanEdit()
        {
            if (base.CanEdit() == false)
            {
                return false;
            }

            var user = UserRepository.GetCurrent();

            if (Entity.Department.Id == user.Department.Id)
            {
                return Entity.Status.IsEq(StatusEnum.AoDraft, StatusEnum.AoOnCompletion);                
            }

            return false;
        }

        public override bool CanDelete()
        {
            if (base.CanDelete() == false)
            {
                return false;
            }

            var user = UserRepository.GetCurrent();

            if (Entity.Department.Id == user.Department.Id)
            {
                return Entity.Status.IsEq(StatusEnum.AoDraft, StatusEnum.AoOnCompletion);
            }

            return false;
        }

    }
}
