﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class NPATypeMap : ClassMapBase<NPAType>
    {
        public NPATypeMap()
            : base("NPA_Type")
        {
            //            Table("w_");

            //            Id(x => x.Id, "NPA_Type_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            string args = @" @NPA_Type_Name = ?
            //                            ,@NPA_Type_ID = ?";

            //            SqlInsert(@"exec ZZZ_Prc_NPA_Type_Insert " + args).Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_NPA_Type_Update " + args).Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_NPA_Type_Delete
            //						  @NPA_Type_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "NPA_Type_Name");
        }
    }
}