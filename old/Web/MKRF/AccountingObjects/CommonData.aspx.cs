﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB;
using GB.Controls;
using GB.Extentions;
using GB.Handlers;
using GB.Helpers;
using GB.MKRF.Dtos;
using GB.MKRF.Entities.AccountingObjects;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Extensions;
using GB.Repository;
using DevExpress.Web.ASPxCallback;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.Data;
using NHibernate.Hql.Ast.ANTLR;
using Stimulsoft.Report;
using GB.Controls.Extensions;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.AccountingObjects
{
    public partial class CommonData : BasePageAccountingObject
    {

        #region Prop

        #endregion Prop


        #region Handlers

        #region PageHandlers
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            txtNumber.DataBind();
            txtFullName.DataBind();
            cbxStatus.DataBind();
            cbxType.DataBind();
            cbxKind.DataBind();
            gridDocuments.DataBind();
        }

        #endregion PageHandlers

        #region Control Handlers

        #region Grid

        #region EditFom

        protected void txtDocName_OnDataBinding(object sender, EventArgs e)
        {
            DocumentController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Name));
        }

        protected void txtDocNumber_OnDataBinding(object sender, EventArgs e)
        {
            DocumentController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Number));
        }

        protected void txtFioSigner_OnDataBinding(object sender, EventArgs e)
        {
            DocumentController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.FioSigner));
        }

        protected void txtPostSigner_OnDataBinding(object sender, EventArgs e)
        {
            DocumentController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.PostSigner));
        }

        protected void txtUrl_OnDataBinding(object sender, EventArgs e)
        {
            DocumentController.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Url));
        }

        protected void fuFiles_OnDataBinding(object sender, EventArgs e)
        {
            DocumentController
                .With(x => x.CurrentEdit)
                .With(currentEdit => currentEdit.Documents)
                .Do(
                    documents =>
                        FileUploaderDataBinding(sender as FileUploadControl2, documents));
        }

        #endregion EditForm


        protected void gridDocuments_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            gridDocuments.DataSource = DocumentController.Repository.GetAllByAccountingObject(AccountingObjectId);
        }

        protected void gridDocuments_OnRowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            DocumentController.Create(GetDocumentAccountingObjectArgs());
            gridDocuments.GridCancelEdit(e);
        }

        protected void gridDocuments_OnRowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            DocumentController.Update(GetDocumentAccountingObjectArgs((int)e.Keys[0]));
            gridDocuments.GridCancelEdit(e);
        }

        protected void gridDocuments_OnStartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            DocumentController.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void gridDocuments_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            DocumentController.Delete(new DocumentAccountingObjectArgs()
            {
                Id = (int)e.Keys[0]
            });

            gridDocuments.GridCancelEdit(e);
        }

        #endregion Grid

        #region Forms

        protected void cbxStatus_OnDataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(RepositoryBase<StatusAccountingObject>.GetAll().OrderBy(x => x.Id))
                .SetValueComboBox(CurrentAccountingObject.With(x => x.Status).Return(x => x.Id, EntityBase.Default));
        }

        protected void txtNumber_OnDataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(CurrentAccountingObject.With(x => x.Number));
        }

        protected void cbxType_OnDataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(IKTComponentRepository.GetAllParents())
                .SetValueComboBox(CurrentAccountingObject.With(x => x.Type).Return(x => x.Id, EntityBase.Default));
        }

        protected void cbxKind_OnDataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(IKTComponentRepository
                .GetAllChildren(CurrentAccountingObject.With(x => x.Type)
                .Return(x => x.Id, EntityBase.Default)))
                .SetValueComboBox(CurrentAccountingObject.With(x => x.Kind).Return(x => x.Id, EntityBase.Default));
        }

        protected void txtFullName_OnDataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(CurrentAccountingObject.With(x => x.Name));
        }

        #region CallBack

        protected void callBack_OnCallback(object source, CallbackEventArgs e)
        {
            AccountingObjectService.Update(new AccountingObjectCommonDataArgs()
            {
                Id = CurrentAccountingObject.Id,
                StatusId = (int)cbxStatus.Value,
                TypeId = (int)cbxType.Value,
                KindId = (int)cbxKind.Value,
                Number = txtNumber.Value.ToStringOrDefault(),
                FullName = txtFullName.Value.ToStringOrDefault(),
            });
        }

        protected void cbxKind_OnCallback(object sender, CallbackEventArgsBase e)
        {
            int typeId;

            if (int.TryParse(e.Parameter, out typeId) == false)
            {
                return;
            }

            var kinds = IKTComponentRepository.GetAllChildren(typeId);

            cbxKind.DataSource = kinds;
            cbxKind.DataBind();

            cbxKind.Value = EntityBase.Default;
        }

        #endregion CallBack

        #endregion Forms


        #endregion Control Handlers

        #endregion Handlers


        #region Helpers

        public static string GetUrl(object accountingObjectId)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/AccountingObjects/CommonData.aspx"), EditCard.AccountingObjectIdKey, accountingObjectId);
        }

        private DocumentAccountingObjectArgs GetDocumentAccountingObjectArgs(int id = 0)
        {
            var result = new DocumentAccountingObjectArgs()
            {
                Id = id,
                AccountingObject = CurrentAccountingObject,
                Name = hf["Name"].ToStringOrDefault(),
                Number = hf["Number"].ToStringOrDefault(),
                FioSigner = hf["FioSigner"].ToStringOrDefault(),
                PostSigner = hf["PostSigner"].ToStringOrDefault(),
                Url = hf["Url"].ToStringOrDefault(),
                Files = hf["Files"].ToStringOrDefault()
            };

            return result;
        }

        private void FileUploaderDataBinding(FileUploadControl2 uploader, IEnumerable<Document> list)
        {
            uploader.FileItemTemplate = "<a href=\"" + FileHandler.GetUrl(string.Empty) + "{id}\">{name}</a>";

            foreach (Document doc in list)
            {
                uploader.SetValue(doc.FileName, doc.Id.ToString());
            }
        }

        protected string GetDownloadUrl(string fileInfo)
        {
            if (string.IsNullOrEmpty(fileInfo))
                return null;

            List<string> links = new List<string>();

            string[] files = fileInfo.Split(';');
            foreach (string f in files)
            {
                string[] pair = f.Split(':');
                links.Add(string.Format("<a href='{0}'>{1}</a>", FileHandler.GetUrl(pair[0]),
                                                                 pair.Length > 1 ? pair[1] : "Скачать"));
            }
            return string.Join("<br />", links);
        }

        #endregion Helpers

    }
}