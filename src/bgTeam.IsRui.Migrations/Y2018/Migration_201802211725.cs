﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201802211725)]
    public class Migration_201802211725 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("OU_Software").AddColumn("Manufacturer_Castom_Name").AsString().Nullable();

            Alter.Column("Producer_ID").OnTable("OU_Software").AsInt32().Nullable();
        }
    }
}
