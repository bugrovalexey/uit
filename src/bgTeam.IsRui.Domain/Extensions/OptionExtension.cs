﻿using bgTeam.IsRui.Domain.Entities.Common;
using System;

namespace bgTeam.IsRui.Domain.Extensions
{
    public static class OptionExtension
    {
        /// <summary>
        /// Сериализация значения
        /// </summary>
        /// <param name="option"></param>
        /// <param name="value"></param>
        public static void Serialize(this Option option, Object value)
        {
            switch (option.Type)
            {
                case OptionsTypeEnum.Bool:
                    option.Value = (bool)value ? "true" : "false";
                    break;

                case OptionsTypeEnum.Date:
                    option.Value = ((DateTime)value).ToString();
                    break;

                case OptionsTypeEnum.Int:
                    option.Value = ((int)value).ToString();
                    break;

                case OptionsTypeEnum.String:
                    option.Value = (string)value;
                    break;

                default:
                    throw new ArgumentException(option.Type.ToString());
            }
        }

        /// <summary>
        /// Десериализация значения
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public static object Deserialize(this Option option)
        {
            if (option.Value == null)
                return null;

            object val = null;

            switch (option.Type)
            {
                case OptionsTypeEnum.Bool:
                    val = (string.Compare(option.Value, "true", true) == 0);
                    break;

                case OptionsTypeEnum.Date:
                    DateTime date;
                    if (DateTime.TryParse(option.Value, out date))
                        val = date;
                    break;

                case OptionsTypeEnum.Int:
                    int num = 0;
                    if (int.TryParse(option.Value, out num))
                        val = num;
                    break;

                case OptionsTypeEnum.String:
                    val = option.Value;
                    break;

                default:
                    throw new ArgumentException(option.Type.ToString());
            }

            return val;
        }
    }
}