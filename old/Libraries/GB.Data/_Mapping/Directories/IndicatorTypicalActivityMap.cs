﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class IndicatorTypicalActivityMap : ClassMapBase<IndicatorTypicalActivity>
    {
        public IndicatorTypicalActivityMap() :
            base("Indicator_Typical_Activity")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Indicator_Typical_Activity_Name");

            References<TypicalActivities>(x => x.TypicalActivities, "Typical_Activities_ID");
            References<MeasureUnit>(x => x.Measure, "Measure_Unit_ID");
            HasManyToMany(x => x.ActivityTypes)
                .Table("Indicator_Typical_Type")
                .ParentKeyColumn("Indicator_Typical_Activity_ID")
                .ChildKeyColumn("Plans_Activity_Type2_ID")
                .ReadOnly();

            HasManyToMany(x => x.IKTComponents)
                .Table("Indicator_Typical_IKT_Component")
                .ParentKeyColumn("Indicator_Typical_Activity_ID")
                .ChildKeyColumn("IKT_Component_ID")
                .ReadOnly();
        }
    }
}