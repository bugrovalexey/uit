﻿namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    /// <summary>
    /// Внутренние ИС и компоненты ИТКИ
    /// </summary>
    public class InternalIsAndComponentsItki : EntityBase
    {
        /// <summary>
        /// Собственник
        /// </summary>
        public virtual AccountingObject Owner { get; set; }

        /// <summary>
        /// Внутренний ИС
        /// </summary>
        public virtual AccountingObject Object { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        public virtual string Purpose { get; set; }

    }
}
