﻿namespace GB.DataAccess.Command.Workflow
{
    public class StatusChangePostCommand : ICommand<StatusChangePostContext>
    {
        SessionFactory factory;

        public StatusChangePostCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(StatusChangePostContext context)
        {
            var session = factory.CreateInternal();

            session.Update(context.Obj);
        }
    }
}
