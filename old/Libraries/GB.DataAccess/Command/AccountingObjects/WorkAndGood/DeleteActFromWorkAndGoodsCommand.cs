﻿using GB.Data.AccountingObjects.WorksAndGoods;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.WorkAndGood
{
    public class DeleteActFromWorkAndGoodsCommand : ICommand<DeleteActFromWorkAndGoodsContext>
    {
        ISessionFactoryGb factory;

        public DeleteActFromWorkAndGoodsCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(DeleteActFromWorkAndGoodsContext context)
        {            
            if (context.xId.HasValue && context.xId != 0)
            {
                var entity = new Repository<WorkAndGoods>().Get(context.xId.Value);

                if (entity != null)
                {
                    entity.ActOfAcceptance = null;

                    using (var session = factory.Create())
                    {                        
                        session.SaveOrUpdate(entity);
                        session.Commit();
                    }
                }
            }
        }
    }
}
