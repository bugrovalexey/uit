﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="ExpenseItems.aspx.cs" Inherits="Uviri.MKRF.Web.Selectors.ExpenseItems" %>
<asp:Content ID="Head" ContentPlaceHolderID="Head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
     <script type="text/javascript">
         function rowClick(s, e) {
             
             var id = s.GetRowKey(e.visibleIndex);
             s.GetRowValues(e.visibleIndex, 'Id;Code;Name', function (values) {
                 var result = id + ';' + values[1] + ';' + values[2];
                 
                 window.parent.EIPopupClose(result);
             });
         }
    </script>
    <dxwgv:ASPxGridView ID="SelectorsExpenseItemsGrid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" KeyFieldName="Id"
        OnBeforePerformDataSelect="SelectorsExpenseItemsGrid_BeforePerformDataSelect" Width="100%" OnHtmlRowCreated="SelectorsExpenseItemsGrid_HtmlRowCreated">
        <Columns>
            <dxwgv:GridViewDataTextColumn Caption="Код" FieldName="Code" Width="100px">
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>            
        </Columns>
        
        <Styles>
            <AlternatingRow Enabled="true" />
            <Header Wrap="True" />
        </Styles> 
        <ClientSideEvents RowClick="rowClick" />
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="15" />
    </dxwgv:ASPxGridView>
</asp:Content>
