﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.Controls
{
    public class YandexMetrika
    {
        public static string GetScript(string MetrikaId)
        {
            if (string.IsNullOrEmpty(MetrikaId))
                return null;
#if DEBUG
            return null;
#else
            string script = string.Format(@"
<script type='text/javascript'>
(function (d, w, c) {{
    (w[c] = w[c] || []).push(function() {{
        try {{
            w.yaCounter{0} = new Ya.Metrika({{id:{0},
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true}});
        }} catch(e) {{ }}
    }});
 
    var n = d.getElementsByTagName('script')[0],
        s = d.createElement('script'),
        f = function () {{ n.parentNode.insertBefore(s, n); }};
    s.type = 'text/javascript';
    s.async = true;
    s.src = (d.location.protocol == 'https:' ? 'https:' : 'http:') + '//mc.yandex.ru/metrika/watch.js';
 
    if (w.opera == '[object Opera]') {{
        d.addEventListener('DOMContentLoaded', f, false);
    }} else {{ f(); }}
}})(document, window, 'yandex_metrika_callbacks');
</script>
<noscript><div><img src='//mc.yandex.ru/watch/{0}' style='position:absolute; left:-9999px;' alt='' /></div></noscript>", MetrikaId);
            return script;
#endif
        }
    }
}
