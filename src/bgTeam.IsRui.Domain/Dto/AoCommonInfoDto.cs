﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoCommonInfoDto
    {
        public int Id { get; set; }

        public int? KindId { get; set; }

        public int? ArticleId { get; set; }

        public int? KindOfActivityId { get; set; }

        public string Name { get; set; }
    }
}
