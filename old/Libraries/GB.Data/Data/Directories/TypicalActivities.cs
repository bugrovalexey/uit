﻿namespace GB.MKRF.Entities.Directories
{
    /// <summary>
    /// Типовая деятельность
    /// </summary>
    public class TypicalActivities : EntityBase
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }
    }
}