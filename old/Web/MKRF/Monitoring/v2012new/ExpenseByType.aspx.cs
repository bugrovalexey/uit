﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GB.Entity.Directories;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;

namespace GB.MKRF.Web.Monitoring.v2012new
{
    public partial class ExpenseByType : System.Web.UI.Page
    {
        private int currentYear = 0;
        protected int CurrentYear
        {
            get
            {
                if (currentYear == 0)
                {
                 
                    var ys = Request.QueryString["Year"];
                    int yi = 0;
                    if (!string.IsNullOrWhiteSpace(ys) && int.TryParse(ys, out yi))
                    {
                        currentYear = yi;
                    }
                    else
                    {
                        currentYear = DateTime.Now.Year;
                    }
                }
                return currentYear;
            }
        }

        private int expType = 0;
        protected int ExpType
        {
            get
            {
                if (expType == 0)
                {

                    var ds = Request.QueryString["type"];
                    int di = 0;
                    if (!string.IsNullOrWhiteSpace(ds) && int.TryParse(ds, out di))
                    {
                        expType = di;
                    }
                    else
                    {
                        expType = 0;
                    }
                }
                return expType;
            }
        }

        private DataTable _data = null;
        private DataTable Data
        {
            get
            {
                if (_data == null)
                {
                    _data = MonitoringRepository.ExpenseByTypes(ExpType, CurrentYear);
                }

                return _data;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            YearsComboBox.DataBind();
            ExpenseTypeComboBox.DataBind();
            ChartDataBinding();
            grid.DataBind();
        }   

        protected void ChartCallbackPanel_OnCallback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            ChartDataBinding();
        }

        private void ChartDataBinding()
        {
            Chart.Series.Clear();
            Dictionary<string, double> values = new Dictionary<string, double>();
            foreach (DataRow row in Data.Rows)
            {
                if (string.IsNullOrWhiteSpace(row["PlanId"].ToString()))
                {
                    continue;
                }
                var sum = Convert.ToDouble(row["ExpenseVolumeY0"]);
                var name = Convert.ToString(row["Name"]);
                if (!values.ContainsKey(name))
                {
                    values.Add(name, 0d);
                }

                values[name] += sum;
            }

            foreach (string key in values.Keys)
            {
                AddSeries(key, values[key]);
            }
            //AddSeries("Подведомственные департаменты", values["Подведомственные департаменты"]);
            //AddSeries("Территориальные органы", values["Территориальные органы"]);
            //AddSeries("Департаменты", values["Департаменты"]);
        }
        private void AddSeries(string name, double value)
        {
            int i = Chart.Series.Add(name, ViewType.Bar);
            Chart.Series[i].LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            Chart.Series[i].Label.ResolveOverlappingMode = ResolveOverlappingMode.HideOverlapped;
            Chart.Series[i].Points.Clear();
            Chart.Series[i].Points.Add(new SeriesPoint(name, value));
        }

        protected void ExpenseTypeComboBox_DataBinding(object sender, System.EventArgs e)
        {
            IList<ExpenseType> data = RepositoryBase<ExpenseType>.GetAll();
            ASPxComboBox box = (ASPxComboBox)sender;
            box.DataSource = data;
            box.Value = ExpType;
        }
     

        protected void YearsComboBox_DataBinding(object sender, System.EventArgs e)
        {
            IList<Years> data = RepositoryBase<Years>.GetAll(x => x.Value >= 2010).Take(10).ToList();
			ASPxComboBox box = (ASPxComboBox)sender;
			box.DataSource = data;
			box.Value = CurrentYear;
        }

        protected void grid_DataBinding(object sender, EventArgs e)
        {
            var result = Data.Select("ExpenseVolumeY0 > 0 AND PlanId > 0 AND (" + ExpType + " = 0 OR Expense_Type_ID = " + ExpType + " )").AsEnumerable()
                .Select(x =>
                new
                {
                    Name = Convert.ToString(x["Name"]),
                    OGV = Convert.ToString(x["OGV"]),
                    PlanId = Convert.ToInt32(x["PlanId"]),
                    ExpenseVolumeY0 = Convert.ToDouble(x["ExpenseVolumeY0"]),
                })
                .GroupBy(x => x.OGV)
                .Select(x =>
                    new
                    {
                        OGV = Convert.ToString(x.Key),
                        Name = x.Select( n => n.Name).First(), // ОГВ может иметь только один тип организации
                        PlanId = x.Select(n => n.PlanId).First(), // за данный год у одного ОГВ может быть только один план
                        ExpenseVolumeY0 = x.Sum(exp => exp.ExpenseVolumeY0)
                    })
                .ToList();

            grid.DataSource = result;
        }

        protected void Chart_CustomDrawSeriesPoint(object sender, CustomDrawSeriesPointEventArgs e)
        {
            e.LabelText = Convert.ToDouble(e.LabelText).ToString("#,##0.0000");
        }
    }
}