﻿using System;
using System.Data;
using System.Linq;
using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.Common;
using GB.Data.Directories;
using GB.Data.Documents;
using GB.Data.Extensions;
using GB.Data.Plans;
using GB.DataAccess.Repository;

namespace GB.DataAccess.Services
{
    #region args

    public interface IPlansActivityArgs
    {
        int Id { get; set; }

        string Name { get; set; }

        string Description { get; set; }

        string ResponsibleDepartment { get; set; }

        int? ResponsibleInformation { get; set; }

        int? ResponsibleTechnical { get; set; }

        int? ResponsibleFinancial { get; set; }

        int? AccountingObject { get; set; }

        int? Signing { get; set; }

        string Logo { get; set; }

        string Files { get; set; }

        int? Order { get; set; }

        DateTime? OrderDate { get; set; }

        string Prerequisites { get; set; }

        string Goal { get; set; }

        string Task { get; set; }

        string Conception { get; set; }
    }

    public interface IPlansActivityExpensesArgs
    {
        int Id { get; set; }

        int? GRBS { get; set; }

        int? SectionKBK { get; set; }

        int? WorkForm { get; set; }

        int? CSR { get; set; }

        int? KOSGU { get; set; }

        object VolY0 { get; set; }

        object VolY1 { get; set; }

        object VolY2 { get; set; }
    }

    public interface IPlansActivityReasonsArgs
    {
        int Id { get; set; }

        int StateProgram { get; set; }

        int StateSubProgram { get; set; }

        int StateMainEvent { get; set; }
    }

    public interface IPlansActivityCreateArgs
    {
        int Id { get; set; }

        string NameSmall { get; set; }

        int Type { get; set; }
    }

    //public class PlansActivityExpertiseArgs : IPlansActivityExpensesArgs
    //{
    //    public Plan
    //    public object ActivityExpertiseConclusion { get; set; }
    //}

    public class PlansActivityArgs : IPlansActivityExpensesArgs
    {
        public int Id { get; set; }

        public object СurrentPlan { get; set; }

        public object Name { get; set; }

        public object IsPriority { get; set; }

        //common data
        public object AnnotationData { get; set; }

        public object ActivityType2 { get; set; }

        public object IsMoveUp { get; set; }

        public object IKTSelectionId { get; set; }

        public object ResponsibleId { get; set; }

        public object FundingId { get; set; }

        public object IKTObjectId { get; set; }

        // expenses
        public int? GRBS { get; set; }

        public int? SectionKBK { get; set; }

        public int? WorkForm { get; set; }

        public int? CSR { get; set; }

        public int? KOSGU { get; set; }

        public object Subentry { get; set; }

        public object VolY0 { get; set; }

        public object VolY1 { get; set; }

        public object VolY2 { get; set; }

        public object PlansActivityAddVolY0 { get; set; }

        public object PlansActivityAddVolY1 { get; set; }

        public object PlansActivityAddVolY2 { get; set; }

        public object PlansActivityFactVolY0 { get; set; }

        public object PlansActivityFactVolY1 { get; set; }

        public object PlansActivityFactVolY2 { get; set; }

        public object PlansActivityFactDate { get; set; }

        public object NewDepartamentId { get; set; }

        public object DatePlacement { get; set; }

        public object ResponsibleInformation { get; set; }

        public object ResponsibleTechnical { get; set; }

        public object ResponsibleFinancial { get; set; }

        public object Files { get; set; }

        public object AccountingObject { get; set; }
    }

    public class UnionArgs
    {
        public int source { get; set; }

        public int target { get; set; }

        public string name { get; set; }

        public bool priority { get; set; }
    }

    #endregion args

    public class ActivityService : ServiceBase<PlansActivity>
    {
               public PlansActivity Create(IPlansActivityCreateArgs args)
        {
            var activity = new PlansActivity();

            activity.NameSmall = args.NameSmall;
            activity.ActivityType2 = GetEntity<ActivityType>(args.Type);
            activity.Department = UserRepository.GetCurrent().Department;
            activity.Status = new StatusRepository().GetDefault(activity);

            activity.PlansActivityVolY0 = 0;
            activity.PlansActivityVolY1 = 0;
            activity.PlansActivityVolY2 = 0;

            activity.PlansActivityAddVolY0 = 0;
            activity.PlansActivityAddVolY1 = 0;
            activity.PlansActivityAddVolY2 = 0;

            activity.ExpertiseConclusion = new ActivityExpertiseConclusionService().Create(null);

            base.SaveOrUpdate(activity);

            return activity;
        }

        public PlansActivity Update2(PlansActivityArgs args)
        {
            var activity = Repository.Get(args.Id);

            activity.Name = Convert.ToString(args.Name);

            base.SaveOrUpdate(activity);

            return activity;
        }

        public PlansActivity Update4(IPlansActivityExpensesArgs args)
        {
            var activity = Repository.GetExisting(args.Id);
            //if (!activity.CanEdit())
            //    throw new InvalidOperationException("Нет прав на изменение объекта.");

            activity.GRBS = GetEntity<GRBS>(args.GRBS);
            activity.SectionKBK = GetEntity<SectionKBK>(args.SectionKBK);
            activity.WorkForm = GetEntity<WorkForm>(args.WorkForm);
            activity.ExpenseItem = GetEntity<ExpenseItem>(args.CSR);
            activity.ExpenditureItem = GetEntity<ExpenditureItem>(args.KOSGU);

            activity.PlansActivityVolY0 = args.VolY0.ToDoubleNull();
            activity.PlansActivityVolY1 = args.VolY1.ToDoubleNull();
            activity.PlansActivityVolY2 = args.VolY2.ToDoubleNull();

            base.SaveOrUpdate(activity);

            return activity;
        }

        public PlansActivity Update5(IPlansActivityReasonsArgs args)
        {
            var activity = Repository.GetExisting(args.Id);
            //if (!activity.CanEdit())
            //    throw new InvalidOperationException("Нет прав на изменение объекта.");

            activity.StateProgram = GetEntity<StateProgram>(args.StateProgram);
            activity.StateSubProgram = GetEntity<StateProgram>(args.StateSubProgram);
            activity.StateMainEvent = GetEntity<StateProgram>(args.StateMainEvent);

            base.SaveOrUpdate(activity);
            return activity;
        }

        public void Delete(PlansActivityArgs args)
        {
            //activityListCache = null;
            Logger.Debug("Удаление мероприятия " + args.Id);
            //SetCurrentEdit(args.Id);
            var CurrentEdit = Repository.GetExisting(args.Id);
            //if (!CurrentEdit.CanDelete())
            //    throw new InvalidOperationException("Нет прав на удаление объекта.");
            //var plan = CurrentEdit.Plan;
            base.Delete(args.Id);
            //NumHelper<PlansActivity>.OrderNums(PlanRepository.GetActivityListWithoutSecurity(plan));
        }

        private DataTable DataTableCustomFilter(DataTable dt, string filterExpression)
        {
            var rows = dt.Select(filterExpression);
            if (rows.Any())
            {
                return rows.CopyToDataTable();
            }
            else
            {
                dt.Rows.Clear();
                return dt;
            }
        }

        public void SaveExpenses(PlansActivityArgs args)
        {
            //SetCurrentEdit(args.Id);
            var CurrentEdit = Repository.GetExisting(args.Id);

            Logger.Debug("Обновление мероприятия " + args.Id);

            CurrentEdit.GRBS = GetEntity<GRBS>(args.GRBS);
            CurrentEdit.SectionKBK = GetEntity<SectionKBK>(args.SectionKBK);
            CurrentEdit.WorkForm = GetEntity<WorkForm>(args.WorkForm);
            CurrentEdit.ExpenseItem = GetEntity<ExpenseItem>(args.CSR);
            //            CurrentEdit.ExpenditureItem = GetEntity<ExpenditureItem>(args.ExpenditureItemsId);

            if (args.Subentry != null && !string.IsNullOrEmpty(args.Subentry.ToString()))
                CurrentEdit.Subentry = args.Subentry.ToString();

            double parseValue = 0;
            if (args.VolY0 != null && double.TryParse(args.VolY0.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityVolY0 = parseValue;
            }

            if (args.VolY1 != null && double.TryParse(args.VolY1.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityVolY1 = parseValue;
            }

            if (args.VolY2 != null && double.TryParse(args.VolY2.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityVolY2 = parseValue;
            }

            if (args.PlansActivityAddVolY0 != null && double.TryParse(args.PlansActivityAddVolY0.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityAddVolY0 = parseValue;
            }

            if (args.PlansActivityAddVolY1 != null && double.TryParse(args.PlansActivityAddVolY1.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityAddVolY1 = parseValue;
            }

            if (args.PlansActivityAddVolY2 != null && double.TryParse(args.PlansActivityAddVolY2.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityAddVolY2 = parseValue;
            }

            if (args.PlansActivityFactVolY0 != null && double.TryParse(args.PlansActivityFactVolY0.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityFactVolY0 = parseValue;
            }

            if (args.PlansActivityFactVolY1 != null && double.TryParse(args.PlansActivityFactVolY1.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityFactVolY1 = parseValue;
            }

            if (args.PlansActivityFactVolY2 != null && double.TryParse(args.PlansActivityFactVolY2.ToString(), out parseValue))
            {
                CurrentEdit.PlansActivityFactVolY2 = parseValue;
            }

            DateTime fact;
            if (args.PlansActivityFactDate != null
                && DateTime.TryParse(args.PlansActivityFactDate.ToString(), out fact)
                && fact > new DateTime(1, 1, 1))
                CurrentEdit.PlansActivityFactDate = fact;
            else
                CurrentEdit.PlansActivityFactDate = null;

            base.SaveOrUpdate(CurrentEdit);
        }

        private object FindValue(DataTable table, object key, string column = null)
        {
            if (table == null || key == null)
                return null;

            string keyColumnName = table.TableName + "_ID";

            if (string.IsNullOrEmpty(column))
            {
                return table.Rows.Cast<DataRow>().Where(row => row[keyColumnName].Equals(key))
                                                 .FirstOrDefault();
            }
            else
            {
                return table.Rows.Cast<DataRow>().Where(row => row[keyColumnName].Equals(key))
                                                 .Select(row => row[column])
                                                 .FirstOrDefault();
            }
        }

        private T getValue<T>(DataRow row, string column)
        {
            if (row[column] == DBNull.Value)
            {
                return default(T);
            }
            else
            {
                return (T)row[column];
            }
        }

        private T getValue<T>(DataTable table, int rowIndex, string column)
        {
            if (table == null || table.Rows.Count <= rowIndex)
            {
                return default(T);
            }
            else
            {
                return getValue<T>(table.Rows[rowIndex], column);
            }
        }
    }
}