﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace bgTeam.IsRui.WebApp.SignalRAccount
{
    [HubName("removeAccountObjectHub")]
    public class RemoveAccountObjectHub : Hub
    {
        public void RemoveAccountingObject(int id)
        {
            Clients.Others.cardWasRemoved(id);
        }
    }

    [HubName("userConnectHub")]
    public class UserAccountHub : Hub
    {
        private readonly static ConnectionMapping<string> _sessions = new ConnectionMapping<string>();        

        public void SetSessionParams(string sessionId, string dateStamp)
        {
            var date = DateTime.Parse(dateStamp);
            string user = Context.User.Identity.Name;
            var sessIds = _sessions.GetConnections(user);

            if (!sessIds.Any())
            {
                //сессий для данного логина нет, ты - первый, заходи!!!
                _sessions.Add(user, sessionId, date);
            }
            else //сессии есть - разбираемся дальше...
            {
                if (!sessIds.Keys.Contains(sessionId))
                {
                    //входящей сессии нет в справочнике - либо кто-то уже работает под этим логином и мы отказываем пытающемуся войти сейчас(если будет ответ от работающего клиента DenyAccess),
                    //либо кто-то закрыл браузер и уже не работает(от него не будет ответа на отказ) - мы пускаем пытающегося войти
                    Clients.OthersInGroup(user).askAlive(Context.ConnectionId);

                    _sessions.Add(user, sessionId, date);
                }
                else if (sessIds.Count > 1)
                {
                    //Удаляем все кроме самой новой сессии по времени, раньше удаляли все кроме текущей
                    var aliveSession = sessIds.OrderBy(x => x.Value).LastOrDefault();
                    _sessions.RemoveAllExcept(user, aliveSession.Key);

                    if (sessionId != aliveSession.Key)
                    {
                        //если входящая сессия не равна оставшейся в справочнике, то отказываем входящему
                        Clients.Client(Context.ConnectionId).sendDeny("Уже кто-то сидит под логином " + "'" + user + "'.");                                        
                    }
                }
            }            
        }

        public void DenyAccess(string connId, string sessionId)
        {
            //Опевещение от работающего клиента под логином(ответ на запрос askAlive), что он еще работает и нужно отказать вновь входящему
            _sessions.RemoveAllExcept(Context.User.Identity.Name, sessionId);
            Clients.Client(connId).sendDeny("Уже кто-то сидит под логином " + "'" + Context.User.Identity.Name + "'.");
        }

        public void DeleteUserSession(string sessionId)
        {
            //запрос на удаление сессии(при корректном выходе через LOGUOT)
            string user = Context.User.Identity.Name;

            _sessions.Remove(user, sessionId);
            Groups.Remove(Context.ConnectionId, user);
        }

        public override Task OnConnected()
        {            
            Groups.Add(Context.ConnectionId, Context.User.Identity.Name);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Groups.Remove(Context.ConnectionId, Context.User.Identity.Name);
            return base.OnDisconnected(stopCalled);
        }             
    }
}