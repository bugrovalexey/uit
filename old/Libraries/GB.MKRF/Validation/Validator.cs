﻿using System;
using System.Collections.Generic;
using GB;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Statuses;
using GB.MKRF.Repository;
using GB.Repository;

namespace GB.MKRF.Validation
{
    public enum CompositeEnum
    {
        Edit = 1,
        Delete = 2,
    }

    public class Validator : ValidatorBase
    {
        //internal static bool CheckStatus(Status status, params StatusEnum[] statuses)
        //{
        //    if (statuses == null)
        //        return true;

        //    foreach (StatusEnum se in statuses)
        //    {
        //        if (se.IsEq(status))
        //            return true;
        //    }

        //    return false;
        //}

        /// <summary>
        /// Валидация
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="action">Действие</param>
        /// <returns>Доступность</returns>
        public static bool Validate(EntityBase Entity, ObjActionEnum Action)
        {
            return (bool)(ValidateEx(Entity, Action) ?? false);           
        }


        /// <summary>
        /// Валидация
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="action">Действие</param>
        /// <returns>true - можно / false - нельзя / null - временно нельзя</returns>
        private static bool? ValidateEx(EntityBase Entity, ObjActionEnum Action)
        {
            // TODO: внедрить механизм кэширования повторяющихся запросов

            User currentUser = UserRepository.GetCurrent();
            if (currentUser == null || Entity == null)
                return false;

            // подготовка параметров для выполнения валидации:
            // Entity и parameter не должны быть null

            string parameter = null;

            switch (Entity.GetTableTypeOld())
            {
                case TableTypeEnum.Activity:
                    parameter = Activity.PrepareToValidate(Entity, Action);
                    break;
                
                case TableTypeEnum.PlansWork:
                    parameter = Works.PrepareToValidate(Entity, Action);
                    break;

                case TableTypeEnum.PlansSpec:
                    parameter = Specs.PrepareToValidate(Entity, Action);
                    break;

                case TableTypeEnum.PlansActivityFunc:
                    parameter = PlansActivityFuncs.PrepareToValidate(Entity, Action);
                    break;

                //case TableTypeEnum.Exp_Conclusion:
                //    parameter = Conclusion.PrepareToValidate(Entity, Action);
                //    break;

                //case TableTypeEnum.ExpObjectConclusion:
                //    parameter = ObjectConclusion.PrepareToValidate(Entity, Action);
                //    break;


                case TableTypeEnum.Document:
                    parameter = Document.PrepareToValidate(Entity, Action);
                    break;

                //case TableTypeEnum.Executors:
                //    parameter = Executor.PrepareToValidate(Entity, Action);
                //    break;

                case TableTypeEnum.UserRequest:
                    parameter = UserRequest.PrepareToValidate(Entity, Action);
                    break;

                case TableTypeEnum.PaperObject:
                    parameter = PaperObject.PrepareToValidate(Entity, Action);
                    break;

                //case TableTypeEnum.PurchasePlan:
                //    parameter = PurchasePlan.PrepareToValidate(Entity, Action);
                //    break;

                //case PortalTableEnum.Department:
                //case PortalTableEnum.Exp_Demand:
                //case PortalTableEnum.ExpObjectDemand:
                //case PortalTableEnum.FaqDoc:
                //case PortalTableEnum.MksResponsible:
                case TableTypeEnum.Plan:
                    parameter = Plan.PrepareToValidate(Entity, Action);
                    break;

                case TableTypeEnum.Exp_Demand:
                    parameter = Demand.PrepareToValidate(Entity, Action);
                    break;

                //case TableTypeEnum.PlansSchedule:
                //    parameter = PlansSchedule.PrepareToValidate(Entity, Action);
                //    break;

                //case TableTypeEnum.ProjectManagement:
                //    parameter = ProjectManagement.PrepareToValidate(Entity, Action);
                //    break;
                    
                //case TableTypeEnum.Reference:
                //    parameter = Reference.PrepareToValidate(Entity, Action);
                //    break;

                case TableTypeEnum.ActivityGoal:
                    parameter = ActivityGoal.PrepareToValidate(Entity, Action);
                    break;

                case TableTypeEnum.ActivityReason:
                    parameter = ActivityReason.PrepareToValidate(Entity, Action);
                    break;

                case TableTypeEnum.PlansGoalIndicator:
                    parameter = GoalIndicator.PrepareToValidate(Entity, Action);
                    break;

                default:
                    parameter = Entity.Id.ToString();
                    break;

            }

            if (parameter == null)
                return false;


            object result = RepositoryBase<EntityBase>.GetValueViaSQL("select dbo.Get_User_Access (@Users_ID, @Portal_Table_ID, @Action, @Parameter)",
                                                          new Dictionary<string, object>()
                                                                {
                                                                    {"Users_ID", currentUser.Id },
                                                                    {"Portal_Table_ID", (int)Entity.GetTableTypeOld() },
                                                                    {"Action", Action.GetCode() },
                                                                    {"Parameter", parameter },
                                                                });

            return (result == DBNull.Value) ? null : (bool?)result;
        }
        /*
        public static bool CanView(int access)
        {
            if (CanDelete(access))
            {
                return true;
            }

            if (CanEdit(access))
            {
                return true;
            }

            return (access & (int)AccessEnum.Read) == (int)AccessEnum.Read;
        }

        public static bool CanEdit(int access)
        {
            if (CanDelete(access))
            {
                return true;
            }

            return (access & (int)AccessEnum.Edit) == (int)AccessEnum.Edit;

        }

        public static bool CanDelete(int access)
        {
            return (access & (int)AccessEnum.Delete) == (int)AccessEnum.Delete;
        }
        */
    }
}