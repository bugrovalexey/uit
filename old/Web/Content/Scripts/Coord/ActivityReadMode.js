﻿$(function () {
    $('.item_title').live('click', function (e) {
        if (e.target.tagName == "IMG" || e.target.tagName == "A") {
            e.stopPropagation();
        }
        var self = $(this);
        if (self.hasClass('expanded')) {
            self.removeClass('expanded');
            self.addClass('collapsed');
            self.next('.item').slideUp();
        } else if (self.hasClass('collapsed')) {
            self.removeClass('collapsed');
            self.addClass('expanded');
            self.next('.item').slideDown();
        }
    });
    
    $('.showAll').live('click', function (e) {
        $('.item_title').removeClass('collapsed');
        $('.item_title').addClass('expanded');
        $('.item').slideDown();
    });
    
    $('.collapseAll').live('click', function (e) {
        $('.item_title').removeClass('expanded');
        $('.item_title').addClass('collapsed');
        $('.item').slideUp();
    });
});