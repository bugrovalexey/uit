﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.Directories;

    public class CategoryKindOfSupportDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Вид обеспечения
        /// </summary>
        public virtual CategoryTypeOfSupportEnum Type { get; set; }
    }
}
