﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201806042306)]
    public class Migration_201806042306 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Claim")
                .AddColumn("Claim_InitiatorUser_ID").AsInt32().Nullable()
                .AddColumn("Claim_Conclusion_ID").AsInt32().Nullable()
                .AddColumn("Claim_Justification").AsString(2047).Nullable()
                .AddColumn("Claim_Name").AsString(255).Nullable()
                .AddColumn("Claim_Content").AsString(1048576).Nullable();

            Create.Table("Claim_Indicators")
                .WithColumn("Claim_Indicators_ID").AsInt32().PrimaryKey()
                .WithColumn("Claim_ID").AsInt32().NotNullable()
                .WithColumn("MUnit_ID").AsInt32().Nullable()
                .WithColumn("Claim_Indicators_Name").AsString(255).Nullable()
                .WithColumn("Claim_Indicators_Explan").AsString(1023).Nullable()
                .WithColumn("Claim_Indicators_Basic_Value").AsInt32().Nullable()
                .WithColumn("Claim_Indicators_Goal_Value").AsInt32().Nullable()
                .WithColumn("Claim_Indicators_Date").AsDateTime().Nullable()
                .WithColumn("Claim_Indicators_Algorithm").AsString(2047).Nullable()
                .WithColumn("Claim_Indicators_Justification").AsString(2047).Nullable();
            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "Claim_Indicators" });

            Create.Table("Claim_Mark")
                .WithColumn("Claim_Mark_ID").AsInt32().PrimaryKey()
                .WithColumn("Claim_ID").AsInt32().NotNullable()
                .WithColumn("State_Programme_Mark_ID").AsInt32().NotNullable()
                .WithColumn("MUnit_ID").AsInt32().Nullable()
                .WithColumn("Claim_Mark_Name").AsString().Nullable()
                .WithColumn("Claim_Mark_Explan").AsString().Nullable()
                .WithColumn("Claim_Mark_Basic_Value").AsInt32().Nullable()
                .WithColumn("Claim_Mark_Goal_Value").AsInt32().Nullable()
                .WithColumn("Claim_Mark_Date").AsDateTime().Nullable()
                .WithColumn("Claim_Mark_Source_Is_State_Program").AsBoolean().NotNullable()
                .WithColumn("Claim_Mark_Source_Name").AsString(255).Nullable()
                .WithColumn("Claim_Mark_Justif").AsString(2047).Nullable()
                .WithColumn("Claim_Mark_Explanation").AsString(2047).Nullable()
                .WithColumn("Claim_Mark_AlgorithmOfCalculating").AsString(2047).Nullable();
            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "Claim_Mark" });

            Create.Table("ClaimAct")
                .WithColumn("ClaimAct_ID").AsInt32().PrimaryKey()
                .WithColumn("ClaimAct_File_ID").AsInt32().Nullable()
                .WithColumn("ClaimAct_Claim_ID").AsInt32().NotNullable()
                .WithColumn("ClaimAct_Name").AsString(255).Nullable()
                .WithColumn("ClaimAct_Number").AsString(64).Nullable();
            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "ClaimAct" });

            Create.Table("ClaimScopeOfWork")
                .WithColumn("ClaimScopeOfWork_ID").AsInt32().PrimaryKey()
                .WithColumn("ClaimScopeOfWork_Claim_ID").AsInt32().NotNullable()
                .WithColumn("ClaimScopeOfWork_WorkHours").AsInt32().Nullable()
                .WithColumn("ClaimScopeOfWork_WorkType").AsString(255).Nullable()
                .WithColumn("ClaimScopeOfWork_Description").AsString(4096).Nullable()
                .WithColumn("ClaimScopeOfWork_DateBegin").AsDateTime().Nullable()
                .WithColumn("ClaimScopeOfWork_DatePlanEnd").AsDateTime().Nullable();
            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "ClaimScopeOfWork" });

            Create.Table("ClaimConclusion")
                .WithColumn("ClaimConclusion_ID").AsInt32().PrimaryKey()
                .WithColumn("ClaimConclusion_Claim_ID").AsInt32().NotNullable()
                .WithColumn("ClaimConclusion_Comment").AsString(2048).Nullable();
            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "ClaimConclusion" });

            Create.Table("ClaimDocument")
                .WithColumn("ClaimDocument_ID").AsInt32().PrimaryKey()
                .WithColumn("ClaimDocument_Claim_ID").AsInt32().NotNullable()
                .WithColumn("ClaimDocument_Files_ID").AsInt32().Nullable()
                .WithColumn("ClaimDocument_CreateDate").AsDateTime().Nullable();
            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "ClaimDocument" });
        }
    }
}
