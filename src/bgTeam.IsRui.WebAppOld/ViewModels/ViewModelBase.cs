﻿namespace bgTeam.IsRui.WebApp.ViewModels
{
    public abstract class ViewModelBase
    {
        public bool ReadOnly { get; set; }
    }
}