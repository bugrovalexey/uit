﻿using GB.Data.Plans;
using System.Collections.Generic;
using System.Linq;

namespace GB.MKRF.Repository
{
    public class BaseIndicatorRepository<T> : Repository<T> where T : BaseIndicator
    {
        public List<T> GetAllByPlansActivity(int plansActivityId)
        {
            return GetAllEx(q => q
                    .Fetch(x => x.Unit).Eager
                    .Where(x => x.PlansActivity.Id == plansActivityId))
                .ToList();
        }
    }
}