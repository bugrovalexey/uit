﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.Pkcs;
using System.Text;
using GB;
using GB.Helpers;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Web.Services.DigitalSignature;
using GB.Repository;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Report;
using GB.Report;

namespace GB.MKRF.Web.Helpers
{
	/// <summary>Работа с пакетами и ЭЦП</summary>
	public static class DigitalSignatureHelper
	{
		/// <summary>
		/// Получить пакет данных, описывающий ОЭ (документы+опись+печатные формы) в zip-архиве (byte[])
		/// </summary>
		/// <param name="expObjId">Id ОЭ</param>
		/// <param name="expObjType">Тип ОЭ</param>
		/// <returns>zip в base64</returns>
        //public static byte[] GetByteArrayPackage(int expObjId, ExpObjectTypeEnum expObjType)
        //{
        //    if (expObjId < 0)
        //        return null;

        //    ZipFileCollection zip = new ZipFileCollection();
        //    //zip.AddFile(new MemoryStream(DocumentHelper.GetExpObjDocsZip(expObjId, expObjType, DocTypeEnum.demand_document)), "Документы.zip");
        //    zip.AddFile(new MemoryStream(DocumentHelper.GetExpObjDocsZip(expObjId, expObjType, DocTypeEnum.demand_inventory)), "Опись.zip");
        //    zip.AddFile(new MemoryStream(ReportCreator.Export(new ReportData(ReportTypeExport.All, ExportFormat.Pdf, new object[] { expObjType, expObjId }, zip)).CreateZip()), "Печатные формы_PDF.zip");
        //    zip.AddFile(new MemoryStream(ReportCreator.Export(new ReportData(ReportTypeExport.All, ExportFormat.Xls, new object[] { expObjType, expObjId }, zip)).CreateZip()), "Печатные формы_XLS.zip");

        //    return zip.CreateZip();
        //}

		/// <summary>
		/// Проверить подписанный пакет
		/// </summary>
		/// <param name="owner">Владелец документа</param>
		/// <returns>Ответ</returns>
		public static verifyCMSSignatureResponse VerifySignature(EntityBase owner)
		{
			//формируем список подписанных документов в описи (пакеты)
            IList<Document> packages = RepositoryBase<Document>.GetAll(x => x.Owner == owner && 
                                                                            x.DocType == DocTypeEnum.demand_inventory && 
                                                                            x.SignedPackage != null );

			//находим последний пакет
			Document doc = packages.Where(d => d.Id == packages.Max(p => p.Id)).FirstOrDefault();

			byte[] cms = doc.SignedPackage;

			//string msgBase64 = Convert.ToBase64String(DigitalSignatureHelper.ExtractEnvelopedData(doc.File, true));
			//byte[] message = Encoding.Default.GetBytes(msgBase64);
			byte[] message = ExtractEnvelopedData(doc);

			verifyCMSSignatureRequest request = new verifyCMSSignatureRequest();
			request.cms = cms;
			request.message = message;

            DigitalSignatureServiceClient service = new DigitalSignatureServiceClient();
			return service.verifyCMSSignature(request);
		}

		/// <summary>
		/// Проверить валидность подписи (true/false)
		/// </summary>
		/// <param name="owner">Владелец документа</param>
		/// <returns>Ответ</returns>
		public static bool Verify(Document document, StringBuilder Log)
		{

            if (!document.IsSigned)
            {
                Log.Append("Отсутствует ЭП документа");
                return false;
            }
            byte[] cms = document.SignedPackage;
            Log.Append("\nИзвлечение документа из БД.");
            Log.Append("\nНазвание файла: " + document.FileName);
            Log.Append("\nРазмер файла: " + (document.File != null ? document.File.Length.ToString() : null) + " байт");

            
            byte[] hash = Encoding.GetEncoding("windows-1251").GetBytes(CryptoHelper.ComputeHash(document.File));
            Log.Append("\nХэш документа: " + BitConverter.ToString(hash));
            
            DigitalSignatureServiceClient service = new DigitalSignatureServiceClient();

            Log.Append("\nОтправка запроса в удостоверяющий центр");

            verifyCMSSignatureRequest request = new verifyCMSSignatureRequest();
            request.cms = cms;
            request.message = hash;
            
            verifyCMSSignatureResponse resp = service.verifyCMSSignature(request);

            Log.Append("\nПолучен ответ: " + resp.result.ToString());

            return resp.result;
		}

        /// <summary>
        /// Проверить валидность подписи (true/false)
        /// </summary>
        /// <param name="owner">lJREVTYN</param>
        /// <returns>Ответ</returns>
        public static bool IsSignatureValid(EntityBase owner)
        {
            return VerifySignature(owner).result;
        }

		/// <summary>
		/// Получить содержимое подписанного пакета (файл)
		/// </summary>
		/// <param name="signedPackage">Подписанный пакет</param>
		/// <param name="isPlugin">Подписывалось через плагин?</param>
		/// <returns>Исходный файл</returns>
		public static byte[] ExtractEnvelopedData(byte[] signedPackage, bool isPlugin)
		{
			if (signedPackage == null)
				return null;

			ContentInfo content = new ContentInfo(signedPackage);
			SignedCms cms = new SignedCms(content, false);
			try
			{
				cms.Decode(signedPackage);
			}
			catch
			{
				throw new Exception("Возникла ошибка при декодировании пакета");
			}
			if (!isPlugin)
			{
				return cms.ContentInfo.Content;
			}
			else
			{
				//return Convert.FromBase64String(Encoding.GetEncoding("windows-1251").GetString(cms.ContentInfo.Content));
				//return Convert.FromBase64String(Encoding.GetEncoding("utf-8").GetString(cms.ContentInfo.Content));
				return Convert.FromBase64String(Encoding.GetEncoding("utf-16").GetString(cms.ContentInfo.Content));
			}
		}

		/// <summary>
		/// Получить содержимое подписанного пакета (файл)
		/// </summary>
		/// <param name="doc">Документ</param>
		/// <param name="isPlugin">Подписывалось через плагин?</param>
		/// <returns>Исходный файл</returns>
		public static byte[] ExtractEnvelopedData(Document doc)
		{
			if (doc == null)
				return null;

			return Encoding.GetEncoding("windows-1251").GetBytes(Convert.ToBase64String(doc.File));
		}

		///// <summary>
		///// Подписать пакет (данные)
		///// </summary>
		///// <param name="package">Пакет</param>
		///// <param name="certificate">Сертификат</param>
		///// <returns>Подписанный пакет</returns>
		//public static byte[] SignPackage(byte[] package, X509Certificate2 certificate)
		//{
		//    if (package == null)
		//        return null;

		//    if (certificate == null)
		//        certificate = GetMyCertCollection()[0];

		//    ContentInfo content = new ContentInfo(package);
		//    SignedCms signedCms = new SignedCms(content, false);

		//    CmsSigner signer = new CmsSigner(certificate);
		//    signedCms.ComputeSignature(signer);

		//    return signedCms.Encode();
		//}

		///// <summary>Получить коллекцию сертификатов в Личном хранилище Текущего пользователя</summary>
		//public static X509Certificate2Collection GetMyCertCollection()
		//{
		//    X509Certificate2Collection certs;
		//    X509Store MyStore = new X509Store(StoreName.My, StoreLocation.CurrentUser);
		//    MyStore.Open(OpenFlags.ReadOnly);
		//    certs = MyStore.Certificates;
		//    MyStore.Close();
		//    return certs;
		//}

		///// <summary>Получить словарь сертификатов из Личного хранилища Текущего пользователя {серийный номер, наименование}</summary>
		//public static Dictionary<string,string> GetMyCertList()
		//{
		//    Dictionary<string,string> list = new Dictionary<string, string>();

		//    X509Certificate2Collection certs = GetMyCertCollection();

		//    foreach (X509Certificate2 cert in certs)
		//    {
		//        if (cert.SerialNumber != null)
		//            list.Add(cert.SerialNumber, cert.SubjectName.Name);
		//    }

		//    return list;
		//}

		///// <summary>
		///// Получить сертификат по серийному номеру
		///// </summary>
		///// <param name="serialNumber">Серийный номер</param>
		///// <returns>Сертификат</returns>
		//public static X509Certificate2 GetCertificate(string serialNumber)
		//{
		//    return GetMyCertCollection().Find(X509FindType.FindBySerialNumber, serialNumber, true)[0];
		//}

		/// <summary>
		/// Получить JS-скрипт для заполнения ComboBox списком сертификатов (CSP)
		/// </summary>
		/// <param name="plugin">Имя функции, возвращающей экземпляр плагина</param>
		/// <returns>Скрипт</returns>
		public static string PluginGetCspCertListStringJS(string plugin, string comboBoxName)
		{
			return string.Format(@"var list = {0}.cspKeyList;
										for (var i = 0; i <= list.length - 1; i++) 
											{1}.AddItem(list[i].split(':')[4] + ' - ' + list[i].split(':')[6], list[i].split(':')[0] + '&' + list[i].split(':')[2]);
										if (list.length > 0) 
											{1}.SetSelectedIndex(0);
								", plugin, comboBoxName);
		}

		/// <summary>
		/// Получить JS-скрипт для заполнения ComboBox списком сертификатов (Token)
		/// </summary>
		/// <param name="plugin">Имя функции, возвращающей экземпляр плагина</param>
		/// <returns>Скрипт</returns>
		public static string PluginGetEtgCertListStringJS(string plugin, string comboBoxName, int slotNumber = 1)
		{
			return string.Format(@"var list = {0}.etgGetKeyList({2});
										for (var i = 0; i <= list.length - 1; i++) 
											{1}.AddItem(list[i].split(':')[2] + ' - ' + list[i].split(':')[4], list[i].split(':')[0] + '&{2}');
										if (list.length > 0) 
											{1}.SetSelectedIndex(0);
								", plugin, comboBoxName, slotNumber);
		}

		public static string ByteArrayToBase64InEncode(byte[] bytes, Encoding encoding)
		{
			string str = encoding.GetString(bytes);
			return Convert.ToBase64String(encoding.GetBytes(str));
		}

		/// <summary>
		/// Проверка возможности подписывать ОЭ пользователем
		/// </summary>
		/// <param name="user">Пользователь</param>
		/// <param name="demand">ОЭ</param>
		public static bool SignValidator(ExpDemandBase demand)
		{
			//если это не продуктив
            //подписывать ЭЦП может только ОГВ и в статусе "Создано"
			//return !(bool)(SettingsProvider.Instance[OptionsEnum.SystemIsProductive] ?? false)
			//        && Validator.Validate(demand, ObjActionEnum.CanSignECP);

			return Validation.Validator.Validate(demand, ObjActionEnum.CanSignECP);
		}

        ///// <summary>Удалить предыдущие Пакеты с ЭЦП</summary>
        //public static void RemovePreviousPackages(ExpDemand Owner)
        //{
        //    int statusCount = 0;

        //    //получаем кол-во переводов статуса "Отправлено в Минкультуры"
        //    switch (Owner.ExpObject.PlanType)
        //    {
        //        case ExpObjectTypeEnum.Plan:
        //            statusCount = StatusRepository.GetStatusChangesCount(Owner.Id, (int)StatusEnum.Pl11PostedInTheMKS);
        //            break;
        //        case ExpObjectTypeEnum.Plan2012:
        //        case ExpObjectTypeEnum.PreliminaryPlan:
        //            statusCount = StatusRepository.GetStatusChangesCount(Owner.Id, (int)StatusEnum.PrePostedInTheMKS);
        //            break;
        //    }

        //    IList<Document> packages = RepositoryBase<Document>.GetAll(x => x.DocType == DocTypeEnum.demand_inventory &&
        //                                                                    x.Owner == Owner &&
        //                                                                    x.SignedPackage != null);

        //    packages.Remove(RepositoryBase<Document>.Get(packages.Max(p => p.Id)));

        //    foreach (Document doc in packages)
        //        RepositoryBase<Document>.Delete(doc.Id);
        //}

        ///// <summary>Удалить все Пакеты с ЭЦП</summary>
        //public static void RemoveAllPackages(ExpDemand Owner)
        //{
        //    IList<Document> packages = RepositoryBase<Document>.GetAll(x => x.DocType == DocTypeEnum.demand_inventory &&
        //                                                        x.Owner == Owner &&
        //                                                        x.SignedPackage != null);

        //    foreach (Document doc in packages)
        //        RepositoryBase<Document>.Delete(doc.Id);
        //}
	}
}