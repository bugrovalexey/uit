﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class UsersMsgStatusMap : ClassMapBase<UsersMsgStatus>
    {
        public UsersMsgStatusMap()
            : base("Users_Msg_Status")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Users_Msg_Status_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_Users_Msg_Status_Insert
            //						  @Users_Msg_Status_Name = ?
            //						 ,@Users_Msg_Status_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Users_Msg_Status_Update
            //						  @Users_Msg_Status_Name = ?
            //						 ,@Users_Msg_Status_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Users_Msg_Status_Delete
            //						  @Users_Msg_Status_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Users_Msg_Status_Name").Nullable();
        }
    }
}