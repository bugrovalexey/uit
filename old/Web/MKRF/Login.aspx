﻿<%@ Page Language="C#" MasterPageFile="~/MainEmpty.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="GB.MKRF.Web.Login" %>
<%@ Import Namespace="GB.Helpers" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/GB/Styles/form_auth.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <div class="page_wrapper">
    <form id="form1" runat="server">
    <div id="header" style="margin-bottom: 10px">
        <div class="page">
            <div id="logo" class="coord" style="cursor: pointer;" onclick="location = <%=UrlHelper.Resolve("~/") %>">АИС Координация Культура России</div>
        </div>
    </div>
    <div style="width: 350px; margin: 0 auto; padding: 60px 0;">
        <cc:LoginControl ID="loginForm" runat="server" ErrorPanelID="div_error_panel" ButtonCssClass="big green" OnAuthorize="loginForm_Authorize" />
        <div id="div_error_panel" runat="server" class="none curved" style="margin: 15px 0px;"></div>
    </div>
    </form>
    <div class="page_footer_empty" style="margin-top: 10px"></div>
    </div>
    <div id="footer">
        <div class="page">
            <span class="text">© Изготовлено для Минкультуры, 2015 г.</span>
            <%--<span class="text" style="float: right;">
                Служба поддержки:
                
                <%=Uviri.Coord.Web.Code.SettingsProvider.Instance[Uviri.Coord.Entities.Admin.OptionsEnum.TechSpecialistPhone]%>
                |
                <a href="mailto:<%=Uviri.Coord.Web.Code.SettingsProvider.Instance[Uviri.Coord.Entities.Admin.OptionsEnum.TechSpecialistEmail]%>"><%=Uviri.Coord.Web.Code.SettingsProvider.Instance[Uviri.Coord.Entities.Admin.OptionsEnum.TechSpecialistEmail]%></a>
            </span>--%>
        </div>
    </div>
</asp:Content>
