﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Extensions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    [MetadataType(typeof(ActivityEditViewModel_Valid))]
    public class ActivityEditViewModel
    {
        #region consts

        public const string NameSmallCaption = "Наименование";
        public const string TypeCaption = "Тип мероприятия";
        public const string YearCaption = "Год реализации";
        public const string VolY0Caption = "Очередной финансовый год, руб.";
        public const string VolY1Caption = "Первый год планового периода, руб.";
        public const string VolY2Caption = "Второй год планового периода, руб.";

        #endregion

        public int? xId { get; set; }

        public int AOId { get; set; }

        [DisplayName(NameSmallCaption)]
        public string Name { get; set; }

        [DisplayName(TypeCaption)]
        public int Type { get; set; }

        [DisplayName(YearCaption)]
        public int Year { get; set; }

        [DisplayName(VolY0Caption)]
        public object VolY0 { get; set; }

        [DisplayName(VolY1Caption)]
        public object VolY1 { get; set; }

        [DisplayName(VolY2Caption)]
        public object VolY2 { get; set; }

        public SelectList TypeList { get; set; }
        public SelectList YearList { get; set; }

        public ActivityEditViewModel(int aoId, int? id)
        {
            AOId = aoId;
            var ao = new RepositoryDc<AccountingObject>().GetExisting(aoId);

            if (id.HasValue)
            {
                AoActivity activity = new RepositoryDc<AoActivity>().Get(id);

                if (activity != null)
                {
                    xId = activity.Id;
                    Name = activity.Name;
                    Type = (int)activity.ActivityType;
                    Year = activity.Year;
                    VolY0 = activity.CostY0;
                    VolY1 = activity.CostY1;
                    VolY2 = activity.CostY2;
                }
            }
            
            //Stage = Current.Stage;
            Year = DateTime.Now.Year;


            var activityTypes = ao.Stage.ToActivityTypeEnum();
            TypeList = new SelectList(activityTypes.Select(x => new { Key = x.ToString("D"), Value = x.GetDescription() }),
                    "Key", "Value");

            YearList = new SelectList(new YearRepository().GetAll(), "Id", "Value");
        }
    }

    public class ActivityEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }
    }
}