﻿using System.Collections.Generic;
using System.Linq;
using GB.Data;
using GB.Data.Directories;
using NHibernate.Criterion;

namespace GB.DataAccess.Repository
{
    public class IKTComponentRepository : Repository<IKTComponent>
    {
        public IList<IKTComponent> GetAllParents()
        {
            return GetAllEx(q => q.Where(x => x.Parent == null).OrderBy(x => x.Id));
        }

        public IList<IKTComponent> GetAllChildren(int parentId)
        {
            return GetAllEx(q => q.Where(x => (x.Parent != null && x.Parent.Id == parentId) || x.Id == EntityBase.Default).OrderBy(x => x.Id));
        }

        public IList<IKTComponent> GetAllWhoNotHaveChildren()
        {
            var parentIds = Select<int>(q => q.Where(x => x.Parent != null)
                .Select(Projections.Distinct(
                Projections.ProjectionList()
                    .Add(Projections.Property<IKTComponent>(x => x.Parent.Id)))));
            
            return GetAllEx(q => q.WhereRestrictionOn(x => x.Id).Not.IsIn(parentIds.ToList()));
        }

    }
}