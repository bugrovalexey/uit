﻿using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Admin
{
    internal class UsersMsgMap : ClassMapBase<UsersMsg>
    {
        public UsersMsgMap()
            : base("Users_Msg")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Users_Msg_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_Users_Msg_Insert
            //                         @Users_Msg_Text = ?
            //                        ,@Users_Msg_Date = ?
            //                        ,@Users_Msg_Admin_Text = ?
            //                        ,@Users_Msg_Admin_Date = ?
            //                        ,@Users_Msg_Status_ID = ?
            //                        ,@Users_ID = ?
            //                        ,@Users_Msg_Admin_Users_ID = ?
            //                        ,@Users_Msg_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Users_Msg_Update
            //                         @Users_Msg_Text = ?
            //                        ,@Users_Msg_Date = ?
            //                        ,@Users_Msg_Admin_Text = ?
            //                        ,@Users_Msg_Admin_Date = ?
            //                        ,@Users_Msg_Status_ID = ?
            //                        ,@Users_ID = ?
            //                        ,@Users_Msg_Admin_Users_ID = ?
            //                        ,@Users_Msg_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Users_Msg_Delete
            //						  @Users_Msg_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Text, "Users_Msg_Text");
            Map(x => x.Date, "Users_Msg_Date").Not.Nullable();
            Map(x => x.AdminText, "Users_Msg_Admin_Text");
            Map(x => x.AdminDate, "Users_Msg_Admin_Date");
            Map(x => x.Url, "Users_Msg_Url");

            References<UsersMsgStatus>(x => x.UsersMsgStatus, "Users_Msg_Status_ID").Not.Nullable();
            References<User>(x => x.User, "Users_ID").Not.Nullable();
            References<User>(x => x.AdminUser, "Users_Msg_Admin_Users_ID");
        }
    }
}