﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateKindAccountingObjectContext : ICommandContext
    {
        public int xId { get; set; }

        public int KindId { get; set; }
    }
}
