﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System;
    using System.Threading.Tasks;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;

    public class AoTechnicalSupportDeleteTechSupportStory : IStory<AoTechnicalSupportDeleteTechSupportStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;


        public AoTechnicalSupportDeleteTechSupportStory(ISessionNHibernateFactory factory)
        {
            _factory = factory;
        }

        public bool Execute(AoTechnicalSupportDeleteTechSupportStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(AoTechnicalSupportDeleteTechSupportStoryContext context)
        {
            bool result = false;

            using (var session = _factory.OpenSession())
            {
                try
                {
                    await session.DeleteAsync<TechnicalSupport>(context.Id);

                    result = true;
                }
                catch (Exception exc)
                {
                    await session.RollbackAsync();

                    throw;
                }
            }

            return result;
        }
    }
}
