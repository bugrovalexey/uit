﻿using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Validator
{
    class ProjectManagement
    {
        public static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create)
            {
                ProjectManagementSystem entity = Entity as ProjectManagementSystem;

                if (entity.Plan != null)
                    return "0;" + entity.Plan.Id.ToString();
                else
                    return "0";
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
