﻿using System.ComponentModel;

namespace GB.Data.Plans
{
    public enum YearEnum
    {
        [Description("Не задано")]
        None = 0,

        /// <summary>
        /// Очередной год
        /// </summary>
        [Description("Очередной год")]
        Next = 1,

        /// <summary>
        /// Первый плановый год
        /// </summary>
        [Description("Первый плановый год")]
        First = 2,

        /// <summary>
        /// Второй плановый год
        /// </summary>
        [Description("Второй плановый год")]
        Second = 3
    }
}