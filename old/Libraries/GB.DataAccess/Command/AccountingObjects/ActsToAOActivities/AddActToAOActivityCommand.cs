﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;

namespace GB.DataAccess.Command.AccountingObjects.ActsToAOActivities
{
    public class AddActToAOActivityCommand : ICommand<AddActToAOActivityContext, ActToAOActivity>
    {
         ISessionFactoryGb factory;

         public AddActToAOActivityCommand()
        {
            factory = new SessionFactory();
        }

         public ActToAOActivity Execute(AddActToAOActivityContext context)
         {
             var actToActivity = new ActToAOActivity();
             actToActivity.AoActivity = new Repository<AoActivity>().GetExisting(context.AoActivityId);

             if (context.ActId != 0)
             {
                 actToActivity.ActOfAcceptance = new Repository<ActOfAcceptance>().GetExisting(context.ActId);
             }

             using (var session = factory.Create())
            {
                session.Insert(actToActivity);
                session.Commit();
            }

             return actToActivity;
         }
    }
}
