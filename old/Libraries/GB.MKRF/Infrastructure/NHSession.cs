﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace GB.MKRF.Infrastructure
{
    public sealed class NHSession
    {
        private static Mutex sessionMutex = new Mutex();
        private static ISessionFactory Factory;

        private static string NHIBERNATE_SESSION = string.Empty;
        private const string CLIENTCOMPUTERNAME = "ClientComputerName";
        private static string NHIBERNATE_CONNECTIONSTRING_KEY = string.Empty;

        public NHSession(string prefix)
        {
#if DEBUG
            HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
#endif

            NHIBERNATE_SESSION = string.Concat(prefix, "NHibernateSession");
            NHIBERNATE_CONNECTIONSTRING_KEY = string.Concat(prefix, "ConnectionString");
        }

        private static Dictionary<string, string> _dictionary;

        public static string ClientComputerName
        {
            get
            {
                if (string.IsNullOrEmpty(CurrentUserLogin) || _dictionary == null || !_dictionary.ContainsKey(CurrentUserLogin + "ClientComputerName"))
                    return null;

                var compName = _dictionary[CurrentUserLogin + CLIENTCOMPUTERNAME];
                return compName;
            }
        }

        private static void SetClientComputerName(string compName)
        {
            if (string.IsNullOrEmpty(CurrentUserLogin))
                return;

            if (_dictionary == null)
                _dictionary = new Dictionary<string, string>();

            lock (compName)
            {
                if (!_dictionary.ContainsKey(CurrentUserLogin + CLIENTCOMPUTERNAME))
                    _dictionary.Add(CurrentUserLogin + CLIENTCOMPUTERNAME, compName);
            }
        }

        internal static ISession CurrentSession
        {
            get
            {
                ISession session = CallContext.GetData(NHIBERNATE_SESSION) as ISession;

                if (session == null)
                {
                    session = CreateSession();
                    CallContext.SetData(NHIBERNATE_SESSION, session);
                }

                return session;
            }
        }

        public static string CurrentUserLogin
        {
            get
            {
                return (Thread.CurrentPrincipal != null &&
                         Thread.CurrentPrincipal.Identity != null &&
                         Thread.CurrentPrincipal.Identity.Name != null) ?
                         Thread.CurrentPrincipal.Identity.Name : null;
            }
        }

        //public static void BeginRequest()
        //{
        //}

        public static void EndRequest()
        {
            ISession session = CallContext.GetData(NHIBERNATE_SESSION) as ISession;

            if (session != null)
            {
                try
                {
                    session.Close();
                    session.Dispose();
                }
                catch { }

                CallContext.SetData(NHIBERNATE_SESSION, null);
            }
        }

        private static ISession CreateSession()
        {
            ISession session = CreateFactory();

            if (session == null)
                throw new InvalidOperationException("Call to factory.OpenSession() returned null.");

            session.FlushMode = FlushMode.Never;

            return session;
        }

        private static ISession CreateFactory()
        {
            if (Factory == null)
            {
                sessionMutex.WaitOne();

                //Инициализация фабрики первым потоком
                try
                {
                    if (Factory == null)
                    {
                        FluentConfiguration config = GetConfig();
                        //Fluently.Configure()
                        //.Database(MsSqlConfiguration.MsSql2008.ConnectionString(cs => cs.FromConnectionStringWithKey(NHIBERNATE_CONNECTIONSTRING_KEY)))
                        //.Mappings(x =>
                        //{
                        //    x.FluentMappings.AddFromAssembly(GetAssemblyMappings());
                        //    x.FluentMappings.Conventions.Setup(f => f.Add(AutoImport.Never()));
                        //});

#if CACHING
                        config.Cache(c => c.UseSecondLevelCache()
                                            .UseQueryCache()
                                            .ProviderClass<NHibernate.Caches.SysCache2.SysCacheProvider>());
#endif

                        Factory = config.BuildSessionFactory();
                    }
                }
                catch (Exception exp)
                {
                    Logger.Fatal(exp);
                    throw exp;
                }

                sessionMutex.ReleaseMutex();
            }

            if (Factory == null)
                throw new InvalidOperationException("При создании ISessionFactory возникла ошибка. (возможно неправильный маппинг)");

            return Factory.OpenSession();
        }

        private static FluentConfiguration GetConfig()
        {
            return Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008.ConnectionString(cs => cs.FromConnectionStringWithKey(NHIBERNATE_CONNECTIONSTRING_KEY)))
                .Mappings(x =>
                {
                    x.FluentMappings.AddFromAssembly(GetAssemblyMappings());
                    x.FluentMappings.Conventions.Setup(f => f.Add(AutoImport.Never()));
                });
        }

        public static ISession SchemaBuild(bool script, bool export)
        {
            ISessionFactory session = GetConfig()
                .ExposeConfiguration(c => new NHibernate.Tool.hbm2ddl.SchemaExport(c).Create(script, export))
                .BuildSessionFactory();
            //.BuildConfiguration();

            return session.OpenSession();
        }

        //private static void BuildSchema(Configuration config)
        //{
        //    //Creates database structure
        //    new SchemaExport(config)
        //       .Create(false, true);
        //}

        /// <summary>
        /// Поиск сборок помеченных аттрибутом маппинга
        /// </summary>
        private static List<Assembly> GetAssemblyMappings()
        {
            Type type = typeof(AssemblyMappings);
            List<Assembly> list = new List<Assembly>();

            Assembly[] massAssemby = AppDomain.CurrentDomain.GetAssemblies();

            for (int i = 0; i < massAssemby.Length; i++)
            {
                object[] arrObj = massAssemby[i].GetCustomAttributes(type, true);

                if (arrObj.Length > 0)
                {
                    list.Add(massAssemby[i]);
                }
            }

            return list;
        }
    }
}