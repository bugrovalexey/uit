﻿using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Entities.Plans;
using GB.Controls;
using GB.MKRF.Validation;
using DevExpress.Web.ASPxEditors;
using GB;
using GB.Helpers;

namespace GB.MKRF.Web.Activities
{
    public partial class List : System.Web.UI.Page
    {
        protected string GetFavStatus(bool isFavor, int activityID)
        {
            return String.Format("<span class='checkFavor {0}' title='{1}' data-id={2}>&nbsp;</span>", isFavor ? "favorite" : "unfavorite", isFavor ? "Отмечено как избранное" : string.Empty, activityID);
        }

        protected string GetLinkToActivityCard(object Id, object Name)
        {
            return string.Format("<a class='gridLink' href='{0}'>{1}</a>", CommonData.GetUrl(Id), Name);
        }

        protected override void OnPreRender(EventArgs e)
        {
            Grid.DataBind();

            base.OnPreRender(e);
        }

        #region Grid
        PlansActivityController controller = new PlansActivityController();
        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var data = controller.Repository.GetAllEx(q => q
                .Where(x => x.Department == UserRepository.GetCurrent().Department)
                .Fetch(f => f.ActivityType2).Eager
                .Fetch(f => f.AccountingObject).Eager.Clone());

            (sender as ASPxGridView).DataSource = data;
        }

        protected void Grid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void Grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var args = new PlansActivityArgs()
            {
                Name = hf["Name"],
                ActivityType2 = hf["Type"]
            };
            controller.Create(args);

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void Grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var args = new PlansActivityArgs()
            {
                Id = (int)e.Keys[0],
                Name = hf["Name"],
                ActivityType2 = hf["Type"]
            };
            controller.Update2(args);

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void Grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            controller.Delete(new PlansActivityArgs()
            {
                Id = (int)e.Keys[0],
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }
        
        
        protected void NameMemo_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit == null)
                return;

            (sender as ASPxMemo).Text = controller.CurrentEdit.Name;
        }

        protected void TypeLabel_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit != null)
            {
                (sender as ASPxLabel).Text = controller.CurrentEdit.ActivityType2.Name;
            }
        }
        
        protected void TypeComboBox_DataBinding(object sender, EventArgs e)
        {
            var box = sender as ASPxComboBox;

            if (controller.CurrentEdit == null)
            {
                box.DataSource = RepositoryBase<ActivityType2>.GetAll();
                box.Value = EntityBase.Default;
            }
            else
            {
                box.ClientVisible = false;
            }
        }
        
        protected void DepCheckBox_DataBinding(object sender, EventArgs e)
        {
            (sender as ASPxLabel).Text = UserRepository.GetCurrent().Department.Name;
        }

        protected void Grid_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            var grid = sender as ASPxGridView;

            switch (e.ButtonID)
            {
                case "copyButton":
                    var argsCopy = new PlansActivityArgs()
                    {
                        Id = (int)grid.GetRowValues(e.VisibleIndex, "Id"),
                    };
                    controller.Copy(argsCopy);
                    grid.DataBind();
                    break;
            }
        }
        #endregion

        
    }
}