﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public class CharacteristicsViewModel : ActivitiesCardBase
    {
        //public IEnumerable<TableCharacteristics> Items { get; set; }

        public IEnumerable<TableCharacteristic> Characteristics { get; set; }


        public CharacteristicsViewModel(IStatusRepository statusRepository, int id)
            : base(statusRepository, id)
        {
            Characteristics = Characteristics = new AccountingObjectCharacteristicsRepository()
                .GetAllByAccountingObject(CurrentActivity.AccountingObject.Return(ao => ao.Id, default(int))).Select(x => new
                TableCharacteristic(x));

            //Items = AccountingObjectCharacteristicsValueRepository.GetAllForActivity(CurrentActivity)
            //    .Select(x => new TableCharacteristics(x) { ReadOnly = base.ReadOnly });
        }

        public bool AddNew
        {
            get { return !base.ReadOnly && base.CurrentActivity.AccountingObject != null; }
        }
    }

    public class TableCharacteristics : NotificationBaseViewModel
    {
        public object Id { get; set; }

        public object CharValueId { get; set; }

        public object Name { get; set; }

        public object Type { get; set; }

        public object Unit { get; set; }

        public object Fact { get; set; }

        public object Norm { get; set; }

        public object Max { get; set; }

        public bool IsRegister { get; set; }

        public string DeleteLink
        {
            get
            {
                if (ReadOnly || IsRegister)
                    return null;
                return LinkBuilder.AjaxDeleteLink("CharacteristicsDelete", "Activities", new { id = Id }, "Grid");
            }
        }

        public TableCharacteristics(GetActivityCharact x)
        {
            Id = x.CharId;
            Name = x.Name;
            Type = x.Type;
            Unit = x.Unit;
            Fact = x.Fact;
            Norm = x.Norm;
            Max = x.Max;
            IsRegister = x.IsRegister;
        }

        public TableCharacteristics(AccountingObjectCharacteristics cr, AccountingObjectCharacteristicsValue crv)
        {
            if (cr == null)
                cr = crv.Characteristics;

            Id = cr.Id;
            Name = cr.Name;
            Type = cr.Type.Name;
            Unit = cr.Unit.Name;
            Fact = cr.Fact;
            Norm = crv.Norm;
            Max = crv.Max;
            IsRegister = cr.IsRegister;
        }
    }
}