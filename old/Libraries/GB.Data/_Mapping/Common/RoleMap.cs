﻿using GB.Data.Common;

namespace GB._Mapping.Common
{
    internal class RoleMap : ClassMapHiLo<Role>
    {
        public RoleMap()
            : base("Role")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Role_Name");
            Map(x => x.WebPage, "Role_Web_Page");
        }
    }
}