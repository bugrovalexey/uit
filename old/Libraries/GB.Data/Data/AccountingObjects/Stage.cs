﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.Data.AccountingObjects
{
    public class Stage : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }
    }
}
