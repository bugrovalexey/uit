﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/v2012new/ActivityCard.master"
    AutoEventWireup="true" CodeBehind="Expenses.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.Expenses" %>

<asp:Content ID="contentTop" ContentPlaceHolderID="detailTopContent" runat="server">
    <script>
        function SaveExpenses(s, e) {            
            WorkFormMemo.Validate();
            SectionKBKMemo.Validate();
            GRBSMemo.Validate();

            var skip = false;
            if (typeof (SubentryTextBox) == 'undefined') {
                skip = true;
            }
            else {
                SubentryTextBox.Validate();
            }

            if (skip || SubentryTextBox.isValid) {
                var Y0 = PlansActivityVolY0SpinEdit.GetValue();
                var Y1 = PlansActivityVolY1SpinEdit.GetValue();
                var Y2 = PlansActivityVolY2SpinEdit.GetValue();

                if (Y0 <= 0 || Y1 <= 0 || Y2 <= 0) {
                    if (confirm('В полях по бюджетных ассигнованиям присутствуют нулевые значения. Продолжить?')) {
                        callBackEx.PerformCallback('');
                        MainLoadingPanel.Show();
                    }
                }
                else {
                    callBackEx.PerformCallback('');
                    MainLoadingPanel.Show();
                }
            }
        }
    </script>
    <dxe:ASPxButton runat="server" AutoPostBack="false" Text="Сохранить" ID="saveButton" CausesValidation="true"
        ValidationGroup="Expenses" CssClass="fleft topButton">
        <ClientSideEvents Click="function(s,e){ SaveExpenses(); ShowConfirm(false); }" />
    </dxe:ASPxButton>
    <dxc:ASPxCallback runat="server" ID="callBackEx" ClientInstanceName="callBackEx"
        OnCallback="callBackEx_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { MainLoadingPanel.Hide(); ExpenseDirectionsGrid.Refresh(); }" />
    </dxc:ASPxCallback>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function hfExpenseDirectionsSave() {
            hfExpenseDirections.Clear();
            hfExpenseDirections.Set('Id', ExpenseDirectionComboBox.GetValue());
            hfExpenseDirections.Set('Y0', Y0TextBox.GetValue());
            hfExpenseDirections.Set('Y1', Y1TextBox.GetValue());
            hfExpenseDirections.Set('Y2', Y2TextBox.GetValue());
            hfExpenseDirections.Set('AY0', AddY0TextBox.GetValue());
            hfExpenseDirections.Set('AY1', AddY1TextBox.GetValue());
            hfExpenseDirections.Set('AY2', AddY2TextBox.GetValue());
        }

        function validateComboBox(s, e) {
            var cbValue = s.GetValue();
            e.isValid = (cbValue != undefined || cbValue) && cbValue != 0;
        }
        function validateMemo(s, e) {
            var cbValue = s.GetValue();
            e.isValid = (cbValue != undefined || cbValue) && cbValue != '- Не задано';
        }

        function ShowConfirm(isChange) {
            if (isChange) {
                window.onbeforeunload = function (e) {
                    return "Внесенные на странице данные не сохранены.";
                }
            } else {
                window.onbeforeunload = null;
            }
        }

        $(document).ready(function () {
            if (typeof (SubentryTextBox) != 'undefined') {
                SubentryTextBox.TextChanged.AddHandler(function(s, e) { ShowConfirm(true); });
            }

            //ExpenditureItemsComboBox.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            PlansActivityVolY0SpinEdit.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            PlansActivityAddVolY0SpinEdit.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            PlansActivityVolY1SpinEdit.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            PlansActivityAddVolY1SpinEdit.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            PlansActivityVolY2SpinEdit.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            PlansActivityAddVolY2SpinEdit.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });

            PlansActivityFactDate.DateChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            PlansActivityFactVolY0SpinEdit.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            PlansActivityFactVolY1SpinEdit.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });
            PlansActivityFactVolY2SpinEdit.TextChanged.AddHandler(function (s, e) { ShowConfirm(true); });

            <% if(!EnableEdit) {%>            
            $('a .command, .catalogButton').each(function () {                
                $(this).css("display", "none");
            });            
            <% }%>

            UpdateButtonClear('GRBS');
            UpdateButtonClear('ExpenseItem');
            UpdateButtonClear('SectionKBK');
            UpdateButtonClear('WorkForm');
            UpdateButtonClear('ExpenditureItems');
        });

        function UpdateButtonClear(name)
        {
            var val = hf.Get(name + 'Id');

            if (val && val != 0) {
                var btName = '#' + name + 'Memo';
                var bt = $(btName).parents('td.form_edit_input').find('div.ButtonClear');

                bt.css('display', '');
            }
        }


        SelectValue = function (e) {
            PopupControl.SetContentHtml("");
            var title;
            switch (e) {
                case 'ExpenseItem':
                    title = 'Выберите БК ЦСР';
                    break;
                case 'GRBS':
                    title = 'Выберите БК ГРБС';
                    break;
                case 'SectionKBK':
                    title = 'Выберите БК ПЗ/РЗ';
                    break;
                case 'WorkForm':
                    title = 'Выберите БК ВР';
                    break;

                case 'ExpenditureItems':
                    title = 'Выбирите БК КОСГУ';
                    break;
                default:
                    alert('error');
            }

            PopupControl.SetContentUrl("../../Selectors/CatalogItems.aspx?year=<%= CurrentActivity.Plan.Year%>&type=" + e);
            PopupControl.RefreshContentUrl();
            PopupControl.SetHeaderText(title);
            PopupControl.Show();
        };

        CatalogPopupClose = function (type, result) {
            PopupControl.Hide();
            var mass = result.split(';');
            if (mass[0]) hf.Set(type + 'Id', mass[0]);
            if (mass[1]) hf.Set(type + 'Code', mass[1]);
            if (mass[2]) hf.Set(type + 'Name', mass[2]);
            var Memo = '#' + type + 'Memo';
            $(Memo).find('input').val(hf.Get(type + 'Code') + ' ' + hf.Get(type + 'Name'));
            UpdateButtonClear(type);
            ShowConfirm(true);

            PopupControl.SetContentUrl("../../Selectors/CatalogItems.aspx?year=<%= CurrentActivity.Plan.Year%>&type=-999");
            PopupControl.RefreshContentUrl();
        };

        
        function ClearValue(s, name) {
            if (hf.Contains(name + 'Id')) {
                ShowConfirm(true);
            }

            hf.Set(name + 'Id', '0');
            hf.Set(name + 'Name', '');
            hf.Set(name + 'Code', '');

            var Memo = '#' + name + 'Memo';
            $(Memo).find('input').val('- Не задано');

            $(s).css('display', 'none');
        };

    </script>

    <p class="header_grid">
        Сведения об объемах расходов по мероприятиям за счет средств федерального бюджета (государственных внебюджетных фондов)
        <span class="subtitle">
            Заполняется на первом этапе
        </span>
    </p>
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" SyncWithServer="true" />
    <dxpc:ASPxPopupControl ID="PopupControl" runat="server" AllowDragging="True" AllowResize="True"
        CloseAction="CloseButton" Modal="True" EnableViewState="False" 
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px" Height="600px" 
        HeaderText="Выберите БК ГРБС" ClientInstanceName="PopupControl" EnableHierarchyRecreation="True">
    </dxpc:ASPxPopupControl>

    <table class="form_edit">
        <tr>
            <td class="form_edit_desc">
                <span class="required"></span>
                Код по БК ГРБС
            </td>
            <td class="form_edit_input">
                <div style="position:relative">
                    <dxe:ASPxTextBox ID="GRBSMemo" runat="server" ClientInstanceName="GRBSMemo" ClientIDMode="Static" ReadOnly="true" OnDataBinding="Memo_DataBinding">
                        <ClientSideEvents Validation="function(s,e){validateMemo(s,e);}" />
                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Expenses">
                            <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                            <RegularExpression ErrorText="Обязательное поле" ValidationExpression="^[- Не задано]" />
                        </ValidationSettings>
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('GRBS'); return false;" ></div>
                    <div class="ButtonClear catalogButton" style="display:none;" onclick="ClearValue(this, 'GRBS'); return false;" ></div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                <span class="required"></span>
                Код по БК ПЗ/РЗ
            </td>
            <td class="form_edit_input" >
                <div style="position:relative">
                    <dxe:ASPxTextBox ID="SectionKBKMemo" runat="server" ClientInstanceName="SectionKBKMemo" ClientIDMode="Static" ReadOnly="true" OnDataBinding="Memo_DataBinding">
                        <ClientSideEvents Validation="function(s,e){validateMemo(s,e);}" />
                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Expenses">
                            <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                            <RegularExpression ErrorText="Обязательное поле" ValidationExpression="^[- Не задано]" />
                        </ValidationSettings>
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('SectionKBK'); return false;" ></div>
                    <div class="ButtonClear catalogButton" style="display:none;" onclick="ClearValue(this, 'SectionKBK'); return false;" ></div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">
                <span class="required"></span>
                Код по БК ЦСР
            </td>
            <td class="form_edit_input">
                <div style="position:relative">
                    <dxe:ASPxTextBox ID="ExpenseItemMemo" runat="server" ClientInstanceName="ExpenseItemMemo" ClientIDMode="Static" ReadOnly="true" OnDataBinding="Memo_DataBinding">
                        <ClientSideEvents Validation="function(s,e){validateMemo(s,e);}" />
                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Expenses">
                            <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                            <RegularExpression ErrorText="Обязательное поле" ValidationExpression="^[- Не задано]" />
                        </ValidationSettings>
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('ExpenseItem'); return false;" ></div>
                    <div class="ButtonClear catalogButton" style="display:none;" onclick="ClearValue(this, 'ExpenseItem'); return false;" ></div>
                </div>
            </td>
        </tr>
        <%if (CurrentActivity.Plan.Year.Year > 2013)
          { %>
        <tr>
            <td class="form_edit_desc">Подпрограмма
            </td>
            <td class="form_edit_input">
                <dxe:ASPxTextBox ID="SubentryTextBox" runat="server" ClientInstanceName="SubentryTextBox" MaxLength="10">
                    <MaskSettings Mask="00 00" ErrorText="Значение неверно"/>
                    <ClientSideEvents Validation="
                            function(s,e)
                            {
                                if(!(e.value.trim()))
                                    e.isValid = true;
                            }" />                    
                </dxe:ASPxTextBox>
            </td>
        </tr>
        <% } %>
        <tr>
            <td class="form_edit_desc">
                <span class="required"></span>
                Код по БК ВР
            </td>
            <td class="form_edit_input">
                <div style="position:relative">
                    <dxe:ASPxTextBox ID="WorkFormMemo" runat="server" ClientInstanceName="WorkFormMemo" ClientIDMode="Static" Rows="2" ReadOnly="true" OnDataBinding="Memo_DataBinding">
                        <ClientSideEvents Validation="function(s,e){validateMemo(s,e);}" />
                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Expenses">
                            <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                            <RegularExpression ErrorText="Обязательное поле" ValidationExpression="^[- Не задано]" />
                        </ValidationSettings>
                    </dxe:ASPxTextBox>
                    <div class="ButtonSelected catalogButton" onclick="SelectValue('WorkForm'); return false;" ></div>
                    <div class="ButtonClear catalogButton" style="display:none;" onclick="ClearValue(this, 'WorkForm'); return false;" ></div>   
                </div>      
            </td>
        </tr>
    </table>

    <p class="header_grid">
        Объем планируемых бюджетных ассигнований, тыс. руб
        <span class="subtitle">
            Заполняется на первом этапе
        </span>
    </p>

    <table>
        <tr>
            <td></td>
            <td class="form_edit_input">Основной объем:</td>
            <td class="form_edit_input">Дополнительная потребность:</td>
        </tr>
        <tr>
            <td class="form_edit_desc">на <%=CurrentActivity.Plan.Year.Year %> г.
            </td>
            <td class="form_edit_input">
                <dxe:ASPxSpinEdit ID="PlansActivityVolY0SpinEdit" runat="server" AllowNull="False" Width="194"
                    ClientInstanceName="PlansActivityVolY0SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                    HorizontalAlign="Right" Increment="100" MinValue="0" Number="0">
                    <%--<ClientSideEvents Validation="function(s,e){validateSpin(s,e);}" />--%>
                </dxe:ASPxSpinEdit>
            </td>
            <td class="form_edit_input">
                <dxe:ASPxSpinEdit ID="PlansActivityAddVolY0SpinEdit" runat="server" AllowNull="False"
                    ClientInstanceName="PlansActivityAddVolY0SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                    HorizontalAlign="Right" Increment="100" MinValue="0" Number="0" Width="194">
                    <%--<ClientSideEvents Validation="function(s,e){validateSpin(s,e);}" />--%>
                </dxe:ASPxSpinEdit>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">на <%=CurrentActivity.Plan.Year.Year + 1 %> г.
            </td>
            <td class="form_edit_input">
                <dxe:ASPxSpinEdit ID="PlansActivityVolY1SpinEdit" runat="server" AllowNull="False" Width="194"
                    ClientInstanceName="PlansActivityVolY1SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                    HorizontalAlign="Right" Increment="100" MinValue="0" Number="0">
                    <%--<ClientSideEvents Validation="function(s,e){validateSpin(s,e);}" />--%>
                </dxe:ASPxSpinEdit>
            </td>
            <td class="form_edit_input">
                <dxe:ASPxSpinEdit ID="PlansActivityAddVolY1SpinEdit" runat="server" AllowNull="False"
                    ClientInstanceName="PlansActivityAddVolY1SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                    HorizontalAlign="Right" Increment="100" MinValue="0" Number="0" Width="194">
                    <%-- <ClientSideEvents Validation="function(s,e){validateSpin(s,e);}" />--%>
                </dxe:ASPxSpinEdit>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">на <%=CurrentActivity.Plan.Year.Year + 2 %> г.
            </td>
            <td class="form_edit_input">
                <dxe:ASPxSpinEdit ID="PlansActivityVolY2SpinEdit" runat="server" AllowNull="False" Width="194"
                    ClientInstanceName="PlansActivityVolY2SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                    HorizontalAlign="Right" Increment="100" MinValue="0" Number="0">
                    <%--<ClientSideEvents Validation="function(s,e){validateSpin(s,e);}" />--%>
                </dxe:ASPxSpinEdit>
            </td>
            <td class="form_edit_input">
                <dxe:ASPxSpinEdit ID="PlansActivityAddVolY2SpinEdit" runat="server" AllowNull="False"
                    ClientInstanceName="PlansActivityAddVolY2SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                    HorizontalAlign="Right" Increment="100" MinValue="0" Number="0" Width="194">
                    <%--<ClientSideEvents Validation="function(s,e){validateSpin(s,e);}" />--%>
                </dxe:ASPxSpinEdit>
            </td>
        </tr>
    </table>
    
    <p class="header_grid">
        Объем фактически израсходованных бюджетных ассигнований, тыс. руб
        <span class="subtitle">
            Заполняется на первом этапе
        </span>
    </p>

    <table class="form_edit">
        <tr>
            <td class="form_edit_desc">по состоянию на дату
            </td>
            <td class="form_edit_input">
                <dxe:ASPxDateEdit runat="server" ID="PlansActivityFactDate" ClientInstanceName="PlansActivityFactDate" />
            </td>
        </tr>

        <tr>
            <td class="form_edit_desc">фактически израсходовано в текущем финансовом году
            </td>
            <td class="form_edit_input">
                <dxe:ASPxSpinEdit ID="PlansActivityFactVolY0SpinEdit" runat="server" AllowNull="False"
                    ClientInstanceName="PlansActivityFactVolY0SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                    HorizontalAlign="Right" Increment="100" MinValue="0" Number="0">
                    <%--<ClientSideEvents Validation="function(s,e){validateSpin(s,e);}" />--%>
                </dxe:ASPxSpinEdit>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">в отчетном финансовом году (текущий год -1)
            </td>
            <td class="form_edit_input">
                <dxe:ASPxSpinEdit ID="PlansActivityFactVolY1SpinEdit" runat="server" AllowNull="False"
                    ClientInstanceName="PlansActivityFactVolY1SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                    HorizontalAlign="Right" Increment="100" MinValue="0" Number="0">
                    <%--<ClientSideEvents Validation="function(s,e){validateSpin(s,e);}" />--%>
                </dxe:ASPxSpinEdit>
            </td>
        </tr>
        <tr>
            <td class="form_edit_desc">в финансовом году, предшествующему отчетному (текущий год -2)
            </td>
            <td class="form_edit_input">
                <dxe:ASPxSpinEdit ID="PlansActivityFactVolY2SpinEdit" runat="server" AllowNull="False"
                    ClientInstanceName="PlansActivityFactVolY2SpinEdit" DecimalPlaces="4" DisplayFormatString="#,##0.0000"
                    HorizontalAlign="Right" Increment="100" MinValue="0" Number="0">
                    <%-- <ClientSideEvents Validation="function(s,e){validateSpin(s,e);}" />--%>
                </dxe:ASPxSpinEdit>
            </td>
        </tr>
    </table>

    <dxhf:ASPxHiddenField runat="server" ID="hfExpenseDirections" ClientInstanceName="hfExpenseDirections" />

    <p class="header_grid">
        Направления расходования средств на ИКТ
        <span class="subtitle">
            Заполняется на втором этапе
        </span>
    </p>

    <dxe:ASPxButton ID="AddExpenseDirectionsButton" runat="server" ClientInstanceName="AddExpenseDirectionsButton"
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {ExpenseDirectionsGrid.AddNewRow();}" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="ExpenseDirectionsGrid" Width="100%" ClientInstanceName="ExpenseDirectionsGrid"
        KeyFieldName="Id"
        OnInitNewRow="ExpenseDirectionsGrid_InitNewRow"
        OnBeforePerformDataSelect="ExpenseDirectionsGrid_BeforePerformDataSelect"
        OnStartRowEditing="ExpenseDirectionsGrid_StartRowEditing"
        OnRowInserting="ExpenseDirectionsGrid_RowInserting"
        OnRowUpdating="ExpenseDirectionsGrid_RowUpdating"
        OnRowDeleting="ExpenseDirectionsGrid_RowDeleting"
        OnSummaryDisplayText="ExpenseDirectionsGrid_SummaryDisplayText">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn FieldName="CodeName" Caption="Вид затрат" />
            <dxwgv:GridViewBandColumn Caption="Объем планируемых бюджетных ассигнований, тыс. руб.">
                <Columns>
                    <dxwgv:GridViewDataTextColumn FieldName="ExpenseVolY0" Caption="очередной финансовый год">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <HeaderStyle Wrap="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="ExpenseVolY1" Caption="первый год планового периода">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <HeaderStyle Wrap="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="ExpenseVolY2" Caption="второй год планового периода">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <HeaderStyle Wrap="False" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
            <dxwgv:GridViewBandColumn Caption="Дополнительная потребность, тыс. руб.">
                <Columns>
                    <dxwgv:GridViewDataTextColumn FieldName="ExpenseAddVolY0" Caption="очередной финансовый год">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <HeaderStyle Wrap="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="ExpenseAddVolY1" Caption="первый год планового периода">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <HeaderStyle Wrap="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="ExpenseAddVolY2" Caption="второй год планового периода">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <HeaderStyle Wrap="False" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
            <dxwgv:GridViewBandColumn Caption="Итого, тыс. руб.">
                <Columns>
                    <dxwgv:GridViewDataTextColumn FieldName="IY0" Caption="очередной финансовый год">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <HeaderStyle Wrap="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="IY1" Caption="первый год планового периода">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <HeaderStyle Wrap="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="IY2" Caption="второй год планового периода">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <HeaderStyle Wrap="False" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
        </Columns>
        <TotalSummary>
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="ExpenseVolY0" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="ExpenseVolY1" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="ExpenseVolY2" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="ExpenseAddVolY0" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="ExpenseAddVolY1" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="ExpenseAddVolY2" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="IY0" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="IY1" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="IY2" SummaryType="Sum" />
        </TotalSummary>
        <Settings ShowFooter="True" />
        <SettingsBehavior ConfirmDelete="True" />
        <Styles>
            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
        </Styles>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Вид затрат
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ExpenseDirectionComboBox" runat="server" ClientInstanceName="ExpenseDirectionComboBox"
                                TextField="Value" ValueField="Key" ValueType="System.Int32" EncodeHtml="false" Width="450px"
                                
                                ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                OnDataBinding="ExpenseDirectionComboBox_DataBinding">
                                <ClientSideEvents Validation="function(s,e){validateComboBox(s,e);}" />
                                <ValidationSettings ErrorText="Обязательное поле" ErrorDisplayMode="None" ValidationGroup="EditForm">
                                    <RequiredField IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>

                    <tr>
                        <td><b>Основной объем:</b></td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">очередной финансовый год</td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="Y0TextBox" runat="server" ClientInstanceName="Y0TextBox" MaskSettings-Mask="<0..999999999g>.<0000..9999>"
                                OnDataBinding="Y0TextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">первый год планового периода</td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="Y1TextBox" runat="server" ClientInstanceName="Y1TextBox" MaskSettings-Mask="<0..999999999g>.<0000..9999>"
                                OnDataBinding="Y1TextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">второй год планового периода</td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="Y2TextBox" runat="server" ClientInstanceName="Y2TextBox" MaskSettings-Mask="<0..999999999g>.<0000..9999>"
                                OnDataBinding="Y2TextBox_DataBinding" />
                        </td>
                    </tr>

                    <tr>
                        <td><b>Доп. потребность:</b></td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">очередной финансовый год</td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="AddY0TextBox" runat="server" ClientInstanceName="AddY0TextBox" MaskSettings-Mask="<0..999999999g>.<0000..9999>"
                                OnDataBinding="AddY0TextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">первый год планового периода</td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="AddY1TextBox" runat="server" ClientInstanceName="AddY1TextBox" MaskSettings-Mask="<0..999999999g>.<0000..9999>"
                                OnDataBinding="AddY1TextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">второй год планового периода</td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="AddY2TextBox" runat="server" ClientInstanceName="AddY2TextBox" MaskSettings-Mask="<0..999999999g>.<0000..9999>"
                                OnDataBinding="AddY2TextBox_DataBinding" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                                <span class="ok"><dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" /></span>
                                <span class="cancel"><dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" /></span>
                            </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') hfExpenseDirectionsSave(e); }" />
    </dxwgv:ASPxGridView>

    <div style="padding: 10px 20px; font: 0.9em Arial;">
        * В случае, если объем бюджетных ассигнований по направлениям расходов на ИКТ превышает общий объем планируемых 
                    бюджетных ассигнований, значение выделено <span style="color: red">красным цветом</span>
    </div>

</asp:Content>

