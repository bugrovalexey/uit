﻿using GB.Data.AccountingObjects;
using GB.DataAccess.Repository;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public class InformationInteractionService : ServiceBase<InformationInteraction>, IService<InformationInteraction, InformationInteractionArgs>
    {
        public InformationInteraction Create(InformationInteractionArgs args)
        {
            var entity = ConvertDtoToEntity.FillInformationInteractionFromDto(new InformationInteraction(), args);
            base.SaveOrUpdate(entity);

            return entity;
        }

        public InformationInteraction Update(InformationInteractionArgs args)
        {
            //SetCurrentEdit(args.Id);
            var entity = Repository.GetExisting(args.Id);
            ConvertDtoToEntity.FillInformationInteractionFromDto(entity, args);
            base.SaveOrUpdate(entity);

            return entity;
        }

        public void Delete(InformationInteractionArgs args)
        {
            base.Delete((int)args.Id);
        }

        private InformationInteractionRepository _repository = new InformationInteractionRepository();

        public new InformationInteractionRepository Repository
        {
            get { return _repository; }
        }
    }
}