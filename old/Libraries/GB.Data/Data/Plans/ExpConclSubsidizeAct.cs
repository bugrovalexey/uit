﻿using System.Collections.Generic;
using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>Акт экспертного заключения(мероприятия по заключению)</summary>
    public class ExpConclSubsidizeAct : EntityBase // ExpConclActBase
    {

        /// <summary>Эксперт</summary>
        //public virtual new ExpConclusion ExpConclusion
        //{
        //    get { return (ExpConclusion)base.ExpConclusion; }
        //    protected set { base.ExpConclusion = value; }
        //}

        /// <summary>Мероприятие Плана</summary>
        public virtual PlansActivityMU PlanActivityMU { get; set; }

        /// <summary>Соответствие критериям</summary>
        public virtual IList<ExpCriterionEquivalent> ExpCriterionEquivalent
        {
            get;
            protected set;
        }
        //{
        //    get { return base.ExpCriterionEquivalent.Cast<ExpCriterionEquivalent>().ToList(); }
        //    protected set { base.ExpCriterionEquivalent = value.Cast<ExpCriterionEquivalentBase>().ToList(); }
        //}

        /// <summary>Экспертное заключение</summary>
        public virtual ExpConclusion ExpConclusion { get; set; }

        /// <summary>Cоответствие</summary>
        public virtual ExpActEquivalent ExpActEquivalent { get; set; }

        /// <summary>Несоответствие</summary>
        public virtual string Disagreement { get; set; }//null

        /// <summary>
        /// Проведение мероприятий с точки зрения эффективности
        /// </summary>
        public virtual Appropriate GoalEffective { get; set; }

        /// <summary>
        /// Финансирование мероприятий в запрашиваемых объемах
        /// </summary>
        public virtual Appropriate OrderFinance { get; set; }

        /// <summary>
        /// Целесообразный объем финансирования, текущий год
        /// </summary>
        public virtual double AppropriateVolumeFinanceY0 { get; set; }

        /// <summary>
        /// Целесообразный объем финансирования, первый год
        /// </summary>
        public virtual double AppropriateVolumeFinanceY1 { get; set; }

        /// <summary>
        /// Целесообразный объем финансирования, второй год
        /// </summary>
        public virtual double AppropriateVolumeFinanceY2 { get; set; }

        /// <summary>
        /// Предложение по доработке
        /// </summary>
        public virtual string Completion { get; set; }
    }
}