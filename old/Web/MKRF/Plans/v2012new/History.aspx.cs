﻿using System;
using System.Data;
using System.Text;
using GB.Helpers;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Repository;
using GB.MKRF.Report;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class History : System.Web.UI.Page
    {
        public static string GetUrl(int planId)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/Plans/v2012new/History.aspx"), PlanCard.plansIdKey, planId);
        }


        protected override void OnPreRender(EventArgs e)
        {
            HistoryGrid.DataBind();
            
            // Видимость "версии экспертных заключений"
            //var curUserRole = UserRepository.GetCurrent().Role;
            //if (!RoleEnum.Expert.IsEq(curUserRole) && (!RoleEnum.MKS.IsEq(curUserRole)))
            //{
            //    //ExpConclGrid.Visible = false;
            //    HistoryGrid.Columns["ExpConclVer"].Visible = false;
            //}
            //else ExpConclGrid.DataBind();

            base.OnPreRender(e);
        }

        public class historyDate
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }


        protected void HistoryGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            HistoryGrid.DataSource = PlanRepository.GetHistory((Master as PlanCard).CurrentPlan, (Master as PlanCard).CurrentPlan.PlanType);
        }

        protected string getHistoryActionLink(DateTime date)
        {
            return "<a class='popup' href='" + HistoryAction.GetUrl((Master as PlanCard).CurrentPlan, date) + "'>Мероприятия</a>";
        }

        //protected string getViewLinks(DateTime date)
        //{
        //    int Id = (Master as PlanCard).CurrentPlan.Id;

        //    System.Text.StringBuilder sb = new System.Text.StringBuilder();

        //    sb.AppendFormat("<a href='{0}'>Р1</a><br/><br/>", ReportPage.GetUrl(ReportType.PlanSectionOne, Id, date.Ticks));
        //    sb.AppendFormat("<a href='{0}'>Р2</a><br/><br/>", ReportPage.GetUrl(ReportType.PlanSectionTwo, Id, date.Ticks));
        //    sb.AppendFormat("<a href='{0}'>Р3</a>", ReportPage.GetUrl(ReportType.PlanSectionThree, Id));

        //    return sb.ToString();
        //}

        //protected void VersionsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        //{
        //    (sender as ASPxGridView).DataSource = ConclusionVersionRepository.GetConclusionVersions((Master as PlanCard).CurrentPlan);
        //}

        //protected void VersionsGrid_CustomCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        //{
        //    //string[] parameters = e.Parameters.Split('#');
        //    //switch (parameters[0])
        //    //{
        //    //    //добавляем эксперта из списка
        //    //    case "addVersion":
        //    //        AddVersion(Conclusion);
        //    //        (sender as ASPxGridView).DataBind();
        //    //        break;
        //    //    default:
        //    //        break;
        //    //}
        //}

        //protected string GetCommandButtons(string id, string expNumber)
        //{
        //    return String.Format("<a href='HistoryExpertDetail.aspx?Id={0}&plan={1}'>ЭЗ №{2}</a>", id, (Master as PlanCard).CurrentPlan.Id, expNumber);
        //}

        //protected string getExpConclVersionLinks(string vers)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    if (String.IsNullOrEmpty(vers)) return "";
        //    string[] expConclIds = vers.Split(';');
        //    foreach (var id in expConclIds)
        //    {
        //        string[] line = id.Split(',');
        //        string href = "<a href='"+UrlHelper.Resolve("/coord/Plans/v2012new/HistoryExpertDetail.aspx") + "?Id={0}&plan={1}'>"+
        //            "{2}<href>"+"<br />";
        //        sb.Append(String.Format(href, line[0], (Master as PlanCard).CurrentPlan.Id, line[1]));
        //    }

        //    return sb.ToString();

        //}


    }
}