﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class MarksCreateOrUpdateMarkStory : IStory<MarksCreateOrUpdateMarkStoryContext, ActivitysMarksDto>
    {
        private readonly IMapperBase _mapper;
        private readonly ISessionNHibernateFactory _sessionNHibernateFactory;
        private readonly IRepositoryRui _repositoryRui;

        public MarksCreateOrUpdateMarkStory(IMapperBase mapper, ISessionNHibernateFactory factory, IRepositoryRui repository)
        {
            _mapper = mapper;
            _sessionNHibernateFactory = factory;
            _repositoryRui = repository;
        }

        public ActivitysMarksDto Execute(MarksCreateOrUpdateMarkStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivitysMarksDto> ExecuteAsync(MarksCreateOrUpdateMarkStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            using (var session = _sessionNHibernateFactory.OpenSession())
            {
                PlansActivityMark mark;

                if (context.Id.HasValue)
                {
                    mark = _repositoryRui.Get<PlansActivityMark>(context.Id);
                }
                else
                {
                    mark = new PlansActivityMark();

                    mark.PlansActivity = _repositoryRui.Get<PlansActivity>(context.ActivityId);
                }

                _mapper.Map(context, mark);

                mark.Unit = _repositoryRui.Get<Units>(context.UnitId);

                await session.SaveOrUpdateAsync(mark);

                return _mapper.Map(mark, new ActivitysMarksDto());
            }
        }
    }
}
