﻿namespace bgTeam.IsRui.WebApp.ViewModels.Shared
{
    public class AjaxLinkViewModel
    {
        public string Text { get; set; }

        public string Callback { get; set; }

        public string ConfirmMessage { get; set; }

        public string Class { get; set; }

        public string Title { get; set; }

        public bool Disabled { get; set; }

        public string Css { get; set; }
    }
}