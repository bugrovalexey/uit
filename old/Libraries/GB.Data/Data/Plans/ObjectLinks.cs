﻿namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>
    /// Связь между объектами экспертизы
    /// </summary>
    public class ObjectLinks : EntityBase
    {
        public virtual EntityBase Parent { get; set; }
        public virtual EntityBase Child { get; set; }
    }
}
