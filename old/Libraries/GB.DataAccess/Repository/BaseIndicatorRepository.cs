﻿using System.Collections.Generic;
using System.Linq;
using GB.Data.Plans;

namespace GB.DataAccess.Repository
{
    public class BaseIndicatorRepository<T> : Repository<T> where T : BaseIndicator
    {
        public List<T> GetAllByPlansActivity(int plansActivityId)
        {
            return GetAllEx(q => q
                    .Fetch(x => x.Unit).Eager
                    .Where(x => x.PlansActivity.Id == plansActivityId))
                .ToList();
        }
    }
}