﻿using System;
using GB.MKRF.Entities.Plans;
using GB.Repository;
using DevExpress.Web.ASPxTabControl;
using GB.MKRF.Report;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class ActivityCard : System.Web.UI.MasterPage
    {
        public const string ActivityIdKey = "ActivityId";

        /// <summary>
        /// Текущее мероприятие
        /// </summary>
        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as ActivityCardCommon).CurrentActivity;
            }
        }

        public bool EnableEdit
        {
            get
            {
                return (Master as ActivityCardCommon).EnableEdit;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //if (CurrentActivity != null)
            //{
            //    foreach (Tab t in tc.Tabs)
            //    {
            //        t.Visible = DbSiteMapProvider.СheckAccessUser(t.NavigateUrl);
            //        if (!t.Visible)
            //            continue;
            //        t.NavigateUrl = String.Format("{0}?ActivityId={1}", t.NavigateUrl, CurrentActivity.Id);
            //    }
            //}
        }

        public void RefreshActivity()
        {
            RepositoryBase<PlansActivity>.Refresh(CurrentActivity);
        }

        //protected string getPlanTitle()
        //{
        //    if (CurrentActivity == null)
        //        return null;

        //    return CurrentActivity.Plan.Department.Name + " (" + CurrentActivity.Plan.Department.Code + ")" +
        //        ", " + CurrentActivity.Plan.Year.Year.ToString() + "г.";
        //}

        //protected string getPriority()
        //{
        //    if (CurrentActivity.IsPriority)
        //        return "Приоритетное мероприятие по информатизации";
        //    else
        //        return "Мероприятие по информатизации, не являющееся приоритетным";
        //}

       
    }
}