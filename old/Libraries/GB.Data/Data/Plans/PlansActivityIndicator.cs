﻿namespace GB.Data.Plans
{
    public class PlansActivityIndicator : BaseIndicator
    {
        /// <summary>
        /// Алгоритм расчета
        /// </summary>
        public virtual string AlgorithmOfCalculating { get; set; }
    }
}