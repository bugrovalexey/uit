﻿using System;
using System.Text;
using GB.Helpers;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;

namespace GB.MKRF.Web.Common
{
    public partial class ImproveInterfaceButton : System.Web.UI.UserControl
    {
        protected void impCallBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            if ((MessageMemo.Value != null) && (!MessageMemo.Value.ToString().Trim().IsNullOrWhiteSpace()))
            {
                string text = MessageMemo.Value.ToString().Trim();
                if (!string.IsNullOrWhiteSpace(text))
                {
                    e.Result = "ok";

                    // Запустить  в отдельном потоке
                    System.Threading.Thread thread = new System.Threading.Thread(
                        delegate()
                        {
                            SendMessage(text, Request.Url.OriginalString);
                        });

                    thread.Start();
                }
            }
        }


        private void SendMessage(string text, string url)
        {
            User currentUser = UserRepository.GetCurrent();
            string departmentName = currentUser.Department == null ? null : currentUser.Department.Name;

            UsersMsg message = new UsersMsg();
            message.User = currentUser;
            message.Text = text;
            message.Date = DateTime.Now;
            message.Url = url;
            message.UsersMsgStatus = RepositoryBase<UsersMsgStatus>.Get((int)Undefined.value);

            RepositoryBase<UsersMsg>.SaveOrUpdate(message);
        }
    }
}