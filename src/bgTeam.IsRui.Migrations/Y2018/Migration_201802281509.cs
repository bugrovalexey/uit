﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201802281509)]
    public class Migration_201802281509 : ForwardOnlyMigration
    {
        public override void Up()
        {

            Create.Table("Plans_Activity_Docs")
                .WithColumn("Plans_Activity_Docs_ID").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Plans_Activity_ID").AsInt32().NotNullable()
                .WithColumn("Plans_Activity_Docs_Name").AsString().Nullable()
                .WithColumn("Plans_Activity_Docs_Number").AsString().Nullable()
                .WithColumn("Plans_Activity_Docs_Date_Adoption").AsDateTime().Nullable()
                .WithColumn("Plans_Activity_Docs_ParticleOfDocument").AsString().Nullable()
                .WithColumn("OU_Acts_FileId").AsInt32().Nullable()
                .WithColumn("OU_Acts_FileName").AsString().Nullable()
                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                 .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Create.ForeignKey("FK_Plans_Activity_Docs")
               .FromTable("Plans_Activity_Docs").ForeignColumn("Plans_Activity_ID")
               .ToTable("Plans_Activity").PrimaryColumn("Plans_Activity_ID");

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "Plans_Activity_Docs" });

        }
    }
}
