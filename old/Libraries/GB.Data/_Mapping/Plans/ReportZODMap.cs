﻿using Uviri.Entity.Import;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ReportZODMap : ClassMapBase<ReportZod>
        
    {
        public ReportZODMap():
            base("Report_ZOD")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.IKTComponent, "Report_ZOD_IKT_Component");
            Map(x => x.IsChange, "Report_ZOD_Is_Change");

            References<PlansActivity>(x => x.PlansActivity, "Plans_Activity_ID");
            References<Zod>(x => x.Zod, "ZOD_ID");
            References<Department>(x => x.OGV, "Govt_Organ_ID");
            References<Department>(x => x.OGVAuthorized, "Authorized_Govt_Organ_ID");
            References<RegistryImport>(x => x.RegistryImport, "Registry_Import_ID").Nullable();
        }
    }
}
