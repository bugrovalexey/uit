﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="GB.MKRF.Web.Monitoring.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script>
        ActiveMenuItem = 3;
    </script>
    <div class="page">
        <ul>
            <li><a href="DepartmentStatFull.aspx">Статистика по департаментам</a></li>
            <li><a href="SummaryAskedReport.aspx">Сводно-задаваемый отчет</a></li>
            <li><a href="ReportActivity.aspx">Сведения по мероприятиям</a></li>
        </ul>
    </div>
</asp:Content>
