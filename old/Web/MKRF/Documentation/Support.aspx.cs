﻿using System;
using System.Text;
using GB.MKRF.Entities;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.Repository;

namespace GB.MKRF.Web.Documentation
{
	public partial class Support : System.Web.UI.Page
	{
        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            SendMessage(e.Parameter);
        }

		private void SendMessage(string text)
        {
            User currentUser = UserRepository.GetCurrent();
            string departmentName = currentUser.Department == null ? null : currentUser.Department.Name;

            UsersMsg message = new UsersMsg();
            message.User = currentUser;
            message.Text = text;
            message.Date = DateTime.Now;
            message.UsersMsgStatus = RepositoryBase<UsersMsgStatus>.Get((int)Undefined.value);// "новое"

            RepositoryBase<UsersMsg>.SaveOrUpdate(message);

            //EmailQueue queue = new EmailQueue();
            //queue.From = "robot";
            //queue.To = "mkrf";
            ////queue.To = (string)RepositoryBase<Option>.Get((int)OptionsEnum.TechSupportObservers).Deserialize();
            //queue.SheduledDate = DateTime.Now;
            //queue.IsHtml = true;

            //StringBuilder builder = new StringBuilder(text);
            //builder.Append("<br />");
            //builder.Append("<i>Информация о пользователе:</i><br />");
            //if (!string.IsNullOrEmpty(departmentName))
            //{
            //    builder.Append("<br />");
            //    builder.Append("ОГВ: " + departmentName);
            //}

            //if (!string.IsNullOrEmpty(currentUser.FullName))
            //{
            //    builder.Append("<br />");
            //    builder.Append("ФИО пользователя: " + currentUser.FullName);
            //}

            //if (!string.IsNullOrEmpty(currentUser.Email))
            //{
            //    builder.Append("<br />");
            //    builder.Append("E-mail: " + currentUser.Email);
            //}

            //if (!string.IsNullOrEmpty(currentUser.WorkPhone))
            //{
            //    builder.Append("<br />");
            //    builder.Append("Тел: " + currentUser.WorkPhone);
            //}

            //queue.Text = builder.ToString();

            //queue.Subject = "Сообщение администратору ИТ 'Аудит'";

            //if (!string.IsNullOrEmpty(departmentName))
            //{
            //    queue.Subject += (" от ОГВ '" + departmentName + "'");
            //}

            //RepositoryBase<EmailQueue>.SaveOrUpdate(queue);
        }
	}
}