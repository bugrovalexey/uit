﻿using GB.Data.AccountingObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.GovContracts
{
    public class SaveGovContractCommand : ICommand<SaveGovContractContext, GovContract>
    {
        ISessionFactoryGb factory;

        public SaveGovContractCommand()
        {
            factory = new SessionFactory();
        }

        public GovContract Execute(SaveGovContractContext context)
        {
            var c = new Repository.Repository<GovContract>().Get(context.Id);

            c.ReasonsCostDeviation = context.RcDeviation;

            using (var session = factory.Create())
            {
                session.SaveOrUpdate(c);
                session.Commit();
            }

            return c; 
        }
    }
}
