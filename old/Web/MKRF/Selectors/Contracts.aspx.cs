﻿using System;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.Selectors
{
    public partial class Contracts : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            SelectorsContractsGrid.DataBind();

            base.OnPreRender(e);
        }

        protected void SelectorsContractsGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var spz = Request.QueryString["spz"];
            var source = ContractRepository.GetList(spz);

            SelectorsContractsGrid.DataSource = source;
        }

        protected void SelectorsContractsGrid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", "$(this).addClass(\"grid_mouseover\")");
            e.Row.Attributes.Add("onmouseout", "$(this).removeClass(\"grid_mouseover\")");
        }
    }
}