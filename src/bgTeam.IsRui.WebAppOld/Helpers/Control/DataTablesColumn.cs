﻿using System;
using System.Collections.Specialized;
using System.Linq.Expressions;
using System.Reflection;

namespace bgTeam.IsRui.WebApp.Helpers.Control
{
    /// <summary>
    /// Класс для описания свойств колонки таблицы DataTables
    /// </summary>
    public class DataTablesColumn
    {
        /// <summary>
        /// Поле, по которому биндимся к датасорсу
        /// </summary>
        public string FieldName { get; protected set; }

        /// <summary>
        /// Отображаемое название
        /// </summary>
        public string Caption { get; protected set; }

        /// <summary>
        /// Используется в фильтрах?
        /// </summary>
        public bool Searchable { get; protected set; }

        /// <summary>
        /// Используется в сортировке?
        /// </summary>
        public bool Sortable { get; protected set; }

        /// <summary>
        /// true - Фильтр в виде выпадающего списка, если Searchable == true
        /// false - Фильтр в виде текстового поля
        /// по умолчанию - false
        /// </summary>
        public bool Selectable { get; protected set; }

        /// <summary>
        /// Отображается ли?
        /// </summary>
        public bool Visible { get; protected set; }

        /// <summary>
        /// Css классы применяемые к тегам td
        /// </summary>
        public string CssClass { get; protected set; }

        /// <summary>
        /// Ширина столбца, применяемае к тегам td
        /// </summary>
        public string Width { get; protected set; }

        /// <summary>
        /// Колонка ли булевских значений?
        /// </summary>
        public bool CheckBoxColumn { get; protected set; }

        public string Format { get; protected set; }

        public LambdaExpression Expression { get; private set; }

        private Delegate compiledExpression;

        public DataTablesColumn()
        {
            Searchable = false;
            Sortable = false;
            Visible = true;
        }

        public DataTablesColumn(string fieldName, string caption)
            : this()
        {
            this.FieldName = fieldName;
            this.Caption = caption;
        }

        public DataTablesColumn(LambdaExpression expression, string fieldName, string caption)
            : this()
        {
            this.Expression = expression;
            this.FieldName = fieldName;
            this.Caption = caption;

            compiledExpression = this.Expression.Compile();
        }

        public object GetValue(object data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            if (compiledExpression != null)
            {
                try
                {
                    return compiledExpression.DynamicInvoke(data);
                }
                catch (TargetInvocationException e)
                {
                    if (e.InnerException is NullReferenceException)
                        return null;
                    throw;
                }
            }
            var property = data.GetType().GetProperty(FieldName);
            if (property == null)
                throw new ArgumentException("В данных не найден столбец - " + FieldName);

            return property.GetValue(data, null);
        }

        public DataTablesColumn SetVisiblie(bool value)
        {
            Visible = value; return this;
        }

        public DataTablesColumn SetWidth(string value)
        {
            Width = value; return this;
        }

        public DataTablesColumn SetFormat(string value)
        {
            Format = value; return this;
        }

        public DataTablesColumn Search(bool value)
        {
            Searchable = value; return this;
        }

        public DataTablesColumn Sort(bool value)
        {
            Sortable = value; return this;
        }

        public DataTablesColumn Select(bool value)
        {
            Selectable = value; return this;
        }
    }

    public class DataTablesCommandColumn : DataTablesColumn
    {
        /// <summary>
        /// Строка, содержащая ссылку, по которой происходит действие
        /// </summary>
        public string CommandLink { get; set; }

        /// <summary>
        /// Параметры, которые будут переданы в ссылку CommandLink.
        /// В качестве key - наименование параметра в ссылке.
        /// В качестве value - поле экземпляра коллекции, из которого достается значение параметра
        /// </summary>
        public StringDictionary Parameters { get; set; }

        // public string JsHendler { get; set; }

        public DataTablesCommandColumn()
        {
            Searchable = false;
            Sortable = false;
            Caption = string.Empty;

            Parameters = new StringDictionary();
        }
    }

    public class DataTablesDeleteColumn : DataTablesColumn
    {
        public DataTablesDeleteColumn(bool visible, string field = "DeleteLink")
        {
            this.FieldName = field;
            this.Caption = string.Empty;
            this.Visible = visible;

            this.Searchable = false;
            this.Sortable = false;
        }
    }

    public class DataTablesDownloadColumn : DataTablesColumn
    {
        public DataTablesDownloadColumn(string field = "DownloadLink")
        {
            this.FieldName = field;
            this.Caption = string.Empty;

            this.Searchable = false;
            this.Sortable = false;
        }
    }
}