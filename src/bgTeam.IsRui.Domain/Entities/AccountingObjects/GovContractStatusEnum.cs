﻿using System.ComponentModel;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    public enum GovContractStatusEnum
    {
        /// <summary>
        ///   Исполнение
        /// </summary>
        [Description("Исполнение")]
        InProgress = 1,

        /// <summary>
        ///   Исполнение завершено
        /// </summary>
        [Description("Исполнение завершено")]
        Finished = 2,

        /// <summary>
        ///   Исполнение прекращено
        /// </summary>
        [Description("Исполнение прекращено")]
        Stopped = 3,

        /// <summary>
        ///   Реестровая запись аннулирована
        /// </summary>
        [Description("Реестровая запись аннулирована")]
        Annulled = 4,

        /// <summary>
        ///   Подготовка сведений
        /// </summary>
        [Description("Подготовка сведений")]
        Preparing = 5
    }
}
