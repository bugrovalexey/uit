﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class OKVEDMap : ClassMapHiLo<OKVED>
    {
        public OKVEDMap()
            : base("OKVED")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Code, "OKVED_Code").Nullable();
            Map(x => x.Name, "OKVED_Name").Nullable();
            Map(x => x.Salary, "OKVED_Salary").Not.Nullable();
        }
    }
}