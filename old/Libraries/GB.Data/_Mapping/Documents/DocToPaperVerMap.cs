﻿using GB.MKRF.Entities.Documents;

namespace GB.MKRF.Mapping.Documents
{
    class DocToPaperVerMap : ClassMapBase<DocToPaperVer>
    {
        public DocToPaperVerMap()
            : base("Doc_Papper_Ver")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Type, "Doc_Papper_Ver_Type").CustomType<DocToPaperType>().Not.Nullable();
            Map(x => x.IsExpertConclusion, "Doc_Papper_Ver_Is_Concl");

            References<DocumentVer>(x => x.Document, "Document_Ver_ID").Not.Nullable();
            References<PaperObjectVer>(x => x.PaperObject, "Plans_Paper_Ver_ID").Not.Nullable();
        }
    }
}