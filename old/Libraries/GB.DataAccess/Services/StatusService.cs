﻿using GB.Data;
using GB.Data.Workflow;
using GB.DataAccess.Services.Args;

namespace GB.DataAccess.Services
{
    public sealed class StatusService : ServiceBase<Status>, IService<Status, StatusArgs>
    {
        public Status Create(StatusArgs args)
        {
            var entity = new Status();
            return SaveOrUpdate(entity, args);
        }

        public Status Update(StatusArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private Status SaveOrUpdate(Status entity, StatusArgs args)
        {
            //TODO StatusService SaveOrUpdate
            //entity.Role = GetEntity<Role>(args.RoleId);
            //entity.From = GetEntity<Status>(args.FromId);
            //entity.To = GetEntity<Status>(args.ToId);

            base.SaveOrUpdate(entity);
            return entity;
        }

        public void Delete(StatusArgs args)
        {
            base.Delete(args.Id);
        }

        public void UpdateObj(IWorkflowObject obj)
        {
            NHibernateStaticMethods.Update(obj);
        }
    }
}