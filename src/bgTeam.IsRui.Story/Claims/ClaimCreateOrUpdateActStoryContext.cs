﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.IsRui.Domain.Dto;
    using System;

    public class ClaimCreateOrUpdateActStoryContext
    {
        public int? Id { get; set; }

        public int? ClaimId { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public DateTime? DateAdoption { get; set; }

        public decimal Cost { get; set; }

        public CreateDocumentFileData File { get; set; }
    }
}
