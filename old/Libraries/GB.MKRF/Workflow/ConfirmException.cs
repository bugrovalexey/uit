﻿using System;

namespace GB.MKRF.Workflow
{
    public class OperationConfirmException : Exception
    {
        public OperationConfirmException(string message)
            : base(message)
        {
        }
    }
}