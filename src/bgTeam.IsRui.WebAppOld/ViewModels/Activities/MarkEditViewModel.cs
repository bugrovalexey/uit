﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.WebApp.Helpers;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public class TableMark : NotificationBaseViewModel
    {
        public TableMark(PlansActivityMark entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            UnitName = entity.Unit.Return(unit => unit.Name, default(string));
            BaseValue = entity.BaseValue;
            TargetedValue = entity.TargetedValue;
            DateToAchieveGoalValueStr = entity.DateToAchieveGoalValue.Return(date => date.ToString("dd.MM.yyyy"), default(string));
            Justification = entity.Justification;
            Source = GetSourceName(entity.SourceIsStateProgram);
            SourceName = entity.SourceName;
            ExplanationRelationshipWithStateProgramMark = entity.ExplanationRelationshipWithStateProgramMark;
            StateProgramMarkName = entity.StateProgramMark.Return(mark => mark.Name, default(string));
        }

        private string GetSourceName(bool sourceIsStateProgram)
        {
            if (sourceIsStateProgram)
                return MarkEditViewModel.SourceTypeStateProgram;

            return MarkEditViewModel.SourceTypeOther;
        }

        public int Id { get; set; }

        [DisplayName(MarkEditViewModel.SourceCaption)]
        public string Source { get; set; }

        [DisplayName(MarkEditViewModel.SourceNameCaption)]
        public string SourceName { get; set; }

        [DisplayName(MarkEditViewModel.StateProgramMarkCaption)]
        public string StateProgramMarkName { get; set; }

        [DisplayName(MarkEditViewModel.ExplanationRelationshipWithStateProgramMarkCaption)]
        public string ExplanationRelationshipWithStateProgramMark { get; set; }

        [DisplayName(MarkEditViewModel.NameCaption)]
        public string Name { get; set; }

        [DisplayName(MarkEditViewModel.JustificationCaption)]
        public string Justification { get; set; }

        [DisplayName(MarkEditViewModel.BaseValueCaption)]
        public int? BaseValue { get; set; }

        [DisplayName(MarkEditViewModel.TargetedValueCaption)]
        public int? TargetedValue { get; set; }

        [DisplayName(MarkEditViewModel.UnitCaption)]
        public string UnitName { get; set; }

        [DisplayName(MarkEditViewModel.DateToAchieveGoalValueCaption)]
        public string DateToAchieveGoalValueStr { get; set; }

        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("DeleteMark", "Activities", new { id = Id }, "GridMarks"); }
        }

    }

    [MetadataType(typeof(MarkEditViewModel_Valid))]
    public class MarkEditViewModel : ActivitiesCardBase
    {
        #region Consts

        public const string NameCaption = "Наименование показателя";
        public const string JustificationCaption = "Обоснование";
        public const string BaseValueCaption = "Базовое значение";
        public const string TargetedValueCaption = "Целевое значение";
        public const string UnitCaption = "Единица измерения";
        public const string DateToAchieveGoalValueCaption = "Дата достижения целевого значения";
        public const string SourceCaption = "Источник формирования показателя";
        public const string SourceNameCaption = "Наименование источника формирования";
        public const string StateProgramMarkCaption = "Показатель госпрограммы, на достижение которого направлен показатель мероприятия";
        public const string ExplanationRelationshipWithStateProgramMarkCaption = "Пояснение связи с показателем госпрограммы";

        public const string SourceTypeStateProgram = "Госпрограмма";
        public const string SourceTypeOther = "Другой источник";

        #endregion Consts

        #region Ctors

        public MarkEditViewModel(IStatusRepository statusRepository, int activityId, int? id)
            : base(statusRepository, activityId)
        {
            UnitsList = new SelectList(
                new RepositoryDc<Units>().GetAll(), "Id", "Name");

            StateProgramList = new SelectList(
                new RepositoryDc<StateProgram>()
                    .GetAllEx(q => q.Where(x => x.Id == EntityBase.Default
                                                || (x.Parent == null || x.Parent.Id == 0))), //первый уровень
                "Id", "Name");

            StateProgramMarkList = new SelectList(CollectionHelper.GetEmptyForSelectList(), "Id", "Name");
            
            var entity = id.HasValue
                ? new BaseIndicatorRepository<PlansActivityMark>().Get(
                    q => q.Where(x => x.Id == id).Fetch(x => x.StateProgramMark).Eager.Clone())
                : null;
        
            entity.Do(Fill);
        }

        #endregion Ctors


        #region Methods

        protected void Fill(PlansActivityMark entity)
        {
            MarkId = entity.Id;
            Name = entity.Name;
            UnitId = entity.Unit.Return(unit => unit.Id, EntityBase.Default);
            BaseValue = entity.BaseValue;
            TargetedValue = entity.TargetedValue;
            DateToAchieveGoalValue = entity.DateToAchieveGoalValue.ToStringOrDefault();
            Justification = entity.Justification;
            SourceIsStateProgram = entity.SourceIsStateProgram;
            SourceName = entity.SourceName;
            ExplanationRelationshipWithStateProgramMark = entity.ExplanationRelationshipWithStateProgramMark;
            StateProgramMarkId = entity.StateProgramMark.Return(mark => (int?)mark.Id, default(int?));
            StateProgramId = entity.StateProgramMark.Return(mark => (int?) GetParentProgram(mark.StateProgram).Id, default(int?));

            StateProgramId.Do<int>(stPr =>
            {
                foreach (SelectListItem item in StateProgramList)
                {
                    if (Convert.ToInt32(item.Value) == StateProgramId)
                    {
                        item.Selected = true;
                    }
                }

                StateProgramMarkList = new SelectList(new StateProgramMarkRepository().GetAllMarksByStateProgram(StateProgramId.Value), "Id",
                    "Name",
                    StateProgramMarkId);
            });
        }

        protected StateProgram GetParentProgram(StateProgram program)
        {
            if (program.Parent == null)
                return program;

            return GetParentProgram(program.Parent);
        }

        #endregion Methods

        #region Props

        public int MarkId { get; set; }

        [DisplayName(SourceCaption)]
        public bool SourceIsStateProgram { get; set; }

        [DisplayName(SourceNameCaption)]
        public string SourceName { get; set; }

        [DisplayName(SourceTypeStateProgram)]
        public int? StateProgramId { get; set; }

        [DisplayName(StateProgramMarkCaption)]
        public int? StateProgramMarkId { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(ExplanationRelationshipWithStateProgramMarkCaption)]
        public string ExplanationRelationshipWithStateProgramMark { get; set; }

        [DisplayName(JustificationCaption)]
        public string Justification { get; set; }

        [DisplayName(BaseValueCaption)]
        public int? BaseValue { get; set; }

        [DisplayName(TargetedValueCaption)]
        public int? TargetedValue { get; set; }

        [DisplayName(UnitCaption)]
        public int UnitId { get; set; }

        [DisplayName(DateToAchieveGoalValueCaption)]
        public string DateToAchieveGoalValue { get; set; }

        public SelectList UnitsList { get; set; }

        public SelectList StateProgramList { get; set; }

        public SelectList StateProgramMarkList { get; set; }

        #endregion Props
    }

    public class MarkEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int? BaseValue { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = RangeIntText)]
        public int? TargetedValue { get; set; }
    }
}