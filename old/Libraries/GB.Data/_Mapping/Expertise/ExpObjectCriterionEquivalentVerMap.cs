﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.OtherExpObjects;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpObjectCriterionEquivalentVerMap : ClassMapBase<ExpObjectCriterionEquivalentVer>
	{
        public ExpObjectCriterionEquivalentVerMap() : base("Exp_Criterion_Equivalent_Other_Ver"){}

        protected override void InitMap()
        {
            Map(x => x.Comm, "Exp_Criterion_Equivalent_Other_Ver_Comm");

            References<ExpObjectConclActVer>(x => x.ExpConclActVer, "Exp_Concl_Act_Other_Ver_ID");
            References<ExpCriterion>(x => x.ExpCriterion, "Exp_Criterion_ID");
            References<ExpConformity>(x => x.ExpConformity, "Exp_Conformity_ID");            
        }
    }
}