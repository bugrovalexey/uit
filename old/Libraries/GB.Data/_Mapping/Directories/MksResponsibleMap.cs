﻿using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class MksResponsibleMap : ClassMapBase<MksResponsible>
    {
        public MksResponsibleMap()
            : base("Govt_Organ_Resp")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Govt_Organ_Resp_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            string sql = @" @Govt_Organ_ID = ?
            //							 ,@Users_ID = ?
            //							 ,@Govt_Organ_Resp_ID = ?";

            //            SqlInsert(@"exec ZZZ_Prc_Govt_Organ_Resp_Insert"+sql).Check.None();
            //            SqlUpdate(@"exec ZZZ_Prc_Govt_Organ_Resp_Update"+sql).Check.None();
            //            SqlDelete(@"exec ZZZ_Prc_Govt_Organ_Resp_Delete
            //						  @Govt_Organ_Resp_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            References<Department>(x => x.Department, "Govt_Organ_ID");
            References<User>(x => x.User, "Users_ID");
        }
    }
}