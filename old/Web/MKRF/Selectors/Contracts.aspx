﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="Contracts.aspx.cs" Inherits="GB.MKRF.Web.Selectors.Contracts" %>
<asp:Content ID="Head" ContentPlaceHolderID="Head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
     <%--<h1>Выберите госконтракт</h1>--%>
     <script type="text/javascript">
         function rowClick(s, e) {
             var id = s.GetRowKey(e.visibleIndex);
             s.GetRowValues(e.visibleIndex, 'Id;Number', function (values) {
                 var result = id + ';' + values[1];
                 
                 window.parent.PopupClose(result);
             });
         }
    </script>
    <dxwgv:ASPxGridView ID="SelectorsContractsGrid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" KeyFieldName="Id"
        OnBeforePerformDataSelect="SelectorsContractsGrid_BeforePerformDataSelect" Width="100%" OnHtmlRowCreated="SelectorsContractsGrid_HtmlRowCreated">
        <Columns>
            <dxwgv:GridViewDataTextColumn Caption="Номер реестровой записи" FieldName="Reg_Num" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Номер контракта" FieldName="Number" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataDateColumn Caption="Дата заключения" FieldName="SignDate" />
            <dxwgv:GridViewDataTextColumn Caption="Заказчик" FieldName="CustomerName" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Статус" FieldName="Status" Name="Status" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
            
            <dxwgv:GridViewDataMemoColumn Caption="Предмет контракта" FieldName="ProductName" Width="500px" Name="ProductName" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
                <PropertiesMemoEdit EncodeHtml="False">
                </PropertiesMemoEdit>
            </dxwgv:GridViewDataMemoColumn>
        </Columns>
        
        <Styles>
            <AlternatingRow Enabled="true" />
            <Header Wrap="True" />
        </Styles> 
        <ClientSideEvents RowClick="rowClick" />
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="15" />
    </dxwgv:ASPxGridView>
</asp:Content>
