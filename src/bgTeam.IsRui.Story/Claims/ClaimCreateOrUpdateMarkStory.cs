﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimCreateOrUpdateMarkStory : IStory<ClaimCreateOrUpdateMarkStoryContext, ClaimMarkDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapperBase;
        private readonly ISessionNHibernateFactory _factory;

        public ClaimCreateOrUpdateMarkStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase,
            ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _repository = repositoryRui;
            _mapperBase = mapperBase;
            _factory = sessionNHibernateFactory;
        }

        public ClaimMarkDto Execute(ClaimCreateOrUpdateMarkStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ClaimMarkDto> ExecuteAsync(ClaimCreateOrUpdateMarkStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка: {context.ClaimId}");
            }

            ClaimMark mark;

            if (context.Id.HasValue && context.Id.Value > 0)
            {
                mark = _repository.Get<ClaimMark>(context.Id.Value);
                mark = _mapperBase.Map(context, mark);
            }
            else
            {
                mark = _mapperBase.Map(context, new ClaimMark());
            }

            using (var session = _factory.OpenSession())
            {
                await session.SaveOrUpdateAsync(mark);
                await session.CommitAsync();
            }

            return _mapperBase.Map(mark, new ClaimMarkDto());
        }
    }
}
