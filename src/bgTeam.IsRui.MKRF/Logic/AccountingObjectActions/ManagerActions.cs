﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.MKRF.Logic.AccountingObjectActions
{
    public class ManagerActions : ManagerBase<IManagerActionsHandler, AccountingObjectsStageEnum>
    {
        protected ManagerActions() { }
    }
    //{
    //    private readonly object syncRoot = new object();
    //    private readonly Dictionary<AccountingObjectsStageEnum, IManagerActionsHandler> handlers = new Dictionary<AccountingObjectsStageEnum, IManagerActionsHandler>();

    //    public IManagerActionsHandler GetHandler(AccountingObjectsStageEnum stage)
    //    {
    //        IManagerActionsHandler handler = null;
    //        lock (syncRoot)
    //        {
    //            Dictionary<MasterEditTypeEnum, IMasterEditHandler> dict;

    //            handlers.TryGetValue(typeof(T), out dict);

    //            if (dict != null)
    //            {
    //                dict.TryGetValue(GetType(obj), out handler);
    //            }
    //        }
    //        return handler;
    //    }
    //}
}
