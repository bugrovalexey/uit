﻿namespace bgTeam.IsRui.Story.StoryMapping
{
    using AutoMapper;
    using bgTeam.Extensions;
    using bgTeam.IsRui.Domain._Mapping;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Directories;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Frgu;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Domain.Entities.Users;
    using bgTeam.IsRui.Domain.Entities.Workflow;
    using bgTeam.IsRui.Story.Account;
    using bgTeam.IsRui.Story.AccountingObjects;
    using bgTeam.IsRui.Story.Activities;
    using bgTeam.IsRui.Story.Admin;
    using bgTeam.IsRui.Story.Claims;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AutoMapperMap : IMapperBase
    {
        private readonly IMapper _mapper;

        public AutoMapperMap()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDto>()
                    .ForMember(m => m.DepartmentId, d => d.MapFrom(p => p.Department.Id))
                    .ForMember(m => m.DepartmentName, d => d.MapFrom(p => p.Department.Name));

                cfg.CreateMap<AccountingObject, AccountingObjectDto>()
                    .ForMember(m => m.TypeName, d => d.MapFrom(p => p.Type.Name))
                    .ForMember(m => m.KindName, d => d.MapFrom(p => p.Kind.Name))
                    .ForMember(m => m.StatusName, d => d.MapFrom(p => p.Status.Name))
                    .ForMember(m => m.Favorite, d => d.MapFrom(p => p.Favorites.Any()))
                    .ForMember(m => m.Mode, d => d.MapFrom(p => p.Mode.GetDescription()))
                    .ForMember(m => m.Stage, d => d.MapFrom(p => p.Stage.GetDescription()))
                    .ForMember(m => m.KindOfActivityName, d => d.MapFrom(p => p.ClassificationCategory.Name));

                cfg.CreateMap<AccountingObject, AccountingObjectInfoDto>()
                    .ForMember(m => m.ParentId, d => d.MapFrom(p => p.Parent.Id))
                    .ForMember(m => m.TypeId, d => d.MapFrom(p => p.Type.Id))
                    .ForMember(m => m.TypeName, d => d.MapFrom(p => p.Type.Name))
                    .ForMember(m => m.TypeCode, d => d.MapFrom(p => p.Type.Code))
                    .ForMember(m => m.KindId, d => d.MapFrom(p => p.Kind.Id))
                    .ForMember(m => m.KindName, d => d.MapFrom(p => p.Kind.Name))
                    .ForMember(m => m.KindCode, d => d.MapFrom(p => p.Kind.Code))
                    .ForMember(m => m.ArticleId, d => d.MapFrom(p => p.Article.Id))
                    .ForMember(m => m.ArticleName, d => d.MapFrom(p => p.Article.Name))
                    .ForMember(m => m.ExploitationDepartmentId, d => d.MapFrom(p => p.ExploitationDepartment.Id))
                    .ForMember(m => m.ExploitationDepartmentName, d => d.MapFrom(p => p.ExploitationDepartment.Name))
                    .ForMember(m => m.Grbs, d => d.MapFrom(p => p.BK_GRBS))
                    .ForMember(m => m.SectionKbkPzrz, d => d.MapFrom(p => p.BK_PZRZ))
                    .ForMember(m => m.ExpenseItemCsr, d => d.MapFrom(p => p.BK_CSR))
                    .ForMember(m => m.WorkFormWr, d => d.MapFrom(p => p.BK_WR))
                    .ForMember(m => m.ExpenditureItemKosgu, d => d.MapFrom(p => p.BK_KOSGU))
                    .ForMember(m => m.AddAoDepartment, d => d.MapFrom(p => p.Department))
                    //.ForMember(m => m.Activities, d => d.MapFrom(p => p.AoActivities.Where(x => x.Activity != null)))
                    .ForMember(m => m.KindOfActivityName, d => d.MapFrom(p => p.ClassificationCategory.Name));

                cfg.CreateMap<AccountingObject, AoCommonInfoDto>()
                   .ForMember(m => m.Name, d => d.MapFrom(p => p.ShortName))
                   .ForMember(m => m.KindId, d => d.MapFrom(p => p.Kind.Id))
                   .ForMember(m => m.ArticleId, d => d.MapFrom(p => p.Article.Id))
                   .ForMember(m => m.KindOfActivityId, d => d.MapFrom(p => p.ClassificationCategory.Id));

                cfg.CreateMap<AccountingObjectUserComment, ConclusionHistoryDto>()
                    .ForMember(m => m.Text, d => d.MapFrom(p => p.Comment))
                    .ForMember(m => m.Files, d => d.MapFrom(p => p.Documents))
                    .ForMember(m => m.Status, d => d.MapFrom(p => p.Status.Name))
                    .ForMember(m => m.Author, d => d.MapFrom(p => p.User.FullName))
                    .ForMember(m => m.StatusId, d => d.MapFrom(p => p.Status.Id))
                    .ForMember(m => m.AuthorId, d => d.MapFrom(p => p.User.Id));

                cfg.CreateMap<AccountingObjectUserComment, AccountingObjectUserCommentDto>()
                    .ForMember(m => m.StatusId, d => d.MapFrom(p => p.Status.Id))
                    .ForMember(m => m.StatusName, d => d.MapFrom(p => p.Status.Name))
                    .ForMember(m => m.UserId, d => d.MapFrom(p => p.User.Id))
                    .ForMember(m => m.UserName, d => d.MapFrom(p => p.User.Name))
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id));

                //cfg.CreateMap<CreateDocumentFile, FilesDto>()
                //    .ForMember(m => m.OwnerId, d => d.MapFrom(p => p.Owner.Id));

                cfg.CreateMap<Department, DepartmentDto>();
                cfg.CreateMap<Responsible, ResponsibleDto>();

                cfg.CreateMap<Status, StatusDto>()
                    .ForMember(m => m.StatusGroup, d => d.MapFrom(p => p.StatusGroup.Name));

                cfg.CreateMap<GRBS, GRBSDto>();
                cfg.CreateMap<SectionKBK, SectionKBKDto>();
                cfg.CreateMap<ExpenseItem, ExpenseItemDto>();
                cfg.CreateMap<WorkForm, WorkFormDto>();
                cfg.CreateMap<ExpenditureItem, ExpenditureItemDto>();
                cfg.CreateMap<AOGovService, AOGovServiceDto>();
                cfg.CreateMap<AOGovServiceNPA, AOGovServiceNpaDto>();
                cfg.CreateMap<GovService, GovServiceDto>();

                cfg.CreateMap<AccountingObjectCharacteristics, AccountingObjectCharacteristicsDto>();
                cfg.CreateMap<AccountingObjectCharacteristicsType, AccountingObjectCharacteristicsTypeDto>();
                cfg.CreateMap<Units, UnitsDto>();
                cfg.CreateMap<CharacteristicOfLicense, CharacteristicOfLicenseDto>();
                cfg.CreateMap<AoSoftwareAddOrUpdateSoftwareStoryContext, CharacteristicOfLicense>()
                    .ForMember(m => m.Id, d => d.MapFrom(p => p.CharacteristicOfLicenseId))
                    .ForMember(m => m.Name, d => d.MapFrom(p => p.CharacteristicOfLicenseName));
                cfg.CreateMap<CategoryKindOfSupport, CategoryKindOfSupportDto>();
                cfg.CreateMap<Software, SoftwareDto>()
                    .ForMember(m => m.ManufacturerId, d => d.MapFrom(p => p.Manufacturer.Id))
                    .ForMember(m => m.ManufacturerName, d => d.MapFrom(p => p.Manufacturer.Name));
                cfg.CreateMap<OKVED, OkvedDto>();
                cfg.CreateMap<WorkAndService, WorkAndServiceDto>()
                    .ForMember(m => m.CategoryKindOfSupportId, d => d.MapFrom(p => p.CategoryKindOfSupport.Id))
                    .ForMember(m => m.CategoryKindOfSupportName, d => d.MapFrom(p => p.CategoryKindOfSupport.Name));
                cfg.CreateMap<InternalIsAndComponentsItki, InternalIsAndComponentsItkiDto>()
                    .ForMember(m => m.Name, d => d.MapFrom(p => p.Object.ShortName))
                    .ForMember(m => m.KindName, d => d.MapFrom(p => p.Object.Kind.Name))
                    .ForMember(m => m.DepartmentName, d => d.MapFrom(p => p.Object.ResponsibleDepartment.Name));
                cfg.CreateMap<ExternalIsAndComponentsItki, ExternalIsAndComponentsItkiDto>();
                cfg.CreateMap<ExternalUserInterfaceOfIs, ExternalUserInterfaceOfIsDto>()
                    .ForMember(m => m.TypeOfInterfaceId, d => d.MapFrom(p => p.TypeOfInterface.Id))
                    .ForMember(m => m.TypeOfInterfaceName, d => d.MapFrom(p => p.TypeOfInterface.Name));
                cfg.CreateMap<GovPurchase, GovPurchaseDto>();
                cfg.CreateMap<GovContract, GovContractDto>();
                cfg.CreateMap<ActOfAcceptance, ActOfAcceptanceDto>()
                .ForMember(m => m.GovContractId, d => d.MapFrom(p => p.GovContract.Id))
                .ForMember(m => m.GovContractNumber, d => d.MapFrom(p => p.GovContract.Number));

                cfg.CreateMap<AODecisionToCreate, AODecisionToCreateDto>()
                    .ForMember(m => m.File, d => d.MapFrom(p => p.Documents.FirstOrDefault()))
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id));

                cfg.CreateMap<Files, FilesDto>()
                    .ForMember(m => m.FileName, d => d.MapFrom(p => p.Name));

                cfg.CreateMap<AoDocumentBaseCreateOrUpdateDocumentStoryContext, AODecisionToCreate>()
                    .ForMember(m => m.AccountingObject, d => d.MapFrom(p => new AccountingObject() { Id = p.AoId }));

                cfg.CreateMap<AODecisionToCreate, AoDocumentBaseCreateOrUpdateDocumentStoryContext>()
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id));

                cfg.CreateMap<AoDocumentBaseCreateOrUpdateDocumentStoryContext, CreateFileAnswerDto>();

                cfg.CreateMap<AoCommonInfoUpdateStoryContext, AccountingObject>()
                    .ForMember(x => x.Id, opt => opt.Ignore())
                    .ForMember(m => m.ShortName, d => d.MapFrom(p => p.Name))
                    .ForMember(m => m.Kind, d => d.MapFrom(p => new IKTComponent() { Id = p.KindId.Value }))
                    .ForMember(m => m.Article, d => d.MapFrom(p => new AoArticle() { Id = p.ArticleId.Value }));

                cfg.CreateMap<TechnicalSupport, TechnicalSupportDto>()
                    .ForMember(m => m.AccountingObject, d => d.MapFrom(p => p.AccountingObject.Id));

                cfg.CreateMap<AoActivity, AoActivityDto>()
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id))
                    .ForMember(m => m.PlansActivity, d => d.MapFrom(p => p.Activity.Id));

                cfg.CreateMap<AoDeleteFile, AoDeleteFileDto>();

                cfg.CreateMap<AoGoalsAndPurposeUpdateInfoStoryContext, AODecisionToCreate>()
                    .ForMember(m => m.AccountingObject, d => d.MapFrom(p => new AccountingObject() { Id = p.AoId, PurposeAndScope = p.PurposeAndScope, Targets = p.Targets }));

                cfg.CreateMap<AoDocumentBaseCreateOrUpdateDocumentStoryContext, AccountingObjectUserComment>();

                cfg.CreateMap<IKTComponent, KindDto>()
                    .ForMember(m => m.KindId, d => d.MapFrom(p => p.Id));

                cfg.CreateMap<AccountingObjectCharacteristics, AoAllCharacteristicsDto>()
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id))
                    .ForMember(m => m.TypeId, d => d.MapFrom(p => p.Type.Id))
                    .ForMember(m => m.UnitId, d => d.MapFrom(p => p.Unit.Id));

                cfg.CreateMap<AoCharacteristicsAddOrUpdateCharacteristicStoryContext, AccountingObjectCharacteristics>();

                cfg.CreateMap<AccountingObjectCharacteristics, CharacteristicsAddOrUpdateDto>()
                     .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id))
                     .ForMember(m => m.TypeId, d => d.MapFrom(p => p.Type.Id))
                     .ForMember(m => m.UnitId, d => d.MapFrom(p => p.Unit.Id));

                cfg.CreateMap<Software, AllSoftwareDto>()
                    .ForMember(m => m.CategoryKindOfSupportId, d => d.MapFrom(p => p.CategoryKindOfSupport.Id))
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id))
                    .ForMember(m => m.ManufacturerId, d => d.MapFrom(p => p.Manufacturer.Id))
                    .ForMember(m => m.CategoryKindOfSupportId, d => d.MapFrom(p => p.CategoryKindOfSupport.Id))
                    .ForMember(m => m.CharacteristicOfLicenseId, d => d.MapFrom(p => p.CharacteristicsOfLicense.Id))
                    .ForMember(m => m.CharacteristicOfLicenseName, d => d.MapFrom(p => p.CharacteristicsOfLicense.Name))
                    .ForMember(m => m.CharacteristicOfLicenseModel, d => d.MapFrom(p => p.CharacteristicsOfLicense.Model))
                    .ForMember(m => m.CharacteristicOfLicenseTypeOfConnectionToServer, d => d.MapFrom(p => p.CharacteristicsOfLicense.TypeOfConnectionToServer))
                    .ForMember(m => m.CharacteristicOfLicenseNumberOnlineSameTime, d => d.MapFrom(p => p.CharacteristicsOfLicense.NumberOnlineSameTime))
                    .ForMember(m => m.CharacteristicOfLicenseProductNumber, d => d.MapFrom(p => p.CharacteristicsOfLicense.ProductNumber))
                    .ForMember(m => m.CharacteristicOfLicenseNumberAllocatedSameTime, d => d.MapFrom(p => p.CharacteristicsOfLicense.NumberAllocatedSameTime))
                    .ForMember(m => m.CharacteristicOfLicenseLicensor, d => d.MapFrom(p => p.CharacteristicsOfLicense.Licensor));

                cfg.CreateMap<AoSoftwareAddOrUpdateSoftwareStoryContext, Software>();

                cfg.CreateMap<AoSoftwareAddOrUpdateSoftwareStoryContext, CharacteristicOfLicense>()
                     .ForMember(m => m.Id, d => d.MapFrom(p => p.CharacteristicOfLicenseId))
                     .ForMember(m => m.Name, d => d.MapFrom(p => p.CharacteristicOfLicenseName))
                     .ForMember(m => m.Model, d => d.MapFrom(p => p.CharacteristicOfLicenseModel))
                     .ForMember(m => m.TypeOfConnectionToServer, d => d.MapFrom(p => p.CharacteristicOfLicenseTypeOfConnectionToServer))
                     .ForMember(m => m.NumberOnlineSameTime, d => d.MapFrom(p => p.CharacteristicOfLicenseNumberOnlineSameTime))
                     .ForMember(m => m.ProductNumber, d => d.MapFrom(p => p.CharacteristicOfLicenseProductNumber))
                     .ForMember(m => m.NumberAllocatedSameTime, d => d.MapFrom(p => p.CharacteristicOfLicenseNumberAllocatedSameTime))
                     .ForMember(m => m.Licensor, d => d.MapFrom(p => p.CharacteristicOfLicenseLicensor)); 

                cfg.CreateMap<Software, AddOrUpdateSoftwareDto>()
                     .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id))
                     .ForMember(m => m.ManufacturerId, d => d.MapFrom(p => p.Manufacturer.Id))
                     .ForMember(m => m.CategoryKindOfSupportId, d => d.MapFrom(p => p.CategoryKindOfSupport.Id))
                     .ForMember(m => m.CharacteristicOfLicenseId, d => d.MapFrom(p => p.CharacteristicsOfLicense.Id))
                     .ForMember(m => m.CharacteristicOfLicenseName, d => d.MapFrom(p => p.CharacteristicsOfLicense.Name))
                     .ForMember(m => m.CharacteristicOfLicenseModel, d => d.MapFrom(p => p.CharacteristicsOfLicense.Model))
                     .ForMember(m => m.CharacteristicOfLicenseTypeOfConnectionToServer, d => d.MapFrom(p => p.CharacteristicsOfLicense.TypeOfConnectionToServer))
                     .ForMember(m => m.CharacteristicOfLicenseNumberOnlineSameTime, d => d.MapFrom(p => p.CharacteristicsOfLicense.NumberOnlineSameTime))
                     .ForMember(m => m.CharacteristicOfLicenseProductNumber, d => d.MapFrom(p => p.CharacteristicsOfLicense.ProductNumber))
                     .ForMember(m => m.CharacteristicOfLicenseNumberAllocatedSameTime, d => d.MapFrom(p => p.CharacteristicsOfLicense.NumberAllocatedSameTime))
                     .ForMember(m => m.CharacteristicOfLicenseLicensor, d => d.MapFrom(p => p.CharacteristicsOfLicense.Licensor));

                cfg.CreateMap<AoActivity, ActivityDto>()
                    .ForMember(m => m.StatusName, d => d.MapFrom(m => m.Stage.GetDescription()))
                    .ForMember(m => m.TypeName, d => d.MapFrom(m => m.ActivityType.GetDescription()));

                cfg.CreateMap<TechnicalSupport, AllTechnicalSupportDto>()
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id))
                    .ForMember(m => m.ManufacturerId, d => d.MapFrom(p => p.Manufacturer.Id))
                    .ForMember(m => m.ManufacturerCustomName, d => d.MapFrom(p => p.Manufacturer.Name));

                cfg.CreateMap<AoTechnicalSupportAddOrUpdateTechSupportStoryContext, TechnicalSupport>();

                cfg.CreateMap<TechnicalSupport, TechnicalSupportAddOrUpdateDto>()
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id));

                cfg.CreateMap<AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStoryContext, CharacteristicOfTechnicalSupport>();
    
                cfg.CreateMap<CharacteristicOfTechnicalSupport, TechnicalSupportAddOrUpdateTechSupportCharacteristicDto>()
                    .ForMember(m => m.UnitId, d => d.MapFrom(p => p.Unit.Id));

                cfg.CreateMap<CharacteristicOfTechnicalSupport, CharacteristicOfTechnicalSupportGetDto>()
                    .ForMember(m => m.UnitId, d => d.MapFrom(p => p.Unit.Id));

                cfg.CreateMap<WorkAndService, WorkAndServiceGetAllWorkDto>()
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id));

                cfg.CreateMap<AoWorkAndServiceAddOrUpdateWorkStoryContext, WorkAndService>();

                cfg.CreateMap<WorkAndService, WorkAndServiceAddOrUpdateWorkDto>()
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id))
                    .ForMember(m => m.OkvedId, d => d.MapFrom(p => p.Okved.Id));

                cfg.CreateMap<AoFinancingUpdateInfoStoryContext, AccountingObject>()
                    .ForMember(m => m.FinancingQuarter1, d => d.MapFrom(p => p.quarter1))
                    .ForMember(m => m.FinancingQuarter2, d => d.MapFrom(p => p.quarter2))
                    .ForMember(m => m.FinancingQuarter3, d => d.MapFrom(p => p.quarter3))
                    .ForMember(m => m.FinancingQuarter4, d => d.MapFrom(p => p.quarter4));

                cfg.CreateMap<AccountingObject, FinancingDto>()
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.Id))
                    .ForMember(m => m.quarter1, d => d.MapFrom(p => p.FinancingQuarter1))
                    .ForMember(m => m.quarter2, d => d.MapFrom(p => p.FinancingQuarter2))
                    .ForMember(m => m.quarter3, d => d.MapFrom(p => p.FinancingQuarter3))
                    .ForMember(m => m.quarter4, d => d.MapFrom(p => p.FinancingQuarter4));

                cfg.CreateMap<PlansActivity, ActivityDto>()
                    .ForMember(m => m.StatusName, d => d.MapFrom(m => m.Workflow.Status.Name))
                    .ForMember(m => m.TypeName, d => d.MapFrom(m => m.ActivityType2.Name));

                cfg.CreateMap<AoContractAddOrUpdateStoryContext, AccountingObjectContract>();

                cfg.CreateMap<AccountingObjectContract, ContractDto>()
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id));

                cfg.CreateMap<EntityDirectories, DictionaryDto>();

                cfg.CreateMap<AoActsAddOrUpdateActStoryContext, AccountingObjectActs>()
                    .ForMember(m => m.FileId, d => d.MapFrom(p => p.File.Id))
                    .ForMember(m => m.FileName, d => d.MapFrom(p => p.File.FileName));

                cfg.CreateMap<AccountingObjectActs, ActsDto>()
                    .ForMember(m => m.AoId, d => d.MapFrom(p => p.AccountingObject.Id))
                    .ForMember(m => m.File, d => d.MapFrom(p => new ActsFileDto() { Id = p.FileId, FileName = p.FileName }));

                cfg.CreateMap<PlansActivity, ActivityCostDto>()
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.Id))
                    .ForMember(m => m.BudgetItemId, d => d.MapFrom(p => p.BudgetItem.Id))
                    .ForMember(m => m.BudgetItemName, d => d.MapFrom(p => p.BudgetItem.Name));
                    //.ForMember(m => m.VolY2, d => d.MapFrom(p => p.PlansActivityVolY2));

                cfg.CreateMap<CostsUpdateCostsStoryContext, PlansActivity>();
                    //.ForMember(m => m.PlansActivityVolY0, d => d.MapFrom(p => p.VolY0))
                    //.ForMember(m => m.PlansActivityVolY1, d => d.MapFrom(p => p.VolY1))
                    //.ForMember(m => m.PlansActivityVolY2, d => d.MapFrom(p => p.VolY2));

                cfg.CreateMap<Files, ActivityDocumentDto>();

                cfg.CreateMap<DocumentsCreateOrUpdateDocumentStoryContext, ActivityDocuments>();

                cfg.CreateMap<ActivityDocuments, ActivityDocumentDto>()
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.Activity.Id))
                    .ForMember(m => m.file, d => d.MapFrom(p => new CreateDocumentFileData() { Id = p.FileId, FileName = p.FileName }));

                cfg.CreateMap<ServicesCreateOrUpdateServiceStoryContext, PlansActivityService>();

                cfg.CreateMap<PlansActivityService, ActivitiesServiceDto>()
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.Activity.Id))
                    .ForMember(m => m.KOSGUId, d => d.MapFrom(p => p.KOSGU.Id))
                    .ForMember(m => m.KOSGUName, d => d.MapFrom(p => p.KOSGU.Name));

                cfg.CreateMap<MarksCreateOrUpdateMarkStoryContext, PlansActivityMark>();

                cfg.CreateMap<PlansActivityMark, ActivitysMarksDto>()
                    .ForMember(m => m.UnitId, d => d.MapFrom(p => p.Unit.Id))
                    .ForMember(m => m.UnitName, d => d.MapFrom(p => p.Unit.Name))
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.PlansActivity.Id));

                cfg.CreateMap<IndicatorsCreateOrUpdateIndicatorStoryContext, PlansActivityIndicator>();

                cfg.CreateMap<PlansActivityIndicator, ActivityIndicatorDto>()
                    .ForMember(m => m.UnitId, d => d.MapFrom(p => p.Unit.Id))
                    .ForMember(m => m.UnitName, d => d.MapFrom(p => p.Unit.Name))
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.PlansActivity.Id));

                cfg.CreateMap<WorksCreateOrUpdateWorkStoryContext, ActivityWorks>();

                cfg.CreateMap<ActivityWorks, ActivityWorksDto>()
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.PlansActivityId))
                    /*.ForMember(m => m.ExpenseDirectionId, d => d.MapFrom(p => p.ExpenseDirection.Id))
                    .ForMember(m => m.ExpenseTypeId, d => d.MapFrom(p => p.ExpenseType.Id))
                    .ForMember(m => m.KOSGUId, d => d.MapFrom(p => p.KOSGU.Id))
                    .ForMember(m => m.OKPDId, d => d.MapFrom(p => p.OKPD.Id))
                    .ForMember(m => m.OKVEDId, d => d.MapFrom(p => p.OKVED.Id))*/;

                cfg.CreateMap<ActivityWorks, ActivityWorksGetDto>()
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.PlansActivityId))
                    /*.ForMember(m => m.ExpenseDirectionId, d => d.MapFrom(p => p.ExpenseDirection.Id))
                    .ForMember(m => m.ExpenseTypeId, d => d.MapFrom(p => p.ExpenseType.Id))
                    .ForMember(m => m.KOSGUId, d => d.MapFrom(p => p.KOSGU.Id))
                    .ForMember(m => m.OKPDId, d => d.MapFrom(p => p.OKPD.Id))
                    .ForMember(m => m.OKVEDId, d => d.MapFrom(p => p.OKVED.Id))*/;

                cfg.CreateMap<CommonInfoUpdateInfoStoryContext, PlansActivity>();

                cfg.CreateMap<PlansActivity, ActivityCommonInfoDto>()
                   .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.Id))
                   .ForMember(m => m.ResponsibleDepartmentId, d => d.MapFrom(p => p.Department.Id))
                   .ForMember(m => m.ResponsibleDepartmentName, d => d.MapFrom(p => p.Department.Name))
                   .ForMember(m => m.SigningId, d => d.MapFrom(p => p.Signing.Id));

                cfg.CreateMap<ActivityGoods, ActivityGoodsDto>()
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.PlansActivityId));

                cfg.CreateMap<ActivityGoods, ActivityGoodsGetDto>()
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.PlansActivityId));

                cfg.CreateMap<WorkflowUserConclusionSaveOrUpdateStoryContext, WorkflowUserConclusionDto>();
                   //.ForMember(m => m.ActivityId, d => d.MapFrom(p => p.PlansActivity.Id));

                cfg.CreateMap<GoodsCreateOrUpdateGoodsStoryContext, ActivityGoods>();

                cfg.CreateMap<ActivityGoods, ActivityGoodsDto>()
                   .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.PlansActivityId));

                cfg.CreateMap<GoodsAddOrUpdateGoodsCharacteristicStoryContext, GoodsCharacteristic>();

                cfg.CreateMap<GoodsCharacteristic, GoodsCharacteristicDto>()
                    .ForMember(m => m.UnitId, d => d.MapFrom(p => p.Unit.Id));

                cfg.CreateMap<WorkflowUserConclusion, WorkflowUserConclusionHistory>()
                    .ForMember(m => m.Id_Ref, d => d.MapFrom(p => p.Id));

                cfg.CreateMap<WorkflowUserConclusionHistory, ConclusionHistoryDto>()
                    .ForMember(m => m.AuthorId, d => d.MapFrom(p => p.User.Id))
                    .ForMember(m => m.StatusId, d => d.MapFrom(p => p.Status.Id))
                    .ForMember(m => m.Author, d => d.MapFrom(p => p.User.FullName))
                    .ForMember(m => m.Status, d => d.MapFrom(p => p.Status.Name))
                    .ForMember(m => m.Files, d => d.MapFrom(p => p.Documents));

                cfg.CreateMap<UserAddOrUpdateUserStoryContext, UsersEntity>();

                cfg.CreateMap<UsersEntity, UsersDto>()
                    .ForMember(m => m.GovtOrganId, d => d.MapFrom(p => p.GovtOrgan.Id));

                cfg.CreateMap<Role, RoleDto>();

                cfg.CreateMap<DepartmentProc, DepartmentDtoProc>()
                    .ForMember(m => m.Id, d => d.MapFrom(p => p.Govt_Organ_ID))
                    .ForMember(m => m.Code, d => d.MapFrom(p => p.Govt_Organ_Code))
                    .ForMember(m => m.Name, d => d.MapFrom(p => p.Govt_Organ_Name))
                    .ForMember(m => m.NameSmall, d => d.MapFrom(p => p.Govt_Organ_SName))
                    .ForMember(m => m.Addr, d => d.MapFrom(p => p.Govt_Organ_Addr))
                    .ForMember(m => m.Phone, d => d.MapFrom(p => p.Govt_Organ_Phone))
                    .ForMember(m => m.Web, d => d.MapFrom(p => p.Govt_Organ_Web))
                    .ForMember(m => m.IsActive, d => d.MapFrom(p => p.Govt_Organ_Is_Active))
                    .ForMember(m => m.SPZ, d => d.MapFrom(p => p.Govt_Organ_SPZ))
                    .ForMember(m => m.ParentId, d => d.MapFrom(p => p.Govt_Organ_Par_ID));

                cfg.CreateMap<UpdateInfoStoryContext, User>();

                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<FileAddStoryContext, Files>()
                    .ForMember(m => m.Extension, d => d.MapFrom(p => Path.GetExtension(p.Name)))
                    .ForMember(m => m.EntityOwner, d => d.MapFrom(p => p.Entity));

                cfg.CreateMap<ConclusionAddFileStoryContext, FileAddStoryContext>()
                    .ForMember(m => m.Name, d => d.MapFrom(p => p.FileName));

                cfg.CreateMap<AoConclusionFileAddStoryContext, FileAddStoryContext>()
                    .ForMember(m => m.Name, d => d.MapFrom(p => p.FileName));

                cfg.CreateMap<CreateDocumentFileData, FileAddStoryContext>()
                    .ForMember(m => m.Name, d => d.MapFrom(p => p.FileName));

                cfg.CreateMap<Claim, ClaimDto>()
                    .ForMember(m => m.AccountingObjectName, d => d.MapFrom(p => p.AccountingObject.ShortName))
                    .ForMember(m => m.StatusName, d => d.MapFrom(p => p.Workflow.Status.Name))
                    .ForMember(m => m.ActivityName, d => d.MapFrom(p => p.PlansActivity.Name));

                cfg.CreateMap<ClaimIndicator, ClaimIndicatorDto>()
                    .ForMember(m => m.UnitId, d => d.MapFrom(p => p.Unit.Id))
                    .ForMember(m => m.UnitName, d => d.MapFrom(p => p.Unit.Name))
                    .ForMember(m => m.ClaimId, d => d.MapFrom(p => p.Claim.Id));

                cfg.CreateMap<ClaimMark, ClaimMarkDto>()
                    .ForMember(m => m.UnitId, d => d.MapFrom(p => p.Unit.Id))
                    .ForMember(m => m.UnitName, d => d.MapFrom(p => p.Unit.Name))
                    .ForMember(m => m.ClaimId, d => d.MapFrom(p => p.Claim.Id));

                cfg.CreateMap<Claim, ClaimInfoDto>()
                    .ForMember(m => m.ClaimNumber, d => d.MapFrom(p => p.Number))
                    .ForMember(m => m.AccountingObjectId, d => d.MapFrom(p => p.AccountingObject.Id))
                    .ForMember(m => m.AccountingObjectName, d => d.MapFrom(p => p.AccountingObject.LocationOrgName))
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.PlansActivity.Id))
                    .ForMember(m => m.ActivityName, d => d.MapFrom(p => p.PlansActivity.Name))
                    .ForMember(m => m.StatusId, d => d.MapFrom(p => p.Workflow.Status.Id))
                    .ForMember(m => m.StatusName, d => d.MapFrom(p => p.Workflow.Status.Name))
                    .ForMember(m => m.WorkflowId, d => d.MapFrom(p => p.Workflow.Id));

                cfg.CreateMap<ClaimAct, ClaimActDto>()
                    .ForMember(m => m.ClaimId, d => d.MapFrom(p => p.Claim.Id))
                    .ForMember(m => m.ClaimName, d => d.MapFrom(p => p.Claim.Name))
                    .ForMember(m => m.Number, d => d.MapFrom(p => p.DocNumber))
                    .ForMember(m => m.Name, d => d.MapFrom(p => p.DocName));

                cfg.CreateMap<ClaimConclusion, ClaimConclusionDto>()
                    .ForMember(m => m.ClaimId, d => d.MapFrom(p => p.Claim.Id))
                    .ForMember(m => m.ClaimName, d => d.MapFrom(p => p.Claim.Name));

                cfg.CreateMap<ClaimDocument, ClaimDocumentDto>()
                    .ForMember(m => m.ClaimId, d => d.MapFrom(p => p.Claim.Id));

                cfg.CreateMap<ClaimScopeOfWork, ClaimScopeOfWorkDto>()
                    .ForMember(m => m.ClaimId, d => d.MapFrom(p => p.Claim.Id))
                    .ForMember(m => m.SpecialistId, d => d.Condition(p => p.Specialist != null))
                    .ForMember(m => m.SpecialistId, d => d.MapFrom(p => p.Specialist.Id))
                    .ForMember(m => m.SpecialistName, d => d.Condition(p => p.Specialist != null))
                    .ForMember(m => m.SpecialistName, d => d.MapFrom(p => p.Specialist.Name));

                cfg.CreateMap<Claim, ClaimContentDto>();

                cfg.CreateMap<Claim, ClaimJustificationDto>()
                    .ForMember(m => m.ClaimId, d => d.MapFrom(p => p.Id));

                cfg.CreateMap<ClaimCreateOrUpdateIndicatorStoryContext, ClaimIndicator>()
                    .ForMember(m => m.Unit, d => d.Condition(p => p.UnitId.HasValue))
                    .ForMember(m => m.Unit, d => d.MapFrom(p => new Units() { Id = p.UnitId ?? 0 }))
                    .ForMember(m => m.Claim, d => d.Condition(p => p.ClaimId.HasValue))
                    .ForMember(m => m.Claim, d => d.MapFrom(p => new Claim() { Id = p.ClaimId ?? 0 }));

                cfg.CreateMap<ClaimCreateOrUpdateMarkStoryContext, ClaimMark>()
                    .ForMember(m => m.Unit, d => d.Condition(p => p.UnitId.HasValue))
                    .ForMember(m => m.Unit, d => d.MapFrom(p => new Units() { Id = p.UnitId ?? 0 }))
                    .ForMember(m => m.Claim, d => d.Condition(p => p.ClaimId.HasValue))
                    .ForMember(m => m.Claim, d => d.MapFrom(p => new Claim() { Id = p.ClaimId ?? 0 }))
                    .ForMember(m => m.StateProgramMark, d => d.Condition(p => p.StateProgramMarkId.HasValue))
                    .ForMember(m => m.StateProgramMark, d => d.MapFrom(p => new StateProgramMark() { Id = p.StateProgramMarkId ?? 0 }));

                cfg.CreateMap<ClaimCreateOrUpdateDocumentStoryContext, ClaimDocument>()
                    .ForMember(m => m.Claim, d => d.MapFrom(p => new Claim() { Id = p.ClaimId.Value }));

                cfg.CreateMap<CreateDocumentFileData, Files>()
                    .ForMember(m => m.Name, d => d.MapFrom(p => p.FileName));

                cfg.CreateMap<ClaimCreateOrUpdateActStoryContext, ClaimAct>()
                    .ForMember(m => m.Claim, d => d.MapFrom(p => new Claim() { Id = p.ClaimId.Value }))
                    .ForMember(m => m.DocNumber, d => d.MapFrom(p => p.Number))
                    .ForMember(m => m.DocName, d => d.MapFrom(p => p.Name));

                cfg.CreateMap<ClaimCreateOrUpdateScopeOfWorkStoryContext, ClaimScopeOfWork>()
                    .ForMember(m => m.Claim, d => d.Condition(p => p.ClaimId.HasValue))
                    .ForMember(m => m.Claim, d => d.MapFrom(p => new Claim() { Id = p.ClaimId.Value }))
                    .ForMember(m => m.Specialist, d => d.MapFrom(p => new User { Id = p.SpecialistId }));

                cfg.CreateMap<ClaimUserComment, ClaimUserCommentDto>()
                    .ForMember(m => m.ClaimId, d => d.MapFrom(p => p.Claim.Id));

                cfg.CreateMap<ClaimAddFileUserCommentStoryContext, FileAddStoryContext>()
                    .ForMember(m => m.Name, d => d.MapFrom(p => p.FileName));

                cfg.CreateMap<InformationInteraction, InformationInteractionDto>()
                    .ForMember(m => m.AccountingObjectId, d => d.MapFrom(p => p.AccountingObject.Id))
                    .ForMember(m => m.AccountingObjectName, d => d.MapFrom(p => p.AccountingObject.ShortName))
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.PlansActivity.Id))
                    .ForMember(m => m.TypeId, d => d.MapFrom(p => p.Type.Id))
                    .ForMember(m => m.TypeName, d => d.MapFrom(p => p.Type.Name));

                cfg.CreateMap<ClassificationCategory, TreeDictionaryDto>()
                    .ForMember(m => m.Parent, d => d.MapFrom(p => p.Parent))
                    .ForMember(m => m.Childrens, d => d.MapFrom(p => p.Childs));

                cfg.CreateMap<ActivityCreateOrUpdateActStoryContext, PlansActivityAct>()
                    .ForMember(m => m.FileId, d => d.MapFrom(p => p.File.Id))
                    .ForMember(m => m.FileName, d => d.MapFrom(p => p.File.FileName));

                cfg.CreateMap<PlansActivityAct, ActivityActDto>()
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.Activity.Id));

                cfg.CreateMap<Files, ActsFileDto>()
                    .ForMember(m => m.FileName, d => d.MapFrom(p => p.Name));

                cfg.CreateMap<ActivityAddOrUpdateContractStoryContext, ActivityContract>();

                cfg.CreateMap<ActivityContract, ActivityContractDto>()
                    .ForMember(m => m.Activity, d => d.MapFrom(p => p.PlansActivity));

                cfg.CreateMap<ActivityAddOrUpdatePurchaseStoryContext, ActivityPurchase>();

                cfg.CreateMap<ActivityPurchase, ActivityPurchaseDto>()
                    .ForMember(m => m.ActivityId, d => d.MapFrom(p => p.Activity.Id));
            });
            _mapper = config.CreateMapper();
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            return _mapper.Map(source, destination);
        }
    }
}
