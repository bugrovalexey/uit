﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Configuration;
using GB;
using GB.Helpers;
using GB.Infrastructure;

namespace GB.MKRF.Web
{
	public class Global : System.Web.HttpApplication
	{
		protected void Application_Start(object sender, EventArgs e)
		{
            new NHSession("Coord");

            Logger.Info("Start Coord");

#if CACHING
            Logger.Info("Кэширование включено");
#endif

            FileHelper.Init();
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            var application = (HttpApplication)sender;
            var request = application.Context.Request;
            NHSession.SetClientComputerName(request.ServerVariables["REMOTE_HOST"]);
        }

		protected void Application_EndRequest(object sender, EventArgs e)
		{
			NHSession.EndRequest();

            //if (IsSetCookiePath)
            //{
            //    var app = (HttpApplication) sender;

            //    var cookiePath = app.Request.ApplicationPath;
            //    if (cookiePath.Length > 1) 
            //        cookiePath += '/';

            //    foreach (string name in app.Response.Cookies.AllKeys)
            //    {
            //        var cookie = app.Response.Cookies[name];
            //        cookie.Path = cookiePath;
            //    }
            //}
		}

        public static bool IsSetCookiePath
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings["SetCookiePath"] ?? "false"); }
        }

		protected void Application_Error(object sender, EventArgs e)
		{
//#if !DEBUG
            //System.Web.HttpContext context = HttpContext.Current;
            System.Exception exp = Context.Server.GetLastError();

            if (exp != null)
            {
                Logger.Fatal(exp);
                //Logger.Flush();
                var cookieValues = new Dictionary<string, string>()
                {
				    {"em",exp.Message},
                    {"im",exp.InnerException != null ? exp.InnerException.Message : string.Empty}
                };
                CookieHelper.SaveCookie("atcerror", cookieValues, DateTime.Now.AddMinutes(60));
            }  
//#endif
        }
	}
}