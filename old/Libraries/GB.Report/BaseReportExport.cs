﻿using Stimulsoft.Report;
using Stimulsoft.Report.Export;
using System.Collections.Generic;

namespace GB.Report
{
    public abstract class BaseReportExport
    {
        protected GetExportSettings _SettingsPdf;
        protected GetExportSettings _SettingsRtf;
        protected GetExportSettings _SettingsXls;

        public BaseReportExport()
        {
            _SettingsPdf = SettingsEmpty;
            _SettingsRtf = SettingsEmpty;
            _SettingsXls = SettingsEmpty;
        }

        internal void FillData(ZipFileCollection zip, ExportFormat format, object[] param)
        {
            IList<StiReport> list = GetReports(param);

            foreach (StiReport item in list)
            {
                zip.AddFile(Export(item, format), item.ReportName);
            }
        }

        protected abstract IList<StiReport> GetReports(object[] param);

        private bool SettingsEmpty(StiExportSettings settings)
        {
            return false;
        }

        private System.IO.MemoryStream Export(StiReport report, ExportFormat format)
        {
            StiExportSettings settings = null;
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            switch (format)
            {
                case ExportFormat.Pdf:
                    report.Render(false);
                    report.ReportName = string.Format("{0}.pdf", report.ReportName);

                    if (_SettingsPdf(settings))
                    {
                        report.ExportDocument(Stimulsoft.Report.StiExportFormat.Pdf, ms, settings);
                    }
                    else
                    {
                        report.ExportDocument(Stimulsoft.Report.StiExportFormat.Pdf, ms);
                    }
                    break;
                case ExportFormat.Rtf:
                    report.Render(false);
                    report.ReportName = string.Format("{0}.rtf", report.ReportName);

                    if (_SettingsRtf(settings))
                    {
                        report.ExportDocument(Stimulsoft.Report.StiExportFormat.Rtf, ms, settings);
                    }
                    else
                    {
                        report.ExportDocument(Stimulsoft.Report.StiExportFormat.Rtf, ms);
                    }
                    break;
                case ExportFormat.Xls:
                    report.Render(false);
                    report.ReportName = string.Format("{0}.xls", report.ReportName);

                    if (_SettingsXls(settings))
                    {
                        report.ExportDocument(Stimulsoft.Report.StiExportFormat.Excel, ms, settings);
                    }
                    else
                    {
                        report.ExportDocument(Stimulsoft.Report.StiExportFormat.Excel, ms);
                    }
                    break;
            }

            return ms;
        }
    }

    public delegate bool GetExportSettings(StiExportSettings settings);
}
