﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpCriterionEquivalentMap : ClassMapBase<ExpCriterionEquivalent>
	{
        public ExpCriterionEquivalentMap()
            : base("Exp_Criterion_Equivalent")
		{
		}

        protected override void InitMap()
        {
            Map(x => x.Comm, "Exp_Criterion_Equivalent_Comm");

            References<ExpConclAct>(x => x.ExpConclAct, "Exp_Concl_Act_ID");
            References<ExpCriterion>(x => x.ExpCriterion, "Exp_Criterion_ID");
            References<ExpConformity>(x => x.ExpConformity, "Exp_Conformity_ID");

            HasMany<ExpValuationReason>(x => x.ValuationReasons)
                .KeyColumns.Add("Exp_Criterion_Equivalent_ID")
                .Inverse();
        }
    }
}