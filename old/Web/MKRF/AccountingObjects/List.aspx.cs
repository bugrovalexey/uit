﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using GB;
using GB.Extentions;
using GB.MKRF.Controllers;
using GB.MKRF.Dtos;
using GB.MKRF.Extensions;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using ICSharpCode.SharpZipLib.GZip;
using GB.Controls.Extensions;
using GB.MKRF.Entities.AccountingObjects;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Report;
using GB.MKRF.Validation;
using GB.MKRF.Web.Helpers;

namespace GB.MKRF.Web.AccountingObjects
{
    public partial class List : System.Web.UI.Page
    {
        #region Fields

        private AccountingObjectService _service;
        private IKTComponentRepository _iktComponentRepository;

        #endregion Fields

        #region Props

        public AccountingObjectService Service
        {
            get
            {
                if (_service == null)
                {
                    _service = new AccountingObjectService();
                }

                return _service;
            }
        }

        public IKTComponentRepository IKTComponentRepository
        {
            get
            {
                if (_iktComponentRepository == null)
                {
                    _iktComponentRepository = new IKTComponentRepository();
                }

                return _iktComponentRepository;
            }
        }

        #endregion Props

        #region Handlers

        #region page handlers

        protected override void OnPreRender(EventArgs e)
        {
            gridData.DataBind();
            base.OnPreRender(e);
        }

        #endregion pageHandlers


        #region control handlers

        #region grid handlers

        protected void gridData_BeforePerformDataSelect(object sender, EventArgs e)
        {
            gridData.DataSource = new AccessRepository().GetAllWithAccess<AccountingObject>();
        }

        protected void gridData_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            Service.Create(GetAccountingObjectControllerArgs());
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void gridData_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            Service.Update(GetAccountingObjectControllerArgs((int)e.Keys[0]));
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void gridData_OnStartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            Service.SetCurrentEdit(Convert.ToInt32(e.EditingKeyValue));
        }

        protected void gridData_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            Service.Delete(new AccountingObjectArgs() { Id = e.Keys[0] });
            (sender as ASPxGridView).GridCancelEdit(e);
        }

        #endregion grid handlers

        protected void Type_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(IKTComponentRepository.GetAllParents())
                .SetValueComboBox(Service
                    .With(cntrl => cntrl.CurrentEdit)
                    .With(currentEdit => currentEdit.Type)
                    .Return(type => type.Id, EntityBase.Default));
        }

        protected void cbxKind_OnDataBinding(object sender, EventArgs e)
        {
            var typeId = Service
                .With(cntrl => cntrl.CurrentEdit)
                .With(currentEdit => currentEdit.Type)
                .Return(type => type.Id, EntityBase.Default);

            sender.SetDataComboBox(IKTComponentRepository.GetAllChildren(typeId))
                .SetValueComboBox(Service
                    .With(cntrl => cntrl.CurrentEdit)
                    .With(currentEdit => currentEdit.Kind)
                    .Return(kind => kind.Id, EntityBase.Default));
        }

        protected void txtShortName_OnDataBinding(object sender, EventArgs e)
        {
            Service.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.ShortName));
        }

        protected void txtFullName_OnDataBinding(object sender, EventArgs e)
        {
            Service.With(x => x.CurrentEdit).Do(currentEdit => sender.SetValueTextBox(currentEdit.Name));
        }

        #endregion control handlers

        #endregion Handlers

        #region Helpers

        private AccountingObjectArgs GetAccountingObjectControllerArgs(int id = 0)
        {
            var result = new AccountingObjectArgs()
            {
                Id = id,
                ShortName = hf["ShortName"].ToStringOrDefault(),
            };

            return result;
        }

        protected string GetLinkToAccountingObject(object id, object shortName)
        {
            return string.Format("<a class='gridLink' href='{0}'>{1}</a>", CommonData.GetUrl(id), shortName);
        }

        #endregion Helpers

    }

}