﻿using System;
using System.Linq;
using GB.Helpers;
using GB.Providers;

namespace GB.MKRF.Web.Monitoring
{
    public partial class Monitoring : System.Web.UI.MasterPage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            //Проверка доступа
            //if (!this.AccessGranted())
            //{
            //    ResponseHelper.DenyAccess(Response);
            //}
        }
        /// <summary>
        /// True, если доступ разрешен
        /// </summary>
        /// <returns></returns>
        //public bool AccessGranted()
        //{
        //    if (DbSiteMapProvider.СheckAccessUser(UrlAccess()))
        //    {
        //        return true;
        //    }

        //    return false;
        //}

        protected virtual string UrlAccess()
        {
            return Request.AppRelativeCurrentExecutionFilePath;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    //string s = Request.AppRelativeCurrentExecutionFilePath;
        //    //var pageUrl = s.Split('/')[s.Split('/').Count() - 1];
        //    //if (pageUrl == "PlansFinances.aspx"
        //    //    || pageUrl == "DemandStatusHistory.aspx"
        //    //    || pageUrl == "InformationCommunity.aspx"
        //    //    || pageUrl == "ActualBudgetAssigns.aspx"
        //    //    || pageUrl == "ConsiderationOfBudget.aspx")
        //    //    FilterControl.YearsVisible = false;
        //    //else
        //    //    FilterControl.YearsVisible = true;
        //}
    }
}