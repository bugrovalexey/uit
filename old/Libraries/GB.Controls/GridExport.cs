﻿using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxCallback;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export;
using System.IO;
using System.Web.UI;

namespace GB.Controls
{
    public class GridExport : CompositeControl
    {
        const string FILE_NAME = "fileName";
        const string FILE_PATH = "filePath";
        static readonly string TempFolder = "~/Temp/";

        protected ASPxCallback callback;
        protected HtmlGenericControl linkPdf;
        protected HtmlGenericControl linkXls;
        protected HtmlGenericControl linkRtf;
        protected HtmlGenericControl container;
        protected ASPxGridViewExporter Grid_ViewExporter;

        
        public ASPxGridView Grid { get; set; }
        public string FileName { get; set; }
        public string UserStyle { get; set; }
        

        public GridExport()
        {
            container = new HtmlGenericControl("div");

            linkPdf = new HtmlGenericControl("a");
            linkXls = new HtmlGenericControl("a");
            linkRtf = new HtmlGenericControl("a");

            Grid_ViewExporter = new ASPxGridViewExporter();
            Grid_ViewExporter.Landscape = true;
            Grid_ViewExporter.PaperKind = System.Drawing.Printing.PaperKind.A4;
            Grid_ViewExporter.LeftMargin = 1;
            Grid_ViewExporter.RightMargin = 1;
            
            callback = new ASPxCallback();
            callback.Callback += callback_linkClick;

            this.EnableViewState = false;
        }

        /// <summary>
        /// Создаем дочерние элементы управления, задаем свойства
        /// </summary>
        protected override void CreateChildControls()
        {
            Grid_ViewExporter.GridViewID = Grid.UniqueID;

            //pdf
            linkPdf.InnerText = " .pdf";
            linkPdf.Attributes.Add("class", "command button_panel_item");
            linkPdf.Style.Add("cursor", "pointer");
            linkPdf.Style.Add("border-bottom-style", "none");
            
            //xls
            linkXls.InnerText = " .xls";
            linkXls.Attributes.Add("class", "command button_panel_item");
            linkXls.Style.Add("cursor", "pointer");
            linkXls.Style.Add("border-bottom-style", "none");
            
            //rtf
            linkRtf.InnerText = " .rtf";
            linkRtf.Attributes.Add("class", "command button_panel_item");
            linkRtf.Style.Add("cursor", "pointer");
            linkRtf.Style.Add("border-bottom-style", "none");
            
            if (!string.IsNullOrWhiteSpace(UserStyle))
                container.Attributes.Add("style", UserStyle);

            container.InnerText ="Выгрузить в:";
            //container.Attributes.Add("style", Style);
            //container.Style.Add("position", "absolute");
            //container.Style.Add("right", "10px");
            //container.Style.Add("margin-top", "-18px");

            Controls.Add(Grid_ViewExporter);
            Controls.Add(callback);
            Controls.Add(container);

            container.Controls.Add(linkPdf);
            container.Controls.Add(linkRtf);
            container.Controls.Add(linkXls);
        }

        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            base.RenderBeginTag(writer);

            linkPdf.Attributes.Add("onclick", callback.ClientID + ".PerformCallback('ExportPdf')");
            linkXls.Attributes.Add("onclick", callback.ClientID + ".PerformCallback('ExportXls')");
            linkRtf.Attributes.Add("onclick", callback.ClientID + ".PerformCallback('ExportRtf')");
        }

        /// <summary>
        /// Обработка ссылок
        /// </summary>
        protected void callback_linkClick(object sender, CallbackEventArgs e)
        {
            if (e.Parameter == "ExportXls")
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    Grid_ViewExporter.WriteXls(stream);
                    ASPxGridView.RedirectOnCallback(GetLinkFile(stream, !string.IsNullOrEmpty(FileName) ? FileName + ".xls" : "Table.xls"));
                }
            }
            else if (e.Parameter == "ExportPdf")
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    Grid_ViewExporter.WritePdf(stream);
                    ASPxGridView.RedirectOnCallback(GetLinkFile(stream, !string.IsNullOrEmpty(FileName) ? FileName + ".pdf" : "Table.pdf"));
                }
            }
            else if (e.Parameter == "ExportRtf")
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    Grid_ViewExporter.WriteRtf(stream);
                    ASPxGridView.RedirectOnCallback(GetLinkFile(stream, !string.IsNullOrEmpty(FileName) ? FileName + ".rtf" : "Table.rtf"));
                }
            }
        }

        // Скопировано из FileHelper чтобы не переносить
        static string GetLinkFile(MemoryStream stream, string fileName)
        {
            return GetLinkFile(SaveInTemp(stream), fileName);
        }

        static string GetLinkFile(string path, string fileName)
        {
            return string.Concat(VirtualPathUtility.ToAbsolute("~/File.aspx"), "?", FILE_NAME, "=", HttpUtility.UrlEncode(fileName), "&", FILE_PATH, "=", HttpUtility.UrlEncode(path));
        }

        static string SaveInTemp(MemoryStream stream)
        {
            string path = HttpContext.Current.Server.MapPath(TempFolder) + System.Guid.NewGuid().ToString();

            using (FileStream fs = System.IO.File.OpenWrite(path))
            {
                stream.WriteTo(fs);
            }

            return path;
        }

        static string ToAbsolute(string url)
        {
            return VirtualPathUtility.ToAbsolute(url);
        }
    }
}
