﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    using bgTeam.IsRui.Domain.Entities.Directories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GoodsCharacteristic : EntityBase
    {
        public virtual string Name { get; set; }

        public virtual int? Value { get; set; }

        public virtual Units Unit { get; set; }

        public virtual ActivityGoods Goods { get; set; }
    }
}
