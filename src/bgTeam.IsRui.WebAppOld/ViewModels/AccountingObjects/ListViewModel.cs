﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Workflow;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.Helpers.Control;
using bgTeam.IsRui.MKRF.Logic.Access;
using bgTeam.IsRui.MKRF.Logic.Tabs;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public abstract class ListBaseViewModel
    {
        private readonly IStatusRepository _statusRepository;

        string _CurrentTab = null;
        

        public string AddNew { get; set; }
        
        //public string URLCreate { get; set; }

        public IList<string> Tabs { get; private set; }

        public IEnumerable<TableAccountingObjects> Items { get; set; }

        public SelectList/*List<Status>*/ StatusList { get; set; }

        public int? StatusId { get; set; }

        //public ListBaseViewModel(IStatusRepository statusRepository)
        //{
        //    _statusRepository = statusRepository;
        //}

        public ListBaseViewModel(IStatusRepository statusRepository, string tab)
        {
            _statusRepository = statusRepository;
            _CurrentTab = tab;

            StatusList = new IdNameSelectListWithNull(_statusRepository.GetStatuses(EntityType.AccountingObject), null);//new StatusRepository().GetStatuses(EntityType.AccountingObject).ToList();

            //Load();

            var handlerTab = Singleton<TabsManager>.Instance.GetHandler<AccountingObjectsTabs>();

            Tabs = handlerTab.GetTabsList(LinkBuilderTab);
        }

        //protected abstract void Load();

        private string LinkBuilderTab(string text, string actionName, string controllerName)
        {
            string cl = null;

            if (_CurrentTab == actionName)
                cl = "active";

            return string.Concat("<li class='", cl, "'>", LinkBuilder.ActionLink(text, actionName, controllerName), "</li>");
        }
    }

    /// <summary>Мои объекты учета - список ОУ для роли "Организация"</summary>
    public class ListViewModel : ListBaseViewModel
    {
        private readonly IUserRepository _userRepository;
        private readonly IStatusRepository _statusRepository;

        //public ListViewModel(
        //    IUserRepository userRepository,
        //    IStatusRepository statusRepository)
        //    : base(statusRepository)
        //{
        //    _userRepository = userRepository;
        //    _statusRepository = statusRepository;
        //}

        public ListViewModel(
            IUserRepository userRepository,
            IStatusRepository statusRepository,
            string mode, int? status, int? asc) 
            : base(statusRepository, "List") 
        {
            _userRepository = userRepository;
            _statusRepository = statusRepository;

            if (mode == "all" || (mode == "status" && !status.HasValue))
            {
                Items = new RepositoryDc<AccountingObject>().GetAllEx(q => q
                    .Fetch(x => x.Kind).Eager
                    .Fetch(x => x.Type).Eager
                    .Fetch(x => x.Favorites).Eager
                    .Fetch(x => x.Status).Eager
                    .OrderBy(x => x.CreateDate).Desc
                    .Where(x => x.Department == _userRepository.GetCurrent().Department))
                    //.Where(x => x.Mode == AccountingObjectEnum.One)
                    .Select(x => new TableAccountingObjects(userRepository, x));
            }
            else if (mode == "favs")
            {
                //TODO оптимизировать запрос

                Items = new RepositoryDc<AccountingObject>().GetAllEx(q => q
                    .Fetch(x => x.Kind).Eager
                    .Fetch(x => x.Type).Eager
                    .Fetch(x => x.Favorites).Eager
                    .Fetch(x => x.Status).Eager
                    .OrderBy(x => x.CreateDate).Desc
                    .Where(x => x.Department == _userRepository.GetCurrent().Department))
                    //.Where(x => x.Mode == AccountingObjectEnum.One)
                    .Select(x => new TableAccountingObjects(userRepository, x)).Where(x => x.Favorite == true);
            }
            else if (mode == "status")
            {
                Items = new RepositoryDc<AccountingObject>().GetAllEx(q => q
                                   .Fetch(x => x.Kind).Eager
                                   .Fetch(x => x.Type).Eager
                                   .Fetch(x => x.Favorites).Eager
                                   .Fetch(x => x.Status).Eager
                                   .OrderBy(x => x.CreateDate).Desc
                                   .Where(x => x.Department == _userRepository.GetCurrent().Department && x.Status.Id == status))
                    //.Where(x => x.Mode == AccountingObjectEnum.One)
                                   .Select(x => new TableAccountingObjects(userRepository,x));
            }

            if (asc.HasValue && asc.Value != 0)
            {
                if (asc.Value == 1)
                    Items = Items.OrderBy(x => x.CostValue);
                else
                    Items = Items.OrderByDescending(x => x.CostValue);
            }

            //var handler = Singleton<MasterEditManager>.Instance.GetHandler(new AccountingObject());
            //URLCreate = handler.First().Action;
            AddNew = LinkBuilder.Link("<i class='material-icons'>add</i>", "#", onclick: "CreateNew();", @class: "btn-floating btn-large waves-effect waves-light");

            if (ManagerAccess.CanCreate(EntityType.AccountingObject) == false)
                AddNew = string.Empty;
        }

        //protected override void Load()
        //{
            
        //}
    }

    /// <summary>На согласовании - список ОУ для роли "Куратор"</summary>
    public class ListApproveViewModel : ListBaseViewModel
    {
        public ListApproveViewModel(
            IUserRepository userRepository,
            IStatusRepository statusRepository) 
            : base(statusRepository, "ListApprove") 
        {
            bgTeam.IsRui.Domain.Entities.Common.Department dep = null;
            var rootDep = new RepositoryDc<bgTeam.IsRui.Domain.Entities.Common.Department>().Get(0);

            Items = new RepositoryDc<AccountingObject>().GetAllEx(q => q
                .Fetch(x => x.Kind).Eager
                .Fetch(x => x.Type).Eager
                .Fetch(x => x.Status).Eager
                .Fetch(x => x.Favorites).Eager
                .JoinAlias(x => x.Department, () => dep)
                .OrderBy(x => x.CreateDate).Desc
                .Where(x => x.Status.Id == (int)StatusEnum.AoAgreed)
                .Where(() => (dep.Parent == rootDep && dep.Id == userRepository.GetCurrent().Department.Id) || dep.Parent == userRepository.GetCurrent().Department))
                .Select(x => new TableAccountingObjects(userRepository, x));

            //if (!Singleton<AccessEntityProvider>.Instance.CanCreate(EntityType.AccountingObject))
            //    AddNew = string.Empty;
        }

        //protected override void Load()
        //{
            
        //}
    }

    public class ListMyApproveViewModel : ListBaseViewModel
    {
        public ListMyApproveViewModel(
            IUserRepository userRepository,
            IStatusRepository statusRepository)  
            : base(statusRepository, "ListMyApprove") 
        { 
            Items = new RepositoryDc<AccountingObject>().GetAllEx(q => q
                .Fetch(x => x.Kind).Eager
                .Fetch(x => x.Type).Eager
                .Fetch(x => x.Favorites).Eager
                .Fetch(x => x.Status).Eager.OrderBy(x => x.CreateDate).Desc
                .Where(x => x.Department == userRepository.GetCurrent().Department))
                .Where(x => x.Mode == AccountingObjectEnum.Multi)
                .Select(x => new TableAccountingObjects(userRepository, x));



            //var handler = Singleton<MasterEditManager>.Instance.GetHandler(new AccountingObject());
            //URLCreate = handler.First().Action;

            //if (!Singleton<AccessEntityProvider>.Instance.CanCreate(EntityType.AccountingObject))
            //    AddNew = string.Empty;
        }

        //protected override void Load()
        //{
            
        //}
    }

    public class TableAccountingObjects
    {
        public int Id { get; set; }

        public string Number { get; set; }

        public string ShortName { get; set; }

        public string TypeName { get; set; }

        public string KindName { get; set; }

        public string Stage { get; set; }

        public string StatusName { get; set; }

        public object DeleteLink { get; set; }

        public bool CanDelete { get; set; }

        public bool Favorite { get; set; }

        public string Mode { get; set; }
        
        public string Cost { get; set; }

        public decimal CostValue { get; set; }

        public string Description { get; set; }


        public TableAccountingObjects(IUserRepository userRepository, AccountingObject ao)
        {
            Id = ao.Id;
            Number = ao.Number;
            Mode = ao.Mode == AccountingObjectEnum.Multi ? "Создано на основании согласованного" : string.Empty;
            ShortName = ao.ShortName;
            TypeName = ao.Type.Return(type => type.Name, default(string));
            KindName = ao.Kind.Return(kind => kind.Name, default(string));
            Stage = ao.Stage.GetDescription();
            StatusName = ao.Status.Return(status => status.Name, default(string));
            DeleteLink = GetDeleteLink(ao.Id);
            CanDelete = ManagerAccess.CanDelete(ao);
            Favorite = ao.Favorites.FirstOrDefault() != null;

            //TODO нужно провести оптимизацию
            var currentActivities = new RepositoryDc<AoActivity>().GetAllEx(query => query
               .Where(a => a.AccountingObject == ao));

            CostValue = currentActivities.Sum(a => a.CostY0);
            Cost = CostValue.ToString("N2");

            var childs = new RepositoryDc<AccountingObject>().GetAllEx(q =>
                q.Where(a => a.Parent == ao));

            if (ao.Department == userRepository.GetCurrent().Department)
            {
                if (childs.Count == 1)
                    Description = childs[0].Department.Name;

                if (childs.Count > 1)
                    Description = "Консолидированное";
            }
            else
            {
                Description = ao.Department.Name;

                if (childs.Count == 1)
                    Description = childs[0].Department.Name;
            }

        }
        
        private static object GetDeleteLink(int id)
        {
            return LinkBuilder.AjaxDeleteLink("DeleteAccountingObject", "AccountingObjects", new { id }, "Grid");
        }
    }
}