﻿namespace GB.DataAccess.Services.Args
{
    /// <summary>
    /// Базовый Args ОУ
    /// </summary>
    public class AccountingObjectArgs : BaseArgsNew
    {        
        public string ShortName { get; set; }
    }

    public class AccountingObjectResponsibleServiceArgs : BaseArgsNew
    {
        public int DepartmentId { get; set; }
        public int ResponsibleCoord { get; set; }
        public int ResponsibleInformation { get; set; }
    } 
}