﻿using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.MKRF.Logic.Access.Handlers;
using System;

namespace bgTeam.IsRui.MKRF.Logic.Access.Impl
{
    // ToDo: Move this to proper place
    public sealed class ManagerAccess : IManagerAccess
    {
        public bool CanRead<T>(T entity)
            where T : EntityBase, IWorkflowObject
        {
            return true;
            var handler = GetHandler(entity);
            return handler.CanRead();
        }

        public bool CanEdit<T>(T entity)
            where T : EntityBase, IWorkflowObject
        {
            return true;
            var handler = GetHandler(entity);
            return handler.CanEdit();
        }

        public bool CanCreate<T>(T entity)
            where T : EntityBase, IWorkflowObject
        {
            return true;
            var handler = GetHandler(entity);
            return handler.CanCreate();
        }

        public bool CanCreate(EntityType entityType)
        {
            return true;
            return Singleton<AccessEntityProvider>.Instance.CanCreate(entityType);
        }

        public bool CanDelete<T>(T entity)
            where T : EntityBase, IWorkflowObject
        {
            return true;
            var handler = GetHandler(entity);
            return handler.CanDelete();
        }

        private static IAccessHandler GetHandler<T>(T entity)
            where T : EntityBase, IWorkflowObject
        {
            //if(entity is AccountingObject)
            //    return new AccountingObjectAccessHandler(entity as AccountingObject);

            //if (entity is PlansActivity)
            //    return new PlansActivityAccessHandler(entity as PlansActivity);

            throw new ArgumentOutOfRangeException(string.Format("Тип {0} не имеет обработчика прав.", entity.GetType()));
        }
    }
}
