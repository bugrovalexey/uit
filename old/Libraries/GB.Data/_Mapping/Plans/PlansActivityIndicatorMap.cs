﻿using GB.Data.Plans;

namespace GB._Mapping.Plans
{
    internal class PlansActivityIndicatorMap : ClassMapHiLo<PlansActivityIndicator>
    {
        public PlansActivityIndicatorMap()
            : base("Plans_Activity_Indicators")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Indicators_Name");
            Map(x => x.ExplanationRelationshipWithProjectMark, "Plans_Activity_Indicators_Explan");
            Map(x => x.BaseValue, "Plans_Activity_Indicators_Basic_Value");
            Map(x => x.TargetedValue, "Plans_Activity_Indicators_Goal_Value");
            Map(x => x.DateToAchieveGoalValue, "Plans_Activity_Indicators_Date");
            Map(x => x.AlgorithmOfCalculating, "Plans_Activity_Indicators_Algorithm");

            References(x => x.Unit, "MUnit_ID");
            References(x => x.PlansActivity, "Plans_Activity_ID");
        }
    }
}