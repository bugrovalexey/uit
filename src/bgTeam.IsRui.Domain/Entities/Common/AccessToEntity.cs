﻿using bgTeam.IsRui.Domain.Entities.Workflow;

namespace bgTeam.IsRui.Domain.Entities.Common
{
    public class AccessToEntity : EntityBase
    {
        public virtual EntityType EntityType { get; set; }

        public virtual AccessActionEnum AccessMask { get; set; }

        public virtual Role Role { get; set; }

        public virtual Status Status { get; set; }

        public virtual string Note { get; set; }
    }
}