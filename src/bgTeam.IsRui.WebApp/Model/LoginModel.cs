﻿namespace bgTeam.IsRui.WebApp.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class LoginModel
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}
