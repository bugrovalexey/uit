﻿using GB.Data.AccountingObjects.WorksAndGoods;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.WorkAndGood
{
    public class AddOrSaveWorkAndGoodsCommand : ICommand<AddOrSaveWorkAndGoodsContext, WorkAndGoods>
    {
        ISessionFactoryGb factory;

        public AddOrSaveWorkAndGoodsCommand()
        {
            factory = new SessionFactory();
        }

        public WorkAndGoods Execute(AddOrSaveWorkAndGoodsContext context)
        {
            WorkAndGoods workAndGoods;

            if (context.IsNew)
            {
                workAndGoods = new WorkAndGoods();
            }
            else
            {
                workAndGoods = new Repository<WorkAndGoods>().Get(q => q.Where(x => x.ObjectId == context.ObjectId && x.Type == context.Type));
            }

            AutoMapper.Mapper.Map(context, workAndGoods);
            
            using (var session = factory.Create())
            {
                session.SaveOrUpdate(workAndGoods);
                session.Commit();
            }

            return workAndGoods;
        }
    }
}
