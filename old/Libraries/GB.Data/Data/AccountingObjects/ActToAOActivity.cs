﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Связь Акта и Мероприятия ОУ
    /// </summary>
    public class ActToAOActivity : EntityBase
    {
        /// <summary>
        /// Акт
        /// </summary>
        public virtual ActOfAcceptance ActOfAcceptance { get; set; }

        /// <summary>
        /// Мероприятие
        /// </summary>
        public virtual AoActivity AoActivity { get; set; }
    }
}
