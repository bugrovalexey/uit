﻿using bgTeam.IsRui.Domain.Entities.Plans;

namespace bgTeam.IsRui.MKRF.Logic.Access.Handlers
{
    public class PlansActivityAccessHandler : AccessHandlerBase<PlansActivity>
    {
        public override PlansActivity Entity { get; set; }

        public PlansActivityAccessHandler(PlansActivity pa) : base(pa)
        {
        }
    }
}
