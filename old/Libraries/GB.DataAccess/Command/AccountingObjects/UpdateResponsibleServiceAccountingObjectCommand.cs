﻿using GB.Data.AccountingObjects;
using GB.Data.Common;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateResponsibleServiceAccountingObjectCommand : ICommand<UpdateResponsibleServiceAccountingObjectContext>
    {
        ISessionFactoryGb factory;

        public UpdateResponsibleServiceAccountingObjectCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(UpdateResponsibleServiceAccountingObjectContext context)
        {
            var entity = new Repository<AccountingObject>().GetExisting(context.xId);           

            entity.ResponsibleDepartment = new Repository<Department>().Get(context.DepartmentId);
            entity.ResponsibleCoord = new Repository<Responsible>().Get(context.ResponsibleCoord);
            entity.ResponsibleInformation = new Repository<Responsible>().Get(context.ResponsibleInformation);

            using (var session = factory.Create())
            {
                session.Update(entity);
                session.Commit();
            }
        }
    }
}
