﻿using GB.MKRF.Entities.Statuses;

namespace GB.MKRF.Entities.Plans
{
    /// <summary>Заявка на экспертизу по Плану</summary>
    public class ExpDemand : ExpDemandBase
    {
        /// <summary>План</summary>
        public virtual Plan ExpObject { get; set; }

        /// <summary>Номер</summary>
        //        public virtual string Num { get; set; }//null

        /// <summary>ответственный</summary>
        public virtual string Owner { get; set; }//null

        /// <summary>Время отправки заявки в МКС</summary>
        //public virtual DateTime? SendMKS { get; set; }//null

        /// <summary>Статус хода экспертизы</summary>
        //public virtual string ConclusionStatus { get; set; }

        /// <summary>Заключения</summary>
        //public virtual IList<ExpConclusionBase> ConclusionList { get; protected set; }

        /// <summary>Комментарий</summary>
        //        public virtual string Comment { get; set; }//null

        //public virtual ExpConclusion SummaryConclusion { get; set; }

        /// <summary>Этап</summary>
        //public virtual PlansStage Stage { get; set; }

        public virtual Status ReportByPlanStatus { get; set; }
    }
}