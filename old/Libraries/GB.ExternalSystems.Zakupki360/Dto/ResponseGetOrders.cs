﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.ExternalSystems.Zakupki360.Dto
{
    class ResponseGetOrders
    {
        public string Error { get; set; }

        public IEnumerable<OrderInfo> Orders { get; set; }
    }
}
