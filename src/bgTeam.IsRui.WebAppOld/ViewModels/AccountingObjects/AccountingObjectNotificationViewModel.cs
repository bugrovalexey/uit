﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class AccountingObjectNotificationViewModel: NotificationBaseViewModel
    {
        public AccountingObjectNotificationViewModel(AccountingObject accountingObject)
        {
            Id = accountingObject.Id;
        }

        public int Id { get; set; }
    }
}