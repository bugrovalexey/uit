﻿using GB;
using GB.MKRF.Entities.Admin;

namespace GB.MKRF.Validation
{
    internal class Document
    {
        /// <summary>Параметры для валидации документа</summary>
        internal static string PrepareToValidate(EntityBase Entity, ObjActionEnum Action)
        {
            if (Action == ObjActionEnum.create)
            {
                Entities.Documents.Document Item = (Entities.Documents.Document)Entity;

                return "0;" + (Item.Owner != null ? Item.Owner.Id.ToString() : "0") + ";"
                    + ((int)Item.DocType).ToString() + ";"
                    + (Item.Owner != null ? ((int)Item.Owner.GetTableTypeOld()).ToString() : "0");
            }
            else
            {
                return Entity.Id.ToString();
            }
        }
    }
}
