﻿using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.WebApp.Helpers;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class TableWorkAndGoods : NotificationBaseViewModel
    {
        #region consts

        public const string NameCaption = "Наименование";
        public const string TypeOfSupportCaption = "Тип";
        public const string KindOfSupportCaption = "Вид";
        public const string SummaCaption = "Стоимость";

        #endregion consts

        public TableWorkAndGoods(WorkAndGoods entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            TypeOfSupportDescription = EnumHelper.GetDescription(entity.Type);
            KindOfSupport = entity.CategoryKindOfSupport.Name;
            Summa = entity.Summa.ToMoneyString();
        }

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(TypeOfSupportCaption)]
        public string TypeOfSupportDescription { get; set; }

        [DisplayName(KindOfSupportCaption)]
        public string KindOfSupport { get; set; }

        [DisplayName(SummaCaption)]
        public string Summa { get; set; }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteWorkAndGoods", "AccountingObjects", new { id = Id }, "GridWorksAndGoods");
            }
        }

    }
}