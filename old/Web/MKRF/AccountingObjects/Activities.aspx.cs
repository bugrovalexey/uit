﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.Helpers;
using GB.MKRF.Repository;

namespace GB.MKRF.Web.AccountingObjects
{
    public partial class Activities : BasePageAccountingObject
    {
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            Grid.DataBind();
        }

        protected string GetLinkToActivityCard(object Id, object Name)
        {
            return string.Format("<a class='gridLink' href='{0}'>{1}</a>", MKRF.Web.Activities.CommonData.GetUrl(Id), Name);
        }

        public static string GetUrl(object accountingObjectId)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/AccountingObjects/Activities.aspx"), EditCard.AccountingObjectIdKey, accountingObjectId);
        }


        protected void Grid_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            Grid.DataSource = new PlansActivityRepository().GetActivitiesByAccountingObject(AccountingObjectId.GetValueOrDefault());
        }
    }
}