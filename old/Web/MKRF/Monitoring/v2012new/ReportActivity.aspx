﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Monitoring/Monitoring.master" AutoEventWireup="true" CodeBehind="ReportActivity.aspx.cs" Inherits="GB.MKRF.Web.Monitoring.v2012new.ReportActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="buttons" runat="server">

    <script type="text/javascript">

        function OnValueChanged(s, e) {
            MainLoadingPanel.Show();
            YearsComboBox.PerformCallback(DepComboBox.GetValue());
        }

        function GenerateReport(s, e) {

            var plan = YearsComboBox.GetValue();

            if (!plan) {
                alert('Не задан год.');
                return;
            }

            var colPar = '';
            var colList = ColumnCheckBoxList.GetSelectedValues();

            colList.forEach(function (e, i) {
                colPar += e + ';';
            });

            var url = 'ReportActivityTable.aspx?planId=' + plan + '&col=' + colPar.substring(0, colPar.length - 1);

            window.open(url, '_blank');
        }

    </script>

    <div style="font-family: tahoma; font-size: 14pt; text-align: center; width: 100%; margin-bottom: 15px;">Сведения по мероприятиям</div>
    <table style="width: 100%">
        <tr>
            <td style="width: 1%; white-space: nowrap; text-align: right">Департамент:</td>
            <td style="width: 1%">
                <dxe:ASPxComboBox runat="server" ID="DepComboBox" Width="340px" ValueType="System.Int32"
                    ValueField="Id" TextField="Name" ClientInstanceName="DepComboBox" OnDataBinding="DepComboBox_DataBinding">
                    <ClientSideEvents SelectedIndexChanged="OnValueChanged" />
                </dxe:ASPxComboBox>
            </td>
            <td style="padding-left: 10px">Год:</td>
            <td style="width: 100px">
                <dxe:ASPxComboBox runat="server" ID="YearsComboBox" Width="80px" ValueType="System.Int32"
                    ValueField="Key" TextField="Text" ClientInstanceName="YearsComboBox"
                    OnDataBinding="YearsComboBox_DataBinding" OnCallback="YearsComboBox_Callback">
                    <ClientSideEvents EndCallback="function(s,e){ MainLoadingPanel.Hide(); }" />
                </dxe:ASPxComboBox>
            </td>
            <td style="width: 98%"></td>
        </tr>

        <tr>
            <td style="white-space: nowrap; vertical-align: top; padding-top: 10px">Столбцы отчета:
            </td>
            <td colspan="3" style="padding-top: 10px">

                <dxe:ASPxCheckBoxList ID="ColumnCheckBoxList" runat="server" ClientInstanceName="ColumnCheckBoxList" RepeatColumns="1" RepeatLayout="Table" Width="460px">
                    <Items>
                        <dxe:ListEditItem Selected="true" Value="1" Text="Код мероприятия" />
                        <dxe:ListEditItem Selected="true" Value="2" Text="Наименование мероприятия" />
                        <dxe:ListEditItem Selected="true" Value="3" Text="Тип мероприятия" />
                        <dxe:ListEditItem Selected="true" Value="4" Text="Классификационный признак ИС или компонента ИТ-инфраструктуры" />
                        <dxe:ListEditItem Selected="true" Value="5" Text="Наименование ИС или объекта ИКТ-инфраструктуры" />
                        <dxe:ListEditItem Selected="true" Value="6" Text="Аннотация" />
                        <dxe:ListEditItem Selected="true" Value="7" Text="Источник финансирования" />
                        <dxe:ListEditItem Selected="true" Value="8" Text="ФИО ответственного сотрудника" />
                        <dxe:ListEditItem Selected="true" Value="9" Text="Цель мероприятия" />

                        <dxe:ListEditItem Selected="true" Value="10" Text="Код по БК ПЗ/РЗ" />
                        <dxe:ListEditItem Selected="true" Value="11" Text="Код по БК ЦСР" />
                        <dxe:ListEditItem Selected="true" Value="12" Text="Код по БК ВР" />
                        <%--<dxe:ListEditItem Selected="true" Value="13" Text="Код по БК КОСГУ" />--%>

                        <dxe:ListEditItem Selected="true" Value="13" Text="Ранее заключенные гос. контракты  (номер контракта, дата контракта)" />

                        <dxe:ListEditItem Selected="true" Value="14" Text="Объем планируемых бюджетных ассигнований, тыс. руб" />
                        <dxe:ListEditItem Selected="true" Value="15" Text="Дополнительная потребность, тыс. руб" />
                        <dxe:ListEditItem Selected="true" Value="16" Text="Объем фактически израсходованных бюджетных ассигнований, тыс. руб" />
                    </Items>
                </dxe:ASPxCheckBoxList>
            </td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="center" runat="server">
    <div style="padding-left: 103px;">
        <dxe:ASPxButton ID="GenerateButton" runat="server" ClientInstanceName="GenerateButton" Text="Сформировать" AutoPostBack="false">
            <ClientSideEvents Click="GenerateReport" />
        </dxe:ASPxButton>
    </div>
</asp:Content>
