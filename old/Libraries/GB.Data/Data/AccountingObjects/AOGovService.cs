﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.Frgu;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Реализуемые государственные услуги и функции объекта учета
    /// </summary>
    public class AOGovService : EntityBase
    {
        public AOGovService()
        {
            AoGovServicesNpa = new List<AOGovServiceNPA>();
        }

        /// <summary>
        /// Родитель
        /// </summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Если копировали из ГУ/ГФ
        /// </summary>
        public virtual GovService GovService { get; set; }

        /// <summary>
        /// Наименование государственной услуги (функции)
        /// </summary>
        //public virtual string Name { get; set; }

        /// <summary>
        /// Процент использования ресурсов ИС при автоматизации исполнения государственной услуги (функции)
        /// </summary>
        public virtual int? PercentOfUseResource { get; set; }

        /// <summary>
        /// Нормативно-правовые акты
        /// </summary>
        public virtual IList<AOGovServiceNPA> AoGovServicesNpa { get; set; }

    }
}
