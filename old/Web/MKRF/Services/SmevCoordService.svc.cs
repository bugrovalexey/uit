﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Threading;
using System.Xml.Linq;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Repository;
using Uviri.Repository;
using Uviri.Smev.Data;

namespace Uviri.MKRF.Web.Services
{
    [ServiceContract]
    public interface ISmevCoordService
    {
        [OperationContract]
        FunctionResponse GetPlans(FunctionRequest request);
        
        [OperationContract]
        FunctionResponse GetDepartments(FunctionRequest request);

        [OperationContract]
        List<IKTClassifierResponse> GetIKTClassifier();

        [OperationContract]
        List<SchedulePlan> GetSchedulePlans();

        //[OperationContract]
        //void GetSchedulePlan();
    }


    public class SmevCoordService : ISmevCoordService
    {
        /// <summary>
        /// Получить список планов на определённый год
        /// </summary>
        public FunctionResponse GetPlans(FunctionRequest request)
        {
            FunctionResponse resp = new FunctionResponse();

            if (request.MessageData.AppData.CoordGetPlansRequest.Year >= 2010 && request.MessageData.AppData.CoordGetPlansRequest.Year <= 2020)
            {
                System.Threading.Thread.CurrentPrincipal = new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity("mks"), null);

                XDocument xdoc = PlanRepository.GetPlansXML(request.MessageData.AppData.CoordGetPlansRequest.Year);

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                StringWriterWithEncoding swe = new StringWriterWithEncoding(sb, System.Text.Encoding.UTF8);

                using (System.Xml.XmlTextWriter xw = new System.Xml.XmlTextWriter(swe))
                {
                    xdoc.Save(xw);
                }

                resp.MessageData.AppData.CoordGetPlansResult = new Smev.Data.CoordGetPlansResult();
                resp.MessageData.AppData.CoordGetPlansResult.File = System.Text.Encoding.UTF8.GetBytes(sb.ToString());

#if DEBUG
                using (var ms = new System.IO.MemoryStream())
                {
                    using (var sw = new System.IO.StreamWriter(ms))
                    {
                        sw.Write(sb.ToString());
                     
                        try
                        {
                            Uviri.Helpers.FileHelper.SaveInTemp(ms, string.Concat("Plans_", request.MessageData.AppData.CoordGetPlansRequest.Year, ".xml"));
                        }
                        catch { }
                    }
                }
#endif
            }

            return resp;
        }
        
        
        /// <summary>
        /// Получить список департаментов
        /// </summary>
        public FunctionResponse GetDepartments(FunctionRequest request)
        {
            FunctionResponse resp = new FunctionResponse();

            System.Threading.Thread.CurrentPrincipal = new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity("mks"), null);

            XDocument xdoc = DepartmentRepository.GetDepartmentsXML();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            Web.Services.SmevCoordService.StringWriterWithEncoding swe = new Web.Services.SmevCoordService.StringWriterWithEncoding(sb, System.Text.Encoding.UTF8);

            using (System.Xml.XmlTextWriter xw = new System.Xml.XmlTextWriter(swe))
            {
                xdoc.Save(xw);
            }

            resp.MessageData.AppData.CoordGetDepartmentsResult = new Smev.Data.CoordGetDepartmentsResult();
            resp.MessageData.AppData.CoordGetDepartmentsResult.File = System.Text.Encoding.UTF8.GetBytes(sb.ToString());

#if DEBUG
            using (var ms = new System.IO.MemoryStream())
            {
                using (var sw = new System.IO.StreamWriter(ms))
                {
                    sw.Write(sb.ToString());
                 
                    try
                    {
                        Uviri.Helpers.FileHelper.SaveInTemp(ms, "Departments.xml");
                    }
                    catch { }
                }
            }
#endif

            return resp;
        }


        /// <summary>
        /// Получить классификатор ИС/ИКТ
        /// </summary>
        /// <returns></returns>
        public List<IKTClassifierResponse> GetIKTClassifier()
        {
            Thread.CurrentPrincipal = new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity("mks"), null);

            var list = RepositoryBase<IKTComponent>.GetAll().ToList();

            var response = list.ConvertAll(ConvertToIKTClassifier);

#if DEBUG
            var sb = new System.Text.StringBuilder();
            foreach (var item in response)
            {
                sb.AppendFormat("ID = {0}, Code = {1}, Name = {2}, Year = {3} ", item.ID, item.Code, item.Name, item.Year)
                  .AppendLine();
            }
            using (var ms = new System.IO.MemoryStream())
            {
                using (var sw = new System.IO.StreamWriter(ms))
                {
                    sw.Write(sb.ToString());

                    try
                    {
                        Uviri.Helpers.FileHelper.SaveInTemp(ms, "IKTClassifier.xml");
                    }
                    catch { }
                }
            }
#endif
            
            return response;
        }

        private static IKTClassifierResponse ConvertToIKTClassifier(IKTComponent x)
        {
            return new IKTClassifierResponse()
                       {
                           ID = x.Id,
                           Code = x.Code,
                           Name = x.Name,
                           Year = x.Year.HasValue ? x.Year.Value : DateTime.Now.Year
                       };
        }

        public class StringWriterWithEncoding : System.IO.StringWriter
        {

            private readonly System.Text.Encoding _Encoding;


            public StringWriterWithEncoding(System.Text.StringBuilder sb, System.Text.Encoding encoding)
                : base(sb)
            {
                _Encoding = encoding;
            }


            public override System.Text.Encoding Encoding 
            { 
                get { return _Encoding ?? base.Encoding; 
                } 
            }
        }

        public List<SchedulePlan> GetSchedulePlans()
        {
            var result = new List<SchedulePlan>();
            var table = PlanRepository.GetSchedulePlans();
            foreach (System.Data.DataRow row in table.Rows)
            {
                var item = new SchedulePlan();
                item.GovtOrganName = row["Govt_Organ_Name"].ToString();
                item.Year = Convert.ToInt32(row["Year"]);
                item.SchedulePlanID = Convert.ToInt32(row["Plans_Schedule_ID"]);
                item.SchedulePlanNum = Convert.ToInt32(row["Plans_Schedule_Num"]);
                item.PlanSubjectName = row["Plans_Schedule_Subject_Name"].ToString();
                item.PlanActivityName = row["Plans_Activity_Name"].ToString();
                item.PlanWorkName = row["Plans_Work_Name"].ToString();
                item.PlanSpecType = row["Plans_Spec_Equip_Type"].ToString();
                item.OKDPCodeName = row["OKDP_CodeName"].ToString();
                item.GRBSCodeName = row["GRBS_CodeName"].ToString();
                item.SectionKBKCodeName  = row["SectionKBK_CodeName"].ToString();
                item.ExpenseItem = row["ExpenseItem_CodeName"].ToString();
                item.WorkForm = row["WorkForm_CodeName"].ToString();

                DateTime dt;
                if(DateTime.TryParse(row["Plans_Schedule_Exec_Time"].ToString(), out dt))
                    item.SchedulePlanExecTime = dt;

                decimal money;
                if (decimal.TryParse(row["Cost"].ToString(), out money))
                    item.Cost = money;
                result.Add(item);
            }

            return result;
        }
    }


    /// <summary>
    /// Классификатор ИС/ИКТ
    /// </summary>
    [DataContract]
    public class IKTClassifierResponse
    {
        /// <summary> Идентификатор ИС/ИКТ </summary>
        [DataMember]
        public int ID { get; set; }

        /// <summary> Наименование </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary> Код для сортировки </summary>
        [DataMember]
        public string Code { get; set; }

        /// <summary> Год в котором действует этот классификатор </summary>
        [DataMember]
        public int Year { get; set; }
    }

    [DataContract]
    public class SchedulePlan
    {
        [DataMember]
        public string GovtOrganName { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public int SchedulePlanID { get; set; }

        [DataMember]
        public int SchedulePlanNum{ get; set; }

        [DataMember]
        public string PlanSubjectName { get; set; }

        [DataMember]
        public string PlanActivityName { get; set; }

        [DataMember]
        public string PlanWorkName { get; set; }

        [DataMember]
        public string PlanSpecType { get; set; }

        [DataMember]
        public DateTime SchedulePlanExecTime { get; set; }

        [DataMember]
        public string OKDPCodeName { get; set; }

        [DataMember]
        public string GRBSCodeName { get; set; }

        [DataMember]
        public string SectionKBKCodeName { get; set; }
                                   
        [DataMember]
        public string ExpenseItem { get; set; }

        [DataMember]
        public string WorkForm { get; set; }

        [DataMember]
        public decimal Cost { get; set; }
    }
}