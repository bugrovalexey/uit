﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class StateProgramMarkMap : ClassMapHiLo<StateProgramMark>
    {
        public StateProgramMarkMap()
            : base("State_Programme_Mark")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "State_Programme_Mark_Name").Not.Nullable();
            References(x => x.StateProgram, "State_Programme_ID").Not.Nullable();
        }
    }
}