﻿using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Expertise;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpCmsnUserMap : ClassMapBase<ExpCmsnUser>
	{
        public ExpCmsnUserMap()
            : base("Exp_Cmsn_User")
		{
//            Table("w_");

//            Id(x => x.Id, "Exp_Cmsn_User_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            SqlInsert(@"exec ZZZ_Prc_Exp_Cmsn_User_Insert
//						  @Exp_Cmsn_Meeting_ID = ?
//						 ,@Users_ID = ?
//						 ,@Exp_Cmsn_User_ID = ?").Check.None();
			
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Cmsn_User_Update
//						  @Exp_Cmsn_Meeting_ID = ?
//						 ,@Users_ID = ?
//						 ,@Exp_Cmsn_User_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Cmsn_User_Delete
//						  @Exp_Cmsn_User_ID = ?").Check.None();
		}

        protected override void InitMap()
        {
            References<ExpCmsnMeeting>(x => x.ExpCmsnMeeting, "Exp_Cmsn_Meeting_ID").Not.Nullable();
			References<User>(x => x.User, "Users_ID").Not.Nullable();
        }
    }
}