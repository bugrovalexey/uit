﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class AddOrSaveAOAdoptionBudgetContext : BaseAddOrSaveContext
    {        
        public DateTime? AdoptionBudgetDate { get; set; }
        
        public int? BK_GRBS { get; set; }
        
        public int? BK_PZRZ { get; set; }
        
        public int? BK_CSR { get; set; }
        
        public int? BK_WR { get; set; }
        
        public decimal BalanceCost { get; set; }

        public int AccountingObjectId { get; set; }

        
        public string Name { get; set; }
        
        public string Number { get; set; }
        
        public DateTime? DateAdoption { get; set; }
        
        public string Paragraph { get; set; }
        
        public string Files { get; set; }
    }
}
