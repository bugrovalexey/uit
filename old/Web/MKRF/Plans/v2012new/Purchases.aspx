﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/v2012new/ActivityCard.master" AutoEventWireup="true" CodeBehind="Purchases.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.Purchases" %>

<asp:Content ID="Content1" ContentPlaceHolderID="detailTopContent" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="center" runat="server">

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />

    <script type="text/javascript">
        
        function hfGridSave(e) {
            hf.Set('LotZk', ZkComboBox.GetValue());
            hf.Set('LotNum', NumTextBox.GetText());
            hf.Set('LotCode', CodeTextBox.GetValue());
            hf.Set('LotName', NameTextBox.GetText());
            hf.Set('LotUnit', UnitTextBox.GetValue());
            hf.Set('LotCount', CountTextBox.GetText());
            hf.Set('Price', PriceTextBox.GetText());
        }

        function hfGridZKSave(e) {

            hf.Set('NameZk', NameZkMemo.GetText());
            hf.Set('PublishDate', PublishDateEdit.GetValue());
            hf.Set('OrderWay', OrderWayComboBox.GetValue());
            hf.Set('Status', StatusComboBox.GetValue());
            hf.Set('IzvNumb', NumZkTextBox.GetText());
            hf.Set('Site', SiteTextBox.GetText());
            hf.Set('IzvDate', IzvDateEdit.GetValue());
            hf.Set('OpenDate', OpenDateEdit.GetValue());
            hf.Set('ConsDate', ConsDateEdit.GetValue());
            hf.Set('ItogDate', ItogDateEdit.GetValue());
        }
        
        function RefreshGridZakupki()
        {
            var f = hf.Get('UPDATE_LOT');

            if(f == 1)
            {
                GridZk.Refresh();
                hf.Set('UPDATE_LOT', 0);
            }
        }

        function RefreshGridLots() {
            var f = hf.Get('UPDATE_ZK');

            if (f == 1) {
                Grid.Refresh();
                hf.Set('UPDATE_ZK', 0);
            }
        }

    </script>

    <p class="header_grid" style="">
        Информация о закупке
    </p>
    <dxe:ASPxButton ID="AddButtonZk" runat="server" 
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) { GridZk.AddNewRow(); }" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="GridZk" Width="100%" ClientInstanceName="GridZk"
        KeyFieldName="Id"
        OnBeforePerformDataSelect="GridZk_BeforePerformDataSelect"
        OnRowInserting="GridZk_RowInserting"
        OnRowUpdating="GridZk_RowUpdating"
        OnStartRowEditing="GridZk_StartRowEditing"
        OnRowDeleting="GridZk_RowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False"
                AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <%--<dxwgv:GridViewDataTextColumn FieldName="Number" Caption="Порядковый номер" />--%>
            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Наименование объекта закупки" />
            <dxwgv:GridViewDataColumn Caption="Страница на официальном сайте">
                <DataItemTemplate>
                    <%# GetLink(Eval("Site") as string) %>
                </DataItemTemplate>
            </dxwgv:GridViewDataColumn>
            <dxwgv:GridViewDataDateColumn FieldName="PublishDate" Caption="Планируемая дата публикации" />
            <dxwgv:GridViewDataDateColumn FieldName="OrderWay.Name" Caption="Способ размещения заказа" />
            <dxwgv:GridViewDataTextColumn FieldName="Status.Name" Caption="Статус" />
            <dxwgv:GridViewDataTextColumn FieldName="IzvNumb" Caption="Номер извещения" />
            <dxwgv:GridViewDataDateColumn FieldName="IzvDate" Caption="Дата извещения" />
            <dxwgv:GridViewDataDateColumn FieldName="OpenDate" Caption="Дата вскрытия конвертов" />
            <dxwgv:GridViewDataDateColumn FieldName="ConsDate" Caption="Дата рассмотрения заявок" />
            <dxwgv:GridViewDataDateColumn FieldName="ItogDate" Caption="Дата подведения итогов" />
            <dxwgv:GridViewDataTextColumn FieldName="Summ" Caption="Сумма" />
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Планируемая дата публикации
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="PublishDateEdit" ClientInstanceName="PublishDateEdit" OnDataBinding="PublishDateEdit_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Способ размещения заказа
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="OrderWayComboBox" runat="server" ClientInstanceName="OrderWayComboBox"
                                TextField="Name" ValueField="Id" ValueType="System.Int32" 
                                OnDataBinding="OrderWayComboBox_DataBinding">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Наименование объекта закупки
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo ID="NameZkMemo" runat="server" ClientInstanceName="NameZkMemo" OnDataBinding="NameZkMemo_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Статус 
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="StatusComboBox" runat="server" ClientInstanceName="StatusComboBox"
                                TextField="Name" ValueField="Id" ValueType="System.Int32" 
                                OnDataBinding="StatusComboBox_DataBinding">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Номер извещения
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="NumZkTextBox" ClientInstanceName="NumZkTextBox" OnDataBinding="NumZkTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Страница на официальном сайте
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="SiteTextBox" ClientInstanceName="SiteTextBox" OnDataBinding="SiteTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Дата извещения
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="IzvDateEdit" ClientInstanceName="IzvDateEdit" OnDataBinding="IzvDateEdit_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Дата вскрытия конвертов
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="OpenDateEdit" ClientInstanceName="OpenDateEdit" OnDataBinding="OpenDateEdit_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Дата рассмотрения заявок
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="ConsDateEdit" ClientInstanceName="ConsDateEdit" OnDataBinding="ConsDateEdit_DataBinding"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Дата подведения итогов
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="ItogDateEdit" ClientInstanceName="ItogDateEdit" OnDataBinding="ItogDateEdit_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <%--<TotalSummary>
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY0" ShowInColumn="PlansActivityVolY0" SummaryType="Sum" />
        </TotalSummary>--%>
        <ClientSideEvents 
            BeginCallback="
            function(s,e)
            { 
                if (e.command=='UPDATEEDIT') hfGridZKSave(e); 
                if(e.command=='DELETEROW') hf.Set('UPDATE_ZK', 1);
            }" 
            EndCallback="function(s,e){ RefreshGridLots(); }"/>
    </dxwgv:ASPxGridView>
    <p class="header_grid">
        Лоты
    </p>

    <dxe:ASPxButton ID="AddButton" runat="server" 
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) { Grid.AddNewRow(); }" />
    </dxe:ASPxButton>
    <dxwgv:ASPxGridView runat="server" ID="Grid" Width="100%" ClientInstanceName="Grid"
        KeyFieldName="Id"
        OnBeforePerformDataSelect="Grid_BeforePerformDataSelect"
        OnRowInserting="Grid_RowInserting"
        OnRowUpdating="Grid_RowUpdating"
        OnStartRowEditing="Grid_StartRowEditing"
        OnRowDeleting="Grid_RowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False"
                AllowDragDrop="False">
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Zakupki.NumberName" Caption="Закупка" GroupIndex="1" />
            <dxwgv:GridViewDataTextColumn FieldName="Number" Caption="Номер" />
            <dxwgv:GridViewDataTextColumn FieldName="Code" Caption="Код ОКПД" />
            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Наименование" />
            <dxwgv:GridViewDataTextColumn FieldName="Unit" Caption="Единица измерения" />
            <dxwgv:GridViewDataTextColumn FieldName="Count" Caption="Количество" />
            <dxwgv:GridViewDataTextColumn FieldName="Price" Caption="Стоимость" >
                <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior AutoExpandAllGroups="true" />
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc required">Закупка
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="ZkComboBox" runat="server" ClientInstanceName="ZkComboBox"  ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                TextField="NumberName" ValueField="Id" ValueType="System.Int32" 
                                OnDataBinding="ZkComboBox_DataBinding">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Номер лота:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="NumTextBox" ClientInstanceName="NumTextBox" OnDataBinding="NumTextBox_DataBinding"  ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                    <RegularExpression ErrorText="Только целые числа" ValidationExpression="[0-9]+" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Код ОКПД:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="CodeTextBox" ClientInstanceName="CodeTextBox" OnDataBinding="CodeTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Наименование лота:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="NameTextBox" ClientInstanceName="NameTextBox" OnDataBinding="NameTextBox_DataBinding"  ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Единица измерения:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="UnitTextBox" ClientInstanceName="UnitTextBox" OnDataBinding="UnitTextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Количество:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="CountTextBox" ClientInstanceName="CountTextBox" OnDataBinding="CountTextBox_DataBinding"  ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>" >
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RegularExpression ErrorText="Только целые числа" ValidationExpression="[0-9]+" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Стоимость:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="PriceTextBox" ClientInstanceName="PriceTextBox" OnDataBinding="PriceTextBox_DataBinding"
                                ValidationSettings-ValidationGroup="<%#Container.ValidationGroup %>">
                                <%--<MaskSettings Mask="<0..999999999g>.<0000..9999>" IncludeLiterals="DecimalSymbol" />--%>
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RegularExpression ValidationExpression="([0-9]*)[.,/]?([0-9]+)" ErrorText="введите сумму" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents 
            BeginCallback="
            function(s,e)
            {  
                if (e.command=='UPDATEEDIT') hfGridSave(e); 
                if(e.command=='DELETEROW' || e.command=='UPDATEEDIT') hf.Set('UPDATE_LOT', 1);
            }"
            EndCallback="function(s,e){ RefreshGridZakupki(); }" />
    </dxwgv:ASPxGridView>
</asp:Content>
