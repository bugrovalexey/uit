﻿using GB.Data.AccountingObjects;
using GB.Data.Directories;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateStageAccountingObjectCommand : ICommand<UpdateStageAccountingObjectContext>
    {
        ISessionFactoryGb factory;

        public UpdateStageAccountingObjectCommand()
        {
            factory = new SessionFactory();
        }

        public void Execute(UpdateStageAccountingObjectContext context)
        {
            var ao = new Repository<AccountingObject>().GetExisting(context.Id);

            ao.Stage = (AccountingObjectsStageEnum)context.StageId;

            using (var session = factory.Create())
            {
                session.Update(ao);
                session.Commit();
            }
        }
    }
}
