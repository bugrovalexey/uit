﻿using GB.Data.Common;
using GB.Data.Plans;

namespace GB._Mapping.Plans
{
    internal class ActivityCommentMap : ClassMapHiLo<ActivityComment>
    {
        public ActivityCommentMap()
            : base("Activity_Comment") { }

        protected override void InitMap()
        {
            Map(x => x.Page, "Activity_Comment_Page");
            Map(x => x.Text, "Activity_Comment_Text");
            Map(x => x.Date, "Activity_Comment_Create_DT").ReadOnly();

            References<User>(x => x.User, "Users_ID").Not.Nullable();
            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID").Not.Nullable();
        }
    }
}