﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.Workflow
{
    public class CreateMultiContext : ICommandContext
    {
        public Data.AccountingObjects.AccountingObject AO { get; set; }
    }
}
