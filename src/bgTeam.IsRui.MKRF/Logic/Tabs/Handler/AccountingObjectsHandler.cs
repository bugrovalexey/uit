﻿using bgTeam.IsRui.MKRF.Providers;

namespace bgTeam.IsRui.MKRF.Logic.Tabs.Handler
{
    internal class AccountingObjectsHandler : TabsBaseHandler
    {
        public AccountingObjectsHandler(IAccessControllerProvider accessControllerProvider)
            : base("AccountingObjects", accessControllerProvider)
        {
            Add("Мои объекты учета", "List");
            Add("На согласовании", "ListApprove");
            //Add("Согласованные", "ListMyApprove");
        }
    }
}