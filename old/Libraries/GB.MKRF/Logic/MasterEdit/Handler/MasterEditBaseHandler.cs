﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.MKRF.Logic.MasterEdit.Handler
{
    abstract class MasterEditBaseHandler : IMasterEditHandler
    {
        readonly IDictionary<MasterItemEnum, MasterItem> _ListPage;

        public MasterEditBaseHandler()
        {
            _ListPage = new Dictionary<MasterItemEnum, MasterItem>();

            var list = GetListPage();

            for (int i = 1; i < list.Length - 1; i++)
            {
                var current = list[i];
                var prev = list[i - 1];
                var next = list[i + 1];

                _ListPage.Add(list[i].Item, new MasterItem()
                {
                    Position = i,
                    Title = current.Title,
                    TitleStep = current.TitleStep ?? MasterItemEnumDescription.GetStepDescription(list[i].Item),
                    Action = current.Item.ToString(),
                    Params = current.Params,
                    Prev = prev.Item.ToString(),
                    PrevParams = prev.Params,
                    Next = next.Item.ToString(),
                    NextParams = next.Params,
                    Item = list[i].Item,
                });
            }
        }

        protected abstract MasterInItem[] GetListPage();

        public MasterItem First()
        {
            return _ListPage.First().Value;
        }

        public MasterItem GetItem(MasterItemEnum e)
        {
            return _ListPage[e];
        }

        public IEnumerable<MasterItem> GetItems()
        {
            return _ListPage.Values;
        }
    }

    class MasterInItem
    {
        public MasterItemEnum Item { get; set; }
        public object Params { get; set; }

        public string Title { get; set; }
        public string TitleStep { get; set; }

        public MasterInItem(MasterItemEnum item)
        {
            this.Item = item;
        }

        public MasterInItem(MasterItemEnum item, object p)
        {
            this.Item = item;
            this.Params = p;
        }
    }
}
