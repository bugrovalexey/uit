﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Domain.Entities.Documents;
    using System.Threading.Tasks;
    using bgTeam.Extensions;

    public class ActivityGetActsStory : IStory<ActivityGetActsStoryContext, IEnumerable<ActivityActDto>>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;

        public ActivityGetActsStory(
            IRepositoryRui repository,
            IMapperBase mapperBase)
        {
            _repository = repository;
            _mapper = mapperBase;
        }

        public IEnumerable<ActivityActDto> Execute(ActivityGetActsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivityActDto>> ExecuteAsync(ActivityGetActsStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var activity = _repository.Get<PlansActivity>(context.ActivityId);

            if (activity == null)
            {
                throw new IsRuiException($"Не найдено мероприятие");
            }

            var acts = _repository.GetAllEx<PlansActivityAct>(q =>
                q.Where(x => x.Activity.Id == context.ActivityId));

            List<ActivityActDto> dtos = new List<ActivityActDto>();

            foreach (var act in acts)
            {
                var doc = _repository.Get<Files>(act.FileId);
                var dto = _mapper.Map(act, new ActivityActDto());

                dto.File = _mapper.Map(doc, new ActsFileDto());

                dtos.Add(dto);
            }

            return dtos;
        }
    }
}
