﻿using System;
using System.Collections.Generic;
using GB.Repository;

namespace GB.Helpers
{
    public class EntityHelper
    {
        /// <summary>
        /// Возвращает значение свойства сущности
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <typeparam name="TValue">Тип значения свойства</typeparam>
        /// <param name="id">ID сущности</param>
        /// <param name="selector">Выражение для получения значения свойств</param>
        /// <exception cref="ArgumentException">Сущность не найдена в системе</exception>
        public static TValue GetValue<TEntity, TValue>(int id, Func<TEntity, TValue> selector) where TEntity : EntityBase
        {
            TEntity entity = new RepositoryBaseNew<TEntity>().GetExisting(id);
            return selector(entity);
        }

        /// <summary>
        /// Преобразует коллекцию сущностей в справочник.
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="Data">Коллекция</param>
        /// <returns>Справочник</returns>
        public static Dictionary<int, T> ConvertToDictionary<T>(ICollection<T> Data) where T : EntityBase
        {
            Dictionary<int, T> dict = new Dictionary<int, T>();
            foreach (T item in Data)
            {
                dict.Add(item.Id, item);
            }
            return dict;
        }


        /// <summary>
        /// Делегат для конвертации объекта типа T в объект анонимного типа
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <param name="entity">Сущность</param>
        /// <returns>Анонимный тип</returns>
        public delegate object entityToVar<T>(T entity);

        /// <summary>
        /// Преобразует коллекцию типа T в коллекцию из элементов анонимного типа
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <param name="source">Исходная коллекция</param>
        /// <param name="func">Делегат для конвертации</param>
        /// <returns>Коллекция</returns>
        [Obsolete("Для этого есть Select в LINQ")]
        public static IList<object> ConvertToAnonymous<T>(ICollection<T> source, entityToVar<T> func)
        {
            IList<object> result = new List<object>();
            foreach (T item in source)
            {
                result.Add(func(item));
            }
            return result;
        }

        //public static void SaveList<T>(ICollection<T> source, DevExpress.Web.ASPxEditors.ASPxListBox listBox, entityToVarEx<T> func) where T : EntityBase
        //{
        //    foreach (T item in source)
        //    {
        //        RepositoryBase<T>.Delete(item.Id);
        //    }

        //    foreach (DevExpress.Web.ASPxEditors.ListEditItem item in listBox.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            T entity = Activator.CreateInstance<T>();

        //            func(entity, item.Value);

        //            RepositoryBase<T>.SaveOrUpdate(entity);
        //        }
        //    }
        //}
    }
}