﻿namespace bgTeam.IsRui.Zakupki360.Dto
{
    class RequestGetOrders
    {
        public SearchParametersOrders Filter { get; set; }

        public string ApiKey { get; set; }
    }
}
