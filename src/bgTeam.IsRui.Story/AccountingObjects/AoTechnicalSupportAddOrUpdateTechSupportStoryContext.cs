﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using System.Collections.Generic;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;

    public class AoTechnicalSupportAddOrUpdateTechSupportStoryContext
    {
        public int? Id { get; set; }

        public int? AoId { get; set; }

        public virtual int Amount { get; set; }

        public int? CategoryKindOfSupportId { get; set; }

        public IList<CharacteristicOfTechnicalSupport> Characteristics { get; set; }

        public int? ManufacturerId { get; set; }

        public string ManufacturerCustomName { get; set; }

        public string Name { get; set; }

        public decimal Summa { get; set; }
    }
}
