﻿namespace GB.MKRF.Logic.MasterEdit.Handler
{
    class AccountingObjectPlannedSAHandler : MasterEditBaseHandler
    {
        protected override MasterInItem[] GetListPage()
        {
            return new MasterInItem[] 
            {
                new MasterInItem(MasterItemEnum.None),
                new MasterInItem(MasterItemEnum.DocumentBase),
                new MasterInItem(MasterItemEnum.GoalsAndPurpose),
                new MasterInItem(MasterItemEnum.KindMasterEdit),
                new MasterInItem(MasterItemEnum.ResponsibleService),
                new MasterInItem(MasterItemEnum.SpecialActivity),
                new MasterInItem(MasterItemEnum.CharacteristicsCreate)
                {
                    Title = "Заполните функциональные характеристики объекта учета",
                },
                new MasterInItem(MasterItemEnum.ActivityCreate)
                {
                    Title = "Заполните сведения о мероприятии по информатизации направленном на создание данного объекта учета",
                },
                new MasterInItem(MasterItemEnum.Software)
                {
                    Title = "Заполните перечень программного обеспечения" +
                            " необходимого для функционирования объекта учета",
                },
                new MasterInItem(MasterItemEnum.TechnicalSupport)
                {
                    Title = "Заполните перечень технического " +
                            "оборудования необходимого для функционирования объекта учета",
                },
                
                new MasterInItem(MasterItemEnum.WorkAndService)
                {
                    Title = "Заполните перечень работ",
                },
                //new MasterInItem(MasterItemEnum.CharacteristicsCreate)
                //{
                //    Title = "Заполните характеристики объекта учета",
                //},
                //new MasterInItem(MasterItemEnum.InformationAboutSecurity),
                new MasterInItem(MasterItemEnum.InformationInteractionCreate),
                new MasterInItem( MasterItemEnum.Purchase)
                {
                    Title = "Выберите закупки направленные на данный объект учета",
                },
                new MasterInItem(MasterItemEnum.CardForRead)
            };
        }
    }
}
