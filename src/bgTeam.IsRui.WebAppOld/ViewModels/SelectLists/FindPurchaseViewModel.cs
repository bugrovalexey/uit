﻿using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.Zakupki360.Dto;
using System.ComponentModel;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.SelectLists
{
    public class FindPurchaseViewModel
    {
        public int Id { get; set; }

        [DisplayName("Реестровый номер")]
        public string Number { get; set; }

        [DisplayName("Тип")]
        public int? Type { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Дата публикации c")]
        public string FromDate { get; set; }

        [DisplayName("Дата публикации по")]
        public string ToDate { get; set; }

        [DisplayName("Стоимость ")]
        public object Price { get; set; }
        
        [DisplayName("Сумма расходов на данный объект учета")]
        public object Summ { get; set; }


        public SelectList TypeList { get; set; }


        public FindPurchaseViewModel(int id)
        {
            Id = id;

            TypeList = new SelectList(EnumExtensions.ToKeyValuePairs<PurchaseTypeEnum>(), "Key", "Value");
        }

        
    }
}