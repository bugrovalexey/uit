﻿using GB;
using GB.Helpers;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using System;
using System.Linq;
using GB.Controls;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class Purchases : System.Web.UI.Page
    {
        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as ActivityCard).CurrentActivity;
            }
        }
        
        public static string GetUrl(int ActId)
        {
            return UrlHelper.Resolve("~/Plans/v2012new/Purchases.aspx") + "?" + ActivityCard.ActivityIdKey + "=" + ActId;
        }


        
        protected override void OnPreRender(EventArgs e)
        {
            SetAccess((Master as ActivityCard).EnableEdit);

            GridZk.DataBind();
            Grid.DataBind();

            base.OnPreRender(e);
        }

        private void SetAccess(bool acs)
        {
            AddButtonZk.Visible = acs;
            GridZk.Columns["cmdColumn"].Visible = acs;

            AddButton.Visible = acs;
            Grid.Columns["cmdColumn"].Visible = acs;
        }

        protected string GetLink(string url)
        {
            if (!string.IsNullOrWhiteSpace(url))
                return string.Format("<a target='_blank' href='{0}'>Перейти по ссылке</a>", url);

            return string.Empty;
        }


        #region Grid Zakupki
        PlansZakupkiController zc = new PlansZakupkiController();
        protected void GridZk_BeforePerformDataSelect(object sender, EventArgs e)
        {
            (sender as ASPxGridView).DataSource = CurrentActivity.PlansZakupki.OrderBy(x => x.Number).Select(x => new
            {
                Id = x.Id,
                Number = x.Number,
                Status = x.Status,
                OrderWay = x.OrderWay,
                Activity = x.Activity,
                PublishDate = x.PublishDate,
                Name = x.Name,
                IzvNumb = x.IzvNumb,
                IzvDate = x.IzvDate,
                OpenDate = x.OpenDate,
                ConsDate = x.ConsDate,
                ItogDate = x.ItogDate,
                Site = x.Site,
                Summ = x.Lots.Sum(l => l.Price),
            });
        }

        protected void GridZk_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            zc.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void GridZk_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            var pz = zc.Create(new PlansZakupkiControllerArgs()
            {
                Activity = CurrentActivity,
                Name = hf.Get("NameZk"),
                PublishDate = hf.Get("PublishDate"),
                OrderWay = hf.Get("OrderWay"),
                Status = hf.Get("Status"),
                IzvNumb = hf.Get("IzvNumb"),
                IzvDate = hf.Get("IzvDate"),
                OpenDate = hf.Get("OpenDate"),
                ConsDate = hf.Get("ConsDate"),
                ItogDate = hf.Get("ItogDate"),
                Site = hf.Get("Site"),
                
            });

            zc.Refresh(pz);

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void GridZk_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            zc.Update(new PlansZakupkiControllerArgs()
            {
                Id = e.Keys[0],
                Name = hf.Get("NameZk"),
                PublishDate = hf.Get("PublishDate"),
                OrderWay = hf.Get("OrderWay"),
                Status = hf.Get("Status"),
                IzvNumb = hf.Get("IzvNumb"),
                IzvDate = hf.Get("IzvDate"),
                OpenDate = hf.Get("OpenDate"),
                ConsDate = hf.Get("ConsDate"),
                ItogDate = hf.Get("ItogDate"),
                Site = hf.Get("Site"),
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void GridZk_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            zc.Delete(new PlansLotsArgs()
            {
                Id = e.Keys[0]
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void StatusComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = RepositoryBase<PlansZakupkiStatus>.GetAll();

            box.Value = zc.CurrentEdit == null ? EntityBase.Default : zc.CurrentEdit.Status.Id;
        }

        protected void OrderWayComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = RepositoryBase<OrderWay>.GetAll();

            box.Value = zc.CurrentEdit == null ? EntityBase.Default : zc.CurrentEdit.OrderWay.Id;
        }

        protected void PublishDateEdit_DataBinding(object sender, EventArgs e)
        {
            ASPxDateEdit box = sender as ASPxDateEdit;

            box.Value = zc.CurrentEdit == null ? null : zc.CurrentEdit.PublishDate;
        }

        protected void NameZkMemo_DataBinding(object sender, EventArgs e)
        {
            ASPxMemo box = sender as ASPxMemo;

            box.Value = zc.CurrentEdit == null ? null : zc.CurrentEdit.Name;
        }

        protected void NumZkTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = sender as ASPxTextBox;

            box.Value = zc.CurrentEdit == null ? null : zc.CurrentEdit.IzvNumb;
        }

        protected void SiteTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = sender as ASPxTextBox;

            box.Value = zc.CurrentEdit == null ? null : zc.CurrentEdit.Site;
        }

        protected void IzvDateEdit_DataBinding(object sender, EventArgs e)
        {
            ASPxDateEdit box = sender as ASPxDateEdit;

            box.Value = zc.CurrentEdit == null ? null : zc.CurrentEdit.IzvDate;
        }

        protected void OpenDateEdit_DataBinding(object sender, EventArgs e)
        {
            ASPxDateEdit box = sender as ASPxDateEdit;

            box.Value = zc.CurrentEdit == null ? null : zc.CurrentEdit.OpenDate;
        }

        protected void ConsDateEdit_DataBinding(object sender, EventArgs e)
        {
            ASPxDateEdit box = sender as ASPxDateEdit;

            box.Value = zc.CurrentEdit == null ? null : zc.CurrentEdit.ConsDate;
        }

        protected void ItogDateEdit_DataBinding(object sender, EventArgs e)
        {
            ASPxDateEdit box = sender as ASPxDateEdit;

            box.Value = zc.CurrentEdit == null ? null : zc.CurrentEdit.ItogDate;
        } 
        #endregion
     
   
        #region Grid Lots
        PlansLotsController lc = new PlansLotsController();
        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            (sender as ASPxGridView).DataSource = CurrentActivity.Lots;
        }


        protected void Grid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            lc.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void Grid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            lc.Create(new PlansLotsArgs()
            {
                Activity = CurrentActivity,
                Zakupki = hf["LotZk"],
                Number = hf["LotNum"],
                Code = hf["LotCode"],
                Name = hf["LotName"],
                Unit = hf["LotUnit"],
                Count = hf["LotCount"],
                Price = hf["Price"],
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void Grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            lc.Update(new PlansLotsArgs()
            {
                Id = e.Keys[0],
                Zakupki = hf["LotZk"],
                Number = hf["LotNum"],
                Code = hf["LotCode"],
                Name = hf["LotName"],
                Unit = hf["LotUnit"],
                Count = hf["LotCount"],
                Price = hf["Price"],
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void Grid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            lc.Delete(new PlansLotsArgs()
            {
                Id = e.Keys[0]
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }


        protected void ZkComboBox_DataBinding(object sender, EventArgs e)
        {
            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = CurrentActivity.PlansZakupki;

            box.Value = lc.CurrentEdit == null ? null : (int?)lc.CurrentEdit.Zakupki.Id;
        }

        protected void NumTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = sender as ASPxTextBox;

            box.Value = lc.CurrentEdit == null ? null : lc.CurrentEdit.Number;
        }

        protected void CodeTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = sender as ASPxTextBox;

            box.Value = lc.CurrentEdit == null ? string.Empty : lc.CurrentEdit.Code;
        }

        protected void NameTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = sender as ASPxTextBox;

            box.Value = lc.CurrentEdit == null ? string.Empty : lc.CurrentEdit.Name;
        }

        protected void UnitTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = sender as ASPxTextBox;

            box.Value = lc.CurrentEdit == null ? string.Empty : lc.CurrentEdit.Unit;
        }

        protected void CountTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = sender as ASPxTextBox;

            box.Value = lc.CurrentEdit == null ? null : lc.CurrentEdit.Count;
        } 
        
        protected void PriceTextBox_DataBinding(object sender, EventArgs e)
        {
            ASPxTextBox box = sender as ASPxTextBox;

            box.Value = lc.CurrentEdit == null ? 0 : lc.CurrentEdit.Price;
        }
        #endregion
    }
}