﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.Domain.Dto;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class FinancingReportStoryContext
    {
        public string Year { get; set; }

        public string Title { get; set; }

        public string TitleYaer { get; set; }

        public List<TemplateBlockDto> PositionsBlocks { get; set; }
    }
}
