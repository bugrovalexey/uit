﻿using System;
using System.Text;
using GB.Helpers;
using GB.MKRF.Report;
using GB.MKRF.Web.Services.Registry;
using GB.Report;

namespace GB.MKRF.Web
{
    public partial class ReportPage : System.Web.UI.Page
    {
        /// <summary>
        /// Надо удалить (Устарело не используем)
        /// </summary>
        public static string GetUrl(ReportType ReportType, params object[] Parameters)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(UrlHelper.Resolve("~/Report.aspx"));
            builder.Append("?");
            builder.Append(ReportHelper.RepType);
            builder.Append("=");
            builder.Append(ReportType);

            if (Parameters != null)
            {
                int count = 1;
                foreach (object par in Parameters)
                {
                    builder.Append("&p");
                    builder.Append(count);
                    builder.Append("=");
                    builder.Append(par);
                	count++;
                }
            }
            return builder.ToString();
        }

        public static string GetLink(string name, ReportType ReportType, params object[] parameters)
        {
            return ReportHelper.CreateLink(name, ReportType, parameters);
        }

        //protected void Page_Load(object sender, EventArgs e)
        //{
            
        //}

        protected override void OnPreRender(EventArgs e)
        {
            if (Request.QueryString[ReportHelper.RepType] != null)
            {
                ReportType type = (ReportType)Enum.Parse(typeof(ReportType), Request.QueryString[ReportHelper.RepType]);

                this.PrintControl.ReportData = new ReportData(type, ReportHelper.GetParams(Request.Params));

                //AddData(type);
            }
            
            base.OnPreRender(e);
        }

        //private void AddData(ReportType type)
        //{
        //    //if (type == ReportType.PassportIS)
        //    //{
        //    //    this.PrintControl.OnReportCreate += new PrintControlEventHandler(PrintControl_PassportIS);
        //    //}
        //    //if (type == ReportType.ReportByPlanFull)
        //    //{
        //    //    this.PrintControl.OnReportCreate += new PrintControlEventHandler(PrintControl_ReportByPlanFull);
        //    //}
        //}

        //void PrintControl_PassportIS(object sender, PrintControlEventArgs e)
        //{
        //    object[] param = ReportHelper.GetParams(Request.Params);

        //    int id = -1;

        //    if (param.Length > 0)
        //    {
        //        if (!int.TryParse(param[0].ToString(), out id))
        //            return;
        //    }

        //    RegistryServiceSoapClient client = new RegistryServiceSoapClient();

        //    PassportData ps = client.GetPassport(id);

        //    e.Report.RegBusinessObject("Passport", ps);
        //    e.Report.RegData("Services", ps.Services);
        //    e.Report.RegData("Functional", ps.Functional);
        //    e.Report.RegData("Responsibles", ps.Responsibles);
        //    e.Report.RegData("Contracts", ps.Contracts);
        //}

        //private void PrintControl_ReportByPlanFull(object sender, PrintControlEventArgs e)
        //{
        //    object[] p = ReportHelper.GetParams(Request.Params);

        //    Stimulsoft.Report.StiReport r1 = ReportCreator.Create(new ReportData(ReportType.ReportByPlan_1, p));
        //    Stimulsoft.Report.StiReport r2 = ReportCreator.Create(new ReportData(ReportType.ReportByPlan_2, p));
        //    Stimulsoft.Report.StiReport r3 = ReportCreator.Create(new ReportData(ReportType.ReportByPlan_3, p));
        //    Stimulsoft.Report.StiReport r4 = ReportCreator.Create(new ReportData(ReportType.ReportByPlan_4, p));
        //    Stimulsoft.Report.StiReport r5 = ReportCreator.Create(new ReportData(ReportType.ReportByPlan_5, p));

        //    e.Report = r1;
        //    e.Report.SubReports.Add(r2);
        //    e.Report.SubReports.Add(r3);
        //    e.Report.SubReports.Add(r4);
        //    e.Report.SubReports.Add(r5);
        //}
    }
}