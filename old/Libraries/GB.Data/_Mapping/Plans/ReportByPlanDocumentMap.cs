﻿using GB.MKRF.Entities.Plans;

namespace GB.MKRF.Mapping.Plans
{
    class ReportByPlanDocumentMap : ClassMapBase<ReportByPlanDocument>
    {
        public ReportByPlanDocumentMap() :
            base("Report_By_Plan_Document")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Comment, "Report_By_Plan_Document_Comment");
            Map(x => x.File, "Report_By_Plan_Document_File").Length(int.MaxValue).LazyLoad();
            Map(x => x.FileName, "Report_By_Plan_Document_Name");
            Map(x => x.ContentType, "Report_By_Plan_Document_Content_Type");
            Map(x => x.DateCreate, "Report_By_Plan_Document_Create_DT").ReadOnly();

            References<OfferByPlan>(x => x.OfferByPlan, "Offer_By_Plan_ID").Not.Nullable();
        }
    }
}