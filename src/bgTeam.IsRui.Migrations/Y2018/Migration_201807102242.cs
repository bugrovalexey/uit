﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807102242)]
    public class Migration_201807102242 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Information_Exchange").AlterColumn("Information_Exchange_Name").AsString(500).Nullable();
        }
    }
}
