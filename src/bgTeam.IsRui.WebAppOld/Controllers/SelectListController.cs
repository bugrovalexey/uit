﻿using bgTeam.IsRui.WebApp.ViewModels.Admin.SiteMap;
using bgTeam.IsRui.WebApp.ViewModels.SelectLists;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master;
using SiteMapNode = bgTeam.IsRui.Domain.Entities.Admin.SiteMapNode;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public class SelectListController : Controller
    {
        private readonly IUserRepository _userRepository;

        public SelectListController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        //
        // GET: /SelectList/

        public ActionResult UsersMyDepartment(string selectListCallback, bool multiple, string value)
        {
            //var excludeIds = GetValues(value);

            IList<User> list = new RepositoryDc<User>().GetAllEx(q => q
                .Where(x => x.IsActive
                        && x.Department == _userRepository.GetCurrent().Department)
                    .OrderBy(x => x.Surname).Asc
                //.Fetch(f => f.Roles).Eager //в GetAllEx так нельзя, записи задваиваются
                    .Fetch(f => f.Department).Eager.Clone());

            // Из всех полей, ссылающихся на справочник пользователей, надо убрать админа
            //list = list.Except(list.Where(user => user.Roles.Any(role => role.Id == (int)RoleEnum.Admin))).ToList();

            var m = new SelectListViewModel()
            {
                Data = list
                    .Select(user => new
                    {
                        user.Id,
                        Name = user.FullName,
                        Role = string.Join("; ", user.Roles.Select(x => x.Name)),
                        Department = user.Department.Name, //string.Concat(user.Department.Code, " - ", user.Department.Name),
                        Job = user.Job,
                        Email = user.Email,
                        Phone = string.IsNullOrWhiteSpace(user.WorkPhone) ? user.MobPhone : user.WorkPhone
                    }),
                SelectListCallback = selectListCallback,
                IsMultipleSelect = multiple
            };
            //SelectListViewModel

            return View(m);
        }

        public ActionResult ResponsiblesMyDepartment(string selectListCallback, bool multiple, string value)
        {
            var list = new RepositoryDc<Responsible>().GetAllEx(q => q
                .Where(x => x.IsActive
                        && x.Department == _userRepository.GetCurrent().Department)
                    .OrderBy(x => x.Name).Asc
                    .Fetch(f => f.Department).Eager
                    .Fetch(f => f.Job).Eager.Clone());

            var m = new SelectListViewModel<TableResponsible>()
            {
                Data = list.Select(responsible => new TableResponsible(responsible)),
                SelectListCallback = selectListCallback,
                IsMultipleSelect = multiple
            };

            return View(m);
        }

        public ActionResult WorksAndGoods(string selectListCallback, bool multiple, string value,
            int accountingObjectId)
        {
            var list = new RepositoryDc<WorkAndGoods>().GetAllEx(q => q
                    .Fetch(f => f.CategoryKindOfSupport).Eager
                    .Where(x => x.AccountingObject.Id == accountingObjectId && x.ActOfAcceptance == null));

            var m = new SelectListViewModel<TableWorkAndGoods>()
            {
                Data = list.Select(x => new TableWorkAndGoods(x)),
                SelectListCallback = selectListCallback,
                IsMultipleSelect = multiple
            };

            return View(m);
        }


        public ActionResult UsersRole(string selectListCallback, bool multiple, string value, string ignore, string roles)
        {
            if (roles == null)
                return View(new SelectListViewModel()
                {
                    Data = null,
                    SelectListCallback = selectListCallback,
                    IsMultipleSelect = multiple
                });

            var listId = roles.Split(';').Select(x => int.Parse(x));
            var listIgnore = string.IsNullOrEmpty(ignore) ? new int[] { } : ignore.Split(';').Select(x => int.Parse(x));

            var list = new RepositoryDc<UserToRole>().GetAllEx(q => q
                .WhereRestrictionOn(x => x.User).Not.IsIn(listIgnore.ToArray())
                .WhereRestrictionOn(x => x.Role).IsIn(listId.ToArray())
                .JoinQueryOver(x => x.User, NHibernate.SqlCommand.JoinType.InnerJoin)
                .Where(x => x.IsActive && x.Department == _userRepository.GetCurrent().Department));

            var m = new SelectListViewModel()
            {
                Data = list
                    .Select(x => new
                    {
                        x.User.Id,
                        Name = x.User.FullName,
                        Role = string.Join("; ", x.User.Roles.Select(z => z.Name)),
                        Department = x.User.Department.Name, //string.Concat(user.Department.Code, " - ", user.Department.Name),
                        Job = x.User.Job,
                        Email = x.User.Email,
                        Phone = string.IsNullOrWhiteSpace(x.User.WorkPhone) ? x.User.MobPhone : x.User.WorkPhone
                    }),
                SelectListCallback = selectListCallback,
                IsMultipleSelect = multiple
            };

            return View(m);
        }

        public ActionResult SiteMapList(string selectListCallback, bool multiple, string value, int exludeId)
        {
            var m = new SelectListViewModel()
            {
                Data = new RepositoryDc<SiteMapNode>().GetAll().Where(x => x.Id != exludeId).Select(x => new TableSiteMap(x)),
                SelectListCallback = selectListCallback,
                IsMultipleSelect = multiple
            };

            return View(m);
        }

        public ActionResult AccountingObjects(string selectListCallback, bool multiple, string value, int? exludeId = null)
        {
            var data = new AccountingObjectRepository().GetAllEx(q => q
                .Fetch(f => f.Type).Eager
                .Fetch(f => f.Kind).Eager
                .OrderBy(x => x.CreateDate).Desc
                .Where(x=> x.Department == _userRepository.GetCurrent().Department))
                .Select(x =>
                    new
                    {
                        Id = x.Id,
                        Name = x.ShortName,
                        Type = x.Type.Name,
                        Kind = x.Kind.Name
                    });

            exludeId.Do<int>(id =>
            {
                data = data.Except(data.Where(x => x.Id == id));
            });

            var m = new SelectListViewModel()
            {
                Data = data,
                SelectListCallback = selectListCallback,
                IsMultipleSelect = multiple
            };

            return View(m);
        }

        public ActionResult DictionaryDateTime(string selectListCallback, bool multiple, string value, string type)
        {
            System.Collections.IEnumerable list = null;

            int year = DateTime.Now.Year;

            switch (type.ToUpper())
            {
                case "CSR":
                    list = DirectoriesRepositary.GetDirectories<ExpenseItem>(year).OrderBy(x => x.Code);
                    break;

                case "GRBS":
                    list = DirectoriesRepositary.GetDirectories<GRBS>(year).OrderBy(x => x.Code);
                    break;

                case "SECTIONKBK":
                    list = DirectoriesRepositary.GetDirectories<SectionKBK>(year).OrderBy(x => x.Code);
                    break;

                case "WORKFORM":
                    list = DirectoriesRepositary.GetDirectories<WorkForm>(year).OrderBy(x => x.Code);
                    break;

                case "KOSGU":
                    list = DirectoriesRepositary.GetDirectories<ExpenditureItem>(year).OrderBy(x => x.Code);
                    break;
            }

            var m = new SelectListViewModel()
            {
                Data = list,
                SelectListCallback = selectListCallback,
                IsMultipleSelect = multiple
            };

            return View(m);
        }

        public ActionResult GovService(string selectListCallback, bool multiple, string value, string type)
        {
            var m = new SelectListViewModel()
            {
                Data = new RepositoryDc<bgTeam.IsRui.Domain.Entities.Frgu.GovService>().GetAll(),
                SelectListCallback = selectListCallback,
                IsMultipleSelect = multiple
            };

            return View(m);
        }

        public ActionResult IKTComponentGhild(int? id)
        {
            return
                Json(
                    new SelectList(new IKTComponentRepository().GetAllChildren(id.Value), "Id", "Name",
                        EntityBase.Default), JsonRequestBehavior.AllowGet);
        }

        public ActionResult StateProgramMarkByStateProgram(int id)
        {
            return
                Json(
                    new SelectList(new StateProgramMarkRepository().GetAllMarksByStateProgram(id), "Id", "Name"),
                    JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Возвращает типы расходов для указанного вида расходов
        /// </summary>
        /// <param name="id">id вида расходов</param>
        public ActionResult ExpenseTypes(int id)
        {
            var data = new RepositoryDc<ExpenseType>().GetAllEx(q => q.Where(x =>
                (x.ExpenseDirection != null && x.ExpenseDirection.Id == id) || x.Id == EntityBase.Default));

            return Json(new SelectList(data, "Id", "Name", EntityBase.Default), JsonRequestBehavior.AllowGet);
        }

        /// <summary>Возвращает подпрограммы или мероприятия государственной программы</summary>
        /// <param name="id">id госпрограммы</param>
        public ActionResult StatePrograms(int id)
        {
            var data = new RepositoryDc<StateProgram>().GetAllEx(q => q.Where(x =>
                (x.Parent != null && x.Parent.Id == id)
                || x.Id == EntityBase.Default));

            return Json(new SelectList(data, "Id", "Name", EntityBase.Default), JsonRequestBehavior.AllowGet);
        }

        //private static IList<int> GetValues(string value)
        //{
        //    return string.IsNullOrWhiteSpace(value)
        //         ? new List<int>() { -1 }
        //         : value.Split(',').Select(x => Convert.ToInt32(x)).ToList();
        //}

        /// <summary>
        /// Поиск контрактов - 360
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FindGovernmentContract(int id)
        {
            return View(new FindGovernmentContractViewModel(id));
        }

        /// <summary>
        /// Поиск закупок - 360
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FindPurchase(int id)
        {
            return View(new FindPurchaseViewModel(id));
        }


        
    }
}