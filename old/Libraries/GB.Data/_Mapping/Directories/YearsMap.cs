﻿using GB.Data.Directories;

namespace GB._Mapping.Directories
{
    internal class YearsMap : ClassMapBase<Years>
    {
        public YearsMap()
            : base("Year")
        {
            //Table("");
            //Id(x => x.Id).Column("Year").GeneratedBy.Assigned();

            //SqlInsert(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlUpdate(@"raiserror ('Unsupported',10,1)").Check.None();
            //SqlDelete(@"raiserror ('Unsupported',10,1)").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Value, "Year").Not.Nullable();
        }
    }
}