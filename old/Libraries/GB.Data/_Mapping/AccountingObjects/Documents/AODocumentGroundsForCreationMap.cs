﻿using GB.Data.AccountingObjects.Documents;
using GB.Data.Documents;
using GB.Data;

namespace GB._Mapping.AccountingObjects.Documents
{
    class AODocumentGroundsForCreationMap : AODocumentBaseMap<AODocumentGroundsForCreation>
    {
        protected DocTypeEnum Type
        {
            get { return AODocumentGroundsForCreation.DocType; }
        }

        protected override void AdditionalMap()
        {
            HasMany(x => x.Documents).KeyColumns.Add("Document_Owner_ID")
                .Where("Doc_Type_ID = " + (int)Type)
                .Where("Portal_Table_ID = " + (int)EntityType.AODocumentGroundsForCreation)
                .Inverse();
        }
    }
}
