﻿namespace bgTeam.IsRui.Story.Claims
{
    using System;

    public class ClaimCreateOrUpdateIndicatorStoryContext
    {
        public int? Id { get; set; }

        /// <summary>
        /// Заявка
        /// </summary>
        public int? ClaimId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Обоснование
        /// </summary>
        public string Justification { get; set; }

        /// <summary>
        /// Базовое значение
        /// </summary>
        public int? BaseValue { get; set; }

        /// <summary>
        /// Целевое значение
        /// </summary>
        public int? TargetedValue { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public int? UnitId { get; set; }

        /// <summary>
        /// Дата достижения целевого значения
        /// </summary>
        public DateTime? DateToAchieveGoalValue { get; set; }

        /// <summary>
        /// Алгоритм расчета
        /// </summary>
        public string AlgorithmOfCalculating { get; set; }

        /// <summary>
        /// Пояснение связи с показателем проекта
        /// </summary>
        public virtual string ExplanationRelationshipWithProjectMark { get; set; }
    }
}
