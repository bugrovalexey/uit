﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plans/v2012new/ActivityCard.master" AutoEventWireup="true" CodeBehind="Functions.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.Functions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function hfFuncSave(e) {
            if (FuncNameTextBox.GetText().length == 0 && !MissFuncASPxCheckBox.GetChecked()) {
                return false;
            }

            hfFunc.Set('Name', FuncNameTextBox.GetText());

            if (FuncTypeComboBox.GetValue() == 1)
                hfFunc.Set('ServiceId', '');

            hfFunc.Set('CommonFunction', CFComboBox.GetValue());
            hfFunc.Set('NPAName', FuncNPANameTextBox.GetText());
            hfFunc.Set('NPATypeId', FuncNPATypeComboBox.GetValue());
            hfFunc.Set('NPAKindId', FuncNPAKindComboBox.clientEnabled ? FuncNPAKindComboBox.GetValue() : -1);
            hfFunc.Set('NPANum', FuncNPANumTextBox.GetText());
            hfFunc.Set('NPADate', FuncNPADateDateEdit.GetValue());
            hfFunc.Set('NPAItem', FuncItemTextBox.GetText());
            hfFunc.Set('FuncId', FuncTypeComboBox.GetValue());
            hfFunc.Set('UsedBefore', UsedBeforeASPxCheckBox.GetValue());
            hfFunc.Set('IsMissFunc', MissFuncASPxCheckBox.GetChecked());
            hfFunc.Set('MissFuncName', MissFuncNameTextBox.GetText());
        }

        var updateRow = false;
        function FuncTypeComboBoxSelectedIndexChanged() {
            document.getElementById('trUsedBefore').style.display = (FuncTypeComboBox.GetValue() == 2) ? "" : "none";
            document.getElementById('trMissFunc').style.display = (FuncTypeComboBox.GetValue() == 2) ? "" : "none";
            document.getElementById('trMissFuncName').style.display = (FuncTypeComboBox.GetValue() == 2) ? "" : "none";

            if (FuncTypeComboBox.GetValue() == 1) {
                FuncNameTextBox.SetText('Автоматизация административно-хозяйственной деятельности');
                FuncNameTextBox.SetVisible(false);
                CFComboBox.SetVisible(true);
                document.getElementById('selectServiceLink').style.display = "none";

                VisibleEditPanel(false);
            }
            else {
                if (FuncNameTextBox.GetText() == 'Автоматизация административно-хозяйственной деятельности') {
                    FuncNameTextBox.SetText('');
                }
                FuncNameTextBox.SetVisible(true);
                CFComboBox.SetVisible(false);
                document.getElementById('selectServiceLink').style.display = "";
                VisibleEditPanel(false);
                ChangeMissFunc();
            }

            if (updateRow) {
                FuncTypeComboBox.SetEnabled(false);
                FuncNameTextBox.SetEnabled(false);
                MissFuncASPxCheckBox.SetEnabled(false);
                document.getElementById('tr7').style.display = "";
                document.getElementById('selectServiceLink').style.display = "none";
            }
        }

        function VisibleEditPanel(display) {
            var visible = display ? "" : "none";
            document.getElementById('tr1').style.display = visible;
            document.getElementById('tr2').style.display = visible;
            document.getElementById('tr3').style.display = visible;
            document.getElementById('tr4').style.display = visible;
            document.getElementById('tr5').style.display = visible;
            document.getElementById('tr6').style.display = visible;
            document.getElementById('tr7').style.display = visible;
        }

        function SetNPAKindComboBoxEnabledFromNPAType(cbNPAType, cbNPAKind) {
            var npaTypeID = cbNPAType.GetValue();
            var condition = npaTypeID != null && npaTypeID != 0 && npaTypeID != 6;
            if (!condition)
                cbNPAKind.SetValue(null);
            cbNPAKind.SetEnabled(condition);
        }

        function ChangeMissFunc() {
            var checked = MissFuncASPxCheckBox.GetChecked();

            document.getElementById('trMissFuncName').style.display = checked ? "" : "none";
            document.getElementById('selectServiceLink').style.display = !checked ? "" : "none";
            VisibleEditPanel(checked);
        }


        $(document).ready(function () {
            <% if (!AddFuncButton.Visible) {%>
            $('a .command, .catalogButton').each(function () {
                $(this).css("display", "none");
            });
            <% }%>

            UpdateButtonClear('FuncName');
        });

        function PopupClose(e) {
            FuncNamePopupControl.Hide();

            var mass = e.split(';');
            if (mass[0])
                hfFunc.Set('ServiceId', mass[0]);
            if (mass[1])
                FuncNameTextBox.SetText(mass[1]);

            UpdateButtonClear('FuncName');
        }

        function UpdateButtonClear(name) {
            var val = hfFunc.Get('ServiceId');

            if (val && val != 0) {
                var btName = '#' + name + 'TextBox';
                var bt = $(btName).parents('td.form_edit_input').find('div.ButtonClear');

                bt.css('display', '');
            }
        }

        SelectValue = function (e) {
            FuncNamePopupControl.Show();
        };

        function ClearValue(s, name) {
            hfFunc.Set('ServiceId', '0');
            hfFunc.Set('Name', '');
            FuncNameTextBox.SetText('');
            $(s).css('display', 'none');
        };

    </script>
    <style>
        .dxgvGroupRow_MetropolisBlue td.dxgv
        {
            white-space: normal;
        }
    </style>

    <p class="header_grid">
        Функции и полномочия государственного органа, на автоматизацию которого направлено мероприятие
        <span class="subtitle">
            Заполняется на первом этапе
        </span>
    </p>

    <dxe:ASPxButton ID="AddFuncButton" runat="server" ClientInstanceName="AddFuncButton"
        AutoPostBack="false" Text="Добавить" Width="80px" Style="margin-left: 10px; margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) { FuncGrid.AddNewRow(); updateRow = false; }" />
    </dxe:ASPxButton>
    <dxcp:ASPxCallbackPanel runat="server" ID="ServicePanel" ClientInstanceName="ServicePanel" >
        <PanelCollection>
            <dxp:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                <dxhf:ASPxHiddenField runat="server" ID="hfFunc" ClientInstanceName="hfFunc" />
                <dxwgv:ASPxGridView runat="server" ID="FuncGrid" Width="100%"
                    ClientInstanceName="FuncGrid"
                    KeyFieldName="Id"
                    OnInitNewRow="FuncGrid_InitNewRow"
                    OnBeforePerformDataSelect="FuncGrid_BeforePerformDataSelect"
                    OnStartRowEditing="FuncGrid_StartRowEditing"
                    OnRowInserting="FuncGrid_RowInserting"
                    OnRowUpdating="FuncGrid_RowUpdating"
                    OnRowDeleting="FuncGrid_RowDeleting"
                    OnCommandButtonInitialize="FuncGrid_CommandButtonInitialize"
                    OnCustomButtonCallback="FuncGrid_CustomButtonCallback">
                    <Columns>
                        <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False"
                            AllowDragDrop="False">
                            <EditButton Visible="True" />
                            <DeleteButton Visible="True" />
                            <ClearFilterButton Visible="True" />
                            <CustomButtons>
                                <dxwgv:GridViewCommandColumnCustomButton runat="server" ID="DeleteAllByService" Text="Удалить всю группу" />
                            </CustomButtons>
                        </dxwgv:GridViewCommandColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Выполняемые функции органом государственной власти"
                            GroupIndex="1" />
                        <dxwgv:GridViewDataTextColumn FieldName="Type" Caption="Вид деятельности" CellStyle-Wrap="True" />
                        <dxwgv:GridViewDataTextColumn FieldName="NormativeAct" Caption="Наименование и реквизиты регулирующих НПА (с указанием конкретного пункта, статьи нормативно-правового акта)"
                            CellStyle-Wrap="True" />
                        <dxwgv:GridViewDataColumn FieldName="Editable" Visible="false" ShowInCustomizationForm="false" />
                    </Columns>
                    <SettingsBehavior ConfirmDelete="True" />
                    <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
                    <SettingsEditing Mode="EditForm" />
                    <Styles>
                        <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                        <GroupRow Wrap="True"></GroupRow>
                    </Styles>
                    <Templates>
                        <EditForm>
                            <table class="form_edit">
                                <tr>
                                    <td class="form_edit_desc">Вид деятельности:
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxComboBox ID="FuncTypeComboBox" runat="server" ClientInstanceName="FuncTypeComboBox"
                                            TextField="Value" ValueField="Key" ValueType="System.Int32" EncodeHtml="false"
                                             OnDataBinding="FuncTypeComboBox_DataBinding" EnableSynchronization="False">
                                            <ClientSideEvents SelectedIndexChanged="FuncTypeComboBoxSelectedIndexChanged" Init="FuncTypeComboBoxSelectedIndexChanged" />
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="form_edit_desc">Выполняемые функции органом государственной власти:
                                    </td>
                                    <td class="form_edit_input">

                                        <dxe:ASPxComboBox ID="CFComboBox" runat="server" ClientInstanceName="CFComboBox"
                                            TextField="Name" ValueField="Id" ValueType="System.Int32" EncodeHtml="false"
                                             EnableSynchronization="False"
                                            OnDataBinding="CFComboBox_DataBinding" />
                                        
                                        <div style="position:relative">
                                            <dxe:ASPxTextBox ID="FuncNameTextBox" runat="server" ClientInstanceName="FuncNameTextBox" ClientIDMode="Static" OnDataBinding="TextBox_DataBinding" ReadOnly="true">
                                                <ClientSideEvents TextChanged="function (s, e) { alert('t'); }"   />
                                            </dxe:ASPxTextBox>
                                            <div class="ButtonSelected catalogButton" id="selectServiceLink" onclick="SelectValue(this); return false;" ></div>
                                            <div class="ButtonClear catalogButton" style="display:none;"  onclick="ClearValue(this, 'FuncName'); return false;" ></div>
                                        </div>

                                        <dxpc:ASPxPopupControl ID="FuncNamePopupControl" runat="server" AllowDragging="True" AllowResize="True" ContentUrl="../../Selectors/Services.aspx"
                                            CloseAction="CloseButton" Modal="True" EnableViewState="False" PopupElementID="selectServiceLink"
                                            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="820px" MinHeight="800px"
                                            HeaderText="Выберите госуслугу (госфункцию)" ClientInstanceName="FuncNamePopupControl" EnableHierarchyRecreation="True">
                                        </dxpc:ASPxPopupControl>

                                    </td>
                                </tr>
                                <tr id="trUsedBefore">
                                    <td class="form_edit_desc">Оказание государственной услуги (выполнение государственной функции) не осуществлялось до момента формирования мероприятия:
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxCheckBox ID="UsedBeforeASPxCheckBox" runat="server" ClientInstanceName="UsedBeforeASPxCheckBox"
                                            OnDataBinding="UsedBeforeASPxCheckBox_DataBinding" />
                                    </td>
                                </tr>
                                <tr id="trMissFunc">
                                    <td class="form_edit_desc">Функция, отсутствующая в Федеральном реестре:
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxCheckBox ID="MissFuncASPxCheckBox" runat="server" ClientInstanceName="MissFuncASPxCheckBox"
                                            OnDataBinding="MissFuncASPxCheckBox_DataBinding">
                                            <ClientSideEvents CheckedChanged="ChangeMissFunc" />
                                        </dxe:ASPxCheckBox>
                                    </td>
                                </tr>
                                <tr id="trMissFuncName">
                                    <td class="form_edit_desc">Иная функция:
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxTextBox runat="server" ID="MissFuncNameTextBox" ClientInstanceName="MissFuncNameTextBox"
                                            OnDataBinding="MissFuncNameTextBox_DataBinding" />
                                    </td>
                                </tr>

                                <tr id="tr1">
                                    <td class="form_edit_section" colspan="2">
                                        <div class="text">
                                            Наименование и реквизиты регулирующих НПА
                                        </div>
                                        <div class="line">
                                        </div>
                                    </td>
                                </tr>
                                <tr id="tr2">
                                    <td class="form_edit_desc">Вид документа:
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxComboBox runat="server" ID="FuncNPATypeComboBox" ClientInstanceName="FuncNPATypeComboBox"
                                            TextField="Value" ValueField="Key" ValueType="System.Int32" EncodeHtml="false"
                                            OnDataBinding="FuncNPATypeComboBox_DataBinding">
                                            <ClientSideEvents SelectedIndexChanged="function(){
                                                    SetNPAKindComboBoxEnabledFromNPAType(FuncNPATypeComboBox, FuncNPAKindComboBox)
                                            }"
                                                Init="function(){
                                                    SetNPAKindComboBoxEnabledFromNPAType(FuncNPATypeComboBox, FuncNPAKindComboBox)
                                            }" />
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr id="tr3">
                                    <td class="form_edit_desc">Тип документа:
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxComboBox runat="server" ID="FuncNPAKindComboBox" ClientInstanceName="FuncNPAKindComboBox"
                                            TextField="Value" ValueField="Key" ValueType="System.Int32" EncodeHtml="false"
                                            OnDataBinding="FuncNPAKindComboBox_DataBinding" />
                                    </td>
                                </tr>
                                <tr id="tr4">
                                    <td class="form_edit_desc">Наименование документа:
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxTextBox runat="server" ID="FuncNPANameTextBox" ClientInstanceName="FuncNPANameTextBox"
                                            OnDataBinding="FuncNPANameTextBox_DataBinding" />
                                    </td>
                                </tr>
                                <tr id="tr5">
                                    <td class="form_edit_desc">Номер документа:
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxTextBox runat="server" ID="FuncNPANumTextBox" ClientInstanceName="FuncNPANumTextBox"
                                            OnDataBinding="FuncNPANumTextBox_DataBinding" />
                                    </td>
                                </tr>
                                <tr id="tr6">
                                    <td class="form_edit_desc">Дата документа:
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxDateEdit runat="server" ID="FuncNPADateDateEdit" ClientInstanceName="FuncNPADateDateEdit"
                                            OnDataBinding="FuncNPADateDateEdit_DataBinding" />
                                    </td>
                                </tr>
                                <tr id="tr7">
                                    <td class="form_edit_desc">Пункт, статья документа:
                                    </td>
                                    <td class="form_edit_input">
                                        <dxe:ASPxTextBox runat="server" ID="FuncItemTextBox" ClientInstanceName="FuncItemTextBox"
                                            OnDataBinding="FuncItemTextBox_DataBinding" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="form_edit_buttons">
                                        <span class="ok"><dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" /></span>
                                        <span class="cancel"><dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" /></span>
                                    </td>
                                </tr>
                            </table>
                        </EditForm>
                    </Templates>
                    <ClientSideEvents BeginCallback="function(s,e){ 
                                                        if (e.command=='UPDATEEDIT') 
                                                            hfFuncSave(e); 
                                                        if (e.command=='STARTEDIT')
                                                            updateRow = true;
                                                            }" />
                </dxwgv:ASPxGridView>
            </dxp:PanelContent>
        </PanelCollection>
        
    </dxcp:ASPxCallbackPanel>
</asp:Content>
