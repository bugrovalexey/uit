﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
    public class KeyContainerMap : ClassMapBase<KeyContainer>
    {
        public KeyContainerMap()
            : base("PRS_8_6_Key_Cont")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_6_Key_Cont_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_8_6_Key_Cont_Name = ? 
//						  ,@PRS_8_6_Key_Cont_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_6_Key_Cont_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_6_Key_Cont_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_6_Key_Cont_Delete @PRS_8_6_Key_Cont_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "PRS_8_6_Key_Cont_Name");
        }
    }
}