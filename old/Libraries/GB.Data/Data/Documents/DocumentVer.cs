﻿using GB.MKRF.Entities.Admin;
using System;

namespace GB.MKRF.Entities.Documents
{
    /// <summary>Документ</summary>
    public class DocumentVer : EntityBase
    {
        protected DocumentVer()
        {
        }

        public DocumentVer(DocTypeEnum DocumentType, EntityBase Owner = null)
        {
            this.DocType = DocumentType;
            this.Owner = Owner;
        }

        /// <summary>Родительский объект</summary>
        public virtual EntityBase Owner { get; set; }

        /// <summary>Тип документа</summary>
        public virtual DocTypeEnum DocType { get; protected set; }

        /// <summary>Тип документа (строковое поле)</summary>
        public virtual string DocTypeText { get; set; }

        /// <summary>Номер</summary>
        public virtual string Num { get; set; }//null

        /// <summary>Дата</summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null

        /// <summary>Файл</summary>
        public virtual Byte[] File { get; set; }//null

        /// <summary>Имя файла</summary>
        public virtual string FileName { get; set; }//null

        /// <summary>Тип контента</summary>
        public virtual string ContentType { get; set; }//null

        /// <summary>Комментарий</summary>
        public virtual string Comm { get; set; }//null

        /// <summary>Автор</summary>
        public virtual User Author { get; set; }

        /// <summary>Дата создания</summary>
        public virtual DateTime? DateCreate { get; set; }

        public virtual bool IsSigned { get; protected set; }

        /// <summary>Подписанный пакет</summary>
        public virtual Byte[] SignedPackage { get; set; }//null

        /// <summary>Информация о сертификате</summary>
        public virtual string SignInfo { get; set; }

        /// <summary>Тип подписи(частное/должностное лицо)</summary>
        public virtual int? SignType { get; set; }

        public virtual DocGroupEnum DocGroup { get; set; }

        /// <summary> Решение прав комиссии</summary>
        public virtual bool DecisionGovComission { get; set; }
    }
}