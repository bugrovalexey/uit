﻿using Uviri.MKRF.Entities.PlanReport.SectionVIII;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.PlanReport.SectionVIII
{
    public class UCMap : ClassMapBase<UC>
    {
		public UCMap()
            : base("PRS_8_6")
        {
//            Table("w_");

//            Id(x => x.Id, "PRS_8_6_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

//            string sql = @"@PRS_8_6_1 = ? 
//                          ,@PRS_8_6_2 = ? 
//                          ,@PRS_8_6_3 = ? 
//                          ,@PRS_8_6_5 = ? 
//                          ,@PRS_8_6_7 = ? 
//                          ,@PRS_8_6_8 = ? 
//                          ,@Plans_ID = ? 
//                          ,@PRS_8_6_Sign_Form_ID = ? 
//                          ,@PRS_8_6_Key_Cont_ID = ? 
//						  ,@PRS_8_6_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_PRS_8_6_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_PRS_8_6_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_PRS_8_6_Delete @PRS_8_6_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "PRS_8_6_1");
			Map(x => x.Name, "PRS_8_6_2");
			Map(x => x.Software, "PRS_8_6_3");
			Map(x => x.IsAccreditaded, "PRS_8_6_5");
			Map(x => x.GivenCertCount, "PRS_8_6_7");
			Map(x => x.ValidCertCount, "PRS_8_6_8");

			References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
			References<SignForm>(x => x.SignForm, "PRS_8_6_Sign_Form_ID").Not.Nullable();
			References<KeyContainer>(x => x.KeyContainer, "PRS_8_6_Key_Cont_ID").Not.Nullable();
        }
    }
}