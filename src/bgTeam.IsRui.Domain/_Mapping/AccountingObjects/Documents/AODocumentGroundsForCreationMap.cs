﻿namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects.Documents
{
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects.Documents;
    using bgTeam.IsRui.Domain.Entities.Documents;

    internal class AODocumentGroundsForCreationMap : AODocumentBaseMap<AODocumentGroundsForCreation>
    {
        protected DocTypeEnum Type
        {
            get { return AODocumentGroundsForCreation.DocType; }
        }

        protected override void AdditionalMap()
        {
            HasMany(x => x.Documents).KeyColumns.Add("EntityOwnerId")
                .Where("EntityType = " + (int)EntityType.AODocumentGroundsForCreation)
                .Where("Type = " + (int)Type)
                .Cascade.Delete().Inverse();
        }
    }
}
