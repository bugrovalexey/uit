﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpConclSubsidizeActMap : ClassMapBase<ExpConclSubsidizeAct>
	{
        public ExpConclSubsidizeActMap()
            : base("Exp_Concl_Subsidize_Act")
		{
		}

        protected override void InitMap()
        {
            Map(x => x.Disagreement, "Exp_Concl_Subsidize_Act_Disagreement");
            Map(x => x.AppropriateVolumeFinanceY0, "Exp_Concl_Subsidize_Act_Appropriate_VF_Y0").Not.Nullable();
            Map(x => x.AppropriateVolumeFinanceY1, "Exp_Concl_Subsidize_Act_Appropriate_VF_Y1").Not.Nullable();
            Map(x => x.AppropriateVolumeFinanceY2, "Exp_Concl_Subsidize_Act_Appropriate_VF_Y2").Not.Nullable();
            Map(x => x.Completion, "Exp_Concl_Subsidize_Act_Completion");

            References<Appropriate>(x => x.GoalEffective, "Goal_Effective_ID").Not.Nullable();
            References<Appropriate>(x => x.OrderFinance, "Order_Finance_ID").Not.Nullable();
			References<ExpConclusion>(x => x.ExpConclusion, "Exp_Conclusion_ID").Not.Nullable();
            References<PlansActivityMU>(x => x.PlanActivityMU, "Plans_Activity_MU_ID").Not.Nullable();
			References<ExpActEquivalent>(x => x.ExpActEquivalent, "Exp_Act_Equivalent_ID").Not.Nullable();

            //HasMany<ExpCriterionEquivalent>(x => x.ExpCriterionEquivalent)
            //        .KeyColumns.Add("Exp_Concl_Subsidize_Act_ID")
            //        .Inverse()
            //        .Cascade.AllDeleteOrphan();
        }
    }
}