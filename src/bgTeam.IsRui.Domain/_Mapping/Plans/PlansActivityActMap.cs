﻿namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class PlansActivityActMap : ClassMapHiLo<PlansActivityAct>
    {
        public PlansActivityActMap()
            : base("Plans_Activity_Act")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Name");
            Map(x => x.Number, "Plans_Activity_Number");
            Map(x => x.DateAdoption, "Plans_Activity_Date_Adoption");
            Map(x => x.Cost, "Plans_Activity_Cost");
            Map(x => x.FileId, "Plans_Activity_FileId");
            Map(x => x.FileName, "Plans_Activity_FileName");
            Map(x => x.ChangeDate, "Plans_Activity_TimeStamp");

            References(x => x.Activity, "Plans_Activity_ID").Not.Nullable();
        }
    }
}
