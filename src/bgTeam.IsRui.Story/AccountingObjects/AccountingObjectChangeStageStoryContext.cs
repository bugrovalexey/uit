﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    public class AccountingObjectChangeStageStoryContext
    {
        /// <summary>
        /// Идентификатор ОУ
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Новая стадия работы с ОУ
        /// </summary>
        public string Stage { get; set; }
    }
}
