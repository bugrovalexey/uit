﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Внешние ИС и компоненты ИТКИ
    /// </summary>
    public class ExternalIsAndComponentsItki : EntityBase
    {
        /// <summary>
        /// Собственник
        /// </summary>
        public virtual AccountingObject Owner { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        public virtual string Purpose { get; set; }

        /// <summary>
        /// СМЭВ
        /// </summary>
        public virtual string Smev { get; set; }

        /// <summary>
        /// URL адрес
        /// </summary>
        public virtual string Url { get; set; }
    
    }
}
