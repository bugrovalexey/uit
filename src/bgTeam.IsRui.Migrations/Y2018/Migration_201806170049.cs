﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201806170049)]
    public class Migration_201806170049 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Claim_Mark").AlterColumn("State_Programme_Mark_ID").AsDecimal().Nullable();
        }
    }
}
