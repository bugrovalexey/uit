﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    class NPAKindMap : ClassMapHiLo<NPAKind>
    {
        public NPAKindMap()
            : base("NPA_Kind")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "NPA_Kind_Name");
        }
    }
}