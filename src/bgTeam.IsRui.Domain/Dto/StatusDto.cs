﻿namespace bgTeam.IsRui.Domain.Dto
{
    public class StatusDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Группа
        /// </summary>
        public string StatusGroup { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название для кнопок
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// Стили для кнопок
        /// </summary>
        public string Css { get; set; }
    }
}
