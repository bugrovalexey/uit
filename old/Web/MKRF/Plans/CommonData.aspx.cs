﻿using GB.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GB.MKRF.Web.Plans
{
    public partial class CommonData : System.Web.UI.Page
    {
        public static string GetUrl(object id)
        {
            return string.Format("{0}?{1}={2}", UrlHelper.Resolve("~/Plans/CommonData.aspx"), EditCard.PlanIdKey, id);
        }
    }
}