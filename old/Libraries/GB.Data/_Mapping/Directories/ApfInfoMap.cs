﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class ApfInfoMap : ClassMapBase<ApfInfo>
    {
        public ApfInfoMap()
            : base("APF_Info")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "APF_Info_Name").Nullable();
            Map(x => x.BestPractice, "APF_Info_Best_Practice");
            Map(x => x.SoftwareKind, "APF_Info_Software_Kind_ID").CustomType(typeof(SoftwareKind?)).Nullable();
            Map(x => x.OrigId, "APF_Info_Orig_ID").Nullable();
            Map(x => x.OrganizationName, "APF_Info_Organization_Name").Nullable();
            Map(x => x.Num, "APF_Info_Num").Nullable();
            Map(x => x.RegistryNum, "APF_Info_Registry_Num").Nullable();

            Map(x => x.Version, "APF_Info_Version").Nullable();
            Map(x => x.Date, "APF_Info_Placement_Date_In_FAP").Nullable();
        }
    }
}