﻿using System;
using GB.ESIA.Web.Helpers;

namespace GB.ESIA.Web
{
    public class ESIALogoutControl : ESIAControlsBase
    {
        /// <summary> СНИЛС, авторизованного пользователя </summary>
        public string Snils { get; set; }

        protected override void SetValues()
        {
            //LinkButton.Text = LinkButtonText ?? "Выйти";
            //if (!string.IsNullOrEmpty(CssClass))
            //    LinkButton.CssClass = CssClass;
        }

        protected override void LinkButtonClick(object sender, EventArgs e)
        {
            var nameIdentifier = string.Empty;
            if(!string.IsNullOrEmpty(Snils))
                nameIdentifier = (string)Util.RemoveFromCache(Snils);

            var logoutRequest = new LogoutRequest(Page, CertificateAccessor.ServiceProviderCert, ConfigAccessor.SingleLogoutServiceUrl, nameIdentifier);
            if (ConfigAccessor.DebugMode)
                Util.WriteLog("Temp/LastLogoutRequest.xml", logoutRequest.Xml.OuterXml);
            
            logoutRequest.Send();
        }
    }
}
