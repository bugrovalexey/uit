﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.Common;
using System.ComponentModel;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    public class TableRoles : NotificationBaseViewModel
    {
        private const string NameCaption = "Наименование";

        private const string WebPageCaption = "Web страница";

        public TableRoles(Role role)
        {
            Id = role.Id;
            Name = role.Name;
            WebPage = role.WebPage;
        }

        public int Id { get; set; }

        [DisplayName(NameCaption)]
        public string Name { get; set; }

        [DisplayName(WebPageCaption)]
        public string WebPage { get; set; }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteRole", "Admin", new { id = Id }, "Grid");
            }
        }
    }
}