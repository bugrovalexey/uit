﻿namespace bgTeam.IsRui.WebApp.Admin.Domain
{
    public enum MetaColumnTypeEnum
    {
        TString,
        TBigString,
        TInt,
        TDecimal,
        TRef,
        TChild,
        TBool,
        TDateTime
    }
}