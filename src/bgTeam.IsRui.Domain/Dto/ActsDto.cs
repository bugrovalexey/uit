﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActsDto
    {
        public int? Id { get; set; }

        public int? AoId { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public DateTime? DateAdoption { get; set; }

        public int? Cost { get; set; }

        public ActsFileDto File { get; set; }
    }
}
