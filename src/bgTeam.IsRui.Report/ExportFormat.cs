﻿namespace bgTeam.IsRui.Report
{
    public enum ExportFormat
    {
        Pdf = 0,
        Rtf = 1,
        Xls = 2
    }
}
