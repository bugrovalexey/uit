﻿using bgTeam.IsRui.Domain.Entities.Common;
using bgTeam.IsRui.Domain.Entities.Workflow;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.ViewModels.Admin
{
    [MetadataType(typeof(StatusChangeEditViewModel_Valid))]
    public class StatusChangeEditViewModel : ViewModelBase
    {
        private const string RoleCaption = "Роль";
        private const string StatusCaption = "Статус";
        private const string NoteCaption = "Примечание";

        public StatusChangeEditViewModel(IStatusRepository statusRepository,int? id)
        {
            var access = id.HasValue ? new RepositoryDc<StatusList>().GetExisting(id) : null;
            access.Do(Fill);

            StatusList = new SelectList(
                statusRepository.GetAll(),
                "Id", "Name");
            RoleList = new SelectList(
                new RepositoryDc<Role>().GetAll(),
                "Id", "Name", Role);
        }

        public void Fill(StatusList value)
        {
            Id = value.Id;
            Role = value.Role.Return(x => x.Id, default(int));
            From = value.From.Return(x => x.Id, default(int));
            To = value.To.Return(x => x.Id, default(int));
        }

        public int Id { get; set; }

        [DisplayName(StatusChangeViewModel.RoleCaption)]
        public int Role { get; set; }

        [DisplayName(StatusChangeViewModel.StatusFromCaption)]
        public int From { get; set; }

        [DisplayName(StatusChangeViewModel.StatusToCaption)]
        public int To { get; set; }

        public SelectList RoleList { get; set; }

        public SelectList StatusList { get; set; }
    }

    public class StatusChangeEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public int Role { get; set; }
    }
}