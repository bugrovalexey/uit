﻿using System.Web;
using System.Web.UI;
using System.Security.Permissions;
using GB.Helpers;

namespace GB.Controls
{
    [AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal),
    AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    public class BackLinkControl : Control
    {
        public string BackUrl { get; set; }

        public string Text { get; set; }

        protected string GetBackLink()
        {
            string back = "<span class=\"backlink_back\">←</span>";

            if (!string.IsNullOrEmpty(BackUrl))
            {
                return string.Concat("<a style=\"text-decoration: none\" href=\"", this.BackUrl, "\" >", back, " <span class=\"backlink_text\">", Text ?? "Вернуться к списку", "<span></a>");
            }

            return string.Concat("<a style=\"text-decoration: none\" href=\"", UrlHelper.Resolve("~/Default.aspx"), "\" >", back, " <span class=\"backlink_text\">", "На главную<span></a>");
        }

        protected override void Render(HtmlTextWriter writer)
        {
            string html = GetBackLink();

            writer.Write(html);

            base.Render(writer);
        }
    }
}