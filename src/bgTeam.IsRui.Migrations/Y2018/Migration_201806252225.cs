﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201806252225)]
    public class Migration_201806252225 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("ClaimScopeOfWork")
                .AddColumn("ClaimScopeOfWork_LaborCostsHours").AsInt32().Nullable()
                .AddColumn("ClaimScopeOfWork_SpecialistId").AsInt32().Nullable()
                .AddColumn("ClaimScopeOfWork_Cost").AsDecimal().Nullable();
        }
    }
}
