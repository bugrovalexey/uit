﻿using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Mapping.Directories
{
    class PlansActivityDirectionMap : ClassMapBase<PlansActivityDirection>
    {
        public PlansActivityDirectionMap()
            : base("Plans_Activity_Direction")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Plans_Activity_Direction_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"exec ZZZ_Prc_Plans_Activity_Direction_Insert
            //						  @Plans_Activity_Direction_Name = ?
            //						 ,@Plans_Activity_Direction_ID = ?").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Plans_Activity_Direction_Update
            //						  @Plans_Activity_Direction_Name = ?
            //						 ,@Plans_Activity_Direction_ID = ?").Check.None();

            //            SqlDelete(@"exec ZZZ_Prc_Plans_Activity_Direction_Delete
            //						  @Plans_Activity_Direction_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Direction_Name").Nullable();
        }
    }
}