﻿namespace bgTeam.IsRui.Story.Common
{
    public class WorkflowStatusChangeStoryContext
    {
        public int? WorkflowId { get; set; }

        public int? StatusId { get; set; }
    }
}