﻿namespace bgTeam.AdminDB.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam;
    using bgTeam.AdminDB.Web.Domain;
    using bgTeam.DataAccess;

    public class BuilderQuery : IBuilderQuery
    {
        public const string REF_PREFIX = "RF";

        public readonly string PARAM_PREFIX;

        private readonly IAppLogger _logger;
        private readonly IRepository _repositiry;

        public BuilderQuery(
            IAppLogger logger,
            IRepository repositiry,
            SqlDialectEnum dbType)
        {
            _logger = logger;
            _repositiry = repositiry;

            switch (dbType)
            {
                case SqlDialectEnum.MsSql:
                    PARAM_PREFIX = "@";
                    break;
                case SqlDialectEnum.Oracle:
                    PARAM_PREFIX = ":";
                    break;
                case SqlDialectEnum.PostgreSql:
                case SqlDialectEnum.MySql:
                default:
                    throw new NotSupportedException($"db type - {dbType}");
            }
        }

        public async Task<string> BuildAsync(MetaDictionary dict, IEnumerable<MetaColumn> columns)
        {
            string Columns = string.Empty;//string.Concat("th.", AppSettings.PRIMARY_KEY, ",");
            string TablesRef = string.Empty;

            columns.GetAndCheckColumnPK(dict);

            foreach (var item in columns)
            {
                switch (item.Type)
                {
                    case MetaColumnTypeEnum.TString:
                    case MetaColumnTypeEnum.TInt:
                    case MetaColumnTypeEnum.TDecimal:
                    case MetaColumnTypeEnum.TBigString:
                    case MetaColumnTypeEnum.TBool:
                    case MetaColumnTypeEnum.TDateTime:
                        Columns = string.Format("{0}th.{1}, ", Columns, item.ColumnName);
                        break;

                    case MetaColumnTypeEnum.TRef:

                        var rdict = await _repositiry.GetAsync<MetaDictionary>(x => x.Id == item.RefId);
                        var rcolumns = await _repositiry.GetAllAsync<MetaColumn>(x => x.DictionaryId == item.RefId);

                        var rcol = rcolumns.Single(x => x.Id == item.RefColumn);

                        Columns = string.Format("{0}th.{2},", Columns, dict.TableName, item.ColumnName);
                        var pseudo = item.ColumnName + "_To_" + rdict.TableName;
                        Columns = string.Format("{0}{1}.{2} as {4}{3},", Columns, pseudo, rcol.ColumnName,
                                                item.ColumnName, REF_PREFIX);

                        var colPK = rcolumns.GetAndCheckColumnPK(rdict);

                        TablesRef = string.Format("{0} left join {1} as {3} on {3}.{2} = th.{4}",
                                                  TablesRef,
                                                  rdict.TableName,
                                                  colPK.ColumnName,
                                                  pseudo,
                                                  item.ColumnName);
                        break;

                    //case MetaColumnTypeEnum.TChild:
                    //    MetaDictionary cdict = GetDataDictionary(item.RefId, false);
                    //    Columns = string.Format("{0}{1}.{2},", Columns, cdict.ViewName, item.ColumnName);
                    //    break;

                    default:
                        throw new NotSupportedException("Тип не поддерживается: " + item.Type);
                }
            }

            //if (dict.IsUse_InUse)
            //{
            //    Columns = string.Format("{0}{1},", Columns, MetaDictionary.INUSE);
            //}

            Columns = Columns.TrimEnd(',', ' ');

            return LoggerSql(string.Format("select {0} from {1} th {2}", Columns, dict.TableName, TablesRef));
        }

        public async Task<string> BuildWithDelete(MetaDictionary dict, IEnumerable<MetaColumn> columns)
        {
            var str = await BuildAsync(dict, columns);

            return LoggerSql($"{str} where th.DELETED < 1 or th.DELETED is null");
        }

        public async Task<string> BuildItemAsync(MetaDictionary dict, IEnumerable<MetaColumn> columns, int itemId)
        {
            var sql = await BuildAsync(dict, columns);

            if (dict.ReadOnly)
            {
                return LoggerSql(sql);
            }

            var colPK = columns.GetAndCheckColumnPK(dict);

            return LoggerSql($"{sql} where th.{colPK.ColumnName} = {itemId}");
        }

        public string BuildInsert(MetaDictionary dict, IEnumerable<MetaColumn> columns)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("INSERT INTO {0} (", dict.TableName);

            foreach (var item in columns)
            {
                sb.AppendFormat("{0}, ", item.ColumnName);
            }

            sb.AppendFormat(") VALUES (");

            foreach (var item in columns)
            {
                sb.AppendFormat("{1}{0}, ", item.ColumnName, PARAM_PREFIX);
            }

            sb.Append(")");

            return LoggerSql(sb.ToString().Replace(", )", ")"));
        }

        public string BuildUpdate(MetaDictionary dict, IEnumerable<MetaColumn> columns)
        {
            var sb = new StringBuilder();

            var colPK = columns.GetAndCheckColumnPK(dict);

            sb.AppendFormat("UPDATE {0} SET ", dict.TableName);

            foreach (var item in columns)
            {
                if (item.ColumnName == colPK.ColumnName)
                {
                    continue;
                }

                sb.AppendFormat("{0} = {1}{0}, ", item.ColumnName, PARAM_PREFIX);
            }

            sb.AppendFormat("WHERE {0} = {1}{0}", colPK.ColumnName, PARAM_PREFIX);

            return LoggerSql(sb.ToString().Replace(", WHERE", " WHERE"));
        }

        public string BuildDelete(MetaDictionary dict, MetaColumn column)
        {
            return LoggerSql($"DELETE FROM {dict.TableName} WHERE {column.ColumnName} = {PARAM_PREFIX}Id");
        }

        public string BuildDeleteWithDelete(MetaDictionary dict, MetaColumn column)
        {
            return LoggerSql($"UPDATE {dict.TableName} SET DELETED = 1 WHERE {column.ColumnName} = {PARAM_PREFIX}Id");
        }

        public string BuildGetPrimaryKey(MetaDictionary dict, MetaColumn column)
        {
            return $"SELECT ISNULL(MAX({column.ColumnName}), 0) + 1 FROM {dict.TableName}";
        }

        public string BuildSelectList(MetaDictionary dict, string fieldValue, string fieldName)
        {
            return LoggerSql($"SELECT {fieldValue}, {fieldName} FROM {dict.TableName} ORDER BY {fieldName}");
        }

        private string LoggerSql(string sql)
        {
            _logger.Debug(sql);

            return sql;
        }
    }
}