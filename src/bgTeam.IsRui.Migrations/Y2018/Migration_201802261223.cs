﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201802261226)]
    public class Migration_201802261223 : ForwardOnlyMigration
    {
        public override void Up()
        {

            Create.Table("OU_Acts")
                .WithColumn("OU_Acts_ID").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("OU_ID").AsInt32().NotNullable()
                .WithColumn("OU_Acts_Name").AsString().Nullable()
                .WithColumn("OU_Acts_Number").AsString().Nullable()
                .WithColumn("OU_Acts_Date_Adoption").AsDateTime().Nullable()
                .WithColumn("OU_Acts_Cost").AsInt32().Nullable()
                .WithColumn("OU_Acts_FileId").AsInt32().Nullable()
                .WithColumn("OU_Acts_FileName").AsString().Nullable()
                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                 .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Create.ForeignKey("FK_OU_Acts")
               .FromTable("OU_Acts").ForeignColumn("OU_ID")
               .ToTable("OU").PrimaryColumn("OU_ID");

        }
    }
}
