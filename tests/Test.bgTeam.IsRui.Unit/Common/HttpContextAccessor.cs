﻿namespace Test.bgTeam.IsRui.Unit.Common
{
    using System;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Threading;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Authentication;
    using Microsoft.AspNetCore.Http.Features;

    public class HttpContextAccessor : IHttpContextAccessor
    {
        public HttpContextAccessor()
        {
            HttpContext = new HttpContextTest();
        }

        public HttpContext HttpContext { get; set; }
    }

    public class HttpContextTest : HttpContext
    {
        public HttpContextTest()
        {
            User = new ClaimsPrincipal(new Identity());
        }

        public override IFeatureCollection Features => throw new NotImplementedException();

        public override HttpRequest Request => throw new NotImplementedException();

        public override HttpResponse Response => throw new NotImplementedException();

        public override ConnectionInfo Connection => throw new NotImplementedException();

        public override WebSocketManager WebSockets => throw new NotImplementedException();

        public override AuthenticationManager Authentication => throw new NotImplementedException();

        public override IDictionary<object, object> Items { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public override IServiceProvider RequestServices { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public override CancellationToken RequestAborted { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public override string TraceIdentifier { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public override ISession Session { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override ClaimsPrincipal User { get; set; }

        public override void Abort()
        {
            throw new NotImplementedException();
        }
    }

    public class Identity : IIdentity
    {
        public string Name => "Work3";

        public string AuthenticationType => "Base";

        public bool IsAuthenticated => true;
    }
}
