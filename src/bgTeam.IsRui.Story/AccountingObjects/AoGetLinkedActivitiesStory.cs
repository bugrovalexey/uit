﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoGetLinkedActivitiesStory : IStory<AoGetLinkedActivitiesStoryContext, IEnumerable<ActivityDto>>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IMapperBase _mapper;

        public AoGetLinkedActivitiesStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase)
        {
            _repositoryRui = repositoryRui;
            _mapper = mapperBase;
        }

        public IEnumerable<ActivityDto> Execute(AoGetLinkedActivitiesStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivityDto>> ExecuteAsync(AoGetLinkedActivitiesStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            var ao = _repositoryRui.Get<AccountingObject>(context.AoId);

            if (ao == null)
            {
                throw new IsRuiException("Не найден объект управления");
            }

            var activities = _repositoryRui
                .GetAllEx<PlansActivity>(query => query
                .JoinQueryOver(x => x.AoActivity)
                .Where(a => a.AccountingObject.Id == ao.Id)).ToList();

            return activities
                .Select(x => _mapper.Map(x, new ActivityDto()));
        }
    }
}
