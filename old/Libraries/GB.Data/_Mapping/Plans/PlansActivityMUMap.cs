﻿using Uviri.Entity.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class PlansActivityMUMap : ClassMapBase<PlansActivityMU>
	{
        public PlansActivityMUMap()
            : base("Plans_Activity_MU")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_MU_Name");
            Map(x => x.Annotation, "Plans_Activity_MU_Annotation");

            Map(x => x.FactY0, "Plans_Activity_MU_Vol_FB_Y0");
            Map(x => x.FactY1, "Plans_Activity_MU_Vol_FB_Y1");
            Map(x => x.FactY2, "Plans_Activity_MU_Vol_FB_Y2");

            Map(x => x.BudgetY0, "Plans_Activity_MU_Vol_B_Y0");
            Map(x => x.BudgetY1, "Plans_Activity_MU_Vol_B_Y1");
            Map(x => x.BudgetY2, "Plans_Activity_MU_Vol_B_Y2");

            Map(x => x.ExpenseAddY0, "Plans_Activity_MU_Vol_Add_Y0");
            Map(x => x.ExpenseAddY1, "Plans_Activity_MU_Vol_Add_Y1");
            Map(x => x.ExpenseAddY2, "Plans_Activity_MU_Vol_Add_Y2");

            Map(x => x.JointFundingY0, "Plans_Activity_MU_Vol_JF_Y0");
            Map(x => x.JointFundingY1, "Plans_Activity_MU_Vol_JF_Y1");
            Map(x => x.JointFundingY2, "Plans_Activity_MU_Vol_JF_Y2");

            Map(x => x.Subprogram, "Plans_Activity_MU_Subprogram");

            References<Plan>(x => x.Plan, "Plans_ID");
            References<SectionKBK>(x => x.SectionKBK, "Section_KBK_ID");
            References<GRBS>(x => x.GRBS, "GRBS_ID");
            References<ExpenseItem>(x => x.ExpenseItem, "Expense_Item_ID");
            References<WorkForm>(x => x.WorkForm, "Work_Form_ID");

            //References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
            HasMany(x => x.Regions).KeyColumns.Add("Plans_Activity_MU_ID").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ReportRegions).KeyColumns.Add("Plans_Activity_MU_ID").Cascade.AllDeleteOrphan().Inverse();

            //ApplyFilter<SecurityFilter>("Govt_Organ_ID in (select Govt_Organ_ID from Govt_Organ where Govt_Organ_Type_ID = 2)");
        }
    }
}