﻿using System;

namespace bgTeam.IsRui.Domain.Entities.Directories
{
    /// <summary>
    /// БК ГРБС
    /// </summary>
    public class GRBS : EntityBase, IEntityCodeName, IPeriodDirectories
    {
        /// <summary>Код</summary>
        public virtual string Code { get; set; }//null

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }//null

        /// <summary>Краткое наименование</summary>
        public virtual string SName { get; set; }//null

        /// <summary>Адрес</summary>
        public virtual string Addr { get; set; }//null

        /// <summary>Телефон</summary>
        public virtual string Phone { get; set; }//null

        /// <summary>Сайт</summary>
        public virtual string Web { get; set; }//null

        /// <summary> Дата начала действия справочника </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary> Дата окончания действия справочника </summary>
        public virtual DateTime? DateEnd { get; set; }

        public virtual string CodeName { get { return EntityHelper.GetCodeName(this); } }
    }
}