﻿using GB.Data.AccountingObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB._Mapping.AccountingObjects
{
    class AoActivityMap : ClassMapHiLo<AoActivity>
    {
        public AoActivityMap()
            : base("OU_Plans_Activity") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Plans_Activity_SName");
            Map(x => x.Year, "OU_Plans_Activity_Year");
            Map(x => x.ActivityType, "Plans_Activity_Type2_ID").CustomType<Data.Plans.ActivityTypeEnum>();
            Map(x => x.CostY0, "OU_Plans_Activity_Y0");
            Map(x => x.CostY1, "OU_Plans_Activity_Y1");
            Map(x => x.CostY2, "OU_Plans_Activity_Y2");
            Map(x => x.Stage, "OU_Stage_ID").Not.Nullable().CustomType<AccountingObjectsStageEnum>();

            References(x => x.Activity, "Plans_Activity_ID");
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();

            HasMany(x => x.ActToAOActivitys).KeyColumns.Add("OU_Plans_Activity_ID").Cascade.Delete().Inverse();
        }
    }
}
