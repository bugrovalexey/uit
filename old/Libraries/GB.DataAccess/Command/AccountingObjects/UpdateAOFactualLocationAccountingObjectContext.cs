﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateAOFactualLocationAccountingObjectContext : ICommandContext
    {        
        public string LocationOrgName { get; set; }
        
        public string LocationFioResponsible { get; set; }
        
        public string LocationOrgAddress { get; set; }
        
        public string LocationOrgPhone { get; set; }

        public int xId { get; set; }
    }
}
