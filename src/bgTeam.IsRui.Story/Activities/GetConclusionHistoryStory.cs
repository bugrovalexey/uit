﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Domain.Entities.Workflow;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GetConclusionHistoryStory : IStory<GetConclusionHistoryStoryContext, IEnumerable<ConclusionHistoryDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public GetConclusionHistoryStory(IRepositoryRui repository, IMapperBase mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<ConclusionHistoryDto> Execute(GetConclusionHistoryStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ConclusionHistoryDto>> ExecuteAsync(GetConclusionHistoryStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            var entity = _repository.Get<PlansActivity>(context.Id);

            var workflow = _repository.Get<Workflow>(q => q.Where(x => x.EntityOwner == entity));

            var conclusion = _repository.GetAllEx<WorkflowUserConclusionHistory>(
                q => q.Where(x => x.WorkflowId == workflow.Id))
                .Select(x => _mapper.Map(x, new ConclusionHistoryDto()));

            return conclusion;
        }
    }
}
