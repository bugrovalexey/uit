﻿using System;
using System.Collections.Generic;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Repository;
using GB.MKRF.Validation;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

namespace GB.MKRF.Web.Users
{
	public partial class Requests : System.Web.UI.Page
	{
		bool isNewRow = false;
		UserRequest currentRequest = null;

		protected string getDocLink(int id)
		{
			return string.Concat("<a href='", RequestsDoc.GetUrl(id), "'>Документы</a>");
		}
			
		protected override void OnPreRender(EventArgs e)
		{
			grid.DataBind();

			if (RoleEnum.CA.IsEq(UserRepository.GetCurrent().Role))
			{
				grid.Columns["IsComplete"].Visible = true;
				grid.Columns["IsComplete"].ShowInCustomizationForm = true;
			}

			addButton.Visible = Validation.Validator.Validate(new UserRequest(), ObjActionEnum.create);

			base.OnPreRender(e);
		}

		protected void grid_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
		{
			isNewRow = true;
		}

		protected void grid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
		{
			currentRequest = RepositoryBase<UserRequest>.Get(e.EditingKeyValue);
		}

		protected void grid_BeforePerformDataSelect(object sender, EventArgs e)
		{
			grid.DataSource = UserRequestRepository.GetAlldt();
		}

		protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
		{
			currentRequest = new UserRequest();
			currentRequest.Author = UserRepository.GetCurrent();

			saveRow(currentRequest);

			GridCancelEdit(grid, e);
		}

		protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
		{
			UserRequest request = RepositoryBase<UserRequest>.Get((int)e.Keys[0]);

			if (Validation.Validator.Validate(request, ObjActionEnum.delete))
			{
				RepositoryBase<UserRequest>.Delete(request.Id);
			}
			
			GridCancelEdit(grid, e);
		}

		protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
		{
			currentRequest = RepositoryBase<UserRequest>.Get((int)e.Keys[0]);
			saveRow(currentRequest);

			GridCancelEdit(grid, e);
		}

		protected void ComboBox_DataBinding(object sender, EventArgs e)
		{
			if (!isNewRow && currentRequest == null)
				return;

			ASPxComboBox box = (ASPxComboBox)sender;
            box.ReadOnly = true;
			IList<Department> departments = RepositoryBase<Department>.GetAll();
			box.DataSource = departments;

			if (!isNewRow)
			{
				box.Value = currentRequest.Department.Id;
			}
			else
			{
				//box.SelectedIndex = (departments != null && departments.Count == 1) ? 0 : -1;
                box.Value = UserRepository.GetCurrent().Department.Id;
			}
		}

		private void GridCancelEdit(ASPxGridView grid, System.ComponentModel.CancelEventArgs e)
		{
			grid.CancelEdit();
			e.Cancel = true;

			grid.DataBind();
		}

		protected void TextBox_DataBinding(object sender, EventArgs e)
		{
			if (!isNewRow && currentRequest == null)
				return;

			ASPxTextBox box = (ASPxTextBox)sender;
			if (isNewRow)
			{
				box.Text = string.Empty;
			}
			else
			{
				switch (box.ID)
				{
					case "SurnameTextBox": box.Text = currentRequest.Surname; break;
					case "NameTextBox": box.Text = currentRequest.Name; break;
					case "MiddlenameTextBox": box.Text = currentRequest.Middlename; break;
					case "PhoneWorkTextBox": box.Text = currentRequest.PhoneWork; break;
					case "PhoneMobTextBox": box.Text = currentRequest.PhoneMob; break;
					case "EmailTextBox": box.Text = currentRequest.Email; break;
					case "JobTextBox": box.Text = currentRequest.Job; break;
					case "CommentTextBox": box.Text = currentRequest.Comment; break;
				}

			}
		}

		bool saveRow(UserRequest obj)
		{
			obj.Surname = (string)hf["Surname"];
			obj.Name = (string)hf["Name"];
			obj.Middlename = (string)hf["Middlename"];
			obj.PhoneWork = (string)hf["PhoneWork"];
			obj.PhoneMob = (string)hf["PhoneMob"];
			obj.Email = (string)hf["Email"];
			obj.Job = (string)hf["Job"];
			obj.Comment = (string)hf["Comment"];
            if (obj.Id==-1) obj.Department = UserRepository.GetCurrent().Department;

			if (Validation.Validator.Validate(obj, obj.IsNew() ? ObjActionEnum.create : ObjActionEnum.edit))
			{
				RepositoryBase<UserRequest>.SaveOrUpdate(obj);
				return true;
			}

			return false;
		}

		protected void grid_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs e)
		{
			ASPxGridView grid = (ASPxGridView)sender;
			int? composite = null;

			composite = (int?)grid.GetRowValues(e.VisibleIndex, "Composite");

			if (e.ButtonType == ColumnCommandButtonType.Edit)
			{
				e.Visible = (composite.HasValue && (composite.Value & (int)CompositeEnum.Edit) != 0);
			}
			else if (e.ButtonType == ColumnCommandButtonType.Delete)
			{
				e.Visible = (composite.HasValue && (composite.Value & (int)CompositeEnum.Delete) != 0);
			}
		}


		protected void ToExcelButton_Click(object sender, EventArgs e)
		{
			DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions(); options.SheetName = "Ответственные за мероприятий по ИКТ";
			options.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Value; ToExcelExporter.WriteXlsToResponse("Otvetstvennie_za_IKT", options);
		}
	}
}