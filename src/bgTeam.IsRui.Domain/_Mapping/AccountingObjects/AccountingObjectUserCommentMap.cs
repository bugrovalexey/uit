﻿namespace bgTeam.IsRui.Domain._Mapping.AccountingObjects
{
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Documents;

    internal class AccountingObjectUserCommentMap : ClassMapHiLo<AccountingObjectUserComment>
    {
        public AccountingObjectUserCommentMap()
            : base("OU_User_Comment")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.CreateDate, "OU_User_Comment_Create_DT").Not.Nullable();
            Map(x => x.Comment, "Comment");
            References(x => x.Status, "Status_ID").Nullable();

            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
            References(x => x.User, "Users_ID").Not.Nullable();

            HasMany(x => x.Documents).KeyColumns.Add("EntityOwnerId")
                .Where("EntityType = " + (int)EntityType.AccountingObjectCommentDocument)
                .Where("Type = " + (int)DocTypeEnum.AccountingObjectCommentDocument)
                .Cascade.Delete().Inverse();
        }
    }
}