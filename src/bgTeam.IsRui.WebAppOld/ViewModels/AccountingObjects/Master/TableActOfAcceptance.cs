﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common.Extentions;
using bgTeam.IsRui.Common.Helpers;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.WebApp.Helpers;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class TableActOfAcceptance : NotificationBaseViewModel
    {
        #region consts

        public const string DocNameCaption = "Наименование";
        public const string DocNumberCaption = "Номер документа";
        public const string DocDateAdoptionCaption = "Дата подписания";
        public const string CostCaption = "Стоимость";
        public const string FileLinkCaption = "Документ";

        #endregion consts


        public TableActOfAcceptance(ActOfAcceptance entity)
        {
            Id = entity.Id;
            DocName = entity.DocName;
            DocNumber = entity.DocNumber;
            DocDateAdoption = entity.DocDateAdoption.ToStringOrDefault();
            Cost = entity.Cost.ToMoneyString();
            CostMoney = entity.Cost;
            GovContract = entity.GovContract.Number;

            FilesData = entity.Documents.Return(docs => docs.Select(x => new SelectEntityInfo() { Id = x.Id, Name = x.Name }), default(IEnumerable<SelectEntityInfo>));

            WorkAndGoods = new RepositoryDc<WorkAndGoods>().GetAllEx(
                q =>
                    q.Fetch(x => x.CategoryKindOfSupport)
                        .Eager.Where(x => x.ActOfAcceptance.Id == entity.Id)).Select(x => new TableWorkAndGoods(x));

            ActivityType = entity.ActivityType;
        }

        public int Id { get; set; }

        [DisplayName(DocNameCaption)]
        public string DocName { get; set; }

        [DisplayName(DocNumberCaption)]
        public string DocNumber { get; set; }

        [DisplayName(DocDateAdoptionCaption)]
        public string DocDateAdoption { get; set; }

        [DisplayName(CostCaption)]
        public string Cost { get; set; }

        public string GovContract { get; set; }

        private IEnumerable<IEntityName> FilesData { get; set; }

        public IEnumerable<TableWorkAndGoods> WorkAndGoods { get; set; }

        public ActivityTypeEnum? ActivityType { get; set; }

        public decimal CostMoney { get; set; }

        [DisplayName(FileLinkCaption)]
        public string FileLink
        {
            get
            {
                var links = FilesData.Select(LinkBuilder.Link);
                return string.Join("<br />", links);
            }
        }

        public string DeleteLink
        {
            get
            {
                return LinkBuilder.AjaxDeleteLink("DeleteActOfAcceptance", "AccountingObjects", new { id = Id }, "GridActs");
            }
        }

    }
}