﻿using GB.MKRF.Logic.Tabs.Handler;

namespace GB.MKRF.Logic.Tabs
{
    /// <summary>
    /// Регистрация менеджера, логика по отображению доступных вкладок для пользователя
    /// а также по переходу на доступную страницу
    /// </summary>
    public class TabsRegistration : ManagerRegistrationBase<TabsManager>
    {
        protected override void RegisterHandlers(TabsManager manager)
        {
            manager.RegisterHandler<MainTabs>(new MainTabsHandler());

            manager.RegisterHandler<ActivityTabs>(new ActivityHandler());
            manager.RegisterHandler<ActivityCardTabs>(new ActivityCardHandler());
            manager.RegisterHandler<AccountingObjectsTabs>(new AccountingObjectsHandler());
            manager.RegisterHandler<AnalyticsTabs>(new AccountingObjectsHandler());
            manager.RegisterHandler<AccountingObjectsCardTabs>(new AccountingObjectsCardHandler());
        }
    }
}