﻿namespace bgTeam.IsRui.Domain.Entities.Workflow
{
    /// <summary>
    /// Статусы
    /// </summary>
    public enum StatusEnum
    {
        /// <summary>Статус не задан</summary>
        None = 0,

        /// Статусы мероприятий

        /// <summary>Создано</summary>
        ActCreated = 1,

        /// <summary>На экспертизе в ЦА</summary>
        ActOnExpertise = 2,

        /// <summary>На доработке</summary>
        ActOnCompletion = 3,

        /// <summary>Согласовано ЦА</summary>
        ActAgreed = 4,

        /// <summary>Отправлено в МКС</summary>
        ActSendedToMKS = 5,

        /// <summary>Отклонено в МКС</summary>
        ActRejectedAtMKS = 6,

        /// <summary>Включено в план</summary>
        ActIncludedInPlan = 7,

        /// Статусы экспертизы по мероприятию

        /// <summary>Назначаются эксперты</summary>
        ExpAppointment = 50,

        /// <summary>На экспертизе у технического специалиста</summary>
        ExpTechnician = 51,

        /// <summary>На экспертизе у экономиста</summary>
        ExpEconomist = 52,

        /// <summary>Согласовано экспертами</summary>
        ExpAgreed = 53,


        /// Статусы мероприятий

        /// <summary>Подготовка сведений</summary>
        AoDraft = 100,

        /// <summary>На согласовании</summary>
        AoAgreed = 101,

        /// <summary>На доработке</summary>
        AoOnCompletion = 102,

        /// <summary>Cогласовано вышестоящим</summary>
        AoAccordHigher = 103,

        // Статусы заявок

        /// <summary>Создание</summary>
        ClaimCreate = 200,

        /// <summary>На рассмотрении</summary>
        ClaimOnApproval = 201,

        /// <summary>Оценка</summary>
        ClaimValuation = 202,

        /// <summary>На доработке</summary>
        ClaimOnAdaptation = 203,

        /// <summary>Экспертная группа</summary>
        ClaimExpertGroup = 204,

        /// <summary>Реализация</summary>
        ClaimImplementation = 205,

        /// <summary>Завершено</summary>
        ClaimComplete = 206
    }
}