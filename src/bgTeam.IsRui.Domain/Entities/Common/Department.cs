﻿namespace bgTeam.IsRui.Domain.Entities.Common
{
    /// <summary>Орган государственной власти (ведомство)</summary>
    public class Department : EntityTreeDirectories<Department>
    {
        ///// <summary>Родительский орган</summary>
        //public virtual Department Parent { get; set; }//null

        /// <summary>Подведомства</summary>
        //public virtual IList<Department> Childs { get; set; }

        /// <summary>Код</summary>
        public virtual string Code { get; set; }//null

        ///// <summary>Наименование</summary>
        //public virtual string Name { get; set; }//null

        /// <summary>Адрес</summary>
        public virtual string Addr { get; set; }//null

        /// <summary>Краткое наименование</summary>
        public virtual string NameSmall { get; set; }//null

        /// <summary>Телефон</summary>
        public virtual string Phone { get; set; }//null

        /// <summary>Веб-сайт</summary>
        public virtual string Web { get; set; }//null

        /// <summary>Признак активности</summary>
        public virtual bool IsActive { get; set; }//null

        /// <summary>Сводный перечень заказчиков</summary>
        public virtual string SPZ { get; set; }

        /// <summary>ИНН</summary>
        //public virtual string INN { get; set; }

        /// <summary>Тип (ведомство/регион)</summary>
        //public virtual DepartmentType Type { get; set; }

        /// <summary>Исполнители</summary>
        //public virtual IList<Executor> Executors { get; set; }

        /// <summary>При переводе статуса эцп обязательна</summary>
        //public virtual bool SignatureIsRequired { get; set; }

        /// <summary>Отсылать ответственным за мероприятия письма с отчетами по статусам</summary>
        //public virtual bool IsResponsibleRecieveEmails { get; set; }
    }
}