﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Story.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class FinancingReportXlsStory : IStory<FinancingReportXlsStoryContext, bool>
    {
        private readonly IRepositoryRui _repository;
        private readonly IConvertHtmlToXls _converter;

        public FinancingReportXlsStory(IRepositoryRui repository, IConvertHtmlToXls converter)
        {
            _converter = converter;
            _repository = repository;
        }

        public bool Execute(FinancingReportXlsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(FinancingReportXlsStoryContext context)
        {
            var storyReportHtml = new FinancingReportStory(_repository);

            var reportHtml = await storyReportHtml.ExecuteAsync(new FinancingReportStoryContext());

            _converter.ConvertHtmlToXlsFile(reportHtml);

            return true;
        }
    }
}
