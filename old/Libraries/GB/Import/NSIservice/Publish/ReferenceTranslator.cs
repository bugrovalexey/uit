﻿// -----------------------------------------------------------------------
// <copyright file="ReferenceTranslator.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Xml.Serialization;
using System.Text;

namespace Core.Import.NSIservice.Publish
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ReferenceTranslator
    {
        private static readonly Encoding _encoding = Encoding.UTF8;

        /// <summary>
        /// Возвращает каноническое представление элемента справочника из строки XML.
        /// </summary>
        /// <typeparam name="T">Тип канонической формы элемента справочника.</typeparam>
        /// <param name="xml">Данные элемента справочника в виде строки XML.</param>
        /// <returns>Объект канонического представления элемента справочника.</returns>
        public static T GetCanonicalDataFromXmlData<T>(string xml)
        {
            using (MemoryStream memoryStream = new MemoryStream(_encoding.GetBytes(xml)))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(memoryStream);
            }
        }

        /// <summary>
        /// Возвращает строковое представление XML элемента справочника из канонического представления.
        /// </summary>
        /// <typeparam name="T">Тип канонической формы элемента справочника.</typeparam>
        /// <param name="canonicalData">Объект канонического представления элемента справочника.</param>
        /// <returns>Данные элемента справочника в виде строки XML.</returns>
        public static string GetXmlDataFromCanonicalData<T>(T canonicalData)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                var nss = new XmlSerializerNamespaces();
                nss.Add(string.Empty, string.Empty);
                xmlSerializer.Serialize(memoryStream, canonicalData, nss);
                memoryStream.Position = 0;
                byte[] byteArray = new byte[memoryStream.Length];
                memoryStream.Read(byteArray, 0, byteArray.Length);
                return _encoding.GetString(byteArray);
            }
        }

    }
}
