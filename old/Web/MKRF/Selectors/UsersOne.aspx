﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="UsersOne.aspx.cs" Inherits="GB.MKRF.Web.Selectors.UsersOne" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript">
        function RowClick(s, e) {
            var id = s.GetRowKey(e.visibleIndex);
            s.GetRowValues(e.visibleIndex, 'Id;Name', function (values) {
                var result = id + ';' + values[1];

                window.parent.PopupClose('<% =Request.QueryString["type"] %>', result);
            });
        }
    </script>
    <dxwgv:ASPxGridView ID="UsersGrid" runat="server" ClientInstanceName="Grid" Width="100%"
        AutoGenerateColumns="False" KeyFieldName="Id" 
        OnBeforePerformDataSelect="UsersGrid_BeforePerformDataSelect"
        OnHtmlRowCreated="UsersGrid_HtmlRowCreated">
        <Columns>
            <dxwgv:GridViewDataMemoColumn Caption="ФИО" FieldName="Name" />
            <dxwgv:GridViewDataMemoColumn Caption="ОГВ" FieldName="Department" />
            <dxwgv:GridViewDataMemoColumn Caption="Должность" FieldName="Job" />
            <dxwgv:GridViewDataMemoColumn Caption="Роль" FieldName="Role" />
            <dxwgv:GridViewDataTextColumn Caption="Электропочта" FieldName="Email" />
            <dxwgv:GridViewDataTextColumn Caption="Номер телефона" FieldName="Phone" />
        </Columns>
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="20" />
        <ClientSideEvents RowClick="RowClick" />
    </dxwgv:ASPxGridView>
</asp:Content>
