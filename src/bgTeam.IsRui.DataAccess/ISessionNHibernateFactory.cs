﻿namespace bgTeam.IsRui.DataAccess
{
    public interface ISessionNHibernateFactory
    {
        ISessionNHibernate OpenSession();
    }
}