﻿using bgTeam.DataAccess;
using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess.Command;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Domain.Entities.Workflow;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.MKRF.Logic.Status;
using System;
using System.Web.Mvc;

namespace bgTeam.IsRui.WebApp.Controllers
{
    public class StatusController : Controller
    {
        public ICommandBuilder _Command { get; set; }

        /// <summary>
        /// Перевести статус
        /// </summary>
        [HttpPost]
        public ActionResult Change(int id, EntityType type,  int status)
        {
            return NotificationHelper.NotificationExecute(() =>
            {
                var obj = PreChange(id, type, status);
                
                Change(obj as IWorkflowObject, status);
            },
            "Статус успешно переведён",
            "При переводе статуса возникла ошибка");
        }


        private void Change(IWorkflowObject obj, int newStatusId, string comment = null)
        {
            Status nowStatus = obj.Status;
            Status newStatus = new RepositoryDc<Status>().Get(newStatusId);

            if (newStatus == null)
                throw new ArgumentException("Не найден статус", "newStatusId");

            var handler = Singleton<WorkflowManager>.Instance.GetHandler(obj.GetType());
            if (handler != null)
            {
                string confirm, error;
                if (!handler.ReadyForNewStatus(obj, newStatus, out error, out confirm))
                {
                    throw new InvalidOperationException(error);
                }

                obj.Status = newStatus;

                _Command.Build(new StatusChangeContext() { Object = obj, funChangeAfter = handler.ChangeAfter }).Execute();
            }
            else
                throw new Exception("Не найден обработчик для - " + obj.GetType().Name);
        }

        private IEntity PreChange(int id, EntityType type, int status)
        {
            StatusEnum st = (StatusEnum)status;

            switch (st)
            {
                //case StatusEnum.None:
                //    break;
                //case StatusEnum.ActCreated:
                //    break;
                //case StatusEnum.ActOnExpertise:
                //    break;
                case StatusEnum.ActOnCompletion:
                    if(type == EntityType.ActivityExpertise)
                    {
                        return new RepositoryDc<PlansActivity>().Get(q => q.Where(x=>x.ExpertiseConclusion.Id == id));
                    }
                    break;
                //case StatusEnum.ActAgreed:
                //    break;
                //case StatusEnum.ActSendedToMKS:
                //    break;
                //case StatusEnum.ActRejectedAtMKS:
                //    break;
                //case StatusEnum.ActIncludedInPlan:
                //    break;
                //case StatusEnum.ExpAppointment:
                //    break;
                //case StatusEnum.ExpTechnician:
                //    break;
                //case StatusEnum.ExpEconomist:
                //    break;
                //case StatusEnum.ExpAgreed:
                //    break;
                //case StatusEnum.AoDraft:
                //    break;
                //case StatusEnum.AoAgreed:
                //    break;
                //case StatusEnum.AoOnCompletion:
                //    break;
                //case StatusEnum.AoAccordHigher:
                //    break;
                //default:
                //    break;
            }

            return RepositorySt.Get(type, id);
        }
    }
}
