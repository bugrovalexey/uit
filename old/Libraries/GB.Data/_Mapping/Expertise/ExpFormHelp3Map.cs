﻿using Uviri.MKRF.Entities.Expertise;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Expertise
{
    public class ExpFormHelp3Map : ClassMapBase<ExpFormHelp3>
    {
        public ExpFormHelp3Map()
            : base("Exp_Concl_Sprv_")
        {
//            Table("w_3");

//            Id(x => x.Id, "Exp_Concl_Sprv_3_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            string sql = @"@Exp_Concl_Sprv_3_Info = ?
//                          ,@Plans_Activity_ID = ?
//                          ,@Exp_Concl_Sprv_ID = ?
//						  ,@Exp_Concl_Sprv_3_ID = ?";

//            SqlInsert(@"exec ZZZ_Prc_Exp_Concl_Sprv_3_Insert " + sql).Check.None();
//            SqlUpdate(@"exec ZZZ_Prc_Exp_Concl_Sprv_3_Update " + sql).Check.None();
//            SqlDelete(@"exec ZZZ_Prc_Exp_Concl_Sprv_3_Delete @Exp_Concl_Sprv_3_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Info, "Exp_Concl_Sprv_3_Info");

            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID").Not.Nullable();
            References<ExpFormHelp>(x => x.FormHelp, "Exp_Concl_Sprv_ID").Not.Nullable();
        }
    }
}
