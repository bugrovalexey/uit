﻿namespace bgTeam.IsRui.Domain.Entities.Claims
{
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;

    /// <summary>
    /// Документы заявки
    /// </summary>
    public class ClaimDocument : EntityBase
    {
        /// <summary>
        /// Заявка
        /// </summary>
        public virtual Claim Claim { get; set; }

        /// <summary>
        /// Файл
        /// </summary>
        public virtual Files File { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        public virtual string Number { get; set; }

        public virtual string ParticleOfDocument { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public virtual DateTime? DateAdoption { get; set; }
    }
}
