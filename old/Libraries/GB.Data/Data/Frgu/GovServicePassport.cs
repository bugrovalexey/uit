﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.Data.Frgu
{
    /// <summary>
    /// Паспорт гос. услуги
    /// </summary>
    public class GovServicePassport : EntityBase
    {
        /// <summary>Полное наименование услуги</summary>
        public virtual string Name { get; set; }

        /// <summary>Краткое наименование услуги</summary>
        public virtual string SName { get; set; }

        /// <summary>Уникальный реестровый номер услуги</summary>
        public virtual string Number { get; set; }

        /// <summary>ОГВ</summary>
        public virtual string OGV { get; set; }

        /// <summary>Оригинальный идентификатор</summary>
        //public virtual long Orig_Id { get; set; }

        /// <summary>Описание результата предоставления услуги</summary>
        public virtual string Result { get; set; }

        /// <summary> НПА документы </summary>
        public virtual IList<GovService> Services { get; protected set; }
    }
}
