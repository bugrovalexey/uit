﻿using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class OfficialMap : ClassMapBase<Official>
    {
        public OfficialMap()
            : base("Officials")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.FirstName, "Officials_First_Name");
            Map(x => x.LastName, "Officials_Last_Name");
            Map(x => x.SurName, "Officials_Surname");
            Map(x => x.Post, "Officials_Post");
            Map(x => x.Experience, "Officials_Exp");
            Map(x => x.Qualification, "Officials_Qualification");

            References<ManagementRole>(x => x.ManagementRole, "Gov_Struct_Role_ID");
            References<Plan>(x => x.Plan, "Plans_ID");
        }
    }
}