﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Story.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoConclusionFileAddStory : IStory<AoConclusionFileAddStoryContext, FilesDto>
    {
        private readonly IMapperBase _mapper;
        private readonly IRepositoryRui _repository;
        private readonly IUserRepository _userRepository;
        private readonly ISessionNHibernateFactory _factory;
        private readonly IFileProviderRui _fileProvider;

        public AoConclusionFileAddStory(
            IMapperBase mapper,
            IRepositoryRui repository,
            IUserRepository userRepository,
            ISessionNHibernateFactory factory,
            IFileProviderRui fileProvider)
        {
            _mapper = mapper;
            _repository = repository;
            _userRepository = userRepository;
            _factory = factory;
            _fileProvider = fileProvider;
        }

        public FilesDto Execute(AoConclusionFileAddStoryContext context)
        {
            return ExecuteAsync(context).GetAwaiter().GetResult();
        }

        public async Task<FilesDto> ExecuteAsync(AoConclusionFileAddStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            if (!context.Id.HasValue)
            {
                var createStory = new AoConclusionAddOrUpdateStory(_mapper, _repository, _userRepository, _factory);

                var aoDto = await createStory.ExecuteAsync(new AoConclusionAddOrUpdateStoryContext
                {
                    AoId = context.AoId.Value
                });

                context.Id = aoDto.Id;
            }

            AccountingObjectUserComment ao = _repository.Get<AccountingObjectUserComment>(context.Id);

            var addfilecontext = _mapper.Map(context, new FileAddStoryContext());

            addfilecontext.Type = DocTypeEnum.AccountingObjectCommentDocument;
            addfilecontext.Entity = ao;

            var result = await new FileAddStory(_mapper, _repository, _fileProvider, _factory)
                .ExecuteAsync(addfilecontext);

            //var fileStory = new FileAddStory(_mapper, _repository, _fileProvider, _factory);

            //var file = await fileStory.ExecuteAsync(new FileAddStoryContext()
            //{
            //    Type = DocTypeEnum.AccountingObjectCommentDocument,
            //    Entity = ao,
            //    Name = context.FileName,
            //    TempGuid = context.TempGuid,
            //});

            return _mapper.Map(result, new FilesDto());
        }
    }
}
