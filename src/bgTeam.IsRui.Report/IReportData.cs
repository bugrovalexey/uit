﻿namespace bgTeam.IsRui.Report
{
    /// <summary>
    /// Данные отчёта
    /// </summary>
    public interface IReportData
    {
        /// <summary>
        /// Тип отчета
        /// </summary>
        string Type { get; }

        /// <summary>
        /// Параметры отчёта
        /// </summary>
        object[] Param { get; }

        /// <summary>
        /// Формат выгрузки отчёта. Используется при экспорте
        /// </summary>
        ExportFormat Format { get; }

        /// <summary>
        /// Получить репозитарий отчёта
        /// </summary>
        BaseReportRepository GetRepository();

        /// <summary>
        /// Получить экспортер отчёта
        /// </summary>
        BaseReportExport GetExporter();
    }
}
