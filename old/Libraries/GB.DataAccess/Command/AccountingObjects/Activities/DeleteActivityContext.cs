﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.Activities
{
    public class DeleteActivityContext : ICommandContext
    {
        public int Id { get; set; }
    }
}
