﻿namespace bgTeam.IsRui.Story.AccessObjects
{
    using bgTeam.IsRui.Domain;
    using bgTeam.IsRui.Domain.Entities;
    using System;
    using System.Threading.Tasks;

    public interface IAccessEditService
    {
        void CheckAccessEdit<TWorkflowEntity>(TWorkflowEntity entity)
            where TWorkflowEntity : class, IEntityWorkflow, IEntity;

        Task CheckAccessEditAsync<TWorkflowEntity>(TWorkflowEntity entity)
            where TWorkflowEntity : class, IEntityWorkflow, IEntity;
    }
}
