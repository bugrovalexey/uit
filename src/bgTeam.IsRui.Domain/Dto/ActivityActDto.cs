﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityActDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public DateTime? DateAdoption { get; set; }

        public int Cost { get; set; }

        public ActsFileDto File { get; set; }

        public int FileId { get; set; }

        public int ActivityId { get; set; }

        public DateTime? ChangeDate { get; set; }
    }
}
