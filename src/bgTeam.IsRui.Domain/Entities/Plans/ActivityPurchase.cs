﻿namespace bgTeam.IsRui.Domain.Entities.Plans
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityPurchase : EntityBase
    {
        public virtual PlansActivity Activity { get; set; }

        public virtual int Number { get; set; }

        public virtual Month Month { get; set; }

        public virtual DateTime? PublishDate { get; set; }

        public virtual DateTime? AcceptanceEndDate { get; set; }

        public virtual DateTime? ResultDate { get; set; }
    }

    public enum Month
    {
        [Description("Январь")]
        January = 0,

        [Description("Февраль")]
        February = 1,

        [Description("Март")]
        March = 2,

        [Description("Апрель")]
        April = 3,

        [Description("Май")]
        May = 4,

        [Description("Июль")]
        June = 5,

        [Description("Июнь")]
        July = 6,

        [Description("Август")]
        August = 7,

        [Description("Сентябрь")]
        September = 8,

        [Description("Октябрь")]
        October = 9,

        [Description("Ноябрь")]
        November = 10,

        [Description("Декабрь")]
        December = 11
    }
}
