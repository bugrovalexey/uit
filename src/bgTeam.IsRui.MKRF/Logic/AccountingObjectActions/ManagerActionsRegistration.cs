﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.AccountingObjectActions.Handler;

namespace bgTeam.IsRui.MKRF.Logic.AccountingObjectActions
{
    public class ManagerActionsRegistration : ManagerRegistrationBase<ManagerActions>
    {
        protected override void RegisterHandlers(ManagerActions manager)
        {
            manager.RegisterHandler(AccountingObjectsStageEnum.Planned, new ActionPlannedHandler());
            manager.RegisterHandler(AccountingObjectsStageEnum.Creation, new ActionCreationHandler());
            manager.RegisterHandler(AccountingObjectsStageEnum.Exploitation, new ActionExploitationHandler());
            manager.RegisterHandler(AccountingObjectsStageEnum.OutExploitation, new ActionOutExploitationHandler());
        }
    }
}
