﻿namespace bgTeam.IsRui.WebApp.Controllers
{
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.Domain._Mapping;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Story.AccountingObjects;
    using bgTeam.IsRui.Story.Common;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    /// <summary>
    /// Контроллер взаимодействия с главной страницей
    /// </summary>
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class AccountingObjectsController : Controller
    {
        private readonly IStoryBuilder _builder;
        private readonly IHttpContextAccessor _contextAccessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountingObjectsController"/> class.
        /// </summary>
        /// <param name="builder"></param>
        public AccountingObjectsController(IStoryBuilder builder, IHttpContextAccessor contextAccessor)
        {
            _builder = builder;
            _contextAccessor = contextAccessor;
        }

        /// <summary>
        /// Создать объект учёта
        /// </summary>
        /// <exception cref="IsRuiException">Исключение</exception>
        /// <param name="context">Объект запроса, содержащий наименование объекта учёта</param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<EntityDefault> Create([FromBody]AccountingObjectCreateStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<EntityDefault>();
        }

        /// <summary>
        /// Удалить объект учета
        /// </summary>
        /// <param name="context">Id - идентификатор ОУ</param>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> Delete([FromBody]AccountingObjectDeleteStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        #region List
        /// <summary>
        /// Получение списка объектов учета
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IEnumerable<AccountingObjectDto>> GetMyList([FromBody]AccountingObjectListStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<AccountingObjectDto>>();
        }

        /// <summary>
        /// Получение списка объектов учета не связанных в пользователем
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IEnumerable<AccountingObjectDto>> GetAll([FromBody]AccountingObjectGetAllStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<AccountingObjectDto>>();
        }

        /// <summary>
        /// Добавленее объекта учета в избранное
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> FavoriteAdd([FromBody]AccountingObjecFavoriteAddStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }
        #endregion

        #region Conclusion
        /// <summary>
        /// Получение текущее заключение
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<AccountingObjectUserCommentDto> ConclusionGet([FromBody] AoConclusionGetStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<AccountingObjectUserCommentDto>();
        }

        /// <summary>
        /// Добавить либо обновить информацию по заключению
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<AccountingObjectUserCommentDto> ConclusionAddOrUpdate([FromBody] AoConclusionAddOrUpdateStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<AccountingObjectUserCommentDto>();
        }

        /// <summary>
        /// Добавить файл к заключению
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<FilesDto> ConclusionAddFile([FromBody] AoConclusionFileAddStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<FilesDto>();
        }

        /// <summary>
        /// Удалить файл у заключения
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> ConclusionDeleteFile([FromBody] FileDeleteStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получение истории заключений по ОУ
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IEnumerable<ConclusionHistoryDto>> GetConclusionHistory([FromBody]AccountingObjecGetConclusionHistoryStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ConclusionHistoryDto>>();
        }
        #endregion

        #region CardView
        /// <summary>
        /// Получить подробную информацию об объекте учёта
        /// </summary>
        /// <param name="context">Id - идентификатор ОУ</param>
        /// <returns>Иформация об объекте учёта</returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<AccountingObjectInfoDto> GetObjectInfo([FromBody]AccountingObjectInfoStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<AccountingObjectInfoDto>();
        }

        /// <summary>
        /// Изменение стадии ОУ
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        [Produces("application/json")]
        public async Task<AccountingObjectChangeStageDto> ChangeStage([FromBody]AccountingObjectChangeStageStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<AccountingObjectChangeStageDto>();
        }

        /// <summary>
        /// Изменение статуса ОУ
        /// </summary>
        /// <param name="context"></param>
        [HttpPost]
        [Produces("application/json")]
        public async Task<AccountingObjectChangeStatusDto> ChangeStatus([FromBody]AccountingObjectChangeStatusStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<AccountingObjectChangeStatusDto>();
        }

        /// <summary>
        /// Загрузка списка документов
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IEnumerable<AODecisionToCreateDto>> DocumentBaseGetAllDocuments([FromBody] AoDocumentBaseGetAllDocumentsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<AODecisionToCreateDto>>();
        }

        /// <summary>
        /// Создание/редактирование документа
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<CreateFileAnswerDto> DocumentBaseCreateOrUpdateDocument([FromBody] AoDocumentBaseCreateOrUpdateDocumentStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<CreateFileAnswerDto>();
        }

        /// <summary>
        /// Удаление документа
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<AoDeleteFileDto> DocumentBaseDeleteDocument([FromBody] AoDocumentBaseDeleteDocumentStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<AoDeleteFileDto>();
        }

        /// <summary>
        /// Получение информации о целях и назначении ОУ
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<GoalsAndPurposeInfoDto> GoalsAndPurposeGetInfo([FromBody] AoGoalsAndPurposeGetInfoStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<GoalsAndPurposeInfoDto>();
        }

        /// <summary>
        /// Изменение информации о целях и назначении ОУ
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> GoalsAndPurposeUpdateInfo([FromBody] AoGoalsAndPurposeUpdateInfoStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Загрузка основной информации об ОУ
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<AoCommonInfoDto> CommonInfoGet([FromBody] AoCommonInfoGetStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<AoCommonInfoDto>();
        }

        /// <summary>
        /// Изменение основной информации об ОУ
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> CommonInfoUpdate([FromBody] AoCommonInfoUpdateStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        ///  Загрузка функциональных характеристик ОУ
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IEnumerable<AoAllCharacteristicsDto>> CharacteristicsGetAllCharacteristics([FromBody] AoCharacteristicsGetAllCharacteristicsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<AoAllCharacteristicsDto>>();
        }

        /// <summary>
        ///  Добавление/обновление функциональной характеристики ОУ
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<CharacteristicsAddOrUpdateDto> CharacteristicsAddOrUpdateCharacteristic([FromBody] AoCharacteristicsAddOrUpdateCharacteristicStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<CharacteristicsAddOrUpdateDto>();
        }

        /// <summary>
        ///  Удаление функциональной характеристики ОУ
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> CharacteristicsDeleteCharacteristic([FromBody] AoCharacteristicsDeleteCharacteristicStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        ///  Получение списка ПО
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IEnumerable<AllSoftwareDto>> SoftwareGetAllSoftware([FromBody] AoSoftwareGetAllSoftwareStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<AllSoftwareDto>>();
        }

        /// <summary>
        ///  Добавление/обновление записи о ПО
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<AddOrUpdateSoftwareDto> SoftwareAddOrUpdateSoftware([FromBody] AoSoftwareAddOrUpdateSoftwareStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<AddOrUpdateSoftwareDto>();
        }

        /// <summary>
        ///  Удаление записи о ПО
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> SoftwareDeleteSoftware([FromBody] AoSoftwareDeleteSoftwareStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получение списка технического оборудования
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IEnumerable<AllTechnicalSupportDto>> TechnicalSupportGetAllTechSupport([FromBody] AoTechnicalSupportGetAllTechSupportStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync< IEnumerable<AllTechnicalSupportDto>>();
        }

        /// <summary>
        /// Добавление/обновление записи о Техническом оборудовании
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<TechnicalSupportAddOrUpdateDto> TechnicalSupportAddOrUpdateTechSupport([FromBody] AoTechnicalSupportAddOrUpdateTechSupportStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<TechnicalSupportAddOrUpdateDto>();
        }

        /// <summary>
        ///  Удаление записи о Техническом оборудовании
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> TechnicalSupportDeleteTechSupport([FromBody] AoTechnicalSupportDeleteTechSupportStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Добавление/обновление характеристики технического оборудования
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<TechnicalSupportAddOrUpdateTechSupportCharacteristicDto> TechnicalSupportAddOrUpdateTechSupportCharacteristic([FromBody] AoTechnicalSupportAddOrUpdateTechSupportCharacteristicStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<TechnicalSupportAddOrUpdateTechSupportCharacteristicDto>();
        }

        /// <summary>
        ///  Удаление записи о Характеристике технического оборудования
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> TechnicalSupportDeleteTechSupportCharacteristic([FromBody] AoTechnicalSupportDeleteTechSupportCharacteristicStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получение информации о работах и услугах
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IEnumerable<WorkAndServiceGetAllWorkDto>> WorkAndServiceGetAllWork([FromBody] AoWorkAndServiceGetAllWorkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<WorkAndServiceGetAllWorkDto>>();
        }

        /// <summary>
        /// Изменение/добавление записей о работах-услугах
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<WorkAndServiceAddOrUpdateWorkDto> WorkAndServiceAddOrUpdateWork([FromBody] AoWorkAndServiceAddOrUpdateWorkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<WorkAndServiceAddOrUpdateWorkDto>();
        }

        /// <summary>
        ///  Удаление записи о о работе/услуге
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> WorkAndServiceDeleteWork([FromBody] AoWorkAndServiceDeleteWorkStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получение информации о финансировании
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<FinancingDto> FinancingGetInfo([FromBody] AoFinancingGetInfoStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<FinancingDto>();
        }

        /// <summary>
        /// Обновление информации о финансировании
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<FinancingDto> FinancingUpdateInfo([FromBody] AoFinancingUpdateInfoStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<FinancingDto>();
        }

        /// <summary>
        /// Получение информации о контрактах
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IEnumerable<ActivityContractDto>> GetAllContracts([FromBody] AoContractsGetAllContractsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivityContractDto>>();
        }

        /// <summary>
        /// Обновление информации о контракте
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<ContractDto> ContractsAddOrUpdateContract([FromBody] AoContractAddOrUpdateStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ContractDto>();
        }

        /// <summary>
        ///  Удаление записи о контракте
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> ContractsDeleteContract([FromBody] AoContractsDeleteContractStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Получение информации об актах
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IEnumerable<ActsDto>> ActsGetAllActs([FromBody] AoActsGetAllActsStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActsDto>>();
        }

        /// <summary>
        /// Обновление информации об акте
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<ActsDto> ActsAddOrUpdateActStory([FromBody] AoActsAddOrUpdateActStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<ActsDto>();
        }

        /// <summary>
        ///  Удаление записи об акте
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<bool> ActsDeleteAct([FromBody] AoActsDeleteActStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<bool>();
        }

        /// <summary>
        /// Финансирование инвестиционной программы
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<string> FinancingReport([FromBody] FinancingReportStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<string>();
        }

        /// <summary>
        /// Финансирование инвестиционной программы в Excel
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> FinancingReportExcel()
        {
           var result = await _builder.Build(new FinancingReportXlsStoryContext()).ReturnAsync<bool>();

            if (result)
            {
                var path = Path.Combine(Environment.CurrentDirectory, @"wwwroot\Temp\ReportExcel.xlsx");

                FileStream fs = new FileStream(path, FileMode.Open);

                string fileType = "application/xlsx";

                string fileName = "ReportExcel.xlsx";

                var response = File(fs, fileType, fileName);

                return response;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Финансирование инвестиционной программы в PDF
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> FinancingReportPDF()
        {
            var result = await _builder.Build(new FinancingReportPDFStoryContext()).ReturnAsync<bool>();

            if (result)
            {
                var path = Path.Combine(Environment.CurrentDirectory, @"wwwroot\Temp\ReportPDF.pdf");

                FileStream fs = new FileStream(path, FileMode.Open);

                string fileType = "application/pdf";

                string fileName = "ReportPDF.pdf";

                var response = File(fs, fileType, fileName);

                return response;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Получить список мероприятий, направленных на ОУ
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IEnumerable<ActivityDto>> GetLinkedActivities([FromBody] AoGetLinkedActivitiesStoryContext context)
        {
            return await _builder
                .Build(context)
                .ReturnAsync<IEnumerable<ActivityDto>>();
        }
        #endregion
    }
}
