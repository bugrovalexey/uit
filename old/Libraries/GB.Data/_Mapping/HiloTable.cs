﻿using GB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB._Mapping
{
    public class HiloTable : EntityBase
    {
        public virtual int NextHi { get; set; }

        public virtual string TableKey { get; set; }
    }

    internal class HiloTableMap : ClassMapBase<HiloTable>
    {
        public HiloTableMap()
            : base("NH_HiLo")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.TableKey);
            Map(x => x.NextHi);
        }
    }
}
