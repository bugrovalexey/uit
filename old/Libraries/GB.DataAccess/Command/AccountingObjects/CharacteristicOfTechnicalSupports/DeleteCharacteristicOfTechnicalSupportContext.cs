﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.CharacteristicOfTechnicalSupports
{
    public class DeleteCharacteristicOfTechnicalSupportContext : ICommandContext
    {
        public int? xId { get; set; }
    }
}
