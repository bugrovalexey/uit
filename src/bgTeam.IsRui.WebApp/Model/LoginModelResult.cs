﻿namespace bgTeam.IsRui.WebApp.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Dto;

    public class LoginModelResult
    {
        public UserDto User { get; }

        public bool Result { get; set; }

        public string Error { get; set; }

        public LoginModelResult(UserDto user)
        {
            User = user;
            Result = true;
        }

        public LoginModelResult(string err)
        {
            Error = err;
        }
    }
}
