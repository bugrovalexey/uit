﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects
{
    public class UpdateGoalAndPurposeAccountingObjectContext : ICommandContext
    {        
        public string Targets { get; set; }
        
        public string PurposeAndScope { get; set; }

        public int xId { get; set; }
    }
}
