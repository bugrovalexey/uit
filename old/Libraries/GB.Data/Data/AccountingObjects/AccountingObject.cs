﻿using GB.Data.AccountingObjects.Documents;
using GB.Data.AccountingObjects.WorksAndGoods;
using GB.Data.Common;
using GB.Data.Directories;
using GB.Data.Workflow;
using System;
using System.Collections.Generic;
using System.Linq;
using GB.Extentions;

namespace GB.Data.AccountingObjects
{
    /// <summary>
    /// Объект учета
    /// </summary>
    public class AccountingObject : EntityBase, IWorkflowObject
    {

        public AccountingObject() { }

        internal AccountingObject(int id)
        {
            this.Id = id;
        }

        public AccountingObject(AccountingObjectEnum mode)
        {
            this.Mode = mode;
        }

        /// <summary>
        /// Дата создания
        /// </summary>
        public virtual DateTime CreateDate { get; set; }

        /// <summary>Режим ОУ (составное или одинарное)</summary>
        public virtual AccountingObjectEnum Mode { get; protected set; }

        /// <summary>
        /// Родитель
        /// </summary>
        public virtual AccountingObject Parent { get; set; }

        /// <summary>
        /// Учреждение, которое добавило в систему этот ОУ
        /// </summary>
        public virtual Department Department { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public virtual Status Status { get; set; }

        /// <summary>
        /// Стадия
        /// </summary>
        public virtual AccountingObjectsStageEnum Stage { get; set; }

        /// <summary>
        /// Уникальный идентификационный номер ОУ;
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        /// Тип (родитель)
        /// </summary>
        public virtual IKTComponent Type { get; set; }

        /// <summary>
        /// Вид (потомок)
        /// </summary>
        public virtual IKTComponent Kind { get; set; }

        /// <summary>
        /// Полное наименование
        /// </summary>
        public virtual string FullName { get; set; }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        public virtual string ShortName { get; set; }

        /// <summary>
        /// Решения о создании (закупки) ОУ
        /// </summary>
        public virtual IList<AODecisionToCreate> Documents { get; set; }

        /// <summary>
        /// Цели создания ОУ
        /// </summary>
        public virtual string Targets { get; set; }

        /// <summary>
        /// Назначение и область применения объекта учета
        /// </summary>
        public virtual string PurposeAndScope { get; set; }

        /// <summary>
        /// Характеристики
        /// </summary>
        public virtual IList<AccountingObjectCharacteristics> Characteristics { get; set; }

        /// <summary>
        /// Уровень защищенности
        /// </summary>
        public virtual LevelOfSecurity? LevelOfSecurity { get; set; }

        /// <summary>
        /// Сведения о прохождении специальных проверок
        /// </summary>
        public virtual IList<SpecialCheck> SpecialChecks { get; set; }

        #region Раздел "Сведения о принятие к бюджетному учету"

        /// <summary>
        /// Дата принятия к бюджетному учету
        /// </summary>
        public virtual DateTime? AdoptionBudgetDate { get; set; }

        /// <summary>Код по БК ГРБС</summary>
        public virtual GRBS BK_GRBS { get; set; }

        /// <summary>Код по БК ПЗ/РЗ</summary>
        public virtual SectionKBK BK_PZRZ { get; set; }

        /// <summary>Код по БК ЦСР</summary>
        public virtual ExpenseItem BK_CSR { get; set; }

        /// <summary>Код по БК ВР</summary>
        public virtual WorkForm BK_WR { get; set; }

        /// <summary> Код по БК КОСГУ </summary>
        public virtual ExpenditureItem BK_KOSGU { get; set; }

        /// <summary>
        /// Балансовая стоимость
        /// </summary>
        public virtual decimal BalanceCost { get; set; }

        /// <summary>
        /// Основания принятия к бюджетному учету
        /// </summary>
        public virtual string AdoptionBudgetBasis { get; set; }

        public virtual IList<AODocumentGroundsForCreation> DocumentsGroundsForCreation { get; protected set; }

        /// <summary>
        /// Документ ввода в эксплуатацию ОУ
        /// </summary>
        public virtual AODocumentGroundsForCreation DocumentGroundsForCreation
        {
            get { return DocumentsGroundsForCreation.Return(docs => docs.FirstOrDefault()); }
        }
        
        #endregion Раздел "Сведения о принятие к бюджетному учету"

        #region Раздел "Сведения о вводе в эксплуатацию"

        public virtual IList<AODocumentCommissioning> DocumentsCommissioning { get; protected set; }

        /// <summary>
        /// Документ ввода в эксплуатацию ОУ
        /// </summary>
        public virtual AODocumentCommissioning DocumentCommissioning
        {
            get { return DocumentsCommissioning.Return(docs => docs.FirstOrDefault()); }
        }        

        /// <summary>
        /// Введен в эксплуатацию
        /// </summary>
        public virtual DateTime? CommissioningDate { get; set; }

        #endregion Раздел "Сведения о вводе в эксплуатацию"

        #region Раздел "Сведения о выводе из эксплуатации"

        public virtual IList<AODocumentDecommissioning> DocumentsDecommissioning { get; protected set; }

        /// <summary>
        /// Документ вывода из эксплуатации ОУ
        /// </summary>
        public virtual AODocumentDecommissioning DocumentDecommissioning
        {
            get { return DocumentsDecommissioning.Return(docs => docs.FirstOrDefault()); }
        }

        /// <summary>Выведен из эксплуатации</summary>
        public virtual DateTime? DecommissioningDate { get; set; }

        /// <summary>Выведен из эксплуатации</summary>
        public virtual string DecommissioningReasons { get; set; }

        #endregion Раздел "Сведения о выводе из эксплуатации"

        #region Вкладка "Ответственные"

        /// <summary>
        /// Учреждение, уполномоченное на создание, развитие ОУ
        /// </summary>
        public virtual Department ResponsibleDepartment { get; set; }

        #region Раздел  "Учреждение или иное юридическое лицо, уполномоченное на эксплуатацию"

        /// <summary>
        /// Учреждение или иное юридическое лицо, уполномоченное на эксплуатацию
        /// </summary>
        public virtual AOExploitationDepartment ExploitationDepartment { get; set; }

        #endregion Раздел  "Учреждение или иное юридическое лицо, уполномоченное на эксплуатацию"

        #region Раздел "Сведения об ответственных должностных лицах"

        /// <summary>
        /// Ответственный за координацию мероприятий
        /// по информатизации
        /// </summary>
        public virtual Responsible ResponsibleCoord { get; set; }

        /// <summary>
        /// Ответственный за обеспечение эксплуатации
        /// </summary>
        public virtual Responsible ResponsibleExploitation { get; set; }

        /// <summary>
        /// Ответственный за организацию закупок
        /// </summary>
        public virtual Responsible ResponsiblePurchase { get; set; }

        /// <summary>
        /// Ответственный за размещение сведений об объекте учета
        /// </summary>
        public virtual Responsible ResponsibleInformation { get; set; }

        #endregion Раздел "Сведения об ответственных должностных лицах"

        #region Раздел "Сведения о фактическом месторасположении объекта учета"

        /// <summary>
        /// Наименование организации
        /// </summary>
        public virtual string LocationOrgName { get; set; }

        /// <summary>
        /// ФИО ответственного
        /// </summary>
        public virtual string LocationFioResponsible { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public virtual string LocationOrgAddress { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public virtual string LocationOrgPhone { get; set; }

        #endregion Раздел "Сведения о фактическом месторасположении объекта учета"

        #endregion Вкладка "Ответственные"

        #region Вкладка "Виды обеспечения."

        /// <summary>
        /// Техническое обеспечение ОУ
        /// </summary>
        public virtual IList<TechnicalSupport> TechnicalSupports { get; set; }

        /// <summary>
        /// Комментарии пользователей ОУ
        /// </summary>
        public virtual IList<AccountingObjectUserComment> UserComments { get; set; }

        /// <summary>
        /// Программное обеспечение ОУ
        /// </summary>
        public virtual IList<Software> Softwares { get; set; }

        /// <summary>
        /// Работы и услуги ОУ
        /// </summary>
        public virtual IList<WorkAndService> WorksAndServices { get; set; }

        public virtual IList<WorkAndGoods> WorkAndGoods { get; set; }

        public virtual IList<GovPurchase> GovPurchases { get; set; }

        public virtual IList<GovContract> GovContracts { get; set; }

        #endregion Вкладка "Виды обеспечения."

        #region  Cведения об информационном взаимодействии с иными ИС и компонентами ИТКИ

        /// <summary>
        /// Внутренние ИС и компоненты ИТКИ
        /// </summary>
        public virtual IList<InternalIsAndComponentsItki> InternalIsAndComponentsItki { get; set; }

        /// <summary>
        /// Зависимые внутренние ИС и компоненты ИТКИ
        /// </summary>
        protected internal virtual IList<InternalIsAndComponentsItki> RelationInternalIsAndComponentsItki { get; set; }

        /// <summary>
        /// Внешние ИС и компоненты ИТКИ
        /// </summary>
        public virtual IList<ExternalIsAndComponentsItki> ExternalIsAndComponentsItki { get; set; }

        /// <summary>
        /// Внешние пользовательские интерфейсы ИС
        /// </summary>
        public virtual IList<ExternalUserInterfaceOfIs> ExternalUserInterfacesOfIs { get; set; }


        #endregion  Cведения об информационном взаимодействии с иными ИС и компонентами ИТКИ

        ///// <summary></summary>
        //public virtual string ActivityName { get; set; }

        ///// <summary></summary>
        //public virtual ActivityType ActivityType { get; set; }

        ///// <summary></summary>
        //public virtual double? ActivityVol0 { get; set; }

        ///// <summary></summary>
        //public virtual double? ActivityVol1 { get; set; }

        ///// <summary></summary>
        //public virtual double? ActivityVol2 { get; set; }

        public virtual IList<AOFavorite> Favorites { get; set; }

        //public virtual IList<Plans.PlansActivity> Activities { get; set; }

        /// <summary>
        /// Информационная система, направлена на информирование о деятельности государственного органа
        /// </summary>
        public virtual bool InformAboutActivityGovOrgan { get; set; }

        /// <summary>
        /// Мероприятия ОУ. Для маппинга, для каскада.
        /// </summary>
        internal protected virtual IList<AoActivity> AoActivities { get; set; }

        internal protected virtual IList<AOGovService> AOGovServices { get; set; }

        public virtual decimal ActivitiesCost 
        { 
            get
            {
                return AoActivities.Sum(a => a.CostY0);
            }
        } 

        public virtual string  CodeBK
        {
            get
            {
                var codes = new List<string>();
                
                BK_GRBS.Do(x => codes.Add(x.Code));
                BK_PZRZ.Do(x => codes.Add(x.Code));
                BK_CSR.Do(x => codes.Add(x.Code));
                BK_WR.Do(x => codes.Add(x.Code));

                return string.Join(" ", codes);
            }
        }
    }
}