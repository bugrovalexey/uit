﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using System.Threading.Tasks;

    public class ClaimSetJustificationStory : IStory<ClaimSetJustificationStoryContext, bool>
    {
        private readonly IRepositoryRui _repository;
        private readonly ISessionNHibernateFactory _sessionNHibernateFactory;

        public ClaimSetJustificationStory(
            IRepositoryRui repositoryRui,
            ISessionNHibernateFactory sessionNHibernateFactory)
        {
            _repository = repositoryRui;
            _sessionNHibernateFactory = sessionNHibernateFactory;
        }

        public bool Execute(ClaimSetJustificationStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(ClaimSetJustificationStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка: {context.ClaimId}");
            }

            claim.Justification = context.Justification;

            using (var session = _sessionNHibernateFactory.OpenSession())
            {
                await session.SaveOrUpdateAsync(claim);
                await session.CommitAsync();
            }

            return true;
        }
    }
}
