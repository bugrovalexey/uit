﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Работы и услуги ОУ
    /// </summary>
    public class WorkAndServiceDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Классификационная категория. Id
        /// </summary>
        public int CategoryKindOfSupportId { get; set; }

        /// <summary>
        /// Классификационная категория
        /// </summary>
        public string CategoryKindOfSupportName { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public decimal Summa { get; set; }

        /// <summary>
        /// Сведения по арендуемой инфраструктуре и условиям ее использования
        /// </summary>
        public string InformationLeasedInfrastructure { get; set; }

        /// <summary>
        /// ОКВЭД
        /// </summary>
        public OkvedDto Okved { get; set; }
    }
}
