﻿using System;

namespace GB
{
    /// <summary>
    /// Аттрибут для сборок в которых присутствует маппинг
    /// </summary>
    /// <remarks>
    /// Что бы подключить сборку, в файле AssemblyInfo.cs
    /// нужно добавить строчку [assembly: Uviri.AssemblyMappings()]
    /// </remarks>
    [AttributeUsage(AttributeTargets.Assembly)]
    public sealed class AssemblyMappings : Attribute { }
}