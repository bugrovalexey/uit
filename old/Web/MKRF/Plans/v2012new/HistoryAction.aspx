﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="HistoryAction.aspx.cs" Inherits="GB.MKRF.Web.Plans.v2012new.HistoryAction" %>
<%@ Import Namespace="GB.MKRF.Web.Plans.v2012new" %>
<%@ Register Src="~/Common/GridColumnManager.ascx" TagName="GridColumnManager" TagPrefix="gcm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">

    <script type="text/javascript">
        var currentDate = <%=date.Value.Ticks%>;
        function showActionComparison(d2) {
            if (currentDate && d2 && currentDate == d2)
                return;
            document.location.href = 'HistoryAction.aspx?Plan=<%=CurrentPlan.Id%>&t=' + currentDate + '&t2=' + d2;
        }

          

        var idList = [<%=getComparisonScript()%>];
        var remain = 0;
        var interval_Id;
        var freeSlots = 5;
            
        $(function() {

            if (!idList || idList.length <= 0)
                return;

            freeSlots = 5;
                
            remain = idList.length;

            interval_Id = setInterval( function() {
                    
                if (remain <= 0)
                {
                    clearInterval(interval_Id);
                    return;
                }

                if (freeSlots > 0)
                {
                    var index = idList.length - remain;
                    remain--;
                    freeSlots--;
                    compareCallback.PerformCallback(idList[index]);
                }

            } , 100);
        });

        function compareCallbackComplete(s,e)
        {
            var equ = e.result;
            var className = 'cmp' + e.parameter;
            $('.' + className).each(function() {
                $(this).removeClass( className + ' compareGray');
                if (equ)
                {
                    $(this).addClass('compareYellow');
                }
            });
            freeSlots++;
        }

        function compareCallbackError(s,e)
        {
            e.handled = true; 
            freeSlots++;
        }

        function rowClick(s,e)
        {
            var key = s.GetRowKey(e.visibleIndex);
            if (key)
            {
                e.cancel=true; 
                var url = <%=getJSDatailsLinkTemplate("key")%>
                document.location.href = url;
            }
        }
            
    </script>

    <dxc:ASPxCallback runat="server" ID="compareCallback" ClientInstanceName="compareCallback" OnCallback="compareCallback_Callback">
        <ClientSideEvents CallbackComplete="compareCallbackComplete"
            CallbackError="compareCallbackError" />
    </dxc:ASPxCallback>

    <div class="page" style="position: relative">
        <div class="arrow_back" style="position: absolute; top: -38px;">
            <a href="<%=History.GetUrl(CurrentPlan.Id)%>">Вернуться к карточке плана</a>
        </div>
        <table class="form_edit">
            <tr>
                <td>
                    <table style="margin: 10px; font-size: 1.2em;" cellspacing="3px">
                        <tr>
                            <td style="white-space: nowrap">История мероприятий плана:
                            </td>
                            <td style="font-weight: bold">
                                <%=(CurrentPlan == null) ? null:(CurrentPlan.Department.Name + ", " + CurrentPlan.Year.Year.ToString() + "г.")%>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">от:</td>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="border-collapse: collapse">
                                    <tr>
                                        <td style="font-weight: bold">
                                            <%=date.HasValue ? (date.Value.ToShortDateString() + "&nbsp;" + date.Value.ToShortTimeString() ): "?"%>
                                        </td>
                                        <td style="padding-left: 20px; white-space: nowrap; padding-right: 5px">сравнить с версией от:</td>
                                        <td>
                                            <dxe:ASPxComboBox runat="server" ID="cb" ValueField="Id" TextField="Name" ValueType="System.String">
                                                <ClientSideEvents SelectedIndexChanged="function(s,e){  showActionComparison(s.GetSelectedItem().value.toString() ); }" />
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div class="page">
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
    <div>
        <gcm:GridColumnManager ID="_GridColumnManager" runat="server" GridName="listGrid" />
    </div>
    <dxwgv:ASPxGridView runat="server" ID="listGrid" KeyFieldName="Id" Width="100%"
        ClientInstanceName="listGrid" OnBeforePerformDataSelect="listGrid_BeforePerformDataSelect" OnHtmlDataCellPrepared="listGrid_HtmlDataCellPrepared">
        <Columns>
            <dxwgv:GridViewDataTextColumn Caption="Caption" FieldName="Caption" VisibleIndex="3" Visible="false" ShowInCustomizationForm="false" PropertiesTextEdit-EncodeHtml="true"
                FooterCellStyle-BackColor="#DBEBFF">
                <Settings AllowSort="True" SortMode="Value" />
                <GroupRowTemplate>
                    <%#Eval("Caption").ToString()%>
                </GroupRowTemplate>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Version" VisibleIndex="3" Visible="false" ShowInCustomizationForm="false"
                FooterCellStyle-BackColor="#DBEBFF">
                <Settings AllowSort="True" SortMode="Value" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Код" FieldName="Code" VisibleIndex="4" Width="70px"
                FooterCellStyle-BackColor="#DBEBFF">
                <Settings AllowSort="True" SortMode="Value" />
                <CellStyle HorizontalAlign="Center"></CellStyle>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Name" VisibleIndex="5" Caption="Наименование мероприятия" Width="90%"
                FooterCellStyle-BackColor="#DBEBFF">
                <DataItemTemplate>
                  <%#getDatailsLink(Eval("Name"), Eval("Id"))%>
              </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
            
            <dxwgv:GridViewDataTextColumn Caption="Тип" FieldName="Type" VisibleIndex="6" Visible="false" Width="70px"
                FooterCellStyle-BackColor="#DBEBFF">
                <Settings AllowSort="True" SortMode="Value" />
                <CellStyle HorizontalAlign="Center"></CellStyle>
            </dxwgv:GridViewDataTextColumn>

            <dxwgv:GridViewDataTextColumn Caption="Код строки" FieldName="Num" VisibleIndex="7" Visible="false" Width="70px"
                FooterCellStyle-BackColor="#DBEBFF">
                <Settings AllowSort="True" SortMode="Value" />
                <CellStyle HorizontalAlign="Center"></CellStyle>
            </dxwgv:GridViewDataTextColumn>




            <dxwgv:GridViewBandColumn Caption="Объем планируемых бюджетных ассигнований, тыс. руб" Visible="false"
                VisibleIndex="8" Name="ExpenseVolFB">
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="на очередной финансовый год" FieldName="PlansActivityVolY0"
                        VisibleIndex="9" Width="150px" FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="на первый год планового периода" FieldName="PlansActivityVolY1"
                        VisibleIndex="10" Width="150px" FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="на второй год планового периода" FieldName="PlansActivityVolY2"
                        VisibleIndex="11" Width="150px" FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
            <dxwgv:GridViewBandColumn Caption="Объем фактически израсходованных  бюджетных ассигнований, тыс. руб." VisibleIndex="8" Name="ExpenseFactVolFB" Visible="false">
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="на год, предшествующий отчетному финансовому году" FieldName="PlansActivityFactVolY0"
                        VisibleIndex="7" Width="150px" FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="на отчетный финансовый год" FieldName="PlansActivityFactVolY1"
                        VisibleIndex="8" Width="150px" FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="на текущий финансовый год" FieldName="PlansActivityFactVolY2"
                        VisibleIndex="9" Width="150px" FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="израсходовано, нарастающим итогом" FieldName="SpentPlansActivityFact"
                        VisibleIndex="9" Width="150px" FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
            <dxwgv:GridViewBandColumn Caption="Дополнительная потребность, тыс. руб" VisibleIndex="12" Visible="false"
                Name="ExpenseAddVolFB">
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="на очередной финансовый год" FieldName="PlansActivityAddVolY0"
                        VisibleIndex="13" Width="150px" FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="на первый год планового периода" FieldName="PlansActivityAddVolY1"
                        VisibleIndex="14" Width="150px" FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="на второй год планового периода" FieldName="PlansActivityAddVolY2"
                        VisibleIndex="15" Width="150px" FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
            <dxwgv:GridViewBandColumn Caption="Итого, тыс. руб" VisibleIndex="20" Name="ExpenseItogo">
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="VolY0"
                        FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="VolY1"
                        FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="VolY2"
                        FooterCellStyle-BackColor="#DBEBFF">
                        <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                        <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
            </dxwgv:GridViewBandColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Id" ShowInCustomizationForm="False" Visible="False"
                VisibleIndex="24" FooterCellStyle-BackColor="#DBEBFF">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Num" SortIndex="2" SortOrder="Ascending"
                VisibleIndex="25" ShowInCustomizationForm="False" Visible="False" FooterCellStyle-BackColor="#DBEBFF">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataCheckColumn Caption=" " FieldName="IsPriority" GroupIndex="0"
                Name="IsPriority" ShowInCustomizationForm="False" SortIndex="0" SortOrder="Descending"
                VisibleIndex="26" FooterCellStyle-BackColor="#DBEBFF">
                <PropertiesCheckEdit DisplayTextChecked="Приоритетные мероприятия"
                    DisplayTextUnchecked="Мероприятия по информатизации, не относящиеся к приоритетным">
                </PropertiesCheckEdit>
            </dxwgv:GridViewDataCheckColumn>
            <dxwgv:GridViewDataTextColumn Caption=" " ShowInCustomizationForm="false" Name="ActionDetailLink" VisibleIndex="50" Visible="false">
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <GroupSummary>
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY0"
                ShowInGroupFooterColumn="PlansActivityVolY0" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY1"
                ShowInGroupFooterColumn="PlansActivityVolY1" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY2"
                ShowInGroupFooterColumn="PlansActivityVolY2" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY0"
                ShowInGroupFooterColumn="PlansActivityAddVolY0" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY1"
                ShowInGroupFooterColumn="PlansActivityAddVolY1" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY2"
                ShowInGroupFooterColumn="PlansActivityAddVolY2" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY0"
                ShowInGroupFooterColumn="PlansActivityFactVolY0" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY1"
                ShowInGroupFooterColumn="PlansActivityFactVolY1" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY2"
                ShowInGroupFooterColumn="PlansActivityFactVolY2" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY0" ShowInGroupFooterColumn="VolY0"
                SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY1" ShowInGroupFooterColumn="VolY1"
                SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY2" ShowInGroupFooterColumn="VolY2"
                SummaryType="Sum" />
        </GroupSummary>
        <TotalSummary>
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY0"
                ShowInColumn="PlansActivityVolY0" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY1"
                ShowInColumn="PlansActivityVolY1" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityVolY2"
                ShowInColumn="PlansActivityVolY2" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY0"
                ShowInColumn="PlansActivityAddVolY0" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY1"
                ShowInColumn="PlansActivityAddVolY1" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityAddVolY2"
                ShowInColumn="PlansActivityAddVolY2" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY0"
                ShowInColumn="PlansActivityFactVolY0" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY1"
                ShowInColumn="PlansActivityFactVolY1" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="PlansActivityFactVolY2"
                ShowInColumn="PlansActivityFactVolY2" SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY0" ShowInColumn="VolY0"
                SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY1" ShowInColumn="VolY1"
                SummaryType="Sum" />
            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.0000}" FieldName="VolY2" ShowInColumn="VolY2"
                SummaryType="Sum" />
        </TotalSummary>
        <SettingsBehavior ConfirmDelete="True" AutoExpandAllGroups="True" />
        <Settings ShowFooter="True" GroupFormat="{1} {2}" ShowGroupPanel="False" ShowGroupFooter="VisibleAlways" />
        <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
        <Styles>
            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
            </Header>
        </Styles>
    </dxwgv:ASPxGridView>

<%if(date2.HasValue) { %>
    <br />
    <div>Описание:</div>
    <ul style="width: 140px;">
        <li class="compareYellow" style="padding: 2px; white-space: nowrap">Имеются отличия</li>
        <li class="compareGray" style="padding: 2px; white-space: nowrap">Данные сравниваются</li>
        <li style="background-color: white; white-space: nowrap; padding: 2px; border: 1px solid #BED6F6">Отличий нет</li>
    </ul>
<%} %>

</div>
</asp:Content>