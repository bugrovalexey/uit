﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Activities;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using System.Web.Mvc;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.Controllers
{
    partial class ActivitiesController
    {
        public ActionResult CharacteristicsSingle(int id)
        {
            return View(new CharacteristicsViewModel(_statusRepository, id));
        }

        public ActionResult CharacteristicsSingleEdit(int activityId, int? id)
        {
            return View(new CharacteristicsEditViewModel(_statusRepository, activityId, id));
        }

        [HttpPost]
        public ActionResult CharacteristicsSingleEdit(CharacteristicsArgs data)
        {
            return NotificationHelper.NotificationCreateEdit(delegate()
            {
                AccountingObjectCharacteristics cr = null;

                if (data.CharacteristicsId != null)
                {
                    if (!data.IsRegister)
                        cr = new AccountingObjectCharacteristicsService().Update(data);
                }
                else
                {
                    cr = new AccountingObjectCharacteristicsService().Create(data);
                    data.CharacteristicsId = cr.Id;
                }

                var crv = new AccountingObjectCharacteristicsValueService().Update(data);

                return new TableCharacteristics(cr, crv);
            });
        }

        [HttpPost]
        public ActionResult CharacteristicsDelete(int id)
        {
            return NotificationHelper.NotificationDelete(id, delegate()
            {
                new AccountingObjectCharacteristicsService()
                .Delete(new AccountingObjectCharacteristicsArgs() { Id = id });
            });
        }
    }
}