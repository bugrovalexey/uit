﻿namespace bgTeam.IsRui.Story.Common
{
    using bgTeam.IsRui.Common.Extentions;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Story.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class BuildXLSStory : IStory<BuildXLSStoryContext, DownloadFileDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IFileProviderRui _fileProvider;
        private readonly IBuildXLS _converter;

        public BuildXLSStory(IRepositoryRui repository, IFileProviderRui fileProvider, IBuildXLS converter)
        {
            _repository = repository;
            _fileProvider = fileProvider;
            _converter = converter;
        }

        public DownloadFileDto Execute(BuildXLSStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<DownloadFileDto> ExecuteAsync(BuildXLSStoryContext context)
        {
            //var testData = new List<XlsOrderViewModelExtension>();

            // var testString = new XlsOrderViewModelExtension() { ArticleName = "Name", Code = "1" };

            //testData.Add(testString);

            var data = GetData();

            _converter.ExportToXLS(data);

            return null;
        }

        public List<XlsOrderViewModelExtension> GetData()
        {

            var aoList = _repository.GetAll<AccountingObject>();

            int countId = 0;

            var block = new List<XlsOrderViewModelExtension>();

            var articleList = _repository.GetAll<AoArticle>();

            foreach (AoArticle article in articleList)
            {
                var aoBlock = aoList.Where(q => q.Article == article);

                if (aoBlock != null)
                {
                    block.Add(new XlsOrderViewModelExtension()
                    {
                        Article = article.Name,
                        Quart1 = aoBlock.Sum(x => x.FinancingQuarter1).Value,
                        Quart2 = aoBlock.Sum(x => x.FinancingQuarter2).Value,
                        Quart3 = aoBlock.Sum(x => x.FinancingQuarter3).Value,
                        Quart4 = aoBlock.Sum(x => x.FinancingQuarter4).Value,
                        Summ = aoBlock.Sum(x => x.FinancingQuarter1).Value +
                               aoBlock.Sum(x => x.FinancingQuarter2).Value +
                               aoBlock.Sum(x => x.FinancingQuarter3).Value +
                               aoBlock.Sum(x => x.FinancingQuarter4).Value
                    });

                    foreach (AccountingObject ao in aoBlock)
                    {
                        var position = new XlsOrderViewModelExtension();

                        countId++;

                        position.Id = countId;

                        position.SubDivision = ao.Department.Name;

                        position.Article = ao.Article.Name;

                        position.AccountingObject = ao.Kind.Name;

                        position.Name = ao.FullName;

                        if (ao.FinancingQuarter1 != null)
                        {
                            position.Quart1 = ao.FinancingQuarter1.Value;
                        }

                        if (ao.FinancingQuarter2 != null)
                        {
                            position.Quart2 = ao.FinancingQuarter2.Value;
                        }

                        if (ao.FinancingQuarter3 != null)
                        {
                            position.Quart3 = ao.FinancingQuarter3.Value;
                        }

                        if (ao.FinancingQuarter4 != null)
                        {
                            position.Quart4 = ao.FinancingQuarter4.Value;
                        }

                        block.Add(position);
                    }
                }
            }

            return block;
        }
    }
}
