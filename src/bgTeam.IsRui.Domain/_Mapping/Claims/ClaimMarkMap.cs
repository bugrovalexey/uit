﻿namespace bgTeam.IsRui.Domain._Mapping.Claims
{
    using bgTeam.IsRui.Domain.Entities.Claims;

    internal class ClaimMarkMap : ClassMapHiLo<ClaimMark>
    {
        public ClaimMarkMap()
            : base("Claim_Mark")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Claim_Mark_Name");
            Map(x => x.ExplanationRelationshipWithProjectMark, "Claim_Mark_Explan");
            Map(x => x.BaseValue, "Claim_Mark_Basic_Value");
            Map(x => x.TargetedValue, "Claim_Mark_Goal_Value");
            Map(x => x.DateToAchieveGoalValue, "Claim_Mark_Date");
            Map(x => x.Justification, "Claim_Mark_Justif");
            Map(x => x.SourceIsStateProgram, "Claim_Mark_Source_Is_State_Program").Not.Nullable();
            Map(x => x.SourceName, "Claim_Mark_Source_Name");
            Map(x => x.ExplanationRelationshipWithStateProgramMark, "Claim_Mark_Explanation");
            Map(x => x.AlgorithmOfCalculating, "Claim_Mark_AlgorithmOfCalculating");

            References(x => x.Unit, "MUnit_ID");
            References(x => x.Claim, "Claim_ID");
            References(x => x.StateProgramMark, "State_Programme_Mark_ID");
        }
    }
}
