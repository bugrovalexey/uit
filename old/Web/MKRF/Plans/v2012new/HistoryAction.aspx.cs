﻿using System;
using System.Collections.Generic;
using System.Data;
using GB.Helpers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

namespace GB.MKRF.Web.Plans.v2012new
{
    public partial class HistoryAction : System.Web.UI.Page
    {
        public const string historyActionTicksKey = "t";
        public const string historyActionKey = "Action";

        private bool isNewRow = false;
        PlansActivity currentActivity = null;

        DateTime? dateInternal;
        protected DateTime? date
        {
            get
            {
                if (!dateInternal.HasValue)
                {
                    dateInternal = LongToDate(Request.QueryString[historyActionTicksKey]);

                    if (dateInternal == null)
                        ResponseHelper.DenyAccess(this.Response);
                }

                return dateInternal;
            }

        }

        DateTime? date2Internal;
        protected DateTime? date2
        {
            get
            {
                if (!date2Internal.HasValue)
                {
                    date2Internal = LongToDate(Request.QueryString["t2"]);


                    if (date2Internal == null)
                        date2Internal = DateTime.MinValue;
                }

                return date2Internal.Value != DateTime.MinValue ? date2Internal : null;
            }

        }

        Plan currentPlan = null;
        public Plan CurrentPlan
        {
            get
            {
                if (currentPlan == null)
                {
                    int id = 0;
                    if (int.TryParse(Request.QueryString[PlanCard.plansIdKey], out id))
                    {
                        currentPlan = RepositoryBase<Plan>.Get(id);

                        if (currentPlan != null && !Validation.Validator.Validate(currentPlan.Demand, ObjActionEnum.view))
                            currentPlan = null;
                    }

                    if (currentPlan == null)
                    {
                        ResponseHelper.DenyAccess(this.Response);
                    }
                }
                return currentPlan;
            }
        }

        protected PlansActivity CurrentActivity
        {
            get
            {
                if (currentActivity == null && !isNewRow)
                {
                    try
                    {
                        currentActivity = RepositoryBase<PlansActivity>.Get(listGrid.GetRowValues(listGrid.EditingRowVisibleIndex, "Id"));
                    }
                    catch
                    {
                        return currentActivity;
                    }
                }
                return currentActivity;
            }
            set
            {
                if (value != null)
                    currentActivity = value;
            }
        }

        //protected bool IsRegistryIntegration
        //{
        //    get
        //    {
        //        return (bool)(SettingsProvider.Instance[OptionsEnum.IsRegistryIntegration] ?? false);
        //    }
        //}

        private static DateTime? LongToDate(string longValue)
        {
            DateTime? dt = null;
            long ticks = 0;
            if (long.TryParse(longValue, out ticks))
            {
                dt = new DateTime(ticks);
                if (dt > new DateTime(9999, 1, 1) || dt < new DateTime(1900, 1, 1))
                    dt = null;
            }
            return dt;
        }

        public static string GetUrl(Plan plan, DateTime date)
        {
            return string.Concat(
                UrlHelper.Resolve("~/Plans/v2012new/HistoryAction.aspx"),
                "?",
                PlanCard.plansIdKey,
                "=",
                plan.Id,
                "&",
                historyActionTicksKey,
                "=",
                date.Ticks);
        }

        protected string getDetailsUrl(int id)
        {
            return HistoryActionDetails.GetUrl(id, date.Value);
        }

        protected override void OnPreRender(EventArgs e)
        {
            listGrid.DataBind();
           // listGrid.Columns["Registry"].Visible = IsRegistryIntegration;

            DataTable versions = HistoryRepository.GetVersionsByPlan(CurrentPlan.Id, date.Value);
            versions.Columns.Add("Id");
            versions.Columns.Add("Name");
            if (versions.Rows.Count > 0)
            {
                foreach (DataRow row in versions.Rows)
                {
                    DateTime d = (DateTime)row["DT"];
                    row["Id"] = d.Ticks;
                    row["Name"] = d.ToString();
                }

                DataRow r = versions.NewRow();
                r["Id"] = 0;
                r["Name"] = "Выберите версию";
                versions.Rows.InsertAt(r, 0);
            }
            else
            {
                DataRow r = versions.NewRow();
                r["Id"] = -1;
                r["Name"] = "Не с чем сравнивать";
                versions.Rows.InsertAt(r, 0);
                cb.Enabled = false;
            }

            cb.DataSource = versions;
            cb.DataBind();

            if (date2.HasValue)
            {
                var selectedItem = cb.Items.FindByValue(date2.Value.Ticks.ToString());
                if (selectedItem != null)
                    cb.SelectedIndex = selectedItem.Index;
            }
            else
            {
                cb.SelectedIndex = 0;
            }

            base.OnPreRender(e);
        }

        DataTable preActivityListInternal;
        DataTable preActivityList
        {
            get
            {
                if (preActivityListInternal == null)
                {
                    preActivityListInternal = PlanRepository.GetActivityList2012new(CurrentPlan, UserRepository.GetCurrent().Id);
                }
                return preActivityListInternal;
            }
        }

        private DataTable activityDepList;
        private DataTable ActivityDepList
        {
            get
            {
                if (activityDepList == null)
                {
                    activityDepList = PlanRepository.GetDepActivityList2012new(CurrentPlan);
                }

                return activityDepList;
            }
        }

        //Option linkCard = null;

        DataTable gridDataSource = null;
        IList<PlansActivityCompare> reversedActivity = null;

      

        //protected string GetRegistryLink(object id, string name)
        //{
        //    if (linkCard == null)
        //        linkCard = RepositoryBase<Option>.Get((int)OptionsEnum.RegistryCard);

        //    if ((int)id != 0)
        //        return string.Concat("<a target='__self' href='", linkCard.Value, id, "'>", name, "</a>");
        //    else
        //        return name;

        //}

        protected void listGrid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            if (gridDataSource != null)
            {
                listGrid.DataSource = gridDataSource;
                return;
            }

            if (date2.HasValue) // если есть дата 2 ...
            {
                DataTable dt1 = PlanRepository.GetHistoryNewActivityList(CurrentPlan, date.Value);
                DataTable dt2 = PlanRepository.GetHistoryNewActivityList(CurrentPlan, date2.Value);

                dt1.Columns.Add("Version");
                dt1.Columns.Add("RowId");
                dt1.Columns.Add("Caption");
                dt1.Columns.Add("Changes");

                
                dt2.Columns.Add("Version");
                dt2.Columns.Add("RowId");
                dt2.Columns.Add("Caption");
                dt2.Columns.Add("Changes");

                Dictionary<object, DataRow> rowDict = new Dictionary<object,DataRow>();
                foreach (DataRow row in dt2.Rows)
                {
                    var id = row["Id"];
                    if (!rowDict.ContainsKey(id))
                        rowDict.Add(id, row);
                }

                var currentActivityIdList = new List<int>();
                Dictionary<int, DataRow> reversedRowDictionary = new Dictionary<int,DataRow>();
                foreach (DataRow row in dt1.Rows)
                {
                    int id = (int)row["Id"];
                    if (rowDict.ContainsKey(id))
                    {
                        row["Caption"] = "<div class='r1_cmp2 gridHeader compareWhite'>Текущие мероприятия</div>";
                        row["Changes"] = "0";
                        rowDict.Remove(id);

                        currentActivityIdList.Add(id);
                        if (!reversedRowDictionary.ContainsKey(id))
                        {
                            reversedRowDictionary.Add(id, row);
                        }
                    }
                    else
                    {
                        row["Caption"] = "<div class='r2_new gridHeader compareGreen'>Добавленные мероприятия</div>";
                        row["Changes"] = "+";
                    }
                }

                foreach (object key in rowDict.Keys)
                {
                    rowDict[key]["Caption"] = "<div class='r3_del gridHeader compareRed'>Удалённые мероприятия</div>";
                    rowDict[key]["Changes"] = "-";
                    dt1.Rows.Add(rowDict[key].ItemArray);
                }

                reversedActivity = RepositoryBase<PlansActivityCompare>.GetAll(new GetAllArgs<PlansActivityCompare>() { 
                    Expression = x=>(x.Date1 == date && x.Date2 == date2.Value),
                    IdsSelector = (x=>x.Activity.Id),
                    Ids = currentActivityIdList
                });

                foreach (PlansActivityCompare cmp in reversedActivity)
                {
                    if (reversedRowDictionary.ContainsKey(cmp.Activity.Id))
                    {
                        reversedRowDictionary[cmp.Activity.Id]["Changes"] = cmp.Changes;
                    }
                }

                gridDataSource = dt1;

                // настраиваем таблицу




                //listGrid.Columns["Num"].Visible = false;
                listGrid.Columns["ExpenseVolFB"].Visible = false;
                listGrid.Columns["ExpenseAddVolFB"].Visible = false;
                //listGrid.Columns["ExpenseFactVolFB"].Visible = false;
                listGrid.Columns["ExpenseItogo"].Visible = false;


                GridViewDataTextColumn rowCaptColumn = (GridViewDataTextColumn)listGrid.Columns["Caption"];
                rowCaptColumn.FieldName = "Caption";
                rowCaptColumn.Visible = true;
                rowCaptColumn.GroupIndex = 1;

                GridViewDataTextColumn versionColumn = (GridViewDataTextColumn)listGrid.Columns["Version"];
                versionColumn.FieldName = "Version";

                GridViewDataTextColumn detailLinkColumn = (GridViewDataTextColumn)listGrid.Columns["ActionDetailLink"];
                detailLinkColumn.FieldName = "Changes";
                


                GridViewDataCheckColumn isNotPriorityColumn = (GridViewDataCheckColumn)listGrid.Columns["IsPriority"];
                isNotPriorityColumn.PropertiesCheckEdit.UseDisplayImages = false;
                isNotPriorityColumn.Caption = "Приоритетное";
                isNotPriorityColumn.GroupIndex = -1;
                isNotPriorityColumn.Width = 70;
                isNotPriorityColumn.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
                isNotPriorityColumn.PropertiesCheckEdit.DisplayTextChecked = "Да";
                isNotPriorityColumn.PropertiesCheckEdit.DisplayTextUnchecked = "Нет";

                listGrid.GroupSummary.Clear();
                listGrid.TotalSummary.Clear();
                listGrid.Settings.ShowGroupFooter = GridViewGroupFooterMode.Hidden;
                listGrid.Settings.ShowFooter = false;
                listGrid.SettingsPager.PageSize = 1000;
            }
            else
            {
                gridDataSource = PlanRepository.GetHistoryNewActivityList(CurrentPlan, date.Value);


                if (gridDataSource != null)
                {
                    gridDataSource.Columns.Add("Version");
                    gridDataSource.Columns.Add("RowId");
                    gridDataSource.Columns.Add("Caption");
                    gridDataSource.Columns.Add("Changes");
                }
            }

            listGrid.DataSource = gridDataSource;

        }

        protected string getActionLink(int id)
        {
            return string.Format("<a href='{0}'>Детали</a>", getDetailsUrl(id));
        }

        protected string getDatailsLink(object Text, object ActionId)
        {
            return string.Format("<a href='HistoryActionDetails.aspx?Activity={0}&t={1}{2}'>{3}</a>",
                    ActionId,
                    this.date.Value.Ticks,
                    (date2.HasValue) ? ("&t2=" + date2.Value.Ticks) : null,
                    Text
                );
        }

        protected string getJSDatailsLinkTemplate(string KeyName)
        {
            string url = "'HistoryActionDetails.aspx?Activity=' + " + KeyName + " + '&t=" + this.date.Value.Ticks;
            if (date2.HasValue)
                url += ("&t2=" + date2.Value.Ticks);
            url += "';";

            return url;
        }


        protected void NameMemo_DataBinding(object sender, EventArgs e)
        {
            if (!isNewRow && CurrentActivity == null)
                return;

            ASPxMemo box = (ASPxMemo)sender;

            box.Text = isNewRow ? string.Empty : CurrentActivity.Name;
        }

        protected void IsPriorityCheckBox_DataBinding(object sender, EventArgs e)
        {
            if (!isNewRow && CurrentActivity == null)
                return;

            ASPxCheckBox box = (ASPxCheckBox)sender;

            box.Value = isNewRow ? false : CurrentActivity.IsPriority;
        }

//#warning нет вызова данного метода, удалить при следующем рефакторинге
        //protected void RegistryComboBox_DataBinding(object sender, EventArgs e)
        //{
        //    DataTable dt = null;

        //    try
        //    {
        //        RegistryServiceSoapClient client = new RegistryServiceSoapClient();

        //        dt = client.GetPassportList(CurrentPlan.Department.Code);
        //    }
        //    catch (Exception) { }

        //    ASPxComboBox box = sender as ASPxComboBox;

        //    if (dt == null)
        //    {
        //        dt = new DataTable();

        //        dt.Columns.Add("Id", typeof(int));
        //        dt.Columns.Add("Name", typeof(string));
        //    }

        //    DataRow dr = dt.NewRow();

        //    dr["Id"] = 0;
        //    dr["Name"] = "Не задано";

        //    dt.Rows.InsertAt(dr, 0);
        //    Dictionary<int, string> listStr = new Dictionary<int, string>();
        //    foreach (DataRow item in dt.Rows)
        //    {
        //        listStr.Add(Convert.ToInt32(item["Id"]), item["Name"].ToString());
        //    }
        //    box.DataSource = listStr;

        //    if (CurrentActivity == null || CurrentActivity.RegistryInfo == null)
        //    {
        //        box.Value = 0;
        //        return;
        //    }

        //    box.Value = CurrentActivity.RegistryInfo.OridId;
        //}

        //protected void CreatePassportLabel_DataBinding(object sender, EventArgs e)
        //{
        //    if (IsRegistryIntegration)
        //        (sender as ASPxLabel).Text = string.Concat("<a href='", RepositoryBase<Option>.Get((int)OptionsEnum.RegistryCardNew).Value, "?", LinkHelper.BACK_URL, "=", System.Web.HttpUtility.UrlEncode(Request.Url.AbsoluteUri), "'>Создать паспорт ИС</a>");
        //}

        //KeyValuePair<int, HashSet<string>> currentChanges;

        //protected void ActivityMUGrid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        //{
        //    if (!date2.HasValue)
        //        return;

        //    ASPxGridView grid = (ASPxGridView)sender;

        //    string id = grid.GetRowValues(e.VisibleIndex, HistoryData.rowIdColumnName).ToString();

        //    e.Cell.Font.Bold = (id.IndexOf("__1") >= 0)
        //                        && (grid.GetRowValues(e.VisibleIndex, HistoryData.captionColumnName).ToString().IndexOf("_cmp") >= 0);



        //    if (string.IsNullOrEmpty(e.DataColumn.FieldName))
        //        return;

        //    if (currentChanges.Key != e.VisibleIndex)
        //    {
        //        // актуализируем информацию об изменениях
        //        string[] splitted = id.Split('@');

        //        string changesRaw = (splitted.Length > 1) ? splitted[1] : "";
        //        currentChanges = new KeyValuePair<int, HashSet<string>>(e.VisibleIndex, new HashSet<string>(changesRaw.Split(';')));
        //    }


        //    if (currentChanges.Value != null &&
        //        currentChanges.Value.Contains(e.DataColumn.FieldName) &&
        //        e.DataColumn.FieldName != HistoryData.keyColumnName &&
        //        e.Cell.CssClass.IndexOf("compareYellow") < 0)
        //    {
        //        e.Cell.CssClass += " compareYellow";
        //    }
        //}

        //protected void ActivityMUGrid_BeforePerformDataSelect(object sender, EventArgs e)
        //{
        //    ASPxGridView grid = (ASPxGridView)sender;

        //    DataTable table = HistoryRepository.GetActivityMU(CurrentPlan.Id, date.Value);

        //    if (table != null && this.date2.HasValue)
        //    {
        //        DataTable table2 = HistoryRepository.GetActivityMU(CurrentPlan.Id, date2.Value);
        //        table = HistoryData.mergeTables(table, table2);
        //    }

        //    grid.DataSource = table;

        //    if (date2.HasValue)
        //    {
        //        grid.KeyFieldName = HistoryData.rowIdColumnName;
        //        GridViewDataColumn captionColumn = (GridViewDataColumn)grid.Columns["Caption"];
        //        captionColumn.Visible = true;
        //        captionColumn.GroupIndex = 0; 
        //    }
        //}

        protected void listGrid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (!date2.HasValue)
                return;

            ASPxGridView grid = (ASPxGridView)sender;

            if (e.DataColumn.FieldName == "Code")
            {
                var value = grid.GetRowValues(e.VisibleIndex, HistoryData.changesColumnName).ToString();
                if (value == "0")
                {
                    string id = grid.GetRowValues(e.VisibleIndex, HistoryData.idColumnName).ToString();
                    e.Cell.CssClass += (" compareGray cmp" + id);
                }
                else if (!string.IsNullOrWhiteSpace(value) && value != "-" && value != "+")
                {
                    e.Cell.CssClass += " compareYellow";
                }
            }
        }

        protected void compareCallback_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            HistoryData history = new HistoryData(int.Parse(e.Parameter), date.Value, date2);
            e.Result = history.Compare();
        }

        protected string getComparisonScript()
        {
            if (!date2.HasValue || gridDataSource == null || gridDataSource.Rows.Count <= 0)
                return null;

            var idList = new List<string>();
            foreach (DataRow row in gridDataSource.Rows)
            {
                if (row[HistoryData.changesColumnName].ToString() == "0")
                {
                    idList.Add(row["id"].ToString());
                }
            }

            return (idList.Count > 0) ? string.Join(",", idList) : null;
        }
    }
}