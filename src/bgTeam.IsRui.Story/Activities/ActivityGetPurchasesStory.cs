﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityGetPurchasesStory : IStory<ActivityGetPurchasesStoryContext, IEnumerable<ActivityPurchaseDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapper;

        public ActivityGetPurchasesStory(
            IRepositoryRui repository,
            IMapperBase mapper)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public IEnumerable<ActivityPurchaseDto> Execute(ActivityGetPurchasesStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ActivityPurchaseDto>> ExecuteAsync(ActivityGetPurchasesStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var activity = _repository.Get<PlansActivity>(context.ActivityId);

            if (activity == null)
            {
                throw new IsRuiException("Не найдено мероприятие");
            }

            var purchases = _repository.GetAllEx<ActivityPurchase>(q =>
                q.Where(x => x.Activity.Id == context.ActivityId));

            return purchases.Select(x => _mapper.Map(x, new ActivityPurchaseDto()));
        }
    }
}
