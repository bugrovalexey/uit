﻿using System.Collections;
using System.Collections.Generic;
using GB.Extentions;
using NHibernate.Util;

namespace GB.MKRF.Extensions
{
    public static class CollectionExtensions
    {
        public static List<int> ToListInt(this IEnumerable collection)
        {
            var result = new List<int>();
            int intValue;

            collection.If(coll => coll.Any()).Do(items =>
             {
                 foreach (var item in items)
                 {
                     if (int.TryParse(item.ToString(), out intValue))
                     {
                         result.Add(intValue);
                     }
                 }
             });

            return result;
        }

    }
}
