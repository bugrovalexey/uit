﻿namespace bgTeam.IsRui.Story.Claims
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Claims;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ClaimGetScopeOfWorkStory : IStory<ClaimGetScopeOfWorkStoryContext, IEnumerable<ClaimScopeOfWorkDto>>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapperBase;

        public ClaimGetScopeOfWorkStory(
            IRepositoryRui repositoryRui,
            IMapperBase mapperBase)
        {
            _repository = repositoryRui;
            _mapperBase = mapperBase;
        }

        public IEnumerable<ClaimScopeOfWorkDto> Execute(ClaimGetScopeOfWorkStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<IEnumerable<ClaimScopeOfWorkDto>> ExecuteAsync(ClaimGetScopeOfWorkStoryContext context)
        {
            context.ClaimId.CheckNull(nameof(context.ClaimId));

            var claim = _repository.Get<Claim>(context.ClaimId);

            if (claim == null)
            {
                throw new IsRuiException($"Не найдена заявка: {context.ClaimId}");
            }

            var scope = _repository
                .GetAllEx<ClaimScopeOfWork>(q => q.Where(x => x.Claim.Id == context.ClaimId));

            return scope.Select(x => _mapperBase.Map(x, new ClaimScopeOfWorkDto()));
        }
    }
}
