﻿namespace GB.MKRF.Services.Args
{
    public class StatusListArgs : BaseArgsNew, IServiceArgs
    {
        /// <summary>Текущий статус</summary>
        public int FromId { get; set; }

        /// <summary>Следующий статус</summary>
        public int ToId { get; set; }

        /// <summary>Роль</summary>
        public int RoleId { get; set; }
    }
}