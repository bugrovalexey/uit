﻿
using GB.Data.Directories;

namespace GB.DataAccess.Services.Args
{
    public class WorkAndGoodsArgs : BaseArgsNew, IWorkAndGoodsArgs
    {
        public int? AccountingObjectId { get; set; }

        public int ObjectId { get; set; }
    
        public string Name { get; set; }

        public int? CategoryKindOfSupportId { get; set; }

        public CategoryTypeOfSupportEnum Type { get; set; }

        public decimal Summa { get; set; }
    }
}
