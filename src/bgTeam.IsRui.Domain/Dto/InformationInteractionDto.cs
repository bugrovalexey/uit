﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class InformationInteractionDto
    {
        /// <summary>
        /// Идентификатор объекта учета
        /// </summary>
        public int AccountingObjectId { get; set; }

        /// <summary>
        ///  Идентификатор мероприятия
        /// </summary>
        public int ActivityId { get; set; }

        /// <summary>
        /// Наименование объекта учета
        /// </summary>
        public string AccountingObjectName { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// Идентификаор типа
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Наименование типа
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// СМЭВ
        /// </summary>
        public string SMEV { get; set; }

        /// <summary>
        /// Заголовок
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// URL
        /// </summary>
        public string Url { get; set; }
    }
}
