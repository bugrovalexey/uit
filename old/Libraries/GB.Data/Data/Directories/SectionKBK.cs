﻿using System;

namespace GB.Data.Directories
{
    /// <summary>
    /// БК ПЗ/РЗ
    /// </summary>
    public class SectionKBK : EntityBase, IEntityCodeName, IPeriodDirectories
    {
        /// <summary>Код</summary>
        public virtual string Code { get; set; } // null

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; } // null

        /// <summary> Дата начала действия справочника </summary>
        public virtual DateTime? DateStart { get; set; }

        /// <summary> Дата окончания действия справочника </summary>
        public virtual DateTime? DateEnd { get; set; }

        public virtual string CodeName { get { return EntityHelper.GetCodeName(this); } }
    }
}