﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GB.Data.AccountingObjects;

namespace GB._Mapping.AccountingObjects
{
    class AOGovServiceMap : ClassMapHiLo<AOGovService>
    {
        public AOGovServiceMap()
            : base("OU_Uslugi")
        {
        }

        protected override void InitMap()
        {
            References(x => x.AccountingObject, "OU_ID").Not.Nullable();
            References(x => x.GovService, "Gov_Service_ID");

            //Map(x => x.Name, "OU_Uslugi_Name");
            Map(x => x.PercentOfUseResource, "OU_Uslugi_Percent");

            HasMany(x => x.AoGovServicesNpa).KeyColumns.Add("OU_Uslugi_ID").Cascade.All().Inverse();
        }
    }
}
