﻿namespace bgTeam.IsRui.Domain.Entities.Common
{
    /// <summary>
    /// Вспомогательный класс связи ManyToMany
    /// чтобы сохранение шло через процедуры
    /// </summary>
    public class UserToRole : EntityBase
    {
        public virtual Role Role { get; set; }

        public virtual User User { get; set; }
    }
}