﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GoodsDeleteGoodsStory : IStory<GoodsDeleteGoodsStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;

        public GoodsDeleteGoodsStory(ISessionNHibernateFactory factory)
        {
            _factory = factory;
        }

        public bool Execute(GoodsDeleteGoodsStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(GoodsDeleteGoodsStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            bool result;

            using (var session = _factory.OpenSession())
            {
                    await session.DeleteAsync<ActivityGoods>(context.Id.Value);

                    result = true;
            }

            return result;
        }
    }
}
