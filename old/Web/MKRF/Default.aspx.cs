﻿using System;
using GB.MKRF.Repository;

namespace GB.MKRF.Web
{
    public partial class _Default : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
			if (UserRepository.GetCurrent() != null && UserRepository.GetCurrent().Role != null && !string.IsNullOrEmpty(UserRepository.GetCurrent().Role.WebPage))
				Response.Redirect(UserRepository.GetCurrent().Role.WebPage);
			else
				Response.Redirect(ResolveClientUrl("~/Errors/AccessDenied.htm"));

        	base.OnInit(e);
        }
    }
}