﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects;

namespace bgTeam.IsRui.Domain.Extensions
{
    public static class StageExtension
    {
        public static bool IsEq(this AccountingObjectsStageEnum s1, AccountingObjectsStageEnum s2)
        {
            return s1 == s2;
        }

    }
}
