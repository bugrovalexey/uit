﻿using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraCharts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB.MKRF.Repository;
using GB.MKRF.Entities.Directories;

namespace GB.MKRF.Web.Monitoring.v2012new
{
    public partial class PlanFact : System.Web.UI.Page
    {
        private DataTable _data = null;
        private DataTable Data
        {
            get
            {
                if (_data == null)
                {
                    //_data = MonitoringRepository.GetExpenseStructure(CurrentYear, CurrentDepType);
                    _data = MonitoringRepository.GetPlanFact(DepComboBox.Value);
                }

                return _data;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            GridExport.Grid = Grid;

            //Chart.ClientSideEvents.CustomDrawCrosshair
        }

        protected override void OnPreRender(EventArgs e)
        {
            DepComboBox.DataBind();
            ChartDataBinding();
            Grid.DataBind();

            base.OnPreRender(e);
        }

        protected void DepComboBox_DataBinding(object sender, EventArgs e)
        {
            var data = DepartmentRepository.GetAllWithPlan();

            ASPxComboBox box = sender as ASPxComboBox;

            box.DataSource = data;

            var dep = Request.QueryString["dep"];

            if (string.IsNullOrEmpty(dep))
            {
                box.SelectedIndex = 0;
            }
            else
            {
                box.Value = int.Parse(dep);
            }
        }

        private void ChartDataBinding()
        {
            int i = Chart.Series.Add("Планируемые расходы", ViewType.Bar);
            Chart.Series[i].LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            Chart.Series[i].Label.ResolveOverlappingMode = ResolveOverlappingMode.HideOverlapped;
            Chart.Series[i].CrosshairLabelPattern = "{S} : {V:#,##0.0000}";
            Chart.Series[i].Label.TextPattern = "{V:#,##0.0000}";
            Chart.Series[i].Points.Clear();

            int k = Chart.Series.Add("Фактические расходы", ViewType.Bar);
            Chart.Series[k].LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            Chart.Series[k].Label.ResolveOverlappingMode = ResolveOverlappingMode.HideOverlapped;
            Chart.Series[k].CrosshairLabelPattern = "{S} : {V:#,##0.0000}";
            Chart.Series[k].Label.TextPattern = "{V:#,##0.0000}";
            Chart.Series[k].Points.Clear();

            hf.Clear();

            foreach (DataRow r in Data.Rows)
            {
                var data = new DateTime((short)r["Year"], 1, 1);

                Chart.Series[i].Points.Add(new SeriesPoint(data, r["Plan"]));
                Chart.Series[k].Points.Add(new SeriesPoint(data, r["Fact"]));

                var year = r["Year"];

                hf.Add(year + "Limit", Convert.ToDouble(r["Limit"]).ToString("#,##0.0000"));
                hf.Add(year + "Difference", Convert.ToDouble(r["Difference"]).ToString("#,##0.0000"));
            }
        }

        protected void Grid_DataBinding(object sender, EventArgs e)
        {
            (sender as ASPxGridView).DataSource = Data;
        }
    }
}