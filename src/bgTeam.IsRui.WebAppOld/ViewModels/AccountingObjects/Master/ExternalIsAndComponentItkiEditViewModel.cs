﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    [MetadataType(typeof(ExternalIsAndComponentItkiEditViewModel_Valid))]
    public class ExternalIsAndComponentItkiEditViewModel : ViewModelBase
    {
        public ExternalIsAndComponentItkiEditViewModel(int accountingObjectId, int? id)
        {
            AccountingObjectId = accountingObjectId;

            var entity = id.HasValue
                ? new RepositoryDc<ExternalIsAndComponentsItki>().Get(id)
                : null;

            entity.Do(Fill);
        }

        #region Methods

        protected void Fill(ExternalIsAndComponentsItki entity)
        {
            Id = entity.Id;
            AccountingObjectId = entity.Owner.Id;
            Purpose = entity.Purpose;
            Name = entity.Name;
            Smev = entity.Smev;
            Url = entity.Url;
        }

        #endregion Methods

        #region Props

        public int Id { get; set; }

        public int AccountingObjectId { get; set; }

        [DisplayName(TableExternalIsAndComponentItki.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableExternalIsAndComponentItki.PurposeCaption)]
        public string Purpose { get; set; }

        [DisplayName(TableExternalIsAndComponentItki.SmevCaption)]
        public string Smev { get; set; }

        [DisplayName(TableExternalIsAndComponentItki.UrlCaption)]
        public string Url { get; set; }

        #endregion Props
    }

    public class ExternalIsAndComponentItkiEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Required(ErrorMessage = RequiredText)]
        public string Purpose { get; set; }
    }

}