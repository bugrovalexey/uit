﻿using System;

namespace bgTeam.IsRui.Zakupki360.Dto
{
    public class SearchParametersContracts
    {
        public string Number { get; set; }
        public string OrderNumber { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? Status { get; set; }
        public string Performer { get; set; }
        public int? ResCount { get; set; }
    }
}
