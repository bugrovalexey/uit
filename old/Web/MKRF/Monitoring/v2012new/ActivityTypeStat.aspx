﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Monitoring/Monitoring.master" AutoEventWireup="true"
    CodeBehind="ActivityTypeStat.aspx.cs" Inherits="GB.MKRF.Web.Monitoring.v2012new.ActivityTypeStat" %>

<%@ Register Src="~/Common/GridColumnManager.ascx" TagName="GridColumnManager" TagPrefix="gcm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        function reloadPage() {
            var args = [];
            args.push('Year=' + YearsComboBox.GetValue());
            args.push('dep=' + DepartmentComboBox.GetValue());
            window.open(window.location.pathname + "?" + args.join('&'), '_self');
        }

        function SelectedItem(s, e) {

            hf.Set('Type', e.additionalHitObject.argument);

            $('#grid_block').show();
            Grid.Refresh();
        }
    </script>
    <style>
        .infoLable {
            text-align: left;
            font-family: Tahoma;
            font-size: 14px;
        }
    </style>
    <table width="100%">
        <tr>
            <td align="center">
                <table style="width: 800px">
                    <tr>
                        <td>Департамент:</td>
                        <td>
                            <dxe:ASPxComboBox runat="server" ID="DepartmentComboBox" Width="480px" ValueType="System.Int32"
                                ValueField="ID" TextField="Name" ClientInstanceName="DepartmentComboBox" OnDataBinding="DepartmentComboBox_DataBinding">
                                <ClientSideEvents SelectedIndexChanged="function(s,e) { reloadPage();  }" />
                            </dxe:ASPxComboBox>
                        </td>
                        <td>Год:</td>
                        <td style="width: 100px">
                            <dxe:ASPxComboBox runat="server" ID="YearsComboBox" Width="80px" ValueType="System.Int32"
                                ValueField="Year" TextField="Year" ClientInstanceName="YearsComboBox" OnDataBinding="YearsComboBox_DataBinding">
                                <ClientSideEvents SelectedIndexChanged="function(s,e) { reloadPage();  }" />
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
                <dxchartsui:WebChartControl runat="server" Width="800px" Height="500px"
                    LoadingPanelText="Загрузка…" ID="ChartCategories" 
                    AppearanceName="In A Fog" PaletteName="Palette1" ToolTipEnabled="True" >
                    <backimage imageurl="/Content/Style/Coord/backgrounds/diagram_back.jpg"></backimage>

                    <borderoptions visible="False"></borderoptions>

                    <seriesserializable>
                        <dxcharts:Series Name="Categories" SynchronizePointOptions="False">
                            <ViewSerializable>
                                <dxcharts:PieSeriesView RuntimeExploding="True"></dxcharts:PieSeriesView>
                            </ViewSerializable>
                            <LabelSerializable>
                                <dxcharts:PieSeriesLabel >
                                    <Border Visible="False"></Border>
                                </dxcharts:PieSeriesLabel>
                            </LabelSerializable>
                            <PointOptionsSerializable>
                                <dxcharts:PiePointOptions PointView="Values">
                                    <PercentOptions ValueAsPercent="True"></PercentOptions>
                                    <ValueNumericOptions Format="Percent" Precision="0"></ValueNumericOptions>
                                </dxcharts:PiePointOptions>
                            </PointOptionsSerializable>
                            <LegendPointOptionsSerializable>
                                <dxcharts:PiePointOptions PointView="Argument">
                                    <ValueNumericOptions Format="Percent" ></ValueNumericOptions>
                                </dxcharts:PiePointOptions>
                            </LegendPointOptionsSerializable>
                        </dxcharts:Series>
                    </seriesserializable>

                    <titles>
		                <dxcharts:ChartTitle WordWrap="True" MaximumLinesCount="4" Text="Проектный срез" Font="Tahoma, 14pt"/>
	                </titles>
                    <palettewrappers>
                        <dxchartsui:PaletteWrapper Name="Palette1" ScaleMode="Repeat">
                            <Palette>
                                <dxcharts:PaletteEntry Color="79, 129, 189" Color2="51, 90, 136"></dxcharts:PaletteEntry>
                                <dxcharts:PaletteEntry Color="192, 80, 77" Color2="139, 52, 49"></dxcharts:PaletteEntry>
                                <dxcharts:PaletteEntry Color="155, 187, 89" Color2="111, 137, 56"></dxcharts:PaletteEntry>
                                <dxcharts:PaletteEntry Color="128, 100, 162" Color2="89, 69, 115"></dxcharts:PaletteEntry>
                                <dxcharts:PaletteEntry Color="75, 172, 198" Color2="46, 124, 145"></dxcharts:PaletteEntry>
                                <dxcharts:PaletteEntry Color="247, 150, 70" Color2="213, 101, 9"></dxcharts:PaletteEntry>
                                <dxcharts:PaletteEntry Color="230, 230, 230" Color2="132, 132, 132"></dxcharts:PaletteEntry>
                                <dxcharts:PaletteEntry Color="180, 95, 4" Color2="223, 58, 1"></dxcharts:PaletteEntry>
                            </Palette>
                        </dxchartsui:PaletteWrapper>
                    </palettewrappers>
                    <DiagramSerializable>
                        <dxcharts:SimpleDiagram></dxcharts:SimpleDiagram>
                    </DiagramSerializable>
                    <ClientSideEvents ObjectSelected="SelectedItem" />
                </dxchartsui:WebChartControl>
            </td>
        </tr>
        <tr>
            <td align="center">
                <dxe:ASPxLabel ID="LimitInfoLabel" runat="server" CssClass="infoLable" Width="800px">
                </dxe:ASPxLabel>
                <dxe:ASPxLabel ID="ExpenseInfoLabel" runat="server" CssClass="infoLable" Width="800px">
                </dxe:ASPxLabel>
                <dxe:ASPxLabel ID="OverdraftInfoLabel" runat="server" CssClass="infoLable" Width="800px">
                </dxe:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td style="padding-top:20px">
                <div id="grid_block" style="display:none">
                    <cc:GridExport runat="server" ID="GridExport" FileName="Проектный_срез" UserStyle="float:left" />
                    <gcm:GridColumnManager runat="server" ID="GridColumnManager"  GridName="Grid" UserStyle="float:right" />
                    <div style="clear:both"></div>
                    <dxwgv:ASPxGridView runat="server" ID="Grid" KeyFieldName="Id"
                        Width="100%" AutoGenerateColumns="False" ClientInstanceName="Grid"
                        OnDataBinding="Grid_DataBinding" >
                        <Columns>
                            <dxwgv:GridViewDataTextColumn Caption="Наименование мероприятия" FieldName="CodeName">
                                <DataItemTemplate>
                                    <%#  string.Format("<a target='_blank' class='gridLink' href='../../Plans/v2012new/ActivityReadOnly.aspx?ActivityId={0}&from=0'>{1}</a>", Eval("Id"), Eval("CodeName")) %>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Тип мероприятия" FieldName="Type" />
                            <dxwgv:GridViewDataTextColumn Caption="Расходы" FieldName="Y0" >
                                <PropertiesTextEdit DisplayFormatString="#,##0.0000"/>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Ответственный" FieldName="Responsible" Visible="false" />
                            <dxwgv:GridViewDataTextColumn Caption="Источник финансирования" FieldName="Source" Visible="false" />
                        </Columns>
                        <SettingsPager PageSize="50" />
                        <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
                        <%--<Settings ShowFilterRow="True" ShowHeaderFilterBlankItems="False" />
                        
                        <Styles>
                            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            </Header>
                        </Styles>--%>
                    </dxwgv:ASPxGridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
