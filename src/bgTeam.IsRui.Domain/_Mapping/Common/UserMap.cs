﻿using bgTeam.IsRui.Domain.Entities.Common;

namespace bgTeam.IsRui.Domain._Mapping.Common
{
    internal class UserMap : ClassMapHiLo<User>
    {
        public UserMap()
            : base("Users")
        {
            //            Table("w_");

            //            Id(x => x.Id, "Users_ID").GeneratedBy.Custom(typeof(IdGenerator));

            //            SqlInsert(@"raiserror ('Unsupported',10,1)").Check.None();

            //            SqlUpdate(@"exec ZZZ_Prc_Users_Update
            //                              @Users_Is_Active = ?
            //                             ,@Users_Login  = ?
            //                             ,@Users_Password  = ?
            //                             ,@Users_Surname  = ?
            //                             ,@Users_Name  = ?
            //                             ,@Users_Middlename  = ?
            //                             ,@Users_Job = ?
            //                             ,@Users_Work_Phone  = ?
            //                             ,@Users_Mob_Phone  = ?
            //                             ,@Users_Fax  = ?
            //                             ,@Users_Email  = ?
            //                             ,@Users_Work = ?
            //                             ,@Users_Education = ?
            //                             ,@Users_Receive_Mail = ?
            //                             ,@Users_Is_Responsible = ?
            //                             ,@Role_ID = ?
            //                             ,@Govt_Organ_ID = ?
            //                             ,@Users_ID = ?").Check.None();

            //            SqlDelete(@"raiserror ('Unsupported',10,1)").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.IsActive, "Users_Is_Active").Not.Nullable();
            Map(x => x.Login, "Users_Login");
            Map(x => x.Password, "Users_Password");
            Map(x => x.Surname, "Users_Surname");
            Map(x => x.Name, "Users_Name");
            Map(x => x.Middlename, "Users_Middlename");
            Map(x => x.Job, "Users_Job");
            Map(x => x.WorkPhone, "Users_Work_Phone");
            Map(x => x.MobPhone, "Users_Mob_Phone");
            Map(x => x.Fax, "Users_Fax");
            Map(x => x.Email, "Users_Email");
            Map(x => x.Work, "Users_Work");
            Map(x => x.Education, "Users_Education");
            Map(x => x.ReceiveMail, "Users_Receive_Mail");
            Map(x => x.IsResponsible, "Users_Is_Responsible");
            //Map(x => x.Options, "Users_Data");

            Map(x => x.Snils, "Users_Snils");
            Map(x => x.IsESIAUser, "Users_Is_ESIA");
            Map(x => x.TokenInfo, "Users_Token");

            //References<Role>(x => x.Role, "Role_ID");
            References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();

            //HasMany<UserToNotification>(x => x.NotificationList).KeyColumns.Add("Users_ID").Inverse();
            //HasMany<UsersRight>(x => x.RightList).KeyColumns.Add("Users_ID").Inverse();
            //HasMany<UsersMsg>(x => x.MessageList).KeyColumns.Add("Users_ID").Inverse();

            HasManyToMany<Role>(x => x.Roles)
                .Table("Users_To_Role")
                .ParentKeyColumn("Users_ID")
                .ChildKeyColumn("Role_ID")
                //.LazyLoad()
                .ReadOnly();
        }
    }
}