﻿namespace GB.Data.Common
{
    /// <summary>Ответственное лицо</summary>
    public class Responsible : EntityBase, IEntityName
    {
        /// <summary>ФИО</summary>
        public virtual string Name { get; set; }

        /// <summary>Должность</summary>
        public virtual ResponsibleJob Job { get; set; }

        /// <summary>Учреждение</summary>
        public virtual Department Department { get; set; }

        /// <summary>Телефон</summary>
        public virtual string Phone { get; set; }

        /// <summary>Email </summary>
        public virtual string Email { get; set; }

        public virtual bool IsActive { get; set; }

        /// <summary>Не мапиться, служит для описания - ответственен за </summary>
        public virtual string Description { get; set; }
    }

    public class ResponsibleJob : EntityBase
    {
        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }
    }

    public class ResponsibleDescription
    {
        public string Name { get; set; }
        public string Job { get; set; }
        public string Department { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        
        public ResponsibleDescription(Responsible responsible, string description)
        {
            Name = responsible.Name;
            Job = responsible.Job.Name;
            Department = responsible.Department.Name;
            Phone = responsible.Phone;
            Email = responsible.Email;
            Description = description;
        }
        

    }
}