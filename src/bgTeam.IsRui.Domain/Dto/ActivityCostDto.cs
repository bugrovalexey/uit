﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ActivityCostDto
    {
        public int? ActivityId { get; set; }

        public int BudgetItemId { get; set; }

        public string BudgetItemName { get; set; }

        public decimal? PlanQuarter1 { get; set; }

        public decimal? PlanQuarter2 { get; set; }

        public decimal? PlanQuarter3 { get; set; }

        public decimal? PlanQuarter4 { get; set; }

        public decimal? ForecastQuarter1 { get; set; }

        public decimal? ForecastQuarter2 { get; set; }

        public decimal? ForecastQuarter3 { get; set; }

        public decimal? ForecastQuarter4 { get; set; }
    }
}
