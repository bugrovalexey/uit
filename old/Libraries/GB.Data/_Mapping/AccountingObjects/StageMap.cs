﻿using GB.Data.AccountingObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB._Mapping.AccountingObjects
{
    class StageMap : ClassMapHiLo<Stage>
    {
        public StageMap()
            : base("OU_Stage") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "OU_Stage_Name").Not.Nullable();
        }
    }
}
