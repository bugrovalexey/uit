﻿namespace bgTeam.IsRui.WebApp.Admin.Models
{
    using bgTeam.IsRui.WebApp.Admin.Domain;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using System.Collections.Generic;
    using System.Linq;

    public class EditorViewModel
    {
        public int? Dictionary { get; set; }
        public SelectList DictionaryList { get; protected set; }

        public EditorViewModel(IEnumerable<MetaDictionary> dict, int? defValue = null)
        {
            Dictionary = defValue;
            DictionaryList = new SelectList(dict, "Id", "Name");

            if (!Dictionary.HasValue && dict.Any())
            {
                Dictionary = dict.First().Id;
            }
        }
    }
}