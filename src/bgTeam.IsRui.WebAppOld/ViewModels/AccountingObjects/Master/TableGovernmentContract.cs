﻿using System.ComponentModel;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.Extensions;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class TableGovernmentContract : NotificationBaseViewModel
    {
        public const string contract_url = "http://www.zakupki.gov.ru/epz/contract/contractCard/common-info.html?reestrNumber=";
        
        //TODO нужно выяснить формирование url
        public const string zakupka_url = "http://www.zakupki.gov.ru/epz/order/notice/view/common-info.html?regNumber=";

        public const string NumberCaption = "Реестровый номер";
        public const string DateCaption = "Дата заключения";
        public const string StatusCaption = "Статус госконтракта";
        public const string ExecuterCaption = "Исполнитель";
        public const string PriceCaption = "Стоимость";
        public const string RdResultsCaption = "Причины отклонения от ожидаемых результатов";
        public const string OrderCaption = "Закупка";
        public const string StageCaption = "Стадия";

        public int Id { get; set; }

        [DisplayName(NumberCaption)]
        public object Number { get; set; }

        public string NumberStr { get; set; }

        [DisplayName(DateCaption)]
        public string Date { get; set; }

        [DisplayName(StatusCaption)]
        public string Status { get; set; }

        [DisplayName(ExecuterCaption)]
        public string Executer { get; set; }

        [DisplayName(PriceCaption)]
        public decimal Price { get; set; }

        [DisplayName(RdResultsCaption)]
        public string RcDeviation { get; set; }

        [DisplayName(OrderCaption)]
        public string Order { get; set; }

        [DisplayName(StageCaption)]
        public string Stage { get; set; }

        public string DeleteLink
        {
            get { return LinkBuilder.AjaxDeleteLink("GovernmentContractDelete", "AccountingObjects", new { id = Id },
                "GridGovernmentContract", "Вместе с госконтрактом будут удалены все связанные акты сдачи-приемки. Продолжить?"); }
        }


        public TableGovernmentContract(bgTeam.IsRui.Domain.Entities.AccountingObjects.GovContract c)
        {
            Id = c.Id;
            Number = LinkBuilder.Link(c.Number, string.Concat(contract_url, c.Number), inNewTab: true);
            NumberStr = c.Number;
            Date = c.Date.HasValue ? c.Date.Value.ToString("dd.MM.yyyy") : string.Empty;
            Status = c.Status.GetDescription();
            Executer = c.Executer;
            Price = c.Price;
            RcDeviation = c.ReasonsCostDeviation;
            Order = c.Order;
            Stage = c.Stage.GetDescription();
        }
    }
}