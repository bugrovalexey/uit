﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/AdminMaster.master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="GB.MKRF.Web.Administrator.Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">

    <script type="text/javascript">
        function hfUsersSave(e) {
            hf.Clear();
            hf.Set('Roles', Role_ListBox.GetSelectedValues());
            hf.Set('Department', Department_ComboBox.GetValue());

            hf.Set('Login', Login_TextBox.GetValue());
            hf.Set('MiddleName', MiddleName_TextBox.GetValue());
            hf.Set('FirstName', FirstName_TextBox.GetValue());
            hf.Set('SurName', SurName_TextBox.GetValue());
            hf.Set('Email', Email_TextBox.GetValue());
            hf.Set('Password', PasswordTextBox.GetValue());
            hf.Set('Active', IsActive_CheckBox.GetChecked());
            hf.Set('WorkPhone', WorkPhone_TextBox.GetValue());
            hf.Set('MobPhone', MobPhone_TextBox.GetValue());
        }

        function validatePwd(s, e) {
            e.isValid = true;
        }

        function GeneratePwdComplete(pwdText) {
            SetGeneratedPassword(pwdText);
            PasswordTextBox.SetText(pwdText);
            ConfirmPasswordTextBox.SetText(pwdText);
        }
        function SetGeneratedPassword(pwdText) {
            Pass_TextBox.SetText(pwdText);
            Pass_TextBox.SetIsValid(pwdText != null && pwdText);
        }
        function validateComboBox(s, e) {
            var cbValue = s.GetValue();
            e.isValid = (cbValue != undefined || cbValue);
        }

        function validateListBox(s, e) {
            var lbValue = s.GetSelectedValues();
            e.isValid = (lbValue != undefined && lbValue.length > 0);
        }


    </script>

    <style>
        .dxeButtonEditSys {
            margin-left: -3px;
        }
    </style>

    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />

    <dxe:ASPxButton ID="AddUsersButton" runat="server" ClientInstanceName="AddUsersButton"
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {UsersGrid.AddNewRow();}" />
    </dxe:ASPxButton>

    <dxwgv:ASPxGridView runat="server" ID="UsersGrid" Width="100%" ClientInstanceName="UsersGrid"
        KeyFieldName="Id"
        OnInitNewRow="UsersGrid_InitNewRow"
        OnBeforePerformDataSelect="UsersGrid_BeforePerformDataSelect"
        OnRowInserting="UsersGrid_RowInserting"
        OnRowUpdating="UsersGrid_RowUpdating"
        OnStartRowEditing="UsersGrid_StartRowEditing"
        OnRowDeleting="UsersGrid_RowDeleting">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False"
                AllowDragDrop="False">
                <EditButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Login" Caption="Логин">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Roles" Caption="Роли">
                <Settings AutoFilterCondition="Contains" AllowHeaderFilter="True" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="ФИО">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Department" Caption="Департамент">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="WorkPhone" Caption="Рабочий телефон">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="MobPhone" Caption="Мобильный телефон">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataCheckColumn FieldName="IsEsia" Caption="ЕСИА" Width="150px">
                <PropertiesCheckEdit DisplayTextChecked="Использует ЕСИА" DisplayTextUnchecked="Не использует ЕСИА" />
            </dxwgv:GridViewDataCheckColumn>
            <dxwgv:GridViewDataCheckColumn FieldName="IsActive" Caption="Активный" Width="100px">
                <PropertiesCheckEdit DisplayTextChecked="Активный" DisplayTextUnchecked="Заблокированный" />
            </dxwgv:GridViewDataCheckColumn>
        </Columns>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
        <SettingsEditing Mode="EditForm" />
        <SettingsPager PageSize="18" />
        <Settings ShowFilterRow="True" />

        <Styles>
            <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
        </Styles>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Активный
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxCheckBox ID="IsActive_CheckBox" ClientInstanceName="IsActive_CheckBox" runat="server" OnDataBinding="IsActive_CheckBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Логин:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="Login_TextBox" ClientInstanceName="Login_TextBox" Width="700px"
                                OnDataBinding="TextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Пароль:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="Pass_TextBox" runat="server" ClientInstanceName="Pass_TextBox"
                                Width="700px" OnDataBinding="TextBox_DataBinding" ReadOnly="True" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>

                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">&nbsp;
                        </td>
                        <td class="form_edit_input">
                            <dxpc:ASPxPopupControl runat="server" ID="PasswordPopup" ClientInstanceName="PasswordPopup"
                                HeaderText="Установка пароля" Width="300px">
                                <ClientSideEvents PopUp="function(s,e) {
                                                        PasswordTextBox.SetText(null); 
                                                        ConfirmPasswordTextBox.SetText(null);
                                                        } " />
                                <ContentCollection>
                                    <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server"
                                        SupportsDisabledAttribute="True">
                                        <table>
                                            <tr>
                                                <td style="width: 150px">Пароль
                                                </td>
                                                <td>
                                                    <dxe:ASPxTextBox runat="server" ID="PasswordTextBox" Width="120px"
                                                        ClientInstanceName="PasswordTextBox">
                                                        <ValidationSettings ValidationGroup="pwd" ErrorDisplayMode="ImageWithTooltip">
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Подтверждение пароля
                                                </td>
                                                <td>
                                                    <dxe:ASPxTextBox runat="server" ID="ConfirmPasswordTextBox" Width="120px"
                                                        ClientInstanceName="ConfirmPasswordTextBox">
                                                        <ClientSideEvents Validation="function(s,e){ validatePwd(s,e); }" />
                                                        <ValidationSettings ValidationGroup="pwd" ErrorDisplayMode="ImageWithTooltip">
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <dxe:ASPxButton runat="server" ID="PasswordOkButton" Text="OK" Width="80px" ValidationGroup="pwd"
                                                                    AutoPostBack="false">
                                                                    <ClientSideEvents Click="function(s,e){ PasswordPopup.Hide(); SetGeneratedPassword(PasswordTextBox.GetText());   }" />
                                                                </dxe:ASPxButton>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxButton runat="server" ID="PasswordCancelButton" Text="Отмена" Width="80px"
                                                                    CausesValidation="false" AutoPostBack="false">
                                                                    <ClientSideEvents Click="function(s,e){PasswordPopup.Hide();}" />
                                                                </dxe:ASPxButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxpc:PopupControlContentControl>
                                </ContentCollection>
                            </dxpc:ASPxPopupControl>
                            <table cellpadding="10" cellspacing="10">
                                <tr>
                                    <td>
                                        <dxe:ASPxButton runat="server" ID="SetPasswordButton" Text="Задать пароль" AutoPostBack="false"
                                            CausesValidation="false">
                                            <ClientSideEvents Click="function(s,e) {PasswordPopup.ShowAtElement(s.GetMainElement());}" />
                                        </dxe:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Роли:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxListBox ID="Role_ListBox" runat="server" ClientInstanceName="Role_ListBox" SelectionMode="CheckColumn" Width="700px"
                                ValueField="Id" ValueType-="System.Int32" TextField="Name" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>"
                                OnDataBinding="Role_ListBox_OnDataBinding" OnDataBound="Role_ListBox_OnDataBound" AutoPostBack="False">
                                <ValidationSettings  ErrorDisplayMode="Text" >
                                    <RequiredField IsRequired="true" ErrorText="Обязательное поле"/>
                                </ValidationSettings>
                            </dxe:ASPxListBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Фамилия:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="SurName_TextBox" runat="server" ClientInstanceName="SurName_TextBox" Width="700px"
                                OnDataBinding="TextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Имя:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="FirstName_TextBox" ClientInstanceName="FirstName_TextBox" Width="700px"
                                OnDataBinding="TextBox_DataBinding" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="form_edit_desc">Отчество:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="MiddleName_TextBox" ClientInstanceName="MiddleName_TextBox" Width="700px"
                                OnDataBinding="TextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">
                            <span class="required"></span>
                            Департамент:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox runat="server" ID="Department_ComboBox" ClientInstanceName="Department_ComboBox" ValueType="System.Int32" ValueField="Id"
                                Width="700px" TextField="Name" OnDataBinding="comboBox_DataBinding"
                                ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ClientSideEvents Validation="function(s,e){validateComboBox(s,e);}" />
                                <ValidationSettings ErrorText="Обязательное поле" ErrorDisplayMode="None" ValidationGroup="EditForm">
                                    <RequiredField IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="form_edit_desc">Email:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="Email_TextBox" ClientInstanceName="Email_TextBox" Width="700px"
                                OnDataBinding="TextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Рабочий телефон:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="WorkPhone_TextBox" ClientInstanceName="WorkPhone_TextBox" Width="700px"
                                OnDataBinding="TextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Сотовый телефон:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="MobPhone_TextBox" ClientInstanceName="MobPhone_TextBox" Width="700px"
                                OnDataBinding="TextBox_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="ReasonGridTemplateReplacement1" runat="server" ReplacementType="EditFormUpdateButton" />
                            </span>
                            <span class="cancel">
                                <dxwgv:ASPxGridViewTemplateReplacement ID="ReasonGridTemplateReplacement2" runat="server" ReplacementType="EditFormCancelButton" />
                            </span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') hfUsersSave(e); }" />
    </dxwgv:ASPxGridView>

</asp:Content>
