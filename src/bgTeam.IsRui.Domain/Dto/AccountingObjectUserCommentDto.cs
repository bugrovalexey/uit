﻿namespace bgTeam.IsRui.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AccountingObjectUserCommentDto
    {
        public int Id { get; set; }

        public string Comment { get; set; }

        public DateTime CreateDate { get; set; }

        public int StatusId { get; set; }

        public string StatusName { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public int AoId { get; set; }

        public IList<FilesDto> Documents { get; set; }
    }
}
