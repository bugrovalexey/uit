﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using GB.MKRF.Entities;

namespace GB.MKRF.Web.Helpers
{
    public class CoordEntityHelper
    {
        /// <summary>
        /// Преобразует Enum в список [Id, Description]
        /// Если Description не задан, используется имя элемента перечисления
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        //public static IList<NamedEntity> EnumToDatasource<T>()
        //{
        //    if (!(default(T) is Enum))
        //        return null;

        //    List<NamedEntity> dataSource = new List<NamedEntity>();

        //    Type type = typeof(T);
        //    foreach (object item in Enum.GetValues(type))
        //    {
        //        dataSource.Add(new NamedEntity(Convert.ToInt32(item), GetDescription(item)));
        //    }

        //    return dataSource;
        //}

        /// <summary>
        /// Извлекает из объекта атрибут Description
        /// </summary>
        /// <param name="Item"></param>
        /// <returns></returns>
        public static string GetDescription(Object Item)
        {
            FieldInfo fi = Item.GetType().GetField(Item.ToString());
            DescriptionAttribute[] attributes = new DescriptionAttribute[] { };
            if (fi != null)
                attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description : Item.ToString();
        }
    }
}