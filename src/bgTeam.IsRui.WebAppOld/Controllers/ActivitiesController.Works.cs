﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Activities;
using bgTeam.IsRui.Domain.Entities.Plans;
using System.Web.Mvc;
using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.Controllers
{
    partial class ActivitiesController
    {
        private IService<PlansWork, PlansWorkArgs> worksController;

        private IService<PlansWork, PlansWorkArgs> WorksController
        {
            get { return worksController ?? (worksController = new PlansWorkService()); }
        }

        public ActionResult WorksSingle(int Id)
        {
            return View(new WorksViewModel(_statusRepository, Id));
        }

        public ActionResult WorkEdit(int? id, int ownerId)
        {
            return View(new WorkEditViewModel(_statusRepository, ownerId, id));
        }

        [HttpPost]
        public ActionResult WorkEdit(PlansWorkArgs args)
        {
            return NotificationHelper.NotificationCreateEdit<TableWork>(() =>
            {
                bool isNew = args.Id == 0;
                var result = isNew
                    ? WorksController.Create(args)
                    : WorksController.Update(args);

                return new TableWork(result)
                {
                    success = true,
                    message = isNew ? "Запись успешно создана" : NotificationHelper.AddTextSuccess
                };
            });
        }

        [HttpPost]
        public ActionResult WorkDelete(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => WorksController.Delete(new PlansWorkArgs { Id = id }));
        }
    }
}