﻿using GB.Extentions;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.AccountingObjects;
using GB.MKRF.Entities.Directories;
using GB.MKRF.Entities.Plans;
using GB.MKRF.Extensions;
using GB.MKRF.Repository;
using GB.Repository;
using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using GB.Controls;
using GB.Controls.Extensions;

namespace GB.MKRF.Web.Activities
{
    public partial class CharacteristicsOU : System.Web.UI.Page
    {
        public PlansActivity CurrentActivity
        {
            get
            {
                return (Master as EditCard).CurrentActivity;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (CurrentActivity.AccountingObject == null)
                Response.Redirect(CommonData.GetUrl(CurrentActivity.Id, new Dictionary<string, string> { { "alr", "Заполните Объект учета" } }));

            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            Grid.DataBind();
            //PopupControl.DataBind();

            base.OnPreRender(e);
        }

        #region Grid

        private AccountingObjectCharacteristicsValueController controller = new AccountingObjectCharacteristicsValueController();

        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            (sender as ASPxGridView).DataSource = AccountingObjectCharacteristicsValueRepository.GetAllForActivity(CurrentActivity);
        }

        protected void Grid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue, CurrentActivity);
        }

        protected void Grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var res = new AccountingObjectCharacteristicsService()
            .Create(new AccountingObjectCharacteristicsArgs()
            {
                AccountingObject = CurrentActivity.AccountingObject,

                Name = hf["ValName"].ToStringOrDefault(),
                TypeId = hf["ValType"].ToIntNull(),
                UnitId = hf["ValUnit"].ToIntNull(),
            });

            controller.Update(new AccountingObjectCharacteristicsValueArgs()
            {
                ActivityId = CurrentActivity.Id,
                CharacteristicsId = res.Id,
                Norm = ControllerBase.ToIntNull(hf.Get("ValNorm")),
                Max = ControllerBase.ToIntNull(hf.Get("ValMax")),
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void Grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            if (!Convert.ToBoolean(Grid.GetRowValuesByKeyValue(e.Keys[0], "IsRegister")))
            {
                var res = new AccountingObjectCharacteristicsService()
                .Update(new AccountingObjectCharacteristicsArgs()
                {
                    Id = (int)e.Keys[0],
                    Name = hf["ValName"].ToStringOrDefault(),
                    TypeId = hf["ValType"].ToIntNull(),
                    UnitId = hf["ValUnit"].ToIntNull(),
                });
            }

            controller.Update(new AccountingObjectCharacteristicsValueArgs()
            {
                ActivityId = CurrentActivity.Id,
                CharacteristicsId = (int)e.Keys[0],
                Norm = ControllerBase.ToIntNull(hf.Get("ValNorm")),
                Max = ControllerBase.ToIntNull(hf.Get("ValMax")),
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void Grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            new AccountingObjectCharacteristicsService()
                .Delete(new AccountingObjectCharacteristicsArgs() { Id = (int)e.Keys[0] });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void NameTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(controller.CurrentCharacteristics.With(x => x.Name))
                .ClientEnabled = !controller.CurrentCharacteristics.Return(x => x.IsRegister);


        }

        protected void TypeComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(new RepositoryBaseNew<AccountingObjectCharacteristicsType>().GetAll())
                .SetValueComboBoxEnt(controller.CurrentCharacteristics.With(x => x.Type))
                .ClientEnabled = !controller.CurrentCharacteristics.Return(x => x.IsRegister);
        }

        protected void UnitComboBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetDataComboBox(new RepositoryBaseNew<Units>().GetAll())
                .SetValueComboBoxEnt(controller.CurrentCharacteristics.With(x => x.Unit))
                .ClientEnabled = !controller.CurrentCharacteristics.Return(x => x.IsRegister);
        }

        protected void NormTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(controller.CurrentEdit.With(x => x.Norm));
        }

        protected void MaxTextBox_DataBinding(object sender, EventArgs e)
        {
            sender.SetValueTextBox(controller.CurrentEdit.With(x => x.Max));
        }

        private static System.Drawing.Color ColorRowDepartment = System.Drawing.Color.FromArgb(249, 240, 228);

        protected void Grid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                var flag = (bool)Grid.GetRowValues(e.VisibleIndex, "IsRegister");

                if (!flag)
                    e.Row.BackColor = ColorRowDepartment;
            }
        }

        protected void Grid_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;

            var flag = (bool)Grid.GetRowValues(e.VisibleIndex, "IsRegister");

            if (e.ButtonType == ColumnCommandButtonType.Delete && flag)
            {
                e.Visible = false;
            }
        }

        #endregion Grid

        //protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        //{
        //var res = new AccountingObjectCharacteristicsController()
        //    .Create(new AccountingObjectCharacteristicsArgs()
        //    {
        //        AccountingObject = CurrentActivity.AccountingObject,

        //        Name = hf["CrName"],
        //        TypeId = hf["CrType"],
        //        UnitId = hf["CrUnit"],
        //    });

        //    new AccountingObjectCharacteristicsValueController()
        //        .Update(new AccountingObjectCharacteristicsValueArgs()
        //        {
        //            Activity = CurrentActivity,
        //            Characteristics = res.Id,
        //            Norm = hf.Get("CrNorm"),
        //            Max = hf.Get("CrMax"),
        //        });
        //}
    }
}