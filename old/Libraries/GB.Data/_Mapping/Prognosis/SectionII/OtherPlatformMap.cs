﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Prognosis.SectionII;

namespace Uviri.MKRF.Mapping.Prognosis.SectionII
{
    public class OtherPlatformMap : ClassMapBase<OtherPlatform>
	{
        public OtherPlatformMap()
            : base("MF2_Hardv")
        {
//            Table("w_");

//            Id(x => x.Id, "MF2_Hardv_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_MF2_Hardv_Insert
//                          @MF2_Hardv_CA = ?
//                         ,@MF2_Hardv_TP = ?
//                         ,@MF2_Hardv_Comm = ?
//						 ,@Hardware_Type_ID = ?
//						 ,@MF2_ID = ?
//                         ,@MF2_Hardv_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_MF2_Hardv_Update
//                          @MF2_Hardv_CA = ?
//                         ,@MF2_Hardv_TP = ?
//                         ,@MF2_Hardv_Comm = ?
//						 ,@Hardware_Type_ID = ?
//						 ,@MF2_ID = ?
//                         ,@MF2_Hardv_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_MF2_Hardv_Delete
//						  @MF2_Hardv_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.CountCA, "MF2_Hardv_CA");
			Map(x => x.CountTP, "MF2_Hardv_TP");
            Map(x => x.Comment, "MF2_Hardv_Comm");

            References<HardwareType>(x => x.HardwareType, "Hardware_Type_ID").Not.Nullable();
			References<TechInfo>(x => x.TechInfo, "MF2_ID").Not.Nullable();
        }
    }
}