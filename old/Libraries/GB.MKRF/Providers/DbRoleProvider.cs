﻿using GB.Data.Common;
using GB.MKRF.Repository;
using System;
using System.Linq;
using System.Web.Security;

namespace GB.MKRF.Providers
{
    public class DbRoleProvider : RoleProvider
    {
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            //Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("admin"), null);
            base.Initialize(name, config);
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            var roles = new Repository<Role>().GetAll();

            return roles.Select(x => x.Name).ToArray();
        }

        public override string[] GetRolesForUser(string username)
        {
            var user = UserRepository.GetCurrent();

            if (user != null)
                return user.Roles.Select(x => x.Name).ToArray();

            return null;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}