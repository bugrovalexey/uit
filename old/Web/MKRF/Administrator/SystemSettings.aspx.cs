﻿using System;
using GB.MKRF.Entities.Admin;
using GB.Repository;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

namespace GB.MKRF.Web.Administrator
{
    public partial class SystemSettings : System.Web.UI.Page
    {
        Option currentOption = null;

        void setCurrentOption(int optId)
        {
            if (currentOption!= null && currentOption.Id == optId)
                return;
            
            currentOption = RepositoryBase<Option>.Get(optId);
        }

        protected override void OnPreRender(EventArgs e)
        {
//#if DEBUG
//            TestPanel.Visible = true;
//#endif

            grid.DataBind();
            //statusGrid.DataBind();
            //TransitionPlansStageGrid.DataBind();
            base.OnPreRender(e);
        }

        protected void grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            grid.DataSource = RepositoryBase<Option>.GetAll();
        }

        protected void grid_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.EditForm)
            {
                setCurrentOption((int)e.KeyValue);
                string controlName = null;
                switch (currentOption.Type)
                {
                    case OptionsTypeEnum.String:
                        controlName = "ValueTextBox";
                        break;
                    case OptionsTypeEnum.Int:
                        controlName = "ValueSpin";
                        break;
                    case OptionsTypeEnum.Bool:
                        controlName = "ValueCheckBox";
                        break;
					case OptionsTypeEnum.Date:
						controlName = "ValueDateEdit";
						break;
                    default:
                        throw new ArgumentException(currentOption.Type.ToString());
                }
                ASPxEdit control = (ASPxEdit)grid.FindEditFormTemplateControl(controlName);
                control.Visible = true;
                control.Value = currentOption.Deserialize();
            }
        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {

            Option currentOption = RepositoryBase<Option>.Get((int)e.Keys[0]);

            if (currentOption.Type.ToString() != (string)hf["typ"])
                throw new Exception("currentOption.Type.ToString() != hf['typ']");
            
            string v = (string)hf["val"];

            switch (currentOption.Type)
            {
                case OptionsTypeEnum.String:
                    {
                        currentOption.Serialize(v);
                    }
                    break;
                case OptionsTypeEnum.Int:
                    {
                        currentOption.Serialize(Convert.ToInt32(v));
                    }
                    break;
                case OptionsTypeEnum.Bool:
                    {
                        currentOption.Serialize(v == "true");
                    }
                    break;
				case OptionsTypeEnum.Date:
					{
						currentOption.Serialize(v);
					}
					break;  
                default:
                    throw new ArgumentException(currentOption.Type.ToString());
            }

            RepositoryBase<Option>.SaveOrUpdate(currentOption);
            //Web.Code.SettingsProvider.Instance.Reload();

            grid.CancelEdit();
            e.Cancel = true;
            grid.DataBind();
        }

        protected void grid_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
        {
            if (currentOption != null)
            {
                e.Properties["cpType"] = currentOption.Type.ToString();
                
            }
        }

        //protected void statusGrid_BeforePerformDataSelect(object sender, EventArgs e)
        //{
        //    statusGrid.DataSource = RepositoryBase<Status>.GetAll();
        //}

        //protected void statusGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        //{
        //    var currentStatus = RepositoryBase<Status>.Get((int)e.Keys[0]);
        //    currentStatus.Name = e.NewValues["Name"].ToString();
        //    currentStatus.DaysCount = Convert.ToInt32(e.NewValues["DaysCount"]);

        //    RepositoryBase<Status>.SaveOrUpdate(currentStatus);
        //    Web.Code.SettingsProvider.Instance.Reload();

        //    statusGrid.CancelEdit();
        //    e.Cancel = true;
        //    statusGrid.DataBind();
        //}

        //private TransitionPlansStage currentStage;
        //protected void TransitionPlansStageGrid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        //{
        //    currentStage = RepositoryBase<TransitionPlansStage>.Get(e.EditingKeyValue);
        //}

        //protected void TransitionPlansStageGrid_BeforePerformDataSelect(object sender, EventArgs e)
        //{
        //    TransitionPlansStageGrid.DataSource = RepositoryBase<TransitionPlansStage>.GetAll();
        //}


        //protected void DateEdit_DataBinding(object sender, EventArgs e)
        //{
        //    if (currentStage == null)
        //        return;

        //    ASPxDateEdit box = (ASPxDateEdit)sender;
        //    switch (box.ID)
        //    {
        //        case "ChangeSecondStageDateEdit":
        //            box.Value = currentStage.ChangeSecondStage;
        //            break;
        //        case "ChangeThirdStageDateEdit":
        //            box.Value = currentStage.ChangeThirdStage;
        //            break;
        //    }
        //}

        //protected void TransitionPlansStageGrid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        //{
        //    currentStage = RepositoryBase<TransitionPlansStage>.Get((int)e.Keys[0]);
        //    currentStage.ChangeSecondStage = Convert.ToDateTime(e.NewValues["ChangeSecondStage"].ToString());
        //    currentStage.ChangeThirdStage = Convert.ToDateTime(e.NewValues["ChangeThirdStage"].ToString());
        //    currentStage.FirstStageDate = Convert.ToDateTime(e.NewValues["FirstStageDate"].ToString());

        //    RepositoryBase<TransitionPlansStage>.SaveOrUpdate(currentStage);
        //    TransitionPlansStageGrid.CancelEdit();
        //    e.Cancel = true;
        //    TransitionPlansStageGrid.DataBind();
        //    //TransitionPlansStageGrid.CancelEdit();
        //    //e.Cancel = true;
        //    //TransitionPlansStageGrid.DataBind();
        //}

        //protected void TransitionTestButton_Click(object sender, EventArgs e)
        //{
        //    var tps = TransitionPlansStageRepository.GetAll(i => i.ChangeSecondStage <= DateTime.Now.Date);
        //    foreach (TransitionPlansStage tp in tps)
        //    {
        //        var changes = TransitionPlansStageRepository.TransitionToSecondStage(tp.Year);
        //        foreach (var change in changes)
        //        {
        //            Response.Write(change);    
        //        }
        //    }

        //    tps = TransitionPlansStageRepository.GetAll(i => i.ChangeThirdStage <= DateTime.Now.Date);
        //    foreach (TransitionPlansStage tp in tps)
        //    {
        //        var changes = TransitionPlansStageRepository.TransitionToThirdStage(tp.Year);
        //        foreach (var change in changes)
        //        {
        //            Response.Write(change);
        //        }
        //    }
        //}
    }
}