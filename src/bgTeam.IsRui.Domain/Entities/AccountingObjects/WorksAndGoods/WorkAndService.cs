﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods
{
    /// <summary>
    /// Работы и услуги ОУ
    /// </summary>
    public class WorkAndService : EntityBase
    {
        /// <summary>
        /// Вид обеспечения
        /// </summary>
        public static readonly CategoryTypeOfSupportEnum Type = CategoryTypeOfSupportEnum.WorksAndServices;

        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Классификационная категория
        /// </summary>
        public virtual CategoryKindOfSupport CategoryKindOfSupport { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public virtual decimal Summa { get; set; }

        /// <summary>
        /// Объект учета
        /// </summary>
        public virtual AccountingObject AccountingObject { get; set; }

        /// <summary>
        /// Сведения по арендуемой инфраструктуре и условиям ее использования
        /// </summary>
        public virtual string InformationLeasedInfrastructure { get; set; }

        /// <summary>
        /// ОКВЭД
        /// </summary>
        public virtual OKVED Okved { get; set; }

    }
}