﻿namespace GB.DataAccess.Services.Args
{
    public class UserCardArgs : BaseArgsNew
    {
        public string Name { get; set; }

        public string Middlename { get; set; }

        public string Surname { get; set; }

        public string Job { get; set; }

        public string Email { get; set; }

        public string MobPhone { get; set; }

        public string WorkPhone { get; set; }
    }
}