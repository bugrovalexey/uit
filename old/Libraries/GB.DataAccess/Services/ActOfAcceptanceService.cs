﻿using System.Collections.Generic;
using AutoMapper;
using GB.Data.AccountingObjects;
using GB.Data.Documents;
using GB.DataAccess.Command.AccountingObjects.ActsToAOActivities;
using GB.DataAccess.Services.Args;
using GB.DataAccess.Command.Documents;
using GB.DataAccess.Command.AccountingObjects.WorkAndGood;

namespace GB.DataAccess.Services
{
    public class ActOfAcceptanceService : ServiceBase<ActOfAcceptance>, IService<ActOfAcceptance, ActOfAcceptanceArgs>
    {
        ISessionFactoryGb factory;

        public ActOfAcceptanceService()
        {
            factory = new SessionFactory();
        }

        public IEnumerable<ActOfAcceptance> GetAllByAccountingObject(int accountingObjectId, AccountingObjectsStageEnum stage)
        {
            GovContract contr = null;

            var acts = Repository.GetAllEx(q => q.JoinAlias(x => x.GovContract, () => contr)
                    .Where(x => contr.AccountingObject.Id == accountingObjectId && contr.Stage == stage));

            return acts;
        }

        public ActOfAcceptance Create(ActOfAcceptanceArgs args)
        {
            var entity = new ActOfAcceptance();
            var result = SaveOrUpdate(entity, args);
            
            new UpdateActToAOActivityCommand().Execute(new UpdateActToAOActivityContext()
            {
                ActId = result.Id,
                ActToActivityIds = args.ActToActivityIds
            });            

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = args.Files,
                listDocs = entity.Documents,
                owner = entity,
                type = DocTypeEnum.ActOfAcceptance
            });

            Refresh(result);

            return result;
        }

        public ActOfAcceptance Update(ActOfAcceptanceArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            var result = SaveOrUpdate(entity, args);

            //new DocumentService().Save(args.Files, entity.Documents, entity, DocTypeEnum.ActOfAcceptance);

            new SaveDocumentCommand(factory)
                .Execute(new SaveDocumentContext
            {
                fileList = args.Files,
                listDocs = entity.Documents,
                owner = entity,
                type = DocTypeEnum.ActOfAcceptance
            });

            Refresh(result);
            
            return result;

        }

        private ActOfAcceptance SaveOrUpdate(ActOfAcceptance entity, ActOfAcceptanceArgs args)
        {
            Mapper.Map(args, entity);
            SaveOrUpdate(entity);
            
            return entity;
        }

        public void Delete(ActOfAcceptanceArgs args)
        {            
            new DeleteWorkAndGoodsByActCommand().Execute(new DeleteWorkAndGoodsByActContext() { xId = args.Id });
            Delete(args.Id);
        }
    }
}
