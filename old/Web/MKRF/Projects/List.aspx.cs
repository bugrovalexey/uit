﻿using System;
using System.Collections.Generic;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using GB.MKRF.Controllers;
using GB.MKRF.Entities.Projects;
using GB.MKRF.Repository;
using GB.MKRF.Web.Helpers;

namespace GB.MKRF.Web.Projects
{
    public partial class List : System.Web.UI.Page
    {
        ProjectController controller = new ProjectController();

        protected override void OnPreRender(EventArgs e)
        {
            Grid.DataBind();
            base.OnPreRender(e);
        }

        #region Handlers

        protected void Grid_BeforePerformDataSelect(object sender, EventArgs e)
        {
            IList<Project> projects = ProjectRepository.GetAll();
            (sender as ASPxGridView).DataSource = projects;
        }

        protected void Grid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            controller.SetCurrentEdit(e.EditingKeyValue);
        }

        protected void Grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            controller.Create(new ProjectControllerArgs
            {
                ShortName = (string)hf["ShortName"]
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void Grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            controller.UpdateShortName(new ProjectControllerArgs
            {
                Id = e.Keys[0],
                ShortName = (string)hf["ShortName"]
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void Grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            controller.Delete(new ProjectControllerArgs
            {
                Id = e.Keys[0]
            });

            (sender as ASPxGridView).GridCancelEdit(e);
        }

        protected void ShortNameTextBox_DataBinding(object sender, EventArgs e)
        {
            if (controller.CurrentEdit != null)
            {
                ((ASPxTextBox)sender).Text = controller.CurrentEdit.ShortName;
            }
        }

        #endregion

        protected string GetLinkToCard(object id, object name)
        {
            return string.Format("<a class='gridLink' href='{0}'>{1}</a>", CommonData.GetUrl(id), name);
        }

    }
}