﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807132335)]
    public class Migration_201807132335 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("Plans_Activity_Act")
                .WithColumn("Plans_Activity_Act_ID").AsInt32().PrimaryKey()
                .WithColumn("Plans_Activity_ID").AsInt32().NotNullable()
                .WithColumn("Plans_Activity_FileId").AsString().Nullable()
                .WithColumn("Plans_Activity_Name").AsString().Nullable()
                .WithColumn("Plans_Activity_Number").AsString().Nullable()
                .WithColumn("Plans_Activity_Date_Adoption").AsString().Nullable()
                .WithColumn("Plans_Activity_Cost").AsString().Nullable()
                .WithColumn("Plans_Activity_FileName").AsString().Nullable()
                .WithColumn("Plans_Activity_TimeStamp").AsString().Nullable();

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "Plans_Activity_Act" });
        }
    }
}
