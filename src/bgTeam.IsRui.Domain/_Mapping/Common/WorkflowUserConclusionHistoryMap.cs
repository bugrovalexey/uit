﻿namespace bgTeam.IsRui.Domain._Mapping.Common
{
    using bgTeam.IsRui.Domain.Entities;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using FluentNHibernate.Mapping;

    internal class WorkflowUserConclusionHistoryMap : ClassMapHiLoNew<WorkflowUserConclusionHistory>
    {
        public WorkflowUserConclusionHistoryMap()
            : base("WorkflowUserConclusionHistory")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Id_Ref, "Id_Ref");
            Map(x => x.Text, "Text");
            Map(x => x.TimeStamp, "TimeStamp");

            Map(x => x.WorkflowId, "WorkflowId").Not.Nullable();

            References(x => x.User, "UserId");
            References(x => x.Status, "StatusId");

            HasMany(x => x.Documents).KeyColumns.Add("EntityOwnerId")
                .Where("EntityType = " + (int)EntityType.WorkflowUserConclusionHistory)
                .Where("Type = " + (int)DocTypeEnum.WorkflowUserConclusionHistory)
                .Cascade.Delete().Inverse();
        }
    }
}
