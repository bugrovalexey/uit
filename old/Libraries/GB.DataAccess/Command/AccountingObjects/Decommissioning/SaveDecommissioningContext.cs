﻿using System;

namespace GB.DataAccess.Command.AccountingObjects.Decommissioning
{
    public class SaveDecommissioningContext : ICommandContext
    {
        public int AccountingObjectId { get; set; }

        /// <summary>
        /// Наименование документа
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Дата принятия документа
        /// </summary>
        public DateTime? DateAdoption { get; set; }

        /// <summary>
        /// Введен в эксплуатацию
        /// </summary>
        public DateTime? DecommissioningDate { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public string DecommissioningFiles { get; set; }
    }
}
