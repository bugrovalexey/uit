﻿namespace bgTeam.IsRui.Story.Activities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class SetInformationInteractionStoryContext
    {
        public int? ActivityId { get; set; }

        public int? AoId { get; set; }

        public string ExternalSystemName { get; set; }
    }
}
