﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Selectors/MainSelectors.Master" AutoEventWireup="true" CodeBehind="IKTObject.aspx.cs" Inherits="GB.MKRF.Web.Selectors.IKTObjectSelector" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript">
        function hfIKTObjectSave(e) {
            hfIKTObject.Set('Number', NumberTextBox.GetText());
            hfIKTObject.Set('Name', NameTextBox.GetText());
            hfIKTObject.Set('ShortName', ShortNameTextBox.GetText());
            hfIKTObject.Set('Status', StatusComboBox.GetValue());
            hfIKTObject.Set('CreationDate', CreationDate_DateEdit.GetValue());
        }

        function rowClick(s, e) {
            var id = s.GetRowKey(e.visibleIndex);
            s.GetRowValues(e.visibleIndex, 'Id;Name', function (values) {
                var result = id + ';' + values[1];

                window.parent.PopupClose('IKTObject', result);
            });
        }

        function validateComboBox(s, e) {
            e.isValid = s.GetValue() != 0;
        }
    </script>
    
    <dxe:ASPxButton ID="AddIKTObjectButton" runat="server" ClientInstanceName="AddIKTObjectButton"
        AutoPostBack="false" Text="Добавить" Style="margin-bottom: 10px;">
        <ClientSideEvents Click="function(s,e) {IKTObjectGrid.AddNewRow();}" />
    </dxe:ASPxButton>
     <dxwgv:ASPxGridView ID="IKTObjectGrid" ClientInstanceName="IKTObjectGrid" runat="server" AutoGenerateColumns="False" KeyFieldName="Id" Width="100%" 
        OnBeforePerformDataSelect="IKTObjectGrid_BeforePerformDataSelect" 
        OnRowInserting="IKTObjectGrid_RowInserting"
        OnRowUpdating="IKTObjectGrid_RowUpdating"
        OnStartRowEditing="IKTObjectGrid_StartRowEditing"
        OnHtmlRowCreated="IKTObjectGrid_HtmlRowCreated">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" ShowInCustomizationForm="False"
                AllowDragDrop="False">
                <EditButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>

            <dxwgv:GridViewDataTextColumn FieldName="Number" Caption="Уникальный идентификационный номер объекта учета" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="ShortName" Caption="Краткое наименование" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Name" Caption="Полное наименование" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Status.Name" Caption="Статус" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataDateColumn FieldName="CreationDate" Caption="Дата регистрации в АИС Учета" >
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="True"/>
            </dxwgv:GridViewDataDateColumn>
        </Columns>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Уникальный идентификационный номер объекта учета:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="NumberTextBox" ClientInstanceName="NumberTextBox"
                                OnDataBinding="TextBox_DataBinding" >
                                <%--<ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>--%>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Краткое наименование:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="ShortNameTextBox" ClientInstanceName="ShortNameTextBox"
                                OnDataBinding="TextBox_DataBinding" >
                                <%--<ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>--%>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc required">Полное наименование:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox runat="server" ID="NameTextBox" ClientInstanceName="NameTextBox"
                                OnDataBinding="TextBox_DataBinding"  ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                <ValidationSettings ErrorDisplayMode="Text">
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Статус:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox ID="StatusComboBox" runat="server" EnableCallbackMode="true" ClientInstanceName="StatusComboBox"
                                 ValueType="System.Int32" ValueField="Id" TextField="Name" DropDownStyle="DropDown"
                                OnDataBinding="StatusComboBox_DataBinding" style="">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Дата создания:
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit ID="CreationDate_DateEdit" ClientInstanceName="CreationDate_DateEdit" runat="server" OnDataBinding="CreationDate_DateEdit_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok"><dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" /></span>
                            <span class="cancel"><dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" /></span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') hfIKTObjectSave(e); }" 
            RowClick="rowClick"/>
        <Styles>
            <AlternatingRow Enabled="true" />
            <Header Wrap="True" />
        </Styles> 
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="15" />
    </dxwgv:ASPxGridView>

    <dxhf:ASPxHiddenField runat="server" ID="hfIKTObject" ClientInstanceName="hfIKTObject" />
</asp:Content>

