﻿namespace bgTeam.AdminDB.Web.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using bgTeam.DataAccess.Impl.Dapper;

    /// <summary>
    /// Предоставляет метаданные описывающие столбец справочника
    /// </summary>
    [TableName("BG_Meta_DictionariesColumns")]
    public class MetaColumn
    {
        public int Id { get; set; }

        public int DictionaryId { get; set; }

        public string Name { get; set; }

        public string ColumnName { get; set; }

        public string ColumnType { get; set; }

        public int RefId { get; set; }

        public int RefColumn { get; set; }

        public int Visible { get; protected set; }

        public int OrderBy { get; set; }

        [Ignore]
        public MetaColumnTypeEnum Type
        {
            get
            {
                return MetaColumnHelper.Convertor(ColumnType);
            }
        }
    }

    internal static class MetaColumnHelper
    {
        public static MetaColumnTypeEnum Convertor(string str)
        {
            switch (str)
            {
                case "String":
                    return MetaColumnTypeEnum.TString;

                case "Number":
                    return MetaColumnTypeEnum.TInt;

                case "Reference":
                    return MetaColumnTypeEnum.TRef;

                case "Boolean":
                    return MetaColumnTypeEnum.TBool;

                case "DateTime":
                    return MetaColumnTypeEnum.TDateTime;
            }

            throw new FormatException(str);
        }

        public static MetaColumn GetAndCheckColumnPK(this IEnumerable<MetaColumn> columns, MetaDictionary dictionary)
        {
            var colPK = columns.SingleOrDefault(x => x.Name == AppSettings.PRIMARY_KEY);

            if (colPK == null)
            {
                throw new ArgumentException($"Not find column {AppSettings.PRIMARY_KEY} for {dictionary.TableName}");
            }

            return colPK;
        }
    }
}