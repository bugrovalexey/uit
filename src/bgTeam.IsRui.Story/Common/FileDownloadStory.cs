﻿namespace bgTeam.IsRui.Story.Common
{
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.Common.Helpers;
    using bgTeam.IsRui.Common.Services;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.Documents;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;

    public class FileDownloadStory : IStory<FileDownloadStoryContext, DownloadFileDto>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IFileProviderRui _fileProvider;

        public FileDownloadStory(IRepositoryRui repositoryRui, IFileProviderRui fileProvider)
        {
            _repositoryRui = repositoryRui;
            _fileProvider = fileProvider;
        }

        public DownloadFileDto Execute(FileDownloadStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<DownloadFileDto> ExecuteAsync(FileDownloadStoryContext context)
        {
            if (context.Id <= 0)
            {
                throw new IsRuiException("Идентификатор файла не может быть меньше единицы");
            }

            var document = _repositoryRui.GetExisting<Files>(context.Id);

            if (document == null)
            {
                throw new IsRuiException($"Файл с идентификатором {context.Id} не найден");
            }

            var file = new DownloadFileDto()
            {
                Id = document.Id,
                FileName = document.Name,
                MimeType = MimeTypeHelper.GetMimeTypeFromFileName(document.Name)
            };

            if (string.IsNullOrEmpty(document.Url))
            {
                file.FileData = _fileProvider.OpenFileToRead(context.Id);
            }
            else
            {
                file.Url = document.Url;
            }

            return file;
        }
    }
}
