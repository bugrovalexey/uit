﻿namespace bgTeam.IsRui.Story.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UserChangePasswordStoryContext
    {
        public int? Id { get; set; }

        public string NewPassword { get; set; }
    }
}
