﻿using GB.Extentions;
using GB.DataAccess.Services;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess
{
    public class SessionHibernate : IDisposable
    {
        public static NHibernate.ISession Session
        {
            get { return NHSession.CurrentSession; }
        }

        private static readonly object _lock = new object();


        public ITransaction Transaction { get; set; }

        public SessionHibernate()
        {
        }
        
        public SessionHibernate(System.Data.IsolationLevel isolationLevel)
        {
            Transaction = Session.BeginTransaction(isolationLevel);
        }


        internal void Insert(IEntity entity)
        {
            lock (_lock)
            {
                try
                {
                    //NHibernateStaticMethods.RegUser();

                    Session.Save(entity);
                    Session.Flush();

                    //нужно вызывать перед Commit
                    //NHibernateStaticMethods.UnRegUser();
                }
                catch (Exception exp)
                {
                    //NHibernateStaticMethods.UnRegUser();
                    //Session.Evict(entity);
                    if (Transaction != null)
                        Transaction.Rollback();

                    throw exp.GetInnerException();
                }
            }
        }

        internal void Update(IEntity entity)
        {
            lock (_lock)
            {
                try
                {
                    //NHibernateStaticMethods.RegUser();

                    Session.Update(entity);
                    Session.Flush();

                    //нужно вызывать перед Commit
                    //NHibernateStaticMethods.UnRegUser();
                }
                catch (Exception exp)
                {
                    //NHibernateStaticMethods.UnRegUser();
                    Session.Evict(entity);
                    if (Transaction != null)
                        Transaction.Rollback();

                    throw exp.GetInnerException();
                }
            }
        }

        internal void Update<T>(IList<T> list) where T : IEntity
        {
            lock (_lock)
            {
                try
                {
                    //NHibernateStaticMethods.RegUser();

                    foreach (var item in list)
                    {
                        Session.Update(item);
                        Session.Flush();
                    }
                    

                    //нужно вызывать перед Commit
                    //NHibernateStaticMethods.UnRegUser();
                }
                catch (Exception exp)
                {
                    //NHibernateStaticMethods.UnRegUser();
                    foreach (var item in list)
                    {
                        Session.Evict(item);
                    }
                    if (Transaction != null)
                        Transaction.Rollback();

                    throw exp.GetInnerException();
                }
            }
        }

        internal void SaveOrUpdate(IEntity entity)
        {
            lock (_lock)
            {
                try
                {
                    //NHibernateStaticMethods.RegUser();

                    Session.SaveOrUpdate(entity);
                    Session.Flush();

                    //нужно вызывать перед Commit
                    //NHibernateStaticMethods.UnRegUser();
                }
                catch (Exception exp)
                {
                    //NHibernateStaticMethods.UnRegUser();
                    Session.Evict(entity);
                    if (Transaction != null)
                        Transaction.Rollback();

                    throw exp.GetInnerException();
                }
            }
        }

        internal void Delete<T>(int id) where T : IEntity
        {
            T entity = Session.Get<T>(id);

            if (entity != null)
                Delete(entity);
        }

        internal void Delete(IEntity entity)
        {
            try
            {
                //NHibernateStaticMethods.RegUser();

                Session.Delete(entity);
                Session.Flush();

                //NHibernateStaticMethods.UnRegUser();
            }
            catch (Exception exp)
            {
                //NHibernateStaticMethods.UnRegUser();
                if (Transaction != null)
                    Transaction.Rollback();

                throw exp.GetInnerException();
            }

        }

        internal void Refresh(IEntity entity)
        {
            try
            {
                Session.Refresh(entity);
                Session.Flush();             
            }
            catch (Exception exp)
            {                
                if (Transaction != null)
                    Transaction.Rollback();

                throw exp.GetInnerException();
            }

        }
        
        internal void Commit()
        {
            Transaction.Commit();
        }

        internal void Rollback()
        {
            Transaction.Rollback();
        }


        public void Dispose()
        {
            if (Transaction != null)
                Transaction.Dispose();
        }       
    }
}
