﻿using GB.MKRF.Entities.Directories;
using System;

namespace GB.MKRF.Entities.Admin
{
    /// <summary>Пользовательское сообщение</summary>
    public class UsersMsg : EntityBase
    {
        /// <summary>Статус сообщения</summary>
        public virtual UsersMsgStatus UsersMsgStatus { get; set; }

        /// <summary>Пользователь</summary>
        public virtual User User { get; set; }

        /// <summary>Aдминистратор</summary>
        public virtual User AdminUser { get; set; }//null

        /// <summary>Текст сообщения</summary>
        public virtual string Text { get; set; }//null

        /// <summary>Страница</summary>
        public virtual string Url { get; set; }//null

        /// <summary>Дата сообщения</summary>
        public virtual DateTime Date { get; set; }

        /// <summary>Ответ администратора</summary>
        public virtual string AdminText { get; set; }//null

        /// <summary>Дата ответа администратора</summary>
        public virtual DateTime? AdminDate { get; set; }//null
    }
}