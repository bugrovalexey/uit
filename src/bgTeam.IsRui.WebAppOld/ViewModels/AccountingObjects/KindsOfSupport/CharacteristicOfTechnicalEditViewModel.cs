﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.Domain.Entities.Directories;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using bgTeam.IsRui.MKRF.Logic.Access;
using NHibernate.Criterion;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.KindsOfSupport
{
    [MetadataType(typeof(CharacteristicOfTechnicalEditViewModel_Valid))]
    public class CharacteristicOfTechnicalEditViewModel : ViewModelBase
    {
        public CharacteristicOfTechnicalEditViewModel(int technicalSupportId, int accountingObjectId, int? id)
        {
            TechnicalSupportId = technicalSupportId;
            ReadOnly = ManagerAccess.CanEdit(new RepositoryDc<AccountingObject>().GetExisting(accountingObjectId)) ==
                       false;

            UnitsList = new SelectList(
                new RepositoryDc<Units>().GetAll(),
                "Id", "FullName");

            //TODO : нужно создать репозиторий и вынести это туда
            NamesJson =
                JsonHelper.ToJson(
                    new RepositoryDc<CharacteristicOfTechnicalSupport>().Select<string>(
                        q => q.Select(
                            Projections.Distinct(
                                Projections.ProjectionList()
                                    .Add(Projections.Property<CharacteristicOfTechnicalSupport>(x => x.Name))))));

            var entity = id.HasValue
                ? new RepositoryDc<CharacteristicOfTechnicalSupport>().GetExisting(id)
                : null;
            entity.Do(Fill);
        }

        protected void Fill(CharacteristicOfTechnicalSupport entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            UnitId = entity.Unit.Return(unit => unit.Id, default(int));
            Value = entity.Value;
        }

        public int Id { get; set; }

        public int TechnicalSupportId { get; set; }

        [DisplayName(TableCharacteristicOfTechnicalSupport.NameCaption)]
        public string Name { get; set; }

        [DisplayName(TableCharacteristicOfTechnicalSupport.UnitCaption)]
        public int UnitId { get; set; }

        [DisplayName(TableCharacteristicOfTechnicalSupport.ValueCaption)]
        public double? Value { get; set; }

        public string NamesJson { get; set; }    

        public SelectList UnitsList { get; set; }
    }

    internal class CharacteristicOfTechnicalEditViewModel_Valid : ValidateViewModel
    {
        [Required(ErrorMessage = RequiredText)]
        public string Name { get; set; }

        [Required(ErrorMessage = RequiredText)]
        [Range(1, int.MaxValue, ErrorMessage = NotZeroValueText)]
        public int UnitId { get; set; }
    }
}