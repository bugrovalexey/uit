﻿namespace bgTeam.IsRui.Migrations.Y2018
{
    using FluentMigrator;

    [Migration(201807110058)]
    public class Migration_201807110058 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Alter.Table("Plans_Activity").AddColumn("Plans_Year").AsInt32().Nullable();
        }
    }
}
