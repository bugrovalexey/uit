﻿using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.Directories;
using GB.Data.Documents;
using GB.Data.Extensions;
using GB.MKRF.Helpers;
using GB.MKRF.Repository;
using System;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    /// <summary>
    /// Контроллер объектов учета
    /// </summary>
    public class AccountingObjectService : ServiceBase<AccountingObject>, IService<AccountingObject, AccountingObjectArgs>
    {
        //public IList<AccountingObject> GetList()
        //{
        //    return Repository.GetAll();
        //}

        #region AccountingObjectArgs

        public AccountingObject Create(AccountingObjectArgs args)
        {
            var entity = ConvertDtoToEntity.FillAccountingObjectFromDto(new AccountingObject(), args);

            //Проставляем значения по умолчанию для Not Nullable полей
            entity.Type = GetEntity<IKTComponent>(EntityBase.Default);
            entity.Kind = GetEntity<IKTComponent>(EntityBase.Default);
            entity.Status = new StatusRepository().GetDefault(entity);

            base.SaveOrUpdate(entity);

            return entity;
        }

        public AccountingObject Update(AccountingObjectArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            entity = ConvertDtoToEntity.FillAccountingObjectFromDto(entity, args);
            base.SaveOrUpdate(entity);

            return entity;
        }

        public void Delete(AccountingObjectArgs args)
        {
            base.Delete((int)args.Id);
        }

        #endregion AccountingObjectArgs

        public AccountingObject Update(AccountingObjectCommonDataArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            entity = ConvertDtoToEntity.FillAccountingObjectFromDto(entity, args);

            new DocumentService()
                .Save(Convert.ToString(args.CommissioningFiles), entity.CommissioningDocuments, entity,
                    DocTypeEnum.CommissioningAccountingObject);

            new DocumentService()
                .Save(Convert.ToString(args.DecommissioningFiles), entity.DecommissioningDocuments, entity,
                    DocTypeEnum.DecommissioningAccountingObject);

            base.SaveOrUpdate(entity);
            return entity;
        }

        public bool IsReadOnly(int id)
        {
            return Repository.GetExistingWithAccess(id).CanEdit() == false;
        }

        public AccountingObject Update(AccountingObjectResponsiblesArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            entity = ConvertDtoToEntity.FillAccountingObjectFromDto(entity, args);

            base.SaveOrUpdate(entity);
            return entity;
        }

        #region AccountingObjectControllerArgs

        public AccountingObject Create(IAccountingObjectControllerArgs args)
        {
            var entity = new AccountingObject();
            entity.Status = new StatusRepository().GetDefault(entity);
            return SaveOrUpdate(entity, args);
        }

        public AccountingObject Update(AccountingObjectControllerArgs args)
        {
            var entity = Repository.GetExisting(args.Id);

            return SaveOrUpdate(entity, args);
        }

        public AccountingObject Update(IAccountingObjectControllerArgs args)
        {
            var entity = Repository.Get(args.Id);

            if (entity == null)
                return Create(args);

            return SaveOrUpdate(entity, args);
        }

        private AccountingObject SaveOrUpdate(AccountingObject entity, IAccountingObjectControllerArgs args)
        {
            entity = ConvertDtoToEntity.FillAccountingObjectFromDto(entity, args);
            base.SaveOrUpdate(entity);

            return entity;
        }

        #endregion AccountingObjectControllerArgs
    }
}