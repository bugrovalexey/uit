﻿namespace bgTeam.IsRui.Common.Extentions
{
    public static class NumberExtensions
    {
        public static string ToPercentString(this int? value)
        {
            if (value.HasValue == false)
            {
                return string.Empty;
            }

            return string.Format("{0} %", value.Value);
        }

        public static string ReturnThisOrDefault(this int? value, string @default)
        {
            if (value.HasValue == false)
            {
                return @default;
            }

            return value.ToString();
        }
    }
}
