﻿namespace bgTeam.IsRui.MKRF.Logic.AccountingObjectActions
{
    public class ActionDescription
    {
        public string Name { get; set; }

        public string Action { get; set; }

        public string Controller { get; set; }

        public string Template { get; set; }
    }
}
