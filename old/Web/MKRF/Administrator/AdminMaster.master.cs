﻿using System;
using DevExpress.Web.ASPxTabControl;
using GB.MKRF.Validation;

namespace GB.MKRF.Web.Administrator
{
    public partial class AdminMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            foreach (Tab t in tc.Tabs)
            {
                t.Visible = Validator.UserAccessToPage(t.NavigateUrl);
            }
        }

        public void SetTabActivity(int index)
        {
            tc.ActiveTabIndex = index;
        }

        public void SetTabActivity(string tabName)
        {
            tc.ActiveTab = tc.Tabs.FindByName(tabName);
        }
    }
}