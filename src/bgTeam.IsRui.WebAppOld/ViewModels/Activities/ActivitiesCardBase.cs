﻿using bgTeam.Extensions;
using bgTeam.IsRui.Common;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Plans;
using bgTeam.IsRui.Domain.Entities.Workflow;
using bgTeam.IsRui.Domain.Extensions;
using bgTeam.IsRui.MKRF.Logic.Access;
using bgTeam.IsRui.MKRF.Logic.Status;
using bgTeam.IsRui.MKRF.Logic.Tabs;
using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace bgTeam.IsRui.WebApp.ViewModels.Activities
{
    public class ActivitiesCardBase //: ViewModelBase
    {
        public int Id
        {
            get
            {
                return CurrentActivity.Return(x => x.Id, default(int));
            }
        }

        private readonly IStatusRepository _statusRepository;

        public PlansActivity CurrentActivity { get; private set; }

        public ActivitiesCardBase(IStatusRepository statusRepository, int id, params Expression<Func<PlansActivity, object>>[] fetches)
        {
            _statusRepository = statusRepository;

            this.CurrentActivity = new RepositoryDc<PlansActivity>().Get(q =>
            {
                q = q.Where(x => x.Id == id)
                     .Fetch(x => x.Status).Eager
                     .Fetch(x => x.ExpertiseConclusion).Eager
                     .Fetch(x => x.ExpertiseConclusion.Status).Eager;
                foreach (var f in fetches)
                {
                    q.Fetch(f).Eager.Clone();
                }
            });
            if (this.CurrentActivity == null)
                throw new EntityNotFoundException(id);

            if (ManagerAccess.CanRead(CurrentActivity) == false)
                throw new ObjectAccessException();

            ReadOnly = ManagerAccess.CanEdit(CurrentActivity) == false;

            var handler = Singleton<TabsManager>.Instance.GetHandler<ActivityCardTabs>();

            Tabs = handler.GetTabsList(InitTabs);
        }

        private string InitTabs(string text, string actionName, string controllerName)
        {
            return LinkBuilder.ActionLink(text, actionName, controllerName, new { Id });
        }

        public bool ReadOnly { get; protected set; }

        public IEnumerable<AjaxLinkViewModel> GetNextStatusLinks()
        {
            //var repo = new StatusRepository();

            var handlerActivity = Singleton<WorkflowManager>.Instance.GetHandler<PlansActivity>();
            foreach (StatusList item in _statusRepository.GetNext(CurrentActivity))
            {
                var link = CreateAjaxLink(CurrentActivity, handlerActivity, item);
                yield return link;
            }

            if (StatusEnum.ActOnExpertise.IsEq(CurrentActivity) && CurrentActivity.ExpertiseConclusion != null)
            {
                //если на экспертизе, то добавляем статусы экспертизы
                var handlerExpertise = Singleton<WorkflowManager>.Instance.GetHandler<ActivityExpertiseConclusion>();
                foreach (StatusList item in _statusRepository.GetNext(CurrentActivity.ExpertiseConclusion))
                {
                    var link = CreateAjaxLink(CurrentActivity.ExpertiseConclusion, handlerExpertise, item);
                    yield return link;
                }
            }
        }

        private AjaxLinkViewModel CreateAjaxLink(IWorkflowObject wo, IWorkflowHandler handler, StatusList item)
        {
            var link = new AjaxLinkViewModel
            {
                Text = item.To.ActionName ?? item.To.Name,
                Title = string.Format("Переход в статус '{0}'", item.To.Name),
                Callback = string.Format("ChangeStatus({0}, '{1}', {2})", wo.Id, wo.GetTableType(), item.To.Id),
                Css = item.To.Css,
            };

            if (handler != null)
            {
                string confirm, error;
                if (!handler.ReadyForNewStatus(wo, item.To, out error, out confirm))
                {
                    link.Disabled = true;
                    link.Title = error;
                    link.Callback = string.Format("alert('{0}')", error.Replace("'", "\"").Replace(Environment.NewLine, " "));
                }
                else
                {
                    link.ConfirmMessage = confirm;
                }
            }
            return link;
        }

        #region Layout

        public string Title
        {
            get
            {
                return CurrentActivity.NameSmall;
            }
        }

        public string Status
        {
            get
            {
                if (StatusEnum.ActOnExpertise.IsEq(CurrentActivity) && CurrentActivity.ExpertiseConclusion != null)
                    return string.Format("{0} \u2192 {1}", CurrentActivity.Status.Name, CurrentActivity.ExpertiseConclusion.Status.Name);
                return CurrentActivity.Status.Name;
            }
        }

        public IList<string> Tabs { get; private set; }

        #endregion Layout
    }
}