﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.IsRui.Migrations.Y2018
{
    [Migration(201803031320)]
    public class Migration_201803031315 : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("Workflow")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("StatusId").AsInt32().NotNullable()

                .WithColumn("EntityType").AsInt32().NotNullable()
                .WithColumn("EntityOwnerId").AsInt32().NotNullable()

                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Create.ForeignKey("FK_Workflow_Status")
               .FromTable("Workflow").ForeignColumn("StatusId")
               .ToTable("Status").PrimaryColumn("Status_ID");

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "Workflow" });

            Create.Table("WorkflowUserConclusion")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("WorkflowId").AsInt32().NotNullable()
                .WithColumn("Text").AsString().Nullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("StatusId").AsInt32().NotNullable()

                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Create.ForeignKey("FK_WorkflowUserConclusion_Workflow")
               .FromTable("WorkflowUserConclusion").ForeignColumn("WorkflowId")
               .ToTable("Workflow").PrimaryColumn("Id");

            Create.ForeignKey("FK_WorkflowUserConclusion_User")
               .FromTable("WorkflowUserConclusion").ForeignColumn("UserId")
               .ToTable("Users").PrimaryColumn("Users_ID");

            Create.ForeignKey("FK_WorkflowUserConclusion_Status")
               .FromTable("WorkflowUserConclusion").ForeignColumn("StatusId")
               .ToTable("Status").PrimaryColumn("Status_ID");

            Create.UniqueConstraint("UK_WorkflowUserConclusion").OnTable("WorkflowUserConclusion")
                .Columns("WorkflowId", "UserId", "StatusId");

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "WorkflowUserConclusion" });

            Create.Table("WorkflowUserConclusionHistory")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("WorkflowId").AsInt32().NotNullable()
                .WithColumn("Text").AsString().Nullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("StatusId").AsInt32().NotNullable()

                .WithColumn("CreateDate").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("TimeStamp").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);

            Create.ForeignKey("FK_WorkflowUserConclusionHistory_Workflow")
               .FromTable("WorkflowUserConclusionHistory").ForeignColumn("WorkflowId")
               .ToTable("Workflow").PrimaryColumn("Id");

            Create.ForeignKey("FK_WorkflowUserConclusionHistory_User")
               .FromTable("WorkflowUserConclusionHistory").ForeignColumn("UserId")
               .ToTable("Users").PrimaryColumn("Users_ID");

            Create.ForeignKey("FK_WorkflowUserConclusionHistory_Status")
               .FromTable("WorkflowUserConclusionHistory").ForeignColumn("StatusId")
               .ToTable("Status").PrimaryColumn("Status_ID");

            Insert.IntoTable("NH_HiLo").Row(new { NextHi = 0, TableKey = "WorkflowUserConclusionHistory" });

            Create.Column("WorkflowId").OnTable("Plans_Activity").AsInt32().Nullable();

            Create.ForeignKey("FK_Plans_Activity_Workflow")
               .FromTable("Plans_Activity").ForeignColumn("WorkflowId")
               .ToTable("Workflow").PrimaryColumn("Id");
        }
    }
}
