﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Web;

namespace GB.Helpers
{
    /// <summary>
    /// Включает различные методы для работы с файлами
    ///
    /// Рекомендации по работе с файлами:
    ///
    /// 1. Загрузить файл во временную папку - SaveInTemp. Метод сохранит файл и вернёт его GUID
    /// 2. Сохранить информацию о файле в БД (название, дату и всё такое) и получить Id записи (таблицы Document)
    /// 3. Используя GUID и Id Переместить файл из Temp в Files методом MoveToGeneric(GUID, Id)
    /// 4. Если файл нужно удалить вызывать DeleteFile(key). В зависимости от типа key файл удаляется либо из Temp, либо из Files
    /// 5. Для получения полного пути к файлу использовать GetPhysicalPath(key). Важно: если key типа GUID будет ссылка на файл в Temp,
    ///    если key типа int или string будет ссылка на файл в папке Files
    /// </summary>
    public class FileHelper
    {
        private const string GENERIC_FOLDER_URL = "~/Files/";
        private const string GENERIC_FOLDER_KEY = "GenericFilesFolder";
        private const string GENERIC_FOLDER_LOGIN = "GenericFilesLogin";
        private const string GENERIC_FOLDER_PASS = "GenericFilesPass";
        private const string TEMP_FOLDER_KEY = "TempFilesFolder";

        private static string genericFolderPhysicalPath;
        private static string genericFolderPhysicalPath_Pub;
        private static string genericTempFolderPhysicalPath;
        //private static string genericFolderPhysicalLogin;
        //private static string genericFolderPhysicalPass;

        private static string initPhysicalFolder(string directory, string settingsKey)
        {
            string settingsValue = System.Configuration.ConfigurationManager.AppSettings[settingsKey];
            if (!string.IsNullOrEmpty(settingsValue))
                directory = settingsValue;

            if (directory.StartsWith("~"))
            {
                //changed for correct ApfTest
                directory = string.Concat(AppDomain.CurrentDomain.BaseDirectory, directory.TrimStart('~', '/').Replace('/', '\\')); //System.Web.Hosting.HostingEnvironment.MapPath(directory);
            }

            if (directory == null)
                return "";

            if (!directory.EndsWith("\\"))
                directory += "\\";

            Logger.Debug(string.Format("Инициализация директории - {0} : {1}", settingsKey, directory));

            if (!Directory.Exists(directory))
            {
                try
                {
                    Directory.CreateDirectory(directory);
                }
                catch (Exception exp)
                {
                    Logger.Error(exp);
                }
            }

            return directory;
        }

        //private static byte[] loadFile(string path)
        //{
        //    if (!System.IO.File.Exists(path))
        //    {
        //        return new byte[0];
        //    }
        //    else
        //    {
        //        return System.IO.File.ReadAllBytes(path);
        //    }
        //}

        /// <summary>Полный путь к файлу в папке Files</summary>
        /// <remarks>
        /// По умолчанию путь к файлу формируется из [путь к папке][имя файла]
        /// Если задаётся идентификатор документа, то путь становится [путь к папке][ид]_[имя файла]
        /// </remarks>
        [Obsolete("Использовать GetPhysicalPath")]
        public static string GetGenericFilePhysicalPath(string Name, int? Id = null)
        {
            return genericFolderPhysicalPath + (Id.HasValue ? Id.Value.ToString() + "_" : null) + Name;
        }

        /// <summary>Полный путь к файлу в папке Temp</summary>
        [Obsolete("Использовать GetPhysicalPath")]
        public static string GetTempFilePhysicalPath(string Name)
        {
            return genericTempFolderPhysicalPath + Name;
        }

        /// <summary>
        /// Перемещает файл из папки Temp в папку Files
        /// </summary>
        /// <param name="guid">Guid в папке Temp</param>
        /// <param name="id">Id c которым сохранить файл в Files.</param>
        public static void MoveToGeneric(Guid guid, int id, bool replace = false)
        {
            string source = GetPhysicalPath(guid);
            string dest = GetPhysicalPath(id);
            if (replace && System.IO.File.Exists(dest))
            {
                System.IO.File.Delete(dest);
            }
            //if (dest.Contains("\\\\") && !string.IsNullOrEmpty(genericFolderPhysicalLogin))
            //{
            //    var lastIndex = dest.LastIndexOf("\\", StringComparison.Ordinal);
            //    //var nc = new NetworkConnection(lastIndex > 0 ? dest.Substring(0, lastIndex) : dest, new NetworkCredential(genericFolderPhysicalLogin, genericFolderPhysicalPass));
            //}
            System.IO.File.Move(source, dest);
        }

        /// <summary>
        /// Открывает файл в папке Files на чтение и тут же закрывает. Сделано для антивирусной проверки.
        /// </summary>
        /// <param name="id">Id c которым сохранить файл в Files</param>
        public static void OpenReadFile(int id)
        {
            string path = GetPhysicalPath(id);

            using (var stream = File.OpenRead(path))
            {
                stream.ReadByte();
            }
        }

        /// <summary>
        /// Скопировать файл из папки Temp в папку Files
        /// </summary>
        /// <param name="guid">Guid в папке Temp</param>
        /// <param name="id">Id c которым сохранить файл в Files.</param>
        public static void CopyToGeneric(Guid guid, int id)
        {
            string i = GetPhysicalPath(guid);
            string c = GetPhysicalPath(id);

            System.IO.File.Copy(i, c);
        }

        /// <summary>
        /// Скопировать файл в папке Files
        /// </summary>
        /// <param name="idI">Id источника</param>
        /// <param name="idC">Id копии</param>
        public static void Copy(int idI, int idC)
        {
            string i = GetPhysicalPath(idI);
            string c = GetPhysicalPath(idC);

            System.IO.File.Copy(i, c);
        }

        /// <summary>Удаляет файл</summary>
        public static void DeleteFile(object id)
        {
            string path = GetPhysicalPath(id);
            File.Delete(path);
        }

        /// <summary>
        /// Полный путь к файлу
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetPhysicalPath(object id)
        {
            if (id is int)
            {
                return string.Concat(genericFolderPhysicalPath, id);
            }
            else if (id is Guid)
            {
                return string.Concat(genericTempFolderPhysicalPath, id);
            }
            else if (id is string)
            {
                return string.Concat(genericFolderPhysicalPath, id.ToString().Replace('/', '\\'));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// проверяет наличие файла в файловой системе
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool IsExist(object id)
        {
            return File.Exists(GetPhysicalPath(id));
        }

        /// <summary>Возвращает признак общедоступности файла
        /// Если файл общедоступный, то в Files\Pub есть файл с именем Id или ид файла Guid
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static bool IsPublic(object Id)
        {
            return (Id != null &&
                    (Id is Guid || (Id is int && System.IO.File.Exists(genericFolderPhysicalPath_Pub + Id.ToString()))));
        }

        private const string TEMP_FOLDER = "~/Temp/";
        private const string FILES_FOLDER = "~/Files/";
        private const string LOGS_FOLDER = "~/Logs/";

        public static readonly string TempFolder = "~/Temp/";
        public static readonly string FilesFolder = "~/Files/";
        public static readonly string LogsFolder = "~/Logs/";

        private static Dictionary<string, string> contentTypes;

        //private static string tempFolderPhysicalPath;

        static FileHelper()
        {
            #region Новый функционал

            genericFolderPhysicalPath = initPhysicalFolder(GENERIC_FOLDER_URL, GENERIC_FOLDER_KEY);
            genericFolderPhysicalPath_Pub = genericFolderPhysicalPath + "pub\\";
            genericTempFolderPhysicalPath = initPhysicalFolder(TEMP_FOLDER, TEMP_FOLDER_KEY);
            //genericFolderPhysicalLogin = System.Configuration.ConfigurationManager.AppSettings[GENERIC_FOLDER_LOGIN];
            //genericFolderPhysicalPass = System.Configuration.ConfigurationManager.AppSettings[GENERIC_FOLDER_PASS];

            #endregion Новый функционал

            InitDirectory(ref TempFolder, "TempFolder");
            InitDirectory(ref FilesFolder, "FilesFolder");
            InitDirectory(ref LogsFolder, "LogsFolder");

            contentTypes = new Dictionary<string, string>();
            contentTypes.Add("rtf", "text/enriched");
            contentTypes.Add("html", "text/html");
            contentTypes.Add("txt", "text/plain");
            contentTypes.Add("csv", "text/csv");
            contentTypes.Add("xml", "text/xml");
            contentTypes.Add("mht", "multipart/related");
            contentTypes.Add("pdf", "applications/pdf");
            contentTypes.Add("doc", "application/msword");
            contentTypes.Add("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            contentTypes.Add("xls", "application/vnd.ms-excel");
            contentTypes.Add("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            contentTypes.Add("ppt", "application/vnd.ms-powerpoint");
            contentTypes.Add("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
            contentTypes.Add("odt", "application/vnd.oasis.opendocument.text");
            contentTypes.Add("ods", "application/vnd.oasis.opendocument.spreadsheet");
            contentTypes.Add("odp", "application/vnd.oasis.opendocument.presentation");

            //applicationPath = System.Web.Hosting.HostingEnvironment.MapPath("~/");
        }

        /// <summary>
        /// Вызывается для инициализации конструктора
        /// </summary>
        public static void Init()
        {
            if (!Directory.Exists(genericFolderPhysicalPath)) throw new Exception("Не найдено хранилище файлов. Проверьте параметр - GenericFilesFolder");
            if (!Directory.Exists(genericTempFolderPhysicalPath)) throw new Exception("Не найдено хранилище временных файлов. Проверьте параметр - TempFilesFolder");
        }

        private static string InitDirectory(ref string folder, string param)
        {
            string temp = System.Configuration.ConfigurationManager.AppSettings[param];

            if (!string.IsNullOrWhiteSpace(temp))
                folder = temp;

            folder = GetPath(folder);

            if (!Directory.Exists(folder))
            {
                try
                {
                    Directory.CreateDirectory(folder);
                }
                catch { }
            }

            return folder;
        }

        public static string GetPath(string folder)
        {
            if (folder.StartsWith("~"))
            {
                //Не понятно в чем разница
                //                if (HttpContext.Current != null)
                //                    return HttpContext.Current.Server.MapPath(folder);

                return string.Concat(System.AppDomain.CurrentDomain.BaseDirectory, folder.TrimStart('~', '/').Replace('/', '\\'));
            }

            return folder;
        }

        /// <summary>
        /// Сохранить в темповую папку
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public static string SaveInTemp(MemoryStream stream)
        {
            return SaveInTemp(stream, null);
        }

        //public static string SaveInTemp(string data, string name)
        //{
        //    using (var ms = new System.IO.MemoryStream())
        //    {
        //        using (var sw = new System.IO.StreamWriter(ms))
        //        {
        //            sw.Write(data ?? string.Empty);

        //            try
        //            {
        //                return SaveInTemp(ms, name);
        //            }
        //            catch
        //            {
        //                return string.Empty;
        //            }
        //        }
        //    }
        //}

        //[Obsolete("Использовать SaveInTemp(sb.ToString(), name + '.xml' )")]
        public static string SaveInTemp(System.Text.StringBuilder sb, string name)
        {
            using (var ms = new System.IO.MemoryStream())
            {
                using (var sw = new System.IO.StreamWriter(ms))
                {
                    sw.Write(sb.ToString());

                    try
                    {
                        return SaveInTemp(ms, string.Concat(name, ".xml"));
                    }
                    catch
                    {
                        return string.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// Сохранить в темповую папку
        /// </summary>
        public static string SaveInTemp(Stream stream, string name)
        {
            string fname = !string.IsNullOrEmpty(name) ? name : System.Guid.NewGuid().ToString();

            string fpath = string.Concat(GetPath(TempFolder), fname);

            if (!Directory.Exists(GetPath(TempFolder)))
            {
                Directory.CreateDirectory(GetPath(TempFolder));
            }

            var s = stream as MemoryStream;

            if (s != null)
            {
                System.IO.File.WriteAllBytes(fpath, s.GetBuffer());

                //using (FileStream file = new FileStream(fpath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                //{
                //    byte[] bytes = (stream as MemoryStream).GetBuffer();

                //    file.Write(bytes, 0, bytes.Length);
                //}
            }
            else
            {
                using (Stream file = File.OpenWrite(fpath))
                {
                    byte[] buffer = new byte[8 * 1024];
                    int len;
                    while ((len = stream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        file.Write(buffer, 0, len);
                    }
                }
            }

            return fpath;
        }

        /// <summary>
        /// Сохраняет файл во временной папке
        /// </summary>
        /// <param name="buffer">Содержимое файла</param>
        /// <returns></returns>
        public static string SaveInTemp(byte[] buffer)
        {
            string path = GetPhysicalPath(System.Guid.NewGuid());

            System.IO.File.WriteAllBytes(path, buffer);

            return path;
        }

        /// <summary>
        /// Возвращает полный путь к файлу в папке Files
        /// </summary>
        /// <param name="GenericFileName"></param>
        /// <returns></returns>
        public static string GetFilePath(string GenericFileName)
        {
            return Path.Combine(HttpContext.Current.Server.MapPath(FilesFolder), GenericFileName);
        }

        ///////////////////// Отправка файла клиенту ///////////////////////////

        /// <summary>
        /// убирает из имени файла недопустимые символы
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public static string TranslateFileName(string FileName)
        {
            if (string.IsNullOrEmpty(FileName))
                return "noname";
            FileName = FileName.Trim();

            char[] chars = FileName.ToCharArray();
            for (int idx = 0; idx < chars.Length; idx++)
            {
                char ch = chars[idx];
                if (char.IsLetterOrDigit(ch) || ch == '!' || ch == '@' || ch == '~'
                    || ch == '(' || ch == ')' || ch == '[' || ch == ']' || ch == '-' || ch == '.')
                    continue;
                chars[idx] = '_';
            }
            return new string(chars);
        }

        public static string GetContentType(string fileFormat)
        {
            if (contentTypes.ContainsKey(fileFormat))
            {
                return contentTypes[fileFormat];
            }

            return ("application/" + fileFormat);
        }

        /// <summary>
        /// Отсылает файл клиенту
        /// </summary>
        /// <param name="context"></param>
        /// <param name="fileName"></param>
        /// <param name="contentType"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public static void SendFile(HttpContext context, string fileName, string contentType, byte[] fileData)
        {
            if (fileData == null)
                fileData = new byte[0];

            context.Response.Clear();
            context.Response.Buffer = false;
            context.Response.ContentType = contentType ?? "application/octet-stream";

            context.Response.AppendHeader("Content-Length", fileData.Length.ToString());
            context.Response.AppendHeader("Content-Transfer-Encoding", "binary");
            context.Response.AppendHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0, max-age=0");

            string disposition = "attachment"; // "inline"
            if (context.Request.Browser.ActiveXControls)
                context.Response.AppendHeader("Content-Disposition", string.Format("{0}; filename={1}", disposition, HttpUtility.UrlEncode(fileName).Replace("+", "%20")));
            else
                context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");

            context.Response.BinaryWrite(fileData);
            context.Response.End();
        }

        /// <summary>
        /// Отсылает файл клиенту
        /// </summary>
        public static void SendFile(HttpContext context, string physicalFilePath, string fileName = null)
        {
            if (string.IsNullOrEmpty(physicalFilePath) || !System.IO.File.Exists(physicalFilePath))
            {
                ResponseHelper.FileNotFound(context.Response,
                                            string.Format("Файл не найден. physicalPath={0} fileName={1}", physicalFilePath, fileName));
                return;
            }

            context.Response.Clear();
            context.Response.Buffer = false;
            context.Response.ContentType = "application/octet-stream";

            context.Response.AppendHeader("Content-Length", (new FileInfo(physicalFilePath)).Length.ToString());
            context.Response.AppendHeader("Content-Transfer-Encoding", "binary");
            context.Response.AppendHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0, max-age=0");

            if (string.IsNullOrEmpty(fileName))
            {
                // извлекаем имя файла
                int index = physicalFilePath.LastIndexOfAny(new[] { '/', '\\' });
                fileName = (index > 0) ? physicalFilePath.Substring(index + 1) : physicalFilePath;
            }

            string disposition = "attachment"; // "inline"
            if (context.Request.Browser.ActiveXControls)
                context.Response.AppendHeader("Content-Disposition", string.Format("{0}; filename={1}", disposition, HttpUtility.UrlEncode(fileName).Replace("+", "%20")));
            else
                context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");

            context.Response.TransmitFile(physicalFilePath);
            context.Response.End();
        }

        /// <summary>
        /// Генерирует новое случайное название файла
        /// </summary>
        public static string GenerateFileName(string extension = null)
        {
            var fileName = Guid.NewGuid();

            if (!extension.IsNullOrWhiteSpace())
            {
                return string.Format("{0}.{1}", fileName, extension);
            }
            return fileName.ToString();
        }

        /////////////////////  Старое  /////////////////////

        public static string GetFileName(string fullPath)
        {
            return Path.GetFileName(fullPath);
        }

        public static void DeleteTempFile(string filename)
        {
            var filepath = FileHelper.GetTempFilePhysicalPath(filename);
            if (!string.IsNullOrEmpty(filename) && File.Exists(filepath))
            {
                File.Delete(filepath);
            }
        }

        /// <summary>
        /// Сохранить файл в папке с файлами
        /// </summary>
        public static string SaveInFiles(string fileName, string text)
        {
            string path = GetFilePath(fileName);

            File.WriteAllText(path, text + Environment.NewLine);

            return path;
        }

        /// <summary>
        /// Дописать в файл в папке с файлами
        /// </summary>
        public static string AppendInFiles(string fileName, string appendText)
        {
            string path = GetFilePath(fileName);

            File.AppendAllText(path, appendText + Environment.NewLine);

            return path;
        }

        /// <summary>
        /// Возвращает все данные из файла
        /// </summary>
        /// <param name="FileName">Имя файла во временной папке</param>
        /// <returns></returns>
        public static string ReadInFiles(string FileName)
        {
            string path = GetFilePath(FileName);

            return File.ReadAllText(path);
        }

        public static string ComputeMD5Checksum(string path)
        {
            using (FileStream fs = File.OpenRead(path))
            {
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] fileData = new byte[fs.Length];
                fs.Read(fileData, 0, (int)fs.Length);
                byte[] checkSum = md5.ComputeHash(fileData);
                string result = BitConverter.ToString(checkSum).Replace("-", String.Empty);
                return result;
            }
        }

        //////////////////// Вынесено из координации: переделать на GUID  ////////////////////

        private const string tempUrl = "~/TempFiles/";

        /// <summary>
        /// Возвращает путь к папке с файлами
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        public static string GetFileDirectory(string url = tempUrl)
        {
            string path = HttpContext.Current.Server.MapPath(url);
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.DirectoryInfo di = System.IO.Directory.CreateDirectory(path);
                //di.SetAccessControl();
            }
            return path;
        }
    }
}