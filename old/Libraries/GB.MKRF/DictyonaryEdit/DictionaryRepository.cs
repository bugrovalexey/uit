﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using GB.DataAccess.Repository;

namespace GB.MKRF.DictyonaryEdit
{
    public class DictionaryRepository : RepositorySt
    {
        public const string RefPrefix = "RF";

        /// <summary>
        /// Построить запрос выборки данных для справочника
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="itemID"></param>
        /// <returns></returns>
        private static string GetSQLQuery(MetaDictionary dict)
        {
            string Columns = string.Empty;
            string TablesRef = string.Empty;

            foreach (var item in dict.Columns.Values)
            {
                switch (item.Type)
                {
                    case ColumnType.TString:
                    case ColumnType.TInt:
                    case ColumnType.TDecimal:
                    case ColumnType.TBigString:
                    case ColumnType.TBool:
                    case ColumnType.TDateTime:
                        Columns = string.Format("{0}_this.{1},", Columns, item.ColumnName);
                        break;

                    case ColumnType.TRef:
                        MetaDictionary sdict = GetDataDictionary(item.RefId, false);
                        MetaDictionaryColumn columns = sdict.Columns[item.RefColumn];

                        Columns = string.Format("{0}_this.{2},", Columns, dict.ViewName, item.ColumnName);
                        Columns = string.Format("{0}_{1}.{2} as {4}{3},", Columns, sdict.ViewName, columns.ColumnName,
                                                item.ColumnName, RefPrefix);

                        TablesRef = string.Format("{0} left join {1} as _{1} on _{1}.{2} = _this.{4}",
                                                  TablesRef,
                                                  sdict.ViewName,
                                                  sdict.PrimaryKey.ColumnName,
                                                  dict.ViewName,
                                                  item.ColumnName);
                        break;

                    case ColumnType.TChild:
                        MetaDictionary cdict = GetDataDictionary(item.RefId, false);
                        Columns = string.Format("{0}{1}.{2},", Columns, cdict.ViewName, item.ColumnName);
                        break;

                    default:
                        throw new NotSupportedException("Тип не поддерживается: " + item.Type);
                }
            }

            if (dict.IsUse_InUse)
            {
                Columns = string.Format("{0}{1},", Columns, MetaDictionary.INUSE);
            }

            Columns = Columns.TrimEnd(',');

            return string.Format("select {0} from {1}  as _this {2}", Columns, dict.ViewName, TablesRef);
        }

        private static DataTable GetDictionaryData(MetaDictionary dict)
        {
            var sql = GetSQLQuery(dict);

            DataTable dt = null;
            try
            {
                dt = GetTableViaSQL(sql, null, "ResultData", dict.PrimaryKey.ColumnName);
            }
            catch (Exception e)
            {
                Logger.Fatal(e);
            }
            return dt;
        }

        /// <summary>
        /// Получиь данные записи справочника
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="itemID"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public static DataTable GetDictionaryDataItem(MetaDictionary dict, int itemID,
                                                        ref string error)
        {
            string sql = GetSQLQuery(dict);

            sql = string.Format("{0} where _this.{1}={2}", sql, dict.PrimaryKey.ColumnName, itemID);

            DataTable dt = GetTableViaSQL(sql, null, "ResultData");

            if (dt.Rows.Count <= 0)
            {
                error = "Не могу найти данные.";
            }

            if (dt.Rows.Count > 1)
            {
                error = "Данных слишком много.";
            }

            return dt;
        }

        /// <summary>
        /// Вставить новую запись в справочник
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public static string InsertDictionaryItem(MetaDictionary dict, Dictionary<string, string> values)
        {
            return UpdateDictionaryItem(dict, -1, values, true);
        }

        /// <summary>
        /// Обновить данные записи справочника
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="itemID"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public static string UpdateDictionaryItem(MetaDictionary dict, int itemID, Dictionary<string, string> values)
        {
            return UpdateDictionaryItem(dict, itemID, values, false);
        }

        /// <summary>
        /// Обновить данные записи справочника или вставить новую запись в справочник
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="itemID"></param>
        /// <param name="table"></param>
        /// <param name="isNewRow">Признак того что необходимо вставить повую запись</param>
        /// <returns></returns>
        private static string UpdateDictionaryItem(MetaDictionary dict, int itemID, Dictionary<string, string> values,
                                                   bool isNewRow)
        {
            string procedureName;
            var parameters = new Dictionary<string, object>();

            foreach (var item in dict.Columns.Values)
            {
                if (item.Visible)
                {
                    switch (item.Type)
                    {
                        case ColumnType.TBool:
                            try
                            {
                                parameters.Add(item.ColumnName, Convert.ToBoolean(values[item.ColumnName]));
                            }
                            catch (FormatException fe)
                            {
                                return fe.Message;
                            }
                            break;

                        case ColumnType.TString:
                        case ColumnType.TBigString:
                            parameters.Add(item.ColumnName, values[item.ColumnName]);
                            break;

                        case ColumnType.TInt:
                            try
                            {
                                parameters.Add(item.ColumnName, Convert.ToInt32(values[item.ColumnName]));
                            }
                            catch (FormatException fe)
                            {
                                return fe.Message;
                            }
                            break;

                        case ColumnType.TDecimal:
                            try
                            {
                                parameters.Add(item.ColumnName, Convert.ToDecimal(values[item.ColumnName]));
                            }
                            catch (FormatException fe)
                            {
                                return fe.Message;
                            }
                            break;

                        case ColumnType.TDateTime:
                            try
                            {
                                parameters.Add(item.ColumnName, Convert.ToDateTime(values[item.ColumnName]));
                            }
                            catch (FormatException fe)
                            {
                                return fe.Message;
                            }
                            break;

                        case ColumnType.TRef:
                            parameters.Add(item.ColumnName, values[item.ColumnName]);
                            break;

                        case ColumnType.TChild:
                            break;

                        default:
                            throw new NotSupportedException("Тип не поддерживается: " + item.Type);
                    }
                }
            }

            if (isNewRow)
            {
                procedureName = dict.ProcedureInsert;
            }
            else
            {
                procedureName = dict.ProcedureUpdate;
                if (parameters.ContainsKey(dict.PrimaryKey.ColumnName) == false)
                {
                    parameters.Add(dict.PrimaryKey.ColumnName, itemID);
                }
            }

            var currentUser = UserRepository.GetCurrent();
            parameters.Add("ZZZ_USERS_ID", currentUser != null
                ? currentUser.Id
                : new UserRepository().GetDefault().Id);

            try
            {
                ExecuteStoredProcedure(procedureName, parameters);
            }
            catch (Exception exp)
            {
                return exp.Message;
            }

            return string.Empty;
        }

        /// <summary>
        /// Удалить запись справочника
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public static string DeleteDictionaryItem(MetaDictionary dict, int itemID)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add(dict.PrimaryKey.ColumnName, itemID);

            var currentUser = UserRepository.GetCurrent();
            parameters.Add("ZZZ_USERS_ID", currentUser != null
                ? currentUser.Id
                : new UserRepository().GetDefault().Id);

            try
            {
                ExecuteStoredProcedure(dict.ProcedureDelete, parameters);
            }
            catch (Exception exp)
            {
                if (exp is SqlException)
                {
                    if ((exp as SqlException).Number == 50006)
                    {
                        return "Ошибка - удаление запрещено, существуют связанные объекты.";
                    }
                }

                return exp.Message;
            }

            return string.Empty;
        }

        /// <summary>
        /// Получить данные описывающие справочники
        /// </summary>
        public static Dictionary<int, MetaDictionary> GetDataDictionaries()
        {
            var sqlString =
                "SELECT ID, Name, ViewName, FieldUse_InUse as IsUse_InUse, ProcedureInsert, ProcedureUpdate" +
                ", ProcedureDelete, Visible, Access as AccessType " +
                "FROM Meta_Directories ORDER BY Sort";

            var items = ExecuteSqlToObject<MetaDictionary>(sqlString);

            var result = items.ToDictionary(i => i.ID, i => i);

            return result;
        }

        /// <summary>
        /// Получить данные описывающие справочник
        /// </summary>
        public static MetaDictionary GetDataDictionary(int id, bool withData = true)
        {
            var sqlString =
                        string.Format("SELECT ID, Name, ViewName, FieldUse_InUse as IsUse_InUse, ProcedureInsert, ProcedureUpdate" +
                                      ", ProcedureDelete, Visible, Access as AccessType " +
                                      "FROM Meta_Directories WHERE ID = {0} ORDER BY Sort", id);

            var dict = ExecuteSqlToObject<MetaDictionary>(sqlString).Single();

            dict.Columns = GetDataDictionaryColumns(id);
            dict.PrimaryKey =
                dict.Columns.Values.SingleOrDefault(x => x.Name == MetaDictionary.PRIMARY_KEY_NAME);

            if (withData)
                dict.Data = GetDictionaryData(dict);

            return dict;
        }

        /// <summary>
        /// Получить данные описывающие столбцы справочника
        /// </summary>
        public static Dictionary<int, MetaDictionaryColumn> GetDataDictionaryColumns(int dictId)
        {
            var sqlString =
                string.Format(
                    "SELECT ID, Dictionary_ID, Name, ColumnName, Visible" +
                    ", Type as TypeString, ISNULL(RefDictionary, -1) as RefId, ISNULL(RefColumn, -1) as RefColumn  " +
                    "FROM Meta_Directories_Column WHERE Dictionary_ID = {0} ORDER BY Sort",
                    dictId);

            try
            {
                var items = ExecuteSqlToObject<MetaDictionaryColumn>(sqlString);

                var result = items.ToDictionary(i => i.ID, i => i);

                return result;
            }
            catch (Exception exp)
            {
                throw new Exception("Meta_Directories_Column ID = " + dictId, exp);
            }
        }


    }
}