﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class Expenses242TypeMap : ClassMapBase<Expenses242Type>
    {
        public Expenses242TypeMap()
            : base("GO_Expense_2012")
        {
            //Table("w_");

            //Id(x => x.Id, "GO_Expense_2012_ID").GeneratedBy.Custom(typeof(IdGenerator));

			

			
        }

        protected override void InitMap()
        {
            Map(x => x.FirstYearExpense, "GO_Expense_2012_Y0");
			Map(x => x.SecondYearExpense, "GO_Expense_2012_Y1");
			Map(x => x.ThirdYearExpense, "GO_Expense_2012_Y2");
            
            References<Department>(x => x.Department, "Govt_Organ_ID");
        }
    }
}
