﻿namespace bgTeam.IsRui.Domain.Dto
{
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CharacteristicOfLicenseDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование типовой лицензии
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Модель лицензирования
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Тип соединения с сервером
        /// </summary>
        public TypeOfConnectionToServerEnum? TypeOfConnectionToServer { get; set; }

        /// <summary>
        /// Количество одновременных подключений
        /// </summary>
        public int? NumberOnlineSameTime { get; set; }

        /// <summary>
        /// Продуктовый номер лицензии
        /// </summary>
        public string ProductNumber { get; set; }

        /// <summary>
        /// Количество единовременно распределяемых лицензий
        /// </summary>
        public int? NumberAllocatedSameTime { get; set; }

        /// <summary>
        /// Лицензиар
        /// </summary>
        public string Licensor { get; set; }
    }
}
