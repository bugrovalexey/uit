﻿using bgTeam.IsRui.WebApp.Helpers;
using bgTeam.IsRui.WebApp.ViewModels.Activities;
using bgTeam.IsRui.Domain.Entities.Plans;
using System.Web.Mvc;
using bgTeam.IsRui.DataAccess;
using bgTeam.IsRui.DataAccess.Services;

namespace bgTeam.IsRui.WebApp.Controllers
{
    partial class ActivitiesController
    {
        public ActionResult ReasonsSingle(int id)
        {
            return View(new ReasonsViewModel(_statusRepository, id));
        }

        [HttpPost]
        public ActionResult ReasonsSingleEdit(ActivityReasonsArgs data)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                new ActivityService(_userRepository, _statusRepository).Update5(data);
            });
        }

        #region Другие основания реализации

        private IService<Reason, ProjectReasonArgs> _otherReasonsController;

        private IService<Reason, ProjectReasonArgs> OtherReasonsController
        {
            get { return _otherReasonsController ?? (_otherReasonsController = new OtherReasonsService()); }
        }

        public ActionResult OtherReasonEdit(int? id, int ownerId)
        {
            return View(new OtherReasonEditViewModel(_statusRepository, ownerId, id));
        }

        [HttpPost]
        public ActionResult OtherReasonEdit(ProjectReasonArgs args)
        {
            return NotificationHelper.NotificationCreateEdit(() =>
            {
                bool isNew = args.Id == 0;
                var result = isNew
                    ? OtherReasonsController.Create(args)
                    : OtherReasonsController.Update(args);

                return new TableOtherReason(result)
                {
                    success = true,
                    message = isNew ? "Запись успешно создана" : NotificationHelper.AddTextSuccess
                };
            });
        }

        [HttpPost]
        public ActionResult OtherReasonDelete(int id)
        {
            return NotificationHelper.NotificationDelete(id, () => OtherReasonsController.Delete(new ProjectReasonArgs { Id = id }));
        }

        #endregion Другие основания реализации
    }
}