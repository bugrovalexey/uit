﻿namespace bgTeam.IsRui.Domain.Entities.Documents
{
    /// <summary>
    /// Тип документа
    /// </summary>
    public enum DocTypeEnum
    {
        None = 0,

        /// <summary>Реквизиты исходящих</summary>
        Outgoing = 1,

        /// <summary>Реквизиты входящих</summary>
        Inbox = 4,

        /// <summary>Документ к заявке</summary>
        demand_document = 2,

        /// <summary>Опись</summary>
        demand_inventory = 3,

        /// <summary>Особое мнение</summary>
        separate_opinion = 5,

        /// <summary>Нормативная документация</summary>
        normative_doc = 8,

        /// <summary>Заседание экспертной комиссии (протоколы и решения)</summary>
        cmsn_meeting = 10,

        /// <summary>Документы для FAQ</summary>
        faq_doc = 17,

        /// <summary>Документы ответственного за мероприятие</summary>
        UserRequest = 6,

        /// <summary>Документы-основания для мероприятия </summary>
        ActivityReason = 7,

        /// <summary>Экспертное заключения</summary>
        ExpertConclusion = 11,

        /// <summary>ЭЦП Эксперта</summary>
        PackageExpert = 18,

        /// <summary>ЭЦП Минкомсвязи</summary>
        PackageMKS = 19,

        /// <summary>ЭЦП ОГВ</summary>
        PackageOGV = 20,

        /// <summary>Документ-основание проекта</summary>
        Reference = 21,

        /// <summary>Документ для отчета по плану</summary>
        ReportByPlan = 22,

        /// <summary>Подписанный архив с отчётами по плану</summary>
        SignedReportByPlan = 23,

        /// <summary>Документы работ по мероприятию</summary>
        Work = 24,

        /// <summary>Документы товаров по мероприятию</summary>
        Spec = 25,

        /// <summary>Логотип</summary>
        Logo = 26,

        /// <summary>Документы мероприятия</summary>
        Activity = 27,

        /// <summary>Дополнительный материалы в рамках ФЭО</summary>
        ActivityFEO = 28,

        /// <summary>Решение о создании (закупке) ОУ</summary>
        DecisionToCreateAccountingObject = 29,

        /// <summary>Документы-основания реализации мероприятия</summary>
        OtherReason = 30,

        /// <summary>Концепция</summary>
        Conception = 31,

        /// <summary>
        /// Документ ввода в эксплуатацию ОУ
        /// </summary>
        CommissioningAccountingObject = 32,

        /// <summary>
        /// Документ вывода из эксплуатации ОУ
        /// </summary>
        DecommissioningAccountingObject = 33,

        /// <summary>
        /// Сведения о прохождении специальных проверок
        /// </summary>
        SpecialCheck = 34,

        /// <summary>
        /// Основания для создания ОУ
        /// </summary>
        GroundsForCreationAccountingObject = 35,

        /// <summary>
        /// Акт сдачи-приемки
        /// </summary>
        ActOfAcceptance = 36,

        /// <summary>Прикрепленные документы к комментарию</summary>
        AccountingObjectCommentDocument = 37,

        /// <summary>
        /// Заключение пользователя при переводе статуса объекта
        /// </summary>
        WorkflowUserConclusion = 38,

        /// <summary>
        /// История заключения пользователя при переводе статуса объекта
        /// </summary>
        WorkflowUserConclusionHistory = 39,

        /// <summary>
        /// Документ заявки
        /// </summary>
        ClaimDocument = 40,

        /// <summary>
        /// Акт заявки
        /// </summary>
        ClaimAct = 41,

        /// <summary>
        /// Комментарий к заявке
        /// </summary>
        ClaimUserComment = 42,

        /// <summary>
        /// Акт мероприятия
        /// </summary>
        ActivityAct = 43
    }
}