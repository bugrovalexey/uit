﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.DataAccess;
    using bgTeam.Extensions;
    using bgTeam.IsRui.Common;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class GetInformationInteractionStory : IStory<GetInformationInteractionStoryContext, InformationInteractionDto>
    {
        private readonly IRepositoryRui _repository;
        private readonly IMapperBase _mapperBase;

        public GetInformationInteractionStory(
            IRepositoryRui repository,
            IMapperBase mapperBase)
        {
            _repository = repository;
            _mapperBase = mapperBase;
        }

        public InformationInteractionDto Execute(GetInformationInteractionStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<InformationInteractionDto> ExecuteAsync(GetInformationInteractionStoryContext context)
        {
            context.ActivityId.CheckNull(nameof(context.ActivityId));

            var res = _repository.GetAllEx<InformationInteraction>(q =>
                    q.Where(x => x.PlansActivity.Id == context.ActivityId))
                .FirstOrDefault();

            if (res == null)
            {
                return new InformationInteractionDto();
            }

            return _mapperBase.Map(res, new InformationInteractionDto());
        }
    }
}
