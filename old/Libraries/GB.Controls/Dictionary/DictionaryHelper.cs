﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using GB;
using DevExpress.Utils;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

namespace GB.Controls.Dictionary
{
    /// <summary>
    /// Предоставляет дополнительные методы при работе со справочниками
    /// </summary>
    internal static class DictionaryHelper
    {
        internal const string ButtonEdit = "Редактировать";
        internal const string ButtonDelete = "Удалить";
        internal const string ButtonSelect = "Выбрать";

        internal const string RefPrefix = "RF";

        /// <summary>
        /// Заполнить таблицу столбцами по описанию справочника
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="grid"></param>
        /// <param name="forDistionaryEditControl">флаг создаётся ли грид в forDistionaryEditControl</param>
        public static void CreareGridColumns(MetaDictionary dict, DevExpress.Web.ASPxGridView.ASPxGridView grid, bool forDistionaryEditControl = false)
        {
            GridViewColumn ecolumn = grid.Columns["cmdColumn"];

            grid.Columns.Clear();

            int i = 1;
            foreach (var item in dict.ListColumns.Values)
            {
                DevExpress.Web.ASPxGridView.GridViewDataTextColumn Column = new DevExpress.Web.ASPxGridView.GridViewDataTextColumn();

                Column.Caption = item.Name;
                Column.FieldName = item.Type != ColumnType.TRef ? item.ColumnName : RefPrefix + item.ColumnName;
                Column.VisibleIndex = i;
                Column.Visible = item.Visible;
                Column.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                Column.PropertiesTextEdit.EncodeHtml = false;

                grid.Columns.Add(Column);

                if (item.Type == ColumnType.TRef)
                {
                    Column = new DevExpress.Web.ASPxGridView.GridViewDataTextColumn
                                 {
                                     Caption = item.ColumnName,
                                     FieldName = item.ColumnName,
                                     VisibleIndex = ++i,
                                     Visible = false
                                 };
                    grid.Columns.Add(Column);
                }

                i++;
            }


            if (dict.IsUse_InUse)
            {
                DevExpress.Web.ASPxGridView.GridViewDataTextColumn Column = new DevExpress.Web.ASPxGridView.GridViewDataTextColumn();

                Column.Caption = "In Use";
                Column.FieldName = MetaDictionary.INUSE;
                Column.VisibleIndex = 99;
                Column.Visible = false;

                grid.Columns.Add(Column);
            }

            grid.KeyFieldName = dict.PrimaryKey.ColumnName;

            ecolumn.VisibleIndex = 100;
            ecolumn.Visible = !dict.ReadOnly || forDistionaryEditControl;

            grid.Columns.Add(ecolumn);
        }

        /// <summary>
        /// Построить запрос выборки данных для справочника
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="itemID"></param>
        /// <returns></returns>
        internal static string GetSQLQuery(MetaDictionary dict)
        {
            string Columns = string.Empty;
            string TablesRef = string.Empty;

            foreach (var item in dict.ListColumns.Values)
            {
                switch (item.Type)
                {
                    case ColumnType.TString:
                    case ColumnType.TInt:
                    case ColumnType.TDecimal:
                    case ColumnType.TBigString:
                    case ColumnType.TDateTime:
                    case ColumnType.TBool:
                        Columns = string.Format("{0}_this.{1},", Columns, item.ColumnName);
                        break;
                    case ColumnType.TRef:
                        MetaDictionary sdict = MetaDictionaries.ListDictionaryItem[item.RefId];
                        MetaDictionaryColumn columns = sdict.ListColumns[item.RefColumn];

                        Columns = string.Format("{0}_this.{2},", Columns, dict.ViewName, item.ColumnName);
                        Columns = string.Format("{0}_{1}.{2} as {4}{3},", Columns, sdict.ViewName, columns.ColumnName, item.ColumnName, RefPrefix);

                        TablesRef = string.Format("{0} left join {1} AS _{1} on _{1}.{2} = _this.{4}",
                            TablesRef,
                            sdict.ViewName,
                            sdict.PrimaryKey.ColumnName,
                            dict.ViewName,
                            item.ColumnName);
                        break;
                    case ColumnType.TChild:
                        MetaDictionary cdict = MetaDictionaries.ListDictionaryItem[item.RefId];
                        Columns = string.Format("{0}{1}.{2},", Columns, cdict.ViewName, item.ColumnName);
                        break;
                    default:
                        throw new NotSupportedException("Тип не поддерживается: " + item.Type);
                }
            }

            if (dict.IsUse_InUse)
            {
                Columns = string.Format("{0}{1},", Columns, MetaDictionary.INUSE);
            }

            Columns = Columns.TrimEnd(',');

            return string.Format("select {0} from {1} as _this {2}", Columns, dict.ViewName, TablesRef);
        }

        internal static DataTable GetDataDictionary(MetaDictionary dict)
        {
            var sql = GetSQLQuery(dict);

            if(!string.IsNullOrEmpty(dict.OrderBy))
                sql = string.Format("{0} order by {1}", sql, dict.OrderBy);

            SqlDataAdapter adapter = new SqlDataAdapter(sql, MetaDictionaries.ConnectionString);

            DataTable dt = new DataTable("ResultData");
            try
            {
                adapter.Fill(dt);
            }
            catch(Exception e)
            {
                Logger.Fatal(e);
            }
            return dt;
        }

        /// <summary>
        /// Получиь данные записи справочника
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="itemID"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        internal static DataTable GetDataDictionaryItem(MetaDictionary dict, int itemID, ref string error)
        {
            string Query = GetSQLQuery(dict);

            Query = string.Format("{0} where _this.{1}={2} {3}", 
                Query, 
                dict.PrimaryKey.ColumnName, 
                itemID, 
                string.IsNullOrEmpty(dict.OrderBy) ? string.Empty : " order by " + dict.OrderBy);

            SqlDataAdapter adapter = new SqlDataAdapter(Query, MetaDictionaries.ConnectionString);

            DataTable dt = new DataTable("ResultData");

            try
            {
                adapter.Fill(dt);

                if (dt.Rows.Count <= 0)
                {
                    error = "Не могу найти данные.";
                }

                if (dt.Rows.Count > 1)
                {
                    error = "Данных слишком много.";
                }
            }
            catch (Exception exp)
            {
                error = exp.Message;
            }

            return dt;
        }

        /// <summary>
        /// Вставить новую запись в справочник
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        internal static string InsertDictionaryItem(MetaDictionary dict, Table table, int userId)
        {
            return UpdateDictionaryItem(dict, -1, table, true, userId);
        }

        /// <summary>
        /// Обновить данные записи справочника
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="itemID"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        internal static string UpdateDictionaryItem(MetaDictionary dict, int itemID, Table table, int userId)
        {
            return UpdateDictionaryItem(dict, itemID, table, false, userId);
        }

        /// <summary>
        /// Обновить данные записи справочника или вставить новую запись в справочник
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="itemID"></param>
        /// <param name="table"></param>
        /// <param name="isNewRow">Признак того что необходимо вставить повую запись</param>
        /// <returns></returns>
        private static string UpdateDictionaryItem(MetaDictionary dict, int itemID, Table table, bool isNewRow, int userId)
        {
            using (SqlConnection connection = new SqlConnection(MetaDictionaries.ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();

                foreach (var item in dict.ListColumns.Values)
                {
                    if (item.Visible)
                    {
                        Control contr = FindControl(table, item.ColumnName);//table.FindControl(item.ColumnName);

                        if (contr != null)
                        {
                            switch (item.Type)
                            {
                                case ColumnType.TBool:
                                    command.Parameters.Add("@" + item.ColumnName, SqlDbType.Bit).Value = (contr as ASPxCheckBox).Checked;
                                    break;
                                case ColumnType.TString:
                                    command.Parameters.Add("@" + item.ColumnName, SqlDbType.VarChar).Value = (contr as ASPxTextBox).Text;
                                    break;
                                case ColumnType.TBigString:
                                    command.Parameters.Add("@" + item.ColumnName, SqlDbType.VarChar).Value = (contr as ASPxMemo).Text;
                                    break;
                                case ColumnType.TInt:
                                    try
                                    {
                                        command.Parameters.Add("@" + item.ColumnName, SqlDbType.Int).Value = Convert.ToInt32((contr as ASPxTextBox).Text);
                                    }
                                    catch (FormatException fe)
                                    {
                                        return fe.Message;
                                    }
                                    break;
                                case ColumnType.TDecimal:
                                    try
                                    {
                                        command.Parameters.Add("@" + item.ColumnName, SqlDbType.Money).Value =
                                            Convert.ToDecimal((contr as ASPxTextBox).Text);
                                    }
                                    catch (FormatException fe)
                                    {
                                        return fe.Message;
                                    }
                                    break;

                                case ColumnType.TDateTime:
                                    try
                                    {
                                        command.Parameters.Add("@" + item.ColumnName, SqlDbType.DateTime).Value = Convert.ToDateTime((contr as ASPxDateEdit).Value);
                                    }
                                    catch (FormatException fe)
                                    {
                                        return fe.Message;
                                    }
                                    break;
                                case ColumnType.TRef:
                                    Control cont = FindControl(table, "HiddenField_" + item.ColumnName);
                                    command.Parameters.Add("@" + item.ColumnName, SqlDbType.VarChar).Value = (cont as HiddenField).Value;
                                    break;
                                default:
                                    throw new NotSupportedException("Тип не поддерживается: " + item.Type);

                            }
                        }
                    }
                }

                command.CommandType = CommandType.StoredProcedure;

                if (isNewRow)
                {
                    command.CommandText = dict.ProcedureInsert;
                }
                else
                {
                    command.CommandText = dict.ProcedureUpdate;
                    if (!command.Parameters.Contains("@" + dict.PrimaryKey.ColumnName))
                        command.Parameters.Add("@" + dict.PrimaryKey.ColumnName, SqlDbType.Int).Value = itemID;
                }

                command.Parameters.Add("@ZZZ_USERS_ID", SqlDbType.Int).Value = userId;

                try
                {
                    connection.Open();

                    command.ExecuteNonQuery();
                }
                catch (Exception exp)
                {
                    return exp.Message;
                }
            }

            return string.Empty;
        }


        /// <summary>
        /// Удалить запись справочника
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="itemID"></param>
        /// <returns></returns>
        internal static string DeleteDictionaryItem(MetaDictionary dict, int itemID, int userId)
        {
            using (SqlConnection connection = new SqlConnection(MetaDictionaries.ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = dict.ProcedureDelete;

                command.Parameters.Add("@" + dict.PrimaryKey.ColumnName, SqlDbType.Int).Value = itemID;
                command.Parameters.Add("@ZZZ_USERS_ID", SqlDbType.Int).Value = userId; 

                try
                {
                    connection.Open();

                    command.ExecuteNonQuery();
                }
                catch (Exception exp)
                {
                    if (exp is SqlException)
                    {
                        if ((exp as SqlException).Number == 50006)
                        {
                            return "Ошибка - удаление запрещено, существуют связанные объекты.";
                        }
                    }

                    return exp.Message;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Найти контрол
        /// </summary>
        internal static Control FindControl(Control root, string id)
        {
            if (root.ID == id)
                return root;

            foreach (Control Ctl in root.Controls)
            {
                Control FoundCtl = FindControl(Ctl, id);
                if (FoundCtl != null)
                    return FoundCtl;
            }

            return null;
        }
    }
}