﻿using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.MKRF.Logic.MasterEdit;
using System.Collections.Generic;
using System.Linq;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects.Master
{
    public class DocumentBaseViewModel : StepBaseViewModel
    {
        public IEnumerable<TableAODecisionToCreate> Documents { get; set; }


        public DocumentBaseViewModel(int id)
            : base(id, MasterItemEnum.DocumentBase)
        {
            Documents =
                new RepositoryDc<AODecisionToCreate>().GetAllEx(
                    q => q.Fetch(x => x.Documents).Eager.Where(x => x.AccountingObject.Id == id))
                    .Select(doc => new TableAODecisionToCreate(doc));
        }
    }
}