﻿namespace GB.MKRF.Entities.Admin
{
    /// <summary>
    /// Тип рассылки
    /// </summary>
    public enum NotificationTypeEnum
    {
        Test = 0,

        //Уведомление о наступлении планируемой даты публикации
        PlansZakupkiPublishDate = 1,

        //Уведомление о наступлении даты вскрытия конвертов
        PlansZakupkiOpenDate = 2,

        //Уведомление о наступлении даты рассмотрения заявок
        PlansZakupkiConsDate = 3,

        //Уведомление о наступлении даты подведения итогов
        PlansZakupkiItogDate = 4,

        //Уведомление о нарушении сроков
        PlansZakupkiViolationOfTerms = 5
    }

    /// <summary>
    /// Рассылка (шаблон)
    /// </summary>
    public class Notification : EntityBase
    {
        /// <summary>
        /// Название
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Тема сообщения
        /// </summary>
        public virtual string Subject { get; set; }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public virtual string Message { get; set; }

        /// <summary>
        /// Активна?
        /// </summary>
        public virtual bool IsActive { get; set; }
    }
}