﻿namespace bgTeam.IsRui.Domain.Entities.Purchase
{
    /// <summary>Предмет контракта</summary>
    public class ContractProduct : EntityBase
    {
        /// <summary> контракт </summary>
        public virtual Contract Contract { get; set; }

        /// <summary>Наименование товара, работ, услуг </summary>
        public virtual string Name { get; set; }

        /// <summary>Количество поставляемого товара, объѐма выполняемых работ, оказываемых услуг </summary>
        public virtual double Quantity { get; set; }

        /// <summary>Классификация товаров, работ и услуг </summary>
        public virtual string OKDP_Code { get; set; }

        /// <summary>Наименование товара, работ, услуг </summary>
        public virtual string OKDP_Name { get; set; }

        /// <summary>Единицы измерения </summary>
        public virtual string OKEI_Code { get; set; }

        /// <summary>Наименование единиц измерения </summary>
        public virtual string OKEI_Name { get; set; }

        /// <summary>Цена контракта</summary>
        public virtual decimal Price { get; set; }

        /// <summary>Стоимость поставляемого товара, выполняемых работ, оказываемых услуг </summary>
        public virtual decimal Sum { get; set; }
    }
}