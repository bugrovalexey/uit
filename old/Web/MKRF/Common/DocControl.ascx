﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocControl.ascx.cs" Inherits="GB.MKRF.Web.Common.DocControl" %>
<%@ Import Namespace="GB" %>

<script type="text/javascript">
    function hfSave(e, hidf) {
        hidf.Set('Name', NameTextBox.GetText());
        hidf.Set('Num', NumTextBox.GetText());
        hidf.Set('Date', DateDateEdit.GetValue());

        //if (typeof (DecisionGovComissionCheckBox) == 'undefined') {
        //    hidf.Set('DecisionGovComission', false);
        //} else hidf.Set('DecisionGovComission', DecisionGovComissionCheckBox.GetChecked());

        hidf.Set('DocTypeText', typeof (DocTypeTextTextBox) != 'undefined' ? DocTypeTextTextBox.GetText() : null);
        hidf.Set('Comment', typeof (CommentMemo) != 'undefined' ? CommentMemo.GetText() : null);
        hidf.Set('File', uploader2.GetFiles());
    }
</script>
<div id="panel">
    <dxc:ASPxCallback runat="server" ID="callBack" oncallback="callBack_Callback">
    </dxc:ASPxCallback>
    <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server"
        Modal="True" ContainerElementID="panel" 
        Text="Проверка подписи&amp;hellip;">
    </dxlp:ASPxLoadingPanel>
    <dxe:ASPxButton ID="addButton" runat="server" AutoPostBack="false"
        Text="Добавить" Style="margin-bottom: 10px;">
    </dxe:ASPxButton>
    <dxhf:ASPxHiddenField runat="server" ID="hfDoc" ClientInstanceName="hfDoc" />
    <dxwgv:ASPxGridView runat="server" ID="docsGrid" Width="100%" KeyFieldName="Id" AutoGenerateColumns="False"
        OnBeforePerformDataSelect="docsGrid_BeforePerformDataSelect"
        OnInitNewRow="docsGrid_InitNewRow" OnRowDeleting="docsGrid_RowDeleting" OnRowInserting="docsGrid_RowInserting"
        OnRowUpdating="docsGrid_RowUpdating" OnStartRowEditing="docsGrid_StartRowEditing"
        OnCancelRowEditing="docsGrid_CancelRowEditing" OnCommandButtonInitialize="docsGrid_CommandButtonInitialize">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdCheckBox" ShowSelectCheckbox="true" VisibleIndex="0"
                AllowDragDrop="False" Visible="false" Width="50px" />
            <dxwgv:GridViewCommandColumn Caption=" " Name="cmdColumn" AllowDragDrop="False"
                ShowInCustomizationForm="False" >
                <EditButton Visible="True" />
                <DeleteButton Visible="True" />
                <ClearFilterButton Visible="True" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Тип" Name="DocTypeText" FieldName="DocTypeText"  Visible="false">
                <Settings FilterMode="DisplayText" AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Наименование" FieldName="Name" >
                <Settings FilterMode="DisplayText" AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Номер" FieldName="Num" >
                <Settings FilterMode="DisplayText" AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataDateColumn Caption="Дата" FieldName="DateDoc" />
            <dxwgv:GridViewDataDateColumn Caption="Дата добавления" FieldName="Date" />
            <dxwgv:GridViewDataTextColumn Caption="Ответственный" FieldName="Responsible" />
            <dxwgv:GridViewDataTextColumn Caption="Комментарий" FieldName="Comment" Name="Comment" Visible="false" />
            
            <%--<dxwgv:GridViewDataTextColumn Caption="Решение Прав. Комиссии" FieldName="IsDecision" Name="IsDecision" />--%>

            <dxwgv:GridViewDataTextColumn ShowInCustomizationForm="False">
                <Settings AllowDragDrop="False" />
                <DataItemTemplate>
                    <%#getDownloadUrl((int)Eval("Id"), (bool)Eval("IsSigned"), (int)Eval("Composite"))%>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Settings />
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsPager PageSize="100" />
        <SettingsEditing Mode="EditForm" />
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc required">Наименование</td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="NameTextBox" runat="server" ClientInstanceName="NameTextBox" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>" 
                                OnDataBinding="NameTextBox_DataBinding">
                                <ValidationSettings ErrorDisplayMode="Text" >
                                    <RequiredField ErrorText="Обязательное поле" IsRequired="true" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>

                    <%if (ShowDocType) {%>
                    <tr>
                        <td class="form_edit_desc">Тип документа</td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="DocTypeTextTextBox" runat="server" ClientInstanceName="DocTypeTextTextBox" 
                                OnDataBinding="DocTypeTextTextBox_DataBinding">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <%} %>

                    <tr>
                        <td class="form_edit_desc">Номер</td>
                        <td class="form_edit_input">
                            <dxe:ASPxTextBox ID="NumTextBox" runat="server" ClientInstanceName="NumTextBox" 
                                OnDataBinding="NumTextBox_DataBinding">
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Дата</td>
                        <td class="form_edit_input">
                            <dxe:ASPxDateEdit runat="server" ID="DateDateEdit" ClientInstanceName="DateDateEdit" 
                                ondatabinding="DateDateEdit_DataBinding" >
                            </dxe:ASPxDateEdit>
                        </td>
                    </tr>

                    <%if (ShowComment) {%>
                    <tr>
                        <td class="form_edit_desc">Комментарий</td>
                        <td class="form_edit_input">
                            <dxe:ASPxMemo ID="CommentMemo" runat="server" ClientInstanceName="CommentMemo" 
                                OnDataBinding="CommentMemo_DataBinding">
                            </dxe:ASPxMemo>
                        </td>
                    </tr>
                    <%} %>
                    
                    <%--<%if (ShowIsDecision) {%>
                    <tr>
                        <td class="form_edit_desc">Решение Прав. Комиссии</td>
                        <td class="form_edit_input">
                            <dxe:ASPxCheckBox ID="DecisionGovComissionCheckBox" runat="server" ClientInstanceName="DecisionGovComissionCheckBox" 
                                OnDataBinding="DecisionGovComissionCheckBox_DataBinding" OnInit="DecisionGovComissionCheckBox_Init" >
                            </dxe:ASPxCheckBox>
                        </td>
                    </tr>
                    <%} %>--%>

                    <tr id="Tr1" runat="server">
                        <td class="form_edit_desc required">
                            Файл
                        </td>
                        <td class="form_edit_input">
                            <%--<div id="docDiv">
                                <%=currentDoc != null ? currentDoc.FileName : ""%>
                            </div>
                            <dxuc:ASPxUploadControl ID="uploader" runat="server" OnDataBinding="uploader_DataBinding"
                                OnFileUploadComplete="uploader_FileUploadComplete" >
                                <BrowseButton Text="Открыть..." />
                                <AdvancedModeSettings PacketSize="512000" />
                            </dxuc:ASPxUploadControl>--%>

                            <cc:FileUploadControl2 runat="server" ID="uploader2" Mode="OneFile" ClientInstanceName="uploader2"
                                OnDataBinding="uploader2_DataBinding" AutoUpload="true" />

                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                            <span class="ok"><dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" /></span>
                            <span class="cancel"><dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" /></span>
                        </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
    </dxwgv:ASPxGridView>
    <dxc:ASPxCallback runat="server" ID="RoleCallBack" 
        ClientInstanceName="RoleCallBack" oncallback="RoleCallBack_Callback" >
        <ClientSideEvents CallbackComplete="function(s, e) { PopupWindow.Hide(); }" />
    </dxc:ASPxCallback>
    <dxpc:ASPxPopupControl ID="PopupWindow" runat="server" ClientInstanceName="PopupWindow"
        CloseAction="CloseButton" HeaderText="Настройка доступа" Modal="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="True" 
        Width="700px" >
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <dxwgv:ASPxGridView runat="server" ID="RoleGrid" ClientInstanceName="RoleGrid"
                    AutoGenerateColumns="False"
                    Width="100%" KeyFieldName="Id" OnCustomCallback="RoleGrid_CustomCallback" >
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Роль" FieldName="Name"
                            VisibleIndex="2">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewCommandColumn Caption=" " ShowSelectCheckbox="True"  VisibleIndex="3"
                            Width="30px">
                        </dxwgv:GridViewCommandColumn>
                        <dxwgv:GridViewDataTextColumn Caption=" " FieldName="DocId" VisibleIndex="4" Visible="false" />
                    </Columns>
                    <SettingsPager PageSize="20">
                    </SettingsPager>
                    <Styles>
                        <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        </Header>
                    </Styles>
                    <ClientSideEvents EndCallback="function(s, e) { PopupWindow.Show(); }" />
                </dxwgv:ASPxGridView>
                <br />
                <div style="float: right;">
                    <table>
                        <tr>
                            <td>
                                <dxe:ASPxButton ID="SaveButton" runat="server" AutoPostBack="False" Text="Сохранить" Width="80px">
                                    <ClientSideEvents Click="function(s, e) { RoleCallBack.PerformCallback(''); }" />
                                </dxe:ASPxButton>
                            </td>
                            <td>
                                <dxe:ASPxButton ID="CloseButton" runat="server" AutoPostBack="False" CommandName="Close" Width="80px"
                                    Text="Закрыть">
                                    <ClientSideEvents Click="function(s, e) { PopupWindow.Hide(); }" />
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="11px" />
                        </tr>
                    </table>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
</div>
