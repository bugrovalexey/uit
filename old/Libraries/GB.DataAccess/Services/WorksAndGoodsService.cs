﻿using AutoMapper;
using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.WorksAndGoods;
using GB.DataAccess.Services.Args;
using NHibernate.Util;
using System;
using System.Collections;

namespace GB.DataAccess.Services
{
    public class WorksAndGoodsService : ServiceBase<WorkAndGoods>, IService<WorkAndGoods, IWorkAndGoodsArgs>
    {
        public WorkAndGoods Create(IWorkAndGoodsArgs args)
        {
            var workAndGoodsArgs = Mapper.Map<WorkAndGoodsArgs>(args);

            var entity = new WorkAndGoods();
            var result = SaveOrUpdate(entity, workAndGoodsArgs);

            return result;
        }


        public WorkAndGoods Create(WorkAndGoodsEditArgs args)
        {
            var entity = Repository.GetExisting(args.NewId);

            if (args.ActsOfAcceptanceId.HasValue)
            {
                entity.ActOfAcceptance = GetEntity<ActOfAcceptance>(args.ActsOfAcceptanceId);
                SaveOrUpdate(entity);
            }

            return entity;
        }

        public WorkAndGoods Update(IWorkAndGoodsArgs args)
        {
            var workAndGoodsArgs = Mapper.Map<WorkAndGoodsArgs>(args);

            var entity = Get(workAndGoodsArgs);
            return SaveOrUpdate(entity, workAndGoodsArgs);
        }

        public WorkAndGoods Update(WorkAndGoodsEditArgs args)
        {
            var entity = Repository.GetExisting(args.NewId);

            if (args.Id == args.NewId)
            {
                return entity;
            }

            var lastEntity = Repository.GetExisting(args.Id);
            lastEntity.ActOfAcceptance = null;
            SaveOrUpdate(lastEntity);

            entity.ActOfAcceptance = GetEntity<ActOfAcceptance>(args.ActsOfAcceptanceId);
            SaveOrUpdate(entity);

            return entity;
        }

        public void SetActOfAcceptanceForWorkAndGoods(ICollection worksAndGoodsIds,
            ActOfAcceptance actOfAcceptance)
        {
            var worksAndGoods = Repository.GetAllEx(q => q.WhereRestrictionOn(x => x.Id).IsIn(worksAndGoodsIds));

            worksAndGoods.ForEach(x =>
            {
                if (x.ActOfAcceptance == null)
                {
                    x.ActOfAcceptance = actOfAcceptance;
                    SaveOrUpdate(x);
                }
            });
        }

        private WorkAndGoods SaveOrUpdate(WorkAndGoods entity, WorkAndGoodsArgs args)
        {
            Mapper.Map(args, entity);
            SaveOrUpdate(entity);
            return entity;
        }

        public void Delete(IWorkAndGoodsArgs args)
        {
            var workAndGoodsArgs = Mapper.Map<WorkAndGoodsArgs>(args);
            var entity = Get(workAndGoodsArgs);
            ClearActOfAcceptance(entity);
            Delete(entity);
        }

        public void Delete(WorkAndGoodsEditArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            ClearActOfAcceptance(entity);
        }


        public void DeleteAllByActOfAcceptance(int actOfAcceptanceId)
        {
            var worksAndGoods = Repository.GetAllEx(q => q.Where(x => x.ActOfAcceptance.Id == actOfAcceptanceId));
            worksAndGoods.ForEach(ClearActOfAcceptance);
        }

        private void ClearActOfAcceptance(WorkAndGoods entity)
        {
            entity.ActOfAcceptance = null;
            SaveOrUpdate(entity);
        }

        private WorkAndGoods Get(WorkAndGoodsArgs args)
        {
            var result = Repository.Get(q => q.Where(x => x.ObjectId == args.ObjectId && x.Type == args.Type));

            if (result == null)
            {
                throw new ArgumentException(
                    String.Format("Объект Работа/Товар ОУ с ObjectId = {0} и Type = {1} не найден в системе",
                        args.ObjectId, args.Type));
            }

            return result;
        }
    }
}
