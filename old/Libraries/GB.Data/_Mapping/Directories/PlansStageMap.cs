﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class PlansStageMap : ClassMapBase<PlansStage>
	{
        public PlansStageMap()
            : base("Plans_Stage")
		{
		}

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Stage_Name");
        }
    }
}
