﻿namespace bgTeam.IsRui.Common.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public static class EnumHelper
    {
        public static Dictionary<T, string> ToDictionary<T>()
            where T : struct
        {
            Type type = typeof(T);
            if (!type.IsEnum)
            {
                throw new ArgumentException("T must be enum");
            }

            Dictionary<T, string> list = new Dictionary<T, string>();
            Array enumValues = Enum.GetValues(type);

            foreach (T value in enumValues)
            {
                list.Add(value, GetDescription(value));
            }

            return list;
        }

        public static System.Collections.IList ToList<T>()
            where T : struct
        {
            Type type = typeof(T);
            if (!type.IsEnum)
            {
                throw new ArgumentException("T must be enum");
            }

            System.Collections.ArrayList list = new System.Collections.ArrayList();

            foreach (T value in Enum.GetValues(type))
            {
                list.Add(new KeyValuePair<int, string>((int)Enum.Parse(type, value.ToString()), GetDescription(value)));
            }

            return list;
        }

        public static string GetDescription<T>(T value)
            where T : struct
        {
            Type type = typeof(T);
            if (!type.IsEnum)
            {
                throw new ArgumentException("T must be enum");
            }

            string description = Enum.GetName(type, value);

            var fieldInfo = type.GetField(description);
            var attr = (DescriptionAttribute)Attribute.GetCustomAttribute(fieldInfo, typeof(DescriptionAttribute), false);
            if (attr != null)
            {
                description = attr.Description;
            }

            return description;
        }

        public static T Parse<T>(object id)
            where T : struct, IConvertible
        {
            Type type = typeof(T);
            if (!type.IsEnum)
            {
                throw new ArgumentException("T must be enum");
            }

            if (id == null)
            {
                throw new ArgumentNullException("Id");
            }

            if (id is T)
            {
                return (T)id;
            }

            return (T)Enum.Parse(type, id.ToString());
        }
    }
}