﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.SpecialChecks
{
    public class DeleteSpecialCheckContext : ICommandContext
    {
        public int? xId { get; set; }
    }
}
