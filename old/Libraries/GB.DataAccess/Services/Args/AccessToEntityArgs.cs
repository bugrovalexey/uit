﻿using GB.Data;

namespace GB.DataAccess.Services.Args
{
    public class AccessToEntityArgs : BaseArgsNew, IServiceArgs
    {
        public EntityType EntityType { get; set; }

        public bool AccessRead { get; set; }

        public bool AccessEdit { get; set; }

        public bool AccessDelete { get; set; }

        public bool AccessCreate { get; set; }

        public AccessActionEnum AccessMask
        {
            get
            {
                AccessActionEnum a = AccessActionEnum.None;
                if (AccessRead)
                    a |= AccessActionEnum.Read;
                if (AccessEdit)
                    a |= AccessActionEnum.Edit;
                if (AccessDelete)
                    a |= AccessActionEnum.Delete;
                if (AccessCreate)
                    a |= AccessActionEnum.Create;
                return a;
            }
        }

        public int RoleId { get; set; }

        public int? StatusId { get; set; }

        public string Note { get; set; }
    }
}