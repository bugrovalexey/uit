﻿using bgTeam.IsRui.Domain.Entities;

namespace bgTeam.IsRui.MKRF.Logic.Access
{
    // ToDo: Move this to proper place
    public interface IManagerAccess
    {
        bool CanRead<T>(T entity)
            where T : EntityBase, IWorkflowObject;

        bool CanEdit<T>(T entity)
            where T : EntityBase, IWorkflowObject;

        bool CanCreate<T>(T entity)
            where T : EntityBase, IWorkflowObject;

        bool CanDelete<T>(T entity)
            where T : EntityBase, IWorkflowObject;

        bool CanCreate(EntityType entityType);
    }
}
