﻿namespace GB.Data
{
    public class EntityNumerable : EntityBase
    {
        /// <summary>Номер</summary>
        public virtual int Num { get; set; }
    }
}