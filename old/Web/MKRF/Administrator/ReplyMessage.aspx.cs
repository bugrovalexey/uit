﻿using System;
using GB.Helpers;
using GB.MKRF.Entities.Admin;
using GB.MKRF.Repository;
using GB.Repository;

namespace GB.MKRF.Web.Administrator
{
    public partial class ReplyMessage : System.Web.UI.Page
    {
        public const string MessageIdKey = "Message";
        public static string GetUrl(int MsgId)
        {
            return UrlHelper.Resolve("~/Administrator/ReplyMessage.aspx") + "?" + MessageIdKey + "=" + MsgId.ToString();
        }

        UsersMsg currentMessageInternal = null;
        protected UsersMsg currentMessage
        {
            get
            {
                if (currentMessageInternal == null)
                {

                    int id = 0;
                    if (int.TryParse(Request.QueryString[MessageIdKey], out id))
                    {
                        currentMessageInternal = RepositoryBase<UsersMsg>.Get(id);
                    }

                    if (currentMessageInternal == null)
                    {
                        ResponseHelper.DenyAccess(Response);
                    }
                }
                return currentMessageInternal;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            toEmail.Text = currentMessage.User.Email;
            base.OnPreRender(e);
        }

        protected void callBack_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            string msg = string.Format(@"Уважаемый пользователь {0}!

В ответ на Ваше сообщение '{1}'
от {2}
отвечаю Вам следующее:

{3}

С уважением,
Администратор системы ИТ «Аудит»",
                currentMessage.User.FullName,
                currentMessage.Text,
                currentMessage.Date.ToString(),
                answerMemo.Text);

            currentMessage.AdminDate = DateTime.Now;
            currentMessage.AdminText = toEmail.Text + (string.IsNullOrEmpty(copyToEmail.Text) ? null : (" (" + copyToEmail.Text + ")")) + "\r\n" + answerMemo.Text;
            currentMessage.AdminUser = UserRepository.GetCurrent();

            RepositoryBase<UsersMsg>.SaveOrUpdate(currentMessage);

            EmailQueue queue = new EmailQueue();
            queue.From = "robot";
            queue.To = string.IsNullOrEmpty(toEmail.Text) ? null : toEmail.Text;
            queue.CopyTo = string.IsNullOrEmpty(copyToEmail.Text) ? null : copyToEmail.Text;
            queue.SheduledDate = DateTime.Now;
            queue.Text = msg;
            queue.Subject = "Ответ на Ваше сообщение";
            RepositoryBase<EmailQueue>.SaveOrUpdate(queue);

        }
    }
}