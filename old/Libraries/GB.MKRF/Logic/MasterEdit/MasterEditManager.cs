﻿using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.Directories;
using GB.Data.Extensions;
using GB.DataAccess.Repository;
using System;
using System.Collections.Generic;

namespace GB.MKRF.Logic.MasterEdit
{
    public class MasterEditManager : IManager
    {
        protected MasterEditManager() { }


        private readonly object syncRoot = new object();
        private readonly Dictionary<Type, Dictionary<MasterEditTypeEnum, IMasterEditHandler>> handlers = new Dictionary<Type, Dictionary<MasterEditTypeEnum, IMasterEditHandler>>();

        public IMasterEditHandler GetHandler<T>(T obj) where T : IWorkflowObject
        {
            IMasterEditHandler handler = null;
            lock (syncRoot)
            {
                Dictionary<MasterEditTypeEnum, IMasterEditHandler> dict;

                handlers.TryGetValue(typeof(T), out dict);

                if (dict != null)
                {
                    dict.TryGetValue(GetType(obj), out handler);
                }
            }
            return handler;
        }

        private MasterEditTypeEnum GetType(IWorkflowObject obj)
        {
            var ao = obj as AccountingObject;
            if (ao != null)
            {
                if (ao.Mode == AccountingObjectEnum.One)
                {
                    if (ao.Stage.IsEq(AccountingObjectsStageEnum.Planned) || ao.Id < 0)
                    {
                        if (ao.Kind.IsEq(IKTComponentEnum.Code10))
                            return MasterEditTypeEnum.AOPlannedSA;

                        return MasterEditTypeEnum.AOPlanned;
                    }
                    if (ao.Stage.IsEq(AccountingObjectsStageEnum.Creation))
                    {
                        return MasterEditTypeEnum.AOCreate;
                    }
                    if (ao.Stage.IsEq(AccountingObjectsStageEnum.Exploitation))
                    {
                        return MasterEditTypeEnum.AOExploitation;
                    }
                    //if (ao.Stage.IsEq(AccountingObjectsStageEnum.Evolution))
                    //{
                    //    if (ExistGovContracts(ao.Id, AccountingObjectsStageEnum.Evolution))
                    //    {
                    //        return MasterEditTypeEnum.AOEvolution;
                    //    }
                    //    else
                    //    {
                    //        return MasterEditTypeEnum.AOEvolutionWithoutContract;
                    //    }
                    //}
                    if (ao.Stage.IsEq(AccountingObjectsStageEnum.OutExploitation))
                    {
                        return MasterEditTypeEnum.AOOutExploitation;
                    }
                }
                else
                {
                    return MasterEditTypeEnum.AOMulti;
                }

                return MasterEditTypeEnum.None;
            }

            throw new Exception("Не тип используемого Handler");
        }

        //TODO: перенести в команды на чтение, когда они появятся
        internal void RegisterHandler<T>(IMasterEditHandler handler, MasterEditTypeEnum type) where T : IWorkflowObject
        {
            lock (syncRoot)
            {
                if (handlers.ContainsKey(typeof(T)))
                    handlers[typeof(T)][type] = handler;
                else
                {
                    var dict = new Dictionary<MasterEditTypeEnum, IMasterEditHandler>();

                    dict.Add(type, handler);

                    handlers[typeof(T)] = dict;
                }
            }
        }
    }

    enum MasterEditTypeEnum
    {
        None,
        AOPlanned,
        AOPlannedSA,
        AOCreate,
        //AOCreateWithoutContract,
        AOExploitation,
        //AOExploitationWithoutContract,
        AOEvolution,
        //AOEvolutionWithoutContract,
        AOOutExploitation,
        //AOOutExploitationWithoutContract,

        AOMulti,
    }
}
