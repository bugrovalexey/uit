﻿using bgTeam.IsRui.Domain.Entities;
using bgTeam.IsRui.Domain.Entities.Documents;
using bgTeam.IsRui.Domain.Entities.Plans;

namespace bgTeam.IsRui.Domain._Mapping.Plans
{
    internal class ReasonMap : ClassMapHiLo<Reason>
    {
        public ReasonMap()
            : base("Other_Reason")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Other_Reason_Name").Not.Nullable();
            Map(x => x.Num, "Other_Reason_Number").Not.Nullable();
            Map(x => x.SignerName, "Other_Reason_FIO").Nullable();
            Map(x => x.SignerPosition, "Other_Reason_Position").Nullable();
            Map(x => x.URL, "Other_Reason_URL").Nullable();

            References<PlansActivity>(x => x.Owner, "Project_ID").Not.Nullable();

            HasMany<Files>(x => x.Documents)
               .KeyColumns.Add("Document_Owner_ID")
               .Where("Doc_Type_ID = " + (int)DocTypeEnum.OtherReason)
               .Where("Portal_Table_ID = " + (int)EntityType.OtherReason)
               .Inverse();
        }
    }
}