﻿using GB.Data;
using GB.Data.AccountingObjects;
using GB.Data.AccountingObjects.WorksAndGoods;
using GB.Data.Admin;
using GB.Data.Common;
using GB.Data.Directories;
using GB.Data.Plans;
using GB.Data.Workflow;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;
using GB.DataAccess.Services.Args;
using GB.Extentions;
using System;

namespace GB.DataAccess
{
    /// <summary>
    /// Содержит методы преобразования Dto в Entity
    /// и наоборот
    /// </summary>
    public static class ConvertDtoToEntity
    {
        #region AccountingObject

        public static AccountingObject FillAccountingObjectFromDto(AccountingObject entity, IAccountingObjectControllerArgs dto)
        {
            entity.FullName = dto.FullName.ToStringOrDefault();
            entity.ShortName = dto.ShortName.ToStringOrDefault();
            entity.Type = GetEntity<IKTComponent>(dto.Type);
            entity.Kind = GetEntity<IKTComponent>(dto.Kind);
            return entity;
        }

        public static AccountingObject FillAccountingObjectFromDto(AccountingObject entity, AccountingObjectArgs dto)
        {
            entity.ShortName = dto.ShortName;
            return entity;
        }

        public static AccountingObject FillAccountingObjectFromDto(AccountingObject entity, AccountingObjectCommonDataArgs dto)
        {
            entity.Stage = dto.Stage;
            entity.Type = GetEntity<IKTComponent>(dto.TypeId);
            entity.Kind = GetEntity<IKTComponent>(dto.KindId);
            entity.Status = GetEntity<Status>(dto.StatusId);
            entity.FullName = dto.FullName;
            entity.Number = ApplyMaskForAccountingObjectNumber(dto.Number.ToStringOrDefault());
            entity.Targets = dto.Targets;
            entity.PurposeAndScope = dto.PurposeAndScope;
            entity.AdoptionBudgetDate = dto.AdoptionBudgetDate;
            entity.BK_KOSGU = GetEntity<ExpenditureItem>(dto.ExpenditureItemId);
            entity.BalanceCost = dto.BalanceCost;
            entity.AdoptionBudgetBasis = dto.AdoptionBudgetBasis;
            entity.CommissioningDate = dto.CommissioningDate;
            entity.DecommissioningDate = dto.DecommissioningDate;

            return entity;
        }

        public static AccountingObject FillAccountingObjectFromDto(AccountingObject entity, AccountingObjectResponsiblesArgs dto)
        {
            entity.ResponsibleDepartment = GetEntity<Department>(dto.DepartmentId);
            entity.ResponsibleCoord = GetEntity<Responsible>(dto.ResponsibleCoordId);
            entity.ResponsibleExploitation = GetEntity<Responsible>(dto.ResponsibleExploitationId);
            entity.ResponsiblePurchase = GetEntity<Responsible>(dto.ResponsiblePurchaseId);
            entity.ResponsibleInformation = GetEntity<Responsible>(dto.ResponsibleInformationId);
            entity.LocationOrgName = dto.LocationOrgName;
            entity.LocationFioResponsible = dto.LocationFioResponsible;
            entity.LocationOrgAddress = dto.LocationOrgAddress;
            entity.LocationOrgPhone = dto.LocationOrgPhone;

            return entity;
        }

        public static AccountingObject FillAccountingObjectFromDto(AccountingObject entity, AccountingObjectGoalsAndPurposeArgs dto)
        {
            entity.Targets = dto.Targets;
            entity.PurposeAndScope = dto.PurposeAndScope;

            return entity;
        }

        public static AccountingObject FillAccountingObjectFromDto(AccountingObject entity, AccountingObjectDataProtectionArgs dto)
        {
            entity.LevelOfSecurity = dto.LevelOfSecurity;

            return entity;
        }

        public static SpecialCheck FillSpecCheckFromDto(SpecialCheck entity,
            SpecialCheckArgs dto)
        {
            entity.AccountingObject = GetEntity<AccountingObject>(dto.AccountingObjectId);
            entity.Name = dto.Name.ToStringOrDefault();

            return entity;
        }

        public static AODecisionToCreate FillDocumentAccountingObjectFromDto(AODecisionToCreate entity,
            AODecisionToCreateArgs dto)
        {
            entity.AccountingObject = GetEntity<AccountingObject>(dto.AccountingObjectId);
            entity.Name = dto.Name;
            entity.Number = dto.Number;
            entity.DateAdoption = dto.DateAdoption;
            entity.ParticleOfDocument = dto.ParticleOfDocument;
            entity.Url = dto.Url;

            return entity;
        }

        public static InformationInteraction FillInformationInteractionFromDto(InformationInteraction entity,
            InformationInteractionArgs dto)
        {
            entity.AccountingObject = GetEntity<AccountingObject>(dto.AccountingObjectId);
            entity.Name = dto.Name;
            entity.Purpose = dto.Purpose;
            entity.Type = GetEntity<InformationInteractionType>(dto.TypeId);
            entity.SMEV = dto.SMEV;
            entity.Title = dto.Title;
            entity.Url = dto.Url;

            return entity;
        }
      

        public static AccountingObjectCharacteristics FillAccountingObjectCharacteristicsFromDto(
            AccountingObjectCharacteristics entity,
            AccountingObjectCharacteristicsArgs dto)
        {
            //entity.AccountingObject = dto.AccountingObject;
            entity.Name = dto.Name;
            entity.Norm = dto.Norm;
            entity.Max = dto.Max;
            entity.Fact = dto.Fact;
            entity.Type = GetEntity<AccountingObjectCharacteristicsType>(dto.TypeId);
            entity.Unit = GetEntity<Units>(dto.UnitId);
            entity.DateBeginning = dto.DateBeginning;
            entity.DateEnd = dto.DateEnd;
            //entity.IsRegister = dto.IsRegister;

            return entity;
        }

        public static void Fill(this AccountingObjectCharacteristics ao, IAccountingObjectCharacteristicsActivityArgs args)
        {
            ao.Name = args.Name;
            ao.Type = GetEntity<AccountingObjectCharacteristicsType>(args.Type);
            ao.Unit = GetEntity<Units>(args.Unit);
        }

        #region KindOfSupport

        public static void FillTechnicalSupportFromArgs(TechnicalSupport entity, TechnicalSupportArgs args)
        {
            entity.AccountingObject = GetEntity<AccountingObject>(args.AccountingObjectId);
            entity.Name = args.Name;
            entity.CategoryKindOfSupport = GetEntity<CategoryKindOfSupport>(args.CategoryKindOfSupportId);
            entity.Manufacturer = GetEntity<Manufacturer>(args.ManufacturerId);
            entity.Amount = args.Amount;
            entity.Summa = args.Summa;
        }

        public static void FillCharacteristicOfTechnicalSupportFromArgs(CharacteristicOfTechnicalSupport entity,
            CharacteristicOfTechnicalSupportArgs args)
        {
            entity.TechnicalSupport = GetEntity<TechnicalSupport>(args.TechnicalSupportId);
            entity.Name = args.Name;
            entity.Unit = GetEntity<Units>(args.UnitId);
            entity.Value = args.Value;
        }

        public static void FillSoftwareFromArgs(Software entity, SoftwareArgs args)
        {
            entity.AccountingObject = GetEntity<AccountingObject>(args.AccountingObjectId);
            entity.Name = args.Name;
            entity.CategoryKindOfSupport = GetEntity<CategoryKindOfSupport>(args.CategoryKindOfSupportId);
            entity.Manufacturer = GetEntity<Manufacturer>(args.ManufacturerId);
            entity.Amount = args.Amount;
            entity.Summa = args.Summa;
            entity.Right = args.Right;

        }

        #endregion KindOfSupport

        private static string ApplyMaskForAccountingObjectNumber(string number)
        {
            if (String.IsNullOrWhiteSpace(number) || number.Length != 9)
            {
                return number;
            }

            return number.Insert(2, ".");
        }

        #endregion AccountingObject

        #region PlansActivity

        public static PlansActivityMark FillPlansActivityMarkFromDto(PlansActivityMark entity, PlansActivityMarkArgs dto)
        {
            FillBaseIndicatorFromDto(entity, dto);
            entity.Justification = dto.Justification.ToStringOrDefault();
            entity.SourceIsStateProgram = dto.SourceIsStateProgram;

            if (entity.SourceIsStateProgram)
            {
                entity.SourceName = null;
                entity.ExplanationRelationshipWithStateProgramMark = dto.ExplanationRelationshipWithStateProgramMark;
                entity.StateProgramMark = GetEntity<StateProgramMark>(dto.StateProgramMarkId);
            }
            else
            {
                entity.SourceName = dto.SourceName;
                entity.ExplanationRelationshipWithStateProgramMark = null;
                entity.StateProgramMark = null;
            }

            return entity;
        }

        public static PlansActivityIndicator FillPlansActivityIndicatorFromDto(PlansActivityIndicator entity, PlansActivityIndicatorArgs dto)
        {
            FillBaseIndicatorFromDto(entity, dto);
            entity.AlgorithmOfCalculating = dto.AlgorithmOfCalculating.ToStringOrDefault();

            return entity;
        }

        private static void FillBaseIndicatorFromDto(BaseIndicator entity, BaseIndicatorArgs dto)
        {
            entity.PlansActivity = dto.PlansActivity ?? new Repository<PlansActivity>().Get(dto.PlansActivityId);
            entity.Name = dto.Name.ToStringOrDefault();
            entity.ExplanationRelationshipWithProjectMark = dto.ExplanationRelationshipWithProjectMark.ToStringOrDefault();
            entity.Unit = GetEntity<Units>(dto.UnitId);
            entity.BaseValue = dto.BaseValue.ToIntNull();
            entity.TargetedValue = dto.TargetedValue.ToIntNull();
            entity.DateToAchieveGoalValue = dto.DateToAchieveGoalValue.ToDateTimeNull();
        }

        #endregion PlansActivity

        #region Admin

        internal static void FillUser(this User entity, UserCardArgs args)
        {
            entity.Name = args.Name;
            entity.Middlename = args.Middlename;
            entity.Surname = args.Surname;
            entity.Job = args.Job;
            entity.Email = args.Email;
            entity.MobPhone = args.MobPhone;
            entity.WorkPhone = args.WorkPhone;
        }

        internal static void FillUser(this User entity, UserArgs args)
        {
            entity.IsActive = args.IsActive;
            entity.Login = args.Login;
            args.NewPassword.Do(psw => entity.Password = GB.Helpers.CryptoHelper.GetMD5Hash(psw));
            entity.Surname = args.Surname;
            entity.Name = args.Name;
            entity.Middlename = args.Middlename;
            entity.Department = GetEntity<Department>(args.DepartmentId);
            entity.Email = args.Email;
            entity.MobPhone = args.MobPhone;
            entity.WorkPhone = args.WorkPhone;
        }

        internal static void FillRole(this Role entity, RoleArgs args)
        {
            entity.Name = args.Name;
            entity.WebPage = args.WebPage;
        }

        internal static void FillSiteMap(this SiteMapNode entity, SiteMapArgs args)
        {
            entity.Title = args.Title;
            entity.Url = args.Url;
            entity.Controller = args.Controller;
            entity.Action = args.Action;
            entity.Parent = GetEntity<SiteMapNode>(args.ParentId);
            
        }

        internal static void FillSiteToRoleMap(this SiteMapToRole entity, SiteMapToRoleArgs args)
        {
            entity.SiteMap = GetEntity<SiteMapNode>(args.SiteMapId);
            entity.Role = GetEntity<Role>(args.RoleId);
        }

        #endregion Admin

        #region Common

        internal static T GetEntity<T>(object val) where T : EntityBase
        {
            int id;

            if (Int32.TryParse(Convert.ToString(val), out id))
            {
                return new Repository<T>().Get(id);
            }

            return null;
        }

        #endregion Common
    }
}