﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/AdminMaster.master" AutoEventWireup="true" CodeBehind="Messages.aspx.cs" Inherits="GB.MKRF.Web.Administrator.Messages" %>

<%@ Import Namespace="GB.MKRF.Web.Administrator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">

    <script type="text/javascript">
        function RefreshContent() {
            grid.Refresh();
        }
        $(function () {
            CreatePopup();
        });
        function CreatePopup() {
            $("a.popup").click(function (e) {
                e.preventDefault();
                var leftVal = (screen.width - 1000) / 2;
                var topVal = (screen.height - 600) / 2;

                if (leftVal < 0) leftVal = 0;
                if (topVal < 0) topVal = 0;

                var url = $(this).attr("href");
                window.open(url, "_blank", "height=600,width=1000,left=" + leftVal + ",top=" + topVal + "directories=0,location=0,menubar=0,status=0,toolbar=0,scrollbars=1,resizable=1");
            });
        }
        function hfSave() {
            hf.Set('status', StatusComboBox.GetValue());
        }
    </script>
    <dxhf:ASPxHiddenField runat="server" ID="hf" ClientInstanceName="hf" />
    <dxwgv:ASPxGridView runat="server" ID="grid" ClientInstanceName="grid" AutoGenerateColumns="False" Width="100%"
        KeyFieldName="Id" OnBeforePerformDataSelect="grid_BeforePerformDataSelect" OnRowUpdating="grid_RowUpdating"
        OnStartRowEditing="grid_StartRowEditing"
        OnCellEditorInitialize="grid_CellEditorInitialize">
        <Columns>
            <dxwgv:GridViewCommandColumn Caption=" ">
                <EditButton Visible="true" />
                <ClearFilterButton Visible="true" />
            </dxwgv:GridViewCommandColumn>
            <dxwgv:GridViewDataTextColumn Caption="Пользователь" FieldName="User">
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Дата/время" FieldName="Date">
                <Settings AllowAutoFilter="False" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Страница" FieldName="Url" >
                <Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Содержание" FieldName="Text">
                <Settings AllowAutoFilter="False" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Ответ администратора" FieldName="Response">
                <Settings AllowAutoFilter="False" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption="Статус" FieldName="Status">
                <Settings AllowHeaderFilter="True" />
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="7" Caption="ФИО" FieldName="UserName" Visible="False">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="8" Caption="Должность" 
                FieldName="Job" Visible="False">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="8" Caption="Департамент" 
                FieldName="Department" Visible="False">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="9" Caption="Раб. тел." 
                FieldName="Phone" Visible="False">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="10" Caption="Эл. почта" 
                FieldName="Email" Visible="False">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="11" Caption="Логин" 
                FieldName="Login" Visible="False">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn VisibleIndex="12" Caption="Роль" 
                FieldName="RoleName" Visible="False">
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn Caption=" ">
                <DataItemTemplate>
                    <a class="popup" href="<%#ReplyMessage.GetUrl((int)Eval("Id"))%>">Ответить</a>
                </DataItemTemplate>
            </dxwgv:GridViewDataTextColumn>
        </Columns>
        <Settings ShowFilterRow="True" />
        <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
        <Templates>
            <EditForm>
                <table class="form_edit">
                    <tr>
                        <td class="form_edit_desc">Дата/время
                        </td>
                        <td class="form_edit_input">
                            <%#Eval("Date") %>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Статус
                        </td>
                        <td class="form_edit_input">
                            <dxe:ASPxComboBox runat="server" ID="StatusComboBox" ValueField="Id" TextField="Name"
                                Width="500px" ClientInstanceName="StatusComboBox" ValueType="System.Int32"
                                OnDataBinding="StatusComboBox_DataBinding">
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Содержание
                        </td>
                        <td class="form_edit_input">
                            <%#Eval("Text") %>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Ответ администратора
                        </td>
                        <td class="form_edit_input">
                            <%#Eval("Response")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Департамент
                        </td>
                        <td class="form_edit_input">
                            <%#Eval("Department")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">ФИО
                        </td>
                        <td class="form_edit_input">
                            <%#Eval("UserName")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Должность
                        </td>
                        <td class="form_edit_input">
                            <%#Eval("Job")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Раб. тел.
                        </td>
                        <td class="form_edit_input">
                            <%#Eval("Phone")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Эл. почта
                        </td>
                        <td class="form_edit_input">
                            <%#Eval("Email")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Логин
                        </td>
                        <td class="form_edit_input">
                            <%#Eval("Login")%>                            
                        </td>
                    </tr>
                    <tr>
                        <td class="form_edit_desc">Роль
                        </td>
                        <td class="form_edit_input">
                            <%#Eval("RoleName")%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="form_edit_buttons">
                                <span class="ok"><dxwgv:ASPxGridViewTemplateReplacement ID="rp1" runat="server" ReplacementType="EditFormUpdateButton" /></span>
                                <span class="cancel"><dxwgv:ASPxGridViewTemplateReplacement ID="rp2" runat="server" ReplacementType="EditFormCancelButton" /></span>
                            </td>
                    </tr>
                </table>
            </EditForm>
        </Templates>
        <ClientSideEvents BeginCallback="function(s,e){ if (e.command=='UPDATEEDIT') hfSave(e); }" EndCallback="function(s,e){CreatePopup();}" />
    </dxwgv:ASPxGridView>
</asp:Content>
