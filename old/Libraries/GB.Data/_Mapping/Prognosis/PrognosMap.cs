﻿using FluentNHibernate;
using Uviri.MKRF.Entities.Admin;
using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Prognosis;
using Uviri.MKRF.Entities.Prognosis.SectionII;
using Uviri.MKRF.Entities.Prognosis.SelectionIV;
using Uviri.MKRF.Entities.Prognosis.SelectionV;

namespace Uviri.MKRF.Mapping.Prognosis
{
    public class PrognosMap : ClassMapBase<Prognos>
    {
        public PrognosMap()
            : base("Mon_Form")
        {
//            Table("w_");

//            Id(x => x.Id, "Mon_Form_ID").GeneratedBy.Custom(typeof(IdGenerator));

            

//            SqlInsert(@"exec ZZZ_Prc_Mon_Form_Insert
//                          @Mon_Form_Name = ?
//                         ,@Mon_Form_Addr = ?
//                         ,@Mon_Form_Phone = ?
//                         ,@Mon_Form_DT = ?
//                         ,@Mon_Form_Org_Chief_FIO = ?
//                         ,@Mon_Form_Org_Chief_Job = ?
//                         ,@Mon_Form_Org_Chief_Phone = ?
//                         ,@Mon_Form_Dep_Chief_FIO = ?
//                         ,@Mon_Form_Dep_Chief_Job = ?
//                         ,@Mon_Form_Dep_Chief_Phone = ?
//                         ,@Mon_Form_Responsible_FIO = ?
//                         ,@Mon_Form_Responsible_Job = ?
//                         ,@Mon_Form_Responsible_Phone = ?
//                         ,@Mon_Form_Deputy_FIO = ?
//                         ,@Mon_Form_Deputy_Job = ?
//                         ,@Mon_Form_Deputy_Phone = ?
//                         ,@Mon_Form_CA_Staff_Count = ?
//                         ,@Mon_Form_CA_Fact_Count = ?
//                         ,@Mon_Form_TP_Count = ?
//                         ,@Mon_Form_TP_Staff_Count = ?
//                         ,@Mon_Form_TP_Fact_Count = ?
//                         ,@Mon_Form_Realty_Count = ?
//                         ,@Year = ?
//                         ,@Govt_Organ_ID = ?
//                         ,@Users_ID = ?
//                         ,@Mon_Form_ID = ?").Check.None();

//            SqlUpdate(@"exec ZZZ_Prc_Mon_Form_Update
//                          @Mon_Form_Name = ?
//                         ,@Mon_Form_Addr = ?
//                         ,@Mon_Form_Phone = ?
//                         ,@Mon_Form_DT = ?
//                         ,@Mon_Form_Org_Chief_FIO = ?
//                         ,@Mon_Form_Org_Chief_Job = ?
//                         ,@Mon_Form_Org_Chief_Phone = ?
//                         ,@Mon_Form_Dep_Chief_FIO = ?
//                         ,@Mon_Form_Dep_Chief_Job = ?
//                         ,@Mon_Form_Dep_Chief_Phone = ?
//                         ,@Mon_Form_Responsible_FIO = ?
//                         ,@Mon_Form_Responsible_Job = ?
//                         ,@Mon_Form_Responsible_Phone = ?
//                         ,@Mon_Form_Deputy_FIO = ?
//                         ,@Mon_Form_Deputy_Job = ?
//                         ,@Mon_Form_Deputy_Phone = ?
//                         ,@Mon_Form_CA_Staff_Count = ?
//                         ,@Mon_Form_CA_Fact_Count = ?
//                         ,@Mon_Form_TP_Count = ?
//                         ,@Mon_Form_TP_Staff_Count = ?
//                         ,@Mon_Form_TP_Fact_Count = ?
//                         ,@Mon_Form_Realty_Count = ?
//                         ,@Year = ?
//                         ,@Govt_Organ_ID = ?
//                         ,@Users_ID = ?
//                         ,@Mon_Form_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Mon_Form_Delete
//						  @Mon_Form_ID = ?").Check.None();
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Mon_Form_Name");
            Map(x => x.Address, "Mon_Form_Addr");
            Map(x => x.Phone, "Mon_Form_Phone");
            Map(x => x.DateCompletion, "Mon_Form_DT");
            Map(x => x.Department_FIO, "Mon_Form_Org_Chief_FIO");
            Map(x => x.Department_Job, "Mon_Form_Org_Chief_Job");
            Map(x => x.Department_Phone, "Mon_Form_Org_Chief_Phone");
            Map(x => x.HeadStructural_FIO, "Mon_Form_Dep_Chief_FIO");
            Map(x => x.HeadStructural_Job, "Mon_Form_Dep_Chief_Job");
            Map(x => x.HeadStructural_Phone, "Mon_Form_Dep_Chief_Phone");
            Map(x => x.ResponsibleInfo_FIO, "Mon_Form_Responsible_FIO");
            Map(x => x.ResponsibleInfo_Job, "Mon_Form_Responsible_Job");
            Map(x => x.ResponsibleInfo_Phone, "Mon_Form_Responsible_Phone");
            Map(x => x.DeputyResponsibleInfo_FIO, "Mon_Form_Deputy_FIO");
            Map(x => x.DeputyResponsibleInfo_Job, "Mon_Form_Deputy_Job");
            Map(x => x.DeputyResponsibleInfo_Phone, "Mon_Form_Deputy_Phone");
            Map(x => x.StaffCountEmployeesCA, "Mon_Form_CA_Staff_Count");
            Map(x => x.ActualCountEmployeesCA, "Mon_Form_CA_Fact_Count");
            Map(x => x.CountTP, "Mon_Form_TP_Count");
            Map(x => x.StaffCountEmployeesTP, "Mon_Form_TP_Staff_Count");
            Map(x => x.ActualCountEmployeesTP, "Mon_Form_TP_Fact_Count");
            Map(x => x.CountProperties, "Mon_Form_Realty_Count");

            References<Years>(x => x.Year, "Year").Not.Nullable();
            References<Department>(x => x.Department, "Govt_Organ_ID").Not.Nullable();
            References<User>(x => x.User, "Users_ID").Not.Nullable();

            HasMany<TechInfo>(Reveal.Member<Prognos>("TechInfos"))
                .KeyColumns.Add("Mon_Form_ID")
                .Access.Field()
                .Inverse();

            HasMany<StaffingInfo>(Reveal.Member<Prognos>("StaffingInfos"))
                .KeyColumns.Add("Mon_Form_ID")
                .Access.Field()
                .Inverse();

            HasMany<FinanceInfo>(Reveal.Member<Prognos>("FinanceInfos"))
                .KeyColumns.Add("Mon_Form_ID")
                .Access.Field()
                .Inverse();
        }
    }
}
