﻿using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain._Mapping.Directories
{
    internal class ManufacturerMap : ClassMapHiLo<Manufacturer>
    {
        public ManufacturerMap()
            : base("Producer")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Name, "Producer_Name");
        }
    }
}