﻿namespace bgTeam.IsRui.WebApp
{
    using bgTeam.Core;
    using bgTeam.DataAccess;
    using bgTeam.IsRui.Common.Services;

    public interface IAppSettings
    {
    }

    public class AppSettings : IAppSettings, IConnectionSetting, IFileProviderRuiSetting
    {
        public AppSettings(IAppConfiguration appConfiguration)
        {
            ConnectionString = appConfiguration.GetConnectionString("ISRUIDB");

            TempFolderPhysicalPath = appConfiguration[nameof(TempFolderPhysicalPath)];

            FilesFolderPhysicalPath = appConfiguration[nameof(FilesFolderPhysicalPath)];
        }

        public string ConnectionString { get; set; }

        public string TempFolderPhysicalPath { get; set; }

        public string FilesFolderPhysicalPath { get; set; }
    }
}
