﻿using GB.Data.Directories;
using GB.Data.Plans;

namespace GB._Mapping.Plans
{
    internal class PlansActivityServiceMap : ClassMapHiLo<PlansActivityService>
    {
        public PlansActivityServiceMap()
            : base("Plans_Activity_Service") { }

        protected override void InitMap()
        {
            Map(x => x.Name, "Plans_Activity_Service_Name");
            Map(x => x.Year, "Plans_Activity_Service_Year").CustomType(typeof(YearEnum)).Not.Nullable();
            Map(x => x.Count, "Plans_Activity_Service_Count");
            Map(x => x.Cost, "Plans_Activity_Service_Price").Not.Nullable();

            References<ExpenditureItem>(x => x.KOSGU, "Expenditure_Items_ID");
            References<PlansActivity>(x => x.Activity, "Plans_Activity_ID");
        }
    }
}