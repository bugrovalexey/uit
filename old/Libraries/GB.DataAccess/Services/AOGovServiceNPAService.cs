﻿using AutoMapper;
using GB.Data.AccountingObjects;
using GB.Data.Frgu;
using GB.DataAccess.Services.Args;
using NHibernate.Util;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GB.DataAccess.Services
{
    public class AOGovServiceNPAService : ServiceBase<AOGovServiceNPA>, IService<AOGovServiceNPA, AOGovServiceNPAArgs>
    {
        public AOGovServiceNPA Create(AOGovServiceNPAArgs args)
        {
            var entity = new AOGovServiceNPA();
            return SaveOrUpdate(entity, args);
        }

        public AOGovServiceNPA Update(AOGovServiceNPAArgs args)
        {
            var entity = Repository.GetExisting(args.Id);
            return SaveOrUpdate(entity, args);
        }

        private AOGovServiceNPA SaveOrUpdate(AOGovServiceNPA entity, AOGovServiceNPAArgs args)
        {
            Mapper.Map(args, entity);
            SaveOrUpdate(entity);
            return entity;
        }

        public void SetAOGovServiceForAOGovServicesNPA(ICollection aoGovServicesNpaIds,
            AOGovService aoGovService)
        {
            var aoGovServicesNpa = Repository.GetAllEx(q => q.WhereRestrictionOn(x => x.Id).IsIn(aoGovServicesNpaIds));

            aoGovServicesNpa.ForEach(x =>
            {
                x.GovService = aoGovService;
                SaveOrUpdate(x);
            });
        }

        public void Delete(AOGovServiceNPAArgs args)
        {
            Delete(args.Id);
        }

        public void CopyGovServiceNPAInGovService(IList<GovServiceNPA> npas, int aoGovServiceId)
        {
            var npasIds = npas.Select(n => n.Id).ToList();
            var existAONpa = Repository.GetAllEx(q => q.Where(x => x.GovService.Id == aoGovServiceId));
            var existNpaIds = existAONpa.Where(x => x.GovServiceNpa != null).Select(x => x.GovServiceNpa.Id).ToList();

            var deleteNPA =
                existAONpa.Where(
                    x => x.GovServiceNpa == null || npasIds.Contains(x.GovServiceNpa.Id) == false).ToList();

            var addNPA =
                npas.Where(x => existNpaIds.Contains(x.Id) == false).ToList();

            deleteNPA.ForEach(Delete);

            addNPA.ForEach(npa =>
            {
                var entity = new AOGovServiceNPA();
                entity.Name = npa.Name;
                entity.GovServiceNpa = npa;
                entity.GovService = GetEntity<AOGovService>(aoGovServiceId);
                SaveOrUpdate(entity);
            });
        }
    }
}