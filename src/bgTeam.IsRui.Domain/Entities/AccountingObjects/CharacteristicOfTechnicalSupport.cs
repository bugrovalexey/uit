﻿using bgTeam.IsRui.Domain.Entities.AccountingObjects.WorksAndGoods;
using bgTeam.IsRui.Domain.Entities.Directories;

namespace bgTeam.IsRui.Domain.Entities.AccountingObjects
{
    /// <summary>
    /// Характеристики ТO
    /// </summary>
    public class CharacteristicOfTechnicalSupport : EntityBase
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public virtual Units Unit { get; set; }

        /// <summary>
        /// Техническое обеспечение ОУ
        /// </summary>
        public virtual TechnicalSupport TechnicalSupport { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        public virtual double? Value { get; set; }
    }
}