﻿namespace bgTeam.IsRui.Common.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface ICacheManager
    {
        bool TryGetValue<TKey, TValu>(TKey key, out TValu value);

        bool TrySetValue<Tkey, TValue>(Tkey key, TValue value);

        bool TrySetValue<Tkey, TValue>(Tkey key, TValue value, TimeSpan timeToExpire);
    }
}
