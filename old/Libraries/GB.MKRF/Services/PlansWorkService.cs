﻿using GB.Data.Directories;
using GB.Data.Plans;
using GB.MKRF.Helpers;
using GB.MKRF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using GB.MKRF.Services.Args;

namespace GB.MKRF.Services
{
    public class PlansWorkService : ServiceBase<PlansWork>, IService<PlansWork, PlansWorkArgs>
    {
        //[Obsolete]
        //public PlansWork Create(PlansWorkControllerArgs args)
        //{
        //    var work = new PlansWork();
        //    work.PlanActivity = args.ParentActivity;

        //    work.Num = work.PlanActivity.Works.Count > 0 ? NumHelper<PlansWork>.GetMinAvailableNum(work.PlanActivity.Works) : 1;

        //    work.OrigNum = GetNextNum(args.ParentActivity);

        //    return SaveOrUpdate(work, args.Values);
        //}

        //[Obsolete]
        //public PlansWork Update(PlansWorkControllerArgs args)
        //{
        //    return SaveOrUpdate(Repository.Get(args.PlansWorkId), args.Values);
        //}

        //private PlansWork SaveOrUpdate(PlansWork work, Dictionary<string, object> values)
        //{
        //    if (string.IsNullOrEmpty(values["Name"].ToString()))
        //        throw new ArgumentException("Укажите наименование");

        //    var edid = Convert.ToInt32(values["ExpenseDirectionId"]);
        //    if (edid == 0)
        //        throw new ArgumentException("Укажите Вид затрат");

        //    work.Name = values["Name"].ToString();
        //    work.OKDPText = values["OKDP"].ToString();
        //    work.OKPD = RepositoryBase<OKPD>.GetDefault();
        //    work.WorkForm = RepositoryBase<WorkForm>.GetDefault();

        //    work.ExpenditureItem = GetEntity<ExpenditureItem>(values["ExpenditureItemsId"]);

        //    ExpenseDirection ed = RepositoryBase<ExpenseDirection>.Get(edid);

        //    if (work.ExpenseDirection != null && work.ExpenseDirection.Id != ed.Id)
        //    {
        //        var items = RepositoryBase<WorkType>.GetAll(x => x.PlanWork == work);

        //        foreach (var item in items)
        //        {
        //            RepositoryBase<WorkType>.Delete(item);
        //        }
        //    }

        //    work.ExpenseDirection = ed;
        //    work.ExpenseType = RepositoryBase<ExpenseType>.GetDefault();

        //    work.SectionKBK = RepositoryBase<SectionKBK>.GetDefault();
        //    work.ExpenseItem = RepositoryBase<ExpenseItem>.GetDefault();
        //    work.GRBS = RepositoryBase<GRBS>.GetDefault();
        //    work.ApfInfo = RepositoryBase<ApfInfo>.GetDefault();
        //    //work.Status = StatusRepositoryOld.GetFirstStatus(work);

        //    base.SaveOrUpdate(work);

        //    foreach (KeyValuePair<string, object> value in values)
        //    {
        //        if (value.Key.StartsWith("ExpenseType_"))
        //        {
        //            var i = value.Key.Split('_')[1];
        //            WorkType wt = null;
        //            if (values.Keys.Contains("ExpenseTypeId_" + i))
        //            {
        //                wt = RepositoryBase<WorkType>.Get(Convert.ToInt32(values["ExpenseTypeId_" + i]));
        //            }

        //            if (wt == null) //при смене вида затрат удаляются все типы затрат
        //                wt = new WorkType(work);

        //            double volume = 0D;
        //            string val = values["ExpenseTypeVolume_" + i].ToString().Replace('.', ',');
        //            if (double.TryParse(val, out volume))
        //                SaveWorkType(wt, values, volume, Convert.ToInt32(values["ExpenseType_" + i]));
        //            else
        //                throw new Exception("Число имеет неправильный формат. (Например - 9 999,9999)");
        //        }
        //    }

        //    return work;
        //}

        //public void Delete(int id)
        //{
        //    base.Delete(id);
        //}

        //private void SaveWorkType(WorkType wt, Dictionary<string, object> values, double wty0 = 0D, int wttype = 0)
        //{
        //    double sum = wty0;
        //    if (wty0.Equals(0D) && values.Keys.Contains("WTY0"))
        //    {
        //        double.TryParse(values["WTY0"].ToString(), out sum);
        //    }

        //    wt.ExpenseVolumeYear0 = Math.Abs(sum);

        //    int expType = wttype;
        //    if (wttype == 0 && values.Keys.Contains("WTType"))
        //    {
        //        int.TryParse(values["WTType"].ToString(), out expType);
        //    }

        //    wt.ExpenseType = RepositoryBase<ExpenseType>.Get(expType);

        //    RepositoryBase<WorkType>.SaveOrUpdate(wt);

        //    var work = wt.PlanWork;

        //    // сохраняем сумму
        //    double summa = RepositoryBase<WorkType>.GetAll(x => x.PlanWork == work).Sum(x => x.ExpenseVolumeYear0);
        //    work.ExpenseVolumeCurrentYear = summa;
        //    base.SaveOrUpdate(work);
        //}

        //public void DeleteWorkType(int id)
        //{
        //    WorkType wt = RepositoryBase<WorkType>.Get(id);
        //    PlansWork work = wt.PlanWork;
        //    RepositoryBase<WorkType>.Delete(wt);

        //    // сохраняем сумму
        //    double summa = RepositoryBase<WorkType>.GetAll(x => x.PlanWork == work).Sum(x => x.ExpenseVolumeYear0);
        //    wt.PlanWork.ExpenseVolumeCurrentYear = summa;
        //    base.SaveOrUpdate(wt.PlanWork);
        //}

        private int GetNextNum(PlansActivity activity)
        {
            if (activity.Works.Count == 0)
                return 1;

            return activity.Works.OrderByDescending(i => i.OrigNum).First().OrigNum + 1;
        }

        //public Dictionary<int, bool> GetExpenseError(PlansActivity activity)
        //{
        //    Dictionary<int, decimal> listExp = new Dictionary<int, decimal>();

        //    foreach (var item in activity.ExpenseDirections)
        //    {
        //        int id = item.ExpenseDirection.Id;

        //        if (listExp.ContainsKey(id))
        //            listExp[id] += item.ExpenseVolY0;
        //        else
        //            listExp.Add(id, item.ExpenseVolY0);
        //    }

        //    Dictionary<int, decimal> listWork = new Dictionary<int, decimal>();

        //    foreach (System.Data.DataRow item in WorksRepository.GetWorksList(activity).Rows)
        //    {
        //        int id = (int)item["ExpenseDirectionID"];

        //        if (listWork.ContainsKey(id))
        //            listWork[id] += (decimal)item["ExpenseVolumeYear0"];
        //        else
        //            listWork.Add(id, (decimal)item["ExpenseVolumeYear0"]);
        //    }

        //    Dictionary<int, bool> list = new Dictionary<int, bool>();

        //    foreach (var item in listExp)
        //    {
        //        if (listWork.ContainsKey(item.Key))
        //        {
        //            if (listWork[item.Key] > item.Value)
        //            {
        //                list.Add(item.Key, true);
        //            }
        //        }
        //    }

        //    return list;
        //}

        //public IList<ExpenseType> GetExpenseTypes(int expenseDirectionId)
        //{
        //    return RepositoryBase<ExpenseType>.GetAll(x => x.ExpenseDirection.Id == expenseDirectionId || x.ExpenseDirection.Id == EntityBase.Default);
        //}

        //public ExpenseDirections GetExpenseDirections(PlansActivity activity)
        //{
        //    Dictionary<int, string> listStr = new Dictionary<int, string>();

        //    var ignoreED = new[] { "12", "2" };

        //    foreach (var item in activity.ExpenseDirections.OrderBy(x => x.ExpenseDirection.Code))
        //    {
        //        if (ignoreED.Contains(item.ExpenseDirection.Code))
        //            continue;

        //        if (!listStr.ContainsKey(item.ExpenseDirection.Id))
        //            listStr.Add(item.ExpenseDirection.Id, item.ExpenseDirection.CodeName);
        //    }

        //    if (!listStr.ContainsKey(0))
        //        listStr.Add(0, "Не задано");

        //    if (CurrentEdit != null)
        //        if (!listStr.ContainsKey(CurrentEdit.ExpenseDirection.Id))
        //            listStr.Add(CurrentEdit.ExpenseDirection.Id, CurrentEdit.ExpenseDirection.CodeName);

        //    return new ExpenseDirections
        //        {
        //            List = listStr,
        //            Selected = CurrentEdit != null ? CurrentEdit.ExpenseDirection.Id : EntityBase.Default
        //        };
        //}

        //public void CopyWork(object id)
        //{
        //    CopyHelper.CopyWork(null, Repository.Get(id));
        //}

        //public DataTable GetWorkList(PlansActivity activity)
        //{
        //    return WorksRepository.GetWorksList(activity, true);
        //}

        #region ReturnPackages

        public class ExpenseDirections
        {
            public Dictionary<int, string> List;
            public int Selected;
        }

        #endregion ReturnPackages

        public PlansWork Create(PlansWorkArgs args)
        {
            var work = new PlansWork();
            work.PlanActivity = args.Activity ?? GetEntity<PlansActivity>(args.ActivityId);

            work.Num = work.PlanActivity.Works.Count > 0 ? NumHelper<PlansWork>.GetMinAvailableNum(work.PlanActivity.Works) : 1;
            work.OrigNum = GetNextNum(work.PlanActivity);

            return SaveOrUpdate(work, args);
        }

        public PlansWork Update(PlansWorkArgs args)
        {
            return SaveOrUpdate(Repository.Get(args.Id), args);
        }

        private PlansWork SaveOrUpdate(PlansWork work, PlansWorkArgs args)
        {
            if (string.IsNullOrEmpty(args.Name))
                throw new ArgumentException("Укажите наименование");

            var edid = Convert.ToInt32(args.ExpenseDirection);
            if (edid == 0)
                throw new ArgumentException("Укажите Вид затрат");

            work.Name = args.Name;
            //work.OKDPText = values["OKDP"].ToString();

            //Неизвестные, но обязательные поля
            work.WorkForm = new Repository<WorkForm>().GetDefault();
            work.GRBS = new Repository<GRBS>().GetDefault();
            work.SectionKBK = new Repository<SectionKBK>().GetDefault();
            //work.ApfInfo = new Repository<ApfInfo>().GetDefault();
            work.ExpenseItem = new Repository<ExpenseItem>().GetDefault();
            //work.Status = StatusRepositoryOld.GetFirstStatus(work);

            work.OKPD = GetEntity<OKPD>(args.OKPD);
            work.ExpenseDirection = GetEntity<ExpenseDirection>(edid);
            work.ExpenseType = GetEntity<ExpenseType>(args.ExpenseType);
            work.ExpenditureItem = GetEntity<ExpenditureItem>(args.KOSGU);
            work.OKVED = GetEntity<OKVED>(args.OKVED);

            work.Year = (YearEnum)Convert.ToInt32(args.Year);
            work.SpecialistsCount = args.SpecCount;
            work.Salary = args.Salary;
            work.Duration = args.Duration;

            base.SaveOrUpdate(work);

            return work;
        }

        public void Delete(PlansWorkArgs args)
        {
            base.Delete(args.Id);
        }
    }

    [Obsolete]
    public class PlansWorkControllerArgs
    {
        public object PlansWorkId { get; set; }

        public Dictionary<string, object> Values { get; set; }

        public PlansActivity ParentActivity { get; set; }
    }

    public class PlansWorkArgs : BaseArgsNew
    {
        public string Name { get; set; }

        public int OKPD { get; set; }

        public int ExpenseDirection { get; set; }

        public int ExpenseType { get; set; }

        public int KOSGU { get; set; }

        public int OKVED { get; set; }

        public YearEnum Year { get; set; }

        public int SpecCount { get; set; }

        public int Duration { get; set; }

        public decimal Salary { get; set; }

        public PlansActivity Activity { get; set; }

        public int ActivityId { get; set; }
    }
}