﻿using System.ComponentModel;
using System.Web.Mvc;
using bgTeam.IsRui.Domain.Entities.AccountingObjects;
using bgTeam.IsRui.DataAccess.Repository;
using bgTeam.Extensions;
using System.Collections.Generic;
using System.Linq;
using bgTeam.IsRui.Common.Helpers;

namespace bgTeam.IsRui.WebApp.ViewModels.AccountingObjects
{
    public class CharacteristicsViewModel : AccountingObjectBase
    {
        #region consts

        private const string LevelOfSecurityCaption = "Уровень защищенности";

        #endregion consts

        public CharacteristicsViewModel(int id)
            : base(id, ao => ao.SpecialChecks)
        {
            LevelsOfSecurity = new SelectList(EnumHelper.ToDictionary<LevelOfSecurity>(), "Key", "Value");

            Characteristics = new AccountingObjectCharacteristicsRepository()
                .GetAllByAccountingObject(id).Select(x => new
                TableCharacteristic(x));

            SpecChecks = Current.SpecialChecks.Select(x => new TableSpecCheck(x));
        }


        [DisplayName(LevelOfSecurityCaption)]
        public LevelOfSecurity? LevelOfSecurity
        {
            get { return Current.Return(ao => ao.LevelOfSecurity, default(LevelOfSecurity?)); }
        }

        public SelectList LevelsOfSecurity { get; set; }

        public IEnumerable<TableCharacteristic> Characteristics { get; set; }

        public IEnumerable<TableSpecCheck> SpecChecks { get; set; }



    }
}