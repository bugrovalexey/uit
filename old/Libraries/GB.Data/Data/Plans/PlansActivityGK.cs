﻿using System;

namespace Uviri.MKRF.Entities.Plans
{
    /// <summary>Реквизиты ГК</summary>
    public class PlansActivityGK : EntityBase
    {
        public PlansActivityGK()
        {

        }

        public PlansActivityGK(PlansActivity activity)
        {
            this.Activity = activity;
        }
        public virtual PlansActivity Activity { get; set; }

        /// <summary>Дата</summary>
        public virtual DateTime? Date { get; set; }

        /// <summary>Номер</summary>
        public virtual string Num { get; set; }

        /// <summary>Предмет</summary>
        public virtual string Subject { get; set; }

        /// <summary>Год</summary>
        public virtual Int16 Year { get; set; }
    }
}
