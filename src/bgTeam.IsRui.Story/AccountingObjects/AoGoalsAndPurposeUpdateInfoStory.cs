﻿namespace bgTeam.IsRui.Story.AccountingObjects
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AoGoalsAndPurposeUpdateInfoStory : IStory<AoGoalsAndPurposeUpdateInfoStoryContext, bool>
    {
        private readonly ISessionNHibernateFactory _factory;
        private readonly IRepositoryRui _repository;

        public AoGoalsAndPurposeUpdateInfoStory(ISessionNHibernateFactory factory, IRepositoryRui repository)
        {
            _factory = factory;
            _repository = repository;
        }

        public bool Execute(AoGoalsAndPurposeUpdateInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<bool> ExecuteAsync(AoGoalsAndPurposeUpdateInfoStoryContext context)
        {
            context.AoId.CheckNull(nameof(context.AoId));

            bool result;

            AccountingObject ao = _repository.Get<AccountingObject>(context.AoId);

            ao.Targets = context.Targets;
            ao.PurposeAndScope = context.PurposeAndScope;

            using (var session = _factory.OpenSession())
            {
                try
                {
                    await session.SaveOrUpdateAsync(ao);

                    await session.CommitAsync();

                    result = true;
                }
                catch (Exception)
                {
                    await session.RollbackAsync();

                    throw;
                }
            }

            return result;
        }
    }
}
