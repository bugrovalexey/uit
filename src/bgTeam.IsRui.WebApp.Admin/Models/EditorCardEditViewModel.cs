﻿namespace bgTeam.IsRui.WebApp.Admin.Models
{
    using bgTeam.IsRui.WebApp.Admin.Domain;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    public class EditorCardEditViewModel
    {
        public int DictId { get; private set; }
        public IDictionary<string, object> Data { get; private set; }
        public IEnumerable<MetaColumn> Columns { get; private set; }
        public bool ReadOnly { get; private set; }

        public EditorCardEditViewModel(MetaDictionary dict, IEnumerable<MetaColumn> columns, dynamic data = null)
        {
            DictId = dict.Id;
            ReadOnly = dict.ReadOnly;
            Columns = columns;

            if (data != null)
            {
                Data = new TableRowDataView(data).Values;
            }
            else
            {
                Data = new Dictionary<string, object>();
                foreach (var item in columns)
                {
                    Data.Add(item.ColumnName, null);
                }
            }
        }
    }
}