﻿namespace bgTeam.IsRui.Domain.Entities.Documents
{
    using System;
    using bgTeam.IsRui.Domain.Entities.Common;

    /// <summary>Документ</summary>
    public class Files : EntityBase, IEntityName
    {
        //protected Files()
        //{
        //}

        //public Files(DocTypeEnum documentType, IEntity owner = null)
        //{
        //    this.Type = documentType;
        //    this.EntityOwner = owner;
        //}

        public virtual int EntityType { get; set; }

        /// <summary>Родительский объект</summary>
        public virtual IEntity EntityOwner { get; set; }

        /// <summary>Тип документа</summary>
        public virtual DocTypeEnum Type { get; set; }

        /// <summary>Вид документа</summary>
        public virtual DocKindEnum Kind { get; set; }

        /// <summary>Наименование</summary>
        public virtual string Name { get; set; }

        /// <summary>Тип контента</summary>
        public virtual string Extension { get; set; }

        public virtual int? Size { get; set; }

        public virtual string Url { get; set; }

        public virtual string MimeType { get; set; }


        ///// <summary>Тип документа (строковое поле)</summary>
        //public virtual string DocTypeText { get; set; }

        ///// <summary>Номер</summary>
        //public virtual string Num { get; set; }//null

        ///// <summary>Дата</summary>
        //public virtual DateTime? Date { get; set; }

        

        ///// <summary>Файл</summary>
        ////public virtual Byte[] File { get; set; }//null

        ///// <summary>Имя файла</summary>
        //public virtual string FileName { get; set; }//null

        ///// <summary>Тип контента</summary>
        //public virtual string ContentType { get; set; }//null

        ///// <summary>Комментарий</summary>
        //public virtual string Comm { get; set; }//null

        ///// <summary>Автор</summary>
        //public virtual User Author { get; set; }

        ///// <summary>Дата создания</summary>
        //public virtual DateTime? DateCreate { get; set; }

        //public virtual bool IsSigned { get; protected set; }

        ///// <summary>Подписанный пакет(что подписывали)</summary>
        ////public virtual Byte[] SignedPackage { get; set; }//null

        ///// <summary>Информация о сертификате(ЭЦП)</summary>
        //public virtual string SignInfo { get; set; }

        ///// <summary>Тип подписи(частное/должностное лицо)</summary>
        //public virtual int? SignType { get; set; }

        ////public virtual DocGroupEnum DocGroup { get; set; }

        ///// <summary> Решение прав комиссии</summary>
        //public virtual bool DecisionGovComission { get; set; }

        ///// <summary> Решение прав комиссии</summary>
        //public virtual int EntityType { get; protected set; }
    }

    public enum DocKindEnum
    {
        File = 0,

        GoogleDrive = 1
    }
}