﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.ActsOfAcceptance
{
    public class DeleteActOfAcceptanceContext : ICommandContext
    {
        public int? xId { get; set; }
    }
}
