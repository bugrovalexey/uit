﻿using GB.Data.AccountingObjects.WorksAndGoods;
using GB.Data.Directories;
using GB.DataAccess.Command.AccountingObjects.WorkAndGood;
using GB.DataAccess.Repository;
using GB.DataAccess.Services;
using GB.DataAccess.Services.Args;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GB.DataAccess.Command.AccountingObjects.WorkAndServices
{
    public class AddOrSaveWorkAndServiceCommand : ICommand<AddOrSaveWorkAndServiceContext, WorkAndService>
    {
        ISessionFactoryGb factory;

        public AddOrSaveWorkAndServiceCommand()
        {
            factory = new SessionFactory();
        }

        public WorkAndService Execute(AddOrSaveWorkAndServiceContext context)
        {
            WorkAndService workAndServ;

            if (context.xId.HasValue && context.xId != 0)
                workAndServ = new Repository<WorkAndService>().Get(context.xId.Value);
            else
            {
                workAndServ = new WorkAndService();                
            }

            AutoMapper.Mapper.Map(context, workAndServ);
            
            using (var session = factory.Create())
            {
                session.SaveOrUpdate(workAndServ);
                session.Commit();
            }            

            new AddOrSaveWorkAndGoodsCommand().Execute(new AddOrSaveWorkAndGoodsContext()
            {
                Type = CategoryTypeOfSupportEnum.WorksAndServices,
                AccountingObjectId = context.AccountingObjectId,
                CategoryKindOfSupportId = context.CategoryKindOfSupportId,
                Name = context.Name,
                ObjectId = workAndServ.Id,
                Summa = context.Summa,
                IsNew = !context.xId.HasValue || context.xId == 0
            });

            return workAndServ;
        }
    }
}
