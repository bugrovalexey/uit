﻿namespace bgTeam.IsRui.Story.Activities
{
    using bgTeam.Extensions;
    using bgTeam.IsRui.DataAccess.Repository;
    using bgTeam.IsRui.Domain.Dto;
    using bgTeam.IsRui.Domain.Entities.AccountingObjects;
    using bgTeam.IsRui.Domain.Entities.Common;
    using bgTeam.IsRui.Domain.Entities.Plans;
    using bgTeam.IsRui.Story.Common;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class GetObjectInfoStory : IStory<GetObjectInfoStoryContext, ActivityInfoDto>
    {
        private readonly IRepositoryRui _repositoryRui;
        private readonly IUserRepository _userRepository;
        private readonly IMapperBase _mapper;
        private readonly IStory<ActivityGetActsStoryContext, IEnumerable<ActivityActDto>> _actStory;

        public GetObjectInfoStory(
            IRepositoryRui repositoryRui,
            IUserRepository userRepository,
            IMapperBase mapperBase,
            IStory<ActivityGetActsStoryContext, IEnumerable<ActivityActDto>> actStory)
        {
            _repositoryRui = repositoryRui;
            _userRepository = userRepository;
            _mapper = mapperBase;
            _actStory = actStory;
        }

        public ActivityInfoDto Execute(GetObjectInfoStoryContext context)
        {
            return ExecuteAsync(context).Result;
        }

        public async Task<ActivityInfoDto> ExecuteAsync(GetObjectInfoStoryContext context)
        {
            context.Id.CheckNull(nameof(context.Id));

            var dto = new ActivityInfoDto();

            dto.Id = context.Id;

            var activity = _repositoryRui.Get<PlansActivity>(context.Id);

            if (activity.ActivityType2 != null)
            {
                dto.TypeId = activity.ActivityType2.Id;

                dto.TypeName = activity.ActivityType2.Name;
            }

            if (activity.Workflow != null)
            {
                dto.StatusId = activity.Workflow.Status.Id;

                dto.StatusName = activity.Workflow.Status.Name;
            }

            dto.Name = activity.Name;

            dto.Year = activity.Year;

            dto.Description = activity.Description;

            dto.ResponsibleDepartmentId = activity.Department.Id;
            dto.ResponsibleDepartmentName = activity.Department.Name;

            //if (activity.ResponsibleInformation != null)
            //{
            //    dto.ResponsibleInformationId = activity.ResponsibleInformation.Id;

            //    dto.ResponsibleInformationName = activity.ResponsibleInformation.Name;
            //}

            if (activity.Signing != null)
            {
                dto.SigningId = activity.Signing.Id;

                dto.SigningName = activity.Signing.Name;
            }

            dto.AoLinkId = activity.AccountingObject?.Id;
            dto.AoLinkName = activity.AccountingObject?.ShortName;

            dto.Documents = _repositoryRui.GetAllEx<ActivityDocuments>(
                q => q.Where(x => x.Activity.Id == context.Id))
                .Select(x => _mapper.Map(x, new ActivityDocumentDto()));

            dto.Cost = _mapper.Map(_repositoryRui.Get<PlansActivity>(context.Id), new ActivityCostDto());

            dto.Services = _repositoryRui.GetAllEx<PlansActivityService>(
                q => q.Where(x => x.Activity.Id == context.Id))
                .Select(x => _mapper.Map(x, new ActivitiesServiceDto()));

            dto.Indicator = _repositoryRui.GetAllEx<PlansActivityIndicator>(
                q => q.Where(x => x.PlansActivity.Id == context.Id))
                .Select(x => _mapper.Map(x, new ActivityIndicatorDto()));

            dto.Marks = _repositoryRui.GetAllEx<PlansActivityMark>(
               q => q.Where(x => x.PlansActivity.Id == context.Id))
               .Select(x => _mapper.Map(x, new ActivitysMarksDto()));

            dto.Works = _repositoryRui.GetAllEx<ActivityWorks>(
                q => q.Where(x => x.PlansActivityId == context.Id))
                .Select(x => _mapper.Map(x, new ActivityWorksGetDto()));

            dto.Goods = _repositoryRui.GetAllEx<ActivityGoods>(
                q => q.Where(x => x.PlansActivityId == context.Id))
                .Select(x => _mapper.Map(x, new ActivityGoodsGetDto()));

            dto.Contracts = _repositoryRui.GetAllEx<ActivityContract>(q =>
                q.Where(x => x.PlansActivity.Id == context.Id.Value))
                .Select(x => _mapper.Map(x, new ActivityContractDto()));

            dto.Purchases = _repositoryRui.GetAllEx<ActivityPurchase>(q =>
                q.Where(x => x.Activity.Id == context.Id.Value))
                .Select(x => _mapper.Map(x, new ActivityPurchaseDto()));

            dto.InformationInteractions = _repositoryRui.GetAllEx<InformationInteraction>(q =>
                q.Where(x => x.PlansActivity.Id == context.Id.Value))
                .Select(x => _mapper.Map(x, new InformationInteractionDto()));

            dto.Acts = await _actStory.ExecuteAsync(new ActivityGetActsStoryContext
            {
                ActivityId = context.Id.Value
            });

            if (activity.Workflow != null)
            {
                var curUser = _userRepository.GetCurrent();

                var conc = _repositoryRui.Get<WorkflowUserConclusion>(q => q
                    .Where(x => x.WorkflowId == activity.Workflow.Id &&
                        x.StatusId == activity.Workflow.Status.Id &&
                        x.UserId == curUser.Id));

                dto.WorkflowId = activity.Workflow.Id;

                dto.Conclusion = _mapper.Map(conc, new WorkflowUserConclusionDto());

                var statusGetNext = new WorkflowStatusGetNextListStory(_mapper, _repositoryRui, _userRepository);
                dto.StatusGetNext = statusGetNext.Execute(new WorkflowStatusGetNextListStoryContext
                {
                    WorkflowId = activity.Workflow.Id
                });
            }

            return dto;
        }
    }
}
