﻿using Uviri.MKRF.Entities.Directories;
using Uviri.MKRF.Entities.Plans;

namespace Uviri.MKRF.Mapping.Plans
{
    public class ManagementStructureMap : ClassMapBase<ManagementStructure>
    {
        public ManagementStructureMap()
            : base("Gov_Struct")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Element, "Gov_Struct_Element");
            Map(x => x.Period, "Gov_Struct_Period");
            Map(x => x.Comment, "Gov_Struct_Comment");
            Map(x => x.ManagerLastName, "Gov_Struct_Manager_Last_Name");
            Map(x => x.ManagerFirstName, "Gov_Struct_Manager_First_Name");
            Map(x => x.ManagerSurName, "Gov_Struct_Manager_Surname");
            Map(x => x.ManagerPost, "Gov_Struct_Manager_Post");


            References<Plan>(x => x.Plan, "Plans_ID").Not.Nullable();
            //References<ManagementLevel>(x => x.Level, "Gov_Struct_Lvl_ID");

            //Map(x => x.Manager, "Gov_Struct_Manager");
            //Map(x => x.Role, "Gov_Struct_Role");
            //Map(x => x.RoleFunction, "Gov_Struct_Role_Func");
        }
    }
}