﻿using GB.Data;
using GB.MKRF.Infrastructure;
using GB.MKRF.Providers;
using GB.Patterns;
using NHibernate;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace GB.MKRF.Repository
{
    /// <summary>
    /// Общий репозиторий
    /// </summary>
    /// <typeparam name="T">Тип объекта</typeparam>
    public class RepositorySt
    {
        protected const int CommandTimeout = 300;

        protected internal static NHibernate.ISession Session
        {
            get { return NHSession.CurrentSession; }
        }

        /// <summary>
        /// Выполнить sql запрос через NHibernate
        /// </summary>
        /// <typeparam name="TOutput"></typeparam>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static IList<TOutput> ExecuteSqlToObject<TOutput>(string sql, Action<IQuery> query = null)
        {
            IQuery _query = Session.CreateSQLQuery(sql);

            if (query != null)
                query(_query);

            return _query
                .SetResultTransformer(Transformers.AliasToBean<TOutput>())
                .List<TOutput>();
        }

        /// <summary>
        /// Получает DataTable по запросу с параметрами
        /// </summary>
        /// <param name="sql">SQL запрос</param>
        /// <param name="pars">Параметры запроса</param>
        /// <param name="tableName">Наименование DataTable который вернётся.</param>
        /// <param name="primaryKey">ColumnName в качестве primaryKey, если нужно указать</param>
        /// <returns></returns>
        internal static DataTable GetTableViaSQL(string sql, Dictionary<string, object> pars = null, string tableName = null, string primaryKey = null)
        {
            DataTable dt = new DataTable();

            if (!string.IsNullOrEmpty(tableName))
                dt.TableName = tableName;

            SqlCommand command = new SqlCommand(sql, (SqlConnection)Session.Connection);
            command.CommandTimeout = CommandTimeout;
            Session.Transaction.Enlist(command);
            SqlDataAdapter adapter = new SqlDataAdapter(command);

            if (pars != null)
            {
                foreach (string parameterName in pars.Keys)
                {
                    IDbDataParameter parameter = command.CreateParameter();
                    parameter.ParameterName = parameterName;
                    parameter.Value = pars[parameterName];
                    command.Parameters.Add(parameter);
                }
            }

            adapter.Fill(dt);

            if (!string.IsNullOrEmpty(primaryKey))
                dt.PrimaryKey = new DataColumn[] { dt.Columns[primaryKey] };

            return dt;
        }
    }

    /// <summary>
    /// Общий репозиторий
    /// </summary>
    /// <typeparam name="T">Тип объекта</typeparam>
    public class Repository<T> : RepositorySt where T : EntityBase
    {
        /// <summary>
        /// Возвращает объект по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Объект</returns>
        public T Get(object id)
        {
            if (id == null)
                return null;
            else if (id is double)
                id = Convert.ToInt32(id);
            else if (id is Enum)
                id = (int)id;
            else if (id is string)
            {
                int value = 0;
                if (!int.TryParse(id.ToString(), out value))
                    return null;
                id = value;
            }

            return Session.Get<T>(id);
        }

        public T Get(Action<IQueryOver<T, T>> query)
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.SingleOrDefault();
        }

        /// <summary>
        /// Возвращает существующую запись
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Запись, либо ArgumentException, если ее неудалось найти</returns>
        public T GetExisting(object id)
        {
            var entity = Get(id);
            if (entity == null)
                throw new ArgumentException(String.Format("Объект <{0}> не найден в системе", id));

            return entity;
        }

        /// <summary>
        /// Возвращает существующую запись с правами доступа
        /// </summary>
        public T GetExistingWithAccess(object id)
        {
            var entity = GetExisting(id);
            if (entity is IWorkflowObject)
                Singleton<AccessEntityProvider>.Instance.SetAccess((IWorkflowObject)entity);

            return entity;
        }

        public T GetDefault()
        {
            return Session.Get<T>(EntityBase.Default);
        }

        public T GetOrDefault(object id)
        {
            T ob = Get(id);

            if (ob == null)
                return GetDefault();

            return ob;
        }

        #region GetAll

        /// <summary>
        /// Возвращает все объекты
        /// </summary>
        /// <returns>Список объектов</returns>
        public IList<T> GetAll()
        {
            return Session.CreateCriteria(typeof(T)).List<T>();
        }

        /// <summary>
        /// Возвращает все объекты
        /// </summary>
        /// <returns>Список объектов</returns>
        public IList<T> GetAllEx(Action<IQueryOver<T, T>> query)
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.List<T>();
        }

        /// <summary>
        /// Можно вытаскивать типы отличные от Root типа
        /// (конкретное поле, несколько полей, другой объект, используя TransformUsing)
        /// </summary>
        /// <typeparam name="TReturn">Возвращаемый тип</typeparam>
        /// <param name="query">Запрос</param>
        /// <returns>Список объектов возращаемых запросом</returns>
        public IList<TReturn> Select<TReturn>(Action<IQueryOver<T, T>> query)
        {
            IQueryOver<T, T> _query = Session.QueryOver<T>();

            query(_query);

            return _query.List<TReturn>();
        }


        [Obsolete("Надо избавляться от вызовов этого метода, он провоцирует неоптимальные запросы")]
        public IList<T> GetAll(System.Linq.Expressions.Expression<Func<T, bool>> expression)
        {
            return GetAllEx(x => x.Where(expression));
        }

        #endregion GetAll
    }
}