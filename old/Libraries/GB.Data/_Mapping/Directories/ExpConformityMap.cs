﻿using Uviri.MKRF.Entities.Directories;

namespace Uviri.MKRF.Mapping.Directories
{
    public class ExpConformityMap : ClassMapBase<ExpConformity>
	{
        public ExpConformityMap()
            : base("Exp_Conformity")
		{
//            Table("w_");

//            Id(x => x.Id, "Exp_Conformity_ID").GeneratedBy.Custom(typeof(IdGenerator));
			

//            SqlInsert(@"exec ZZZ_Prc_Exp_Conformity_Insert
//						  @Exp_Conformity_Code = ?
//						 ,@Exp_Conformity_Name = ?
//						 ,@Exp_Obj_Type_ID = ?
//                         ,@Exp_Conformity_ID = ?").Check.None();


//            SqlUpdate(@"exec ZZZ_Prc_Exp_Conformity_Update
//						  @Exp_Conformity_Code = ?
//						 ,@Exp_Conformity_Name = ?
//						 ,@Exp_Obj_Type_ID = ?
//                         ,@Exp_Conformity_ID = ?").Check.None();

//            SqlDelete(@"exec ZZZ_Prc_Exp_Conformity_Delete
//						  @Exp_Conformity_ID = ?").Check.None();

		}

        protected override void InitMap()
        {
            Map(x => x.Code, "Exp_Conformity_Code").Nullable();
			Map(x => x.Name, "Exp_Conformity_Name").Nullable();
			Map(x => x.ExpObjectType, "Exp_Obj_Type_ID").CustomType(typeof(ExpObjectTypeEnum)).Not.Nullable();
        }
    }
}
