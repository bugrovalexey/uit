﻿namespace bgTeam.IsRui.Story.Activities
{
    public class WorkflowUserConclusionSaveOrUpdateStoryContext
    {
        public int? Id { get; set; }

        public int? ActivityId { get; set; }

        public string Text { get; set; }
    }
}