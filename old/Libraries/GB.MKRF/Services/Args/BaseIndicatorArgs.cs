﻿using System;
using GB.Data.Plans;

namespace GB.MKRF.Services.Args
{
    public class BaseIndicatorArgs : BaseArgsNew
    {
        /// <summary>
        /// Пояснение связи с показателем проекта
        /// </summary>
        public string ExplanationRelationshipWithProjectMark { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Базовое значение
        /// </summary>
        public int? BaseValue { get; set; }

        /// <summary>
        /// Целевое значение
        /// </summary>
        public int? TargetedValue { get; set; }

        /// <summary>
        /// Id единицы измерения
        /// </summary>
        public int? UnitId { get; set; }

        /// <summary>
        /// Дата достижения целевого значения
        /// </summary>
        public DateTime? DateToAchieveGoalValue { get; set; }

        /// <summary>
        /// Мероприятие
        /// </summary>
        public PlansActivity PlansActivity { get; set; }

        /// <summary>
        /// Id Мероприятия
        /// </summary>
        public int PlansActivityId { get; set; }
    }
}