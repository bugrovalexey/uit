﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace bgTeam.IsRui.DataAccess
{
    public interface IDataTransaction : IDisposable
    {
        bool IsProgress { get; }

        bool IsCommitted { get; }

        bool IsRolledBack { get; }

        Task CommitAsync(CancellationToken cancellationToken = default);

        Task RollbackAsync(CancellationToken cancellationToken = default);
    }
}