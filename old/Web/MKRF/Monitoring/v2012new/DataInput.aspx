﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="DataInput.aspx.cs" Inherits="GB.MKRF.Web.Monitoring.v2012new.DataInput" %>

<%--<%@ Register Assembly="DevExpress.XtraCharts.v14.1, Version=14.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>--%>

<%@ Register Src="~/Common/GridColumnManager.ascx" TagName="GridColumnManager" TagPrefix="gcm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="center" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            document.getElementById('trInputDataWithPlans').style.display = 'none';
            document.getElementById('trPlansSendToExpertise').style.display = 'none';
            //document.getElementById('trPlansSendToExpertise_add').style.display = 'none';
            document.getElementById('trPlansNotExist').style.display = 'none';
            document.getElementById('trPlansAccord').style.display = 'none';
        });

        
            $('.arrow_back').css('display', 'none');

        function InputDataWithPlans(e) {
            document.getElementById('trInputDataWithPlans').style.display = '';
            document.getElementById('trPlansSendToExpertise').style.display = 'none';
            //document.getElementById('trPlansSendToExpertise_add').style.display = 'none';
            document.getElementById('trPlansNotExist').style.display = 'none';
            document.getElementById('trPlansAccord').style.display = 'none';
            document.getElementById('caption').innerHTML = 'Вносятся данные по планам информатизации';
            //document.getElementById('caption').style.color = '#596183';
        }

        function PlansSendToExpertise(e) {
            document.getElementById('trInputDataWithPlans').style.display = 'none';
            document.getElementById('trPlansSendToExpertise').style.display = '';
            //document.getElementById('trPlansSendToExpertise_add').style.display = '';
            document.getElementById('trPlansNotExist').style.display = 'none';
            document.getElementById('trPlansAccord').style.display = 'none';
            document.getElementById('caption').innerHTML = 'Планы, направленные на согласование';
            //document.getElementById('caption').style.color = '#6b92b2';
        }

        function PlansNotExist(e) {
            document.getElementById('trInputDataWithPlans').style.display = 'none';
            document.getElementById('trPlansSendToExpertise').style.display = 'none';
            //document.getElementById('trPlansSendToExpertise_add').style.display = 'none';
            document.getElementById('trPlansNotExist').style.display = '';
            document.getElementById('trPlansAccord').style.display = 'none';
            document.getElementById('caption').innerHTML = 'По планам информатизации не вводились данные';
            //document.getElementById('caption').style.color = '#bac449';
        }

        function PlansAccord(e) {
            document.getElementById('trInputDataWithPlans').style.display = 'none';
            document.getElementById('trPlansSendToExpertise').style.display = 'none';
            //document.getElementById('trPlansSendToExpertise_add').style.display = 'none';
            document.getElementById('trPlansNotExist').style.display = 'none';
            document.getElementById('trPlansAccord').style.display = '';
            document.getElementById('caption').innerHTML = 'Согласованные планы информатизации';
            //document.getElementById('caption').style.color = '#FFA500';
        }
        
        function reloadPage() {

            pageUrl = 'DataInput.aspx';

            var args = [];
            args.push('Year=' + YearsComboBox.GetValue());
            args.push('DepType=0');// + OGVTypeComboBox.GetValue());

            window.open(pageUrl + "?" + args.join('&'), '_self');
        }
    </script>
    <div style="width: 100%; " align="center">
    <table width="1000px">
         <tr>
            <td align="center">
                <table style="width: 150px">
                    <tr>
                        <td style="width: 1%; white-space:nowrap">Год плана:</td>
                        <td style="width: 1%">
                            <dxe:ASPxComboBox runat="server" ID="YearsComboBox" Width="80px" ValueType="System.Int32"
                                ValueField="Year" TextField="Year" ClientInstanceName="YearsComboBox" OnDataBinding="YearsComboBox_DataBinding">
                                <ClientSideEvents SelectedIndexChanged="function(s,e) { reloadPage();  }" />
                            </dxe:ASPxComboBox>
                        </td>
                        <%--<td style="width: 1%; white-space:nowrap; padding-left:30px">
                            Тип организации:
                        </td>
                        <td style="width: 97%;">
                            <dxe:ASPxComboBox runat="server" ID="OGVTypeComboBox" Width="180px" ValueType="System.Int32"
                                ClientInstanceName="OGVTypeComboBox">
                                <ClientSideEvents SelectedIndexChanged="function(s,e) { reloadPage();  }" />
                                <Items>
                                    <dxe:ListEditItem Text="Все" Value="0" />
                                    <dxe:ListEditItem Text="Департаменты" Value="3" />
                                    <dxe:ListEditItem Text="Тер. органы" Value="2" />
                                    <dxe:ListEditItem Text="Подведомственные" Value="1" />
                                </Items>
                            </dxe:ASPxComboBox>
                        </td>--%>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td style="width: 650px;"> 
                <dxchartsui:WebChartControl runat="server" Width="650px" Height="500px" SideBySideEqualBarWidth="True"
                    ID="Chart" ClientInstanceName="Chart" AppearanceName="In A Fog" PaletteName="Origin">
                    <padding left="0" top="0" right="0" bottom="0"></padding>
                    <borderoptions visible="False" />

                    <BackImage ImageUrl="/Content/Style/Coord/backgrounds/diagram_back.jpg"></BackImage>

<BorderOptions Visible="False"></BorderOptions>
                    <diagramserializable>
                        <dxcharts:XYDiagram>
                            <AxisX VisibleInPanesSerializable="-1"><Range SideMarginsEnabled="True"></Range></AxisX>

                            <AxisY Title-Text="Количество департаментов" Title-Visible="True" 
                                VisibleInPanesSerializable="-1" GridSpacingAuto="False" GridSpacing="5"><Label Antialiasing="True"></Label><Range SideMarginsEnabled="True"></Range></AxisY>

                            <DefaultPane BackColor="White" EnableAxisXScrolling="False" EnableAxisYScrolling="False" EnableAxisXZooming="False" EnableAxisYZooming="False"></DefaultPane>
                        </dxcharts:XYDiagram>
                    </diagramserializable>
                    <fillstyle>
                        <OptionsSerializable>
                            <dxcharts:SolidFillOptions></dxcharts:SolidFillOptions>
                        </OptionsSerializable>
                    </fillstyle>
                    <legend alignmenthorizontal="Center" alignmentvertical="BottomOutside" visible="False"
                        font="Tahoma, 12pt" equallyspaceditems="False"></legend>
                    <titles>
		                <dxcharts:ChartTitle WordWrap="True" MaximumLinesCount="4" Text="Состояние ввода данных" Font="Tahoma, 14pt"/>
	                </titles>
                    <seriesserializable>
                    <dxcharts:Series Name="PlansNotExist" LegendText="По планам информатизации не вносились данные">
                        <Points>
                            <dxcharts:SeriesPoint Values="5" ArgumentSerializable=" "></dxcharts:SeriesPoint>
                        </Points>
                        <ViewSerializable>
                            <dxcharts:SideBySideBarSeriesView>
                                <fillstyle fillmode="Gradient">
                                    <optionsserializable>
                                        <dxcharts:RectangleGradientFillOptions GradientMode="LeftToRight" />
                                    </optionsserializable>
                                </fillstyle>
                            </dxcharts:SideBySideBarSeriesView>
                        </ViewSerializable>
                        <LabelSerializable>
                            <dxcharts:SideBySideBarSeriesLabel BackColor="Transparent" Font="Tahoma, 12pt" Antialiasing="True" LineVisible="False">
                                <Border Visible="False"></Border>
                                <FillStyle>
                                    <OptionsSerializable>
                                        <dxcharts:SolidFillOptions></dxcharts:SolidFillOptions>
                                    </OptionsSerializable>
                                </FillStyle>
                            </dxcharts:SideBySideBarSeriesLabel>
                        </LabelSerializable>
                        <PointOptionsSerializable>
                            <dxcharts:PointOptions></dxcharts:PointOptions>
                        </PointOptionsSerializable>
                        <LegendPointOptionsSerializable>
                            <dxcharts:PointOptions></dxcharts:PointOptions>
                        </LegendPointOptionsSerializable>
                    </dxcharts:Series>
                    <dxcharts:Series Name="InputDataWithPlans" LegendText="Вносятся данные по планам информатизации">
                        <Points>
                            <dxcharts:SeriesPoint Values="20" ArgumentSerializable=" "></dxcharts:SeriesPoint>
                        </Points>
                        <ViewSerializable>
                            <dxcharts:SideBySideBarSeriesView>
                                <fillstyle fillmode="Gradient">
                                    <optionsserializable>
                                        <dxcharts:RectangleGradientFillOptions GradientMode="LeftToRight" />
                                    </optionsserializable>
                                </fillstyle>
                            </dxcharts:SideBySideBarSeriesView>
                        </ViewSerializable>
                        <LabelSerializable>
                                <dxcharts:SideBySideBarSeriesLabel BackColor="Transparent" Font="Tahoma, 12pt" Antialiasing="True" LineVisible="False">
                                    <Border Visible="False"></Border>
                                    <FillStyle>
                                        <OptionsSerializable>
                                            <dxcharts:SolidFillOptions></dxcharts:SolidFillOptions>
                                        </OptionsSerializable>
                                    </FillStyle>
                                </dxcharts:SideBySideBarSeriesLabel>
                        </LabelSerializable>
                        <PointOptionsSerializable>
                            <dxcharts:PointOptions></dxcharts:PointOptions>
                        </PointOptionsSerializable>
                        <LegendPointOptionsSerializable>
                            <dxcharts:PointOptions></dxcharts:PointOptions>
                        </LegendPointOptionsSerializable>
                    </dxcharts:Series>
                    <dxcharts:Series Name="PlansSendToExpertise" LegendText="Планы, направленные на согласование">
                        <Points>
                            <dxcharts:SeriesPoint Values="30" ArgumentSerializable=" "></dxcharts:SeriesPoint>
                        </Points>
                        <ViewSerializable>
                            <dxcharts:SideBySideBarSeriesView>
                                <fillstyle fillmode="Gradient">
                                    <optionsserializable>
                                        <dxcharts:RectangleGradientFillOptions GradientMode="LeftToRight" />
                                    </optionsserializable>
                                </fillstyle>
                            </dxcharts:SideBySideBarSeriesView>
                        </ViewSerializable>
                        <LabelSerializable>
                            <dxcharts:SideBySideBarSeriesLabel BackColor="Transparent" Font="Tahoma, 12pt" Antialiasing="True" LineVisible="False">
                                <Border Visible="False"></Border>
                                <FillStyle>
                                    <OptionsSerializable>
                                    <dxcharts:SolidFillOptions></dxcharts:SolidFillOptions>
                                    </OptionsSerializable>
                                </FillStyle>
                            </dxcharts:SideBySideBarSeriesLabel>
                        </LabelSerializable>
                        <PointOptionsSerializable>
                            <dxcharts:PointOptions></dxcharts:PointOptions>
                        </PointOptionsSerializable>
                        <LegendPointOptionsSerializable>
                            <dxcharts:PointOptions></dxcharts:PointOptions>
                        </LegendPointOptionsSerializable>
                    </dxcharts:Series>
                    
                    <dxcharts:Series Name="PlansAccord" LegendText="Согласованные планы информатизации">
                        <Points>
                            <dxcharts:SeriesPoint Values="30" ArgumentSerializable=" "></dxcharts:SeriesPoint>
                        </Points>
                        <ViewSerializable>
                            <dxcharts:SideBySideBarSeriesView>
                                <fillstyle fillmode="Gradient">
                                    <optionsserializable>
                                        <dxcharts:RectangleGradientFillOptions GradientMode="LeftToRight" />
                                    </optionsserializable>
                                </fillstyle>
                            </dxcharts:SideBySideBarSeriesView>
                        </ViewSerializable>
                        <LabelSerializable>
                            <dxcharts:SideBySideBarSeriesLabel BackColor="Transparent" Font="Tahoma, 12pt" Antialiasing="True" LineVisible="False">
                                <Border Visible="False"></Border>
                                <FillStyle>
                                    <OptionsSerializable>
                                    <dxcharts:SolidFillOptions></dxcharts:SolidFillOptions>
                                    </OptionsSerializable>
                                </FillStyle>
                            </dxcharts:SideBySideBarSeriesLabel>
                        </LabelSerializable>
                        <PointOptionsSerializable>
                            <dxcharts:PointOptions></dxcharts:PointOptions>
                        </PointOptionsSerializable>
                        <LegendPointOptionsSerializable>
                            <dxcharts:PointOptions></dxcharts:PointOptions>
                        </LegendPointOptionsSerializable>
                    </dxcharts:Series>
                    </seriesserializable>
                    <seriestemplate>
                        <ViewSerializable>
                            <dxcharts:SideBySideBarSeriesView></dxcharts:SideBySideBarSeriesView>
                        </ViewSerializable>
                        <LabelSerializable>
                            <dxcharts:SideBySideBarSeriesLabel LineVisible="True">
                                <FillStyle>
                                    <OptionsSerializable>
                                        <dxcharts:SolidFillOptions></dxcharts:SolidFillOptions>
                                    </OptionsSerializable>
                                </FillStyle>
                            </dxcharts:SideBySideBarSeriesLabel>
                        </LabelSerializable>
                        <PointOptionsSerializable>
                            <dxcharts:PointOptions></dxcharts:PointOptions>
                        </PointOptionsSerializable>
                        <LegendPointOptionsSerializable>
                            <dxcharts:PointOptions></dxcharts:PointOptions>
                        </LegendPointOptionsSerializable>
                    </seriestemplate>
                    <titles>
                        <dxcharts:ChartTitle WordWrap="True" MaximumLinesCount="4" Text="" 
                            Font="Tahoma, 14pt"></dxcharts:ChartTitle>
                    </titles>
                </dxchartsui:WebChartControl>
            </td>
            <td align="left" style="width: 350px;">
                <table >
                    <tbody>
                        <tr>
                            <td style="width: 30px; height: 22px; background-color: #596183">
                            </td>
                            <td style="width: 320px; height: 22px">
                                <dxe:ASPxButton ID="PlansNotExistButton" runat="server" Text="По планам информатизации не вносились данные"
                                    Width="320px" Height="100%" HorizontalAlign="Left" AutoPostBack="False">
                                    <ClientSideEvents Click="function(s,e) {PlansNotExist();}" />
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30px; height: 22px; background-color: #6b92b2">
                            </td>
                            <td style="width: 320px; height: 22px">
                                <dxe:ASPxButton ID="InputDataWithPlansButton" runat="server" Text="Вносятся данные по планам информатизации"
                                    Width="320px" Height="100%" HorizontalAlign="Left" AutoPostBack="False">
                                    <ClientSideEvents Click="function(s,e) {InputDataWithPlans();}" />
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30px; height: 22px; background-color: #bac449">
                            </td>
                            <td style="width: 320px; height: 22px">
                                <dxe:ASPxButton ID="PlansSendToExpertiseButton" runat="server" Text="Планы, направленные на согласование"
                                    Width="320px" Height="100%" HorizontalAlign="Left" AutoPostBack="False">
                                    <ClientSideEvents Click="function(s,e) {PlansSendToExpertise();}" />
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30px; height: 22px; background-color: #FFA500">
                            </td>
                            <td style="width: 320px; height: 22px">
                                <dxe:ASPxButton ID="PlansAccordButton" runat="server" Text="Согласованные планы информатизации"
                                    Width="320px" Height="100%" HorizontalAlign="Left" AutoPostBack="False">
                                    <ClientSideEvents Click="function(s,e) {PlansAccord();}" />
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td id="caption" style="font-size: medium; font-weight: bold;">
            </td>
        </tr>
        <tr id="trInputDataWithPlans">
            <td colspan="2">
                <dxe:ASPxButton runat="server" ID="InputDataWithPlansToExcelButton" ClientInstanceName="InputDataWithPlansToExcelButton"
                    Text="Выгрузить в Excel" onclick="InputDataWithPlansToExcelButton_Click" Style="margin: 10px 0" CssClass="fleft">
                </dxe:ASPxButton>
                <gcm:GridColumnManager ID="GridColumnManager" runat="server" GridName="InputDataWithPlansGrid" UserStyle="float:right"/>
                <dxwgv:ASPxGridView runat="server" ID="InputDataWithPlansGrid" KeyFieldName="Id"
                    Width="100%" AutoGenerateColumns="False" ClientInstanceName="InputDataWithPlansGrid"
                    OnDataBinding="InputDataWithPlansGrid_DataBinding">
                    <Columns>
                        <dxwgv:GridViewDataTextColumn FieldName="DepartmentName">
                            <DataItemTemplate>
                                <%#string.Format("<a class='gridLink' href='{0}'>{1}</a>", GB.MKRF.Web.Plans.PlanCard.GetUrl((int)Eval("Plans_Id"), GB.MKRF.Entities.Directories.ExpObjectTypeEnum.Plan2012new), Eval("DepartmentName").ToString()) %>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Количество мероприятий" FieldName="Plans_Activity_Count">
                            <Settings AutoFilterCondition="Contains" ShowInFilterControl="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewBandColumn Caption="Итого, тыс. руб" Name="PlanActivity2012Total" >
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="PlanActivity2012TotalY0">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="PlanActivity2012TotalY1">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="PlanActivity2012TotalY2">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                        <%--<dxwgv:GridViewDataTextColumn Caption="Куратор" FieldName="Curator"
                            Visible="false">
                            <Settings AutoFilterCondition="Contains" />
                        </dxwgv:GridViewDataTextColumn>--%>
                        <dxwgv:GridViewDataTextColumn Caption="Ответственный за подготовку планов" FieldName="User_Responsible" Visible="false">
                            <Settings AutoFilterCondition="Contains" ShowInFilterControl="True" />
                        </dxwgv:GridViewDataTextColumn>

                    </Columns>
                    <Settings ShowFilterRow="True" ShowHeaderFilterBlankItems="False" />
                    <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
                    <Styles>
                        <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        </Header>
                    </Styles>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
        <tr id="trPlansSendToExpertise">
            <td colspan="2" >
                <dxe:ASPxButton runat="server" ID="PlansSendToExpertiseToExcelButton" ClientInstanceName="PlansSendToExpertiseToExcelButton"
                                Text="Выгрузить в Excel" onclick="PlansSendToExpertiseToExcelButton_Click" Style="margin: 10px 0" CssClass="fleft">
                            </dxe:ASPxButton>
                <gcm:GridColumnManager ID="GridColumnManager1" runat="server" GridName="PlansSendToExpertiseGrid" UserStyle="float:right"/>
                
                <dxwgv:ASPxGridView runat="server" ID="PlansSendToExpertiseGrid" KeyFieldName="Id"
                    Width="100%" AutoGenerateColumns="False" ClientInstanceName="PlansSendToExpertiseGrid"
                    OnDataBinding="PlansSendToExpertiseGrid_DataBinding">
                    <Columns>
                        <dxwgv:GridViewDataTextColumn FieldName="DepartmentName">
                            <Settings AutoFilterCondition="Contains" ShowInFilterControl="True" />
                            <DataItemTemplate>
                                <%#string.Format("<a class='gridLink' href='{0}'>{1}</a>", GB.MKRF.Web.Plans.PlanCard.GetUrl((int)Eval("Plans_Id"), GB.MKRF.Entities.Directories.ExpObjectTypeEnum.Plan2012new), Eval("DepartmentName").ToString()) %>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Количество мероприятий" FieldName="Plans_Activity_Count">
                            <Settings AutoFilterCondition="Contains" ShowInFilterControl="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewBandColumn Caption="Итого, тыс. руб" Name="PlanActivity2012Total" >
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="PlanActivity2012TotalY0">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="PlanActivity2012TotalY1">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="PlanActivity2012TotalY2">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                        <%--<dxwgv:GridViewDataTextColumn Caption="Куратор" FieldName="Curator" Visible="false">
                            <Settings AutoFilterCondition="Contains" />
                        </dxwgv:GridViewDataTextColumn>--%>
                        <dxwgv:GridViewDataTextColumn Caption="Ответственный за подготовку планов" FieldName="User_Responsible" Visible="False">
                            <Settings AutoFilterCondition="Contains" ShowInFilterControl="True" />
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <Settings ShowFilterRow="True" ShowHeaderFilterBlankItems="False" />
                    <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
                    <Styles>
                        <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        </Header>
                    </Styles>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
        <tr id="trPlansNotExist">
            <td colspan="2" >
                <dxe:ASPxButton runat="server" ID="PlansNotExistToExcelButton" ClientInstanceName="PlansNotExistToExcelButton"
                    Text="Выгрузить в Excel" OnClick="PlansNotExistToExcelButton_Click" Style="margin: 10px 0" CssClass="fleft">
                </dxe:ASPxButton>
                <dxwgv:ASPxGridView runat="server" ID="PlansNotExistGrid" KeyFieldName="Id" Width="100%"
                    AutoGenerateColumns="False" ClientInstanceName="PlansNotExistGrid" OnDataBinding="PlansNotExistGrid_DataBinding">
                    <Columns>
                        <dxwgv:GridViewDataTextColumn FieldName="DepartmentName">
                            <Settings AutoFilterCondition="Contains" ShowInFilterControl="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Ответственный за подготовку планов" FieldName="User_Responsible">
                            <Settings AutoFilterCondition="Contains" ShowInFilterControl="True" />
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <Settings ShowFilterRow="True" ShowHeaderFilterBlankItems="False" />
                    <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
                    <Styles>
                        <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    </Styles>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
        <tr id="trPlansAccord">
            <td colspan="2" >
                <dxe:ASPxButton runat="server" ID="PlansAccordToExcelButton" ClientInstanceName="PlansAccordToExcelButton"
                                Text="Выгрузить в Excel" onclick="PlansAccordToExcelButton_Click" Style="margin: 10px 0" CssClass="fleft">
                            </dxe:ASPxButton>
                <gcm:GridColumnManager ID="GridColumnManager3" runat="server" GridName="PlansAccordGrid" UserStyle="float:right"/>
                <dxwgv:ASPxGridView runat="server" ID="PlansAccordGrid" KeyFieldName="Id"
                    Width="100%" AutoGenerateColumns="False" ClientInstanceName="PlansAccordGrid"
                    OnDataBinding="PlansAccordGrid_DataBinding">
                    <Columns>
                        <dxwgv:GridViewDataTextColumn FieldName="DepartmentName">
                            <DataItemTemplate>
                                <%#string.Format("<a class='gridLink' href='{0}'>{1}</a>", GB.MKRF.Web.Plans.PlanCard.GetUrl((int)Eval("Plans_Id"), GB.MKRF.Entities.Directories.ExpObjectTypeEnum.Plan2012new), Eval("DepartmentName").ToString()) %>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Количество мероприятий" FieldName="Plans_Activity_Count">
                            <Settings AutoFilterCondition="Contains" ShowInFilterControl="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewBandColumn Caption="Итого, тыс. руб" Name="PlanActivity2012Total" VisibleIndex="3">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="очередной финансовый год" FieldName="PlanActivity2012TotalY0">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="первый год планового периода" FieldName="PlanActivity2012TotalY1">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="второй год планового периода" FieldName="PlanActivity2012TotalY2">
                                    <PropertiesTextEdit DisplayFormatString="#,##0.0000" />
                                    <Settings AutoFilterCondition="Contains" ShowFilterRowMenu="True" />
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                        </dxwgv:GridViewBandColumn>
                        <%--<dxwgv:GridViewDataTextColumn Caption="Куратор" FieldName="Curator" Visible="false">
                            <Settings AutoFilterCondition="Contains" />
                        </dxwgv:GridViewDataTextColumn>--%>
                        <dxwgv:GridViewDataTextColumn Caption="Ответственный за подготовку планов" FieldName="User_Responsible" Visible="false">
                            <Settings AutoFilterCondition="Contains" ShowInFilterControl="True" />
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <Settings ShowFilterRow="True" ShowHeaderFilterBlankItems="False" />
                    <SettingsCustomizationWindow Enabled="True" PopupHorizontalAlign="Center" PopupVerticalAlign="WindowCenter" />
                    <Styles>
                        <Header Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        </Header>
                    </Styles>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
    </table>
    </div>
    <dxwgve:ASPxGridViewExporter runat="server" ID="ToExcelExporter">
    </dxwgve:ASPxGridViewExporter>
</asp:Content>
