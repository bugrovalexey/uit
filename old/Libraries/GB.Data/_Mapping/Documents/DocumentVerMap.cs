﻿using GB.MKRF.Entities.Admin;
using GB.MKRF.Entities.Documents;
using GB.MKRF.Entities.Plans;

//using Uviri.MKRF.Entities.OtherExpObjects;

namespace GB.MKRF.Mapping.Documents
{
    class DocumentVerMap : ClassMapBase<DocumentVer>
    {
        public DocumentVerMap()
            : base("Document_Ver")
        {
        }

        protected override void InitMap()
        {
            Map(x => x.Num, "Document_Ver_Num");
            Map(x => x.Date, "Document_Ver_DT");
            Map(x => x.Name, "Document_Ver_Name");
            Map(x => x.File, "Document_Ver_File").Length(int.MaxValue).LazyLoad();
            Map(x => x.FileName, "Document_Ver_File_Name");
            Map(x => x.ContentType, "Document_Ver_Content_Type");
            Map(x => x.Comm, "Document_Ver_Comm");
            Map(x => x.IsSigned).Formula("case when DATALENGTH(Document_Ver_Sign_File) > 0 then 1 else 0 end").ReadOnly();
            Map(x => x.SignedPackage, "Document_Ver_Sign_File").CustomType("BinaryBlob").Length(int.MaxValue).LazyLoad();
            Map(x => x.SignType, "Document_Ver_Sign_Type");
            Map(x => x.SignInfo, "Document_Ver_ECP");

            Map(x => x.DocTypeText, "Document_Ver_Doc_Type");
            Map(x => x.DocType, "Doc_Type_ID").CustomType(typeof(DocTypeEnum)).Not.Nullable();
            Map(x => x.DocGroup, "Deed_Type_ID").CustomType(typeof(DocGroupEnum)).Not.Nullable();
            Map(x => x.DateCreate, "Document_Ver_Create_DT").ReadOnly();
            Map(x => x.DecisionGovComission, "Document_Ver_Is_Decision");

            ReferencesAny(x => x.Owner)
                .AddMetaValue<ExpDemand>(((int)TableTypeEnum.Exp_Demand).ToString())
                //.AddMetaValue<ExpObjectDemand>(((int)TableTypeEnum.ExpObjectDemand).ToString())
                //.AddMetaValue<ExpConclusion>(((int)TableTypeEnum.Exp_Conclusion).ToString())
                //.AddMetaValue<ExpObjectConclusion>(((int)TableTypeEnum.ExpObjectConclusion).ToString())
                //.AddMetaValue<UserRequest>(((int)TableTypeEnum.UserRequest).ToString())
                .AddMetaValue<PlansActivityReason>(((int)TableTypeEnum.ActivityReason).ToString())
                .EntityTypeColumn("Portal_Table_ID")
                .EntityIdentifierColumn("Document_Ver_Owner_ID").IdentityType<int>();

            References<User>(x => x.Author, "Document_Ver_Creator").NotFound.Ignore().Not.Nullable().ReadOnly();
        }
    }
}