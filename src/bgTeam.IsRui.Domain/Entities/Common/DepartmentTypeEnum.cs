﻿namespace bgTeam.IsRui.Domain.Entities.Common
{
    /// <summary>
    /// Типы ведомств
    /// </summary>
    public enum DepartmentTypeEnum
    {
        /// <summary>Не задано</summary>
        None = 0,

        /// <summary>Ведомство</summary>
        Establishment = 1,

        /// <summary>Регион</summary>
        Region = 2,

        /// <summary>Департамент</summary>
        Department = 3
    }
}